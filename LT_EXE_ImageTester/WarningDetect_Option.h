#pragma once

#include "EtcModelDlg.h"
#include "afxcmn.h"
#include "afxwin.h"

// CWarningDetect_Option 대화 상자입니다.

typedef struct _EstCode{
	int m_Code;
	CString str_Name;
	CString str_DATA;
}tEstCode;

struct WarningData{
	BOOL	enable;

	CString testName[50];	
	bool WarningCheckResult[50];
	double templateRatio[50];
};

class CWarningDetect_Option : public CDialog
{
	DECLARE_DYNAMIC(CWarningDetect_Option)

public:
	CWarningDetect_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CWarningDetect_Option();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_WARNING };
	void Setup(CWnd* IN_pMomWnd);
	void Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
//	TESTData TestData;
	tINFO		m_INFO;
	WarningData m_sWarningData;
	BOOL	WarningChange(CString FName);
	BOOL	Warning_Etc_Compare(int num);
	void	Warning_INIT_TEST();
	void	Warning_END_TEST(); 

	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();

	//EVMS 적용
	void	InitEVMS();

	int Lot_StartCnt;
	void	LOT_InsertDataList();

	tResultVal Run();
	void	Pic(CDC *cdc);

	void	InitPrm();
	int		Old_ModelCode;
	CWnd		*m_pMomWnd;	
	CEdit		*pTStat;
	CString		str_model;
	CString		str_ModelPath;
	CString		strSelectedModelPath;
	BOOL		GetWarningImage(BYTE lencode,BYTE rescod,CString Filename);
	void		EtcCamInit();
	void		UpateMasterWarningFileList();
	void		UpateMasterWarningFileList_ORG();
	void		ShowWarningImage(IplImage *srcImage);
	void		Save_parameter();
	void		Load_parameter();
	int			m_dMatchingRatio;
	int			m_number;

	tEstCode	ResCode[20];
	tEstCode	LangCode[31];
	void	DefaultCodeSet();

	bool CompareWarning(LPBYTE IN_RGBIn_Target, IplImage *master_image,WarningData *sWarningData, int Cnt);
	bool CompareWarning(LPBYTE IN_RGBIn_Target, IplImage *master_image, int start_x, int start_y, int end_x, int end_y, double threshold, WarningData *sWarningData, int Cnt);	
	bool CompareWarningOnUltimateImage(LPBYTE IN_RGBIn_Target, IplImage *master_image, int start_x, int start_y, int end_x, int end_y, double threshold);

	int m_dTemplateRatio[50];
	bool m_dResult[50];

	void SETTING_DATA();	
	void InsertList();

	int InsertIndex;
	int ListItemNum;

	CString TESTLOT[100];
	int TEST_LIST_COUNT;
	CString TESTNAME[40];

	void Set_List(CListCtrl *List);
	void LOT_Set_List(CListCtrl *List);
	int	Lot_InsertNum;

	void WarningList_Set();
	void EXCEL_SAVE();
	void LOT_EXCEL_SAVE();
	bool EXCEL_UPLOAD();
	bool LOT_EXCEL_UPLOAD();
	BOOL	b_StopFail;
	void FAIL_UPLOAD();
	void Setup_List();
	int StartCnt;

	int		m_Resum;
	int		m_WModeNum;
	int m_WNum;
	CString str_WMode;
	void	EnableEndRststate(BOOL state);
	IplImage* GetWarningArea(IplImage *srcImage);
private:	
	CComboBox	*pResCombo;
	int			m_nWarningListCnt;
	CString		m_szWarningListFile[60];			// 검사언어 항목수		

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_WarningWorkList;
	CListBox m_lsWarningCheckList;
	CListBox m_lsWarningExtrCandidate;
	CListBox m_lsWarningTotal;
	virtual BOOL OnInitDialog();
	afx_msg void OnLbnSelchangeListWarningTotal();
	afx_msg void OnBnClickedBtnTotal2candidate();
	afx_msg void OnBnClickedBtnCandidate2total();
	afx_msg void OnBnClickedBtnWarningMasterExtact();
	CStatic m_stWarningFrame;
	int m_dWarningStart_x;
	int m_dWarningStart_y;
	int m_dWarningEnd_x;
	int m_dWarningEnd_y;
	afx_msg void OnLbnSelchangeListWarning();
	afx_msg void OnBnClickedBtnWarningChecklistDelete();
	afx_msg void OnBnClickedBtnWarningSave();
	afx_msg void OnBnClickedBtnSetWarningArea();
	//	BOOL b_Reg_En;
	//	afx_msg void OnBnClickedChkReg();
	afx_msg void OnCbnSelchangeComboRes();
	BOOL b_Chk_ResEx;
	afx_msg void OnBnClickedCheckResex();
	afx_msg void OnLbnSelchangeListWarningCandidate();
//	afx_msg void OnCbnSelchangeComboWmode();
	afx_msg void OnEnChangeEditTestName();
//	afx_msg void OnCbnSelchangeComboWmode2();
	afx_msg void OnBnClickedBtnTotal2candidate2();
	afx_msg void OnBnClickedBtnCandidate2total2();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	BOOL b_Chk_EndRst;
	afx_msg void OnBnClickedCheckReset();
	afx_msg void OnBnClickedBtnWarnigAdd();
	CString str_List_End;
	afx_msg void OnBnClickedBtnWarnigDel();
	afx_msg void OnBnClickedBtnWarningInit();
	afx_msg void OnBnClickedBtnWarningTest();
	afx_msg void OnBnClickedBtnCandidate2clr();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	CListCtrl m_Lot_WarningWorkList;
};
