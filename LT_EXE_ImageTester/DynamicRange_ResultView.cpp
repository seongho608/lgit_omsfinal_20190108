// DynamicRange_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "DynamicRange_ResultView.h"
#include "afxdialogex.h"


// CDynamicRange_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDynamicRange_ResultView, CDialog)

CDynamicRange_ResultView::CDynamicRange_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CDynamicRange_ResultView::IDD, pParent)
{

}

CDynamicRange_ResultView::~CDynamicRange_ResultView()
{
}

void CDynamicRange_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDynamicRange_ResultView, CDialog)
	ON_WM_CTLCOLOR()
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGETXT, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangetxt)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGENUM, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGENUM5, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum5)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGERESULT, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangeresult)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGETXT2, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangetxt2)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGENUM2, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum2)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGENUM6, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum6)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGERESULT2, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangeresult2)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGETXT3, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangetxt3)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGENUM3, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum3)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGENUM7, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum7)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGERESULT3, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangeresult3)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGETXT4, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangetxt4)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGENUM4, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum4)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGENUM8, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum8)
	ON_EN_SETFOCUS(IDC_EDIT_DYNAMICRANGERESULT4, &CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangeresult4)
END_MESSAGE_MAP()


// CDynamicRange_ResultView 메시지 처리기입니다.
void CDynamicRange_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd = IN_pMomWnd;
}

HBRUSH CDynamicRange_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGETXT){
		pDC->SetTextColor(RGB(255, 255, 255));

		pDC->SetBkColor(RGB(86, 86, 86));
	}


	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGENUM){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}
	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGENUM5){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGERESULT){
		pDC->SetTextColor(txcol_TVal);
		pDC->SetBkColor(bkcol_TVal);
	}

	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGETXT2){
		pDC->SetTextColor(RGB(255, 255, 255));

		pDC->SetBkColor(RGB(86, 86, 86));
	}


	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGENUM2){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}
	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGENUM6){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}
	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGERESULT2){
		pDC->SetTextColor(txcol_TVal2);
		pDC->SetBkColor(bkcol_TVal2);
	}

	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGETXT3){
		pDC->SetTextColor(RGB(255, 255, 255));

		pDC->SetBkColor(RGB(86, 86, 86));
	}


	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGENUM3){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}
	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGENUM7){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}
	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGERESULT3){
		pDC->SetTextColor(txcol_TVal3);
		pDC->SetBkColor(bkcol_TVal3);
	}

	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGETXT4){
		pDC->SetTextColor(RGB(255, 255, 255));

		pDC->SetBkColor(RGB(86, 86, 86));
	}


	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGENUM4){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}
	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGENUM8){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}
	if (pWnd->GetDlgCtrlID() == IDC_EDIT_DYNAMICRANGERESULT4){
		pDC->SetTextColor(txcol_TVal4);
		pDC->SetBkColor(bkcol_TVal4);
	}
	return hbr;
}


BOOL CDynamicRange_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	Font_Size_Change(IDC_EDIT_DYNAMICRANGETXT, &ft1, 40, 23);
	GetDlgItem(IDC_EDIT_DYNAMICRANGENUM)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_DYNAMICRANGENUM5)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_DYNAMICRANGERESULT)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_DYNAMICRANGETXT2)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_DYNAMICRANGENUM2)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_DYNAMICRANGENUM6)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_DYNAMICRANGERESULT2)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_DYNAMICRANGETXT3)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_DYNAMICRANGENUM3)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_DYNAMICRANGENUM7)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_DYNAMICRANGERESULT3)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_DYNAMICRANGETXT4)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_DYNAMICRANGENUM4)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_DYNAMICRANGENUM8)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_DYNAMICRANGERESULT4)->SetFont(&ft1);


	Initstat();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDynamicRange_ResultView::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDynamicRange_ResultView::Initstat()
{
	RESULT_TEXT(0, "STAND BY");
	DYNAMICRANGENUM_TEXT("");
	DYNAMICRANGE_TEXT("LT");

	RESULT_TEXT2(0, "STAND BY");
	DYNAMICRANGENUM_TEXT2("");
	DYNAMICRANGE_TEXT2("LB");

	RESULT_TEXT3(0, "STAND BY");
	DYNAMICRANGENUM_TEXT3("");
	DYNAMICRANGE_TEXT3("RT");

	RESULT_TEXT4(0, "STAND BY");
	DYNAMICRANGENUM_TEXT4("");
	DYNAMICRANGE_TEXT4("RB");
}
void CDynamicRange_ResultView::Font_Size_Change(int nID, CFont *font, LONG Weight, LONG Height)
{
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = Weight;

	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);

	GetDlgItem(nID)->SetFont(font);
}

void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangetxt()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}


void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}
void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum5()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangeresult()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}


void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangetxt2()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}


void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum2()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}
void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum6()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangeresult2()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangetxt3()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}


void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum3()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}
void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum7()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangeresult3()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangetxt4()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}


void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum4()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}
void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangenum8()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void CDynamicRange_ResultView::OnEnSetfocusEditDynamicrangeresult4()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}
void CDynamicRange_ResultView::DYNAMICRANGE_TEXT(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGETXT))->SetWindowText(lpcszString);
}
void CDynamicRange_ResultView::DYNAMICRANGENUM_TEXT(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGENUM))->SetWindowText(lpcszString);
}
void CDynamicRange_ResultView::DYNAMICRANGENUM_TEXT5(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGENUM5))->SetWindowText(lpcszString);
}
void CDynamicRange_ResultView::RESULT_TEXT(unsigned char col, LPCSTR lpcszString, ...)
{
	if (col == 0){//대기
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}
	else if (col == 1){//성공
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18, 69, 171);
	}
	else if (col == 2){//실패
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}
	else if (col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGERESULT))->SetWindowText(lpcszString);
}
void CDynamicRange_ResultView::DYNAMICRANGE_TEXT2(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGETXT2))->SetWindowText(lpcszString);
}
void CDynamicRange_ResultView::DYNAMICRANGENUM_TEXT2(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGENUM2))->SetWindowText(lpcszString);
}
void CDynamicRange_ResultView::DYNAMICRANGENUM_TEXT6(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGENUM6))->SetWindowText(lpcszString);
}
void CDynamicRange_ResultView::RESULT_TEXT2(unsigned char col, LPCSTR lpcszString, ...)
{
	if (col == 0){//대기
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(47, 157, 39);
	}
	else if (col == 1){//성공
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(18, 69, 171);
	}
	else if (col == 2){//실패
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(201, 0, 0);
	}
	else if (col == 3){
		txcol_TVal2 = RGB(0, 0, 0);
		bkcol_TVal2 = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGERESULT2))->SetWindowText(lpcszString);
}

void CDynamicRange_ResultView::DYNAMICRANGE_TEXT3(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGETXT3))->SetWindowText(lpcszString);
}
void CDynamicRange_ResultView::DYNAMICRANGENUM_TEXT3(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGENUM3))->SetWindowText(lpcszString);
}
void CDynamicRange_ResultView::DYNAMICRANGENUM_TEXT7(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGENUM7))->SetWindowText(lpcszString);
}
void CDynamicRange_ResultView::RESULT_TEXT3(unsigned char col, LPCSTR lpcszString, ...)
{
	if (col == 0){//대기
		txcol_TVal3 = RGB(255, 255, 255);
		bkcol_TVal3 = RGB(47, 157, 39);
	}
	else if (col == 1){//성공
		txcol_TVal3 = RGB(255, 255, 255);
		bkcol_TVal3 = RGB(18, 69, 171);
	}
	else if (col == 2){//실패
		txcol_TVal3 = RGB(255, 255, 255);
		bkcol_TVal3 = RGB(201, 0, 0);
	}
	else if (col == 3){
		txcol_TVal3 = RGB(0, 0, 0);
		bkcol_TVal3 = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGERESULT3))->SetWindowText(lpcszString);
}

void CDynamicRange_ResultView::DYNAMICRANGE_TEXT4(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGETXT4))->SetWindowText(lpcszString);
}
void CDynamicRange_ResultView::DYNAMICRANGENUM_TEXT4(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGENUM4))->SetWindowText(lpcszString);
}
void CDynamicRange_ResultView::DYNAMICRANGENUM_TEXT8(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGENUM8))->SetWindowText(lpcszString);
}
void CDynamicRange_ResultView::RESULT_TEXT4(unsigned char col, LPCSTR lpcszString, ...)
{
	if (col == 0){//대기
		txcol_TVal4 = RGB(255, 255, 255);
		bkcol_TVal4 = RGB(47, 157, 49);
	}
	else if (col == 1){//성공
		txcol_TVal4 = RGB(255, 255, 255);
		bkcol_TVal4 = RGB(18, 69, 171);
	}
	else if (col == 2){//실패
		txcol_TVal4 = RGB(255, 255, 255);
		bkcol_TVal4 = RGB(201, 0, 0);
	}
	else if (col == 3){
		txcol_TVal4 = RGB(0, 0, 0);
		bkcol_TVal4 = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_DYNAMICRANGERESULT4))->SetWindowText(lpcszString);
}
void CDynamicRange_ResultView::DYNAMICRANGENUM_TEXT(int NUM, LPCSTR lpcszString, ...){

	if (NUM == 0)
		DYNAMICRANGENUM_TEXT(lpcszString);
	if (NUM == 1)
		DYNAMICRANGENUM_TEXT2(lpcszString);
	if (NUM == 2)
		DYNAMICRANGENUM_TEXT3(lpcszString);
	if (NUM == 3)
		DYNAMICRANGENUM_TEXT4(lpcszString);
	if (NUM == 4)
		DYNAMICRANGENUM_TEXT5(lpcszString);
	if (NUM == 5)
		DYNAMICRANGENUM_TEXT6(lpcszString);
	if (NUM == 6)
		DYNAMICRANGENUM_TEXT7(lpcszString);
	if (NUM == 7)
		DYNAMICRANGENUM_TEXT8(lpcszString);
}
void CDynamicRange_ResultView::RESULT_TEXT(int NUM, unsigned char col, LPCSTR lpcszString, ...){

	if (NUM == 0)
		RESULT_TEXT(col, lpcszString);
	if (NUM == 1)
		RESULT_TEXT2(col, lpcszString);
	if (NUM == 2)
		RESULT_TEXT3(col, lpcszString);
	if (NUM == 3)
		RESULT_TEXT4(col, lpcszString);

}