#pragma once


// CNTSC_ResultView 대화 상자입니다.

class CNTSC_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CNTSC_ResultView)

public:
	CNTSC_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CNTSC_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_NTSC };
	void	Initstat();
	void	Setup(CWnd* IN_pMomWnd);

	CFont ft1;
	COLORREF txcol_TVal,bkcol_TVal;
	COLORREF txcol_TVal2,bkcol_TVal2;
	void	VPP_TEXT(LPCSTR lpcszString, ...);
	void	VPPNUM_TEXT(LPCSTR lpcszString, ...);
	void	VPP_RESULT_TEXT(unsigned char col,LPCSTR lpcszString, ...);
	void	SYNC_TEXT(LPCSTR lpcszString, ...);
	void	SYNCNUM_TEXT(LPCSTR lpcszString, ...);
	void	SYNC_RESULT_TEXT(unsigned char col,LPCSTR lpcszString, ...);

private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
