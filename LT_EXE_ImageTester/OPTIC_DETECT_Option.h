#pragma once

#include "EtcModelDlg.h"
#include "afxwin.h"
#include "afxcmn.h"

// COPTIC_DETECT_Option 대화 상자입니다.

#define	   n_Cluster   144    // 사진내 Cluster의 총 격자점 수 
#define    N_Point     109  // 격자점 갭수  10*11 - 1 [원점제외]
//#define    N_Picture   4
#define    ViewField   150
#define    pi          3.141592
#define    Focal       1.27
//-=-------------------------------------------

class COPTIC_DETECT_Option : public CDialog
{
	DECLARE_DYNAMIC(COPTIC_DETECT_Option)

public:
	COPTIC_DETECT_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COPTIC_DETECT_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_OPTICAL };

	CRectData C_RECT[4];
	int m_CAM_SIZE_WIDTH;
	int m_CAM_SIZE_HEIGHT;
	int StartCnt;
	void	SETLIST();
	void	UploadList();
	int InIndex;
//	_TOTWrdata CenterVal;
	_EtcWrdata CenterVal;
	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
	tResultVal Run(void);

	bool b_TESTResult;
	
	double GetDistance(int x1, int y1, int x2, int y2);
	bool g_stat;

	double	OpticCenterX;//실재 계측된 중심점 X
	double	OpticCenterY;//실재 계측된 중심점 Y

	CvPoint GetCenterPoint(LPBYTE IN_RGB);
	IplImage *g_pImage;
		int		make_iplimage(IplImage *img, BYTE *buf_img, int Width, int Height);
	unsigned int ReadPicturePoint(IplImage    *srcImage, double **ms1, double **xs1, int *ErrorCount);
	char	SeparateSector(double *CenterPoint ,double **msTmp, double ***DataS, int n);
	//void	MakeChessboard(double *CenterPoint ,double **Data, double ***DataS, int n );
	void	MakeChessboard(double *CenterPoint ,double **Data, double ***DataS, int n ,IplImage *srcImage);
	
	void	FixClusterPoint(double *CenterPoint, double **Data, int NumInputData, int NumOutputData);
	double  Distance(double *A, double *B);
	void    SwapPoint(double *A, double *B);
	double  FindMinDistPoint1(int *Index, double **DataTmp, double *RefPoint, int N, int Mode);
	void    Swapdata(double *A, double *B);
	void	SwapPoint4(double *A, double *B);
	int     CheckArea(int x, int y, int n);
	void	MakeCornerMap(double **Data,int N, double R);
	void    CopyPoint4(double *Dest, double *Src);
	void	CalCoefficient( double *ReturnVal );
	void	RadialProjection(double *p);
	void	MulMatrix(double **A, double **B, double **C, unsigned int Row, unsigned int Col, unsigned int n, unsigned int Mode);
	void	InvMatrix22(double **A, double **B);
	void	polyval3(double *x, double *y, double *K, unsigned int NumAlpha);
	void	Max(double *data, unsigned int *MaxPosition, unsigned int count);
	void	poly3roots(double *a, double *result);
	void	SVD(double *dataA, unsigned int row, unsigned int col, double **MatU, double **MatW, double **MatVT);
	double	BackProjectGeneric(double **RtnH, double **m, double **xs, double *p, double ThetaMax1, double *Theta, double *phi, unsigned int NumPoint);
	double	Norm(double * data, unsigned int n);
	void	HomAngleErr(double *VecD, double **H, double **SphereX, double **xs , unsigned int NumPoint);
	void	FindJAC(double **JAC,  double *DeltaX,  double *VecD, double **H, double **SphereX, double **xs, unsigned int NumPoint );
	void	FindGradF(double *gradF, double **JAC, double *costFun,  unsigned int JacRow, unsigned int JacCol);
	double	MaxAbs(double * data, unsigned int n);
	void	GetStep(double **AugJac, double *AugRes1D, double *step1D, unsigned int row, unsigned int col);
	void	NormalizeMat(double **MatA, unsigned int row , unsigned int col, unsigned int Mode);
	void	SetDeltaX(double *DeltaX, double *VecH, double *TypicalX, double Min, double Max , unsigned int N);
	void	pInverse(double **A, unsigned int Row, unsigned int Col, double **RtnMat);
	void	MinimiseProjErrs(double ***Rs, double **Ts, double *p, double ***ms, double *** xs,double ***Rs0, double **Ts0, double *p0);
	double	AbsDouble(double data);
	void	Inverse(double **dataMat, unsigned int n, double **MatRtn);
	void	RotationPars(double *RtnT, double **R);
	void	Lsqnonlin(double *Par, double *Par0, double ***ms, double ***xs);
	void	RotationMat(double **RtnH, double *RotVect);
	void	ProjErr(double *d, double *Par, double ***ms, double ***xs);
	void	LevenbergMarquardt(double *XOUT, double *Par0 , double *CostFun, double ***ms, double ***xs);
	void	InitialiseExternalp(double **RtnRs, double *RtnTs, double **Hs);
	void	GenericProjExtended(double **Rtn, double **xs, double *p, double **R, double *t);
	void	FindJAC1(double **GradF,  double *DeltaX, double *VecD, double *Par0,  double ***ms, double ***xs,unsigned int n_vecD,unsigned int n_par);
	//-----------------------------------------------
	
	//---------------sdy-------------------------
	CComboBox	*pFileCombo;
	CComboBox	*pRunCombo;
	_OptPr	CenterPrm;
	CRectData centerPt;
	CRectData GetOpticCenterCoordinate(LPBYTE IN_RGB);


	int		m_EtcNumber;
	int		m_file_sel;
	void	BinFiledel();
	void	SetFileSel();
//	void	FileSearch(CString compdata);
//	void	SetFileSave();
//	void	SaveFilePrm(_OptPr *Center,CString Path);
	void	Save_parameter();
	void	Load_parameter();
	void	StatePrintf(LPCSTR lpcszString, ...);

	CString		BinfileName;
	CString		oldBinfileName;
	CString		model_bin_path;

	int		BinFileCHK(int stat);
	BYTE	BinFileCHK();
	BYTE	m_filemode;
	unsigned long	AddOfOffset[4];
	char			OffsetBuffer[32][256];
	char	xoffdata;
	char	yoffdata;
	char	m_DChkSum1;
	char	m_DChkSum2;
	BOOL	B_FileChk;

	BOOL	m_Running;
	int		m_RunMode;
	CString ModeName;
	int		OnSendSerial(_EtcWrdata * INDATA);
//	int		OnSendSerial(_TOTWrdata * INDATA);
	int		OffsetWrite(_TOTWrdata * INDATA);
	BOOL	SendData8Byte(unsigned char * TxData, int Num,_TOTWrdata * INDATA);
	BOOL	EtcSendData8Byte(_TOTWrdata * INDATA,int Num);
//	void	centergen(double centx,double centy);
	void	centergen(double centx,double centy,_EtcWrdata *EtcDATA);
	void	OpticalAsixModify(_EtcWrdata * INDATA);
	void	MoveCenter(void);
	int		MicomOnSendSerial(_EtcWrdata * INDATA);

	CvPoint EtcPt[4];
//	void	Find_offset(_TOTWrdata * INDATA);
	bool	Find_offset(_EtcWrdata * INDATA);
	
	BOOL	ChkTstEnd(_TOTWrdata * INDATA);
	BOOL	ChkTstEnd(_EtcWrdata * INDATA);
	
	void	Initgen(_EtcWrdata *EtcDATA);

	void	Center_Pic(CDC* cdc);
	void	ShowIplImage(IplImage *src, HDC hDCDst);
	void	FillBitmapInfo( BITMAPINFO* bmi,int width, int height, int bpp );
	void	Center_Pic(CDC* cdc, int NUM);
	CvPoint	m_dCurrCenterCoord;
	int		m_CheckWrite;
	int		m_ChkSector;
	CComboBox	*pINTWROPTCombo;
	CComboBox	*pWROPTCombo;
	DWORD	m_dwd1pdelay;
	DWORD	m_dwd16pdelay;


	void	EtcModelNameChange(int num);
	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void InsertList();
	int InsertIndex;
	int ListItemNum;

	BOOL CheckSettingDetect_1();

	CString TESTLOT[4][30];
	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();
	void FAIL_UPLOAD();
	BOOL	b_StopFail;
	void Setup_List();
	void CPK_DATA_SUM();
	int CPK_DATA[2][200];
	int CPKcount;

	double SaveCPK_X;
	double SaveCPK_Y;

	void	Change_DATA();
	bool ChangeCheck;
	int ChangeItem[4];
	int changecount;
	void List_COLOR_Change();
	int iSavedItem, iSavedSubitem;
	CRect rect;
	bool Change_DATA_CHECK(bool FLAG);
	void	EditWheelIntSet(int nID,int Val);

	// MES 정보저장
	CString m_szMesResult;
	CString Mes_Result();
#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	int Lot_StartCnt;
	int Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

#pragma endregion

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	CString		str_model;
	CString		str_ModelPath;
	CString		str_BinPath;
	tINFO		m_INFO;

	double	ThetaMax;
	double  ThetaFirstMax;
	unsigned int fwdFindDiff;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
//	int m_channel_Number;
//	afx_msg void OnCbnSelchangeCombo2();
//	afx_msg void OnCbnSelchangeComboSelchnum();
//	CStatic m_stCenterDisplay;
	afx_msg void OnBnClickedBtnBinadd();
	virtual BOOL OnInitDialog();
	UINT m_iStandX;
	UINT m_iStandY;
	double m_dPassDeviatX;
	double m_dPassDeviatY;
	double m_dRateX;
	double m_dRateY;
	UINT m_iEnPixX;
	UINT m_iEnPixY;
	UINT m_iMaxCount;
	int m_iWRMODE;
	afx_msg void OnBnClickedOpticalstatSave();
	afx_msg void OnCbnSelchangeComboMode();

	CvPoint m_ptMasterCoord[4];
	CvPoint m_ptCenterPointCoord[4];
	CListCtrl m_CenterWorklist;
	double m_dPassMaxDevX;
	double m_dPassMaxDevY;
	afx_msg void OnCbnSelchangeComboIntwropt();
	afx_msg void OnCbnSelchangeComboWropt2();
	afx_msg void OnBnClickedOpticalstatSave2();
	DWORD m_dwd1pdelay_buf;
	DWORD m_dwd16pdelay_buf;
	afx_msg void OnBnClickedOpticalstatSave3();
	afx_msg void OnEnChangeEdit1pdelay();
	afx_msg void OnEnChangeEdit16pdelay();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedChkImg();
	BOOL b_Chk_Img;
	double m_Optical_radius;
	double m_Sensitivity;
	int i_Sensitivity;
	afx_msg void OnBnClickedButtonPrmsave();
	CString str_Sensitivity;
	afx_msg void OnBnClickedButtonDefaultrect();
	CListCtrl C_DATALIST;
	afx_msg void OnBnClickedButtonLoadprm();
	afx_msg void OnBnClickedButtonSaveprm();
	afx_msg void OnNMDblclkListRect(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawListRect(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusEditRectOptic();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnEnChangeEditRectOptic();
	CStatic m_SubFrame;
	afx_msg void OnBnClickedButtonMicom();
	afx_msg void OnBnClickedButtontest();
	CListCtrl m_Lot_OpticalCenterList;
};
