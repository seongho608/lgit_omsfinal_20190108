#pragma once
#include "afxcmn.h"

enum RectNum{
	Rect_Left,
	Rect_Right,
	Rect_FullNum,

};


static LPCTSTR g_szTestRect[] =
{
	_T("Rect_Left"),				
	_T("Rect_Right"),					
	_T("Rect_Full"),			
	NULL
};


// COverlayPosition_Option 대화 상자입니다.
class COverlayPosition_Option : public CDialog
{
	DECLARE_DYNAMIC(COverlayPosition_Option)

public:
	COverlayPosition_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COverlayPosition_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_OVERLAYPOSITION };

	tResultVal Run(void);
	tResultVal OverlayPosition(LPBYTE IN_RGB);

	void	Pic(CDC *cdc);
	void	InitPrm();
	void	InitUI();

	void	FAIL_UPLOAD();
	BOOL	b_StopFail;

	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();
	void	InitEVMS();

	void	Save_parameter();
	void	Load_parameter();
	void	Save(int NUM);

	CString H_filename;

	bool	OverlayPic(CDC *cdc);
	bool	OverlayPic(CDC *cdc, int NUM);
	
	/* HTH */
	int Data_Static[720][480];
	int m_CAM_SIZE_WIDTH;
	int m_CAM_SIZE_HEIGHT;
	IplImage* OriginImage ;
	void Image_Binarization();

	CFont	ft;
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);

	//-------------------------------------------------------------------worklist
	void	Set_List(CListCtrl *List);
	void	LOT_Set_List(CListCtrl *List);
	
	int		Lot_InsertNum;
	void	InsertList();
	int		InsertIndex;
	int		ListItemNum;

	void	EXCEL_SAVE();
	void	LOT_EXCEL_SAVE();
	bool	EXCEL_UPLOAD();
	bool	LOT_EXCEL_UPLOAD();

	int		StartCnt;

	int		Lot_StartCnt;
	void	LOT_InsertDataList();

	void	StatePrintf(LPCSTR lpcszString, ...);
	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
	CString		strTitle;

	CRectData	m_param[2];			// 0: left, 1: right

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()

public:
	int		m_nDistance;
	int		m_nCutOff;
	int		m_offset_left;
	int		m_offset_right;


	CString str_OverlayPos;
	CString str_OverCutOff;

	CString str_OverlayLeftX;
	CString str_OverlayLeftY;
	CString str_OverlayRightX;
	CString str_OverlayRightY;

	int		m_nLeftX;
	int		m_nLeftY;
	int		m_nRightX;
	int		m_nRightY;

	CListCtrl m_OverlayList;
	CListCtrl m_Lot_OverlayList;

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnPaint();
	afx_msg void OnBnClickedBtnOverlayPositionSave();
	afx_msg void OnBnClickedBtnOverlayTest();
	afx_msg void OnEnChangeEditSetDistance();
	afx_msg void OnEnChangeEditSetOverlayCutoff();
	afx_msg void OnBnClickedBtnOverlayPointSave();
	afx_msg void OnEnChangeEditOverlayLeftX();
	afx_msg void OnEnChangeEditOverlayLeftY();
	afx_msg void OnEnChangeEditOverlayRightX();
	afx_msg void OnEnChangeEditOverlayRightY();
	CString m_OFFSET_LEFT;
	CString m_OFFSET_RIGHT;
	afx_msg void OnBnClickedBtnOffsetSave2();

	CRectData Test_RECT[2];
	void Rect_SETLIST();
	void Rect_InsertList();
	void Load_parameter_Rect();
	void Save_parameter_Rect(UINT MODE);
	CListCtrl m_RectList;
	int iSavedItem;
	int iSavedSubitem;
	int ChangeCheck;
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnBnClickedButtonLoad();
	afx_msg void OnNMDblclkListRect(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusEditRectlist();
	void List_COLOR_Change(int itemnum);
	void Change_DATA();
	int		changecount;
	int		ChangeItem[Rect_FullNum];
	afx_msg void OnNMCustomdrawListRect(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	bool Change_DATA_CHECK(bool FLAG);
	void EditWheelIntSet(int nID,int Val);
	afx_msg void OnEnSetfocusEditRectlist();
};
