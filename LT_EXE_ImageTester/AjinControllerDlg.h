#pragma once

#include "JogButton.h"
// CAjinControllerDlg 대화 상자입니다.

#include "AXL.h"
#include "AXD.h"
class CAjinControllerDlg : public CDialog
{
	DECLARE_DYNAMIC(CAjinControllerDlg)

public:
	CAjinControllerDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAjinControllerDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_AJINCONTROLLER_DIALOG };

	void	CModelOpt_MotorSet_ShowWindow(int inshow);
	void	InitMotorSetup();
	void	CMotorSet_Create();
	void	CMotorSet_Delete();
	BOOL	InitLibrary();
	BOOL	AddAxisInfo();
	BOOL	InitControl();
	BOOL	UpdateState();

	DWORD		ConvertComboToAxm(CComboBox *pCboItem);						// ComboBox콘트롤에 현재 선택된 Item Index를 모션보드에 설정할 값으로 변경하는 함수.	
	long		ConvertAxmToCombo(CComboBox *pCboItem, DWORD dwCurData);	// 설정값과 ComboBox콘트롤의 Item과 일치하는 Item을 찾아 선택하는 함수.	
	void		SetDlgItemDouble(int nID, double value, int nPoint = 3);	// double값을 지정한 콘트롤에 설정하는 함수
	double		GetDlgItemDouble(int nID);									// 지정한 콘트롤에 설정되어 있는 텍스트를 double값으로 변환하여 반환하는 함수.


	//CMotorSetting *m_pMotorSetWnd;

	long		m_lAxisNo;					// 제어할 축 번호
	long		m_lAxisCount;				// 유효한 전체 모션축수
	DWORD		m_dwModuleID;				// 선택한 축의 모델ID

	double		m_dOldCmdPos;				// OnTimer에서 사용할 이전 Command Position			
	double		m_dOldActPos;				// OnTimer에서 사용할 이전 Actual Position	
	double		m_dOldCmdVel;				// OnTimer에서 사용할 이전 Command Velocity

	CJogButton	m_btnJogMoveP;
	CJogButton	m_btnJogMoveN;


	BOOL		m_bTestActive[8];
	HANDLE		m_hTestThread[8];


	void	Setup(CWnd* IN_pMomWnd);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

private:
	CWnd		*m_pMomWnd;
	// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg LRESULT OnJogBtnDn(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnJogBtnUp(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:

	afx_msg void OnBnClickedSettingMot();
	afx_msg void OnCbnSelchangeCboSelAxis();
	CComboBox m_cboSelAxis;
	afx_msg void OnBnClickedBtnLoadFile();
	afx_msg void OnBnClickedBtnSaveFile();
	afx_msg void OnBnClickedParamsApply();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonOrigin();
	afx_msg void OnBnClickedButtonTestmove();
	afx_msg void OnBnClickedButtonStop();
	afx_msg void OnBnClickedChkServoOn();
	afx_msg void OnDestroy();
};
