// OverlayDetect_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "OverlayDetect_Option.h"

extern BYTE	m_RGBScanbuf[720 * 480 * 4];

// COverlayDetect_Option 대화 상자입니다.

IMPLEMENT_DYNAMIC(COverlayDetect_Option, CDialog)

COverlayDetect_Option::COverlayDetect_Option(CWnd* pParent /*=NULL*/)
	: CDialog(COverlayDetect_Option::IDD, pParent)
{
	for(int lop = 0;lop<5;lop++){
		pSubDlgEdit[lop] = NULL;
	}
	StartCnt = 0;
	Lot_StartCnt = 0;
	b_StopFail = FALSE;
}

COverlayDetect_Option::~COverlayDetect_Option()
{
}

void COverlayDetect_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_OVERLAY_FRAME, m_stOverlayFrame);
	DDX_Control(pDX, IDC_LIST_OVERLAY, m_lsOverlayList);
	DDX_Control(pDX, IDC_LIST_OVERLAY_WORKLIST, m_OverlayWorkList);
	DDX_Control(pDX, IDC_LIST_OVERLAY_WORKLIST_LOT, m_Lot_OverlayWorkList);
}


BEGIN_MESSAGE_MAP(COverlayDetect_Option, CDialog)
	ON_LBN_SELCHANGE(IDC_LIST_OVERLAY, &COverlayDetect_Option::OnLbnSelchangeListOverlay)
	ON_BN_CLICKED(IDC_BTN_DELETEOVERLAYMASTERLIST, &COverlayDetect_Option::OnBnClickedBtnDeleteoverlaymasterlist)
	ON_BN_CLICKED(IDC_BTN_MASTER_IMG_ADD, &COverlayDetect_Option::OnBnClickedBtnMasterImgAdd)
	ON_BN_CLICKED(IDC_BTN_OVERLAY_SAVE, &COverlayDetect_Option::OnBnClickedBtnOverlaySave)
	ON_BN_CLICKED(IDC_BTN_SET_AREA, &COverlayDetect_Option::OnBnClickedBtnSetArea)
	ON_BN_CLICKED(IDC_BTN_MASTER_IMG_AUTO_EXT, &COverlayDetect_Option::OnBnClickedBtnMasterImgAutoExt)
//	ON_CBN_SELCHANGE(IDC_COMBO_WMODE, &COverlayDetect_Option::OnCbnSelchangeComboWmode)
//	ON_EN_CHANGE(IDC_EDIT_MASTEROVERLAY_ANGLE, &COverlayDetect_Option::OnEnChangeEditMasteroverlayAngle)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

void COverlayDetect_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
	str_ModelPath=((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	strSelectedModelPath = ((CImageTesterDlg *)m_pMomWnd)->Overlay_path;
	str_model.Empty();
	str_model.Format("OVERLAY OPTION");
}



void COverlayDetect_Option::Setup(CWnd* IN_pMomWnd,CEdit *pTEDIT,tINFO INFO)
{
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	Setup(IN_pMomWnd);

	m_ShowMode = 0;
}

void COverlayDetect_Option::Save_parameter()
{
	CString str="";
	CString strTitle="";

	str.Empty();
	str.Format("%d",m_INFO.ID);
	WritePrivateProfileString(str_model,"ID",str,str_ModelPath);
	str.Empty();
	str.Format("%s",m_INFO.MODE);
	WritePrivateProfileString(str_model,"MODE",str,str_ModelPath);
	str.Empty();
	str.Format("%s",m_INFO.NAME);
	WritePrivateProfileString(str_model,"NAME",str,str_ModelPath);

	str.Empty();
	GetDlgItemText(IDC_EDIT_OVERLAT_START_X, str);
	WritePrivateProfileString(str_model,"OVERLAY_START_X",str,str_ModelPath);	
	str.Empty();

	GetDlgItemText(IDC_EDIT_OVERLAT_START_Y, str);
	WritePrivateProfileString(str_model,"OVERLAY_START_Y",str,str_ModelPath);
	str.Empty();

	GetDlgItemText(IDC_EDIT_OVERLAT_END_X, str);
	WritePrivateProfileString(str_model,"OVERLAY_END_X",str,str_ModelPath);
	str.Empty();

	GetDlgItemText(IDC_EDIT_OVERLAT_END_Y, str);
	WritePrivateProfileString(str_model,"OVERLAY_END_Y",str,str_ModelPath);
	str.Empty();

	GetDlgItemText(IDC_EDIT_MATCHING_RATIO, str);
	WritePrivateProfileString(str_model,"OVERLAY_MATCHING_RATIO",str,str_ModelPath);

}

void COverlayDetect_Option::Load_parameter()
{
	CFileFind filefind;
	CString str="";
	CString strTitle="";
	
	m_dOverlayStart_x = GetPrivateProfileInt(str_model,"OVERLAY_START_X",-1,str_ModelPath);	
	if(m_dOverlayStart_x == -1){
		m_dOverlayStart_x = 15;
		str.Empty();
		str.Format("%d",m_dOverlayStart_x);
		WritePrivateProfileString(str_model,"OVERLAY_START_X",str,str_ModelPath);
	}

	m_dOverlayStart_y = GetPrivateProfileInt(str_model,"OVERLAY_START_Y",-1,str_ModelPath);	
	if(m_dOverlayStart_y == -1){
		m_dOverlayStart_y = 90;
		str.Empty();
		str.Format("%d",m_dOverlayStart_y);
		WritePrivateProfileString(str_model,"OVERLAY_START_Y",str,str_ModelPath);
	}

	m_dOverlayEnd_x = GetPrivateProfileInt(str_model,"OVERLAY_END_X",-1,str_ModelPath);	
	if(m_dOverlayEnd_x == -1){
		m_dOverlayEnd_x = 700;
		str.Empty();
		str.Format("%d",m_dOverlayEnd_x);
		WritePrivateProfileString(str_model,"OVERLAY_END_X",str,str_ModelPath);
	}

	m_dOverlayEnd_y = GetPrivateProfileInt(str_model,"OVERLAY_END_Y",-1,str_ModelPath);	
	if(m_dOverlayEnd_y == -1){
		m_dOverlayEnd_y = 410;
		str.Empty();
		str.Format("%d",m_dOverlayEnd_y);
		WritePrivateProfileString(str_model,"OVERLAY_END_Y",str,str_ModelPath);
	}
	
	m_dMatchingRatio = GetPrivateProfileInt(str_model,"OVERLAY_MATCHING_RATIO",-1,str_ModelPath);	
	if(m_dMatchingRatio == -1){
		m_dMatchingRatio = 90;
		str.Empty();
		str.Format("%d",m_dMatchingRatio);
		WritePrivateProfileString(str_model,"OVERLAY_MATCHING_RATIO",str,str_ModelPath);
	}
		
	SetDlgItemInt(IDC_EDIT_OVERLAT_START_X, m_dOverlayStart_x);
	SetDlgItemInt(IDC_EDIT_OVERLAT_START_Y, m_dOverlayStart_y);
	SetDlgItemInt(IDC_EDIT_OVERLAT_END_X, m_dOverlayEnd_x);
	SetDlgItemInt(IDC_EDIT_OVERLAT_END_Y, m_dOverlayEnd_y);
	SetDlgItemInt(IDC_EDIT_MATCHING_RATIO, m_dMatchingRatio);

	int Angel[5] = {-25,-17,0,17,25};
	for(int lop = 0;lop<5;lop++){
		str.Format("%d",Angel[lop]);
		pSubDlgEdit[lop]->SetWindowText(str);
	}

}

void COverlayDetect_Option::UpateMasterFileList()
{
	CFileFind folderfind;

	if(!folderfind.FindFile(strSelectedModelPath + "\\Overlay")){ //원하는 폴더가 없으면 만든다.
		CreateDirectory(strSelectedModelPath + "\\Overlay", NULL); 
	}

	CFileFind finder;
	BOOL bWorking = finder.FindFile(strSelectedModelPath + "\\Overlay\\" +  "*.bmp");
	
	CString str_file[50];
	CString str_fbuf[50];
	for(int lop = 0;lop<50;lop++){
		str_file[lop] = "";
		str_fbuf[lop] = "";
	}

	int ficnt = 0;
	while( bWorking )
	{
		bWorking = finder.FindNextFile();
		CString str = finder.GetFileName();
			
		int start = str.Find('_');
		int end = str.Find('.');
		str = str.Mid(start+1, (end - start)-1);
		str_file[ficnt] = str;
		ficnt++;
	}

	//우선 숫자 sort를 진행한다.
	
	int min = 0;
	CString str = 0;
	for(int lop2 = 0;lop2 <ficnt-1;lop2++){
		min = lop2;
		for(int lop = lop2+1;lop<ficnt;lop++){
			if(atoi(str_file[lop]) < atoi(str_file[min])){
				min = lop;
				str = str_file[lop2];
				str_file[lop2] = str_file[min];
				str_file[min] = str;
			}
		}
	}

	//모델별 sort를 진행한다. 
	int sortcnt = 0;
	/*for(int lop = 0;lop<ficnt;lop++){
		if(strstr(str_file[lop],((CImageTesterDlg *)m_pMomWnd)->ModelCode[1].str_DATA)){
			str_fbuf[sortcnt] = str_file[lop];
			str_file[lop] = "";
			sortcnt++;
		}
	}
	for(int lop = 0;lop<ficnt;lop++){
		if(strstr(str_file[lop],((CImageTesterDlg *)m_pMomWnd)->ModelCode[2].str_DATA)){
			str_fbuf[sortcnt] = str_file[lop];
			str_file[lop] = "";
			sortcnt++;
		}
	}*/
	for(int lop = 0;lop<ficnt;lop++){
		if(str_file[lop] != ""){	
			str_fbuf[sortcnt] = str_file[lop];
			str_file[lop] = "";
			sortcnt++;
		}
	}
	m_lsOverlayList.ResetContent();
	for(int lop = 0;lop<ficnt;lop++){
		 str_file[lop] = str_fbuf[lop];
		 m_lsOverlayList.AddString(str_file[lop]);
	}

	

}

void COverlayDetect_Option::OnBnClickedBtnDeleteoverlaymasterlist()
{
	m_ShowMode = 0;

	CString str;
	int sel;
	int cnt;

	cnt = m_lsOverlayList.GetCount();
	sel = m_lsOverlayList.GetCurSel();

	if(sel > -1)
	{		
		CString str;
		m_lsOverlayList.GetText(sel, str);
		DeleteFile(strSelectedModelPath + "\\Overlay\\" + "Trajectory_" + str + ".bmp");
		m_lsOverlayList.DeleteString(sel);
	}
	SETTING_DATA();
	Set_List(&m_OverlayWorkList);
}

void COverlayDetect_Option::OnBnClickedBtnMasterImgAdd()
{
	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->JIG_MOVE_CHK();
	
	
	((CImageTesterDlg *)m_pMomWnd)->Power_On();
	Wait(1500);
	if(!((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK()){
		AfxMessageBox("카메라가 연결되어 있지 않습니다.");
		((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->ShowWindow(SW_SHOW);
		return;
	}
	m_ShowMode = 0;

	CString str;
	CString str_buf = "";
	int MoCode = -1;
	GetDlgItemText(IDC_EDIT_MASTEROVERLAY_ANGLE, str);
	str_buf = str;

	str.Replace(" ","");
	if(str == ""){
		AfxMessageBox("추가할 값을 선택해주세요. ");
		return;
	}

	bool Flag = TRUE;
	((CImageTesterDlg *)m_pMomWnd)->Overlay_Start();
	if(m_lsOverlayList.GetCount())
	{
		for(int i=0; i<m_lsOverlayList.GetCount(); i++)
		{
			CString str2;
			m_lsOverlayList.GetText(i, str2);
			if(str_buf == str2)
			{
				Flag = FALSE;
				break;
			}
		}
		if(Flag == TRUE)
		{
			m_lsOverlayList.AddString(str_buf);
			GetOverlayImage(str_buf);
		}
	}
	else
	{
		m_lsOverlayList.AddString(str_buf);
		GetOverlayImage(str_buf);
	}
	SETTING_DATA();
	Set_List(&m_OverlayWorkList);
	UpateMasterFileList();
}


void COverlayDetect_Option::EtcCamInit(int Model)
{
	////=====================================================
	//((CImageTesterDlg *)m_pMomWnd)->Power_Off();
	//((CImageTesterDlg *)m_pMomWnd)->sleep(500);
	//((CImageTesterDlg *)m_pMomWnd)->Power_On();
	//((CImageTesterDlg *)m_pMomWnd)->sleep(1000);
	////=====================================================

	//for(int i=0; i<4; i++)
	//{
	//	((CImageTesterDlg *)m_pMomWnd)->Camera_Init();
	//	Wait(400);
	//}

	//for(int lop = 0;lop <100;lop++){
	//	if(((CImageTesterDlg *)m_pMomWnd)->Init_Wait == TRUE){
	//		break;
	//	}
	//	Wait(10);
	//}
}

void COverlayDetect_Option::OnBnClickedBtnMasterImgAutoExt()//이미지 추출
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str;
	bool Flag = TRUE;

	//=====================================================
	
	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->JIG_MOVE_CHK();
	
	((CImageTesterDlg *)m_pMomWnd)->Power_On();
// 	Wait(1000);
// 	((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(1);
// 	Wait(500);
	if(!((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK()){
		AfxMessageBox("카메라가 연결되어 있지 않습니다.");
		
		((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->ShowWindow(SW_SHOW);
		return;
	}
	m_ShowMode = 0;

	int OCnt = m_lsOverlayList.GetCount();
	
	if(OCnt != 0){
		for(int lop = OCnt-1;lop>=0;lop--){
			m_lsOverlayList.GetText(lop, str);
			DeleteFile(strSelectedModelPath + "\\Overlay\\" + "Trajectory_" + str + ".bmp");
			m_lsOverlayList.DeleteString(lop);// GetText(sel, str);
		}
		SETTING_DATA();
		Set_List(&m_OverlayWorkList);
	}

	int Angel[5] = {-25,-17,0,17,25};

	CString str_Angle[5];
	for(int lop = 0;lop<5;lop++){
		pSubDlgEdit[lop]->GetWindowText(str_Angle[lop]);
	}

	((CImageTesterDlg *)m_pMomWnd)->Overlay_Start();
	for(int lop = 0;lop<5;lop++)
	{
		str = str_Angle[lop];
		if(str != ""){
			m_lsOverlayList.AddString(str);
			GetOverlayImage(str);
		}
	}

	SETTING_DATA();
	Set_List(&m_OverlayWorkList);
	UpateMasterFileList();
}



bool COverlayDetect_Option::CompareOverlay(LPBYTE IN_RGBIn_Target, IplImage *master_image,OverlayData *sOverlayData, int Cnt)
{
	int start_x, start_y;
	int end_x, end_y;

	start_x = GetDlgItemInt(IDC_EDIT_OVERLAT_START_X);
	start_y = GetDlgItemInt(IDC_EDIT_OVERLAT_START_Y);
	end_x = GetDlgItemInt(IDC_EDIT_OVERLAT_END_X);
	end_y = GetDlgItemInt(IDC_EDIT_OVERLAT_END_Y);
	
	double threshold = GetDlgItemInt(IDC_EDIT_MATCHING_RATIO);		
	threshold = threshold / 100.0;

	bool ret = CompareOverlay(IN_RGBIn_Target,master_image,start_x, start_y, end_x, end_y, threshold, sOverlayData, Cnt);
	return ret;
}

bool COverlayDetect_Option::CompareOverlay(LPBYTE IN_RGBIn_Target, IplImage *master_image, int start_x, int start_y, int end_x, int end_y, double threshold, OverlayData *sOverlayData, int Cnt)
{
	int width = end_x - start_x;
	int height = end_y - start_y;

	IplImage *src_image = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	IplImage *target_image = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	
	BYTE R, G, B;
	double Sum_Y;
	double total_Sum_Y=0;

	for(int y=start_y; y<end_y; y++)
	{
		for(int x=start_x; x<end_x; x++)
		{
			B = master_image->imageData[y*master_image->widthStep + x*3 + 0];
			G = master_image->imageData[y*master_image->widthStep + x*3 + 1];
			R = master_image->imageData[y*master_image->widthStep + x*3 + 2];
			
			if(R < 80 && G < 80 && B < 80)
				Sum_Y = 255;
			else
				Sum_Y = (R+G+B)/3;

			total_Sum_Y += Sum_Y;

			src_image->imageData[(y-start_y)*src_image->widthStep+(x-start_x)] = Sum_Y;			

			B = IN_RGBIn_Target[y*(720*4) + x*4    ];
			G = IN_RGBIn_Target[y*(720*4) + x*4 + 1];
			R = IN_RGBIn_Target[y*(720*4) + x*4 + 2];
			
			if(R < 80 && G < 80 && B < 80)
				Sum_Y = 255;
			else
				Sum_Y = (R+G+B)/3;

			target_image->imageData[(y-start_y)*target_image->widthStep+(x-start_x)] = Sum_Y;
		}
	}	

	double min_val, max_val;

	IplImage *temp = cvCreateImage(cvSize(src_image->width - target_image->width+1, src_image->height - target_image->height+1), IPL_DEPTH_32F, 1);
	cvMatchTemplate(target_image, src_image, temp, CV_TM_CCOEFF_NORMED);
	cvMinMaxLoc(temp, &min_val, &max_val, 0, 0, 0);
	
	cvReleaseImage(&temp);
	cvReleaseImage(&src_image);
	cvReleaseImage(&target_image);
	
	sOverlayData->templateRatio[Cnt] = max_val * 100.0;
	
	if(total_Sum_Y < 1000)
	{
		max_val = 0.0;
		sOverlayData->templateRatio[Cnt] = 0;
	}

	if(max_val > threshold)
		return TRUE;
	else
		return FALSE;
}


void COverlayDetect_Option::GetOverlayImage(CString str_angle)
{	
	
	int angle = atoi(str_angle);

	//오버레이 무브 알고리즘 추가 

	int ret = FALSE;
	memset(m_RGBScanbuf,0,sizeof(m_RGBScanbuf));
	((CImageTesterDlg *)m_pMomWnd)->Overlay_Move(angle);//추출이 안될 경우 2초를 기다린다.
	for(int lop =0;lop<2;lop++){
		ret = ((CImageTesterDlg *)m_pMomWnd)->Overlay_Move(angle);//추출이 안될 경우 2초를 기다린다. 
		if(ret == TRUE){
			if(((CImageTesterDlg *)m_pMomWnd)->Buf_IMG_SUC == TRUE){
				break;
			}
		}
	}

	IplImage *testImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 3);

	for(int y = 0; y<480; y++)
	{
		for(int x=0; x<720; x++)
		{
			testImage->imageData[y * testImage->widthStep + x * 3 + 0] = m_RGBScanbuf[y * 2880 + x * 4 + 0];
			testImage->imageData[y * testImage->widthStep + x * 3 + 1] = m_RGBScanbuf[y * 2880 + x * 4 + 1];
			testImage->imageData[y * testImage->widthStep + x * 3 + 2] = m_RGBScanbuf[y * 2880 + x * 4 + 2];
		}
	}
		
	CString savePath;
	savePath.Format("Trajectory_%s.bmp", str_angle);

	//cvSaveImage(strSelectedModelPath + "\\Overlay\\" + savePath, testImage);

	ShowOverlayImage(testImage);

	cvReleaseImage(&testImage);
}



void COverlayDetect_Option::GetOverlayImage(int angle)
{	

	int ret = FALSE;
	memset(m_RGBScanbuf,0,sizeof(m_RGBScanbuf));
	
	((CImageTesterDlg *)m_pMomWnd)->Overlay_Move(angle);//추출이 안될 경우 2초를 기다린다.
	for(int lop =0;lop<2;lop++){
		ret = ((CImageTesterDlg *)m_pMomWnd)->Overlay_Move(angle);//추출이 안될 경우 2초를 기다린다. 
		if(ret == TRUE){
			if(((CImageTesterDlg *)m_pMomWnd)->Buf_IMG_SUC == TRUE){
				break;
			}
		}
	}
	
	IplImage *testImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 3);

	for(int y = 0; y<480; y++)
	{
		for(int x=0; x<720; x++)
		{
			testImage->imageData[y * testImage->widthStep + x * 3 + 0] = m_RGBScanbuf[y * 2880 + x * 4 + 0];
			testImage->imageData[y * testImage->widthStep + x * 3 + 1] = m_RGBScanbuf[y * 2880 + x * 4 + 1];
			testImage->imageData[y * testImage->widthStep + x * 3 + 2] = m_RGBScanbuf[y * 2880 + x * 4 + 2];
		}
	}
		
	CString savePath;
	savePath.Format("Trajectory_%d.bmp", angle);

	//cvSaveImage(strSelectedModelPath + "\\Overlay\\" + savePath, testImage);

	ShowOverlayImage(testImage);
	

	cvReleaseImage(&testImage);
}

void COverlayDetect_Option::OnLbnSelchangeListOverlay()
{
	m_ShowMode = 0;

	CString str;
	int sel = m_lsOverlayList.GetCurSel();
	m_lsOverlayList.GetText(sel, str);
	
	IplImage *srcImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);
	srcImage = cvLoadImage(strSelectedModelPath + "\\Overlay\\" + "Trajectory_" + str + ".bmp");
	
	if(!srcImage)
		return;

	ShowOverlayImage(srcImage);

	cvReleaseImage(&srcImage);
}


void COverlayDetect_Option::OnBnClickedBtnSetArea()
{
	if(m_lsOverlayList.GetCount() == 0)
		return;

	m_ShowMode = 0;

	m_dOverlayStart_x = GetDlgItemInt(IDC_EDIT_OVERLAT_START_X);
	m_dOverlayStart_y = GetDlgItemInt(IDC_EDIT_OVERLAT_START_Y);
	m_dOverlayEnd_x = GetDlgItemInt(IDC_EDIT_OVERLAT_END_X);
	m_dOverlayEnd_y = GetDlgItemInt(IDC_EDIT_OVERLAT_END_Y);	

	CString str;
	int sel = m_lsOverlayList.GetCurSel();

	if(sel == -1)
		sel = 0;

	m_lsOverlayList.GetText(sel, str);

	
	IplImage *srcImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);
	srcImage = cvLoadImage(strSelectedModelPath + "\\Overlay\\" + "Trajectory_" + str + ".bmp");
	
	if(!srcImage)
		return;

	ShowOverlayImage(srcImage);

	cvReleaseImage(&srcImage);
}

void COverlayDetect_Option::ShowOverlayImage(IplImage *srcImage)
{
	unsigned char *RGBBuf = new unsigned char [CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];

	BITMAPINFO		m_biInfo;
	ZeroMemory(&m_biInfo, sizeof(m_biInfo));
	m_biInfo.bmiHeader.biSize		= sizeof(BITMAPINFOHEADER);
	m_biInfo.bmiHeader.biWidth		= CAM_IMAGE_WIDTH;
	m_biInfo.bmiHeader.biHeight		= CAM_IMAGE_HEIGHT;
	m_biInfo.bmiHeader.biPlanes		= 1;
	m_biInfo.bmiHeader.biBitCount	= 32;
	m_biInfo.bmiHeader.biCompression	= 0;
	m_biInfo.bmiHeader.biSizeImage	= CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4;	

	CDC *pcDC;

	//-----------가상메모리를 만든다.------------------
	CDC	*pcMemDC = new CDC;
	CBitmap	*pcDIB	= new CBitmap;

	pcDC = m_stOverlayFrame.GetDC();

	for(int y=0; y<CAM_IMAGE_HEIGHT; y++)
		for(int x=0; x<CAM_IMAGE_WIDTH; x++)
		{
			RGBBuf[y*(CAM_IMAGE_WIDTH*4) + x*4 + 0] = srcImage->imageData[y*srcImage->widthStep + x*3 + 0];
			RGBBuf[y*(CAM_IMAGE_WIDTH*4) + x*4 + 1] = srcImage->imageData[y*srcImage->widthStep + x*3 + 1];
			RGBBuf[y*(CAM_IMAGE_WIDTH*4) + x*4 + 2] = srcImage->imageData[y*srcImage->widthStep + x*3 + 2];
		}

	pcMemDC->CreateCompatibleDC(pcDC);
	pcDIB->CreateCompatibleBitmap(pcDC,CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT);
	CBitmap	*pcOldDIB = pcMemDC->SelectObject(pcDIB);
	
	

	//---------------------------------------------
	::SetStretchBltMode(pcMemDC->m_hDC, COLORONCOLOR);//MemDC.m_hDC//pcDC->m_hDC
	::StretchDIBits(pcMemDC->m_hDC, 
					0,  CAM_IMAGE_HEIGHT-1,
					CAM_IMAGE_WIDTH, -CAM_IMAGE_HEIGHT,
					0, 0,
					m_biInfo.bmiHeader.biWidth,
					m_biInfo.bmiHeader.biHeight,
					RGBBuf, &m_biInfo, DIB_RGB_COLORS, SRCCOPY);
//******************************************************************************

	CPen	my_Pan, my_Pan2, *old_pan;

	if(m_ShowMode == 0)
	{		
		::SetBkMode(pcMemDC->m_hDC,TRANSPARENT);
		my_Pan.CreatePen(PS_SOLID,3,RGB(255, 255, 255));
		old_pan = pcMemDC->SelectObject(&my_Pan);
		pcMemDC->MoveTo(m_dOverlayStart_x,	m_dOverlayStart_y);
		pcMemDC->LineTo(m_dOverlayEnd_x,	m_dOverlayStart_y);
		pcMemDC->LineTo(m_dOverlayEnd_x,	m_dOverlayEnd_y);
		pcMemDC->LineTo(m_dOverlayStart_x,	m_dOverlayEnd_y);
		pcMemDC->LineTo(m_dOverlayStart_x,	m_dOverlayStart_y);
	}
	else
	{
		
	}

	my_Pan.DeleteObject();
//******************************************************************************

	//--------------모니터 창에 맞추어 RGB정보를 할당한다.-------------------------
	::SetStretchBltMode(pcDC->m_hDC, COLORONCOLOR);
	::StretchBlt(	pcDC->m_hDC,
					0,  0,
					//720, 480,//1080, 720,
					360,228,
					pcMemDC->m_hDC,
					0, 0,
					CAM_IMAGE_WIDTH,
					CAM_IMAGE_HEIGHT,SRCCOPY);
	

	m_stOverlayFrame.ReleaseDC(pcDC);

	delete pcDIB;
	delete pcMemDC;	
	delete []RGBBuf;
	
}

void COverlayDetect_Option::OnBnClickedBtnOverlaySave()
{
	Save_parameter();
}


BOOL COverlayDetect_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	//*****************************************************//TEST용	Overlay, Warninig
//	pWModeCombo = (CComboBox *)GetDlgItem(IDC_COMBO_WMODE);
	
	pSubDlgEdit[0] = (CEdit*)GetDlgItem(IDC_EDIT_AE_OVERLAY_ANGLE_1);
	pSubDlgEdit[1] = (CEdit*)GetDlgItem(IDC_EDIT_AE_OVERLAY_ANGLE_2);
	pSubDlgEdit[2] = (CEdit*)GetDlgItem(IDC_EDIT_AE_OVERLAY_ANGLE_3);
	pSubDlgEdit[3] = (CEdit*)GetDlgItem(IDC_EDIT_AE_OVERLAY_ANGLE_4);
	pSubDlgEdit[4] = (CEdit*)GetDlgItem(IDC_EDIT_AE_OVERLAY_ANGLE_5);
	
	Load_parameter();
	UpdateData(FALSE);	
	UpateMasterFileList();

	SETTING_DATA();
	
	for(int k=0; k<100; k++)
		TESTLOT[k]="";

	Set_List(&m_OverlayWorkList);
	LOT_Set_List(&m_Lot_OverlayWorkList);
	InitEVMS();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void COverlayDetect_Option::InitPrm(){
	Load_parameter();
	UpdateData(FALSE);	
	UpateMasterFileList();
	SETTING_DATA();
}
void COverlayDetect_Option::SETTING_DATA(){
	TEST_LIST_COUNT=0;	//검사항목 수
	TEST_LIST_COUNT = m_lsOverlayList.GetCount();

	int AttrCnt = 0;

	for(int i=0; i<m_lsOverlayList.GetCount(); i++)
	{	
		CString str;
		m_lsOverlayList.GetText(i, str);
		TESTNAME[AttrCnt] = str;
		AttrCnt++;
	}

	for(int i=0; i<4; i++)
	{
		for(int j=0; j<50; j++)
		{
			m_dTemplateRatio[i][j] = 0;
			m_dResult[i][j] = FALSE;
		}
	}
}
//---------------------------------------------------------------------WORKLIST
void COverlayDetect_Option::Set_List(CListCtrl *List){
	
	int count=0;
	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);

	for(int t=0; t<TEST_LIST_COUNT; t++){
		int k = t*2;
		List->InsertColumn(4+k,TESTNAME[t],LVCFMT_CENTER, 80);
		List->InsertColumn(5+k,TESTNAME[t]+"_RESULT",LVCFMT_CENTER, 80);
		count+=2;
	}
	List->InsertColumn(count+4,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(count+5,"비고",LVCFMT_CENTER, 80);
	
	ListItemNum=count+6;
	Copy_List(&m_OverlayWorkList,((CImageTesterDlg *)m_pMomWnd)->m_pWorkList,ListItemNum);
}

void COverlayDetect_Option::LOT_Set_List(CListCtrl *List){
	
	int count=0;
	while(List->DeleteColumn(0));
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);

	for(int t=0; t<TEST_LIST_COUNT; t++){
		int k = t*2;
		List->InsertColumn(4+k,TESTNAME[t],LVCFMT_CENTER, 80);
		List->InsertColumn(5+k,TESTNAME[t]+"_RESULT",LVCFMT_CENTER, 80);
		count+=2;
	}
	List->InsertColumn(count+4,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(count+5,"비고",LVCFMT_CENTER, 80);
	
	Lot_InsertNum=count+6;
}

void COverlayDetect_Option::InsertList(){
	
	TESTLOT[1]=((CImageTesterDlg *)m_pMomWnd)->modelName;
	TESTLOT[2]=((CImageTesterDlg *)m_pMomWnd)->strDay;
	TESTLOT[3]=((CImageTesterDlg *)m_pMomWnd)->strTime;
}

void COverlayDetect_Option::LOT_InsertDataList()
{
	CString strCnt="";

	int Index = m_Lot_OverlayWorkList.InsertItem(Lot_StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index+1);
	m_Lot_OverlayWorkList.SetItemText(Index,0,strCnt);

	int CopyIndex = m_OverlayWorkList.GetItemCount()-1;

	int count =0;
	for(int t=0; t< Lot_InsertNum;t++){
		if((t != 2)&&(t!=0)){
			m_Lot_OverlayWorkList.SetItemText(Index,t, m_OverlayWorkList.GetItemText(CopyIndex,count));
			count++;

		}else if(t==0){
			count++;
		}else{
			m_Lot_OverlayWorkList.SetItemText(Index,t,((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;
}

void COverlayDetect_Option::FAIL_UPLOAD(){
	if ((((CImageTesterDlg *)m_pMomWnd)->LotMod == 1) && (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)){
			InsertList();
			TESTLOT[ListItemNum-2]="FAIL";
			TESTLOT[ListItemNum-1]="STOP";
			Setup_List();
			//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_OVERLAYDET,"STOP");
			StartCnt++;
			b_StopFail = TRUE;
	}
}

void COverlayDetect_Option::Setup_List(){
	CString strCnt ="";
	int count = StartCnt;
	InsertIndex = m_OverlayWorkList.InsertItem(count,"",0);
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_OverlayWorkList.SetItemText(InsertIndex,0,strCnt);
	for(int t=0; t<ListItemNum-1; t++){
		m_OverlayWorkList.SetItemText(InsertIndex,t+1,TESTLOT[t+1]);
	}			
}

void COverlayDetect_Option::EXCEL_SAVE(){
	CString Item[100]={"",};//수정해야할 곳 0203
	int count =0;
	for(int t=0; t<TEST_LIST_COUNT; t++){
		int k = t*2;
		Item[k]=TESTNAME[t];
		Item[k+1]=TESTNAME[t]+"RESULT";
		count+=2;
	}
	CString Data ="";
	CString str="";
	str = "***********************조향연동검사***********************\n\n";
	Data += str;
	
	str.Empty();
	str.Format("매칭율, %d,, \n ",m_dMatchingRatio);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("조향연동검사",Item,count,&m_OverlayWorkList,Data);
}

void COverlayDetect_Option::LOT_EXCEL_SAVE(){
	CString Item[100]={"",};//수정해야할 곳 0203
	int count =0;
	for(int t=0; t<TEST_LIST_COUNT; t++){
		int k = t*2;
		Item[k]=TESTNAME[t];
		Item[k+1]=TESTNAME[t]+"RESULT";
		count+=2;
	}
	CString Data ="";
	CString str="";
	str = "***********************조향연동검사***********************\n\n";
	Data += str;
	
	str.Empty();
	str.Format("매칭율, %d,, \n ",m_dMatchingRatio);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("조향연동검사",Item,count,&m_Lot_OverlayWorkList,Data);
}

bool COverlayDetect_Option::EXCEL_UPLOAD(){
	m_OverlayWorkList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("조향연동검사",&m_OverlayWorkList,1,&StartCnt) == TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return 0;
}

bool COverlayDetect_Option::LOT_EXCEL_UPLOAD(){
	m_OverlayWorkList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("조향연동검사",&m_Lot_OverlayWorkList,1,&StartCnt) == TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return 0;
}

BOOL COverlayDetect_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}



//void COverlayDetect_Option::OnCbnSelchangeComboWmode()
//{
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	m_WModeNum = pWModeCombo->GetCurSel();
//}

void COverlayDetect_Option::OnEnChangeEditMasteroverlayAngle()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
void COverlayDetect_Option::Pic(CDC *cdc)
{
	/*for(int lop=0;lop<4;lop++){
		AveRGBPic(cdc,lop);
	}*/
}
tResultVal COverlayDetect_Option::Run(){

	

	tResultVal retval = {0,};
	b_StopFail = FALSE;
	((CImageTesterDlg *)m_pMomWnd)->m_pResOverlay_CAN_OptWnd->m_listCh1OverlayResult.DeleteAllItems();//리스트 컨트롤 초기화

	int overlayCnt = m_lsOverlayList.GetCount();
	int totalCheckCnt = overlayCnt+1;

	if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand==TRUE){
		for(int k=0; k<100; k++){
			TESTLOT[k]="";
		}
		InsertList();
	}

	
	for(int j=0; j<50; j++){
		m_sOverlayData.templateRatio[j] = 0.0;
	}
	m_sOverlayData.enable = TRUE;
	

	((CImageTesterDlg *)m_pMomWnd)->Overlay_Start();
	for(int i=0; i<overlayCnt; i++)
	{
		Overlay_Etc_Compare(i);
	}

	totalCheckCnt = overlayCnt;

	bool FLAG = TRUE;
	for(int i=0; i<overlayCnt; i++)
	{
		if(m_sOverlayData.OverlayCheckResult[i] == TRUE)
		{
		
		}else{
			FLAG = FALSE;
		}
	}		
	
	if(FLAG == TRUE){
		retval.m_Success = 1;
		Str_Mes[0] = "PASS";
		Str_Mes[1] = "1";
		retval.ValString = "오버레이 검사 성공";
		TESTLOT[(TEST_LIST_COUNT*2+4)] ="PASS";
		//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_OVERLAYDET,"PASS");
	}else if(FLAG == FALSE){
		retval.m_Success = 0;
		Str_Mes[0] = "FAIL";
		Str_Mes[1] = "0";
		retval.ValString = "오버레이 검사 실패";
		TESTLOT[(TEST_LIST_COUNT*2+4)] ="FAIL";
		//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_OVERLAYDET,"FAIL");
	}

	if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand==TRUE){
		Setup_List();
		StartCnt++;
	}
	
	BOOL ret = FALSE;
	for(int lop = 0;lop<3;lop++){
		ret = ((CImageTesterDlg *)m_pMomWnd)->Overlay_Init();
		if(ret == TRUE){
			break;
		}
	}

	return retval;

}


BOOL COverlayDetect_Option::Overlay_Etc_Compare(int num) //오버레이 조향각 Angle만큼 움직임 
{
	CString str = "";
	CString RES = "";
	CString NUM = "";

	m_lsOverlayList.GetText(num, str);

	IplImage *MasterImage = cvLoadImage(strSelectedModelPath + "\\Overlay\\" + "Trajectory_" + str + ".bmp", 1);//비교할 이미지를 추출한다.
	if(!MasterImage){
		return FALSE;
	}
	
	int ret = FALSE;
	memset(m_RGBScanbuf,0,sizeof(m_RGBScanbuf));

	((CImageTesterDlg *)m_pMomWnd)->Overlay_Move(atoi(str));//추출이 안될 경우 2초를 기다린다. 
	for(int lop =0;lop<2;lop++){
		ret = ((CImageTesterDlg *)m_pMomWnd)->Overlay_Move(atoi(str));//추출이 안될 경우 2초를 기다린다. 
		if(ret == TRUE){
			if(((CImageTesterDlg *)m_pMomWnd)->Buf_IMG_SUC == TRUE){
				break;
			}
		}
	}


	BOOL result = FALSE;
	if((ret == FALSE)||(((CImageTesterDlg *)m_pMomWnd)->Buf_IMG_SUC == FALSE)){
		//이미지 추출을 실패한 경우 
		m_sOverlayData.templateRatio[num] = 0.0;
		result = FALSE;
	}else{
		if(m_sOverlayData.enable == TRUE){
			result = CompareOverlay(m_RGBScanbuf, MasterImage, &m_sOverlayData, num);
		}
	}
	((CImageTesterDlg *)m_pMomWnd)->Get_Wait = FALSE;
	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = FALSE;	

	m_sOverlayData.testName[num] = str;
	m_sOverlayData.OverlayCheckResult[num] = result;
	NUM.Empty();
	NUM.Format("%3.1f",m_sOverlayData.templateRatio[num]);
	RES.Empty();
	if(result == TRUE){
		RES = "PASS";	
	}else{
		RES = "FAIL";			
	}
	int k = num*2;
	TESTLOT[4+k]=NUM;
	TESTLOT[5+k]=RES;

	if(m_sOverlayData.enable == TRUE)
	{	

		((CImageTesterDlg *)m_pMomWnd)->m_pResOverlay_CAN_OptWnd->m_listCh1OverlayResult.InsertItem(num, "");
		((CImageTesterDlg *)m_pMomWnd)->m_pResOverlay_CAN_OptWnd->m_listCh1OverlayResult.SetItemText(num, 0, m_sOverlayData.testName[num]);

		if(m_sOverlayData.OverlayCheckResult[num] == TRUE)
		{
			((CImageTesterDlg *)m_pMomWnd)->m_pResOverlay_CAN_OptWnd->m_listCh1OverlayResult.SetItemText(num, 1, "OK");
		}
		else
		{
			((CImageTesterDlg *)m_pMomWnd)->m_pResOverlay_CAN_OptWnd->m_listCh1OverlayResult.SetItemText(num, 1, "FAIL");
		}

		((CImageTesterDlg *)m_pMomWnd)->m_pResOverlay_CAN_OptWnd->m_listCh1OverlayResult.SetItemText(num, 2, NUM);					
	}

	cvReleaseImage(&MasterImage);
}
void COverlayDetect_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;

	}
}

CString COverlayDetect_Option ::Mes_Result()
{
	CString sz;
	m_szMesResult = _T("");
	if(b_StopFail == FALSE){
	m_szMesResult.Format(_T("%s:%s"),Str_Mes[0],Str_Mes[1]);
	}else{
		m_szMesResult.Format(_T(":1"));
	}

	return m_szMesResult;
}

void COverlayDetect_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CButton *)GetDlgItem(IDC_BTN_DELETEOVERLAYMASTERLIST))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_MASTER_IMG_AUTO_EXT))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_MASTER_IMG_ADD))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_SET_AREA))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_OVERLAY_SAVE))->EnableWindow(0);
	
		((CEdit *)GetDlgItem(IDC_EDIT_MASTEROVERLAY_ANGLE))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_AE_OVERLAY_ANGLE_1))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_AE_OVERLAY_ANGLE_2))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_AE_OVERLAY_ANGLE_3))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_AE_OVERLAY_ANGLE_4))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_AE_OVERLAY_ANGLE_5))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_OVERLAT_START_X))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_OVERLAT_START_Y))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_OVERLAT_END_X))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_OVERLAT_END_Y))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MATCHING_RATIO))->EnableWindow(0);
	}
}