
// LT_EXE_ImageTesterDlg.h : 헤더 파일
//

#pragma once
// #include "usb3_dio01_frm_import.h"
// #pragma comment(lib, "USB3-DIO01.lib")

#include "cv.h"
#include "highgui.h"

//#include "ComArt.h"
#include "afxwin.h"
#include "resource.h"
#include "ETCUSER.H"
#include "CommThread.h"	// Added by ClassView
#include "CommDlg.h"

#include "StandDlg.h"
#include "Option_ResultWStand.h"
#include "Lot_StandDlg.h"
#include "WorkList.h"
#include "ModSetDlg.h"
#include "ModSelDlg.h"
#include "ModOptDlg.h"
#include "StandOptDlg.h"
#include "StandOptMesDlg.h"
#include "Pogo_Option.h"
#include "EtcModelDlg.h"
#include "UserSwitching.h"

#include "MasterMonitorDlg.h"
//#include "RMasterMonitorDlg.h"

#include "ModelCopyDlg.h"

#include "PopUP.h"
#include "PopUP2.h"
#include "PopUP3.h"

#include "NewLot.h"
#include "ExistLot.h"

#include "Option_SubStand.h"
#include "Option_ResultStand.h"

#include "Angle_Option.h"
#include "Angle_ResultView.h"

#include "Brightness_Option.h"
#include "Brightness_ResultView.h"

#include "CENTER_DETECT_Option.h"
#include "CENTER_DETECT_ResultView.h"
#include "Micom_Center.h"

#include "Color_Option.h"
#include "Color_ResultView.h"

#include "ColorReversal_Option.h"
#include "ColorReversal_ResultView.h"

#include "Distortion_Option.h"
#include "Distortion_ResultView.h"

#include "IRFilter_Option.h"
#include "IRFilter_ResultView.h"

#include "OPTIC_DETECT_Option.h"
#include "OPTICAL_DETECT_ResultView.h"
#include "Micom_Optic.h"


#include "Particle_Option.h"
#include "Particle_ResultView.h"

#include "Particle_Option_LG.h"
#include "Particle_ResultView_LG.h"

#include "DynamicRange_Option.h"
#include "DynamicRange_ResultView.h"

#include "PatternNoise_Option.h"
#include "PatternNoise_ResultView.h"

#include "ResolutionDetection_Option.h"
#include "ResolutionDetection_ResultView.h"

#include "Rotate_Option.h"
#include "Rotate_ResultView.h"

#include "Particle_View.h"
#include "Particle_View_LG.h"

#include "OverlayDetect_Option.h"
#include "Option_ResultOverlay.h"

#include "WarningDetect_Option.h"
#include "Option_ResultWarning.h"

#include "NTSC_Option.h"
#include "NTSC_ResultView.h"

#include "SYSTEM_Option.h"
#include "SYSTEM_ResultView.h"

#include "DTC_Option.h"
#include "DTC_ResultView.h"

#include "VER_Option.h"
#include "VER_ResultView.h"

#include "OverlayPosition_Option.h"
#include "OverlayPosition_ResultView.h"

#include "LowLight_Option.h"
#include "LowLight_ResultView.h"

#include "Option_Verify.h"
#include "Verify_ResultView.h"

#include "SFRTILTOptionDlg.h"
#include "SFR_ResultView.h"

#include "Option_FixedPattern.h"
#include "FixedPattern_ResultView.h"

#include "_3D_Depth_Option.h"
#include "_3D_Depth_ResultView.h"

#include "HotPixel_Option.h"
#include "HotPixel_ResultView.h"

#include "DepthNoise_Option.h"
#include "DepthNoise_ResultView.h"


#include "Temperature_Option.h"
#include "Temperature_ResultView.h"

// 모터제어
#include "./Motor/MotionControl.h"
#include "./Motor/DlgOrigin.h"

#include "./Include/Socket/SocketManager.h"

#include  "ControlDlg.h"

#include "afxcmn.h"

//#include "LGE_Video.h"
#include "DAQWrapper.h"
#include "LGE_CamSetDlg.h"
#include "LGE_CamSet_ParticleDlg.h"
#include "Dlg_Emergency.h"


enum enImageSaveMode
{
	Image_NoSave = 0,
	Image_OriginSave,
	Image_ALLSave,
};


enum enParticleImageSaveMode
{
	P_Image_NoSave = 0,
	P_Image_FailSave ,
	P_Image_ALLSave,
};

typedef struct _ST_MES_CURRENT
{
	CString szData;
	INT		nResult;

	_ST_MES_CURRENT()
	{
		Reset();
	};

	void Reset()
	{
		szData.Empty();
		nResult = TEST_NG;
	}
}ST_MES_CURRENT;

#define		WM_CAMERA_CHG_STATUS	WM_USER + 35	// 카메라 영상 상태가 변경됨
#define		WM_CAMERA_RECV_VIDEO	WM_USER + 36	// 카메라 영상 데이터가 영상보드로부터 수신됨
#define		WM_READ_CAM		WM_USER + 1


typedef enum __WORK_NUM 
{
	WORKLIST_MODELSTART		=	5,
	WORKLIST_CURRENT,
	WORKLIST_EEPROMCHK,
	WORKLIST_CENTER,
	WORKLIST_ROTATE,
	WORKLIST_SFR,
	WORKLIST_ANGLE,
	WORKLIST_DISTORTION,
	WORKLIST_3D_DEPTH,
	WORKLIST_DUST,
	WORKLIST_BRIGHTNESS,
	WORKLIST_DYNAMICRANGE,
	WORKLIST_FIXED_PATTERN,
	WORKLIST_HOTPIXEL,
	WORKLIST_DEPTHNOISE,
    TEST_ITEM_NUM,

} WORK_NUM;


#ifndef CAM_IMAGE_WIDTH
	#define CAM_IMAGE_WIDTH		640
#endif

#ifndef CAM_IMAGE_HEIGHT
	#define CAM_IMAGE_HEIGHT	480
#endif

#define	PROGRAM_NAME	_T("LG FINAL TEST NTSC V1.0.0.0")

#define MAX_TEST_NUM		30
#define TM_MES_RETRYCONNECT	200
#define TM_CAM_FRAME		130
#define TM_MACHINE_CHECK	201

#define WM_COMM_START (WM_USER +32)			// callback 설정 -- thread가 부모 프로세스에 보낼 메세지
#define WM_COMM_STOP (WM_USER +33)			// callback 설정 -- thread가 부모 프로세스에 보낼 메세지
#define WM_COMM_EMERGENCY (WM_USER +34)			// callback 설정 -- thread가 부모 프로세스에 보낼 메세지

#define RESULT_AUTO_CAPTURE	0 //1:신뢰성 0: eol

#define hjm_re  0 // 0:만곡현상 수치 안나오도록 함 1:만곡현상 수치 낮아지도록 함 

#define QL_IG 0	//0 : QL, 1 : IG

typedef enum __MES_NUM {
	LOT_ID = 0, NUM, TOTAL_RESULT, MES_TEST1, MES_TEST2, MES_TEST3, MES_TEST4, MES_TEST5, MES_TEST6, MES_TEST7, MES_TEST8, MES_TEST9, MES_TEST10, MES_TEST11, MES_TEST12, MES_TEST13, MES_TEST14, MES_TEST15, MES_TEST16, MES_TEST17, MES_TEST18, MES_TEST19, MES_TEST20, MAX_MES_NUM
} MES_NUM;

typedef enum __NEWMES_NUM 
{
	NewLOT_ID = 0, 
	NewNUM, 
	NewTOTAL_RESULT, 
	NewMES_PREEEPROM,
	NewMES_Current,
	//NewMES_Temp_Sensor,
	NewMES_Temperature,
	NewMES_SFR, 
	NewMES_FOV, 
	NewMES_Distortion, 
	NewMES_3D_Depth, 
	NewMES_Particle, 
	NewMES_Brightness,//광량비 Shading
	NewMES_SNR,//이물 SNR
	NewMES_DynamicRange, 
	NewMES_CenterPoint, 
	NewMES_Rotate, 
	NewMES_PixedPattern,
	NewMES_HotPixel,
	NewMES_DepthNoise,
	MAX_NewMES_NUM
} NEWMES_NUM;


typedef enum __NEWMES_DataNUM
{
	NewMESDataNum_PREEEPROM			= 4,
	NewMESDataNum_Current			= 5,
	NewMESDataNum_Temperature		= 1,
	NewMESDataNum_SFR				= 37,
	NewMESDataNum_FOV				= 2,
	NewMESDataNum_Distortion		= 1,
	NewMESDataNum_3D_Depth			= 2,
	NewMESDataNum_Particle			= 1,
	NewMESDataNum_Brightness		= 73,//광량비 Shading
	NewMESDataNum_SNR				= 56,//이물 SNR
	NewMESDataNum_DynamicRange		= 8,
	NewMESDataNum_CenterPoint		= 2,
	NewMESDataNum_Rotate			= 1,
	NewMESDataNum_PixedPattern		= 1,
	NewMESDataNum_HotPixel			= 1,
	NewMESDataNum_DepthNoise		= 1,
	MAX_NewMES_DataNUM
} NEWMES_DataNUM;


typedef enum __VIDEO_REG_NUM {
	VR_POWEROFF = 0,
	VR_PO_REGISTER,
	VR_PO_REGISTER_2,
} VIDEO_REG_NUM;




struct TESTData{
	int m_ID;
	int m_Success;// 0 - 초기값 1- 양품 2- 불량 3- 비활성
	double m_Val[50];
	CString m_TestName[50];
	CString ValString;
	bool ComportState;
	bool CameraState;
	bool PowerONOFF;
	int m_Enable;// 0 - 초기값 1- 활성 2- 비활성 // Model_Start에서만 쓰임.
	//-----------LOT에 관한 것
	CString ModelName;
	CString Lot_Info;
	CString Day;
	CString Time;
	CString TestName;
	CString LOT_Operator;
};

typedef struct _ST_EEPROM
{
	BOOL bReadCheck;
	BOOL bCheckSumCheck;
	BOOL bZeroCheck;
	DWORD wSlave;
	DWORD wOC_X_Addr;
	DWORD wOC_Y_Addr;
	DWORD wCheckSum_Addr;

	DWORD wOC_X_Len;
	DWORD wOC_Y_Len;
	DWORD wCheckSum_Len;

	UINT nOC_X_Min;
	UINT nOC_X_Max;
	UINT nOC_Y_Min;
	UINT nOC_Y_Max;

	_ST_EEPROM()
	{
		bZeroCheck = 1;
		bReadCheck =1;
		bCheckSumCheck = 1;

		wSlave = 0xAC;
		wOC_X_Addr = 0x40;
		wOC_Y_Addr = 0x44;
		wCheckSum_Addr = 0xFA;

		wOC_X_Len = 4;
		wOC_Y_Len = 4;
		wCheckSum_Len = 1;

		nOC_X_Min=313;
		nOC_X_Max=327;
		nOC_Y_Min=233;
		nOC_Y_Max=247;
	};

}ST_EEPROM;

typedef struct _ST_EEPROM_DATA
{
	float   fOC_X;
	float   fOC_Y;
	BOOL bResult_OC_X;
	BOOL bResult_OC_Y;
//	DWORD wCheckSum;
	BOOL bResult_CheckSum;
	BOOL bResult_CheckZero;
	CString szCheckSum;
	_ST_EEPROM_DATA()
	{
	
	};

	void reset(){
		fOC_X=0;
		fOC_Y = 0;
		bResult_OC_X = 0;
		bResult_OC_Y = 0;
		szCheckSum.Empty();
		bResult_CheckSum = 0;
		bResult_CheckZero = 0;
	}; 

}ST_EEPROM_DATA;
// CImageTesterDlg 대화 상자
class CImageTesterDlg : public CDialog
{
// 생성입니다.
public:
	CImageTesterDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

	CString m_stNewMesData[MAX_NewMES_NUM];
	int m_nNewMesDataNum[MAX_NewMES_NUM];
	void onInitFuncMesData();
	BOOL PreEEPROM_CHECK();
	CString Mes_Result_EEPROM();
	void Set_List_Eeprom(CListCtrl *List);
	void InsertList_Eeprom();
	void FAIL_UPLOAD_Eeprom();
	void EXCEL_SAVE_Eeprom();
	bool EXCEL_UPLOAD_Eeprom();
	void LOT_Set_List_Eeprom(CListCtrl *List);
	void LOT_InsertDataList_Eeprom();
	void LOT_EXCEL_SAVE_Eeprom();
	bool LOT_EXCEL_UPLOAD_Eeprom();
	int InsertIndex_Eeprom;
	int Lot_InsertIndex_Eeprom;
	int ListItemNum_Eeprom;
	int StartCnt_Eeprom;


	int		Lot_StartCnt_Eeprom;
	int		Lot_InsertNum_Eeprom;

	BOOL b_InsertEepromList;

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_LT_EXE_IMAGETESTER_DIALOG };

#pragma region		영상관련
	// LG전자 SDK ---------------------------------------------------
	afx_msg LRESULT	OnCameraChgStatus(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnCameraRecvVideo(WPARAM wParam, LPARAM lParam);

	//CLGE_Video				m_LGEVideo;
	CDAQWrapper		m_LVDSVideo;
	ST_DAQOption	stDaqOpt;
	virtual void	InitDevicez						(__in HWND hWndOwner = NULL);
	virtual BOOL	ConnectGrabberBrd				(__in BOOL bConnect);
	
	BOOL LGEVideo_Start (UINT nMODE);
	void			LGEVideo_Stop					();

	void			DisplayVideo					(__in UINT nChIdx, __in LPBYTE lpbyRGB, __in DWORD dwRGBSize, __in UINT nWidth, __in UINT nHeight);
	void			IMG_COPY(BYTE *RGBScanBuf);
	void			IMG_COPY_GRAY(WORD *lpbyRGB, __in DWORD dwRGBSize);

	// ** 임시 **
	// 수동 생산 모드 (이미지 파일 불러와서 검사 진행)
	void	LoadPNGImage();
	void   TestImageSaveMode(int nMode, LPWORD pGrayBuf, CString szTestName);
	CString strLoadIMGPath;
	CString strLoadIMGPath_16;
	// --------------------------------------------------------------

	BOOL b_Chk_Align;//Align용 Pic
	int m_Align;

	UINT m_nVideoRegisterMode;
	BOOL mB_Check_RegisterChange;
#pragma endregion

#pragma region		TEST START STOP


	int		m_exbtn;
	BYTE	btndelaycnt;
	void	RunStart();
	void	RunStop();

	bool	STAY_TIME();
	void	Initprm();
	afx_msg LRESULT OnStartBtn(WPARAM wParam, LPARAM lParam);//시리얼통신을 통해 데이터가 들어왔을 경우 실행됨
	afx_msg LRESULT OnStopBtn(WPARAM wParam, LPARAM lParam);//시리얼통신을 통해 데이터가 들어왔을 경우 실행됨
	afx_msg LRESULT OnEmergencyPause(WPARAM wParam, LPARAM lParam);//시리얼통신을 통해 데이터가 들어왔을 경우 실행됨

	int i_MST_H;
	int i_MST_M;
	int EndTime;
	DWORD TESTstartTime;
	void	Mid_Time(DWORD start, DWORD end);

	int m_nRoffset;

	void LoadMotorParameter();
	void SaveMotorParameter();

#pragma endregion

#pragma region 기타

	void Pic(CDC *cdc);
	BOOL b_CamSet;
	CLGE_LIB_Data CAMSET_SAVE; // TEST 진행용
	CLGE_LIB_Data CAMSET_SAVE_Particel; // TEST 진행용
	CLGE_LIB_Data CAMSET_TEST; // 메뉴얼 세팅용
	void	SetProperty_ALL(CLGE_LIB_Data Property);
	void	SetProperty_TOF1_Alpha(UINT nValue);
	void	SetProperty_TOF1_CaliSlope(UINT wValue);
	void	SetProperty_TOF1_SmallSignalRemoval(UINT wValue);
	void	SetProperty_TOF1_IRMode(BOOL bOn);
	void	SetProperty_TOF1_IRGain(UINT byValue);
	void	SetProperty_TOF1_Emission(UINT nValue);
	void SetUse_Gain(__in BOOL bUse, __in int nExp);

	BOOL	CamOnCheck;
	BOOL	m_ScanState;
	BOOL	B_CAMRUN;
	CString str_FailName;
	BYTE	m_HuType;

	HWND	hMainWnd;

	void	StatePrintf(LPCSTR lpcszString, ...);
	CEdit	*m_pTStat; //스테이트 변수가 저장되는 위치 

	//CComArt *ComArtWnd;
	
	CString			MatchPassWord;

	void	LoadParameterSet();
	void	InitDlgProgram();
	void	InitProgram();
	void	FontSetup();
	bool	m_ImgScan;//영상 스캔시 사용되는 변수
	bool	m_ImgScan_Gray;//영상 스캔시 사용되는 변수
	BOOL	m_YUVSCAN;
	BOOL	m_YScanBuf;

	CString			TESTNAME[30];
	CString			m_exepath;		//exe파일 경로
	CString			m_inifilename;	//ini파일 경로
	CString			m_model_path;
	CString			Overlay_path;
	CString			OverlayImagePath;
	CString			model_bin_path;
	CString			ruiinitfilename;
	CString			modelfile_Name;
	CString			m_passwordname;
	CString			genfolderpath;
	CString			binfileName;
	CString			oldbinfileName;
	CString			Micom_Path;
	CString			INITMODELFILE;
	CString			CAMSETFILE;
	CString			modelFolder_SetFile_path;//I2C파일 위치
	CString			SAVE_SETFILE;//I2C파일 이름
	CString			SAVE_SETFILE_Second;//I2C파일 이름

	CFont ft1,ft2,ft3,ft4,ft5;
	COLORREF txcol_PortState,bkcol_PortState,txcol_CamState,bkcol_CamState,txcol_TVal,bkcol_TVal,txcol_pg,bkcol_pg;
	COLORREF txcol_ConPortState,bkcol_ConPortState,txcol_SubPortState,bkcol_SubPortState;
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);

	void PORT_NAME(LPCSTR lpcszString, ...);
	void CAM_NAME(LPCSTR lpcszString, ...);
	void PORT_STAT_CAM(unsigned char col,LPCSTR lpcszString, ...);
	void CAM_STAT(unsigned char col,LPCSTR lpcszString, ...);
	void TEST_STAT(unsigned char col,LPCSTR lpcszString, ...);

	void	Time_TEXT(LPCSTR lpcszString, ...);
	void	Time_NUM(LPCSTR lpcszString, ...);
	void	ProTime_TEXT(LPCSTR lpcszString, ...);
	void	ProTime_NUM(LPCSTR lpcszString, ...);

	void	SubBmpPic(UINT nIDResource);
	void	SubBmpPic_master(UINT nIDResource);
	
	void	Focus_move_start();


	bool	b_SecretLgOption;
	bool	b_SecretOption;
	void	ButtonInit();

	BOOL	b_PROTECT;
	BOOL	EVMS_Mode;
	CString	envpath;

	void	InitEVMS();
	void	SavePositionSet(int Xdistance,int Ydistance);

#pragma endregion

#pragma region		모델정보
	int			m_combo_sel; //현재 지정된 모델의 번호
	CComboBox	*m_pMainCombo;
	CWnd		*m_pBKStateDlg;
	CWnd		*m_pBKDialog;
	CTabCtrl	*m_pModResTabWnd;
	CTabCtrl	*m_pModResTabWnd2;

	//----각각 모델의 생성
	int				m_SubRunNum;

	CString			m_modelName;

	tINFO			m_ModelInfo[MAX_TEST_NUM];
	tINFO			m_ModelStart;
	tINFO			m_ModelEnd;

	CString			m_oldmodelname;
	CString			m_newmodelname;
#pragma endregion
#pragma region			모델 정보 함수

	
	CString			Start_oldmodelname;
	CString			Start_newmodelname;

	void	SETTING_USER_Switching();
	void	MainModelSel();
	void	ModelSet();
	void	ModelGen();
	void	ModelClear();
	void	ModelSave();
	void	ModelSearch(CString compdata);
	void	SetModelSet();

	void	FullOptionEnable(bool benable);
	void worklist_uploadList(bool stat);

#pragma endregion
	
#pragma region		서브다이얼로그 생성 및 삭제 함수

	void	OnSelchangeModSubCtrtab();
	void	OnSelchangingModSubCtrtab();

	bool	b_MasterDlg;

	void	InitModelSetup(void);
	void	EndModelSetup(void);

	CRect	InitOptionRect;
	CRect	InitOptionRect2;
	CTabCtrl				*m_pModOptTabWnd;


	CStandOptDlg			*m_pStandOptWnd;
	CStandOptMesDlg			*m_pStandOptMesWnd;
	CEtcModelDlg			*m_psubmodel[MAX_TEST_NUM];
	CModSetDlg				*m_pModSetDlgWnd;
	CModOptDlg				*m_pModOptDlgWnd;
	CModSelDlg				*m_pModSelDlgWnd;

	void	CModOpt_Create(void);
	void	CModSet_Create();
	void	CModSel_Create();
	void	CStandOpt_Create();

	void	CModSet_Delete();
	void	CModSel_Delete();
	void	CStandOpt_Delete();
	void	CModOpt_Delete(void);

	void	CEtcModel_Create(tINFO INFO);
	void	CEtcModel_Delete(int NUM);
	void	CEtcModel_Delete(tINFO INFO);

	COption_ResultWStand *m_pResWorkerOptWnd;
	CLot_StandDlg *m_pResLotWnd;
	void	CModel_Worker_Create(void);
	void	CModel_Worker_Delete(void);

	CStandDlg		*m_pStandWnd; 
	void	CStand_Create(void);
	void	CStand_Delete(void);

	CWorkList		*m_pWorkListWnd;
	CListCtrl		*m_pWorkList;
	
	void	WorkListCreate();
	void	WorkListDelete(void);

	CLGE_CamSetDlg *m_pLGE_CamSetWnd;
	void	CLGE_CamSet_Create();
	void	CLGE_CamSet_Delete();
	void	CLGE_CamSet_ShowWindow();

	CLGE_CamSet_ParticleDlg *m_pLGE_CamSet_PartiWnd;
	void	CLGE_CamSet_Parti_Create();
	void	CLGE_CamSet_Parti_Delete();
	void	CLGE_CamSet_Parti_ShowWindow();

	CControlDlg *m_pControlWnd;
	void	CControl_Create();
	void	CControl_Delete();
	void	CControl_ShowWindow();
#pragma endregion

#pragma region 광원 밝기조절



#pragma endregion

#pragma region TEST 관련

	BOOL	b_DoorOpen;
	BOOL	b_SocketReady;

	tResultVal	Model_Start();
	void	Model_Start_Pic(CDC *cdc,tResultVal *result);
	tResultVal	Model_End(int Number);
	
	BOOL	m_TOTAL_SUCCESS;

	BOOL b_Particle_fail;
	HWND FrameWnd;
	void		SaveScan(HWND hWnd, LPCTSTR lpszFileName);
	void	Original_ImageCapture();
	void	PNG_ImageCapture(CString TIME);
	void	RAW_ImageCapture(CString TIME);
	void	Capture(CString Path);
	void	SaveBitmapToDirectFile(CDC* pDC, int BitCount, CString Path);
	CString	imagepic_path_copy;
	CString	winimage_path_copy;
	bool	b_CaptureMode;
	bool	b_CaptureWindowMode;
	bool	b_ParticleCaptureMode;
	int	i_CapturePaticleMode;
	bool	b_Particle_Result;
	HANDLE DDBToDIB(CBitmap &bitmap, DWORD dwCompression, CPalette *pPal);
	bool WriteDIB(CString szFile, HANDLE hDIB);	
	CString			str_pCsvdata;
	bool b_ETCRunMODE;
	bool b_StartCommand;
	bool b_StopCommand;
	void	RUN_MODE_CHK(bool Mode);

	CString	imagepic_Passcopy;
	CString	imagepic_Failcopy;

	TESTData	TestData[20];
	BOOL CAMERA_STAT_CHK();
	void	OverlayRegionINIT(int data);
	void	Overlay_InputBuffer(bool mod);
	void	OverlayImageSeach();

	BOOL	b_Chk_CurrentChkEn;
	BOOL	b_Chk_Volton;
	BOOL	b_Chk_TestSkip;
	BOOL	b_Chk_AreaSensor;
	double	m_MinAmp;
	double	m_MaxAmp;
	double	m_Offset;
	double	m_MinAmp2;
	double	m_MaxAmp2;
	double	m_Offset2;
	double		m_MinAmp3;
	double		m_MaxAmp3;
	double	m_Offset3;
	double		m_MinAmp4;
	double		m_MaxAmp4;
	double	m_Offset4;
	double		m_MinAmp5;
	double		m_MaxAmp5;
	double	m_Offset5;

	double	m_CurrentVal;
	double	m_CurrentVal2;
	double	m_CurrentVal3;
	double	m_CurrentVal4;
	double	m_CurrentVal5;


	ST_MES_CURRENT	m_stMes[5];
	BOOL	Current_Wait;
	void	Input_Current_Detect();
	void	CurrentCheck();
	BOOL	Current_Detect();

	CMasterMonitorDlg	*m_pMasterMonitor;
	//CRMasterMonitorDlg	*m_pMasterMonitor;
	void	CMasterMo_Create(void);
	void	CMasterMo_Delete(void);
	bool b_masterBtn;


	COption_SubStand *m_pSubStandOptWnd;
	COption_ResultStand *m_pResStandOptWnd;
	void	CModelOpt_Stand_Create(void);
	void	CModelOpt_Stand_Delete(void);
	void	Model_ShowWindow(int inshow);

	//0x06 화각검사
	CAngle_Option *m_pAngleOptWnd;
	CAngle_ResultView *m_pResAngleOptWnd;
	void	CModelOpt_Angle_Create(tINFO INFO);
	void	CModelOpt_Angle_Delete();
	void	CModelOpt_Angle_ShowWindow(int inshow);

//0x0b 광량비
	CBrightness_Option *m_pBrightnessOptWnd;
	CBrightness_ResultView *m_pResBrightnessOptWnd;
	void	CModelOpt_Brightness_Create(tINFO INFO);
	void	CModelOpt_Brightness_Delete();
	void	CModelOpt_Brightness_ShowWindow(int inshow);
	
	//0x03 보임량광축
	bool	OpticEnable;
	CCENTER_DETECT_Option *m_pSubCPointOptWnd;
	CCENTER_DETECT_ResultView *m_pResCPointOptWnd;
	_TOTWrdata CenterVal;
	void	CModelOpt_CPoint_Create(tINFO INFO);
	void	CModelOpt_CPoint_Delete();
	void	CModelOpt_CPoint_ShowWindow(int inshow);

	CMicom_Center			*m_pMicomCenterWnd;
	void	MicomCenter_Popup();
	void	CMicomCenter_Create(void);
	void	CMicomCenter_Delete(void);

	//0x01 NTSC 테스트
	CNTSC_Option *m_pSubCNTSCOptWnd;
	CNTSC_ResultView *m_pResCNTSCOptWnd;

	void	CModelOpt_NTSC_Create(tINFO INFO);
	void	CModelOpt_NTSC_Delete();
	void	CModelOpt_NTSC_ShowWindow(int inshow);

	//0x0e 칼라검사
	CColor_Option *m_pColorOptWnd;
	CColor_ResultView *m_pResColorOptWnd;
	void	CModelOpt_Color_Create(tINFO INFO);
	void	CModelOpt_Color_Delete();
	void	CModelOpt_Color_ShowWindow(int inshow);

	//0x07 칼라반전
	CColorReversal_Option *m_pColorRvsOptWnd;
	CColorReversal_ResultView *m_pResColorRvsOptWnd;
	void	CModelOpt_ColorReversal_Create(tINFO INFO);
	void	CModelOpt_ColorReversal_Delete();
	void	CModelOpt_ColorReversal_ShowWindow(int inshow);

	//0x0d Distortion
	CDistortion_Option *m_pDistortionOptWnd;
	CDistortion_ResultView *m_pResDistortionOptWnd;
	void	CModelOpt_Distortion_Create(tINFO INFO);
	void	CModelOpt_Distortion_Delete();
	void	CModelOpt_Distortion_ShowWindow(int inshow);

	//0x09 IRFILTER
	CIRFilter_Option *m_pIRFilterOptWnd;
	CIRFilter_ResultView *m_pResIRFilterOptWnd;
	void	CModelOpt_IRFilter_Create(tINFO INFO);
	void	CModelOpt_IRFilter_Delete();
	void	CModelOpt_IRFilter_ShowWindow(int inshow);

		//0x0c//왜곡광축
	COPTIC_DETECT_Option *m_pSubCOpticPointOptWnd;
	COPTICAL_DETECT_ResultView *m_pResCOpticPointOptWnd;
	void	CModelOpt_COPoint_Create(tINFO INFO);
	void	CModelOpt_COPoint_Delete();
	void	CModelOpt_COPoint_ShowWindow(int inshow);

	CMicom_Optic		*m_pMicomOpticWnd;
	void	MicomOptic_Popup();
	void	CMicomOptic_Create(void);
	void	CMicomOptic_Delete(void);
	//0x0a 이물
	CParticle_Option *m_pParticleOptWnd;
	CParticle_ResultView *m_pResParticleOptWnd;
	void	CModelOpt_Particle_Create(tINFO INFO);
	void	CModelOpt_Particle_Delete();
	void	CModelOpt_Particle_ShowWindow(int inshow);


	//0x0f
	COverlayDetect_Option *m_pOverlay_CAN_OptWnd;
	COption_ResultOverlay *m_pResOverlay_CAN_OptWnd;
	void	CModelOpt_Overlay_CAN_Create(tINFO INFO);
	void	CModelOpt_Overlay_CAN_Delete();
	void	CModelOpt_Overlay_CAN_ShowWindow(int inshow);

	//0x1f
	CWarningDetect_Option *m_pWarning_CAN_OptWnd;
	COption_ResultWarning *m_pResWarning_CAN_OptWnd;
	void	CModelOpt_Warning_CAN_Create(tINFO INFO);
	void	CModelOpt_Warning_CAN_Delete();
	void	CModelOpt_Warning_CAN_ShowWindow(int inshow);


	//0x10 이물_LG
	BOOL	bParticleView;
	int		bParticleCnt;

	CParticle_Option_LG *m_pParticleOptLGWnd;
	CParticle_ResultView_LG *m_pResParticleOptLGWnd;
	void	CModelOpt_ParticleLG_Create(tINFO INFO);
	void	CModelOpt_ParticleLG_Delete();
	void	CModelOpt_ParticleLG_ShowWindow(int inshow);
	
	CRect	InitMasterMoRect;
	CParticle_View_LG *m_ParticleViewLG;
	void	CParticleViewLG_Create();
	void	CParticleViewLG_Delete();
	void	CParticleViewLG_Show(BOOL MODE);
	
	void	YUV_SAVE(LPCTSTR PATH);
	void	Y_SAVE(LPCTSTR PATH);
	void	SAVE_CSV(LPCSTR PATH);


	CParticle_View *m_ParticleView;
	bool b_ManualTestMode;
//	bool b_ETCRunMODE;
	bool b_Particle;

	//0x0C
	COption_FixedPattern *m_pFixedPatternOptWnd;
	CFixedPattern_ResultView *m_pResFixedPatternWnd;
	void	CModelOpt_FixedPattern_Create(tINFO INFO);
	void	CModelOpt_FixedPattern_Delete();
	void	CModelOpt_FixedPattern_ShowWindow(int inshow);

	//0x08//저조도
	CPatternNoise_Option *m_pPatternNoiseOptWnd;
	CPatternNoise_ResultView *m_pResPatternNoiseOptWnd;
	void	CModelOpt_PatternNoise_Create(tINFO INFO);
	void	CModelOpt_PatternNoise_Delete();
	void	CModelOpt_PatternNoise_ShowWindow(int inshow);

	//0x0g//Dynamic Range
	CDynamicRange_Option *m_pDynamicRangeOptWnd;
	CDynamicRange_ResultView *m_pResDynamicRangeOptWnd;
	void	CModelOpt_DynamicRange_Create(tINFO INFO);
	void	CModelOpt_DynamicRange_Delete();
	void	CModelOpt_DynamicRange_ShowWindow(int inshow);

	//0x05
	CResolutionDetection_Option *m_pResolutionOptWnd;
	CResolutionDetection_ResultView *m_pResResolutionOptWnd;
	void	CModelOpt_Resolution_Create(tINFO INFO);
	void	CModelOpt_Resolution_Delete();
	void	CModelOpt_Resolution_ShowWindow(int inshow);

	//0x15
	CSFRTILTOptionDlg *m_pSFROptWnd;
	CSFR_ResultView *m_pResSFROptWnd;
	void	CModelOpt_SFR_Create(tINFO INFO);
	void	CModelOpt_SFR_Delete();
	void	CModelOpt_SFR_ShowWindow(int inshow);

	//0x04 로테이트
	CRotate_Option		*m_pRotateOptWnd;
	CRotate_ResultView	*m_pResRotateOptWnd;
	void	CModelOpt_Rotate_Create(tINFO INFO);
	void	CModelOpt_Rotate_Delete();
	void	CModelOpt_Rotate_ShowWindow(int inshow);
	

	//0x1b MFGDATA W/R
	CSYSTEM_Option		*m_pSYSTEMOptWnd;
	CSYSTEM_ResultView	*m_pResSYSTEMOptWnd;
	void	CModelOpt_SYSTEM_Create(tINFO INFO);
	void	CModelOpt_SYSTEM_Delete();
	void	CModelOpt_SYSTEM_ShowWindow(int inshow);

	//0x1a
	CDTC_Option *m_pDTCOptWnd;
	CDTC_ResultView *m_pResDTCOptWnd;
	void	CModelOpt_DTC_Create(tINFO INFO);
	void	CModelOpt_DTC_Delete();
	void	CModelOpt_DTC_ShowWindow(int inshow);

	//0x1c
	CVER_Option *m_pVEROptWnd;
	CVER_ResultView *m_pResVEROptWnd;
	void	CModelOpt_VER_Create(tINFO INFO);
	void	CModelOpt_VER_Delete();
	void	CModelOpt_VER_ShowWindow(int inshow);

	//0x1c
	COverlayPosition_Option		*m_pOverlayPositionOptWnd;
	COverlayPosition_ResultView	*m_pResOverlayPositionOptWnd;
	void	CModelOpt_OVERLAYPOSITION_Create(tINFO INFO);
	void	CModelOpt_OVERLAYPOSITION_Delete();
	void	CModelOpt_OVERLAYPOSITION_ShowWindow(int inshow);

	//0x12 저조도(LG 백점검사)
	CLowLight_Option		*m_pLowLightOptWnd;
	CLowLight_ResultView	*m_pResLowLightOptWnd;
	void	CModelOpt_LowLight_Create(tINFO INFO);
	void	CModelOpt_LowLight_Delete();
	void	CModelOpt_LowLight_ShowWindow(int inshow);

	//0xff Verify
	COption_Verify *m_pVerifyOptWnd;
	CVerify_ResultView *m_pResVerifyOptWnd;
	void	CModelOpt_Verify_Create(tINFO INFO);
	void	CModelOpt_Verify_Delete();
	void	CModelOpt_Verify_ShowWindow(int inshow);

	C_3D_Depth_Option *m_p3D_DepthOptWnd;
	C_3D_Depth_ResultView *m_pRes3D_DepthOptWnd;

	void CModelOpt_3D_Depth_Create(tINFO INFO);
	void CModelOpt_3D_Depth_Delete();
	void CModelOpt_3D_Depth_ShowWindow(int inshow);

	CHotPixel_Option *m_pHotPixel_OptWnd;
	CHotPixel_ResultView *m_pResHotPixelOptWnd;
	void CModelOpt_HotPixel_Create(tINFO INFO);
	void CModelOpt_HotPixel_Delete();
	void CModelOpt_HotPixel_ShowWindow(int inshow);

	CDepthNoise_Option		*m_pDepthNoise_OptWnd;
	CDepthNoise_ResultView	*m_pResDepthNoiseOptWnd;
	void CModelOpt_DepthNoise_Create(tINFO INFO);
	void CModelOpt_DepthNoise_Delete();
	void CModelOpt_DepthNoise_ShowWindow(int inshow);


	Temperature_Option		*m_pTemperature_OptWnd;
	CTemperature_ResultView	*m_pResTemperatureOptWnd;
	void CModelOpt_Temperature_Create(tINFO INFO);
	void CModelOpt_Temperature_Delete();
	void CModelOpt_Temperature_ShowWindow(int inshow);

	int count_V;
	//----------------DCC W/R 작업------------------------------------------------
	CString str_FWrdata[5];//0: HW_READ 1: SW_READ 2: CAN READ 3: 제조일자 4: 품번
	
	BOOL	DCC_DTC_READ();
	void	DCC_DTC_READ_ACK();
	BOOL	Buf_Wait_DTCRD;
	BOOL	m_DTCRDSTATE;
	BYTE	DTCRDDATA[4];
	BYTE	DTC_BUFDATA[8];

	BOOL	DCC_DTC_ERASE();
	void	DCC_DTC_ERASE_ACK();
	BOOL	Buf_Wait_DTCER;
	
	CString str_SYS_RD[5];
	BYTE	SYS_BUFDATA[35];
	BOOL	m_SYSRDSTATE;
	int		m_RdNum;
	BOOL	DCC_SYSTEM_READ_ALL();
	BOOL	DCC_SYSTEM_READ(int Number);
	void	DCC_SYSTEM_READ_ACK();
	BOOL	Buf_Wait_SYSRD;

	CString str_SYS_WR[5];
	BOOL	DCC_SYSTEM_WRITE(...);
	BOOL	Buf_Wait_SYSWR;
	void	DCC_SYSTEM_WRITE_ACK();

	CString str_VER_RD[2];
	DWORD	m_VER_RD[2];
	BOOL	Buf_Wait_VERRD;
	BOOL	DCC_VER_READ();
	void	DCC_VER_READ_ACK();

	BOOL	Buf_Wait_DLUP;
	BOOL	DL_UPDATEMODE(BOOL state);

	BOOL	Buf_Wait_DLADDR;
	BOOL	DL_ADDRSET(DWORD ADDR,DWORD SIZE);

	BOOL	Buf_Wait_DLDATASET;
	BOOL	DL_DATASET(_EtcWrdata * INDATA,int NUM);

	BOOL	Buf_Wait_DLDATAWR;
	BOOL	DL_DATAWR(BYTE BLS,BYTE SESIZE);

	BYTE	m_RDOffset[2];
	BOOL	Buf_Wait_OFFRD;
	BOOL	DL_OFFSETRD();
	void	DL_OFFSETRD_ACK();

	BOOL	Buf_Wait_OFFWR;
	BOOL	DL_OFFSETWR(BYTE OFFSETX,BYTE OFFSETY);
	void	DL_OFFSETWR_ACK();

	void	READ_DATA();
	BOOL	MCU_READ_MODE(BOOL MODE);
	BOOL	MCU_READ_ADDR(BYTE* ADDR);
	BOOL	MCU_READ_START();
	BOOL	MCU_READ_START_SEND();
	BYTE	CAMREADBUFF[256];

	unsigned int  m_DCCTotalChk;
	BYTE	m_DCCChksum[2];//0:hige 1:low
	BOOL	Buf_Wait_CHKSUM;
	BOOL	DCC_CHECKSUM();
	void	DCC_CHECKSUM_ACK();
	
#pragma endregion
#pragma region		POGO 관련
	CString Pogo_Name;
	CString Pogo_path;
	CString PogoSelect_path;

	int		i_PogoPinSET;
	int		i_PogoCnt;
	BOOL	b_Pogostate;
	void	Pogo_Load_Prm();
	void	POGOGROUPNAME(LPCSTR lpcszString, ...);
	void	SetCountUpdate(void);
	void	TestCountText(LPCSTR lpcszString, ...);

	CPogo_Option *m_pOptPogoDlgWnd;
	void 	COptPogo_Create();
	void	COptPogo_Delete();

#pragma endregion


#pragma region 관리자,작업자 변환

	BOOL	b_UserMode;
	UINT	STATID;

#pragma endregion

#pragma region MES & LOT 관련
	/////////////////////////////////////////////MES
	CString		m_szMesPacket[MAX_MES_NUM];


	BOOL		b_MesRunEn;
	BOOL		b_AutoMesConnect;
	CString		m_str_ModelID;
	CString		m_strLotId;
	CString		m_strLotIdFull;
	CString		m_strdegre;

	CSocketManager	m_SocketManager;
	SockAddrIn		m_SockPeer;
	int				m_nSockType;

	int				m_nSockRetryConnect;

	unsigned int m_socket;
	BOOL	b_Chk_MESEn;
	CString	m_szIp;
	CString m_szPort;
	CString m_szCode;
	CString MESPath;

	void	SocketConnect();
	void	SocketDisconnect();
	void	MESRunStart();
	void	MESRunStop();
	void	MesSavFileGen(int nNum);

	CString m_AddFinalMesResult;
	CString m_szMesResult;
	CString Mes_Result_C();

	/////////////////////////////////////////////LOT
	CString			SAVE_IMAGEPATH;

	//---Lot관련
	CString			SAVE_FOLDERPATH;
	CString			SAVE_FOLDERPATH_P;
	CString			Excelfilename_Path;
	CString			DATE_FOLDERPATH;
	CString			DATE_FOLDERPATH_P;
	CString			MODEL_FOLDERPATH;
	CString			MODEL_FOLDERPATH_P;
	CString			ExcelfilePath ;
	CString			LOT_OPERATOR;
	CString			modelLotPath;
	CString			modelName;

	int				LotMod;
	CString			LOT_NUMBER;
	CString			LOT_SAVE_FOLDERPATH;
	CString			LOT_DATE_FOLDERPATH;
	CString			LOT_MODEL_FOLDERPATH;
	CString			LOT_FOLDERPATH;
	

	int	Lot_StartCnt;
	int Lot_InsertIndex; 
	int Lot_Passint;
	int Lot_Failint;
	int Lot_Totalint;
	double Lot_Percentint;

	int	TestItemNum;
	int ListItemNum;



	CNewLot *m_pNewLotWnd;
	void	ClickedBtnLotStart();
	void	ClickedBtnLotExit();
	void	NewLotCreate();
	
	void LOT_RESET_LIST(int state);
	void LOT_CREATE_Folder(LPCTSTR lpcszString, int saveNum, ...);

	CExistLot *m_ExistLotWnd;
	void	CExistLOTDlg_Create();

	void	LOT_SAVE_EXCEL(LPCSTR lpcszString,CString Item[],int ItemNum,CListCtrl *List, CString Data, ...);
	void	LOT_SAVE_EXCEL_TOTAL(LPCSTR lpcszString, CString Item[], int ItemNum, CListCtrl *List, CString Data, ...);
	bool	LOT_EXCEL_UPLOAD(LPCSTR lpcszString, CListCtrl *List,int state, int *NUM, ...);

	void	SETTING_LotInfo();
	void	CREATE_Folder(LPCTSTR lpcszString, int saveNum, ...);
	void	ALL_WORKLIST_UPLOAD();
	bool	EXCEL_UPLOAD(LPCSTR lpcszString, CListCtrl *List,int state, int *NUM, ...);

	void	EmptyList_Input_X_Data(CListCtrl *List);
	void	InsertList();
	void	Set_List(CListCtrl *List);
	int InsertIndex;
	CString strTime, strDay, strEndTime;

	void	SAVE_EXCEL(LPCSTR lpcszString,CString Item[],int ItemNum,CListCtrl *List, CString Data, ...);
	void	SAVE_EXCEL_TOTAL(LPCSTR lpcszString, CString Item[], int ItemNum, CListCtrl *List, CString Data, ...);
	void Set_List_C(CListCtrl *List);
	void InsertList_C();
	int InsertIndex_C;
	int Lot_InsertIndex_C;
	int ListItemNum_C;
	void EXCEL_SAVE_C();
	bool EXCEL_UPLOAD_C();
	void	FAIL_UPLOAD();

	void	SAVE_FILE();
	void	FileCopy_AnotherPath_Func();

	CString old_DATE_FOLDERPATH;
	CString Copy_DATE_FOLDERPATH;
	CString	SAME_DATE_COPYPATH;
	CString SAVEFILE_RPATH;

	int				YEAR;
	int				MONTH;
	int				DAY;
	int				StartCnt;
	int				StartCnt_C;
	CString		SaveData_Date;



	//--메인
	CString TestModelName[20];

	void 	LOT_Set_List(CListCtrl *List);

	int Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

	//--전류

	void 	LOT_Set_List_C(CListCtrl *List);
	void	LOT_InsertDataList_C();

	int		Lot_StartCnt_C;
	int		Lot_InsertNum_C;

	void	LOT_EXCEL_SAVE_C();
	bool	LOT_EXCEL_UPLOAD_C();


#pragma endregion

#pragma region 통신관련

	CCommThread		m_CameraPort;  //서브 관련
	_SERIAL			m_CamSerial;
	_SERIAL			m_OldCamSerial;

	CCommThread		m_ControlPort_Center;  //센터 광원컨트롤 관련
	_SERIAL			m_ConSerial_Center;
	_SERIAL			m_OldConSerial_Center;

	CCommThread		m_ControlPort_Side;  //사이드 광원컨트롤 관련
	_SERIAL			m_ConSerial_Side;
	_SERIAL			m_OldConSerial_Side;

	BOOL	Portopen_Cam();
	BOOL	PortChk_Cam();
	void	PortClose_Cam();
	BOOL	PortTst_Cam();
	BOOL	AutoPortChk_Cam();
	char	SendData8Byte_Cam(unsigned char * TxData, int Num);
	BOOL	Read_Comport_Cam(CCommThread* serialport); //통신입력 관련//
	CData	m_SendData_Cam;
	CData	m_RXData_Cam;//
	BYTE	m_SerialInput_Cam[1024];//
	void	Chk_Trans_Cam();//
	BOOL	Buf_Wait_Cam;//

	//-------------------------------------------------------Center광원 통신
	CData	m_RXData_ConCenter;
	BYTE	m_SerialInput_ConCenter[1024];
	BOOL	Buf_Wait_ConCenter;
	char	SendData8Byte_ConCenter(unsigned char * TxData, int Num);
	BOOL	Read_Comport_ConCenter(CCommThread* serialport); //통신입력 관련
	void	Chk_Trans_ConCenter();

	BOOL	Portopen_ConCenter();
	BOOL	PortChk_ConCenter();
	void	PortClose_ConCenter();
	BOOL	PortTst_ConCenter();
	BOOL	AutoPortChk_ConCenter();
	//-------------------------------------------------------Side광원 통신
	CData	m_RXData_ConSide;
	BYTE	m_SerialInput_ConSide[1024];
	BOOL	Buf_Wait_ConSide;
	char	SendData8Byte_ConSide(unsigned char * TxData, int Num);
	BOOL	Read_Comport_ConSide(CCommThread* serialport); //통신입력 관련
	void	Chk_Trans_ConSide();

	BOOL	Portopen_ConSide();
	BOOL	PortChk_ConSide();
	void	PortClose_ConSide();
	BOOL	PortTst_ConSide();
	BOOL	AutoPortChk_ConSide();
	//-------------------------------------------------------//광원 제어
	int Power_Ctl_Data[5];
	int Current_Ctl_Data[5];

	bool Power_Total_Control();
	bool Power_Total_Control_off();
	bool Power_Each_Control(int SLOT);
	bool Power_Each_Control_Off(int SLOT);
	bool Control_Voltage_SLOT(int SLOT, int Volt);
	bool Control_Current_SLOT(int SLOT, int Current);
	bool Control_Voltage(int Board, int CH, int Volt);
	bool Control_Current(int Board, int ch, int Current);

	//-------------------------------------------------------

	BOOL	Get_Overcurrent;
	void	BUTTON_STATE_CONTROL();
	BOOL	CHK_SENSER();
	BOOL	b_Cylinder_In;
	BOOL	b_Cylinder_Out;
	BOOL	Cylinder_MOVE(int InOut);
	BOOL	SIGNAL_LAMP(int MODE); // 0:ALL OFF 1:RED 2:YELLOW 3:GREEN

	BOOL	Get_Wait;
	BOOL	Buf_IMG_SUC;
	void	Overlay_Move_Stand(int Angle,BYTE LAN,BYTE PAL);  
	BOOL	Overlay_Move(int Angle,BYTE LAN,BYTE PAL); 
	BOOL	Overlay_Move(int Angle);
	BOOL	Overlay_Move(BYTE LAN,BYTE PAL);
	void	Overlay_Start();
	BOOL	Init_Wait;
	BOOL	Overlay_Init();
	BOOL	Enable_Wait;
	BOOL	Overlay_Enable(BOOL stat);
	BOOL	Overlay_State;

	double	dVoltSave;
	BOOL	b_PWRON;
	BOOL Power_On(UINT nMODE = VR_PO_REGISTER);
	BOOL	Power_Off();
	BOOL	Power_Rst();

	BOOL	Manual_Exp_Mode();
	BOOL	Manual_Exp_Gain_Ctrl(CString Target_Value);

	CCommDlg				*m_pCommWnd;
	void	Create_CommDlg();
	void	Delete_CommDlg();

	void	PortUpdate();

	int		m_Voltpp;
	int		m_VoltSync;
	BOOL	VSync_Wait;
	void	Video_Sync_Detect();
	void	VideoSyncCheck();
	void	OnImgCapture(CString Path);
#pragma endregion


#pragma region		모터 IO 관련
	BOOL m_bSafetySensor;
	BOOL m_bDoorSensor;
	BOOL m_bUseSensor;
	BOOL m_EmergencyStatus;

	AXISMOVEPARAM	m_AxisParam[MAX_NUM_AXIS];
	double			m_dbMotorWarningPos;
	CDlgOrigin*		m_pDlgOrigin;
	CMotionControl	m_Motion;
	BOOL			b_MotorzStandbyCheck;

	BOOL	InitMotor();
	void	RUN_LED_CONTROL(int stat);

	int		m_dChartDistance;
	int		m_dXModuleDistance;
	int		m_dYModuleDistance;
	int		m_dDistortDistance;
	int		m_dSModuleDistance;

	BOOL	b_SubMotorInit;
	BOOL	JIG_MOVE();
	BOOL	DISTORTION_JIG_MOVE();

	bool	b_move_Z;
	BOOL	Move_Chart(int distance, BOOL bMove=TRUE);
	BOOL	Move_ModuleX(int distance, BOOL bMove=TRUE);
	BOOL	Move_ModuleY(int distance, BOOL bMove = TRUE);
	BOOL	Move_ModuleS(int distance, BOOL bMove = TRUE);

	BOOL	IRDust_ONOFF(BOOL IR, BOOL Dust);
	BOOL	LightRAY_ONOFF(BOOL Light, BOOL Ray);
	BOOL	Light_ONOFF(bool CHK);
	BOOL	RAY_ONOFF(bool CHK);

	BOOL	IO_CHECK_LIGHT_RAY(BOOL Light,BOOL Ray);

	BOOL	ALARM_ONOFF(int MODE);
	BOOL	ALARM_OPERATING(int MODE);


	BOOL	BUZZER_ONOFF(int MODE);
	BOOL	BUZZER_OPERATING(int MODE);
	BOOL	Motion_All_Stop();

	int Buzzer_Cnt;
	BOOL Buzzer_MODE;

	int Alarm_Cnt;
	BOOL Alarm_MODE;

	int i_Alarmdelay;
	int i_Buzzerdelay;
	int i_Lightondelay;
	int i_Rayondelay;
	int i_Lightoffdelay;
	int i_VoltageDelay;

	BOOL	m_bAreaSensor;
	BOOL	m_bDoorOpen;

#pragma endregion

	void	IMG_Capture(CString SavePath);
//	void	SaveScan(HWND hWnd, LPCTSTR lpszFileName);

	void	WaitCheckMachineSafety();

	ST_EEPROM m_stEEPROM;
	ST_EEPROM_DATA m_stEEPROMTEST_DATA;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

	afx_msg LRESULT OnCamCommunication(WPARAM wParam, LPARAM lParam);//통신관련
	afx_msg LRESULT OnConCommunication_Center(WPARAM wParam, LPARAM lParam);//통신관련
	afx_msg LRESULT OnConCommunication_Side(WPARAM wParam, LPARAM lParam);//통신관련
	afx_msg LRESULT OnCamDisplay(WPARAM wParam, LPARAM lParam);//영상 쓰레드에서 영상이 입력받았을경우 실행됨

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg LRESULT OnUpdateConnection(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTCPReceive(WPARAM wParam, LPARAM lParam);		// 소켓통신 메시지 받기

	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CStatic m_CamFrame;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedBtnUserChange();
protected:
	virtual void OnCancel();
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedBtnWorkButton();
	afx_msg void OnCbnSelchangeModelName();
	afx_msg void OnBnClickedBtnTestSetting();
	afx_msg void OnBnClickedBtnEtcSetting();
	afx_msg void OnBnClickedBtnMasterSet();
	afx_msg void OnBnClickedBtnMain();
	afx_msg void OnEnSetfocusEditPortname();
	afx_msg void OnEnSetfocusEditPortstate();
	afx_msg void OnEnSetfocusEditCamname();
	afx_msg void OnEnSetfocusEditCamstate();
	afx_msg void OnEnSetfocusTestStatus();
	afx_msg void OnBnClickedCancel();
	CListCtrl MainWork_List;
	CListCtrl m_Lot_CurrentList;
	CListCtrl m_Lot_MainWorkList;
	CListCtrl m_CurrList;
	CEdit m_pMesRecv;
	afx_msg void OnTcnSelchangeTabResult(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTcnSelchangingTabResult(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBtnStart();
	afx_msg void OnBnClickedBtnStop();
	afx_msg void OnEnSetfocusEditPogoname();
	afx_msg void OnEnSetfocusEditCountTest();
	afx_msg void OnEnSetfocusEditProtime();
	afx_msg void OnEnSetfocusEditProtime2();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedBtnCommu();
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
	afx_msg void OnCapture();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton8();
	afx_msg void OnBnClickedButton7();
	afx_msg void OnBnClickedButton9();
	afx_msg void OnBnClickedButton10();
	afx_msg void OnBnClickedButton11();
	afx_msg void OnBnClickedBtnCapture();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton12();
	afx_msg void OnBnClickedButton13();
	afx_msg void OnBnClickedButton14();
	afx_msg void OnBnClickedButton2();
	CListCtrl m_Lot_EepromList;
	CListCtrl m_Work_EepromList;
	afx_msg void OnBnClickedInit();
};
