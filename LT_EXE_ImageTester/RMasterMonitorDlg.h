#pragma once
#include "afxwin.h"


// CRMasterMonitorDlg 대화 상자입니다.

class CRMasterMonitorDlg : public CDialog
{
	DECLARE_DYNAMIC(CRMasterMonitorDlg)

public:
	CRMasterMonitorDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CRMasterMonitorDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_RMASTERMONITOR };

	void	Focus_move_start();
	CFont	font1,font2;
	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT);
	void	StatePrintf(LPCSTR lpcszString, ...);
	
	void	SubBmpPic(UINT nIDResource);
	void	SubBmpPic(LPCSTR lpBitmapName);

	CComboBox	*pDirectModeCombo;

	CvPoint CenterData;
	CvPoint ACenterData;
	int		MasterOffsetX;
	int		MasterOffsetY;
	int		MasterStandX;
	int		MasterStandY;
	int		OrgStandX;
	int		OrgStandY;

	int		m_dXModuleDistance;
	int		m_dYModuleDistance;
	int		m_RatePix;

	void	InitSet();
	void	UpdateMaState();
	void	AutosetRunning();
	BOOL	ClickNUM;

	void	UpdatePositionVal();
	void	MasterState_Val(int col,LPCSTR lpcszString, ...);
	void	PositionX_Txt(LPCSTR lpcszString, ...);
	void	PositionY_Txt(LPCSTR lpcszString, ...);
	void	PositionValX_Txt(LPCSTR lpcszString, ...);
	void	PositionValY_Txt(LPCSTR lpcszString, ...);
	void	Master_OffsetX_Txt(LPCSTR lpcszString, ...);
	void	Master_OffsetY_Txt(LPCSTR lpcszString, ...);
	void	Master_AxisX_Txt(LPCSTR lpcszString, ...);
	void	Master_AxisY_Txt(LPCSTR lpcszString, ...);
	void	Pixel_Rate_Set();
	void	OnCamDisplay(CDC *cdc);

	COLORREF Matxtcol;
	COLORREF MaBkcol;

	CvPoint ChartPointSet();
	CvPoint GetCenterPoint(LPBYTE IN_RGB);
	double GetDistance(int x1, int y1, int x2, int y2);
	void	RUN_MODE_CHK(bool Mode);
	BOOL	b_Running;
	BOOL	b_TESTMOD;
	void	PositionValChk();

private :
	CWnd	*m_pMomWnd;	
	CEdit		*pTStat;
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CStatic m_stCenterDisplay;
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
//	afx_msg void OnBnClickedBtnPowerOn2();
	afx_msg void OnBnClickedBtnPowerOn2();
	afx_msg void OnBnClickedBtnPowerOff();
	afx_msg void OnBnClickedBtnOverlayon();
	afx_msg void OnBnClickedBtnOverlayoff();
	afx_msg void OnBnClickedBtnAutoset();
	afx_msg void OnEnChangeEditMoffsetx();
	afx_msg void OnEnChangeEditMoffsety();
	afx_msg void OnBnClickedBtnPositionStand();
	afx_msg void OnBnClickedBtnPositionMove();
	afx_msg void OnBnClickedBtnPositionSet();
	afx_msg void OnEnChangePositionValx();
	afx_msg void OnEnChangePositionValy();
	afx_msg void OnBnClickedBtnCenterdetect();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnChangeEditPixelrate();
	afx_msg void OnBnClickedButtonUserExit();
protected:
	virtual void OnCancel();
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};
