#pragma once


// CHotPixel_ResultView 대화 상자입니다.

class CHotPixel_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CHotPixel_ResultView)

public:
	CHotPixel_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CHotPixel_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_HOTPIXEL };
	CFont ft1;
	COLORREF txcol_TVal,bkcol_TVal;
	void	HotPixel_TEXT(LPCSTR lpcszString, ...);
	void	HotPixelNUM_TEXT(LPCSTR lpcszString, ...);
	void	RESULT_TEXT(unsigned char col,LPCSTR lpcszString, ...);
	void	Setup(CWnd* IN_pMomWnd);
void	Initstat();
private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnSetfocusEditHotPixeltxt();
	afx_msg void OnEnSetfocusEditHotPixelnum();
	afx_msg void OnEnSetfocusEditHotPixelresult();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
