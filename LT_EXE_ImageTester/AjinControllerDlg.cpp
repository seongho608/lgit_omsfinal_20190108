// AjinControllerDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "FINAL_TEST_M.h"
#include "FINAL_TEST_MDlg.h"
#include "AjinControllerDlg.h"

#include "AXL.h"
#include "AXM.h"
#include "AXHS.h"
#include "AXDev.h"
// CAjinControllerDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAjinControllerDlg, CDialog)

CAjinControllerDlg::CAjinControllerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAjinControllerDlg::IDD, pParent)
{

	m_lAxisNo		= -1;					// 제어할 축 번호 초기화
	m_lAxisCount	= 0;					// 제어 가능한 축갯수 초기화
	m_dwModuleID	= 0;					// 제어할 축의 모듈ID 초기화
}

CAjinControllerDlg::~CAjinControllerDlg()
{
}

void CAjinControllerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_SEL_AXIS, m_cboSelAxis);
	DDX_Control(pDX, IDC_JOG_MOVE1, m_btnJogMoveP);
	DDX_Control(pDX, IDC_JOG_MOVE2, m_btnJogMoveN);
}

BEGIN_MESSAGE_MAP(CAjinControllerDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_SETTING_MOT, &CAjinControllerDlg::OnBnClickedSettingMot)
	ON_CBN_SELCHANGE(IDC_CBO_SEL_AXIS, &CAjinControllerDlg::OnCbnSelchangeCboSelAxis)
	ON_BN_CLICKED(IDC_BTN_LOAD_FILE, &CAjinControllerDlg::OnBnClickedBtnLoadFile)
	ON_BN_CLICKED(IDC_BTN_SAVE_FILE, &CAjinControllerDlg::OnBnClickedBtnSaveFile)
	ON_BN_CLICKED(IDC_PARAMS_APPLY, &CAjinControllerDlg::OnBnClickedParamsApply)
	ON_MESSAGE(UM_JOG_BTN_DN, OnJogBtnDn)
	ON_MESSAGE(UM_JOG_BTN_UP, OnJogBtnUp)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_ORIGIN, &CAjinControllerDlg::OnBnClickedButtonOrigin)
	ON_BN_CLICKED(IDC_BUTTON_TESTMOVE, &CAjinControllerDlg::OnBnClickedButtonTestmove)
	ON_BN_CLICKED(IDC_BUTTON_STOP, &CAjinControllerDlg::OnBnClickedButtonStop)
	ON_BN_CLICKED(IDC_CHK_SERVO_ON, &CAjinControllerDlg::OnBnClickedChkServoOn)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CAjinControllerDlg 메시지 처리기
void CAjinControllerDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;

}

BOOL CAjinControllerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	for(int i = 0; i < 8; i++)
	{
		m_bTestActive[i]	= FALSE;
		m_hTestThread[i]	= NULL;
	}

	// 연결여부
	InitMotorSetup();
	InitLibrary();					// 라이브러리 초기화 및 Mot파일을 불러옵니다.
	AddAxisInfo();					// 검색된 축 정보를 ComboBox에 등록/Control들을 초기화합니다.
	UpdateState();					// 모션보드의 상태와 Control들의 상태를 일치시킵니다.

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}


// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CAjinControllerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.

void CAjinControllerDlg::InitMotorSetup(void){

	//CMotorSet_Create();




	//	MainModelSel();//경로상에 폴더가 있는지 확인한다.
	//	ModelSet(); //luri 파일을 읽어 각 모델이 무엇인지 파악한다.
	//	ModelGen();

}

BOOL CAjinControllerDlg::UpdateState()
{
	if(m_lAxisCount < 1)	return FALSE;

	DWORD dwAbsRelMode = POS_ABS_MODE, dwProfile = ASYM_S_CURVE_MODE, dwOnOff = 0;

	//++ 지정한 축의 서보온 상태를 반환합니다.
	AxmSignalIsServoOn(m_lAxisNo, &dwOnOff);
	CheckDlgButton(IDC_CHK_SERVO_ON, dwOnOff);

	////++ 지정한 축의 구동 좌표계 설정값을 확인합니다.
	//AxmMotGetAbsRelMode(m_lAxisNo, &dwAbsRelMode);	
	//CheckRadioButton(IDC_RDO_ABS, IDC_RDO_REL, IDC_RDO_ABS + dwAbsRelMode);

	////++ 지정한 축의 구동 프로파일 설정값을 확인합니다.
	//AxmMotGetProfileMode(m_lAxisNo, &dwProfile);
	//if (dwProfile > 2) m_cboProfile.SetCurSel(dwProfile-1);
	//else m_cboProfile.SetCurSel(dwProfile);


	double  dInitPos, dInitVel, dInitAccel, dInitDecel;

	AxmMotGetParaLoad(m_lAxisNo, &dInitPos, &dInitVel, &dInitAccel, &dInitDecel);

	SetDlgItemDouble(IDC_EDT_VELOCITY, dInitVel);
	SetDlgItemDouble(IDC_EDT_ACCEL, dInitAccel);
	SetDlgItemDouble(IDC_EDT_DECEL, dInitDecel);


	return TRUE;
}




void CAjinControllerDlg::OnBnClickedSettingMot()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

}

void CAjinControllerDlg::OnCbnSelchangeCboSelAxis()
{
	m_lAxisNo = m_cboSelAxis.GetCurSel();		// 축 선택 ComboBox에서 선택된 축번호를 제어할 축 번호 변수에 설정합니다.

	//++ 지정한 축번호의 모듈ID를 반환합니다.
	// [INFO] 여러개의 정보를 읽는 함수 사용시 불필요한 정보는 NULL(0)을 입력하면 됩니다.
	AxmInfoGetAxis(m_lAxisNo, NULL, NULL, &m_dwModuleID);

	UpdateState();	

	//m_pMotorSetWnd->OnSelchangeCboSelAxis();
}

void CAjinControllerDlg::OnBnClickedBtnLoadFile()
{

	CString	temp;
	CFileDialog dlg(TRUE, "Mot", NULL, OFN_HIDEREADONLY, "AXM File (*.mot)|*.mot|All Files (*.*)|*.*||");

	if (dlg.DoModal() == IDOK){
		LPTSTR strFilename;
		temp		= dlg.GetPathName();
		strFilename	= temp.GetBuffer(0);

		//++ 함수 실행 성공시 지정한 Mot파일의 설정값으로 모션축 설정이 일괄 변경됩니다.
		if (AxmMotLoadParaAll(strFilename) != AXT_RT_SUCCESS)
			AfxMessageBox("File load fail!");		
		else{
			//	m_pMotorSetWnd->UploadPrm(temp);

			UpdateState();		// 변경된 모션축의 상태와 Control들의 상태를 일치시킵니다.
		}	

	}	
}


void CAjinControllerDlg::OnBnClickedBtnSaveFile()
{
	CString temp;
	CFileDialog dlg(FALSE, "mot", NULL, OFN_HIDEREADONLY, "AXM File (*.mot)|*.mot|All Files (*.*)|*.*||");

	if(dlg.DoModal() == IDOK)
	{
		temp = dlg.GetPathName();
		LPTSTR strFilename = temp.GetBuffer(0);
		//++ 함수 실행 성공시 각각의 모션축 설정값으로 지정한 Mot파일을 생성 및 저장 됩니다.
		if (AXT_RT_SUCCESS != AxmMotSaveParaAll(strFilename))
			AfxMessageBox( "File save fail!");
		temp.ReleaseBuffer();	
	}		
}

void CAjinControllerDlg::OnBnClickedParamsApply()
{
	double  dInitPos, dInitVel, dInitAccel, dInitDecel;

	dInitPos	= GetDlgItemDouble(IDC_STC_ACT_POS);
	dInitVel	= GetDlgItemDouble(IDC_EDT_VELOCITY);
	dInitAccel	= GetDlgItemDouble(IDC_EDT_ACCEL);
	dInitDecel	= GetDlgItemDouble(IDC_EDT_DECEL);
	//++ 지정 축의 사용자 관련 파라메타들을 설정합니다.
	AxmMotSetParaLoad(m_lAxisNo, dInitPos, dInitVel, dInitAccel, dInitDecel);	
}

LRESULT CAjinControllerDlg::OnJogBtnDn(WPARAM wParam, LPARAM lParam)
{
	CString strData;
	DWORD	dwRetCode;
	double	dJogVel, dJogAcc, dJogDec;

	dJogVel	= fabs(GetDlgItemDouble(IDC_JOG_VELOCITY));
	dJogAcc	= fabs(GetDlgItemDouble(IDC_JOG_ACCEL));
	dJogDec	= fabs(GetDlgItemDouble(IDC_JOG_DECEL));

	switch(wParam)
	{
	case IDC_JOG_MOVE1:		
		//++ 지정한 축을 (+)방향으로 지정한 속도/가속도/감속도로 모션구동합니다.
		dwRetCode = AxmMoveVel(m_lAxisNo, dJogVel, dJogAcc, dJogDec);
		if(dwRetCode != AXT_RT_SUCCESS){
			strData.Format("AxmMoveVel return error[Code:%04d]", dwRetCode);
			MessageBox(strData, "Error", MB_OK | MB_ICONERROR);
		}
		break;

	case IDC_JOG_MOVE2:
		//++ 지정한 축을 (-)방향으로 지정한 속도/가속도/감속도로 모션구동합니다.
		dwRetCode = AxmMoveVel(m_lAxisNo, -dJogVel, dJogAcc, dJogDec);
		if(dwRetCode != AXT_RT_SUCCESS){
			strData.Format("AxmMoveVel return error[Code:%04d]", dwRetCode);
			MessageBox(strData, "Error", MB_OK | MB_ICONERROR);
		}
		break;
	}

	return 0;
}

LRESULT CAjinControllerDlg::OnJogBtnUp(WPARAM wParam, LPARAM lParam)
{
	//++ 지정한 축의 Jog구동(모션구동/원점검색구동)을 감속정지합니다.
	AxmMoveSStop(m_lAxisNo);	
	return 0;
}

void CAjinControllerDlg::OnTimer(UINT_PTR nIDEvent)
{
	double dCmdPos = 0.0, dActPos = 0.0, dCmdVel = 0.0;

	switch(nIDEvent){
	case TM_DISPLAY:		
		//++ 지정한 축의 지령(Command)위치를 반환합니다.
		AxmStatusGetCmdPos(m_lAxisNo, &dCmdPos);
		if(m_dOldCmdPos != dCmdPos){
			SetDlgItemDouble(IDC_STC_CMD_POS, dCmdPos);
			m_dOldCmdPos = dCmdPos;
		}

		//++ 지정한 축의 실제(Feedback)위치를 반환합니다.
		AxmStatusGetActPos(m_lAxisNo, &dActPos);
		if(m_dOldActPos != dActPos){
			SetDlgItemDouble(IDC_STC_ACT_POS, dActPos);
			m_dOldActPos = dActPos;
		}

		//++ 지정한 축의 구동 속도를 반환합니다.
		AxmStatusReadVel(m_lAxisNo, &dCmdVel);
		if(m_dOldCmdVel != dCmdVel){
			SetDlgItemDouble(IDC_STC_CMD_VEL, dCmdVel);
			m_dOldCmdVel = dCmdVel;
		}
		break;
	}

	CDialog::OnTimer(nIDEvent);
}

BOOL CAjinControllerDlg::InitLibrary()
{
	//++ AXL(AjineXtek Library)을 사용가능하게 하고 장착된 보드들을 초기화합니다.
	if ((AxlOpen(7) != AXT_RT_SUCCESS)){
		AfxMessageBox("AxlOpen Fail...");
		return FALSE;
	}

	// ※ [CAUTION] 아래와 다른 Mot파일(모션 설정파일)을 사용할 경우 경로를 변경하십시요.
	char szFilePath[] = "C:\\Program Files\\EzSoftware RM\\EzSoftware\\MotionDefault.mot";

	//++ 지정한 Mot파일의 설정값들로 모션보드의 설정값들을 일괄변경 적용합니다.
	if(AxmMotLoadParaAll(szFilePath) != AXT_RT_SUCCESS){
		AfxMessageBox("Mot File Not Found.");
	}
	return TRUE;
}

BOOL CAjinControllerDlg::AddAxisInfo()
{
	CString	strData;
	DWORD	dwModuleID;

	//++ 유효한 전체 모션축수를 반환합니다.
	AxmInfoGetAxisCount(&m_lAxisCount);
	if(m_lAxisCount < 1)	return FALSE;

	m_lAxisNo = 0;
	//++ 지정한 축의 정보를 반환합니다.
	// [INFO] 여러개의 정보를 읽는 함수 사용시 불필요한 정보는 NULL(0)을 입력하면 됩니다.
	AxmInfoGetAxis(m_lAxisNo, NULL, NULL, &m_dwModuleID);

	// 유효한 전체 모션축수의 정보를 읽어 ComboBox에 등록합니다.
	for(int i = 0; i < m_lAxisCount; i++)
	{
		//++ 지정한 축의 정보를 반환합니다.
		// [INFO] 여러개의 정보를 읽는 함수 사용시 불필요한 정보는 NULL(0)을 입력하면 됩니다.
		AxmInfoGetAxis(i, NULL, NULL, &dwModuleID);
		switch(dwModuleID){
		case AXT_SMC_4V04:			strData.Format("%02ld-[PCIB-QI4A]",i);		break;
		case AXT_SMC_2V04:			strData.Format("%02ld-[PCIB-QI2A]",i);		break;
		case AXT_SMC_R1V04A4:		strData.Format("%02ld-[RTEX-A4N]", i);		break;
		case AXT_SMC_R1V04A5:		strData.Format("%02ld-[RTEX-A5N]", i);		break;
		case AXT_SMC_R1V04:			strData.Format("%02ld-(RTEX-PM)", i);		break;
		case AXT_SMC_R1V04PM2Q:		strData.Format("%02ld-(RTEX-PM2Q)", i);		break;
		case AXT_SMC_R1V04PM2QE:	strData.Format("%02ld-(RTEX-PM2QE)", i);	break;
		case AXT_SMC_R1V04MLIISV:	strData.Format("%02ld-[MLII-SGDV]", i);		break;
		case AXT_SMC_R1V04MLIIPM:	strData.Format("%02ld-(MLII-PM)", i);		break;
		case AXT_SMC_R1V04MLIICL:	strData.Format("%02ld-[MLII-CSDL]", i);		break;
		case AXT_SMC_R1V04MLIICR:	strData.Format("%02ld-[MLII-CSDH]", i);		break;
		case AXT_SMC_R1V04SIIIHMIV:	strData.Format("%02ld-(SIIIH-MRJ4)", i);	break;
		default:					strData.Format("%02ld-[Unknown]",i);
		}

		m_cboSelAxis.AddString(strData);
	}

	InitControl();							// Control 변수들을 등록하고, 초기 설정값들을 설정합니다.
	SetTimer(TM_DISPLAY, 100, NULL);		// 일정한 주기로 변경되는 위치/속도/정보등을 Control에 반영할 타이머를 실행합니다.
	return TRUE;
}

BOOL CAjinControllerDlg::InitControl()
{
	m_cboSelAxis.SetCurSel(m_lAxisNo);

	// Edit Control에 초기값 설정
	//SetDlgItemDouble(IDC_EDT_POS1, 0.0);
	//SetDlgItemDouble(IDC_EDT_POS2, 100.0);
	//	SetDlgItemDouble(IDC_EDT_MOVE_VEL, 100.0);
	//	SetDlgItemDouble(IDC_EDT_MOVE_ACC, 400.0);
	//	SetDlgItemDouble(IDC_EDT_MOVE_DEC, 400.0);

	SetDlgItemDouble(IDC_JOG_VELOCITY, 100.0);
	SetDlgItemDouble(IDC_JOG_ACCEL, 400.0);
	SetDlgItemDouble(IDC_JOG_DECEL, 400.0);

	return TRUE;
}

DWORD CAjinControllerDlg::ConvertComboToAxm(CComboBox *pCboItem)
{
	if(pCboItem == NULL)	return 0;

	CString strText;
	long	lCount, lSelData;	

	lCount = pCboItem->GetCount();
	if(lCount == 0)		return 0;

	lSelData = pCboItem->GetCurSel();
	if(lSelData < 0)	return 0;

	pCboItem->GetLBText(lSelData, strText);	
	return (DWORD)atoi(strText);
}

long CAjinControllerDlg::ConvertAxmToCombo(CComboBox *pCboItem, DWORD dwCurData)
{
	if(pCboItem == NULL)	return 0;

	long	lCount, lSelData;
	CString strText;

	lCount = pCboItem->GetCount();
	if(lCount == 0)	return 0;

	for(int i = 0; i < lCount; i++){
		pCboItem->GetLBText(i, strText);
		lSelData = atoi(strText);
		if(lSelData == (long)dwCurData){	
			pCboItem->SetCurSel(i);
			return i;
		}
	}

	pCboItem->SetCurSel(0);
	return 0;
}

void CAjinControllerDlg::SetDlgItemDouble(int nID, double value, int nPoint)
{
	CString sTemp, sTemp2;
	sTemp2.Format("%%.%df", nPoint);
	sTemp.Format(sTemp2, value);
	GetDlgItem(nID)->SetWindowText(sTemp);
}

double CAjinControllerDlg::GetDlgItemDouble(int nID)
{
	double dRet;
	CString sTemp;
	GetDlgItem(nID)->GetWindowText(sTemp);
	dRet = atof((LPCTSTR)sTemp);
	return dRet;
}




void CAjinControllerDlg::OnBnClickedButtonOrigin()
{
	DWORD	dwRetCode;

	//++ 지정한 축에 원점검색을 진행합니다.
	dwRetCode = AxmHomeSetStart(m_lAxisNo);	
	if(dwRetCode != AXT_RT_SUCCESS){
		CString strData;
		strData.Format("AxmHomeSetStart return error[Code:%04d]", dwRetCode);
		MessageBox(strData, "Error", MB_OK | MB_ICONERROR);
	}
}

void CAjinControllerDlg::OnBnClickedButtonTestmove()
{
	CString strData;
	DWORD	dwRetCode;
	double	dMovePos, dMoveVel, dMoveAcc, dMoveDec;

	AxmMotGetParaLoad(m_lAxisNo, &dMovePos, &dMoveVel, &dMoveAcc, &dMoveDec);

	//dMovePos = GetDlgItemDouble(IDC_STC_CMD_POS);
	SetDlgItemDouble(IDC_EDT_VELOCITY,dMoveVel);
	SetDlgItemDouble(IDC_EDT_ACCEL,dMoveAcc);
	SetDlgItemDouble(IDC_EDT_DECEL,dMoveDec);


	//dMovePos = GetDlgItemDouble(IDC_STC_CMD_POS);
	dMoveVel = GetDlgItemDouble(IDC_EDT_VELOCITY);
	dMoveAcc = GetDlgItemDouble(IDC_EDT_ACCEL);
	dMoveDec = GetDlgItemDouble(IDC_EDT_DECEL);

	//++ 지정한 축을 지정한 거리(또는 위치)/속도/가속도/감속도로 모션구동하고 모션 종료여부와 상관없이 함수를 빠져나옵니다.

	dwRetCode = AxmMoveStartPos(m_lAxisNo, dMovePos, dMoveVel, dMoveAcc, dMoveDec);
	if(dwRetCode != AXT_RT_SUCCESS){
		strData.Format("AxmMoveStartPos return error[Code:%04d]", dwRetCode);
		MessageBox(strData, "Error", MB_OK | MB_ICONERROR);
	}

}


void CAjinControllerDlg::OnBnClickedButtonStop()
{
	CString strData;
	DWORD	dwRetCode;

	if(m_bTestActive[m_lAxisNo])
	{
		dwRetCode = AxmMoveSStop(m_lAxisNo);
		m_bTestActive[m_lAxisNo] = FALSE;
		if(dwRetCode != AXT_RT_SUCCESS)
		{
			strData.Format("AxmMoveSStop return error[Code:%04d]", dwRetCode);
			MessageBox(strData, "Error", MB_OK | MB_ICONERROR);
		}
	}
	else
	{
		dwRetCode = AxmMoveSStop(m_lAxisNo);
		if(dwRetCode != AXT_RT_SUCCESS)
		{
			strData.Format("AxmMoveSStop return error[Code:%04d]", dwRetCode);
			MessageBox(strData, "Error", MB_OK | MB_ICONERROR);
		}
	}	
}

void CAjinControllerDlg::OnBnClickedChkServoOn()
{
	DWORD dwOnOff;	
	dwOnOff = IsDlgButtonChecked(IDC_CHK_SERVO_ON);
	//++ 지정 축의 Servo On/Off 신호를 출력합니다.
	AxmSignalServoOn(m_lAxisNo, dwOnOff);
}

void CAjinControllerDlg::OnDestroy()
{
	CDialog::OnDestroy();

	//++ AXL 사용을 종료합니다.
	AxlClose();	
}
