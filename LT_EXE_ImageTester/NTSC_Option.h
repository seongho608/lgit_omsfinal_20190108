#pragma once
#include "afxcmn.h"
#include "ETCUSER.H"

// CNTSC_Option 대화 상자입니다.

class CNTSC_Option : public CDialog
{
	DECLARE_DYNAMIC(CNTSC_Option)

public:
	CNTSC_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CNTSC_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_NTSC };
	CRectData C_RECT[1];
	tResultVal Run(void);
	void	Pic(CDC *cdc);
	void	InitPrm();
	void	FAIL_UPLOAD();
	BOOL	b_StopFail;
	

	void	LOT_InsertDataList();
	void NTSCGen();

	double d_Vpp;
	double d_VSync;

	double d_Vppmin;
	double d_Vppmax;
	double d_Vsyncmin;
	double d_Vsyncmax;

	int Lot_InsertNum;


	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
//	void	StatePrintf(LPCSTR lpcszString, ...);

	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();

	void	InitEVMS();
	void	Save_parameter();
	void	Load_parameter();
	void	Save(int NUM);

	CString H_filename;
	int CurrentGen();
	char SendData8Byte(unsigned char * TxData, int Num);
	bool NTSCPic(CDC *cdc);
	BOOL	m_Etc_State;
	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void LOT_Set_List(CListCtrl *List);
	
	void InsertList();
	int InsertIndex;
	int ListItemNum;

	void EXCEL_SAVE();
	void LOT_EXCEL_SAVE();

	bool EXCEL_UPLOAD();
	bool LOT_EXCEL_UPLOAD();
	int StartCnt;

	int Lot_StartCnt;

	int m_FontSize;
	int m_PosX;
	int m_PosY;

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
	CString		strTitle;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_NTSCList;
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnBnClickedButtonVppSave();
	CString str_VppMin;
	CString str_VppMax;
	CString str_VSyncMin;
	CString str_VSyncMax;
	afx_msg void OnBnClickedButtonVsyncSave();
	afx_msg void OnEnChangeEditMinVpp();
	afx_msg void OnEnChangeEditMaxVpp();
	afx_msg void OnEnChangeEditMinVsync();
	afx_msg void OnEnChangeEditMaxVsync();
	CString str_Fontsize;
	CString str_PosX;
	CString str_PosY;
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	BOOL m_checkVpp;
	BOOL m_checkVsync;
	afx_msg void OnBnClickedCheckVpp();
	afx_msg void OnBnClickedCheckVsync();
	CListCtrl m_Ntsc_Worklist;
};