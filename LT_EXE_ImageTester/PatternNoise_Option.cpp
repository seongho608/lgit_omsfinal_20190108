

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "PatternNoise_Option.h"

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
extern WORD	m_GRAYScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];
// CPatternNoise_Option 대화 상자입니다.
extern BYTE	m_OverlayArea[CAM_IMAGE_WIDTH][CAM_IMAGE_HEIGHT];
IMPLEMENT_DYNAMIC(CPatternNoise_Option, CDialog)

CPatternNoise_Option::CPatternNoise_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CPatternNoise_Option::IDD, pParent)
	, ThrDark(0)
	, ThrNoise(0)
	, ThrDiff(0)
	, PN_Total_PosX(0)
	, PN_Total_PosY(0)
	, PN_Dis_Width(0)
	, PN_Dis_Height(0)
	, PN_Total_Width(0)
	, PN_Total_Height(0)
	, str_CamBrightness(_T(""))
	, str_CamContrast(_T(""))
	, b_BlockDataSave(FALSE)
	, m_dPatternNoiseOffset(0)
	, str_MasterPath(_T(""))
{
	noisePercent =0;
	changecount=0;
	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;
//	b_ChangeMod=0;
	StartCnt=0;
	b_StopFail = FALSE;
}

CPatternNoise_Option::~CPatternNoise_Option()
{
}

void CPatternNoise_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_Thresold_Y, ThrDark);
	DDX_Text(pDX, IDC_Thresold_V, ThrNoise);
	DDX_Text(pDX, IDC_Thresold_D, ThrDiff);
	DDX_Control(pDX, IDC_LIST_PatternNoiseLIST, m_PatternNoiseList);
	DDX_Text(pDX, IDC_PN_PosX, PN_Total_PosX);
	DDX_Text(pDX, IDC_PN_PosY, PN_Total_PosY);
	DDX_Text(pDX, IDC_PN_Dis_W, PN_Dis_Width);
	DDX_Text(pDX, IDC_PN_Dis_H, PN_Dis_Height);
	DDX_Text(pDX, IDC_PN_Width, PN_Total_Width);
	DDX_Text(pDX, IDC_PN_Height, PN_Total_Height);
	DDX_Control(pDX, IDC_LIST_PatternNoise, PN_DATALIST);
	DDX_Text(pDX, IDC_EDIT_CAM_Bright, str_CamBrightness);
	DDX_Text(pDX, IDC_EDIT_CAM_Contrast, str_CamContrast);
	DDX_Control(pDX, IDC_LIST_PatternNoiseLIST_LOT, m_Lot_PatternNoiseList);
	DDX_Check(pDX, IDC_CHK_BLOCK_DATA_SAVE, b_BlockDataSave);
	DDX_Text(pDX, IDC_PATTERN_OFFSET, m_dPatternNoiseOffset);
	DDX_Text(pDX, IDC_EDIT_MasterPath, str_MasterPath);
}


BEGIN_MESSAGE_MAP(CPatternNoise_Option, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_PN_SAVE, &CPatternNoise_Option::OnBnClickedButtonPnSave)
	ON_EN_CHANGE(IDC_Thresold_Y, &CPatternNoise_Option::OnEnChangeThresoldY)
	ON_EN_CHANGE(IDC_Thresold_V, &CPatternNoise_Option::OnEnChangeThresoldV)
	ON_EN_CHANGE(IDC_Thresold_D, &CPatternNoise_Option::OnEnChangeThresoldD)
	ON_WM_SHOWWINDOW()
	//ON_BN_CLICKED(IDC_BUTTON_PN_SAVE2, &CPatternNoise_Option::OnBnClickedButtonPnSave2)
	ON_BN_CLICKED(IDC_BUTTON_PN_LOAD, &CPatternNoise_Option::OnBnClickedButtonPnLoad)
	ON_BN_CLICKED(IDC_BUTTON_PN_SAVEZONE, &CPatternNoise_Option::OnBnClickedButtonPnSavezone)
	ON_NOTIFY(NM_CLICK, IDC_LIST_PatternNoise, &CPatternNoise_Option::OnNMClickListPatternnoise)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_PatternNoise, &CPatternNoise_Option::OnNMCustomdrawListPatternnoise)
	ON_EN_KILLFOCUS(IDC_EDIT_A_MOD, &CPatternNoise_Option::OnEnKillfocusEditAMod)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_PatternNoise, &CPatternNoise_Option::OnNMDblclkListPatternnoise)
	ON_BN_CLICKED(IDC_BUTTON_setPNZone, &CPatternNoise_Option::OnBnClickedButtonsetpnzone)
	ON_WM_MOUSEWHEEL()
	ON_EN_CHANGE(IDC_EDIT_A_MOD, &CPatternNoise_Option::OnEnChangeEditAMod)
	ON_EN_CHANGE(IDC_PN_PosX, &CPatternNoise_Option::OnEnChangePnPosx)
	ON_EN_CHANGE(IDC_PN_PosY, &CPatternNoise_Option::OnEnChangePnPosy)
	ON_EN_CHANGE(IDC_PN_Dis_W, &CPatternNoise_Option::OnEnChangePnDisW)
	ON_EN_CHANGE(IDC_PN_Dis_H, &CPatternNoise_Option::OnEnChangePnDisH)
	ON_EN_CHANGE(IDC_PN_Width, &CPatternNoise_Option::OnEnChangePnWidth)
	ON_EN_CHANGE(IDC_PN_Height, &CPatternNoise_Option::OnEnChangePnHeight)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_CAM, &CPatternNoise_Option::OnBnClickedButtonCam)
	ON_EN_CHANGE(IDC_EDIT_CAM_Bright, &CPatternNoise_Option::OnEnChangeEditCamBright)
	ON_EN_CHANGE(IDC_EDIT_CAM_Contrast, &CPatternNoise_Option::OnEnChangeEditCamContrast)
	ON_BN_CLICKED(IDC_BUTTON_test, &CPatternNoise_Option::OnBnClickedButtontest)
	ON_BN_CLICKED(IDC_CHK_BLOCK_DATA_SAVE, &CPatternNoise_Option::OnBnClickedChkBlockDataSave)
	ON_BN_CLICKED(IDC_BUTTON_MASTERLOAD, &CPatternNoise_Option::OnBnClickedButtonMasterload)
END_MESSAGE_MAP()

// CPatternNoise_Option 메시지 처리기입니다.

void CPatternNoise_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CPatternNoise_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO    = INFO; 
	str_model.Empty();
	str_model.Format(INFO.NAME);;
}

BOOL CPatternNoise_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;

	PN_filename =((CImageTesterDlg  *)m_pMomWnd)->modelfile_Name;
	SetList();
	UpdateData(TRUE);
	Load_parameter();
	UpdateData(FALSE);
	//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->EnableWindow(0);
	Set_List(&m_PatternNoiseList);
	LOT_Set_List(&m_Lot_PatternNoiseList);

	dark = 0;
	noise = 0;
	differ = 0;

	InitEVMS();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPatternNoise_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_PN_LOAD))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVEZONE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_CAM))->EnableWindow(0);

		
		((CEdit *)GetDlgItem(IDC_Thresold_Y))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Thresold_V))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_PN_PosX))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_PN_PosY))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_PN_Dis_W))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_PN_Dis_H))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_PN_Width))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_PN_Height))->EnableWindow(0);

		((CEdit *)GetDlgItem(IDC_EDIT_CAM_Bright))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_CAM_Contrast))->EnableWindow(0);
	}
}


bool CPatternNoise_Option::PatternNoiseGen(LPBYTE IN_RGB,int NUM){

	m_Success = FALSE;
	//BYTE *BW;
	BYTE R,G,B;
	//BYTE abstraction;
	double total_abs=0;
	double avg_abs=0;

	double BrightnessValue = (double)ThrDiff;///ThrDiff 루리텍 비밀 옵션
	double env_Value = (double)ThrNoise;///ThrDiff 루리텍 비밀 옵션
	double dBThreshold = (double)ThrDark;

	int i =0;
	BYTE **BW;
	BW = (BYTE **)malloc(sizeof(BYTE *)*CAM_IMAGE_HEIGHT);                     //   
		for(i=0;i<CAM_IMAGE_HEIGHT;i++)BW[i]=(BYTE *)malloc(sizeof(BYTE )* CAM_IMAGE_WIDTH);

	
	for(int t=0; t<m_CAM_SIZE_HEIGHT; t++){
		for(int k=0; k<m_CAM_SIZE_WIDTH; k++){
			BW[t][k]=0;
		}
	}

	int startx = PN_RECT[NUM].m_Left;
	int starty = PN_RECT[NUM].m_Top;
	int endx = startx + PN_RECT[NUM].Length()-2;
	int endy = starty + PN_RECT[NUM].Height()-2;


	double **Cal_X;
	Cal_X = (double **)malloc(sizeof(double *)*CAM_IMAGE_HEIGHT);                     //   
		for(i=0;i<CAM_IMAGE_HEIGHT;i++)Cal_X[i]=(double *)malloc(sizeof(double )* 2);

	double **Cal_Y;
	Cal_Y = (double **)malloc(sizeof(double *)*CAM_IMAGE_WIDTH);                     //   
		for(i=0;i<CAM_IMAGE_WIDTH;i++)Cal_Y[i]=(double *)malloc(sizeof(double )* 2);

	double **testX;
	testX = (double **)malloc(sizeof(double *)*2);                     //   
		for(i=0;i<2;i++)testX[i]=(double *)malloc(sizeof(double )* CAM_IMAGE_HEIGHT);

	double **testY;
	testY = (double **)malloc(sizeof(double *)*2);                     //   
		for(i=0;i<2;i++)testY[i]=(double *)malloc(sizeof(double )* CAM_IMAGE_WIDTH);

	for(int t=0; t<m_CAM_SIZE_HEIGHT; t++){
		Cal_X[t][0]=0;
		Cal_X[t][1]=0;
		testX[0][t]=0;
		testX[1][t]=0;
		for(int k=0; k<m_CAM_SIZE_WIDTH; k++){
			BW[t][k]=0;
			Cal_Y[k][0]=0;
			Cal_Y[k][1]=0;
			testY[0][k]=0;
			testY[1][k]=0;

		}
	}


	
	/*double Cal_X[CAM_IMAGE_HEIGHT][2]={0,},testX[2][CAM_IMAGE_HEIGHT];
	double Cal_Y[CAM_IMAGE_WIDTH][2]={0,},testY[2][CAM_IMAGE_WIDTH]; */

	int count =0, CountLine[2]={0,};
	total_abs= 0;
	double originData= 0;
	for(int y=starty; y<endy; y++)
	{
		Cal_X[y-starty][0]=0;	Cal_X[y-starty][1]=0; CountLine[0]=0;
		for(int x=startx; x<endx; x++)
		{
			if((PN_RECT[NUM].m_Left <=x) &&(PN_RECT[NUM].m_Right >=x)){
				if((PN_RECT[NUM].m_Top <=y) &&(PN_RECT[NUM].m_Bottom >=y)){

					if(m_OverlayArea[x][y] == 1){
						B = (IN_RGB[y *(m_CAM_SIZE_WIDTH * 3) + (x * 3) + 0]);
						G = (IN_RGB[y *(m_CAM_SIZE_WIDTH * 3) + (x * 3) + 1]);
						R = (IN_RGB[y *(m_CAM_SIZE_WIDTH * 3) + (x * 3) + 2]);

						//originData += (BYTE)((0.29900*(R))+(0.58700*(G))+(0.11400*(B)));

					
						BW[y][x] = (255 - (BYTE)((0.29900*(R))+(0.58700*(G))+(0.11400*(B))));


						total_abs += BW[y][x];
						count++;
						CountLine[0]++;
						Cal_X[y-starty][0] +=(double)BW[y][x];			Cal_X[y-starty][1] +=(double)BW[y][x]*(double)BW[y][x];
					}
				}
			}
		}
		if(CountLine[0]>0){
			Cal_X[y-starty][0] /= (double)CountLine[0]; Cal_X[y-starty][1] /= (double)CountLine[0];
			Cal_X[y-starty][1] = Cal_X[y-starty][1] - Cal_X[y-starty][0]*Cal_X[y-starty][0];
		}
	}

	double avg_origin =0;
	avg_origin = total_abs / (double)count;
	//avg_abs = total_abs / (double)count; // 추출된 rgb 값에 대한 평균(전체 평균)
	CountLine[1]=0;
	for(int x = startx;x < endx;x++){
		Cal_Y[x-startx][0]=0;		Cal_Y[x-startx][1]=0;  CountLine[1]=0;

		for(int y = starty;y < endy;y++){

			if(m_OverlayArea[x][y] == 1){
				Cal_Y[x-startx][0] +=(double)BW[y][x];			Cal_Y[x-startx][1] +=(double)BW[y][x]*(double)BW[y][x];
				CountLine[1]++;
			}
		}
		if(CountLine[1]>0){
			Cal_Y[x-startx][0] /= (double)CountLine[1]; Cal_Y[x-startx][1] /= (double)CountLine[1];
			Cal_Y[x-startx][1] = Cal_Y[x-startx][1] - Cal_Y[x-startx][0]*Cal_Y[x-startx][0];
		}
	}


	double Dark = 0, Noise[2];
	Noise[0]=Noise[1]=0;
	for (long i = 0; i<PN_RECT[NUM].Height() - 2; i++)	Dark += Cal_X[i][0];    Dark /= (double)PN_RECT[NUM].Height() - 2;
	for (long i = 0; i<PN_RECT[NUM].Height() - 2; i++)	Noise[0] += Cal_X[i][1];    Noise[0] /= (double)PN_RECT[NUM].Height() - 2;
	for (long i = 0; i<PN_RECT[NUM].Length() - 2; i++)	Noise[1] += Cal_Y[i][1];    Noise[1] /= (double)PN_RECT[NUM].Length() - 2;
	
	double SNR[4]={0,}, Sum[4]={0,};
	int Margin;
	int cnt[2]={0,};

//	Margin=(int)(endx*0.2);
//	for(int i=0+Margin;i<endx-Margin;i++){
	for (int i = 0; i<PN_RECT[NUM].Length() - 2; i++){

		Sum[0] += Cal_Y[i][0];		SNR[0] += Cal_Y[i][0]*Cal_Y[i][0]; 
		Sum[1] += Cal_Y[i][1];		SNR[1] += Cal_Y[i][1]*Cal_Y[i][1]; 
		cnt[0]++;
	}
	
	Sum[0] /= (double) cnt[0];		SNR[0] /= (double) cnt[0]; 
	Sum[1] /= (double) cnt[0];		SNR[1] /= (double) cnt[0];

	SNR[0] = SNR[0]- Sum[0]*Sum[0];
	SNR[1] = SNR[1]- Sum[1]*Sum[1];

//	Margin=(int)(endy*0.2);
//	for(int i=0+Margin;i<endy-Margin;i++){
	for (int i = 0; i<PN_RECT[NUM].Height() - 2; i++){
		Sum[2] += Cal_Y[i][2];		SNR[2] += Cal_Y[i][2]*Cal_Y[i][2]; 
		Sum[3] += Cal_Y[i][3];		SNR[3] += Cal_Y[i][3]*Cal_Y[i][3]; 
		cnt[1]++;
	}
	
	Sum[2] /= (double) cnt[1];		SNR[2] /= (double) cnt[1]; 
	Sum[3] /= (double) cnt[1];		SNR[3] /= (double) cnt[1];

	SNR[2] = SNR[2]- Sum[2]*Sum[2];
	SNR[3] = SNR[3]- Sum[3]*Sum[3];


	//double TotalSNR=(Sum[0]*Sum[1]*Sum[2]*Sum[3])/sqrt(SNR[0]*SNR[1]*SNR[2]*SNR[3]);

	double TotalSNR=(Sum[0])/sqrt(SNR[0]);
	TotalSNR = 10*log10l(TotalSNR)*1.2;

	if (SNR[0]==0)
	{
		TotalSNR = 100;
	}

	TotalSNR *= m_dPatternNoiseOffset;

	double Difference_Data = (Noise[0]/Noise[1]);
	
	
	for(int i=0;i<m_CAM_SIZE_HEIGHT;i++){
		testX[0][i]= Cal_X[i][0]; testX[1][i]= Cal_X[i][1];

	}

	for(int i=0;i<m_CAM_SIZE_WIDTH;i++){
		testY[0][i]= Cal_Y[i][0]; testY[1][i]= Cal_Y[i][1];
	}

	if(Dark < 0){
		Dark =0;
	}
	if(Noise[0] <0){
		Noise[0] = 0;
	}
	if(Difference_Data <0){
		Difference_Data =0;
	}
	


	// 만약 기준 신호레벨은 0이라면, SNR은 1이다.(전체평균 = 평균 noise 편차)
	if(avg_origin >env_Value){
		m_Success = FALSE;	
		Result.Empty();
		Result.Format("TEST환경이 아닙니다.");
		dark =0;
		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
			m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, Lot_InsertNum-1, "TEST환경X");////////
		}
		else{
			m_PatternNoiseList.SetItemText(InsertIndex, ListItemNum-1, "TEST환경X");////비고 넘버
		}
		PN_RECT[NUM].m_result = 0;
		PN_RECT[NUM].m_Success = FALSE;
	}else{
		if(TotalSNR >= dBThreshold){
			m_Success = TRUE;
			Result.Empty();
			Result.Format("OK");
			PN_RECT[NUM].m_Success = TRUE;

		}else{
			m_Success = FALSE;	
			Result.Empty();
			Result.Format("FAIL");
			PN_RECT[NUM].m_Success = FALSE;

		}

		dark = TotalSNR;

		PN_RECT[NUM].m_result = TotalSNR;
		CString str ="";
		str.Empty();
		str.Format("%3.1f",dark);	
	}

	
	for(i=0;i<m_CAM_SIZE_HEIGHT;i++)free(BW[i]);   free(BW);
	for(i=0;i<m_CAM_SIZE_HEIGHT;i++)free(Cal_X[i]);   free(Cal_X);
	for(i=0;i<CAM_IMAGE_WIDTH;i++)free(Cal_Y[i]);   free(Cal_Y);
	for(i=0;i<2;i++)free(testX[i]);   free(testX);
	for(i=0;i<2;i++)free(testY[i]);   free(testY);

	return m_Success;

			
}
bool CPatternNoise_Option::PatternNoiseGen_12bit(LPWORD IN_RGB, int NUM){

	m_Success = FALSE;

	double total_abs = 0;
	double avg_abs = 0;

	double BrightnessValue = (double)ThrDiff;///ThrDiff 루리텍 비밀 옵션
	double env_Value = (double)ThrNoise;///ThrDiff 루리텍 비밀 옵션
	double dBThreshold = (double)ThrDark;

	int i = 0;
	WORD **BW;
	BW = (WORD **)malloc(sizeof(WORD *)*CAM_IMAGE_HEIGHT);                     //   
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)BW[i] = (WORD *)malloc(sizeof(WORD)* CAM_IMAGE_WIDTH);


	for (int t = 0; t<m_CAM_SIZE_HEIGHT; t++){
		for (int k = 0; k<m_CAM_SIZE_WIDTH; k++){
			BW[t][k] = 0;
		}
	}

	int startx = PN_RECT[NUM].m_Left;
	int starty = PN_RECT[NUM].m_Top;
	int endx = startx + PN_RECT[NUM].Length() - 2;
	int endy = starty + PN_RECT[NUM].Height() - 2;


	double **Cal_X;
	Cal_X = (double **)malloc(sizeof(double *)*CAM_IMAGE_HEIGHT);                     //   
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)Cal_X[i] = (double *)malloc(sizeof(double)* 2);

	double **Cal_Y;
	Cal_Y = (double **)malloc(sizeof(double *)*CAM_IMAGE_WIDTH);                     //   
	for (i = 0; i<CAM_IMAGE_WIDTH; i++)Cal_Y[i] = (double *)malloc(sizeof(double)* 2);

	double **testX;
	testX = (double **)malloc(sizeof(double *)* 2);                     //   
	for (i = 0; i<2; i++)testX[i] = (double *)malloc(sizeof(double)* CAM_IMAGE_HEIGHT);

	double **testY;
	testY = (double **)malloc(sizeof(double *)* 2);                     //   
	for (i = 0; i<2; i++)testY[i] = (double *)malloc(sizeof(double)* CAM_IMAGE_WIDTH);

	for (int t = 0; t<m_CAM_SIZE_HEIGHT; t++){
		Cal_X[t][0] = 0;
		Cal_X[t][1] = 0;
		testX[0][t] = 0;
		testX[1][t] = 0;
		for (int k = 0; k<m_CAM_SIZE_WIDTH; k++){
			BW[t][k] = 0;
			Cal_Y[k][0] = 0;
			Cal_Y[k][1] = 0;
			testY[0][k] = 0;
			testY[1][k] = 0;

		}
	}


	int count = 0, CountLine[2] = { 0, };
	total_abs = 0;
	double originData = 0;
	for (int y = starty; y<endy; y++)
	{
		Cal_X[y - starty][0] = 0;	Cal_X[y - starty][1] = 0; CountLine[0] = 0;
		for (int x = startx; x<endx; x++)
		{
			if ((PN_RECT[NUM].m_Left <= x) && (PN_RECT[NUM].m_Right >= x)){
				if ((PN_RECT[NUM].m_Top <= y) && (PN_RECT[NUM].m_Bottom >= y)){

						BW[y][x] = 4095 - IN_RGB[y*m_CAM_SIZE_WIDTH + x];

						total_abs += BW[y][x];
						count++;
						CountLine[0]++;
						Cal_X[y - starty][0] += (double)BW[y][x];			Cal_X[y - starty][1] += (double)BW[y][x] * (double)BW[y][x];
				}
			}
		}
		if (CountLine[0]>0){
			Cal_X[y - starty][0] /= (double)CountLine[0]; Cal_X[y - starty][1] /= (double)CountLine[0];
			Cal_X[y - starty][1] = Cal_X[y - starty][1] - Cal_X[y - starty][0] * Cal_X[y - starty][0];
		}
	}

	double avg_origin = 0;
	avg_origin = total_abs / (double)count;
	//avg_abs = total_abs / (double)count; // 추출된 rgb 값에 대한 평균(전체 평균)
	CountLine[1] = 0;
	for (int x = startx; x < endx; x++){
		Cal_Y[x - startx][0] = 0;		Cal_Y[x - startx][1] = 0;  CountLine[1] = 0;

		for (int y = starty; y < endy; y++){

			if (m_OverlayArea[x][y] == 1){
				Cal_Y[x - startx][0] += (double)BW[y][x];			Cal_Y[x - startx][1] += (double)BW[y][x] * (double)BW[y][x];
				CountLine[1]++;
			}
		}
		if (CountLine[1]>0){
			Cal_Y[x - startx][0] /= (double)CountLine[1]; Cal_Y[x - startx][1] /= (double)CountLine[1];
			Cal_Y[x - startx][1] = Cal_Y[x - startx][1] - Cal_Y[x - startx][0] * Cal_Y[x - startx][0];
		}
	}


	double Dark = 0, Noise[2];
	Noise[0] = Noise[1] = 0;
	for (long i = 0; i<PN_RECT[NUM].Height() - 2; i++)	Dark += Cal_X[i][0];    Dark /= (double)PN_RECT[NUM].Height() - 2;
	for (long i = 0; i<PN_RECT[NUM].Height() - 2; i++)	Noise[0] += Cal_X[i][1];    Noise[0] /= (double)PN_RECT[NUM].Height() - 2;
	for (long i = 0; i<PN_RECT[NUM].Length() - 2; i++)	Noise[1] += Cal_Y[i][1];    Noise[1] /= (double)PN_RECT[NUM].Length() - 2;

	double SNR[4] = { 0, }, Sum[4] = { 0, };
	int Margin;
	int cnt[2] = { 0, };

	//	Margin=(int)(endx*0.2);
	//	for(int i=0+Margin;i<endx-Margin;i++){
	for (int i = 0; i<PN_RECT[NUM].Length() - 2; i++){

		Sum[0] += Cal_Y[i][0];		SNR[0] += Cal_Y[i][0] * Cal_Y[i][0];
		Sum[1] += Cal_Y[i][1];		SNR[1] += Cal_Y[i][1] * Cal_Y[i][1];
		cnt[0]++;
	}

	Sum[0] /= (double)cnt[0];		SNR[0] /= (double)cnt[0];
	Sum[1] /= (double)cnt[0];		SNR[1] /= (double)cnt[0];

	SNR[0] = SNR[0] - Sum[0] * Sum[0];
	SNR[1] = SNR[1] - Sum[1] * Sum[1];

	//	Margin=(int)(endy*0.2);
	//	for(int i=0+Margin;i<endy-Margin;i++){
	for (int i = 0; i<PN_RECT[NUM].Height() - 2; i++){
		Sum[2] += Cal_Y[i][2];		SNR[2] += Cal_Y[i][2] * Cal_Y[i][2];
		Sum[3] += Cal_Y[i][3];		SNR[3] += Cal_Y[i][3] * Cal_Y[i][3];
		cnt[1]++;
	}

	Sum[2] /= (double)cnt[1];		SNR[2] /= (double)cnt[1];
	Sum[3] /= (double)cnt[1];		SNR[3] /= (double)cnt[1];

	SNR[2] = SNR[2] - Sum[2] * Sum[2];
	SNR[3] = SNR[3] - Sum[3] * Sum[3];


	//double TotalSNR=(Sum[0]*Sum[1]*Sum[2]*Sum[3])/sqrt(SNR[0]*SNR[1]*SNR[2]*SNR[3]);

	double TotalSNR = (Sum[0]) / sqrt(SNR[0]);
	TotalSNR = 10 * log10l(TotalSNR)*1.2;

	if (SNR[0] == 0)
	{
		TotalSNR = 100;
	}



	double Difference_Data = (Noise[0] / Noise[1]);


	for (int i = 0; i<m_CAM_SIZE_HEIGHT; i++){
		testX[0][i] = Cal_X[i][0]; testX[1][i] = Cal_X[i][1];

	}

	for (int i = 0; i<m_CAM_SIZE_WIDTH; i++){
		testY[0][i] = Cal_Y[i][0]; testY[1][i] = Cal_Y[i][1];
	}

	if (Dark < 0){
		Dark = 0;
	}
	if (Noise[0] <0){
		Noise[0] = 0;
	}
	if (Difference_Data <0){
		Difference_Data = 0;
	}



	// 만약 기준 신호레벨은 0이라면, SNR은 1이다.(전체평균 = 평균 noise 편차)
	if (avg_origin >env_Value){
		m_Success = FALSE;
		Result.Empty();
		Result.Format("TEST환경이 아닙니다.");
		dark = 0;
		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
			m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, Lot_InsertNum - 1, "TEST환경X");////////
		}
		else{
			m_PatternNoiseList.SetItemText(InsertIndex, ListItemNum - 1, "TEST환경X");////비고 넘버
		}
		PN_RECT[NUM].m_result = 0;
		PN_RECT[NUM].m_Success = FALSE;
	}
	else{
		if (TotalSNR >= dBThreshold){
			m_Success = TRUE;
			Result.Empty();
			Result.Format("OK");
			PN_RECT[NUM].m_Success = TRUE;

		}
		else{
			m_Success = FALSE;
			Result.Empty();
			Result.Format("FAIL");
			PN_RECT[NUM].m_Success = FALSE;

		}

		dark = TotalSNR;

		PN_RECT[NUM].m_result = TotalSNR;
		CString str = "";
		str.Empty();
		str.Format("%3.1f", dark);
	}


	for (i = 0; i<m_CAM_SIZE_HEIGHT; i++)free(BW[i]);   free(BW);
	for (i = 0; i<m_CAM_SIZE_HEIGHT; i++)free(Cal_X[i]);   free(Cal_X);
	for (i = 0; i<CAM_IMAGE_WIDTH; i++)free(Cal_Y[i]);   free(Cal_Y);
	for (i = 0; i<2; i++)free(testX[i]);   free(testX);
	for (i = 0; i<2; i++)free(testY[i]);   free(testY);

	return m_Success;


}

bool CPatternNoise_Option::PatternNoisePic(CDC *cdc,int NUM){

	//if(EIJARect.Chkdata() == FALSE){return 0;}//재대로 된 데이터가 아니면 바로 빠져나간다.

	CPen	my_Pan,*old_pan;
	CString TEXTDATA ="";
	::SetBkMode(cdc->m_hDC,TRANSPARENT);

//	if (PN_RECT[NUM].ENABLE == TRUE)
	//{


// 		if(PN_RECT[NUM].m_Success == TRUE){
// 			my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
// 			cdc->SetTextColor(BLUE_COLOR);
// 		}else{
// 			my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
// 			cdc->SetTextColor(RED_COLOR);
// 		}
// 
// 
// 
// 		old_pan = cdc->SelectObject(&my_Pan);
// 
// 		cdc->SetTextAlign(TA_CENTER|TA_BASELINE);
// 
// 		cdc->SelectObject(old_pan);
	// 



	// 		cdc->MoveTo(PN_RECT[NUM].m_Left,PN_RECT[NUM].m_Top);
	// 		cdc->LineTo(PN_RECT[NUM].m_Right,PN_RECT[NUM].m_Top);
	// 		cdc->LineTo(PN_RECT[NUM].m_Right,PN_RECT[NUM].m_Bottom);
	// 		cdc->LineTo(PN_RECT[NUM].m_Left,PN_RECT[NUM].m_Bottom);
	// 		cdc->LineTo(PN_RECT[NUM].m_Left,PN_RECT[NUM].m_Top);
	// 
	// 		TEXTDATA.Format("STD: %6.2f",PN_RECT[NUM].m_result);	
	// 		//	}
	// 		cdc->TextOut(PN_RECT[NUM].m_PosX,PN_RECT[NUM].m_Top - 3,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());



// 		old_pan->DeleteObject();
// 		my_Pan.DeleteObject();


		my_Pan.CreatePen(PS_SOLID, 2, WHITE_COLOR);
			cdc->SetTextColor(RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		int posX = 0;
		for (int t = 0; t < st_PNMaster.nMaster_Block_X; t++)
		{
			if (t ==0)
			{
				posX += st_PNMaster.nMaster_Detect_X / 2;
			}
			else{
				posX += st_PNMaster.nMaster_Detect_X;
			}
			cdc->MoveTo(posX, 0);
			cdc->LineTo(posX, CAM_IMAGE_HEIGHT);
		}

		int posY = 0;
		for (int t = 0; t < st_PNMaster.nMaster_Block_Y; t++)
		{
			if (t == 0)
			{
				posY += st_PNMaster.nMaster_Detect_Y / 2;
			}
			else{
				posY += st_PNMaster.nMaster_Detect_Y;
			}
			cdc->MoveTo(0,posY);
			cdc->LineTo(CAM_IMAGE_WIDTH, posY);
		}

		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();


		my_Pan.CreatePen(PS_SOLID, 2, YELLOW_COLOR);
		cdc->SetTextColor(YELLOW_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);

		cdc->SetTextAlign(TA_CENTER|TA_BASELINE);


		for (int t = 0; t < st_PNMaster.pt_StartPoint_PixPosList.size(); t++)
		{
			cdc->MoveTo(st_PNMaster.pt_StartPoint_PixPosList[t].x, st_PNMaster.pt_StartPoint_PixPosList[t].y);
			cdc->LineTo(st_PNMaster.pt_StartPoint_PixPosList[t].x + st_PNMaster.nMaster_Detect_X, st_PNMaster.pt_StartPoint_PixPosList[t].y);
			cdc->LineTo(st_PNMaster.pt_StartPoint_PixPosList[t].x + st_PNMaster.nMaster_Detect_X, st_PNMaster.pt_StartPoint_PixPosList[t].y + st_PNMaster.nMaster_Detect_Y);
			cdc->LineTo(st_PNMaster.pt_StartPoint_PixPosList[t].x , st_PNMaster.pt_StartPoint_PixPosList[t].y + st_PNMaster.nMaster_Detect_Y);
			cdc->LineTo(st_PNMaster.pt_StartPoint_PixPosList[t].x, st_PNMaster.pt_StartPoint_PixPosList[t].y);

			if (!((st_PNMaster.pt_Min.x == st_PNMaster.pt_StartPoint_PixPosList[t].x) && (st_PNMaster.pt_Min.y == st_PNMaster.pt_StartPoint_PixPosList[t].y)))
			{
				if (st_PNMaster.v_dResult.size() != 0)
				{
					TEXTDATA.Format("%6.1f", st_PNMaster.v_dResult[t]);
					cdc->TextOut(st_PNMaster.pt_StartPoint_PixPosList[t].x + st_PNMaster.nMaster_Detect_X / 3, st_PNMaster.pt_StartPoint_PixPosList[t].y + st_PNMaster.nMaster_Detect_Y / 2, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());

				}
			}


		}

	

		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();

		if (st_PNMaster.dMinResult != -1)
		{
			if (st_PNMaster.bResult)
			{
				my_Pan.CreatePen(PS_SOLID, 2, BLUE_COLOR);
				cdc->SetTextColor(BLUE_COLOR);

			}
			else{
				my_Pan.CreatePen(PS_SOLID, 2, RED_COLOR);
				cdc->SetTextColor(RED_COLOR);
			}
			old_pan = cdc->SelectObject(&my_Pan);
			cdc->SetTextAlign(TA_CENTER | TA_BASELINE);


			cdc->MoveTo(st_PNMaster.pt_Min.x, st_PNMaster.pt_Min.y);
			cdc->LineTo(st_PNMaster.pt_Min.x + st_PNMaster.nMaster_Detect_X, st_PNMaster.pt_Min.y);
			cdc->LineTo(st_PNMaster.pt_Min.x + st_PNMaster.nMaster_Detect_X, st_PNMaster.pt_Min.y + st_PNMaster.nMaster_Detect_Y);
			cdc->LineTo(st_PNMaster.pt_Min.x, st_PNMaster.pt_Min.y + st_PNMaster.nMaster_Detect_Y);
			cdc->LineTo(st_PNMaster.pt_Min.x, st_PNMaster.pt_Min.y);


			TEXTDATA.Format("%6.1f", st_PNMaster.dMinResult);
			cdc->TextOut(st_PNMaster.pt_Min.x + st_PNMaster.nMaster_Detect_X / 3, st_PNMaster.pt_Min.y + st_PNMaster.nMaster_Detect_Y / 2, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());


			cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();

	}

	return 1;
}

void CPatternNoise_Option::Pic(CDC *cdc)
{
	for(int lop=0;lop<9;lop++){
		PatternNoisePic(cdc,lop);
	}
	//PatternNoisePic(cdc,0);
}

void CPatternNoise_Option::InitPrm()
{

	dark = 0;
	noise = 0;
	differ = 0;
	noisePercent = 0;

	for(int t=0; t<9; t++){
		PN_RECT[t].m_result =0;
		PN_RECT[t].m_Success =FALSE;
	}
	Load_parameter();
	UpdateData(FALSE);

	//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->EnableWindow(0);
}


tResultVal CPatternNoise_Option::Run()
{	
	CString str="";
	b_StopFail = FALSE;

// 	if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 		LOT_InsertDataList();
// 	}
// 	else{
// 		InsertList();
// 	}

	st_PNMaster.dMinResult = 0;
	st_PNMaster.bResult = FALSE;

	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

		InsertList();
	}
		((CImageTesterDlg  *)m_pMomWnd)->m_pResPatternNoiseOptWnd->m_ResultList.DeleteAllItems();
		int camcnt = 0;
		tResultVal retval = {0,};
	
		//StatePrintf("저조도 측정 모드를 시작합니다");
		

		DoEvents(500);


		/////////////////////////////////////

		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = 1;
		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;

		for (int i = 0; i < 10; i++){
			if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0 && ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0){
				break;
			}
			else{
				DoEvents(50);
			}
		}
// 		if (b_BlockDataSave == TRUE){
// 			for (int Y = 0; Y < BLOCK_HEIGHT; Y++){
// 				for (int X = 0; X < BLOCK_WIDTH; X++){
// 					//PatternNoiseGen_Block(m_RGBScanbuf, FULL_RECT, X, Y);
// 					//PatternNoiseGen_Block_12bit(m_GRAYScanbuf, FULL_RECT, X, Y);
// 					STDGen_Block_16bit(m_GRAYScanbuf, FULL_RECT, X, Y);
// 				}
// 			}
// 			SAVE_FILE_STD(FULL_RECT, BLOCK_WIDTH, BLOCK_HEIGHT);
// 		}

		st_PNMaster.v_iSignal.clear();
		st_PNMaster.v_dNoise.clear();
		st_PNMaster.v_dResult.clear();





		double dMinData = 0;
		double dTempData = 99999999;
		if (!Master_Data_Load())
		{
			//Fail 처리./
			st_PNMaster.dMinResult = 0;
			st_PNMaster.bResult = FALSE;
			retval.m_Success = FALSE;
			retval.ValString = "Setting File X";
		}
		else{
			for (int t = 0; t< st_PNMaster.pt_StartPointList.size(); t++)
			{
				SNRGen_16bit(m_GRAYScanbuf, t);

				st_PNMaster.v_dResult[t] *= m_dPatternNoiseOffset;

				if (dTempData  > st_PNMaster.v_dResult[t])
				{
					dTempData = st_PNMaster.v_dResult[t];
					dMinData = dTempData;

					st_PNMaster.dMinResult = dMinData;
					st_PNMaster.pt_Min = st_PNMaster.pt_StartPoint_PixPosList[t];
				}
			}


			if (ThrDark < st_PNMaster.dMinResult)
			{
				st_PNMaster.bResult = TRUE;
			}
			else{
				st_PNMaster.bResult = FALSE;

			}



			//int count = 0;
			//int Passcount = 0;
			//for (int lop = 0; lop < 9; lop++){
			//	if (PN_RECT[lop].ENABLE == TRUE){
			//		//bool Result = PatternNoiseGen(m_RGBScanbuf, lop);
			//		//bool Result = PatternNoiseGen_12bit(m_GRAYScanbuf, lop);
			//		bool Result = STDGen_16bit(m_GRAYScanbuf, lop);
			//		if (Result == TRUE){
			//			Passcount++;
			//		}
			//		count++;
			//	}
			//}

			//if(Passcount == count){
			if (st_PNMaster.bResult == TRUE){
				m_Success = TRUE;
				((CImageTesterDlg  *)m_pMomWnd)->m_pResPatternNoiseOptWnd->Y_RESULT(1, "PASS");

				if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
					m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, Lot_InsertNum - 2, "PASS");////////
				}
				else{
					m_PatternNoiseList.SetItemText(InsertIndex, ListItemNum - 2, "PASS");////////
				}



			}
			else{
				m_Success = FALSE;
				((CImageTesterDlg  *)m_pMomWnd)->m_pResPatternNoiseOptWnd->Y_RESULT(2, "FAIL");
				if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
					m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, Lot_InsertNum - 2, "FAIL");////////
				}
				else{
					m_PatternNoiseList.SetItemText(InsertIndex, ListItemNum - 2, "FAIL");////////
				}

			}
		}
 		CString stateDATA ="저조도 검사_ ";
// 
// 		for(int lop=0; lop<9;lop++){
// 
// 			if(lop ==0){
// 				str.Empty();str="CENTER";
// 			}
// 			if(lop ==1){
// 				str.Empty();str="LEFT";
// 			}
// 			if(lop ==2){
// 				str.Empty();str="RIGHT";
// 			}
// 			if(lop ==3){
// 				str.Empty();str="TOP";
// 			}
// 			if(lop ==4){
// 				str.Empty();str="BOTTOM";
// 			}	
// 			if(lop ==5){
// 				str.Empty();str="LEFT-TOP";
// 			}
// 			if(lop ==6){
// 				str.Empty();str="RIGHT-TOP";
// 			}
// 			if(lop ==7){
// 				str.Empty();str="LEFT-BOTTOM";
// 			}
// 			if(lop ==8){
// 				str.Empty();str="RIGHT-BOTTOM";
// 			}
// 
// 			if(PN_RECT[lop].ENABLE ==TRUE){
// 				CString dstr;
// 				dstr.Empty();
// 				dstr.Format("%6.2f",PN_RECT[lop].m_result);
// 
// 				stateDATA += str+"_ "+dstr+", ";
// 
// 			}
// 			else{
// 				stateDATA += str+"_ X, ";
// 				
// 			}	
// 		}
	
// 		for(int lop=0; lop<9;lop++){
// 
// 			int Index = ((CImageTesterDlg  *)m_pMomWnd)->m_pResPatternNoiseOptWnd->m_ResultList.InsertItem(lop,"",0);
// 
// 
// 			if(PN_RECT[lop].ENABLE ==TRUE){
// 				str.Empty();
// 				str.Format("%6.2f",PN_RECT[lop].m_result);
// 
// 				((CImageTesterDlg  *)m_pMomWnd)->m_pResPatternNoiseOptWnd->m_ResultList.SetItemText(Index,1,str);
// 
// 				if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 					m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, 5 + lop, str);
// 				}
// 				else{
// 					m_PatternNoiseList.SetItemText(InsertIndex, 4 + lop, str);
// 				}
// 
// 			}
// 			else{
// 				((CImageTesterDlg  *)m_pMomWnd)->m_pResPatternNoiseOptWnd->m_ResultList.SetItemText(Index,1,"X");
// 
// 				if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 					m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, 5 + lop, str);
// 				}
// 				else{
// 					m_PatternNoiseList.SetItemText(InsertIndex, 4 + lop, str);
// 
// 				}
// 
// 			}
// 			if (lop == 0){
// 				str.Empty(); str = "CENTER";
// 			}
// 			if (lop == 1){
// 				str.Empty(); str = "LEFT";
// 			}
// 			if (lop == 2){
// 				str.Empty(); str = "RIGHT";
// 			}
// 			if (lop == 3){
// 				str.Empty(); str = "TOP";
// 			}
// 			if (lop == 4){
// 				str.Empty(); str = "BOTTOM";
// 			}
// 			if (lop == 5){
// 				str.Empty(); str = "LEFT-TOP";
// 			}
// 			if (lop == 6){
// 				str.Empty(); str = "RIGHT-TOP";
// 			}
// 			if (lop == 7){
// 				str.Empty(); str = "LEFT-BOTTOM";
// 			}
// 			if (lop == 8){
// 				str.Empty(); str = "RIGHT-BOTTOM";
// 			}
// 
// 
// 			((CImageTesterDlg  *)m_pMomWnd)->m_pResPatternNoiseOptWnd->m_ResultList.SetItemText(Index, 0, str);
// 
// 			if (PN_RECT[lop].m_Success == TRUE){
// 				((CImageTesterDlg  *)m_pMomWnd)->m_pResPatternNoiseOptWnd->m_ResultList.SetItemText(Index, 2, "PASS");
// 
// 			}
// 			else{
// 				((CImageTesterDlg  *)m_pMomWnd)->m_pResPatternNoiseOptWnd->m_ResultList.SetItemText(Index, 2, "FAIL");
// 
// 			}
// 
// 
// 		}
		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
			str.Empty();
			str.Format("%6.2f", st_PNMaster.dMinResult);
			//m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, 5, str);
			int LISTNUM = 5;

			CString str;
			for (int t = 0; t < st_PNMaster.pt_StartPointList.size(); t++)
			{
				str.Format(_T("%6.2f"), st_PNMaster.v_dResult[t]);
				m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, LISTNUM++, str);
			}
		}
		else{

			int LISTNUM = 4;

			CString str;
			for (int t = 0; t < st_PNMaster.pt_StartPointList.size(); t++)
			{
				str.Format(_T("%6.2f"), st_PNMaster.v_dResult[t]);
				m_PatternNoiseList.SetItemText(InsertIndex, LISTNUM++, str);
			}

		

		}

// 		for (int t = 0; t< st_PNMaster.pt_StartPointList.size(); t++)
// 		{
// 			str.Empty();
// 			str.Format("%6.2f", st_PNMaster.v_dResult[t]);
// 			
// 			if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 				m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, 6 + t, str);
// 			}
// 			else{
// 				m_PatternNoiseList.SetItemText(InsertIndex, 5 + t, str);
// 			}
// 		}


		//---------------------------

		retval.m_ID = 0x08;
		retval.ValString.Empty();
		if(m_Success == TRUE){
			retval.m_Success = TRUE;
			Str_Mes[0] = "PASS";
			Str_Mes[1] = "1";
			if (((CImageTesterDlg *)m_pMomWnd)->LotMod != 1){

				//((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_LOW_LIGHT, "PASS");
			}
			stateDATA+= "_ PASS";
			retval.ValString.Format("OK");

		}else{
			retval.m_Success = FALSE;
			Str_Mes[0] = "FAIL";
			Str_Mes[1] = "0";
			if (((CImageTesterDlg *)m_pMomWnd)->LotMod != 1){

				//((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_LOW_LIGHT, "FAIL");
			}
			stateDATA+= "_ FAIL";
			retval.ValString.Format("FAIL");


		}
		//StatePrintf("저조도 측정 모드가 종료되었습니다");
		((CImageTesterDlg  *)m_pMomWnd)->StatePrintf(stateDATA);
// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 			if (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == TRUE){
// 				Lot_StartCnt++;
// 			}
// 		}
// 		else{
// 			StartCnt++;
// 		}
		if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			StartCnt++;
		}
		return retval;
	
}	

void CPatternNoise_Option::FAIL_UPLOAD(){

	if (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1){
// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 			LOT_InsertDataList();
// // 			for (int t = 0; t < 9; t++)
// // 			{
// 				m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, 5, "X");//result
// 
// /*			}*/
// 			m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, Lot_InsertNum - 2, "FAIL");//result
// 			m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, Lot_InsertNum - 1, "STOP");//비고
// 			b_StopFail = TRUE;
// 			Lot_StartCnt++;
// 		}
// 		else{
			InsertList();
// 			for (int t = 0; t < 9; t++)
// 			{
				m_PatternNoiseList.SetItemText(InsertIndex, 4, "X");//result

/*			}*/
			m_PatternNoiseList.SetItemText(InsertIndex, ListItemNum - 2, "FAIL");//result
			m_PatternNoiseList.SetItemText(InsertIndex, ListItemNum - 1, "STOP");//비고
			Str_Mes[0] = "FAIL";
			Str_Mes[1] = "0";
		//	((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_LOW_LIGHT, "STOP");
			StartCnt++;
			b_StopFail = TRUE;
// 		}
}
}
void CModel_Create(CPatternNoise_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO){
	CRect OptionRect;
	if(pTabWnd != NULL){
		((CPatternNoise_Option *)*pWnd) = new CPatternNoise_Option(pMomWnd);
		((CPatternNoise_Option *)*pWnd)->Setup(pMomWnd,pEdit,INFO);
		((CPatternNoise_Option *)*pWnd)->Create(((CPatternNoise_Option *)*pWnd)->IDD,pTabWnd);//&m_CtrTab);
		((CPatternNoise_Option *)*pWnd)->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		((CPatternNoise_Option *)*pWnd)->MoveWindow(20,40,OptionRect.Width(),OptionRect.Height());
	}
}
void CModel_Delete(CPatternNoise_Option **pWnd){
	if(((CPatternNoise_Option *)*pWnd) != NULL){
		delete ((CPatternNoise_Option *)*pWnd);
		((CPatternNoise_Option *)*pWnd) = NULL;	
	}
}

void CPatternNoise_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}




void CPatternNoise_Option::SetList(){
		PN_DATALIST.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);
		PN_DATALIST.InsertColumn(0,"Zone",LVCFMT_CENTER, 100);
// 		PN_DATALIST.InsertColumn(1,"PosX",LVCFMT_CENTER, 70);
// 		PN_DATALIST.InsertColumn(2,"PosY",LVCFMT_CENTER, 70);
		PN_DATALIST.InsertColumn(1,"START X",LVCFMT_CENTER, 70);
		PN_DATALIST.InsertColumn(2,"START Y",LVCFMT_CENTER, 70);
		PN_DATALIST.InsertColumn(3,"EXIT X",LVCFMT_CENTER, 70);
		PN_DATALIST.InsertColumn(4,"EXIT Y",LVCFMT_CENTER, 70);
		PN_DATALIST.InsertColumn(5,"Width",LVCFMT_CENTER, 70);
		PN_DATALIST.InsertColumn(6,"Height",LVCFMT_CENTER, 70);


}
void CPatternNoise_Option::Load_parameter(){

	CFileFind filefind;
	CString str="";
	CString strTitle="";
	strTitle.Empty();
	strTitle="PN_INIT";

	int rect_Width =	((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.065;
	int rect_Height = ((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.065;

	if((GetPrivateProfileCString(str_model,"NAME",PN_filename) != m_INFO.NAME)||(GetPrivateProfileCString(str_model,"MODE",PN_filename) != m_INFO.MODE)){//ini파일이 없으면 파일을 생성한다.
		b_BlockDataSave = 1;
		ThrDark=25;
		ThrNoise=255;
		ThrDiff=100;
		m_dPatternNoiseOffset = 1.0;
		PN_Total_PosX = m_CAM_SIZE_WIDTH/2;
		PN_Total_PosY = m_CAM_SIZE_HEIGHT/2;
		PN_Dis_Width = m_CAM_SIZE_WIDTH*0.277;
		PN_Dis_Height = m_CAM_SIZE_HEIGHT*0.2083;
		PN_Total_Width = rect_Width;
		PN_Total_Height = rect_Height;
		//PN_Thresold = 10;

		for (int t = 0; t < 9; t++){
			PN_RECT[t].m_sizeX = 4;
			PN_RECT[t].m_sizeY = 4;
		}

		PN_RECT[0].m_startX = BLOCK_WIDTH / 2 - PN_RECT[0].m_sizeX / 2;
		PN_RECT[0].m_startY = BLOCK_HEIGHT / 2 - PN_RECT[0].m_sizeY / 2;

		PN_RECT[1].m_startX = BLOCK_WIDTH  / 5 - PN_RECT[1].m_sizeX / 2;
		PN_RECT[1].m_startY = BLOCK_HEIGHT / 2  - PN_RECT[1].m_sizeY / 2;

		PN_RECT[2].m_startX = BLOCK_WIDTH * 4 / 5 - PN_RECT[2].m_sizeX / 2;
		PN_RECT[2].m_startY = BLOCK_HEIGHT / 2 - PN_RECT[2].m_sizeY / 2;

		PN_RECT[3].m_startX = BLOCK_WIDTH / 2 - PN_RECT[3].m_sizeX / 2;
		PN_RECT[3].m_startY = BLOCK_HEIGHT / 2 - 7 - PN_RECT[3].m_sizeY / 2;

		PN_RECT[4].m_startX = BLOCK_WIDTH / 2 - PN_RECT[4].m_sizeX / 2;
		PN_RECT[4].m_startY = BLOCK_HEIGHT / 2 + 7 - PN_RECT[4].m_sizeY / 2;


		PN_RECT[5].m_startX = BLOCK_WIDTH / 5 - PN_RECT[5].m_sizeX / 2;
		PN_RECT[5].m_startY = BLOCK_HEIGHT / 2 - 7 - PN_RECT[5].m_sizeY / 2;

		PN_RECT[6].m_startX = BLOCK_WIDTH * 4 / 5 - PN_RECT[6].m_sizeX / 2;
		PN_RECT[6].m_startY = BLOCK_HEIGHT / 2 - 7 - PN_RECT[6].m_sizeY / 2;
		
		PN_RECT[7].m_startX = BLOCK_WIDTH / 5 - PN_RECT[7].m_sizeX / 2;
		PN_RECT[7].m_startY = BLOCK_HEIGHT / 2 + 7 - PN_RECT[7].m_sizeY / 2;

		PN_RECT[8].m_startX = BLOCK_WIDTH * 4 / 5 - PN_RECT[8].m_sizeX / 2;
		PN_RECT[8].m_startY = BLOCK_HEIGHT / 2 + 7 - PN_RECT[8].m_sizeY / 2;


		for(int t=0; t<9; t++){
			
			PN_RECT[t].m_endX = PN_RECT[t].m_startX + PN_RECT[t].m_sizeX;
			PN_RECT[t].m_endY = PN_RECT[t].m_startY + PN_RECT[t].m_sizeY;

			Rect_Set(PN_RECT, t);

			PN_RECT[t].ENABLE = 1;
		}



// 		//center
// 		PN_RECT[0].m_PosX= m_CAM_SIZE_WIDTH/2;
// 		PN_RECT[0].m_PosY= m_CAM_SIZE_HEIGHT/2;
// 
// 		//left
// 		PN_RECT[1].m_PosX=m_CAM_SIZE_WIDTH*0.222;
// 		PN_RECT[1].m_PosY= m_CAM_SIZE_HEIGHT/2;
// 
// 		//right
// 		PN_RECT[2].m_PosX=m_CAM_SIZE_WIDTH*0.777;
// 		PN_RECT[2].m_PosY= m_CAM_SIZE_HEIGHT/2;
// 
// 		//top
// 		PN_RECT[3].m_PosX=m_CAM_SIZE_WIDTH/2;
// 		PN_RECT[3].m_PosY=m_CAM_SIZE_HEIGHT*0.2916;
// 
// 		//bottom
// 		PN_RECT[4].m_PosX=m_CAM_SIZE_WIDTH/2;
// 		PN_RECT[4].m_PosY=m_CAM_SIZE_HEIGHT*0.7083;
// 		//left top
// 		PN_RECT[5].m_PosX=m_CAM_SIZE_WIDTH*0.222;
// 		PN_RECT[5].m_PosY=m_CAM_SIZE_HEIGHT*0.2916;
// 
// 		//right top
// 		PN_RECT[6].m_PosX=m_CAM_SIZE_WIDTH*0.777;
// 		PN_RECT[6].m_PosY=m_CAM_SIZE_HEIGHT*0.2916;
// 		//left bottom
// 		PN_RECT[7].m_PosX=m_CAM_SIZE_WIDTH*0.222;
// 		PN_RECT[7].m_PosY=m_CAM_SIZE_HEIGHT*0.7083;
// 
// 		//right bottom
// 		PN_RECT[8].m_PosX=m_CAM_SIZE_WIDTH*0.777;
// 		PN_RECT[8].m_PosY=m_CAM_SIZE_HEIGHT*0.7083;
// 
// 
// 		for(int t=0; t<9; t++){
// 		
// 			PN_Original[t].m_PosX = PN_RECT[t].m_PosX;
// 			PN_Original[t].m_PosY = PN_RECT[t].m_PosY;
// 		}
// 
// 
// 		m_Brightness= 255;
// 		m_Contrast= 255;
// 		//AutomationMod =0;
// 		for(int t=0; t<9; t++){
// 			
// 			PN_RECT[t].m_Width=rect_Width;
// 			PN_RECT[t].m_Height=rect_Height;
// 			PN_RECT[t].EX_RECT_SET(PN_RECT[t].m_PosX,PN_RECT[t].m_PosY,PN_RECT[t].m_Width,PN_RECT[t].m_Height);//////C
// 			PN_RECT[t].ENABLE = 1;
// 
// 		}
		//TestMod =0;

		str_MasterPath = "";
		st_PNMaster.str_MasterPath = str_MasterPath;

		Master_Data_Load();

		Save_parameter();
	}else{


		strTitle.Empty();
		strTitle="PN_INIT";


		str_MasterPath = GetPrivateProfileCString(str_model, "MASTERPATH", PN_filename);

		st_PNMaster.str_MasterPath = str_MasterPath;

		Master_Data_Load();

		b_BlockDataSave = GetPrivateProfileInt(str_model, "BLOCK_DATA_SAVE_PN", -1, PN_filename);
		if (b_BlockDataSave == -1)
		{
			b_BlockDataSave = 1;
			str.Format("%d", b_BlockDataSave);
			WritePrivateProfileString(str_model, "BLOCK_DATA_SAVE_PN", str, PN_filename);
		}

		ThrDark=GetPrivateProfileInt(str_model,strTitle+"Dark",-1,PN_filename);
		if(ThrDark == -1){
			ThrDark = 25;
			str.Empty();
			str.Format("%d",ThrDark);
			WritePrivateProfileString(str_model,"Dark",str,PN_filename);

		}
		ThrNoise=GetPrivateProfileInt(str_model,strTitle+"Noise",-1,PN_filename);
		if(ThrNoise == -1){
			ThrNoise = 255;
			str.Empty();
			str.Format("%d",ThrNoise);
			WritePrivateProfileString(str_model,"Noise",str,PN_filename);

		}
		ThrDiff=GetPrivateProfileInt(str_model,strTitle+"Diff",-1,PN_filename);
		if(ThrDiff == -1){
			ThrDiff = 0;
			str.Empty();
			str.Format("%d",ThrDiff);
			WritePrivateProfileString(str_model,"Diff",str,PN_filename);

		}

		m_dPatternNoiseOffset = GetPrivateProfileDouble(str_model, strTitle + "OFFSET", -999, PN_filename);
		if (m_dPatternNoiseOffset == -999){
			m_dPatternNoiseOffset = 1.0;
			str.Empty();
			str.Format("%6.2f", m_dPatternNoiseOffset);
			WritePrivateProfileString(str_model, "OFFSET", str, PN_filename);

		}
		
	


		PN_Total_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,PN_filename);
		if(PN_Total_PosX == -1){
			PN_Total_PosX =m_CAM_SIZE_WIDTH/2;
			str.Empty();
			str.Format("%d",PN_Total_PosX);
			WritePrivateProfileString(str_model,strTitle+"PosX",str,PN_filename);
		}
		PN_Total_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,PN_filename);
		if(PN_Total_PosY == -1){
			PN_Total_PosY =m_CAM_SIZE_HEIGHT/2;
			str.Empty();
			str.Format("%d",PN_Total_PosY);
			WritePrivateProfileString(str_model,strTitle+"PosY",str,PN_filename);
		}
		PN_Dis_Width=GetPrivateProfileInt(str_model,strTitle+"DisW",-1,PN_filename);
		if(PN_Dis_Width  == -1){
			PN_Dis_Width  =m_CAM_SIZE_WIDTH*0.277;
			str.Empty();
			str.Format("%d",PN_Dis_Width);
			WritePrivateProfileString(str_model,strTitle+"DisW",str,PN_filename);
		}
		PN_Dis_Height=GetPrivateProfileInt(str_model,strTitle+"DisH",-1,PN_filename);
		if(PN_Dis_Height  == -1){
			PN_Dis_Height  =m_CAM_SIZE_HEIGHT*0.2083;
			str.Empty();
			str.Format("%d",PN_Dis_Height);
			WritePrivateProfileString(str_model,strTitle+"DisH",str,PN_filename);
		}
		PN_Total_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,PN_filename);
		if(PN_Total_Width  == -1){
			PN_Total_Width  =rect_Width;
			str.Empty();
			str.Format("%d",PN_Total_Width);
			WritePrivateProfileString(str_model,strTitle+"Width",str,PN_filename);
		}
		PN_Total_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,PN_filename);
		if(PN_Total_Height  == -1){
			PN_Total_Height  =rect_Height;
			str.Empty();
			str.Format("%d",PN_Total_Height);
			WritePrivateProfileString(str_model,strTitle+"Height",str,PN_filename);
		}


		m_Brightness=GetPrivateProfileInt(str_model,strTitle+"Bright",-1,PN_filename);
		if(m_Brightness ==-1){
			m_Brightness =255;
			
			str.Empty();
			str.Format("%d",m_Brightness);
			WritePrivateProfileString(str_model,strTitle+"Bright",str,PN_filename);
		}
		str_CamBrightness.Format("%d",m_Brightness);

		m_Contrast=GetPrivateProfileInt(str_model,strTitle+"Contrast",-1,PN_filename);
		if(m_Contrast == -1){
			m_Contrast =255;

			str_CamContrast.Format("%d",m_Contrast);

			str.Empty();
			str.Format("%d",m_Contrast);
			WritePrivateProfileString(str_model,strTitle+"Contrast",str,PN_filename);
		}

		str_CamContrast.Format("%d",m_Contrast);
	
		for(int t=0; t<9; t++){
			if(t ==0){
				strTitle.Empty();
				strTitle="CENTER_";
			}
			if(t ==1){
				strTitle.Empty();
				strTitle="LEFT_";		
			}
			if(t ==2){
				strTitle.Empty();
				strTitle="RIGHT_";
			}
			if(t ==3){
				strTitle.Empty();
				strTitle="TOP_";		
			}
			if(t ==4){
				strTitle.Empty();
				strTitle="BOTTOM_";
			}
			if(t ==5){
				strTitle.Empty();
				strTitle="LEFT-TOP_";
			}
			if(t ==6){
				strTitle.Empty();
				strTitle="RIGHT-TOP_";
			}	
			if(t ==7){
				strTitle.Empty();
				strTitle="LEFT-BOTTOM_";
			}	
			if(t ==8){
				strTitle.Empty();
				strTitle="RIGHT-BOTTOM_";
			}	

						
			PN_RECT[t].m_startX = GetPrivateProfileInt(str_model, strTitle + "LEFT", -1, PN_filename);
			if (PN_RECT[t].m_startX == -1){

				for (int t = 0; t < 9; t++){
					PN_RECT[t].m_sizeX = 4;
					PN_RECT[t].m_sizeY = 4;
				}

				PN_RECT[0].m_startX = BLOCK_WIDTH / 2 - PN_RECT[0].m_sizeX / 2;
				PN_RECT[0].m_startY = BLOCK_HEIGHT / 2 - PN_RECT[0].m_sizeY / 2;

				PN_RECT[1].m_startX = BLOCK_WIDTH / 5 - PN_RECT[1].m_sizeX / 2;
				PN_RECT[1].m_startY = BLOCK_HEIGHT / 2 - PN_RECT[1].m_sizeY / 2;

				PN_RECT[2].m_startX = BLOCK_WIDTH * 4 / 5 - PN_RECT[2].m_sizeX / 2;
				PN_RECT[2].m_startY = BLOCK_HEIGHT / 2 - PN_RECT[2].m_sizeY / 2;

				PN_RECT[3].m_startX = BLOCK_WIDTH / 2 - PN_RECT[3].m_sizeX / 2;
				PN_RECT[3].m_startY = BLOCK_HEIGHT / 2 - 7 - PN_RECT[3].m_sizeY / 2;

				PN_RECT[4].m_startX = BLOCK_WIDTH / 2 - PN_RECT[4].m_sizeX / 2;
				PN_RECT[4].m_startY = BLOCK_HEIGHT / 2 + 7 - PN_RECT[4].m_sizeY / 2;


				PN_RECT[5].m_startX = BLOCK_WIDTH / 5 - PN_RECT[5].m_sizeX / 2;
				PN_RECT[5].m_startY = BLOCK_HEIGHT / 2 - 7 - PN_RECT[5].m_sizeY / 2;

				PN_RECT[6].m_startX = BLOCK_WIDTH * 4 / 5 - PN_RECT[6].m_sizeX / 2;
				PN_RECT[6].m_startY = BLOCK_HEIGHT / 2 - 7 - PN_RECT[6].m_sizeY / 2;
				
				PN_RECT[7].m_startX = BLOCK_WIDTH / 5 - PN_RECT[7].m_sizeX / 2;
				PN_RECT[7].m_startY = BLOCK_HEIGHT / 2 + 7 - PN_RECT[7].m_sizeY / 2;

				PN_RECT[8].m_startX = BLOCK_WIDTH * 4 / 5 - PN_RECT[8].m_sizeX / 2;
				PN_RECT[8].m_startY = BLOCK_HEIGHT / 2 + 7 - PN_RECT[8].m_sizeY / 2;


				for(int t=0; t<9; t++){
			
					PN_RECT[t].m_endX = PN_RECT[t].m_startX + PN_RECT[t].m_sizeX;
					PN_RECT[t].m_endY = PN_RECT[t].m_startY + PN_RECT[t].m_sizeY;

					Rect_Set(PN_RECT, t);
					PN_RECT[t].ENABLE = 1;

				}
			}
			PN_RECT[t].m_startY = GetPrivateProfileInt(str_model, strTitle + "TOP", -1, PN_filename);
			PN_RECT[t].m_endX = GetPrivateProfileInt(str_model, strTitle + "RIGHT", -1, PN_filename);
			PN_RECT[t].m_endY = GetPrivateProfileInt(str_model, strTitle + "BOTTOM", -1, PN_filename);
			PN_RECT[t].m_sizeX = GetPrivateProfileInt(str_model, strTitle + "SIZEX", -1, PN_filename);
			PN_RECT[t].m_sizeY = GetPrivateProfileInt(str_model, strTitle + "SIZEY", -1, PN_filename);
			PN_RECT[t].m_Master = GetPrivateProfileInt(str_model, strTitle + "MASTER", -1, PN_filename);
			PN_RECT[t].m_Thresold = GetPrivateProfileInt(str_model, strTitle + "Thresold", -1, PN_filename);
			PN_RECT[t].ENABLE=GetPrivateProfileInt(str_model,strTitle+"Enable",-1,PN_filename);	

			Rect_Set(PN_RECT, t);
// 			PN_RECT[t].m_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,PN_filename);
// 
// 			if(PN_RECT[t].m_PosX == -1){
// 				//center
// 						
// 				PN_Total_PosX = m_CAM_SIZE_WIDTH/2;
// 				PN_Total_PosY = m_CAM_SIZE_HEIGHT/2;
// 				PN_Dis_Width = m_CAM_SIZE_WIDTH*0.277;
// 				PN_Dis_Height = m_CAM_SIZE_HEIGHT*0.2083;
// 				PN_Total_Width = rect_Width;
// 				PN_Total_Height = rect_Height;
// 				//PN_Thresold = 10;
// 
// 				//center
// 				PN_RECT[0].m_PosX= m_CAM_SIZE_WIDTH/2;
// 				PN_RECT[0].m_PosY= m_CAM_SIZE_HEIGHT/2;
// 
// 				//left
// 				PN_RECT[1].m_PosX=m_CAM_SIZE_WIDTH*0.222;
// 				PN_RECT[1].m_PosY= m_CAM_SIZE_HEIGHT/2;
// 
// 				//right
// 				PN_RECT[2].m_PosX=m_CAM_SIZE_WIDTH*0.777;
// 				PN_RECT[2].m_PosY= m_CAM_SIZE_HEIGHT/2;
// 
// 				//top
// 				PN_RECT[3].m_PosX=m_CAM_SIZE_WIDTH/2;
// 				PN_RECT[3].m_PosY=m_CAM_SIZE_HEIGHT*0.2916;
// 
// 				//bottom
// 				PN_RECT[4].m_PosX=m_CAM_SIZE_WIDTH/2;
// 				PN_RECT[4].m_PosY=m_CAM_SIZE_HEIGHT*0.7083;
// 				//left top
// 				PN_RECT[5].m_PosX=m_CAM_SIZE_WIDTH*0.222;
// 				PN_RECT[5].m_PosY=m_CAM_SIZE_HEIGHT*0.2916;
// 
// 				//right top
// 				PN_RECT[6].m_PosX=m_CAM_SIZE_WIDTH*0.777;
// 				PN_RECT[6].m_PosY=m_CAM_SIZE_HEIGHT*0.2916;
// 				//left bottom
// 				PN_RECT[7].m_PosX=m_CAM_SIZE_WIDTH*0.222;
// 				PN_RECT[7].m_PosY=m_CAM_SIZE_HEIGHT*0.7083;
// 
// 				//right bottom
// 				PN_RECT[8].m_PosX=m_CAM_SIZE_WIDTH*0.777;
// 				PN_RECT[8].m_PosY=m_CAM_SIZE_HEIGHT*0.7083;
// 
// 
// 				for(int t=0; t<9; t++){
// 				
// 					PN_Original[t].m_PosX = PN_RECT[t].m_PosX;
// 					PN_Original[t].m_PosY = PN_RECT[t].m_PosY;
// 				}
// 
// 
// 				for(int t=0; t<9; t++){
// 			
// 					PN_RECT[t].m_Width=rect_Width;
// 					PN_RECT[t].m_Height=rect_Height;
// 					PN_RECT[t].EX_RECT_SET(PN_RECT[t].m_PosX,PN_RECT[t].m_PosY,PN_RECT[t].m_Width,PN_RECT[t].m_Height);//////C
// 					PN_RECT[t].ENABLE = 1;
// 
// 				}
// 
// 			}
// 			PN_Original[t].m_PosX=PN_RECT[t].m_PosX;
// 			PN_RECT[t].m_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,PN_filename);
// 			PN_Original[t].m_PosY=PN_RECT[t].m_PosY;
// 			PN_RECT[t].m_Left=GetPrivateProfileInt(str_model,strTitle+"StartX",-1,PN_filename);
// 			PN_RECT[t].m_Top=GetPrivateProfileInt(str_model,strTitle+"StartY",-1,PN_filename);
// 			PN_RECT[t].m_Right=GetPrivateProfileInt(str_model,strTitle+"ExitX",-1,PN_filename);
// 			PN_RECT[t].m_Bottom=GetPrivateProfileInt(str_model,strTitle+"ExitY",-1,PN_filename);
// 			PN_RECT[t].m_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,PN_filename);
// 			PN_RECT[t].m_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,PN_filename);	
// 			PN_RECT[t].ENABLE=GetPrivateProfileInt(str_model,strTitle+"Enable",-1,PN_filename);	

		}


	}

	UploadList();

}

void CPatternNoise_Option::Save_parameter(){

		CString str="";
		CString strTitle="";

		//WritePrivateProfileString(str_model,NULL,"",PN_filename);

		WritePrivateProfileString(str_model, "MASTERPATH", str_MasterPath, PN_filename);

		str.Empty();
		str.Format("%d", b_BlockDataSave);
		WritePrivateProfileString(str_model, "BLOCK_DATA_SAVE_PN", str, PN_filename);
		str.Empty();
		str.Format("%d",m_INFO.ID);
		WritePrivateProfileString(str_model,"ID",str,PN_filename);
		str.Empty();
		str.Format("%s",m_INFO.MODE);
		WritePrivateProfileString(str_model,"MODE",str,PN_filename);
		str.Empty();
		str.Format("%s",m_INFO.NAME);
		WritePrivateProfileString(str_model,"NAME",str,PN_filename);

		strTitle.Empty();
		strTitle="PN_INIT";

		str.Empty();
		str.Format("%d",ThrDark);
		WritePrivateProfileString(str_model,strTitle+"Dark",str,PN_filename);
		str.Empty();
		str.Format("%d",ThrNoise);
		WritePrivateProfileString(str_model,strTitle+"Noise",str,PN_filename);
		str.Empty();
		str.Format("%d",ThrDiff);
		WritePrivateProfileString(str_model,strTitle+"Diff",str,PN_filename);


		str.Empty();
		str.Format("%6.2f", m_dPatternNoiseOffset);
		WritePrivateProfileString(str_model, strTitle + "OFFSET", str, PN_filename);

		str.Empty();
		str.Format("%d",PN_Total_PosX);
		WritePrivateProfileString(str_model,strTitle+"PosX",str,PN_filename);
		str.Empty();
		str.Format("%d",PN_Total_PosY);
		WritePrivateProfileString(str_model,strTitle+"PosY",str,PN_filename);
		str.Empty();
		str.Format("%d",PN_Dis_Width);
		WritePrivateProfileString(str_model,strTitle+"DisW",str,PN_filename);
		str.Empty();
		str.Format("%d",PN_Dis_Height);
		WritePrivateProfileString(str_model,strTitle+"DisH",str,PN_filename);
		str.Empty();
		str.Format("%d",PN_Total_Width);
		WritePrivateProfileString(str_model,strTitle+"Width",str,PN_filename);
		str.Empty();
		str.Format("%d",PN_Total_Height);
		WritePrivateProfileString(str_model,strTitle+"Height",str,PN_filename);

		str_CamBrightness.Format("%d",m_Brightness);
		str.Empty();
		str.Format("%d",m_Brightness);
		WritePrivateProfileString(str_model,strTitle+"Bright",str,PN_filename);
		str_CamContrast.Format("%d",m_Contrast);

		str.Empty();
		str.Format("%d",m_Contrast);
		WritePrivateProfileString(str_model,strTitle+"Contrast",str,PN_filename);
	
		for(int t=0; t<9; t++){

			if(t ==0){
				strTitle.Empty();
				strTitle="CENTER_";
			}
			if(t ==1){
				strTitle.Empty();
				strTitle="LEFT_";		
			}
			if(t ==2){
				strTitle.Empty();
				strTitle="RIGHT_";
			}
			if(t ==3){
				strTitle.Empty();
				strTitle="TOP_";		
			}
			if(t ==4){
				strTitle.Empty();
				strTitle="BOTTOM_";
			}
			if(t ==5){
				strTitle.Empty();
				strTitle="LEFT-TOP_";
			}
			if(t ==6){
				strTitle.Empty();
				strTitle="RIGHT-TOP_";
			}	
			if(t ==7){
				strTitle.Empty();
				strTitle="LEFT-BOTTOM_";
			}	
			if(t ==8){
				strTitle.Empty();
				strTitle="RIGHT-BOTTOM_";
			}	
			str.Empty();
			str.Format("%d", PN_RECT[t].m_startX);
			WritePrivateProfileString(str_model, strTitle + "LEFT", str, PN_filename);
			str.Empty();
			str.Format("%d", PN_RECT[t].m_startY);
			WritePrivateProfileString(str_model, strTitle + "TOP", str, PN_filename);
			str.Empty();
			str.Format("%d", PN_RECT[t].m_endX);
			WritePrivateProfileString(str_model, strTitle + "RIGHT", str, PN_filename);
			str.Empty();
			str.Format("%d", PN_RECT[t].m_endY);
			WritePrivateProfileString(str_model, strTitle + "BOTTOM", str, PN_filename);
			str.Empty();
			str.Format("%d", PN_RECT[t].m_sizeX);
			WritePrivateProfileString(str_model, strTitle + "SIZEX", str, PN_filename);
			str.Empty();
			str.Format("%d", PN_RECT[t].m_sizeY);
			WritePrivateProfileString(str_model, strTitle + "SIZEY", str, PN_filename);

			str.Empty();
			str.Format("%d",PN_RECT[t].m_PosX);
			PN_Original[t].m_PosX=PN_RECT[t].m_PosX;
			WritePrivateProfileString(str_model,strTitle+"PosX",str,PN_filename);
			str.Empty();
			str.Format("%d",PN_RECT[t].m_PosY);
			PN_Original[t].m_PosY=PN_RECT[t].m_PosY;
			WritePrivateProfileString(str_model,strTitle+"PosY",str,PN_filename);
			str.Empty();
			str.Format("%d",PN_RECT[t].m_Left);
			WritePrivateProfileString(str_model,strTitle+"StartX",str,PN_filename);
			str.Empty();
			str.Format("%d",PN_RECT[t].m_Top);
			WritePrivateProfileString(str_model,strTitle+"StartY",str,PN_filename);
			str.Empty();
			str.Format("%d",PN_RECT[t].m_Right);
			WritePrivateProfileString(str_model,strTitle+"ExitX",str,PN_filename);
			str.Empty();
			str.Format("%d",PN_RECT[t].m_Bottom);
			WritePrivateProfileString(str_model,strTitle+"ExitY",str,PN_filename);
			str.Empty();
			str.Format("%d",PN_RECT[t].m_Width);
			WritePrivateProfileString(str_model,strTitle+"Width",str,PN_filename);
			str.Empty();
			str.Format("%d",PN_RECT[t].m_Height);
			WritePrivateProfileString(str_model,strTitle+"Height",str,PN_filename);

			str.Empty();
			str.Format("%d",PN_RECT[t].ENABLE);
			WritePrivateProfileString(str_model,strTitle+"Enable",str,PN_filename);
	
		}

		UploadList();

}

void CPatternNoise_Option::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",PN_filename);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

//---------------------------------------------------------------------WORKLIST

void CPatternNoise_Option::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	
	InsertIndex = m_PatternNoiseList.InsertItem(StartCnt,"",0);
	
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_PatternNoiseList.SetItemText(InsertIndex,0,strCnt);
	
	m_PatternNoiseList.SetItemText(InsertIndex,1,((CImageTesterDlg  *)m_pMomWnd)->m_modelName);
	m_PatternNoiseList.SetItemText(InsertIndex,2,((CImageTesterDlg  *)m_pMomWnd)->strDay+"");
	m_PatternNoiseList.SetItemText(InsertIndex,3,((CImageTesterDlg  *)m_pMomWnd)->strTime+"");



}
void CPatternNoise_Option::Set_List(CListCtrl *List){/////

	while(List->DeleteColumn(0));
	((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	//List->InsertColumn(2, "LOT CARD", LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);

	int LISTNUM = 4;

	CString str;
	for (int t = 0; t< st_PNMaster.pt_StartPointList.size(); t++)
	{
		str.Format(_T("SNR_%d"), t + 1);
		List->InsertColumn(LISTNUM++, str, LVCFMT_CENTER, 80);
	}




// 	CString ListName;
// 	int ListNUM = 5;
// 
// 	for (int t = 0; t < 400; t++)
// 	{
// 		ListName.Format("[%d]-SNR", t + 1);
// 		List->InsertColumn(ListNUM, ListName, LVCFMT_CENTER, 80);
// 		ListNUM++;
// 
// 	}

	List->InsertColumn(LISTNUM++, "RESULT", LVCFMT_CENTER, 80);
	List->InsertColumn(LISTNUM++, "비고", LVCFMT_CENTER, 80);

	ListItemNum = LISTNUM;
	Copy_List(&m_PatternNoiseList, ((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList, ListItemNum);
}

CString CPatternNoise_Option::Mes_Result()
{
	CString sz[9];
	int		nResult[9];

	// 제일 마지막 항목을 추가하자
	int nSel = m_PatternNoiseList.GetItemCount()-1;

	// C-DB
	CString str;
	CString strData;

	
	strData.Empty();
	m_szMesResult = _T("");
	if(b_StopFail == FALSE){
		int LISTNUM = 4;
		int nCount = 0;
		for (int t = 0; t < st_PNMaster.pt_StartPointList.size(); t++)
		{


			sz[0] = m_PatternNoiseList.GetItemText(nSel, LISTNUM++);

			strData += sz[0] + ":";

			if (t != st_PNMaster.pt_StartPointList.size() - 1){
				if (st_PNMaster.bResult)
				{
					strData += "1,";
				}
				else{
					strData += "0,";
				}

			}
			else{
				if (st_PNMaster.bResult)
				{
					strData += "1";
				}
				else{
					strData += "0";
				}
			}

			nCount++;
		}

		if (nCount < 56)
		{

			strData += (",");
			for (int t = nCount; t < 55; t++)
			{
				strData += (":1,");
			}
			strData += (":1");
		}


		m_szMesResult = strData;
		m_szMesResult.Replace(" ", "");
	}
	else{
		for (int t = 0; t < st_PNMaster.pt_StartPointList.size(); t++)
		{

			if (t != st_PNMaster.pt_StartPointList.size() - 1)
			{
				strData += ":1,";
			}
			else{
				strData += ":1";

			}
		}


		m_szMesResult = strData;
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_stNewMesData[NewMES_SNR] = m_szMesResult;

	return m_szMesResult;
}

void CPatternNoise_Option::EXCEL_SAVE()
{
	//CString Item[9]={"Center_","Left_","Right_","Top_","Bottom_","Left-Top_","Right-Top_","Left-Bottom_","Right-Bottom_"};
	CString Item[400] = { "MIN" };

	CString ListName;
	int ListNUM = 0;

	
	CString str;
	for (int t = 0; t < st_PNMaster.pt_StartPointList.size(); t++)
	{
		str.Format(_T("SNR_%d"), t + 1);
		Item[ListNUM] = str;
		ListNUM++;
	}


	CString Data ="";
	str = "***********************이물광원 SNR [ 단위 : 표준편차 ] ***********************\n\n";
	Data += str;
	str.Empty();
	str.Format("STD Threshold, %d  , \n",ThrDark);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	
	((CImageTesterDlg  *)m_pMomWnd)->SAVE_EXCEL("이물광원SNR", Item, ListNUM, &m_PatternNoiseList, Data);
}

bool CPatternNoise_Option::EXCEL_UPLOAD(){
	m_PatternNoiseList.DeleteAllItems();
	if(((CImageTesterDlg  *)m_pMomWnd)->EXCEL_UPLOAD("이물광원SNR",&m_PatternNoiseList,1,&StartCnt)== TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}

	return TRUE;
}

void CPatternNoise_Option::OnEnChangeThresoldY()
{
	int buf_ThrDark = GetDlgItemInt(IDC_Thresold_Y);	
	int buf_ThrNoise = GetDlgItemInt(IDC_Thresold_V);
	int buf_ThrDiff = GetDlgItemInt(IDC_Thresold_D);

	if((buf_ThrDark >= 0)/*&&(buf_ThrDark <= 100)&&(buf_ThrNoise >= 0)&&(buf_ThrNoise <= 255)&&(buf_ThrDiff >= 0)&&(buf_ThrDiff <= 255)*/){
		if((buf_ThrDark == ThrDark)&&(buf_ThrNoise == ThrNoise)/*&&(buf_ThrDiff == ThrDiff)*/){
			//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->EnableWindow(0);
		}else{
			//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->EnableWindow(1);
		}

	}else{
		//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->EnableWindow(0);
	}

	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CPatternNoise_Option::OnEnChangeThresoldV()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	int buf_ThrDark = GetDlgItemInt(IDC_Thresold_Y);	
	int buf_ThrNoise = GetDlgItemInt(IDC_Thresold_V);
	int buf_ThrDiff = GetDlgItemInt(IDC_Thresold_D);

	if((buf_ThrDark >= 0)&&(buf_ThrDark <= 255)&&(buf_ThrNoise >= 0)&&(buf_ThrNoise <= 255)/*&&(buf_ThrDiff >= 0)&&(buf_ThrDiff <= 255)*/){
		if((buf_ThrDark == ThrDark)&&(buf_ThrNoise == ThrNoise)/*&&(buf_ThrDiff == ThrDiff)*/){
			//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->EnableWindow(0);
		}else{
			//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->EnableWindow(1);
		}

	}else{
		//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->EnableWindow(0);
	}

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CPatternNoise_Option::OnEnChangeThresoldD()
{
	UpdateData(TRUE);
}

void CPatternNoise_Option::OnBnClickedButtonPnSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);


	CString strTitle ="";
	strTitle.Empty();
	strTitle="PN_INIT";

	CString str="";
	str.Empty();
	str.Format("%d",ThrDark);
	WritePrivateProfileString(str_model,strTitle+"Dark",str,PN_filename);
	str.Empty();
	str.Format("%d",ThrNoise);
	WritePrivateProfileString(str_model,strTitle+"Noise",str,PN_filename);
	str.Empty();
	str.Format("%d",ThrDiff);
	WritePrivateProfileString(str_model,strTitle+"Diff",str,PN_filename);
	str.Empty();
	str.Format("%6.2f", m_dPatternNoiseOffset);
	WritePrivateProfileString(str_model, strTitle + "OFFSET", str, PN_filename);

	

	UpdateData(FALSE);
	//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->EnableWindow(0);

}

BOOL CPatternNoise_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

		if (pMsg->wParam == VK_RETURN){
			if ((pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Thresold_Y))->GetSafeHwnd())||  
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Thresold_V))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Thresold_D))->GetSafeHwnd()))
			{	
				if(((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->IsWindowEnabled() == TRUE){
					OnBnClickedButtonPnSave();
				}
			}
			
			return TRUE;
		}
		
	}

	return CDialog::PreTranslateMessage(pMsg);
}

	
void CPatternNoise_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == TRUE){
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;

		if(((CImageTesterDlg  *)m_pMomWnd)->b_SecretOption == 1){
			((CEdit *)GetDlgItem(IDC_Thresold_D))->ShowWindow(1);
			//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE2))->ShowWindow(1);

		}else{
			((CEdit *)GetDlgItem(IDC_Thresold_D))->ShowWindow(0);
			//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE2))->ShowWindow(0);

		}
		
	}else{
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = -1;
		
	}
}

// void CPatternNoise_Option::OnBnClickedButtonPnSave2()
// {
// 	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
// 
// 	CString str;
// 	str.Empty();
// 	str.Format("%d",ThrDiff);
// 	WritePrivateProfileString(str_model,"Diff",str,PN_filename);
// 	UpdateData(FALSE);
// }

void CPatternNoise_Option::OnBnClickedButtonPnLoad()
{

		Load_parameter();
		UpdateData(FALSE);
		UploadList();
	
}

void CPatternNoise_Option::UploadList(){
	CString str="";

	PN_DATALIST.DeleteAllItems();

	for(int t=0; t<9; t++){
		InIndex = PN_DATALIST.InsertItem(t,"",0);


		if(t ==0){
			str.Empty();str="CENTER";
		}
		if(t ==1){
			str.Empty();str="LEFT";
		}
		if(t ==2){
			str.Empty();str="RIGHT";
		}
		if(t ==3){
			str.Empty();str="TOP";
		}
		if(t ==4){
			str.Empty();str="BOTTOM";
		}	
		if(t ==5){
			str.Empty();str="LEFT-TOP";
		}
		if(t ==6){
			str.Empty();str="RIGHT-TOP";
		}
		if(t ==7){
			str.Empty();str="LEFT-BOTTOM";
		}
		if(t ==8){
			str.Empty();str="RIGHT-BOTTOM";
		}
		PN_DATALIST.SetItemText(InIndex,0,str);

		str.Empty(); str.Format("%d", PN_RECT[t].m_startX);
		PN_DATALIST.SetItemText(InIndex,1,str);
		str.Empty(); str.Format("%d", PN_RECT[t].m_startY);
		PN_DATALIST.SetItemText(InIndex,2,str);

		str.Empty();
		str.Format("%d", PN_RECT[t].m_endX);
		PN_DATALIST.SetItemText(InIndex,3,str);
		str.Empty();
		str.Format("%d", PN_RECT[t].m_endY);
		PN_DATALIST.SetItemText(InIndex,4,str);

		str.Empty();
		str.Format("%d", PN_RECT[t].m_sizeX);
		PN_DATALIST.SetItemText(InIndex,5,str);
		str.Empty();
		str.Format("%d", PN_RECT[t].m_sizeY);
		PN_DATALIST.SetItemText(InIndex,6,str);


// 		str.Empty();str.Format("%d", PN_RECT[t].m_PosX);
// 		PN_DATALIST.SetItemText(InIndex,1,str);
// 		str.Empty();str.Format("%d", PN_RECT[t].m_PosY);
// 		PN_DATALIST.SetItemText(InIndex,2,str);
// 
// 		str.Empty();
// 		str.Format("%d", PN_RECT[t].m_Left);
// 		PN_DATALIST.SetItemText(InIndex,3,str);
// 		str.Empty();
// 		str.Format("%d", PN_RECT[t].m_Top);
// 		PN_DATALIST.SetItemText(InIndex,4,str);
// 
// 		str.Empty();
// 		str.Format("%d", PN_RECT[t].m_Right+1);
// 		PN_DATALIST.SetItemText(InIndex,5,str);
// 		str.Empty();
// 		str.Format("%d", PN_RECT[t].m_Bottom+1);
// 		PN_DATALIST.SetItemText(InIndex,6,str);
// 
// 		str.Empty();
// 		str.Format("%d", PN_RECT[t].m_Width);
// 		PN_DATALIST.SetItemText(InIndex,7,str);
// 		str.Empty();
// 		str.Format("%d",PN_RECT[t].m_Height);
// 		PN_DATALIST.SetItemText(InIndex,8,str);

		if(PN_RECT[t].ENABLE == TRUE){
			PN_DATALIST.SetItemState(t,0x2000,LVIS_STATEIMAGEMASK);
		}else{
			PN_DATALIST.SetItemState(t,0x1000,LVIS_STATEIMAGEMASK);
		}
	}
}
void CPatternNoise_Option::OnBnClickedButtonPnSavezone()
{
	UpdateData(TRUE);
	Save_parameter();
	UpdateData(FALSE);
	//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->EnableWindow(0);

	ChangeCheck =0;
	for(int t=0; t<9; t++){
		ChangeItem[t]=-1;
	}
	for(int t=0; t<9; t++){
		PN_DATALIST.Update(t);
	}
}

void CPatternNoise_Option::Save_parameter_set()
{
	
	CString str="";
	//CString strTitle="";

	//	WritePrivateProfileString(str_model,NULL,"",PN_filename);
		str.Empty();
		str.Format("%d",m_INFO.ID);
		WritePrivateProfileString(str_model,"ID",str,PN_filename);
		str.Empty();
		str.Format("%s",m_INFO.MODE);
		WritePrivateProfileString(str_model,"MODE",str,PN_filename);
		str.Empty();
		str.Format("%s",m_INFO.NAME);
		WritePrivateProfileString(str_model,"NAME",str,PN_filename);

		str.Empty();
		str.Format("%d",ThrDark);
		WritePrivateProfileString(str_model,"Dark",str,PN_filename);
		str.Empty();
		str.Format("%d",ThrNoise);
		WritePrivateProfileString(str_model,"Noise",str,PN_filename);
		str.Empty();
		str.Format("%d",ThrDiff);
		WritePrivateProfileString(str_model,"Diff",str,PN_filename);

}


void CPatternNoise_Option::OnNMClickListPatternnoise(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;
}
void CPatternNoise_Option::List_COLOR_Change(){
	bool FLAG = TRUE;
	int Check=0;


	for(int t=0;t<9; t++){
		if(ChangeItem[t]==0)Check++;
		if(ChangeItem[t]==1)Check++;
		if(ChangeItem[t]==2)Check++;
		if(ChangeItem[t]==3)Check++;
		if(ChangeItem[t]==4)Check++;
		if(ChangeItem[t]==5)Check++;
		if(ChangeItem[t]==6)Check++;
		if(ChangeItem[t]==7)Check++;
		if(ChangeItem[t]==8)Check++;
	}
	if(Check !=8){
		FLAG =FALSE;
		ChangeItem[changecount]=iSavedItem;
	}

	if(!FLAG){
		for(int t=0; t<changecount+1; t++){
			for(int k=0; k<changecount+1; k++){

				if(ChangeItem[t] == ChangeItem[k]){
					if(t==k){break;	}			
					ChangeItem[k] =-1;
				}


			}
		}
		//----------------데이터 정렬
		int temp=0;

		for(int i=0; i<9; i++)
		{
			for(int j=i+1; j<9; j++)
			{
				if(ChangeItem[i] < ChangeItem[j])  // 내림차순
				{
					temp = ChangeItem[i];
					ChangeItem[i] = ChangeItem[j];
					ChangeItem[j] = temp;
				}
			}
		}
		//--------------------------------
		for(int t=0; t<9; t++){

			if(ChangeItem[t] ==-1){
				changecount =t;
				break;
			}
		}
	}
	for(int t=0; t<9; t++){
		PN_DATALIST.Update(t);
	}

}


void CPatternNoise_Option::Change_DATA(){

	CString str;

	((CEdit *)GetDlgItemText(IDC_EDIT_A_MOD, str));
	PN_DATALIST.SetItemText(iSavedItem, iSavedSubitem, str);

	int num = _ttoi(str);
	if(num < 0){
		num =0;

	}
	if(iSavedSubitem ==1){
		PN_RECT[iSavedItem].m_startX = num;
		PN_RECT[iSavedItem].m_endX = num + PN_RECT[iSavedItem].m_sizeX;
		//			PN_RECT[iSavedItem].m_PosX = num;

	}
	if(iSavedSubitem ==2){
		PN_RECT[iSavedItem].m_startY = num;
		PN_RECT[iSavedItem].m_endY = num + PN_RECT[iSavedItem].m_sizeY;
		//			PN_RECT[iSavedItem].m_PosY = num;
	}
	if(iSavedSubitem ==3){
		PN_RECT[iSavedItem].m_endX = num;
		PN_RECT[iSavedItem].m_sizeX = PN_RECT[iSavedItem].m_endX - PN_RECT[iSavedItem].m_startX;
		// 			PN_RECT[iSavedItem].m_Left = num;
		// 			PN_RECT[iSavedItem].m_PosX=(PN_RECT[iSavedItem].m_Right+1 +PN_RECT[iSavedItem].m_Left) /2;
		// 			PN_RECT[iSavedItem].m_Width=PN_RECT[iSavedItem].m_Right+1 -PN_RECT[iSavedItem].m_Left;

	}
	if(iSavedSubitem ==4){
		PN_RECT[iSavedItem].m_endY = num;
		PN_RECT[iSavedItem].m_sizeY = PN_RECT[iSavedItem].m_endY - PN_RECT[iSavedItem].m_startY;
		// 			PN_RECT[iSavedItem].m_Top = num;
		// 			PN_RECT[iSavedItem].m_PosY=(PN_RECT[iSavedItem].m_Top +PN_RECT[iSavedItem].m_Bottom+1)/2;
		// 			PN_RECT[iSavedItem].m_Height=PN_RECT[iSavedItem].m_Bottom+1 -PN_RECT[iSavedItem].m_Top;

	}
	if(iSavedSubitem ==5){
		PN_RECT[iSavedItem].m_sizeX = num;
		PN_RECT[iSavedItem].m_endX = PN_RECT[iSavedItem].m_startX + num;

		// 			if(num == 0){
		// 				PN_RECT[iSavedItem].m_Right = 0;
		// 			}
		// 			else{
		// 				PN_RECT[iSavedItem].m_Right = num;
		// 				PN_RECT[iSavedItem].m_PosX=(PN_RECT[iSavedItem].m_Right +PN_RECT[iSavedItem].m_Left+1) /2;
		// 				PN_RECT[iSavedItem].m_Width=PN_RECT[iSavedItem].m_Right+1 -PN_RECT[iSavedItem].m_Left;
		// 			}
	}
	if (iSavedSubitem == 6){
		PN_RECT[iSavedItem].m_sizeY = num;
		PN_RECT[iSavedItem].m_endY = PN_RECT[iSavedItem].m_startY + num;

		// 			if(num ==0){
		// 				PN_RECT[iSavedItem].m_Bottom =0;
		// 			}
		// 			else{
		// 				PN_RECT[iSavedItem].m_Bottom = num;
		// 				PN_RECT[iSavedItem].m_PosY=(PN_RECT[iSavedItem].m_Top +PN_RECT[iSavedItem].m_Bottom+1)/2;
		// 				PN_RECT[iSavedItem].m_Height=PN_RECT[iSavedItem].m_Bottom+1 -PN_RECT[iSavedItem].m_Top;
		// 			}
	}

	Rect_Set(PN_RECT, iSavedItem);

// 	if(iSavedSubitem ==7){
// 		PN_RECT[iSavedItem].m_Width = num;
// 	}
// 	if(iSavedSubitem ==8){
// 		PN_RECT[iSavedItem].m_Height = num;
// 	}
	/*if(iSavedSubitem ==9){
		PN_RECT[iSavedItem].m_Thresold = num;
	}*/
// 	PN_RECT[iSavedItem].EX_RECT_SET(PN_RECT[iSavedItem].m_PosX,PN_RECT[iSavedItem].m_PosY,PN_RECT[iSavedItem].m_Width,PN_RECT[iSavedItem].m_Height);
// 
// 
// 	if(PN_RECT[iSavedItem].m_Left < 0){//////////////////////수정
// 		PN_RECT[iSavedItem].m_Left = 0;
// 		PN_RECT[iSavedItem].m_Width = PN_RECT[iSavedItem].m_Right+1 -PN_RECT[iSavedItem].m_Left;
// 		PN_RECT[iSavedItem].EX_RECT_SET(PN_RECT[iSavedItem].m_PosX,PN_RECT[iSavedItem].m_PosY,PN_RECT[iSavedItem].m_Width,PN_RECT[iSavedItem].m_Height);
// 	}
// 	if(PN_RECT[iSavedItem].m_Right >m_CAM_SIZE_WIDTH){
// 		PN_RECT[iSavedItem].m_Right = m_CAM_SIZE_WIDTH;
// 		PN_RECT[iSavedItem].m_Width = PN_RECT[iSavedItem].m_Right+1 -PN_RECT[iSavedItem].m_Left;
// 		PN_RECT[iSavedItem].EX_RECT_SET(PN_RECT[iSavedItem].m_PosX,PN_RECT[iSavedItem].m_PosY,PN_RECT[iSavedItem].m_Width,PN_RECT[iSavedItem].m_Height);
// 	}
// 	if(PN_RECT[iSavedItem].m_Top< 0){
// 		PN_RECT[iSavedItem].m_Top = 0;
// 		PN_RECT[iSavedItem].m_Height = PN_RECT[iSavedItem].m_Bottom+1 -PN_RECT[iSavedItem].m_Top;
// 		PN_RECT[iSavedItem].EX_RECT_SET(PN_RECT[iSavedItem].m_PosX,PN_RECT[iSavedItem].m_PosY,PN_RECT[iSavedItem].m_Width,PN_RECT[iSavedItem].m_Height);
// 	}
// 	if(PN_RECT[iSavedItem].m_Height >m_CAM_SIZE_HEIGHT){
// 		PN_RECT[iSavedItem].m_Height = m_CAM_SIZE_HEIGHT;
// 		PN_RECT[iSavedItem].m_Width = PN_RECT[iSavedItem].m_Bottom+1 -PN_RECT[iSavedItem].m_Top;
// 		PN_RECT[iSavedItem].EX_RECT_SET(PN_RECT[iSavedItem].m_PosX,PN_RECT[iSavedItem].m_PosY,PN_RECT[iSavedItem].m_Width,PN_RECT[iSavedItem].m_Height);
// 	}
// 
// 	if(PN_RECT[iSavedItem].m_Height<= 0){
// 		PN_RECT[iSavedItem].m_Height = 1;
// 		PN_RECT[iSavedItem].EX_RECT_SET(PN_RECT[iSavedItem].m_PosX,PN_RECT[iSavedItem].m_PosY,PN_RECT[iSavedItem].m_Width,PN_RECT[iSavedItem].m_Height);
// 	}
// 	if(PN_RECT[iSavedItem].m_Height >=m_CAM_SIZE_HEIGHT){
// 		PN_RECT[iSavedItem].m_Height = m_CAM_SIZE_HEIGHT;
// 		PN_RECT[iSavedItem].EX_RECT_SET(PN_RECT[iSavedItem].m_PosX,PN_RECT[iSavedItem].m_PosY,PN_RECT[iSavedItem].m_Width,PN_RECT[iSavedItem].m_Height);
// 	}
// 
// 	if(PN_RECT[iSavedItem].m_Width <= 0){
// 		PN_RECT[iSavedItem].m_Width = 1;
// 		PN_RECT[iSavedItem].EX_RECT_SET(PN_RECT[iSavedItem].m_PosX,PN_RECT[iSavedItem].m_PosY,PN_RECT[iSavedItem].m_Width,PN_RECT[iSavedItem].m_Height);
// 	}
// 	if(PN_RECT[iSavedItem].m_Width >=m_CAM_SIZE_WIDTH){
// 		PN_RECT[iSavedItem].m_Width = m_CAM_SIZE_WIDTH;
// 		PN_RECT[iSavedItem].EX_RECT_SET(PN_RECT[iSavedItem].m_PosX,PN_RECT[iSavedItem].m_PosY,PN_RECT[iSavedItem].m_Width,PN_RECT[iSavedItem].m_Height);
// 	}
// 
// 
// 
// 	PN_RECT[iSavedItem].EX_RECT_SET(PN_RECT[iSavedItem].m_PosX,PN_RECT[iSavedItem].m_PosY,PN_RECT[iSavedItem].m_Width,PN_RECT[iSavedItem].m_Height);


	CString data = "";

	if(iSavedSubitem ==1){
		data.Format("%d", PN_RECT[iSavedItem].m_startX);
	}
	else if(iSavedSubitem ==2){
		data.Format("%d", PN_RECT[iSavedItem].m_startY);
	}
	else if(iSavedSubitem ==3){
		data.Format("%d", PN_RECT[iSavedItem].m_endX);
	}
	else if(iSavedSubitem ==4){
		data.Format("%d", PN_RECT[iSavedItem].m_endY);
	}
	else if(iSavedSubitem ==5){
		data.Format("%d", PN_RECT[iSavedItem].m_sizeX);
	}
	else if(iSavedSubitem ==6){
		data.Format("%d", PN_RECT[iSavedItem].m_sizeY);
	}

	((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText(data);

}

bool CPatternNoise_Option::Change_DATA_CHECK(bool FLAG){

	BOOL STAT = TRUE;
	//상한
	if (PN_RECT[iSavedItem].m_startX > BLOCK_WIDTH - 1)
		STAT = FALSE;
	if (PN_RECT[iSavedItem].m_startY > BLOCK_HEIGHT - 1)
		STAT = FALSE;
	if (PN_RECT[iSavedItem].m_endX > BLOCK_WIDTH)
		STAT = FALSE;
	if (PN_RECT[iSavedItem].m_endY > BLOCK_HEIGHT)
		STAT = FALSE;
	if (PN_RECT[iSavedItem].m_sizeX > BLOCK_WIDTH)
		STAT = FALSE;
	if (PN_RECT[iSavedItem].m_sizeY > BLOCK_HEIGHT)
		STAT = FALSE;


	if(PN_RECT[iSavedItem].m_Width > m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(PN_RECT[iSavedItem].m_Height > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(PN_RECT[iSavedItem].m_PosX > m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(PN_RECT[iSavedItem].m_PosY > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(PN_RECT[iSavedItem].m_Top > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(PN_RECT[iSavedItem].m_Bottom > m_CAM_SIZE_HEIGHT  )
		STAT = FALSE;
	if(PN_RECT[iSavedItem].m_Left> m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(PN_RECT[iSavedItem].m_Right> m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	/*if(PN_RECT[iSavedItem].m_Thresold> 200 )
		STAT = FALSE;*/

	//하한
	if (PN_RECT[iSavedItem].m_startX < 0)
		STAT = FALSE;
	if (PN_RECT[iSavedItem].m_startY < 0)
		STAT = FALSE;
	if (PN_RECT[iSavedItem].m_endX < 1)
		STAT = FALSE;
	if (PN_RECT[iSavedItem].m_endY < 1)
		STAT = FALSE;
	if (PN_RECT[iSavedItem].m_sizeX < 1)
		STAT = FALSE;
	if (PN_RECT[iSavedItem].m_sizeY < 1)
		STAT = FALSE;

	if(PN_RECT[iSavedItem].m_Width < 1 )
		STAT = FALSE;
	if(PN_RECT[iSavedItem].m_Height < 1 )
		STAT = FALSE;
	if(PN_RECT[iSavedItem].m_PosX < 0 )
		STAT = FALSE;
	if(PN_RECT[iSavedItem].m_PosY < 0 )
		STAT = FALSE;
	if(PN_RECT[iSavedItem].m_Top < 0 )
		STAT = FALSE;
	if(PN_RECT[iSavedItem].m_Bottom < 0  )
		STAT = FALSE;
	if(PN_RECT[iSavedItem].m_Left< 0 )
		STAT = FALSE;
	if(PN_RECT[iSavedItem].m_Right< 0 )
		STAT = FALSE;
	/*if(PN_RECT[iSavedItem].m_Thresold< 0 )
		STAT = FALSE;*/


	if(FLAG  == 1){
		if(PN_RECT[iSavedItem].m_Width == 1 )
			STAT = FALSE;
		if(PN_RECT[iSavedItem].m_Height == 1 )
			STAT = FALSE;

	}

	return STAT;



}

void CPatternNoise_Option::OnNMCustomdrawListPatternnoise(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	 COLORREF text_color = 0;
	 COLORREF bg_color = RGB(255, 255, 255);
     /////////////////////////////////////////////////////////////default 컬러

     LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
		
	//	lplvcd->iPartId
        switch(lplvcd->nmcd.dwDrawStage){
            case CDDS_PREPAINT:
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
             
            // 배경 혹은 텍스트를 수정한다.
            case CDDS_ITEMPREPAINT:
                // 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
               /* if(lplvcd->nmcd.dwItemSpec == 0) text_color = RGB(255, 0, 0);
                else if(lplvcd->nmcd.dwItemSpec == 1) text_color = RGB(0, 255, 0);
                else text_color = RGB(0, 0, 255);
                lplvcd->clrText = text_color;*/
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
           
            // 서브 아이템의 배경 혹은 텍스트를 수정한다.
            case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
                if(lplvcd->iSubItem != 0){
                    // 1번째 행이라면...

					if(lplvcd->nmcd.dwItemSpec >=0 ||lplvcd->nmcd.dwItemSpec <9){
						/*if(AutomationMod ==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 7 ){
								text_color = RGB(0, 0, 0);
								bg_color = RGB(200, 200, 200);
							}
						}
						else{
							text_color = RGB(0, 0, 0);
							bg_color = RGB(255, 255, 255);
						
						}*/

						text_color = RGB(0, 0, 0);
						bg_color = RGB(255, 255, 255);

						if(ChangeCheck==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 9 ){
								for(int t=0; t<9; t++){
									if(lplvcd->nmcd.dwItemSpec == ChangeItem[t]){
										text_color = RGB(0, 0, 0);
										bg_color = RGB(250, 230, 200);					
									}
								}
							}
						
						
						}
					
					}
					

				    lplvcd->clrText = text_color;
                    lplvcd->clrTextBk = bg_color;
					
                }
                *pResult = CDRF_NEWFONT;
                return;
        }
	//*pResult = 0;
}

void CPatternNoise_Option::OnEnKillfocusEditAMod()
{
	CString str = "";
	ChangeCheck=1;
	if((iSavedItem != -1)&&(iSavedSubitem != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_A_MOD, str));
		List_COLOR_Change();
		if(PN_DATALIST.GetItemText(iSavedItem, iSavedSubitem) != str){
			Change_DATA();
			UploadList();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );
	iSavedItem = -1;
	iSavedSubitem = -1;
	((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
}

void CPatternNoise_Option::OnNMDblclkListPatternnoise(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		return;
	}

	if(pNMITEM->iSubItem == 0 || pNMITEM->iSubItem == -1 || pNMITEM->iItem == -1 )
		return ;
	iSavedItem = pNMITEM->iItem;
	iSavedSubitem = pNMITEM->iSubItem;


	if(pNMITEM->iItem != -1)
	{
		if(pNMITEM->iSubItem != 0)
		{		
			PN_DATALIST.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			PN_DATALIST.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText(PN_DATALIST.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetFocus();

		}else{
			SetTimer(160,5,NULL); //InitProgram을 불러들이는데 쓰인다.
		}
	}
	List_COLOR_Change();
	UpdateData(FALSE);

	*pResult = 0;
}

void CPatternNoise_Option::OnBnClickedButtonsetpnzone()
{
	ChangeCheck=1;
	UpdateData(TRUE);

	for(int t=0; t<9; t++){
		PN_RECT[t].m_sizeX = 4;
		PN_RECT[t].m_sizeY = 4;
		}

	PN_RECT[0].m_startX = BLOCK_WIDTH / 2 - PN_RECT[0].m_sizeX / 2;
	PN_RECT[0].m_startY = BLOCK_HEIGHT / 2 - PN_RECT[0].m_sizeY / 2;

	PN_RECT[1].m_startX = BLOCK_WIDTH / 5 - PN_RECT[1].m_sizeX / 2;
	PN_RECT[1].m_startY = BLOCK_HEIGHT / 2 - PN_RECT[1].m_sizeY / 2;

	PN_RECT[2].m_startX = BLOCK_WIDTH * 4 / 5 - PN_RECT[2].m_sizeX / 2;
	PN_RECT[2].m_startY = BLOCK_HEIGHT / 2 - PN_RECT[2].m_sizeY / 2;

	PN_RECT[3].m_startX = BLOCK_WIDTH / 2 - PN_RECT[3].m_sizeX / 2;
	PN_RECT[3].m_startY = BLOCK_HEIGHT / 2 - 7 - PN_RECT[3].m_sizeY / 2;

	PN_RECT[4].m_startX = BLOCK_WIDTH / 2 - PN_RECT[4].m_sizeX / 2;
	PN_RECT[4].m_startY = BLOCK_HEIGHT / 2 + 7 - PN_RECT[4].m_sizeY / 2;


	PN_RECT[5].m_startX = BLOCK_WIDTH / 5 - PN_RECT[5].m_sizeX / 2;
	PN_RECT[5].m_startY = BLOCK_HEIGHT / 2 - 7 - PN_RECT[5].m_sizeY / 2;

	PN_RECT[6].m_startX = BLOCK_WIDTH * 4 / 5 - PN_RECT[6].m_sizeX / 2;
	PN_RECT[6].m_startY = BLOCK_HEIGHT / 2 - 7 - PN_RECT[6].m_sizeY / 2;

	PN_RECT[7].m_startX = BLOCK_WIDTH / 5 - PN_RECT[7].m_sizeX / 2;
	PN_RECT[7].m_startY = BLOCK_HEIGHT / 2 + 7 - PN_RECT[7].m_sizeY / 2;

	PN_RECT[8].m_startX = BLOCK_WIDTH * 4 / 5 - PN_RECT[8].m_sizeX / 2;
	PN_RECT[8].m_startY = BLOCK_HEIGHT / 2 + 7 - PN_RECT[8].m_sizeY / 2;


	for (int t = 0; t < 9; t++){

		PN_RECT[t].m_endX = PN_RECT[t].m_startX + PN_RECT[t].m_sizeX;
		PN_RECT[t].m_endY = PN_RECT[t].m_startY + PN_RECT[t].m_sizeY;

		Rect_Set(PN_RECT, t);


	}

// 	for(int t=0; t<9; t++){
// 		PN_RECT[t].m_Width= PN_Total_Width;
// 		PN_RECT[t].m_Height=PN_Total_Height;
// 
// 		if (t==0)
// 		{
// 			PN_RECT[t].m_PosX = PN_Total_PosX;
// 			PN_RECT[t].m_PosY = PN_Total_PosY;
// 		}
// 		if (t==1)
// 		{
// 			PN_RECT[t].m_PosX = PN_Total_PosX-PN_Dis_Width;
// 			PN_RECT[t].m_PosY = PN_Total_PosY/*-PN_Dis_Height*/;
// 		}
// 		if (t==2)
// 		{
// 			PN_RECT[t].m_PosX = PN_Total_PosX+PN_Dis_Width;
// 			PN_RECT[t].m_PosY = PN_Total_PosY;
// 		}
// 		if (t==3)
// 		{
// 			PN_RECT[t].m_PosX = PN_Total_PosX;
// 			PN_RECT[t].m_PosY = PN_Total_PosY-PN_Dis_Height;
// 		}
// 		if (t==4)
// 		{
// 			PN_RECT[t].m_PosX = PN_Total_PosX;
// 			PN_RECT[t].m_PosY = PN_Total_PosY+PN_Dis_Height;
// 		}
// 		if (t==5)
// 		{
// 			PN_RECT[t].m_PosX = PN_Total_PosX-PN_Dis_Width;
// 			PN_RECT[t].m_PosY = PN_Total_PosY-PN_Dis_Height;
// 		}
// 		if (t==6)
// 		{
// 			PN_RECT[t].m_PosX = PN_Total_PosX+PN_Dis_Width;
// 			PN_RECT[t].m_PosY = PN_Total_PosY-PN_Dis_Height;
// 		}
// 		if (t==7)
// 		{
// 			PN_RECT[t].m_PosX = PN_Total_PosX-PN_Dis_Width;
// 			PN_RECT[t].m_PosY = PN_Total_PosY+PN_Dis_Height;
// 		}
// 		if (t==8)
// 		{
// 			PN_RECT[t].m_PosX = PN_Total_PosX+PN_Dis_Width;
// 			PN_RECT[t].m_PosY = PN_Total_PosY+PN_Dis_Height;
// 		}
// 
// 
// 		PN_RECT[t].EX_RECT_SET(PN_RECT[t].m_PosX,PN_RECT[t].m_PosY,PN_RECT[t].m_Width,PN_RECT[t].m_Height);//////C
// 
// 		if(PN_RECT[t].m_Left < 0){//////////////////////수정
// 			PN_RECT[t].m_Left = 0;
// 			PN_RECT[t].m_Width = PN_RECT[t].m_Right+1 -PN_RECT[t].m_Left;
// 			PN_RECT[t].EX_RECT_SET(PN_RECT[t].m_PosX,PN_RECT[t].m_PosY,PN_RECT[t].m_Width,PN_RECT[t].m_Height);
// 		}
// 		if(PN_RECT[t].m_Right >m_CAM_SIZE_WIDTH){
// 			PN_RECT[t].m_Right = m_CAM_SIZE_WIDTH;
// 			PN_RECT[t].m_Width = PN_RECT[t].m_Right+1 -PN_RECT[t].m_Left;
// 			PN_RECT[t].EX_RECT_SET(PN_RECT[t].m_PosX,PN_RECT[t].m_PosY,PN_RECT[t].m_Width,PN_RECT[t].m_Height);
// 		}
// 		if(PN_RECT[t].m_Top< 0){
// 			PN_RECT[t].m_Top = 0;
// 			PN_RECT[t].m_Height = PN_RECT[t].m_Bottom+1 -PN_RECT[t].m_Top;
// 			PN_RECT[t].EX_RECT_SET(PN_RECT[t].m_PosX,PN_RECT[t].m_PosY,PN_RECT[t].m_Width,PN_RECT[t].m_Height);
// 		}
// 		if(PN_RECT[t].m_Bottom >m_CAM_SIZE_HEIGHT){
// 			int gap = PN_RECT[t].m_Bottom - m_CAM_SIZE_HEIGHT;
// 			PN_RECT[t].m_Bottom = m_CAM_SIZE_HEIGHT;
// 			PN_RECT[t].m_Height = PN_RECT[t].m_Bottom -PN_RECT[t].m_Top -gap;
// 			PN_RECT[t].EX_RECT_SET(PN_RECT[t].m_PosX,PN_RECT[t].m_PosY,PN_RECT[t].m_Width,PN_RECT[t].m_Height);
// 		}
// 		PN_RECT[t].EX_RECT_SET(PN_RECT[t].m_PosX,PN_RECT[t].m_PosY,PN_RECT[t].m_Width,PN_RECT[t].m_Height);
// 
// 		//PN_RECT[t].m_Thresold = A_Thresold;
// 	}
	UpdateData(FALSE);

	UploadList();//리스트컨트롤의 입력


	for(int t=0; t<9; t++){
		ChangeItem[t]=t;
	}

	for(int t=0; t<9; t++){
		PN_DATALIST.Update(t);
	}

	((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(1);
	//Save_parameter();

}


BOOL CPatternNoise_Option::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	int znum = zDelta/120;
	CWnd *focusid;
	focusid = GetFocus();
	bool FLAG=0;
	if(znum > 0 ){
		FLAG = 1;
	}
	else if(znum < 0 ){
		FLAG =0;
	}
	else if(znum == 0){
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	}
	int casenum = focusid->GetDlgCtrlID();
	switch(casenum){
		case IDC_PN_PosX   :
			EditWheelIntSet(IDC_PN_PosX ,znum);
			OnEnChangePnPosx();
			break;
		case IDC_PN_PosY :
			EditWheelIntSet(IDC_PN_PosY,znum);
			OnEnChangePnPosy();
			break;
		case IDC_PN_Dis_W :
			EditWheelIntSet(IDC_PN_Dis_W ,znum);
			OnEnChangePnDisW();
			break;
		case IDC_PN_Dis_H :
			EditWheelIntSet(IDC_PN_Dis_H,znum);
			OnEnChangePnDisH();
			break;
		case  IDC_PN_Width :
			EditWheelIntSet( IDC_PN_Width,znum);
			OnEnChangePnWidth();
			break;
		case IDC_PN_Height :
			EditWheelIntSet(IDC_PN_Height,znum);
			OnEnChangePnHeight();
			break;
	
		case IDC_EDIT_A_MOD :
			if((iSavedSubitem != 9 )&& (iSavedSubitem != 10)){
				if(FLAG == 1){
					if((Change_DATA_CHECK(1) == TRUE) /*|| (znum ==-1)*/){
						EditWheelIntSet(IDC_EDIT_A_MOD,znum);
						Change_DATA();
						UploadList();
						OnEnChangeEditAMod();
					}
				}
				if(FLAG == 0){
					if((Change_DATA_CHECK(0) == TRUE) /*|| (znum == 1)*/){
						EditWheelIntSet(IDC_EDIT_A_MOD,znum);
						Change_DATA();
						UploadList();
						OnEnChangeEditAMod();
					}
				}



			}

			break;
	}

	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}
void CPatternNoise_Option::EditWheelIntSet(int nID,int Val)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = 0;
	}else{
		buf = GetDlgItemInt(nID);
		buf+= Val;
	}
	SetDlgItemInt(nID,buf,1);
	((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
}
void CPatternNoise_Option::OnEnChangeEditAMod()
{
	int num = iSavedItem;
	if((PN_RECT[num].m_PosX >= 0)&&(PN_RECT[num].m_PosX < m_CAM_SIZE_WIDTH)&&
		(PN_RECT[num].m_PosY >= 0)&&(PN_RECT[num].m_PosY < m_CAM_SIZE_HEIGHT)&&
		(PN_RECT[num].m_Left>=0)&&(PN_RECT[num].m_Left<m_CAM_SIZE_WIDTH)&&
		(PN_RECT[num].m_Top>=0)&&(PN_RECT[num].m_Top<m_CAM_SIZE_HEIGHT)&&
		(PN_RECT[num].m_Right>=0)&&(PN_RECT[num].m_Right<m_CAM_SIZE_WIDTH)&&
		(PN_RECT[num].m_Bottom>=0)&&(PN_RECT[num].m_Bottom<m_CAM_SIZE_HEIGHT)&&
		(PN_RECT[num].m_Width>=0)&&(PN_RECT[num].m_Width<m_CAM_SIZE_WIDTH)&&
		(PN_RECT[num].m_Height>=0)&&(PN_RECT[num].m_Height<m_CAM_SIZE_HEIGHT)){
			((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVEZONE))->EnableWindow(1);

			/*if((num != 0)&&(num !=1)){
				if((PN_RECT[num].MTFRatio>=1)&&(PN_RECT[num].MTFRatio<99)&&
					(PN_RECT[num].m_Thresold>=MIN_RESOLUTION)&&(PN_RECT[num].m_Thresold<=MAX_RESOLUTION)){
						((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVEZONE))->EnableWindow(1);


				}else{
					((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVEZONE))->EnableWindow(0);

				}
			}
*/


	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVEZONE))->EnableWindow(0);
	}
}


void CPatternNoise_Option::EditMinMaxIntSet(int nID,int* Val,int Min,int Max)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = Min;
		retval = FALSE;
	}else{
		buf = GetDlgItemInt(nID);
		if(buf<Min){
			buf = *Val;
			retval = TRUE;
		}else if(buf > Max){
			buf = *Val;
			retval = TRUE;
		}else{
			retval = FALSE;
		}
	}
	*Val = buf;
	if(retval == TRUE){
		SetDlgItemInt(nID,buf,1);
		((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
	}
}

void CPatternNoise_Option::OnEnChangePnPosx()
{
	EditMinMaxIntSet(IDC_PN_PosX,&PN_Total_PosX,0,m_CAM_SIZE_WIDTH);
	PN_Total_PosX = GetDlgItemInt(IDC_PN_PosX);	
	PN_Total_PosY = GetDlgItemInt(IDC_PN_PosY);
	PN_Dis_Width	= GetDlgItemInt(IDC_PN_Dis_W);
	PN_Dis_Height = GetDlgItemInt(IDC_PN_Dis_H);
	PN_Total_Width = GetDlgItemInt(IDC_PN_Width);
	PN_Total_Height = GetDlgItemInt(IDC_PN_Height);

	if((PN_Total_PosX >= 0)&&(PN_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(PN_Total_PosY >= 0)&&(PN_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(PN_Dis_Width>=0)&&(PN_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(PN_Dis_Height>=0)&&(PN_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(PN_Total_Width>=0)&&(PN_Total_Width<m_CAM_SIZE_WIDTH)&&
		(PN_Total_Height>=0)&&(PN_Total_Height<m_CAM_SIZE_HEIGHT)){
	/*	if((PN_RECT[0].m_PosX == m_PosX)&&(PN_RECT[0].m_PosY == m_PosY)&&(PN_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(0);
	}
}

void CPatternNoise_Option::OnEnChangePnPosy()
{
	EditMinMaxIntSet(IDC_PN_PosY,&PN_Total_PosY,0,m_CAM_SIZE_HEIGHT);
	PN_Total_PosX = GetDlgItemInt(IDC_PN_PosX);	
	PN_Total_PosY = GetDlgItemInt(IDC_PN_PosY);
	PN_Dis_Width	= GetDlgItemInt(IDC_PN_Dis_W);
	PN_Dis_Height = GetDlgItemInt(IDC_PN_Dis_H);
	PN_Total_Width = GetDlgItemInt(IDC_PN_Width);
	PN_Total_Height = GetDlgItemInt(IDC_PN_Height);

	if((PN_Total_PosX >= 0)&&(PN_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(PN_Total_PosY >= 0)&&(PN_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(PN_Dis_Width>=0)&&(PN_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(PN_Dis_Height>=0)&&(PN_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(PN_Total_Width>=0)&&(PN_Total_Width<m_CAM_SIZE_WIDTH)&&
		(PN_Total_Height>=0)&&(PN_Total_Height<m_CAM_SIZE_HEIGHT)){
	/*	if((PN_RECT[0].m_PosX == m_PosX)&&(PN_RECT[0].m_PosY == m_PosY)&&(PN_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(0);
	}
}

void CPatternNoise_Option::OnEnChangePnDisW()
{
	EditMinMaxIntSet(IDC_PN_Dis_W,&PN_Dis_Width,0,m_CAM_SIZE_WIDTH);
	PN_Total_PosX = GetDlgItemInt(IDC_PN_PosX);	
	PN_Total_PosY = GetDlgItemInt(IDC_PN_PosY);
	PN_Dis_Width	= GetDlgItemInt(IDC_PN_Dis_W);
	PN_Dis_Height = GetDlgItemInt(IDC_PN_Dis_H);
	PN_Total_Width = GetDlgItemInt(IDC_PN_Width);
	PN_Total_Height = GetDlgItemInt(IDC_PN_Height);

	if((PN_Total_PosX >= 0)&&(PN_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(PN_Total_PosY >= 0)&&(PN_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(PN_Dis_Width>=0)&&(PN_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(PN_Dis_Height>=0)&&(PN_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(PN_Total_Width>=0)&&(PN_Total_Width<m_CAM_SIZE_WIDTH)&&
		(PN_Total_Height>=0)&&(PN_Total_Height<m_CAM_SIZE_HEIGHT)){
	/*	if((PN_RECT[0].m_PosX == m_PosX)&&(PN_RECT[0].m_PosY == m_PosY)&&(PN_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(0);
	}
}

void CPatternNoise_Option::OnEnChangePnDisH()
{
	EditMinMaxIntSet(IDC_PN_Dis_H,&PN_Dis_Height,0,m_CAM_SIZE_HEIGHT);
	PN_Total_PosX = GetDlgItemInt(IDC_PN_PosX);	
	PN_Total_PosY = GetDlgItemInt(IDC_PN_PosY);
	PN_Dis_Width	= GetDlgItemInt(IDC_PN_Dis_W);
	PN_Dis_Height = GetDlgItemInt(IDC_PN_Dis_H);
	PN_Total_Width = GetDlgItemInt(IDC_PN_Width);
	PN_Total_Height = GetDlgItemInt(IDC_PN_Height);

	if((PN_Total_PosX >= 0)&&(PN_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(PN_Total_PosY >= 0)&&(PN_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(PN_Dis_Width>=0)&&(PN_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(PN_Dis_Height>=0)&&(PN_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(PN_Total_Width>=0)&&(PN_Total_Width<m_CAM_SIZE_WIDTH)&&
		(PN_Total_Height>=0)&&(PN_Total_Height<m_CAM_SIZE_HEIGHT)){
	/*	if((PN_RECT[0].m_PosX == m_PosX)&&(PN_RECT[0].m_PosY == m_PosY)&&(PN_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(0);
	}
}

void CPatternNoise_Option::OnEnChangePnWidth()
{
	EditMinMaxIntSet(IDC_PN_Width,&PN_Total_Width,0,m_CAM_SIZE_WIDTH);
	PN_Total_PosX = GetDlgItemInt(IDC_PN_PosX);	
	PN_Total_PosY = GetDlgItemInt(IDC_PN_PosY);
	PN_Dis_Width	= GetDlgItemInt(IDC_PN_Dis_W);
	PN_Dis_Height = GetDlgItemInt(IDC_PN_Dis_H);
	PN_Total_Width = GetDlgItemInt(IDC_PN_Width);
	PN_Total_Height = GetDlgItemInt(IDC_PN_Height);

	if((PN_Total_PosX >= 0)&&(PN_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(PN_Total_PosY >= 0)&&(PN_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(PN_Dis_Width>=0)&&(PN_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(PN_Dis_Height>=0)&&(PN_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(PN_Total_Width>=0)&&(PN_Total_Width<m_CAM_SIZE_WIDTH)&&
		(PN_Total_Height>=0)&&(PN_Total_Height<m_CAM_SIZE_HEIGHT)){
		((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(1);
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(0);
	}
}

void CPatternNoise_Option::OnEnChangePnHeight()
{
	EditMinMaxIntSet(IDC_PN_Height,&PN_Total_Height,0,m_CAM_SIZE_HEIGHT);
	PN_Total_PosX = GetDlgItemInt(IDC_PN_PosX);	
	PN_Total_PosY = GetDlgItemInt(IDC_PN_PosY);
	PN_Dis_Width	= GetDlgItemInt(IDC_PN_Dis_W);
	PN_Dis_Height = GetDlgItemInt(IDC_PN_Dis_H);
	PN_Total_Width = GetDlgItemInt(IDC_PN_Width);
	PN_Total_Height = GetDlgItemInt(IDC_PN_Height);

	if((PN_Total_PosX >= 0)&&(PN_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(PN_Total_PosY >= 0)&&(PN_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(PN_Dis_Width>=0)&&(PN_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(PN_Dis_Height>=0)&&(PN_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(PN_Total_Width>=0)&&(PN_Total_Width<m_CAM_SIZE_WIDTH)&&
		(PN_Total_Height>=0)&&(PN_Total_Height<m_CAM_SIZE_HEIGHT)){
	/*	if((PN_RECT[0].m_PosX == m_PosX)&&(PN_RECT[0].m_PosY == m_PosY)&&(PN_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(0);
	}
}

void CPatternNoise_Option::OnTimer(UINT_PTR nIDEvent)
{
	UINT BUF = 0;
	if(nIDEvent == 150){
		KillTimer(150);
		BUF = PN_DATALIST.GetItemState(iSavedItem,LVIS_STATEIMAGEMASK);
		if( BUF == 0x2000){
			PN_RECT[iSavedItem].ENABLE = TRUE;
		}else if(BUF == 0x1000){
			PN_RECT[iSavedItem].ENABLE = FALSE;
		}
	}else if(nIDEvent == 160){
		KillTimer(160);
		BUF = PN_DATALIST.GetItemState(iSavedItem,LVIS_STATEIMAGEMASK);
		/*if( BUF == 0x2000){
			PN_RECT[iSavedItem].ENABLE = TRUE;
		}else if(BUF == 0x1000){
			PN_RECT[iSavedItem].ENABLE = FALSE;
		}*/
	}
	CDialog::OnTimer(nIDEvent);
}

void CPatternNoise_Option::OnBnClickedButtonCam()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int bright= atoi(str_CamBrightness);

	if(bright >=255){
		bright = 255;
		str_CamBrightness.Format("%d",bright);
	}
	if(bright <0){
		bright =0;
		str_CamBrightness.Format("%d",bright);
	}

	str_CamBrightness.Format("%d",bright);

	int Contrast= atoi(str_CamContrast);

	if(Contrast >=255){
		Contrast = 255;
		str_CamContrast.Format("%d",Contrast);
	}
	if(Contrast <0){
		Contrast =0;
		str_CamContrast.Format("%d",Contrast);
	}

	str_CamContrast.Format("%d",Contrast);

	m_Brightness=bright;
	m_Contrast=Contrast;
	CString strTitle ="";
	strTitle.Empty();
	strTitle="PN_INIT";
	CString str ="";


	str_CamBrightness.Format("%d",m_Brightness);
	str.Empty();
	str.Format("%d",m_Brightness);
	WritePrivateProfileString(str_model,strTitle+"Bright",str,PN_filename);
	str_CamContrast.Format("%d",m_Contrast);

	str.Empty();
	str.Format("%d",m_Contrast);
	WritePrivateProfileString(str_model,strTitle+"Contrast",str,PN_filename);
	UpdateData(FALSE);


}

void CPatternNoise_Option::OnEnChangeEditCamBright()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CPatternNoise_Option::OnEnChangeEditCamContrast()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CPatternNoise_Option::OnBnClickedButtontest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(0);
	((CImageTesterDlg  *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(1);
}


#pragma region LOT관련 함수
void CPatternNoise_Option::LOT_Set_List(CListCtrl *List){

	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT 명",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);
// 	List->InsertColumn(5,"C-STD",LVCFMT_CENTER, 80);
// 	List->InsertColumn(6,"L-STD",LVCFMT_CENTER, 80);
// 	List->InsertColumn(7,"R-STD",LVCFMT_CENTER, 80);
// 	List->InsertColumn(8,"T-STD",LVCFMT_CENTER, 80);
// 	List->InsertColumn(9,"B-STD",LVCFMT_CENTER, 80);
// 	List->InsertColumn(10,"LT-STD",LVCFMT_CENTER, 80);
// 	List->InsertColumn(11,"RT-STD",LVCFMT_CENTER, 80);
// 	List->InsertColumn(12,"LB-STD",LVCFMT_CENTER, 80);
// 	List->InsertColumn(13,"RB-STD",LVCFMT_CENTER, 80);
// 	List->InsertColumn(14,"RESULT",LVCFMT_CENTER, 80);
// 	List->InsertColumn(15,"비고",LVCFMT_CENTER, 80);

	List->InsertColumn(5, "MIN", LVCFMT_CENTER, 80);

// 	CString ListName;
// 	int ListNUM = 6;
// 
// 	for (int t = 0; t < 400; t++)
// 	{
// 		ListName.Format("[%d]-SNR", t + 1);
// 		List->InsertColumn(ListNUM, ListName, LVCFMT_CENTER, 80);
// 		ListNUM++;
// 
// 	}

	List->InsertColumn(6, "RESULT", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "비고", LVCFMT_CENTER, 80);

	Lot_InsertNum = 8;
	Copy_List(&m_Lot_PatternNoiseList, ((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList, Lot_InsertNum);
}

void CPatternNoise_Option::LOT_InsertDataList(){
	CString strCnt="";

	//int Index = m_Lot_PatternNoiseList.InsertItem(Lot_StartCnt,"",0);
	Lot_InsertIndex = m_Lot_PatternNoiseList.InsertItem(Lot_StartCnt, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Lot_InsertIndex + 1);
	m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, 0, strCnt);

	m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, 1, ((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, 2, ((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);////////LOT 이름
	m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, 3, ((CImageTesterDlg *)m_pMomWnd)->strDay + "");
	m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, 4, ((CImageTesterDlg *)m_pMomWnd)->strTime + "");

// 	int CopyIndex = m_PatternNoiseList.GetItemCount()-1;
// 
// 	int count =0;
// 	for(int t=0; t< Lot_InsertNum;t++){
// 		if((t != 2)&&(t!=0)){
// 			m_Lot_PatternNoiseList.SetItemText(Index,t, m_PatternNoiseList.GetItemText(CopyIndex,count));
// 			count++;
// 
// 		}else if(t==0){
// 			count++;
// 		}else{
// 			m_Lot_PatternNoiseList.SetItemText(Index,t,((CImageTesterDlg  *)m_pMomWnd)->LOT_NUMBER);
// 		}
// 	}
// 
// 	Lot_StartCnt++;

}
void CPatternNoise_Option::LOT_EXCEL_SAVE(){
	//CString Item[9]={"Center_","Left_","Right_","Top_","Bottom_","Left-Top_","Right-Top_","Left-Bottom_","Right-Bottom_"};
	CString Item[401] = { "MIN", };

	CString ListName;
	int ListNUM = 1;

	for (int t = 0; t < 400; t++)
	{
		ListName.Format("[%d]-SNR", t + 1);
		Item[ListNUM] = ListName;
		ListNUM++;
	}


	CString Data ="";
	CString str="";
	str = "***********************이물광원 SNR [ 단위 : 표준편차 ] ***********************\n\n";
	Data += str;
	str.Empty();
	str.Format("STD Threshold, %d  , \n",ThrDark);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	
	((CImageTesterDlg  *)m_pMomWnd)->LOT_SAVE_EXCEL("이물광원SNR",Item,401,&m_Lot_PatternNoiseList,Data);
}

bool CPatternNoise_Option::LOT_EXCEL_UPLOAD(){
	m_Lot_PatternNoiseList.DeleteAllItems();
	if(((CImageTesterDlg  *)m_pMomWnd)->LOT_EXCEL_UPLOAD("이물광원SNR",&m_Lot_PatternNoiseList,1,&Lot_StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}
 
#pragma endregion 

void CPatternNoise_Option::Rect_Set(CRectData *Rect, int Num){

	Rect[Num].m_Left = Rect[Num].m_startX * BLOCK_SIZE;
	Rect[Num].m_Right = Rect[Num].m_endX * BLOCK_SIZE;
	Rect[Num].m_Top = Rect[Num].m_startY*BLOCK_SIZE;
	Rect[Num].m_Bottom = Rect[Num].m_endY*BLOCK_SIZE;
	Rect[Num].m_PosX = (Rect[Num].m_Left + Rect[Num].m_Right) /2;
	Rect[Num].m_PosY = (Rect[Num].m_Top + Rect[Num].m_Bottom) / 2;
	Rect[Num].m_Width = Rect[Num].m_sizeX * BLOCK_SIZE;
	Rect[Num].m_Height = Rect[Num].m_sizeY * BLOCK_SIZE;

}
void CPatternNoise_Option::PatternNoiseGen_Block(LPBYTE IN_RGB, CRectData *Rect, int X, int Y){

	m_Success = FALSE;
	//BYTE *BW;
	BYTE R, G, B;
	//BYTE abstraction;
	double total_abs = 0;
	double avg_abs = 0;

	double BrightnessValue = (double)ThrDiff;///ThrDiff 루리텍 비밀 옵션
	double env_Value = (double)ThrNoise;///ThrDiff 루리텍 비밀 옵션
	double dBThreshold = (double)ThrDark;

	int i = 0;
	BYTE **BW;
	BW = (BYTE **)malloc(sizeof(BYTE *)*CAM_IMAGE_HEIGHT);                     //   
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)BW[i] = (BYTE *)malloc(sizeof(BYTE)* CAM_IMAGE_WIDTH);


	for (int t = 0; t<m_CAM_SIZE_HEIGHT; t++){
		for (int k = 0; k<m_CAM_SIZE_WIDTH; k++){
			BW[t][k] = 0;
		}
	}

	int startx = X*BLOCK_SIZE;
	int starty = Y*BLOCK_SIZE;
	int endx = startx + BLOCK_SIZE - 1;
	int endy = starty + BLOCK_SIZE - 1;


	double **Cal_X;
	Cal_X = (double **)malloc(sizeof(double *)*CAM_IMAGE_HEIGHT);                     //   
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)Cal_X[i] = (double *)malloc(sizeof(double)* 2);

	double **Cal_Y;
	Cal_Y = (double **)malloc(sizeof(double *)*CAM_IMAGE_WIDTH);                     //   
	for (i = 0; i<CAM_IMAGE_WIDTH; i++)Cal_Y[i] = (double *)malloc(sizeof(double)* 2);

	double **testX;
	testX = (double **)malloc(sizeof(double *)* 2);                     //   
	for (i = 0; i<2; i++)testX[i] = (double *)malloc(sizeof(double)* CAM_IMAGE_HEIGHT);

	double **testY;
	testY = (double **)malloc(sizeof(double *)* 2);                     //   
	for (i = 0; i<2; i++)testY[i] = (double *)malloc(sizeof(double)* CAM_IMAGE_WIDTH);

	for (int t = 0; t<m_CAM_SIZE_HEIGHT; t++){
		Cal_X[t][0] = 0;
		Cal_X[t][1] = 0;
		testX[0][t] = 0;
		testX[1][t] = 0;
		for (int k = 0; k<m_CAM_SIZE_WIDTH; k++){
			BW[t][k] = 0;
			Cal_Y[k][0] = 0;
			Cal_Y[k][1] = 0;
			testY[0][k] = 0;
			testY[1][k] = 0;

		}
	}



	/*double Cal_X[CAM_IMAGE_HEIGHT][2]={0,},testX[2][CAM_IMAGE_HEIGHT];
	double Cal_Y[CAM_IMAGE_WIDTH][2]={0,},testY[2][CAM_IMAGE_WIDTH]; */

	int count = 0, CountLine[2] = { 0, };
	total_abs = 0;
	double originData = 0;
	for (int y = starty; y<endy; y++)
	{
		Cal_X[y - starty][0] = 0;	Cal_X[y - starty][1] = 0; CountLine[0] = 0;
		for (int x = startx; x<endx; x++)
		{
			B = (IN_RGB[y *(m_CAM_SIZE_WIDTH * 3) + (x * 3) + 0]);
			G = (IN_RGB[y *(m_CAM_SIZE_WIDTH * 3) + (x * 3) + 1]);
			R = (IN_RGB[y *(m_CAM_SIZE_WIDTH * 3) + (x * 3) + 2]);

			//originData += (BYTE)((0.29900*(R))+(0.58700*(G))+(0.11400*(B)));


			BW[y][x] = (255 - (BYTE)((0.29900*(R)) + (0.58700*(G)) + (0.11400*(B))));


			total_abs += BW[y][x];
			count++;
			CountLine[0]++;
			Cal_X[y - starty][0] += (double)BW[y][x];			Cal_X[y - starty][1] += (double)BW[y][x] * (double)BW[y][x];
		}
		if (CountLine[0]>0){
			Cal_X[y - starty][0] /= (double)CountLine[0]; Cal_X[y - starty][1] /= (double)CountLine[0];
			Cal_X[y - starty][1] = Cal_X[y - starty][1] - Cal_X[y - starty][0] * Cal_X[y - starty][0];
		}
	}

	double avg_origin = 0;
	avg_origin = total_abs / (double)count;
	//avg_abs = total_abs / (double)count; // 추출된 rgb 값에 대한 평균(전체 평균)
	CountLine[1] = 0;
	for (int x = startx; x < endx; x++){
		Cal_Y[x - startx][0] = 0;		Cal_Y[x - startx][1] = 0;  CountLine[1] = 0;

		for (int y = starty; y < endy; y++){

			if (m_OverlayArea[x][y] == 1){
				Cal_Y[x - startx][0] += (double)BW[y][x];			Cal_Y[x - startx][1] += (double)BW[y][x] * (double)BW[y][x];
				CountLine[1]++;
			}
		}
		if (CountLine[1]>0){
			Cal_Y[x - startx][0] /= (double)CountLine[1]; Cal_Y[x - startx][1] /= (double)CountLine[1];
			Cal_Y[x - startx][1] = Cal_Y[x - startx][1] - Cal_Y[x - startx][0] * Cal_Y[x - startx][0];
		}
	}


	double Dark = 0, Noise[2];
	Noise[0] = Noise[1] = 0;
	for (long i = 0; i<BLOCK_SIZE - 1; i++)	Dark += Cal_X[i][0];    Dark /= (double)BLOCK_SIZE - 1;
	for (long i = 0; i<BLOCK_SIZE - 1; i++)	Noise[0] += Cal_X[i][1];    Noise[0] /= (double)BLOCK_SIZE - 1;
	for (long i = 0; i<BLOCK_SIZE - 1; i++)	Noise[1] += Cal_Y[i][1];    Noise[1] /= (double)BLOCK_SIZE - 1;

	double SNR[4] = { 0, }, Sum[4] = { 0, };
	int Margin;
	int cnt[2] = { 0, };

	//	Margin=(int)(endx*0.2);
	//	for(int i=0+Margin;i<endx-Margin;i++){
	for (int i = 0; i<BLOCK_SIZE - 1; i++){

		Sum[0] += Cal_Y[i][0];		SNR[0] += Cal_Y[i][0] * Cal_Y[i][0];
		Sum[1] += Cal_Y[i][1];		SNR[1] += Cal_Y[i][1] * Cal_Y[i][1];
		cnt[0]++;
	}

	Sum[0] /= (double)cnt[0];		SNR[0] /= (double)cnt[0];
	Sum[1] /= (double)cnt[0];		SNR[1] /= (double)cnt[0];

	SNR[0] = SNR[0] - Sum[0] * Sum[0];
	SNR[1] = SNR[1] - Sum[1] * Sum[1];

	//	Margin=(int)(endy*0.2);
	//	for(int i=0+Margin;i<endy-Margin;i++){
	for (int i = 0; i<BLOCK_SIZE - 1; i++){
		Sum[2] += Cal_Y[i][2];		SNR[2] += Cal_Y[i][2] * Cal_Y[i][2];
		Sum[3] += Cal_Y[i][3];		SNR[3] += Cal_Y[i][3] * Cal_Y[i][3];
		cnt[1]++;
	}

	Sum[2] /= (double)cnt[1];		SNR[2] /= (double)cnt[1];
	Sum[3] /= (double)cnt[1];		SNR[3] /= (double)cnt[1];

	SNR[2] = SNR[2] - Sum[2] * Sum[2];
	SNR[3] = SNR[3] - Sum[3] * Sum[3];


	//double TotalSNR=(Sum[0]*Sum[1]*Sum[2]*Sum[3])/sqrt(SNR[0]*SNR[1]*SNR[2]*SNR[3]);

	double TotalSNR = (Sum[0]) / sqrt(SNR[0]);
	TotalSNR = 10 * log10l(TotalSNR)*1.2;

	if (SNR[0] == 0)
	{
		TotalSNR = 100;
	}



	double Difference_Data = (Noise[0] / Noise[1]);


	for (int i = 0; i<m_CAM_SIZE_HEIGHT; i++){
		testX[0][i] = Cal_X[i][0]; testX[1][i] = Cal_X[i][1];

	}

	for (int i = 0; i<m_CAM_SIZE_WIDTH; i++){
		testY[0][i] = Cal_Y[i][0]; testY[1][i] = Cal_Y[i][1];
	}

	if (Dark < 0){
		Dark = 0;
	}
	if (Noise[0] <0){
		Noise[0] = 0;
	}
	if (Difference_Data <0){
		Difference_Data = 0;
	}



	// 만약 기준 신호레벨은 0이라면, SNR은 1이다.(전체평균 = 평균 noise 편차)
	if (avg_origin >env_Value){

		Rect[Y*BLOCK_WIDTH + X].m_result = 0;
	}
	else{
		Rect[Y*BLOCK_WIDTH + X].m_result = TotalSNR;

		

	}


	for (i = 0; i<m_CAM_SIZE_HEIGHT; i++)free(BW[i]);   free(BW);
	for (i = 0; i<m_CAM_SIZE_HEIGHT; i++)free(Cal_X[i]);   free(Cal_X);
	for (i = 0; i<CAM_IMAGE_WIDTH; i++)free(Cal_Y[i]);   free(Cal_Y);
	for (i = 0; i<2; i++)free(testX[i]);   free(testX);
	for (i = 0; i<2; i++)free(testY[i]);   free(testY);

	return;


}

void CPatternNoise_Option::PatternNoiseGen_Block_12bit(LPWORD IN_RGB, CRectData *Rect, int X, int Y){

	m_Success = FALSE;
	//BYTE *BW;
	BYTE R, G, B;
	//BYTE abstraction;
	double total_abs = 0;
	double avg_abs = 0;

	double BrightnessValue = (double)ThrDiff;///ThrDiff 루리텍 비밀 옵션
	double env_Value = (double)ThrNoise;///ThrDiff 루리텍 비밀 옵션
	double dBThreshold = (double)ThrDark;

	int i = 0;
	WORD **BW;
	BW = (WORD **)malloc(sizeof(WORD *)*CAM_IMAGE_HEIGHT);                     //   
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)BW[i] = (WORD *)malloc(sizeof(WORD)* CAM_IMAGE_WIDTH);


	for (int t = 0; t<m_CAM_SIZE_HEIGHT; t++){
		for (int k = 0; k<m_CAM_SIZE_WIDTH; k++){
			BW[t][k] = 0;
		}
	}

	int startx = X*BLOCK_SIZE;
	int starty = Y*BLOCK_SIZE;
	int endx = startx + BLOCK_SIZE - 1;
	int endy = starty + BLOCK_SIZE - 1;


	double **Cal_X;
	Cal_X = (double **)malloc(sizeof(double *)*CAM_IMAGE_HEIGHT);                     //   
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)Cal_X[i] = (double *)malloc(sizeof(double)* 2);

	double **Cal_Y;
	Cal_Y = (double **)malloc(sizeof(double *)*CAM_IMAGE_WIDTH);                     //   
	for (i = 0; i<CAM_IMAGE_WIDTH; i++)Cal_Y[i] = (double *)malloc(sizeof(double)* 2);

	double **testX;
	testX = (double **)malloc(sizeof(double *)* 2);                     //   
	for (i = 0; i<2; i++)testX[i] = (double *)malloc(sizeof(double)* CAM_IMAGE_HEIGHT);

	double **testY;
	testY = (double **)malloc(sizeof(double *)* 2);                     //   
	for (i = 0; i<2; i++)testY[i] = (double *)malloc(sizeof(double)* CAM_IMAGE_WIDTH);

	for (int t = 0; t<m_CAM_SIZE_HEIGHT; t++){
		Cal_X[t][0] = 0;
		Cal_X[t][1] = 0;
		testX[0][t] = 0;
		testX[1][t] = 0;
		for (int k = 0; k<m_CAM_SIZE_WIDTH; k++){
			BW[t][k] = 0;
			Cal_Y[k][0] = 0;
			Cal_Y[k][1] = 0;
			testY[0][k] = 0;
			testY[1][k] = 0;

		}
	}



	/*double Cal_X[CAM_IMAGE_HEIGHT][2]={0,},testX[2][CAM_IMAGE_HEIGHT];
	double Cal_Y[CAM_IMAGE_WIDTH][2]={0,},testY[2][CAM_IMAGE_WIDTH]; */

	int count = 0, CountLine[2] = { 0, };
	total_abs = 0;
	double originData = 0;
	for (int y = starty; y<endy; y++)
	{
		Cal_X[y - starty][0] = 0;	Cal_X[y - starty][1] = 0; CountLine[0] = 0;
		for (int x = startx; x<endx; x++)
		{
			BW[y][x] = 4095 - IN_RGB[y*m_CAM_SIZE_WIDTH + x];

			total_abs += BW[y][x];
			count++;
			CountLine[0]++;
			Cal_X[y - starty][0] += (double)BW[y][x];			Cal_X[y - starty][1] += (double)BW[y][x] * (double)BW[y][x];
		}
		if (CountLine[0]>0){
			Cal_X[y - starty][0] /= (double)CountLine[0]; Cal_X[y - starty][1] /= (double)CountLine[0];
			Cal_X[y - starty][1] = Cal_X[y - starty][1] - Cal_X[y - starty][0] * Cal_X[y - starty][0];
		}
	}

	double avg_origin = 0;
	avg_origin = total_abs / (double)count;
	//avg_abs = total_abs / (double)count; // 추출된 rgb 값에 대한 평균(전체 평균)
	CountLine[1] = 0;
	for (int x = startx; x < endx; x++){
		Cal_Y[x - startx][0] = 0;		Cal_Y[x - startx][1] = 0;  CountLine[1] = 0;

		for (int y = starty; y < endy; y++){

			if (m_OverlayArea[x][y] == 1){
				Cal_Y[x - startx][0] += (double)BW[y][x];			Cal_Y[x - startx][1] += (double)BW[y][x] * (double)BW[y][x];
				CountLine[1]++;
			}
		}
		if (CountLine[1]>0){
			Cal_Y[x - startx][0] /= (double)CountLine[1]; Cal_Y[x - startx][1] /= (double)CountLine[1];
			Cal_Y[x - startx][1] = Cal_Y[x - startx][1] - Cal_Y[x - startx][0] * Cal_Y[x - startx][0];
		}
	}


	double Dark = 0, Noise[2];
	Noise[0] = Noise[1] = 0;
	for (long i = 0; i<BLOCK_SIZE - 1; i++)	Dark += Cal_X[i][0];    Dark /= (double)BLOCK_SIZE - 1;
	for (long i = 0; i<BLOCK_SIZE - 1; i++)	Noise[0] += Cal_X[i][1];    Noise[0] /= (double)BLOCK_SIZE - 1;
	for (long i = 0; i<BLOCK_SIZE - 1; i++)	Noise[1] += Cal_Y[i][1];    Noise[1] /= (double)BLOCK_SIZE - 1;

	double SNR[4] = { 0, }, Sum[4] = { 0, };
	int Margin;
	int cnt[2] = { 0, };

	//	Margin=(int)(endx*0.2);
	//	for(int i=0+Margin;i<endx-Margin;i++){
	for (int i = 0; i<BLOCK_SIZE - 1; i++){

		Sum[0] += Cal_Y[i][0];		SNR[0] += Cal_Y[i][0] * Cal_Y[i][0];
		Sum[1] += Cal_Y[i][1];		SNR[1] += Cal_Y[i][1] * Cal_Y[i][1];
		cnt[0]++;
	}

	Sum[0] /= (double)cnt[0];		SNR[0] /= (double)cnt[0];
	Sum[1] /= (double)cnt[0];		SNR[1] /= (double)cnt[0];

	SNR[0] = SNR[0] - Sum[0] * Sum[0];
	SNR[1] = SNR[1] - Sum[1] * Sum[1];

	//	Margin=(int)(endy*0.2);
	//	for(int i=0+Margin;i<endy-Margin;i++){
	for (int i = 0; i<BLOCK_SIZE - 1; i++){
		Sum[2] += Cal_Y[i][2];		SNR[2] += Cal_Y[i][2] * Cal_Y[i][2];
		Sum[3] += Cal_Y[i][3];		SNR[3] += Cal_Y[i][3] * Cal_Y[i][3];
		cnt[1]++;
	}

	Sum[2] /= (double)cnt[1];		SNR[2] /= (double)cnt[1];
	Sum[3] /= (double)cnt[1];		SNR[3] /= (double)cnt[1];

	SNR[2] = SNR[2] - Sum[2] * Sum[2];
	SNR[3] = SNR[3] - Sum[3] * Sum[3];


	//double TotalSNR=(Sum[0]*Sum[1]*Sum[2]*Sum[3])/sqrt(SNR[0]*SNR[1]*SNR[2]*SNR[3]);

	double TotalSNR = (Sum[0]) / sqrt(SNR[0]);
	TotalSNR = 10 * log10l(TotalSNR)*1.2;

	if (SNR[0] == 0)
	{
		TotalSNR = 100;
	}



	double Difference_Data = (Noise[0] / Noise[1]);


	for (int i = 0; i<m_CAM_SIZE_HEIGHT; i++){
		testX[0][i] = Cal_X[i][0]; testX[1][i] = Cal_X[i][1];

	}

	for (int i = 0; i<m_CAM_SIZE_WIDTH; i++){
		testY[0][i] = Cal_Y[i][0]; testY[1][i] = Cal_Y[i][1];
	}

	if (Dark < 0){
		Dark = 0;
	}
	if (Noise[0] <0){
		Noise[0] = 0;
	}
	if (Difference_Data <0){
		Difference_Data = 0;
	}



	// 만약 기준 신호레벨은 0이라면, SNR은 1이다.(전체평균 = 평균 noise 편차)
	if (avg_origin >env_Value){

		Rect[Y*BLOCK_WIDTH + X].m_result = 0;
	}
	else{
		Rect[Y*BLOCK_WIDTH + X].m_result = TotalSNR;



	}


	for (i = 0; i<m_CAM_SIZE_HEIGHT; i++)free(BW[i]);   free(BW);
	for (i = 0; i<m_CAM_SIZE_HEIGHT; i++)free(Cal_X[i]);   free(Cal_X);
	for (i = 0; i<CAM_IMAGE_WIDTH; i++)free(Cal_Y[i]);   free(Cal_Y);
	for (i = 0; i<2; i++)free(testX[i]);   free(testX);
	for (i = 0; i<2; i++)free(testY[i]);   free(testY);

	return;


}
void CPatternNoise_Option::SAVE_FILE_STD(CRectData *Rect, int X, int Y)
{
	CTime time = CTime::GetTickCount();
	int y = time.GetYear();
	int m = time.GetMonth();
	int d = time.GetDay();

	CTime thetime = CTime::GetCurrentTime();

	int hour = 0, minute = 0, second = 0, hour2 = 0;
	hour = thetime.GetHour(); minute = thetime.GetMinute(); second = thetime.GetSecond();
	CString strFilename;
	strFilename = ((CImageTesterDlg *)m_pMomWnd)->m_modelName;
	((CImageTesterDlg *)m_pMomWnd)->CREATE_Folder(strFilename, 5);
	CString DataSave_Path = ((CImageTesterDlg *)m_pMomWnd)->MODEL_FOLDERPATH_P;
	strFilename.Format("%s\\STD_%04d_%02d_%02d_%02dh_%02dm_%02ds.csv", DataSave_Path, y, m, d, hour, minute, second);

	CStdioFile File;
	CFileException e;
	if (!File.Open((LPCTSTR)strFilename, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
		return;

	}

	CString strOut = "";
	CString str = "";


	for (int t = 0; t < Y; t++){
		for (int i = 0; i < X; i++){
			str.Empty();
			str.Format("%0.3f,", Rect[X*t + i].m_result);
			strOut += str;
			if (i == X - 1){
				strOut += "\n";
			}
		}

	}
	File.WriteString(LPCTSTR(strOut));
	File.Close();
}
bool CPatternNoise_Option::STDGen_16bit(WORD *GRAYScanBuf, int NUM){

	double dBThreshold = (double)ThrDark;
	CvScalar MeanValue, StdValue;

	int startx = PN_RECT[NUM].m_Left;
	int starty = PN_RECT[NUM].m_Top;
	int endx = PN_RECT[NUM].m_Right+1;
	int endy = PN_RECT[NUM].m_Bottom+1;
	int ImgWidth = PN_RECT[NUM].m_Width;
	int ImgHeight = PN_RECT[NUM].m_Height;

	int data = 0;
	int mean = 0;
	unsigned int Val = 0;
	int count = 0;
	for (int y = starty; y < endy; y++)
	{
		for (int x = startx; x < endx; x++)
		{
			data += GRAYScanBuf[y*CAM_IMAGE_WIDTH + x];
		}
	}
	mean = data / (ImgWidth*ImgHeight);
	for (int y = starty; y < endy; y++)
	{
		for (int x = startx; x < endx; x++)
		{
			data = abs(mean - GRAYScanBuf[y*CAM_IMAGE_WIDTH + x]);
			Val += data*data;
		}
	}
	PN_RECT[NUM].m_result = sqrt(Val / (ImgWidth*ImgHeight))*m_dPatternNoiseOffset;



// 	IplImage *OriginImage = cvCreateImage(cvSize(ImgWidth, ImgHeight), IPL_DEPTH_16U, 1);
// 	int count_X = 0, count_Y = 0;
// 	for (int y = starty; y < endy; y++)
// 	{
// 		count_X = 0;
// 		for (int x = startx; x < endx; x++)
// 		{
// 			OriginImage->imageData[(count_Y * ImgWidth*2) + count_X] = GRAYScanBuf[y*CAM_IMAGE_WIDTH + x] >> 8;
//  			OriginImage->imageData[(count_Y * ImgWidth*2) + count_X +1] = GRAYScanBuf[y*CAM_IMAGE_WIDTH + x];
// 			count_X+=2;
// 		}
// 		count_Y++;
// 	}
// 
// 	cvAvgSdv(OriginImage, &MeanValue, &StdValue);
// 
// 	cvReleaseImage(&OriginImage);
// 
// 	int a = MeanValue.val[0];
// 	PN_RECT[NUM].m_result = StdValue.val[0];

	if (PN_RECT[NUM].m_result <= dBThreshold){
		m_Success = TRUE;
		Result.Empty();
		Result.Format("OK");
		PN_RECT[NUM].m_Success = TRUE;

	}
	else{
		m_Success = FALSE;
		Result.Empty();
		Result.Format("FAIL");
		PN_RECT[NUM].m_Success = FALSE;

	}

	return m_Success;

}
void CPatternNoise_Option::STDGen_Block_16bit(LPWORD GRAYScanBuf, CRectData *Rect, int X, int Y){

	CvScalar MeanValue, StdValue;

	int startx = X*BLOCK_SIZE;
	int starty = Y*BLOCK_SIZE;
	int endx = startx + BLOCK_SIZE;
	int endy = starty + BLOCK_SIZE;

	int data = 0;
	int mean = 0;
	int Val = 0;
	int count = 0;
	for (int y = starty; y < endy; y++)
	{
		for (int x = startx; x < endx; x++)
		{
			data += GRAYScanBuf[y*CAM_IMAGE_WIDTH + x];
			count++;
		}
	}
	mean = data / (BLOCK_SIZE*BLOCK_SIZE);
	for (int y = starty; y < endy; y++)
	{
		for (int x = startx; x < endx; x++)
		{
			data = abs(mean - GRAYScanBuf[y*CAM_IMAGE_WIDTH + x]);
			Val += data*data;
		}
	}
	Rect[Y*BLOCK_WIDTH + X].m_result = sqrt(Val / (BLOCK_SIZE*BLOCK_SIZE));


// 	IplImage *OriginImage = cvCreateImage(cvSize(BLOCK_SIZE, BLOCK_SIZE), IPL_DEPTH_16U, 1);
// 	int count_X = 0, count_Y = 0;
// 	for (int y = starty; y < endy; y++)
// 	{
// 		count_X = 0;
// 		for (int x = startx; x < endx; x++)
// 		{
// 			OriginImage->imageData[(count_Y * BLOCK_SIZE * 2) + count_X] = GRAYScanBuf[y*CAM_IMAGE_WIDTH + x] >> 8;
// 			OriginImage->imageData[(count_Y * BLOCK_SIZE * 2) + count_X + 1] = GRAYScanBuf[y*CAM_IMAGE_WIDTH + x];
// 			count_X += 2;
// 		}
// 		count_Y++;
// 	}
// 
// 	cvAvgSdv(OriginImage, &MeanValue, &StdValue);
// 	cvSaveImage("D:\\STDGen_Block_16bit.png", OriginImage);
// 	cvReleaseImage(&OriginImage);
// 
// 	Rect[Y*BLOCK_WIDTH + X].m_result = StdValue.val[0];
	return;

}

void CPatternNoise_Option::OnBnClickedChkBlockDataSave()
{
	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);

	CString str;
	str.Empty();
	str.Format("%d", b_BlockDataSave);
	WritePrivateProfileString(str_model, "BLOCK_DATA_SAVE_PN", str, str_ModelPath);

	b_BlockDataSave = GetPrivateProfileInt(str_model, "BLOCK_DATA_SAVE_PN", 1, str_ModelPath);

	UpdateData(FALSE);
}


void CPatternNoise_Option::OnBnClickedButtonMasterload()
{
	UpdateData(TRUE);
	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), _T("txt"), _T("txt"));
	CString strFileSel;
	strFileSel.Format(_T("*.%s"), _T("txt"));


	TCHAR szExePath[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, szExePath, MAX_PATH);

	TCHAR drive[_MAX_DRIVE];
	TCHAR dir[_MAX_DIR];
	TCHAR file[_MAX_FNAME];
	TCHAR ext[_MAX_EXT];
	_tsplitpath_s(szExePath, drive, _MAX_DRIVE, dir, _MAX_DIR, file, _MAX_FNAME, ext, _MAX_EXT);

	CString szProgramPath;
	szProgramPath.Format(_T("%s%s"), drive, dir);


	// 파일 불러오기
	CFileDialog fileDlg(TRUE, _T("txt"), strFileSel, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = szProgramPath;

	if (fileDlg.DoModal() == IDOK)
	{
		strFullPath = fileDlg.GetPathName();
		strFileTitle = fileDlg.GetFileTitle();

		str_MasterPath = strFullPath;
		st_PNMaster.str_MasterPath = str_MasterPath;
		WritePrivateProfileString(str_model, "MASTERPATH", str_MasterPath, PN_filename);

		Master_Data_Load();

	}
	UpdateData(FALSE);
}


BOOL CPatternNoise_Option::Master_Data_Load()
{
	st_PNMaster.Reset();

	CString str;
	str.Format(_T("%d"), st_PNMaster.nMaster_Block_X);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_X))->SetWindowText(str);
	str.Format(_T("%d"), st_PNMaster.nMaster_Block_Y);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_Y))->SetWindowText(str);
	str.Format(_T("%d"), st_PNMaster.nMaster_Detect_X);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_X2))->SetWindowText(str);
	str.Format(_T("%d"), st_PNMaster.nMaster_Detect_Y);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_Y2))->SetWindowText(str);

	CStdioFile File;

	CString filePath;  //파일 경로 저장
	CString tmp = "";        // 한줄씩 읽은 문자열 저장
	CString pb;         // 문자열에서 잘라낸 로그값 저장
	CString strFilename = "";

	CFileException e;
	TCHAR			lpszBuf[256];

	int count = 0;

	if (!File.Open((LPCTSTR)st_PNMaster.str_MasterPath, CFile::modeRead | CFile::shareDenyNone, &e)){
		//AfxMessageBox("해당 csv파일을 찾을 수 없습니다. 처음부터 작업시작합니다.");
		return FALSE;
	}
	File.SeekToBegin();

	// - Master Block Area 찾기

	// 첫번째 줄.
	File.ReadString(tmp);

	CString szData;
	int length = 0;
	CString szDataTemp;
	int space_index = 0;
	szData = tmp;

	//첫번째 줄
	while (true)
	{
		length = szData.GetLength();
		space_index = szData.Find('\t');

		if (space_index ==-1)
		{
			st_PNMaster.nMaster_Block_X = _ttoi(szData);
			break;
		}
		szData = szData.Mid(space_index+1, length - space_index);
	}
	POINT pt_Pix;
	//두번째 줄
	while (File.ReadString(szData))
	{
		length = szData.GetLength();
		space_index = szData.Find('\t');

		if (space_index != -1)
		{
		//	szDataTemp = szData.Mid(space_index + 1, length - space_index);
		//	int Tapspace_index = szDataTemp.Find('\t');

			szDataTemp = szData.Mid(0, space_index);

			st_PNMaster.nMaster_Block_Y = _ttoi(szDataTemp);

			for (int t = 0; t < st_PNMaster.nMaster_Block_X; t++)
			{
				szDataTemp = szData.Mid(space_index + 1, length - space_index);
				int Tapspace_index = szDataTemp.Find('\t');

				szDataTemp = szDataTemp.Mid(0, Tapspace_index);

				int idata = _ttoi(szDataTemp);

				space_index += (Tapspace_index+1);

				if (idata == 1)
				{
					pt_Pix.x = t;
					pt_Pix.y = st_PNMaster.nMaster_Block_Y - 1;

					st_PNMaster.pt_StartPointList.push_back(pt_Pix);
				}				
			}
		}
	}

	int nDataCnt = st_PNMaster.pt_StartPointList.size();


	if (st_PNMaster.nMaster_Block_Y < 2)
	{
		return FALSE;
	}
	if (st_PNMaster.nMaster_Block_X < 2)
	{
		return FALSE;
	}
	//한 블럭 당 Pix 수

	int nVerCnt = CAM_IMAGE_HEIGHT / (st_PNMaster.nMaster_Block_Y - 1);
	int nHorCnt = CAM_IMAGE_WIDTH / (st_PNMaster.nMaster_Block_X - 1);

	st_PNMaster.nMaster_Detect_X = nHorCnt;
	st_PNMaster.nMaster_Detect_Y= nVerCnt;
	POINT pt_PixData;
	for (int t = 0; t < nDataCnt; t++)
	{
		pt_Pix = st_PNMaster.pt_StartPointList[t];

		pt_PixData.x = (pt_Pix.x * st_PNMaster.nMaster_Detect_X) - st_PNMaster.nMaster_Detect_X / 2;
		pt_PixData.y = (pt_Pix.y * st_PNMaster.nMaster_Detect_Y) - st_PNMaster.nMaster_Detect_Y / 2;
		st_PNMaster.pt_StartPoint_PixPosList.push_back(pt_PixData);
	}

	str.Format(_T("%d"), st_PNMaster.nMaster_Block_X);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_X))->SetWindowText(str);
	str.Format(_T("%d"), st_PNMaster.nMaster_Block_Y);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_Y))->SetWindowText(str);
	str.Format(_T("%d"), st_PNMaster.nMaster_Detect_X);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_X2))->SetWindowText(str);
	str.Format(_T("%d"), st_PNMaster.nMaster_Detect_Y);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_Y2))->SetWindowText(str);
// 	while (File.ReadString(tmp))
// 	{
// 		AfxExtractSubString(str, tmp, count - 1, ',');
// 
// 		if (count > 5){	/////////////////////////////////워크리스트에 보여질 부분 Update
// 
// 			List->InsertItem(count - 6, str);
// 			for (int t = 0; t < 50; t++){
// 				AfxExtractSubString(pb, tmp, t, ',');
// 				List->SetItemText(count - 6, t, pb);
// 			}
// 		}
// 		count++;/////////////////////////////////저장되었던 csv에 토탈 라인 수..
// 
// 	}
// 	fileName.Format("D:\\%dcalib_data.ini", i + 1);
// 	StatePrintf(fileName);
// 
// 	sFile.Open(fileName, CFile::modeRead | CFile::typeText);
// 
// 	CString data[109];
// 	sFile.ReadString(data[0]);
// 
// 	for (int j = 0; j < N_Point; j++)
// 	{
// 		sFile.ReadString(data[j]);
// 
// 		CString x, y;
// 
// 		int length = data[j].GetLength();
// 		int space_index = data[j].Find('\t');
// 
// 
// 		x = data[j].Mid(0, space_index);
// 
// 		int space_index2 = data[j].Find('\t', space_index + 1);
// 		space_index2 = data[j].Find('\t', space_index2 + 1);
// 		y = data[j].Mid(space_index, space_index2 - space_index);
// 		xs[i][j][0] = atof(x);
// 		xs[i][j][1] = atof(y);
// 		xs[i][j][2] = 0.0;
// 
// 		int space_index3 = data[j].Find('\t', space_index2 + 1);
// 		space_index3 = data[j].Find('\t', space_index3 + 1);
// 
// 		int space_index4 = data[j].Find('\t', space_index3 + 1);
// 		space_index4 = data[j].Find('\t', space_index4 + 1);
// 
// 		x = data[j].Mid(space_index3, space_index4 - space_index3);
// 
// 		int space_index5 = data[j].Find('\t', space_index4 + 1);
// 		space_index5 = data[j].Find('\t', space_index5 + 1);
// 
// 		y = data[j].Mid(space_index4, space_index5 - space_index4);
// 
// 		ms[i][j][0] = atof(x);
// 		ms[i][j][1] = atof(y);
// 	}
// 	sFile.Close();
// }

	File.Close();
	return TRUE;
}


bool CPatternNoise_Option::SNRGen_16bit(WORD *GRAYScanBuf, int NUM){

	CvScalar MeanValue, StdValue;
	bool m_Success = false;
	int count = 0;

	int startx = 0;
	int starty = 0;
	int endx = 0;
	int endy = 0;
	int ImgWidth = 0;
	int ImgHeight = 0;

	int data = 0;
	int Signal;
	double Noise;
	unsigned int Val = 0;

	

	data = 0;
	Val = 0;
	ImgWidth = st_PNMaster.nMaster_Detect_X;
	ImgHeight = st_PNMaster.nMaster_Detect_Y;

	startx = st_PNMaster.pt_StartPoint_PixPosList[NUM].x;
	starty = st_PNMaster.pt_StartPoint_PixPosList[NUM].y;
	endx = st_PNMaster.pt_StartPoint_PixPosList[NUM].x + ImgWidth;
	endy = st_PNMaster.pt_StartPoint_PixPosList[NUM].y + ImgHeight;


	for (int y = starty; y < endy; y++)
	{
		for (int x = startx; x < endx; x++)
		{
			data += GRAYScanBuf[y*CAM_IMAGE_WIDTH + x];
		}
	}
	Signal = data / (ImgWidth*ImgHeight);
	for (int y = starty; y < endy; y++)
	{
		for (int x = startx; x < endx; x++)
		{
			data = abs(Signal - GRAYScanBuf[y*CAM_IMAGE_WIDTH + x]);
			Val += data*data;
		}
	}
	Noise = sqrt(Val / (ImgWidth*ImgHeight));


	st_PNMaster.v_iSignal.push_back(Signal);
	st_PNMaster.v_dNoise.push_back(Noise);
		

	if (Noise == 0)
	{
		st_PNMaster.v_dResult.push_back(0);
	}
	else{
		double data = 20 * log10l(Signal / Noise);
		st_PNMaster.v_dResult.push_back(data);
	}


	return m_Success;

}
