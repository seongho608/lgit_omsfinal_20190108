// Rotate_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Rotate_ResultView.h"


// CRotate_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CRotate_ResultView, CDialog)

CRotate_ResultView::CRotate_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CRotate_ResultView::IDD, pParent)
{

}

CRotate_ResultView::~CRotate_ResultView()
{
}

void CRotate_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CRotate_ResultView, CDialog)
	ON_WM_CTLCOLOR()
	ON_EN_SETFOCUS(IDC_EDIT_ROTATE_TXT, &CRotate_ResultView::OnEnSetfocusEditRotateTxt)
	ON_EN_SETFOCUS(IDC_EDIT_ROTATE_VALUE, &CRotate_ResultView::OnEnSetfocusEditRotateValue)
	ON_EN_SETFOCUS(IDC_EDIT_ROTATE_RESULT, &CRotate_ResultView::OnEnSetfocusEditRotateResult)
	ON_EN_SETFOCUS(IDC_EDIT_TILT_TXT, &CRotate_ResultView::OnEnSetfocusEditTiltTxt)
	ON_EN_SETFOCUS(IDC_EDIT_TILT_TXT2, &CRotate_ResultView::OnEnSetfocusEditTiltTxt2)
	ON_EN_SETFOCUS(IDC_EDIT_TILT_TXT3, &CRotate_ResultView::OnEnSetfocusEditTiltTxt3)
	ON_EN_SETFOCUS(IDC_EDIT_TILT_TXT4, &CRotate_ResultView::OnEnSetfocusEditTiltTx4)
	ON_EN_SETFOCUS(IDC_EDIT_TILT_VALUE, &CRotate_ResultView::OnEnSetfocusEditTiltValue)
	ON_EN_SETFOCUS(IDC_EDIT_TILT_VALUE2, &CRotate_ResultView::OnEnSetfocusEditTiltValue2)
	ON_EN_SETFOCUS(IDC_EDIT_TILT_VALUE3, &CRotate_ResultView::OnEnSetfocusEditTiltValue3)
	ON_EN_SETFOCUS(IDC_EDIT_TILT_VALUE4, &CRotate_ResultView::OnEnSetfocusEditTiltValue4)
END_MESSAGE_MAP()


// CRotate_ResultView 메시지 처리기입니다.
void CRotate_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CRotate_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
void CRotate_ResultView::Initstat(){
	RESULT_TEXT(0,"STAND BY");
//	RESULT_TEXT2(0,"STAND BY");
	VALUE_TEXT("");
//	VALUE_TEXT2("");

	LOCATION_TEXT("Degree");
//	LOCATION_TEXT2("Vertical");

	TILT_TEXT("LT - RT ");
	TILT_TEXT2("RT - RB ");
	TILT_TEXT3("RB - LB ");
	TILT_TEXT4("LB - LT ");
	TILT_VALUE_TEXT(0, "");
	TILT_VALUE_TEXT2(0, "");
	TILT_VALUE_TEXT3(0, "");
	TILT_VALUE_TEXT4(0, "");
}
BOOL CRotate_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_EDIT_ROTATE_TXT,&ft1,40,22);
	GetDlgItem(IDC_EDIT_ROTATE_TXT2)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_ROTATE_VALUE)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_ROTATE_VALUE2)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_ROTATE_RESULT)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_ROTATE_RESULT2)->SetFont(&ft1);

	GetDlgItem(IDC_EDIT_TILT_TXT)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_TILT_VALUE)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_TILT_TXT2)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_TILT_VALUE2)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_TILT_TXT3)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_TILT_VALUE3)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_TILT_TXT4)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_TILT_VALUE4)->SetFont(&ft1);
	
	GetDlgItem(IDC_EDIT_VALUE)->SetFont(&ft1);
	Initstat();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

HBRUSH CRotate_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_ROTATE_TXT){
			pDC->SetTextColor(RGB(255, 255, 255));

			pDC->SetBkColor(RGB(86,86,86));
	}
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_ROTATE_VALUE){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255,255,255));
	}


	if(pWnd->GetDlgCtrlID() == IDC_EDIT_ROTATE_RESULT){
		pDC->SetTextColor(txcol_TVal);
		pDC->SetBkColor(bkcol_TVal);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_ROTATE_TXT2){
			pDC->SetTextColor(RGB(255, 255, 255));

			pDC->SetBkColor(RGB(86,86,86));
	}
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_ROTATE_VALUE2){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255,255,255));
	}


	if(pWnd->GetDlgCtrlID() == IDC_EDIT_ROTATE_RESULT2){
		pDC->SetTextColor(txcol_TVal2);
		pDC->SetBkColor(bkcol_TVal2);
	}


	if (pWnd->GetDlgCtrlID() == IDC_EDIT_TILT_TXT){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}
	if (pWnd->GetDlgCtrlID() == IDC_EDIT_TILT_VALUE){
		pDC->SetTextColor(txcol_Val);
		pDC->SetBkColor(bkcol_Val);
	}

	if (pWnd->GetDlgCtrlID() == IDC_EDIT_TILT_TXT2){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}
	if (pWnd->GetDlgCtrlID() == IDC_EDIT_TILT_VALUE2){
		pDC->SetTextColor(txcol_Val2);
		pDC->SetBkColor(bkcol_Val2);
	}

	if (pWnd->GetDlgCtrlID() == IDC_EDIT_TILT_TXT3){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}
	if (pWnd->GetDlgCtrlID() == IDC_EDIT_TILT_VALUE3){
		pDC->SetTextColor(txcol_Val3);
		pDC->SetBkColor(bkcol_Val3);
	}

	if (pWnd->GetDlgCtrlID() == IDC_EDIT_TILT_TXT4){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}
	if (pWnd->GetDlgCtrlID() == IDC_EDIT_TILT_VALUE4){
		pDC->SetTextColor(txcol_Val4);
		pDC->SetBkColor(bkcol_Val4);
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}
void CRotate_ResultView::LOCATION_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_ROTATE_TXT))->SetWindowText(lpcszString);
}
void CRotate_ResultView::LOCATION_TEXT2(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_ROTATE_TXT2))->SetWindowText(lpcszString);
}
void CRotate_ResultView::VALUE_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_ROTATE_VALUE))->SetWindowText(lpcszString);
}
void CRotate_ResultView::VALUE_TEXT2(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_ROTATE_VALUE2))->SetWindowText(lpcszString);
}
void CRotate_ResultView::RESULT_TEXT(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0){//대기
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18,  69,171);
	}else if(col == 2){//실패
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_ROTATE_RESULT))->SetWindowText(lpcszString);
}
void CRotate_ResultView::RESULT_TEXT2(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0){
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(47, 157, 39);
	}else if(col == 1){
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(18, 69, 171);
	}else if(col == 2){
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal2 = RGB(0, 0, 0);
		bkcol_TVal2 = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_ROTATE_RESULT2))->SetWindowText(lpcszString);
}
BOOL CRotate_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CRotate_ResultView::OnEnSetfocusEditRotateTxt()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CRotate_ResultView::OnEnSetfocusEditRotateValue()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CRotate_ResultView::OnEnSetfocusEditRotateResult()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}


void CRotate_ResultView::OnEnSetfocusEditTiltTxt()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}


void CRotate_ResultView::OnEnSetfocusEditTiltTxt2()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}


void CRotate_ResultView::OnEnSetfocusEditTiltTxt3()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}


void CRotate_ResultView::OnEnSetfocusEditTiltTx4()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}


void CRotate_ResultView::OnEnSetfocusEditTiltValue()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}


void CRotate_ResultView::OnEnSetfocusEditTiltValue2()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}


void CRotate_ResultView::OnEnSetfocusEditTiltValue3()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}


void CRotate_ResultView::OnEnSetfocusEditTiltValue4()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}
void CRotate_ResultView::TILT_TEXT(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_TILT_TXT))->SetWindowText(lpcszString);
}
void CRotate_ResultView::TILT_TEXT2(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_TILT_TXT2))->SetWindowText(lpcszString);
}
void CRotate_ResultView::TILT_TEXT3(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_TILT_TXT3))->SetWindowText(lpcszString);
}
void CRotate_ResultView::TILT_TEXT4(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_TILT_TXT4))->SetWindowText(lpcszString);
}
void CRotate_ResultView::TILT_VALUE_TEXT(unsigned char col, LPCSTR lpcszString, ...)
{
	if (col == 0){//대기
		txcol_Val = RGB(255, 255, 255);
		bkcol_Val = RGB(47, 157, 39);
	}
	else if (col == 1){//성공
		txcol_Val = RGB(255, 255, 255);
		bkcol_Val = RGB(18, 69, 171);
	}
	else if (col == 2){//실패
		txcol_Val = RGB(255, 255, 255);
		bkcol_Val = RGB(201, 0, 0);
	}
	else if (col == 3){
		txcol_Val = RGB(0, 0, 0);
		bkcol_Val = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_TILT_VALUE))->SetWindowText(lpcszString);
}
void CRotate_ResultView::TILT_VALUE_TEXT2(unsigned char col, LPCSTR lpcszString, ...)
{
	if (col == 0){//대기
		txcol_Val2 = RGB(255, 255, 255);
		bkcol_Val2 = RGB(47, 157, 39);
	}
	else if (col == 1){//성공
		txcol_Val2 = RGB(255, 255, 255);
		bkcol_Val2 = RGB(18, 69, 171);
	}
	else if (col == 2){//실패
		txcol_Val2 = RGB(255, 255, 255);
		bkcol_Val2 = RGB(201, 0, 0);
	}
	else if (col == 3){
		txcol_Val2 = RGB(0, 0, 0);
		bkcol_Val2 = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_TILT_VALUE2))->SetWindowText(lpcszString);
}
void CRotate_ResultView::TILT_VALUE_TEXT3(unsigned char col, LPCSTR lpcszString, ...)
{
	if (col == 0){//대기
		txcol_Val3 = RGB(255, 255, 255);
		bkcol_Val3 = RGB(47, 157, 39);
	}
	else if (col == 1){//성공
		txcol_Val3 = RGB(255, 255, 255);
		bkcol_Val3 = RGB(18, 69, 171);
	}
	else if (col == 2){//실패
		txcol_Val3 = RGB(255, 255, 255);
		bkcol_Val3 = RGB(201, 0, 0);
	}
	else if (col == 3){
		txcol_Val3 = RGB(0, 0, 0);
		bkcol_Val3 = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_TILT_VALUE3))->SetWindowText(lpcszString);
}
void CRotate_ResultView::TILT_VALUE_TEXT4(unsigned char col, LPCSTR lpcszString, ...)
{
	if (col == 0){//대기
		txcol_Val4 = RGB(255, 255, 255);
		bkcol_Val4 = RGB(47, 157, 39);
	}
	else if (col == 1){//성공
		txcol_Val4 = RGB(255, 255, 255);
		bkcol_Val4 = RGB(18, 69, 171);
	}
	else if (col == 2){//실패
		txcol_Val4 = RGB(255, 255, 255);
		bkcol_Val4 = RGB(201, 0, 0);
	}
	else if (col == 3){
		txcol_Val4 = RGB(0, 0, 0);
		bkcol_Val4 = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_TILT_VALUE4))->SetWindowText(lpcszString);
}