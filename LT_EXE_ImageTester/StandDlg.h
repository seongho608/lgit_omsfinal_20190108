#pragma once
#include "resource.h"

// CStandDlg 대화 상자입니다.

class CStandDlg : public CDialog
{
	DECLARE_DYNAMIC(CStandDlg)

public:
	CStandDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CStandDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_STAND_DIALOG };
	void	Setup(CWnd* IN_pMomWnd);
	CEdit	*m_pStandEdit;
	
	CFont	font1,font2;
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);

	void	TOTAL_TEXT(LPCSTR lpcszString, ...);       
	void	PASS_TEXT(LPCSTR lpcszString, ...);       
	void	FAIL_TEXT(LPCSTR lpcszString, ...);       
	void	PERCENT_TEXT(LPCSTR lpcszString, ...);
	
	void	TOTAL_NUM(LPCSTR lpcszString, ...);
	void	PASS_NUM(LPCSTR lpcszString, ...);
	void	FAIL_NUM(LPCSTR lpcszString, ...);
	void	PERCENT_NUM(LPCSTR lpcszString, ...);
	void	CPK_TEXT(LPCSTR lpcszString, ...);
	void	CPK_X(LPCSTR lpcszString, ...);
	void	CPK_Y(LPCSTR lpcszString, ...);
private:
	CWnd		*m_pMomWnd;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnEnSetfocusTotal();
	afx_msg void OnEnSetfocusTotal2();
	afx_msg void OnEnSetfocusPass();
	afx_msg void OnEnSetfocusPass2();
	afx_msg void OnEnSetfocusFail();
	afx_msg void OnEnSetfocusFail2();
	afx_msg void OnEnSetfocusPercent();
	afx_msg void OnEnSetfocusPercent2();
	afx_msg void OnEnSetfocusEditCpk();
	afx_msg void OnEnSetfocusEditCpkX();
	afx_msg void OnEnSetfocusEditCpkY();
	afx_msg void OnEnSetfocusStatEdit();
};
