#pragma once
#include "afxwin.h"


// CMasterMonitorDlg 대화 상자입니다.

class CMasterMonitorDlg : public CDialog
{
	DECLARE_DYNAMIC(CMasterMonitorDlg)

public:
	CMasterMonitorDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMasterMonitorDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_MASTERMONITOR };

	void	Focus_move_start();
	CFont	font1,font2;
	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT);
	CComboBox	*pChannelCombo;
	CComboBox	*pDirectModeCombo;
	void	StatePrintf(LPCSTR lpcszString, ...);
	void	MoveDistance_Setup();
	void	InitSet();
	void	UpdateChartVal();
	void	UpdateCenterVal(CvPoint Data);
	int		m_MonChannel;
	void	ChartX_Txt(LPCSTR lpcszString, ...);
	void	ChartY_Txt(LPCSTR lpcszString, ...);
	void	ChartValX_Txt(LPCSTR lpcszString, ...);
	void	ChartValY_Txt(LPCSTR lpcszString, ...);
	void	Master_OffsetX_Txt(LPCSTR lpcszString, ...);
	void	Master_OffsetY_Txt(LPCSTR lpcszString, ...);
	void	Master_SettX_Txt(LPCSTR lpcszString, ...);
	void	Master_SettY_Txt(LPCSTR lpcszString, ...);

	
	void	Master_AxisX_Txt(LPCSTR lpcszString, ...);
	void	Master_AxisY_Txt(LPCSTR lpcszString, ...);

	void	Master_Writemod_Txt(LPCSTR lpcszString, ...);

	void	MasterState_Val(int ch,int col,LPCSTR lpcszString, ...);

	COLORREF Matxtcol[4];
	COLORREF MaBkcol[4];

	CvPoint CenterData;
	CvPoint Change_CenterData;
	void	OnCamDisplay(CDC *cdc);
	int		MasterOffsetX;
	int		MasterOffsetY;
	
	int		MasterStandX;
	int		MasterStandY;
	void	RUN_MODE_CHK(bool Mode);
	void	SubBmpPic(UINT nIDResource);
	void	SubBmpPic(LPCSTR lpBitmapName);
	int		ClickNUM[4];
	void	ChAutoSet(int ch);
	bool	b_TESTMOD;
	void	UpdateMaState();
	void	AllMSetInit();
	void	AutosetRunning(bool MOD);
	CvPoint centergen(double centx,double centy);
	CvPoint GetCenterPoint(LPBYTE IN_RGB);
	double GetDistance(int x1, int y1, int x2, int y2);
	CvPoint EtcPt;

	//double GetDistance(int x1, int y1, int x2, int y2);
	void	GetSideCircleOnManualMode(LPBYTE IN_RGB);
	void	GetSideCircleCoordinate(LPBYTE IN_RGB);
	void	RotateRunning(bool MOD);
	CRectData A_RECT[5];
	CRectData SC_RECT[5];
	VOID	Chartset();
	CRectData Standard_SC_RECT[5];
	double m_fVer_Degree, m_fHor_Degree;
	double MasterDegree;
	double horipointY1; 
	double horipointY2;

	double horipointX1; 
	double horipointX2;

	double vetipointX1;
	double vetipointX2;

	double vetipointY1;
	double vetipointY2;

	double Vertical_GetRotationCheck(double pt_x1, double pt_y1, double pt_x2, double pt_y2);
	double Horizontal_GetRotationCheck(double pt_x1, double pt_y1, double pt_x2, double pt_y2);
	
	void Pic(CDC *cdc);
	bool AnglePic(CDC *cdc,int NUM);
	bool b_Rotate;
	bool b_Center;
	void	Rotate_Enable(bool MOD);
	void	Center_Enable(bool MOD);
	int m_OffsetX;
	int m_OffsetY;


	void	Save_Enable(bool MOD);

	double m_Minoffset;
	double m_Maxoffset;

	double m_MinRotate;
	double m_MaxRotate;

	void	Load_parameter();
private :
	CWnd	*m_pMomWnd;	
	CEdit	*pTStat;
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CStatic m_stCenterDisplay;
	afx_msg void OnCbnSelchangeComboSelchnum();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnChartstand();
	afx_msg void OnBnClickedBtnChartset();
	afx_msg void OnBnClickedBtnAutoset();
	afx_msg void OnEnChangeEditMoffsetx();
	afx_msg void OnEnChangeEditMoffsety();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedBtnPowerOn();
	afx_msg void OnBnClickedBtnPowerOn2();
protected:
	virtual void OnCancel();
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedBtnMasterMonitorMove();
	afx_msg void OnBnClickedButtonUserExit();
//	CComboBox m_directMODE;
	afx_msg void OnBnClickedBtnSetCh1();
	afx_msg void OnBnClickedBtnSetCh2();
	afx_msg void OnBnClickedBtnSetCh3();
	afx_msg void OnBnClickedBtnSetCh4();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedBtnSetArea();
	afx_msg void OnBnClickedBtnJigdown();
	afx_msg void OnEnSetfocusStateCh1();
	//afx_msg void OnEnSetfocusStateCh2();
	afx_msg void OnEnSetfocusStateCh3();
	afx_msg void OnEnSetfocusStateCh4();
	afx_msg void OnBnClickedBtnAutoset2();
	CString str_RotateDegree;
	CString str_OffsetX;
	CString str_OffsetY;
	afx_msg void OnBnClickedButtonSave();
	CString str_Minoffset;
	CString str_Maxoffset;
	afx_msg void OnEnChangeEditMinoffset();
	afx_msg void OnEnChangeEditMaxoffset();
	afx_msg void OnBnClickedButtonSave2();
	afx_msg void OnEnChangeEditMinoffset2();
	afx_msg void OnEnChangeEditMaxoffset2();
	CString str_MinRotate;
	CString str_MaxRotate;
	CString str_RotateOffset;
	afx_msg void OnEnChangeEditDegree();
	afx_msg void OnEnSetfocusEditWritemode();
	afx_msg void OnEnSetfocusEditMaxisx();
	afx_msg void OnEnSetfocusEditMaxisy();
	afx_msg void OnEnSetfocusStateCh2();
	afx_msg void OnBnClickedBtnOverlayon();
	afx_msg void OnBnClickedBtnOverlayoff();
	CString str_msettX;
	CString str_msettY;
	afx_msg void OnEnChangeEditMsettx();
	afx_msg void OnEnChangeEditMsetty();
};
