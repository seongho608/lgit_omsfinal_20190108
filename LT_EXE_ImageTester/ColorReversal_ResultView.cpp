// ColorReversal_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "ColorReversal_ResultView.h"


// CColorReversal_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CColorReversal_ResultView, CDialog)

CColorReversal_ResultView::CColorReversal_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CColorReversal_ResultView::IDD, pParent)
{

}

CColorReversal_ResultView::~CColorReversal_ResultView()
{
}

void CColorReversal_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CColorReversal_ResultView, CDialog)
	ON_WM_CTLCOLOR()
	ON_EN_CHANGE(IDC_STATIC_MASTER, &CColorReversal_ResultView::OnEnChangeStaticMaster)
	ON_EN_SETFOCUS(IDC_STATIC_MASTER, &CColorReversal_ResultView::OnEnSetfocusStaticMaster)
	ON_EN_SETFOCUS(IDC_VALUE_MASTER, &CColorReversal_ResultView::OnEnSetfocusValueMaster)
	ON_EN_SETFOCUS(IDC_STATIC_TEST, &CColorReversal_ResultView::OnEnSetfocusStaticTest)
	ON_EN_SETFOCUS(IDC_VALUE_TEST, &CColorReversal_ResultView::OnEnSetfocusValueTest)
	ON_EN_SETFOCUS(IDC_VALUE_REVERSAL, &CColorReversal_ResultView::OnEnSetfocusValueReversal)
	ON_EN_SETFOCUS(IDC_STATIC_LEFT_T, &CColorReversal_ResultView::OnEnSetfocusStaticLeftT)
	ON_EN_SETFOCUS(IDC_VALUE_LEFT_T, &CColorReversal_ResultView::OnEnSetfocusValueLeftT)
	ON_EN_SETFOCUS(IDC_STATIC_RIGHT_T, &CColorReversal_ResultView::OnEnSetfocusStaticRightT)
	ON_EN_SETFOCUS(IDC_VALUE_RIGHT_T, &CColorReversal_ResultView::OnEnSetfocusValueRightT)
	ON_EN_SETFOCUS(IDC_STATIC_LEFT_B, &CColorReversal_ResultView::OnEnSetfocusStaticLeftB)
	ON_EN_SETFOCUS(IDC_VALUE_LEFT_B, &CColorReversal_ResultView::OnEnSetfocusValueLeftB)
	ON_EN_SETFOCUS(IDC_STATIC_RIGHT_B, &CColorReversal_ResultView::OnEnSetfocusStaticRightB)
	ON_EN_SETFOCUS(IDC_VALUE_RIGHT_B, &CColorReversal_ResultView::OnEnSetfocusValueRightB)
END_MESSAGE_MAP()


// CColorReversal_ResultView 메시지 처리기입니다.
void CColorReversal_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CColorReversal_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
BOOL CColorReversal_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();
	Font_Size_Change(IDC_STATIC_MASTER,&ft,100,20);
	GetDlgItem(IDC_VALUE_MASTER)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_TEST)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_TEST)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_LEFT_T)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_LEFT_T)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_RIGHT_T)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_RIGHT_T)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_LEFT_B)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_LEFT_B)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_RIGHT_B)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_RIGHT_B)->SetFont(&ft);

	Font_Size_Change(IDC_VALUE_REVERSAL,&ft2,100,38);

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	initstat();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CColorReversal_ResultView::initstat()
{
	MASTER_TEXT("MASTER");
	TEST_TEXT("TEST");
	LT_TEXT("L-T");
	RT_TEXT("R-T");
	LB_TEXT("L-B");
	RB_TEXT("R-B");

	MASTER_VALUE("");
	TEST_VALUE("");
	LT_VALUE("");
	RT_VALUE("");
	LB_VALUE("");
	RB_VALUE("");
	RV_VALUE(0,"STAND BY");
}

HBRUSH CColorReversal_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_MASTER){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_TEST){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_LEFT_T){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_RIGHT_T){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_LEFT_B){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_RIGHT_B){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_MASTER){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_TEST){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_LEFT_T){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_RIGHT_T){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_LEFT_B){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_RIGHT_B){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_REVERSAL){
			pDC->SetTextColor(txcol_TVal);
			pDC->SetBkColor(bkcol_TVal);
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CColorReversal_ResultView::MASTER_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_MASTER))->SetWindowText(lpcszString);
}

void CColorReversal_ResultView::TEST_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_TEST))->SetWindowText(lpcszString);
}

void CColorReversal_ResultView::LT_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_LEFT_T))->SetWindowText(lpcszString);
}

void CColorReversal_ResultView::RT_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_RIGHT_T))->SetWindowText(lpcszString);
}

void CColorReversal_ResultView::LB_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_LEFT_B))->SetWindowText(lpcszString);
}

void CColorReversal_ResultView::RB_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_RIGHT_B))->SetWindowText(lpcszString);
}
///
void CColorReversal_ResultView::MASTER_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_MASTER))->SetWindowText(lpcszString);
}

void CColorReversal_ResultView::TEST_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_TEST))->SetWindowText(lpcszString);
}

void CColorReversal_ResultView::LT_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_LEFT_T))->SetWindowText(lpcszString);
}

void CColorReversal_ResultView::RT_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_RIGHT_T))->SetWindowText(lpcszString);
}

void CColorReversal_ResultView::LB_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_LEFT_B))->SetWindowText(lpcszString);
}

void CColorReversal_ResultView::RB_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_RIGHT_B))->SetWindowText(lpcszString);
}

void CColorReversal_ResultView::RV_VALUE(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_VALUE_REVERSAL))->SetWindowText(lpcszString);
}
BOOL CColorReversal_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CColorReversal_ResultView::OnEnChangeStaticMaster()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CColorReversal_ResultView::OnEnSetfocusStaticMaster()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColorReversal_ResultView::OnEnSetfocusValueMaster()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColorReversal_ResultView::OnEnSetfocusStaticTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColorReversal_ResultView::OnEnSetfocusValueTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColorReversal_ResultView::OnEnSetfocusValueReversal()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColorReversal_ResultView::OnEnSetfocusStaticLeftT()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColorReversal_ResultView::OnEnSetfocusValueLeftT()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColorReversal_ResultView::OnEnSetfocusStaticRightT()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColorReversal_ResultView::OnEnSetfocusValueRightT()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColorReversal_ResultView::OnEnSetfocusStaticLeftB()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColorReversal_ResultView::OnEnSetfocusValueLeftB()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColorReversal_ResultView::OnEnSetfocusStaticRightB()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColorReversal_ResultView::OnEnSetfocusValueRightB()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}
