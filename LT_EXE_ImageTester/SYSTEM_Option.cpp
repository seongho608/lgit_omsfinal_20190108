// SYSTEM_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "SYSTEM_Option.h"


// CSYSTEM_Option 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSYSTEM_Option, CDialog)

CSYSTEM_Option::CSYSTEM_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CSYSTEM_Option::IDD, pParent)
	, str_Wrdata1(_T(""))
	, str_Wrdata2(_T(""))
	, str_Wrdata3(_T(""))
	, str_Wrdata4(_T(""))
	, str_Wrdata5(_T(""))
	, b_CurTimeSet(FALSE)
{
	InsertIndex=0;
	ListItemNum=0;
	b_SYSMODE = 1;
	StartCnt = 0;
	Lot_StartCnt=0;
	b_StopFail = FALSE;
	for(int lop = 0;lop<5;lop++){
		m_Etc_State[lop] = 0;
	}
}

CSYSTEM_Option::~CSYSTEM_Option()
{
}

void CSYSTEM_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_VALUE_WR1, str_Wrdata1);
	DDX_Text(pDX, IDC_EDIT_VALUE_WR2, str_Wrdata2);
	DDX_Text(pDX, IDC_EDIT_VALUE_WR3, str_Wrdata3);
	DDX_Text(pDX, IDC_EDIT_VALUE_WR4, str_Wrdata4);
	DDX_Text(pDX, IDC_EDIT_VALUE_WR5, str_Wrdata5);
	DDX_Check(pDX, IDC_CHECK_CURTIMESET, b_CurTimeSet);
	DDX_Control(pDX, IDC_LIST_SYSLIST, m_SYSList);
	DDX_Control(pDX, IDC_LIST_SYSLIST_LOT, m_Lot_SYSList);
}


BEGIN_MESSAGE_MAP(CSYSTEM_Option, CDialog)
	ON_BN_CLICKED(IDC_BTN_SYSREAD, &CSYSTEM_Option::OnBnClickedBtnSysread)
	ON_WM_CTLCOLOR()
	ON_EN_CHANGE(IDC_EDIT_VALUE_WR1, &CSYSTEM_Option::OnEnChangeEditValueWr1)
	ON_EN_CHANGE(IDC_EDIT_VALUE_WR2, &CSYSTEM_Option::OnEnChangeEditValueWr2)
	ON_EN_CHANGE(IDC_EDIT_VALUE_WR3, &CSYSTEM_Option::OnEnChangeEditValueWr3)
	ON_EN_CHANGE(IDC_EDIT_VALUE_WR4, &CSYSTEM_Option::OnEnChangeEditValueWr4)
	ON_EN_CHANGE(IDC_EDIT_VALUE_WR5, &CSYSTEM_Option::OnEnChangeEditValueWr5)
	ON_BN_CLICKED(IDC_BTN_WRSAVE, &CSYSTEM_Option::OnBnClickedBtnWrsave)
	ON_BN_CLICKED(IDC_BTN_SYSDEFAULT, &CSYSTEM_Option::OnBnClickedBtnSysdefault)
	ON_BN_CLICKED(IDC_CHECK_CURTIMESET, &CSYSTEM_Option::OnBnClickedCheckCurtimeset)
	ON_BN_CLICKED(IDC_BTN_SYSWRIE, &CSYSTEM_Option::OnBnClickedBtnSyswrie)
//	ON_CBN_SELCHANGE(IDC_COMBO_SYSTEMMODE, &CSYSTEM_Option::OnCbnSelchangeComboSystemmode)
END_MESSAGE_MAP()


// CSYSTEM_Option 메시지 처리기입니다.
BOOL CSYSTEM_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_EDIT_SYS_NAME1,&ft,100,20);
	GetDlgItem(IDC_EDIT_SYS_NAME2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_SYS_NAME3)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_SYS_NAME4)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_SYS_NAME5)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_SYS1)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_SYS2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_SYS3)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_SYS4)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_SYS5)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_SYS_DATA)->SetFont(&ft);

	GetDlgItem(IDC_EDIT_WR_NAME1)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_WR_NAME2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_WR_NAME3)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_WR_NAME4)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_WR_NAME5)->SetFont(&ft);

	GetDlgItem(IDC_EDIT_VALUE_RD1)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_RD2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_RD3)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_RD4)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_RD5)->SetFont(&ft);

	GetDlgItem(IDC_EDIT_VALUE_WR1)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_WR2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_WR3)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_WR4)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_WR5)->SetFont(&ft);

	Font_Size_Change(IDC_EDIT_SYS_MODE,&ft2,150,30);

	((CEdit *)GetDlgItem(IDC_EDIT_SYS_NAME1))->SetWindowText("품번");
	((CEdit *)GetDlgItem(IDC_EDIT_SYS_NAME2))->SetWindowText("제조일자");
	((CEdit *)GetDlgItem(IDC_EDIT_SYS_NAME3))->SetWindowText("CAN 버전 ");
	((CEdit *)GetDlgItem(IDC_EDIT_SYS_NAME4))->SetWindowText("SW 버전");
	((CEdit *)GetDlgItem(IDC_EDIT_SYS_NAME5))->SetWindowText("HW 버전");

	((CEdit *)GetDlgItem(IDC_EDIT_WR_NAME1))->SetWindowText("품번");
	((CEdit *)GetDlgItem(IDC_EDIT_WR_NAME2))->SetWindowText("제조일자");
	((CEdit *)GetDlgItem(IDC_EDIT_WR_NAME3))->SetWindowText("CAN 버전 ");
	((CEdit *)GetDlgItem(IDC_EDIT_WR_NAME4))->SetWindowText("SW 버전");
	((CEdit *)GetDlgItem(IDC_EDIT_WR_NAME5))->SetWindowText("HW 버전");

	pComboMode = (CComboBox *)GetDlgItem(IDC_COMBO_SYSTEMMODE);
	Load_parameter();
	SYS_RESULT(5,0,"");//모두 NONE표시
	SYS_STATE(0,"Stand By");
	SYS_RESULT_STATE(0,"STAND BY");
	
	Set_List(&m_SYSList);
	LOT_Set_List(&m_Lot_SYSList);

	InitEVMS();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSYSTEM_Option::LOT_InsertDataList()
{	
	CString strCnt="";

	int Index = m_Lot_SYSList.InsertItem(Lot_StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index+1);
	m_Lot_SYSList.SetItemText(Index,0,strCnt);

	int CopyIndex = m_SYSList.GetItemCount()-1;

	int count =0;
	for(int t=0; t< Lot_InsertNum;t++){
		if((t != 2)&&(t!=0)){
			m_Lot_SYSList.SetItemText(Index,t, m_SYSList.GetItemText(CopyIndex,count));
			count++;

		}else if(t==0){
			count++;
		}else{
			m_Lot_SYSList.SetItemText(Index,t,((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;
}	

void CSYSTEM_Option::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}

void CSYSTEM_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CSYSTEM_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
	strTitle.Empty();
	strTitle="SYSOPT_";
}

void CSYSTEM_Option::Pic(CDC *cdc)
{
	SystemPic(cdc);
}

bool CSYSTEM_Option::SystemPic(CDC *cdc){
	
	CPen	my_Pan,*old_pan;
	CString TEXTNAME[5] = {"  품 번  : "," 제조일자 : ","CAN 버전: "," SW 버전: "," HW 버전: "};
	CString TEXTDATA = "";
	CFont m_font;

	for(int lop = 0;lop<5;lop++){
		if(m_Etc_State[lop] == 0){
			return TRUE;
		}
	}
	
	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	m_font.CreatePointFont(200,"Arial");
	cdc->SelectObject(&m_font);
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);

	for(int lop = 0;lop<5;lop++){
		if(m_Etc_State[4-lop] == 1){
			my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
			old_pan = cdc->SelectObject(&my_Pan);
			cdc->SetTextColor(BLUE_COLOR);
			TEXTDATA = TEXTNAME[lop] + ((CImageTesterDlg *)m_pMomWnd)->str_SYS_RD[4-lop];
		}else{
			my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
			old_pan = cdc->SelectObject(&my_Pan);
			cdc->SetTextColor(RED_COLOR);
			TEXTDATA = TEXTNAME[lop] + ((CImageTesterDlg *)m_pMomWnd)->str_SYS_RD[4-lop];
		}
		cdc->TextOut(200,60+(lop*30),TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();
	}
	m_font.DeleteObject();
	return TRUE;
}

void CSYSTEM_Option::InitPrm()
{
	Load_parameter();
	UpdateData(FALSE);
}

void CSYSTEM_Option::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

void CSYSTEM_Option::Save_parameter(){
	CString str = "";
	CString stretc="";
	
	WritePrivateProfileString(str_model,NULL,"",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%d",m_INFO.ID);
	WritePrivateProfileString(str_model,"ID",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%s",m_INFO.MODE);
	WritePrivateProfileString(str_model,"MODE",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%s",m_INFO.NAME);
	WritePrivateProfileString(str_model,"NAME",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	for(int lop = 0;lop<5;lop++){
		stretc.Format("DATA%d",lop+1);
		WritePrivateProfileString(str_model,strTitle+stretc,((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[lop],((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	str.Empty();
	str.Format("%d",b_CurTimeSet);
	WritePrivateProfileString(str_model,strTitle+"CURTIME",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
}

void CSYSTEM_Option::Load_parameter(){
	CString str="";
	CString stretc = "";

	((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[4] = "95760D9900";
	((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[3] = CurrntDay();
	((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[2] = "CAN_13.05.01";
	((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[1] = "B00";
	((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[0] = "B00";

	b_CurTimeSet = GetPrivateProfileInt(str_model,strTitle+"CURTIME",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(b_CurTimeSet  == -1){
		b_CurTimeSet = 1;
		str.Empty();
		str.Format("%d",b_CurTimeSet);
		WritePrivateProfileString(str_model,strTitle+"CURTIME",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	b_SYSMODE = GetPrivateProfileInt(str_model,strTitle+"TESTMODE",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(b_SYSMODE  == -1){
		b_SYSMODE = 1;
		str.Empty();
		str.Format("%d",b_SYSMODE);
		WritePrivateProfileString(str_model,strTitle+"TESTMODE",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}
	pComboMode->SetCurSel(b_SYSMODE);

	for(int lop = 0;lop<5;lop++){
		stretc.Format("DATA%d",lop+1);
		str = GetPrivateProfileCString(str_model,strTitle+stretc,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		if(str == ""){
			WritePrivateProfileString(str_model,strTitle+stretc,((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[lop],((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		}else{
			((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[lop] = str;
		}
	}
	
	
	if(b_CurTimeSet == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[3] = CurrntDay();
		((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR2))->EnableWindow(0);
	}else{
		((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR2))->EnableWindow(1);
	}

	if(b_SYSMODE == 0){
		SYS_MODE_STATE("MFGDATA READ TEST");
		((CButton *)GetDlgItem(IDC_BTN_SYSWRIE))->SetWindowText("MFGDATA READ TEST");
	}else{
		SYS_MODE_STATE("MFGDATA WRITE/READ TEST");
		((CButton *)GetDlgItem(IDC_BTN_SYSWRIE))->SetWindowText("MFGDATA WRITE/READ TEST");
	}

	SYS_ST_VALUE_SET();
	SYS_ST_WR_SET();
}

tResultVal CSYSTEM_Option::Run()
{
	tResultVal retval = {0,};
	b_StopFail = FALSE;
	for(int lop = 0;lop<5;lop++){
		m_Etc_State[lop] = 0;
	}

	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
		InsertList();
	}

	if(b_CurTimeSet == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[3] = CurrntDay();
	}

	if(b_SYSMODE == TRUE){
		if(SYSTEM_WRITE_ALL() == FALSE){//SYS WRITE를 실패한 경우 
			for(int lop = 0;lop<5;lop++){
				m_SYSList.SetItemText(InsertIndex,4+lop,str_SYS[lop]);//INIT DTC
			}
			m_SYSList.SetItemText(InsertIndex,9,"FAIL");
			retval.ValString.Empty();
			retval.ValString.Format("WRITE FAIL");
			retval.m_Success = FALSE;
			Str_Mes[0] = "FAIL";
			Str_Mes[1] = "0";

			SYS_RESULT_STATE(2,"WRITE FAIL");
			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
				StartCnt++;
			}
			return retval;
		}
		for(int lop = 0;lop<5;lop++){
			m_SYSList.SetItemText(InsertIndex,4+lop,str_SYS[lop]);//INIT DTC
		}
		m_SYSList.SetItemText(InsertIndex,9,"PASS");
		retval.ValString.Empty();
		retval.ValString.Format("WRITE SUCCESS");
		retval.m_Success = TRUE;
		Str_Mes[0] = "PASS";
		Str_Mes[1] = "1";
		SYS_RESULT_STATE(1,"WRITE SUCCESS");
	}else{
		if(SYSTEM_READ_ALL_TEST() == FALSE){//SYS WRITE를 실패한 경우 
			for(int lop = 0;lop<5;lop++){
				m_SYSList.SetItemText(InsertIndex,4+lop,str_SYS[lop]);//INIT DTC
			}
			m_SYSList.SetItemText(InsertIndex,9,"FAIL");
			retval.ValString.Empty();
			retval.ValString.Format("TEST FAIL");
			retval.m_Success = FALSE;
			Str_Mes[0] = "FAIL";
			Str_Mes[1] = "0";
			SYS_RESULT_STATE(2,"TEST FAIL");

			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
				StartCnt++;
			}
			return retval;
		}
		for(int lop = 0;lop<5;lop++){
			m_SYSList.SetItemText(InsertIndex,4+lop,str_SYS[lop]);//INIT DTC
		}
		m_SYSList.SetItemText(InsertIndex,9,"PASS");
		retval.ValString.Empty();
		retval.ValString.Format("TEST SUCCESS");
		retval.m_Success = TRUE;
		Str_Mes[0] = "PASS";
		Str_Mes[1] = "1";
		SYS_RESULT_STATE(1,"TEST SUCCESS");
	}	

	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
		if(retval.m_Success == TRUE)
		{
		//	((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_MFGDATA,"PASS");
		}
		else
		{
		//	((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_MFGDATA,"FAIL");
		}

		StartCnt++;
	}


	return retval;
}

//---------------------------------------------------------------------WORKLIST
void CSYSTEM_Option::Set_List(CListCtrl *List){
	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"HW Ver",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"SW Ver",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"CAN Ver",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"제조일자",LVCFMT_CENTER, 80);
	List->InsertColumn(8,"품번",LVCFMT_CENTER, 80);
	List->InsertColumn(9,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(10,"비고",LVCFMT_CENTER, 80);

	ListItemNum=11;
	Copy_List(&m_SYSList,((CImageTesterDlg *)m_pMomWnd)->m_pWorkList,ListItemNum);
}

void CSYSTEM_Option::LOT_Set_List(CListCtrl *List)
{
	while(List->DeleteColumn(0));
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"HW Ver",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"SW Ver",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"CAN Ver",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"제조일자",LVCFMT_CENTER, 80);
	List->InsertColumn(8,"품번",LVCFMT_CENTER, 80);
	List->InsertColumn(9,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(10,"비고",LVCFMT_CENTER, 80);

	Lot_InsertNum=11;
}

void CSYSTEM_Option::InsertList(){
	CString strCnt;

	InsertIndex = m_SYSList.InsertItem(StartCnt,"",0);
	
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_SYSList.SetItemText(InsertIndex,0,strCnt);
	
	m_SYSList.SetItemText(InsertIndex,1,((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_SYSList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_SYSList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");


}

void CSYSTEM_Option::FAIL_UPLOAD(){
	if ((((CImageTesterDlg *)m_pMomWnd)->LotMod == 1) && (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)){
		InsertList();
		for(int t=0; t<5;t++){
			m_SYSList.SetItemText(InsertIndex,t+4,"X");
		}
		m_SYSList.SetItemText(InsertIndex,9,"FAIL");
		m_SYSList.SetItemText(InsertIndex,10,"STOP");
		//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_MFGDATA,"STOP");
		b_StopFail = TRUE;
		StartCnt++;
	}
}

void CSYSTEM_Option::EXCEL_SAVE(){
	CString Item[9]={"HW Ver","SW Ver","CAN Ver","제조일자","품번"};
	CString Data ="";
	CString str="";
	str = "***********************SYSTEM 진단***********************\n\n";
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("MFGDATA TEST",Item,5,&m_SYSList,Data);
}

void CSYSTEM_Option::LOT_EXCEL_SAVE(){
	CString Item[9]={"HW Ver","SW Ver","CAN Ver","제조일자","품번"};
	CString Data ="";
	CString str="";
	str = "***********************SYSTEM 진단***********************\n\n";
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("MFGDATA TEST",Item,5,&m_Lot_SYSList,Data);
}

bool CSYSTEM_Option::EXCEL_UPLOAD(){
	m_SYSList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("MFGDATA TEST",&m_SYSList,1,&StartCnt)==TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return FALSE;
}

bool CSYSTEM_Option::LOT_EXCEL_UPLOAD(){
	m_SYSList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("MFGDATA TEST",&m_Lot_SYSList,1,&StartCnt)==TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return FALSE;
}

CString CSYSTEM_Option::Mes_Result()
{
	CString sz;

	m_szMesResult = _T("");
	if(b_StopFail == FALSE){
	m_szMesResult.Format(_T("%s:%s"),Str_Mes[0],Str_Mes[1]);
	}else{
		m_szMesResult.Format(_T(":1"));
	}
	return m_szMesResult;
}
//-----------------------------------------------------------------------------

//-----------------------------이벤트 처리 함수--------------------------------
BOOL CSYSTEM_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		
		if (pMsg->wParam == VK_RETURN){

			return TRUE;
		}

		if(pMsg->wParam == VK_F7)
		{
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

HBRUSH CSYSTEM_Option::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_SYS_NAME1){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_SYS_NAME2){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_SYS_NAME3){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_SYS_NAME4){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_SYS_NAME5){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_WR_NAME1){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_WR_NAME2){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_WR_NAME3){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_WR_NAME4){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_WR_NAME5){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_SYS1){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[4]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_SYS2){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[3]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_SYS3){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[2]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_SYS4){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[1]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_SYS5){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[0]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_SYS_DATA){
		pDC->SetTextColor(st_col);
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_SYS_MODE){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86,86,86));
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CSYSTEM_Option::OnBnClickedBtnSysread()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Power_On();
	DoEvents(500);
	SYSTEM_READ_ALL();
}

void CSYSTEM_Option::OnBnClickedBtnSyswrie()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Power_On();
	DoEvents(500);
	if(b_SYSMODE == TRUE){
		SYSTEM_WRITE_ALL();
	}else{
		SYSTEM_READ_ALL_TEST();
	}

}

void CSYSTEM_Option::OnEnChangeEditValueWr1()//품번
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR1))->GetSel();
	str_Wrdata1.Replace(" ","");
	int len = str_Wrdata1.GetLength();
	if(len > 10){
		str_Wrdata1 = Old_str_Wrdata1;
	}else{
		Old_str_Wrdata1 = str_Wrdata1;
	}
	
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR1))->SetSel(SELCURSER,SELCURSER);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CSYSTEM_Option::OnEnChangeEditValueWr2()//제조일자
{
	UpdateData(TRUE);
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR2))->GetSel();
	str_Wrdata2.Replace(" ","");
	int len = str_Wrdata2.GetLength();
	if(len > 8){
		str_Wrdata2 = Old_str_Wrdata2;
	}else{
		Old_str_Wrdata2 = str_Wrdata2;
	}
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR2))->SetSel(SELCURSER,SELCURSER);
}


void CSYSTEM_Option::OnEnChangeEditValueWr3()//CAN 버전
{
	UpdateData(TRUE);
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR3))->GetSel();
	str_Wrdata3.Replace(" ","");
	int len = str_Wrdata3.GetLength();
	if(len > 12){
		str_Wrdata3 = Old_str_Wrdata3;
	}else{
		Old_str_Wrdata3 = str_Wrdata3;
	}
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR3))->SetSel(SELCURSER,SELCURSER);
}

void CSYSTEM_Option::OnEnChangeEditValueWr4()//SW 버전
{
	UpdateData(TRUE);
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR4))->GetSel();
	str_Wrdata4.Replace(" ","");
	int len = str_Wrdata4.GetLength();
	if(len > 3){
		str_Wrdata4 = Old_str_Wrdata4;
	}else{
		Old_str_Wrdata4 = str_Wrdata4;
	}
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR4))->SetSel(SELCURSER,SELCURSER);
}

void CSYSTEM_Option::OnEnChangeEditValueWr5()//HW 버전
{
	UpdateData(TRUE);
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR5))->GetSel();
	str_Wrdata5.Replace(" ","");
	int len = str_Wrdata5.GetLength();
	if(len > 3){
		str_Wrdata5 = Old_str_Wrdata5;
	}else{
		Old_str_Wrdata5 = str_Wrdata5;
	}
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR5))->SetSel(SELCURSER,SELCURSER);
}

void CSYSTEM_Option::OnBnClickedBtnWrsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	UpdateData(TRUE);
	((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[4] = str_Wrdata1;
	((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[3] = str_Wrdata2;
	((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[2] = str_Wrdata3;
	((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[1] = str_Wrdata4;
	((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[0] = str_Wrdata5;

	b_SYSMODE = pComboMode->GetCurSel();
	str.Empty();
	str.Format("%d",b_SYSMODE);
	WritePrivateProfileString(str_model,strTitle+"TESTMODE",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	for(int lop = 0;lop<5;lop++){
		str.Format("DATA%d",lop+1);
		WritePrivateProfileString(str_model,strTitle+str,((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[lop],((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	if(b_CurTimeSet == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[3] = CurrntDay();
		((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR2))->EnableWindow(0);
	}else{
		((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR2))->EnableWindow(1);
	}

	if(b_SYSMODE == 0){
		SYS_MODE_STATE("MFGDATA READ TEST");
		((CButton *)GetDlgItem(IDC_BTN_SYSWRIE))->SetWindowText("MFGDATA READ TEST");
	}else{
		SYS_MODE_STATE("MFGDATA WRITE/READ TEST");
		((CButton *)GetDlgItem(IDC_BTN_SYSWRIE))->SetWindowText("MFGDATA WRITE/READ TEST");
	}

	SYS_ST_VALUE_SET();
	SYS_ST_WR_SET();
}

void CSYSTEM_Option::OnBnClickedBtnSysdefault()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SYS_ST_WR_SET();
}

void CSYSTEM_Option::OnBnClickedCheckCurtimeset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";

	if(b_CurTimeSet == 0){
		b_CurTimeSet =1;
	}else{
		b_CurTimeSet =0;
	}
	UpdateData(FALSE);

	str.Empty();
	str.Format("%d",b_CurTimeSet);
	WritePrivateProfileString(str_model,strTitle+"CURTIME",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	if(b_CurTimeSet == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[3] = CurrntDay();
		((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR2))->EnableWindow(0);
	}else{
		((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR2))->EnableWindow(1);
	}
}
//-----------------------------------------------------------------------


void CSYSTEM_Option::initstat()
{
	((CEdit *)GetDlgItem(IDC_EDIT_SYS_NAME1))->SetWindowText("품번");
	((CEdit *)GetDlgItem(IDC_EDIT_SYS_NAME2))->SetWindowText("제조일자");
	((CEdit *)GetDlgItem(IDC_EDIT_SYS_NAME3))->SetWindowText("CAN 버전 ");
	((CEdit *)GetDlgItem(IDC_EDIT_SYS_NAME4))->SetWindowText("SW 버전");
	((CEdit *)GetDlgItem(IDC_EDIT_SYS_NAME5))->SetWindowText("HW 버전");

	((CEdit *)GetDlgItem(IDC_EDIT_WR_NAME1))->SetWindowText("품번");
	((CEdit *)GetDlgItem(IDC_EDIT_WR_NAME2))->SetWindowText("제조일자");
	((CEdit *)GetDlgItem(IDC_EDIT_WR_NAME3))->SetWindowText("CAN 버전 ");
	((CEdit *)GetDlgItem(IDC_EDIT_WR_NAME4))->SetWindowText("SW 버전");
	((CEdit *)GetDlgItem(IDC_EDIT_WR_NAME5))->SetWindowText("HW 버전");

	SYS_RESULT(5,0,"");//모두 NONE표시
	SYS_STATE(0,"Stand By");
	SYS_RESULT_STATE(0,"STAND BY");
}

BOOL CSYSTEM_Option::SYSTEM_READ_ALL()
{
	BOOL ret = FALSE;
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";

	if(((CImageTesterDlg *)m_pMomWnd)->DCC_SYSTEM_READ_ALL() == TRUE){
		SYS_STATE(1,"ALL READ SUCCESS");
		for(int lop = 0;lop<5;lop++){
			SYS_RESULT(lop,1,((CImageTesterDlg *)m_pMomWnd)->str_SYS_RD[lop]);
		}
		ret =  TRUE;
	}else{//실패시 
		for(int lop = 0;lop<5;lop++){
			if(((CImageTesterDlg *)m_pMomWnd)->str_SYS_RD[lop] != ""){
				SYS_RESULT(lop,1,((CImageTesterDlg *)m_pMomWnd)->str_SYS_RD[lop]);
			}else{
				SYS_RESULT(lop,2,"READ FAIL");
			}
		}
		SYS_STATE(2,"ALL READ FAIL");
		ret = FALSE;
	}
	return ret;
}



BOOL CSYSTEM_Option::SYSTEM_READ_ALL_TEST()
{
	BOOL ret = FALSE;
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	for(int lop = 0;lop<5;lop++){
		((CImageTesterDlg *)m_pMomWnd)->str_SYS_WR[lop] = ((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[lop];
	}

	BOOL ERRSTATE = FALSE;
	
	//성공시 
	if(((CImageTesterDlg *)m_pMomWnd)->DCC_SYSTEM_READ_ALL() == FALSE){
		SYS_RESULT(5,2,"READ FAIL");
		SYS_STATE(2,"DATA READ TEST FAIL");
		ret = FALSE;
		for(int lop = 0;lop<5;lop++){
			m_Etc_State[lop] = 2;
		}
		return ret;
	}

	if(ERRSTATE == TRUE){
		for(int lop = 0;lop<3;lop++){
			if(DTC_ERASE() == TRUE){
				break;
			}
		}
	}

	//READ 성공시
	ret = TRUE;
	for(int lop = 0;lop<5;lop++){
		if(((CImageTesterDlg *)m_pMomWnd)->str_SYS_RD[lop] != ((CImageTesterDlg *)m_pMomWnd)->str_SYS_WR[lop]){
			SYS_RESULT(lop,2,((CImageTesterDlg *)m_pMomWnd)->str_SYS_RD[lop]);
			m_Etc_State[lop] = 2;
			ret = FALSE;
		}else{
			SYS_RESULT(lop,1,((CImageTesterDlg *)m_pMomWnd)->str_SYS_RD[lop]);
			m_Etc_State[lop] = 1;
		}
	}

	if(ret == FALSE){
		SYS_STATE(2,"DATA READ TEST FAIL");
	}else{
		SYS_STATE(1,"DATA READ TEST SUCCESS");
	}
	return ret;
}

BOOL CSYSTEM_Option::SYSTEM_WRITE_ALL()
{
	BOOL ret = FALSE;
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	for(int lop = 0;lop<5;lop++){
		((CImageTesterDlg *)m_pMomWnd)->str_SYS_WR[lop] = ((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[lop];
	}

	BOOL ERRSTATE = FALSE;
	for(int lop = 0;lop<3;lop++){
		if(((CImageTesterDlg *)m_pMomWnd)->DCC_SYSTEM_WRITE() == FALSE){
			if(lop == 2){
				SYS_RESULT(5,2,"WRITE FAIL");
				SYS_STATE(2,"ALL WRITE FAIL");
				for(int t=0;t<5;t++){			
					m_Etc_State[t] = 2;
				}
				ret = FALSE;
				return ret;
			}
			ERRSTATE = TRUE;
			DoEvents(10);
		}else{
			break;
		}	
	}


	//성공시 
	if(((CImageTesterDlg *)m_pMomWnd)->DCC_SYSTEM_READ_ALL() == FALSE){
		SYS_RESULT(5,2,"READ FAIL");
		SYS_STATE(2,"ALL WRITE FAIL");
		ret = FALSE;
		for(int t=0;t<5;t++){			
			m_Etc_State[t] = 2;
		}
		return ret;
	}

	if(ERRSTATE == TRUE){
		/*for(int lop = 0;lop<3;lop++){
			if(DTC_ERASE() == TRUE){
				break;
			}
		}*/
	}

	//READ 성공시
	ret = TRUE;
	for(int lop = 0;lop<5;lop++){
		if(((CImageTesterDlg *)m_pMomWnd)->str_SYS_RD[lop] != ((CImageTesterDlg *)m_pMomWnd)->str_SYS_WR[lop]){
			SYS_RESULT(lop,2,((CImageTesterDlg *)m_pMomWnd)->str_SYS_RD[lop]);
			m_Etc_State[lop] = 2;
			ret = FALSE;
		}else{
			SYS_RESULT(lop,1,((CImageTesterDlg *)m_pMomWnd)->str_SYS_RD[lop]);
			m_Etc_State[lop] = 1;
		}
	}

	if(ret == FALSE){
		SYS_STATE(2,"ALL WRITE FAIL");
	}else{
		SYS_STATE(1,"WRITE SUCCESS");
	}
	return ret;
}

BOOL CSYSTEM_Option::DTC_ERASE()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//CString str = "";
	//BOOL ret = FALSE;
	//if(((CImageTesterDlg *)m_pMomWnd)->DCC_DTC_ERASE() == FALSE){
	//	return FALSE;
	//}
	//sleep(1000);
	//if(((CImageTesterDlg *)m_pMomWnd)->CIU_DTC_READ() == TRUE){
	//	if(((CImageTesterDlg *)m_pMomWnd)->m_DTCRDSTATE == 1){//DTC ERR가 없음
	//		ret = TRUE;
	//	}else{
	//		ret = FALSE;
	//	}
	//}else{//실패시 
	//	ret = FALSE;
	//}
	//return TRUE;	
	return TRUE;	
}

void CSYSTEM_Option::SYS_RESULT(int num,int col,LPCSTR lpcszString, ...)
{	
	switch(num){
		case 0:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_SYS5))->SetWindowText(lpcszString);
			break;
		case 1:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_SYS4))->SetWindowText(lpcszString);
			break;
		case 2:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_SYS3))->SetWindowText(lpcszString);
			break;
		case 3:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_SYS2))->SetWindowText(lpcszString);
			break;
		case 4:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_SYS1))->SetWindowText(lpcszString);
			break;
		default:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_SYS1))->SetWindowText(lpcszString);
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_SYS2))->SetWindowText(lpcszString);
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_SYS3))->SetWindowText(lpcszString);
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_SYS4))->SetWindowText(lpcszString);
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_SYS5))->SetWindowText(lpcszString);
	}

	if(num <=4){
		if(col == 0){
			Valcol[num] = RGB(86,86,86);
		}else if(col  ==1){
			Valcol[num] = RGB(18,69,171);
		}else{
			Valcol[num] = RGB(201,0,0);
		}
		str_SYS[num] = lpcszString;
	}else{
		for(int lop = 0;lop<5;lop++){
			if(col == 0){
				Valcol[lop] = RGB(86,86,86);
			}else if(col  ==1){
				Valcol[lop] = RGB(18,69,171);
			}else{
				Valcol[lop] = RGB(201,0,0);
			}
			str_SYS[lop] = lpcszString;
		}
	}
	if(((CImageTesterDlg *)m_pMomWnd)->m_pResSYSTEMOptWnd != NULL){
		((CImageTesterDlg *)m_pMomWnd)->m_pResSYSTEMOptWnd->SYS_RESULT(num,col,lpcszString);
	}
}

void CSYSTEM_Option::SYS_RESULT_STATE(int col,LPCSTR lpcszString, ...)
{	
	if(((CImageTesterDlg *)m_pMomWnd)->m_pResSYSTEMOptWnd != NULL){
		((CImageTesterDlg *)m_pMomWnd)->m_pResSYSTEMOptWnd->SYS_STATE(col,lpcszString);
	}
}

void CSYSTEM_Option::SYS_STATE(int col,LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_SYS_DATA))->SetWindowText(lpcszString);
	if(col == 0){
		st_col = RGB(86,86,86);
	}else if(col  ==1){
		st_col = RGB(18,69,171);
	}else{
		st_col = RGB(201,0,0);		
	}
}



void CSYSTEM_Option::SYS_ST_VALUE(int num,LPCSTR lpcszString, ...)
{	
	switch(num){
		case 0:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_RD5))->SetWindowText(lpcszString);
			break;
		case 1:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_RD4))->SetWindowText(lpcszString);
			break;
		case 2:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_RD3))->SetWindowText(lpcszString);
			break;
		case 3:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_RD2))->SetWindowText(lpcszString);
			break;
		case 4:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_RD1))->SetWindowText(lpcszString);
			break;
	}
}

void CSYSTEM_Option::SYS_ST_WR(int num,LPCSTR lpcszString, ...)
{	
	switch(num){
		case 0:
			str_Wrdata5 = lpcszString;
			Old_str_Wrdata5 = lpcszString;
			break;
		case 1:
			str_Wrdata4 = lpcszString;
			Old_str_Wrdata4 = lpcszString;
			break;
		case 2:
			str_Wrdata3 = lpcszString;
			Old_str_Wrdata3 = lpcszString;
			break;
		case 3:
			str_Wrdata2 = lpcszString;
			Old_str_Wrdata2 = lpcszString;
			break;
		case 4:
			str_Wrdata1 = lpcszString;
			Old_str_Wrdata1 = lpcszString;
			break;
	}
}

void CSYSTEM_Option::SYS_ST_VALUE_SET()
{	
	for(int lop = 0;lop<5;lop++){
		SYS_ST_VALUE(lop,((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[lop]);
	}
}

void CSYSTEM_Option::SYS_ST_WR_SET()
{	
	for(int lop = 0;lop<5;lop++){
		SYS_ST_WR(lop,((CImageTesterDlg *)m_pMomWnd)->str_FWrdata[lop]);
	}
	UpdateData(FALSE);

}

void CSYSTEM_Option::SYS_MODE_STATE(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_SYS_MODE))->SetWindowText(lpcszString);
}

//void CSYSTEM_Option::OnCbnSelchangeComboSystemmode()
//{
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//}

void CSYSTEM_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		pComboMode->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_CURTIMESET))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_SYSWRIE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_SYSDEFAULT))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_WRSAVE))->EnableWindow(0);

		((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR1))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR2))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR3))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR4))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_VALUE_WR5))->EnableWindow(0);
	}
}