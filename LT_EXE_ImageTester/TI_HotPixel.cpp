#include "stdafx.h"
#include "TI_HotPixel.h"

#define STEP_PIXEL			1	// Step Pixel수
#define SIZE_SCAN_BLOCK		1	// Block Size
#define CAM_IMAGE_WIDTH		640	
#define CAM_IMAGE_HEIGHT	480	
#define HOTPIXEL_INDX_MAX	100	

CTI_HotPixel::CTI_HotPixel()
{
}

CTI_HotPixel::~CTI_HotPixel()
{
}

//=============================================================================
// Method		: HotPixel_Test
// Access		: public  
// Returns		: void
// Parameter	: __in float fTreshold
// Parameter	: __out int & iIndexCount
// Parameter	: __out WORD * sPointX
// Parameter	: __out WORD * sPointY
// Parameter	: __out float * fConcentration
// Parameter	: WORD* wdImageBuf
// Qualifier	:
// Last Update	: 2017/12/19 - 13:28
// Desc.		:
//=============================================================================
void CTI_HotPixel::HotPixel_Test(__in float fTreshold, __out int& iIndexCount, __out WORD* sPointX, __out WORD* sPointY, __out float* fConcentration, WORD* wdImageBuf)
{
	int nStart_X = 0;
	int nStart_Y = 0;
	int nEnd_X	 = CAM_IMAGE_WIDTH;
	int nEnd_Y	 = CAM_IMAGE_HEIGHT;

	unsigned long int *iIntegral = new unsigned long int[(CAM_IMAGE_WIDTH + 1) * (CAM_IMAGE_HEIGHT + 1)];

	// 테이블 생성
	for (int y = 0; y < nEnd_Y; y++)
	{
		for (int x = 0; x < nEnd_X; x++)
		{
			if (x == 0 && y == 0)
			{
				iIntegral[0] = 0;
				iIntegral[(y + 1) * (CAM_IMAGE_WIDTH + 1) + (x + 1)] = wdImageBuf[y * CAM_IMAGE_WIDTH + x];
			}
			else
			{
				if (x == 0)
				{
					iIntegral[(y + 1) * (CAM_IMAGE_WIDTH + 1) + 1] = iIntegral[y * (CAM_IMAGE_WIDTH + 1) + 1] + wdImageBuf[y * CAM_IMAGE_WIDTH];
					iIntegral[(y + 1) * (CAM_IMAGE_WIDTH + 1)] = 0;
				}

				if (y == 0)
				{
					iIntegral[(CAM_IMAGE_WIDTH + 1) + (x + 1)] = iIntegral[(CAM_IMAGE_WIDTH + 1) + x] + wdImageBuf[x];
					iIntegral[x + 1] = 0;
				}
			}

			if (x > 0 && y > 0)
			{
				iIntegral[(y + 1) * (CAM_IMAGE_WIDTH + 1) + (x + 1)] = iIntegral[(y + 1) * (CAM_IMAGE_WIDTH + 1) + x] + iIntegral[y * (CAM_IMAGE_WIDTH + 1) + (x + 1)]
					- iIntegral[y * (CAM_IMAGE_WIDTH + 1) + x] + wdImageBuf[y * CAM_IMAGE_WIDTH + x];
			}
		}
	}

	iIntegral[1] = iIntegral[CAM_IMAGE_WIDTH + 1] = 0;

	double dbSum_Outter = 0.0, dbSum_Inner = 0.0;
	double dbMean_Inner = 0.0, dbMean_Outter = 0.0;

	int nInner	= SIZE_SCAN_BLOCK * SIZE_SCAN_BLOCK;
	int nOutter = nInner * 8;
	int nBlock	= SIZE_SCAN_BLOCK;
	bool IsNearDetect = false;
	double dbDistance = 9999.0;
	double dbValue = 0.0;
	double dbDifference;

	for (int y = nStart_Y; y < (nEnd_Y - nBlock * 3) && (iIndexCount < HOTPIXEL_INDX_MAX); y += STEP_PIXEL)
	{
		for (int x = nStart_X; (x < nEnd_X - nBlock * 3) && (iIndexCount < HOTPIXEL_INDX_MAX); x += STEP_PIXEL)
		{
			// 중앙 합
			dbSum_Inner = (double)(iIntegral[(y + nBlock * 2)*(CAM_IMAGE_WIDTH + 1) + (x + nBlock * 2)] + iIntegral[(y + nBlock)*(CAM_IMAGE_WIDTH + 1) + (x + nBlock)] - iIntegral[(y + nBlock)*(CAM_IMAGE_WIDTH + 1) + (x + nBlock * 2)] - iIntegral[(y + nBlock * 2)*(CAM_IMAGE_WIDTH + 1) + (x + nBlock)]);
			// 중앙 평균
			dbMean_Inner = dbSum_Inner / (double)nInner;

			// 외곽 합
			dbSum_Outter = (double)(iIntegral[(y + nBlock * 3)*(CAM_IMAGE_WIDTH + 1) + (x + nBlock * 3)] + iIntegral[(y)*(CAM_IMAGE_WIDTH + 1) + (x)]
				- iIntegral[(y)*(CAM_IMAGE_WIDTH + 1) + (x + nBlock * 3)] - iIntegral[(y + nBlock * 3)*(CAM_IMAGE_WIDTH + 1) + (x)] - dbSum_Inner);
			// 외곽 평균
			dbMean_Outter = dbSum_Outter / (double)nOutter;

			// 차이
			dbDifference = dbMean_Outter - dbMean_Inner;

			// Hot Pixel일 때만 한다.
			// Dead Pixel은 하지 않음
			dbDifference *= -1.0;

			// 농도 계산
			dbValue = dbDifference / dbMean_Outter * 100.0;

			if (dbValue > fTreshold && iIndexCount < HOTPIXEL_INDX_MAX)
			{
				IsNearDetect = false;

// 				for (int i = 0; i < iIndexCount; i++)
// 				{
// 					dbDistance = sqrt((double)(sPointX[i] - (x + nBlock))*(sPointX[i] - (x + nBlock)) +
// 						(double)(sPointY[i] - (y + nBlock))*(sPointY[i] - (y + nBlock)));
// 
// 					if (dbDistance < (double)nBlock * 1.5)
// 					{
// 						IsNearDetect = true;
// 						break;
// 					}
//				}

				if (IsNearDetect == false)
				{
					// 농도
					fConcentration[iIndexCount] = (float)dbValue;

					// 검색 영역의 평균 값
					sPointX[iIndexCount] = ((x + nBlock) + (x + nBlock * 2)) / 2;
					sPointY[iIndexCount] = ((y + nBlock) + (y + nBlock * 2)) / 2;

					// 갯수
					iIndexCount++;

					// HOTPIXEL_INDX_MAX 이상 된다면 더이상 찾지 않고 검색을 종료한다
					if (iIndexCount >= HOTPIXEL_INDX_MAX)
					{
						delete[] iIntegral;
						return;
					}
				}
			}
		}
	}

	delete[] iIntegral;
}