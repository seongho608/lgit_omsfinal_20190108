#pragma once


// CColorReversal_ResultView 대화 상자입니다.

class CColorReversal_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CColorReversal_ResultView)

public:
	CColorReversal_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CColorReversal_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_COLOR_REVERSAL };
	CFont ft,ft2;
	COLORREF txcol_TVal,bkcol_TVal;
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);

	void	initstat();
	void	Setup(CWnd* IN_pMomWnd);
	void	MASTER_TEXT(LPCSTR lpcszString, ...);
	void	TEST_TEXT(LPCSTR lpcszString, ...);
	void	LT_TEXT(LPCSTR lpcszString, ...);
	void	RT_TEXT(LPCSTR lpcszString, ...);
	void	LB_TEXT(LPCSTR lpcszString, ...);
	void	RB_TEXT(LPCSTR lpcszString, ...);

	void	MASTER_VALUE(LPCSTR lpcszString, ...);
	void	TEST_VALUE(LPCSTR lpcszString, ...);
	void	LT_VALUE(LPCSTR lpcszString, ...);
	void	RT_VALUE(LPCSTR lpcszString, ...);
	void	LB_VALUE(LPCSTR lpcszString, ...);
	void	RB_VALUE(LPCSTR lpcszString, ...);
	void	RV_VALUE(unsigned char col,LPCSTR lpcszString, ...);

private :
	CWnd	*m_pMomWnd;	

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnEnChangeStaticMaster();
	afx_msg void OnEnSetfocusStaticMaster();
	afx_msg void OnEnSetfocusValueMaster();
	afx_msg void OnEnSetfocusStaticTest();
	afx_msg void OnEnSetfocusValueTest();
	afx_msg void OnEnSetfocusValueReversal();
	afx_msg void OnEnSetfocusStaticLeftT();
	afx_msg void OnEnSetfocusValueLeftT();
	afx_msg void OnEnSetfocusStaticRightT();
	afx_msg void OnEnSetfocusValueRightT();
	afx_msg void OnEnSetfocusStaticLeftB();
	afx_msg void OnEnSetfocusValueLeftB();
	afx_msg void OnEnSetfocusStaticRightB();
	afx_msg void OnEnSetfocusValueRightB();
};
