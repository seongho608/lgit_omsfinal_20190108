#pragma once


// CExistLot 대화 상자입니다.

class CExistLot : public CDialog
{
	DECLARE_DYNAMIC(CExistLot)

public:
	CExistLot(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CExistLot();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_ExistLot };
	void	Setup(CWnd* IN_pMomWnd);
private:
	CWnd		*m_pMomWnd;	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
