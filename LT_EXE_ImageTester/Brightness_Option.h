#pragma once
#include "afxcmn.h"

#include "ETCUSER.H"
#include "afxwin.h"
// CBrightness_Option 대화 상자입니다.




typedef enum enBRPoint
{
	BRPoint_1st,
	BRPoint_2nd,
	BRPoint_3rd,
	BRPoint_4th,
	BRPoint_Center,
	BRPoint_MaxNum,
};


typedef struct _tag_BRMaster
{

	CString str_MasterPath;

	int nMaster_Block_X;
	int nMaster_Block_Y;

	int nMaster_Detect_X;
	int nMaster_Detect_Y;

	std::vector<POINT> pt_StartPointList[BRPoint_MaxNum];
	std::vector<POINT> pt_StartPoint_PixPosList[BRPoint_MaxNum];

	std::vector<double> v_dBrightNess[BRPoint_MaxNum];
	std::vector<double> v_dCenterGap[BRPoint_MaxNum];
	std::vector<double> v_dStandardGap[BRPoint_MaxNum]; // 0번 : Center data

	std::vector<int> v_iSignal[BRPoint_MaxNum];
	std::vector<double> v_dNoise[BRPoint_MaxNum];
	std::vector<double> v_dResult[BRPoint_MaxNum];
	std::vector<int> v_B_Result[BRPoint_MaxNum];

	POINT pt_Min;
	double dMinResult;
	BOOL bResult;

	_tag_BRMaster()
	{
	};

	void Reset()
	{

		for (int t = 0; t < BRPoint_MaxNum; t++)
		{
			pt_StartPoint_PixPosList[t].erase(pt_StartPoint_PixPosList[t].begin(), pt_StartPoint_PixPosList[t].end());
			pt_StartPointList[t].erase(pt_StartPointList[t].begin(), pt_StartPointList[t].end());
			v_dBrightNess[t].erase(v_dBrightNess[t].begin(), v_dBrightNess[t].end());
			v_dCenterGap[t].erase(v_dCenterGap[t].begin(), v_dCenterGap[t].end());
			v_B_Result[t].erase(v_B_Result[t].begin(), v_B_Result[t].end());
		}


		nMaster_Block_X = 0;
		nMaster_Block_Y = 0;
		nMaster_Detect_X = 0;
		nMaster_Detect_Y = 0;

		pt_Min.x = 0;
		pt_Min.y = 0;
		dMinResult = -1;
		bResult = 0;
	};

	/*변수 교환 함수*/
	_tag_BRMaster& operator= (_tag_BRMaster& ref)
	{
		str_MasterPath = ref.str_MasterPath;
		nMaster_Block_X = ref.nMaster_Block_X;
		nMaster_Block_Y = ref.nMaster_Block_Y;

		nMaster_Detect_X = ref.nMaster_Detect_X;
		nMaster_Detect_Y = ref.nMaster_Detect_Y;

		return *this;
	};

}ST_BRMaster, *PST_BRMaster;

class CBrightness_Option : public CDialog
{
	DECLARE_DYNAMIC(CBrightness_Option)

public:
	CBrightness_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CBrightness_Option();


	std::vector<int> v_B_Result[BRPoint_MaxNum];

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_BRIGHTNESS };

	ST_BRMaster st_BRMaster;


	CRectData C_RECT[5];
	int m_LineFailcnt[BRPoint_MaxNum];
	CRectData FULL_RECT[BLOCK_WIDTH*BLOCK_HEIGHT];
	void Rect_Set(CRectData *Rect, int Num);
	tResultVal Run(void);
	double Compute_Result(int NUM);
	void AveGen_Block(LPBYTE IN_RGB, CRectData *Rect, int X, int Y);
	void AveGen_Block_16bit(LPWORD IN_RGB, CRectData *Rect, int X, int Y);
	void SAVE_FILE_AVE(CRectData *Rect, int X, int Y);
	void Pic(CDC *cdc);
	void New_Brightness_Pic(CDC *cdc);
	void InitPrm();
	void FAIL_UPLOAD();
	BOOL	b_StopFail;
	double m_dOffset;

	
	int m_CAM_SIZE_WIDTH;
	int m_CAM_SIZE_HEIGHT;

	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
	void	StatePrintf(LPCSTR lpcszString, ...);

	void	SETLIST();
	void	SETLIST_ITEM();
	void	UploadList();
	void	Save_parameter();
	void	Load_parameter();
	void	Save(int NUM);
	void	Change_DATA();

	int PosX,PosY;
	int InIndex;
	int iSavedItem, iSavedSubitem;
	int iSavedSubitem2;
	int state;
	int EnterState;
	int NewItem,NewSubitem;
	CRect rect;

	bool MasterMod;

	CString C_filename;

	bool AvePic(CDC *cdc,int NUM);
	bool AveGen(LPBYTE IN_RGB,int NUM);
	bool AveGen_16bit(LPWORD IN_RGB, int NUM);
	bool AveGen_16bit_New(LPWORD IN_RGB, int nMode, int NUM);
	bool SNRGen_16bit(WORD *GRAYScanBuf, int nMode, int NUM);
	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void InsertList();
	int InsertIndex;
	int Lot_InsertIndex;
	int ListItemNum;
	int StartCnt;

	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();

	bool ChangeCheck;
	int ChangeItem[5];
	int changecount;
	void List_COLOR_Change();
	void	EditWheelIntSet(int nID,int Val);
	void	EditMinMaxIntSet(int nID,int* Val,int Min,int Max);
	bool Change_DATA_CHECK(bool FLAG);

	// MES 정보저장
	CString m_szMesResult;
	CString Mes_Result();

	void	InitEVMS();

#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	int Lot_StartCnt;
	int Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

#pragma endregion

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl B_DATALIST;
	CListCtrl m_BrightnessList;
	int B_Total_PosX;
	int B_Total_PosY;
	int B_Dis_Width;
	int B_Dis_Height;
	int B_Total_Width;
	int B_Total_Height;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
//	afx_msg void OnTimer(UINT_PTR nIDEvent);
	int B_Thresold;
	afx_msg void OnBnClickedButtonBrightnessSave();
	afx_msg void OnBnClickedButtonBrightnessSetzone();
	afx_msg void OnNMClickListBrightness(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListBrightness(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnNMCustomdrawListBrightness(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusEditBright();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnEnChangeEditBrightnessPosx();
	afx_msg void OnEnChangeEditBrightnessPosy();
	afx_msg void OnEnChangeEditBrightnessDisw();
	afx_msg void OnEnChangeEditBrightnessDish();
	afx_msg void OnEnChangeEditBrightnessW();
	afx_msg void OnEnChangeEditBrightnessH();
	afx_msg void OnEnChangeEditBrightThresold();
	afx_msg void OnEnChangeEditBright();
	afx_msg void OnBnClickedButtontest();
	CListCtrl m_Lot_BrightnessList;
	afx_msg void OnBnClickedButtonBrightnessLoad();
	afx_msg void OnBnClickedButtonBrightnessMaster();
	BOOL b_BlockDataSave;
	afx_msg void OnBnClickedChkBlockDataSave();
	BOOL b_MasterDev;
	afx_msg void OnBnClickedChkMastermode();
	afx_msg void OnBnClickedButtonMasterload();
	BOOL Master_Data_Load();
	CString str_MasterPath;
	CComboBox m_Cb_ImageSave;
	afx_msg void OnCbnSelendokComboImageSave();
private:
	int m_nImageSaveMode;
};

void CModel_Create(CBrightness_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO);
void CModel_Delete(CBrightness_Option **pWnd);