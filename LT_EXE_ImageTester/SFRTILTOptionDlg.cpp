// SFRTILTOptionDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "SFRTILTOptionDlg.h"

#define SFRTEST_16bit 1
#define SFRTEST_16bit_Ver2 0
#define PI				3.1415926536

// CSFRTILTOptionDlg 대화 상자입니다.
extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
extern WORD	m_GRAYScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];
extern BYTE	m_RGBIn[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
CString SFR_RectName[34] = { "C1", "C2", "C3", "C4", "S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10", "S11", "S12", "S13", "S14", "S15", "S16", "S17", "S18", "S19", "S20", "S21", "S22", "S23", "S24", "S25", "S26", "S27", "S28", "S29", "S30"};
IMPLEMENT_DYNAMIC(CSFRTILTOptionDlg, CDialog)

CSFRTILTOptionDlg::CSFRTILTOptionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSFRTILTOptionDlg::IDD, pParent)
	, b_Smooth(FALSE)
	, b_Edge_Off(FALSE)
	, str_ImageSavePath(_T(""))
	, m_bAutomation(FALSE)
{
	iSavedItem = 0;
	iSavedSubitem = 0;
	POSITIONDATA =0;
	Tilt_x = 0;
	Tilt_y = 0;
	Insertcheck=0;
	InsertIndex=0;
	StartCnt=0;
	Lot_StartCnt = 0;
	m_ETC_State = 0;
	m_bSFRmode = 0;
	b_StopFail = TRUE;
	m_nIdex = 0;

	m_dXtilt_result = 0;
	m_dY1tilt_result = 0;
	m_dY2tilt_result = 0;

	for (int i = 0; i < 3;i++)
	{
		EachTiltResult[i] = 0;
	}
	for(int i =0; i<34; i++)
	{
		SFR_DATA[i].NAME				= "";
		SFR_DATA[i].nPosX				= 0;
		SFR_DATA[i].nPosY				= 0;
		SFR_DATA[i].nWidth				= 0;
		SFR_DATA[i].nHeight			= 0;
		SFR_DATA[i].nEdgeDir				= 0;
		SFR_DATA[i].nFont				= 0;
		SFR_DATA[i].nGrouping		 = 0;
		SFR_DATA[i].dOffset				= 0;
		SFR_DATA[i].dThreshold_MIN		= 0;
		SFR_DATA[i].dThreshold_MAX		= 0;
		SFR_DATA[i].dResultData		= 0;
		SFR_DATA[i].bEnable			= FALSE;
		SFR_DATA[i].dTotalData		 = 0;
	}
}

CSFRTILTOptionDlg::~CSFRTILTOptionDlg()
{
}

void CSFRTILTOptionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_ROI, m_ROIList);
	DDX_Check(pDX, IDC_CHK_OVERSHOOT, b_Smooth);
	DDX_Control(pDX, IDC_LIST_SFR, m_SFRList);
	DDX_Control(pDX, IDC_LIST_SFR_LOT, m_Lot_SFRList);
	DDX_Check(pDX, IDC_CHECK_EDGEOFF, b_Edge_Off);
	DDX_Control(pDX, IDC_COMBO_IMAGESAVE, m_Combo_ImageSave);
	DDX_Text(pDX, IDC_EDIT_IMAGESAVE_PATH, str_ImageSavePath);
	DDX_Check(pDX, IDC_AUTOMATION_CHECK, m_bAutomation);
	DDX_Control(pDX, IDC_COMBO_IMAGE_SAVE, m_Cb_ImageSave);
	DDX_Control(pDX, IDC_COMBO_XTilt1, m_Cb_XTilt1);
	DDX_Control(pDX, IDC_COMBO_XTilt2, m_Cb_XTilt2);
	DDX_Control(pDX, IDC_COMBO_Y1Tilt1, m_Cb_Y1Tilt1);
	DDX_Control(pDX, IDC_COMBO_Y1Tilt2, m_Cb_Y1Tilt2);
	DDX_Control(pDX, IDC_COMBO_Y2Tilt1, m_Cb_Y2Tilt1);
	DDX_Control(pDX, IDC_COMBO_Y2Tilt2, m_Cb_Y2Tilt2);
	DDX_Control(pDX, IDC_LIST_GROUP, m_GroupList);
}


BEGIN_MESSAGE_MAP(CSFRTILTOptionDlg, CDialog)
	ON_WM_MOUSEWHEEL()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_ROI, &CSFRTILTOptionDlg::OnNMDblclkListRoi)
	ON_BN_CLICKED(IDC_BTN_SAVE, &CSFRTILTOptionDlg::OnBnClickedBtnSave)
	ON_BN_CLICKED(IDC_BTN_LOAD, &CSFRTILTOptionDlg::OnBnClickedBtnLoad)
	ON_EN_KILLFOCUS(IDC_EDIT_RD_MOD, &CSFRTILTOptionDlg::OnEnKillfocusEditRdMod)
	ON_CBN_KILLFOCUS(IDC_COMBO_RD_MODE, &CSFRTILTOptionDlg::OnCbnKillfocusComboRdMode)
	ON_CBN_KILLFOCUS(IDC_COMBO_FONT_MODE, &CSFRTILTOptionDlg::OnCbnKillfocusComboFontMode)
	ON_CBN_KILLFOCUS(IDC_COMBO_GROUP_MODE, &CSFRTILTOptionDlg::OnCbnKillfocusComboGroupMode)
	ON_BN_CLICKED(IDC_BTN_ETC_SAVE, &CSFRTILTOptionDlg::OnBnClickedBtnEtcSave)
	ON_BN_CLICKED(IDC_BTN_DEFAULT_SET, &CSFRTILTOptionDlg::OnBnClickedBtnDefaultSet)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_CHK_OVERSHOOT, &CSFRTILTOptionDlg::OnBnClickedChkOvershoot)
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_LIST_ROI, &CSFRTILTOptionDlg::OnNMClickListRoi)
	ON_BN_CLICKED(IDC_BTN_AUTO_CHECK, &CSFRTILTOptionDlg::OnBnClickedBtnAutoCheck)
	ON_BN_CLICKED(IDC_BUTTON_SET, &CSFRTILTOptionDlg::OnBnClickedButtonSet)
	ON_BN_CLICKED(IDC_BUTTON_SET2, &CSFRTILTOptionDlg::OnBnClickedButtonSet2)
	ON_BN_CLICKED(IDC_CHECK_EDGEOFF, &CSFRTILTOptionDlg::OnBnClickedCheckEdgeoff)
	ON_BN_CLICKED(IDC_BUTTON_test, &CSFRTILTOptionDlg::OnBnClickedButtontest)
	ON_BN_CLICKED(IDC_DELTA_SAVE, &CSFRTILTOptionDlg::OnBnClickedDeltaSave)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_OP, &CSFRTILTOptionDlg::OnBnClickedButtonSaveOp)
	ON_BN_CLICKED(IDC_BUTTON_PATHOPEN, &CSFRTILTOptionDlg::OnBnClickedButtonPathopen)
	ON_BN_CLICKED(IDC_BUTTON1, &CSFRTILTOptionDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON5, &CSFRTILTOptionDlg::OnBnClickedButton5)
	ON_CBN_SELCHANGE(IDC_COMBO_SFRMODE, &CSFRTILTOptionDlg::OnCbnSelchangeComboSfrmode)
	ON_BN_CLICKED(IDC_AUTOMATION_CHECK, &CSFRTILTOptionDlg::OnBnClickedAutomationCheck)
	ON_CBN_SELENDOK(IDC_COMBO_IMAGE_SAVE, &CSFRTILTOptionDlg::OnCbnSelendokComboImageSave)
	ON_CBN_SELENDOK(IDC_COMBO_XTilt1, &CSFRTILTOptionDlg::OnCbnSelendokComboXtilt1)
	ON_CBN_SELENDOK(IDC_COMBO_XTilt2, &CSFRTILTOptionDlg::OnCbnSelendokComboXtilt2)
	ON_CBN_SELENDOK(IDC_COMBO_Y1Tilt1, &CSFRTILTOptionDlg::OnCbnSelendokComboY1tilt1)
	ON_CBN_SELENDOK(IDC_COMBO_Y1Tilt2, &CSFRTILTOptionDlg::OnCbnSelendokComboY1tilt2)
	ON_CBN_SELENDOK(IDC_COMBO_Y2Tilt1, &CSFRTILTOptionDlg::OnCbnSelendokComboY2tilt1)
	ON_CBN_SELENDOK(IDC_COMBO_Y2Tilt2, &CSFRTILTOptionDlg::OnCbnSelendokComboY2tilt2)
	ON_BN_CLICKED(IDC_BUTTON_Tilt_Save, &CSFRTILTOptionDlg::OnBnClickedButtonTiltSave)
	ON_BN_CLICKED(IDC_BUTTON_Capture_Count_Save, &CSFRTILTOptionDlg::OnBnClickedButtonCaptureCountSave)
END_MESSAGE_MAP()


// CSFRTILTOptionDlg 메시지 처리기입니다.
void CSFRTILTOptionDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}
void CSFRTILTOptionDlg::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);;
	strTitle.Empty();
	strTitle="SFR_INIT";	

}
void CModel_Create(CSFRTILTOptionDlg **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO){
	CRect OptionRect;
	if(pTabWnd != NULL){
		((CSFRTILTOptionDlg *)*pWnd) = new CSFRTILTOptionDlg(pMomWnd);
		((CSFRTILTOptionDlg *)*pWnd)->Setup(pMomWnd,pEdit,INFO);
		((CSFRTILTOptionDlg *)*pWnd)->Create(((CSFRTILTOptionDlg *)*pWnd)->IDD,pTabWnd);//&m_CtrTab);	
		((CSFRTILTOptionDlg *)*pWnd)->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		((CSFRTILTOptionDlg *)*pWnd)->MoveWindow(15,25,OptionRect.Width(),OptionRect.Height());
	}
}

void CModel_Delete(CSFRTILTOptionDlg **pWnd){
	if(((CSFRTILTOptionDlg *)*pWnd) != NULL){
		((CSFRTILTOptionDlg *)*pWnd)->DestroyWindow();
		delete ((CSFRTILTOptionDlg *)*pWnd);
		((CSFRTILTOptionDlg *)*pWnd) = NULL;	
	}
}
void CSFRTILTOptionDlg::InitStat()
{
	//초기화 알고리즘
	//((CFINAL_TEST_LDlg *)m_pMomWnd)->b_InitStart = TRUE;
	for(int i=0; i<34; i++)
	{
		SFR_DATA[i].dResultData = 0;

	}
	Insertcheck =0;
}
BOOL CSFRTILTOptionDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN)
		{
			return TRUE;
		}

		if(pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CSFRTILTOptionDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	pSFRMode = (CComboBox *)GetDlgItem(IDC_COMBO_SFRMODE);
	Set_List();
	Set_GroupList();
	Set_List(&m_SFRList);
	LOT_Set_List(&m_Lot_SFRList);
	Load_Parameter();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
BOOL CSFRTILTOptionDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	int znum = zDelta/120;
	CWnd *focusid;
	focusid = GetFocus();

	int casenum = focusid->GetDlgCtrlID();

	CString str;

	switch(casenum)
	{
	case IDC_EDIT_RD_MOD:

		Change_Data_Val(znum);
		break;
	}

	return CDialog::OnMouseWheel(nFlags, zDelta, pt);

}
void CSFRTILTOptionDlg::Set_List()
{
	CString str_Num = "";

	m_ROIList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_CHECKBOXES);
	m_ROIList.InsertColumn(0,"구분",LVCFMT_CENTER, 55);
	m_ROIList.InsertColumn(1,"Start X",LVCFMT_CENTER, 70);
	m_ROIList.InsertColumn(2,"Start Y",LVCFMT_CENTER, 70);
	m_ROIList.InsertColumn(3,"Width",LVCFMT_CENTER, 60);
	m_ROIList.InsertColumn(4,"Height",LVCFMT_CENTER, 60);
	m_ROIList.InsertColumn(5,"Thre_MIN",LVCFMT_CENTER, 70);
	m_ROIList.InsertColumn(6,"Thre_MAX", LVCFMT_CENTER, 70);
	m_ROIList.InsertColumn(7,"Type",LVCFMT_CENTER, 60);
	
	if (((CImageTesterDlg *)m_pMomWnd)->b_SecretOption == TRUE || ((CImageTesterDlg *)m_pMomWnd)->b_SecretLgOption == TRUE)
	{
		m_ROIList.InsertColumn(8, "Offset", LVCFMT_CENTER, 70);
		m_nIdex = 9;
	}
	else
	{ 
		m_nIdex = 8;
	}
	
	SETLIST(m_bSFRmode);




	for(int i=0; i<4; i++)
	{
		str_Num.Format("%d", i+1);
		m_ROIList.InsertItem(i, "C"+str_Num);
		SFR_DATA[i].NAME =	"C"+str_Num; 
	}
	for(int i=0; i<30; i++)
	{
		str_Num.Format("%d", i+1);
		m_ROIList.InsertItem(i+4, "S"+str_Num);
		SFR_DATA[i+4].NAME =	"S"+str_Num; 
	}
}
void CSFRTILTOptionDlg::Set_GroupList()
{
	CString str_Num = "";

	m_GroupList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_CHECKBOXES);
	m_GroupList.InsertColumn(0, "구분", LVCFMT_CENTER, 70);
	m_GroupList.InsertColumn(1, "Min Spec", LVCFMT_CENTER, 100);
	m_GroupList.InsertColumn(2, "Max Spec", LVCFMT_CENTER, 100);
	
	m_GroupList.InsertItem(0, "0.1F");
	m_GroupList.InsertItem(1, "0.3F");
	m_GroupList.InsertItem(2, "0.5F");
	m_GroupList.InsertItem(3, "0.7F");
	m_GroupList.InsertItem(4, "0.5FA");
	m_GroupList.InsertItem(5, "0.7FA");
	
}

void CSFRTILTOptionDlg::SETLIST(BOOL MODE){
	if (MODE == 0){
		m_ROIList.InsertColumn(m_nIdex, "Linepair", LVCFMT_CENTER, 70);
		m_ROIList.InsertColumn(m_nIdex + 1, "Font", LVCFMT_CENTER, 60);
		m_ROIList.InsertColumn(m_nIdex + 2, "Grouping", LVCFMT_CENTER, 60);
	}
	else{
		m_ROIList.InsertColumn(m_nIdex, "MTF", LVCFMT_CENTER, 70);
		m_ROIList.InsertColumn(m_nIdex + 1, "Font", LVCFMT_CENTER, 60);
		m_ROIList.InsertColumn(m_nIdex + 2, "Grouping", LVCFMT_CENTER, 60);
	}
}

void CSFRTILTOptionDlg::InitPrm()  
{

	for(int i=0; i<34; i++)
	{
		SFR_DATA[i].dResultData = 0;
		SFR_DATA[i].m_Success =0;

	}

}
void CSFRTILTOptionDlg::Load_Parameter()
{
	int	nPos_X = 0; 
	int nPos_Y = 0; 
	int nWidth = 0; 
	int nHeight = 0; 
	int nMode = 0; 
	double dThreshold = 0; 
	double dOffset = 0; 

	BOOL	bEnable = TRUE;

	CString str_Value = "";
	CString str_Index = "";

	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;

	m_bSFRmode = GetPrivateProfileInt("SFR_DETECT", "SFR_Mode", -1, str_ModelPath);
	if (m_bSFRmode == -1)
	{
		m_bSFRmode = 2;
		str_Value.Format("%d", m_bSFRmode);
		WritePrivateProfileString("SFR_DETECT", "SFR_Mode", str_Value, str_ModelPath);
	}

	b_Smooth = GetPrivateProfileInt(str_model, "OVERSHOOT", -1, str_ModelPath);
	if (b_Smooth == -1)
	{
		b_Smooth = 1;
		str_Value.Format("%d", b_Smooth);
		WritePrivateProfileString(str_model, "OVERSHOOT", str_Value, str_ModelPath);
	}
	b_OverShoot = b_Smooth;
	b_OverShoot = 0;
	b_Edge_Off = GetPrivateProfileInt(str_model,"EDGE_OFF",-1,str_ModelPath);
	if(b_Edge_Off == -1){
		b_Edge_Off=0;
		str_Value.Format("%d", b_Edge_Off);
		WritePrivateProfileString(str_model,"EDGE_OFF",str_Value,str_ModelPath);
	}
	b_Edge_Off = 0;//엣지 off제외

//161208	((CImageTesterDlg *)m_pMomWnd)->b_Edge_Off = b_Edge_Off;

	m_Capture_Count = GetPrivateProfileInt("SFR_DETECT", "CAPTURE_COUNT", -1, str_ModelPath);
	if (m_Capture_Count == -1)
	{
		m_Capture_Count = 1;
		str_Value.Format("%d", m_Capture_Count);
		WritePrivateProfileString("SFR_DETECT", "CAPTURE_COUNT", str_Value, str_ModelPath);
	}

	// default Set
	m_DefaultSet_PosX = GetPrivateProfileInt("SFR_DETECT", "DEFAULT_POSX", -1, str_ModelPath);
	if(m_DefaultSet_PosX == -1)
	{
		m_DefaultSet_PosX = CAM_IMAGE_WIDTH/2; 
		str_Value.Format("%d", m_DefaultSet_PosX);
		WritePrivateProfileString("SFR_DETECT", "DEFAULT_POSX", str_Value, str_ModelPath);
	}

	m_DefaultSet_PosY = GetPrivateProfileInt("SFR_DETECT", "DEFAULT_POSY", -1, str_ModelPath);
	if(m_DefaultSet_PosY == -1)
	{
		m_DefaultSet_PosY =  CAM_IMAGE_HEIGHT/2; 
		str_Value.Format("%d", m_DefaultSet_PosY);
		WritePrivateProfileString("SFR_DETECT", "DEFAULT_POSY", str_Value, str_ModelPath);
	}

	m_DefaultSet_Width = GetPrivateProfileInt("SFR_DETECT", "DEFAULT_WIDTH", -1, str_ModelPath);
	if(m_DefaultSet_Width == -1)
	{
		m_DefaultSet_Width = 40; 
		str_Value.Format("%d", m_DefaultSet_Width);
		WritePrivateProfileString("SFR_DETECT", "DEFAULT_WIDTH", str_Value, str_ModelPath);
	}

	m_DefaultSet_Height = GetPrivateProfileInt("SFR_DETECT", "DEFAULT_HEIGHT", -1, str_ModelPath);
	if(m_DefaultSet_Height == -1)
	{
		m_DefaultSet_Height = 30; 
		str_Value.Format("%d", m_DefaultSet_Height);
		WritePrivateProfileString("SFR_DETECT", "DEFAULT_HEIGHT", str_Value, str_ModelPath);
	}

// 	m_DefaultSet_Threshold = GetPrivateProfileDouble("SFR_DETECT", "DEFAULT_THRESHOLD", -1, str_ModelPath);
// 	if(m_DefaultSet_Threshold == -1)
// 	{
// 		m_DefaultSet_Threshold = 0.30; 
// 		str_Value.Format("%0.2f", m_DefaultSet_Threshold);
// 		WritePrivateProfileString("SFR_DETECT", "DEFAULT_THRESHOLD", str_Value, str_ModelPath);
// 	}else if(m_DefaultSet_Threshold > 1.0){
// 		m_DefaultSet_Threshold = 0.40; 
// 		str_Value.Format("%0.2f", m_DefaultSet_Threshold);
// 		WritePrivateProfileString("SFR_DETECT", "DEFAULT_THRESHOLD", str_Value, str_ModelPath);
// 	}


	m_dPixelSize = GetPrivateProfileDouble("SFR_DETECT", "PIXEL_SIZE", -1, str_ModelPath);
	if(m_dPixelSize == -1)
	{
		m_dPixelSize = 2.8; 
		str_Value.Format("%0.2f", m_dPixelSize);
		WritePrivateProfileString("SFR_DETECT", "PIXEL_SIZE", str_Value, str_ModelPath);
	}

	m_dLinePerPixel = GetPrivateProfileDouble("SFR_DETECT", "LINEPAIL_PIXEL", -1, str_ModelPath);
	if(m_dLinePerPixel <= -1)
	{
		m_dLinePerPixel = 60.0; 
		str_Value.Format("%0.2f", m_dLinePerPixel);
		WritePrivateProfileString("SFR_DETECT", "LINEPAIL_PIXEL", str_Value, str_ModelPath);
	}
	m_dThreshold = GetPrivateProfileDouble("SFR_DETECT", "DEFAULT_THRESHOLD", -1, str_ModelPath);
	if(m_dThreshold <= -1)
	{
		m_dThreshold = 0.4; 
		str_Value.Format("%0.2f", m_dThreshold);
		WritePrivateProfileString("SFR_DETECT", "DEFAULT_THRESHOLD", str_Value, str_ModelPath);
	}

	m_dLinePerPixel_Side = GetPrivateProfileDouble("SFR_DETECT", "LINEPAIL_PIXEL_SIDE", -1, str_ModelPath);
	if(m_dLinePerPixel_Side <= -1)
	{
		m_dLinePerPixel_Side = 60.0; 
		str_Value.Format("%0.2f", m_dLinePerPixel_Side);
		WritePrivateProfileString("SFR_DETECT", "LINEPAIL_PIXEL_SIDE", str_Value, str_ModelPath);
	}
	m_dThreshold_Side = GetPrivateProfileDouble("SFR_DETECT", "DEFAULT_THRESHOLD_SIDE", -1, str_ModelPath);
	if(m_dThreshold_Side <= -1)
	{
		m_dThreshold_Side = 0.4; 
		str_Value.Format("%0.2f", m_dThreshold_Side);
		WritePrivateProfileString("SFR_DETECT", "DEFAULT_THRESHOLD_SIDE", str_Value, str_ModelPath);
	}

	delta_V_Thre = GetPrivateProfileDouble("SFR_DETECT", "V_DELTA", -1, str_ModelPath);
	if(delta_V_Thre <= -1)
	{
		delta_V_Thre = 0.3; 
		str_Value.Format("%0.2f", delta_V_Thre);
		WritePrivateProfileString("SFR_DETECT", "V_DELTA", str_Value, str_ModelPath);
	}
	delta_H_Thre = GetPrivateProfileDouble("SFR_DETECT", "H_DELTA", -1, str_ModelPath);
	if(delta_H_Thre <= -1)
	{
		delta_H_Thre = 0.3; 
		str_Value.Format("%0.2f", delta_H_Thre);
		WritePrivateProfileString("SFR_DETECT", "H_DELTA", str_Value, str_ModelPath);
	}

	m_dXtilt = GetPrivateProfileDouble("SFR_DETECT", "XtiltSpec", -1, str_ModelPath);
	if (m_dXtilt == -1)
	{
		m_dXtilt = 10;
		str_Value.Format("%0.2f", m_dXtilt);
		WritePrivateProfileString("SFR_DETECT", "XtiltSpec", str_Value, str_ModelPath);
	}

	m_dY1tilt = GetPrivateProfileDouble("SFR_DETECT", "Y1tiltSpec", -1, str_ModelPath);
	if (m_dY1tilt == -1)
	{
		m_dY1tilt = 10;
		str_Value.Format("%0.2f", m_dY1tilt);
		WritePrivateProfileString("SFR_DETECT", "Y1tiltSpec", str_Value, str_ModelPath);
	}

	m_dY2tilt = GetPrivateProfileDouble("SFR_DETECT", "Y2tiltSpec", -1, str_ModelPath);
	if (m_dY2tilt == -1)
	{
		m_dY2tilt = 10;
		str_Value.Format("%0.2f", m_dY2tilt);
		WritePrivateProfileString("SFR_DETECT", "Y2tiltSpec", str_Value, str_ModelPath);
	}


	str_ImageSavePath = GetPrivateProfileCString("SFR_DETECT","ImageSavePath",str_ModelPath);

	if (str_ImageSavePath.IsEmpty())
	{
		str_ImageSavePath = "D:\\SFRImage";
		folder_gen(str_ImageSavePath);
		WritePrivateProfileString("SFR_DETECT","ImageSavePath",str_ImageSavePath,str_ModelPath);
	}

	m_ImageSaveMode = GetPrivateProfileInt("SFR_DETECT","ImageSaveMode",-1,str_ModelPath);
	if(m_ImageSaveMode == -1){
		m_ImageSaveMode = 0;
		str_Value.Empty();
		str_Value.Format("%d",m_ImageSaveMode);
		WritePrivateProfileString("SFR_DETECT","ImageSaveMode",str_Value,str_ModelPath);	
	}

//161208	((CImageTesterDlg *)m_pMomWnd)->b_SFRCaptureMode = FALSE;
//161208	((CImageTesterDlg *)m_pMomWnd)->i_CaptureSFRMode = m_ImageSaveMode;


	m_Combo_ImageSave.SetCurSel(m_ImageSaveMode);


	int nFLAG = GetPrivateProfileInt("SFR_DETECT", "Automation_roi", -1, str_ModelPath);
	if (nFLAG == -1)
	{
		nFLAG = 1;
		str_Value.Format("%d", nFLAG);
		WritePrivateProfileString("SFR_DETECT", "Automation_roi", str_Value, str_ModelPath);
	}

	m_bAutomation = nFLAG;

	nFLAG = GetPrivateProfileInt("SFR_DETECT", "Image_SaveMode", -1, str_ModelPath);
	if (nFLAG == -1)
	{
		nFLAG = 1;
		str_Value.Format("%d", nFLAG);
		WritePrivateProfileString("SFR_DETECT", "Image_SaveMode", str_Value, str_ModelPath);
	}

	m_nImageSaveMode = nFLAG;
	m_Cb_ImageSave.SetCurSel(nFLAG);

	nFLAG = GetPrivateProfileInt("SFR_DETECT", "XTilt1", -1, str_ModelPath);
	if (nFLAG == -1)
	{
		nFLAG = 1;
		str_Value.Format("%d", nFLAG);
		WritePrivateProfileString("SFR_DETECT", "XTilt1", str_Value, str_ModelPath);
	}

	m_nXtilt1 = nFLAG;
	m_Cb_XTilt1.SetCurSel(nFLAG);

	nFLAG = GetPrivateProfileInt("SFR_DETECT", "XTilt2", -1, str_ModelPath);
	if (nFLAG == -1)
	{
		nFLAG = 1;
		str_Value.Format("%d", nFLAG);
		WritePrivateProfileString("SFR_DETECT", "XTilt2", str_Value, str_ModelPath);
	}

	m_nXtilt2 = nFLAG;
	m_Cb_XTilt2.SetCurSel(nFLAG);

	nFLAG = GetPrivateProfileInt("SFR_DETECT", "Y1Tilt1", -1, str_ModelPath);
	if (nFLAG == -1)
	{
		nFLAG = 1;
		str_Value.Format("%d", nFLAG);
		WritePrivateProfileString("SFR_DETECT", "Y1Tilt1", str_Value, str_ModelPath);
	}

	m_nY1tilt1 = nFLAG;
	m_Cb_Y1Tilt1.SetCurSel(nFLAG);

	nFLAG = GetPrivateProfileInt("SFR_DETECT", "Y1Tilt2", -1, str_ModelPath);
	if (nFLAG == -1)
	{
		nFLAG = 1;
		str_Value.Format("%d", nFLAG);
		WritePrivateProfileString("SFR_DETECT", "Y1Tilt2", str_Value, str_ModelPath);
	}

	m_nY1tilt2 = nFLAG;
	m_Cb_Y1Tilt2.SetCurSel(nFLAG);

	nFLAG = GetPrivateProfileInt("SFR_DETECT", "Y2Tilt1", -1, str_ModelPath);
	if (nFLAG == -1)
	{
		nFLAG = 1;
		str_Value.Format("%d", nFLAG);
		WritePrivateProfileString("SFR_DETECT", "Y2Tilt1", str_Value, str_ModelPath);
	}

	m_nY2tilt1 = nFLAG;
	m_Cb_Y2Tilt1.SetCurSel(nFLAG);

	nFLAG = GetPrivateProfileInt("SFR_DETECT", "Y2Tilt2", -1, str_ModelPath);
	if (nFLAG == -1)
	{
		nFLAG = 1;
		str_Value.Format("%d", nFLAG);
		WritePrivateProfileString("SFR_DETECT", "Y2Tilt2", str_Value, str_ModelPath);
	}

	m_nY2tilt2 = nFLAG;
	m_Cb_Y2Tilt2.SetCurSel(nFLAG);

	for(int i=0; i<34; i++)
	{
		str_Index.Format("%d", i);

		SFR_DATA[i].bEnable = GetPrivateProfileInt("SFR_DETECT", "SFR_RECT_"+str_Index+"_ENABLE", -1, str_ModelPath);
		if(SFR_DATA[i].bEnable <= -1)
		{
			SFR_DATA[i].bEnable = bEnable; 
			str_Value.Format("%d", SFR_DATA[i].bEnable);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_ENABLE", str_Value, str_ModelPath);
		}

		SFR_DATA[i].nPosX = GetPrivateProfileInt("SFR_DETECT", "SFR_RECT_"+str_Index+"_POS_X", -1, str_ModelPath);
		if(SFR_DATA[i].nPosX <= -1)
		{
			SFR_DATA[i].nPosX = nPos_X; 
			str_Value.Format("%d", SFR_DATA[i].nPosX);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_POS_X", str_Value, str_ModelPath);
		}

		SFR_DATA[i].nPosY = GetPrivateProfileInt("SFR_DETECT", "SFR_RECT_"+str_Index+"_POS_Y", -1, str_ModelPath);
		if(SFR_DATA[i].nPosY <= -1)
		{
			SFR_DATA[i].nPosY = nPos_Y; 
			str_Value.Format("%d", SFR_DATA[i].nPosY);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_POS_Y", str_Value, str_ModelPath);
		}

		SFR_DATA[i].nWidth = GetPrivateProfileInt("SFR_DETECT", "SFR_RECT_"+str_Index+"_WIDTH", -1, str_ModelPath);
		if(SFR_DATA[i].nWidth <= -1)
		{
			SFR_DATA[i].nWidth = nWidth; 
			str_Value.Format("%d", SFR_DATA[i].nWidth);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_WIDTH", str_Value, str_ModelPath);
		}

		SFR_DATA[i].nHeight = GetPrivateProfileInt("SFR_DETECT", "SFR_RECT_"+str_Index+"_HEIGHT", -1, str_ModelPath);
		if(SFR_DATA[i].nHeight <= -1)
		{
			SFR_DATA[i].nHeight = nHeight; 
			str_Value.Format("%d", SFR_DATA[i].nHeight);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_HEIGHT", str_Value, str_ModelPath);
		}

		SFR_DATA[i].dThreshold_MIN = GetPrivateProfileDouble("SFR_DETECT", "SFR_RECT_"+str_Index+"_THRESHOLD_MIN", -1, str_ModelPath);
		if ((SFR_DATA[i].dThreshold_MIN <= -1)/*||(SFR_DATA[i].dThreshold > 1.0)*/)
		{
			SFR_DATA[i].dThreshold_MIN = 0.4;
			str_Value.Format("%.2f", SFR_DATA[i].dThreshold_MIN);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_THRESHOLD_MIN", str_Value, str_ModelPath);
		}

		SFR_DATA[i].dThreshold_MAX = GetPrivateProfileDouble("SFR_DETECT", "SFR_RECT_" + str_Index + "_THRESHOLD_MAX", -1, str_ModelPath);
		if ((SFR_DATA[i].dThreshold_MAX <= -1)/*||(SFR_DATA[i].dThreshold > 1.0)*/)
		{
			SFR_DATA[i].dThreshold_MAX = 0.4;
			str_Value.Format("%.2f", SFR_DATA[i].dThreshold_MAX);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_" + str_Index + "_THRESHOLD_MAX", str_Value, str_ModelPath);
		}

		SFR_DATA[i].nEdgeDir = GetPrivateProfileInt("SFR_DETECT", "SFR_RECT_"+str_Index+"_TYPE", -1, str_ModelPath);
		if(SFR_DATA[i].nEdgeDir <= -1)
		{
			SFR_DATA[i].nEdgeDir = nMode; 
			str_Value.Format("%d", SFR_DATA[i].nEdgeDir);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_TYPE", str_Value, str_ModelPath);
		}

		SFR_DATA[i].nFont = GetPrivateProfileInt("SFR_DETECT", "SFR_RECT_" + str_Index + "_FONT", -1, str_ModelPath);
		if (SFR_DATA[i].nFont <= -1)
		{
			SFR_DATA[i].nFont = nMode;
			str_Value.Format("%d", SFR_DATA[i].nFont);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_" + str_Index + "_FONT", str_Value, str_ModelPath);
		}

		SFR_DATA[i].dOffset = GetPrivateProfileDouble("SFR_DETECT", "SFR_RECT_"+str_Index+"_OFFSET", -1, str_ModelPath);
		if(SFR_DATA[i].dOffset <= -1)
		{
			SFR_DATA[i].dOffset = dOffset; 
			str_Value.Format("%.f", SFR_DATA[i].dOffset);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_OFFSET", str_Value, str_ModelPath);
		}

		SFR_DATA[i].nGrouping = GetPrivateProfileInt("SFR_DETECT", "SFR_RECT_" + str_Index + "_GROUPING", -1, str_ModelPath);
		if (SFR_DATA[i].nGrouping <= -1)
		{
			SFR_DATA[i].nGrouping = nMode;
			str_Value.Format("%d", SFR_DATA[i].nGrouping);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_" + str_Index + "_GROUPING", str_Value, str_ModelPath);
		}

		SFR_DATA[i].dLinepare = GetPrivateProfileDouble("SFR_DETECT", "SFR_RECT_"+str_Index+"_LINEPARE", -1, str_ModelPath);
		if(SFR_DATA[i].dLinepare <= -1)
		{
			SFR_DATA[i].dLinepare = 60; 
			str_Value.Format("%0.2f", SFR_DATA[i].dLinepare);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_LINEPARE", str_Value, str_ModelPath);
		}

		SFR_DATA[i].MTF = GetPrivateProfileInt("SFR_DETECT", "SFR_RECT_" + str_Index + "_MTF", -1, str_ModelPath);
		if (SFR_DATA[i].MTF <= -1)
		{
			SFR_DATA[i].MTF = 50;
			str_Value.Format("%d", SFR_DATA[i].MTF);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_" + str_Index + "_MTF", str_Value, str_ModelPath);
		}

		INIT_SFR_DATA[i].nPosX = SFR_DATA[i].nPosX;
		INIT_SFR_DATA[i].nPosY = SFR_DATA[i].nPosY;
		INIT_SFR_DATA[i].nWidth = SFR_DATA[i].nWidth;
		INIT_SFR_DATA[i].nHeight = SFR_DATA[i].nHeight;
	}
	if(SFR_DATA[0].nWidth==0 ){

		for(int i=24; i<34; i++)
		{
			SFR_DATA[i].bEnable = 0; 
			str_Value.Format("%d", SFR_DATA[i].bEnable);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_ENABLE", str_Value, str_ModelPath);
			SFR_DATA[i].nWidth = 30;
			str_Value.Format("%d", SFR_DATA[i].nWidth);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_WIDTH", str_Value, str_ModelPath);
			SFR_DATA[i].nHeight = 40;
			str_Value.Format("%d", SFR_DATA[i].nHeight);
			WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_HEIGHT", str_Value, str_ModelPath);

		}
	}
	pSFRMode->SetCurSel(m_bSFRmode);
	CHANGESELLIST(m_bSFRmode);
	Init_Dlg();

	if(SFR_DATA[0].nWidth==0 ){
		OnBnClickedBtnDefaultSet();
	}
}
void CSFRTILTOptionDlg::Init_Dlg()
{
	CString str_Buf = "";

	str_Buf.Format("%0.2f", m_dPixelSize);
	((CEdit *)GetDlgItem(IDC_EDIT_PIXEL_SIZE))->SetWindowText(str_Buf);

	str_Buf.Format("%0.2f", m_dXtilt);
	((CEdit *)GetDlgItem(IDC_EDIT_XTilt_upper_limit))->SetWindowText(str_Buf);

	str_Buf.Format("%0.2f", m_dY1tilt);
	((CEdit *)GetDlgItem(IDC_EDIT_Y1_Tilt_upper_limit))->SetWindowText(str_Buf);
	
	str_Buf.Format("%0.2f", m_dY2tilt);
	((CEdit *)GetDlgItem(IDC_EDIT_Y2_Tilt_upper_limit))->SetWindowText(str_Buf);

	str_Buf.Format("%d", m_DefaultSet_PosX);
	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_POSX))->SetWindowText(str_Buf);

	str_Buf.Format("%d", m_Capture_Count);
	((CEdit *)GetDlgItem(IDC_EDIT_CAPTURE_COUNT))->SetWindowText(str_Buf);

	str_Buf.Format("%d", m_DefaultSet_PosY);
	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_POSY))->SetWindowText(str_Buf);

	str_Buf.Format("%d", m_DefaultSet_Width);
	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_WIDTH))->SetWindowText(str_Buf);

	str_Buf.Format("%d", m_DefaultSet_Height);
	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_HEIGHT))->SetWindowText(str_Buf);


	str_Buf.Format("%0.2f", m_dLinePerPixel);
	((CEdit *)GetDlgItem(IDC_EDIT_LINEPAIR))->SetWindowText(str_Buf);
	str_Buf.Format("%.f", m_dThreshold);
	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_THRESHOLD))->SetWindowText(str_Buf);

	str_Buf.Format("%0.2f", m_dLinePerPixel_Side);
	((CEdit *)GetDlgItem(IDC_EDIT_LINEPAIR2))->SetWindowText(str_Buf);
	str_Buf.Format("%0.2f", m_dThreshold_Side);
	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_THRESHOLD2))->SetWindowText(str_Buf);

	str_Buf.Format("%0.2f", delta_V_Thre);
	((CEdit *)GetDlgItem(IDC_EDIT_VDELTA))->SetWindowText(str_Buf);
	str_Buf.Format("%0.2f", delta_H_Thre);
	((CEdit *)GetDlgItem(IDC_EDIT_HDELTA))->SetWindowText(str_Buf);



	((CButton *)GetDlgItem(IDC_CHK_OVERSHOOT))->SetCheck(b_OverShoot);

	Upload_List();

}
void CSFRTILTOptionDlg::Upload_List()
{
	CString str_Value = "";

	for(int i=0; i<34; i++)
	{
		ListView_SetCheckState(m_ROIList, i, SFR_DATA[i].bEnable);

		str_Value.Format("%d", SFR_DATA[i].nPosX);
		m_ROIList.SetItemText(i, 1, str_Value);

		str_Value.Format("%d", SFR_DATA[i].nPosY);
		m_ROIList.SetItemText(i, 2, str_Value);

		str_Value.Format("%d", SFR_DATA[i].nWidth);
		m_ROIList.SetItemText(i, 3, str_Value);

		str_Value.Format("%d", SFR_DATA[i].nHeight);
		m_ROIList.SetItemText(i, 4, str_Value);

		str_Value.Format("%.2f", SFR_DATA[i].dThreshold_MIN);
		m_ROIList.SetItemText(i, 5, str_Value);

		str_Value.Format("%.2f", SFR_DATA[i].dThreshold_MAX);
		m_ROIList.SetItemText(i, 6, str_Value);

		if(SFR_DATA[i].nEdgeDir == 0)
			str_Value = "◁▶";
		if(SFR_DATA[i].nEdgeDir == 1)
			str_Value = "◀▷";
		if(SFR_DATA[i].nEdgeDir == 2)
			str_Value = "△▼";
		if(SFR_DATA[i].nEdgeDir == 3)
			str_Value = "▲▽";

		m_ROIList.SetItemText(i, 7, str_Value);

		if (((CImageTesterDlg *)m_pMomWnd)->b_SecretOption == TRUE || ((CImageTesterDlg *)m_pMomWnd)->b_SecretLgOption == TRUE)
		{
			str_Value.Format("%.f", SFR_DATA[i].dOffset);
			m_ROIList.SetItemText(i, 8, str_Value);
		}

		if (m_bSFRmode == 0){
			str_Value.Format("%0.2f", SFR_DATA[i].dLinepare);
			m_ROIList.SetItemText(i, m_nIdex, str_Value);
		}
		else{
			str_Value.Format("%d", SFR_DATA[i].MTF);
			m_ROIList.SetItemText(i, m_nIdex, str_Value);
		}

		if (SFR_DATA[i].nFont == 0)
			str_Value = "◁";
		if (SFR_DATA[i].nFont == 1)
			str_Value = "▷";
		if (SFR_DATA[i].nFont == 2)
			str_Value = "△";
		if (SFR_DATA[i].nFont == 3)
			str_Value = "▽";

		m_ROIList.SetItemText(i, m_nIdex + 1, str_Value);
		// SFR Grouping List [1/8/2019 Seongho.Lee]
		if (SFR_DATA[i].nGrouping == 0)
			str_Value = "0.1F";
		if (SFR_DATA[i].nGrouping == 1)
			str_Value = "0.3F";
		if (SFR_DATA[i].nGrouping == 2)
			str_Value = "0.5F(TL)";
		if (SFR_DATA[i].nGrouping == 3)
			str_Value = "0.5F(TR)";
		if (SFR_DATA[i].nGrouping == 4)
			str_Value = "0.5F(BL)";
		if (SFR_DATA[i].nGrouping == 5)
			str_Value = "0.5F(BR)";
		if (SFR_DATA[i].nGrouping == 6)
			str_Value = "0.7F(TL)";
		if (SFR_DATA[i].nGrouping == 7)
			str_Value = "0.7F(TR)";
		if (SFR_DATA[i].nGrouping == 8)
			str_Value = "0.7F(BL)";
		if (SFR_DATA[i].nGrouping == 9)
			str_Value = "0.7F(BR)";

		m_ROIList.SetItemText(i, m_nIdex + 2, str_Value);
// 		for (int ngroupcnt=0; ngroupcnt < SFR_GROUP_MAX; ngroupcnt++)
// 		{
// 			if (SFR_DATA[i].nGrouping == ngroupcnt)
// 				str_Value = g_szSFR_Grouping[ngroupcnt];
// 			m_ROIList.SetItemText(i, m_nIdex + 2, str_Value);
// 		}
	}

	UpdateData(FALSE);
}
void CSFRTILTOptionDlg::Save_Parameter()
{
	CString str_Value, str_Index;
	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;

	m_bSFRmode = pSFRMode->GetCurSel();
	str_Value.Format("%d", m_bSFRmode);
	WritePrivateProfileString("SFR_DETECT", "SFR_Mode", str_Value, str_ModelPath);

	for(int i=0; i<34; i++)
	{
		str_Index.Format("%d", i);

		str_Value.Format("%d", SFR_DATA[i].bEnable);
		WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_ENABLE", str_Value, str_ModelPath);

		str_Value.Format("%d", SFR_DATA[i].nPosX);
		WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_POS_X", str_Value, str_ModelPath);

		str_Value.Format("%d", SFR_DATA[i].nPosY);
		WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_POS_Y", str_Value, str_ModelPath);

		str_Value.Format("%d", SFR_DATA[i].nWidth);
		WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_WIDTH", str_Value, str_ModelPath);

		str_Value.Format("%d", SFR_DATA[i].nHeight);
		WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_HEIGHT", str_Value, str_ModelPath);

		str_Value.Format("%0.2f", SFR_DATA[i].dThreshold_MIN);
		WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_THRESHOLD_MIN", str_Value, str_ModelPath);

		str_Value.Format("%0.2f", SFR_DATA[i].dThreshold_MAX);
		WritePrivateProfileString("SFR_DETECT", "SFR_RECT_" + str_Index + "_THRESHOLD_MAX", str_Value, str_ModelPath);

		str_Value.Format("%d", SFR_DATA[i].nEdgeDir);
		WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_TYPE", str_Value, str_ModelPath);

		str_Value.Format("%d", SFR_DATA[i].nFont);
		WritePrivateProfileString("SFR_DETECT", "SFR_RECT_" + str_Index + "_FONT", str_Value, str_ModelPath);

		str_Value.Format("%0.2f", SFR_DATA[i].dOffset);
		WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_OFFSET", str_Value, str_ModelPath);

		str_Value.Format("%d", SFR_DATA[i].nGrouping);
		WritePrivateProfileString("SFR_DETECT", "SFR_RECT_" + str_Index + "_GROUPING", str_Value, str_ModelPath);

		str_Value.Format("%0.2f", SFR_DATA[i].dLinepare);
		WritePrivateProfileString("SFR_DETECT", "SFR_RECT_"+str_Index+"_LINEPARE", str_Value, str_ModelPath);

		str_Value.Format("%d", SFR_DATA[i].MTF);
		WritePrivateProfileString("SFR_DETECT", "SFR_RECT_" + str_Index + "_MTF", str_Value, str_ModelPath);
	}
}

void CSFRTILTOptionDlg::OnNMDblclkListRoi(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	iSavedItem = pNMItemActivate->iItem;
	iSavedSubitem = pNMItemActivate->iSubItem;

	if((pNMItemActivate->iItem != -1)&&(pNMItemActivate->iSubItem < 11))
	{
		if(pNMItemActivate->iSubItem != 0)
		{
			CRect rect;
			m_ROIList.GetSubItemRect(pNMItemActivate->iItem, pNMItemActivate->iSubItem, LVIR_BOUNDS, rect);
			m_ROIList.ClientToScreen(rect);
			this->ScreenToClient(rect);

			if(pNMItemActivate->iSubItem == 7)
			{
				((CComboBox *)GetDlgItem(IDC_COMBO_RD_MODE))->SetCurSel(SFR_DATA[iSavedItem].nEdgeDir);
				((CComboBox *)GetDlgItem(IDC_COMBO_RD_MODE))->SetWindowPos(NULL, rect.left+1, rect.top-3, (rect.right+1) - (rect.left+1), (rect.bottom-3) - (rect.top-3), SWP_SHOWWINDOW );
				((CComboBox *)GetDlgItem(IDC_COMBO_RD_MODE))->SetFocus();
			}
			else if (pNMItemActivate->iSubItem == m_nIdex + 1)
			{
				((CComboBox *)GetDlgItem(IDC_COMBO_FONT_MODE))->SetCurSel(SFR_DATA[iSavedItem].nFont);
				((CComboBox *)GetDlgItem(IDC_COMBO_FONT_MODE))->SetWindowPos(NULL, rect.left + 1, rect.top - 3, (rect.right + 1) - (rect.left + 1), (rect.bottom - 3) - (rect.top - 3), SWP_SHOWWINDOW);
				((CComboBox *)GetDlgItem(IDC_COMBO_FONT_MODE))->SetFocus();
			}
			else if (pNMItemActivate->iSubItem == m_nIdex + 2)
			{
				((CComboBox *)GetDlgItem(IDC_COMBO_GROUP_MODE))->SetCurSel(SFR_DATA[iSavedItem].nGrouping);
				((CComboBox *)GetDlgItem(IDC_COMBO_GROUP_MODE))->SetWindowPos(NULL, rect.left + 1, rect.top - 3, (rect.right + 1) - (rect.left + 1), (rect.bottom - 3) - (rect.top - 3), SWP_SHOWWINDOW);
				((CComboBox *)GetDlgItem(IDC_COMBO_GROUP_MODE))->SetFocus();
			}
			else
			{
				CString str_Out = m_ROIList.GetItemText(pNMItemActivate->iItem, pNMItemActivate->iSubItem);
				((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Out);
				((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
				((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetFocus();
			}
		}else{
			SetTimer(160,5,NULL); //InitProgram을 불러들이는데 쓰인다.
		}
	}
	*pResult = 0;
}

void CSFRTILTOptionDlg::OnBnClickedBtnSave()
{
	for(int i=0; i<34; i++)
	{
		SFR_DATA[i].bEnable = ListView_GetCheckState(m_ROIList.m_hWnd, i);
		SFR_DATA[i].nPosX = atoi(m_ROIList.GetItemText(i, 1));
		SFR_DATA[i].nPosY = atoi(m_ROIList.GetItemText(i, 2));
		SFR_DATA[i].nWidth = atoi(m_ROIList.GetItemText(i, 3));
		SFR_DATA[i].nHeight = atoi(m_ROIList.GetItemText(i, 4));
		SFR_DATA[i].dThreshold_MIN = atof(m_ROIList.GetItemText(i, 5));
		SFR_DATA[i].dThreshold_MAX = atof(m_ROIList.GetItemText(i, 6));

		if(m_ROIList.GetItemText(i, 7) == "◁▶")
			SFR_DATA[i].nEdgeDir = 0;
		if(m_ROIList.GetItemText(i, 7) == "◀▷")
			SFR_DATA[i].nEdgeDir = 1;
		if(m_ROIList.GetItemText(i, 7) == "△▼")
			SFR_DATA[i].nEdgeDir = 2;
		if(m_ROIList.GetItemText(i, 7) == "▲▽")
			SFR_DATA[i].nEdgeDir = 3;

		if (((CImageTesterDlg *)m_pMomWnd)->b_SecretOption == TRUE || ((CImageTesterDlg *)m_pMomWnd)->b_SecretLgOption == TRUE)
		{
			SFR_DATA[i].dOffset = atof(m_ROIList.GetItemText(i, 8));
		}

		if (m_bSFRmode == 0){
			SFR_DATA[i].dLinepare = atof(m_ROIList.GetItemText(i, m_nIdex));
		}
		else{
			SFR_DATA[i].MTF = atoi(m_ROIList.GetItemText(i, m_nIdex));
		}

		if (m_ROIList.GetItemText(i, m_nIdex + 1) == "◁")
			SFR_DATA[i].nFont = 0;
		if (m_ROIList.GetItemText(i, m_nIdex + 1) == "▷")
			SFR_DATA[i].nFont = 1;
		if (m_ROIList.GetItemText(i, m_nIdex + 1) == "△")
			SFR_DATA[i].nFont = 2;
		if (m_ROIList.GetItemText(i, m_nIdex + 1) == "▽")
			SFR_DATA[i].nFont = 3;

// Grouping [1/8/2019 Seongho.Lee]
		if (m_ROIList.GetItemText(i, m_nIdex + 2) == "0.1F ")
			SFR_DATA[i].nGrouping = 0;
		if (m_ROIList.GetItemText(i, m_nIdex + 2) == "0.3F")
			SFR_DATA[i].nGrouping = 1;
		if (m_ROIList.GetItemText(i, m_nIdex + 2) == "0.5F(TL)")
			SFR_DATA[i].nGrouping = 2;
		if (m_ROIList.GetItemText(i, m_nIdex + 2) == "0.5F(TR)")
			SFR_DATA[i].nGrouping = 3;
		if (m_ROIList.GetItemText(i, m_nIdex + 2) == "0.5F(BL)")
			SFR_DATA[i].nGrouping = 4;
		if (m_ROIList.GetItemText(i, m_nIdex + 2) == "0.5F(BR)")
			SFR_DATA[i].nGrouping = 5;
		if (m_ROIList.GetItemText(i, m_nIdex + 2) == "0.7F(TL)")
			SFR_DATA[i].nGrouping = 6;
		if (m_ROIList.GetItemText(i, m_nIdex + 2) == "0.7F(TR)")
			SFR_DATA[i].nGrouping = 7;
		if (m_ROIList.GetItemText(i, m_nIdex + 2) == "0.7F(BL)")
			SFR_DATA[i].nGrouping = 8;
		if (m_ROIList.GetItemText(i, m_nIdex + 2) == "0.7F(BR)")
			SFR_DATA[i].nGrouping = 9;
// 		for (int nGroupCnt = 0; nGroupCnt < SFR_GROUP_MAX; nGroupCnt++)
// 		{
// 			if (m_ROIList.GetItemText(i, m_nIdex + 1) == g_szSFR_Grouping[nGroupCnt])
// 				SFR_DATA[i].nGrouping = nGroupCnt;
// 		}

	}

	Save_Parameter();
	Load_Parameter();
}

void CSFRTILTOptionDlg::OnBnClickedBtnLoad()
{
	Load_Parameter();
}

void CSFRTILTOptionDlg::OnEnKillfocusEditRdMod()
{
	CString str;

	((CEdit *)GetDlgItemText(IDC_EDIT_RD_MOD, str));

	Change_Data(str);

	((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );

	iSavedItem = -1;
	iSavedSubitem = -1;
}
void CSFRTILTOptionDlg::Change_Data(CString str_Value)
{
	int nValue = 0;

	if(iSavedSubitem == 1)
	{
		nValue = atoi(str_Value);

		if(nValue < 0)
		{
			nValue = 0;
			SFR_DATA[iSavedItem].nPosX = nValue;
		}
		else if(nValue > CAM_IMAGE_WIDTH)
		{
			nValue = CAM_IMAGE_WIDTH;
			SFR_DATA[iSavedItem].nPosX = nValue;
		}
		else
		{
			SFR_DATA[iSavedItem].nPosX = nValue;
		}

		str_Value.Format("%d", nValue);
		m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
		((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
	}

	if(iSavedSubitem == 2)
	{
		nValue = atoi(str_Value);

		if(nValue < 0)
		{
			nValue = 0;
			SFR_DATA[iSavedItem].nPosY = nValue;
		}
		else if(nValue > CAM_IMAGE_HEIGHT)
		{
			nValue = CAM_IMAGE_HEIGHT;
			SFR_DATA[iSavedItem].nPosY = nValue;
		}
		else
		{
			SFR_DATA[iSavedItem].nPosY = nValue;
		}

		str_Value.Format("%d", nValue);
		m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
		((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
	}

	if(iSavedSubitem == 3)
	{
		nValue = atoi(str_Value);

		if(nValue < 0)
		{
			nValue = 0;
			SFR_DATA[iSavedItem].nWidth = nValue;
		}
		else if(nValue > CAM_IMAGE_WIDTH)
		{
			nValue = CAM_IMAGE_WIDTH;
			SFR_DATA[iSavedItem].nWidth = nValue;
		}
		else
		{
			SFR_DATA[iSavedItem].nWidth = nValue;
		}

		str_Value.Format("%d", nValue);
		m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
		((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
	}

	if(iSavedSubitem == 4)
	{
		nValue = atoi(str_Value);

		if(nValue < 0)
		{
			nValue = 0;
			SFR_DATA[iSavedItem].nHeight = nValue;
		}
		else if(nValue > CAM_IMAGE_HEIGHT)
		{
			nValue = CAM_IMAGE_HEIGHT;
			SFR_DATA[iSavedItem].nHeight = nValue;
		}
		else
		{
			SFR_DATA[iSavedItem].nHeight = nValue;
		}

		str_Value.Format("%d", nValue);
		m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
		((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
	}

	//if(iSavedSubitem == 6) // 콤보박스

	if(iSavedSubitem == 5)
	{
		double dValue = atof(str_Value);

		if(dValue < 0)
		{
			dValue = 0;
			SFR_DATA[iSavedItem].dThreshold_MIN = dValue;
		}
// 		else if(dValue > 1.0)
// 		{
// 			dValue = 1.0;
// 			SFR_DATA[iSavedItem].dThreshold = dValue;
// 		}
		else
		{
			SFR_DATA[iSavedItem].dThreshold_MIN = dValue;
		}

		str_Value.Format("%.2f", dValue);
		m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
		((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
	}

	if (iSavedSubitem == 6)
	{
		double dValue = atof(str_Value);

		if (dValue < 0)
		{
			dValue = 0;
			SFR_DATA[iSavedItem].dThreshold_MAX = dValue;
		}
		// 		else if(dValue > 1.0)
		// 		{
		// 			dValue = 1.0;
		// 			SFR_DATA[iSavedItem].dThreshold = dValue;
		// 		}
		else
		{
			SFR_DATA[iSavedItem].dThreshold_MAX = dValue;
		}

		str_Value.Format("%.2f", dValue);
		m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
		((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
	}

	if (((CImageTesterDlg *)m_pMomWnd)->b_SecretOption == TRUE || ((CImageTesterDlg *)m_pMomWnd)->b_SecretLgOption == TRUE)
	{
		if (iSavedSubitem == 8)
		{
			double dValue = atof(str_Value);

			if (dValue < 0)
			{
				dValue = 0;
				SFR_DATA[iSavedItem].dOffset = dValue;
			}
			else if (dValue > 1.0)
			{
				nValue = 1.0;
				SFR_DATA[iSavedItem].dOffset = dValue;
			}
			else
			{
				SFR_DATA[iSavedItem].dOffset = dValue;
			}

			str_Value.Format("%.f", dValue);
			m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
			((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
		}
	}

	if (iSavedSubitem == m_nIdex)
	{
		if (m_bSFRmode == 0){
			double dValue = atof(str_Value);

			if (dValue < 0)
			{
				dValue = 0;
				SFR_DATA[iSavedItem].dLinepare = dValue;
			}
			else
			{
				SFR_DATA[iSavedItem].dLinepare = dValue;
			}

			str_Value.Format("%0.2f", dValue);
			m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
			((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
		}
		else{
			int dValue = atoi(str_Value);

			if (dValue < 0)
			{
				dValue = 0;
				SFR_DATA[iSavedItem].MTF = dValue;
			}
			else if (dValue>100){
				dValue = 100;
				SFR_DATA[iSavedItem].MTF = dValue;
			}
			else
			{
				SFR_DATA[iSavedItem].MTF = dValue;
			}

			str_Value.Format("%d", dValue);
			m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
			((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
		}
	}
}
void CSFRTILTOptionDlg::OnCbnKillfocusComboRdMode()
{
	CString str;

	int nIndex = ((CComboBox *)GetDlgItem(IDC_COMBO_RD_MODE))->GetCurSel();

	// 방향 지정
	SFR_DATA[iSavedItem].nEdgeDir = nIndex;

	((CComboBox *)GetDlgItem(IDC_COMBO_RD_MODE))->GetLBText(nIndex, str);
	m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str);

	((CComboBox *)GetDlgItem(IDC_COMBO_RD_MODE))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );

	iSavedItem = -1;
	iSavedSubitem = -1;
}

void CSFRTILTOptionDlg::OnCbnKillfocusComboFontMode()
{
	CString str;

	int nIndex = ((CComboBox *)GetDlgItem(IDC_COMBO_FONT_MODE))->GetCurSel();

	// 방향 지정
	SFR_DATA[iSavedItem].nFont = nIndex;

	((CComboBox *)GetDlgItem(IDC_COMBO_FONT_MODE))->GetLBText(nIndex, str);
	m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str);

	((CComboBox *)GetDlgItem(IDC_COMBO_FONT_MODE))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

	iSavedItem = -1;
	iSavedSubitem = -1;
}
// Grouping [1/8/2019 Seongho.Lee]
void CSFRTILTOptionDlg::OnCbnKillfocusComboGroupMode()
{
	CString str;

	int nIndex = ((CComboBox *)GetDlgItem(IDC_COMBO_GROUP_MODE))->GetCurSel();

	// 방향 지정
	SFR_DATA[iSavedItem].nGrouping = nIndex;

	((CComboBox *)GetDlgItem(IDC_COMBO_GROUP_MODE))->GetLBText(nIndex, str);
	m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str);

	((CComboBox *)GetDlgItem(IDC_COMBO_GROUP_MODE))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

	iSavedItem = -1;
	iSavedSubitem = -1;
}


void CSFRTILTOptionDlg::Change_Data_Val(int Val)
{
	CString str_Value = "";
	int buf = 0;
	int retval = 0;

	GetDlgItemText(IDC_EDIT_RD_MOD,str_Value);
	str_Value.Remove(' ');

	int nValue = 0;
	int nValue_buf = 0;

	double dValue = 0;
	double dValue_buf = 0;

	if(iSavedSubitem == 1)
	{
		nValue_buf = atoi(str_Value);
		nValue = nValue_buf + Val;

		if(nValue < 0)
		{
			nValue = 0;
			SFR_DATA[iSavedItem].nPosX = nValue;
		}
		else if(nValue > CAM_IMAGE_WIDTH)
		{
			nValue = CAM_IMAGE_WIDTH;
			SFR_DATA[iSavedItem].nPosX = nValue;
		}
		else
		{
			SFR_DATA[iSavedItem].nPosX = nValue;
		}

		str_Value.Format("%d", nValue);
		m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
		((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
	}

	if(iSavedSubitem == 2)
	{
		nValue_buf = atoi(str_Value);
		nValue = nValue_buf + Val;

		if(nValue < 0)
		{
			nValue = 0;
			SFR_DATA[iSavedItem].nPosY = nValue;
		}
		else if(nValue > CAM_IMAGE_HEIGHT)
		{
			nValue = CAM_IMAGE_HEIGHT;
			SFR_DATA[iSavedItem].nPosY = nValue;
		}
		else
		{
			SFR_DATA[iSavedItem].nPosY = nValue;
		}

		str_Value.Format("%d", nValue);
		m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
		((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
	}

	if(iSavedSubitem == 3)
	{
		nValue_buf = atoi(str_Value);
		nValue = nValue_buf + Val;

		if(nValue < 0)
		{
			nValue = 0;
			SFR_DATA[iSavedItem].nWidth = nValue;
		}
		else if(nValue > CAM_IMAGE_WIDTH)
		{
			nValue = CAM_IMAGE_WIDTH;
			SFR_DATA[iSavedItem].nWidth = nValue;
		}
		else
		{
			SFR_DATA[iSavedItem].nWidth = nValue;
		}

		str_Value.Format("%d", nValue);
		m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
		((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
	}

	if(iSavedSubitem == 4)
	{
		nValue_buf = atoi(str_Value);
		nValue = nValue_buf + Val;

		if(nValue < 0)
		{
			nValue = 0;
			SFR_DATA[iSavedItem].nHeight = nValue;
		}
		else if(nValue > CAM_IMAGE_HEIGHT)
		{
			nValue = CAM_IMAGE_HEIGHT;
			SFR_DATA[iSavedItem].nHeight = nValue;
		}
		else
		{
			SFR_DATA[iSavedItem].nHeight = nValue;
		}

		str_Value.Format("%d", nValue);
		m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
		((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
	}

	//if(iSavedSubitem == 6) // 콤보박스

	if(iSavedSubitem == 5)
	{
		dValue_buf = atof(str_Value);

 		dValue = dValue_buf + ((double)Val*0.1);
		//dValue = dValue_buf + (double)Val;

		if(dValue < 0)
		{
			dValue = 0;
			SFR_DATA[iSavedItem].dThreshold_MIN = dValue;
		}
// 		else if(dValue > 1.0)
// 		{
// 			dValue = 1.0;
// 			SFR_DATA[iSavedItem].dThreshold = dValue;
// 		}
		else
		{
			SFR_DATA[iSavedItem].dThreshold_MIN = dValue;
		}

		str_Value.Format("%.2f", dValue);
		m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
		((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
	}

	if (iSavedSubitem == 6)
	{
		dValue_buf = atof(str_Value);

		 		dValue = dValue_buf + ((double)Val*0.1);
		//dValue = dValue_buf + (double)Val;

		if (dValue < 0)
		{
			dValue = 0;
			SFR_DATA[iSavedItem].dThreshold_MAX = dValue;
		}
		// 		else if(dValue > 1.0)
		// 		{
		// 			dValue = 1.0;
		// 			SFR_DATA[iSavedItem].dThreshold = dValue;
		// 		}
		else
		{
			SFR_DATA[iSavedItem].dThreshold_MAX = dValue;
		}

		str_Value.Format("%.2f", dValue);
		m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
		((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
	}

	if (((CImageTesterDlg *)m_pMomWnd)->b_SecretOption == TRUE || ((CImageTesterDlg *)m_pMomWnd)->b_SecretLgOption == TRUE)
	{
		if (iSavedSubitem == 8)
		{
			dValue_buf = atof(str_Value);
			//		dValue = dValue_buf + ((double)Val*0.1);
			dValue = dValue_buf + (double)Val;


			if (dValue < 0)
			{
				dValue = 0;
				SFR_DATA[iSavedItem].dOffset = dValue;
			}
			else if (dValue > 1.0)
			{
				nValue = 1.0;
				SFR_DATA[iSavedItem].dOffset = dValue;
			}
			else
			{
				SFR_DATA[iSavedItem].dOffset = dValue;
			}

			str_Value.Format("%.f", dValue);
			m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
			((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
		}
	}

	if(iSavedSubitem == m_nIdex)
	{
		if (m_bSFRmode == 0){
			dValue_buf = atof(str_Value);
			dValue = dValue_buf + ((double)Val*1.0);

			if (dValue < 0)
			{
				dValue = 0;
				SFR_DATA[iSavedItem].dLinepare = dValue;
			}
			else
			{
				SFR_DATA[iSavedItem].dLinepare = dValue;
			}

			str_Value.Format("%0.2f", dValue);
			m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
			((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
		}
		else{
			nValue_buf = atoi(str_Value);
			nValue = nValue_buf + Val;

			if (nValue < 0)
			{
				nValue = 0;
				SFR_DATA[iSavedItem].MTF = nValue;
			}
			else if (nValue>100){
				nValue = 100;
				SFR_DATA[iSavedItem].MTF = nValue;
			}
			else
			{
				SFR_DATA[iSavedItem].MTF = nValue;
			}

			str_Value.Format("%d", nValue);
			m_ROIList.SetItemText(iSavedItem, iSavedSubitem, str_Value);
			((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(str_Value);
		}
	}

	((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetSel(-2, -1);

}
void CSFRTILTOptionDlg::OnBnClickedBtnEtcSave()
{
	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	CString str_Buf = "";

	((CEdit *)GetDlgItem(IDC_EDIT_PIXEL_SIZE))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "PIXEL_SIZE", str_Buf, str_ModelPath);
	m_dPixelSize = atof(str_Buf);

	// default Set
	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_POSX))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "DEFAULT_POSX", str_Buf, str_ModelPath);

	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_POSY))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "DEFAULT_POSY", str_Buf, str_ModelPath);

	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_WIDTH))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "DEFAULT_WIDTH", str_Buf, str_ModelPath);

	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_HEIGHT))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "DEFAULT_HEIGHT", str_Buf, str_ModelPath);

}

void CSFRTILTOptionDlg::OnBnClickedBtnDefaultSet()
{
	CString str_Buf = "";

	int nDefaultSet_PosX = 0;
	int nDefaultSet_PosY = 0;
	int nDefaultSet_Width = 0;
	int nDefaultSet_Height = 0;

	int nBuf_PosX = 0;
	int nBuf_PosY = 0;

	int nBuf_Gap = 0;
	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	// default Set
	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_POSX))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "DEFAULT_POSX", str_Buf, str_ModelPath);

	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_POSY))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "DEFAULT_POSY", str_Buf, str_ModelPath);

	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_WIDTH))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "DEFAULT_WIDTH", str_Buf, str_ModelPath);

	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_HEIGHT))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "DEFAULT_HEIGHT", str_Buf, str_ModelPath);


	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_POSX))->GetWindowText(str_Buf);
	nDefaultSet_PosX = atoi(str_Buf);

	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_POSY))->GetWindowText(str_Buf);
	nDefaultSet_PosY = atoi(str_Buf);

	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_WIDTH))->GetWindowText(str_Buf);
	nDefaultSet_Width = atoi(str_Buf);

	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_HEIGHT))->GetWindowText(str_Buf);
	nDefaultSet_Height = atoi(str_Buf);


//	m_Capture_Count = nDefaultSet_PosX;
	m_DefaultSet_PosX = nDefaultSet_PosX;
	m_DefaultSet_PosY = nDefaultSet_PosY;
	m_DefaultSet_Width = nDefaultSet_Width;
	m_DefaultSet_Height = nDefaultSet_Height;

	for (int i = 0; i < 34; i++)
		SFR_DATA[i].nFont = 0;

	for(int i = 0;i<4; i++){//CENTER
		nBuf_PosX = nDefaultSet_PosX;
		nBuf_PosY = nDefaultSet_PosY;
		nBuf_Gap = (CAM_IMAGE_HEIGHT/24);

			if(i == 0)
			{
				SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2);
				SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2) - nBuf_Gap;
				SFR_DATA[i].nWidth = nDefaultSet_Width;
				SFR_DATA[i].nHeight = nDefaultSet_Height;
				SFR_DATA[i].nEdgeDir = 2;
			}
			if(i == 1)
			{
				SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2) + nBuf_Gap;
				SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2);
				SFR_DATA[i].nWidth = nDefaultSet_Height;
				SFR_DATA[i].nHeight = nDefaultSet_Width;
				SFR_DATA[i].nEdgeDir = 1;
			}
			if(i == 2)
			{
				SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2);
				SFR_DATA[i].nPosY = (nBuf_PosY - (nDefaultSet_Height/2)) + nBuf_Gap;
				SFR_DATA[i].nWidth = nDefaultSet_Width;
				SFR_DATA[i].nHeight = nDefaultSet_Height;
				SFR_DATA[i].nEdgeDir = 3;
			}
			if(i == 3)
			{
				SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2) - nBuf_Gap;
				SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2);
				SFR_DATA[i].nWidth = nDefaultSet_Height;
				SFR_DATA[i].nHeight = nDefaultSet_Width;
				SFR_DATA[i].nEdgeDir = 0;
			}
	}

	for(int i = 4;i<16; i++){//MIDDLE
		if( i==4 || i== 5 ){
			nBuf_PosX = nDefaultSet_PosX + (CAM_IMAGE_WIDTH/5);
			nBuf_PosY = nDefaultSet_PosY - (CAM_IMAGE_HEIGHT/5);
			nBuf_Gap = (CAM_IMAGE_HEIGHT/12);
		}
		if(i == 4)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2) - nBuf_Gap;
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2);
			SFR_DATA[i].nWidth = nDefaultSet_Height;
			SFR_DATA[i].nHeight = nDefaultSet_Width;
			SFR_DATA[i].nEdgeDir = 0;
		}
		if(i == 5)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2);
			SFR_DATA[i].nPosY = (nBuf_PosY - (nDefaultSet_Height/2)) + nBuf_Gap;
			SFR_DATA[i].nWidth = nDefaultSet_Width;
			SFR_DATA[i].nHeight = nDefaultSet_Height;
			SFR_DATA[i].nEdgeDir = 3;
		}

		if( i==6|| i== 7 ){
			nBuf_PosX = nDefaultSet_PosX + (CAM_IMAGE_WIDTH/5);
			nBuf_PosY = nDefaultSet_PosY;
			nBuf_Gap = (CAM_IMAGE_HEIGHT/12);
		}
		if(i == 6)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2);
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2) - nBuf_Gap;
			SFR_DATA[i].nWidth = nDefaultSet_Width;
			SFR_DATA[i].nHeight = nDefaultSet_Height;
			SFR_DATA[i].nEdgeDir = 2;
		}
		if(i == 7)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2) - nBuf_Gap;
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2);
			SFR_DATA[i].nWidth = nDefaultSet_Height;
			SFR_DATA[i].nHeight = nDefaultSet_Width;
			SFR_DATA[i].nEdgeDir = 0;
		}
		if( i==8|| i== 9 ){
			nBuf_PosX = nDefaultSet_PosX + (CAM_IMAGE_WIDTH/5);
			nBuf_PosY = nDefaultSet_PosY + (CAM_IMAGE_HEIGHT/5);
			nBuf_Gap = (CAM_IMAGE_HEIGHT/12);
		}
		if(i == 8)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2);
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2) - nBuf_Gap;
			SFR_DATA[i].nWidth = nDefaultSet_Width;
			SFR_DATA[i].nHeight = nDefaultSet_Height;
			SFR_DATA[i].nEdgeDir = 2;
		}
		if(i == 9)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2) - nBuf_Gap;
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2);
			SFR_DATA[i].nWidth = nDefaultSet_Height;
			SFR_DATA[i].nHeight = nDefaultSet_Width;
			SFR_DATA[i].nEdgeDir = 0;
		}
		if( i==10|| i== 11 ){
			nBuf_PosX = nDefaultSet_PosX - (CAM_IMAGE_WIDTH/5);
			nBuf_PosY = nDefaultSet_PosY + (CAM_IMAGE_HEIGHT/5);
			nBuf_Gap = (CAM_IMAGE_HEIGHT/12);
		}
		if(i == 10)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2) + nBuf_Gap;
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2);
			SFR_DATA[i].nWidth = nDefaultSet_Height;
			SFR_DATA[i].nHeight = nDefaultSet_Width;
			SFR_DATA[i].nEdgeDir = 1;
		}
		if(i == 11)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2);
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2) - nBuf_Gap;
			SFR_DATA[i].nWidth = nDefaultSet_Width;
			SFR_DATA[i].nHeight = nDefaultSet_Height;
			SFR_DATA[i].nEdgeDir = 2;
		}
		if( i==12|| i== 13 ){
			nBuf_PosX = nDefaultSet_PosX - (CAM_IMAGE_WIDTH/5);
			nBuf_PosY = nDefaultSet_PosY ;
			nBuf_Gap = (CAM_IMAGE_HEIGHT/12);
		}
		if(i == 12)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2) + nBuf_Gap;
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2);
			SFR_DATA[i].nWidth = nDefaultSet_Height;
			SFR_DATA[i].nHeight = nDefaultSet_Width;
			SFR_DATA[i].nEdgeDir = 1;
		}
		if(i == 13)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2);
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2) - nBuf_Gap;
			SFR_DATA[i].nWidth = nDefaultSet_Width;
			SFR_DATA[i].nHeight = nDefaultSet_Height;
			SFR_DATA[i].nEdgeDir = 2;
		}

		if( i==14|| i== 15 ){
			nBuf_PosX = nDefaultSet_PosX - (CAM_IMAGE_WIDTH/5);
			nBuf_PosY = nDefaultSet_PosY -(CAM_IMAGE_HEIGHT/5) ;
			nBuf_Gap = (CAM_IMAGE_HEIGHT/12);
		}
		if(i ==14)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2);
			SFR_DATA[i].nPosY = (nBuf_PosY - (nDefaultSet_Height/2)) + nBuf_Gap;
			SFR_DATA[i].nWidth = nDefaultSet_Width;
			SFR_DATA[i].nHeight = nDefaultSet_Height;
			SFR_DATA[i].nEdgeDir = 3;
		}
		if(i == 15)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2) + nBuf_Gap;
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2);
			SFR_DATA[i].nWidth = nDefaultSet_Height;
			SFR_DATA[i].nHeight = nDefaultSet_Width;
			SFR_DATA[i].nEdgeDir = 1;
		}
	}

	for(int i = 16 ;i<24; i++){//SIDE
		if( i==16|| i== 17 ){
			nBuf_PosX = nDefaultSet_PosX + (CAM_IMAGE_WIDTH/3);
			nBuf_PosY = nDefaultSet_PosY - (CAM_IMAGE_HEIGHT/3);
			nBuf_Gap = (CAM_IMAGE_HEIGHT/12);
		}
		if(i == 16)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2) - nBuf_Gap;
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2);
			SFR_DATA[i].nWidth = nDefaultSet_Height;
			SFR_DATA[i].nHeight = nDefaultSet_Width;
			SFR_DATA[i].nEdgeDir = 0;
		}
		if(i ==17)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2);
			SFR_DATA[i].nPosY = (nBuf_PosY - (nDefaultSet_Height/2)) + nBuf_Gap;
			SFR_DATA[i].nWidth = nDefaultSet_Width;
			SFR_DATA[i].nHeight = nDefaultSet_Height;
			SFR_DATA[i].nEdgeDir = 3;
		}
		if( i==18|| i== 19 ){
			nBuf_PosX = nDefaultSet_PosX + (CAM_IMAGE_WIDTH/3);
			nBuf_PosY = nDefaultSet_PosY +(CAM_IMAGE_HEIGHT/3);
			nBuf_Gap = (CAM_IMAGE_HEIGHT/12);
		}
		if(i == 18)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2);
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2) - nBuf_Gap;
			SFR_DATA[i].nWidth = nDefaultSet_Width;
			SFR_DATA[i].nHeight = nDefaultSet_Height;
			SFR_DATA[i].nEdgeDir = 2;
		}
		if(i == 19)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2) - nBuf_Gap;
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2);
			SFR_DATA[i].nWidth = nDefaultSet_Height;
			SFR_DATA[i].nHeight = nDefaultSet_Width;
			SFR_DATA[i].nEdgeDir = 0;
		}
		if( i==20|| i== 21 ){
			nBuf_PosX = nDefaultSet_PosX - (CAM_IMAGE_WIDTH/3);
			nBuf_PosY = nDefaultSet_PosY + (CAM_IMAGE_HEIGHT/3);
			nBuf_Gap = (CAM_IMAGE_HEIGHT/12);
		}
		if(i == 20)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2) + nBuf_Gap;
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2);
			SFR_DATA[i].nWidth = nDefaultSet_Height;
			SFR_DATA[i].nHeight = nDefaultSet_Width;
			SFR_DATA[i].nEdgeDir = 1;
		}
		if(i == 21)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2);
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2) - nBuf_Gap;
			SFR_DATA[i].nWidth = nDefaultSet_Width;
			SFR_DATA[i].nHeight = nDefaultSet_Height;
			SFR_DATA[i].nEdgeDir = 2;
		}

		if( i==22|| i== 23 ){
			nBuf_PosX = nDefaultSet_PosX - (CAM_IMAGE_WIDTH/3);
			nBuf_PosY = nDefaultSet_PosY -(CAM_IMAGE_HEIGHT/3) ;
			nBuf_Gap = (CAM_IMAGE_HEIGHT/12);
		}
		if(i ==22)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2);
			SFR_DATA[i].nPosY = (nBuf_PosY - (nDefaultSet_Height/2)) + nBuf_Gap;
			SFR_DATA[i].nWidth = nDefaultSet_Width;
			SFR_DATA[i].nHeight = nDefaultSet_Height;
			SFR_DATA[i].nEdgeDir = 3;
		}
		if(i == 23)
		{
			SFR_DATA[i].nPosX = nBuf_PosX - (nDefaultSet_Width/2) + nBuf_Gap;
			SFR_DATA[i].nPosY = nBuf_PosY - (nDefaultSet_Height/2);
			SFR_DATA[i].nWidth = nDefaultSet_Height;
			SFR_DATA[i].nHeight = nDefaultSet_Width;
			SFR_DATA[i].nEdgeDir = 1;
		}
	}
	for(int a_=0;a_<34; a_++){
		SFR_DATA[a_].bEnable = TRUE;
			if(a_<4){
 	 			//SFR_DATA[a_].dThreshold = m_dThreshold;
				SFR_DATA[a_].dLinepare =m_dLinePerPixel;
			}else{
				//SFR_DATA[a_].dThreshold = m_dThreshold_Side;
				SFR_DATA[a_].dLinepare =m_dLinePerPixel_Side;
			}
	}
	Upload_List();
}
tResultVal CSFRTILTOptionDlg::ETC_Run()
{		

	// 	if(ChangeCheck == TRUE){
	// 	//	OnBnClickedBtnSave();
	// 	}
	// 	RD_CHECK_MODE = TRUE;



	tResultVal retval = {0,};

	retval.m_Success = TRUE;
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;

	int Tilt_type = POSITIONDATA;
	CString str="";
	int count=0;
	int enableCnt = 0;
	int camcnt = 0;
	int avg_check_line = 0;
	int loop_cnt = 0;

	//-------------------------------------------------------TEST진행

	while(loop_cnt <30)
	{
		// 영상 체크
		if(((CImageTesterDlg *)m_pMomWnd)->CamOnCheck == FALSE)
		{
			retval.m_Success  = FALSE;
			return retval;
		}

		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan =1;	
		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;
		for(int i =0;i<50;i++)
		{
			DoEvents(10);
			if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0 && ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0)
			{
				break;
			}
		}

		if (m_bAutomation == TRUE)
		{
			SearchROIArea(m_RGBScanbuf, CAM_IMAGE_WIDTH);
		}
		else{
			Reload_Rect();
		}


		loop_cnt++;

#if SFRTEST_16bit
		//SFR_Gen_16bit(m_GRAYScanbuf, loop_cnt, m_bSFRmode);

		if (m_Capture_Count == 0)
		{
			m_Capture_Count = 1;
		}

		tSFR_Data SFR_DATA_AVR[MAX_SFR_ROI_NUM] = { 0, };

		for (int iROI = 0; iROI < MAX_SFR_ROI_NUM; iROI++)
		{
			SFR_DATA_AVR[iROI].dResultData = 0;
		}

		for (int iNum = 0; iNum < m_Capture_Count; iNum++)
		{
			SFR_Gen_16bit(m_GRAYScanbuf, loop_cnt, m_bSFRmode);
			for (int iROI = 0; iROI < MAX_SFR_ROI_NUM; iROI++)
			{
				if (SFR_DATA[iROI].bEnable == TRUE)
				{
					SFR_DATA_AVR[iROI].dResultData += SFR_DATA[iROI].dResultData;
				}
			}
		}

		for (int iROI = 0; iROI < MAX_SFR_ROI_NUM; iROI++)
		{
			if (SFR_DATA[iROI].bEnable == TRUE)
			{
				SFR_DATA[iROI].dResultData = SFR_DATA_AVR[iROI].dResultData / m_Capture_Count;
			}
		}


#else
		SFR_Gen(m_RGBScanbuf, loop_cnt, m_bSFRmode);
#endif	

		m_dXtilt_result = abs(SFR_DATA[m_nXtilt1].dResultData - SFR_DATA[m_nXtilt2].dResultData); //xTilt result 차이 절대값 표기
		m_dY1tilt_result = abs(SFR_DATA[m_nY1tilt1].dResultData - SFR_DATA[m_nY1tilt2].dResultData); //y1Tilt result 차이 절대값 표기
		m_dY2tilt_result = abs(SFR_DATA[m_nY2tilt1].dResultData - SFR_DATA[m_nY2tilt2].dResultData); //y2Tilt result 차이 절대값 표기
	}

	//// 결과값 포워딩

	


	for(int i = 0; i < 34 ; i++){
		if(SFR_DATA[i].bEnable == TRUE){
			if (SFR_DATA[i].dResultData >= SFR_DATA[i].dThreshold_MIN && SFR_DATA[i].dResultData <= SFR_DATA[i].dThreshold_MAX)
				SFR_DATA[i].m_Success = TRUE;
			else
				SFR_DATA[i].m_Success = FALSE;


		}
	}

	//-------------------------------------------------------워크리스트 추가

	for(int i=0; i<34; i++)
	{
		if(SFR_DATA[i].bEnable == TRUE){
			enableCnt++;
		}
	}


	for(int i=0; i<34; i++)
	{
		if(SFR_DATA[i].m_Success== TRUE && SFR_DATA[i].bEnable){
			count++;
		}

		if( i<4){
			int Index = ((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_CenterList.InsertItem(i,"",0);

			((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_CenterList.SetItemText(Index,0,SFR_RectName[i]);
			if(SFR_DATA[i].m_Success == TRUE){
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_CenterList.SetItemText(Index,2,"PASS");

			}else{
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_CenterList.SetItemText(Index,2,"FAIL");

			}
			if(SFR_DATA[i].bEnable ==TRUE){
				str.Empty();
				str.Format("%d",(int)SFR_DATA[i].dResultData);
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_CenterList.SetItemText(Index,1,str);
			}
			else{
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_CenterList.SetItemText(Index,1,"X");
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_CenterList.SetItemText(Index,2,"NONE");

			}


		}else{
			int Index = ((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_SideList.InsertItem(i,"",0);

			if(SFR_DATA[i].m_Success == TRUE){
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_SideList.SetItemText(Index,2,"PASS");

			}else{
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_SideList.SetItemText(Index,2,"FAIL");

			}
			((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_SideList.SetItemText(Index,0,SFR_RectName[i]);
			if(SFR_DATA[i].bEnable ==TRUE){
				str.Empty();
				str.Format("%d", (int)SFR_DATA[i].dResultData);
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_SideList.SetItemText(Index,1,str);
			}
			else{
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_SideList.SetItemText(Index,1,"X");
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_SideList.SetItemText(Index,2,"NONE");
			}

		}
	}


	if (count == enableCnt && m_dXtilt_result <= m_dXtilt && m_dY1tilt_result <= m_dY1tilt && m_dY2tilt_result <= m_dY2tilt){
		retval.m_Success = TRUE;
	}else{
		retval.m_Success = FALSE;

		if (m_dXtilt_result > m_dXtilt)
		{
			EachTiltResult[0] = FALSE;
		}
		else
		{
			EachTiltResult[0] = TRUE;
		}

		if (m_dY1tilt_result > m_dY1tilt)
		{
			EachTiltResult[1] = FALSE;
		}
		else
		{
			EachTiltResult[1] = FALSE;
		}

		if (m_dY2tilt_result > m_dY2tilt)
		{
			EachTiltResult[2] = FALSE;
		}
		else
		{
			EachTiltResult[2] = FALSE;
		}

		
	}

	retval.m_ID = 0x02;
	retval.ValString.Empty();
	retval.ValString.Format("%d/%d 성공 Xtilt : %.2f,Y1tilt : %.2f,Y2tilt : %.2f ", count, enableCnt, m_dXtilt_result, m_dY1tilt_result, m_dY2tilt_result);
	DoEvents(2);

	return retval;	
}
tResultVal CSFRTILTOptionDlg::Run()
{

	// 	if(ChangeCheck == TRUE){
	// 	//	OnBnClickedBtnSave();
	// 	}
	// 	RD_CHECK_MODE = TRUE;

	m_ETC_State = 0;

	m_dAvr01F_Group = 0;
	m_dAvr03F_Group = 0;
	m_dAvr05F_Group = 0;
	m_dAvr07F_Group = 0;
	m_dAvr05FA_Group = 0;
	m_dAvr07FA_Group = 0;

	delta_V = 99999;
	delta_H = 99999;

	delta_H_Success = FALSE;
	delta_V_Success = FALSE;

	b_StopFail = FALSE;

	tResultVal retval = { 0, };

	retval.m_Success = TRUE;
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;

	int Tilt_type = POSITIONDATA;
	CString str = "";
	int count = 0;
	int enableCnt = 0;
	int camcnt = 0;
	int avg_check_line = 0;
	int loop_cnt = 0;

	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
		InsertList();
	}
	CString str_temp;
	//-------------------------------------------------------TEST진행
	while (loop_cnt <10)
	{
		// 영상 체크
		if (((CImageTesterDlg *)m_pMomWnd)->CamOnCheck == FALSE)
		{
			retval.m_Success = FALSE;
			return retval;
		}

		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = 1;
		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;
		for (int i = 0; i<50; i++)
		{
			DoEvents(10);
			if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0 && ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0)
			{
				break;
			}
		}

		if (m_bAutomation == TRUE)
		{
			SearchROIArea(m_RGBScanbuf, CAM_IMAGE_WIDTH);
		}
		else{
			Reload_Rect();
		}

#if SFRTEST_16bit

		if (m_Capture_Count == 0)
		{
			m_Capture_Count = 1;
		}

		
		for (int iROI = 0; iROI < MAX_SFR_ROI_NUM; iROI++)
		{
			SFR_DATA_AVR[iROI].dResultData = 0; 
		}

		for (int iNum = 0; iNum < m_Capture_Count; iNum++)
		{
			SFR_Gen_16bit(m_GRAYScanbuf, loop_cnt, m_bSFRmode);
			for (int iROI = 0; iROI < MAX_SFR_ROI_NUM; iROI++)
			{
				if (SFR_DATA[iROI].bEnable == TRUE)
				{
					SFR_DATA_AVR[iROI].dResultData += SFR_DATA[iROI].dResultData;
				}
			}
		}

		for (int iROI = 0; iROI < MAX_SFR_ROI_NUM; iROI++)
		{
			if (SFR_DATA[iROI].bEnable == TRUE)
			{
				SFR_DATA[iROI].dResultData = SFR_DATA_AVR[iROI].dResultData / m_Capture_Count;
			}
		}



#else
		SFR_Gen(m_RGBScanbuf, loop_cnt,m_bSFRmode);
#endif		

		m_dXtilt_result = abs(SFR_DATA[m_nXtilt1].dResultData - SFR_DATA[m_nXtilt2].dResultData); //xTilt result 차이 절대값 표기
		m_dY1tilt_result = abs(SFR_DATA[m_nY1tilt1].dResultData - SFR_DATA[m_nY1tilt2].dResultData); //y1Tilt result 차이 절대값 표기
		m_dY2tilt_result = abs(SFR_DATA[m_nY2tilt1].dResultData - SFR_DATA[m_nY2tilt2].dResultData); //y2Tilt result 차이 절대값 표기


		BOOL bTest = TRUE;
		for (int i = 0; i <34; i++){
			if (SFR_DATA[i].bEnable == TRUE){

				str_temp.Format("%.2f", SFR_DATA[i].dResultData);
				double temp_Result = 0.0;
				temp_Result = atof(str_temp);

				if (temp_Result >= SFR_DATA[i].dThreshold_MIN && temp_Result <= SFR_DATA[i].dThreshold_MAX){
					SFR_DATA[i].m_Success = TRUE;
				}
				else{
					SFR_DATA[i].m_Success = FALSE;
					bTest = FALSE;
				}
			}
		}

		if (loop_cnt > 2)
		{
			if (bTest == TRUE)
			{
				break;
			}
		}

		loop_cnt++;
	}


	
	((CImageTesterDlg *)m_pMomWnd)->TestImageSaveMode(m_nImageSaveMode, m_GRAYScanbuf, _T("SFR"));
	// Grouping [1/8/2019 Seongho.Lee]

	double stInput_temp[10] = { 0 };
	double MinMax05F_temp[4] = { 0 };			
	double MinMax07F_temp[4] = { 0 };
	double Temp_05Min = 9999;
	double Temp_05Max = 0;
	double Temp_07Min = 9999;
	double Temp_07Max = 0;

	int temp_count[10] = { 0 };
	for (int nIdx = 0; nIdx < MAX_SFR_ROI_NUM; nIdx++)
	{
		if (SFR_DATA[nIdx].bEnable == TRUE)
		{
			stInput_temp[SFR_DATA[nIdx].nGrouping] += SFR_DATA[nIdx].dResultData;			// 각 필드 SUM(0.1F, 0.3F, 0.5F(TL,TR,BL,BR),0.7F(TL,TR,BL,BR))
			temp_count[SFR_DATA[nIdx].nGrouping]++;																// 각 필드 CNT
		}
	}
	for (int nIdx = 0; nIdx < 10; nIdx++)
	{
		if (temp_count[nIdx] != 0)
			stInput_temp[nIdx] /= temp_count[nIdx];							// 각 필드 AVR
	}
	for (int nIdx = 0; nIdx < 4; nIdx++)
	{
		MinMax05F_temp[nIdx] = stInput_temp[nIdx + 2];
		MinMax07F_temp[nIdx] = stInput_temp[nIdx + 6];
	}
	for (int nIdx = 0; nIdx < 4; nIdx+=2)														// 0.5F(TL,TR,BL,BR), 0.7F(TL,TR,BL,BR) MIN MAX
	{
		if (MinMax05F_temp[nIdx] > MinMax05F_temp[nIdx + 1])	
		{
			if (Temp_05Max < MinMax05F_temp[nIdx])
				Temp_05Max = MinMax05F_temp[nIdx];
			if (Temp_05Min >MinMax05F_temp[nIdx + 1])
				Temp_05Min = MinMax05F_temp[nIdx+1];
		}
		else if (MinMax05F_temp[nIdx] < MinMax05F_temp[nIdx + 1])
		{
			if (Temp_05Max < MinMax05F_temp[nIdx+1])
				Temp_05Max = MinMax05F_temp[nIdx+1];
			if (Temp_05Min >MinMax05F_temp[nIdx])
				Temp_05Min = MinMax05F_temp[nIdx];
		}
		if (MinMax07F_temp[nIdx] > MinMax07F_temp[nIdx + 1])
		{
			if (Temp_07Max < MinMax07F_temp[nIdx])
				Temp_07Max = MinMax07F_temp[nIdx];
			if (Temp_07Min >MinMax07F_temp[nIdx + 1])
				Temp_07Min = MinMax07F_temp[nIdx + 1];
		}
		else if (MinMax07F_temp[nIdx] < MinMax07F_temp[nIdx + 1])
		{
			if (Temp_07Max < MinMax07F_temp[nIdx + 1])
				Temp_07Max = MinMax07F_temp[nIdx + 1];
			if (Temp_07Min >MinMax07F_temp[nIdx])
				Temp_07Min = MinMax07F_temp[nIdx];
		}
	}
	for (int nIdx = 0; nIdx < 4; nIdx++)
	{
		m_dAvr05F_Group += MinMax05F_temp[nIdx];
		m_dAvr07F_Group += MinMax07F_temp[nIdx];
	}
	m_dAvr05F_Group /= 4;																					// 0.5F		AVR
	m_dAvr07F_Group /= 4;																					// 0.7F		AVR
	m_dAvr05FA_Group = (Temp_05Max - Temp_05Min) / m_dAvr05F_Group;	// 0.5FA	AVR
	m_dAvr07FA_Group = (Temp_07Max - Temp_07Min) / m_dAvr07F_Group; // 0.7FA	AVR

	//////////////////////////////////////////////////////////////////////////


	for (int i = 0; i <34; i++){
		if (SFR_DATA[i].bEnable == TRUE){

			str_temp.Format("%.2f", SFR_DATA[i].dResultData);

			double temp_Result = 0.0;
			temp_Result = atof(str_temp);
			
			if (temp_Result >= SFR_DATA[i].dThreshold_MIN && temp_Result <= SFR_DATA[i].dThreshold_MAX)
				SFR_DATA[i].m_Success = TRUE;
			else
				SFR_DATA[i].m_Success = FALSE;
		}
	}

	//-------------------------------------------------------워크리스트 추가


	for (int i = 0; i<34; i++)
	{
		if (SFR_DATA[i].bEnable == TRUE){
			enableCnt++;
		}
	}
	CString stateDATA = "SFR_ ";
	for (int i = 0; i<34; i++)
	{

		if (SFR_DATA[i].bEnable == TRUE){
			str.Empty();
			str.Format(_T("%.2f"), SFR_DATA[i].dResultData);
			m_SFRList.SetItemText(InsertIndex, 4 + i, str);
		}
		else{
			str.Empty();
			str.Format("X");

			m_SFRList.SetItemText(InsertIndex, 4 + i, str);
		}
		if (SFR_DATA[i].m_Success == TRUE && SFR_DATA[i].bEnable){
			count++;
		}

		if (i<4){
			int Index = ((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_CenterList.InsertItem(i, "", 0);

			((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_CenterList.SetItemText(Index, 0, SFR_RectName[i]);
			if (SFR_DATA[i].m_Success == TRUE){
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_CenterList.SetItemText(Index, 2, "PASS");

			}
			else{
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_CenterList.SetItemText(Index, 2, "FAIL");

			}
			if (SFR_DATA[i].bEnable == TRUE){
				str.Empty();
				str.Format("%d", (int)SFR_DATA[i].dResultData);
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_CenterList.SetItemText(Index, 1, str);
			}
			else{
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_CenterList.SetItemText(Index, 1, "X");
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_CenterList.SetItemText(Index, 2, "NONE");

			}


		}
		else{
			int Index = ((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_SideList.InsertItem(i, "", 0);

			if (SFR_DATA[i].m_Success == TRUE){
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_SideList.SetItemText(Index, 2, "PASS");

			}
			else{
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_SideList.SetItemText(Index, 2, "FAIL");

			}
			((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_SideList.SetItemText(Index, 0, SFR_RectName[i]);
			if (SFR_DATA[i].bEnable == TRUE){
				str.Empty();
				str.Format("%d", (int)SFR_DATA[i].dResultData);
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_SideList.SetItemText(Index, 1, str);
			}
			else{
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_SideList.SetItemText(Index, 1, "X");
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->m_SideList.SetItemText(Index, 2, "NONE");
			}

		}
	}

		

	////좌우,상하 편차
	// 	double side_vertical[4] = {SFR_DATA[8].dResultData, SFR_DATA[11].dResultData, SFR_DATA[12].dResultData, SFR_DATA[15].dResultData};
	// 	double side_horizon[4] = {SFR_DATA[9].dResultData, SFR_DATA[10].dResultData, SFR_DATA[13].dResultData, SFR_DATA[14].dResultData};
	// 	
	// 	delta_V = ComputeDelta(side_vertical);
	// 	delta_H = ComputeDelta(side_horizon);
	// 
	// 	if(delta_V <= delta_V_Thre)
	// 		delta_V_Success = TRUE;
	// 	else
	// 		delta_V_Success = FALSE;
	// 
	// 	if(delta_H <= delta_H_Thre)
	// 		delta_H_Success = TRUE;
	// 	else
	// 		delta_H_Success = FALSE;

	// 	if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
	// 		if (count == enableCnt/* && delta_V_Success && delta_H_Success*/){
	// 			retval.m_Success = TRUE;
	// 			m_Lot_SFRList.SetItemText(Lot_InsertIndex, 39, "PASS");
	// 		}
	// 		else{
	// 			retval.m_Success = FALSE;
	// 			m_Lot_SFRList.SetItemText(Lot_InsertIndex, 39, "FAIL");
	// 		}
	// 
	// 		Lot_StartCnt++;
	// 
	// 	}
	// 	else{
	retval.m_ID = 0x15;
	retval.ValString.Empty();

	//m_dXtilt_result = 10;
	//m_dY1tilt_result = 20;
	//m_dY2tilt_result = 30;

	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE)
	{
		if (count == enableCnt && m_dXtilt_result < m_dXtilt && m_dY1tilt_result < m_dY1tilt && m_dY2tilt_result < m_dY2tilt/* && delta_V_Success && delta_H_Success*/){
			retval.m_Success = TRUE;
			
			str.Empty();
			str.Format(_T("%.2f"), m_dXtilt_result);
			m_SFRList.SetItemText(InsertIndex, 38, str);

			str.Empty();
			str.Format(_T("%.2f"), m_dY1tilt_result);
			m_SFRList.SetItemText(InsertIndex, 39, str);

			str.Empty();
			str.Format(_T("%.2f"), m_dY2tilt_result);
			m_SFRList.SetItemText(InsertIndex, 40, str);
			
			m_SFRList.SetItemText(InsertIndex, 41, "PASS");

			retval.ValString.Format("%d/%d 성공 Xtilt : %.2f,Y1tilt : %.2f,Y2tilt : %.2f ", count, enableCnt, m_dXtilt_result, m_dY1tilt_result, m_dY2tilt_result);
			stateDATA += retval.ValString;

			stateDATA += "_PASS";
			((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_SFR, "PASS");

		}
		else{
			retval.m_Success = FALSE;

			if (m_dXtilt_result > m_dXtilt)
			{
				EachTiltResult[0] = FALSE;
			}
			else
			{
				EachTiltResult[0] = TRUE;
			}

			if (m_dY1tilt_result > m_dY1tilt)
			{
				EachTiltResult[1] = FALSE;
			}
			else
			{
				EachTiltResult[1] = FALSE;
			}

			if (m_dY2tilt_result > m_dY2tilt)
			{
				EachTiltResult[2] = FALSE;
			}
			else
			{
				EachTiltResult[2] = FALSE;
			}


			retval.ValString.Format("%d/%d 실패 Xtilt : %.2f,Y1tilt : %.2f,Y2tilt : %.2f ", count, enableCnt, m_dXtilt_result, m_dY1tilt_result, m_dY2tilt_result);
			stateDATA += retval.ValString;

			stateDATA += "_FAIL";

			str.Empty();
			str.Format(_T("%.2f"), m_dXtilt_result);
			m_SFRList.SetItemText(InsertIndex, 38, str);

			str.Empty();
			str.Format(_T("%.2f"), m_dY1tilt_result);
			m_SFRList.SetItemText(InsertIndex, 39, str);

			str.Empty();
			str.Format(_T("%.2f"), m_dY2tilt_result);
			m_SFRList.SetItemText(InsertIndex, 40, str);

			m_SFRList.SetItemText(InsertIndex, 41, "FAIL");
			((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_SFR, "FAIL");

		}

		StartCnt++;
	}
	else
	{
		if (count == enableCnt && m_dXtilt_result <= m_dXtilt && m_dY1tilt_result <= m_dY1tilt && m_dY2tilt_result <= m_dY2tilt/* && delta_V_Success && delta_H_Success*/)
		{
			retval.m_Success = TRUE;
			retval.ValString.Format("%d/%d 성공 Xtilt : %.2f,Y1tilt : %.2f,Y2tilt : %.2f ", count, enableCnt, m_dXtilt_result, m_dY1tilt_result, m_dY2tilt_result);
			stateDATA += retval.ValString;

			stateDATA += "_PASS";
		}
		else
		{
			retval.m_Success = FALSE;
			retval.ValString.Format("%d/%d 실패 Xtilt : %.2f,Y1tilt : %.2f,Y2tilt : %.2f ", count, enableCnt, m_dXtilt_result, m_dY1tilt_result, m_dY2tilt_result);
			stateDATA += retval.ValString;

			stateDATA += "_FAIL";
		}
	}

	((CImageTesterDlg  *)m_pMomWnd)->StatePrintf(stateDATA);
	//DoEvents(200);

	//Image_Save(m_ImageSaveMode, retval.m_Success);


	//// 결과값 포워딩

	DoEvents(150);

	m_ETC_State = 1;

	// 	((CImageTesterDlg *)m_pMomWnd)->Original_ImageCapture();
	// 	((CImageTesterDlg *)m_pMomWnd)->OnBnClickedButton2();

	CopyMesData();
	return retval;
}
void CSFRTILTOptionDlg::Pic(CDC *cdc)
{
	for(int i=0; i<34; i++)
	{
		if(SFR_DATA[i].bEnable == TRUE)
		{
			if(ListView_GetCheckState(m_ROIList, i) == TRUE)
			{
				SFR_Pic(cdc, i);
			}
		}
	}

	
	Delta_Pic(cdc);

// 	if(b_Field){
// 		Field_Pic(cdc);
// 	}

}

void CSFRTILTOptionDlg::Delta_Pic(CDC* cdc)
{
	
	::SetBkMode(cdc->m_hDC,TRANSPARENT);

	//******************************************************************************
	CPen my_Pan, my_Pan2, *old_pan;
//***********************초기 원점 탐색 영역******************************//
	my_Pan.CreatePen(PS_SOLID,3,RGB(255, 255, 255));
	
	CString TEXTDATA ="";
	CFont font;
	font.CreatePointFont(220, "Arial"); 
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);


	/*my_Pan2.CreatePen(PS_SOLID,3,RGB(255, 255, 0));
	old_pan = cdc->SelectObject(&my_Pan2);
	cdc->MoveTo(x1-10, y1);
	cdc->LineTo(x1+10, y1);
	cdc->MoveTo(x1, y1-10);
	cdc->LineTo(x1, y1+10);
	my_Pan2.DeleteObject();*/
	cdc->SelectObject(&font);
	
	
	
	TEXTDATA.Format("H_Delta  = %.2f", delta_H);
	if(delta_H_Success == TRUE){
		cdc->SetTextColor(RGB(0, 0, 255));
	}else{
		cdc->SetTextColor(RGB(255, 0, 0));
	}
	
	if(m_ETC_State == 1)
		cdc->TextOut(520 , 700, TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());


	TEXTDATA.Format("V_Delta = %.2f", delta_V);
	if( delta_V_Success == TRUE){
		cdc->SetTextColor(RGB(0, 0, 255));
	}else{
		cdc->SetTextColor(RGB(255, 0, 0));
	}

	if(m_ETC_State == 1)
		cdc->TextOut(760 , 700, TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	font.DeleteObject();
//*********************************************************************

}


void CSFRTILTOptionDlg::SFR_Pic(CDC *cdc, int nCnt)
{
	CString str_Buf		= "";
	CString str_Val		= "";

	CPen		Pen, *Old_Pen;
	//CBrush	Brush, *Old_Brush;
	CFont		Font, *Old_Font;

	int nCPos_X	= 0;
	int nCPos_Y	= 0;
	double nCPos_Z	= 0;

	int nCOffsetPos_X	= 0;
	int nCOffsetPos_Y	= 0;
	double nCOffsetPos_Z	= 0;

	int AbsCOffsetPos_X	= 0;
	int AbsCOffsetPos_Y	= 0;
	double AbsCOffsetPos_Z	= 0;

	int nPos_X	= 0;
	int nPos_Y	= 0;
	int nWidth	= 0;
	int nHeight	= 0;

	double dFieldWidth = 0.0;
	double dImgZ = 0.0;

	// 계산식
	int Img_Width = CAM_IMAGE_WIDTH;
	int Img_Height = CAM_IMAGE_HEIGHT;

	Img_Width *= Img_Width;
	Img_Height *= Img_Height;

	dFieldWidth = (double)(1 / (sqrt((double)(Img_Width+Img_Height)) / 2 ));

	double dFieldValue = 0.0;

	CRect Rect;
	((CImageTesterDlg *)m_pMomWnd)->m_CamFrame.GetClientRect(Rect);

	nPos_X	= SFR_DATA[nCnt].nPosX;
	nPos_Y	= SFR_DATA[nCnt].nPosY;
	nWidth	= SFR_DATA[nCnt].nWidth;
	nHeight	= SFR_DATA[nCnt].nHeight;

	nCPos_X = nPos_X + (nWidth/2);
	nCPos_Y = nPos_Y + (nHeight/2);

	nCOffsetPos_X = nCPos_X - (CAM_IMAGE_WIDTH/2);
	nCOffsetPos_Y = nCPos_Y - (CAM_IMAGE_HEIGHT/2);

	nCOffsetPos_X *= nCOffsetPos_X;
	nCOffsetPos_Y *= nCOffsetPos_Y;

	nCOffsetPos_Z = sqrt((double)(nCOffsetPos_X+nCOffsetPos_Y));

	dFieldValue = nCOffsetPos_Z * dFieldWidth;

	SFR_DATA[nCnt].dFieldData = dFieldValue;
	str_Buf.Format("%s (%0.2fF)", SFR_DATA[nCnt].NAME, dFieldValue);

	COLORREF nColor = RGB(255, 0, 0);
	COLORREF nResultColor = RGB(255, 54, 255);
	COLORREF nFontColor = RGB(255, 255, 0);
//	COLORREF nFontColor = RGB(22, 83, 209);

	::SetBkMode(cdc->m_hDC, TRANSPARENT);


	//double temp_Thre = 0.0;
	//if(SFR_DATA[nCnt].dThreshold > 0)
	//	temp_Thre = SFR_DATA[nCnt].dThreshold - 0.005;

	if (SFR_DATA[nCnt].m_Success)
	{
		nColor = RGB(18, 69, 171);
	}
	else
	{	
		nColor = RGB(255, 0, 0);
	}


	Pen.CreatePen(PS_SOLID, 3, nColor);

	int nFontSize = Rect.Height() / 5;

	Font.CreatePointFont(nFontSize, "Arial");
 
	Old_Font = cdc->SelectObject(&Font);

	str_Val.Format("%.2f", SFR_DATA[nCnt].dResultData);
	cdc->SetTextColor(nResultColor);

// 	if(SFR_DATA[i].nType == 0)
// 		str_Value = "◁▶";
// 	if(SFR_DATA[i].nType == 1)
// 		str_Value = "◀▷";
// 	if(SFR_DATA[i].nType == 2)
// 		str_Value = "△▼";
// 	if(SFR_DATA[i].nType == 3)
// 		str_Value = "▲▽";

// 	if(nCnt <= 3){
// 		if(SFR_DATA[nCnt].nType == 0)
// 		{
// 			cdc->TextOut(nPos_X , nCPos_Y+35, str_Val.GetBuffer(0), str_Val.GetLength());
// 
// 			cdc->SetTextColor(nFontColor);
// 			cdc->TextOut(nPos_X , nCPos_Y+20, str_Buf.GetBuffer(0), str_Buf.GetLength());
// 		}
// 
// 		if(SFR_DATA[nCnt].nType == 1)
// 		{
// 			cdc->TextOut(nPos_X , nCPos_Y-60, str_Val.GetBuffer(0), str_Val.GetLength());
// 
// 			cdc->SetTextColor(nFontColor);
// 			cdc->TextOut(nPos_X , nCPos_Y-45, str_Buf.GetBuffer(0), str_Buf.GetLength());
// 		}
// 
// 		if(SFR_DATA[nCnt].nType == 2)
// 		{
// 			cdc->TextOut(nCPos_X-20 , nPos_Y-35, str_Val.GetBuffer(0), str_Val.GetLength());
// 
// 			cdc->SetTextColor(nFontColor);
// 			cdc->TextOut(nCPos_X-20 , nPos_Y-20, str_Buf.GetBuffer(0), str_Buf.GetLength());
// 		}
// 
// 		if(SFR_DATA[nCnt].nType == 3)
// 		{
// 			cdc->TextOut(nCPos_X-45 , (nPos_Y+nHeight)+5, str_Val.GetBuffer(0), str_Val.GetLength());
// 
// 			cdc->SetTextColor(nFontColor);
// 			cdc->TextOut(nCPos_X-45 , (nPos_Y+nHeight)+20, str_Buf.GetBuffer(0), str_Buf.GetLength());
// 		}
// 	}else{
// 
// 		if(SFR_DATA[nCnt].nType == 0)
// 		{
// 			cdc->TextOut(nPos_X-60 , nCPos_Y-35, str_Val.GetBuffer(0), str_Val.GetLength());
// 
// 			cdc->SetTextColor(nFontColor);
// 			cdc->TextOut(nPos_X-80 , nCPos_Y-20, str_Buf.GetBuffer(0), str_Buf.GetLength());
// 		}
// 
// 		if(SFR_DATA[nCnt].nType == 1)
// 		{
// 			cdc->TextOut((nPos_X+nWidth)+15 , nCPos_Y+5, str_Val.GetBuffer(0), str_Val.GetLength());
// 
// 			cdc->SetTextColor(nFontColor);
// 			cdc->TextOut((nPos_X+nWidth)+5 , nCPos_Y+20, str_Buf.GetBuffer(0), str_Buf.GetLength());
// 		}
// 
// 		if(SFR_DATA[nCnt].nType == 2)
// 		{
// 			cdc->TextOut(nCPos_X-60 , nPos_Y-35, str_Val.GetBuffer(0), str_Val.GetLength());
// 
// 			cdc->SetTextColor(nFontColor);
// 			cdc->TextOut(nCPos_X-70 , nPos_Y-20, str_Buf.GetBuffer(0), str_Buf.GetLength());
// 		}
// 
// 		if(SFR_DATA[nCnt].nType == 3)
// 		{
// 			cdc->TextOut(nCPos_X-5 , (nPos_Y+nHeight)+5, str_Val.GetBuffer(0), str_Val.GetLength());
// 
// 			cdc->SetTextColor(nFontColor);
// 			cdc->TextOut(nCPos_X-15 , (nPos_Y+nHeight)+20, str_Buf.GetBuffer(0), str_Buf.GetLength());
// 		}
// 
// 	}

	int nFtSz = nFontSize / 20;

	CFont TextFont, *Text_old_font;

	Old_Pen = cdc->SelectObject(&Pen);
	cdc->SetTextColor(nFontColor);

	TextFont.CreatePointFont(100, "Arial");
	Text_old_font = cdc->SelectObject(&TextFont);

	if (SFR_DATA[nCnt].nFont == 0)
		{
		cdc->TextOut(nPos_X - nFtSz - (nFtSz * str_Buf.GetLength()) * 1.5, nPos_Y - nFtSz, str_Buf, str_Buf.GetLength());
		cdc->TextOut(nPos_X - nFtSz - (nFtSz * str_Val.GetLength()) * 1.5, nPos_Y - nFtSz + 11, str_Val, str_Val.GetLength());
		}
	else if (SFR_DATA[nCnt].nFont == 1)
		{
		cdc->TextOut((nPos_X + nWidth) + nFtSz, nPos_Y - nFtSz, str_Buf, str_Buf.GetLength());
		cdc->TextOut((nPos_X + nWidth) + nFtSz, nPos_Y - nFtSz + 10, str_Val, str_Val.GetLength());
		}
	else if (SFR_DATA[nCnt].nFont == 2)
		{
		cdc->TextOut(nPos_X - (nFtSz / 2 * str_Buf.GetLength()), nPos_Y - nFtSz * 6.5, str_Buf, str_Buf.GetLength());
		cdc->TextOut(nPos_X - (nFtSz / 2 * str_Val.GetLength()), nPos_Y - nFtSz * 6.5 + 11, str_Val, str_Val.GetLength());
		}
	else if (SFR_DATA[nCnt].nFont == 3)
		{
		cdc->TextOut(nPos_X - (nFtSz / 2 * str_Buf.GetLength()), (nPos_Y + nHeight) + nFtSz, str_Buf, str_Buf.GetLength());
		cdc->TextOut(nPos_X - (nFtSz / 2 * str_Val.GetLength()), (nPos_Y + nHeight) + nFtSz + 11, str_Val, str_Val.GetLength());
	}

	cdc->SelectObject(Text_old_font);
	TextFont.DeleteObject();
		
	cdc->MoveTo(nPos_X, nPos_Y);
	cdc->LineTo(nPos_X+nWidth, nPos_Y);
	cdc->LineTo(nPos_X+nWidth, nPos_Y+nHeight);
	cdc->LineTo(nPos_X, nPos_Y+nHeight);
	cdc->LineTo(nPos_X, nPos_Y);


 	if(nCnt == Low_SFRDATA_X_Tilt && Low_SFRDATA_X_Tilt != 0){
// 		nColor = RGB(255, 0, 0);
// 		Pen.CreatePen(PS_SOLID, 3, nColor);
 		Old_Pen = cdc->SelectObject(&Pen);
 		cdc->MoveTo(nPos_X-5, nPos_Y-5);
 		cdc->LineTo(nPos_X+nWidth+5, nPos_Y-5);
 		cdc->LineTo(nPos_X+nWidth+5, nPos_Y+nHeight+5);
 		cdc->LineTo(nPos_X-5, nPos_Y+nHeight+5);
 		cdc->LineTo(nPos_X-5, nPos_Y-5);
 	}

	cdc->SelectObject(Old_Pen);
	cdc->SelectObject(Old_Font);

	Pen.DeleteObject();
	Font.DeleteObject();
	//--------------------------------------------PASS.FAIL COUNT
	CFont CountFont, *Count_old_font;
	int PassCnt=0, FailCnt=0, TotalCnt=0;
	int CenterCnt=0,MidCnt=0,SideCnt=0 ;
	double CenterAvgValue = 0,MidAvgValue = 0, SideAvgValue = 0;
	CString str= "";
	CountFont.CreatePointFont(245, "Arial");
	Count_old_font = cdc->SelectObject(&CountFont);

	for(int i=0; i<34; i++)
	{	
		if(SFR_DATA[i].bEnable == TRUE)
		{
			if((SFR_DATA[i].dResultData >=0)&&(SFR_DATA[i].dResultData < 1)){
				if( i < 4 )
				{
					CenterAvgValue += SFR_DATA[i].dResultData;
					CenterCnt++;
				}else if(i < 8){
					MidAvgValue += SFR_DATA[i].dResultData;
					MidCnt++;
				}else{
					SideAvgValue += SFR_DATA[i].dResultData;
					SideCnt++;
				}
			}else{
				if( i < 4 ){
					CenterCnt++;
				}else if(i < 8){
					MidCnt++;
				}else{
					SideCnt++;
				}
			}
			TotalCnt++;

			if(SFR_DATA[i].m_Success == TRUE)
				PassCnt++;
			else
				FailCnt++;
		}
	}	


	if(FailCnt > 0)
	{
		cdc->SetTextColor(RGB(255, 0, 0));
		str.Format("%d / %d : FAIL", PassCnt, TotalCnt);
		cdc->TextOut(50, 40, str.GetBuffer(0),str.GetLength());
	}
	else
	{
		cdc->SetTextColor(RGB(0, 0, 255));
		str.Format("%d / %d : PASS", PassCnt, TotalCnt);
		cdc->TextOut(50, 40, str.GetBuffer(0),str.GetLength());
	}


	if (m_dXtilt_result <= m_dXtilt && m_dY1tilt_result <= m_dY1tilt && m_dY2tilt_result <= m_dY2tilt)
	{
		cdc->SetTextColor(RGB(0, 0, 255));
		str.Format("XDev : %.2f / Y1Dev : %.2f / Y2Dev : %.2f", m_dXtilt_result, m_dY1tilt_result, m_dY2tilt_result);
		cdc->TextOut(20, 400, str.GetBuffer(0), str.GetLength());
	}
	else
	{
		cdc->SetTextColor(RGB(255, 0, 0));
		str.Format("XDev : %.2f / Y1Dev : %.2f / Y2Dev : %.2f", m_dXtilt_result, m_dY1tilt_result, m_dY2tilt_result);
		cdc->TextOut(20, 400, str.GetBuffer(0), str.GetLength());
	}
	cdc->SelectObject(Count_old_font);
	CountFont.DeleteObject();

	//------------------Center/side 평균
// 	CFont NumericalFont, *Numerical_old_font;
// 
// 	NumericalFont.CreatePointFont(145, "Arial");
// 	cdc->SetTextColor(WHITE_COLOR);
// 	Numerical_old_font = cdc->SelectObject(&NumericalFont);
// 
// 	CenterAvgValue = CenterAvgValue / CenterCnt;
// 	MidAvgValue = MidAvgValue / MidCnt;
// 	SideAvgValue = SideAvgValue /SideCnt;
// 	
// 	str.Format("Center Average : %3.3f", CenterAvgValue);
// 	cdc->TextOut(500, CAM_IMAGE_HEIGHT-100, str.GetBuffer(0),str.GetLength());
// 	str.Format("Middle Average : %3.3f",MidAvgValue);
// 	cdc->TextOut(500, CAM_IMAGE_HEIGHT-70, str.GetBuffer(0),str.GetLength());
// 	str.Format("Side Average : %3.3f", SideAvgValue);
// 	cdc->TextOut(500, CAM_IMAGE_HEIGHT-40, str.GetBuffer(0),str.GetLength());
// 		
// 	cdc->SelectObject(Numerical_old_font);
// 	NumericalFont.DeleteObject();

}
void CSFRTILTOptionDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if(bShow == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
	}
}

void CSFRTILTOptionDlg::SearchROIArea(LPBYTE IN_RGB, int tempWidth)
{
	IplImage *OriginImage = cvCreateImage(cvSize(tempWidth,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(tempWidth,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(tempWidth,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(tempWidth,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(tempWidth,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);	
	IplImage *temp_PatternImage = cvCreateImage(cvSize(tempWidth,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);

	//IplImage *tempOriginImage = cvCreateImage(cvSize(tempWidth, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);

	BYTE R,G,B;
	double Sum_Y;

	for (int y=0; y<CAM_IMAGE_HEIGHT; y++)
	{
		for (int x=0; x<tempWidth; x++)
		{
			B = IN_RGB[y*(tempWidth*3) + x*3    ];
			G = IN_RGB[y*(tempWidth*3) + x*3 + 1];
			R = IN_RGB[y*(tempWidth*3) + x*3 + 2];
			
// 			tempOriginImage->imageData[y * tempOriginImage->widthStep + x*4 ] = B;
// 			tempOriginImage->imageData[y * tempOriginImage->widthStep + x*4 +1] = G;
// 			tempOriginImage->imageData[y * tempOriginImage->widthStep + x*4 +2] = R;

			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
		//	int value = (R+G+B)/3;

			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;
		}
	}	
	
 //	cvSaveImage("D:\\ORIGIANLCAMIMAGE.bmp", OriginImage);
//	cvReleaseImage(&tempOriginImage);
//	OriginImage = cvLoadImage("C:\\asdasd.bmp", 0);

//	Capture_Gray_SAVE("C:\\RGBIMAGE", IN_RGB, 720, 480);	
//	cvSaveImage("C:\\Default_Image.bmp", OriginImage);
	
//	BOOL WideCamMode = GetWideCamCheckFunc(RGBOrgImage);

//	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 3, 1, 1);	

//	cvCanny(SmoothImage, CannyImage, 0, 100);

	cvDilate(OriginImage, OriginImage);


	cvThreshold(OriginImage, DilateImage, 0, 255, CV_THRESH_OTSU);
	cvNot(DilateImage, DilateImage);
//	cvCanny(DilateImage, DilateImage, 0, 255);

	//cvSaveImage("D:\\DilateImage.bmp", DilateImage);

	cvCopyImage(DilateImage, temp_PatternImage);
	
	cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);
	
	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
		counter++;
	
	CvRect *rectArray = new CvRect[counter];
//	double *areaArray = new double[counter];
	CvRect rect;
	double area=0, arcCount=0;
	counter = 0;
	double old_dist = 999999;

	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);		
		double circularity = (4.0*PI*area) / (arcCount*arcCount);
		
		rect = cvContourBoundingRect(contour, 1);

		int center_x, center_y;
		center_x = rect.x + rect.width/2;
		center_y = rect.y + rect.height/2;

		BYTE R, G, B;
		B = IN_RGB[center_y*(tempWidth*3) + center_x*3    ];
		G = IN_RGB[center_y*(tempWidth * 3) + center_x * 3 + 1];
		R = IN_RGB[center_y*(tempWidth * 3) + center_x * 3 + 2];
		
		/*if(circularity > 0.7)
		{
			

			double dist = GetDistance(center_x, center_y,  2000, 1500);							
					
			if(old_dist > dist)
			{					
				m_currPt.x = center_x;
				m_currPt.y = center_y;

				old_dist = dist;

				cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 0), 1, 8);
			}				
		}
		else*/
		{
			if(rect.width < (tempWidth/4) && rect.height < (CAM_IMAGE_HEIGHT/4))
			{
				double dRatio = (double)rect.width / (double)rect.height * 100.0;
				if ((rect.width > 30 || rect.height > 30) && dRatio < 120)
				{				
					rectArray[counter] = rect;
//						areaArray[counter] = area;

					counter++;				
					
					double dist = GetDistance(center_x, center_y,  CAM_IMAGE_WIDTH/2, CAM_IMAGE_HEIGHT/2);							
					
					if(old_dist > dist)
					{					
						m_currPt.x = center_x - (CAM_IMAGE_WIDTH/2);
						m_currPt.y = center_y - (CAM_IMAGE_HEIGHT/2);
						
						m_currPt.x = m_currPt.x/2;
						m_currPt.y = m_currPt.y/2;

						old_dist = dist;
					}

					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
				}
			}
		}
	}
	

	//cvSaveImage("D:\\test\\RGBResultImage.bmp", RGBResultImage);

	int rect_cnt = 0;	
	
	if(counter > 0 && abs(m_currPt.x) < 50 && abs(m_currPt.y) < 50)
	{
		for(int q = 0; q < 50; q++)
		{		
			double old_dist = 9999999;		
			double WHRatio_Thr, HWRatio_Thr;
			double areaRatio_Thr;
			
			int shortest_index = -1;

			if(SFR_DATA[q].bEnable == TRUE)
			{
				shortest_index = -1;

				for(int k=0; k<counter; k++)
				{
					rect = rectArray[k];
					double WHRatio = (double)rect.width / (double)rect.height;
					double HWRatio = (double)rect.height / (double)rect.width;
					int White_area_cnt = 0;
					unsigned char pixel_v=0;							
					
					int curr_rect_center_x = rect.x + rect.width/2;
					int curr_rect_center_y = rect.y + rect.height/2;
					int roi_x = INIT_SFR_DATA[q].nPosX + INIT_SFR_DATA[q].nWidth/2;
					int roi_y = INIT_SFR_DATA[q].nPosY + INIT_SFR_DATA[q].nHeight/2;
				//	int curr_centerRect_center_x = center_pos_Rect.x + center_pos_Rect.width/2;
				//	int curr_centerRect_center_y = center_pos_Rect.y + center_pos_Rect.height/2;

					double dist = GetDistance(curr_rect_center_x, curr_rect_center_y,  roi_x, roi_y);							
					
					if(old_dist > dist && dist < 300)
					{					
						shortest_index = k;
						old_dist = dist;
					}				
				}
				if(shortest_index != -1)
				{
					if( SFR_DATA[q].nEdgeDir == 0)
					{
						if(rectArray[shortest_index].x - INIT_SFR_DATA[q].nWidth/2 < 0)
							SFR_DATA[q].nPosX = 0;
						else
						{
							int newPos = (rectArray[shortest_index].x - INIT_SFR_DATA[q].nWidth/2);
							int dev_x = abs(INIT_SFR_DATA[q].nPosX - newPos);
							
							if(dev_x < 100)
							{
								/*if(q==14)
									SFR_DATA[q].nPosX = (rectArray[shortest_index].x - INIT_SFR_DATA[q].nWidth/4.0);
								if(q==18)
									SFR_DATA[q].nPosX = (rectArray[shortest_index].x - INIT_SFR_DATA[q].nWidth/4.0);
								else
									SFR_DATA[q].nPosX = (rectArray[shortest_index].x - INIT_SFR_DATA[q].nWidth/2.5);*/

								//SFR_DATA[q].nPosX = (rectArray[shortest_index].x - INIT_SFR_DATA[q].nWidth/2.5); //원복
								//SFR_DATA[q].nPosX = (rectArray[shortest_index].x - INIT_SFR_DATA[q].nWidth/3); //VW버전
								SFR_DATA[q].nPosX = (rectArray[shortest_index].x - INIT_SFR_DATA[q].nWidth/2); //VW버전
							}
								
						}

						if((rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /2 < 0)
							SFR_DATA[q].nPosY = 0;
						else
						{
							int newPos = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /2;
							int dev_y = abs(INIT_SFR_DATA[q].nPosY - newPos);
							
							if(dev_y < 100){

								if(q >= 4){

									//if(q ==10){
									//	//SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /2.8; // 수정할 부분
									//	SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /2.2; // 2차수정
									//}
									//else if(q == 14)
									//	SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /1.5; // 수정할 부분
									//else if(q == 16)
									//	SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /1.5; // 수정할 부분
									//else if(q == 18)
									//	SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /2.5; // 수정할 부분
									//else
									//	SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /2.0; // 수정할 부분

									SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /2.0; //원복
								}
								else
								{
									//SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /1.5; // 수정할 부분

									//SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /2; //원복
									SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /2; //VW버전
								}

							

							}
						}
					}
					else if(SFR_DATA[q].nEdgeDir == 1)
					{
						if(rectArray[shortest_index].x + rectArray[shortest_index].width - INIT_SFR_DATA[q].nWidth/2 < 0)
							SFR_DATA[q].nPosX = 0;
						else
						{
							int newPos = rectArray[shortest_index].x + rectArray[shortest_index].width - INIT_SFR_DATA[q].nWidth/2;
							int dev_x = abs(INIT_SFR_DATA[q].nPosX - newPos);
							
							if(dev_x < 100)
							{
								/*if (q==4)
								{
									SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width - INIT_SFR_DATA[q].nWidth/1.3;
								} 
								else if (q==8)
								{
									SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width - INIT_SFR_DATA[q].nWidth/1.3;
								}
								else if (q==22)
								{
									SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width - INIT_SFR_DATA[q].nWidth/1.3;
								}
								else
								{
									SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width - INIT_SFR_DATA[q].nWidth/1.5;
								}*/

							//	SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width - INIT_SFR_DATA[q].nWidth / 1.5; //원복
								SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width - INIT_SFR_DATA[q].nWidth / 2; //원복
							}
								
						}
							
						if((rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /2 < 0)
							SFR_DATA[q].nPosY = 0;
						else
						{
							int newPos = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /2;
							int dev_y = abs(INIT_SFR_DATA[q].nPosY - newPos);
							
							if(dev_y < 100)
							{
								/*if(q==8)
									SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /1.8;
								else if(q==4) 
									SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /3.0;
								else if(q==20)
									SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /1.8;
								else
									SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /2.5;*/

								//SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /2.5; //원복
								SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height/2) - INIT_SFR_DATA[q].nHeight /2; //VW 버전

							}
								
						}
					}
					else if(SFR_DATA[q].nEdgeDir == 2)
					{
						if(rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/2 < 0)
							SFR_DATA[q].nPosX = 0;
						else
						{
							int newPos = rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/2;
							int dev_x = abs(INIT_SFR_DATA[q].nPosX - newPos);
							
							if(dev_x < 100)
							{
								/*if(q == 9)
									SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/1.5;
								else if(q==14)
									SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/2.5;
								else if(q == 21)
									SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/1.8;
								else
									SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/2.5;*/

								//SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/2.5; //원복
								SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/2; //VW버전
							}
						}

						if((rectArray[shortest_index].y) - INIT_SFR_DATA[q].nHeight /2 < 0)
							SFR_DATA[q].nPosY = 0;
						else
						{
							int newPos = (rectArray[shortest_index].y) - INIT_SFR_DATA[q].nHeight /2;
							int dev_y = abs(INIT_SFR_DATA[q].nPosY - newPos);
							
							if(dev_y < 100)
								SFR_DATA[q].nPosY = (rectArray[shortest_index].y) - INIT_SFR_DATA[q].nHeight /2;
						}
					}
					else if(SFR_DATA[q].nEdgeDir == 3)
					{
						if (q == 8)
						{
							int t = 0;
						}
						if(rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/2 < 0)
							SFR_DATA[q].nPosX = 0;
						else
						{
							int newPos = rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/2;
							int dev_x = abs(INIT_SFR_DATA[q].nPosX - newPos);
							
							if(dev_x < 100)
							{
								/*if(q==11)
									SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/2.5;
								else if(q==13)
									SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/1.8;
								else if(q==19)
									SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/2.5;
								else
									SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/1.5;*/

								//SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width/2 - INIT_SFR_DATA[q].nWidth/1.5; //원복

								//if (q == 19)
								//	SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].nWidth / 1.5; //VW버전
								//else
								//	SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].nWidth / 4; //VW버전
								SFR_DATA[q].nPosX = rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].nWidth / 2; //VW버전

							}
								
						}

						if((rectArray[shortest_index].y+rectArray[shortest_index].height) - INIT_SFR_DATA[q].nHeight /2 < 0)
							SFR_DATA[q].nPosY = 0;
						else
						{
							int newPos =  (rectArray[shortest_index].y+rectArray[shortest_index].height) - INIT_SFR_DATA[q].nHeight /2;
							int dev_y = abs(INIT_SFR_DATA[q].nPosY - newPos);
							
							if(dev_y < 100)
							{
								/*if(q==5)
									SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height) - INIT_SFR_DATA[q].nHeight /1.3;
								else if(q==1)
									SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height) - INIT_SFR_DATA[q].nHeight /1.3;
								else if(q==23)
									SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height) - INIT_SFR_DATA[q].nHeight /1.3;
								else if(q==19)
									SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height) - INIT_SFR_DATA[q].nHeight /1.3;
								else
									SFR_DATA[q].nPosY = (rectArray[shortest_index].y+rectArray[shortest_index].height) - INIT_SFR_DATA[q].nHeight /1.5;*/

							//	SFR_DATA[q].nPosY = (rectArray[shortest_index].y + rectArray[shortest_index].height) - INIT_SFR_DATA[q].nHeight / 1.5; //원복
								SFR_DATA[q].nPosY = (rectArray[shortest_index].y + rectArray[shortest_index].height) - INIT_SFR_DATA[q].nHeight / 2; //원복

							}
								
						}
					}
				}
			}	
		}
	}

//	cvSaveImage("D:\\test\\RGBResultImage.bmp", RGBResultImage);
	
	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&CannyImage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
//	cvReleaseImage(&RGBOrgImage);

	delete []rectArray;
//	delete []areaArray;
}

void CSFRTILTOptionDlg::SFR_Gen(BYTE *RGBScanBuf, int loopCnt, int SFRMODE)
{
	DWORD R0, G0, B0, Value;

	BYTE	*m_pTemp;

	double dResult	= 0;
	double dResult_buf	= 0;

	int Start_X		= 0;
	int Start_Y		= 0;
	int ImgWidth	= 0;
	int ImgHeight	= 0;

	CString str_Buf	= "";

	int nCnt = 0;

	int nWidth = CAM_IMAGE_WIDTH;

	//SearchROIArea(RGBScanBuf, nWidth);

	for(int i=0; i<34; i++)
	{
		if(SFR_DATA[i].bEnable == TRUE)
		{
			Start_X		= SFR_DATA[i].nPosX;
			Start_Y		= SFR_DATA[i].nPosY;
			ImgWidth		= SFR_DATA[i].nWidth;
			ImgHeight	= SFR_DATA[i].nHeight;

			int CntX	= 0;
			int CntY	= 0;

			//m_pTemp = new BYTE[(ImgWidth-1) * (ImgHeight-1)];
			m_pTemp = new BYTE[(ImgWidth)* (ImgHeight)];
			if(ImgWidth >ImgHeight)
			{
// 				int LimitWidth = Start_X+(ImgWidth-1);
// 				int LimitHeight = Start_Y+(ImgHeight-1);
				int LimitWidth = Start_X + (ImgWidth);
				int LimitHeight = Start_Y + (ImgHeight);

				if(LimitWidth > nWidth)
					LimitWidth = nWidth;
				if(LimitHeight > CAM_IMAGE_HEIGHT)
					LimitHeight = CAM_IMAGE_HEIGHT;

				for(int X=Start_X; X<LimitWidth; X++)
				{
					CntY = 0;
					for(int Y=Start_Y; Y<LimitHeight; Y++)
					{
						B0 = RGBScanBuf[Y * (nWidth*3) + X*3 + 0];//4->3
						G0 = RGBScanBuf[Y * (nWidth*3) + X*3 + 1];
						R0 = RGBScanBuf[Y * (nWidth*3) + X*3 + 2];

						//	Value = (R0 + G0 + B0)/3;
						Value = (BYTE)((0.29900*R0)+(0.58700*G0)+(0.11400*B0));

						//m_pTemp[(CntX * (ImgHeight-1)) + CntY] = (BYTE)Value;
						m_pTemp[(CntX * (ImgHeight)) + CntY] = (BYTE)Value;
						CntY++;
					}
					CntX++;
				}


				//dResult = GetSFRValue(m_pTemp, ImgHeight - 1, ImgWidth - 1, i, SFRMODE);
				dResult = GetSFRValue(m_pTemp, ImgHeight , ImgWidth , i, SFRMODE);
				//IplImage *test = cvCreateImage(cvSize(ImgWidth-1, ImgHeight-1), IPL_DEPTH_8U, 1);
				IplImage *test = cvCreateImage(cvSize(ImgWidth , ImgHeight ), IPL_DEPTH_8U, 1);
// 				for(int y=0; y<ImgHeight-1; y++)
// 				{
// 					for(int x=0; x<ImgWidth-1; x++)
// 					{
// 						test->imageData[y * (test->widthStep) + x] = m_pTemp[x *((ImgHeight-1)) + y];
// 
// 					}
// 				}
				for (int y = 0; y < ImgHeight; y++)
				{
					for (int x = 0; x < ImgWidth; x++)
					{
						test->imageData[y * (test->widthStep) + x] = m_pTemp[x *((ImgHeight)) + y];

					}
				}

				if(b_OverShoot == TRUE){/////////////////////// 오버슈팅 Smooth 체크박스 안들어가게 막아놈 b_OverShoot = 0;찾아봐
					cvSmooth(test, test, CV_GAUSSIAN, 3, 3, 1.0, 1.0);

// 					for(int y=0; y<ImgHeight-1; y++)
// 					{
// 						for(int x=0; x<ImgWidth-1; x++)
// 						{
// 							m_pTemp[x * (ImgHeight-1) + y] = test->imageData[y * (test->widthStep) + x];
// 
// 						}
// 					}
					for (int y = 0; y < ImgHeight; y++)
					{
						for (int x = 0; x < ImgWidth; x++)
						{
							m_pTemp[x * (ImgHeight) + y] = test->imageData[y * (test->widthStep) + x];

						}
					}
					//dResult = GetSFRValue(m_pTemp, ImgHeight - 1, ImgWidth - 1, i, SFRMODE);
					dResult = GetSFRValue(m_pTemp, ImgHeight, ImgWidth , i, SFRMODE);
				}
				//cvSaveImage("D:\\ORIGIANLCAMIMAGE_SFRGEN.bmp", test);
				cvReleaseImage(&test);


			}			
			else
			{
// 				int LimitWidth = Start_X+(ImgWidth-1);
// 				int LimitHeight = Start_Y+(ImgHeight-1);
				int LimitWidth = Start_X + (ImgWidth);
				int LimitHeight = Start_Y + (ImgHeight);

				if(LimitWidth > nWidth)
					LimitWidth = nWidth;
				if(LimitHeight > CAM_IMAGE_HEIGHT)
					LimitHeight = CAM_IMAGE_HEIGHT;

				for(int Y=Start_Y; Y<LimitHeight; Y++)
				{
					CntX = 0;
					for(int X=Start_X; X<LimitWidth; X++)
					{
						B0 = RGBScanBuf[Y * (nWidth*3) + X*3 + 0];//4->3
						G0 = RGBScanBuf[Y * (nWidth*3) + X*3 + 1];
						R0 = RGBScanBuf[Y * (nWidth*3) + X*3 + 2];

						//	Value = (R0 + G0 + B0)/3;
						Value = (BYTE)((0.29900*R0)+(0.58700*G0)+(0.11400*B0));

						//m_pTemp[(CntY * (ImgWidth-1)) + CntX] = (BYTE)Value;
						m_pTemp[(CntY * (ImgWidth)) + CntX] = (BYTE)Value;
						CntX++;
					}
					CntY++;
				}				

				//dResult = GetSFRValue(m_pTemp, ImgWidth - 1, ImgHeight - 1, i, SFRMODE);
				dResult = GetSFRValue(m_pTemp, ImgWidth , ImgHeight , i, SFRMODE);
				//IplImage *test = cvCreateImage(cvSize(ImgWidth-1, ImgHeight-1), IPL_DEPTH_8U, 1);
				IplImage *test = cvCreateImage(cvSize(ImgWidth, ImgHeight), IPL_DEPTH_8U, 1);
// 				for (int y = 0; y < ImgHeight - 1; y++)
// 				{
// 					for (int x = 0; x < ImgWidth - 1; x++)
// 					{
// 						test->imageData[y * (test->widthStep) + x] = m_pTemp[y *((ImgWidth - 1)) + x];
// 
// 					}
// 				}
				for(int y=0; y<ImgHeight; y++)
				{
					for(int x=0; x<ImgWidth; x++)
					{
						test->imageData[y * (test->widthStep) + x] = m_pTemp[y *((ImgWidth)) + x];

					}
				}
				if(b_OverShoot == TRUE){
					cvSmooth(test, test, CV_GAUSSIAN, 3, 3, 1.0, 1.0);

// 					for (int y = 0; y < ImgHeight - 1; y++)
// 					{
// 						for (int x = 0; x < ImgWidth - 1; x++)
// 						{
// 							m_pTemp[y * (ImgWidth - 1) + x] = test->imageData[y * (test->widthStep) + x];
// 
// 						}
// 					}
					for(int y=0; y<ImgHeight; y++)
					{
						for(int x=0; x<ImgWidth; x++)
						{
							m_pTemp[y * (ImgWidth) + x] = test->imageData[y * (test->widthStep) + x];

						}
					}

					//dResult = GetSFRValue(m_pTemp, ImgWidth - 1, ImgHeight - 1, i, SFRMODE);
					dResult = GetSFRValue(m_pTemp, ImgWidth, ImgHeight, i, SFRMODE);
				}				
				cvReleaseImage(&test);

			}

			//	SFR_DATA[i].dTotalData += dResult + SFR_DATA[i].dOffset;

			//	SFR_DATA[i].dResultData = SFR_DATA[i].dTotalData / loopCnt;

			SFR_DATA[i].dResultData = dResult + SFR_DATA[i].dOffset;

			delete []m_pTemp;
		}
	}
}
void	CSFRTILTOptionDlg::SFR_Gen_16bit(WORD *GRAYScanBuf, int loopCnt, int SFRMODE)
{
	DWORD R0, G0, B0, Value;

	WORD	*m_pTemp;

	double dResult = 0;
	double dResult_buf = 0;

	int Start_X = 0;
	int Start_Y = 0;
	int ImgWidth = 0;
	int ImgHeight = 0;

	CString str_Buf = "";

	int nCnt = 0;

	int nWidth = CAM_IMAGE_WIDTH;

	for (int i = 0; i < 34; i++)
	{
		if (SFR_DATA[i].bEnable == TRUE)
		{
			Start_X = SFR_DATA[i].nPosX;
			Start_Y = SFR_DATA[i].nPosY;
			ImgWidth = SFR_DATA[i].nWidth;
			ImgHeight = SFR_DATA[i].nHeight;

			int CntX = 0;
			int CntY = 0;

			//			m_pTemp = new WORD[(ImgWidth - 1) * (ImgHeight - 1)];
			m_pTemp = new WORD[(ImgWidth)* (ImgHeight)];

			if (ImgWidth > ImgHeight)
			{
				//				int LimitWidth = Start_X + (ImgWidth - 1);		// 왜 1을 뺐을까?
				// 				int LimitHeight = Start_Y + (ImgHeight - 1);
				int LimitWidth = Start_X + (ImgWidth);
				int LimitHeight = Start_Y + (ImgHeight);

				if (LimitWidth > nWidth)
					LimitWidth = nWidth;
				if (LimitHeight > CAM_IMAGE_HEIGHT)
					LimitHeight = CAM_IMAGE_HEIGHT;

				for (int X = Start_X; X < LimitWidth; X++)
				{
					CntY = 0;
					for (int Y = Start_Y; Y < LimitHeight; Y++)
					{
						// 						B0 = RGBScanBuf[Y * (nWidth * 3) + X * 3 + 0];//4->3
						// 						G0 = RGBScanBuf[Y * (nWidth * 3) + X * 3 + 1];
						// 						R0 = RGBScanBuf[Y * (nWidth * 3) + X * 3 + 2];
						// 
						// 						//	Value = (R0 + G0 + B0)/3;
						// 						Value = (BYTE)((0.29900*R0) + (0.58700*G0) + (0.11400*B0));
						// 
						// 						m_pTemp[(CntX * (ImgHeight - 1)) + CntY] = (BYTE)Value;

						//						m_pTemp[(CntX * (ImgHeight - 1)) + CntY] = GRAYScanBuf[Y*nWidth + X];
						m_pTemp[(CntX * (ImgHeight)) + CntY] = GRAYScanBuf[Y*nWidth + X];
						CntY++;
					}
					CntX++;
				}


				//dResult = GetSFRValue_16bit(m_pTemp, ImgHeight - 1, ImgWidth - 1, i, SFRMODE);
#if SFRTEST_16bit_Ver2
				dResult = GetSFRValue_16bit(m_pTemp, ImgHeight, ImgWidth, i, SFRMODE);//Ver.2
#else
				dResult = GetSFRValue_16bit(m_pTemp, ImgHeight, ImgWidth, i, m_dPixelSize, SFRMODE);//Ver.3
#endif
				//IplImage *test = cvCreateImage(cvSize(ImgWidth - 1, ImgHeight - 1), IPL_DEPTH_16U, 1);
				IplImage *test = cvCreateImage(cvSize(ImgWidth , ImgHeight ), IPL_DEPTH_16U, 1);

// 				for (int y = 0; y < ImgHeight - 1; y++)
// 				{
// 					for (int x = 0; x < ImgWidth - 1; x++)
// 					{
// 						test->imageData[y * (test->widthStep) + x] = m_pTemp[x *((ImgHeight - 1)) + y];
// 
// 					}
// 				}
				for (int y = 0; y < ImgHeight; y++)
				{
					for (int x = 0; x < ImgWidth; x++)
					{
						test->imageData[y * (test->widthStep) + x] = m_pTemp[x *((ImgHeight)) + y];

					}
				}

				if (b_OverShoot == TRUE){/////////////////////// 오버슈팅 Smooth 체크박스 안들어가게 막아놈 b_OverShoot = 0;찾아봐
					cvSmooth(test, test, CV_GAUSSIAN, 3, 3, 1.0, 1.0);

					//for (int y = 0; y < ImgHeight - 1; y++)
					//{
					//	for (int x = 0; x < ImgWidth - 1; x++)
					//	{
					//		m_pTemp[x * (ImgHeight - 1) + y] = test->imageData[y * (test->widthStep) + x];
					//
					//	}
					//}
					for (int y = 0; y < ImgHeight; y++)
					{
						for (int x = 0; x < ImgWidth; x++)
						{
							m_pTemp[x * (ImgHeight)+y] = test->imageData[y * (test->widthStep) + x];

						}
					}

					//dResult = GetSFRValue_16bit(m_pTemp, ImgHeight - 1, ImgWidth - 1, i, SFRMODE);
#if SFRTEST_16bit_Ver2
					dResult = GetSFRValue_16bit(m_pTemp, ImgHeight, ImgWidth, i, SFRMODE);//Ver.2
#else
					dResult = GetSFRValue_16bit(m_pTemp, ImgHeight, ImgWidth, i, m_dPixelSize, SFRMODE);//Ver.3
#endif
				}
				//cvSaveImage("D:\\ORIGIANLCAMIMAGE_SFRGEN.bmp", test);
				cvReleaseImage(&test);


			}
			else
			{
				// 				int LimitWidth = Start_X + (ImgWidth - 1);
				// 				int LimitHeight = Start_Y + (ImgHeight - 1);
				int LimitWidth = Start_X + (ImgWidth);
				int LimitHeight = Start_Y + (ImgHeight);

				if (LimitWidth > nWidth)
					LimitWidth = nWidth;
				if (LimitHeight > CAM_IMAGE_HEIGHT)
					LimitHeight = CAM_IMAGE_HEIGHT;

				for (int Y = Start_Y; Y < LimitHeight; Y++)
				{
					CntX = 0;
					for (int X = Start_X; X < LimitWidth; X++)
					{
						// 						B0 = RGBScanBuf[Y * (nWidth * 3) + X * 3 + 0];//4->3
						// 						G0 = RGBScanBuf[Y * (nWidth * 3) + X * 3 + 1];
						// 						R0 = RGBScanBuf[Y * (nWidth * 3) + X * 3 + 2];
						// 
						// 						//	Value = (R0 + G0 + B0)/3;
						// 						Value = (BYTE)((0.29900*R0) + (0.58700*G0) + (0.11400*B0));
						// 
						// 						m_pTemp[(CntY * (ImgWidth - 1)) + CntX] = (BYTE)Value;
						//m_pTemp[(CntY * (ImgWidth - 1)) + CntX] = GRAYScanBuf[Y*nWidth + X];
						m_pTemp[(CntY * (ImgWidth)) + CntX] = GRAYScanBuf[Y*nWidth + X];
						CntX++;
					}
					CntY++;
				}

				//dResult = GetSFRValue_16bit(m_pTemp, ImgWidth - 1, ImgHeight - 1, i, SFRMODE);
#if SFRTEST_16bit_Ver2
				dResult = GetSFRValue_16bit(m_pTemp, ImgWidth, ImgHeight, i, SFRMODE);//Ver.2
#else
				dResult = GetSFRValue_16bit(m_pTemp, ImgWidth, ImgHeight, i, m_dPixelSize, SFRMODE);//Ver.3
#endif
				//IplImage *test = cvCreateImage(cvSize(ImgWidth - 1, ImgHeight - 1), IPL_DEPTH_16U, 1);
				IplImage *test = cvCreateImage(cvSize(ImgWidth, ImgHeight), IPL_DEPTH_16U, 1);
				//for (int y = 0; y < ImgHeight - 1; y++)
				//{
				//					for (int x = 0; x < ImgWidth - 1; x++)
				//					{
				//						test->imageData[y * (test->widthStep) + x] = m_pTemp[y *((ImgWidth - 1)) + x];
				//
				//					}
				//				}
				for (int y = 0; y < ImgHeight; y++)
				{
					for (int x = 0; x < ImgWidth; x++)
					{
						test->imageData[y * (test->widthStep) + x] = m_pTemp[y *((ImgWidth)) + x];

					}
				}
				if (b_OverShoot == TRUE){
					cvSmooth(test, test, CV_GAUSSIAN, 3, 3, 1.0, 1.0);

					//for (int y = 0; y < ImgHeight - 1; y++)
					//					{
					//						for (int x = 0; x < ImgWidth - 1; x++)
					//						{
					//							m_pTemp[y * (ImgWidth - 1) + x] = test->imageData[y * (test->widthStep) + x];
					//
					//						}
					//					}
					for (int y = 0; y < ImgHeight; y++)
					{
						for (int x = 0; x < ImgWidth; x++)
						{
							m_pTemp[y * (ImgWidth)+x] = test->imageData[y * (test->widthStep) + x];

						}
					}

					//dResult = GetSFRValue_16bit(m_pTemp, ImgWidth - 1, ImgHeight - 1, i, SFRMODE);
#if SFRTEST_16bit_Ver2
					dResult = GetSFRValue_16bit(m_pTemp, ImgWidth, ImgHeight, i, SFRMODE);//Ver.2
#else
					dResult = GetSFRValue_16bit(m_pTemp, ImgWidth, ImgHeight, i, m_dPixelSize, SFRMODE);//Ver.3
#endif
				}
				cvReleaseImage(&test);

			}

			//	SFR_DATA[i].dTotalData += dResult + SFR_DATA[i].dOffset;

			//	SFR_DATA[i].dResultData = SFR_DATA[i].dTotalData / loopCnt;

			SFR_DATA[i].dResultData = dResult + SFR_DATA[i].dOffset;

			SFR_DATA[i].dResultData += 0.5;

			delete[]m_pTemp;
		}
	}
}
double CSFRTILTOptionDlg::GetSFRValue(BYTE *atemp, int width, int height, int ROI_NUM , int SFRMODE)
{
		
	int imgWidth = width;
	int imgHeight = height;	
	
	double tleft = 0;
	double tright = 0;

	double fil1[2] = {0.5, -0.5};
	double fil2[3] = {0.5, 0, -0.5};
	
	
	//if(ROI_NUM == 0)
	//{
	//	IplImage *srcImage = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);

	//	for(int y=0; y<height; y++)
	//	{
	//		for(int x=0; x<width; x++)
	//		{
	//			srcImage->imageData[y * srcImage->widthStep + x] = atemp[y * width + x];
	//		}
	//	}
	//	
	//	cvSaveImage("d:\\test\\cloneImage.bmp", srcImage);
	//	cvReleaseImage(&srcImage);
	//}

	for(int y=0; y<imgHeight; y++)
	{
		for(int x=0; x<5; x++)
		{
			tleft += (unsigned char)atemp[y * imgWidth + x];	
		}

		for(int x=imgWidth-6; x<imgWidth; x++)
		{
			tright += (unsigned char)atemp[y * imgWidth + x];	
		}
	}
		
	if(tleft > tright)
	{
		fil1[0] = -0.5;
		fil1[1] = 0.5;
		fil2[0] = -0.5;
		fil2[1] = 0;
		fil2[2] = 0.5;
	}	
	
	if( (tleft + tright) == 0.0 )
	{
	//	printf("Zero divsion!\n");
		return 0;
	}
	else
	{
		double test = fabs( (tleft - tright) / (tleft + tright) );
		
		if(test < 0.2)
		{
	//		printf("Edge contrast is less that 20%\n");
			return 0;
		}
	}	

	double n = imgWidth;
	double mid = (imgWidth)/2.0;
	double wid1 = mid;
	double wid2 = n - mid;
	double wid;
		
	if(wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	double arg=0;
	
	double *win1 = new double [imgWidth];
	
	for(int i=0; i<n; i++)
	{
		arg = i - mid;
		win1[i] = 0.54 + 0.46*cos((PI*arg / wid));
	}
	
//******************************deriv1**********************************************//
	int d_n = 2;	//필터열갯수..
	int m = imgWidth + d_n-1;	
	double sum = 0;
	double *Deriv_c = new double[imgHeight * m];	
	double *c = new double[imgWidth * imgHeight];
	
	for(int k=0; k<imgWidth * imgHeight; k++)
		c[k] = 0.0;

	for(int y=0; y<imgHeight; y++)
	{
		for(int k=0; k<imgWidth; k++)
		{
			sum = 0;
			
			for(int d_j=d_n-1; d_j>-1; d_j--)
			{
				if( (k-d_j) > -1 && (k-d_j) < imgWidth)
					sum += (double)((unsigned char)atemp[y * imgWidth + (k-d_j)])*fil1[d_j];
			}

			Deriv_c[y * imgWidth + k] = sum;
		}
	}
	
	for(int y=0; y<imgHeight; y++)
	{
		for(int k=(d_n-1); k<imgWidth; k++)
			c[y * imgWidth + k] = Deriv_c[y * imgWidth + k];
		c[y * imgWidth + d_n-2] = Deriv_c[y * imgWidth + d_n-1];
	}
	
	delete []Deriv_c;
	Deriv_c = NULL;
//************************************************************************************//
	double *loc = new double [imgHeight];
	double loc_v = 0, total_v=0;	
	
	for(int y=0; y<imgHeight; y++)
	{
		loc_v = 0;
		total_v = 0;
		for(int x=0; x<imgWidth; x++)
		{
			loc_v += (c[y * imgWidth + x] * win1[x])*(double)x;
			total_v += c[y * imgWidth + x] * win1[x];
		}
		
		if(total_v == 0 || total_v < 0.0001)
			loc[y] = 0;
		else
			loc[y] = loc_v / total_v - 0.5;
	}
	
	delete []win1;
//*****************************************************************************//	QR-DECOMP
	
//*****************************************************************************//
	int rM = imgHeight;
	int cN = 2;
	
	int i,j,rm,cn;
	int count = 0;

	rm = rM;
	cn = cN;

	double **A = new double *[rM];
	double **pinvA = new double *[cN];
	
	for(i=0; i<rM; i++)
		A[i] = new double[cN];
		
	for(i=0; i<rM; i++)
	{
		A[i][0] = count;
		A[i][1]	= 1.0;
		count++;
	}

	for(i=0; i<cN; i++)
		pinvA[i] = new double[rM];

	pInverse(A, rM, cN, pinvA);
		
	double **B = new double *[rM];
	for(i=0; i<rM; i++)
		B[i] = new double[1];
	
	for(i=0; i<rM; i++)
		B[i][0] = loc[i];			

	double **C = new double *[cN];
	for(i=0; i<cN; i++)
		C[i] = new double[1];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);

	
	printf("%10.4f\n", C[0][0]);
	printf("%10.4f\n", C[1][0]);
	

	double *place = new double[imgHeight];

	for(i=0; i<imgHeight; i++)
		place[i] = C[1][0] + C[0][0] * (double)i;
	
	double *win2 = new double [imgWidth];
	
	for(j=0; j<imgHeight; j++)
	{
//**********hamming Window***************************//
		n = imgWidth;
		mid = place[j];
		wid1 = mid - 1;
		wid2 = n - mid;
		wid;

		if(wid1 > wid2)
			wid = wid1;
		else
			wid = wid2;

		arg=0;

		for(i=0; i<n; i++)
		{
			arg = i - mid;
			win2[i] = 0.54 + 0.46*cos((PI*arg / wid));
		}

		//	for(int y=0; y<imgHeight; y++)
		{
			loc_v = 0;
			total_v = 0;
			for(int x=0; x<imgWidth; x++)
			{
				loc_v += (c[j * imgWidth + x] * win2[x])*x;
				total_v += c[j * imgWidth + x] * win2[x];
			}
			
			if(total_v == 0 || total_v < 0.0001)
				loc[j] = 0;
			else
				loc[j] = loc_v / total_v - 0.5;
		}
	}

	

	delete []win2;
	delete []place;
	delete []c;

	for(i=0; i<rM; i++)
		B[i][0] = loc[i];	
	
	MulMatrix(pinvA, B, C, cN, 1, rM, 0);
	
	for(i=0; i<rM; i++)
	{
		delete []A[i];
		delete []B[i];
	}
	delete []A;
	delete []B;
	
	for(i=0; i<cN; i++)
		delete []pinvA[i];
	delete []pinvA;

	int nbin = 4;
	double nn = imgWidth * nbin;
	double nn2 = nn/2 + 1;
	double *freq = new double [(int)nn];
	double del = 1.0;

	for(i=0; i<(int)nn; i++)
		freq[i] = (double)nbin*((double)i)/(del*nn);
	
	double freqlim = 1.0;
	double nn2out = (nn2*freqlim/2);
	double *win = new double[nbin * imgWidth];

//**********hamming Window***************************//	
	n = nbin * imgWidth;
	mid = (nbin*imgWidth+1)/2;
	wid1 = mid - 1;
	wid2 = n - mid;
	wid;

	if(wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	arg=0;

	for(i=0; i<n; i++)
	{
		arg = i - mid;
		win[i] = 0.54 + 0.46*cos((PI*arg / wid));
	}

	double fac = 4.0;
	double inProject_nn = imgWidth * fac;
	double slope = C[0][0];
//**********hamming Window***************************//
	n = inProject_nn;
	mid = fac * loc[0];
	wid1 = mid - 1;
	wid2 = n - mid;
	wid;

	if(wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	arg=0;

	for(i=0; i<n; i++)
	{
		arg = i - mid;
		win[i] = 0.54 + 0.46*cos((PI*arg / wid));
	}
//***************************************************//
	slope = 1.0/slope;
	
	double offset = fac * (0.0 - (((double)imgHeight - 1.0)/slope) );
	
	del = abs(cvRound(offset));
	if(offset > 0) offset = 0.0;
	
	
	for(i=0; i<cN; i++)
		delete []C[i];

	delete []C;
	delete []loc;

	double **barray = new double *[2];
	for(i=0; i<2; i++)
		barray[i] = new double[(int)inProject_nn + (int)del + 200];
	
	

	for(i=0; i<2; i++)
		for(j=0; j<((int)inProject_nn + (int)del + 100); j++)
			barray[i][j] = 0;
	
	

	for(int x=0; x<imgWidth; x++)
	{
		for(int y=0; y<imgHeight; y++)
		{
			int ling =  cvCeil( ((double)x - (double)y/slope)*fac ) - offset;
			
			double kkk = barray[0][ling];
			barray[0][ling] = kkk+1;
			double kk = barray[1][ling]; 
			barray[1][ling] = kk + (unsigned char)atemp[y * imgWidth + x];
		}
	}	
	
	


	double *point = new double[(int)inProject_nn];
	int start = cvRound(del*0.5);
	double nz = 0;
	int status = 1;

	for(i=start; i<start+(int)inProject_nn-1; i++)
	{
		if(barray[0][i] == 0)
		{
			nz = nz + 1;
			status = 0;
			if(i == 0)
				barray[0][i] = barray[0][i+1];
			else
				barray[0][i] = (barray[0][i-1] + barray[0][i+1])/2.0;
		}
	}
	
	if(status == 0)
	{
		printf(" Zero count(s) found during projection binning. The edge \n");
		printf(" angle may be large, or you may need more lines of data. \n");
		printf(" Execution will continue, but see Users Guide for info. \n");
	}
	
	for(i=0; i<(int)inProject_nn; i++)
	{
		if( barray[0][i+start] != 0)
			point[i] = barray[1][i+start] / barray[0][i+start];
		else
			point[i] = 0;
	}
	
	for(i=0; i<2; i++)
		delete []barray[i];

	delete []barray;
	
//	IplImage *esf_image = cvCreateImage(cvSize(inProject_nn, 255), IPL_DEPTH_8U, 1);
//	
//	cvSetZero(esf_image);
//
//	for(i=0; i<(int)inProject_nn-1; i++)
//		cvLine(esf_image, cvPoint(i, point[i]), cvPoint(i+1, point[i+1]), CV_RGB(255, 255, 255), 1, 8);
//	
//	cvFlip(esf_image);
//
////	cvShowImage("esf", esf_image);
////	cvSaveImage("d:\\test\\esf_image.bmp", esf_image);
//	
//	cvReleaseImage(&esf_image);
	
//******************************deriv1**********************************************//
	m = (int)inProject_nn;
	d_n = 3;	//필터열갯수..
	sum = 0;
	double *pointDeriv = new double[(int)inProject_nn];
	Deriv_c = new double[(int)inProject_nn+d_n-1];
	
	for(int k=0; k<(int)inProject_nn; k++)
		pointDeriv[k] = 0.0;

	for(int k=0; k<m+d_n-1; k++)
	{
		sum = 0;
		
		for(int d_j=d_n-1; d_j>-1; d_j--)
		{
			if( (k-d_j) > -1 && (k-d_j) < m)
				sum += point[k-d_j]*fil2[d_j];
		}

		Deriv_c[k] = sum;
	}
		
	for(int k=(d_n-1); k<(int)inProject_nn; k++)
		pointDeriv[k] = Deriv_c[k];
	pointDeriv[d_n-2] = Deriv_c[d_n-1];
//************************************************************************************//
	
	delete []point;
	delete []Deriv_c;

//*********************centroid*************************************//
	loc_v = 0, total_v=0;
	
	loc_v = 0;
	total_v = 0;
	for(int x=0; x<(int)inProject_nn; x++)
	{
		loc_v += pointDeriv[x]*x;
		total_v += pointDeriv[x];
	}
	
	if (total_v == 0 || total_v < 0.0001){
		delete[]freq;
		delete[]win;
		delete[]pointDeriv;
		return 0.0;
	}else{
		loc_v = loc_v/total_v;
	}

//**************************cent****************************//
	double *temp = new double[(int)inProject_nn];
	for(i=0; i<(int)inProject_nn; i++)
		temp[i] = 0.0;

	mid = cvRound( (inProject_nn+1) / 2 );
	int cent_del = cvRound( cvRound(loc_v) - mid );
	
	if(cent_del > 0)
	{
		for(i=0; i<inProject_nn-cent_del; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else if( cent_del < 1)
	{
		for(i=-cent_del; i<inProject_nn; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else
	{
		for(i=0; i<inProject_nn; i++)
			temp[i] = pointDeriv[i];
	}
	
	for(i=0; i<(int)inProject_nn; i++)
		temp[i] = win[i] * temp[i];
	
	delete []win;
	delete []pointDeriv;

// 	CvMat *sourceFFT_ = cvCreateMat((int)inProject_nn*2, 1, CV_64FC1);
// 	CvMat *resultFFT_ = cvCreateMat((int)inProject_nn*2, 1, CV_64FC1);
// 	CvMat *ConveredResultFFT_ = cvCreateMat((int)inProject_nn, 1, CV_64FC1);
// 
// 	for(i=0; i<(int)inProject_nn; i++)
// 	{
// 		sourceFFT_->data.db[i*2] = temp[i];
// 		sourceFFT_->data.db[i*2+1] = 0;
// 	}
// 
// 	delete []temp;
// 	
// 	cvDFT(sourceFFT_, resultFFT_, CV_DXT_FORWARD, 0);	
// 	
// 	for(i=0; i<(int)inProject_nn; i++)
// 	{
// 		if( i != 0)
// 			ConveredResultFFT_->data.db[i] = sqrt ( pow (resultFFT_->data.db[i*2-1], 2) + pow (resultFFT_->data.db[i*2], 2) ) ;
// 		else
// 			ConveredResultFFT_->data.db[i] = fabs(resultFFT_->data.db[i]);
// 	}
// 	
// 	double *Resultmtf_ = new double[(int)inProject_nn];
// 	int count_j_=0;
// 
// 	for(i=0; i<(int)inProject_nn; i++)
// 	{
// 	//	if( i == 0 || i%2 != 0)
// 	//	{
// 			Resultmtf_[count_j_] = fabs(ConveredResultFFT_->data.db[i])/ fabs(ConveredResultFFT_->data.db[0]);
// 			count_j_++;
// 	//	}
// 		
// 	}
// 	
// 	cvReleaseMat(&sourceFFT_);
// 	cvReleaseMat(&resultFFT_);
// 	cvReleaseMat(&ConveredResultFFT_);
// 
// 	IplImage *SFRGraph_image = cvCreateImage(cvSize(inProject_nn, 150), IPL_DEPTH_8U, 3);
// 	cvSetZero(SFRGraph_image);
// 	
// 	for(int q = 0; q<inProject_nn/2; q++)
// 	{
// 		cvLine(SFRGraph_image, cvPoint(q, cvRound(Resultmtf_[q] * 100.0)), cvPoint( (q+1), cvRound(Resultmtf_[q+1] * 100.0)), CV_RGB(255, 255, 255), 1, 8);
// 	}
// 	
// 	cvFlip(SFRGraph_image);
// 
// 	cvSaveImage("d:\\test\\SFR_GRAPH.bmp", SFRGraph_image);
//
//	cvReleaseImage(&SFRGraph_image);
//
//	double nPixel = 1000.0 / m_pDlg_SFROpt->nFieldWidth;
//	double cyPx = m_pDlg_SFROpt->nLinePerPixel / nPixel;
//
//	int half_sampling_index = -1;
//	double data, old_data= 1.0;	
//	
//	for(int q = 0; q<inProject_nn/2; q++)
//	{
//		data = fabs(freq[q] - cyPx);
//		
//		if(old_data > data)
//		{
//			old_data = data;
//			half_sampling_index = q;
//		}			
//	}	
//		
//	double resultSFR;
//
//	if( Resultmtf[half_sampling_index] < 0  || half_sampling_index == -1)
//		resultSFR = 0.0;
//	else
//		resultSFR = Resultmtf[half_sampling_index] * 100.0;
//
//	
//	delete []freq;
//	delete []Resultmtf;
////	delete []atemp;
//	
//	return resultSFR;

	CvMat *sourceFFT = cvCreateMat((int)inProject_nn*2, 1, CV_64FC1);
	CvMat *resultFFT = cvCreateMat((int)inProject_nn*2, 1, CV_64FC1);
	CvMat *ConveredResultFFT = cvCreateMat((int)inProject_nn, 1, CV_64FC1);

	for(i=0; i<(int)inProject_nn; i++)
	{
		sourceFFT->data.db[i*2] = temp[i];
		sourceFFT->data.db[i*2+1] = 0;
	}
	
	delete []temp;

	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);	

	for(i=0; i<(int)inProject_nn; i++)
	{
		if( i != 0)
		{
			ConveredResultFFT->data.db[i] = sqrt ( pow (resultFFT->data.db[i*2-1], 2) + pow (resultFFT->data.db[i*2], 2) ) ;
		}
		else
		{
			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
		}
	}

//	CvMat *sourceFFT = cvCreateMat((int)12, 1, CV_64FC1);
//	CvMat *resultFFT = cvCreateMat((int)12, 1, CV_64FC1);
//	CvMat *ConveredResultFFT = cvCreateMat((int)12, 1, CV_64FC1);
////	for(i=0; i<(int)inProject_nn; i++)
////		sourceFFT->data.db[i] = temp[i];
//	sourceFFT->data.db[0] = 0.0001;
//	sourceFFT->data.db[1] = 0;
//	sourceFFT->data.db[2] = -0.2222;
//	sourceFFT->data.db[3] = 0;
//	sourceFFT->data.db[4] = 0.1111;
//	sourceFFT->data.db[5] = 0;
//	sourceFFT->data.db[6] = 0.4444;
//	sourceFFT->data.db[7] = 0;
//	sourceFFT->data.db[8] = 0.2211;
//	sourceFFT->data.db[9] = 0;
//	sourceFFT->data.db[10] = -1.0000;
//	sourceFFT->data.db[11] = 0;
//	
//	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);
//
//	for(i=0; i<6; i++)
//	{
//		if( i != 0)
//		{
//			ConveredResultFFT->data.db[i] = sqrt ( pow (resultFFT->data.db[i*2-1], 2) + pow (resultFFT->data.db[i*2], 2) ) ;
//		}
//		else
//		{
//		//	resultFFT->data.db[i*2] = fabs(resultFFT->data.db[i*2]);
//			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
//		}
//	}

//	delete []temp;

//	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);	
	
//	double k1 = sqrt ( pow (resultFFT->data.db[5], 2) + pow (resultFFT->data.db[6], 2) ) ;

	double *Resultmtf = new double[(int)inProject_nn];
	int count_j=0;

	for(i=0; i<(int)inProject_nn; i++)
	{
	//	if( i == 0 || i%2 != 0)
	//	{
			Resultmtf[count_j] = fabs(ConveredResultFFT->data.db[i])/ fabs(ConveredResultFFT->data.db[0]);
			count_j++;
	//	}
		
	}
	
	cvReleaseMat(&sourceFFT);
	cvReleaseMat(&resultFFT);
	cvReleaseMat(&ConveredResultFFT);

#define SAVE_SFR_GRAPH	1
#if SAVE_SFR_GRAPH
	IplImage *SFRGraph_image = cvCreateImage(cvSize(inProject_nn, 150), IPL_DEPTH_8U, 3);
	cvSetZero(SFRGraph_image);
	
	for(int q = 0; q<inProject_nn/2; q++)
	{
		cvLine(SFRGraph_image, cvPoint(q, cvRound(Resultmtf[q] * 100.0)), cvPoint( (q+1), cvRound(Resultmtf[q+1] * 100.0)), CV_RGB(255, 255, 255), 1, 8);
	}
	cvFlip(SFRGraph_image);

	char	szText[64];
	CvFont font;
	cvInitFont(&font, CV_FONT_VECTOR0, 0.5, 0.5, 0, 1);

	for (int q = 0; q<inProject_nn / 2; q++)
	{
		sprintf(szText, "%.04f", Resultmtf[q]);
		cvPutText(SFRGraph_image, szText, cvPoint(q, cvRound(Resultmtf[q] * 100.0)), &font, CV_RGB(255, 0, 0));
		
	}
	cvSaveImage("d:\\test\\SFR_GRAPH.bmp", SFRGraph_image);
	
	cvReleaseImage(&SFRGraph_image);
#endif

	double resultSFR;

	if (SFRMODE == 0){

		// MTF를 결과로 하는 방식
		double nPixel = 1000.0 / m_dPixelSize;
		double cyPx = SFR_DATA[ROI_NUM].dLinepare / nPixel;
		//cyPx *=2.23;

		int half_sampling_index = -1;
		double data, old_data = 1.0;

		for (int q = 0; q<inProject_nn / 2; q++)
		{
			data = fabs(freq[q] - cyPx);

			if (old_data > data)
			{
				old_data = data;
				half_sampling_index = q;
			}
		}

		if (Resultmtf[half_sampling_index] < 0 || half_sampling_index == -1)
			resultSFR = 0.0;
		else
			resultSFR = Resultmtf[half_sampling_index];
	}
	else{


		// cypx를 결과로 하는 방식
#if 0
		// 0.5에 가장 근접한 위치를 찾는 방식
		double Min_Gap = 999.999;
		int result = 0;
		int resultBack = 0;//2번째로 가까운 값
		double m_MTF = (double)SFR_DATA[ROI_NUM].MTF / 100;

		for (int i = 0; i < (int)inProject_nn / 2; i++)
		{
			TRACE("Resultmtf[%d]-- %f\n", i, Resultmtf[i]);
			TRACE("freq[%d];-- %f\n", i, freq[i]);
			double Temp = m_MTF - Resultmtf[i];
			if (abs(Temp) < Min_Gap)
			{
				Min_Gap = abs(Temp);
				resultBack = result;
				result = i;
			}
		}

		resultSFR = freq[result];
#endif

		// 0.5 의 근사치 위치의 값을 양쪽 값을 이용하여 계산하는 방식
		double	dbBefore, dbNext;
		int		nBefore, nNext;

		dbBefore = dbNext = 1.0;
		nBefore = nNext = 0;
		for (int i = 0; i < (int)inProject_nn; i++)
		{
			nNext = i;
			dbNext = Resultmtf[i];
			TRACE("Resultmtf[%d]-- %f\n", i, dbNext);
			if (dbNext < 0.5)
				break;
			dbBefore = dbNext;
			nBefore = nNext;
		}

		double dbBeforeFreq = freq[nBefore];
		double dbNextFreq = freq[nNext];

		//	resultSFR = (0.5 - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq) + dbBeforeFreq;
		// 그래프가 반비례 형태임으로 아래 형태 계산이 더 근접한 계산
		resultSFR = dbNextFreq - (0.5 - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq);

		TRACE("Result mtf range p[%d,%d] - value: [%f,%f] / [%f,%f] --> result SFR: %f \n"
			, nBefore, nNext, dbBefore, dbNext, dbBeforeFreq, dbNextFreq, resultSFR);

		if (resultSFR > 0.5)
		{
			resultSFR = 0.0;
		}
		//////////////////////// 끝

		// cymm를 결과로 하는 방식
		if (SFRMODE == 2){
			resultSFR = (resultSFR / m_dPixelSize) * 1000;
		}
	}

	
	delete []freq;
	delete []Resultmtf;
//	delete []atemp;
	
	return resultSFR;
}
double CSFRTILTOptionDlg::GetSFRValue_16bit(WORD *atemp, int width, int height, int ROI_NUM, int SFRMODE)
{

	int imgWidth = width;
	int imgHeight = height;

	double tleft = 0;
	double tright = 0;

	double fil1[2] = { 0.5, -0.5 };
	double fil2[3] = { 0.5, 0, -0.5 };


	for (int y = 0; y<imgHeight; y++)
	{
		for (int x = 0; x<5; x++)
		{
			tleft += (int)atemp[y * imgWidth + x];
		}

		for (int x = imgWidth - 6; x<imgWidth; x++)
		{
			tright += (int)atemp[y * imgWidth + x];
		}
	}

	if (tleft > tright)
	{
		fil1[0] = -0.5;
		fil1[1] = 0.5;
		fil2[0] = -0.5;
		fil2[1] = 0;
		fil2[2] = 0.5;
	}

	if ((tleft + tright) == 0.0)
	{
		//	printf("Zero divsion!\n");
		return 0;
	}
	else
	{
		double test = fabs((tleft - tright) / (tleft + tright));

		if (test < 0.2)
		{
			//		printf("Edge contrast is less that 20%\n");
			return 0;
		}
	}

	double n = imgWidth;
	double mid = (imgWidth) / 2.0;
	double wid1 = mid;
	double wid2 = n - mid;
	double wid;

	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	double arg = 0;

	double *win1 = new double[imgWidth];

	for (int i = 0; i<n; i++)
	{
		arg = i - mid;
		win1[i] = 0.54 + 0.46*cos((PI*arg / wid));
	}

	//******************************deriv1**********************************************//
	int d_n = 2;	//필터열갯수..
	int m = imgWidth + d_n - 1;
	double sum = 0;
	double *Deriv_c = new double[imgHeight * m];
	double *c = new double[imgWidth * imgHeight];

	for (int k = 0; k<imgWidth * imgHeight; k++)
		c[k] = 0.0;

	for (int y = 0; y<imgHeight; y++)
	{
		for (int k = 0; k<imgWidth; k++)
		{
			sum = 0;

			for (int d_j = d_n - 1; d_j>-1; d_j--)
			{
				if ((k - d_j) > -1 && (k - d_j) < imgWidth)
					sum += (double)(atemp[y * imgWidth + (k - d_j)])*fil1[d_j];
			}

			Deriv_c[y * imgWidth + k] = sum;
		}
	}

	for (int y = 0; y<imgHeight; y++)
	{
		for (int k = (d_n - 1); k<imgWidth; k++)
			c[y * imgWidth + k] = Deriv_c[y * imgWidth + k];
		c[y * imgWidth + d_n - 2] = Deriv_c[y * imgWidth + d_n - 1];
	}

	delete[]Deriv_c;
	Deriv_c = NULL;
	//************************************************************************************//
	double *loc = new double[imgHeight];
	double loc_v = 0, total_v = 0;

	for (int y = 0; y<imgHeight; y++)
	{
		loc_v = 0;
		total_v = 0;
		for (int x = 0; x<imgWidth; x++)
		{
			loc_v += (c[y * imgWidth + x] * win1[x])*(double)x;
			total_v += c[y * imgWidth + x] * win1[x];
		}

		if (total_v == 0 || total_v < 0.0001)
			loc[y] = 0;
		else
			loc[y] = loc_v / total_v - 0.5;
	}

	delete[]win1;
	//*****************************************************************************//	QR-DECOMP

	//*****************************************************************************//
	int rM = imgHeight;
	int cN = 2;

	int i, j, rm, cn;
	int count = 0;

	rm = rM;
	cn = cN;

	double **A = new double *[rM];
	double **pinvA = new double *[cN];

	for (i = 0; i<rM; i++)
		A[i] = new double[cN];

	for (i = 0; i<rM; i++)
	{
		A[i][0] = count;
		A[i][1] = 1.0;
		count++;
	}

	for (i = 0; i<cN; i++)
		pinvA[i] = new double[rM];

	pInverse(A, rM, cN, pinvA);

	double **B = new double *[rM];
	for (i = 0; i<rM; i++)
		B[i] = new double[1];

	for (i = 0; i<rM; i++)
		B[i][0] = loc[i];

	double **C = new double *[cN];
	for (i = 0; i<cN; i++)
		C[i] = new double[1];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);


	printf("%10.4f\n", C[0][0]);
	printf("%10.4f\n", C[1][0]);


	double *place = new double[imgHeight];

	for (i = 0; i<imgHeight; i++)
		place[i] = C[1][0] + C[0][0] * (double)i;

	double *win2 = new double[imgWidth];

	for (j = 0; j<imgHeight; j++)
	{
		//**********hamming Window***************************//
		n = imgWidth;
		mid = place[j];
		wid1 = mid - 1;
		wid2 = n - mid;
		wid;

		if (wid1 > wid2)
			wid = wid1;
		else
			wid = wid2;

		arg = 0;

		for (i = 0; i<n; i++)
		{
			arg = i - mid;
			win2[i] = 0.54 + 0.46*cos((PI*arg / wid));
		}

		//	for(int y=0; y<imgHeight; y++)
		{
			loc_v = 0;
			total_v = 0;
			for (int x = 0; x<imgWidth; x++)
			{
				loc_v += (c[j * imgWidth + x] * win2[x])*x;
				total_v += c[j * imgWidth + x] * win2[x];
			}

			if (total_v == 0 || total_v < 0.0001)
				loc[j] = 0;
			else
				loc[j] = loc_v / total_v - 0.5;
		}
	}



	delete[]win2;
	delete[]place;
	delete[]c;

	for (i = 0; i<rM; i++)
		B[i][0] = loc[i];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);

	for (i = 0; i<rM; i++)
	{
		delete[]A[i];
		delete[]B[i];
	}
	delete[]A;
	delete[]B;

	for (i = 0; i<cN; i++)
		delete[]pinvA[i];
	delete[]pinvA;

	int nbin = 4;
	double nn = imgWidth * nbin;
	double nn2 = nn / 2 + 1;
	double *freq = new double[(int)nn];
	double del = 1.0;

	for (i = 0; i < (int)nn; i++){
		freq[i] = (double)nbin*((double)i) / (del*nn);
		TRACE("freq[%d];-- %f\n", i, freq[i]);
	}

	double freqlim = 1.0;
	double nn2out = (nn2*freqlim / 2);
	double *win = new double[nbin * imgWidth];

	//**********hamming Window***************************//	
	n = nbin * imgWidth;
	mid = (nbin*imgWidth + 1) / 2;
	wid1 = mid - 1;
	wid2 = n - mid;
	wid;

	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	arg = 0;

	for (i = 0; i<n; i++)
	{
		arg = i - mid;
		win[i] = 0.54 + 0.46*cos((PI*arg / wid));
	}

	double fac = 4.0;
	double inProject_nn = imgWidth * fac;
	double slope = C[0][0];
	//**********hamming Window***************************//
	n = inProject_nn;
	mid = fac * loc[0];
	wid1 = mid - 1;
	wid2 = n - mid;
	wid;

	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	arg = 0;

	for (i = 0; i<n; i++)
	{
		arg = i - mid;
		win[i] = 0.54 + 0.46*cos((PI * arg / wid));
	}
	//***************************************************//
	slope = 1.0 / slope;

	double offset = fac * (0.0 - (((double)imgHeight - 1.0) / slope));

	del = abs(cvRound(offset));
	if (offset > 0) offset = 0.0;


	for (i = 0; i<cN; i++)
		delete[]C[i];

	delete[]C;
	delete[]loc;

	double **barray = new double *[2];
	for (i = 0; i<2; i++)
		barray[i] = new double[(int)inProject_nn + (int)del + 200];



	for (i = 0; i<2; i++)
	for (j = 0; j<((int)inProject_nn + (int)del + 100); j++)
		barray[i][j] = 0;



	for (int x = 0; x<imgWidth; x++)
	{
		for (int y = 0; y<imgHeight; y++)
		{
			int ling = cvCeil(((double)x - (double)y / slope)*fac) - offset;

			double kkk = barray[0][ling];
			barray[0][ling] = kkk + 1;
			double kk = barray[1][ling];
			barray[1][ling] = kk + atemp[y * imgWidth + x];
		}
	}




	double *point = new double[(int)inProject_nn];
	int start = cvRound(del*0.5);
	double nz = 0;
	int status = 1;

	for (i = start; i<start + (int)inProject_nn - 1; i++)
	{
		if (barray[0][i] == 0)
		{
			nz = nz + 1;
			status = 0;
			if (i == 0)
				barray[0][i] = barray[0][i + 1];
			else
				barray[0][i] = (barray[0][i - 1] + barray[0][i + 1]) / 2.0;
		}
	}

	if (status == 0)
	{
		printf(" Zero count(s) found during projection binning. The edge \n");
		printf(" angle may be large, or you may need more lines of data. \n");
		printf(" Execution will continue, but see Users Guide for info. \n");
	}

	for (i = 0; i<(int)inProject_nn; i++)
	{
		if (barray[0][i + start] != 0)
			point[i] = barray[1][i + start] / barray[0][i + start];
		else
			point[i] = 0;
	}

	for (i = 0; i<2; i++)
		delete[]barray[i];

	delete[]barray;

	//	IplImage *esf_image = cvCreateImage(cvSize(inProject_nn, 255), IPL_DEPTH_8U, 1);
	//	
	//	cvSetZero(esf_image);
	//
	//	for(i=0; i<(int)inProject_nn-1; i++)
	//		cvLine(esf_image, cvPoint(i, point[i]), cvPoint(i+1, point[i+1]), CV_RGB(255, 255, 255), 1, 8);
	//	
	//	cvFlip(esf_image);
	//
	////	cvShowImage("esf", esf_image);
	////	cvSaveImage("d:\\test\\esf_image.bmp", esf_image);
	//	
	//	cvReleaseImage(&esf_image);

	//******************************deriv1**********************************************//
	m = (int)inProject_nn;
	d_n = 3;	//필터열갯수..
	sum = 0;
	double *pointDeriv = new double[(int)inProject_nn];
	Deriv_c = new double[(int)inProject_nn + d_n - 1];

	for (int k = 0; k<(int)inProject_nn; k++)
		pointDeriv[k] = 0.0;

	for (int k = 0; k<m + d_n - 1; k++)
	{
		sum = 0;

		for (int d_j = d_n - 1; d_j>-1; d_j--)
		{
			if ((k - d_j) > -1 && (k - d_j) < m)
				sum += point[k - d_j] * fil2[d_j];
		}

		Deriv_c[k] = sum;
	}

	for (int k = (d_n - 1); k<(int)inProject_nn; k++)
		pointDeriv[k] = Deriv_c[k];
	pointDeriv[d_n - 2] = Deriv_c[d_n - 1];
	//************************************************************************************//

	delete[]point;
	delete[]Deriv_c;

	//*********************centroid*************************************//
	loc_v = 0, total_v = 0;

	loc_v = 0;
	total_v = 0;
	for (int x = 0; x<(int)inProject_nn; x++)
	{
		loc_v += pointDeriv[x] * x;
		total_v += pointDeriv[x];
	}

	if (total_v == 0 || total_v < 0.0001){
		delete[]freq;
		delete[]win;
		delete[]pointDeriv;
		return 0.0;
	}
	else{
		loc_v = loc_v / total_v;
	}

	//**************************cent****************************//
	double *temp = new double[(int)inProject_nn];
	for (i = 0; i<(int)inProject_nn; i++)
		temp[i] = 0.0;

	mid = cvRound((inProject_nn + 1) / 2);
	int cent_del = cvRound(cvRound(loc_v) - mid);

	if (cent_del > 0)
	{
		for (i = 0; i<inProject_nn - cent_del; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else if (cent_del < 1)
	{
		for (i = -cent_del; i<inProject_nn; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else
	{
		for (i = 0; i<inProject_nn; i++)
			temp[i] = pointDeriv[i];
	}

	for (i = 0; i<(int)inProject_nn; i++)
		temp[i] = win[i] * temp[i];

	delete[]win;
	delete[]pointDeriv;

// 	CvMat *sourceFFT_ = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
// 	CvMat *resultFFT_ = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
// 	CvMat *ConveredResultFFT_ = cvCreateMat((int)inProject_nn, 1, CV_64FC1);
// 
// 	for (i = 0; i < (int)inProject_nn; i++)
// 	{
// 		sourceFFT_->data.db[i * 2] = temp[i];
// 		sourceFFT_->data.db[i * 2 + 1] = 0;
// 	}
// 
// 	delete[]temp;
// 
// 	cvDFT(sourceFFT_, resultFFT_, CV_DXT_FORWARD, 0);
// 
// 	for (i = 0; i < (int)inProject_nn; i++)
// 	{
// 		if (i != 0)
// 			ConveredResultFFT_->data.db[i] = sqrt(pow(resultFFT_->data.db[i * 2 - 1], 2) + pow(resultFFT_->data.db[i * 2], 2));
// 		else
// 			ConveredResultFFT_->data.db[i] = fabs(resultFFT_->data.db[i]);
// 	}
// 
// 	double *Resultmtf_ = new double[(int)inProject_nn];
// 	int count_j_ = 0;
// 
// 	for (i = 0; i < (int)inProject_nn; i++)
// 	{
// 		//	if( i == 0 || i%2 != 0)
// 		//	{
// 		Resultmtf_[count_j_] = fabs(ConveredResultFFT_->data.db[i]) / fabs(ConveredResultFFT_->data.db[0]);
// 		count_j_++;
// 		//	}
// 
// 	}
// 
// 	cvReleaseMat(&sourceFFT_);
// 	cvReleaseMat(&resultFFT_);
// 	cvReleaseMat(&ConveredResultFFT_);
// 
// 	IplImage *SFRGraph_image = cvCreateImage(cvSize(inProject_nn, 150), IPL_DEPTH_8U, 3);
// 	cvSetZero(SFRGraph_image);
// 
// 	for (int q = 0; q < inProject_nn / 2; q++)
// 	{
// 		cvLine(SFRGraph_image, cvPoint(q, cvRound(Resultmtf_[q] * 100.0)), cvPoint((q + 1), cvRound(Resultmtf_[q + 1] * 100.0)), CV_RGB(255, 255, 255), 1, 8);
// 	}
// 
// 	cvFlip(SFRGraph_image);
// 
// 	cvSaveImage("d:\\test\\SFR_GRAPH.bmp", SFRGraph_image);
// 	cvReleaseImage(&SFRGraph_image);
	//
	//	double nPixel = 1000.0 / m_pDlg_SFROpt->nFieldWidth;
	//	double cyPx = m_pDlg_SFROpt->nLinePerPixel / nPixel;
	//
	//	int half_sampling_index = -1;
	//	double data, old_data= 1.0;	
	//	
	//	for(int q = 0; q<inProject_nn/2; q++)
	//	{
	//		data = fabs(freq[q] - cyPx);
	//		
	//		if(old_data > data)
	//		{
	//			old_data = data;
	//			half_sampling_index = q;
	//		}			
	//	}	
	//		
	//	double resultSFR;
	//
	//	if( Resultmtf[half_sampling_index] < 0  || half_sampling_index == -1)
	//		resultSFR = 0.0;
	//	else
	//		resultSFR = Resultmtf[half_sampling_index] * 100.0;
	//
	//	
	//	delete []freq;
	//	delete []Resultmtf;
	////	delete []atemp;
	//	
	//	return resultSFR;

	CvMat *sourceFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	CvMat *resultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	CvMat *ConveredResultFFT = cvCreateMat((int)inProject_nn, 1, CV_64FC1);

	for (i = 0; i<(int)inProject_nn; i++)
	{
		sourceFFT->data.db[i * 2] = temp[i];
		sourceFFT->data.db[i * 2 + 1] = 0;
	}

	delete[]temp;

	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);

	for (i = 0; i<(int)inProject_nn; i++)
	{
		if (i != 0)
		{
			ConveredResultFFT->data.db[i] = sqrt(pow(resultFFT->data.db[i * 2 - 1], 2) + pow(resultFFT->data.db[i * 2], 2));
		}
		else
		{
			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
		}
	}

	//	CvMat *sourceFFT = cvCreateMat((int)12, 1, CV_64FC1);
	//	CvMat *resultFFT = cvCreateMat((int)12, 1, CV_64FC1);
	//	CvMat *ConveredResultFFT = cvCreateMat((int)12, 1, CV_64FC1);
	////	for(i=0; i<(int)inProject_nn; i++)
	////		sourceFFT->data.db[i] = temp[i];
	//	sourceFFT->data.db[0] = 0.0001;
	//	sourceFFT->data.db[1] = 0;
	//	sourceFFT->data.db[2] = -0.2222;
	//	sourceFFT->data.db[3] = 0;
	//	sourceFFT->data.db[4] = 0.1111;
	//	sourceFFT->data.db[5] = 0;
	//	sourceFFT->data.db[6] = 0.4444;
	//	sourceFFT->data.db[7] = 0;
	//	sourceFFT->data.db[8] = 0.2211;
	//	sourceFFT->data.db[9] = 0;
	//	sourceFFT->data.db[10] = -1.0000;
	//	sourceFFT->data.db[11] = 0;
	//	
	//	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);
	//
	//	for(i=0; i<6; i++)
	//	{
	//		if( i != 0)
	//		{
	//			ConveredResultFFT->data.db[i] = sqrt ( pow (resultFFT->data.db[i*2-1], 2) + pow (resultFFT->data.db[i*2], 2) ) ;
	//		}
	//		else
	//		{
	//		//	resultFFT->data.db[i*2] = fabs(resultFFT->data.db[i*2]);
	//			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
	//		}
	//	}

	//	delete []temp;

	//	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);	

	//	double k1 = sqrt ( pow (resultFFT->data.db[5], 2) + pow (resultFFT->data.db[6], 2) ) ;

	double *Resultmtf = new double[(int)inProject_nn];
	int count_j = 0;

	for (i = 0; i<(int)inProject_nn; i++)
	{
		//	if( i == 0 || i%2 != 0)
		//	{
		Resultmtf[count_j] = fabs(ConveredResultFFT->data.db[i]) / fabs(ConveredResultFFT->data.db[0]);
		count_j++;
		//	}

	}

	cvReleaseMat(&sourceFFT);
	cvReleaseMat(&resultFFT);
	cvReleaseMat(&ConveredResultFFT);

	// SFRGraph_image
#define SAVE_SFR_GRAPH	1
#if SAVE_SFR_GRAPH
	IplImage *SFRGraph_image = cvCreateImage(cvSize(inProject_nn*10, 110*10), IPL_DEPTH_8U, 3);
	cvSetZero(SFRGraph_image);

	for (int q = 0; q<inProject_nn / 2; q++)
	{
		cvLine(SFRGraph_image, cvPoint(q * 10, cvRound(Resultmtf[q] * 1000.0)), cvPoint((q + 1)*10, cvRound(Resultmtf[q + 1] * 1000.0)), CV_RGB(255, 255, 255), 1, 8);
	}
	cvFlip(SFRGraph_image);

	char	szText[64];
	CvFont* font = (CvFont*)malloc(sizeof(CvFont));
	cvInitFont(font, CV_FONT_VECTOR0, 0.5, 0.5, 0, 1);

	for (int q = 0; q<inProject_nn / 2; q++)
	{
		sprintf(szText, "%.04f", Resultmtf[q]);
		cvPutText(SFRGraph_image, szText, cvPoint(q*10 - 5, 1100 - cvRound(Resultmtf[q] * 1000.0)), font, CV_RGB(255, 0, 0));

	}

	sprintf(szText, "d:\\test\\SFR_GRAPH_%03d.bmp", ROI_NUM);
	cvSaveImage(szText, SFRGraph_image);

	cvReleaseImage(&SFRGraph_image);
#endif
	// SFRGraph_image 

	double resultSFR;

	if (SFRMODE == 0){

		// MTF를 결과로 하는 방식
		double nPixel = 1000.0 / m_dPixelSize;
		double cyPx = SFR_DATA[ROI_NUM].dLinepare / nPixel;
		//cyPx *=2.23;

		int half_sampling_index = -1;
		double data, old_data = 1.0;

		for (int q = 0; q<inProject_nn / 2; q++)
		{
			data = fabs(freq[q] - cyPx);

			if (old_data > data)
			{
				old_data = data;
				half_sampling_index = q;
			}
		}

		if (Resultmtf[half_sampling_index] < 0 || half_sampling_index == -1)
			resultSFR = 0.0;
		else
			resultSFR = Resultmtf[half_sampling_index];
	}
	else{


		// cypx를 결과로 하는 방식
#if 0
	// 0.5에 가장 근접한 위치를 찾는 방식
		double Min_Gap = 999.999;
		int result = 0;
		int resultBack = 0;//2번째로 가까운 값
		double m_MTF = (double) SFR_DATA[ROI_NUM].MTF / 100;

		for (int i = 0; i < (int)inProject_nn / 2; i++)
		{
			TRACE("Resultmtf[%d]-- %f\n", i, Resultmtf[i]);
			TRACE("freq[%d];-- %f\n", i, freq[i]);
			double Temp = m_MTF - Resultmtf[i];
			if (abs(Temp) < Min_Gap)
			{
				Min_Gap = abs(Temp);
				resultBack = result;
				result = i;
			}
		}

		resultSFR = freq[result];
#endif

		// 0.5 의 근사치 위치의 값을 양쪽 값을 이용하여 계산하는 방식
		double	dbBefore, dbNext;
		int		nBefore, nNext;

		dbBefore = dbNext = 1.0;
		nBefore = nNext = 0;
		for (int i = 0; i < (int)inProject_nn; i++)
		{
			nNext = i;
			dbNext = Resultmtf[i];
			TRACE("Resultmtf[%d]-- %f\n", i, dbNext);
			if (dbNext < 0.5)
				break;
			dbBefore = dbNext;
			nBefore = nNext;
		}

		double dbBeforeFreq = freq[nBefore];
		double dbNextFreq = freq[nNext];

		//	resultSFR = (0.5 - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq) + dbBeforeFreq;
		// 그래프가 반비례 형태임으로 아래 형태 계산이 더 근접한 계산
		resultSFR = dbNextFreq - (0.5 - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq);

		TRACE("Result mtf range p[%d,%d] - value: [%f,%f] / [%f,%f] --> result SFR: %f \n"
			, nBefore, nNext, dbBefore, dbNext, dbBeforeFreq, dbNextFreq, resultSFR);

		if (resultSFR > 0.5)
		{
			resultSFR = 0.0;
		}
		//////////////////////// 끝

		// cymm를 결과로 하는 방식
		if (SFRMODE == 2){
			resultSFR = (resultSFR / m_dPixelSize) * 1000;
		}
	}



	delete[]freq;
	delete[]Resultmtf;
	//	delete []atemp;

	return resultSFR;
}
double CSFRTILTOptionDlg::GetSFRValue_16bit(WORD *atemp, int width, int height, int ROI_NUM, double dPixelSize, int SFRMODE)
{

	int imgWidth = width;
	int imgHeight = height;

	double tleft = 0;
	double tright = 0;

	double fil1[2] = { 0.5, -0.5 };
	double fil2[3] = { 0.5, 0, -0.5 };
	double resultSFR;

#define adjcont		0
#if adjcont == 1
	WORD	wMaxValue = 0;
	for (int y = 0; y < imgHeight; y++)
	for (int x = 0; x < imgWidth; x++)
	if (wMaxValue < atemp[y * imgWidth + x])
		wMaxValue = atemp[y * imgWidth + x];

	WORD	nAdjValue = 0xFFFF / (wMaxValue + 1);
	for (int y = 0; y < imgHeight; y++)
	for (int x = 0; x < imgWidth; x++)
		atemp[y * imgWidth + x] *= nAdjValue;
#endif

	//rotatev2가 들어가야 함.

	for (int y = 0; y < imgHeight; y++)
	{
		for (int x = 0; x < 5; x++)
		{
			tleft += (int)atemp[y * imgWidth + x];
		}

		for (int x = imgWidth - 6; x<imgWidth; x++)
		{
			tright += (int)atemp[y * imgWidth + x];
		}
	}

	if (tleft > tright)
	{
		fil1[0] = -0.5;
		fil1[1] = 0.5;
		fil2[0] = -0.5;
		fil2[1] = 0;
		fil2[2] = 0.5;
	}

	if ((tleft + tright) == 0.0)
	{
		//	printf("Zero divsion!\n");
		return 0;
	}
	else
	{
		double test = fabs((tleft - tright) / (tleft + tright));

		if (test < 0.2)
		{
			//		printf("Edge contrast is less that 20%\n");
			return 0;
		}
	}

	double n = imgWidth;
	//	double mid = (imgWidth) / 2.0;
	double mid = (imgWidth + 1) / 2.0;	// MatLab SFRmat3 에서는 +1을 해줌. #Lucas수정1

	/////////////////////////// ahamming start
	//	double wid1 = mid;
	double wid1 = mid - 1;		// // MatLab SFRmat3 에서는 -1을 해줌. #Lucas수정2
	double wid2 = n - mid;
	double wid;
	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	double arg = 0;

	double *win1 = new double[imgWidth];

	for (int i = 0; i < n; i++)
	{
		//		arg = i - mid;
		arg = i - mid + 1;	// MatLab은 1 base임으로, 0 base 시 1을 더해줌. #Lucas수정3
		win1[i] = 0.54 + 0.46*cos((PI*arg / wid));
	}
	/////////////////////////// ahamming end // 여기까지 sfrmat3와 동일 결과.

	//******************************deriv1**********************************************//
	// todo: #Lucas deriv1의 결과 값이 Matlab과 다름. 디버깅이 필요한데 소스가 달라 복잡함.
	int d_n = 2;	//필터열갯수..
	int m = imgWidth + d_n - 1;
	double sum = 0;
	double *Deriv_c = new double[imgHeight * m];
	double *c = new double[imgWidth * imgHeight];

	for (int k = 0; k < imgWidth * imgHeight; k++)
		c[k] = 0.0;

	for (int y = 0; y<imgHeight; y++)
	{
		for (int k = 0; k<imgWidth; k++)
		{
			sum = 0;

			for (int d_j = d_n - 1; d_j>-1; d_j--)
			{
				if ((k - d_j) > -1 && (k - d_j) < imgWidth)
					sum += (double)(atemp[y * imgWidth + (k - d_j)]) * fil1[d_j];
				//				sum += (double)((unsigned char)atemp[y * imgWidth + (k - d_j)]) * fil1[d_j];
			}

			Deriv_c[y * imgWidth + k] = sum;

		}
	}

	for (int y = 0; y < imgHeight; y++)
	{
		for (int k = (d_n - 1); k < imgWidth; k++)
			c[y * imgWidth + k] = Deriv_c[y * imgWidth + k];
		c[y * imgWidth + d_n - 2] = Deriv_c[y * imgWidth + d_n - 1];
	}

	delete[]Deriv_c;
	Deriv_c = NULL;
	//************************************************************************************//
	double *loc = new double[imgHeight];
	double loc_v = 0, total_v = 0;

	for (int y = 0; y < imgHeight; y++)
	{
		loc_v = 0;
		total_v = 0;
		for (int x = 0; x < imgWidth; x++)
		{
			loc_v += ((double)c[y * imgWidth + x] * win1[x])*((double)x + 1);	//수정 //MATLAB과 최대한 mid position을 맞춰주자..
			total_v += (double)c[y * imgWidth + x] * win1[x];
		}

		if (total_v == 0 || total_v < 0.0001)
			loc[y] = 0;
		else
			loc[y] = loc_v / total_v - 0.5;
	}

	delete[]win1;
	//*****************************************************************************//	QR-DECOMP

	//*****************************************************************************//
	int rM = imgHeight;
	int cN = 2;

	int i, j, rm, cn;
	int count = 0;

	rm = rM;
	cn = cN;

	double **A = new double *[rM];
	double **pinvA = new double *[cN];

	for (i = 0; i < rM; i++)
		A[i] = new double[cN];

	for (i = 0; i < rM; i++)
	{
		A[i][0] = count;
		A[i][1] = 1.0;
		count++;
	}

	for (i = 0; i < cN; i++)
		pinvA[i] = new double[rM];

	pInverse(A, rM, cN, pinvA);

	double **B = new double *[rM];
	for (i = 0; i < rM; i++)
		B[i] = new double[1];

	for (i = 0; i < rM; i++)
		B[i][0] = loc[i];

	double **C = new double *[cN];
	for (i = 0; i < cN; i++)
		C[i] = new double[1];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);


	//	printf("MulMatrix C[0][0]: %10.4f\n", C[0][0]);
	//	printf("MulMatrix C[1][0]: %10.4f\n", C[1][0]);


	double *place = new double[imgHeight];

	//	for (i = 0; i < imgHeight; i++)
	//		place[i] = C[1][0] + C[0][0] * (double)(i+1);

	double *win2 = new double[imgWidth];

	for (j = 0; j<imgHeight; j++)
	{
		//**********hamming Window***************************//
		place[j] = C[1][0] + C[0][0] * (double)(j + 1);

		n = imgWidth;
		mid = place[j];
		wid1 = mid - 1;
		wid2 = n - mid;

		if (wid1 > wid2)
			wid = wid1;
		else
			wid = wid2;

		arg = 0;

		for (i = 0; i < n; i++)
		{
			arg = (i + 1) - mid;
			//	win2[i] = 0.54 + 0.46*cos((PI*arg / wid));
			win2[i] = cos((PI*arg / wid));
		}

		for (i = 0; i < n; i++)
			win2[i] = 0.54 + 0.46*win2[i];

		//	for(int y=0; y<imgHeight; y++)
		{
			loc_v = 0;
			total_v = 0;

			for (int x = 0; x<imgWidth; x++)
				total_v += c[j * imgWidth + x] * win2[x];

			for (int x = 0; x < imgWidth; x++)
			{
				loc_v += (c[j * imgWidth + x] * win2[x])*(x + 1); // 수정
				//	total_v += c[j * imgWidth + x] * win2[x];
			}

			if (total_v == 0 || total_v < 0.0001)
				loc[j] = 0;
			else
				loc[j] = loc_v / total_v - 0.5;
		}
	}



	delete[]win2;
	delete[]place;
	delete[]c;
	/*
	for (i = 0; i < rM; i++)
	B[i][0] = loc[i];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);
	*/
	for (i = 0; i < rM; i++)
	{
		delete[]A[i];
		delete[]B[i];
	}
	delete[]A;
	delete[]B;

	for (i = 0; i < cN; i++)
		delete[]pinvA[i];
	delete[]pinvA;

	//====================centeroid fitting==============// Hamming Window를 통한 값으로 라인 fitting을 한번 더 해준다..(여기서 slope 구함)
	rM = imgHeight;
	cN = 2;

	//	int i, j, rm, cn;
	count = 0;

	rm = rM;
	cn = cN;

	A = new double *[rM];
	pinvA = new double *[cN];

	for (i = 0; i < rM; i++)
		A[i] = new double[cN];

	for (i = 0; i < rM; i++)
	{
		A[i][0] = count;
		A[i][1] = 1.0;
		count++;
	}

	for (i = 0; i < cN; i++)
		pinvA[i] = new double[rM];

	pInverse(A, rM, cN, pinvA);

	B = new double *[rM];
	for (i = 0; i < rM; i++)
		B[i] = new double[1];

	for (i = 0; i < rM; i++)
		B[i][0] = loc[i];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);

	for (i = 0; i < rM; i++)
	{
		delete[]A[i];
		delete[]B[i];
	}
	delete[]A;
	delete[]B;

	for (i = 0; i < cN; i++)
		delete[]pinvA[i];
	delete[]pinvA;
	//===================================================================================================//

	int nbin = 4;
	double nn = imgWidth * nbin;
	double nn2 = nn / 2 + 1;
	double *freq = new double[(int)nn];
	double del = 1.0;

	for (i = 0; i < (int)nn; i++)
		freq[i] = (double)nbin*((double)i) / (del*nn);

	double freqlim = 1.0;
	double nn2out = (nn2*freqlim / 2);
	double *win = new double[nbin * imgWidth];

	//**********hamming Window***************************//	
	n = nbin * imgWidth;
	mid = (nbin*imgWidth + 1) / 2.0;
	wid1 = mid - 1;
	wid2 = n - mid;


	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	arg = 0;

	for (i = 0; i < n; i++)
	{
		arg = (double)(i + 1) - mid;
		win[i] = cos((PI*arg / wid));
	}

	for (i = 0; i < n; i++)
		win[i] = 0.54 + 0.46*win[i];

	//sfrmat3로 수정===============================================================//

	double fac = 4.0;
	double inProject_nn = imgWidth * fac;
	double slope = C[0][0];

	double vslope = slope;

	slope = 1.0 / slope;

	double slope_deg = 180.0 * atan(abs(vslope)) / PI;

	if (slope_deg < 3.5)
	{
		delete[]freq;
		delete[]win;
		return 0.0;
		//resultSFR = 0.0;
		//return resultSFR;//너무 급경사를 가진 Edge라서 제대로 측정할 수 없음. //예외 처리 구문을 넣어야 할듯..return 등등
	}

	double del2 = 0;
	double delfac;

	delfac = cos(atan(vslope));
	del = del * delfac;
	del2 = del / nbin;

	//	double offset = 0;
	double offset = fac * (0.0 - (((double)imgHeight - 1.0) / slope));

	del = abs(cvRound(offset));
	if (offset > 0) offset = 0.0;

	double *dcorr = new double[(int)nn2];
	double temp_m = 3.0;	//length of difference filter
	double scale = 1;

	for (i = 0; i<nn2; i++)
		dcorr[i] = 1.0;

	temp_m = temp_m - 1;

	for (i = 1; i<nn2; i++)
	{
		dcorr[i] = abs((PI*(double)(i + 1)*temp_m / (2.0*(nn2 + 1))) / sin(PI * (double)(i + 1)*temp_m / (2.0*(nn2 + 1))));	//필터를 이용하여 낮은 응답 신호 부분을 증폭시키려는 의도인듯....;;
		dcorr[i] = 1.0 + scale * (dcorr[i] - 1.0);																	//scale로 weight를 조정하는듯..default 1

		if (dcorr[i] > 10)
			dcorr[i] = 10.0;
	}

	//============================================================================//
	//sfrmat3 : PER ISO Algorithm
	imgHeight = cvRound(cvFloor(imgHeight * fabs(C[0][0]))) / fabs(C[0][0]);	//수직방향으로 1 Cycle이 이뤄지게하기 위함인듯..

	for (i = 0; i < cN; i++)
		delete[]C[i];

	delete[]C;
	delete[]loc;

	double **barray = new double *[2];
	for (i = 0; i < 2; i++)
		barray[i] = new double[(int)inProject_nn + (int)del + 200];



	for (i = 0; i < 2; i++)
	for (j = 0; j < ((int)inProject_nn + (int)del + 100); j++)
		barray[i][j] = 0;

	for (int x = 0; x < imgWidth; x++)
	{
		for (int y = 0; y < imgHeight; y++)
		{
			double ttt = ((double)x - (double)y / slope)*fac;
			int ling = cvCeil(ttt) - offset;
			barray[0][ling]++;
			//	double kkk = barray[0][ling];
			//	barray[0][ling] = kkk;
			double kk = barray[1][ling];
			//	barray[1][ling] = kk + (unsigned char)atemp[y * imgWidth + x];
			barray[1][ling] = kk + atemp[y * imgWidth + x];
		}
	}




	double *point = new double[(int)inProject_nn];
	int start = cvRound(del*0.5);
	double nz = 0;
	int status = 1;

	for (i = start; i < start + (int)inProject_nn - 1; i++)
	{
		if (barray[0][i] == 0)
		{
			nz = nz + 1;
			status = 0;
			if (i == 0)
				barray[0][i] = barray[0][i + 1];
			else
				barray[0][i] = (barray[0][i - 1] + barray[0][i + 1]) / 2.0;
		}
	}

	if (status == 0)
	{
		printf(" Zero count(s) found during projection binning. The edge \n");
		printf(" angle may be large, or you may need more lines of data. \n");
		printf(" Execution will continue, but see Users Guide for info. \n");
	}

	for (i = -1; i < (int)inProject_nn - 1; i++)
	{

		if (barray[0][i + start] != 0){
			if (barray[0][(i + 1) + start] == 0){
				point[(i + 1)] = 0;
			}
			else
				point[(i + 1)] = barray[1][(i + 1) + start] / barray[0][(i + 1) + start];
		}
		else
			point[(i + 1)] = 0;
	}

	for (i = 0; i < 2; i++)
		delete[]barray[i];

	delete[]barray;

	//	IplImage *esf_image = cvCreateImage(cvSize(inProject_nn, 255), IPL_DEPTH_8U, 1);
	//	
	//	cvSetZero(esf_image);
	//
	//	for(i=0; i<(int)inProject_nn-1; i++)
	//		cvLine(esf_image, cvPoint(i, point[i]), cvPoint(i+1, point[i+1]), CV_RGB(255, 255, 255), 1, 8);
	//	
	//	cvFlip(esf_image);
	//
	////	cvShowImage("esf", esf_image);
	////	cvSaveImage("d:\\test\\esf_image.bmp", esf_image);
	//	
	//	cvReleaseImage(&esf_image);

	//******************************deriv1**********************************************//
	m = (int)inProject_nn;
	d_n = 3;	//필터열갯수..
	sum = 0;
	double *pointDeriv = new double[(int)inProject_nn];
	Deriv_c = new double[(int)inProject_nn + d_n - 1];

	for (int k = 0; k<(int)inProject_nn; k++)
		pointDeriv[k] = 0.0;

	for (int k = 0; k<m + d_n - 1; k++)
	{
		sum = 0;

		for (int d_j = d_n - 1; d_j>-1; d_j--)
		{
			if ((k - d_j) > -1 && (k - d_j) < m)
				sum += point[k - d_j] * fil2[d_j];
		}

		Deriv_c[k] = sum;
	}

	for (int k = (d_n - 1); k < (int)inProject_nn; k++)		//MATLAB은 배열이 1base이기 때문에 다시 정리해준다..
		pointDeriv[k] = Deriv_c[k];
	pointDeriv[d_n - 2] = Deriv_c[d_n - 1];
	//************************************************************************************//

	delete[]point;
	delete[]Deriv_c;

	//*********************centroid*************************************//
	loc_v = 0, total_v = 0;

	loc_v = 0;
	total_v = 0;
	for (int x = 0; x < (int)inProject_nn; x++)
	{
		loc_v += pointDeriv[x] * x;
		total_v += pointDeriv[x];
	}

	if (total_v == 0 || total_v < 0.0001){
		delete[]freq;
		delete[]win;
		delete[]pointDeriv;
		return 0.0;
	}
	else{
		loc_v = loc_v / total_v;
	}

	//**************************cent****************************//
	double *temp = new double[(int)inProject_nn];
	for (i = 0; i<(int)inProject_nn; i++)
		temp[i] = 0.0;


	mid = cvRound((inProject_nn + 1.0) / 2.0);

	int cent_del = cvRound(cvRound(loc_v) - mid);

	if (cent_del > 0)
	{
		for (i = 0; i < inProject_nn - cent_del; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else if (cent_del < 1)
	{
		for (i = -cent_del; i < inProject_nn; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else
	{
		for (i = 0; i < inProject_nn; i++)
			temp[i] = pointDeriv[i];
	}

	for (i = 0; i < (int)inProject_nn; i++)
		temp[i] = win[i] * temp[i];

	delete[]win;
	delete[]pointDeriv;


	CvMat *sourceFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	CvMat *resultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	CvMat *ConveredResultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);

	for (i = 0; i < (int)inProject_nn; i++)
	{
		sourceFFT->data.db[i * 2] = temp[i];
		sourceFFT->data.db[i * 2 + 1] = 0;
	}

	delete[]temp;

	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);

	for (i = 0; i < (int)inProject_nn; i++)
	{
		if (i != 0)
		{
			ConveredResultFFT->data.db[i] = sqrt(pow(resultFFT->data.db[i * 2 - 1], 2) + pow(resultFFT->data.db[i * 2], 2));
		}
		else
		{
			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
		}
	}

	//	CvMat *sourceFFT = cvCreateMat((int)12, 1, CV_64FC1);
	//	CvMat *resultFFT = cvCreateMat((int)12, 1, CV_64FC1);
	//	CvMat *ConveredResultFFT = cvCreateMat((int)12, 1, CV_64FC1);
	////	for(i=0; i<(int)inProject_nn; i++)
	////		sourceFFT->data.db[i] = temp[i];
	//	sourceFFT->data.db[0] = 0.0001;
	//	sourceFFT->data.db[1] = 0;
	//	sourceFFT->data.db[2] = -0.2222;
	//	sourceFFT->data.db[3] = 0;
	//	sourceFFT->data.db[4] = 0.1111;
	//	sourceFFT->data.db[5] = 0;
	//	sourceFFT->data.db[6] = 0.4444;
	//	sourceFFT->data.db[7] = 0;
	//	sourceFFT->data.db[8] = 0.2211;
	//	sourceFFT->data.db[9] = 0;
	//	sourceFFT->data.db[10] = -1.0000;
	//	sourceFFT->data.db[11] = 0;
	//	
	//	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);
	//
	//	for(i=0; i<6; i++)
	//	{
	//		if( i != 0)
	//		{
	//			ConveredResultFFT->data.db[i] = sqrt ( pow (resultFFT->data.db[i*2-1], 2) + pow (resultFFT->data.db[i*2], 2) ) ;
	//		}
	//		else
	//		{
	//		//	resultFFT->data.db[i*2] = fabs(resultFFT->data.db[i*2]);
	//			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
	//		}
	//	}

	//	delete []temp;
	//	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);	
	//	double k1 = sqrt ( pow (resultFFT->data.db[5], 2) + pow (resultFFT->data.db[6], 2) ) ;

	double *Resultmtf = new double[(int)inProject_nn];
	int count_j = 0;

	memset(Resultmtf, 0, sizeof(Resultmtf));

	// 	double a[200];
	// 	for (int i = 0; i <124; i++){
	// 		a[i]=dcorr[i];
	// 	}
	for (int i = 0; i < (int)inProject_nn / 2; i++)
	{
		Resultmtf[count_j] = fabs(ConveredResultFFT->data.db[i]) / fabs(ConveredResultFFT->data.db[0]);
		Resultmtf[count_j] = Resultmtf[count_j] * dcorr[i];
		count_j++;
	}

	delete[]dcorr;	//correct weight 배열 해제

	/*	ofstream outFile("c:\\output.txt");		//mtf결과 파일로 저장
	for(i=0; i<(int)inProject_nn/2; i++)
	{
	outFile << Resultmtf[i] << endl;
	}
	outFile.close();*/

	cvReleaseMat(&sourceFFT);
	cvReleaseMat(&resultFFT);
	cvReleaseMat(&ConveredResultFFT);

	// SFRGraph_image
#define SAVE_SFR_GRAPH	0
#if SAVE_SFR_GRAPH
	IplImage *SFRGraph_image = cvCreateImage(cvSize(inProject_nn * 10, 110 * 10), IPL_DEPTH_8U, 3);
	cvSetZero(SFRGraph_image);

	for (int q = 0; q<inProject_nn / 2; q++)
	{
		cvLine(SFRGraph_image, cvPoint(q * 10, cvRound(Resultmtf[q] * 1000.0)), cvPoint((q + 1) * 10, cvRound(Resultmtf[q + 1] * 1000.0)), CV_RGB(255, 255, 255), 1, 8);
	}
	cvFlip(SFRGraph_image);

	char	szText[64];
	CvFont* font = (CvFont*)malloc(sizeof(CvFont));
	cvInitFont(font, CV_FONT_VECTOR0, 0.5, 0.5, 0, 1);

	FILE*	logFile;
	sprintf(szText, "d:\\test\\SFR_Result_%03d.txt", ROI_NUM);
	logFile = fopen(szText, "w+");

	sprintf(szText, "SFR Result\t%d\nFrequency\tSFR\n", ROI_NUM);
	fwrite(szText, 1, strlen(szText), logFile);

	for (int q = 0; q<inProject_nn / 2; q++)
	{
		sprintf(szText, "%.04f", Resultmtf[q]);
		cvPutText(SFRGraph_image, szText, cvPoint(q * 10 - 5, 1100 - cvRound(Resultmtf[q] * 1000.0)), font, CV_RGB(255, 0, 0));

		sprintf(szText, "%3.4f\t%3.4f\n", freq[q], Resultmtf[q]);

		if (freq[q] < 1.01)
			fwrite(szText, 1, strlen(szText), logFile);
		//		printf("Result[%d] Freq.:MTF--- %f,%f\n", q, freq[q], Resultmtf[q]);
	}
	fclose(logFile);

	sprintf(szText, "d:\\test\\SFR_GRAPH_%03d.png", ROI_NUM);
	cvSaveImage(szText, SFRGraph_image);

	cvReleaseImage(&SFRGraph_image);
#endif
	// SFRGraph_image 


	double nPixel = 1000.0 / dPixelSize;
	double cyPx = SFR_DATA[ROI_NUM].dLinepare / nPixel;
	//cyPx *=2.23;


	if (SFRMODE == 0){

		// MTF를 결과로 하는 방식
		int half_sampling_index = -1;
		double data, old_data = 1.0;

		for (int q = 0; q<inProject_nn / 2; q++)
		{
			data = fabs(freq[q] - cyPx);

			if (old_data > data)
			{
				old_data = data;
				half_sampling_index = q;
			}
		}

		if (Resultmtf[half_sampling_index] < 0 || half_sampling_index == -1)
			resultSFR = 0.0;
		else
			resultSFR = Resultmtf[half_sampling_index];
	}
	else{

		// cypx를 결과로 하는 방식
		// 0.5 의 근사치 위치의 값을 양쪽 값을 이용하여 계산하는 방식
		double	dbBefore, dbNext;
		int		nBefore, nNext;

		dbBefore = dbNext = 1.0;
		nBefore = nNext = 0;
		for (int i = 0; i < (int)inProject_nn; i++)
		{
			nNext = i;
			dbNext = Resultmtf[i];
			//		printf("Result[%d] Freq.:MTF--- %f,%f\n", i, freq[i], dbNext);
			if (dbNext < 0.5)
				break;
			dbBefore = dbNext;
			nBefore = nNext;
		}

		double dbBeforeFreq = freq[nBefore];
		double dbNextFreq = freq[nNext];

		//	resultSFR = (0.5 - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq) + dbBeforeFreq;
		// 그래프가 반비례 형태임으로 아래 형태 계산이 더 근접한 계산
		resultSFR = dbNextFreq - (0.5 - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq);

		printf("Result mtf range p[%d,%d] - value: [%f,%f] / [%f,%f] --> result SFR: %f \n"
			, nBefore, nNext, dbBefore, dbNext, dbBeforeFreq, dbNextFreq, resultSFR);

		if (resultSFR > 0.5)	// 왜? 0.5보다 크면 안 됨으로
		{
			resultSFR = 0.0;
		}
		//////////////////////// 끝

		// cymm를 결과로 하는 방식
		if (SFRMODE == 2){
			resultSFR = (resultSFR / dPixelSize) * 1000;
		}
	}

	delete[]freq;
	delete[]Resultmtf;
	//	delete []atemp;

	return resultSFR;
}
void CSFRTILTOptionDlg::MulMatrix(double **A, double **B, double **C, unsigned int Row, unsigned int Col, unsigned int n, unsigned int Mode)
{

	////  A[Row][n] x B[n][Col] = [Row x n] x [n x Col] = [ Row x Col ] = C[Row][Col]   ///////
	////  Mode :  [ Mode 0 : A x B , Mode 1 : A' x B, Mode 2 : A x B' ] 

	unsigned int i,j,k;


	for(i=0;i<Row;i++){
		for(j=0;j<Col;j++){
			C[i][j]=0;
			for(k=0;k<n;k++){
				if(Mode==0)	     C[i][j] += A[i][k]*B[k][j];   // Mode 0 : A  x B
				else if(Mode==1) C[i][j] += A[k][i]*B[k][j];   // Mode 1 : A' x B
				else if(Mode==2) C[i][j] += A[i][k]*B[j][k];   // Mode 2 : A  x B' 
			}
		}
	}
}

void CSFRTILTOptionDlg::pInverse(double **A, unsigned int Row, unsigned int Col, double **RtnMat)
{
	unsigned int i;//, j;
	double **AxA, **InvAxA;

	//	AxA = (double **)malloc(sizeof(double*)*Col);
	//	for(i=0;i<Col;i++)AxA[i]=(double *)malloc(sizeof(double)*Col);	
	AxA = new double *[Col];
	for(i=0; i<Col; i++)
		AxA[i] = new double[Col];


	//	InvAxA = (double **)malloc(sizeof(double*)*Col);
	//	for(i=0;i<Col;i++) InvAxA[i]=(double *)malloc(sizeof(double)*Col);
	InvAxA = new double *[Col];
	for(i=0; i<Col; i++)
		InvAxA[i] = new double[Col];

	MulMatrix(A, A, AxA, Col, Col, Row, 1);  // Mode 1 : A' * B

	Inverse(AxA, Col, InvAxA);

	MulMatrix(InvAxA, A, RtnMat, Col, Row, Col, 2);

	//	for(i=0;i<Col;i++)free(InvAxA[i]);   free(InvAxA);
	for(i=0;i<Col;i++)
		delete []InvAxA[i];
	delete []InvAxA;
	//	for(i=0;i<Col;i++)free(AxA[i]);   free(AxA);
	for(i=0;i<Col;i++)
		delete []AxA[i];
	delete []AxA;

}

void CSFRTILTOptionDlg::Inverse(double **dataMat, unsigned int n, double **MatRtn)
{

	unsigned int i,j;
	double *dataA;

	CvMat matA;
	CvMat *pMatB = cvCreateMat(n, n, CV_64F);

	//	dataA = (double *)malloc(sizeof(double)*n*n);
	dataA = new double[n*n];

	for(i=0;i<n;i++)
		for(j=0;j<n;j++)
			dataA[i*n+j]=dataMat[i][j];

	matA = cvMat( n, n, CV_64F, dataA );

	cvInvert(&matA, pMatB);
	//	PrintMat(&matA, "pMatA = Source");    // Mat print

	for(i=0;i<n;i++)
		for(j=0;j<n;j++)
			MatRtn[i][j]=cvGetReal2D(pMatB, i, j);  // A = U * W * VT  ,  MatRtn : VT

	cvReleaseMat(&pMatB);
	delete []dataA;
	//	free(dataA);
}

double CSFRTILTOptionDlg::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt( (double)((x2-x1)*(x2-x1)) +  ((y2-y1)*(y2-y1)));

	return result;
}
void CSFRTILTOptionDlg::OnBnClickedChkOvershoot()
{
	CString str_ModelPath= ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);

	CString str;
	str.Empty();
	str.Format("%d",b_Smooth);
	WritePrivateProfileString(str_model,"OVERSHOOT",str,str_ModelPath);

	b_OverShoot = GetPrivateProfileInt(str_model,"OVERSHOOT",1,str_ModelPath);

 	UpdateData(FALSE);
	
}
// ***M_ x TIlt *** //
//1 :4 - 15
//2 :5 - 14
//3 :6 - 13
//4 :7 - 12
//5 :8 - 11
//6 :9 - 10
///*** M_ y Tilt ***//
//1 :4 - 9
//2 :5 - 8
//3 :11 - 14
//4 :10 - 15

//*** S_x Tilt ***///
//1: 16 - 23
//2: 17 - 22
//3: 18 - 21
//4: 19 - 20
//*** S_y Tilt ***///
//1: 16 - 19
//2: 17 - 18
//3: 20 - 23
//4: 21 - 22
void CSFRTILTOptionDlg::Mid_XTilt_Pair(){

	double Max_TiltValue = 0;
	double Temp_TiltValue[6] = {0,};
	int Id_MaxTilt = 0;
	for(int i = 0; i < 6; i++){
		Temp_TiltValue[i] = abs(SFR_DATA[i+4].dResultData - SFR_DATA[15-i].dResultData);
		if(Temp_TiltValue[i] > Max_TiltValue){
			Max_TiltValue = Temp_TiltValue[i];
			Id_MaxTilt = i;
		}
	}
	Tilt_x = Max_TiltValue;
	if(Id_MaxTilt == 0 ){
		ID_XTiltPoint1 = 4;
		ID_XTiltPoint2 = 15;
	}else if(Id_MaxTilt == 1){
		ID_XTiltPoint1 = 5;
		ID_XTiltPoint2 = 14;
	}else if(Id_MaxTilt == 2){
		ID_XTiltPoint1 = 6;
		ID_XTiltPoint2 = 13;
	}else if(Id_MaxTilt == 3){
		ID_XTiltPoint1 = 7;
		ID_XTiltPoint2 = 12;
	}else if(Id_MaxTilt == 4){
		ID_XTiltPoint1 = 8;
		ID_XTiltPoint2 = 11;
	}else if(Id_MaxTilt == 5){
		ID_XTiltPoint1 = 9;
		ID_XTiltPoint2 = 10;
	}
	
}
void CSFRTILTOptionDlg::Mid_YTilt_Pair(){
	double Max_TiltValue = 0;
	double Temp_TiltValue[4] = {0,};
	int Id_MaxTilt = 0;
	Temp_TiltValue[0] = abs(SFR_DATA[4].dResultData - SFR_DATA[9].dResultData);
	Temp_TiltValue[1] = abs(SFR_DATA[5].dResultData - SFR_DATA[8].dResultData);
	Temp_TiltValue[2] = abs(SFR_DATA[11].dResultData - SFR_DATA[14].dResultData);
	Temp_TiltValue[3] = abs(SFR_DATA[10].dResultData - SFR_DATA[15].dResultData);

	for(int i = 0; i < 4; i++){
		if(Temp_TiltValue[i] > Max_TiltValue){
			Max_TiltValue = Temp_TiltValue[i];
			Id_MaxTilt = i;
		}
	}
	Tilt_y = Max_TiltValue;
	if(Id_MaxTilt == 0 ){
		ID_YTiltPoint1 = 4;
		ID_YTiltPoint2 = 9;
	}else if(Id_MaxTilt == 1){
		ID_YTiltPoint1 = 5;
		ID_YTiltPoint2 = 8;
	}else if(Id_MaxTilt == 2){
		ID_YTiltPoint1 = 11;
		ID_YTiltPoint2 = 14;
	}else if(Id_MaxTilt == 3){
		ID_YTiltPoint1 = 10;
		ID_YTiltPoint2 = 15;
	}

}


void CSFRTILTOptionDlg::Side_XTilt_Pair(){
	double Max_TiltValue = 0;
	double Temp_TiltValue[4] = {0,};
	int Id_MaxTilt = 0;
	for(int i = 0; i < 4; i++){
		Temp_TiltValue[i] = abs(SFR_DATA[i+16].dResultData - SFR_DATA[23-i].dResultData);
		if(Temp_TiltValue[i] > Max_TiltValue){
			Max_TiltValue = Temp_TiltValue[i];
			Id_MaxTilt = i;
		}
	}
	Tilt_x = Max_TiltValue;

	if(Id_MaxTilt == 0 ){
		ID_XTiltPoint1 = 16;
		ID_XTiltPoint2 = 23;
	}else if(Id_MaxTilt == 1){
		ID_XTiltPoint1 = 17;
		ID_XTiltPoint2 = 22;
	}else if(Id_MaxTilt == 2){
		ID_XTiltPoint1 = 18;
		ID_XTiltPoint2 = 21;
	}else if(Id_MaxTilt == 3){
		ID_XTiltPoint1 = 19;
		ID_XTiltPoint2 = 20;
	}

}
void CSFRTILTOptionDlg::Side_YTilt_Pair(){
	double Max_TiltValue = 0;
	double Temp_TiltValue[4] = {0,};
	int Id_MaxTilt = 0;
	Temp_TiltValue[0] = abs(SFR_DATA[16].dResultData - SFR_DATA[19].dResultData);
	Temp_TiltValue[1] = abs(SFR_DATA[17].dResultData - SFR_DATA[18].dResultData);
	Temp_TiltValue[2] = abs(SFR_DATA[20].dResultData - SFR_DATA[23].dResultData);
	Temp_TiltValue[3] = abs(SFR_DATA[21].dResultData - SFR_DATA[22].dResultData);

	for(int i = 0; i < 4; i++){
		if(Temp_TiltValue[i] > Max_TiltValue){
			Max_TiltValue = Temp_TiltValue[i];
			Id_MaxTilt = i;
		}
	}
	Tilt_y = Max_TiltValue;

	if(Id_MaxTilt == 0 ){
		ID_YTiltPoint1 = 16;
		ID_YTiltPoint2 = 19;
	}else if(Id_MaxTilt == 1){
		ID_YTiltPoint1 = 17;
		ID_YTiltPoint2 = 18;
	}else if(Id_MaxTilt == 2){
		ID_YTiltPoint1 = 20;
		ID_YTiltPoint2 = 23;
	}else if(Id_MaxTilt == 3){
		ID_YTiltPoint1 = 21;
		ID_YTiltPoint2 = 22;
	}
	//1: 16 - 19
	//2: 17 - 18
	//3: 20 - 23
	//4: 21 - 22


}
BOOL CSFRTILTOptionDlg::EXCEL_UPLOAD()
{
	m_SFRList.DeleteAllItems();
	if (((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("SFR_TEST", &m_SFRList, 1 ,&StartCnt) == TRUE)
	{
		return TRUE;
	}
	else
	{
	//161209	StartCnt = 0;
		return FALSE;
	}
}

void CSFRTILTOptionDlg::Set_List(CListCtrl *List){
	CString Item[34]={"C1","C2","C3","C4","S1","S2","S3","S4","S5","S6","S7","S8","S9","S10","S11","S12","S13","S14","S15","S16","S17","S18","S19","S20","S21","S22","S23","S24","S25","S26","S27","S28","S29","S30"};

	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	//List->InsertColumn(2,"LOT CARD",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);

	for(int t=0; t<34; t++){
		List->InsertColumn(4+t,Item[t],LVCFMT_CENTER, 80);
	}
	List->InsertColumn(38, "XtiltDev", LVCFMT_CENTER, 80);
	List->InsertColumn(39, "Y1tiltDev", LVCFMT_CENTER, 80);
	List->InsertColumn(40, "Y2tiltDev", LVCFMT_CENTER, 80);
	List->InsertColumn(41,"RESULT",LVCFMT_CENTER, 80);
	//List->InsertColumn(39,"작업자",LVCFMT_CENTER, 80);
	List->InsertColumn(42,"비고",LVCFMT_CENTER, 80);
	//List->InsertColumn(41,"라벨넘버",LVCFMT_CENTER, 80);





	//List->InsertColumn(39,"비고",LVCFMT_CENTER, 80);
//	List->InsertColumn(40,"라벨넘버",LVCFMT_CENTER, 80);
	Copy_List(&m_SFRList, ((CImageTesterDlg *)m_pMomWnd)->m_pWorkList, 43);
}

void CSFRTILTOptionDlg::EXCEL_SAVE()
{

	CString Item[37]={"C1","C2","C3","C4","S1","S2","S3","S4","S5","S6","S7","S8","S9","S10","S11","S12","S13","S14","S15","S16","S17","S18","S19","S20","S21","S22","S23","S24","S25","S26","S27","S28","S29","S30","XTilt","Y1Tilt","Y2Tilt"};

	CString Data ="";
	CString str="";
	
	if (m_bSFRmode == 0)
		str = "***********************SFR 검사 [ 단위 : MTF ] ***********************\n";
	else if (m_bSFRmode == 1)
		str = "***********************SFR 검사 [ 단위 : Cy/Pix ] ***********************\n";
	else if (m_bSFRmode == 2)
		str = "***********************SFR 검사 [ 단위 : Cy/mm ] ***********************\n";

	Data += str;
	for (int num = 0; num < 34; num++){
		if (SFR_DATA[num].bEnable == TRUE){
			if (num < 4){
				str.Empty();
				if (m_bSFRmode == 0)
					str.Format("C_%d,StartX, %d, StartY, %d, Width, %d, Height, %d,LinePair, %6.2f ,Threshold_MIN,%6.2f,Threshold_MAX,%6.2f,/,", num + 1, SFR_DATA[num].nPosX, SFR_DATA[num].nPosY, SFR_DATA[num].nWidth, SFR_DATA[num].nHeight, SFR_DATA[num].dLinepare, SFR_DATA[num].dThreshold_MIN, SFR_DATA[num].dThreshold_MAX);
				else
					str.Format("C_%d,StartX, %d, StartY, %d, Width, %d, Height, %d,MTF, %d ,Threshold_MIN,%6.2f,Threshold_MAX,%6.2f,/,", num + 1, SFR_DATA[num].nPosX, SFR_DATA[num].nPosY, SFR_DATA[num].nWidth, SFR_DATA[num].nHeight, SFR_DATA[num].MTF, SFR_DATA[num].dThreshold_MIN, SFR_DATA[num].dThreshold_MAX);
				Data += str;
			}
			else{
				str.Empty();
				if (m_bSFRmode == 0)
					str.Format("S_%d,StartX, %d, StartY, %d, Width, %d, Height, %d,LinePair, %6.2f ,Threshold_MIN,%6.2f,Threshold_MAX,%6.2f,/,", num - 3, SFR_DATA[num].nPosX, SFR_DATA[num].nPosY, SFR_DATA[num].nWidth, SFR_DATA[num].nHeight, SFR_DATA[num].dLinepare, SFR_DATA[num].dThreshold_MIN, SFR_DATA[num].dThreshold_MAX);
				else
					str.Format("S_%d,StartX, %d, StartY, %d, Width, %d, Height, %d,MTF, %d ,Threshold_MIN,%6.2f,Threshold_MAX,%6.2f,/,", num - 3, SFR_DATA[num].nPosX, SFR_DATA[num].nPosY, SFR_DATA[num].nWidth, SFR_DATA[num].nHeight, SFR_DATA[num].MTF, SFR_DATA[num].dThreshold_MIN, SFR_DATA[num].dThreshold_MAX);
				Data += str;
			}
		}
		if ((num == 3) || (num == 18) || (num == 33))
		{
			str.Empty();
			str.Format(",\n ");
			Data += str;
		}
	}
	str.Empty();
	str = "*************************************************\n";
	Data += str;

// 	if (m_bSFRmode == 0){
// 		str = "***********************SFR 검사***********************\n";
// 		Data += str;
// 		str.Empty();
// 		str.Format("C_1,PosX, %d, PosY, %d, Width, %d, Height, %d,LinePair, %6.2f ,Threshold,%6.2f,/,C_2,PosX, %d, PosY, %d, Width, %d, Height, %d,LinePair, %6.2f ,Threshold,%6.2f,/,C_3,PosX, %d, PosY, %d, Width, %d, Height, %d,LinePair, %6.2f ,Threshold,%6.2f,/,C_4,PosX, %d, PosY, %d, Width, %d, Height, %d,LinePair, %6.2f ,Threshold,%6.2f,,\n ", SFR_DATA[0].nPosX, SFR_DATA[0].nPosY, SFR_DATA[0].nWidth, SFR_DATA[0].nHeight, SFR_DATA[0].dLinepare, SFR_DATA[0].dThreshold, SFR_DATA[1].nPosX, SFR_DATA[1].nPosY, SFR_DATA[1].nWidth, SFR_DATA[1].nHeight, SFR_DATA[1].dLinepare, SFR_DATA[1].dThreshold, SFR_DATA[2].nPosX, SFR_DATA[2].nPosY, SFR_DATA[2].nWidth, SFR_DATA[2].nHeight, SFR_DATA[2].dLinepare, SFR_DATA[2].dThreshold, SFR_DATA[3].nPosX, SFR_DATA[3].nPosY, SFR_DATA[3].nWidth, SFR_DATA[3].nHeight,SFR_DATA[3].dLinepare, SFR_DATA[3].dThreshold);
// 		Data += str;
// 		str.Empty();
// 		str.Format("S_1,LinePair, %6.2f ,Threshold,%6.2f,/,S_2,LinePair, %6.2f ,Threshold,%6.2f,/,S_3,LinePair, %6.2f ,Threshold,%6.2f,/,S_4,LinePair, %6.2f ,Threshold,%6.2f,S_5,LinePair, %6.2f ,Threshold,%6.2f,S_6,LinePair, %6.2f ,Threshold,%6.2f,/,S_7,LinePair, %6.2f ,Threshold,%6.2f,/,S_8,LinePair, %6.2f ,Threshold,%6.2f,/,S_9,LinePair, %6.2f ,Threshold,%6.2f,S_10,LinePair, %6.2f ,Threshold,%6.2f,S_11,LinePair, %6.2f ,Threshold,%6.2f,/,S_12,LinePair, %6.2f ,Threshold,%6.2f,/,S_13,LinePair, %6.2f ,Threshold,%6.2f,/,S_14,LinePair, %6.2f ,Threshold,%6.2f,S_15,LinePair, %6.2f ,Threshold,%6.2f,/,\n  ", SFR_DATA[0].nPosX, SFR_DATA[0].nPosY, SFR_DATA[0].nWidth, SFR_DATA[0].nHeight, SFR_DATA[4].dLinepare, SFR_DATA[4].dThreshold, SFR_DATA[0].nPosX, SFR_DATA[0].nPosY, SFR_DATA[0].nWidth, SFR_DATA[0].nHeight, SFR_DATA[5].dLinepare, SFR_DATA[5].dThreshold, SFR_DATA[0].nPosX, SFR_DATA[0].nPosY, SFR_DATA[0].nWidth, SFR_DATA[0].nHeight, SFR_DATA[6].dLinepare, SFR_DATA[6].dThreshold, SFR_DATA[0].nPosX, SFR_DATA[0].nPosY, SFR_DATA[0].nWidth, SFR_DATA[0].nHeight, SFR_DATA[7].dLinepare, SFR_DATA[7].dThreshold, SFR_DATA[0].nPosX, SFR_DATA[0].nPosY, SFR_DATA[0].nWidth, SFR_DATA[0].nHeight, SFR_DATA[8].dLinepare, SFR_DATA[8].dThreshold, SFR_DATA[9].dLinepare, SFR_DATA[9].dThreshold, SFR_DATA[10].dLinepare, SFR_DATA[10].dThreshold, SFR_DATA[11].dLinepare, SFR_DATA[11].dThreshold, SFR_DATA[12].dLinepare, SFR_DATA[12].dThreshold, SFR_DATA[13].dLinepare, SFR_DATA[13].dThreshold, SFR_DATA[14].dLinepare, SFR_DATA[14].dThreshold, SFR_DATA[15].dLinepare, SFR_DATA[15].dThreshold, SFR_DATA[16].dLinepare, SFR_DATA[16].dThreshold, SFR_DATA[17].dLinepare, SFR_DATA[17].dThreshold, SFR_DATA[18].dLinepare, SFR_DATA[18].dThreshold);
// 		Data += str;
// 		str.Empty();
// 		str.Format("S_16,LinePair, %6.2f ,Threshold,%6.2f,/,S_17,LinePair, %6.2f ,Threshold,%6.2f,/,S_18,LinePair, %6.2f ,Threshold,%6.2f,/,S_19,LinePair, %6.2f ,Threshold,%6.2f,S_20,LinePair, %6.2f ,Threshold,%6.2f,S_21,LinePair, %6.2f ,Threshold,%6.2f,/,S_22,LinePair, %6.2f ,Threshold,%6.2f,/,S_23,LinePair, %6.2f ,Threshold,%6.2f,/,S_24,LinePair, %6.2f ,Threshold,%6.2f,S_25,LinePair, %6.2f ,Threshold,%6.2f,S_26,LinePair, %6.2f ,Threshold,%6.2f,/,S_27,LinePair, %6.2f ,Threshold,%6.2f,/,S_28,LinePair, %6.2f ,Threshold,%6.2f,/,S_29,LinePair, %6.2f ,Threshold,%6.2f,S_30,LinePair, %6.2f ,Threshold,%6.2f,/,\n", SFR_DATA[0].nPosX, SFR_DATA[0].nPosY, SFR_DATA[0].nWidth, SFR_DATA[0].nHeight, SFR_DATA[19].dLinepare, SFR_DATA[19].dThreshold, SFR_DATA[0].nPosX, SFR_DATA[0].nPosY, SFR_DATA[0].nWidth, SFR_DATA[0].nHeight, SFR_DATA[20].dLinepare, SFR_DATA[20].dThreshold, SFR_DATA[0].nPosX, SFR_DATA[0].nPosY, SFR_DATA[0].nWidth, SFR_DATA[0].nHeight, SFR_DATA[21].dLinepare, SFR_DATA[21].dThreshold, SFR_DATA[0].nPosX, SFR_DATA[0].nPosY, SFR_DATA[0].nWidth, SFR_DATA[0].nHeight, SFR_DATA[22].dLinepare, SFR_DATA[22].dThreshold, SFR_DATA[0].nPosX, SFR_DATA[0].nPosY, SFR_DATA[0].nWidth, SFR_DATA[0].nHeight, SFR_DATA[23].dLinepare, SFR_DATA[23].dThreshold, SFR_DATA[24].dLinepare, SFR_DATA[24].dThreshold, SFR_DATA[25].dLinepare, SFR_DATA[25].dThreshold, SFR_DATA[26].dLinepare, SFR_DATA[26].dThreshold, SFR_DATA[27].dLinepare, SFR_DATA[27].dThreshold, SFR_DATA[28].dLinepare, SFR_DATA[28].dThreshold, SFR_DATA[29].dLinepare, SFR_DATA[29].dThreshold, SFR_DATA[30].dLinepare, SFR_DATA[30].dThreshold, SFR_DATA[31].dLinepare, SFR_DATA[31].dThreshold, SFR_DATA[32].dLinepare, SFR_DATA[32].dThreshold, SFR_DATA[33].dLinepare, SFR_DATA[33].dThreshold);
// 		Data += str;
// 		str.Empty();
// 		str = "*************************************************\n";
// 		Data += str;
// 	}
// 	else{
// 		str = "***********************SFR 검사***********************\n";
// 		Data += str;
// 		str.Empty();
// 		str.Format("C_1,MTF, %6.2f ,Threshold,%6.2f,/,C_2,MTF, %6.2f ,Threshold,%6.2f,/,C_3,MTF, %6.2f ,Threshold,%6.2f,/,C_4,MTF, %6.2f ,Threshold,%6.2f,,\n ", SFR_DATA[0].MTF, SFR_DATA[0].dThreshold, SFR_DATA[1].MTF, SFR_DATA[1].dThreshold, SFR_DATA[2].MTF, SFR_DATA[2].dThreshold, SFR_DATA[3].MTF, SFR_DATA[3].dThreshold);
// 		Data += str;
// 		str.Empty();
// 		str.Format("S_1,MTF, %6.2f ,Threshold,%6.2f,/,S_2,MTF, %6.2f ,Threshold,%6.2f,/,S_3,MTF, %6.2f ,Threshold,%6.2f,/,S_4,MTF, %6.2f ,Threshold,%6.2f,S_5,MTF, %6.2f ,Threshold,%6.2f,S_6,MTF, %6.2f ,Threshold,%6.2f,/,S_7,MTF, %6.2f ,Threshold,%6.2f,/,S_8,MTF, %6.2f ,Threshold,%6.2f,/,S_9,MTF, %6.2f ,Threshold,%6.2f,S_10,MTF, %6.2f ,Threshold,%6.2f,S_11,MTF, %6.2f ,Threshold,%6.2f,/,S_12,MTF, %6.2f ,Threshold,%6.2f,/,S_13,MTF, %6.2f ,Threshold,%6.2f,/,S_14,MTF, %6.2f ,Threshold,%6.2f,S_15,MTF, %6.2f ,Threshold,%6.2f,/,\n  ", SFR_DATA[4].MTF, SFR_DATA[4].dThreshold, SFR_DATA[5].MTF, SFR_DATA[5].dThreshold, SFR_DATA[6].MTF, SFR_DATA[6].dThreshold, SFR_DATA[7].MTF, SFR_DATA[7].dThreshold, SFR_DATA[8].MTF, SFR_DATA[8].dThreshold, SFR_DATA[9].MTF, SFR_DATA[9].dThreshold, SFR_DATA[10].MTF, SFR_DATA[10].dThreshold, SFR_DATA[11].MTF, SFR_DATA[11].dThreshold, SFR_DATA[12].MTF, SFR_DATA[12].dThreshold, SFR_DATA[13].MTF, SFR_DATA[13].dThreshold, SFR_DATA[14].MTF, SFR_DATA[14].dThreshold, SFR_DATA[15].MTF, SFR_DATA[15].dThreshold, SFR_DATA[16].MTF, SFR_DATA[16].dThreshold, SFR_DATA[17].MTF, SFR_DATA[17].dThreshold, SFR_DATA[18].MTF, SFR_DATA[18].dThreshold);
// 		Data += str;
// 		str.Empty();
// 		str.Format("S_16,MTF, %6.2f ,Threshold,%6.2f,/,S_17,MTF, %6.2f ,Threshold,%6.2f,/,S_18,MTF, %6.2f ,Threshold,%6.2f,/,S_19,MTF, %6.2f ,Threshold,%6.2f,S_20,MTF, %6.2f ,Threshold,%6.2f,S_21,MTF, %6.2f ,Threshold,%6.2f,/,S_22,MTF, %6.2f ,Threshold,%6.2f,/,S_23,MTF, %6.2f ,Threshold,%6.2f,/,S_24,MTF, %6.2f ,Threshold,%6.2f,S_25,MTF, %6.2f ,Threshold,%6.2f,S_26,MTF, %6.2f ,Threshold,%6.2f,/,S_27,MTF, %6.2f ,Threshold,%6.2f,/,S_28,MTF, %6.2f ,Threshold,%6.2f,/,S_29,MTF, %6.2f ,Threshold,%6.2f,S_30,MTF, %6.2f ,Threshold,%6.2f,/,\n", SFR_DATA[19].MTF, SFR_DATA[19].dThreshold, SFR_DATA[20].MTF, SFR_DATA[20].dThreshold, SFR_DATA[21].MTF, SFR_DATA[21].dThreshold, SFR_DATA[22].MTF, SFR_DATA[22].dThreshold, SFR_DATA[23].MTF, SFR_DATA[23].dThreshold, SFR_DATA[24].MTF, SFR_DATA[24].dThreshold, SFR_DATA[25].MTF, SFR_DATA[25].dThreshold, SFR_DATA[26].MTF, SFR_DATA[26].dThreshold, SFR_DATA[27].MTF, SFR_DATA[27].dThreshold, SFR_DATA[28].MTF, SFR_DATA[28].dThreshold, SFR_DATA[29].MTF, SFR_DATA[29].dThreshold, SFR_DATA[30].MTF, SFR_DATA[30].dThreshold, SFR_DATA[31].MTF, SFR_DATA[31].dThreshold, SFR_DATA[32].MTF, SFR_DATA[32].dThreshold, SFR_DATA[33].MTF, SFR_DATA[33].dThreshold);
// 		Data += str;
// 		str.Empty();
// 		str = "*************************************************\n";
// 		Data += str;
// 	}

	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("SFR_TEST",Item,37,&m_SFRList,Data);
}
void CSFRTILTOptionDlg::FAIL_UPLOAD()
{
	if (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1){
		//	{
		// 		LOT_InsertDataList();
		// 		for (int t = 0; t < 34; t++)
		// 		{
		// 			m_Lot_SFRList.SetItemText(Lot_InsertIndex, t + 5, "X");
		// 
		// 		}
		// 		m_Lot_SFRList.SetItemText(Lot_InsertIndex, 39, "FAIL");
		// 		m_Lot_SFRList.SetItemText(Lot_InsertIndex, 40, "STOP");
		// 		Insertcheck = 1;
		// 		b_StopFail = TRUE;
		// 		Lot_StartCnt++;
		InsertList();
		for (int t = 0; t < 34; t++)
		{
			m_SFRList.SetItemText(InsertIndex, t + 4, "X");

		}
		CString str = "";
		str.Empty();
		str.Format(_T("%.2f"), m_dXtilt_result);
		m_SFRList.SetItemText(InsertIndex, 38, str);

		str.Empty();
		str.Format(_T("%.2f"), m_dY1tilt_result);
		m_SFRList.SetItemText(InsertIndex, 39, str);

		str.Empty();
		str.Format(_T("%.2f"), m_dY2tilt_result);
		m_SFRList.SetItemText(InsertIndex, 40, str);

		m_SFRList.SetItemText(InsertIndex, 41, "FAIL");
		m_SFRList.SetItemText(InsertIndex, 42, "STOP");
		Insertcheck = 1;
		b_StopFail = TRUE;
		((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_SFR, "STOP");
		StartCnt++;
		//	}
		//	else{
		// 		InsertList();
		// 		for (int t = 0; t < 34; t++)
		// 		{
		// 			m_SFRList.SetItemText(InsertIndex, t + 4, "X");
		// 
		// 		}
		// 		m_SFRList.SetItemText(InsertIndex, 38, "FAIL");
		// 		m_SFRList.SetItemText(InsertIndex, 39, "STOP");
		// 		Insertcheck = 1;
		// 		b_StopFail = TRUE;
		// 		((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_SFR, "STOP");
		// 		StartCnt++;
		//	}
	}
}

void CSFRTILTOptionDlg::FINAL_UPLOAD(LPCSTR lpcszString){ //??
	if(((CImageTesterDlg *)m_pMomWnd)->LotMod ==1){
		m_SFRList.SetItemText(InsertIndex,42,lpcszString);
	}
}

void CSFRTILTOptionDlg::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	InsertIndex = m_SFRList.InsertItem(StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_SFRList.SetItemText(InsertIndex,0,strCnt);

	//161209
	//m_SFRList.SetItemText(InsertIndex,1,((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	//m_SFRList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	//m_SFRList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");


	m_SFRList.SetItemText(InsertIndex, 1, ((CImageTesterDlg *)m_pMomWnd)->m_modelName);
//	m_SFRList.SetItemText(InsertIndex,2, ((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);////////LOT 이름
	m_SFRList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_SFRList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");


//	m_SFRList.SetItemText(InsertIndex,40,((CImageTesterDlg *)m_pMomWnd)->LOT_OPERATOR);

//161209	m_SFRList.SetItemText(InsertIndex,40,((CImageTesterDlg *)m_pMomWnd)->str_SENSORID);

}
void CSFRTILTOptionDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	UINT BUF = 0;
	if(nIDEvent == 150){
		KillTimer(150);
		BUF = m_ROIList.GetItemState(iSavedItem,LVIS_STATEIMAGEMASK);
		if( BUF == 0x2000){
			SFR_DATA[iSavedItem].bEnable = TRUE;
		}else if(BUF == 0x1000){
			SFR_DATA[iSavedItem].bEnable = FALSE;
		}
	}else if(nIDEvent == 160){
		KillTimer(160);
		BUF = m_ROIList.GetItemState(iSavedItem,LVIS_STATEIMAGEMASK);
		if( BUF == 0x2000){
			SFR_DATA[iSavedItem].bEnable = TRUE;
		}else if(BUF == 0x1000){
			SFR_DATA[iSavedItem].bEnable = FALSE;
		}
	}
	CDialog::OnTimer(nIDEvent);
}

void CSFRTILTOptionDlg::OnNMClickListRoi(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	iSavedItem = pNMItemActivate->iItem;
	iSavedSubitem = pNMItemActivate->iSubItem;

	if(iSavedSubitem == 0){
		SetTimer(150,5,NULL); //InitProgram을 불러들이는데 쓰인다. 
		UpdateData(FALSE);
	}

	*pResult = 0;
}

void CSFRTILTOptionDlg::OnBnClickedBtnAutoCheck()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
void CSFRTILTOptionDlg::OnBnClickedButtonSet()
{
	CString str_Buf = "";
	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;

	((CEdit *)GetDlgItem(IDC_EDIT_LINEPAIR))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "LINEPAIL_PIXEL", str_Buf, str_ModelPath);
	m_dLinePerPixel = atof(str_Buf);

	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_THRESHOLD))->GetWindowText(str_Buf);
	m_dThreshold = atof(str_Buf);

// 	if(m_dThreshold > 1.0){
// 		m_dThreshold = 1.0;
// 		str_Buf.Format("%0.2f", m_dThreshold);
// 	}
	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_THRESHOLD))->SetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "DEFAULT_THRESHOLD", str_Buf, str_ModelPath);

	for(int a_=0;a_<4; a_++){
		//SFR_DATA[a_].dThreshold = m_dThreshold;
		if (m_bSFRmode == 0){
			SFR_DATA[a_].dLinepare = m_dLinePerPixel;
		}
		else{
			SFR_DATA[a_].MTF = m_dLinePerPixel;
		}
	}
	Upload_List();
}

void CSFRTILTOptionDlg::OnBnClickedButtonSet2()
{
	CString str_Buf = "";
	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;

	((CEdit *)GetDlgItem(IDC_EDIT_LINEPAIR2))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "LINEPAIL_PIXEL_SIDE", str_Buf, str_ModelPath);
	m_dLinePerPixel_Side = atof(str_Buf);

	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_THRESHOLD2))->GetWindowText(str_Buf);
	m_dThreshold_Side = atof(str_Buf);

// 	if(m_dThreshold_Side > 1.0){
// 		m_dThreshold_Side = 1.0;
// 		str_Buf.Format("%0.2f", m_dThreshold_Side);
// 	}
	((CEdit *)GetDlgItem(IDC_EDIT_DEFAULT_SET_THRESHOLD2))->SetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "DEFAULT_THRESHOLD_SIDE", str_Buf, str_ModelPath);

	for(int a_=4;a_<24; a_++){
		//SFR_DATA[a_].dThreshold = m_dThreshold_Side;
		if (m_bSFRmode == 0){
			SFR_DATA[a_].dLinepare = m_dLinePerPixel;
		}
		else{
			SFR_DATA[a_].MTF = m_dLinePerPixel;
		}
	}
	Upload_List();
}
#pragma region LOT관련 함수
void CSFRTILTOptionDlg::LOT_Set_List(CListCtrl *List){
	CString Item[34]={"C1","C2","C3","C4","S1","S2","S3","S4","S5","S6","S7","S8","S9","S10","S11","S12","S13","S14","S15","S16","S17","S18","S19","S20","S21","S22","S23","S24","S25","S26","S27","S28","S29","S30"};

	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT 명",LVCFMT_CENTER, 100);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);

	for(int t=0; t<34; t++){
		List->InsertColumn(5+t,Item[t],LVCFMT_CENTER, 80);
	}
	List->InsertColumn(38, "XtiltDev", LVCFMT_CENTER, 80);
	List->InsertColumn(39, "Y1tiltDev", LVCFMT_CENTER, 80);
	List->InsertColumn(40, "Y2tiltDev", LVCFMT_CENTER, 80);
	List->InsertColumn(41,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(42,"비고",LVCFMT_CENTER, 80);
//161209	List->InsertColumn(41,((CImageTesterDlg *)m_pMomWnd)->InputStringData,LVCFMT_CENTER, 80);

	Lot_InsertNum =43;
	Copy_List(&m_Lot_SFRList, ((CImageTesterDlg *)m_pMomWnd)->m_pWorkList, Lot_InsertNum);
}

void CSFRTILTOptionDlg::LOT_InsertDataList(){
	CString strCnt="";

	//int Index = m_Lot_SFRList.InsertItem(Lot_StartCnt,"",0);

	Lot_InsertIndex = m_Lot_SFRList.InsertItem(Lot_StartCnt, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Lot_InsertIndex + 1);
	m_Lot_SFRList.SetItemText(Lot_InsertIndex, 0, strCnt);

	m_Lot_SFRList.SetItemText(Lot_InsertIndex, 1, ((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_Lot_SFRList.SetItemText(Lot_InsertIndex, 2, ((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);////////LOT 이름
	m_Lot_SFRList.SetItemText(Lot_InsertIndex, 3, ((CImageTesterDlg *)m_pMomWnd)->strDay + "");
	m_Lot_SFRList.SetItemText(Lot_InsertIndex, 4, ((CImageTesterDlg *)m_pMomWnd)->strTime + "");

// 	int CopyIndex = m_SFRList.GetItemCount() - 1;
// 
// 	int count =0;
// 	for(int t=0; t< Lot_InsertNum;t++){
// 		if((t != 2)&&(t!=0)){
// 			m_Lot_SFRList.SetItemText(Index,t, m_SFRList.GetItemText(CopyIndex,count));
// 			count++;
// 
// 		}else if(t==0){
// 			count++;
// 		}else{
// 			m_Lot_SFRList.SetItemText(Index,t,((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
// 		}
// 	}
// 
// 	Lot_StartCnt++;

}
void CSFRTILTOptionDlg::LOT_EXCEL_SAVE(){

	CString Item[37] = { "C1", "C2", "C3", "C4", "S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10", "S11", "S12", "S13", "S14", "S15", "S16", "S17", "S18", "S19", "S20", "S21", "S22", "S23", "S24", "S25", "S26", "S27", "S28", "S29", "S30", "XTilt", "Y1Tilt", "Y2Tilt" };

	CString Data ="";
	CString str="";

	if (m_bSFRmode == 0)
		str = "***********************SFR 검사 [ 단위 : MTF ] ***********************\n";
	else if (m_bSFRmode == 1)
		str = "***********************SFR 검사 [ 단위 : Cy/Pix ] ***********************\n";
	else if (m_bSFRmode == 2)
		str = "***********************SFR 검사 [ 단위 : Cy/mm ] ***********************\n";

	Data += str;
	for (int num = 0; num < 34; num++){
		if (SFR_DATA[num].bEnable == TRUE){
			if (num < 4){
				str.Empty();
				if (m_bSFRmode == 0)
					str.Format("C_%d,StartX, %d, StartY, %d, Width, %d, Height, %d,LinePair, %6.2f ,Threshold_MIN,%6.2f,Threshold_MAX,%6.2f,/,", num + 1, SFR_DATA[num].nPosX, SFR_DATA[num].nPosY, SFR_DATA[num].nWidth, SFR_DATA[num].nHeight, SFR_DATA[num].dLinepare, SFR_DATA[num].dThreshold_MIN, SFR_DATA[num].dThreshold_MAX);
				else
					str.Format("C_%d,StartX, %d, StartY, %d, Width, %d, Height, %d,MTF, %d ,Threshold_MIN,%6.2f,Threshold_MAX,%6.2f,/,", num + 1, SFR_DATA[num].nPosX, SFR_DATA[num].nPosY, SFR_DATA[num].nWidth, SFR_DATA[num].nHeight, SFR_DATA[num].MTF, SFR_DATA[num].dThreshold_MIN, SFR_DATA[num].dThreshold_MAX);
				Data += str;
			}
			else{
				str.Empty();
				if (m_bSFRmode == 0)
					str.Format("S_%d,StartX, %d, StartY, %d, Width, %d, Height, %d,LinePair, %6.2f ,Threshold_MIN,%6.2f,Threshold_MAX,%6.2f,/,", num - 3, SFR_DATA[num].nPosX, SFR_DATA[num].nPosY, SFR_DATA[num].nWidth, SFR_DATA[num].nHeight, SFR_DATA[num].dLinepare, SFR_DATA[num].dThreshold_MIN, SFR_DATA[num].dThreshold_MAX);
				else
					str.Format("S_%d,StartX, %d, StartY, %d, Width, %d, Height, %d,MTF, %d ,Threshold_MIN,%6.2f,Threshold_MAX,%6.2f,/,", num - 3, SFR_DATA[num].nPosX, SFR_DATA[num].nPosY, SFR_DATA[num].nWidth, SFR_DATA[num].nHeight, SFR_DATA[num].MTF, SFR_DATA[num].dThreshold_MIN, SFR_DATA[num].dThreshold_MAX);
				Data += str;
			}
		}
		if ((num == 3) || (num == 18) || (num == 33))
		{
			str.Empty();
			str.Format(",\n ");
			Data += str;
		}
	}
	str.Empty();
	str = "*************************************************\n";
	Data += str;

// 	if (m_bSFRmode == 0){
// 		str = "***********************SFR 검사***********************\n";
// 		Data += str;
// 		str.Empty();
// 		str.Format("C_1,LinePair, %6.2f ,Threshold,%6.2f,/,C_2,LinePair, %6.2f ,Threshold,%6.2f,/,C_3,LinePair, %6.2f ,Threshold,%6.2f,/,C_4,LinePair, %6.2f ,Threshold,%6.2f,,\n ", SFR_DATA[0].dLinepare, SFR_DATA[0].dThreshold, SFR_DATA[1].dLinepare, SFR_DATA[1].dThreshold, SFR_DATA[2].dLinepare, SFR_DATA[2].dThreshold, SFR_DATA[3].dLinepare, SFR_DATA[3].dThreshold);
// 		Data += str;
// 		str.Empty();
// 		str.Format("S_1,LinePair, %6.2f ,Threshold,%6.2f,/,S_2,LinePair, %6.2f ,Threshold,%6.2f,/,S_3,LinePair, %6.2f ,Threshold,%6.2f,/,S_4,LinePair, %6.2f ,Threshold,%6.2f,S_5,LinePair, %6.2f ,Threshold,%6.2f,S_6,LinePair, %6.2f ,Threshold,%6.2f,/,S_7,LinePair, %6.2f ,Threshold,%6.2f,/,S_8,LinePair, %6.2f ,Threshold,%6.2f,/,S_9,LinePair, %6.2f ,Threshold,%6.2f,S_10,LinePair, %6.2f ,Threshold,%6.2f,S_11,LinePair, %6.2f ,Threshold,%6.2f,/,S_12,LinePair, %6.2f ,Threshold,%6.2f,/,S_13,LinePair, %6.2f ,Threshold,%6.2f,/,S_14,LinePair, %6.2f ,Threshold,%6.2f,S_15,LinePair, %6.2f ,Threshold,%6.2f,/,\n  ", SFR_DATA[4].dLinepare, SFR_DATA[4].dThreshold, SFR_DATA[5].dLinepare, SFR_DATA[5].dThreshold, SFR_DATA[6].dLinepare, SFR_DATA[6].dThreshold, SFR_DATA[7].dLinepare, SFR_DATA[7].dThreshold, SFR_DATA[8].dLinepare, SFR_DATA[8].dThreshold, SFR_DATA[9].dLinepare, SFR_DATA[9].dThreshold, SFR_DATA[10].dLinepare, SFR_DATA[10].dThreshold, SFR_DATA[11].dLinepare, SFR_DATA[11].dThreshold, SFR_DATA[12].dLinepare, SFR_DATA[12].dThreshold, SFR_DATA[13].dLinepare, SFR_DATA[13].dThreshold, SFR_DATA[14].dLinepare, SFR_DATA[14].dThreshold, SFR_DATA[15].dLinepare, SFR_DATA[15].dThreshold, SFR_DATA[16].dLinepare, SFR_DATA[16].dThreshold, SFR_DATA[17].dLinepare, SFR_DATA[17].dThreshold, SFR_DATA[18].dLinepare, SFR_DATA[18].dThreshold);
// 		Data += str;
// 		str.Empty();
// 		str.Format("S_16,LinePair, %6.2f ,Threshold,%6.2f,/,S_17,LinePair, %6.2f ,Threshold,%6.2f,/,S_18,LinePair, %6.2f ,Threshold,%6.2f,/,S_19,LinePair, %6.2f ,Threshold,%6.2f,S_20,LinePair, %6.2f ,Threshold,%6.2f,S_21,LinePair, %6.2f ,Threshold,%6.2f,/,S_22,LinePair, %6.2f ,Threshold,%6.2f,/,S_23,LinePair, %6.2f ,Threshold,%6.2f,/,S_24,LinePair, %6.2f ,Threshold,%6.2f,S_25,LinePair, %6.2f ,Threshold,%6.2f,S_26,LinePair, %6.2f ,Threshold,%6.2f,/,S_27,LinePair, %6.2f ,Threshold,%6.2f,/,S_28,LinePair, %6.2f ,Threshold,%6.2f,/,S_29,LinePair, %6.2f ,Threshold,%6.2f,S_30,LinePair, %6.2f ,Threshold,%6.2f,/,\n", SFR_DATA[19].dLinepare, SFR_DATA[19].dThreshold, SFR_DATA[20].dLinepare, SFR_DATA[20].dThreshold, SFR_DATA[21].dLinepare, SFR_DATA[21].dThreshold, SFR_DATA[22].dLinepare, SFR_DATA[22].dThreshold, SFR_DATA[23].dLinepare, SFR_DATA[23].dThreshold, SFR_DATA[24].dLinepare, SFR_DATA[24].dThreshold, SFR_DATA[25].dLinepare, SFR_DATA[25].dThreshold, SFR_DATA[26].dLinepare, SFR_DATA[26].dThreshold, SFR_DATA[27].dLinepare, SFR_DATA[27].dThreshold, SFR_DATA[28].dLinepare, SFR_DATA[28].dThreshold, SFR_DATA[29].dLinepare, SFR_DATA[29].dThreshold, SFR_DATA[30].dLinepare, SFR_DATA[30].dThreshold, SFR_DATA[31].dLinepare, SFR_DATA[31].dThreshold, SFR_DATA[32].dLinepare, SFR_DATA[32].dThreshold, SFR_DATA[33].dLinepare, SFR_DATA[33].dThreshold);
// 		Data += str;
// 		str.Empty();
// 		str = "*************************************************\n";
// 		Data += str;
// 	}
// 	else{
// 		str = "***********************SFR 검사***********************\n";
// 		Data += str;
// 		str.Empty();
// 		str.Format("C_1,MTF, %6.2f ,Threshold,%6.2f,/,C_2,MTF, %6.2f ,Threshold,%6.2f,/,C_3,MTF, %6.2f ,Threshold,%6.2f,/,C_4,MTF, %6.2f ,Threshold,%6.2f,,\n ", SFR_DATA[0].MTF, SFR_DATA[0].dThreshold, SFR_DATA[1].MTF, SFR_DATA[1].dThreshold, SFR_DATA[2].MTF, SFR_DATA[2].dThreshold, SFR_DATA[3].MTF, SFR_DATA[3].dThreshold);
// 		Data += str;
// 		str.Empty();
// 		str.Format("S_1,MTF, %6.2f ,Threshold,%6.2f,/,S_2,MTF, %6.2f ,Threshold,%6.2f,/,S_3,MTF, %6.2f ,Threshold,%6.2f,/,S_4,MTF, %6.2f ,Threshold,%6.2f,S_5,MTF, %6.2f ,Threshold,%6.2f,S_6,MTF, %6.2f ,Threshold,%6.2f,/,S_7,MTF, %6.2f ,Threshold,%6.2f,/,S_8,MTF, %6.2f ,Threshold,%6.2f,/,S_9,MTF, %6.2f ,Threshold,%6.2f,S_10,MTF, %6.2f ,Threshold,%6.2f,S_11,MTF, %6.2f ,Threshold,%6.2f,/,S_12,MTF, %6.2f ,Threshold,%6.2f,/,S_13,MTF, %6.2f ,Threshold,%6.2f,/,S_14,MTF, %6.2f ,Threshold,%6.2f,S_15,MTF, %6.2f ,Threshold,%6.2f,/,\n  ", SFR_DATA[4].MTF, SFR_DATA[4].dThreshold, SFR_DATA[5].MTF, SFR_DATA[5].dThreshold, SFR_DATA[6].MTF, SFR_DATA[6].dThreshold, SFR_DATA[7].MTF, SFR_DATA[7].dThreshold, SFR_DATA[8].MTF, SFR_DATA[8].dThreshold, SFR_DATA[9].MTF, SFR_DATA[9].dThreshold, SFR_DATA[10].MTF, SFR_DATA[10].dThreshold, SFR_DATA[11].MTF, SFR_DATA[11].dThreshold, SFR_DATA[12].MTF, SFR_DATA[12].dThreshold, SFR_DATA[13].MTF, SFR_DATA[13].dThreshold, SFR_DATA[14].MTF, SFR_DATA[14].dThreshold, SFR_DATA[15].MTF, SFR_DATA[15].dThreshold, SFR_DATA[16].MTF, SFR_DATA[16].dThreshold, SFR_DATA[17].MTF, SFR_DATA[17].dThreshold, SFR_DATA[18].MTF, SFR_DATA[18].dThreshold);
// 		Data += str;
// 		str.Empty();
// 		str.Format("S_16,MTF, %6.2f ,Threshold,%6.2f,/,S_17,MTF, %6.2f ,Threshold,%6.2f,/,S_18,MTF, %6.2f ,Threshold,%6.2f,/,S_19,MTF, %6.2f ,Threshold,%6.2f,S_20,MTF, %6.2f ,Threshold,%6.2f,S_21,MTF, %6.2f ,Threshold,%6.2f,/,S_22,MTF, %6.2f ,Threshold,%6.2f,/,S_23,MTF, %6.2f ,Threshold,%6.2f,/,S_24,MTF, %6.2f ,Threshold,%6.2f,S_25,MTF, %6.2f ,Threshold,%6.2f,S_26,MTF, %6.2f ,Threshold,%6.2f,/,S_27,MTF, %6.2f ,Threshold,%6.2f,/,S_28,MTF, %6.2f ,Threshold,%6.2f,/,S_29,MTF, %6.2f ,Threshold,%6.2f,S_30,MTF, %6.2f ,Threshold,%6.2f,/,\n", SFR_DATA[19].MTF, SFR_DATA[19].dThreshold, SFR_DATA[20].MTF, SFR_DATA[20].dThreshold, SFR_DATA[21].MTF, SFR_DATA[21].dThreshold, SFR_DATA[22].MTF, SFR_DATA[22].dThreshold, SFR_DATA[23].MTF, SFR_DATA[23].dThreshold, SFR_DATA[24].MTF, SFR_DATA[24].dThreshold, SFR_DATA[25].MTF, SFR_DATA[25].dThreshold, SFR_DATA[26].MTF, SFR_DATA[26].dThreshold, SFR_DATA[27].MTF, SFR_DATA[27].dThreshold, SFR_DATA[28].MTF, SFR_DATA[28].dThreshold, SFR_DATA[29].MTF, SFR_DATA[29].dThreshold, SFR_DATA[30].MTF, SFR_DATA[30].dThreshold, SFR_DATA[31].MTF, SFR_DATA[31].dThreshold, SFR_DATA[32].MTF, SFR_DATA[32].dThreshold, SFR_DATA[33].MTF, SFR_DATA[33].dThreshold);
// 		Data += str;
// 		str.Empty();
// 		str = "*************************************************\n";
// 		Data += str;
// 	}

	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("SFR_TEST",Item,37,&m_Lot_SFRList,Data);
}

bool CSFRTILTOptionDlg::LOT_EXCEL_UPLOAD(){
	m_Lot_SFRList.DeleteAllItems();

	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("SFR_TEST",&m_Lot_SFRList,1,&Lot_StartCnt)==TRUE)
	{
		return TRUE;
	}
	else
	{
		Lot_StartCnt = 0;
		return FALSE;
	}

	return TRUE ;
}

#pragma endregion 

CString CSFRTILTOptionDlg::Mes_Result()
{
	CString sz;

	m_szMesResult.Empty();

	if (b_StopFail == FALSE)
	{
		sz.Format(_T("%s:%d"), m_stMes.szData[0], m_stMes.nResult[0]);
		sz.Format(_T("%s:%d"), m_stMes.szData[1], m_stMes.nResult[1]);
		sz.Format(_T("%s:%d"), m_stMes.szData[2], m_stMes.nResult[2]);

		for (int _a = 3; _a < 37; _a++)
		{
			if (SFR_DATA[_a - 3].bEnable)
				sz.Format(_T("%s:%d"), m_stMes.szData[_a], m_stMes.nResult[_a]);
			else
				sz = _T(":1");

			if (_a != 36)
				sz += _T(",");

			m_szMesResult += sz;
		}
	}
	else{
		for (int _a = 0; _a < 37; _a++)
		{
			sz = _T(":1");

			if (_a != 36)
				sz += _T(",");

			m_szMesResult += sz;
		}
	}
	// 제일 마지막 항목을 추가하자
// 	int nSel = m_SFRList.GetItemCount()-1;
// 
// 	for(int a_=0;a_ <34;a_++)
// 	{
// 		sz[a_] = m_SFRList.GetItemText(nSel, a_+4);
// 
// 		if(SFR_DATA[a_].m_Success )
// 			nResult[a_] = 1;
// 		else 
// 			nResult[a_] = 0;
// 	}
// 	for(int lop = 0;lop<34;lop++){
// 		if(sz[lop] == 'X'){
// 			nResult[lop] = 1;	
// 		}
// 	}
// 
// 	CString v_delta;
// 	v_delta.Format("%.2f", delta_V);
// 	CString h_delta;
// 	h_delta.Format("%.2f", delta_H);
// 
// 	int v_result, h_result;
// 
// 	if(delta_V_Success == TRUE)
// 		v_result = 1;
// 	else
// 		v_result = 0;
// 
// 	if(delta_H_Success == TRUE)
// 		h_result = 1;
// 	else
// 		h_result = 0;
// 
// 	if(b_StopFail == FALSE)
// 	{
// 		m_szMesResult.Format(_T("%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d")
// 			, sz[0], nResult[0], sz[1], nResult[1], sz[2], nResult[2], sz[3], nResult[3], sz[4], nResult[4],sz[5], nResult[5], sz[6], nResult[6], sz[7], nResult[7], sz[8], nResult[8], sz[9], nResult[9], sz[10], nResult[10], sz[11], nResult[11]
// 		, sz[12], nResult[12], sz[13], nResult[13], sz[14], nResult[14], sz[15], nResult[15], sz[16], nResult[16], sz[17], nResult[17], sz[18], nResult[18], sz[19], nResult[19], sz[20], nResult[20], sz[21], nResult[21], sz[22], nResult[22], sz[23], nResult[23]
// 		, h_delta, h_result, v_delta, v_result);
// 	}
// 	else
// 	{
// 		m_szMesResult.Format(":1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1, :1");
// 	}
	((CImageTesterDlg  *)m_pMomWnd)->m_stNewMesData[NewMES_SFR] = m_szMesResult;

	return m_szMesResult;
}

void CSFRTILTOptionDlg::CopyMesData()
{


	m_stMes.szData[0].Format(_T("%.2f"), SFR_DATA[0].dResultData);
	m_stMes.szData[1].Format(_T("%.2f"), SFR_DATA[1].dResultData);
	m_stMes.szData[2].Format(_T("%.2f"), SFR_DATA[2].dResultData);

	m_stMes.nResult[0] = EachTiltResult[0];
	m_stMes.nResult[1] = EachTiltResult[1];
	m_stMes.nResult[2] = EachTiltResult[2];

	

	for (int _a = 3; _a < 37; _a++)
	{
		if (TRUE == SFR_DATA[_a - 3].bEnable)
		{
			m_stMes.szData[_a].Format(_T("%.2f"), SFR_DATA[_a - 3].dResultData);

			if (SFR_DATA[_a - 3].m_Success)
				m_stMes.nResult[_a] = TEST_OK;
			else
				m_stMes.nResult[_a] = TEST_NG;
		}
	}
}

void CSFRTILTOptionDlg::OnBnClickedCheckEdgeoff()
{
	CString str_ModelPath= ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);

	CString str;
	//((CImageTesterDlg *)m_pMomWnd)->b_Edge_Off = b_Edge_Off;
	str.Empty();
	str.Format("%d",b_Edge_Off);
	WritePrivateProfileString(str_model,"EDGE_OFF",str,str_ModelPath);

	b_Edge_Off = GetPrivateProfileInt(str_model,"EDGE_OFF",1,str_ModelPath);

	UpdateData(FALSE);
}

double CSFRTILTOptionDlg::ComputeDelta(double side[])
{
	double min=0.0,max=0.0;
	double delta=0.0;

	min = side[0];
	max = side[0];

	for(int i = 0; i < 4; i++)
	{
		if(side[i] < min)
			min = side[i];

		if(side[i] > max)
			max = side[i];
	}


	delta = max - min;

	return delta;
}
void CSFRTILTOptionDlg::OnBnClickedButtontest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(0);
	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(1);
}

void CSFRTILTOptionDlg::OnBnClickedDeltaSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str_Buf = "";
	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;

	((CEdit *)GetDlgItem(IDC_EDIT_VDELTA))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "V_DELTA", str_Buf, str_ModelPath);
	delta_V_Thre = atof(str_Buf);

	((CEdit *)GetDlgItem(IDC_EDIT_HDELTA))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "H_DELTA", str_Buf, str_ModelPath);
	delta_H_Thre = atof(str_Buf);

	
}

void CSFRTILTOptionDlg::OnBnClickedButtonSaveOp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//CString str;
	//m_ImageSaveMode = m_Combo_ImageSave.GetCurSel();

	//str.Empty();
	//str.Format("%d",m_ImageSaveMode);
	//WritePrivateProfileString("SFR_DETECT","ImageSaveMode",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
}

void CSFRTILTOptionDlg::OnBnClickedButtonPathopen()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//ITEMIDLIST        *pidlBrowse;
	//char    SaveFolderPath[2046] = {0,}; 
	//BROWSEINFO BrInfo;
	//BrInfo.hwndOwner = NULL; 
	//BrInfo.pidlRoot = NULL;
	//memset( &BrInfo, 0, sizeof(BrInfo) );
	//BrInfo.pszDisplayName = SaveFolderPath;
	//BrInfo.lpszTitle = "저장할 디렉토리를 선택하세요";
	//BrInfo.ulFlags = BIF_RETURNONLYFSDIRS;

	//// Show Dialog
	//pidlBrowse = ::SHBrowseForFolder(&BrInfo);    

	//if( pidlBrowse != NULL)
	//{
	//	//   Get Path
	//	UpdateData(TRUE);
	//	::SHGetPathFromIDList(pidlBrowse, SaveFolderPath);   
	//	str_ImageSavePath = SaveFolderPath;

	//	UpdateData(FALSE);
	//	WritePrivateProfileString("SFR_DETECT","ImageSavePath",str_ImageSavePath,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);	
	//}
}

void CSFRTILTOptionDlg::Image_Save(UINT MODE, BOOL Result){

	/*if (SFR_Image_NoSave == MODE)
	{
		return;
	}

	((CImageTesterDlg *)m_pMomWnd)->b_SFR_Result = Result;

	CString str_Failpath;
	str_Failpath.Format("%s\\%s",str_ImageSavePath,g_szPath[1]);

	folder_gen(str_Failpath);

	CString str_Passpath;
	str_Passpath.Format("%s\\%s",str_ImageSavePath,g_szPath[0]);
	folder_gen(str_Passpath);

	CString str,str_ori;
	CString str_Pass_Daypath;
	CString str_Fail_Daypath;

	CTime thetime = CTime::GetCurrentTime();
	int y=thetime.GetYear(); int m=thetime.GetMonth(); int d=thetime.GetDay();
	str.Format("%04d%02d%02d",y,m,d);

	str_Fail_Daypath =str_Failpath  +(("\\")+str);
	str_Pass_Daypath = str_Passpath + (("\\")+str);

	folder_gen(str_Fail_Daypath);
	folder_gen(str_Pass_Daypath);
	int hour=thetime.GetHour(); int minute=thetime.GetMinute(); int second=thetime.GetSecond();

	if (((CImageTesterDlg *)m_pMomWnd)->b_UserMode == TRUE)
	{
		str.Empty();
		str.Format("%02dh_%02dm_%02ds.bmp",hour,minute,second);
		str_ori.Format("%02dh_%02dm_%02ds_Origin.bmp",hour,minute,second);
	}else{
		CString lotid = ((CImageTesterDlg *)m_pMomWnd)->m_strLotIdFull;

		if (lotid == "")
		{
			lotid.Format("NoLotId_%02dh_%02dm_%02ds",hour,minute,second);
		}
		str.Empty();
		str.Format("%s_%02dh_%02dm_%02ds.bmp",lotid,hour,minute,second);
		str_ori.Format("%s_%02dh_%02dm_%02ds_Origin.bmp",lotid,hour,minute,second);
	}



	CString str_FailFullPath,str_PassFullPath;

	if (MODE != SFR_Image_NoSave)
	{
		((CImageTesterDlg *)m_pMomWnd)->imagepic_Failcopy = str_Fail_Daypath +"\\"+ str;
		((CImageTesterDlg *)m_pMomWnd)->imagepic_Passcopy = str_Pass_Daypath +"\\"+ str;

		((CImageTesterDlg *)m_pMomWnd)->i_CaptureSFRMode = MODE;
		((CImageTesterDlg *)m_pMomWnd)->b_SFRCaptureMode = TRUE;
		DoEvents(300);
		IplImage *OriginImage = cvCreateImage(cvSize(1280, 720), IPL_DEPTH_8U, 3);
		BYTE R,G,B;
		double Sum_Y;

		for (int y=0; y<720; y++)
		{
			for (int x=0; x<1280; x++)
			{
				B = m_RGBScanbuf[y*(1280*4) + x*4    ];
				G = m_RGBScanbuf[y*(1280*4) + x*4 + 1];
				R = m_RGBScanbuf[y*(1280*4) + x*4 + 2];

				OriginImage->imageData[y*OriginImage->widthStep+x* 3 + 0] = B;
				OriginImage->imageData[y*OriginImage->widthStep+x* 3 + 1] = G;
				OriginImage->imageData[y*OriginImage->widthStep+x* 3 + 2] = R;
			}
		}
		if (Result == FALSE)
		{
			cvSaveImage(str_Fail_Daypath + "\\"+str_ori,OriginImage);
		}

		if(MODE == SFR_Image_ALLSave){
			if (Result == TRUE)
			{
				cvSaveImage(str_Pass_Daypath + "\\"+str_ori,OriginImage);
			}
		}
		cvReleaseImage(&OriginImage);

		DoEvents(300);
	}*/
}
void CSFRTILTOptionDlg::OnBnClickedButton1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//((CImageTesterDlg *)m_pMomWnd)->SEND_I2C_EDGE(TRUE);
	//((CImageTesterDlg *)m_pMomWnd)->Edge_Control(1);
}

void CSFRTILTOptionDlg::OnBnClickedButton5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//((CImageTesterDlg *)m_pMomWnd)->SEND_I2C_EDGE(FALSE);

	/*((CImageTesterDlg *)m_pMomWnd)->Edge_Control(0);*/
}

void CSFRTILTOptionDlg::OnCbnSelchangeComboSfrmode()
{
	CString str = "";
	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;

	m_bSFRmode = pSFRMode->GetCurSel();
	str.Format("%d", m_bSFRmode);
	WritePrivateProfileString(str_model, "SFR_Mode", str, str_ModelPath);
	CHANGESELLIST(m_bSFRmode);
	Upload_List();
}
void CSFRTILTOptionDlg::CHANGESELLIST(BOOL MODE){
	LV_COLUMN lCol;
	lCol.mask = LVCF_TEXT;

	if (MODE == 0){
		lCol.pszText = "Linepair";
		m_ROIList.SetColumn(m_nIdex, &lCol);
	}
	else{
		lCol.pszText = "MTF";
		m_ROIList.SetColumn(m_nIdex, &lCol);
	}
}

void CSFRTILTOptionDlg::OnBnClickedAutomationCheck()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (m_bAutomation == TRUE)
	{
		m_bAutomation = FALSE;
	}
	else{
		m_bAutomation = TRUE;
	}
	UpdateData(FALSE);

	CString str = "";
	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;


	str.Format("%d", m_bAutomation);
	WritePrivateProfileString("SFR_DETECT", "Automation_roi", str, str_ModelPath);

}


void CSFRTILTOptionDlg::Reload_Rect(){

	for (int i = 0; i < 34; i++)
	{
		SFR_DATA[i].nPosX = INIT_SFR_DATA[i].nPosX;
		SFR_DATA[i].nPosY = INIT_SFR_DATA[i].nPosY;
		SFR_DATA[i].nWidth = INIT_SFR_DATA[i].nWidth;
		SFR_DATA[i].nHeight = INIT_SFR_DATA[i].nHeight;
	}
}

void CSFRTILTOptionDlg::OnCbnSelendokComboImageSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_nImageSaveMode = m_Cb_ImageSave.GetCurSel();

	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);

	CString str;
	str.Empty();
	str.Format("%d", m_nImageSaveMode);
	WritePrivateProfileString("SFR_DETECT", "Image_SaveMode", str, str_ModelPath);
}


void CSFRTILTOptionDlg::OnCbnSelendokComboXtilt1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_nXtilt1 = m_Cb_XTilt1.GetCurSel();

	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);

	CString str;
	str.Empty();
	str.Format("%d", m_nXtilt1);
	WritePrivateProfileString("SFR_DETECT", "XTilt1", str, str_ModelPath);
}


void CSFRTILTOptionDlg::OnCbnSelendokComboXtilt2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_nXtilt2 = m_Cb_XTilt2.GetCurSel();

	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);

	CString str;
	str.Empty();
	str.Format("%d", m_nXtilt2);
	WritePrivateProfileString("SFR_DETECT", "XTilt2", str, str_ModelPath);
}


void CSFRTILTOptionDlg::OnCbnSelendokComboY1tilt1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_nY1tilt1 = m_Cb_Y1Tilt1.GetCurSel();

	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);

	CString str;
	str.Empty();
	str.Format("%d", m_nY1tilt1);
	WritePrivateProfileString("SFR_DETECT", "Y1Tilt1", str, str_ModelPath);
}


void CSFRTILTOptionDlg::OnCbnSelendokComboY1tilt2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_nY1tilt2 = m_Cb_Y1Tilt2.GetCurSel();

	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);

	CString str;
	str.Empty();
	str.Format("%d", m_nY1tilt2);
	WritePrivateProfileString("SFR_DETECT", "Y1Tilt2", str, str_ModelPath);
}


void CSFRTILTOptionDlg::OnCbnSelendokComboY2tilt1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_nY2tilt1 = m_Cb_Y2Tilt1.GetCurSel();

	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);

	CString str;
	str.Empty();
	str.Format("%d", m_nY2tilt1);
	WritePrivateProfileString("SFR_DETECT", "Y2Tilt1", str, str_ModelPath);
}


void CSFRTILTOptionDlg::OnCbnSelendokComboY2tilt2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_nY2tilt2 = m_Cb_Y2Tilt2.GetCurSel();

	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);

	CString str;
	str.Empty();
	str.Format("%d", m_nY2tilt2);
	WritePrivateProfileString("SFR_DETECT", "Y2Tilt2", str, str_ModelPath);
}


void CSFRTILTOptionDlg::OnBnClickedButtonTiltSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	CString str_Buf = "";

	((CEdit *)GetDlgItem(IDC_EDIT_XTilt_upper_limit))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "XtiltSpec", str_Buf, str_ModelPath);
	m_dXtilt = atof(str_Buf);

	((CEdit *)GetDlgItem(IDC_EDIT_Y1_Tilt_upper_limit))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "Y1tiltSpec", str_Buf, str_ModelPath);
	m_dY1tilt = atof(str_Buf);

	((CEdit *)GetDlgItem(IDC_EDIT_Y2_Tilt_upper_limit))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "Y2tiltSpec", str_Buf, str_ModelPath);
	m_dY2tilt = atof(str_Buf);
}


void CSFRTILTOptionDlg::OnBnClickedButtonCaptureCountSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str_Buf = "";

	int nCapture_Count = 0;

	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	// default Set
	((CEdit *)GetDlgItem(IDC_EDIT_CAPTURE_COUNT))->GetWindowText(str_Buf);
	WritePrivateProfileString("SFR_DETECT", "CAPTURE_COUNT", str_Buf, str_ModelPath);


	((CEdit *)GetDlgItem(IDC_EDIT_CAPTURE_COUNT))->GetWindowText(str_Buf);
	nCapture_Count = atoi(str_Buf);
	m_Capture_Count = nCapture_Count;

}
