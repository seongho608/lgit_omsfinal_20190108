// Distortion_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Distortion_ResultView.h"


// CDistortion_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDistortion_ResultView, CDialog)

CDistortion_ResultView::CDistortion_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CDistortion_ResultView::IDD, pParent)
{
}

CDistortion_ResultView::~CDistortion_ResultView()
{
}

void CDistortion_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDistortion_ResultView, CDialog)
	ON_WM_CTLCOLOR()
	ON_EN_SETFOCUS(IDC_STATIC_LEFT_C, &CDistortion_ResultView::OnEnSetfocusStaticLeftC)
	ON_EN_SETFOCUS(IDC_VALUE_LEFT_C, &CDistortion_ResultView::OnEnSetfocusValueLeftC)
	ON_EN_SETFOCUS(IDC_RESULT_LEFT_C, &CDistortion_ResultView::OnEnSetfocusResultLeftC)
	ON_EN_SETFOCUS(IDC_STATIC_TOP_C, &CDistortion_ResultView::OnEnSetfocusStaticTopC)
	ON_EN_SETFOCUS(IDC_VALUE_TOP_C, &CDistortion_ResultView::OnEnSetfocusValueTopC)
	ON_EN_SETFOCUS(IDC_RESULT_TOP_C, &CDistortion_ResultView::OnEnSetfocusResultTopC)
	ON_EN_SETFOCUS(IDC_STATIC_RIGHT_C, &CDistortion_ResultView::OnEnSetfocusStaticRightC)
	ON_EN_SETFOCUS(IDC_VALUE_RIGHT_C, &CDistortion_ResultView::OnEnSetfocusValueRightC)
	ON_EN_SETFOCUS(IDC_RESULT_RIGHT_C, &CDistortion_ResultView::OnEnSetfocusResultRightC)
	ON_EN_SETFOCUS(IDC_STATIC_BOTTOM_C, &CDistortion_ResultView::OnEnSetfocusStaticBottomC)
	ON_EN_SETFOCUS(IDC_VALUE_BOTTOM_C, &CDistortion_ResultView::OnEnSetfocusValueBottomC)
	ON_EN_SETFOCUS(IDC_RESULT_BOTTOM_C, &CDistortion_ResultView::OnEnSetfocusResultBottomC)
END_MESSAGE_MAP()


// CDistortion_ResultView 메시지 처리기입니다.
void CDistortion_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CDistortion_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
BOOL CDistortion_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();
	Font_Size_Change(IDC_STATIC_LEFT_C,&ft,100,20);
	GetDlgItem(IDC_VALUE_LEFT_C)->SetFont(&ft);
	GetDlgItem(IDC_RESULT_LEFT_C)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_RIGHT_C)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_RIGHT_C)->SetFont(&ft);
	GetDlgItem(IDC_RESULT_RIGHT_C)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_TOP_C)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_TOP_C)->SetFont(&ft);
	GetDlgItem(IDC_RESULT_TOP_C)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_BOTTOM_C)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_BOTTOM_C)->SetFont(&ft);
	GetDlgItem(IDC_RESULT_BOTTOM_C)->SetFont(&ft);
	
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	initstat();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDistortion_ResultView::initstat()
{
	LC_TEXT("DISTORTION");
	RC_TEXT("RIGHT-CENTER");
	TC_TEXT("TOP-CENTER");
	BC_TEXT("BOTTOM-CENTER");

	LC_VALUE("");
	RC_VALUE("");
	TC_VALUE("");
	BC_VALUE("");

	LC_RESULT(0,"STAND BY");
	RC_RESULT(0,"STAND BY");
	TC_RESULT(0,"STAND BY");
	BC_RESULT(0,"STAND BY");
}

HBRUSH CDistortion_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_LEFT_C){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_LEFT_C){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() ==IDC_RESULT_LEFT_C){
		pDC->SetTextColor(txcol_TVal);
		pDC->SetBkColor(bkcol_TVal);
	}

	if(pWnd->GetDlgCtrlID() == IDC_STATIC_RIGHT_C){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_RIGHT_C){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() ==IDC_RESULT_RIGHT_C){
		pDC->SetTextColor(txcol_TVal);
		pDC->SetBkColor(bkcol_TVal);
	}

	if(pWnd->GetDlgCtrlID() == IDC_STATIC_TOP_C){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_TOP_C){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() ==IDC_RESULT_TOP_C){
		pDC->SetTextColor(txcol_TVal);
		pDC->SetBkColor(bkcol_TVal);
	}
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_BOTTOM_C){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_BOTTOM_C){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() ==IDC_RESULT_BOTTOM_C){
		pDC->SetTextColor(txcol_TVal);
		pDC->SetBkColor(bkcol_TVal);
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CDistortion_ResultView::LC_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_LEFT_C))->SetWindowText(lpcszString);
}
void CDistortion_ResultView::RC_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_RIGHT_C))->SetWindowText(lpcszString);
}
void CDistortion_ResultView::TC_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_TOP_C))->SetWindowText(lpcszString);
}
void CDistortion_ResultView::BC_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_BOTTOM_C))->SetWindowText(lpcszString);
}

void CDistortion_ResultView::LC_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_LEFT_C))->SetWindowText(lpcszString);
}
void CDistortion_ResultView::RC_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_RIGHT_C))->SetWindowText(lpcszString);
}
void CDistortion_ResultView::TC_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_TOP_C))->SetWindowText(lpcszString);
}
void CDistortion_ResultView::BC_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_BOTTOM_C))->SetWindowText(lpcszString);
}

void CDistortion_ResultView::LC_RESULT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}

	((CEdit *)GetDlgItem(IDC_RESULT_LEFT_C))->SetWindowText(lpcszString);
}
void CDistortion_ResultView::RC_RESULT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}

	((CEdit *)GetDlgItem(IDC_RESULT_RIGHT_C))->SetWindowText(lpcszString);
}
void CDistortion_ResultView::TC_RESULT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}

	((CEdit *)GetDlgItem(IDC_RESULT_TOP_C))->SetWindowText(lpcszString);
}
void CDistortion_ResultView::BC_RESULT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}

	((CEdit *)GetDlgItem(IDC_RESULT_BOTTOM_C))->SetWindowText(lpcszString);
}
BOOL CDistortion_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CDistortion_ResultView::OnEnSetfocusStaticLeftC()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CDistortion_ResultView::OnEnSetfocusValueLeftC()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_ResultView::OnEnSetfocusResultLeftC()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_ResultView::OnEnSetfocusStaticTopC()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_ResultView::OnEnSetfocusValueTopC()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_ResultView::OnEnSetfocusResultTopC()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_ResultView::OnEnSetfocusStaticRightC()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_ResultView::OnEnSetfocusValueRightC()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_ResultView::OnEnSetfocusResultRightC()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_ResultView::OnEnSetfocusStaticBottomC()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_ResultView::OnEnSetfocusValueBottomC()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_ResultView::OnEnSetfocusResultBottomC()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
