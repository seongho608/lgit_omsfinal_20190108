// ExistLot.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "ExistLot.h"


// CExistLot 대화 상자입니다.

IMPLEMENT_DYNAMIC(CExistLot, CDialog)

CExistLot::CExistLot(CWnd* pParent /*=NULL*/)
	: CDialog(CExistLot::IDD, pParent)
{

}

CExistLot::~CExistLot()
{
}

void CExistLot::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

void CExistLot::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}
BEGIN_MESSAGE_MAP(CExistLot, CDialog)
	ON_BN_CLICKED(IDOK, &CExistLot::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CExistLot::OnBnClickedCancel)
END_MESSAGE_MAP()


// CExistLot 메시지 처리기입니다.

void CExistLot::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->LOT_RESET_LIST(1);
	OnOK();
}

void CExistLot::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	((CImageTesterDlg  *)m_pMomWnd)->Lot_StartCnt=0;
	((CImageTesterDlg  *)m_pMomWnd)->Lot_Totalint=0;
	((CImageTesterDlg  *)m_pMomWnd)->Lot_Passint=0;
	((CImageTesterDlg  *)m_pMomWnd)->Lot_Failint=0;
	((CImageTesterDlg  *)m_pMomWnd)->Lot_Percentint=0;
	
	OnCancel();
}

BOOL CExistLot::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
