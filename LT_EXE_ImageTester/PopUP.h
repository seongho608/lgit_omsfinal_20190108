#pragma once


// CPopUP 대화 상자입니다.

class CPopUP : public CDialog
{
	DECLARE_DYNAMIC(CPopUP)

public:
	CPopUP(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPopUP();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_POPUP };
	void TEXT_CHANGE(LPCSTR lpcszString, ...);
	void	Setup(CWnd* IN_pMomWnd);
	CString Warning_TEXT;
	CFont	Font;
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	int		ILMode;
	BOOL	b_SuccessTrue;
private:
	CWnd		*m_pMomWnd;protected:
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
