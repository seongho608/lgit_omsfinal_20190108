// Particle_View_LG.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Particle_View_LG.h"


// CParticle_View_LG 대화 상자입니다.

IMPLEMENT_DYNAMIC(CParticle_View_LG, CDialog)

CParticle_View_LG::CParticle_View_LG(CWnd* pParent /*=NULL*/)
	: CDialog(CParticle_View_LG::IDD, pParent)
	, str_SaveName(_T(""))
	, str_SavePathedit(_T(""))
{
str_OutName = "";
	str_Path = "";
	str_YPath = "";
	str_YuvPath = "";
	str_BmpPath = "";
	str_CsvPath = "";
}

CParticle_View_LG::~CParticle_View_LG()
{
}

void CParticle_View_LG::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_ParticleView, m_PaticleViewFrame);
	DDX_Text(pDX, IDC_EDIT_SAVENAME, str_SaveName);
	DDX_Text(pDX, IDC_EDIT_PATH, str_SavePathedit);
}


BEGIN_MESSAGE_MAP(CParticle_View_LG, CDialog)
	ON_BN_CLICKED(IDOK, &CParticle_View_LG::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CParticle_View_LG::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BTN_YSAVE, &CParticle_View_LG::OnBnClickedBtnYsave)
//	ON_BN_CLICKED(IDC_BTN_YSAVE2, &CParticle_View_LG::OnBnClickedBtnYsave2)
//	ON_BN_CLICKED(IDC_BTN_YSAVE3, &CParticle_View_LG::OnBnClickedBtnYsave3)
ON_BN_CLICKED(IDC_BTN_YUVSAVE, &CParticle_View_LG::OnBnClickedBtnYuvsave)
ON_BN_CLICKED(IDC_BTN_BMPSAVE, &CParticle_View_LG::OnBnClickedBtnBmpsave)
ON_BN_CLICKED(IDC_BTN_CSVSAVE, &CParticle_View_LG::OnBnClickedBtnCsvsave)
ON_EN_CHANGE(IDC_EDIT_SAVENAME, &CParticle_View_LG::OnEnChangeEditSavename)
ON_BN_CLICKED(IDC_BTN_NAMESAVE, &CParticle_View_LG::OnBnClickedBtnNamesave)
END_MESSAGE_MAP()


// CParticle_View_LG 메시지 처리기입니다.
void CParticle_View_LG::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CParticle_View_LG::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}

void CParticle_View_LG::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

void CParticle_View_LG::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

BOOL CParticle_View_LG::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	//::SetForegroundWindow(this->m_hWnd);
	NameUpdate("Default");
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CParticle_View_LG::OnBnClickedBtnYsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//((CImageTesterDlg *)m_pMomWnd)->Y_SAVE("D:\YSAVE.y");
	((CImageTesterDlg *)m_pMomWnd)->Y_SAVE(str_YPath);
}


void CParticle_View_LG::OnBnClickedBtnYuvsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//((CImageTesterDlg *)m_pMomWnd)->YUV_SAVE("D:\YUVSAVE.yuv");
	((CImageTesterDlg *)m_pMomWnd)->YUV_SAVE(str_YuvPath);
}

void CParticle_View_LG::OnBnClickedBtnBmpsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	HWND SubFrameWnd = m_PaticleViewFrame.GetSafeHwnd();
	//((CImageTesterDlg *)m_pMomWnd)->SaveScan(SubFrameWnd, "D:\YUVSAVE.BMP");
	((CImageTesterDlg *)m_pMomWnd)->SaveScan(SubFrameWnd, str_BmpPath);
}

void CParticle_View_LG::OnBnClickedBtnCsvsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->SAVE_CSV(str_CsvPath);
}

void CParticle_View_LG::OnEnChangeEditSavename()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	if(str_SaveName != str_OutName){
		((CButton *)GetDlgItem(IDC_BTN_NAMESAVE))->EnableWindow(1);
	}else{
		((CButton *)GetDlgItem(IDC_BTN_NAMESAVE))->EnableWindow(0);
	}
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_View_LG::OnBnClickedBtnNamesave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	NameUpdate(str_SaveName);
}

void CParticle_View_LG::NameUpdate(LPCSTR Name)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	str_OutName = Name;
	str_SaveName = str_OutName;
	if(str_Path == ""){
		str_Path = "D:\\OUTDATA";
	}
	str_SavePathedit = str_Path;
	folder_gen(str_Path);
	str_YPath = str_Path +"\\"+ str_OutName + ".y";
	str_YuvPath = str_Path +"\\"+ str_OutName + ".yuv";
	str_BmpPath = str_Path +"\\"+ str_OutName + ".BMP";
	str_CsvPath = str_Path +"\\"+ str_OutName + ".csv";
	UpdateData(FALSE);
	((CButton *)GetDlgItem(IDC_BTN_NAMESAVE))->EnableWindow(0);
}

void CParticle_View_LG::NameUpdate(LPCSTR Name,LPCSTR Path)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	str_Path = Path;
	NameUpdate(Name);
}

BOOL CParticle_View_LG::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		
	}
	return CDialog::PreTranslateMessage(pMsg);
}
