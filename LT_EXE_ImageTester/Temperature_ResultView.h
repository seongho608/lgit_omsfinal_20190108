#pragma once
#include "resource.h"

// CTemperature_ResultView 대화 상자입니다.

class CTemperature_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CTemperature_ResultView)

public:
	CTemperature_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTemperature_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_Temperature }; 

	CFont ft1;
	COLORREF txcol_TVal, bkcol_TVal;
	void	Temperature_TEXT(LPCSTR lpcszString, ...);
	void	TemperatureNUM_TEXT(LPCSTR lpcszString, ...);
	void	RESULT_TEXT(unsigned char col, LPCSTR lpcszString, ...);
	void	Setup(CWnd* IN_pMomWnd);
	void	Initstat();
private:
	CWnd	*m_pMomWnd;
	void	Font_Size_Change(int nID, CFont *font, LONG Weight, LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnSetfocusEditTemperaturetxt();
	afx_msg void OnEnSetfocusEditTemperaturenum();
	afx_msg void OnEnSetfocusEditTemperatureresult();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
