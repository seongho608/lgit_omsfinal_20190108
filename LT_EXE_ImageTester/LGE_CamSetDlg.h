#pragma once


// CLGE_CamSetDlg 대화 상자입니다.

class CLGE_CamSetDlg : public CDialog
{
	DECLARE_DYNAMIC(CLGE_CamSetDlg)

public:
	CLGE_CamSetDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CLGE_CamSetDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_LGE_CAMSET };
	CString			CAMSETFILE;
	void	Load_parameter();
	void	Save_parameter();
	void	Setup(CWnd* IN_pMomWnd);
private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	int m_Slope;
	int m_Alpha;
	int m_IRMode;
	int m_IRGain;
	int m_Removal;
	int m_PgmGain;
	BOOL b_PgmGain;
	BOOL b_Emission;
	afx_msg void OnEnChangeEditPgmgain();
	afx_msg void OnEnChangeEditSlope();
	afx_msg void OnEnChangeEditAlpha();
	afx_msg void OnEnChangeEditIrmode();
	afx_msg void OnEnChangeEditIrgain();
	afx_msg void OnEnChangeEditRemoval();
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnBnClickedButtonLoad();
	afx_msg void OnBnClickedButtonApply();
	afx_msg void OnBnClickedCheckEmission();
	afx_msg void OnBnClickedButtonWslope();
	afx_msg void OnBnClickedButtonWalpha();
	afx_msg void OnBnClickedButtonWirmode();
	afx_msg void OnBnClickedButtonWirgain();
	afx_msg void OnBnClickedButtonWremoval();
	afx_msg void OnBnClickedCheckPgmgainuse();
	afx_msg void OnBnClickedButtonWpgmgain();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};
