#pragma once
#include "resource.h"

// CModSetDlg 대화 상자입니다.

class CModSetDlg : public CDialog
{
	DECLARE_DYNAMIC(CModSetDlg)

public:
	CModSetDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CModSetDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ETCSETUP_DIALOG };
	
	void	Setup(CWnd* IN_pMomWnd);

	CComboBox	*pModelCombo;

	CTabCtrl m_ModCtrTab;
	CStatic m_CamFrame;

	void	SubBmpPic(UINT nIDResource);
	void	TEST_Enable(BOOL MODE);
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);

	CFont ft1;
	COLORREF txcol_TVal,bkcol_TVal;
	void	STATE_VIEW(int MODE);

	void	InitEVMS();

private:
	CWnd		*m_pMomWnd;

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangeSubModel();
	afx_msg void OnTcnSelchangeModctltab(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTcnSelchangingModctltab(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonPoweron();
	afx_msg void OnBnClickedButtonPoweroff();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedBtnSaveYuv();
	afx_msg void OnEnSetfocusEditState();
	afx_msg void OnBnClickedButtonOverlayon();
	afx_msg void OnBnClickedButtonOverlayoff();
	afx_msg void OnBnClickedButtonPoweron2();
};
