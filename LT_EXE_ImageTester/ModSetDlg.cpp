// ModSetDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "ModSetDlg.h"


// CModSetDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CModSetDlg, CDialog)

CModSetDlg::CModSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModSetDlg::IDD, pParent)
{

}

CModSetDlg::~CModSetDlg()
{
}

void CModSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MODCTLTAB, m_ModCtrTab);
	DDX_Control(pDX, IDC_CAM_FRAME2, m_CamFrame);
}


BEGIN_MESSAGE_MAP(CModSetDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_SUB_MODEL, &CModSetDlg::OnCbnSelchangeSubModel)
	ON_NOTIFY(TCN_SELCHANGE, IDC_MODCTLTAB, &CModSetDlg::OnTcnSelchangeModctltab)
	ON_NOTIFY(TCN_SELCHANGING, IDC_MODCTLTAB, &CModSetDlg::OnTcnSelchangingModctltab)
	ON_BN_CLICKED(IDC_BUTTON_POWERON, &CModSetDlg::OnBnClickedButtonPoweron)
	ON_BN_CLICKED(IDC_BUTTON_POWEROFF, &CModSetDlg::OnBnClickedButtonPoweroff)
	ON_WM_CTLCOLOR()
	ON_EN_SETFOCUS(IDC_EDIT_STATE, &CModSetDlg::OnEnSetfocusEditState)
	ON_BN_CLICKED(IDC_BUTTON_OVERLAYON, &CModSetDlg::OnBnClickedButtonOverlayon)
	ON_BN_CLICKED(IDC_BUTTON_OVERLAYOFF, &CModSetDlg::OnBnClickedButtonOverlayoff)
	ON_BN_CLICKED(IDC_BUTTON_POWERON2, &CModSetDlg::OnBnClickedButtonPoweron2)
END_MESSAGE_MAP()


// CModSetDlg 메시지 처리기입니다.
void CModSetDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

BOOL CModSetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	pModelCombo = (CComboBox *)GetDlgItem(IDC_SUB_MODEL);//모델
	Font_Size_Change(IDC_EDIT_STATE,&ft1,50,24);
	STATE_VIEW(0);

	InitEVMS();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CModSetDlg::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		pModelCombo->EnableWindow(0);
	}
}


void CModSetDlg::OnCbnSelchangeSubModel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->m_pMainCombo->SetCurSel(pModelCombo->GetCurSel());
	((CImageTesterDlg *)m_pMomWnd)->OnCbnSelchangeModelName();

	((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->ShowWindow(SW_HIDE);
	((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->ShowWindow(SW_SHOW);
}

void CModSetDlg::OnTcnSelchangeModctltab(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->OnSelchangeModSubCtrtab();
	*pResult = 0;
}

void CModSetDlg::OnTcnSelchangingModctltab(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->OnSelchangingModSubCtrtab();
	*pResult = 0;
}

void CModSetDlg::SubBmpPic(UINT nIDResource)//나중에 전역변수로 간략화 할것임
{	
	CPen  my_Pan,my_Pan2;
	CFont m_font,m_font2;
	CString TEXTDATA ="";

	CRect rect;
	CDC *pcDC = m_CamFrame.GetDC();
	m_CamFrame.GetClientRect(&rect);
			
	CDC MemDC;
	CBitmap image;
	BITMAP bmpinfo;

	MemDC.CreateCompatibleDC(pcDC);
	image.LoadBitmap(nIDResource);
	image.GetBitmap(&bmpinfo);
	CBitmap*pOldBitmap = (CBitmap*)MemDC.SelectObject(&image);


	::SetBkMode(MemDC.m_hDC,TRANSPARENT);	



	pcDC->SetStretchBltMode(COLORONCOLOR);
	pcDC->StretchBlt(0, 0, rect.Width(), rect.Height(), &MemDC, 0, 0, bmpinfo.bmWidth, bmpinfo.bmHeight, SRCCOPY);
	MemDC.SelectObject(pOldBitmap);
	image.DeleteObject();
	MemDC.DeleteDC();
	ReleaseDC(pcDC);
}
void CModSetDlg::OnBnClickedButtonPoweron()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Power_On();
}

void CModSetDlg::OnBnClickedButtonPoweroff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Power_Off();

}


void CModSetDlg::TEST_Enable(BOOL MODE){
	(CTabCtrl *)GetDlgItem(IDC_MODCTLTAB)->EnableWindow(MODE);
	pModelCombo->EnableWindow(MODE);

	InitEVMS();
}

void CModSetDlg::STATE_VIEW(int MODE){
	CString str="";
	if(MODE == 0){///STAND BY
		str = "STAND BY";
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}
	if(MODE == 1){//TESTING
		str = "TESTING....";
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}
	if(MODE == 2){//TEST STATE pass
		str = "PASS";
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18, 69, 171);
	}

	if(MODE == 3){//TEST STATE fail
		str = "FAIL";
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);

	}

	((CEdit *)GetDlgItem(IDC_EDIT_STATE))->SetWindowText(str);
}
HBRUSH CModSetDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_STATE)
	{
		pDC->SetTextColor(txcol_TVal);
		pDC->SetBkColor(bkcol_TVal);
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}
void CModSetDlg::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;

	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);

	GetDlgItem(nID)->SetFont(font);
}

void CModSetDlg::OnEnSetfocusEditState()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	GotoDlgCtrl(((CButton *)GetDlgItem(IDC_BUTTON_POWERON)));

}

void CModSetDlg::OnBnClickedButtonOverlayon()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(1);
}

void CModSetDlg::OnBnClickedButtonOverlayoff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(0);
}


void CModSetDlg::OnBnClickedButtonPoweron2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Power_On(VR_PO_REGISTER_2);
}
