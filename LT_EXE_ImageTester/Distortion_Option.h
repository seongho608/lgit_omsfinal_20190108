#pragma once
#include "afxcmn.h"


// CDistortion_Option 대화 상자입니다.

class CDistortion_Option : public CDialog
{
	DECLARE_DYNAMIC(CDistortion_Option)

public:
	CDistortion_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDistortion_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_DISTORTION };

	int m_CAM_SIZE_WIDTH;
	int m_CAM_SIZE_HEIGHT;
	int m_dDistortionDistance;
	
	CRectData A_RECT[6];
	CRectData A_Original[6];
	void Load_Original_pra();
	CRectData C_CHECKING[6];
	tResultVal Run(void);
	void Pic(CDC *cdc);
	void InitPrm();
	void FAIL_UPLOAD();
	BOOL	b_StopFail;
	void TESTMOVEBTN_Enable(bool MODE);

	CvPoint GetCenterPoint(LPBYTE IN_RGB);
	CRectData SC_RECT[6];	//Side Circle
	CRectData Standard_SC_RECT[6];
	
	void GetSideCircleCoordinate(LPBYTE m_RGBIn);
	double GetDistance(int x1, int y1, int x2, int y2);

// 대화 상자 데이터입니다.
	void	SETLIST();

	void	Setup(CWnd* IN_pMomWnd);
	void    Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
	void	StatePrintf(LPCSTR lpcszString, ...);

	void	UploadList();
	void	Save_parameter();
	void	Save(int NUM);
	CString A_filename;
	int PosX,PosY;
	int InIndex;
	int iSavedItem, iSavedSubitem;
	int iSavedSubitem2;
	int state;
	int EnterState;
	int NewItem,NewSubitem;
	CRect rect;
	void	AngleSum();
	void Change_DATA();

	void Automation_DATA_INPUT();
	double disL,disR,disT,disB;
	int TestMod;
	bool AutomationMod;
	BYTE	m_InR;
	BYTE	m_InG;
	BYTE	m_InB;
	bool MasterMod;
	int	m_Comp_MAX;
	int	m_Comp_MIN;
	void Load_parameter();
	bool AngleGen(LPBYTE IN_RGB,int NUM);
	bool AnglePic(CDC *cdc,int NUM);
	CBitmap bmp;
	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void InsertList();
	int StartCnt;
	int InsertIndex;
	int ListItemNum;
	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();

	//--------
	void List_COLOR_Change();
	bool ChangeCheck;
	int ChangeItem[5];
	int changecount;

	bool Change_DATA_CHECK(bool FLAG);
	bool b_EtcModel_TEST;

	
	void	EditWheelIntSet(int nID,int Val);
	void	EditMinMaxIntSet(int nID,int* Val,int Min,int Max);
	void    EditMinMaxdoubleSet(int nID,double* Val,double Min,double Max);
	bool WheelCheck;

	// MES 정보저장
	CString m_szMesResult;
	CString Mes_Result();
	void	CopyMesData();

	double d_Focallength;
	double d_Campixel;
	int i_ChartDis;
	double Degree_Sum(int Dis);
	double Distortion_Sum(int PixDis);

	int i_MotorDis;
	double d_Distortion_Master;
	double d_Distortion;
	double d_Dev_Distortion;
	double m_dOffset;

	CString m_strDistortionScale;
	double m_dDistortionScale;

	void	InitEVMS();
#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	int Lot_StartCnt;
	int Lot_InsertNum;
	int Lot_InsertIndex;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

#pragma endregion

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	CString		str_model;
//	int m_Number;
	tINFO	m_INFO;

	tResultVal m_retval;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListCtrl A_DATALIST;
	int A_Total_PosX;
	int A_Total_PosY;
	int A_Dis_Width;
	int A_Dis_Height;
	int A_Total_Width;
	int A_Total_Height;
	double A_Thresold;
	afx_msg void OnBnClickedButtonsetanglezone();
	afx_msg void OnBnClickedButtonARectSave();
	afx_msg void OnNMClickListAngle(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
//	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonMasterA();
	CListCtrl m_AngleList;

	afx_msg void OnBnClickedCheckAuto();
	afx_msg void OnNMCustomdrawListAngle(NMHDR *pNMHDR, LRESULT *pResult);
//	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnNMDblclkListAngle(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusEditAMod();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedButtonALoad();

	//afx_msg void OnBnClickedCheckAngleKeeprun();
	BOOL AngleKeepRun;
	CButton btn_AngleKeepRun;
	afx_msg void OnDestroy();
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnEnChangeAnglePosx();
	afx_msg void OnEnChangeAnglePosy();
	afx_msg void OnEnChangeAngleDisW();
	afx_msg void OnEnChangeAngleDisH();
	afx_msg void OnEnChangeAngleWidth();
	afx_msg void OnEnChangeAngleHeight();
	afx_msg void OnEnChangeAngleThresold();
	afx_msg void OnEnChangeEditAMod();
	afx_msg void OnLvnItemchangedListAngle(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtontest();
	CString str_focallength;
	CString str_camPixel;
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnEnChangeEditCamFc();
	afx_msg void OnEnChangeEditCamPixel();
	CString str_ChartDistance;
	afx_msg void OnEnChangeEditChartDis();
	CString str_Distortion;
	CString str_DEV_Distortion;
	afx_msg void OnEnChangeEditDistortion();
	afx_msg void OnBnClickedButtonSaveDistortion();
	afx_msg void OnEnChangeEditDevDistortion();
	CString str_MotorDis;
	afx_msg void OnEnChangeEditCamDistance();
	CListCtrl m_Lot_DistortionList;
	CString str_DistortionOffset;
	afx_msg void OnEnChangeEditDistortionOffset();
	afx_msg void OnEnChangeEditDistortDist();
	afx_msg void OnBnClickedBtnDistortionMoveSave();
	afx_msg void OnBnClickedBtnChartTestmove2();
};

void CModel_Create(CAngle_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO);
void CModel_Delete(CAngle_Option **pWnd);