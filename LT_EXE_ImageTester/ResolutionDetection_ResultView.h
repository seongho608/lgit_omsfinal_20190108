#pragma once
#include "afxcmn.h"


// CResolutionDetection_ResultView 대화 상자입니다.

class CResolutionDetection_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CResolutionDetection_ResultView)

public:
	CResolutionDetection_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CResolutionDetection_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_RESOLUTION };
	void	Initstat();
	void	Setup(CWnd* IN_pMomWnd);
private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_CenterList;
	CListCtrl m_SideList;
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
