#pragma once
#include "afxcmn.h"


// CVerify_ResultView 대화 상자입니다.

class CVerify_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CVerify_ResultView)

public:
	CVerify_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CVerify_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_VERIFY };
	void	Setup(CWnd* IN_pMomWnd);

	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	CFont	ft,ft2;

	COLORREF tx_st_col,bk_st_col;
	void	VER_STATE(int col,LPCSTR lpcszString, ...);
	void	InitStat();

private :
	CWnd	*m_pMomWnd;	

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL OnInitDialog();
	CProgressCtrl m_ProgressTestRes;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
