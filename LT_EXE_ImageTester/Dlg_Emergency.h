#pragma once
#include "resource.h"

// CDlg_Emergency 대화 상자입니다.

class CDlg_Emergency : public CDialog
{
	DECLARE_DYNAMIC(CDlg_Emergency)

public:
	CDlg_Emergency(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_Emergency();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_EMERGENCY };

	CFont m_font;
	CFont m_font1;
	CFont m_font2;
	CFont m_font3;

	CEdit m_edText;
	CEdit m_edText1;
	CEdit m_edBk1;
	CEdit m_edBk2;
	CButton m_btOK;

	int m_iflash;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);

protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedBtnOK();
};
