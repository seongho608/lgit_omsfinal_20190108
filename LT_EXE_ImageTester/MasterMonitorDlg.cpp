// MasterMonitorDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "MasterMonitorDlg.h"

// MasterMonitorDlg.cpp : 구현 파일입니다.

// CMasterMonitorDlg 대화 상자입니다.
extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4]; 

IMPLEMENT_DYNAMIC(CMasterMonitorDlg, CDialog)

CMasterMonitorDlg::CMasterMonitorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMasterMonitorDlg::IDD, pParent)
	, str_RotateDegree(_T(""))
	, str_OffsetX(_T(""))
	, str_OffsetY(_T(""))
	, str_Minoffset(_T(""))
	, str_Maxoffset(_T(""))
	, str_MinRotate(_T(""))
	, str_MaxRotate(_T(""))
	, str_RotateOffset(_T(""))
	, str_msettX(_T(""))
	, str_msettY(_T(""))
{
	m_MonChannel = 0;
	for(int t=0; t<4; t++){
		ClickNUM[t]=0;
	}
	CenterData.x = 0;
	CenterData.y = 0;
	Change_CenterData.x = 0;
	Change_CenterData.y = 0;

	m_OffsetX=0;
	m_OffsetY=0;

	b_TESTMOD =0;

}

CMasterMonitorDlg::~CMasterMonitorDlg()
{
}

void CMasterMonitorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CAM_FRAME, m_stCenterDisplay);
	//	DDX_Control(pDX, IDC_COMBO1, m_directMODE);
	DDX_Text(pDX, IDC_EDIT_ROTATE, str_RotateDegree);
	DDX_Text(pDX, IDC_EDIT_MOFFSETX, str_OffsetX);
	DDX_Text(pDX, IDC_EDIT_MOFFSETY, str_OffsetY);
	DDX_Text(pDX, IDC_EDIT_Minoffset, str_Minoffset);
	DDX_Text(pDX, IDC_EDIT_Maxoffset, str_Maxoffset);
	DDX_Text(pDX, IDC_EDIT_Minoffset2, str_MinRotate);
	DDX_Text(pDX, IDC_EDIT_Maxoffset2, str_MaxRotate);
	DDX_Text(pDX, IDC_EDIT_Degree, str_RotateOffset);
	DDX_Text(pDX, IDC_EDIT_MSETTX, str_msettX);
	DDX_Text(pDX, IDC_EDIT_MSETTY, str_msettY);
}


BEGIN_MESSAGE_MAP(CMasterMonitorDlg, CDialog)
//	ON_CBN_SELCHANGE(IDC_COMBO_SelChNum, &CMasterMonitorDlg::OnCbnSelchangeComboSelchnum)
	ON_BN_CLICKED(IDC_BTN_CHARTSTAND, &CMasterMonitorDlg::OnBnClickedBtnChartstand)
	ON_BN_CLICKED(IDC_BTN_CHARTSET, &CMasterMonitorDlg::OnBnClickedBtnChartset)
	ON_BN_CLICKED(IDC_BTN_AUTOSET, &CMasterMonitorDlg::OnBnClickedBtnAutoset)
	ON_EN_CHANGE(IDC_EDIT_MOFFSETX, &CMasterMonitorDlg::OnEnChangeEditMoffsetx)
	ON_EN_CHANGE(IDC_EDIT_MOFFSETY, &CMasterMonitorDlg::OnEnChangeEditMoffsety)
	ON_BN_CLICKED(IDC_BTN_POWER_ON, &CMasterMonitorDlg::OnBnClickedBtnPowerOn)
	ON_BN_CLICKED(IDC_BTN_POWER_ON2, &CMasterMonitorDlg::OnBnClickedBtnPowerOn2)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BTN_MASTER_MONITOR_MOVE, &CMasterMonitorDlg::OnBnClickedBtnMasterMonitorMove)
	ON_BN_CLICKED(IDC_BUTTON_USER_EXIT, &CMasterMonitorDlg::OnBnClickedButtonUserExit)
	ON_BN_CLICKED(IDC_BTN_SET_CH1, &CMasterMonitorDlg::OnBnClickedBtnSetCh1)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_JIGDOWN, &CMasterMonitorDlg::OnBnClickedBtnJigdown)
	ON_EN_SETFOCUS(IDC_STATE_CH1, &CMasterMonitorDlg::OnEnSetfocusStateCh1)
	
	ON_BN_CLICKED(IDC_BTN_AUTOSET2, &CMasterMonitorDlg::OnBnClickedBtnAutoset2)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CMasterMonitorDlg::OnBnClickedButtonSave)
	ON_EN_CHANGE(IDC_EDIT_Minoffset, &CMasterMonitorDlg::OnEnChangeEditMinoffset)
	ON_EN_CHANGE(IDC_EDIT_Maxoffset, &CMasterMonitorDlg::OnEnChangeEditMaxoffset)
	ON_BN_CLICKED(IDC_BUTTON_SAVE2, &CMasterMonitorDlg::OnBnClickedButtonSave2)
	ON_EN_CHANGE(IDC_EDIT_Minoffset2, &CMasterMonitorDlg::OnEnChangeEditMinoffset2)
	ON_EN_CHANGE(IDC_EDIT_Maxoffset2, &CMasterMonitorDlg::OnEnChangeEditMaxoffset2)
	ON_EN_CHANGE(IDC_EDIT_Degree, &CMasterMonitorDlg::OnEnChangeEditDegree)
	ON_EN_SETFOCUS(IDC_EDIT_WRITEMODE, &CMasterMonitorDlg::OnEnSetfocusEditWritemode)
	ON_EN_SETFOCUS(IDC_EDIT_MAXISX, &CMasterMonitorDlg::OnEnSetfocusEditMaxisx)
	ON_EN_SETFOCUS(IDC_EDIT_MAXISY, &CMasterMonitorDlg::OnEnSetfocusEditMaxisy)
	ON_EN_SETFOCUS(IDC_STATE_CH2, &CMasterMonitorDlg::OnEnSetfocusStateCh2)
	ON_BN_CLICKED(IDC_BTN_OVERLAYON, &CMasterMonitorDlg::OnBnClickedBtnOverlayon)
	ON_BN_CLICKED(IDC_BTN_OVERLAYOFF, &CMasterMonitorDlg::OnBnClickedBtnOverlayoff)
	ON_EN_CHANGE(IDC_EDIT_MSETTX, &CMasterMonitorDlg::OnEnChangeEditMsettx)
	ON_EN_CHANGE(IDC_EDIT_MSETTY, &CMasterMonitorDlg::OnEnChangeEditMsetty)
END_MESSAGE_MAP()


// CMasterMonitorDlg 메시지 처리기입니다.

void CMasterMonitorDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CMasterMonitorDlg::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT)
{
	pTStat		= pTEDIT;
	Setup(IN_pMomWnd);
}

BOOL CMasterMonitorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
//	pChannelCombo = (CComboBox *)GetDlgItem(IDC_COMBO_SelChNum);
	pDirectModeCombo = (CComboBox *)GetDlgItem(IDC_OPT_WRMODE);
	
	
	Font_Size_Change(IDC_STATE_CH1,&font1,500,23);
	GetDlgItem(IDC_STATE_CH2)->SetFont(&font1);
	GetDlgItem(IDC_STATE_CH3)->SetFont(&font1);

	Font_Size_Change(IDC_EDIT_WRITEMODE,&font2,500,23);
	//GetDlgItem(IDC_EDIT_MAXISY)->SetFont(&font2);
	//GetDlgItem(IDC_EDIT_MAXISX)->SetFont(&font2);

	
	
	
	m_MonChannel = 0;
//	pChannelCombo->SetCurSel(m_MonChannel);
	pDirectModeCombo->SetCurSel(0);
	
	InitSet();

	if(((CImageTesterDlg *)m_pMomWnd)->m_newmodelname != ((CImageTesterDlg *)m_pMomWnd)->m_oldmodelname){
		((CImageTesterDlg *)m_pMomWnd)->m_oldmodelname = ((CImageTesterDlg *)m_pMomWnd)->m_newmodelname;
		AllMSetInit();
	}
	MoveDistance_Setup();
	UpdateMaState();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CMasterMonitorDlg::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;
	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);
	LogFont.lfWeight =Weight ;
	LogFont.lfHeight = Height;
	font->CreateFontIndirect(&LogFont);
	GetDlgItem(nID)->SetFont(font);
}


void CMasterMonitorDlg::MoveDistance_Setup()//나중에 전역변수로 간략화 할것임
{	
	//SetDlgItemInt(IDC_EDIT_MASTER_MONITOR_CHART_DISTANCE, ((CImageTesterDlg *)m_pMomWnd)->m_pSubStandOptWnd->m_dChartDistance);
}

void CMasterMonitorDlg::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

void CMasterMonitorDlg::OnCbnSelchangeComboSelchnum()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
}

void CMasterMonitorDlg::InitSet()
{
	CenterData.x =0;
	CenterData.y =0;
	Change_CenterData.x =0;
	Change_CenterData.y =0;
	MasterOffsetX = 0;
	MasterStandX = 320;
	Master_OffsetX_Txt("0");
	Master_SettX_Txt("320");
	Master_AxisX_Txt("320");
	MasterOffsetY = 0;
	MasterStandY = 240;
	Master_OffsetY_Txt("0");
	Master_SettY_Txt("240");
	Master_AxisY_Txt("240");
	UpdateChartVal();
}

void CMasterMonitorDlg::UpdateCenterVal(CvPoint Data)
{
	CString str ="";
	str.Format("%d",Data.x);
	Master_AxisX_Txt(str);
	str.Format("%d",Data.y);
	Master_AxisY_Txt(str);
}



void CMasterMonitorDlg::UpdateChartVal()
{
	/*if(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd == NULL){
		return;
	}
	CString str;
	int x,y;
	x = ((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->m_ptMasterCoord[m_MonChannel].x;
	y = ((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->m_ptMasterCoord[m_MonChannel].y;
	str.Format("%d",x);
	ChartX_Txt(str);
	ChartValX_Txt(str);
	str.Format("%d",y);
	ChartY_Txt(str);
	ChartValY_Txt(str);*/
}

void CMasterMonitorDlg::ChartX_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_Chart_X))->SetWindowText(lpcszString);
}
void CMasterMonitorDlg::ChartY_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_Chart_Y))->SetWindowText(lpcszString);
}
void CMasterMonitorDlg::ChartValX_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_Chart_VALX))->SetWindowText(lpcszString);
}
void CMasterMonitorDlg::ChartValY_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_Chart_VALY))->SetWindowText(lpcszString);
}
void CMasterMonitorDlg::Master_OffsetX_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_MOFFSETX))->SetWindowText(lpcszString);
}
void CMasterMonitorDlg::Master_OffsetY_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_MOFFSETY))->SetWindowText(lpcszString);
}

void CMasterMonitorDlg::Master_SettX_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_MSETTX))->SetWindowText(lpcszString);
}
void CMasterMonitorDlg::Master_SettY_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_MSETTY))->SetWindowText(lpcszString);
}

void CMasterMonitorDlg::Master_AxisX_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_MAXISX))->SetWindowText(lpcszString);
}
void CMasterMonitorDlg::Master_AxisY_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_MAXISY))->SetWindowText(lpcszString);
}

void CMasterMonitorDlg::Master_Writemod_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_WRITEMODE))->SetWindowText(lpcszString);
}

void CMasterMonitorDlg::OnBnClickedBtnChartstand()
{
	
}

void CMasterMonitorDlg::OnBnClickedBtnChartset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
}

void CMasterMonitorDlg::Chartset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	/*if(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd == NULL){
		return;
	}*/

	
}

void CMasterMonitorDlg::OnEnChangeEditMoffsetx()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);


	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMasterMonitorDlg::OnEnChangeEditMoffsety()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMasterMonitorDlg::OnBnClickedBtnAutoset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	RUN_MODE_CHK(1);

	CenterData.x = 0;
	CenterData.y = 0;

	
	
	if(!((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE()){
		
	}


	MasterState_Val(1,2,"Processing");

	((CImageTesterDlg *)m_pMomWnd)->Power_On();//Power_On(); //전원을 넣는다.
	//sleep(1500);
	AutosetRunning(1);//오토셋 진행
	if(ClickNUM[0] == 1){
		DoEvents(1000);
		((CImageTesterDlg *)m_pMomWnd)->Power_Off();
	}
	RUN_MODE_CHK(0);
}


void CMasterMonitorDlg::AutosetRunning(bool MOD)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	/*if(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd == NULL){
		return;
	}*/


	m_OffsetX = -atoi(str_OffsetX);
	m_OffsetY = -atoi(str_OffsetY);

	UpdateData(FALSE);
	memset(m_RGBScanbuf,0,sizeof(m_RGBScanbuf));
	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = TRUE;
	
	for(int i =0;i<10;i++){
		if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == FALSE){
			break;
		}else{
			DoEvents(50);
		}
	}
	EtcPt.x = -1;
	EtcPt.y = -1;
	EtcPt = GetCenterPoint(m_RGBScanbuf);
	CenterData = EtcPt;

	CvPoint Result  = centergen(EtcPt.x,EtcPt.y);
	
	Change_CenterData.x = Result.x - m_OffsetX;

	Change_CenterData.y = Result.y - m_OffsetY;


	if(MOD == 1){///개별 일 경우만

		bool FLAG = TRUE;
		if(abs(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.StandX - Change_CenterData.x) > m_Minoffset){
			 FLAG = FALSE;
			
		}

		if(abs(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.StandY - Change_CenterData.y) > m_Maxoffset){
			 FLAG = FALSE;

		}

		if(!FLAG){
			AfxMessageBox("광축 offset 한계 범위에서 벗어났습니다. \r\n 다시 설정해주세요");

			ShowWindow(FALSE);
			ShowWindow(TRUE);
			RUN_MODE_CHK(0);
			ClickNUM[0] = 0;
			return;
		}
	

		CString str="";
		str.Format("X : %d  Y : %d \r\n 적용하시겠습니까? ",Change_CenterData.x,Change_CenterData.y);
		
		CPopUP3 subdlg;
		subdlg.Setup(this);
		subdlg.Warning_TEXT =str+"\r\n\r\n";
		
		if(subdlg.DoModal() == IDOK){
			UpdateCenterVal(Change_CenterData);

			((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.StandX = Change_CenterData.x;
			((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.StandY = Change_CenterData.y;
			((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->Save_parameter();
			
			ClickNUM[0] = 1;
			
		}else{
			
			ClickNUM[0] = 0;
				
		}

			ShowWindow(FALSE);
			ShowWindow(TRUE);

			MasterState_Val(1,1,"Success");
	}
	


}


CvPoint CMasterMonitorDlg::GetCenterPoint(LPBYTE IN_RGB)
{
	double Cam_PosX = (CAM_IMAGE_WIDTH / 2);
	double Cam_PosY = (CAM_IMAGE_HEIGHT / 2);
	CvPoint resultPt = cvPoint(-99999, -99999);

	IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *PreprecessedImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);

	IplImage *RGBOrgImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);

	BYTE R, G, B;

	for (int y = 0; y < CAM_IMAGE_HEIGHT; y++)
	{
		for (int x = 0; x < CAM_IMAGE_WIDTH; x++)
		{
			/*	B = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 2];

			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));

			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;		*/

			B = IN_RGB[y*(CAM_IMAGE_WIDTH * 3) + x * 3];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH * 3) + x * 3 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH * 3) + x * 3 + 2];

			if (R < 50 && G < 50 && B < 50)
				OriginImage->imageData[y*OriginImage->widthStep + x] = 255;
			else
				OriginImage->imageData[y*OriginImage->widthStep + x] = 0;

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}

	//	cvSaveImage("C:\\CenterImage.bmp", OriginImage);
	//	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	//	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);

	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);

	cvCopyImage(OriginImage, SmoothImage);

	cvCanny(SmoothImage, CannyImage, 0, 255);

	//	cvDilate(CannyImage, DilateImage);

	cvCopyImage(CannyImage, temp_PatternImage);

	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);

	//	cvSaveImage("C:\\CenterImage.bmp", CannyImage);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;

	int counter = 0;

	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
		counter++;

	if (counter == 0){
		cvReleaseImage(&OriginImage);
		cvReleaseImage(&CannyImage);
		cvReleaseImage(&PreprecessedImage);

		cvReleaseImage(&DilateImage);
		cvReleaseImage(&SmoothImage);
		cvReleaseImage(&RGBResultImage);
		cvReleaseImage(&temp_PatternImage);
		cvReleaseImage(&RGBOrgImage);

		return resultPt;
	}

	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area = 0, arcCount = 0;
	counter = 0;
	double old_dist = 999999;

	int old_center_pos_x = 0;
	int old_center_pos_y = 0;
	int center_pt_x = 0;
	int center_pt_y = 0;
	int obj_Cnt = 0;
	int size = 0, old_size = 0;

	for (; contour != 0; contour = contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;

		double circularity = (4.0*3.14*area) / (arcCount*arcCount);

		if (circularity > 0.8)
		{

			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width / 2;
			center_y = rect.y + rect.height / 2;

			//if(center_x > (int)(CAM_IMAGE_WIDTH*0.75) && center_x < (int)(CAM_IMAGE_WIDTH*1.25) && center_y > (int)(CAM_IMAGE_HEIGHT*0.625) && center_y < (int)(CAM_IMAGE_HEIGHT*1.375))
			if (center_x > (int)(Cam_PosX*0.60) && center_x < (int)(Cam_PosX*1.40) && center_y >(int)(Cam_PosY*0.600) && center_y < (int)(Cam_PosY*1.400))

			{

				if (rect.width < Cam_PosX && rect.height < Cam_PosY && rect.width > 20 && rect.height > 20)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);

					/*center_pt_x = center_pt_x+ center_x;
					center_pt_y = center_pt_y+ center_y;
					obj_Cnt++;

					old_center_pos_x = center_pt_x/obj_Cnt;
					old_center_pos_y = center_pt_y/obj_Cnt;*/

					double distance = GetDistance(rect.x + rect.width / 2, rect.y + rect.height / 2, Cam_PosX, Cam_PosY);

					obj_Cnt++;

					size = rect.width * rect.height;

					if (distance < old_dist)
					{
						old_size = size;
						resultPt.x = center_x;
						resultPt.y = center_y;
						old_dist = distance;
					}

					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(255, 0, 0), 1, 8);

				}
			}
		}

		counter++;
	}
	//	cvSaveImage("D:\\RGBResultImage.bmp", RGBResultImage);
	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&CannyImage);
	cvReleaseImage(&PreprecessedImage);

	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
	cvReleaseImage(&RGBOrgImage);

	//delete contour;
//	delete temp_contour;
	delete[]rectArray;
	delete[]areaArray;

	/*if(obj_Cnt != 0)
	{
	resultPt.x = center_pt_x / obj_Cnt;
	resultPt.y = center_pt_y / obj_Cnt;
	}*/
	/*	if(obj_Cnt != 0)
	{
	resultPt.x = center_pt_x;
	resultPt.y = center_pt_y;
	}*/
	return resultPt;
}
double CMasterMonitorDlg::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt( (double)((x2-x1)*(x2-x1)) +  ((y2-y1)*(y2-y1)));

	return result;
}

CvPoint CMasterMonitorDlg::centergen(double centx,double centy)
{
//	int mbuf;

	CvPoint Result;
	double TmpX, TmpY;
	double bufx, bufy;
	char m_Offsetx_buf;
	char m_Offsety_buf;
	CString str="";
	char m_F_Offsetx_buf;
	char m_F_Offsety_buf;
	
	long bufcenterx = (long)((centx+0.005) *100);
	long bufcentery = (long)((centy+0.005) *100);

	double d_F_X_ASIX = CAM_IMAGE_WIDTH - (double)bufcenterx / 100;
	double d_F_Y_ASIX = (double)bufcentery/100;
	
	if(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.WriteMode == 0){ //좌우반전이므로 계측된 x축은 반대다.
		bufx = d_F_X_ASIX;//계측 광축 X 360근처
		bufy = d_F_Y_ASIX;//계측 광축 Y 240근처
// 		bufx = CAM_IMAGE_WIDTH - d_F_X_ASIX;//계측 광축 X 360근처
// 		bufy = d_F_Y_ASIX;//계측 광축 Y 240근처
	}else if(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.WriteMode == 1){//상하반전이므로 계측된 y축은 반대다. 
		bufx = CAM_IMAGE_WIDTH - d_F_X_ASIX;
		bufy = CAM_IMAGE_HEIGHT - d_F_Y_ASIX;//계측 광축 Y 240근처
// 		bufx = d_F_X_ASIX;
// 		bufy = CAM_IMAGE_HEIGHT - d_F_Y_ASIX;//계측 광축 Y 240근처
	}else if(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.WriteMode == 2){//무반전이므로 계측된 x와 y를 그대로 적용한다. 
		bufx = CAM_IMAGE_WIDTH - d_F_X_ASIX;
		bufy = d_F_Y_ASIX;
		// 		bufx = d_F_X_ASIX;
// 		bufy = d_F_Y_ASIX;
	}else if(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.WriteMode == 3){//로테이트이므로 x와 y 둘다 반대로 적용된다. 
		bufx = d_F_X_ASIX;
		bufy = CAM_IMAGE_HEIGHT - d_F_Y_ASIX;
		// 		bufx = CAM_IMAGE_WIDTH - d_F_X_ASIX;
// 		bufy = CAM_IMAGE_HEIGHT - d_F_Y_ASIX;
	}else{
		bufx = CAM_IMAGE_WIDTH - d_F_X_ASIX;//이 부분에 모드에 따라 표현되는 방식이 틀려진다.
		bufy = d_F_Y_ASIX;
// 		bufx = CAM_IMAGE_WIDTH - d_F_X_ASIX;//이 부분에 모드에 따라 표현되는 방식이 틀려진다.
// 		bufy = d_F_Y_ASIX;
	}

	Result.x = bufx;
	Result.y = bufy;
	return Result;
	
	//if(EtcDATA->m_Init_X == 99999 && EtcDATA->m_Init_Y == 99999)
	//{
	//	EtcDATA->m_Init_X = bufx;
	//	EtcDATA->m_Init_Y= bufy;
	//}

	//EtcDATA->m_X_ASIX = bufx;
	//EtcDATA->m_Y_ASIX = bufy;

	//TmpX = (bufx - CenterPrm.StandX); //물리적으로 광축이 벗어난 정도 
	//if((TmpX) > 0.0)	m_Offsetx_buf = (char)((TmpX)+0.5) ;//+ 0x10  ; //+ 0x1E/2 = 15[dec]
	//else m_Offsetx_buf = (char)((TmpX)-0.5) ;//+ 0x10  ; //+ 0x1E/2 = 15[dec]
	//
	//TmpY = (bufy - CenterPrm.StandY);
	//if((TmpY) > 0.0) m_Offsety_buf = (char)((TmpY)+0.5); //+ 0x16/2 = 11[dec]
	//else  m_Offsety_buf = (char)((TmpY) - 0.5); //+ 0x16/2 = 11[dec]

	//char m_Offsetx_buf2 = (unsigned char)(m_Offsetx_buf * CenterPrm.RateX);
	//char m_Offsety_buf2 = (unsigned char)(m_Offsety_buf * CenterPrm.RateY);
	//
	//EtcDATA->m_X_OFFSET = m_Offsetx_buf;
	//EtcDATA->m_Y_OFFSET = m_Offsety_buf;
	//
	//if(EtcDATA->m_Init_Offset_X == 99999 && EtcDATA->m_Init_Offset_Y == 99999)
	//{
	//	EtcDATA->m_Init_Offset_X = m_Offsetx_buf;
	//	EtcDATA->m_Init_Offset_Y = m_Offsety_buf;
	//}


	//if(((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd != NULL){
	//	str.Format("%d",m_Offsetx_buf);
	//	((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->OffsetX_Txt(EtcDATA->m_number,str);
	//	str.Format("%d",m_Offsety_buf);
	//	((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->OffsetY_Txt(EtcDATA->m_number,str);
	//	str.Format("%5.2f",EtcDATA->m_X_ASIX);
	//	((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->AxisX_Txt(EtcDATA->m_number,str);
	//	str.Format("%5.2f",EtcDATA->m_Y_ASIX);
	//	((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->AxisY_Txt(EtcDATA->m_number,str);
	//}
	//
	////중심점 검출후 합격 범위안에 들어오면 성공표시를 하고 테스트를 스톱한다.
	//if(EtcDATA->m_X_ASIX <= (CenterPrm.StandX + CenterPrm.PassDeviatX)&&
	//   EtcDATA->m_X_ASIX >= (CenterPrm.StandX - CenterPrm.PassDeviatX)&&
	//   EtcDATA->m_Y_ASIX >= (CenterPrm.StandY - CenterPrm.PassDeviatY)&&
	//   EtcDATA->m_Y_ASIX <= (CenterPrm.StandY + CenterPrm.PassDeviatY)){
	//   EtcDATA->m_success = TRUE;
	//	if((EtcDATA->m_count >= CenterPrm.MaxCount)||
	//		(EtcDATA->m_X_ASIX <= (CenterPrm.StandX + CenterPrm.PassMaxDevX)&&
	//		EtcDATA->m_X_ASIX >= (CenterPrm.StandX - CenterPrm.PassMaxDevX)&&
	//		EtcDATA->m_Y_ASIX >= (CenterPrm.StandY - CenterPrm.PassMaxDevY)&&
	//		EtcDATA->m_Y_ASIX <= (CenterPrm.StandY + CenterPrm.PassMaxDevY)))
	//	{
	//		EtcDATA->m_Enable = FALSE;
	//	}else{
	//		EtcDATA->m_count++;
	//		EtcDATA->m_oldoffsetX = EtcDATA->m_offsetX;
	//		EtcDATA->m_oldoffsetY = EtcDATA->m_offsetY;
	//		//EtcDATA->m_offsetX += (-1)*(m_Offsetx_buf2);//펌웨어 다운로드의 경우 검출값을 그대로 쓴다.
	//		//EtcDATA->m_offsetY += (-1)*(m_Offsety_buf2);
	//		EtcDATA->m_offsetX = (-1)*(m_Offsetx_buf2);//펌웨어 다운로드의 경우 검출값을 그대로 쓴다.
	//		EtcDATA->m_offsetY = (-1)*(m_Offsety_buf2);
	//		EtcDATA->m_Enable = TRUE;
	//	}
	//}else{//실패할경우 write에 적용할 수치를 적용하고 끝낸다. 
	//	if(EtcDATA->m_count >= CenterPrm.MaxCount){
	//		EtcDATA->m_success = FALSE;
	//		EtcDATA->m_Enable = FALSE;
	//	}else{
	//		EtcDATA->m_count++;
	//		EtcDATA->m_oldoffsetX = EtcDATA->m_offsetX;
	//		EtcDATA->m_oldoffsetY = EtcDATA->m_offsetY;
	//		//EtcDATA->m_offsetX += (-1)*(m_Offsetx_buf2);
	//		//EtcDATA->m_offsetY += (-1)*(m_Offsety_buf2);
	//		EtcDATA->m_offsetX += (-1)*(m_Offsetx_buf2);//펌웨어 다운로드의 경우 검출값을 그대로 쓴다.
	//		EtcDATA->m_offsetY += (-1)*(m_Offsety_buf2);
	//	}
	//}
}

void CMasterMonitorDlg::OnCamDisplay(CDC *cdc)
{
	
	CPen  my_Pan,my_Pan2,*old_pan,*old_pan2;
	CFont m_font,m_font2,*old_font,*old_font2;
	CString RESULTDATA ="";
	CString TEXTDATA ="";

	if(ClickNUM[m_MonChannel] == 1){
		m_font2.CreatePointFont(700,"Arial");
		old_font2 = cdc->SelectObject(&m_font2);
		cdc->SetTextAlign(TA_CENTER|TA_BASELINE);
		DrawOutlineText(cdc,CAM_IMAGE_WIDTH/2,CAM_IMAGE_HEIGHT/2,"SUCCESS",BLUE_COLOR,WHITE_COLOR,2);

		cdc->SelectObject(old_font2);
		old_font2->DeleteObject();
		m_font2.DeleteObject();
	}


	::SetBkMode(cdc->m_hDC,TRANSPARENT);	
	my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
	old_pan = cdc->SelectObject(&my_Pan);
	cdc->SetTextColor(BLUE_COLOR);

	cdc->SelectObject(old_pan);
	old_pan->DeleteObject();
	my_Pan.DeleteObject();
	
	m_font.CreatePointFont(200,"Arial");  
	old_font = cdc->SelectObject(&m_font);
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);
//	TEXTDATA.Format("CHANNEL %d",m_MonChannel+1);
//	cdc->TextOut(620,40,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	cdc->SelectObject(old_font);
	old_font->DeleteObject();
	m_font.DeleteObject();
	if((CenterData.x != 0)&&(CenterData.y != 0)){
		TEXTDATA.Format("X:%d Y:%d",Change_CenterData.x,Change_CenterData.y);
		cdc->TextOut(CAM_IMAGE_WIDTH/2,CAM_IMAGE_HEIGHT-100,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
		my_Pan2.CreatePen(PS_SOLID,2,WHITE_COLOR);
		old_pan2 = cdc->SelectObject(&my_Pan2);
		cdc->SetTextColor(WHITE_COLOR);
		cdc->MoveTo(CenterData.x-10,CenterData.y);
		cdc->LineTo(CenterData.x+10,CenterData.y);
		cdc->MoveTo(CenterData.x,CenterData.y-10);
		cdc->LineTo(CenterData.x,CenterData.y+10);

		
		cdc->SelectObject(old_pan2);
		old_pan2->DeleteObject();
		my_Pan2.DeleteObject();
	}

	if(b_Rotate == 1){
		Pic(cdc);
	}
	
	CDC *pcDC;
	pcDC = m_stCenterDisplay.GetDC();

	::SetStretchBltMode(pcDC->m_hDC, COLORONCOLOR);
	::StretchBlt(pcDC->m_hDC,
				0,  0,
				CAM_IMAGE_WIDTH,
				CAM_IMAGE_HEIGHT,
				cdc->m_hDC,
				0, 0,
				CAM_IMAGE_WIDTH,
				CAM_IMAGE_HEIGHT,SRCCOPY);
	m_stCenterDisplay.ReleaseDC(pcDC);		
}

void CMasterMonitorDlg::SubBmpPic(UINT nIDResource)//나중에 전역변수로 간략화 할것임
{	
	CPen  my_Pan,my_Pan2,*old_pan,*old_pan2;
	CFont m_font,m_font2;
	CString TEXTDATA ="";

	CRect rect;
	CDC *pcDC = m_stCenterDisplay.GetDC();
	m_stCenterDisplay.GetClientRect(&rect);
			
	CDC MemDC;
	CBitmap image;
	BITMAP bmpinfo;

	MemDC.CreateCompatibleDC(pcDC);
	image.LoadBitmap(nIDResource);
	image.GetBitmap(&bmpinfo);
	CBitmap*pOldBitmap = (CBitmap*)MemDC.SelectObject(&image);


	::SetBkMode(MemDC.m_hDC,TRANSPARENT);	


	/*int x1 = CenterData.x;
	int y1 = CenterData.y;


	if(x1 != 0 &&y1 != 0 ){

		my_Pan.CreatePen(PS_SOLID,5,RGB(255, 255, 0));
		old_pan = MemDC.SelectObject(&my_Pan);
		MemDC.MoveTo(x1-15, y1);
		MemDC.LineTo(x1+15, y1);
		MemDC.MoveTo(x1, y1-15);
		MemDC.LineTo(x1, y1+15);
		my_Pan.DeleteObject();
	}*/


	my_Pan.CreatePen(PS_SOLID,2,BLACK_COLOR);
	old_pan = MemDC.SelectObject(&my_Pan);
	MemDC.SetTextColor(BLACK_COLOR);
	
	if(ClickNUM[m_MonChannel] == 1){
		m_font2.CreatePointFont(700,"Arial");
		MemDC.SelectObject(&m_font2);
		MemDC.SetTextAlign(TA_CENTER|TA_BASELINE);
		DrawOutlineText(&MemDC,bmpinfo.bmWidth/2,   bmpinfo.bmHeight/2,"SUCCESS",BLUE_COLOR,WHITE_COLOR,2);
	}

	m_font.CreatePointFont(200,"Arial");  
	MemDC.SelectObject(&m_font);
	MemDC.SetTextAlign(TA_CENTER|TA_BASELINE);
	/*TEXTDATA.Format("CHANNEL %d",m_MonChannel+1);
	MemDC.TextOut(620,40,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());*/



	pcDC->SetStretchBltMode(COLORONCOLOR);
	pcDC->StretchBlt(0, 0, CAM_IMAGE_WIDTH , CAM_IMAGE_HEIGHT, &MemDC, 0, 0, bmpinfo.bmWidth, bmpinfo.bmHeight, SRCCOPY);
	MemDC.SelectObject(pOldBitmap);
	image.DeleteObject();
	MemDC.DeleteDC();
	ReleaseDC(pcDC);
}

void CMasterMonitorDlg::SubBmpPic(LPCSTR lpBitmapName)//나중에 전역변수로 간략화 할것임
{	
	CRect rect;
	CDC *pcDC = m_stCenterDisplay.GetDC();
	m_stCenterDisplay.GetClientRect(&rect);
			
	CDC MemDC;
	CBitmap image;
	BITMAP bmpinfo;

	MemDC.CreateCompatibleDC(pcDC);
	image.LoadBitmap(lpBitmapName);
	image.GetBitmap(&bmpinfo);
	CBitmap*pOldBitmap = (CBitmap*)MemDC.SelectObject(&image);
	pcDC->SetStretchBltMode(COLORONCOLOR);
	pcDC->StretchBlt(0, 0, rect.Width(), rect.Height(), &MemDC, 0, 0, bmpinfo.bmWidth, bmpinfo.bmHeight, SRCCOPY);
	MemDC.SelectObject(pOldBitmap);
	image.DeleteObject();
	MemDC.DeleteDC();
	ReleaseDC(pcDC);
}

void CMasterMonitorDlg::RUN_MODE_CHK(bool Mode){//
	if(Mode == TRUE){
		((CButton *)GetDlgItem(IDC_BTN_CHARTSTAND))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_AUTOSET))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_CHARTSET))->EnableWindow(0);

		((CButton *)GetDlgItem(IDC_BTN_SET_CH1))->EnableWindow(0);
	
		((CButton *)GetDlgItem(IDC_BTN_JIGDOWN))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_MASTER_MONITOR_MOVE))->EnableWindow(0);
		

		((CEdit *)GetDlgItem(IDC_Chart_VALX))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Chart_VALY))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MOFFSETX))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MOFFSETY))->EnableWindow(0);

//		((CComboBox *)GetDlgItem(IDC_COMBO_SelChNum))->EnableWindow(0);
		((CComboBox *)GetDlgItem(IDC_OPT_WRMODE))->EnableWindow(0);

	}else{
		((CButton *)GetDlgItem(IDC_BTN_CHARTSTAND))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BTN_AUTOSET))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BTN_CHARTSET))->EnableWindow(1);
		
		((CButton *)GetDlgItem(IDC_BTN_SET_CH1))->EnableWindow(1);
	
		((CButton *)GetDlgItem(IDC_BTN_JIGDOWN))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BTN_MASTER_MONITOR_MOVE))->EnableWindow(1);

		((CEdit *)GetDlgItem(IDC_Chart_VALX))->EnableWindow(1);
		((CEdit *)GetDlgItem(IDC_Chart_VALY))->EnableWindow(1);
		((CEdit *)GetDlgItem(IDC_EDIT_MOFFSETX))->EnableWindow(1);
		((CEdit *)GetDlgItem(IDC_EDIT_MOFFSETY))->EnableWindow(1);
//		((CComboBox *)GetDlgItem(IDC_COMBO_SelChNum))->EnableWindow(1);
		
		((CComboBox *)GetDlgItem(IDC_OPT_WRMODE))->EnableWindow(1);

	}
}
BOOL CMasterMonitorDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		if(pMsg->wParam == VK_ADD){
	
			return TRUE;
		}
		if (pMsg->wParam == VK_SUBTRACT){
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CMasterMonitorDlg::OnBnClickedBtnPowerOn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Power_On();
}

void CMasterMonitorDlg::OnBnClickedBtnPowerOn2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Power_Off();
}

void CMasterMonitorDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(b_TESTMOD == 1){
		int skip=0; 
		for(int t=0; t<1; t++){
			if(ClickNUM[t] < 1){
				skip =100;
				break;
			}
		}
		if(skip == 100){
			AfxMessageBox("모든 채널의 마스터 세팅이 되지 않았습니다. 다시 세팅 해주세요.");
			ShowWindow(SW_HIDE);
			ShowWindow(SW_SHOW);

			return;
		}
	}

	b_TESTMOD =0; 
	((CImageTesterDlg *)m_pMomWnd)->Power_Off();

	((CImageTesterDlg *)m_pMomWnd)->b_MasterDlg = FALSE;
	CDialog::OnCancel();
}

void CMasterMonitorDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == 1){

		if(((CImageTesterDlg *)m_pMomWnd)->b_masterBtn == 1){
			if(((CImageTesterDlg *)m_pMomWnd)->b_UserMode == 1){
				Save_Enable(1);	
			}else{
				Save_Enable(0);
			}
			Load_parameter();

			if(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd != NULL){
				CvPoint data;
				data.x = ((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.StandX;
				data.y =((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.StandY;

				UpdateCenterVal(data);
				CString str ="";
				if(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.WriteMode == 0){ //좌우반전이므로 계측된 x축은 반대다.
					str="좌우반전";
				}else if(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.WriteMode == 1){//상하반전이므로 계측된 y축은 반대다. 
					str="상하반전";
				
				}else if(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.WriteMode == 2){//무반전이므로 계측된 x와 y를 그대로 적용한다. 
					str="오리지널";
				
				}else if(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.WriteMode == 3){//로테이트이므로 x와 y 둘다 반대로 적용된다. 
					str="로테이트";
				
				}else{
					str="좌우반전";
				
				}


				

				str_OffsetX.Format("%d",m_OffsetX);
				str_OffsetY.Format("%d",m_OffsetY);


				Master_Writemod_Txt(str);
				b_Center = 1;
			}else{
				b_Center = 0;
			}

			Center_Enable(b_Center);

			

		}
		((CImageTesterDlg *)m_pMomWnd)->b_masterBtn =0;
	}
}

void CMasterMonitorDlg::Load_parameter(){

	m_MinRotate = GetPrivateProfileDouble("MASTER","MinRotate",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_MinRotate == -1){
		m_MinRotate = -3;
	}
	str_MinRotate.Format("%3.1f",m_MinRotate);
	WritePrivateProfileString("MASTER","MinRotate",str_MinRotate,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	m_MaxRotate = GetPrivateProfileDouble("MASTER","MaxRotate",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_MaxRotate == -1){
		m_MaxRotate = 3;		
	}
	str_MaxRotate.Format("%3.1f",m_MaxRotate);
	WritePrivateProfileString("MASTER","MaxRotate",str_MaxRotate,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);



	m_Minoffset = GetPrivateProfileDouble("MASTER","Minoffset",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_Minoffset == -1){
		m_Minoffset = 3;
	}
	str_Minoffset.Format("%3.1f",m_Minoffset);
	WritePrivateProfileString("MASTER","Minoffset",str_Minoffset,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	m_Maxoffset = GetPrivateProfileDouble("MASTER","Maxoffset",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_Maxoffset == -1){
		m_Maxoffset = 3;		
	}
	str_Maxoffset.Format("%3.1f",m_Maxoffset);
	WritePrivateProfileString("MASTER","Maxoffset",str_Maxoffset,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	UpdateData(FALSE);
}

void CMasterMonitorDlg::OnBnClickedBtnMasterMonitorMove()
{
	int chartDistance = 0;
	
	chartDistance = GetDlgItemInt(IDC_EDIT_MASTER_MONITOR_CHART_DISTANCE);

	if(chartDistance < 130){
		chartDistance = 130;
	}else if(chartDistance > 350){
		chartDistance = 350;
	}
	//((CImageTesterDlg *)m_pMomWnd)->Move_Chart(chartDistance);	
	
}

void CMasterMonitorDlg::OnBnClickedButtonUserExit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	
	if(((ClickNUM[0] == 1)))
	{
		OnCancel();
	}else if(((CImageTesterDlg *)m_pMomWnd)->MatchPassWord == ""){
		b_TESTMOD =0;
		OnCancel();
		
	}
	else{

		CUserSwitching subdlg;
		subdlg.Setup((CImageTesterDlg *)m_pMomWnd);
	
		if(subdlg.DoModal() == IDOK){
			b_TESTMOD =0;
			OnCancel();		
		}
		else{
			//AfxMessageBox("패스워드가 틀렸습니다");
			ShowWindow(SW_HIDE);
			ShowWindow(SW_SHOW);
		}

		delete subdlg;
		
	}

}

void CMasterMonitorDlg::OnBnClickedBtnSetCh1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChAutoSet(0);
}

void CMasterMonitorDlg::OnBnClickedBtnSetCh2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChAutoSet(1);
}

void CMasterMonitorDlg::OnBnClickedBtnSetCh3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChAutoSet(2);
}

void CMasterMonitorDlg::OnBnClickedBtnSetCh4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChAutoSet(3);
}

void CMasterMonitorDlg::ChAutoSet(int ch)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	RUN_MODE_CHK(1);
	b_TESTMOD =1;
	CenterData.x = 0;
	CenterData.y = 0;
	
	if(!((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE()){
		
	}



	MasterState_Val(0,2,"Processing");

	((CImageTesterDlg *)m_pMomWnd)->Power_On();//Power_On(); //전원을 넣는다.
	//DoEvents(1500);
// 	DoEvents(1000);
// 	
// 	((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(0);
// 
// 	DoEvents(500);

	AutosetRunning(1);//오토셋 진행

	
	UpdateMaState();

	if(ClickNUM[0] == 1){
		DoEvents(1000);
		((CImageTesterDlg *)m_pMomWnd)->Power_Off();
	}
	RUN_MODE_CHK(0);
}


HBRUSH CMasterMonitorDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() ==IDC_STATE_CH1){
		pDC->SetTextColor(Matxtcol[0]);
		pDC->SetBkColor(MaBkcol[0]);
	}
	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_WRITEMODE){
		pDC->SetTextColor(RGB(255,255,255));
		pDC->SetBkColor(RGB(86,86,86));
	}


	if(pWnd->GetDlgCtrlID() ==IDC_STATE_CH2){
		pDC->SetTextColor(Matxtcol[1]);
		pDC->SetBkColor(MaBkcol[1]);
	}
	if(pWnd->GetDlgCtrlID() ==IDC_STATE_CH3){
		pDC->SetTextColor(Matxtcol[2]);
		pDC->SetBkColor(MaBkcol[2]);
	}/*
	if(pWnd->GetDlgCtrlID() ==IDC_STATE_CH4){
		pDC->SetTextColor(Matxtcol[3]);
		pDC->SetBkColor(MaBkcol[3]);
	}*/

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CMasterMonitorDlg::AllMSetInit()
{
	for(int t=0; t<4; t++){
		ClickNUM[t]=0;
	}
	UpdateMaState();
}


void CMasterMonitorDlg::UpdateMaState()
{
	for(int lop = 0;lop<3;lop++){
		if(ClickNUM[lop] == 1){//마스터셋 진행을 성공하였을 때
			MasterState_Val(lop,1,"Success");
		}else{
			MasterState_Val(lop,0,"Stand By");
		}
	}
}

void CMasterMonitorDlg::MasterState_Val(int ch,int col,LPCSTR lpcszString, ...)
{	
	COLORREF txtCol;
	COLORREF BkCol;
	if(col == 0){
		txtCol = RGB(0, 0, 0);
		BkCol = RGB(255, 255, 0);
	}else if(col  ==1){
		txtCol = RGB(255, 255, 255);
		BkCol = RGB(50, 82, 152);
	}else{
		txtCol = RGB(255,255,255);
		BkCol = RGB(47, 157, 39);
	}
	if(ch<3){
		Matxtcol[ch] = txtCol;
		MaBkcol[ch] = BkCol;
	}else{
		for(int lop = 0;lop<3;lop++){
			Matxtcol[lop] = txtCol;
			MaBkcol[lop] = BkCol;
		}
	}
	switch(ch){
		case 0:
			((CEdit *)GetDlgItem(IDC_STATE_CH1))->SetWindowText(lpcszString);
			break;
		case 1:
			((CEdit *)GetDlgItem(IDC_STATE_CH2))->SetWindowText(lpcszString);
			break;
		case 2:
			((CEdit *)GetDlgItem(IDC_STATE_CH3))->SetWindowText(lpcszString);
			break;

		default:
			((CEdit *)GetDlgItem(IDC_STATE_CH1))->SetWindowText(lpcszString);
			break;
	}
}

void CMasterMonitorDlg::OnBnClickedBtnJigdown()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	RUN_MODE_CHK(1);

	RUN_MODE_CHK(0);
}

void CMasterMonitorDlg::Focus_move_start()
{
	if(((CButton *)GetDlgItem(IDC_BTN_SET_CH1))->IsWindowEnabled() == TRUE){
		GotoDlgCtrl(((CButton *)GetDlgItem(IDC_BTN_SET_CH1)));
	}else{
		GotoDlgCtrl(((CStatic *)GetDlgItem(IDC_STATIC)));
	}
}


void CMasterMonitorDlg::OnEnSetfocusStateCh1()
{
	Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}



void CMasterMonitorDlg::OnEnSetfocusStateCh3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMasterMonitorDlg::OnEnSetfocusStateCh4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMasterMonitorDlg::OnBnClickedBtnAutoset2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	RUN_MODE_CHK(1);

	CenterData.x = 0;
	CenterData.y = 0;
	
	if(!((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE()){
		
	}

	MasterState_Val(2,2,"Processing");

	((CImageTesterDlg *)m_pMomWnd)->Power_On();//Power_On(); //전원을 넣는다.
//	DoEvents(1500);
	RotateRunning(1);//오토셋 진행
	if(ClickNUM[0] == 1){
		DoEvents(1000);
		((CImageTesterDlg *)m_pMomWnd)->Power_Off();
	}
	RUN_MODE_CHK(0);
	//str_RotateDegree.
}

void CMasterMonitorDlg::RotateRunning(bool MOD){

	//int count = 0;
	//((CImageTesterDlg *)m_pMomWnd)->m_ImgScan =1;						
	//	
	//for(int i =0;i<10;i++){
	//	if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0){
	//		break;
	//	}else{
	//		DoEvents(50);
	//	}
	//}

	//double m_offset= atof(str_RotateOffset);

	//str_RotateOffset.Format("%6.3f",m_offset);

	//UpdateData(FALSE);

	//for(int t= 0; t<5; t++){
	//	A_RECT[t] = ((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->A_RECT[t];
	//	SC_RECT[t] = ((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->SC_RECT[t];
	//	Standard_SC_RECT[t]=((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->Standard_SC_RECT[t];//왼쪽 Side circle
	//}

	//bool FLAG = FALSE;
	//if(((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->AutomationMod ==1)
	//{
	//	FLAG = TRUE;
	//	GetSideCircleCoordinate(m_RGBScanbuf);
	//}
	//else
	//	GetSideCircleOnManualMode(m_RGBScanbuf);
	//	

	////============================= 0,2 사이값
	//horipointY1 = ((SC_RECT[2].m_resultY + SC_RECT[0].m_resultY)/2);
	//horipointX1 = ((SC_RECT[2].m_resultX + SC_RECT[0].m_resultX)/2);

	////============================= 1,3 사이값
	//horipointY2 = ((SC_RECT[3].m_resultY + SC_RECT[1].m_resultY)/2);
	//horipointX2 = ((SC_RECT[3].m_resultX + SC_RECT[1].m_resultX)/2);

	//m_fHor_Degree = Horizontal_GetRotationCheck(horipointX2, horipointY2, horipointX1, horipointY1);
	//
	////============================= 0,1 사이값

	//vetipointX1 = ((SC_RECT[1].m_resultX + SC_RECT[0].m_resultX)/2);
	//vetipointY1 = ((SC_RECT[1].m_resultY + SC_RECT[0].m_resultY)/2);

	////============================= 2,3 사이값

	//vetipointX2 = ((SC_RECT[3].m_resultX + SC_RECT[2].m_resultX)/2);
	//vetipointY2 = ((SC_RECT[3].m_resultY + SC_RECT[2].m_resultY)/2);
	////
	//m_fVer_Degree = Vertical_GetRotationCheck(vetipointX2,vetipointY2, vetipointX1,vetipointY1);		

	//double Degree = ((m_fHor_Degree) + (m_fVer_Degree)) / 2.0;

	//MasterDegree = (Degree - m_offset);

	//	

	//if(MOD == 1){

	//	if((MasterDegree >= m_MinRotate)&&(MasterDegree <= m_MaxRotate)){
	//	
	//	}else{
	//		AfxMessageBox("로테이트 한계 범위에서 벗어났습니다. \r\n 다시 설정해주세요");
	//		ClickNUM[0] = 0;
	//		ShowWindow(FALSE);
	//		ShowWindow(TRUE);
	//		RUN_MODE_CHK(0);
	//		return;
	//	}

	//	CString str="";
	//	str.Format("Rotate 각도 : %6.3f \r\n 적용하시겠습니까? ",MasterDegree);
	//	
	//	CPopUP3 subdlg;
	//	subdlg.Setup(this);
	//	subdlg.Warning_TEXT =str+"\r\n\r\n";
	//	
	//	if(subdlg.DoModal() == IDOK){

	//		str_RotateDegree.Format("%6.3f",Degree);
	//		UpdateData(FALSE);
	//		((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->MasterDegree = MasterDegree;
	//		((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->Save_parameter();
	//		ClickNUM[0] = 1;
	//	}else{
	//		ClickNUM[0] = 0;	
	//	}

	//		ShowWindow(FALSE);
	//		ShowWindow(TRUE);


	//		MasterState_Val(2,1,"Success");

	//

	//}


}
void CMasterMonitorDlg::Pic(CDC *cdc)
{
	for(int lop=0;lop<4;lop++){
		
			AnglePic(cdc,lop);
	
	}
}  

bool CMasterMonitorDlg::AnglePic(CDC *cdc,int NUM){//AVE값 및 평균 RGB값 추출 및 그리기.
	
	CPen	my_Pan,my_Pan2,*old_pan,*old_pan2; CPen my_Pen3, my_Pen4;
	CString RESULTDATA ="";
	CString TEXTDATA ="";
	
	//if(A_RECT[NUM].Chkdata() == FALSE){return 0;}
	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	//my_Pan.CreatePen(PS_SOLID,2,WHITE_COLOR);
	//old_pan = cdc->SelectObject(&my_Pan);
	//cdc->SetTextColor(WHITE_COLOR);
	if(A_RECT[NUM].m_Success == TRUE){
		my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(BLUE_COLOR);
	}else{
		my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(RED_COLOR);
	}
	
	my_Pan.DeleteObject();



	//cdc->MoveTo(A_RECT[NUM].m_resultX),int(A_RECT[NUM].m_resultY)-5);
	//cdc->LineTo(A_RECT[NUM].m_resultX),int(A_RECT[NUM].m_resultY)+5);
	
//	A_RECT[NUM].m_resultX = A_RECT[NUM].m_Left + A_RECT[NUM].m_Width/2;
//	A_RECT[NUM].m_resultY = A_RECT[NUM].m_Top + A_RECT[NUM].m_Height/2;

	///if(MasterMod == TRUE){
	//	TEXTDATA.Format("R:%03d G:%03d B:%03d",A_RECT[NUM].m_R,A_RECT[NUM].m_G,A_RECT[NUM].m_B);
//	}
//	else{
	/*if(b_CenterValueView == TRUE){
		TEXTDATA.Format("X:%6.2f Y:%6.2f",A_RECT[NUM].m_resultX,A_RECT[NUM].m_resultY);
	}*/
//	}

	CFont font;
	font.CreatePointFont(220, "Arial"); 
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);
	CFont *old_Font = cdc->SelectObject(&font);
	
	TEXTDATA.Format("%6.3f",MasterDegree);
	cdc->TextOut(100 ,CAM_IMAGE_HEIGHT-100,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	my_Pen4.CreatePen(PS_SOLID,2,GREEN_COLOR);
	CPen *old_Pen = cdc->SelectObject(&my_Pen4);
	cdc->MoveTo(SC_RECT[0].m_resultX,SC_RECT[0].m_resultY);
	cdc->LineTo(SC_RECT[1].m_resultX,SC_RECT[1].m_resultY);
	cdc->LineTo(SC_RECT[3].m_resultX,SC_RECT[3].m_resultY);
	cdc->LineTo(SC_RECT[2].m_resultX,SC_RECT[2].m_resultY);
	cdc->LineTo(SC_RECT[0].m_resultX,SC_RECT[0].m_resultY);


	cdc->SelectObject(old_Pen);
	cdc->SelectObject(old_Font);
	old_Pen->DeleteObject();
	old_Font->DeleteObject();
	my_Pen4.DeleteObject();
	font.DeleteObject();
	::SetTextColor(cdc->m_hDC,RGB(255, 255, 0)); 
	
//******************************************************************************************************************//회전각 측정		
	
	my_Pen3.DeleteObject();

//******************************************************************************************************************//회전각 측정
	
	my_Pen3.DeleteObject();
//******************************************************************************************************************

	cdc->SelectObject(old_pan);
	cdc->SelectObject(old_pan2);
	
	my_Pan.DeleteObject();
	my_Pan2.DeleteObject();
	

	return TRUE;
}

double CMasterMonitorDlg::Vertical_GetRotationCheck(double pt_x1, double pt_y1, double pt_x2, double pt_y2)
{
	double degree = atan2( (pt_y1 - pt_y2), (pt_x1 - pt_x2) );
		
	degree = degree * 180.0/3.14 - 90.0;		
		
	return degree;
}

double CMasterMonitorDlg::Horizontal_GetRotationCheck(double pt_x1, double pt_y1, double pt_x2, double pt_y2)
{
	double degree = atan2( (pt_y1 - pt_y2), (pt_x1 - pt_x2) );
		
	degree = degree * 180.0/3.14;

	return degree;
}

void CMasterMonitorDlg::GetSideCircleCoordinate(LPBYTE IN_RGB)
{
	//double Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	//double Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	//IplImage *OriginImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	//IplImage *CannyImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);	
	//IplImage *SmoothImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	//IplImage *RGBResultImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 3);	

	//BYTE R,G,B;
	//double Sum_Y;

	//for (int y=0; y<480; y++)
	//{
	//	for (int x=0; x<720; x++)
	//	{
	//		B = IN_RGB[y*2880 + x*4    ];
	//		G = IN_RGB[y*2880 + x*4 + 1];
	//		R = IN_RGB[y*2880 + x*4 + 2];
	//		
	//		Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));

	//		OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;			
	//	}
	//}
	//
	//

	//cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	//cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0); //객체 외곽의 Overshooting으로 인하여 한번 더 처리 함..

	//cvCanny(SmoothImage, CannyImage, 0, 200);
	//
	//cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);
	//
	//CvMemStorage* contour_storage = cvCreateMemStorage(0);
	//CvSeq *contour = 0;
	//CvSeq *temp_contour = 0;
	//
	//cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	//
	//temp_contour = contour;
	//
	//int counter = 0;
	//CvRect rect;
	//double area=0, arcCount=0;
	//double old_dist = 999999;
	//
	//SC_RECT[0].m_Success = FALSE;

	//for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
	//{
	//	area = cvContourArea(temp_contour, CV_WHOLE_SEQ);
	//	arcCount = cvArcLength(temp_contour, CV_WHOLE_SEQ, -1);
	//	
	//	double circularity = (4.0*3.14*area)/(arcCount*arcCount);
	//	
	//	if(circularity > 0.5)
	//	{			
	//		rect = cvContourBoundingRect(temp_contour, 1);

	//		int center_x, center_y;
	//		center_x = rect.x + rect.width/2;
	//		center_y = rect.y + rect.height/2;
	//		
	//		if(center_x > (int)(CAM_IMAGE_WIDTH*0.75) && center_x < (int)(CAM_IMAGE_WIDTH*1.25) && center_y > (int)(CAM_IMAGE_HEIGHT*0.625) && center_y < (int)(CAM_IMAGE_HEIGHT*1.375))
	//		{
	//	
	//			/*if(rect.width < 360 && rect.height < 240)
	//			{
	//				double st_circle_center_x = Standard_SC_RECT[0].m_resultX;
	//				double st_circle_center_y = Standard_SC_RECT[0].m_resultY;
	//				double curr_circle_center_x = rect.x + rect.width/2;
	//				double curr_circle_center_y = rect.y + rect.height/2;

	//				double dist = GetDistance(st_circle_center_x, st_circle_center_y, curr_circle_center_x, curr_circle_center_y);
	//				
	//				if(dist < old_dist && dist < 120)
	//				{
	//					SC_RECT[0].m_resultX = curr_circle_center_x;
	//					SC_RECT[0].m_resultY = curr_circle_center_y;

	//					old_dist = dist;

	//					SC_RECT[0].m_Success = TRUE;
	//				}
	//			}*/
	//		}
	//		else
	//		{
	//			cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8);  
	//			counter++;
	//		}
	//	}		
	//}
	//
	//int total_circle_count = counter;
	//CvRect *rectArray = new CvRect[counter];
	//counter = 0;
	//
	//for( ; contour != 0; contour=contour->h_next)
	//{
	//	area = cvContourArea(contour, CV_WHOLE_SEQ);
	//	arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

	//	rect = cvContourBoundingRect(contour, 1);		
	//
	//	double circularity = (4.0*3.14*area)/(arcCount*arcCount);		
	//	
	//	if(circularity > 0.5)
	//	{			
	//		rect = cvContourBoundingRect(contour, 1);

	//		int center_x, center_y;
	//		center_x = rect.x + rect.width/2;
	//		center_y = rect.y + rect.height/2;
	//		
	//		//if(center_x > (int)(CAM_IMAGE_WIDTH*0.75) && center_x < (int)(CAM_IMAGE_WIDTH*1.25) && center_y > (int)(CAM_IMAGE_HEIGHT*0.625) && center_y < (int)(CAM_IMAGE_HEIGHT*1.375))
	//		if(center_x > (int)(Cam_PosX*0.75) && center_x < (int)(Cam_PosX*1.25) && center_y > (int)(Cam_PosY*0.625) && center_y < (int)(Cam_PosY*1.375))

	//		{
	//	

	//		}
	//		else
	//		{
	//		//	cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8);  
	//			rectArray[counter] = rect;
	//			counter++;
	//		}
	//	}
	//}
	//
	//old_dist = 999999;

	//for(int i=0; i<4; i++)
	//{
	//	SC_RECT[i].m_Success = FALSE;

	//	for(int j=0; j<total_circle_count; j++)
	//	{
	//		rect = rectArray[j];
	//		
	//	//	double st_circle_center_x = Standard_SC_RECT[i].m_resultX;
	//	//	double st_circle_center_y = Standard_SC_RECT[i].m_resultY;
	//		
	//		double st_circle_center_x = A_RECT[i].m_resultX;
	//		double st_circle_center_y = A_RECT[i].m_resultY;

	//		double curr_circle_center_x = rect.x + rect.width/2;
	//		double curr_circle_center_y = rect.y + rect.height/2;

	//		double dist = GetDistance(st_circle_center_x, st_circle_center_y, curr_circle_center_x, curr_circle_center_y);
	//		
	//		if(dist < old_dist && dist < 150)
	//		{
	//			SC_RECT[i].m_resultX = curr_circle_center_x;
	//			SC_RECT[i].m_resultY = curr_circle_center_y;

	//			old_dist = dist;

	//			SC_RECT[i].m_Success = TRUE;
	//		}
	//	//	cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 255), 1, 8);  
	//	}

	//	old_dist = 999999;
	//}
	//
	///*for(int i=0; i<4; i++)
	//{
	//	if(SC_RECT[i].m_Success == FALSE)
	//	{
	//		SC_RECT[i].m_resultX = Standard_SC_RECT[i].m_resultX;
	//		SC_RECT[i].m_resultY = Standard_SC_RECT[i].m_resultY;
	//	}
	//}*/

	//cvSaveImage("C:\\SideCircleResultImage.bmp", RGBResultImage);
	//
	//int offset_x = A_RECT[0].m_resultX - SC_RECT[0].m_resultX;
	//int offset_y = A_RECT[0].m_resultY - SC_RECT[0].m_resultY;
	//
	//for(int k=0; k<4; k++)
	//{

	//	A_RECT[k].m_Left -= (double)(offset_x);
	//	A_RECT[k].m_Top -= (double)(offset_y);
	//	
	//	A_RECT[k].m_Right = A_RECT[k].m_Left + A_RECT[k].m_Width;
	//	A_RECT[k].m_Bottom = A_RECT[k].m_Top + A_RECT[k].m_Height;

	////	A_RECT[k].m_resultX -= offset_x;
	////	A_RECT[k].m_resultY -= offset_y;

	//	A_RECT[k].m_resultX = A_RECT[k].m_Left + A_RECT[k].m_Width/2;
	//	A_RECT[k].m_resultY = A_RECT[k].m_Top + A_RECT[k].m_Height/2;

	//}

	//cvReleaseMemStorage(&contour_storage);
	//cvReleaseImage(&OriginImage);
	//cvReleaseImage(&CannyImage);	
	//cvReleaseImage(&SmoothImage);
	//cvReleaseImage(&RGBResultImage);	
	//
	//delete []rectArray;
}

void CMasterMonitorDlg::GetSideCircleOnManualMode(LPBYTE IN_RGB)
{
	IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);

	BYTE R,G,B;
	double Sum_Y;

	for (int y=0; y<480; y++)
	{
		for (int x=0; x<720; x++)
		{
			B = IN_RGB[y*2880 + x*4    ];
			G = IN_RGB[y*2880 + x*4 + 1];
			R = IN_RGB[y*2880 + x*4 + 2];
			
			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));	

			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;			
		}
	}	
	
//	Capture_Gray_SAVE("C:\\RGBIMAGE", IN_RGB, 720, 480);	
//	cvSaveImage("C:\\Default_Image.bmp", OriginImage);

	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0); //객체 외곽의 Overshooting으로 인하여 한번 더 처리 함..

	cvCanny(SmoothImage, CannyImage, 0, 200);
	
	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);
	
	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;
	CvRect rect;
	double area=0, arcCount=0;
	double old_dist = 999999;	

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
	{
		area = cvContourArea(temp_contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(temp_contour, CV_WHOLE_SEQ, -1);
		
		rect = cvContourBoundingRect(temp_contour, 1);

		int center_x, center_y;
		center_x = rect.x + rect.width/2;
		center_y = rect.y + rect.height/2;
		
		for(int k=0; k<4; k++)
		{
			if( A_RECT[k].m_Left < center_x && A_RECT[k].m_Right > center_x )
			{
				if( A_RECT[k].m_Top < center_y && A_RECT[k].m_Bottom > center_y )
				{
					SC_RECT[k].m_resultX = center_x;
					SC_RECT[k].m_resultY = center_y;

					SC_RECT[k].m_Success = TRUE;
				}
			}
		}
	}
	
		
	for(int i=0; i<4; i++)
	{
		if(SC_RECT[i].m_Success == FALSE)
		{
			SC_RECT[i].m_resultX = Standard_SC_RECT[i].m_resultX;
			SC_RECT[i].m_resultY = Standard_SC_RECT[i].m_resultY;
		}
	}

//	cvSaveImage("C:\\SideCircleResultImage.bmp", RGBResultImage);
	
	int offset_x = A_RECT[0].m_resultX - SC_RECT[0].m_resultX;
	int offset_y = A_RECT[0].m_resultY - SC_RECT[0].m_resultY;
	
	for(int k=0; k<4; k++)
	{

		A_RECT[k].m_Left -= offset_x/2;
		A_RECT[k].m_Top -= offset_y/2;
		
		A_RECT[k].m_Right = A_RECT[k].m_Left + A_RECT[k].m_Width;
		A_RECT[k].m_Bottom = A_RECT[k].m_Top + A_RECT[k].m_Height;

	//	A_RECT[k].m_resultX -= offset_x;
	//	A_RECT[k].m_resultY -= offset_y;

		A_RECT[k].m_resultX = A_RECT[k].m_Left + A_RECT[k].m_Width/2;
		A_RECT[k].m_resultY = A_RECT[k].m_Top + A_RECT[k].m_Height/2;

	}

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&CannyImage);	
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);	
	
//	delete []rectArray;
}

void CMasterMonitorDlg::Rotate_Enable(bool MOD){

		((CEdit *)GetDlgItem(IDC_STATE_CH3))->EnableWindow(MOD);
		((CEdit *)GetDlgItem(IDC_EDIT_ROTATE))->EnableWindow(MOD);
	//	((CButton *)GetDlgItem(IDC_BTN_AUTOSET2))->EnableWindow(0);

}


void CMasterMonitorDlg::Center_Enable(bool MOD){

		((CEdit *)GetDlgItem(IDC_EDIT_MOFFSETX))->EnableWindow(MOD);
		((CEdit *)GetDlgItem(IDC_EDIT_MOFFSETY))->EnableWindow(MOD);
	//	((CButton *)GetDlgItem(IDC_BTN_AUTOSET2))->EnableWindow(0);

}



void CMasterMonitorDlg::Save_Enable(bool MOD){

		((CEdit *)GetDlgItem(IDC_EDIT_Minoffset))->EnableWindow(MOD);
		((CEdit *)GetDlgItem(IDC_EDIT_Maxoffset))->EnableWindow(MOD);
		((CEdit *)GetDlgItem(IDC_EDIT_Minoffset2))->EnableWindow(MOD);
		((CEdit *)GetDlgItem(IDC_EDIT_Maxoffset2))->EnableWindow(MOD);
		((CButton *)GetDlgItem(IDC_BUTTON_SAVE))->EnableWindow(MOD);
		((CButton *)GetDlgItem(IDC_BUTTON_SAVE2))->EnableWindow(MOD);

}

void CMasterMonitorDlg::OnBnClickedButtonSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_Minoffset = atof(str_Minoffset);
	m_Maxoffset = atof(str_Maxoffset);

	if(m_Minoffset < 0){
		m_Minoffset = abs(m_Minoffset);
	}
	if(m_Maxoffset < 0){
		m_Maxoffset = abs(m_Maxoffset);
	}
	UpdateData(FALSE);

	str_Minoffset.Format("%3.1f",m_Minoffset);
	str_Maxoffset.Format("%3.1f",m_Maxoffset);

	WritePrivateProfileString("MASTER","Minoffset",str_Minoffset,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	WritePrivateProfileString("MASTER","Maxoffset",str_Maxoffset,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
}

void CMasterMonitorDlg::OnEnChangeEditMinoffset()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMasterMonitorDlg::OnEnChangeEditMaxoffset()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMasterMonitorDlg::OnBnClickedButtonSave2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_MinRotate= atof(str_MinRotate);
	m_MaxRotate = atof(str_MaxRotate);

	if(m_MinRotate >= m_MaxRotate){
		m_MaxRotate =m_MinRotate+1;
	}
	UpdateData(FALSE);

	str_MinRotate.Format("%3.1f",m_MinRotate);
	str_MaxRotate.Format("%3.1f",m_MaxRotate);

	WritePrivateProfileString("MASTER","MinRotate",str_MinRotate,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	WritePrivateProfileString("MASTER","MaxRotate",str_MaxRotate,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
}

void CMasterMonitorDlg::OnEnChangeEditMinoffset2()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMasterMonitorDlg::OnEnChangeEditMaxoffset2()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMasterMonitorDlg::OnEnChangeEditDegree()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMasterMonitorDlg::OnEnSetfocusEditWritemode()
{
	Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMasterMonitorDlg::OnEnSetfocusEditMaxisx()
{
	Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMasterMonitorDlg::OnEnSetfocusEditMaxisy()
{
	Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMasterMonitorDlg::OnEnSetfocusStateCh2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
}

void CMasterMonitorDlg::OnBnClickedBtnOverlayon()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(1);
}

void CMasterMonitorDlg::OnBnClickedBtnOverlayoff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(0);
}

void CMasterMonitorDlg::OnEnChangeEditMsettx()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	int settX = atoi(str_msettX);
	int bufoffx = settX - CAM_IMAGE_WIDTH/2;
	str_OffsetX.Format("%d",bufoffx);
	UpdateData(FALSE);
}

void CMasterMonitorDlg::OnEnChangeEditMsetty()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	int settY = atoi(str_msettY);
	int bufoffy = settY - CAM_IMAGE_HEIGHT/2;
	str_OffsetY.Format("%d",bufoffy);
	UpdateData(FALSE);

}
