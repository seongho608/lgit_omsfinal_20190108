// Brightness_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Brightness_ResultView.h"


// CBrightness_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CBrightness_ResultView, CDialog)

CBrightness_ResultView::CBrightness_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CBrightness_ResultView::IDD, pParent)
{
	
}

CBrightness_ResultView::~CBrightness_ResultView()
{
	
}

void CBrightness_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CBrightness_ResultView, CDialog)
	ON_WM_CTLCOLOR()
	ON_EN_SETFOCUS(IDC_EDIT_LOCATION1, &CBrightness_ResultView::OnEnSetfocusEditLocation1)
	ON_EN_SETFOCUS(IDC_EDIT_RESULT, &CBrightness_ResultView::OnEnSetfocusEditResult)
	ON_EN_SETFOCUS(IDC_EDIT_VALUE, &CBrightness_ResultView::OnEnSetfocusEditValue)
	ON_EN_SETFOCUS(IDC_EDIT_LOCATION2, &CBrightness_ResultView::OnEnSetfocusEditLocation2)
	ON_EN_SETFOCUS(IDC_EDIT_VALUE2, &CBrightness_ResultView::OnEnSetfocusEditValue2)
	ON_EN_SETFOCUS(IDC_EDIT_RESULT2, &CBrightness_ResultView::OnEnSetfocusEditResult2)
	ON_EN_SETFOCUS(IDC_EDIT_LOCATION3, &CBrightness_ResultView::OnEnSetfocusEditLocation3)
	ON_EN_SETFOCUS(IDC_EDIT_VALUE3, &CBrightness_ResultView::OnEnSetfocusEditValue3)
	ON_EN_SETFOCUS(IDC_EDIT_RESULT3, &CBrightness_ResultView::OnEnSetfocusEditResult3)
	ON_EN_SETFOCUS(IDC_EDIT_LOCATION4, &CBrightness_ResultView::OnEnSetfocusEditLocation4)
	ON_EN_SETFOCUS(IDC_EDIT_VALUE4, &CBrightness_ResultView::OnEnSetfocusEditValue4)
	ON_EN_SETFOCUS(IDC_EDIT_RESULT4, &CBrightness_ResultView::OnEnSetfocusEditResult4)
	ON_EN_SETFOCUS(IDC_EDIT_LOCATION5, &CBrightness_ResultView::OnEnSetfocusEditLocation5)
	ON_EN_SETFOCUS(IDC_EDIT_VALUE5, &CBrightness_ResultView::OnEnSetfocusEditValue5)
	ON_EN_SETFOCUS(IDC_EDIT_RESULT5, &CBrightness_ResultView::OnEnSetfocusEditResult5)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CBrightness_ResultView 메시지 처리기입니다.
void CBrightness_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CBrightness_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
BOOL CBrightness_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	Font_Size_Change(IDC_EDIT_LOCATION1,&ft1,40,20);
	GetDlgItem(IDC_EDIT_LOCATION2)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_LOCATION3)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_LOCATION4)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_LOCATION5)->SetFont(&ft1);
	
	GetDlgItem(IDC_EDIT_VALUE)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_VALUE2)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_VALUE3)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_VALUE4)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_VALUE5)->SetFont(&ft1);

	GetDlgItem(IDC_EDIT_RESULT)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_RESULT2)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_RESULT3)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_RESULT4)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_RESULT5)->SetFont(&ft1);

	Initstat();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void CBrightness_ResultView::Initstat(){
	RESULT_TEXT(0,"STAND BY");
	RESULT_TEXT2(0,"STAND BY");
	RESULT_TEXT3(0,"STAND BY");
	RESULT_TEXT4(0,"STAND BY");
	RESULT_TEXT5(0,"STAND BY");

	VALUE_TEXT("");
	VALUE_TEXT2("");
	VALUE_TEXT3("");
	VALUE_TEXT4("");
	VALUE_TEXT5("");

	LOCATION_TEXT("LINE_1");
	LOCATION_TEXT2("LINE_2");
	LOCATION_TEXT3("LINE_3");
	LOCATION_TEXT4("LINE_4");
	LOCATION_TEXT5("RIGHT BOTTOM");
}
HBRUSH CBrightness_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_LOCATION1){
			pDC->SetTextColor(RGB(255, 255, 255));

			pDC->SetBkColor(RGB(86,86,86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_LOCATION2){
			pDC->SetTextColor(RGB(255, 255, 255));

			pDC->SetBkColor(RGB(86,86,86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_LOCATION3){
			pDC->SetTextColor(RGB(255, 255, 255));

			pDC->SetBkColor(RGB(86,86,86));
	}

	
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_LOCATION4){
			pDC->SetTextColor(RGB(255, 255, 255));

			pDC->SetBkColor(RGB(86,86,86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_LOCATION5){
			pDC->SetTextColor(RGB(255, 255, 255));

			pDC->SetBkColor(RGB(86,86,86));
	}


	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255,255,255));
	}
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE2){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255,255,255));
	}
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE3){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255,255,255));
	}
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE4){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255,255,255));
	}
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE5){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255,255,255));
	}


	if(pWnd->GetDlgCtrlID() == IDC_EDIT_RESULT){
		pDC->SetTextColor(txcol_TVal);
		pDC->SetBkColor(bkcol_TVal);
	}
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_RESULT2){
		pDC->SetTextColor(txcol_TVal2);
		pDC->SetBkColor(bkcol_TVal2);
	}
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_RESULT3){
		pDC->SetTextColor(txcol_TVal3);
		pDC->SetBkColor(bkcol_TVal3);
	}
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_RESULT4){
		pDC->SetTextColor(txcol_TVal4);
		pDC->SetBkColor(bkcol_TVal4);
	}
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_RESULT5){
		pDC->SetTextColor(txcol_TVal5);
		pDC->SetBkColor(bkcol_TVal5);
	}


	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


void CBrightness_ResultView::LOCATION_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_LOCATION1))->SetWindowText(lpcszString);
}
void CBrightness_ResultView::LOCATION_TEXT2(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_LOCATION2))->SetWindowText(lpcszString);
}
void CBrightness_ResultView::LOCATION_TEXT3(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_LOCATION3))->SetWindowText(lpcszString);
}
void CBrightness_ResultView::LOCATION_TEXT4(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_LOCATION4))->SetWindowText(lpcszString);
}
void CBrightness_ResultView::LOCATION_TEXT5(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_LOCATION5))->SetWindowText(lpcszString);
}

void CBrightness_ResultView::VALUE_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_VALUE))->SetWindowText(lpcszString);
}
void CBrightness_ResultView::VALUE_TEXT2(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_VALUE2))->SetWindowText(lpcszString);
}
void CBrightness_ResultView::VALUE_TEXT3(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_VALUE3))->SetWindowText(lpcszString);
}
void CBrightness_ResultView::VALUE_TEXT4(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_VALUE4))->SetWindowText(lpcszString);
}
void CBrightness_ResultView::VALUE_TEXT5(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_VALUE5))->SetWindowText(lpcszString);
}

void CBrightness_ResultView::RESULT_TEXT(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0){//대기
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18,  69,171);
	}else if(col == 2){//실패
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_RESULT))->SetWindowText(lpcszString);
}
void CBrightness_ResultView::RESULT_TEXT2(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0){
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(47, 157, 39);
	}else if(col == 1){
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(18, 69, 171);
	}else if(col == 2){
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal2 = RGB(0, 0, 0);
		bkcol_TVal2 = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_RESULT2))->SetWindowText(lpcszString);
}
void CBrightness_ResultView::RESULT_TEXT3(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0){
		txcol_TVal3 = RGB(255, 255, 255);
		bkcol_TVal3 = RGB(47, 157, 39);
	}else if(col == 1){
		txcol_TVal3 = RGB(255, 255, 255);
		bkcol_TVal3 = RGB(18, 69, 171);
	}else if(col == 2){
		txcol_TVal3 = RGB(255, 255, 255);
		bkcol_TVal3 = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal3 = RGB(0, 0, 0);
		bkcol_TVal3 = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_RESULT3))->SetWindowText(lpcszString);
}
void CBrightness_ResultView::RESULT_TEXT4(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0){
		txcol_TVal4 = RGB(255, 255, 255);
		bkcol_TVal4 = RGB(47, 157, 39);
	}else if(col == 1){
		txcol_TVal4 = RGB(255, 255, 255);
		bkcol_TVal4 = RGB(18, 69, 171);
	}else if(col == 2){
		txcol_TVal4 = RGB(255, 255, 255);
		bkcol_TVal4 = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal4 = RGB(0, 0, 0);
		bkcol_TVal4 = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_RESULT4))->SetWindowText(lpcszString);
}
void CBrightness_ResultView::RESULT_TEXT5(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0){
		txcol_TVal5 = RGB(255, 255, 255);
		bkcol_TVal5 = RGB(47, 157, 39);
	}else if(col == 1){
		txcol_TVal5 = RGB(255, 255, 255);
		bkcol_TVal5 = RGB(18, 69, 171);
	}else if(col == 2){
		txcol_TVal5 = RGB(255, 255, 255);
		bkcol_TVal5 = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal5 = RGB(0, 0, 0);
		bkcol_TVal5 = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_RESULT5))->SetWindowText(lpcszString);
}

void CBrightness_ResultView::OnEnSetfocusEditLocation1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CBrightness_ResultView::OnEnSetfocusEditResult()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CBrightness_ResultView::OnEnSetfocusEditValue()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CBrightness_ResultView::OnEnSetfocusEditLocation2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CBrightness_ResultView::OnEnSetfocusEditValue2()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CBrightness_ResultView::OnEnSetfocusEditResult2()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CBrightness_ResultView::OnEnSetfocusEditLocation3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CBrightness_ResultView::OnEnSetfocusEditValue3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CBrightness_ResultView::OnEnSetfocusEditResult3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CBrightness_ResultView::OnEnSetfocusEditLocation4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CBrightness_ResultView::OnEnSetfocusEditValue4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CBrightness_ResultView::OnEnSetfocusEditResult4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CBrightness_ResultView::OnEnSetfocusEditLocation5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CBrightness_ResultView::OnEnSetfocusEditValue5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CBrightness_ResultView::OnEnSetfocusEditResult5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

BOOL CBrightness_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CBrightness_ResultView::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	
}
