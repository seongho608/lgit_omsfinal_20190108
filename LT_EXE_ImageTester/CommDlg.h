#pragma once
#include "afxcmn.h"
#include "resource.h"


// CCommDlg 대화 상자입니다.

class CCommDlg : public CDialog
{
	DECLARE_DYNAMIC(CCommDlg)

public:
	CCommDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCommDlg();
	void DisplayDebugData(BYTE* m_data, int len, char mode,int NUM);

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_Communication };
	void	Setup(CWnd* IN_pMomWnd);
private:
	CWnd	*m_pMomWnd;	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CRichEditCtrl m_CommuEdit;
	afx_msg void OnBnClickedButtonClear();
//	afx_msg void OnBnClickedCancel();
protected:
	virtual void OnCancel();
public:
	afx_msg void OnBnClickedCommAsciiCheck2();
	BOOL m_CheckCommAscii;
	virtual BOOL OnInitDialog();
};
