#pragma once
#include "ETCUSER.H"
#include "afxcmn.h"
// #include "ImageTestLibrary.h"
// 
// //#pragma comment(lib, "ImageTestLibrary.lib")
// #pragma comment(lib, "ImageTestLibraryD.lib")

class CParticle_Option_LG : public CDialog
{
	DECLARE_DYNAMIC(CParticle_Option_LG)

public:
	CParticle_Option_LG(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CParticle_Option_LG();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_PARTICLE_LG };
	CRectData P_RECT[255];
	CRectData R_RECT[1];
	CRectData FULL_RECT[1];
	int		ParticleCnt;

	int Stain_ParticleCnt;
	int Black_ParticleCnt;
	
//	BYTE	m_pYDATA[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];
	void	Focus_move_start();
	int		m_CAM_SIZE_WIDTH;
	int		m_CAM_SIZE_HEIGHT;
	
	CString CSVMODELGEN();
	CString ResultString[6];
	CString R_filename;
	tResultVal Run();
	BOOL	m_bBlackSpotEn;
	BOOL	m_bSTAINEn;
	BOOL	CaptureImage();

	int m_STATBS;
	int m_STATST2;

	int m_BSDefectCount;
	int m_BSSingleDefectCount;
	

	BOOL	InspectionBlackSpot(BYTE *m_pFrame8BitBuffer, int width, int height);
	
	int m_ST2DefectCount;
	int m_ST2SingleDefectCount;
	
	BOOL	InspectionStain2(BYTE *m_pFrame8BitBuffer, int width, int height);

	/*CAtomImageBlackSpotContrast m_BlackSpot;
	TBlackSpotContrast BlackSpotConfig;
	const TDefectResult* blackSpot;

	CAtomImageStainRU_Ymean m_Stain;
	TRU_YmeanSpec Stain2Config;
	const TDefectResult* stain;*/

// 	CBlemish *m_BlackSpot;
// 	TBlackSpotContrast BlackSpotConfig;
// 	const TDefectResult* blackSpot;
// 
// 	CRU_Ymean *m_Stain;
// 	TRU_YmeanSpec Stain2Config;
// 	const TDefectResult* stain;


	void	Pic(CDC *cdc);
	void	InitPrm();
	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
	
	void	CdcDrawText(CDC *cdc,int x,int y,COLORREF rgb,int fontsize,LPCSTR format, ... );
	void	CdcDrawRect(CDC *cdc,int x,int y,COLORREF rgb,int wegih,int height,int mode);//사각형을 그린다. 0:x,y가 중심;1:x,y가 시작 2:x
	void	CdcDrawCircle(CDC *cdc,int x,int y,COLORREF rgb,int radio,int size,int mode);

	BOOL	ParticlePic(CDC *cdc,int NUM);
	bool	ParticleGen(LPBYTE IN_Y,int NUM);

	void	Save_parameter();
	void	Load_parameter();
	void	Save(int NUM);
	void	EXCEL_SAVE();
	bool	EXCEL_UPLOAD();
	int		StartCnt;

	void	Set_List(CListCtrl *List);
	void	InsertList();
	int		InsertIndex;
	int		ListItemNum;
	
	void	FAIL_UPLOAD();
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	CFont	ft1,ft2;
	
	void	TESTMODE_NAME(bool MODE);
	// MES 정보저장
	CString m_szMesResult;
	CString Mes_Result();


	int i_Block_W;
	int i_Block_H;
	double d_DF_Ratio;
	int i_Cluster_Size;
	int i_DF_inCluster;
	int i_DefectNum;
	int i_RoiRadius;
	BOOL i_Stain_Debug;

	void 	InputData_BlockSpot();
	void	Save_parameter_BlockSpot();
	void	Load_parameter_BlockSpot();
	void	Edit_En_BlackSpot(bool MODE);


	int i_EdgeSize;
	float f_Center_Thr;
	float f_Edge_Thr;
	int i_Stain_Block_Width;
	int i_Stain_ROIRadius;


	void 	InputData_Stain();
	void	Save_parameter_Stain();
	void	Load_parameter_Stain();
	void	Edit_En_Stain(bool MODE);

	void	BlockspotChangeChk();
	void	StainChangeChk();
#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	int Lot_StartCnt;
	int Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

#pragma endregion
	void	InitEVMS();
private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
//	int m_Number;
	CString str_model;
	CString strTitle;
	tINFO		m_INFO;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_ParticleList;
	CListCtrl m_Lot_ParticleList;
	afx_msg void OnBnClickedCheckManualtestLg();
		virtual BOOL OnInitDialog();
//	BOOL b_ManualTestMode;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CString str_Block_W;
	CString str_Block_H;
	CString str_DF_Ratio;
	CString str_Cluster_Size;
	CString str_DF_inCluster;
	CString str_DefectNum;
	CString str_RoiRadius;
	afx_msg void OnBnClickedButtonBlockspotSave();
	afx_msg void OnEnChangeEditBlackW();
	afx_msg void OnEnChangeEditBlackH();
	afx_msg void OnEnChangeEditDfRatio();
	afx_msg void OnEnChangeEditClustersize();
	afx_msg void OnEnChangeEditDefectincluster();
	afx_msg void OnEnChangeEditDefectnum();
	afx_msg void OnEnChangeEditRoiradius();
	BOOL b_BlackSpotTESTEn;
	afx_msg void OnBnClickedCheckBlackSpot();
	afx_msg void OnBnClickedCheckStaintest();
	BOOL b_StainTESTEn;
	afx_msg void OnEnChangeEditEdgesize();
	afx_msg void OnEnChangeEditCenterThr();
	afx_msg void OnEnChangeEditEdgeThr();
	afx_msg void OnEnChangeEditStainRoiradius();
	afx_msg void OnEnChangeEditBlockwidth();
	afx_msg void OnBnClickedButtonStainSave();
	CString str_EdgeSize;
	CString str_Center_Thr;
	CString str_Edge_Thr;
	CString str_Stain_Block_Width;
	CString str_Stain_ROIRadius;
	BOOL b_ManualTestMode;
	afx_msg void OnCbnSelchangeComboTestmode();
//	int m_uTestMode;
	int m_uTestMode;
	BOOL Stain_Debug;
	afx_msg void OnBnClickedCheckDebug();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnEnSetfocusEditModeStatLg();
};

void CModel_Create(CParticle_Option_LG **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO);
void CModel_Delete(CParticle_Option_LG **pWnd);