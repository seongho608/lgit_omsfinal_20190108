// Angle_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Rotate_Option.h"


// CRotate_Option 대화 상자입니다.
extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4]; 
extern WORD	m_GRAYScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];

IMPLEMENT_DYNAMIC(CRotate_Option, CDialog)

CRotate_Option::CRotate_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CRotate_Option::IDD, pParent)
	, A_Total_PosX(0)
	, A_Total_PosY(0)
	, A_Dis_Width(0)
	, A_Dis_Height(0)
	, A_Total_Width(0)
	, A_Total_Height(0)
	, A_Thresold(0)
	, TestModALL(FALSE)
	, TestModVer(FALSE)
	, TestModHor(FALSE)
	, m_dRotationAngle(0)
	, b_CenterValueView(FALSE)
	, b_DegreeValueView(FALSE)
	, b_RectView(FALSE)
	, str_minDegree(_T(""))
	, str_maxDegree(_T(""))
	, str_MasterDegree(_T(""))
	, OFFSET(_T(""))
	, OFFSET_M(_T(""))
	, bOffsetType(FALSE)
	, str_Angle_Offset(_T(""))
{
	iSavedItem=0;
	iSavedSubitem=0;
	iSavedItem_T = 0;
	iSavedSubitem_T = 0;
	iSavedSubitem2=0;
	state=0;
	EnterState=0;
	MasterMod =FALSE;
	NewItem =0;NewSubitem=0;
	disL=0;
	disR=0;
	disT=0;
	disB=0;
	TestMod=0;
	AutomationMod=0;
	ChangeCheck=0;
	for(int t=0; t<5; t++){
		ChangeItem[t]=-1;
		ChangeItem_T[t] = -1;
	}
	changecount=0;
	changecount_T = 0;
	b_RunEn=0;
	StartCnt =0;
	b_OptionEn=0;

		horipointY1=0; 
	 horipointY2=0;

	 vetipointX1=0;
	 vetipointX2=0;

	WheelCheck=0;
		b_SetZone = FALSE;
		ResultDegree=0;


	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;
	b_StopFail = FALSE;
	m_dAngleOffset = 1.0;

//	//****************Side Circle 기준 탐색 점 설정*******************//
	//Standard_SC_RECT[0].m_resultX = 360;	//중앙 Side circle
	//Standard_SC_RECT[0].m_resultY = 240;
	//Standard_SC_RECT[1].m_resultX = 100;	//왼쪽 Side circle
	//Standard_SC_RECT[1].m_resultY = 240;
	//Standard_SC_RECT[2].m_resultX = 620;	//오른쪽 Side circle
	//Standard_SC_RECT[2].m_resultY = 240;
	//Standard_SC_RECT[3].m_resultX = 360;	//위쪽 Side circle
	//Standard_SC_RECT[3].m_resultY = 0;
	//Standard_SC_RECT[4].m_resultX = 360;	//아래쪽 Side circle
	//Standard_SC_RECT[4].m_resultY = CAM_IMAGE_HEIGHT;
////****************************************************************//

//	Standard_SC_RECT[0].m_resultX = 260;	//왼쪽 Side circle
//	Standard_SC_RECT[0].m_resultY = 140;
//	Standard_SC_RECT[1].m_resultX = 460;	//오른쪽 Side circle
//	Standard_SC_RECT[1].m_resultY = 140;
//	Standard_SC_RECT[2].m_resultX = 260;	//위쪽 Side circle
//	Standard_SC_RECT[2].m_resultY = 340;
//	Standard_SC_RECT[3].m_resultX = 460;	//아래쪽 Side circle
//	Standard_SC_RECT[3].m_resultY = 340;

	Standard_SC_RECT[0].m_resultX = (CAM_IMAGE_WIDTH/2) - ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);        //왼쪽위사각형
    Standard_SC_RECT[0].m_resultY = (CAM_IMAGE_HEIGHT/2) - ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);
    Standard_SC_RECT[1].m_resultX = (CAM_IMAGE_WIDTH/2) + ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);        //오른쪽위사각형
    Standard_SC_RECT[1].m_resultY = (CAM_IMAGE_HEIGHT/2) - ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);
    Standard_SC_RECT[2].m_resultX = (CAM_IMAGE_WIDTH/2) - ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);;        //왼쪽아래사각형
    Standard_SC_RECT[2].m_resultY = (CAM_IMAGE_HEIGHT/2) + ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);
    Standard_SC_RECT[3].m_resultX = (CAM_IMAGE_WIDTH/2) + ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);;        //오른쪽아래사각형
    Standard_SC_RECT[3].m_resultY = (CAM_IMAGE_HEIGHT/2) + ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);


}

CRotate_Option::~CRotate_Option()
{
//	KillTimer(110);
}

void CRotate_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_Angle, A_DATALIST);
	DDX_Text(pDX, IDC_Angle_PosX, A_Total_PosX);
	DDX_Text(pDX, IDC_Angle_PosY, A_Total_PosY);
	DDX_Text(pDX, IDC_Angle_Dis_W, A_Dis_Width);
	DDX_Text(pDX, IDC_Angle_Dis_H, A_Dis_Height);
	DDX_Text(pDX, IDC_Angle_Width, A_Total_Width);
	DDX_Text(pDX, IDC_Angle_Height, A_Total_Height);
	DDX_Text(pDX, IDC_Angle_Thresold, A_Thresold);
	DDX_Control(pDX, IDC_LIST_ANGLELIST, m_AngleList);
	DDX_Check(pDX, IDC_CHECK1, TestModALL);
	DDX_Check(pDX, IDC_CHECK_Ver, TestModVer);
	DDX_Check(pDX, IDC_CHECK_Hor, TestModHor);
	//DDX_Text(pDX, IDC_ROTATION_ANGLE, m_dRotationAngle);
	DDV_MinMaxInt(pDX, m_dRotationAngle, 0, 30);
	DDX_Check(pDX, IDC_CHECK_CENTER, b_CenterValueView);
	DDX_Check(pDX, IDC_CHECK_DEGREE, b_DegreeValueView);
	DDX_Check(pDX, IDC_CHECK_RECT, b_RectView);
	DDX_Text(pDX, IDC_ROTATION_ANGLE, str_minDegree);
	DDX_Text(pDX, IDC_ROTATION_ANGLE2, str_maxDegree);
	DDX_Text(pDX, IDC_EDIT_MASTER, str_MasterDegree);
//	DDX_Control(pDX, IDC_LIST_ANGLELIST_LOT, m_Lot_RotateList);
	DDX_Text(pDX, IDC_ROTATION_OFFSET, OFFSET);
	DDX_Text(pDX, IDC_ROTATION_OFFSET_M, OFFSET_M);
	DDX_Check(pDX, IDC_CHECK_TYPE, bOffsetType);
	DDX_Control(pDX, IDC_LIST_TILT, A_TILTLIST);
	DDX_Text(pDX, IDC_ROTATION_ANGLE_OFFSET, str_Angle_Offset);
}


BEGIN_MESSAGE_MAP(CRotate_Option, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_setAngleZone, &CRotate_Option::OnBnClickedButtonsetanglezone)
	ON_BN_CLICKED(IDC_BUTTON_A_RECT_SAVE, &CRotate_Option::OnBnClickedButtonARectSave)
	ON_NOTIFY(NM_CLICK, IDC_LIST_Angle, &CRotate_Option::OnNMClickListAngle)
//	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_MASTER_A, &CRotate_Option::OnBnClickedButtonMasterA)
	ON_BN_CLICKED(IDC_CHECK_Ver, &CRotate_Option::OnBnClickedCheckVer)
	ON_BN_CLICKED(IDC_CHECK_Hor, &CRotate_Option::OnBnClickedCheckHor)
	ON_BN_CLICKED(IDC_CHECK_AUTO, &CRotate_Option::OnBnClickedCheckAuto)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_Angle, &CRotate_Option::OnNMCustomdrawListAngle)
//	ON_WM_SETFOCUS()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_Angle, &CRotate_Option::OnNMDblclkListAngle)
	ON_EN_KILLFOCUS(IDC_EDIT_A_MOD, &CRotate_Option::OnEnKillfocusEditAMod)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_A_Load, &CRotate_Option::OnBnClickedButtonALoad)
	ON_BN_CLICKED(IDC_CHECK1, &CRotate_Option::OnBnClickedCheck1)
	ON_EN_CHANGE(IDC_ROTATION_ANGLE, &CRotate_Option::OnEnChangeRotationAngle)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CRotate_Option::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_PICSAVE, &CRotate_Option::OnBnClickedButtonPicsave)
	ON_BN_CLICKED(IDC_CHECK_CENTER, &CRotate_Option::OnBnClickedCheckCenter)
	ON_BN_CLICKED(IDC_CHECK_DEGREE, &CRotate_Option::OnBnClickedCheckDegree)
	ON_BN_CLICKED(IDC_CHECK_RECT, &CRotate_Option::OnBnClickedCheckRect)
	ON_EN_CHANGE(IDC_ROTATION_ANGLE2, &CRotate_Option::OnEnChangeRotationAngle2)
	ON_EN_CHANGE(IDC_Angle_PosX, &CRotate_Option::OnEnChangeAnglePosx)
	ON_EN_CHANGE(IDC_Angle_PosY, &CRotate_Option::OnEnChangeAnglePosy)
	ON_EN_CHANGE(IDC_Angle_Dis_W, &CRotate_Option::OnEnChangeAngleDisW)
	ON_EN_CHANGE(IDC_Angle_Dis_H, &CRotate_Option::OnEnChangeAngleDisH)
	ON_EN_CHANGE(IDC_Angle_Width, &CRotate_Option::OnEnChangeAngleWidth)
	ON_EN_CHANGE(IDC_Angle_Height, &CRotate_Option::OnEnChangeAngleHeight)
	ON_WM_MOUSEWHEEL()
	ON_EN_CHANGE(IDC_EDIT_A_MOD, &CRotate_Option::OnEnChangeEditAMod)
	ON_EN_CHANGE(IDC_EDIT1, &CRotate_Option::OnEnChangeEdit1)
	ON_BN_CLICKED(IDC_BUTTON_test, &CRotate_Option::OnBnClickedButtontest)
	ON_BN_CLICKED(IDC_BUTTON_SAVE3, &CRotate_Option::OnBnClickedButtonSave3)
	ON_EN_CHANGE(IDC_EDIT_MASTER, &CRotate_Option::OnEnChangeEditMaster)
	ON_BN_CLICKED(IDC_CHECK_TYPE, &CRotate_Option::OnBnClickedCheckType)
	ON_EN_CHANGE(IDC_EDIT_T_MOD, &CRotate_Option::OnEnChangeEditTMod)
	ON_EN_KILLFOCUS(IDC_EDIT_T_MOD, &CRotate_Option::OnEnKillfocusEditTMod)
	ON_NOTIFY(NM_CLICK, IDC_LIST_TILT, &CRotate_Option::OnNMClickListTilt)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_TILT, &CRotate_Option::OnNMCustomdrawListTilt)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_TILT, &CRotate_Option::OnNMDblclkListTilt)
	ON_EN_CHANGE(IDC_ROTATION_ANGLE_OFFSET, &CRotate_Option::OnEnChangeRotationAngleOffset)
END_MESSAGE_MAP()


// CRotate_Option 메시지 처리기입니다.

void CRotate_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CRotate_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO      = INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);;
	strTitle.Empty();
	strTitle="ROTATE_INIT";	
}
void CModel_Create(CRotate_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO){
	CRect OptionRect;
	if(pTabWnd != NULL){
		((CRotate_Option *)*pWnd) = new CRotate_Option(pMomWnd);
		((CRotate_Option *)*pWnd)->Setup(pMomWnd,pEdit,INFO);
		((CRotate_Option *)*pWnd)->Create(((CRotate_Option *)*pWnd)->IDD,pTabWnd);//&m_CtrTab);	
		((CRotate_Option *)*pWnd)->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		((CRotate_Option *)*pWnd)->MoveWindow(5,40,OptionRect.Width(),OptionRect.Height());
	}
}

void CModel_Delete(CRotate_Option **pWnd){
	if(((CRotate_Option *)*pWnd) != NULL){
//		((CResolutionDetection *)*pWnd)->KillTimer(130);
		((CRotate_Option *)*pWnd)->DestroyWindow();
		delete ((CRotate_Option *)*pWnd);
		((CRotate_Option *)*pWnd) = NULL;	
	}
}


// CRotate_Option 메시지 처리기입니다.

BOOL CRotate_Option::OnInitDialog()
{
	CDialog::OnInitDialog();
	((CEdit *) GetDlgItem(IDC_EDIT_A_MOD))->ShowWindow(FALSE);
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	SETLIST();
	SETLIST_TILT();

	A_filename=((CImageTesterDlg  *)m_pMomWnd)->modelfile_Name;
	
	m_dRotationAngle = 5;

	Load_parameter();
	UpdateData(FALSE);

	UploadList();
	UploadList_Tilt();
//	SetTimer(110,100,NULL);
	Set_List(&m_AngleList);

//	LOT_Set_List(&m_Lot_RotateList);
//	UploadList();
	if(TestMod ==0){
		TestModALL=1;
		TestModHor=0;
		TestModVer=0;
	}
	else if(TestMod ==1){
		TestModALL=0;
		TestModHor=1;
		TestModVer=0;
	}
	else if(TestMod ==2){
		TestModALL=0;
		TestModHor=0;
		TestModVer=1;
	}

	UpdateData(FALSE);

	if(AutomationMod == 1){
		AutomationMod =0;	
		OnBnClickedCheckAuto();
		CheckDlgButton(IDC_CHECK_AUTO,TRUE);
	}

	m_fVer_Degree = 0;
	m_fHor_Degree = 0;		

	InitEVMS();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CRotate_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CButton *)GetDlgItem(IDC_BUTTON_MASTER_A))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_A_Load))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_PICSAVE))->EnableWindow(0);

		((CButton *)GetDlgItem(IDC_CHECK_CENTER))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_AUTO))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_RECT))->EnableWindow(0);

		((CEdit *)GetDlgItem(IDC_EDIT_MASTER))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Angle_PosX))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Angle_PosY))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Angle_Dis_W))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Angle_Dis_H))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Angle_Width))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Angle_Height))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Angle_Dis_W))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Angle_Dis_H))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_ROTATION_ANGLE))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_ROTATION_ANGLE2))->EnableWindow(0);
	}
}

void CRotate_Option::SETLIST(){
	A_DATALIST.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	A_DATALIST.InsertColumn(0,"Zone",LVCFMT_CENTER, 90);
	A_DATALIST.InsertColumn(1,"PosX",LVCFMT_CENTER, 40);
	A_DATALIST.InsertColumn(2,"PosY",LVCFMT_CENTER, 40);
	A_DATALIST.InsertColumn(3,"ST X",LVCFMT_CENTER, 40);
	A_DATALIST.InsertColumn(4,"ST Y",LVCFMT_CENTER, 40);
	A_DATALIST.InsertColumn(5,"EX X",LVCFMT_CENTER, 40);
	A_DATALIST.InsertColumn(6,"EX Y",LVCFMT_CENTER, 40);
	A_DATALIST.InsertColumn(7,"W",LVCFMT_CENTER, 40);
	A_DATALIST.InsertColumn(8,"H",LVCFMT_CENTER, 40);
	//A_DATALIST.InsertColumn(9,"Thresold",LVCFMT_CENTER, 60);
	//A_DATALIST.InsertColumn(10,"Distance",LVCFMT_CENTER, 60);

}
void CRotate_Option::SETLIST_TILT(){
	A_TILTLIST.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	A_TILTLIST.InsertColumn(0, "Zone", LVCFMT_CENTER, 70);
	A_TILTLIST.InsertColumn(1, "±Pixel합격편차", LVCFMT_CENTER,90);
	A_TILTLIST.InsertColumn(2, "Distance", LVCFMT_CENTER, 90);
}
void CRotate_Option::OnBnClickedButtonsetanglezone()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeCheck=1;
	b_SetZone = TRUE;
	UpdateData(TRUE);

	if(AutomationMod ==0){
		PosX=A_Total_PosX;
		PosY=A_Total_PosY;
		

		A_RECT[0].m_PosX = PosX-A_Dis_Width;
		A_RECT[0].m_PosY = PosY-A_Dis_Height;
		A_RECT[1].m_PosX = PosX+A_Dis_Width;
		A_RECT[1].m_PosY = PosY-A_Dis_Height;

		A_RECT[2].m_PosX = PosX-A_Dis_Width;
		A_RECT[2].m_PosY = PosY+A_Dis_Height;
		A_RECT[3].m_PosX = PosX+A_Dis_Width;
		A_RECT[3].m_PosY = PosY+A_Dis_Height;
	}

	for(int t=0; t<4; t++){
		A_RECT[t].m_Width= A_Total_Width;
		A_RECT[t].m_Height=A_Total_Height;
		A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);//////C
		//A_RECT[t].m_Thresold = A_Thresold;
	}



	UpdateData(FALSE);

	UploadList();//리스트컨트롤의 입력


	for(int t=0; t<4; t++){
		ChangeItem[t]=t;
	}

	for(int t=0; t<4; t++){
		A_DATALIST.Update(t);
	}

	for(int k=0; k<4; k++)
	{

		A_RECT[k].m_resultX = A_RECT[k].m_Left + A_RECT[k].m_Width/2;
		A_RECT[k].m_resultY = A_RECT[k].m_Top + A_RECT[k].m_Height/2;

	}

	//Save_parameter();

}

void CRotate_Option::UploadList(){
	CString str="";
	
	A_DATALIST.DeleteAllItems();

	for(int t=0; t<4; t++){
		InIndex = A_DATALIST.InsertItem(t,"초기",0);
		
 		
	
		if(t ==0){
			str.Empty();str="LEFT_TOP";
		}
		if(t ==1){
			str.Empty();str="RIGHT_TOP";
		}
		if(t ==2){
			str.Empty();str="LEFT_BOTTOM";
		}
		if(t ==3){
			str.Empty();str="RIGHT_BOTTOM";
		}	
		A_DATALIST.SetItemText(InIndex,0,str);
		str.Empty();str.Format("%d", A_RECT[t].m_PosX);
		A_DATALIST.SetItemText(InIndex,1,str);
		str.Empty();str.Format("%d", A_RECT[t].m_PosY);
		A_DATALIST.SetItemText(InIndex,2,str);
		
		str.Empty();
		str.Format("%d", A_RECT[t].m_Left);
		A_DATALIST.SetItemText(InIndex,3,str);
		str.Empty();
		str.Format("%d", A_RECT[t].m_Top);
		A_DATALIST.SetItemText(InIndex,4,str);
	
		str.Empty();
		str.Format("%d", A_RECT[t].m_Right+1);
		A_DATALIST.SetItemText(InIndex,5,str);
		str.Empty();
		str.Format("%d", A_RECT[t].m_Bottom+1);
		A_DATALIST.SetItemText(InIndex,6,str);

		str.Empty();
		str.Format("%d", A_RECT[t].m_Width);
		A_DATALIST.SetItemText(InIndex,7,str);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Height);
		A_DATALIST.SetItemText(InIndex,8,str);
		/*str.Empty();
		str.Format("%d",A_RECT[t].m_Thresold);
		A_DATALIST.SetItemText(InIndex,9,str);
		if(t ==0){
			str.Empty();
			str.Format("%d",A_RECT[t].m_resultDis);
			A_DATALIST.SetItemText(InIndex,10,str);
		}
		if(TestMod ==0 || TestMod ==1){
			if(t>0&& t<3){
			str.Empty();
			str.Format("%d",A_RECT[t].m_resultDis);
			A_DATALIST.SetItemText(InIndex,10,str);
			}
		}
		if(TestMod ==0 || TestMod ==2){
			if(t>2&& t<5){
			str.Empty();
			str.Format("%d",A_RECT[t].m_resultDis);
			A_DATALIST.SetItemText(InIndex,10,str);
			}
		}*/
	}
}
void CRotate_Option::UploadList_Tilt(){
	CString str = "";

	A_TILTLIST.DeleteAllItems();

	for (int t = 0; t<4; t++){
		InIndex = A_TILTLIST.InsertItem(t, "초기", 0);

		if (t == 0){
			str.Empty(); str = "LT _ RT";
		}
		if (t == 1){
			str.Empty(); str = "RT _ RB";
		}
		if (t == 2){
			str.Empty(); str = "RB _ LB";
		}
		if (t == 3){
			str.Empty(); str = "LB _ LT";
		}
		A_TILTLIST.SetItemText(InIndex, 0, str);
		str.Empty(); str.Format("%d", T_RECT[t].m_Thresold);
		A_TILTLIST.SetItemText(InIndex, 1, str);
		str.Empty(); str.Format("%d", (int)T_RECT[t].m_resultDis);
		A_TILTLIST.SetItemText(InIndex, 2, str);

	}
}
void CRotate_Option::Save_parameter(){


	CString str="";
	str.Empty();
	str.Format("%d",m_INFO.ID);
	WritePrivateProfileString(str_model,"ID",str,A_filename);
	str.Empty();
	str.Format("%s",m_INFO.MODE);
	WritePrivateProfileString(str_model,"MODE",str,A_filename);
	str.Empty();
	str.Format("%s",m_INFO.NAME);
	WritePrivateProfileString(str_model,"NAME",str,A_filename);


	strTitle.Empty();
	strTitle="ROTATE_INIT";	

	str.Empty();
	str.Format("%d",A_Total_PosX);
	WritePrivateProfileString(str_model,strTitle+"PosX",str,A_filename);
	str.Empty();
	str.Format("%d",A_Total_PosY);
	WritePrivateProfileString(str_model,strTitle+"PosY",str,A_filename);
	str.Empty();
	str.Format("%d",A_Dis_Width);
	WritePrivateProfileString(str_model,strTitle+"DisW",str,A_filename);
	str.Empty();
	str.Format("%d",A_Dis_Height);
	WritePrivateProfileString(str_model,strTitle+"DisH",str,A_filename);
	str.Empty();
	str.Format("%d",A_Total_Width);
	WritePrivateProfileString(str_model,strTitle+"Width",str,A_filename);
	str.Empty();
	str.Format("%d",A_Total_Height);
	WritePrivateProfileString(str_model,strTitle+"Height",str,A_filename);
	str.Empty();
	str.Format("%6.1f",MasterDegree);
	WritePrivateProfileString(str_model,strTitle+"MasterDegree",str,A_filename);
	str_MasterDegree.Format("%6.1f",MasterDegree);

	str_Angle_Offset.Format("%6.1f", m_dAngleOffset);

	WritePrivateProfileString(str_model, strTitle + "RotationAngle_NewOffset", str_Angle_Offset, A_filename);


	// 	str.Empty();
	// 	str.Format("%6.1f",m_dOffset);
	// 	WritePrivateProfileString(str_model,strTitle+"RotationAngle_OFFSET",str,A_filename);
	// 	str.Empty();
	// 	str.Format("%6.1f",m_dOffset_M);
	// 	WritePrivateProfileString(str_model,strTitle+"RotationAngle_OFFSET_M",str,A_filename);


	str.Format("%d",bOffsetType);
	WritePrivateProfileString(str_model,strTitle+"RotationAngle_OFFSET_TYPE",str,A_filename);


	/*str.Empty();
	str.Format("%d",TestMod);
	WritePrivateProfileString(str_model,strTitle+"TestMod",str,A_filename);*/
	//str.Empty();
	//str.Format("%d",m_dRotationAngle);
	//WritePrivateProfileString(str_model,strTitle+"RotationAngle",str,A_filename);


	str_minDegree.Format("%6.1f",m_dRotationAngle_min);
	str_maxDegree.Format("%6.1f",m_dRotationAngle_max);

	WritePrivateProfileString(str_model,strTitle+"RotationAngle_MIN",str_minDegree,A_filename);
	WritePrivateProfileString(str_model,strTitle+"RotationAngle_MAX",str_maxDegree,A_filename);

	str.Empty();
	str.Format("%d",b_CenterValueView);
	WritePrivateProfileString(str_model,strTitle+"CenterValueView",str,A_filename);
	str.Empty();
	str.Format("%d",b_DegreeValueView);
	WritePrivateProfileString(str_model,strTitle+"DegreeValueView",str,A_filename);
	str.Empty();
	str.Format("%d",b_RectView);
	WritePrivateProfileString(str_model,strTitle+"RectView",str,A_filename);

	if(AutomationMod ==1){
		WritePrivateProfileString(str_model,strTitle+"Auto","1",A_filename);}
	if(AutomationMod ==0){
		WritePrivateProfileString(str_model,strTitle+"Auto","0",A_filename);}
	for(int t=0; t<4; t++){
	

		if(t ==0){
			strTitle.Empty();
			strTitle="LEFT_UP_";		
		}
		if(t ==1){
			strTitle.Empty();
			strTitle="RIGHT_UP_";
		}
		if(t ==2){
			strTitle.Empty();
			strTitle="LEFT_BOTTOM_";		
		}
		if(t ==3){
			strTitle.Empty();
			strTitle="RIGHT_BOTTOM_";
		}
		str.Empty();
		str.Format("%d",A_RECT[t].m_PosX);
		A_Original[t].m_PosX=A_RECT[t].m_PosX;
		WritePrivateProfileString(str_model,strTitle+"PosX",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_PosY);
		A_Original[t].m_PosY=A_RECT[t].m_PosY;
		WritePrivateProfileString(str_model,strTitle+"PosY",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Left);
		WritePrivateProfileString(str_model,strTitle+"StartX",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Top);
		WritePrivateProfileString(str_model,strTitle+"StartY",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Right);
		WritePrivateProfileString(str_model,strTitle+"ExitX",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Bottom);
		WritePrivateProfileString(str_model,strTitle+"ExitY",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Width);
		WritePrivateProfileString(str_model,strTitle+"Width",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Height);
		WritePrivateProfileString(str_model,strTitle+"Height",str,A_filename);	

		if (t == 0){
			strTitle.Empty(); strTitle = "LT_RT";
		}
		if (t == 1){
			strTitle.Empty(); strTitle = "RT_RB";
		}
		if (t == 2){
			strTitle.Empty(); strTitle = "RB_LB";
		}
		if (t == 3){
			strTitle.Empty(); strTitle = "LB_LT";
		}
		str.Empty();
		str.Format("%d", T_RECT[t].m_Thresold);
		WritePrivateProfileString(str_model, strTitle + "Thresold", str, A_filename);
		str.Empty();
		str.Format("%d", (int)T_RECT[t].m_resultDis);
		WritePrivateProfileString(str_model, strTitle + "Distance", str, A_filename);
	}
}

void CRotate_Option::OnBnClickedButtonARectSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	b_SetZone = FALSE;

	MasterDegree = atof(str_MasterDegree);
	str_MasterDegree.Format("%6.1f",MasterDegree);
	Save_parameter();
	UpdateData(FALSE);


	ChangeCheck =0;
	ChangeCheck_T = 0;
	for(int t=0; t<4; t++){
		ChangeItem[t]=-1;
		ChangeItem_T[t] = -1;
	}
	for(int t=0; t<4; t++){
		A_DATALIST.Update(t);
		A_TILTLIST.Update(t);
	}
	for(int k=0; k<4; k++)
	{

		A_RECT[k].m_resultX = A_RECT[k].m_Left + A_RECT[k].m_Width/2;
		A_RECT[k].m_resultY = A_RECT[k].m_Top + A_RECT[k].m_Height/2;

	}

}

void CRotate_Option::Load_parameter(){
	CFileFind filefind;
	CString str="";
	CString EmptyStr="";

	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);


	if((GetPrivateProfileCString(str_model,"NAME",A_filename) != m_INFO.NAME)||(GetPrivateProfileCString(str_model,"MODE",A_filename) != m_INFO.MODE)){//ini파일이 없으면 파일을 생성한다.
		A_Total_PosX = Cam_PosX;
		A_Total_PosY = Cam_PosY;
		A_Dis_Width = 90;
		A_Dis_Height = 90;
		A_Total_Width = 80;
		A_Total_Height =80;
	

		A_RECT[0].m_PosX=Cam_PosX-A_Dis_Width;
		A_RECT[0].m_PosY=Cam_PosY-A_Dis_Height;
		A_RECT[0].m_Width=A_Total_Width;
		A_RECT[0].m_Height=A_Total_Height;

		A_RECT[1].m_PosX=Cam_PosX+A_Dis_Width;
		A_RECT[1].m_PosY=Cam_PosY-A_Dis_Height;
		A_RECT[1].m_Width=A_Total_Width;
		A_RECT[1].m_Height=A_Total_Height;

		A_RECT[2].m_PosX=Cam_PosX-A_Dis_Width;
		A_RECT[2].m_PosY=Cam_PosY+A_Dis_Height;
		A_RECT[2].m_Width=A_Total_Width;
		A_RECT[2].m_Height=A_Total_Height;

		A_RECT[3].m_PosX=Cam_PosX+A_Dis_Width;
		A_RECT[3].m_PosY=Cam_PosY+A_Dis_Height;
		A_RECT[3].m_Width=A_Total_Width;
		A_RECT[3].m_Height=A_Total_Height;
		//-----------------------------
		T_RECT[0].m_resultDis = A_Dis_Width * 2;
		T_RECT[1].m_resultDis = A_Dis_Height * 2;
		T_RECT[2].m_resultDis = A_Dis_Width * 2;
		T_RECT[3].m_resultDis = A_Dis_Height * 2;
		for (int t = 0; t < 4; t++){
			T_RECT[t].m_Thresold = 10;

		}
		//===============================
		A_Original[0].m_PosX=Cam_PosX-A_Dis_Width;
		A_Original[0].m_PosY=Cam_PosY-A_Dis_Height;
		A_Original[1].m_PosX=Cam_PosX+A_Dis_Width;
		A_Original[1].m_PosY=Cam_PosY-A_Dis_Height;
		A_Original[2].m_PosX=Cam_PosX-A_Dis_Width;;
		A_Original[2].m_PosY=Cam_PosY+A_Dis_Height;
		A_Original[3].m_PosX=Cam_PosX+A_Dis_Width;
		A_Original[3].m_PosY=Cam_PosY+A_Dis_Height;
		AutomationMod =0;

		bOffsetType = FALSE;
		for(int t=0; t<4; t++){
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);//////C
		
		}

		MasterDegree = 0;
		//TestMod =0;
		m_dAngleOffset = 1.0;

		//m_dRotationAngle = 5;
		b_CenterValueView =1;
		b_DegreeValueView =1;


		m_dRotationAngle_min = -3;
		m_dRotationAngle_max = 3;
		b_RectView =1;
		Save_parameter();
	}else{
		
		strTitle.Empty();
		strTitle="ROTATE_INIT";	
		A_Total_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,A_filename);
		if(A_Total_PosX == -1){
			A_Total_PosX = Cam_PosX;
			str.Empty();
			str.Format("%d",A_Total_PosX);
			WritePrivateProfileString(str_model,strTitle+"PosX",str,A_filename);
		}
		A_Total_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,A_filename);
		if(A_Total_PosY == -1){
			A_Total_PosY = Cam_PosY;
			str.Empty();
			str.Format("%d",A_Total_PosY);
			WritePrivateProfileString(str_model,strTitle+"PosY",str,A_filename);
		}
		A_Dis_Width=GetPrivateProfileInt(str_model,strTitle+"DisW",-1,A_filename);
		if(A_Dis_Width == -1){
			A_Dis_Width = 90;
			str.Empty();
			str.Format("%d",A_Dis_Width);
			WritePrivateProfileString(str_model,strTitle+"DisW",str,A_filename);
		}
		A_Dis_Height=GetPrivateProfileInt(str_model,strTitle+"DisH",-1,A_filename);
		if(A_Dis_Height == -1){
			A_Dis_Height = 90;
			str.Empty();
			str.Format("%d",A_Dis_Height);
			WritePrivateProfileString(str_model,strTitle+"DisH",str,A_filename);
		}
		A_Total_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,A_filename);
		if(A_Total_Width == -1){
			A_Total_Width = 80;
			str.Empty();
			str.Format("%d",A_Total_Width);
			WritePrivateProfileString(str_model,strTitle+"Width",str,A_filename);
		}
		A_Total_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,A_filename);
		if(A_Total_Height == -1){
			A_Total_Height =80;
			str.Empty();
			str.Format("%d",A_Total_Height);
			WritePrivateProfileString(str_model,strTitle+"Height",str,A_filename);
		}
		//A_Thresold=GetPrivateProfileInt(str_model,strTitle+"Thresold",-1,A_filename);
		/*TestMod = GetPrivateProfileInt(str_model,strTitle+"TestMod",0,A_filename);*/
		MasterDegree = GetPrivateProfileDouble(str_model,strTitle+"MasterDegree",0,A_filename);
		m_dRotationAngle_min = GetPrivateProfileDouble(str_model,strTitle+"RotationAngle_MIN",0,A_filename);
		m_dRotationAngle_max = GetPrivateProfileDouble(str_model,strTitle+"RotationAngle_MAX",0,A_filename);
		if(m_dRotationAngle_min >= m_dRotationAngle_max){
			m_dRotationAngle_min = m_dRotationAngle_max -1;
		}

		str_minDegree.Format("%6.1f",m_dRotationAngle_min);
		str_maxDegree.Format("%6.1f",m_dRotationAngle_max);
		
		WritePrivateProfileString(str_model,strTitle+"RotationAngle_MIN",str_minDegree,A_filename);
		WritePrivateProfileString(str_model,strTitle+"RotationAngle_MAX",str_maxDegree,A_filename);
		m_dAngleOffset = GetPrivateProfileDouble(str_model, strTitle + "RotationAngle_NewOffset", -999, A_filename);
		if (m_dAngleOffset == -999)
		{
			m_dAngleOffset = 1.0;
		}
		str_Angle_Offset.Format(_T("%6.1f"), m_dAngleOffset);
		WritePrivateProfileString(str_model, strTitle + "RotationAngle_NewOffset", str_Angle_Offset, A_filename);

		//	m_dOffset = GetPrivateProfileDouble(str_model,strTitle+"RotationAngle_OFFSET",0,A_filename);
		//	m_dOffset_M = GetPrivateProfileDouble(str_model,strTitle+"RotationAngle_OFFSET_M",0,A_filename);
		bOffsetType = GetPrivateProfileInt(str_model,strTitle+"RotationAngle_OFFSET_TYPE",0,A_filename);
		bOffsetType = FALSE;




		OFFSET.Format("%6.1f",m_dOffset);
		OFFSET_M.Format("%6.1f",m_dOffset_M);

		UpdateData(FALSE);
		strTitle.Empty();
		strTitle="ROTATE_INIT";	
		b_CenterValueView=GetPrivateProfileInt(str_model,strTitle+"CenterValueView",-1,A_filename);
		if(b_CenterValueView == -1){
			b_CenterValueView = 1;
			str.Empty();
			str.Format("%d",b_CenterValueView);
			WritePrivateProfileString(str_model,strTitle+"CenterValueView",str,A_filename);
		}
		b_DegreeValueView=GetPrivateProfileInt(str_model,strTitle+"DegreeValueView",-1,A_filename);
		if(b_DegreeValueView == -1){
			b_DegreeValueView = 1;
			str.Empty();
			str.Format("%d",b_DegreeValueView);
			WritePrivateProfileString(str_model,strTitle+"DegreeValueView",str,A_filename);
		}
		b_RectView=GetPrivateProfileInt(str_model,strTitle+"RectView",-1,A_filename);
		if(b_RectView == -1){
			b_RectView = 1;
			str.Empty();
			str.Format("%d",b_RectView);
			WritePrivateProfileString(str_model,strTitle+"RectView",str,A_filename);
		}

		b_CenterValueView = 1;
		b_DegreeValueView = 1;
		b_RectView = 1;

		CString Mod = GetPrivateProfileCString(str_model,strTitle+"Auto",A_filename);
		if("1" == Mod){
			AutomationMod = 1;
		}
		if("0" == Mod){
			AutomationMod = 0;
		}
		for(int t=0; t<4; t++){
		
			if(t ==0){
				strTitle.Empty();
				strTitle="LEFT_UP_";		
			}
			if(t ==1){
				strTitle.Empty();
				strTitle="RIGHT_UP_";
			}
			if(t ==2){
				strTitle.Empty();
				strTitle="LEFT_BOTTOM_";		
			}
			if(t ==3){
				strTitle.Empty();
				strTitle="RIGHT_BOTTOM_";
			}	
			A_RECT[t].m_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,A_filename);
			if(A_RECT[t].m_PosX == -1){
				
			

				A_RECT[0].m_PosX=Cam_PosX-A_Dis_Width;
				A_RECT[0].m_PosY=Cam_PosY-A_Dis_Height;
				A_RECT[0].m_Width=A_Total_Width;
				A_RECT[0].m_Height=A_Total_Height;

				A_RECT[1].m_PosX=Cam_PosX+A_Dis_Width;
				A_RECT[1].m_PosY=Cam_PosY-A_Dis_Height;
				A_RECT[1].m_Width=A_Total_Width;
				A_RECT[1].m_Height=A_Total_Height;

				A_RECT[2].m_PosX=Cam_PosX-A_Dis_Width;
				A_RECT[2].m_PosY=Cam_PosY+A_Dis_Height;
				A_RECT[2].m_Width=A_Total_Width;
				A_RECT[2].m_Height=A_Total_Height;

				A_RECT[3].m_PosX=Cam_PosX+A_Dis_Width;
				A_RECT[3].m_PosY=Cam_PosY+A_Dis_Height;
				A_RECT[3].m_Width=A_Total_Width;
				A_RECT[3].m_Height=A_Total_Height;
				//-----------------------------
				A_Original[0].m_PosX=Cam_PosX-A_Dis_Width;
				A_Original[0].m_PosY=Cam_PosY-A_Dis_Height;
				A_Original[1].m_PosX=Cam_PosX+A_Dis_Width;
				A_Original[1].m_PosY=Cam_PosY-A_Dis_Height;
				A_Original[2].m_PosX=Cam_PosX-A_Dis_Width;;
				A_Original[2].m_PosY=Cam_PosY+A_Dis_Height;
				A_Original[3].m_PosX=Cam_PosX+A_Dis_Width;
				A_Original[3].m_PosY=Cam_PosY+A_Dis_Height;
				AutomationMod =0;
				for(int t=0; t<4; t++){
					A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);//////C
					//A_RECT[t].m_Thresold = 10;
				
				}

				
				Save_parameter();
			}
			A_Original[t].m_PosX=A_RECT[t].m_PosX;
			A_RECT[t].m_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,A_filename);
			A_Original[t].m_PosY=A_RECT[t].m_PosY;
			A_RECT[t].m_Left=GetPrivateProfileInt(str_model,strTitle+"StartX",-1,A_filename);
			A_RECT[t].m_Top=GetPrivateProfileInt(str_model,strTitle+"StartY",-1,A_filename);
			A_RECT[t].m_Right=GetPrivateProfileInt(str_model,strTitle+"ExitX",-1,A_filename);
			A_RECT[t].m_Bottom=GetPrivateProfileInt(str_model,strTitle+"ExitY",-1,A_filename);
			A_RECT[t].m_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,A_filename);
			A_RECT[t].m_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,A_filename);

			Standard_SC_RECT[t].m_resultX = A_RECT[t].m_Left + A_RECT[t].m_Width/2; 
			Standard_SC_RECT[t].m_resultY = A_RECT[t].m_Top + A_RECT[t].m_Height/2;
			
			//A_RECT[t].m_Thresold=GetPrivateProfileInt(str_model,strTitle+"Thresold",-1,A_filename);
			//A_RECT[t].m_resultDis=GetPrivateProfileInt(str_model,strTitle+"Distance",-1,A_filename);
		}
		for (int t = 0; t < 4; t++){

			if (t == 0){
				strTitle.Empty(); strTitle = "LT_RT";
			}
			if (t == 1){
				strTitle.Empty(); strTitle = "RT_RB";
			}
			if (t == 2){
				strTitle.Empty(); strTitle = "RB_LB";
			}
			if (t == 3){
				strTitle.Empty(); strTitle = "LB_LT";
			}

			T_RECT[t].m_Thresold = GetPrivateProfileInt(str_model, strTitle + "Thresold", -1, A_filename);
			if (T_RECT[t].m_Thresold == -1){

				T_RECT[0].m_resultDis = A_Dis_Width * 2;
				T_RECT[1].m_resultDis = A_Dis_Height * 2;
				T_RECT[2].m_resultDis = A_Dis_Width * 2;
				T_RECT[3].m_resultDis = A_Dis_Height * 2;
				for (int t = 0; t < 4; t++){
					T_RECT[t].m_Thresold = 10;

				}

				Save_parameter();
			}
			T_RECT[t].m_resultDis = GetPrivateProfileInt(str_model, strTitle + "Distance", -1, A_filename);

		}
		
	}


	str_MasterDegree.Format("%6.1f",MasterDegree);
	str_Angle_Offset.Format(_T("%6.1f"), m_dAngleOffset);

	for(int k=0; k<4; k++)
	{

		A_RECT[k].m_resultX = A_RECT[k].m_Left + A_RECT[k].m_Width/2;
		A_RECT[k].m_resultY = A_RECT[k].m_Top + A_RECT[k].m_Height/2;

	}

	UpdateData(FALSE);
}
void CRotate_Option::OnNMDblclkListAngle(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		return;
	}
	
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
		//ChangeCheck=1;
	if(pNMITEM->iSubItem == 0 || pNMITEM->iSubItem == -1 || pNMITEM->iItem == -1 )
		return ;
		iSavedItem = pNMITEM->iItem;
		iSavedSubitem = pNMITEM->iSubItem;
	

	if(pNMITEM->iItem != -1)
	{
		if(pNMITEM->iSubItem != 0)
		{		
			A_DATALIST.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			A_DATALIST.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText(A_DATALIST.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetFocus();
			
		}
	}
	//List_COLOR_Change();
	
	*pResult = 0;
}
void CRotate_Option::List_COLOR_Change(){
	bool FLAG = TRUE;
	int Check=0;


	for(int t=0;t<4; t++){
		if(ChangeItem[t]==0)Check++;
		if(ChangeItem[t]==1)Check++;
		if(ChangeItem[t]==2)Check++;
		if(ChangeItem[t]==3)Check++;
	}
 	if(Check !=4){
		FLAG =FALSE;
		ChangeItem[changecount]=iSavedItem;
	}

	if(!FLAG){
		for(int t=0; t<changecount+1; t++){
			for(int k=0; k<changecount+1; k++){
				
				if(ChangeItem[t] == ChangeItem[k]){
					if(t==k){break;	}			
					ChangeItem[k] =-1;
				}
			
			
			}
		}
		//----------------데이터 정렬
		int temp=0;

		for(int i=0; i<4; i++)
		{
			for(int j=i+1; j<4; j++)
			{
				if(ChangeItem[i] < ChangeItem[j])  // 내림차순
				{
					temp = ChangeItem[i];
					ChangeItem[i] = ChangeItem[j];
					ChangeItem[j] = temp;
				}
			}
		}
		//--------------------------------
		for(int t=0; t<4; t++){

			if(ChangeItem[t] ==-1){
				changecount =t;
				break;
			}
		}
	}
	for(int t=0; t<4; t++){
		A_DATALIST.Update(t);
	}

}
void CRotate_Option::List_COLOR_Change_TILT(){
	bool FLAG = TRUE;
	int Check = 0;


	for (int t = 0; t < 4; t++){
		if (ChangeItem_T[t] == 0)Check++;
		if (ChangeItem_T[t] == 1)Check++;
		if (ChangeItem_T[t] == 2)Check++;
		if (ChangeItem_T[t] == 3)Check++;
	}
	if (Check != 4){
		FLAG = FALSE;
		ChangeItem_T[changecount_T] = iSavedItem_T;
	}

	if (!FLAG){
		for (int t = 0; t < changecount_T + 1; t++){
			for (int k = 0; k < changecount_T + 1; k++){

				if (ChangeItem_T[t] == ChangeItem_T[k]){
					if (t == k){ break; }
					ChangeItem_T[k] = -1;
				}


			}
		}
		//----------------데이터 정렬
		int temp = 0;

		for (int i = 0; i < 4; i++)
		{
			for (int j = i + 1; j < 4; j++)
			{
				if (ChangeItem_T[i] < ChangeItem_T[j])  // 내림차순
				{
					temp = ChangeItem_T[i];
					ChangeItem_T[i] = ChangeItem_T[j];
					ChangeItem_T[j] = temp;
				}
			}
		}
		//--------------------------------
		for (int t = 0; t < 4; t++){

			if (ChangeItem_T[t] == -1){
				changecount_T = t;
				break;
			}
		}
	}
	for (int t = 0; t < 4; t++){
		A_TILTLIST.Update(t);
	}

}
void CRotate_Option::OnNMClickListAngle(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.


	*pResult = 0;
}

void CRotate_Option::Change_DATA(){

		CString str;
		
		((CEdit *)GetDlgItemText(IDC_EDIT_A_MOD, str));
		A_DATALIST.SetItemText(iSavedItem, iSavedSubitem, str);

		int num = _ttoi(str);

		if(num < 0){
			num =0;
		}

		if(iSavedSubitem ==1){
			A_RECT[iSavedItem].m_PosX = num;
			
		}
		if(iSavedSubitem ==2){
			A_RECT[iSavedItem].m_PosY = num;
			
		}
		if(iSavedSubitem ==3){
			A_RECT[iSavedItem].m_Left = num;
			A_RECT[iSavedItem].m_PosX=(A_RECT[iSavedItem].m_Right+1 +A_RECT[iSavedItem].m_Left) /2;
			A_RECT[iSavedItem].m_Width=A_RECT[iSavedItem].m_Right+1 -A_RECT[iSavedItem].m_Left;

		}
		if(iSavedSubitem ==4){
			A_RECT[iSavedItem].m_Top = num;
			A_RECT[iSavedItem].m_PosY=(A_RECT[iSavedItem].m_Top +A_RECT[iSavedItem].m_Bottom+1)/2;
			A_RECT[iSavedItem].m_Height=A_RECT[iSavedItem].m_Bottom+1 -A_RECT[iSavedItem].m_Top;

		}
		if(iSavedSubitem ==5){
			if(num == 0){
				A_RECT[iSavedItem].m_Right = 0;
			}
			else{
				A_RECT[iSavedItem].m_Right = num;
				A_RECT[iSavedItem].m_PosX=(A_RECT[iSavedItem].m_Right +A_RECT[iSavedItem].m_Left+1) /2;
				A_RECT[iSavedItem].m_Width=A_RECT[iSavedItem].m_Right+1 -A_RECT[iSavedItem].m_Left;
			}
		}
		if(iSavedSubitem ==6){
			if(num ==0){
				A_RECT[iSavedItem].m_Bottom =0;
			}
			else{
				A_RECT[iSavedItem].m_Bottom = num;
				A_RECT[iSavedItem].m_PosY=(A_RECT[iSavedItem].m_Top +A_RECT[iSavedItem].m_Bottom+1)/2;
				A_RECT[iSavedItem].m_Height=A_RECT[iSavedItem].m_Bottom+1 -A_RECT[iSavedItem].m_Top;
			}
		}
		if(iSavedSubitem ==7){
			A_RECT[iSavedItem].m_Width = num;

		}
		if(iSavedSubitem ==8){
			A_RECT[iSavedItem].m_Height = num;
		}
		
		A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
			
		if(A_RECT[iSavedItem].m_Left < 0){//////////////////////수정
			A_RECT[iSavedItem].m_Left = 0;
			A_RECT[iSavedItem].m_Width = A_RECT[iSavedItem].m_Right+1 -A_RECT[iSavedItem].m_Left;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if(A_RECT[iSavedItem].m_Right >CAM_IMAGE_WIDTH){
			A_RECT[iSavedItem].m_Right = CAM_IMAGE_WIDTH;
			A_RECT[iSavedItem].m_Width = A_RECT[iSavedItem].m_Right+1 -A_RECT[iSavedItem].m_Left;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if(A_RECT[iSavedItem].m_Top< 0){
			A_RECT[iSavedItem].m_Top = 0;
			A_RECT[iSavedItem].m_Height = A_RECT[iSavedItem].m_Bottom+1 -A_RECT[iSavedItem].m_Top;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if(A_RECT[iSavedItem].m_Bottom >CAM_IMAGE_HEIGHT){
			A_RECT[iSavedItem].m_Bottom = CAM_IMAGE_HEIGHT;
			A_RECT[iSavedItem].m_Height = A_RECT[iSavedItem].m_Bottom+1 -A_RECT[iSavedItem].m_Top;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}

		if(A_RECT[iSavedItem].m_Height<= 0){
			A_RECT[iSavedItem].m_Height = 1;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if(A_RECT[iSavedItem].m_Height >=CAM_IMAGE_HEIGHT){
			A_RECT[iSavedItem].m_Height = CAM_IMAGE_HEIGHT;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}

		if(A_RECT[iSavedItem].m_Width <= 0){
			A_RECT[iSavedItem].m_Width = 1;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if(A_RECT[iSavedItem].m_Width >=CAM_IMAGE_WIDTH){
			A_RECT[iSavedItem].m_Width = CAM_IMAGE_WIDTH;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}



		A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);

	

		CString data = "";

		if(iSavedSubitem ==1){
			data.Format("%d",A_RECT[iSavedItem].m_PosX);			
		}
		else if(iSavedSubitem ==2){
			data.Format("%d",A_RECT[iSavedItem].m_PosY);
		}
		else if(iSavedSubitem ==3){
			data.Format("%d",A_RECT[iSavedItem].m_Left);			
		}
		else if(iSavedSubitem ==4){
			data.Format("%d",A_RECT[iSavedItem].m_Top);			
		}
		else if(iSavedSubitem ==5){
			data.Format("%d",A_RECT[iSavedItem].m_Right);			
		}
		else if(iSavedSubitem ==6){
			data.Format("%d",A_RECT[iSavedItem].m_Bottom);			
		}
		else if(iSavedSubitem ==7){
			data.Format("%d",A_RECT[iSavedItem].m_Width);			
		}
		else if(iSavedSubitem ==8){
			data.Format("%d",A_RECT[iSavedItem].m_Height);			
		}		
		//((CEdit *)SetDlgItemText(IDC_EDIT_BRIGHT, data));
		((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText(data);

}

void CRotate_Option::Change_DATA_TILT(){

	CString str;

	((CEdit *)GetDlgItemText(IDC_EDIT_T_MOD, str));
	A_TILTLIST.SetItemText(iSavedItem_T, iSavedSubitem_T, str);

	int num = _ttoi(str);

	if (num < 0){
		num = 0;
	}

	if (iSavedSubitem_T == 1){
		T_RECT[iSavedItem_T].m_Thresold = num;

	}
	if (iSavedSubitem_T == 2){
		T_RECT[iSavedItem_T].m_resultDis = num;

	}

	CString data = "";

	if (iSavedSubitem_T == 1){
		data.Format("%d", T_RECT[iSavedItem_T].m_Thresold);
	}
	else if (iSavedSubitem_T == 2){
		data.Format("%d", (int)T_RECT[iSavedItem_T].m_resultDis);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_T_MOD))->SetWindowText(data);

}
BOOL CRotate_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
			if(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->GetSafeHwnd())//GetSafeHwnd() 자신의 핸들을 가져오는 함수
			{
				//ChangeCheck =1;
				int bufSubitem = iSavedSubitem + 1;
				
				if(bufSubitem ==11){
					if(iSavedItem == 0){
						iSavedItem = 1; bufSubitem =1;
					}
					else if(iSavedItem == 1){
						iSavedItem = 2; bufSubitem =1;
					}
					else if(iSavedItem == 2){
						iSavedItem = 3; bufSubitem =1;
					}
					else if(iSavedItem == 3){
						iSavedItem = 0; bufSubitem =1;
					}
					
				}
				int bufItem = iSavedItem;
				A_DATALIST.EnsureVisible(iSavedItem, FALSE);
				A_DATALIST.GetSubItemRect(iSavedItem,bufSubitem , LVIR_BOUNDS, rect);
				A_DATALIST.ClientToScreen(rect);
				this->ScreenToClient(rect);
				A_DATALIST.SetSelectionMark(iSavedItem);
				A_DATALIST.SetFocus(); //저장
				iSavedItem = bufItem;
				iSavedSubitem = bufSubitem;
				((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText(A_DATALIST.GetItemText(iSavedItem, iSavedSubitem));
				((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
				((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetFocus();
			}
			
			if ((pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_PosX))->GetSafeHwnd())||  
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_PosY))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_Dis_W))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_Dis_H))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_Width))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_Height))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_Thresold))->GetSafeHwnd()))
			{	
				OnBnClickedButtonsetanglezone();
			}

			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		/*if(pMsg->wParam == VK_ADD){
			((CImageTesterDlg  *)m_pMomWnd)->ExBtnStart();
			return TRUE;
		}
		if (pMsg->wParam == VK_SUBTRACT){
			((CImageTesterDlg  *)m_pMomWnd)->ExBtnStop();
			return TRUE;
		}
		if(pMsg->wParam == VK_PRIOR&& ((CImageTesterDlg  *)m_pMomWnd)->b_UserMode ==TRUE){
			((CFINAL_TESTDlg *)m_pMomWnd)->JIG_UP();
			
			return TRUE;
		}
		if (pMsg->wParam == VK_NEXT&& ((CImageTesterDlg  *)m_pMomWnd)->b_UserMode ==TRUE){
			((CFINAL_TESTDlg *)m_pMomWnd)->JIG_DOWN();
			return TRUE;
		}
		if(pMsg->wParam == VK_MULTIPLY&& ((CImageTesterDlg  *)m_pMomWnd)->b_UserMode ==TRUE){
			AfxMessageBox("해당 프로그램에서는 [ JIG IN ] 기능을 지원하지 않습니다.");
			return TRUE;
		}
		if(pMsg->wParam == VK_DIVIDE&& ((CImageTesterDlg  *)m_pMomWnd)->b_UserMode ==TRUE){
			AfxMessageBox("해당 프로그램에서는 [ JIG OUT ] 기능을 지원하지 않습니다.");
			return TRUE;
		}*/
	}

	return CDialog::PreTranslateMessage(pMsg);
}

//void CRotate_Option::OnTimer(UINT_PTR nIDEvent)
//{
//	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
//
//	CDialog::OnTimer(nIDEvent);
//	switch(nIDEvent)	
//	{
//	case 110:
//		if(EnterState==1)
//		{
//	        //int nCount = C_DATALIST.GetItemCount();
//			//C_DATALIST.EnsureVisible(nCount, TRUE); 
//			if(iSavedSubitem2 ==11){
//				if(iSavedItem == 0){
//					iSavedItem = 1; iSavedSubitem2 =1;
//				}
//				else if(iSavedItem == 1){
//					iSavedItem = 2; iSavedSubitem2 =1;
//				}
//				else if(iSavedItem == 2){
//					iSavedItem = 3; iSavedSubitem2 =1;
//				}
//				else if(iSavedItem == 3){
//					iSavedItem = 4; iSavedSubitem2 =1;
//				}
//				else if(iSavedItem == 4){
//					iSavedItem = 0; iSavedSubitem2 =1;
//				}
//				
//				
//			}
//			
//			A_DATALIST.GetSubItemRect(iSavedItem,iSavedSubitem2 , LVIR_BOUNDS, rect);
//			A_DATALIST.ClientToScreen(rect);
//			this->ScreenToClient(rect);
//
//			A_DATALIST.SetSelectionMark(iSavedItem);
//			A_DATALIST.SetFocus();
//			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText(A_DATALIST.GetItemText(iSavedItem, iSavedSubitem2));
//			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
//			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetFocus();
//
//			iSavedSubitem=iSavedSubitem2;
//
//			EnterState=0;
//			
//		}
//
//	break;
//
//	}
//}


bool CRotate_Option::AngleGen(LPBYTE IN_RGB,int NUM){
	int	dwk = 0,dwi = 0;
		DWORD	RGBPIX = 0,RGBLINE = 0;
		DWORD 	SumX = 0,SumY = 0;
		DWORD	N1 = 0;
		BYTE *BW;
		DWORD Total = A_RECT[NUM].Height()*A_RECT[NUM].Length();
		BW = new BYTE[Total];
		memset(BW,0,sizeof(BW));
			int index=0;
		A_RECT[NUM].m_Success =FALSE;

		int offsetX1 = A_RECT[NUM].m_Left;
		int offsetX2 = A_RECT[NUM].m_Right+1;
		int offsetY1 = A_RECT[NUM].m_Top;
		int offsetY2 = A_RECT[NUM].m_Bottom+1;

		unsigned int PostionX[30000]={0,};
		unsigned int PostionY[30000]={0,};

		//if(m_EN_RUN != 1){
		//	m_Success = FALSE;
		//	return	FALSE;
		//}
		
		for (dwk=0; dwk<CAM_IMAGE_HEIGHT; dwk++)
		{	
			for (dwi=0; dwi<CAM_IMAGE_WIDTH; dwi++)
			{
				m_InB = IN_RGB[RGBLINE + RGBPIX];
				m_InG = IN_RGB[RGBLINE + RGBPIX + 1];
				m_InR = IN_RGB[RGBLINE + RGBPIX + 2];
				
				

				if(dwi>offsetX1 && dwi<offsetX2){
					if(dwk >offsetY1 &&dwk<offsetY2){
					//	BW[index] = (0.29900*m_InB)+(0.58700*m_InG)+(0.11400*m_InR);
						
						if(NUM !=0){
							if(m_InR > m_InB + m_InG)
								BW[index] = 0;
							else
								BW[index] = 255;

							if(BW[index]<50 ){
							//if(m_InR<50 && m_InG<50 && m_InB<50 ){ //&& RED1
								PostionX[N1]= dwi; PostionY[N1]=dwk;
								SumX += (double)PostionX[N1]; SumY += (double)PostionY[N1];
								N1 ++;
							}
							index++;
						}
						else{
							if(m_InR <50 && m_InB<50 && m_InG <50){
								BW[index] = 0;
							}
							else{
								BW[index] = 255;
							}
							if(BW[index]<50 ){
							//if(m_InR<50 && m_InG<50 && m_InB<50 ){ //&& RED1
								PostionX[N1]= dwi; PostionY[N1]=dwk;
								SumX += (double)PostionX[N1]; SumY += (double)PostionY[N1];
								N1 ++;
							}
							index++;

						}
					}
				}
				RGBPIX += 4;
			}
			RGBLINE +=  (m_CAM_SIZE_WIDTH*4);
			RGBPIX = 0;
		}
		if(N1 > 5){

			A_RECT[NUM].m_resultX = (int)((SumX/N1)+0.5);//반올림
			A_RECT[NUM].m_resultY = (int)((SumY/N1)+0.5);//반올림
		}

		delete BW;
	return A_RECT[NUM].m_Success;

}

void CRotate_Option::AngleSum(){//AVE값 및 평균 RGB값 추출 및 그리기.
	A_RECT[0].m_resultDis=0;
	
	/*if(MasterMod == TRUE){
		A_RECT[1].m_resultDis=int(sqrt(pow(A_RECT[0].m_resultX-A_RECT[1].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[1].m_resultY,2)));
		A_RECT[2].m_resultDis=int(sqrt(pow(A_RECT[0].m_resultX-A_RECT[2].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[2].m_resultY,2)));

		A_RECT[3].m_resultDis=int(sqrt(pow(A_RECT[0].m_resultX-A_RECT[3].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[3].m_resultY,2)));
		A_RECT[4].m_resultDis=int(sqrt(pow(A_RECT[0].m_resultX-A_RECT[4].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[4].m_resultY,2)));
	}
	else{
		disL=sqrt(pow(A_RECT[0].m_resultX-A_RECT[1].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[1].m_resultY,2));
		disR=sqrt(pow(A_RECT[0].m_resultX-A_RECT[2].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[2].m_resultY,2));	
		disT=sqrt(pow(A_RECT[0].m_resultX-A_RECT[3].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[3].m_resultY,2));
		disB=sqrt(pow(A_RECT[0].m_resultX-A_RECT[4].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[4].m_resultY,2));	


		m_Comp_L_MAX = A_RECT[1].m_resultDis +A_RECT[1].m_Thresold;
		m_Comp_R_MAX= A_RECT[2].m_resultDis +A_RECT[2].m_Thresold;
		m_Comp_L_MIN= A_RECT[1].m_resultDis -A_RECT[1].m_Thresold;
		m_Comp_R_MIN= A_RECT[2].m_resultDis -A_RECT[2].m_Thresold;

		m_Comp_T_MAX = A_RECT[3].m_resultDis +A_RECT[3].m_Thresold;
		m_Comp_B_MAX= A_RECT[4].m_resultDis +A_RECT[4].m_Thresold;
		m_Comp_T_MIN= A_RECT[3].m_resultDis -A_RECT[3].m_Thresold;
		m_Comp_B_MIN= A_RECT[4].m_resultDis -A_RECT[4].m_Thresold;

		if(TestMod == 0 || TestMod == 1){
			CString str="";
			str.Empty();
			str.Format("%6.1f",disL);
			m_AngleList.SetItemText(InsertIndex,5,str);
			str.Empty();
			str.Format("%6.1f",disR);
			m_AngleList.SetItemText(InsertIndex,6,str);
		}
	
		if(TestMod == 0 || TestMod == 2){
			CString str="";
			str.Empty();
			str.Format("%6.1f",disT);
			m_AngleList.SetItemText(InsertIndex,7,str);
			str.Empty();
			str.Format("%6.1f",disB);
			m_AngleList.SetItemText(InsertIndex,8,str);

		}
		for(int NUM =0; NUM<5; NUM++){
			
			if(TestMod == 0 || TestMod == 1){
				if(NUM<3){
					if(disL>= m_Comp_L_MIN&&disL <=m_Comp_L_MAX){
						if(disR >= m_Comp_R_MIN&&disR <=m_Comp_R_MAX){
							A_RECT[NUM].m_Success = TRUE;
						
						}
					}
					A_RECT[0].m_Success = TRUE;
				}
			}
			if(TestMod == 0 || TestMod == 2){
				if(NUM>=3){
					if(disT>= m_Comp_T_MIN&&disT <=m_Comp_T_MAX){
						if(disB >= m_Comp_B_MIN&&disB <=m_Comp_B_MAX){
							A_RECT[NUM].m_Success = TRUE;
						
						}
					}
					A_RECT[0].m_Success = TRUE;
				}
			}
		}

	}*/
	
}/*

int CRotate_Option::Distance_Sum(int x1, int y1, int x2, int y2){

	int Sum=0;
	int X_result=0;
	int Y_result=0;
	X_result =int(sqrt(pow(x2-x,2)+pow(A_RECT[0].m_resultX-A_RECT[1].m_resultX,2)));

	return Sum;
}
*/
bool CRotate_Option::AnglePic(CDC *cdc,int NUM){//AVE값 및 평균 RGB값 추출 및 그리기.
	
	CPen	my_Pan,my_Pan2,*old_pan,*old_pan2; 
	
	
	CPen my_Pen3, my_Pen4, my_Pen5, *old_pan3, *old_pan4;
	
	CString RESULTDATA ="";
	CString TEXTDATA ="";
	CString strDegree="";
	
	CFont font, *old_font;
	//if(A_RECT[NUM].Chkdata() == FALSE){return 0;}

	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	if(A_RECT[NUM].m_Success == TRUE){
		my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(BLUE_COLOR);
	}else{
		my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(RED_COLOR);
	}
	if(b_RectView == TRUE){
		cdc->MoveTo(A_RECT[NUM].m_Left,A_RECT[NUM].m_Top);
		cdc->LineTo(A_RECT[NUM].m_Right,A_RECT[NUM].m_Top);
		cdc->LineTo(A_RECT[NUM].m_Right,A_RECT[NUM].m_Bottom);
		cdc->LineTo(A_RECT[NUM].m_Left,A_RECT[NUM].m_Bottom);
		cdc->LineTo(A_RECT[NUM].m_Left,A_RECT[NUM].m_Top);
	}
	cdc->SelectObject(old_pan);
	old_pan->DeleteObject();
	my_Pan.DeleteObject();

	my_Pan2.CreatePen(PS_SOLID,2,WHITE_COLOR);
	old_pan2 = cdc->SelectObject(&my_Pan2);

	if(b_SetZone == TRUE){
	cdc->MoveTo(A_RECT[0].m_resultX,A_RECT[0].m_resultY);
	cdc->LineTo(A_RECT[1].m_resultX,A_RECT[1].m_resultY);
	cdc->LineTo(A_RECT[3].m_resultX,A_RECT[3].m_resultY);
	cdc->LineTo(A_RECT[2].m_resultX,A_RECT[2].m_resultY);
	cdc->LineTo(A_RECT[0].m_resultX,A_RECT[0].m_resultY);
	}

	cdc->SelectObject(old_pan2);
	old_pan2->DeleteObject();
	my_Pan2.DeleteObject();



	font.CreatePointFont(120, "Arial"); 
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);
	old_font = cdc->SelectObject(&font);
	if(b_CenterValueView == TRUE){
		TEXTDATA.Format("X:%6.1f Y:%6.1f",SC_RECT[NUM].m_resultX,SC_RECT[NUM].m_resultY);
	}else{
		TEXTDATA="";
	}

	cdc->TextOut(A_RECT[NUM].m_PosX,A_RECT[NUM].m_Top - 10,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	my_Pen4.CreatePen(PS_SOLID,2,GREEN_COLOR);
	old_pan4 =cdc->SelectObject(&my_Pen4);
	cdc->MoveTo(SC_RECT[0].m_resultX,SC_RECT[0].m_resultY);
	cdc->LineTo(SC_RECT[1].m_resultX,SC_RECT[1].m_resultY);
	cdc->LineTo(SC_RECT[3].m_resultX,SC_RECT[3].m_resultY);
	cdc->LineTo(SC_RECT[2].m_resultX,SC_RECT[2].m_resultY);
	cdc->LineTo(SC_RECT[0].m_resultX,SC_RECT[0].m_resultY);

	cdc->SelectObject(old_font);
	cdc->SelectObject(old_pan4);
	my_Pen4.DeleteObject();
	font.DeleteObject();

	::SetTextColor(cdc->m_hDC,RGB(255, 255, 0)); 
	
	font.CreatePointFont(150, "Arial"); 
	old_font = cdc->SelectObject(&font);

	if(m_bRotationCheck)
	{
		my_Pen3.CreatePen(PS_SOLID,2, RGB(0, 0, 255));
		old_pan3 = cdc->SelectObject(&my_Pen3);
	}
	else
	{
		my_Pen3.CreatePen(PS_SOLID,2, RGB(255, 0, 0));
		old_pan3 = cdc->SelectObject(&my_Pen3);
	}
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);
	

	strDegree.Empty();
		
	strDegree.Format("%6.3f˚", ResultDegree);
	cdc->TextOut(CAM_IMAGE_WIDTH/2,CAM_IMAGE_HEIGHT-100,strDegree.GetBuffer(0),strDegree.GetLength());

	if (m_bTiltCheck)
	{
		::SetTextColor(cdc->m_hDC, RGB(0, 0, 255));

	}
	else
	{
		::SetTextColor(cdc->m_hDC, RGB(255, 0, 0));

	}
	cdc->SetTextAlign(TA_CENTER | TA_BASELINE);

	int Width,Height;
	if (NUM == 0){
		Width = (SC_RECT[0].m_resultX + SC_RECT[1].m_resultX) / 2;
		Height = (SC_RECT[0].m_resultY + SC_RECT[1].m_resultY) / 2;
	}else if (NUM == 1){
		Width = (SC_RECT[1].m_resultX + SC_RECT[3].m_resultX) / 2;
		Height =( SC_RECT[1].m_resultY + SC_RECT[3].m_resultY) / 2;
	}else if (NUM == 2){
		Width = (SC_RECT[2].m_resultX + SC_RECT[3].m_resultX) / 2;
		Height = (SC_RECT[2].m_resultY + SC_RECT[3].m_resultY) / 2;
	}else if (NUM == 3){
		Width = (SC_RECT[0].m_resultX + SC_RECT[2].m_resultX) / 2;
		Height = (SC_RECT[0].m_resultY + SC_RECT[2].m_resultY) / 2;
	}
	TEXTDATA.Format("%d", (int)A_RECT[NUM].m_resultDis);
	//cdc->TextOut(Width, Height, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());

	cdc->MoveTo(SC_RECT[0].m_resultX, SC_RECT[0].m_resultY);
	cdc->LineTo(SC_RECT[1].m_resultX, SC_RECT[1].m_resultY);
	cdc->LineTo(SC_RECT[3].m_resultX, SC_RECT[3].m_resultY);
	cdc->LineTo(SC_RECT[2].m_resultX, SC_RECT[2].m_resultY);
	cdc->LineTo(SC_RECT[0].m_resultX, SC_RECT[0].m_resultY);

	cdc->SelectObject(old_pan3);
	cdc->SelectObject(old_font);

	old_pan3->DeleteObject();
	old_font->DeleteObject();
	my_Pen3.DeleteObject();
	font.DeleteObject();
//******************************************************************************************************************


	return TRUE;
}

double CRotate_Option::Vertical_GetRotationCheck(double pt_x1, double pt_y1, double pt_x2, double pt_y2)
{
	double degree = atan2( (pt_y1 - pt_y2), (pt_x1 - pt_x2) );
		
	degree = degree * 180.0/3.14 - 90.0;		
		
	return degree;
}

double CRotate_Option::Horizontal_GetRotationCheck(double pt_x1, double pt_y1, double pt_x2, double pt_y2)
{
	double degree = atan2( (pt_y1 - pt_y2), (pt_x1 - pt_x2) );
		
	degree = degree * 180.0/3.14;

	return degree;
}

void CRotate_Option::OnBnClickedButtonMasterA()
{
	b_SetZone = FALSE;
	
	double sum_Degree = 0;

	for(int k=0; k<10; k++)
	{
		int count = 0;
		((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan =1;						
			
		for(int i =0;i<10;i++){
			if(((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan == 0){
				break;
			}else{
				DoEvents(50);
			}
		}


		bool FLAG = FALSE;

		if(AutomationMod ==1)
		{
			FLAG = TRUE;
			GetSideCircleCoordinate(m_RGBScanbuf);
		}
		else
			GetSideCircleOnManualMode(m_RGBScanbuf);
			

		//============================= 0,2 사이값
		horipointY1 = ((SC_RECT[2].m_resultY + SC_RECT[0].m_resultY)/2);
		horipointX1 = ((SC_RECT[2].m_resultX + SC_RECT[0].m_resultX)/2);

		//============================= 1,3 사이값
		horipointY2 = ((SC_RECT[3].m_resultY + SC_RECT[1].m_resultY)/2);
		horipointX2 = ((SC_RECT[3].m_resultX + SC_RECT[1].m_resultX)/2);

		m_fHor_Degree = Horizontal_GetRotationCheck(horipointX2, horipointY2, horipointX1, horipointY1);
		
		//============================= 0,1 사이값

		vetipointX1 = ((SC_RECT[1].m_resultX + SC_RECT[0].m_resultX)/2);
		vetipointY1 = ((SC_RECT[1].m_resultY + SC_RECT[0].m_resultY)/2);

		//============================= 2,3 사이값

		vetipointX2 = ((SC_RECT[3].m_resultX + SC_RECT[2].m_resultX)/2);
		vetipointY2 = ((SC_RECT[3].m_resultY + SC_RECT[2].m_resultY)/2);
		//
		m_fVer_Degree = Vertical_GetRotationCheck(vetipointX2,vetipointY2, vetipointX1,vetipointY1);		

		double Degree = ((m_fHor_Degree) + (m_fVer_Degree)) / 2.0;

		MasterDegree = Degree;
		sum_Degree += MasterDegree;	
	}
	
	MasterDegree = sum_Degree /10.0f;

	MasterDegree *= m_dAngleOffset;

	MasterDegree = 0;

	//MasterDegree += m_dOffset_M;//각도 offset
	
	

	str_MasterDegree.Format("%6.3f",MasterDegree);
	UpdateData(FALSE);
	
	//TILT Master DATA 출력 //추가 수정
	T_RECT[0].m_resultDis = SC_RECT[1].m_resultX - SC_RECT[0].m_resultX;
	T_RECT[1].m_resultDis = SC_RECT[3].m_resultY - SC_RECT[1].m_resultY;
	T_RECT[2].m_resultDis = SC_RECT[3].m_resultX - SC_RECT[2].m_resultX;
	T_RECT[3].m_resultDis = SC_RECT[2].m_resultY - SC_RECT[0].m_resultY;
	UploadList_Tilt();

	MasterMod = FALSE;

	for (int t = 0; t < 4; t++){
		ChangeItem_T[t] = t;
	}
	for (int t = 0; t < 4; t++){
		A_TILTLIST.Update(t);
	}
}
void CRotate_Option::Load_Original_pra(){

	for(int t=0; t<4; t++){
		A_RECT[t].m_PosX=A_Original[t].m_PosX;
		A_RECT[t].m_PosY=A_Original[t].m_PosY;
		A_RECT[t].EX_RECT_SET(A_Original[t].m_PosX,A_Original[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);//////C
	}

}

tResultVal CRotate_Option::Run()
{	
	b_StopFail = FALSE;
	if(ChangeCheck == TRUE){
		OnBnClickedButtonARectSave();
	}	
	ResultDegree=0;
	
	b_RunEn = 1;
	int camcnt = 0;
	int Resultcount=0;
	tResultVal retval = {0,};

	BOOL FLAG = FALSE;
	CString stateDATA = "";
	
	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

		InsertList();
	}
	
	double Sum_Degree = 0;

	for(int k=0; k<10; k++)
	{

		int count = 0;
		((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan =1;						
		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;
		for (int i = 0; i < 50; i++)
		{
			DoEvents(10);
			if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0 && ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0)
			{
				break;
			}
		}

		//if(AutomationMod ==1)
		//{
		//	// 오토메이션 모드
		//	//C_CHECKING[] PosX,PosY 받은 거 체킹	
		//
		//	//Automation_DATA_INPUT();// 성공하면 데이터 C_RECT에 넣기.
		//	//실패하면 그냥 진행...
		//	//Load_parameter();// 실패한 경우
		//	
		//	FLAG = TRUE;

		//	GetSideCircleCoordinate(m_RGBScanbuf);

		//	//for(int lop=0; lop<5; lop++)
		//	//{
		//	//	C_CHECKING[lop].m_PosX = SC_RECT[lop].m_resultX;
		//	//	C_CHECKING[lop].m_PosY = SC_RECT[lop].m_resultY;					
		//	//	C_CHECKING[lop].m_Success = SC_RECT[lop].m_Success;					

		//	//	if(!C_CHECKING[lop].m_Success)
		//	//	{
		//	//		FLAG = FALSE;
		//	//		//break;
		//	//	}
		//	//
		//	//}
		//	//if(FLAG)
		//	//	Automation_DATA_INPUT();
		//	//else{Load_parameter();}	
		//}
		//else
		//GetSideCircleOnManualMode(m_RGBScanbuf);
		
	/*if(!FLAG){
		for(int lop=0; lop<5; lop++){
			
			AngleGen(m_RGBScanbuf,lop);
			
		}
	}
	else{
		for(int t=0; t<5; t++){
			A_RECT[t].m_resultX=C_CHECKING[t].m_PosX;
			A_RECT[t].m_resultY=C_CHECKING[t].m_PosY;
		}
	}*/

	
	/*horipointY1 = ((A_RECT[3].m_resultY - A_RECT[1].m_resultY)/2) + A_RECT[1].m_resultY;
	horipointY2 = ((A_RECT[4].m_resultY - A_RECT[2].m_resultY)/2) + A_RECT[2].m_resultY;

	vetipointX1 = ((A_RECT[2].m_resultX - A_RECT[1].m_resultX)/2) + A_RECT[1].m_resultX;
	vetipointX2 = ((A_RECT[4].m_resultX - A_RECT[3].m_resultX)/2) + A_RECT[3].m_resultX;
	
	m_fHor_Degree = Horizontal_GetRotationCheck(A_RECT[2].m_resultX, horipointY2, A_RECT[1].m_resultX, horipointY1);

	if(fabs(m_fHor_Degree) < m_dRotationAngle)
		m_bHorRotationCheck = TRUE;
	else
		m_bHorRotationCheck = FALSE;

	m_fVer_Degree = Vertical_GetRotationCheck(vetipointX2, A_RECT[3].m_resultY, vetipointX1, A_RECT[1].m_resultY);
		
	if(fabs(m_fVer_Degree) < m_dRotationAngle)
		m_bVerRotationCheck = TRUE;
	else
		m_bVerRotationCheck = FALSE;	*/

		if (AutomationMod == 1)
		{
			FLAG = TRUE;
			GetSideCircleCoordinate(m_RGBScanbuf);
		}
		else
			GetSideCircleOnManualMode(m_RGBScanbuf);

		stateDATA ="Rotate _ ";

		//============================= 0,2 사이값
		horipointY1 = ((SC_RECT[2].m_resultY + SC_RECT[0].m_resultY)/2);
		horipointX1 = ((SC_RECT[2].m_resultX + SC_RECT[0].m_resultX)/2);

		//============================= 1,3 사이값
		horipointY2 = ((SC_RECT[3].m_resultY + SC_RECT[1].m_resultY)/2);
		horipointX2 = ((SC_RECT[3].m_resultX + SC_RECT[1].m_resultX)/2);

		m_fHor_Degree = Horizontal_GetRotationCheck(horipointX2, horipointY2, horipointX1, horipointY1);
		
		//============================= 0,1 사이값

		vetipointX1 = ((SC_RECT[1].m_resultX + SC_RECT[0].m_resultX)/2);
		vetipointY1 = ((SC_RECT[1].m_resultY + SC_RECT[0].m_resultY)/2);

		//============================= 2,3 사이값

		vetipointX2 = ((SC_RECT[3].m_resultX + SC_RECT[2].m_resultX)/2);
		vetipointY2 = ((SC_RECT[3].m_resultY + SC_RECT[2].m_resultY)/2);
		//
		m_fVer_Degree = Vertical_GetRotationCheck(vetipointX2,vetipointY2, vetipointX1,vetipointY1);		

		double Degree = (((m_fHor_Degree) + (m_fVer_Degree)) / 2.0);

		Degree =   Degree - MasterDegree;//원본

		Degree *= m_dAngleOffset;

		//if (bOffsetType ==FALSE)//각도 offset
		//{
		//	Degree =  (MasterDegree - Degree) + m_dOffset;//수정
		//}else{
		//	
		//	Degree = MasterDegree - Degree;

		//	if (Degree > 0)
		//	{
		//		Degree -= m_dOffset;
		//	}else{

		//		Degree += m_dOffset;
		//	}

		//}

		ResultDegree = Degree;

		Sum_Degree += ResultDegree;
	}
	
	ResultDegree = Sum_Degree/10.0f;
	//ResultDegree+= m_dOffset;

	CString str;
	str.Format("%6.3f",ResultDegree);

	stateDATA += " "+ str;
	if( ( m_dRotationAngle_min <= ResultDegree)&&( ResultDegree <= m_dRotationAngle_max) )
	{
		m_bHorRotationCheck = TRUE;
		m_bVerRotationCheck = TRUE;
	}
	else
	{
		m_bHorRotationCheck = FALSE;
		m_bVerRotationCheck = FALSE;
	}
	
	
	if( m_bVerRotationCheck && m_bHorRotationCheck)
		m_bRotationCheck = TRUE;
	else
		m_bRotationCheck = FALSE;
	
	//TILT 결과 확인 // 추가 수정

	//TILT Master DATA 출력 //추가 수정
	A_RECT[0].m_resultDis = SC_RECT[1].m_resultX - SC_RECT[0].m_resultX;
	A_RECT[1].m_resultDis = SC_RECT[3].m_resultY - SC_RECT[1].m_resultY;
	A_RECT[2].m_resultDis = SC_RECT[3].m_resultX - SC_RECT[2].m_resultX;
	A_RECT[3].m_resultDis = SC_RECT[2].m_resultY - SC_RECT[0].m_resultY;

	CString str1, str2 = "";
	int Success = 0;
	for (int a = 0; a < 4; a++)
	{
		if (abs(T_RECT[a].m_resultDis - A_RECT[a].m_resultDis) < T_RECT[a].m_Thresold)
		{
			Success++;
			str1.Format("%d", (int)A_RECT[a].m_resultDis);
			if (a == 0)
				((CImageTesterDlg  *)m_pMomWnd)->m_pResRotateOptWnd->TILT_VALUE_TEXT(1, str1);
			if (a == 1)
				((CImageTesterDlg  *)m_pMomWnd)->m_pResRotateOptWnd->TILT_VALUE_TEXT2(1, str1);
			if (a == 2)
				((CImageTesterDlg  *)m_pMomWnd)->m_pResRotateOptWnd->TILT_VALUE_TEXT3(1, str1);
			if (a == 3)
				((CImageTesterDlg  *)m_pMomWnd)->m_pResRotateOptWnd->TILT_VALUE_TEXT4(1, str1);
		}
		else{
			str1.Format("%d", (int)A_RECT[a].m_resultDis);
			if (a == 0)
				((CImageTesterDlg  *)m_pMomWnd)->m_pResRotateOptWnd->TILT_VALUE_TEXT(2, str1);
			if (a == 1)
				((CImageTesterDlg  *)m_pMomWnd)->m_pResRotateOptWnd->TILT_VALUE_TEXT2(2, str1);
			if (a == 2)
				((CImageTesterDlg  *)m_pMomWnd)->m_pResRotateOptWnd->TILT_VALUE_TEXT3(2, str1);
			if (a == 3)
				((CImageTesterDlg  *)m_pMomWnd)->m_pResRotateOptWnd->TILT_VALUE_TEXT4(2, str1);
		}
	}
	if (Success == 4)
		m_bTiltCheck = TRUE;
	else
		m_bTiltCheck = FALSE;

	retval.m_ID = 0x04;
	retval.ValString.Empty();
//	retval.m_Val[0]= 


	str1.Format("%6.3f",ResultDegree);
	Str_Mes[0] = str1;
	m_AngleList.SetItemText(InsertIndex, 4, str1);

// 	if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 		m_Lot_RotateList.SetItemText(Lot_InsertIndex, 5, str1);//////////////////바뀌면 연동 파라메타
// 		//TILT 결과 입력
// 		for (int a = 0; a < 4; a++){
// 			str2.Format("%d", (int)A_RECT[a].m_resultDis);
// 			m_Lot_RotateList.SetItemText(Lot_InsertIndex, 6 + a, str2);
// 		}
// 	}
// 	else{
	if (((CImageTesterDlg *)m_pMomWnd)->LotMod == TRUE)
	{
	//	m_AngleList.SetItemText(InsertIndex, 4, str1);//////////////////바뀌면 연동 파라메타

// 		//TILT 결과 입력
// 		for (int a = 0; a < 4; a++){
// 			str2.Format("%d", (int)A_RECT[a].m_resultDis);
// 			m_AngleList.SetItemText(InsertIndex, 5 + a, str2);
// 		}
	}

	if (m_bRotationCheck /*&& m_bTiltCheck*/){
		retval.m_Success = TRUE;
		Str_Mes[1] = "1";
		stateDATA += " _ PASS ";
		retval.ValString.Format("%6.3f_OK", ResultDegree);
		
		((CImageTesterDlg  *)m_pMomWnd)->m_pResRotateOptWnd->VALUE_TEXT(str1);
		((CImageTesterDlg  *)m_pMomWnd)->m_pResRotateOptWnd->RESULT_TEXT(1,"PASS");

//		if(((CImageTesterDlg  *)m_pMomWnd)->LotMod ==1){
// 			if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 				m_Lot_RotateList.SetItemText(Lot_InsertIndex, 10, "PASS");
// 			}
// 			else{
				m_AngleList.SetItemText(InsertIndex, 5, "PASS");
				((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_ROTATE, "PASS");
//			}
//		}

		for(int t=0; t<4; t++){
			A_RECT[t].m_Success = TRUE;
		}
		
	}else{
		stateDATA += " _ FAIL ";
		retval.m_Success = FALSE;
		Str_Mes[1] = "0";
		retval.ValString.Format("%6.3f_FAIL", ResultDegree);
		((CImageTesterDlg  *)m_pMomWnd)->m_pResRotateOptWnd->RESULT_TEXT(2,"FAIL");
		
		((CImageTesterDlg  *)m_pMomWnd)->m_pResRotateOptWnd->VALUE_TEXT(str1);


//		if(((CImageTesterDlg  *)m_pMomWnd)->LotMod ==1){

// 			if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 				m_Lot_RotateList.SetItemText(Lot_InsertIndex, 10, "FAIL");
// 			}
// 			else{
				m_AngleList.SetItemText(InsertIndex, 5, "FAIL");
				((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_ROTATE, "FAIL");
//			}
//		}
		for(int t=0; t<4; t++){
			A_RECT[t].m_Success = FALSE;
		}
	}

// 	if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 		Lot_StartCnt++;
// 	}
// 	else{
	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
		StartCnt++;
	}

	((CImageTesterDlg  *)m_pMomWnd)->StatePrintf(stateDATA);

	m_retval = retval;

	return retval;
	
}	


CString CRotate_Option::Mes_Result()
{
	CString sz;
	//int		nResult = 0;
	//int nSel = m_AngleList.GetItemCount()-1;

	//// 각도
	//sz = m_AngleList.GetItemText(nSel, 4);

	//if(m_retval.m_Success)
	//	nResult = 1;
	//else
	//	nResult = 0;

	//m_szMesResult.Format(_T("%s:%d"), sz, nResult);
	m_szMesResult = _T("");
	if(b_StopFail == FALSE){
		m_szMesResult.Format(_T("%s:%s"),Str_Mes[0],Str_Mes[1]);
		m_szMesResult.Replace(" ","");
	}else{
		m_szMesResult.Format(_T(":1"));	
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_stNewMesData[NewMES_Rotate] = m_szMesResult;

	return m_szMesResult;
}


void CRotate_Option::FAIL_UPLOAD(){
//	if (((CImageTesterDlg  *)m_pMomWnd)->LotMod == 1)
//	{
// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 			LOT_InsertDataList();
// 			for (int a = 0; a < 5; a++){
// 				m_Lot_RotateList.SetItemText(Lot_InsertIndex, 5 + a, "X");
// 			}
// 			m_Lot_RotateList.SetItemText(Lot_InsertIndex, 10, "FAIL");
// 			m_Lot_RotateList.SetItemText(Lot_InsertIndex, 11, "STOP");
// 			b_StopFail = TRUE;
// 		}
// 		else{
			InsertList();
		//	for (int a = 0; a < 5; a++){
			m_AngleList.SetItemText(InsertIndex, 4, "X");
		//	}
			m_AngleList.SetItemText(InsertIndex, 5, "FAIL");
			m_AngleList.SetItemText(InsertIndex, 6, "STOP");
			Str_Mes[0] = "0.0";
			Str_Mes[1] = "0";
			((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_ROTATE, "STOP");
			b_StopFail = TRUE;
//		}
//	}
}
void CRotate_Option::Pic(CDC *cdc)
{
	for(int lop=0;lop<4;lop++){
		
			AnglePic(cdc,lop);
	
	}
}  

void CRotate_Option::InitPrm()
{
	Load_parameter();
	UpdateData(FALSE);
ResultDegree=0;
	UploadList();
	UploadList_Tilt();
}

void CRotate_Option::Automation_DATA_INPUT(){
	for(int t=0; t<4; t++){
		A_RECT[t].m_PosX=C_CHECKING[t].m_PosX;
		A_RECT[t].m_PosY=C_CHECKING[t].m_PosY;
		A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
	}

}

void CRotate_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

void CRotate_Option::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",A_filename);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}
//---------------------------------------------------------------------WORKLIST
void CRotate_Option::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	//if(LotMod ==1){
	InsertIndex = m_AngleList.InsertItem(StartCnt,"",0);
	//	totalint = okint + failint +1;/////////////////////////////////////////////////////////////////////////////////////////0527
	//}
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_AngleList.SetItemText(InsertIndex,0,strCnt);
	
	m_AngleList.SetItemText(InsertIndex,1,((CImageTesterDlg  *)m_pMomWnd)->m_modelName);
//	m_AngleList.SetItemText(InsertIndex,2, ((CImageTesterDlg  *)m_pMomWnd)->LOT_NUMBER);////////LOT 이름
	m_AngleList.SetItemText(InsertIndex,2,((CImageTesterDlg  *)m_pMomWnd)->strDay+"");
	m_AngleList.SetItemText(InsertIndex,3,((CImageTesterDlg  *)m_pMomWnd)->strTime+"");

//	m_AngleList.SetItemText(InsertIndex,7,((CImageTesterDlg  *)m_pMomWnd)->LOT_OPERATOR);

}
void CRotate_Option::Set_List(CListCtrl *List){
	

	while(List->DeleteColumn(0));
	((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	//List->InsertColumn(2,"LOT CARD",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"각도",LVCFMT_CENTER, 80);
// 	List->InsertColumn(5, "LT_RT", LVCFMT_CENTER, 80);
// 	List->InsertColumn(6, "RT_RB", LVCFMT_CENTER, 80);
// 	List->InsertColumn(7, "RB_LB", LVCFMT_CENTER, 80);
// 	List->InsertColumn(8, "LB_LT", LVCFMT_CENTER, 80);
	List->InsertColumn(5,"RESULT",LVCFMT_CENTER, 80);
//	List->InsertColumn(7,"작업자",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"비고",LVCFMT_CENTER, 80);
	//List->InsertColumn(12,"Serial",LVCFMT_CENTER, 80);
	ListItemNum=7;
	Copy_List(&m_AngleList, ((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList, ListItemNum);
	
	

}

void CRotate_Option::EXCEL_SAVE()
{
	CString Item[2] = { _T("각도"), _T("") };

	CString Data ="";
	CString str="";
	str = "***********************Rotate/Tilt 검사 [ Rotate 단위 : º ] [ Tilt 단위 : Pixel ] ***********************\n";
	Data += str;
	str.Empty();
	str.Format("로테이트 , %6.1f,~,%6.1f, \n ",m_dRotationAngle_min,m_dRotationAngle_max);
	Data += str;
	str.Empty();
	str.Format("LT_RT, %d ,Threshold, ± %d pixel,, ", (int)T_RECT[0].m_resultDis, T_RECT[0].m_Thresold);
	Data += str;
	str.Empty();
	str.Format("RT_RB , %d ,Threshold, ± %d pixel, \n", (int)T_RECT[1].m_resultDis, T_RECT[1].m_Thresold);
	Data += str;
	str.Empty();
	str.Format("RB_LB , %d ,Threshold, ± %d pixel, ,", (int)T_RECT[2].m_resultDis, T_RECT[2].m_Thresold);
	Data += str;
	str.Empty();
	str.Format("LB_LT , %d ,Threshold, ± %d pixel,\n ", (int)T_RECT[3].m_resultDis, T_RECT[3].m_Thresold);
	Data += str;
	str.Empty();
	str.Empty();
	str = "*************************************************\n";
	Data += str;
	((CImageTesterDlg  *)m_pMomWnd)->SAVE_EXCEL("Rotate_Tilt 검사",Item,1,&m_AngleList,Data);
}

bool CRotate_Option::EXCEL_UPLOAD(){
	m_AngleList.DeleteAllItems();
	if(((CImageTesterDlg  *)m_pMomWnd)->EXCEL_UPLOAD("Rotate_Tilt 검사",&m_AngleList,1,&StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		return FALSE;	
	}
}
void CRotate_Option::OnBnClickedCheckVer()
{
	
	TestMod = 2;
	TestModALL =0;
	TestModVer =1;
	TestModHor =0;
strTitle.Empty();
	strTitle="ROTATE_INIT";	

	CString str="";
	str.Empty();
	str.Format("%d",TestMod);
	WritePrivateProfileString(str_model,strTitle+"TestMod",str,A_filename);

	UpdateData(FALSE);
	
}

void CRotate_Option::OnBnClickedCheckHor()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	TestMod = 1;
	TestModALL =0;
	TestModVer =0;
	TestModHor =1;

strTitle.Empty();
	strTitle="ROTATE_INIT";	
	CString str="";

	str.Empty();
	str.Format("%d",TestMod);
	WritePrivateProfileString(str_model,strTitle+"TestMod",str,A_filename);


	UpdateData(FALSE);
	
}

void CRotate_Option::OnBnClickedCheckAuto()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
		if(AutomationMod ==0){
		
		AutomationMod =1;
		//A_DATALIST.EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_PosX)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_PosY)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_Dis_W)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_Dis_H)->EnableWindow(0);
		/*(CEdit *)GetDlgItem(IDC_Angle_Width)->EnableWindow(0);
		
		(CEdit *)GetDlgItem(IDC_Angle_Height)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_Thresold)->EnableWindow(0);
		*/
//		(CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE)->EnableWindow(0);
		//(CButton *)GetDlgItem(IDC_BUTTON_setAngleZone)->EnableWindow(0);
		
		//////////////////////////////////////////////////////////////////////오토메이션 중심점으로 영역 잡는 소스 추가해야함.
	}
	else{
		AutomationMod =0;
		
		//A_DATALIST.EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_PosX)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_PosY)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_Dis_W)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_Dis_H)->EnableWindow(1);
		/*(CEdit *)GetDlgItem(IDC_Angle_Width)->EnableWindow(1);
		
		(CEdit *)GetDlgItem(IDC_Angle_Height)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_Thresold)->EnableWindow(1);
		*/
//		(CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE)->EnableWindow(1);
		//(CButton *)GetDlgItem(IDC_BUTTON_setAngleZone)->EnableWindow(1);
	
	}

		for(int t=0; t<4; t++){
			A_DATALIST.Update(t);
		}


		strTitle.Empty();
	strTitle="ROTATE_INIT";	
	if(AutomationMod ==1){
		WritePrivateProfileString(str_model,strTitle+"Auto","1",A_filename);}
	if(AutomationMod ==0){
		WritePrivateProfileString(str_model,strTitle+"Auto","0",A_filename);}

}

void CRotate_Option::GetSideCircleCoordinate(LPBYTE IN_RGB)
{
	IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);	
	IplImage *SmoothImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);	

	BYTE R,G,B;
	double Sum_Y;
	double Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	double Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	for (int y=0; y<CAM_IMAGE_HEIGHT; y++)
	{
		for (int x=0; x<CAM_IMAGE_WIDTH; x++)
		{
			B = IN_RGB[y*(m_CAM_SIZE_WIDTH * 3) + x * 3];
			G = IN_RGB[y*(m_CAM_SIZE_WIDTH * 3) + x * 3 + 1];
			R = IN_RGB[y*(m_CAM_SIZE_WIDTH * 3) + x * 3 + 2];
			
			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
		//	if(R > G + B)
		//		Sum_Y = 255;
		//	else
		//		Sum_Y = 0;

			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;			
		}
	}


	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	//cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0); //객체 외곽의 Overshooting으로 인하여 한번 더 처리 함..

	cvThreshold(SmoothImage, OriginImage, 70, 255, CV_THRESH_BINARY);
	cvAdaptiveThreshold(SmoothImage, OriginImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 21, 2);

	//cvNot(OriginImage, OriginImage);
	//cvSaveImage("D:\\SmoothImage.png", OriginImage);

	cvCanny(OriginImage, CannyImage, 0, 150);
	
	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);


	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;
	CvRect rect;
	double area=0, arcCount=0;
	double old_dist = 999999;
	
	SC_RECT[0].m_Success = FALSE;

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
	{
		area = cvContourArea(temp_contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(temp_contour, CV_WHOLE_SEQ, -1);
		
		double circularity = (4.0*3.14*area)/(arcCount*arcCount);
		
		if(circularity > 0.7)
		{			
			rect = cvContourBoundingRect(temp_contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;
		
			cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8); 
			counter++;
		}		
	}
	
	int total_circle_count = counter;
	CvRect *rectArray = new CvRect[counter];
	counter = 0;
	
	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);		
	
		double circularity = (4.0*3.14*area)/(arcCount*arcCount);		
		
		if (circularity > 0.7)
		{			
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;
			
			rectArray[counter] = rect;
			counter++;

		}
	}
	
	old_dist = 999999;

	for(int i=0; i<4; i++)
	{
		SC_RECT[i].m_Success = FALSE;

		for(int j=0; j<total_circle_count; j++)
		{
			rect = rectArray[j];
			
			double st_circle_center_x = A_RECT[i].m_PosX;
			double st_circle_center_y = A_RECT[i].m_PosY;

			double curr_circle_center_x = rect.x + rect.width/2;
			double curr_circle_center_y = rect.y + rect.height/2;

			double dist = GetDistance(st_circle_center_x, st_circle_center_y, curr_circle_center_x, curr_circle_center_y);

			if(dist < old_dist && dist < 150)
			{
//-------------------------------------------------------------무게중심AMH
				int data_x = 0;
				int data_y = 0;
				counter = 0;
				for (int y = rect.y; y < rect.y + rect.height +1; y++)
				{
					for (int x = rect.x; x < rect.x + rect.width+1; x++)
					{
						int BW = OriginImage->imageData[y*CAM_IMAGE_WIDTH + x];
						if (BW != 0){
							data_x += x;
							data_y += y;
							counter++;
						}
					}
				}
				if (counter != 0){
					curr_circle_center_x = data_x / counter;
					curr_circle_center_y = data_y / counter;
				}
//-------------------------------------------------------------
				SC_RECT[i].m_resultX = curr_circle_center_x;
				SC_RECT[i].m_resultY = curr_circle_center_y;

				old_dist = dist;

				SC_RECT[i].m_Success = TRUE;
			}
			cvRectangle(OriginImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(255, 0, 255), 1, 8);
		}

		old_dist = 999999;
	}
	for (int i = 0; i < 4; i++){
		A_RECT[i].m_PosX = SC_RECT[i].m_resultX;
		A_RECT[i].m_PosY = SC_RECT[i].m_resultY;
		A_RECT[i].EX_RECT_SET(A_RECT[i].m_PosX, A_RECT[i].m_PosY, A_RECT[i].m_Width, A_RECT[i].m_Height);
	}
	cvSaveImage("D:\\RGBResultImage_R.bmp", OriginImage);
	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&CannyImage);	
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	//delete contour;
	//delete temp_contour;
	
	delete []rectArray;
}
void CRotate_Option::GetSideCircleOnManualMode(LPBYTE IN_RGB)
{
	CvRect rect[4];

	for (int NUM = 0; NUM < 4; NUM++){
		IplImage *OriginImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 1);
		IplImage *CannyImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 1);
		IplImage *DilateImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 1);
		IplImage *SmoothImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 1);
		IplImage *RGBResultImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 3);

		int Curr_Center_X = -99999;
		int Curr_Center_Y = -99999;

		A_RECT[NUM].m_resultX = -1;
		A_RECT[NUM].m_resultY = -1;

		BYTE R, G, B;
		double Sum_Y;

		BOOL detect = FALSE;

		cvSetZero(OriginImage);

		for (int y = A_RECT[NUM].m_Top; y < A_RECT[NUM].m_Bottom + 1; y++)
		{
			for (int x = A_RECT[NUM].m_Left; x < A_RECT[NUM].m_Right + 1; x++)
			{
				B = IN_RGB[y*CAM_IMAGE_WIDTH * 3 + x * 3];
				G = IN_RGB[y*CAM_IMAGE_WIDTH * 3 + x * 3 + 1];
				R = IN_RGB[y*CAM_IMAGE_WIDTH * 3 + x * 3 + 2];

				Sum_Y = (BYTE)((0.29900*R) + (0.58700*G) + (0.11400*B));

				OriginImage->imageData[(y - (A_RECT[NUM].m_Top))*OriginImage->widthStep + (x - (A_RECT[NUM].m_Left))] = (char)Sum_Y;
			}
		}

		cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 3, 1.0, 1.0);
		//	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);

		//cvSaveImage("D:\\OriginImage_A.jpg", OriginImage);

		//	cvSaveImage("D:\\SmoothImage.jpg", SmoothImage);

		cvThreshold(SmoothImage, SmoothImage, 50, 255, CV_THRESH_BINARY);

		//if ((NUM != 1) && (NUM != 2))
			cvNot(SmoothImage, SmoothImage);


		//	cvCanny(SmoothImage, CannyImage, 0, 50);

		cvDilate(SmoothImage, DilateImage);

		//cvSaveImage("D:\\DilateImage_R.jpg", DilateImage);

		cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);

		CvMemStorage* contour_storage = cvCreateMemStorage(0);
		CvSeq *contour = 0;
		CvSeq *temp_contour = 0;

		cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

		temp_contour = contour;

		int counter = 0;

		for (; temp_contour != 0; temp_contour = temp_contour->h_next)
			counter++;

		//CvRect rect;
		double area = 0, arcCount = 0;
		counter = 0;
		double old_dist = 999999;

		for (; contour != 0; contour = contour->h_next)
		{
			int center_x, center_y;
			area = cvContourArea(contour, CV_WHOLE_SEQ);
			arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

			double circularity = (4.0*3.14*area) / (arcCount*arcCount);

			rect[NUM] = cvContourBoundingRect(contour, 1);

			center_x = rect[NUM].x + rect[NUM].width / 2;
			center_y = rect[NUM].y + rect[NUM].height / 2;

			//-------------------------------------------------------------무게중심AMH
			int data_x = 0;
			int data_y = 0;

			if (circularity > 0.6)
			{
				data_x = 0;
				data_y = 0;
				counter = 0;
				//for (int y = A_RECT[i].m_Top; y < A_RECT[i].m_Top+A_RECT[i].m_Height; y++)
				for (int y = rect[NUM].y; y < rect[NUM].y + rect[NUM].height; y++)
				{
					//for (int x = A_RECT[i].m_Left; x < A_RECT[i].m_Left + A_RECT[i].m_Width; x++)
					for (int x = rect[NUM].x; x < rect[NUM].x + rect[NUM].width; x++)
					{
						int BW = DilateImage->imageData[y*A_RECT[NUM].m_Width + x];
						if (BW != 0){
							data_x += x;
							data_y += y;
							counter++;
						}
					}
				}
				cvRectangle(RGBResultImage, cvPoint(rect[NUM].x, rect[NUM].y), cvPoint(rect[NUM].x + rect[NUM].width, rect[NUM].y + rect[NUM].height), CV_RGB(255, 0, 0), 1, 8);
				//cvSaveImage("D:\\RGBResultImage_R.bmp", RGBResultImage);
				if (counter != 0){
					center_x = data_x / counter;
					center_y = data_y / counter;
				}
		
				double dist = GetDistance(center_x, center_y, (A_RECT[NUM].m_Width / 2), (A_RECT[NUM].m_Height / 2));
				//if (dist != 0)
				{
					if (dist < old_dist)
					{
						SC_RECT[NUM].m_resultX = center_x + A_RECT[NUM].m_Left;
						SC_RECT[NUM].m_resultY = center_y + A_RECT[NUM].m_Top;

						SC_RECT[NUM].m_Success = TRUE;

						old_dist = dist;

						detect = TRUE;

						cvRectangle(RGBResultImage, cvPoint(rect[NUM].x, rect[NUM].y), cvPoint(rect[NUM].x + rect[NUM].width, rect[NUM].y + rect[NUM].height), CV_RGB(0, 0, 255), 1, 8);
						
					}
				}



			}
			counter++;
		}
		//-------------------------------------------------------------	
		cvSaveImage("D:\\ResultImage_R.bmp", RGBResultImage);
		cvReleaseMemStorage(&contour_storage);
		cvReleaseImage(&OriginImage);
		cvReleaseImage(&CannyImage);
		cvReleaseImage(&DilateImage);
		cvReleaseImage(&SmoothImage);
		cvReleaseImage(&RGBResultImage);

	}
}
// void CRotate_Option::GetSideCircleOnManualMode(LPBYTE IN_RGB)
// {
// 	IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
// 	IplImage *CannyImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);	
// 	IplImage *SmoothImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
// 	IplImage *RGBResultImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);	
// 	IplImage *LocalROIImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
// 
// 	BYTE R,G,B;
// 	double Sum_Y;
// 
// 	for (int y=0; y<CAM_IMAGE_HEIGHT; y++)
// 	{
// 		for (int x=0; x<CAM_IMAGE_WIDTH; x++)
// 		{
// 			B = IN_RGB[y*(m_CAM_SIZE_WIDTH * 3) + x * 3];
// 			G = IN_RGB[y*(m_CAM_SIZE_WIDTH * 3) + x * 3 + 1];
// 			R = IN_RGB[y*(m_CAM_SIZE_WIDTH * 3) + x * 3 + 2];
// 			
// 			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));	
// 
// 			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;			
// 		}
// 	}	
// 	
// //	Capture_Gray_SAVE("C:\\RGBIMAGE", IN_RGB, CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT);	
// 	//cvSaveImage("D:\\Default_Image_R.bmp", OriginImage);
// 	
// 	cvSetZero(LocalROIImage);
// 
// 	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 3, 1, 1);
// 	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 3, 1, 1); //객체 외곽의 Overshooting으로 인하여 한번 더 처리 함..
// 	
// 	for(int i=0; i<4; i++)
// 	{
// 		IplImage *ROIImage = cvCreateImage(cvSize(A_RECT[i].m_Width, A_RECT[i].m_Height), IPL_DEPTH_8U, 1);
// 
// 		cvSetImageROI(SmoothImage, cvRect(A_RECT[i].m_Left, A_RECT[i].m_Top, A_RECT[i].m_Width, A_RECT[i].m_Height));
// 		cvSetImageROI(LocalROIImage, cvRect(A_RECT[i].m_Left, A_RECT[i].m_Top, A_RECT[i].m_Width, A_RECT[i].m_Height));
// 		cvCopyImage(SmoothImage, ROIImage);
// 		cvThreshold(ROIImage, ROIImage, 0, 255, CV_THRESH_OTSU);
// 		//cvNot(ROIImage, ROIImage);
// 		cvSaveImage("D:\\ROIImage_R.bmp", ROIImage);
// 		cvCopyImage(ROIImage, LocalROIImage);
// 		cvResetImageROI(SmoothImage);
// 		cvResetImageROI(LocalROIImage);
// 		
// 		cvReleaseImage(&ROIImage);
// 	}
// 	
// 	cvCopyImage(LocalROIImage, SmoothImage);
// 	cvSaveImage("D:\\SmoothImage_R.bmp", SmoothImage);
// // 	cvCanny(SmoothImage, CannyImage, 0, 150);
// // 	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);
// 	
// 	CvMemStorage* contour_storage = cvCreateMemStorage(0);
// 	CvSeq *contour = 0;
// 	CvSeq *temp_contour = 0;
// 	cvSaveImage("D:\\CannyImage_R.bmp", CannyImage);
// 	cvFindContours(SmoothImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
// 
// 	temp_contour = contour;
// 	
// 	int counter = 0;
// 	CvRect rect;
// 	double area=0, arcCount=0;
// 	double old_dist = 999999;	
// 
// 	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
// 	{
// 		int center_x, center_y;
// 		area = cvContourArea(temp_contour, CV_WHOLE_SEQ);
// 		arcCount = cvArcLength(temp_contour, CV_WHOLE_SEQ, -1);
// 
// 		double circularity = (4.0*3.14*area) / (arcCount*arcCount);
// 		
// 		rect = cvContourBoundingRect(temp_contour, 1);
// 
// 		center_x = rect.x + rect.width/2;
// 		center_y = rect.y + rect.height/2;
// // 		for(int k=0; k<4; k++)
// // 		{
// // 			if( A_RECT[k].m_Left < center_x && A_RECT[k].m_Right > center_x )
// // 			{
// // 				if( A_RECT[k].m_Top < center_y && A_RECT[k].m_Bottom > center_y )
// // 				{
// // 					SC_RECT[k].m_resultX = center_x;
// // 					SC_RECT[k].m_resultY = center_y;
// // 
// // 					SC_RECT[k].m_Success = TRUE;
// // 				}
// // 			}
// // 		}
// 		
// //-------------------------------------------------------------무게중심AMH
// 		int data_x = 0;
// 		int data_y = 0;
// 
// 		if (circularity > 0.8)
// 		{
// 			for (int i = 0; i < 4; i++)
// 			{
// 				data_x = 0;
// 				data_y = 0;
// 				counter = 0;
// 				//for (int y = A_RECT[i].m_Top; y < A_RECT[i].m_Top+A_RECT[i].m_Height; y++)
// 				for (int y = rect.y; y < rect.y + rect.height; y++)
// 				{
// 					//for (int x = A_RECT[i].m_Left; x < A_RECT[i].m_Left + A_RECT[i].m_Width; x++)
// 					for (int x = rect.x; x < rect.x + rect.width; x++)
// 					{
// 						int BW = SmoothImage->imageData[y*CAM_IMAGE_WIDTH + x];
// 						if (BW != 0 ){
// 							data_x += x;
// 							data_y += y;
// 							counter++;
// 						}
// 					}
// 				}
// 				cvRectangle(SmoothImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(255, 0, 0), 1, 8);
// 				cvSaveImage("D:\\SmoothImage_R.bmp", SmoothImage);
// 				if (counter != 0){
// 					center_x = data_x / counter;
// 					center_y = data_y / counter;
// 				}
// // 				SC_RECT[i].m_resultX = center_x;
// // 				SC_RECT[i].m_resultY = center_y;
// // 				SC_RECT[i].m_Success = TRUE;
// // 				cvRectangle(CannyImage, cvPoint(A_RECT[i].m_Left, A_RECT[i].m_Top), cvPoint(A_RECT[i].m_Left + A_RECT[i].m_Width, A_RECT[i].m_Top + A_RECT[i].m_Height), CV_RGB(0, 0, 255), 1, 8);
// // 				
// 				for (int k = 0; k < 4; k++)
// 				{
// 					if (A_RECT[k].m_Left < center_x && A_RECT[k].m_Right > center_x)
// 					{
// 						if (A_RECT[k].m_Top < center_y && A_RECT[k].m_Bottom > center_y)
// 						{
// 							SC_RECT[k].m_resultX = center_x;
// 							SC_RECT[k].m_resultY = center_y;
// 
// 							SC_RECT[k].m_Success = TRUE;
// 
// 							cvRectangle(SmoothImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 0, 255), 1, 8);
// 							cvSaveImage("D:\\SideCircleResultImage_R.bmp", CannyImage);
// 						}
// 					}
// 				}
// 			
// 			
// 			}
// 		}
// 	}
// //-------------------------------------------------------------	
// 		
// 	for(int i=0; i<4; i++)
// 	{
// 		if(SC_RECT[i].m_Success == FALSE)
// 		{
// 			SC_RECT[i].m_resultX = Standard_SC_RECT[i].m_resultX;
// 			SC_RECT[i].m_resultY = Standard_SC_RECT[i].m_resultY;
// 		}
// 	}
// 
// 	
// 	//int offset_x = A_RECT[0].m_resultX - SC_RECT[0].m_resultX;
// 	//int offset_y = A_RECT[0].m_resultY - SC_RECT[0].m_resultY;
// 	//
// 	//for(int k=0; k<4; k++)
// 	//{
// 
// 	//	A_RECT[k].m_Left -= offset_x/2;
// 	//	A_RECT[k].m_Top -= offset_y/2;
// 	//	
// 	//	A_RECT[k].m_Right = A_RECT[k].m_Left + A_RECT[k].m_Width;
// 	//	A_RECT[k].m_Bottom = A_RECT[k].m_Top + A_RECT[k].m_Height;
// 
// 	////	A_RECT[k].m_resultX -= offset_x;
// 	////	A_RECT[k].m_resultY -= offset_y;
// 
// 	//	A_RECT[k].m_resultX = A_RECT[k].m_Left + A_RECT[k].m_Width/2;
// 	//	A_RECT[k].m_resultY = A_RECT[k].m_Top + A_RECT[k].m_Height/2;
// 
// 	//}
// 
// 	cvReleaseMemStorage(&contour_storage);
// 	cvReleaseImage(&OriginImage);
// 	cvReleaseImage(&CannyImage);	
// 	cvReleaseImage(&SmoothImage);
// 	cvReleaseImage(&RGBResultImage);
// 	cvReleaseImage(&LocalROIImage);
// 
// 	//delete contour;
// 	delete temp_contour;	
// 	
// 	
// //	delete []rectArray;
// }

double CRotate_Option::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt( (double)((x2-x1)*(x2-x1)) +  ((y2-y1)*(y2-y1)));

	return result;
}
void CRotate_Option::OnNMCustomdrawListAngle(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//*pResult = 0;

	 COLORREF text_color = 0;
        COLORREF bg_color = RGB(255, 255, 255);
     
        LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
		
	//	lplvcd->iPartId
        switch(lplvcd->nmcd.dwDrawStage){
            case CDDS_PREPAINT:
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
             
            // 배경 혹은 텍스트를 수정한다.
            case CDDS_ITEMPREPAINT:
                // 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
               /* if(lplvcd->nmcd.dwItemSpec == 0) text_color = RGB(255, 0, 0);
                else if(lplvcd->nmcd.dwItemSpec == 1) text_color = RGB(0, 255, 0);
                else text_color = RGB(0, 0, 255);
                lplvcd->clrText = text_color;*/
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
           
            // 서브 아이템의 배경 혹은 텍스트를 수정한다.
            case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
                if(lplvcd->iSubItem != 0){
                    // 1번째 행이라면...

					if(lplvcd->nmcd.dwItemSpec >=0 ||lplvcd->nmcd.dwItemSpec <5){
						/*if(AutomationMod ==1)
						{
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 7 ){
								text_color = RGB(0, 0, 0);
								bg_color = RGB(200, 200, 200);
							}
						}
						else*/{
							text_color = RGB(0, 0, 0);
							bg_color = RGB(255, 255, 255);
						
						}

						if(ChangeCheck==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 11 ){
								for(int t=0; t<4; t++){
									if(lplvcd->nmcd.dwItemSpec == ChangeItem[t]){
										text_color = RGB(0, 0, 0);
										bg_color = RGB(250, 230, 200);					
									}
								}
							}

						}
					
					}
					

				    lplvcd->clrText = text_color;
                    lplvcd->clrTextBk = bg_color;
					
                }
                *pResult = CDRF_NEWFONT;
                return;
        }
}


void CRotate_Option::OnEnKillfocusEditAMod()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	ChangeCheck=1;
	if((iSavedItem != -1)&&(iSavedSubitem != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_A_MOD, str));
		List_COLOR_Change();
		if(A_DATALIST.GetItemText(iSavedItem, iSavedSubitem) != str){
			Change_DATA();
			UploadList();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );
	iSavedItem = -1;
	iSavedSubitem = -1;
	((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	
}

void CRotate_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == TRUE){
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
		b_SetZone = FALSE;

	}else{
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = -1;
		b_SetZone = FALSE;

	}
}

void CRotate_Option::OnBnClickedButtonALoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	b_SetZone = FALSE;

	Load_parameter();
	UpdateData(FALSE);
	UploadList();
	UploadList_Tilt();
}

void CRotate_Option::OnBnClickedCheck1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	TestModALL=1;
	TestMod =0;
	TestModHor=0;
	TestModVer=0;

	CString str="";

	strTitle.Empty();
	strTitle="ROTATE_INIT";	

	str.Empty();
	str.Format("%d",TestMod);
	WritePrivateProfileString(str_model,strTitle+"TestMod",str,A_filename);

	UpdateData(FALSE);
}

void CRotate_Option::OnEnChangeRotationAngle()
{
	//m_dRotationAngle = GetDlgItemInt(IDC_ROTATION_ANGLE);
	UpdateData(TRUE);
}

void CRotate_Option::OnBnClickedButtonSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	strTitle.Empty();
	strTitle="ROTATE_INIT";	

	m_dAngleOffset = atof(str_Angle_Offset);
	m_dRotationAngle_min = atof(str_minDegree);
	m_dRotationAngle_max = atof(str_maxDegree);
	if(m_dRotationAngle_min >= m_dRotationAngle_max){
		m_dRotationAngle_min = m_dRotationAngle_max-1.0;
	}
	str_Angle_Offset.Format("%6.1f", m_dAngleOffset);
	str_minDegree.Format("%6.1f",m_dRotationAngle_min);
	str_maxDegree.Format("%6.1f",m_dRotationAngle_max);

	WritePrivateProfileString(str_model, strTitle + "RotationAngle_NewOffset", str_Angle_Offset, A_filename);
	WritePrivateProfileString(str_model,strTitle+"RotationAngle_MIN",str_minDegree,A_filename);
	WritePrivateProfileString(str_model,strTitle+"RotationAngle_MAX",str_maxDegree,A_filename);
	UpdateData(FALSE);

}

void CRotate_Option::InitStat()
{
	//초기화 알고리즘
	b_RunEn =0;
}
void CRotate_Option::OnBnClickedButtonPicsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	strTitle.Empty();
	strTitle="ROTATE_INIT";	

	CString str = "";
	str.Empty();
	str.Format("%d",b_CenterValueView);
	WritePrivateProfileString(str_model,strTitle+"CenterValueView",str,A_filename);
	str.Empty();
	str.Format("%d",b_DegreeValueView);
	WritePrivateProfileString(str_model,strTitle+"DegreeValueView",str,A_filename);
	str.Empty();
	str.Format("%d",b_RectView);
	WritePrivateProfileString(str_model,strTitle+"RectView",str,A_filename);


}

void CRotate_Option::OnBnClickedCheckCenter()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(b_CenterValueView == 1){
		b_CenterValueView = 0;
	}else{
		b_CenterValueView = 1;
	}
}

void CRotate_Option::OnBnClickedCheckDegree()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(b_DegreeValueView == 1){
		b_DegreeValueView = 0;
	}else{
		b_DegreeValueView = 1;
	}
}

void CRotate_Option::OnBnClickedCheckRect()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(b_RectView == 1){
		b_RectView = 0;
	}else{
		b_RectView = 1;
	}
}

void CRotate_Option::OnEnChangeRotationAngle2()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CRotate_Option::OnEnChangeAnglePosx()
{
	EditMinMaxIntSet(IDC_Angle_PosX, &A_Total_PosX,0,CAM_IMAGE_WIDTH);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);

	if((A_Total_PosX >= 0)&&(A_Total_PosX <= CAM_IMAGE_WIDTH)&&
		(A_Total_PosY >= 0)&&(A_Total_PosY <= CAM_IMAGE_HEIGHT)&&
		(A_Dis_Width>=0)&&(A_Dis_Width<=CAM_IMAGE_WIDTH)&&
		(A_Dis_Height>=0)&&(A_Dis_Height<=CAM_IMAGE_HEIGHT)&&
		(A_Total_Width>=0)&&(A_Total_Width<=CAM_IMAGE_WIDTH)&&
		(A_Total_Height>=0)&&(A_Total_Height<=CAM_IMAGE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CRotate_Option::OnEnChangeAnglePosy()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	EditMinMaxIntSet(IDC_Angle_PosY, &A_Total_PosY,0,CAM_IMAGE_HEIGHT);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);

	if((A_Total_PosX >= 0)&&(A_Total_PosX <= CAM_IMAGE_WIDTH)&&
		(A_Total_PosY >= 0)&&(A_Total_PosY <= CAM_IMAGE_HEIGHT)&&
		(A_Dis_Width>=0)&&(A_Dis_Width<=CAM_IMAGE_WIDTH)&&
		(A_Dis_Height>=0)&&(A_Dis_Height<=CAM_IMAGE_HEIGHT)&&
		(A_Total_Width>=0)&&(A_Total_Width<=CAM_IMAGE_WIDTH)&&
		(A_Total_Height>=0)&&(A_Total_Height<=CAM_IMAGE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CRotate_Option::OnEnChangeAngleDisW()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	EditMinMaxIntSet(IDC_Angle_Dis_W, &A_Dis_Width,0,CAM_IMAGE_WIDTH);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);
	if((A_Total_PosX >= 0)&&(A_Total_PosX <= CAM_IMAGE_WIDTH)&&
		(A_Total_PosY >= 0)&&(A_Total_PosY <= CAM_IMAGE_HEIGHT)&&
		(A_Dis_Width>=0)&&(A_Dis_Width<=CAM_IMAGE_WIDTH)&&
		(A_Dis_Height>=0)&&(A_Dis_Height<=CAM_IMAGE_HEIGHT)&&
		(A_Total_Width>=0)&&(A_Total_Width<=CAM_IMAGE_WIDTH)&&
		(A_Total_Height>=0)&&(A_Total_Height<=CAM_IMAGE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CRotate_Option::OnEnChangeAngleDisH()
{
	EditMinMaxIntSet(IDC_Angle_Dis_H, &A_Dis_Height,0,CAM_IMAGE_HEIGHT);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);

	if((A_Total_PosX >= 0)&&(A_Total_PosX <= CAM_IMAGE_WIDTH)&&
		(A_Total_PosY >= 0)&&(A_Total_PosY <= CAM_IMAGE_HEIGHT)&&
		(A_Dis_Width>=0)&&(A_Dis_Width<=CAM_IMAGE_WIDTH)&&
		(A_Dis_Height>=0)&&(A_Dis_Height<=CAM_IMAGE_HEIGHT)&&
		(A_Total_Width>=0)&&(A_Total_Width<=CAM_IMAGE_WIDTH)&&
		(A_Total_Height>=0)&&(A_Total_Height<=CAM_IMAGE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CRotate_Option::OnEnChangeAngleWidth()
{
	EditMinMaxIntSet(IDC_Angle_Width, &A_Total_Width,0,CAM_IMAGE_WIDTH);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);
	if((A_Total_PosX >= 0)&&(A_Total_PosX <= CAM_IMAGE_WIDTH)&&
		(A_Total_PosY >= 0)&&(A_Total_PosY <= CAM_IMAGE_HEIGHT)&&
		(A_Dis_Width>=0)&&(A_Dis_Width<=CAM_IMAGE_WIDTH)&&
		(A_Dis_Height>=0)&&(A_Dis_Height<=CAM_IMAGE_HEIGHT)&&
		(A_Total_Width>=0)&&(A_Total_Width<=CAM_IMAGE_WIDTH)&&
		(A_Total_Height>=0)&&(A_Total_Height<=CAM_IMAGE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CRotate_Option::OnEnChangeAngleHeight()
{
	EditMinMaxIntSet(IDC_Angle_Height, &A_Total_Height,0,CAM_IMAGE_HEIGHT);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);

	if((A_Total_PosX >= 0)&&(A_Total_PosX <= CAM_IMAGE_WIDTH)&&
		(A_Total_PosY >= 0)&&(A_Total_PosY <= CAM_IMAGE_HEIGHT)&&
		(A_Dis_Width>=0)&&(A_Dis_Width<=CAM_IMAGE_WIDTH)&&
		(A_Dis_Height>=0)&&(A_Dis_Height<=CAM_IMAGE_HEIGHT)&&
		(A_Total_Width>=0)&&(A_Total_Width<=CAM_IMAGE_WIDTH)&&
		(A_Total_Height>=0)&&(A_Total_Height<=CAM_IMAGE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}


void CRotate_Option::EditWheelIntSet(int nID,int Val)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = 0;
	}else{
		buf = GetDlgItemInt(nID);
		buf+= Val;
	}
	SetDlgItemInt(nID,buf,1);
	((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
}
void CRotate_Option::EditMinMaxIntSet(int nID,int* Val,int Min,int Max)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = Min;
		retval = FALSE;
	}else{
		buf = GetDlgItemInt(nID);
		if(buf<Min){
			buf = *Val;
			retval = TRUE;
		}else if(buf > Max){
			buf = *Val;
			retval = TRUE;
		}else{
			retval = FALSE;
		}
	}
	*Val = buf;
	if(retval == TRUE){
		SetDlgItemInt(nID,buf,1);
		((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
	}
}

BOOL CRotate_Option::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	int znum = zDelta/120;
	CWnd *focusid;
	focusid = GetFocus();
	bool FLAG = FALSE;
	if(znum > 0 ){
		FLAG = 1;
	}
	else if(znum < 0 ){
		FLAG =0;
	}
	else if(znum == 0){
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	}
	int casenum = focusid->GetDlgCtrlID();
	switch(casenum){
		case IDC_Angle_PosX   :
			EditWheelIntSet(IDC_Angle_PosX ,znum);
			OnEnChangeAnglePosx();
			break;
		case IDC_Angle_PosY :
			EditWheelIntSet(IDC_Angle_PosY,znum);
			OnEnChangeAnglePosy();
			break;
		case IDC_Angle_Dis_W :
			EditWheelIntSet(IDC_Angle_Dis_W ,znum);
			OnEnChangeAngleDisW();
			break;
		case IDC_Angle_Dis_H :
			EditWheelIntSet(IDC_Angle_Dis_H,znum);
			OnEnChangeAngleDisH();
			break;
		case  IDC_Angle_Width :
			EditWheelIntSet( IDC_Angle_Width,znum);
			OnEnChangeAngleWidth();
			break;
		case IDC_Angle_Height :
			EditWheelIntSet(IDC_Angle_Height,znum);
			OnEnChangeAngleHeight();
			break;

		case IDC_EDIT_A_MOD :
			if(FLAG == 1){
				if((Change_DATA_CHECK(1) == TRUE) /*|| (znum ==-1)(WheelCheck ==0)*/){
					EditWheelIntSet(IDC_EDIT_A_MOD,znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditAMod();
					//WheelCheck =1;
				}
			}
			else if(FLAG == 0){
				if((Change_DATA_CHECK(0) == TRUE)/* || (znum == 1)(WheelCheck ==1)*/){
					EditWheelIntSet(IDC_EDIT_A_MOD,znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditAMod();
					//WheelCheck = 0;
				}
			}
			

			break;
		case IDC_EDIT_T_MOD:
			if (FLAG == 1){
				if ((Change_DATA_CHECK_T(1) == TRUE) /*|| (znum ==-1)(WheelCheck ==0)*/){
					EditWheelIntSet(IDC_EDIT_T_MOD, znum);
					Change_DATA_TILT();
					UploadList_Tilt();
					OnEnChangeEditTMod();
					//WheelCheck =1;
				}
			}
			else if (FLAG == 0){
				if ((Change_DATA_CHECK_T(0) == TRUE)/* || (znum == 1)(WheelCheck ==1)*/){
					EditWheelIntSet(IDC_EDIT_T_MOD, znum);
					Change_DATA_TILT();
					UploadList_Tilt();
					OnEnChangeEditTMod();
					//WheelCheck = 0;
				}
			}

			break;
	}
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}


bool CRotate_Option::Change_DATA_CHECK(bool FLAG){
 
	BOOL STAT = TRUE;
	//상한
	if(A_RECT[iSavedItem].m_Width > CAM_IMAGE_WIDTH )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Height > CAM_IMAGE_HEIGHT )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_PosX > CAM_IMAGE_WIDTH )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_PosY > CAM_IMAGE_HEIGHT )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Top > CAM_IMAGE_HEIGHT )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Bottom > CAM_IMAGE_HEIGHT  )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Left> CAM_IMAGE_WIDTH )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Right> CAM_IMAGE_WIDTH )
		STAT = FALSE;

	//하한

	if(A_RECT[iSavedItem].m_Width < 1 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Height < 1 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_PosX < 0 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_PosY < 0 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Top < 0 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Bottom < 0  )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Left< 0 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Right< 0 )
		STAT = FALSE;

	if(FLAG  == 1){
		if(A_RECT[iSavedItem].m_Width == 1 )
			STAT = FALSE;
		if(A_RECT[iSavedItem].m_Height == 1 )
			STAT = FALSE;
		
	}


	return STAT;

}
bool CRotate_Option::Change_DATA_CHECK_T(bool FLAG){

	BOOL STAT = TRUE;
	//상한
	if (T_RECT[iSavedItem_T].m_Thresold > 100)
		STAT = FALSE;
	if (T_RECT[iSavedItem_T].m_resultDis> CAM_IMAGE_HEIGHT)
		STAT = FALSE;


	//하한

	if (T_RECT[iSavedItem_T].m_Thresold < 1)
		STAT = FALSE;
	if (T_RECT[iSavedItem_T].m_resultDis < 1)
		STAT = FALSE;

	return STAT;

}
void CRotate_Option::OnEnChangeEditAMod()
{
		int num = iSavedItem;
	if((A_RECT[num].m_PosX >= 0)&&(A_RECT[num].m_PosX <= CAM_IMAGE_WIDTH)&&
		(A_RECT[num].m_PosY >= 0)&&(A_RECT[num].m_PosY <= CAM_IMAGE_HEIGHT)&&
		(A_RECT[num].m_Left>=0)&&(A_RECT[num].m_Left<=CAM_IMAGE_WIDTH)&&
		(A_RECT[num].m_Top>=0)&&(A_RECT[num].m_Top<=CAM_IMAGE_HEIGHT)&&
		(A_RECT[num].m_Right>=0)&&(A_RECT[num].m_Right<=CAM_IMAGE_WIDTH)&&
		(A_RECT[num].m_Bottom>=0)&&(A_RECT[num].m_Bottom<=CAM_IMAGE_HEIGHT)&&
		(A_RECT[num].m_Width>=0)&&(A_RECT[num].m_Width<=CAM_IMAGE_WIDTH)&&
		(A_RECT[num].m_Height>=0)&&(A_RECT[num].m_Height<=CAM_IMAGE_HEIGHT)){

			((CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE))->EnableWindow(1);
		

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE))->EnableWindow(0);
	}
}

void CRotate_Option::OnEnChangeEdit1()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CRotate_Option::OnBnClickedButtontest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(0);
	((CImageTesterDlg  *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(1);

}


#pragma region LOT관련 함수
void CRotate_Option::LOT_Set_List(CListCtrl *List)
{
// 	while(List->DeleteColumn(0));
// 	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
// 
// 	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
// 	List->InsertColumn(0, "NO", LVCFMT_CENTER, 30);
// 	List->InsertColumn(1, "모델명", LVCFMT_CENTER, 100);
// 	List->InsertColumn(2, "LOT CARD", LVCFMT_CENTER, 100);
// 	List->InsertColumn(3, "Start DATE", LVCFMT_CENTER, 80);
// 	List->InsertColumn(4, "Start TIME", LVCFMT_CENTER, 80);
// 	List->InsertColumn(5, "각도", LVCFMT_CENTER, 80);
// 	List->InsertColumn(6, "RESULT", LVCFMT_CENTER, 80);
// 	List->InsertColumn(7, "비고", LVCFMT_CENTER, 80);
// 
// 	Lot_InsertNum =8;
	//Copy_List(&m_Lot_RotateList, ((CImageTesterDlg *)m_pMomWnd)->m_pWorkList, Lot_InsertNum);
}

void CRotate_Option::LOT_InsertDataList()
{	
// 	CString strCnt="";
// 
// 	int Index = m_Lot_RotateList.InsertItem(Lot_StartCnt,"",0);
// 	strCnt.Empty();
// 	strCnt.Format(_T("%d"), Lot_InsertIndex + 1);
// 	m_Lot_RotateList.SetItemText(Lot_InsertIndex, 0, strCnt);
// 
// 	int CopyIndex = m_AngleList.GetItemCount()-1;
// 
// 	int count =0;
// 	for(int t=0; t< Lot_InsertNum;t++){
// 		if((t != 2)&&(t!=0)){
// 			m_Lot_RotateList.SetItemText(Index,t, m_AngleList.GetItemText(CopyIndex,count));
// 			count++;
// 
// 		}else if(t==0){
// 			count++;
// 		}else{
// 			m_Lot_RotateList.SetItemText(Index,t,((CImageTesterDlg  *)m_pMomWnd)->LOT_NUMBER);
// 		}
// 	}
// 
// 	Lot_StartCnt++;

}
void CRotate_Option::LOT_EXCEL_SAVE()
{
// 	CString Item[5] = { "각도", "LT_RT", "RT_RB", "RB_LB", "LB_LT" };
// 	CString Data = "";
// 	CString str = "";
// 	str = "***********************Rotate/Tilt 검사 [ Rotate 단위 : º ] [ Tilt 단위 : Pixel ] ***********************\n";
// 	Data += str;
// 	str.Empty();
// 	str.Format("로테이트 , %6.1f,~,%6.1f, \n ", m_dRotationAngle_min, m_dRotationAngle_max);
// 	Data += str;
// 	str.Empty();
// 	str.Format("LT_RT, %d ,Threshold, ± %d pixel,, ", (int)T_RECT[0].m_resultDis, T_RECT[0].m_Thresold);
// 	Data += str;
// 	str.Empty();
// 	str.Format("RT_RB , %d ,Threshold, ± %d pixel, \n", (int)T_RECT[1].m_resultDis, T_RECT[1].m_Thresold);
// 	Data += str;
// 	str.Empty();
// 	str.Format("RB_LB , %d ,Threshold, ± %d pixel, ,", (int)T_RECT[2].m_resultDis, T_RECT[2].m_Thresold);
// 	Data += str;
// 	str.Empty();
// 	str.Format("LB_LT , %d ,Threshold, ± %d pixel,\n ", (int)T_RECT[3].m_resultDis, T_RECT[3].m_Thresold);
// 	Data += str;
// 	str.Empty();
// 	str = "*************************************************\n";
// 	Data += str;
// 	((CImageTesterDlg  *)m_pMomWnd)->LOT_SAVE_EXCEL("Rotate_Tilt 검사", Item, 5, &m_Lot_RotateList, Data);
}

bool CRotate_Option::LOT_EXCEL_UPLOAD()
{
// 	m_Lot_RotateList.DeleteAllItems();
// 	if (((CImageTesterDlg  *)m_pMomWnd)->LOT_EXCEL_UPLOAD("Rotate_Tilt 검사", &m_Lot_RotateList, 1, &Lot_StartCnt) == TRUE){
// 		return TRUE;	
// 	}
// 	else{
// 		Lot_StartCnt = 0;
// 		return FALSE;	
// 	}
	return TRUE;
}

#pragma endregion

void CRotate_Option::OnBnClickedButtonSave3()
{
	UpdateData(TRUE);
	strTitle.Empty();
	strTitle="ROTATE_INIT";	

	m_dOffset = atof(OFFSET);
	m_dOffset_M = atof(OFFSET_M);

	OFFSET.Format("%6.1f",m_dOffset);
	OFFSET_M.Format("%6.1f",m_dOffset_M);
	UpdateData(FALSE);

	WritePrivateProfileString(str_model,strTitle+"RotationAngle_OFFSET",OFFSET,A_filename);
	WritePrivateProfileString(str_model,strTitle+"RotationAngle_OFFSET_M",OFFSET_M,A_filename);

	CString str;
	str.Format("%d",bOffsetType);
	WritePrivateProfileString(str_model,strTitle+"RotationAngle_OFFSET_TYPE",str,A_filename);

}

void CRotate_Option::OnEnChangeEditMaster()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CRotate_Option::OnBnClickedCheckType()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	UpdateData(TRUE);
	if (bOffsetType == TRUE)
	{
		bOffsetType = FALSE;
	}else{
		bOffsetType = TRUE;
	}
	UpdateData(FALSE);
}

void CRotate_Option::OnEnChangeEditTMod()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CRotate_Option::OnEnKillfocusEditTMod()
{
	CString str = "";
	ChangeCheck_T = 1;
	if ((iSavedItem_T != -1) && (iSavedSubitem_T != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_T_MOD, str));
		List_COLOR_Change_TILT();
		if (A_TILTLIST.GetItemText(iSavedItem_T, iSavedSubitem_T) != str){
			Change_DATA_TILT();
			UploadList_Tilt();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_T_MOD))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_T_MOD))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
	iSavedItem_T = -1;
	iSavedSubitem_T = -1;
	((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
}


void CRotate_Option::OnNMClickListTilt(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}


void CRotate_Option::OnNMCustomdrawListTilt(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//*pResult = 0;

	COLORREF text_color = 0;
	COLORREF bg_color = RGB(255, 255, 255);

	LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	//	lplvcd->iPartId
	switch (lplvcd->nmcd.dwDrawStage){
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;
		return;

		// 배경 혹은 텍스트를 수정한다.
	case CDDS_ITEMPREPAINT:
		// 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
		*pResult = CDRF_NOTIFYITEMDRAW;
		return;

		// 서브 아이템의 배경 혹은 텍스트를 수정한다.
	case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
		if (lplvcd->iSubItem != 0){
			// 1번째 행이라면...

			if (lplvcd->nmcd.dwItemSpec >= 0 || lplvcd->nmcd.dwItemSpec <5){

				text_color = RGB(0, 0, 0);
				bg_color = RGB(255, 255, 255);

			}

			if (ChangeCheck_T == 1){
				if (lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 11){
					for (int t = 0; t<4; t++){
						if (lplvcd->nmcd.dwItemSpec == ChangeItem_T[t]){
							text_color = RGB(0, 0, 0);
							bg_color = RGB(250, 230, 200);
						}
					}
				}

			}

			}


			lplvcd->clrText = text_color;
			lplvcd->clrTextBk = bg_color;

		}
		*pResult = CDRF_NEWFONT;
		return;
}


void CRotate_Option::OnNMDblclkListTilt(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		return;
	}

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//ChangeCheck=1;
	if (pNMITEM->iSubItem == 0 || pNMITEM->iSubItem == -1 || pNMITEM->iItem == -1)
	return;
	iSavedItem_T = pNMITEM->iItem;
	iSavedSubitem_T = pNMITEM->iSubItem;


	if (pNMITEM->iItem != -1)
	{
		if (pNMITEM->iSubItem != 0)
		{
			A_TILTLIST.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			A_TILTLIST.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_T_MOD))->SetWindowText(A_TILTLIST.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_T_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW);
			((CEdit *)GetDlgItem(IDC_EDIT_T_MOD))->SetFocus();

		}
	}
	//List_COLOR_Change();

	*pResult = 0;
}

void CRotate_Option::OnEnChangeRotationAngleOffset()
{
	UpdateData(TRUE);
}
