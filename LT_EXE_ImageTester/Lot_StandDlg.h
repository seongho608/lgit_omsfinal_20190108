#pragma once
#include "resource.h"

// CLot_StandDlg 대화 상자입니다.

class CLot_StandDlg : public CDialog
{
	DECLARE_DYNAMIC(CLot_StandDlg)

public:
	CLot_StandDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CLot_StandDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_WSTAND_LOT };
	void	Setup(CWnd* IN_pMomWnd);

	void	Voltage_text(LPCSTR lpcszString, ...);
	void	Voltage_Value(LPCSTR lpcszString, ...);
	void	Modelname_text(LPCSTR lpcszString, ...);
	void	LOTID_TEXT(LPCSTR lpcszString, ...);
	void	LOTID_VAL(LPCSTR lpcszString, ...);
	void	TESTNUM_TEXT(LPCSTR lpcszString, ...);
	void	TESTNUM_VAL(LPCSTR lpcszString, ...);

	void	LotBtn_Enable(bool Enable);
	void LotexitBtn_Enable(bool Enable);

	CFont font1,font2,font3,font4;
private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnBnClickedButtonLotstart();
	afx_msg void OnBnClickedButtonLotexit();
	afx_msg void OnEnSetfocusModelLname();
	afx_msg void OnEnSetfocusVoltName();
	afx_msg void OnEnSetfocusVoltValue();
	afx_msg void OnEnSetfocusEditTestnumName();
	afx_msg void OnEnSetfocusEditTestnumVal();
	afx_msg void OnEnSetfocusEditLotidVal();
	afx_msg void OnEnSetfocusEditLotidName();
};
