// PopUP2.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "PopUP2.h"


// CPopUP2 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPopUP2, CDialog)

CPopUP2::CPopUP2(CWnd* pParent /*=NULL*/)
	: CDialog(CPopUP2::IDD, pParent)
{

}

CPopUP2::~CPopUP2()
{
}

void CPopUP2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CPopUP2, CDialog)
	ON_BN_CLICKED(IDOK, &CPopUP2::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPopUP2::OnBnClickedCancel)
END_MESSAGE_MAP()


// CPopUP2 메시지 처리기입니다.
void CPopUP2::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CPopUP2::TEXT_CHANGE(LPCTSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_WARNIGTEXT))->SetWindowText(lpcszString);
}
BOOL CPopUP2::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_EDIT_WARNIGTEXT,&Font,300,20);
	TEXT_CHANGE(Warning_TEXT);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CPopUP2::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			OnBnClickedOk();
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CPopUP2::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight;
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}

void CPopUP2::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

void CPopUP2::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}
