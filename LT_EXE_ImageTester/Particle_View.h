#pragma once
#include "afxwin.h"


// CParticle_View 대화 상자입니다.

class CParticle_View : public CDialog
{
	DECLARE_DYNAMIC(CParticle_View)

public:
	CParticle_View(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CParticle_View();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_PARTICLEVIEW };
	void	Setup(CWnd* IN_pMomWnd);
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
private:
	CWnd		*m_pMomWnd;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CStatic m_PaticleViewFrame;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton4();
};
