#pragma once


// COption_ResultStand 대화 상자입니다.

class COption_ResultStand : public CDialog
{
	DECLARE_DYNAMIC(COption_ResultStand)

public:
	COption_ResultStand(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COption_ResultStand();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_STAND };
	void	Setup(CWnd* IN_pMomWnd);
	CFont	font1,font2;
	int		m_voltstat;
	void	Voltage_text(LPCSTR lpcszString, ...);
	void	Voltage_Value(LPCSTR lpcszString, ...);

	COLORREF Val1col,Val2col,Val3col;
	void	CurrState_text(LPCSTR lpcszString, ...);
	void	CurrState_Val(int col,LPCSTR lpcszString, ...);

	void	UpdateEditRect(int parm_edit_id);
	void	UpdateEdit();
	void	InitStat();

	void	Test1_Name(LPCSTR lpcszString, ...);
	void	Test1_Val(int col,LPCSTR lpcszString, ...);
	void	Test2_Name(LPCSTR lpcszString, ...);
	void	Test2_Val(int col,LPCSTR lpcszString, ...);
	void	Test3_Name(LPCSTR lpcszString, ...);
	void	Test3_Val(int col,LPCSTR lpcszString, ...);

private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
//	afx_msg void OnEnChangeEditName1();
	afx_msg void OnEnSetfocusVoltName();
	afx_msg void OnEnSetfocusEditVoltOn();
	afx_msg void OnEnSetfocusVoltValue();
	afx_msg void OnEnSetfocusEditVoltOff();
	afx_msg void OnEnSetfocusEditName1();
	afx_msg void OnEnSetfocusEditName2();
	afx_msg void OnEnSetfocusEditName3();
	afx_msg void OnEnSetfocusEditVal1();
	afx_msg void OnEnSetfocusEditVal2();
	afx_msg void OnEnSetfocusEditVal3();
	afx_msg void OnEnSetfocusEditVoltOn2();
};
