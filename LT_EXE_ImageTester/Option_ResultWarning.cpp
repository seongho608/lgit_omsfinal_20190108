// Option_ResultWarning.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Option_ResultWarning.h"


// COption_ResultWarning 대화 상자입니다.

IMPLEMENT_DYNAMIC(COption_ResultWarning, CDialog)

COption_ResultWarning::COption_ResultWarning(CWnd* pParent /*=NULL*/)
: CDialog(COption_ResultWarning::IDD, pParent)
{

}

COption_ResultWarning::~COption_ResultWarning()
{
}

void COption_ResultWarning::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CH1_WARNING_RESULT, m_listCh1WarningResult);
}


BEGIN_MESSAGE_MAP(COption_ResultWarning, CDialog)
END_MESSAGE_MAP()


// COption_ResultWarning 메시지 처리기입니다.
void COption_ResultWarning::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}
BOOL COption_ResultWarning::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_listCh1WarningResult.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_listCh1WarningResult.InsertColumn(0, "구분", LVCFMT_CENTER, 80);
	m_listCh1WarningResult.InsertColumn(1, "결과", LVCFMT_CENTER, 40);
	m_listCh1WarningResult.InsertColumn(2, "매칭률", LVCFMT_CENTER, 60);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void COption_ResultWarning::InitStat(){

	m_listCh1WarningResult.DeleteAllItems();
}