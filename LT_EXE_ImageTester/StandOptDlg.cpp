// StandOptDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "StandOptDlg.h"


// CStandOptDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CStandOptDlg, CDialog)

CStandOptDlg::CStandOptDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CStandOptDlg::IDD, pParent)
	, REPORT_PATH(_T(""))
	, m_str_cpyname(_T(""))
	, m_str_name(_T(""))
	, m_PW_Change(_T(""))
	, b_InterLock_Enable(FALSE)
	, REPORT_PATH_P(_T(""))
	, IMAGE_PATH(_T(""))
{

}

CStandOptDlg::~CStandOptDlg()
{
}

void CStandOptDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_SaveReport, REPORT_PATH);
	DDX_Text(pDX, IDC_COPYMODEL, m_str_cpyname);
	DDX_Text(pDX, IDC_MODELNAME, m_str_name);
	DDX_Text(pDX, IDC_PASSEDIT, m_PW_Change);
	DDX_Text(pDX, IDC_EDIT_MS_TIME_H, str_MSTime_H);
	DDX_Text(pDX, IDC_EDIT_MS_TIME_M, str_MSTime_M);
	DDX_Check(pDX, IDC_CHECK_INTERLOCK, b_InterLock_Enable);
	DDX_Text(pDX, IDC_SaveReport_P, REPORT_PATH_P);
	DDX_Text(pDX, IDC_SaveImagePath, IMAGE_PATH);
}


BEGIN_MESSAGE_MAP(CStandOptDlg, CDialog)
	ON_BN_CLICKED(IDC_SERIALSTAT_SAVE, &CStandOptDlg::OnBnClickedSerialstatSave)
	ON_EN_CHANGE(IDC_MODELNAME, &CStandOptDlg::OnEnChangeModelname)
	ON_BN_CLICKED(IDC_BTN_MODELSAVE, &CStandOptDlg::OnBnClickedBtnModelsave)
	ON_EN_CHANGE(IDC_COPYMODEL, &CStandOptDlg::OnEnChangeCopymodel)
	ON_BN_CLICKED(IDC_BTN_MODELCOPY, &CStandOptDlg::OnBnClickedBtnModelcopy)
	ON_BN_CLICKED(IDC_BTN_MODELDEL, &CStandOptDlg::OnBnClickedBtnModeldel)
	ON_BN_CLICKED(IDC_ReportPathSave, &CStandOptDlg::OnBnClickedReportpathsave)
	ON_EN_CHANGE(IDC_PASSEDIT, &CStandOptDlg::OnEnChangePassedit)
	ON_BN_CLICKED(IDC_PASSWORD_CHANGE, &CStandOptDlg::OnBnClickedPasswordChange)
	ON_CBN_SELCHANGE(IDC_PORT_CON, &CStandOptDlg::OnCbnSelchangePortCon)
	ON_CBN_SELCHANGE(IDC_PORT_SUB, &CStandOptDlg::OnCbnSelchangePortSub)
	ON_CBN_SELCHANGE(IDC_PORT_CAM, &CStandOptDlg::OnCbnSelchangePortCam)
	ON_CBN_SELCHANGE(IDC_BORDRATE, &CStandOptDlg::OnCbnSelchangeBordrate)
	ON_CBN_SELCHANGE(IDC_DATA_BIT, &CStandOptDlg::OnCbnSelchangeDataBit)
	ON_CBN_SELCHANGE(IDC_PARITY, &CStandOptDlg::OnCbnSelchangeParity)
	ON_CBN_SELCHANGE(IDC_STOP_BIT, &CStandOptDlg::OnCbnSelchangeStopBit)
	ON_BN_CLICKED(IDC_SERIALSTAT_SAVE2, &CStandOptDlg::OnBnClickedSerialstatSave2)
//	ON_CBN_SELCHANGE(IDC_COMOB_MODEL, &CStandOptDlg::OnCbnSelchangeComobModel)
ON_EN_CHANGE(IDC_EDIT_MS_TIME_H, &CStandOptDlg::OnEnChangeEditMsTimeH)
ON_EN_CHANGE(IDC_EDIT_MS_TIME_M, &CStandOptDlg::OnEnChangeEditMsTimeM)
ON_BN_CLICKED(IDC_BUTTON_MS_TIME, &CStandOptDlg::OnBnClickedButtonMsTime)
ON_BN_CLICKED(IDC_CHECK_INTERLOCK, &CStandOptDlg::OnBnClickedCheckInterlock)
ON_BN_CLICKED(IDC_BUTTON1, &CStandOptDlg::OnBnClickedButton1)
ON_BN_CLICKED(IDC_ReportPathSave_P, &CStandOptDlg::OnBnClickedReportpathsaveP)
ON_BN_CLICKED(IDC_ImageSave, &CStandOptDlg::OnBnClickedImagesave)
END_MESSAGE_MAP()


// CStandOptDlg 메시지 처리기입니다.

void CStandOptDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}
BOOL CStandOptDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	pModelCombo = (CComboBox *)GetDlgItem(IDC_COMOB_MODEL);//모델

	pConCombo = (CComboBox *)GetDlgItem(IDC_PORT_CON);
	pSubCombo = (CComboBox *)GetDlgItem(IDC_PORT_SUB);
	pCamCombo = (CComboBox *)GetDlgItem(IDC_PORT_CAM);

	pBaudCombo = (CComboBox *)GetDlgItem(IDC_BORDRATE);
	pDataCombo  = (CComboBox *)GetDlgItem(IDC_DATA_BIT);
	pStopCombo	= (CComboBox *)GetDlgItem(IDC_STOP_BIT);
	pParityCombo = (CComboBox *)GetDlgItem(IDC_PARITY);


	pCamCombo->SetCurSel(((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.Port);
	pConCombo->SetCurSel(((CImageTesterDlg *)m_pMomWnd)->m_ConSerial_Center.Port);
	pSubCombo->SetCurSel(((CImageTesterDlg *)m_pMomWnd)->m_ConSerial_Side.Port);

	pBaudCombo->SetCurSel(((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.BaudRate);
	pDataCombo->SetCurSel(((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.DataBit);
	pStopCombo->SetCurSel(((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.StopBit);
	pParityCombo->SetCurSel(((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.Parity);


	((CButton *)GetDlgItem(IDC_BTN_MODELSAVE))->EnableWindow(0);
	//((CButton *)GetDlgItem(IDC_BTN_MODELCOPY))->EnableWindow(0);
	//((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE))->EnableWindow(0);
	((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE2))->EnableWindow(0);

	IMAGE_PATH = ((CImageTesterDlg *)m_pMomWnd)->SAVE_IMAGEPATH;
	REPORT_PATH = ((CImageTesterDlg *)m_pMomWnd)->SAVE_FOLDERPATH;
	REPORT_PATH_P = ((CImageTesterDlg *)m_pMomWnd)->SAVE_FOLDERPATH_P;
	m_PW_Change = ((CImageTesterDlg *)m_pMomWnd)->MatchPassWord;

	i_MSTime_M = ((CImageTesterDlg *)m_pMomWnd)->i_MST_M;
	str_MSTime_M.Format("%d",i_MSTime_M);

	i_MSTime_H = ((CImageTesterDlg *)m_pMomWnd)->i_MST_H;
	str_MSTime_H.Format("%d",i_MSTime_H);

	InitEVMS();
	
	
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CStandOptDlg::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		pModelCombo->EnableWindow(0);
		pConCombo->EnableWindow(0);
		pSubCombo->EnableWindow(0);
		pCamCombo->EnableWindow(0);
		pBaudCombo->EnableWindow(0);
		pDataCombo->EnableWindow(0);
		pStopCombo->EnableWindow(0);
		pParityCombo->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_MODELSAVE))->EnableWindow(0);
		//((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE2))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_MODELCOPY))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_MODELDEL))->EnableWindow(0);

		((CEdit *)GetDlgItem(IDC_MODELNAME))->EnableWindow(0);
	}
}


BOOL CStandOptDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		if(pMsg->wParam == VK_ADD){
//			::PostMessage(((CCenterPointModify_4CHDlg *)m_pMomWnd)->hMainWnd, WM_COMM_START, 0, 0 );
			return TRUE;
		}
		if (pMsg->wParam == VK_SUBTRACT){
//			::PostMessage(((CCenterPointModify_4CHDlg *)m_pMomWnd)->hMainWnd, WM_COMM_STOP, 0, 0 );
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CStandOptDlg::OnBnClickedSerialstatSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.Port = pCamCombo->GetCurSel();
	((CImageTesterDlg *)m_pMomWnd)->m_ConSerial_Center.Port = pConCombo->GetCurSel();
	((CImageTesterDlg *)m_pMomWnd)->m_ConSerial_Side.Port = pSubCombo->GetCurSel();
	SaveSerialSet(&((CImageTesterDlg *)m_pMomWnd)->m_CamSerial,"CAMSERIAL",((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
	SaveSerialSet(&((CImageTesterDlg *)m_pMomWnd)->m_ConSerial_Center, "CENTER_CONSERIAL", ((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
	SaveSerialSet(&((CImageTesterDlg *)m_pMomWnd)->m_ConSerial_Side, "SIDE_CONSERIAL", ((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
	((CImageTesterDlg *)m_pMomWnd)->PortUpdate();
	//((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE))->EnableWindow(0);
}

void CStandOptDlg::PortUpdate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	pCamCombo->SetCurSel(((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.Port);
	pConCombo->SetCurSel(((CImageTesterDlg *)m_pMomWnd)->m_ConSerial_Center.Port);
	pSubCombo->SetCurSel(((CImageTesterDlg *)m_pMomWnd)->m_ConSerial_Side.Port);
}


void CStandOptDlg::OnEnChangeModelname()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	m_str_name.Remove(' ');
	if(m_str_name != ""){
		((CButton *)GetDlgItem(IDC_BTN_MODELSAVE))->EnableWindow(1);
	}else{
		((CButton *)GetDlgItem(IDC_BTN_MODELSAVE))->EnableWindow(0);
	}
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandOptDlg::OnBnClickedBtnModelsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString ModelPath = "";
	CString ModelFile = "";
	CString example = "";
	CFileFind modelfind;
	UpdateData(TRUE);
	if (m_str_name != ""){
		ModelPath = ((CImageTesterDlg *)m_pMomWnd)->m_model_path +("\\")+m_str_name;
		ModelFile = ModelPath + ("\\")+m_str_name+(".luri");
		folder_gen(ModelPath);
		if(!modelfind.FindFile(ModelFile)){ //원하는 폴더가 없으면 만든다.
			((CImageTesterDlg *)m_pMomWnd)->modelfile_Name = ModelFile;
			((CImageTesterDlg *)m_pMomWnd)->m_newmodelname = m_str_name;
			if(((CImageTesterDlg *)m_pMomWnd)->m_newmodelname != ((CImageTesterDlg *)m_pMomWnd)->m_oldmodelname){
				((CImageTesterDlg *)m_pMomWnd)->m_oldmodelname = m_str_name;
			}
			example.Empty();
			example="TOTAL";
			WritePrivateProfileString(example,NULL,"",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);//모델을 만든다. 
			WritePrivateProfileString("INITPLACE","MODEL",m_str_name,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);//변경된 폴더를 ini에 저장한다. 
		}else{/////////////////동일한 모델이 있을 경우
			AfxMessageBox("동일한 카메라 모델이 있습니다. 다시 입력해주세요.");
			m_str_name = "";
			UpdateData(FALSE);
			((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_HIDE);
			((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_SHOW);
			return;
		}
		((CImageTesterDlg *)m_pMomWnd)->MainModelSel();
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->pModelCombo->SetCurSel(pModelCombo->GetCurSel());
		((CImageTesterDlg *)m_pMomWnd)->m_pMainCombo->SetCurSel(pModelCombo->GetCurSel());
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->OnCbnSelchangeSubModel();
		m_str_name = "";
		UpdateData(FALSE);
	}
}

void CStandOptDlg::OnEnChangeCopymodel()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	/*UpdateData(TRUE);
	m_str_cpyname.Remove(' ');
	if(m_str_cpyname != ""){
		((CButton *)GetDlgItem(IDC_BTN_MODELCOPY))->EnableWindow(1);
	}else{
		((CButton *)GetDlgItem(IDC_BTN_MODELCOPY))->EnableWindow(0);
	}*/
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandOptDlg::OnBnClickedBtnModelcopy()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int index = pModelCombo->GetCurSel();
	CString str;
	if(index < 0){
		return;
	}
	
	pModelCombo->GetLBText(pModelCombo->GetCurSel(),str);

	CModelCopyDlg subdlg;
	subdlg.Setup((CImageTesterDlg *)m_pMomWnd);
	subdlg.str_copyname = str;


	if (subdlg.DoModal()){
	}else{
		AfxMessageBox("모델을 복사하지 않았습니다.");
	}

	
	((CImageTesterDlg *)m_pMomWnd)->MainModelSel();
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->pModelCombo->SetCurSel(pModelCombo->GetCurSel());
		((CImageTesterDlg *)m_pMomWnd)->m_pMainCombo->SetCurSel(pModelCombo->GetCurSel());
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->OnCbnSelchangeSubModel();
	((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_HIDE);
	((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_SHOW);

	
}

void CStandOptDlg::OnBnClickedBtnModeldel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int index = pModelCombo->GetCurSel();
	CString delstr;
	if(index < 0){
		return;
	}
	
	pModelCombo->GetLBText(pModelCombo->GetCurSel(),delstr);

	CPopUP2 subDlg;
	subDlg.Warning_TEXT = "  [  "+ delstr +"  ] 모델을 삭제하시겠습니까?";
	if(subDlg.DoModal() == IDOK){		
		if(((CImageTesterDlg *)m_pMomWnd)->MatchPassWord == ""){
			AfxMessageBox("패스워드가 설정되어있지 않아 삭제 되지 않습니다");
			
		}else{
			
			CUserSwitching subdlg;
			subdlg.Setup((CImageTesterDlg *)m_pMomWnd);
		
			if(subdlg.DoModal() == IDOK){
				//DeleteDir((((CImageTesterDlg *)m_pMomWnd)->m_model_path) + ("\\") + delstr + ("\\") + delstr+(".lurc"));
				DeleteDir((((CImageTesterDlg *)m_pMomWnd)->m_model_path) + ("\\") + delstr);
				
			}
			else{
				AfxMessageBox("패스워드가 틀렸으므로 삭제 되지 않습니다");
			}

			delete subdlg;
		}
		
		
	}
	((CImageTesterDlg *)m_pMomWnd)->MainModelSel();

		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->pModelCombo->SetCurSel(pModelCombo->GetCurSel());
		((CImageTesterDlg *)m_pMomWnd)->m_pMainCombo->SetCurSel(pModelCombo->GetCurSel());
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->OnCbnSelchangeSubModel();
	((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_HIDE);
	((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_SHOW);
}

void CStandOptDlg::OnBnClickedReportpathsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ITEMIDLIST        *pidlBrowse;
	char    SaveFolderPath[2046] = {0,}; 
	BROWSEINFO BrInfo;
	BrInfo.hwndOwner = NULL; 
	BrInfo.pidlRoot = NULL;
	memset( &BrInfo, 0, sizeof(BrInfo) );
	BrInfo.pszDisplayName = SaveFolderPath;
	BrInfo.lpszTitle = "저장할 디렉토리를 선택하세요";
	BrInfo.ulFlags = BIF_RETURNONLYFSDIRS;
	
	// Show Dialog
	 pidlBrowse = ::SHBrowseForFolder(&BrInfo);    

	 if( pidlBrowse != NULL)
	 {
	//   Get Path
		::SHGetPathFromIDList(pidlBrowse, SaveFolderPath);   
		REPORT_PATH = SaveFolderPath;
		((CImageTesterDlg *)m_pMomWnd)->SAVE_FOLDERPATH =SaveFolderPath;

		((CImageTesterDlg *)m_pMomWnd)->CREATE_Folder(((CImageTesterDlg *)m_pMomWnd)->SAVE_FOLDERPATH,0);
	
		CString strDate="";
		strDate.Format("%d_%02d_%02d",((CImageTesterDlg *)m_pMomWnd)->YEAR,((CImageTesterDlg *)m_pMomWnd)->MONTH,((CImageTesterDlg *)m_pMomWnd)->DAY);
		((CImageTesterDlg *)m_pMomWnd)->SaveData_Date =  strDate;
		((CImageTesterDlg *)m_pMomWnd)->CREATE_Folder(strDate,1);
		((CImageTesterDlg *)m_pMomWnd)->ALL_WORKLIST_UPLOAD();

		WritePrivateProfileString("SAVE_FOLDER_PATH",NULL,"",((CImageTesterDlg *)m_pMomWnd)->m_inifilename);	
		WritePrivateProfileString("SAVE_FOLDER_PATH","SAVE_FOL",SaveFolderPath,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
		UpdateData(FALSE);
		UpdateData(TRUE);
	 }
}

void CStandOptDlg::OnEnChangePassedit()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandOptDlg::OnBnClickedPasswordChange()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
    ((CImageTesterDlg *)m_pMomWnd)->MatchPassWord =	m_PW_Change;
	WritePrivateProfileString("PassWord",NULL,"",((CImageTesterDlg *)m_pMomWnd)->m_inifilename);	
	WritePrivateProfileString("PassWord","PW",m_PW_Change,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);

	UpdateData(FALSE);
}

BOOL CStandOptDlg::PortSettingChk_PORT()//모두 같으면 TRUE 틀리면 FALSE
{
	if(
		(((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.Port == pCamCombo->GetCurSel())){
		return TRUE;
	}else{
		return FALSE;
	}
}
BOOL CStandOptDlg::PortSettingChk_CONPORT()//모두 같으면 TRUE 틀리면 FALSE
{
	if (
		(((CImageTesterDlg *)m_pMomWnd)->m_ConSerial_Center.Port == pConCombo->GetCurSel())){
		return TRUE;
	}
	else{
		return FALSE;
	}
}
BOOL CStandOptDlg::PortSettingChk_SUBPORT()//모두 같으면 TRUE 틀리면 FALSE
{
	if (
		(((CImageTesterDlg *)m_pMomWnd)->m_ConSerial_Side.Port == pSubCombo->GetCurSel())){
		return TRUE;
	}
	else{
		return FALSE;
	}
}
BOOL CStandOptDlg::PortSettingChk_HW()//모두 같으면 TRUE 틀리면 FALSE
{
	if((((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.BaudRate == pBaudCombo->GetCurSel())&&
		(((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.DataBit == pDataCombo->GetCurSel())&&
		(((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.Parity == pParityCombo->GetCurSel())&&
		(((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.StopBit == pStopCombo->GetCurSel())){
		return TRUE;
	}else{
		return FALSE;
	}
}
void CStandOptDlg::OnCbnSelchangePortCon()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(PortSettingChk_PORT() == TRUE){
		//((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE))->EnableWindow(0);
	}else{
		((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE))->EnableWindow(1);
	}
}

void CStandOptDlg::OnCbnSelchangePortSub()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(PortSettingChk_PORT() == TRUE){
		//((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE))->EnableWindow(0);
	}else{
		((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE))->EnableWindow(1);
	}
}

void CStandOptDlg::OnCbnSelchangePortCam()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(PortSettingChk_PORT() == TRUE){
		//((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE))->EnableWindow(0);
	}else{
		((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE))->EnableWindow(1);
	}
}



void CStandOptDlg::OnCbnSelchangeBordrate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(PortSettingChk_HW() == TRUE){
		((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE2))->EnableWindow(0);
	}else{
		((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE2))->EnableWindow(1);
	}
}

void CStandOptDlg::OnCbnSelchangeDataBit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(PortSettingChk_HW() == TRUE){
		((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE2))->EnableWindow(0);
	}else{
		((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE2))->EnableWindow(1);
	}
}

void CStandOptDlg::OnCbnSelchangeParity()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(PortSettingChk_HW() == TRUE){
		((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE2))->EnableWindow(0);
	}else{
		((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE2))->EnableWindow(1);
	}
}

void CStandOptDlg::OnCbnSelchangeStopBit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(PortSettingChk_HW() == TRUE){
		((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE2))->EnableWindow(0);
	}else{
		((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE2))->EnableWindow(1);
	}
}

void CStandOptDlg::OnBnClickedSerialstatSave2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다./*

	((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.BaudRate = pBaudCombo->GetCurSel();
	((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.DataBit = pDataCombo->GetCurSel();
	((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.Parity = pParityCombo->GetCurSel();
	((CImageTesterDlg *)m_pMomWnd)->m_CamSerial.StopBit = pStopCombo->GetCurSel();
	
	SaveSerialSet(&((CImageTesterDlg *)m_pMomWnd)->m_CamSerial,"CAMSERIAL",((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
	((CImageTesterDlg *)m_pMomWnd)->PortUpdate();
	((CButton *)GetDlgItem(IDC_SERIALSTAT_SAVE2))->EnableWindow(0);
}

//void CStandOptDlg::OnCbnSelchangeComobModel()
//{
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//}

void CStandOptDlg::OnEnChangeEditMsTimeH()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandOptDlg::OnEnChangeEditMsTimeM()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandOptDlg::OnBnClickedButtonMsTime()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	i_MSTime_H = atoi(str_MSTime_H);
	i_MSTime_M = atoi(str_MSTime_M);
	


	if(i_MSTime_H >24){
		i_MSTime_H = 24;
	}
	if(i_MSTime_H <0){
		i_MSTime_H = 0;
	}

	if(i_MSTime_M >59){
		i_MSTime_M = 59;
	}
	if(i_MSTime_M <0){
		i_MSTime_M = 0;
	}
	str_MSTime_H.Format("%d",i_MSTime_H);
	str_MSTime_M.Format("%d",i_MSTime_M);
	
	UpdateData(FALSE);
	((CImageTesterDlg *)m_pMomWnd)->i_MST_H= i_MSTime_H;
	((CImageTesterDlg *)m_pMomWnd)->i_MST_M= i_MSTime_M;
	

	WritePrivateProfileString("MSTime","MSTime_M",str_MSTime_M,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
	WritePrivateProfileString("MSTime","MSTime_H",str_MSTime_H,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);


}

void CStandOptDlg::OnBnClickedCheckInterlock()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	/*	if(b_InterLock_Enable == 1){
		b_InterLock_Enable =0;
	}
	else{
		b_InterLock_Enable =1;
	}

	UpdateData(FALSE);
	((CImageTesterDlg *)m_pMomWnd)->b_InterLock= b_InterLock_Enable;

	CString str ="";
	str.Format("%d",b_InterLock_Enable);

	WritePrivateProfileString("INTERLOCK","INTERLOCK",str,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);*/
}

void CStandOptDlg::OnBnClickedButton1()
{
	
}

void CStandOptDlg::OnBnClickedReportpathsaveP()
{
	ITEMIDLIST        *pidlBrowse;
	char    SaveFolderPath_P[2046] = {0,}; 
	BROWSEINFO BrInfo;
	BrInfo.hwndOwner = NULL; 
	BrInfo.pidlRoot = NULL;
	memset( &BrInfo, 0, sizeof(BrInfo) );
	BrInfo.pszDisplayName = SaveFolderPath_P;
	BrInfo.lpszTitle = "저장할 디렉토리를 선택하세요";
	BrInfo.ulFlags = BIF_RETURNONLYFSDIRS;

	// Show Dialog
	pidlBrowse = ::SHBrowseForFolder(&BrInfo);    

	if( pidlBrowse != NULL)
	{
		//   Get Path
		::SHGetPathFromIDList(pidlBrowse, SaveFolderPath_P);   
		REPORT_PATH_P= SaveFolderPath_P;
		((CImageTesterDlg *)m_pMomWnd)->SAVE_FOLDERPATH_P =SaveFolderPath_P;

		((CImageTesterDlg *)m_pMomWnd)->CREATE_Folder(((CImageTesterDlg *)m_pMomWnd)->SAVE_FOLDERPATH_P,3);

		CString strDate="";
		strDate.Format("%d_%02d_%02d",((CImageTesterDlg *)m_pMomWnd)->YEAR,((CImageTesterDlg *)m_pMomWnd)->MONTH,((CImageTesterDlg *)m_pMomWnd)->DAY);
		((CImageTesterDlg *)m_pMomWnd)->SaveData_Date =  strDate;
		((CImageTesterDlg *)m_pMomWnd)->CREATE_Folder(strDate,4);
		((CImageTesterDlg *)m_pMomWnd)->ALL_WORKLIST_UPLOAD();

		WritePrivateProfileString("SAVE_FOLDER_PATH_P",NULL,"",((CImageTesterDlg *)m_pMomWnd)->m_inifilename);	
		WritePrivateProfileString("SAVE_FOLDER_PATH_P","SAVE_FOL",SaveFolderPath_P,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
		UpdateData(FALSE);
		UpdateData(TRUE);
	}
}


void CStandOptDlg::OnBnClickedImagesave()
{
	ITEMIDLIST        *pidlBrowse;
	char    SaveFolderPath[2046] = { 0, };
	BROWSEINFO BrInfo;
	BrInfo.hwndOwner = NULL;
	BrInfo.pidlRoot = NULL;
	memset(&BrInfo, 0, sizeof(BrInfo));
	BrInfo.pszDisplayName = SaveFolderPath;
	BrInfo.lpszTitle = "저장할 디렉토리를 선택하세요";
	BrInfo.ulFlags = BIF_RETURNONLYFSDIRS;

	// Show Dialog
	pidlBrowse = ::SHBrowseForFolder(&BrInfo);

	if (pidlBrowse != NULL)
	{
		//   Get Path
		::SHGetPathFromIDList(pidlBrowse, SaveFolderPath);
		IMAGE_PATH = SaveFolderPath;
		((CImageTesterDlg *)m_pMomWnd)->SAVE_IMAGEPATH = SaveFolderPath;

		WritePrivateProfileString("IMAGE_PATH", NULL, "", ((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
		WritePrivateProfileString("IMAGE_PATH", "SAVE_FOL", SaveFolderPath, ((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
		UpdateData(FALSE);
		UpdateData(TRUE);
	}
}
