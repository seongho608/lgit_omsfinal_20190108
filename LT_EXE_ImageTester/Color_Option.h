#pragma once
#include "ETCUSER.H"
#include "afxcmn.h"

// CColor_Option 대화 상자입니다.

class CColor_Option : public CDialog
{
	DECLARE_DYNAMIC(CColor_Option)

public:
	CColor_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CColor_Option();
	CRectData C_RECT[4];
	CRectData C_Original[4];
	double	m_Result_Cal[4];
	void Load_Original_pra();
	CRectData C_CHECKING[4];
// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_COLOR };
	tResultVal Run(void);
	void Pic(CDC *cdc);
	void InitPrm();

	int m_CAM_SIZE_WIDTH;
	int m_CAM_SIZE_HEIGHT;

	BOOL	b_StopFail;
	void FAIL_UPLOAD();
	
	CRectData CD_RECT[4];	//Color Detection
	CRectData Standard_CD_RECT[4];
	void GetColorAreaCoordinate(LPBYTE m_RGBIn);
	double GetDistance(int x1, int y1, int x2, int y2);

	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
	void	StatePrintf(LPCSTR lpcszString, ...);
	
	void	SETLIST();
	void	UploadList();
	void	Save_parameter();
	void	Load_parameter();
	void	Save(int NUM);
	void	Change_DATA();

	int StartCnt;
	bool AutomationMod;
	void Automation_DATA_INPUT();

	int PosX,PosY;
	int InIndex;
	int iSavedItem, iSavedSubitem;
	int iSavedSubitem2;
	int state;
	int EnterState;
	int NewItem,NewSubitem;
	CRect rect;

	bool MasterMod;

	CString C_filename;
	int	m_AVER,m_AVEG,m_AVEB;
	int	m_Comp_AVER_MAX,m_Comp_AVEG_MAX,m_Comp_AVEB_MAX;
	int	m_Comp_AVER_MIN,m_Comp_AVEG_MIN,m_Comp_AVEB_MIN;
	bool AveRGBPic(CDC *cdc,int NUM);
	void AveRGBGen(LPBYTE IN_RGB,int NUM);

	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void InsertList();
	int InsertIndex;
	int ListItemNum;

	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();
	//--
	void List_COLOR_Change();
	bool ChangeCheck;
	int ChangeItem[4];
	int changecount;
	
	void	EditWheelIntSet(int nID,int Val);
	void	EditMinMaxIntSet(int nID,int* Val,int Min,int Max);

	bool Change_DATA_CHECK(bool FLAG);
CvRect	GetCenterPoint(LPBYTE IN_RGB);

	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();

	void	InitEVMS();

	double m_Result_L[4];
	double m_Result_a[4];
	double m_Result_b[4];

#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	int Lot_StartCnt;
	int Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

#pragma endregion

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	int C_Total_PosX;
	int C_Total_PosY;
	int C_Dis_Width;
	int C_Dis_Height;
	int C_Total_Width;
	int C_Total_Height;
	int C_Thresold;
	afx_msg void OnBnClickedButtonsetcolorzone();
	virtual BOOL OnInitDialog();

	CListCtrl C_DATALIST;
	afx_msg void OnNMClickListColor(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonCRectSave();
protected:
//	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
//	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButtonMasterC();
	CListCtrl m_ColorList;
	afx_msg void OnBnClickedCheckColorAuto();
	afx_msg void OnNMCustomdrawListColor(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListColor(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusEditCMod();
//	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedButtonCLoad();
	afx_msg void OnEnChangeColorPosx();
	afx_msg void OnEnChangeColorPosy();
	afx_msg void OnEnChangeColorDisW();
	afx_msg void OnEnChangeColorDisH();
	afx_msg void OnEnChangeColorWidth();
	afx_msg void OnEnChangeColorHeight();
	afx_msg void OnEnChangeColorThresold();
	afx_msg void OnEnChangeEditCMod();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnBnClickedButtontest();
	CListCtrl m_Lot_ColorList;
};

void CModel_Create(CColor_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO);
void CModel_Delete(CColor_Option **pWnd);