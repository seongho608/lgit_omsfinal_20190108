// LGE_CamSet_ParticleDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "LGE_CamSet_ParticleDlg.h"
#include "afxdialogex.h"


// CLGE_CamSet_ParticleDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLGE_CamSet_ParticleDlg, CDialog)

CLGE_CamSet_ParticleDlg::CLGE_CamSet_ParticleDlg(CWnd* pParent /*=NULL*/)
: CDialog(CLGE_CamSet_ParticleDlg::IDD, pParent)
, m_Slope(0)
, m_Alpha(0)
, m_IRMode(0)
, m_IRGain(0)
, m_Removal(0)
, m_PgmGain(0)
, b_PgmGain(FALSE)
, b_Emission(FALSE)
{

}

CLGE_CamSet_ParticleDlg::~CLGE_CamSet_ParticleDlg()
{
}

void CLGE_CamSet_ParticleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_SLOPE, m_Slope);
	DDX_Text(pDX, IDC_EDIT_ALPHA, m_Alpha);
	DDX_Text(pDX, IDC_EDIT_IRMODE, m_IRMode);
	DDX_Text(pDX, IDC_EDIT_IRGAIN, m_IRGain);
	DDX_Text(pDX, IDC_EDIT_REMOVAL, m_Removal);
	DDX_Text(pDX, IDC_EDIT_PGMGAIN, m_PgmGain);
	DDX_Check(pDX, IDC_CHECK_PGMGAINUSE, b_PgmGain);
	DDX_Check(pDX, IDC_CHECK_EMISSION, b_Emission);
}


BEGIN_MESSAGE_MAP(CLGE_CamSet_ParticleDlg, CDialog)
	ON_EN_CHANGE(IDC_EDIT_PGMGAIN, &CLGE_CamSet_ParticleDlg::OnEnChangeEditPgmgain)
	ON_EN_CHANGE(IDC_EDIT_SLOPE, &CLGE_CamSet_ParticleDlg::OnEnChangeEditSlope)
	ON_EN_CHANGE(IDC_EDIT_ALPHA, &CLGE_CamSet_ParticleDlg::OnEnChangeEditAlpha)
	ON_EN_CHANGE(IDC_EDIT_IRMODE, &CLGE_CamSet_ParticleDlg::OnEnChangeEditIrmode)
	ON_EN_CHANGE(IDC_EDIT_IRGAIN, &CLGE_CamSet_ParticleDlg::OnEnChangeEditIrgain)
	ON_EN_CHANGE(IDC_EDIT_REMOVAL, &CLGE_CamSet_ParticleDlg::OnEnChangeEditRemoval)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CLGE_CamSet_ParticleDlg::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_LOAD, &CLGE_CamSet_ParticleDlg::OnBnClickedButtonLoad)
	ON_BN_CLICKED(IDC_BUTTON_APPLY, &CLGE_CamSet_ParticleDlg::OnBnClickedButtonApply)
	ON_BN_CLICKED(IDC_CHECK_EMISSION, &CLGE_CamSet_ParticleDlg::OnBnClickedCheckEmission)
	ON_BN_CLICKED(IDC_BUTTON_WSLOPE, &CLGE_CamSet_ParticleDlg::OnBnClickedButtonWslope)
	ON_BN_CLICKED(IDC_BUTTON_WALPHA, &CLGE_CamSet_ParticleDlg::OnBnClickedButtonWalpha)
	ON_BN_CLICKED(IDC_BUTTON_WIRMODE, &CLGE_CamSet_ParticleDlg::OnBnClickedButtonWirmode)
	ON_BN_CLICKED(IDC_BUTTON_WIRGAIN, &CLGE_CamSet_ParticleDlg::OnBnClickedButtonWirgain)
	ON_BN_CLICKED(IDC_BUTTON_WREMOVAL, &CLGE_CamSet_ParticleDlg::OnBnClickedButtonWremoval)
	ON_BN_CLICKED(IDC_CHECK_PGMGAINUSE, &CLGE_CamSet_ParticleDlg::OnBnClickedCheckPgmgainuse)
	ON_BN_CLICKED(IDC_BUTTON_WPGMGAIN, &CLGE_CamSet_ParticleDlg::OnBnClickedButtonWpgmgain)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// CLGE_CamSet_ParticleDlg 메시지 처리기입니다.
void CLGE_CamSet_ParticleDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd = IN_pMomWnd;
}

BOOL CLGE_CamSet_ParticleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	CAMSETFILE = ((CImageTesterDlg *)m_pMomWnd)->CAMSETFILE;
	Load_parameter();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CLGE_CamSet_ParticleDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

		if (pMsg->wParam == VK_RETURN){

			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}
void CLGE_CamSet_ParticleDlg::Load_parameter()
{
	CString str;
	CString strTitle = _T("CAMSET_PARTICLE");

	b_PgmGain = GetPrivateProfileInt(strTitle, _T("PGMGAIN_USE"), -1, CAMSETFILE);
	if (b_PgmGain == -1){
		b_PgmGain = 1;
		str.Empty();
		str.Format("%d", b_PgmGain);
		WritePrivateProfileString(strTitle, _T("PGMGAIN_USE"), str, CAMSETFILE);
	}

	m_PgmGain = GetPrivateProfileInt(strTitle, _T("PGMGAIN"), -1, CAMSETFILE);
	if (m_PgmGain == -1){
		m_PgmGain = -4;
		str.Empty();
		str.Format("%d", m_PgmGain);
		WritePrivateProfileString(strTitle, _T("PGMGAIN"), str, CAMSETFILE);
	}

	b_Emission = GetPrivateProfileInt(strTitle, _T("EMISSION_USE"), -1, CAMSETFILE);
	if (b_Emission == -1){
		b_Emission = 0;
		str.Empty();
		str.Format("%d", b_Emission);
		WritePrivateProfileString(strTitle, _T("EMISSION_USE"), str, CAMSETFILE);
	}

	m_Slope = GetPrivateProfileInt(strTitle, _T("SLOPE"), -1, CAMSETFILE);
	if (m_Slope == -1){
		m_Slope = 512;
		str.Empty();
		str.Format("%d", m_Slope);
		WritePrivateProfileString(strTitle, _T("SLOPE"), str, CAMSETFILE);
	}

	m_Alpha = GetPrivateProfileInt(strTitle, _T("ALPHA"), -1, CAMSETFILE);
	if (m_Alpha == -1){
		m_Alpha = 300;
		str.Empty();
		str.Format("%d", m_Alpha);
		WritePrivateProfileString(strTitle, _T("ALPHA"), str, CAMSETFILE);
	}

	m_IRMode = GetPrivateProfileInt(strTitle, _T("IRMODE"), -1, CAMSETFILE);
	if (m_IRMode == -1){
		m_IRMode = 1;
		str.Empty();
		str.Format("%d", m_IRMode);
		WritePrivateProfileString(strTitle, _T("IRMODE"), str, CAMSETFILE);
	}

	m_IRGain = GetPrivateProfileInt(strTitle, _T("IRGAIN"), -1, CAMSETFILE);
	if (m_IRGain == -1){
		m_IRGain = 8;
		str.Empty();
		str.Format("%d", m_IRGain);
		WritePrivateProfileString(strTitle, _T("IRGAIN"), str, CAMSETFILE);
	}

	m_Removal = GetPrivateProfileInt(strTitle, _T("REMOVAL"), -1, CAMSETFILE);
	if (m_Removal == -1){
		m_Removal = 64;
		str.Empty();
		str.Format("%d", m_Removal);
		WritePrivateProfileString(strTitle, _T("REMOVAL"), str, CAMSETFILE);
	}
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.b_ViwerGain = b_PgmGain;
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.m_ViwerGain = m_PgmGain;
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.b_Emission = b_Emission;
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.m_Slope = m_Slope;
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.m_Alpha = m_Alpha;
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.b_IRMode = m_IRMode;
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.m_IRGain = m_IRGain;
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.m_Removal = m_Removal;

	UpdateData(FALSE);
}
void CLGE_CamSet_ParticleDlg::Save_parameter()
{
	CString str;
	CString strTitle = _T("CAMSET_PARTICLE");

	str.Empty();
	str.Format("%d", b_PgmGain);
	WritePrivateProfileString(strTitle, _T("PGMGAIN_USE"), str, CAMSETFILE);

	str.Empty();
	str.Format("%d", m_PgmGain);
	WritePrivateProfileString(strTitle, _T("PGMGAIN"), str, CAMSETFILE);

	str.Empty();
	str.Format("%d", b_Emission);
	WritePrivateProfileString(strTitle, _T("EMISSION_USE"), str, CAMSETFILE);

	str.Empty();
	str.Format("%d", b_Emission);
	WritePrivateProfileString(strTitle, _T("EMISSION_USE"), str, CAMSETFILE);

	str.Empty();
	str.Format("%d", m_Slope);
	WritePrivateProfileString(strTitle, _T("SLOPE"), str, CAMSETFILE);

	str.Empty();
	str.Format("%d", m_Alpha);
	WritePrivateProfileString(strTitle, _T("ALPHA"), str, CAMSETFILE);

	str.Empty();
	str.Format("%d", m_IRMode);
	WritePrivateProfileString(strTitle, _T("IRMODE"), str, CAMSETFILE);

	str.Empty();
	str.Format("%d", m_IRGain);
	WritePrivateProfileString(strTitle, _T("IRGAIN"), str, CAMSETFILE);

	str.Empty();
	str.Format("%d", m_Removal);
	WritePrivateProfileString(strTitle, _T("REMOVAL"), str, CAMSETFILE);

	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.b_ViwerGain = b_PgmGain;
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.m_ViwerGain = m_PgmGain;
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.b_Emission = b_Emission;
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.m_Slope = m_Slope;
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.m_Alpha = m_Alpha;
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.b_IRMode = m_IRMode;
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.m_IRGain = m_IRGain;
	((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel.m_Removal = m_Removal;
}


void CLGE_CamSet_ParticleDlg::OnEnChangeEditPgmgain()
{
	m_PgmGain = GetDlgItemInt(IDC_EDIT_PGMGAIN);
	//UpdateData(TRUE);
}


void CLGE_CamSet_ParticleDlg::OnEnChangeEditSlope()
{
	UpdateData(TRUE);
}


void CLGE_CamSet_ParticleDlg::OnEnChangeEditAlpha()
{
	UpdateData(TRUE);
}


void CLGE_CamSet_ParticleDlg::OnEnChangeEditIrmode()
{
	UpdateData(TRUE);
}


void CLGE_CamSet_ParticleDlg::OnEnChangeEditIrgain()
{
	UpdateData(TRUE);
}


void CLGE_CamSet_ParticleDlg::OnEnChangeEditRemoval()
{
	UpdateData(TRUE);
}


void CLGE_CamSet_ParticleDlg::OnBnClickedButtonSave()
{
	Save_parameter();
}


void CLGE_CamSet_ParticleDlg::OnBnClickedButtonLoad()
{
	Load_parameter();
}


void CLGE_CamSet_ParticleDlg::OnBnClickedButtonApply()
{
	UpdateData(TRUE);
	if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK()){
		((CImageTesterDlg *)m_pMomWnd)->SetProperty_TOF1_Emission(b_Emission);
		((CImageTesterDlg *)m_pMomWnd)->SetProperty_TOF1_CaliSlope(m_Slope);
		((CImageTesterDlg *)m_pMomWnd)->SetProperty_TOF1_Alpha(m_Alpha);
		((CImageTesterDlg *)m_pMomWnd)->SetProperty_TOF1_IRMode(m_IRMode);
		((CImageTesterDlg *)m_pMomWnd)->SetProperty_TOF1_IRGain(m_IRGain);
		((CImageTesterDlg *)m_pMomWnd)->SetProperty_TOF1_SmallSignalRemoval(m_Removal);
		((CImageTesterDlg *)m_pMomWnd)->SetUse_Gain(b_PgmGain, m_PgmGain);
	}

}


void CLGE_CamSet_ParticleDlg::OnBnClickedCheckEmission()
{
	UpdateData(TRUE);
	if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
		((CImageTesterDlg *)m_pMomWnd)->SetProperty_TOF1_Emission(b_Emission);
}


void CLGE_CamSet_ParticleDlg::OnBnClickedButtonWslope()
{
	UpdateData(TRUE);
	if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
		((CImageTesterDlg *)m_pMomWnd)->SetProperty_TOF1_CaliSlope(m_Slope);
}


void CLGE_CamSet_ParticleDlg::OnBnClickedButtonWalpha()
{
	UpdateData(TRUE);
	if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
		((CImageTesterDlg *)m_pMomWnd)->SetProperty_TOF1_Alpha(m_Alpha);
}


void CLGE_CamSet_ParticleDlg::OnBnClickedButtonWirmode()
{
	UpdateData(TRUE);
	if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
		((CImageTesterDlg *)m_pMomWnd)->SetProperty_TOF1_IRMode(m_IRMode);
}


void CLGE_CamSet_ParticleDlg::OnBnClickedButtonWirgain()
{
	UpdateData(TRUE);
	if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
		((CImageTesterDlg *)m_pMomWnd)->SetProperty_TOF1_IRGain(m_IRGain);
}


void CLGE_CamSet_ParticleDlg::OnBnClickedButtonWremoval()
{
	UpdateData(TRUE);
	if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
		((CImageTesterDlg *)m_pMomWnd)->SetProperty_TOF1_SmallSignalRemoval(m_Removal);
}


void CLGE_CamSet_ParticleDlg::OnBnClickedCheckPgmgainuse()
{
	UpdateData(TRUE);
	if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
		((CImageTesterDlg *)m_pMomWnd)->SetUse_Gain(b_PgmGain, m_PgmGain);
}

void CLGE_CamSet_ParticleDlg::OnBnClickedButtonWpgmgain()
{
	UpdateData(TRUE);
	if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
		((CImageTesterDlg *)m_pMomWnd)->SetUse_Gain(b_PgmGain, m_PgmGain);
}


void CLGE_CamSet_ParticleDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if (bShow == TRUE)
		Load_parameter();
}
