// Color_Option.cpp : 구현 파일입니다.


#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Color_Option.h"
// CColor_Option 대화 상자입니다.

#define AVERAGE_INDEX		100
#define AVERAGE_INDEX_DET	10

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];

IMPLEMENT_DYNAMIC(CColor_Option, CDialog)

CColor_Option::CColor_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CColor_Option::IDD, pParent)
	, C_Total_PosX(0)
	, C_Total_PosY(0)
	, C_Dis_Width(0)
	, C_Dis_Height(0)
	, C_Total_Width(0)
	, C_Total_Height(0)
	, C_Thresold(0)
{
	iSavedItem=0;
	iSavedSubitem=0;
	iSavedSubitem2=0;
	state=0;
	EnterState=0;
	NewItem =0;NewSubitem=0;
	MasterMod=FALSE;
	InsertIndex=0;
	pTStat = NULL;
	ListItemNum=0;
	AutomationMod=0;
	ChangeCheck=0;
	b_StopFail=FALSE;
	for(int t=0; t<4; t++){
		ChangeItem[t]=-1;
	}
	changecount=0;
	StartCnt=0;

	for(int i=0; i<4; i++)
	{
		m_Result_L[i] = 0;
		m_Result_a[i] = 0;
		m_Result_b[i] = 0;
	}
	
	
//****************Color Area 기준 탐색 점 설정*******************//
	//Standard_CD_RECT[0].m_resultX = 262;	//왼쪽 위 사각형
	//Standard_CD_RECT[0].m_resultY = 153;
	//Standard_CD_RECT[1].m_resultX = 462;	//오른쪽 위 사각형
	//Standard_CD_RECT[1].m_resultY = 153;
	//Standard_CD_RECT[2].m_resultX = 262;	//왼쪽 아래 사각형
	//Standard_CD_RECT[2].m_resultY = 327;
	//Standard_CD_RECT[3].m_resultX = 462;	//오른쪽 아래 사각형
	//Standard_CD_RECT[3].m_resultY = 327;
//****************************************************************//
	Standard_CD_RECT[0].m_resultX = (CAM_IMAGE_WIDTH/2) - ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);        //왼쪽위사각형
    Standard_CD_RECT[0].m_resultY = (CAM_IMAGE_HEIGHT/2) - ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);
    Standard_CD_RECT[1].m_resultX = (CAM_IMAGE_WIDTH/2) + ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);        //오른쪽위사각형
    Standard_CD_RECT[1].m_resultY = (CAM_IMAGE_HEIGHT/2) - ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);
    Standard_CD_RECT[2].m_resultX = (CAM_IMAGE_WIDTH/2) - ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);;        //왼쪽아래사각형
    Standard_CD_RECT[2].m_resultY = (CAM_IMAGE_HEIGHT/2) + ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);
    Standard_CD_RECT[3].m_resultX = (CAM_IMAGE_WIDTH/2) + ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);;        //오른쪽아래사각형
    Standard_CD_RECT[3].m_resultY = (CAM_IMAGE_HEIGHT/2) + ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);


}

CColor_Option::~CColor_Option()
{
//	KillTimer(110);
}

void CColor_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_Color_PosX, C_Total_PosX);
	DDX_Text(pDX, IDC_Color_PosY, C_Total_PosY);
	DDX_Text(pDX, IDC_Color_Dis_W, C_Dis_Width);
	DDX_Text(pDX, IDC_Color_Dis_H, C_Dis_Height);
	DDX_Text(pDX, IDC_Color_Width, C_Total_Width);
	DDX_Text(pDX, IDC_Color_Height, C_Total_Height);
	DDX_Text(pDX, IDC_Color_Thresold, C_Thresold);

	DDX_Control(pDX, IDC_LIST_Color, C_DATALIST);
	DDX_Control(pDX, IDC_LIST_COLORLIST, m_ColorList);
	DDX_Control(pDX, IDC_LIST_COLORLIST_LOT, m_Lot_ColorList);
}


BEGIN_MESSAGE_MAP(CColor_Option, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_setColorZone, &CColor_Option::OnBnClickedButtonsetcolorzone)
	ON_NOTIFY(NM_CLICK, IDC_LIST_Color, &CColor_Option::OnNMClickListColor)
	ON_BN_CLICKED(IDC_BUTTON_C_RECT_SAVE, &CColor_Option::OnBnClickedButtonCRectSave)
//	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_MASTER_C, &CColor_Option::OnBnClickedButtonMasterC)
	ON_BN_CLICKED(IDC_CHECK_COLOR_AUTO, &CColor_Option::OnBnClickedCheckColorAuto)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_Color, &CColor_Option::OnNMCustomdrawListColor)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_Color, &CColor_Option::OnNMDblclkListColor)
	ON_EN_KILLFOCUS(IDC_EDIT_C_MOD, &CColor_Option::OnEnKillfocusEditCMod)
//	ON_WM_SETFOCUS()
ON_WM_SHOWWINDOW()
ON_BN_CLICKED(IDC_BUTTON_C_Load, &CColor_Option::OnBnClickedButtonCLoad)
ON_WM_MOUSEWHEEL()
ON_EN_CHANGE(IDC_Color_PosX, &CColor_Option::OnEnChangeColorPosx)
ON_EN_CHANGE(IDC_Color_PosY, &CColor_Option::OnEnChangeColorPosy)
ON_EN_CHANGE(IDC_Color_Dis_W, &CColor_Option::OnEnChangeColorDisW)
ON_EN_CHANGE(IDC_Color_Dis_H, &CColor_Option::OnEnChangeColorDisH)
ON_EN_CHANGE(IDC_Color_Width, &CColor_Option::OnEnChangeColorWidth)
ON_EN_CHANGE(IDC_Color_Height, &CColor_Option::OnEnChangeColorHeight)
ON_EN_CHANGE(IDC_Color_Thresold, &CColor_Option::OnEnChangeColorThresold)
ON_EN_CHANGE(IDC_EDIT_C_MOD, &CColor_Option::OnEnChangeEditCMod)
ON_BN_CLICKED(IDC_BUTTON_test, &CColor_Option::OnBnClickedButtontest)
END_MESSAGE_MAP()
// CColor_Option 메시지 처리기입니다.

void CColor_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}



void CColor_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);;
}


BOOL CColor_Option::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;

	((CEdit *) GetDlgItem(IDC_EDIT_C_MOD))->ShowWindow(FALSE);
	SETLIST();
	
	//C_filename=((CImageTesterDlg *)m_pMomWnd)->genfolderpath +"\\ColorTEST.ini";
	C_filename=((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	Load_parameter();
	UpdateData(FALSE);
	UploadList();
//	SetTimer(110,100,NULL);

	Set_List(&m_ColorList);
	LOT_Set_List(&m_Lot_ColorList);

	if(AutomationMod == 1){
		AutomationMod =0;	
		OnBnClickedCheckColorAuto();
		CheckDlgButton(IDC_CHECK_COLOR_AUTO,TRUE);
	}
	InitEVMS();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CColor_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CButton *)GetDlgItem(IDC_BUTTON_MASTER_C))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_C_Load))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_C_RECT_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_COLOR_AUTO))->EnableWindow(0);
		
		((CEdit *)GetDlgItem(IDC_Color_PosX))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Color_PosY))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Color_Dis_W))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Color_Dis_H))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Color_Width))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Color_Height))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Color_Thresold))->EnableWindow(0);
	}
}

void CColor_Option::SETLIST()
{
	C_DATALIST.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	C_DATALIST.InsertColumn(0,"Zone",LVCFMT_CENTER, 80);
	C_DATALIST.InsertColumn(1,"PosX",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(2,"PosY",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(3,"ST X",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(4,"ST Y",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(5,"EX X",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(6,"EX Y",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(7,"W",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(8,"H",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(9,"L",LVCFMT_CENTER, 60);
	C_DATALIST.InsertColumn(10,"a",LVCFMT_CENTER, 60);
	C_DATALIST.InsertColumn(11,"b",LVCFMT_CENTER, 60);
	C_DATALIST.InsertColumn(12,"Threshold",LVCFMT_CENTER, 70);



	//C_DATALIST.InsertColumn(9,"Thresold",LVCFMT_CENTER, 40);
	//C_DATALIST.InsertColumn(9,"Min R",LVCFMT_CENTER, 42);
	//C_DATALIST.InsertColumn(10,"Max R",LVCFMT_CENTER, 42);
	//C_DATALIST.InsertColumn(11,"Min G",LVCFMT_CENTER, 42);
	//C_DATALIST.InsertColumn(12,"Max G",LVCFMT_CENTER, 42);
	//C_DATALIST.InsertColumn(13,"Min B",LVCFMT_CENTER, 42);
	//C_DATALIST.InsertColumn(14,"Max B",LVCFMT_CENTER, 42);

}

void CColor_Option::OnBnClickedButtonsetcolorzone()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeCheck=1;
	UpdateData(TRUE);
	if(AutomationMod ==0){
		PosX=C_Total_PosX;
		PosY=C_Total_PosY;
		
		C_RECT[0].m_PosX = PosX-C_Dis_Width;
		C_RECT[0].m_PosY = PosY-C_Dis_Height;
		C_RECT[1].m_PosX = PosX+C_Dis_Width;
		C_RECT[1].m_PosY = PosY-C_Dis_Height;
		C_RECT[2].m_PosX = PosX-C_Dis_Width;
		C_RECT[2].m_PosY = PosY+C_Dis_Height;
		C_RECT[3].m_PosX = PosX+C_Dis_Width;
		C_RECT[3].m_PosY = PosY+C_Dis_Height;
	}
	for(int t=0; t<4; t++){
		C_RECT[t].m_Width= C_Total_Width;
		C_RECT[t].m_Height=C_Total_Height;
		C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);//////C
		C_RECT[t].m_Thresold = C_Thresold;
	}
	UpdateData(FALSE);

	UploadList();//리스트컨트롤의 입력

	
	for(int t=0; t<4; t++){
		ChangeItem[t]=t;
	}
	for(int t=0; t<4; t++){
		C_DATALIST.Update(t);
	}


	//Save_parameter();
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
}

void CColor_Option::UploadList(){
	CString str="";
	
	C_DATALIST.DeleteAllItems();

	for(int t=0; t<4; t++){
		InIndex = C_DATALIST.InsertItem(t,"초기",0);
		
 	
		if(t ==0){
			str.Empty();str="L_TOP";
		}
		if(t ==1){
			str.Empty();str="R_TOP";
		}
		if(t ==2){
			str.Empty();str="L_BOTTOM";
		}
		if(t ==3){
			str.Empty();str="R_BOTTOM";
		}
		
		C_DATALIST.SetItemText(InIndex,0,str);
		str.Empty();str.Format("%d", C_RECT[t].m_PosX);
		C_DATALIST.SetItemText(InIndex,1,str);
		str.Empty();str.Format("%d", C_RECT[t].m_PosY);
		C_DATALIST.SetItemText(InIndex,2,str);
		
		str.Empty();
		str.Format("%d", C_RECT[t].m_Left);
		C_DATALIST.SetItemText(InIndex,3,str);
		str.Empty();
		str.Format("%d", C_RECT[t].m_Top);
		C_DATALIST.SetItemText(InIndex,4,str);
	
		str.Empty();
		str.Format("%d", C_RECT[t].m_Right+1);
		C_DATALIST.SetItemText(InIndex,5,str);
		str.Empty();
		str.Format("%d", C_RECT[t].m_Bottom+1);
		C_DATALIST.SetItemText(InIndex,6,str);

		str.Empty();
		str.Format("%d", C_RECT[t].m_Width);
		C_DATALIST.SetItemText(InIndex,7,str);
		str.Empty();
		str.Format("%d",C_RECT[t].m_Height);
		C_DATALIST.SetItemText(InIndex,8,str);
		str.Empty();
		str.Format("%0.2f", C_RECT[t].m_L);
		C_DATALIST.SetItemText(InIndex,9,str);
		str.Empty();
		str.Format("%0.2f", C_RECT[t].m_a);
		C_DATALIST.SetItemText(InIndex,10,str);
		str.Empty();
		str.Format("%0.2f",C_RECT[t].m_b);
		C_DATALIST.SetItemText(InIndex,11,str);
		str.Empty();
		str.Format("%0.2f",C_RECT[t].d_Thresold);
		C_DATALIST.SetItemText(InIndex,12,str);

	/*	str.Empty();
		str.Format("%d",C_RECT[t].m_MinB);
		C_DATALIST.SetItemText(InIndex,13,str);
		str.Empty();
		str.Format("%d",C_RECT[t].m_MaxB);
		C_DATALIST.SetItemText(InIndex,14,str);*/
	}
}

void CColor_Option::Save_parameter(){

	CString str="";
	CString strTitle="";

	//WritePrivateProfileString(str_model,NULL,"",C_filename);
	str.Empty();
	str.Format("%d",m_INFO.ID);
	WritePrivateProfileString(str_model,"ID",str,C_filename);
	str.Empty();
	str.Format("%s",m_INFO.MODE);
	WritePrivateProfileString(str_model,"MODE",str,C_filename);
	str.Empty();
	str.Format("%s",m_INFO.NAME);
	WritePrivateProfileString(str_model,"NAME",str,C_filename);

	strTitle.Empty();
	strTitle="COLOR_INIT";

	str.Empty();
	str.Format("%d",C_Total_PosX);
	WritePrivateProfileString(str_model,strTitle+"PosX",str,C_filename);
	str.Empty();
	str.Format("%d",C_Total_PosY);
	WritePrivateProfileString(str_model,strTitle+"PosY",str,C_filename);
	str.Empty();
	str.Format("%d",C_Dis_Width);
	WritePrivateProfileString(str_model,strTitle+"DisW",str,C_filename);
	str.Empty();
	str.Format("%d",C_Dis_Height);
	WritePrivateProfileString(str_model,strTitle+"DisH",str,C_filename);
	str.Empty();
	str.Format("%d",C_Total_Width);
	WritePrivateProfileString(str_model,strTitle+"Width",str,C_filename);
	str.Empty();
	str.Format("%d",C_Total_Height);
	WritePrivateProfileString(str_model,strTitle+"Height",str,C_filename);
	str.Empty();
	str.Format("%d",C_Thresold);
	WritePrivateProfileString(str_model,strTitle+"Thresold",str,C_filename);

	if(AutomationMod ==1){
		WritePrivateProfileString(str_model,strTitle+"Auto","1",C_filename);}
	if(AutomationMod ==0){
		WritePrivateProfileString(str_model,strTitle+"Auto","0",C_filename);}

	for(int t=0; t<4; t++){
	
		if(t ==0){
			strTitle.Empty();
			strTitle="LEFT_TOP_";
		}
		if(t ==1){
			strTitle.Empty();
			strTitle="RIGHT_TOP_";		
		}
		if(t ==2){
			strTitle.Empty();
			strTitle="LEFT_BOTTOM_";
		}
		if(t ==3){
			strTitle.Empty();
			strTitle="RIGHT_BOTTOM_";
		}

		str.Empty();
		str.Format("%d",C_RECT[t].m_PosX);
		WritePrivateProfileString(str_model,strTitle+"PosX",str,C_filename);
		C_Original[t].m_PosX = C_RECT[t].m_PosX;
		str.Empty();
		str.Format("%d",C_RECT[t].m_PosY);
		WritePrivateProfileString(str_model,strTitle+"PosY",str,C_filename);
		C_Original[t].m_PosY = C_RECT[t].m_PosY;
		str.Empty();
		str.Format("%d",C_RECT[t].m_Left);
		WritePrivateProfileString(str_model,strTitle+"StartX",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_Top);
		WritePrivateProfileString(str_model,strTitle+"StartY",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_Right);
		WritePrivateProfileString(str_model,strTitle+"ExitX",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_Bottom);
		WritePrivateProfileString(str_model,strTitle+"ExitY",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_Width);
		WritePrivateProfileString(str_model,strTitle+"Width",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_Height);
		WritePrivateProfileString(str_model,strTitle+"Height",str,C_filename);
		str.Empty();
		str.Format("%0.2f",C_RECT[t].m_L);
		WritePrivateProfileString(str_model,strTitle+"_L",str,C_filename);
		str.Empty();
		str.Format("%0.2f",C_RECT[t].m_a);
		WritePrivateProfileString(str_model,strTitle+"_A",str,C_filename);
		str.Empty();
		str.Format("%0.2f",C_RECT[t].m_b);
		WritePrivateProfileString(str_model,strTitle+"_B",str,C_filename);
		str.Empty();
		str.Format("%0.2f",C_RECT[t].d_Thresold);
		WritePrivateProfileString(str_model,strTitle+"_Threshold",str,C_filename);

		/*str.Empty();
		str.Format("%d",C_RECT[t].m_Thresold);
		WritePrivateProfileString(str_model,strTitle+"Thresold",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_MinR);
		WritePrivateProfileString(str_model,strTitle+"min_R",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_MinG);
		WritePrivateProfileString(str_model,strTitle+"min_G",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_MinB);
		WritePrivateProfileString(str_model,strTitle+"min_B",str,C_filename);

		str.Empty();
		str.Format("%d",C_RECT[t].m_MaxR);
		WritePrivateProfileString(str_model,strTitle+"max_R",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_MaxG);
		WritePrivateProfileString(str_model,strTitle+"max_G",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_MaxB);
		WritePrivateProfileString(str_model,strTitle+"max_B",str,C_filename);*/
	}
}


void CColor_Option::Load_parameter(){
	CFileFind filefind;
	CString str="";
	CString strTitle="";

	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	if((GetPrivateProfileCString(str_model,"NAME",C_filename) != m_INFO.NAME)||(GetPrivateProfileCString(str_model,"MODE",C_filename) != m_INFO.MODE)){//ini파일이 없으면 파일을 생성한다.
		C_Total_PosX=Cam_PosX;
		C_Total_PosY=Cam_PosY;
		C_Dis_Width=90;
		C_Dis_Height=90;
		C_Total_Width=((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.015;
		C_Total_Height=((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.015;
		C_Thresold =15;

		C_RECT[0].m_PosX=Cam_PosX-C_Dis_Width;
		C_RECT[0].m_PosY=Cam_PosY-C_Dis_Height;
		C_RECT[0].m_Width=C_Total_Width;
		C_RECT[0].m_Height=C_Total_Height;

		C_RECT[1].m_PosX=Cam_PosX+C_Dis_Width;
		C_RECT[1].m_PosY=Cam_PosY-C_Dis_Height;
		C_RECT[1].m_Width=C_Total_Width;
		C_RECT[1].m_Height=C_Total_Height;

		C_RECT[2].m_PosX=Cam_PosX-C_Dis_Width;
		C_RECT[2].m_PosY=Cam_PosY+C_Dis_Height;
		C_RECT[2].m_Width=C_Total_Width;
		C_RECT[2].m_Height=C_Total_Height;

		C_RECT[3].m_PosX=Cam_PosX+C_Dis_Width;
		C_RECT[3].m_PosY=Cam_PosY+C_Dis_Height;
		C_RECT[3].m_Width=C_Total_Width;
		C_RECT[3].m_Height=C_Total_Height;
		//-----------------------------
		C_Original[0].m_PosX=Cam_PosX-C_Dis_Width;
		C_Original[0].m_PosY=Cam_PosY-C_Dis_Height;
		C_Original[1].m_PosX=Cam_PosX+C_Dis_Width;
		C_Original[1].m_PosY=Cam_PosY-C_Dis_Height;
		C_Original[2].m_PosX=Cam_PosX-C_Dis_Width;;
		C_Original[2].m_PosY=Cam_PosY+C_Dis_Height;
		C_Original[3].m_PosX=Cam_PosX+C_Dis_Width;
		C_Original[3].m_PosY=Cam_PosY+C_Dis_Height;

		AutomationMod =0;
		
		for(int t=0; t<4; t++){
			C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);//////C
			
			/*C_RECT[t].m_Thresold = 15;
			C_RECT[t].m_MaxR = 255;
			C_RECT[t].m_MaxG = 255;
			C_RECT[t].m_MaxB = 255;

			C_RECT[t].m_MinR = 0;
			C_RECT[t].m_MinG = 0;
			C_RECT[t].m_MinB = 0;*/

			C_RECT[t].d_Thresold = 3.0;
			C_RECT[t].m_L = 0;
			C_RECT[t].m_a = 0;
			C_RECT[t].m_b = 0;
		}

		Save_parameter();
	}else{

		strTitle.Empty();
		strTitle="COLOR_INIT";
		C_Total_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,C_filename);
		if(C_Total_PosX == -1){
			C_Total_PosX =Cam_PosX;
			str.Empty();
			str.Format("%d",C_Total_PosX);
			WritePrivateProfileString(str_model,strTitle+"PosX",str,C_filename);
		}
		C_Total_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,C_filename);
		if(C_Total_PosY == -1){
			C_Total_PosY =Cam_PosY;
			str.Empty();
			str.Format("%d",C_Total_PosY);
			WritePrivateProfileString(str_model,strTitle+"PosX",str,C_filename);
		}
		C_Dis_Width=GetPrivateProfileInt(str_model,strTitle+"DisW",-1,C_filename);
		if(C_Dis_Width == -1){
			C_Dis_Width =90;
			str.Empty();
			str.Format("%d",C_Dis_Width);
			WritePrivateProfileString(str_model,strTitle+"DisW",str,C_filename);
		}
		C_Dis_Height=GetPrivateProfileInt(str_model,strTitle+"DisH",-1,C_filename);
		if(C_Dis_Height == -1){
			C_Dis_Height =90;
			str.Empty();
			str.Format("%d",C_Dis_Height);
			WritePrivateProfileString(str_model,strTitle+"DisH",str,C_filename);
		}
		C_Total_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,C_filename);
		if(C_Total_Width == -1){
			C_Total_Width =((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.015;
			str.Empty();
			str.Format("%d",C_Total_Width);
			WritePrivateProfileString(str_model,strTitle+"Width",str,C_filename);
		}
		C_Total_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,C_filename);
		if(C_Total_Height == -1){
			C_Total_Height =((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.015;
			str.Empty();
			str.Format("%d",C_Total_Height);
			WritePrivateProfileString(str_model,strTitle+"Height",str,C_filename);
		}
		C_Thresold=GetPrivateProfileInt(str_model,strTitle+"Thresold",-1,C_filename);
		if(C_Thresold == -1){
			C_Thresold =15;
			str.Empty();
			str.Format("%d",C_Thresold);
			WritePrivateProfileString(str_model,strTitle+"Thresold",str,C_filename);
		}

		CString Mod = GetPrivateProfileCString(str_model,strTitle+"Auto",C_filename);
		if("1" == Mod){
			AutomationMod = 1;
		}
		if("0" == Mod){
			AutomationMod = 0;
		}

		for(int t=0; t<4; t++){
			if(t ==0){
				strTitle.Empty();
				strTitle="LEFT_TOP_";
			}
			if(t ==1){
				strTitle.Empty();
				strTitle="RIGHT_TOP_";		
			}
			if(t ==2){
				strTitle.Empty();
				strTitle="LEFT_BOTTOM_";
			}
			if(t ==3){
				strTitle.Empty();
				strTitle="RIGHT_BOTTOM_";
			}
			C_RECT[t].m_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,C_filename);
			if(C_Original[t].m_PosX == -1){
				C_Total_PosX=Cam_PosX;
				C_Total_PosY=Cam_PosY;
				C_Dis_Width=90;
				C_Dis_Height=90;
				C_Total_Width=((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.015;
				C_Total_Height=((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.015;
				C_Thresold =15;

				C_RECT[0].m_PosX=Cam_PosX-C_Dis_Width;
				C_RECT[0].m_PosY=Cam_PosY-C_Dis_Height;
				C_RECT[0].m_Width=C_Total_Width;
				C_RECT[0].m_Height=C_Total_Height;

				C_RECT[1].m_PosX=Cam_PosX+C_Dis_Width;
				C_RECT[1].m_PosY=Cam_PosY-C_Dis_Height;
				C_RECT[1].m_Width=C_Total_Width;
				C_RECT[1].m_Height=C_Total_Height;

				C_RECT[2].m_PosX=Cam_PosX-C_Dis_Width;
				C_RECT[2].m_PosY=Cam_PosY+C_Dis_Height;
				C_RECT[2].m_Width=C_Total_Width;
				C_RECT[2].m_Height=C_Total_Height;

				C_RECT[3].m_PosX=Cam_PosX+C_Dis_Width;
				C_RECT[3].m_PosY=Cam_PosY+C_Dis_Height;
				C_RECT[3].m_Width=C_Total_Width;
				C_RECT[3].m_Height=C_Total_Height;
				//-----------------------------
				C_Original[0].m_PosX=Cam_PosX-C_Dis_Width;
				C_Original[0].m_PosY=Cam_PosY-C_Dis_Height;
				C_Original[1].m_PosX=Cam_PosX+C_Dis_Width;
				C_Original[1].m_PosY=Cam_PosY-C_Dis_Height;
				C_Original[2].m_PosX=Cam_PosX-C_Dis_Width;;
				C_Original[2].m_PosY=Cam_PosY+C_Dis_Height;
				C_Original[3].m_PosX=Cam_PosX+C_Dis_Width;
				C_Original[3].m_PosY=Cam_PosY+C_Dis_Height;

				for(int t=0; t<4; t++){
					C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);//////C
					/*C_RECT[t].m_Thresold = 15;
					C_RECT[t].m_MaxR = 255;
					C_RECT[t].m_MaxG = 255;
					C_RECT[t].m_MaxB = 255;

					C_RECT[t].m_MinR = 0;
					C_RECT[t].m_MinG = 0;
					C_RECT[t].m_MinB = 0;*/

					C_RECT[t].d_Thresold = 3.0;
					C_RECT[t].m_L = 0;
					C_RECT[t].m_a = 0;
					C_RECT[t].m_b = 0;
				}

				Save_parameter();
			}

			C_Original[t].m_PosX = C_RECT[t].m_PosX;
			C_RECT[t].m_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,C_filename);
			C_Original[t].m_PosY = C_RECT[t].m_PosY;
			C_RECT[t].m_Left=GetPrivateProfileInt(str_model,strTitle+"StartX",-1,C_filename);
			C_RECT[t].m_Top=GetPrivateProfileInt(str_model,strTitle+"StartY",-1,C_filename);
			C_RECT[t].m_Right=GetPrivateProfileInt(str_model,strTitle+"ExitX",-1,C_filename);
			C_RECT[t].m_Bottom=GetPrivateProfileInt(str_model,strTitle+"ExitY",-1,C_filename);
			C_RECT[t].m_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,C_filename);
			C_RECT[t].m_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,C_filename);
			
			C_RECT[t].m_L=GetPrivateProfileDouble(str_model,strTitle+"_L",0,C_filename);
			C_RECT[t].m_a=GetPrivateProfileDouble(str_model,strTitle+"_a",0,C_filename);
			C_RECT[t].m_b=GetPrivateProfileDouble(str_model,strTitle+"_b",0,C_filename);
			C_RECT[t].d_Thresold=GetPrivateProfileDouble(str_model,strTitle+"_Threshold",3.0,C_filename);

			//C_RECT[t].m_Thresold=GetPrivateProfileInt(str_model,strTitle+"Thresold",-1,C_filename);	
			//C_RECT[t].m_MinR=GetPrivateProfileInt(str_model,strTitle+"min_R",-1,C_filename);
			//C_RECT[t].m_MinG=GetPrivateProfileInt(str_model,strTitle+"min_G",-1,C_filename);
			//C_RECT[t].m_MinB=GetPrivateProfileInt(str_model,strTitle+"min_B",-1,C_filename);
			//C_RECT[t].m_MaxR=GetPrivateProfileInt(str_model,strTitle+"max_R",-1,C_filename);
			//C_RECT[t].m_MaxG=GetPrivateProfileInt(str_model,strTitle+"max_G",-1,C_filename);
			//C_RECT[t].m_MaxB=GetPrivateProfileInt(str_model,strTitle+"max_B",-1,C_filename);
			
			Standard_CD_RECT[t].m_resultX = C_RECT[t].m_Left + C_RECT[t].m_Width/2;
			Standard_CD_RECT[t].m_resultY = C_RECT[t].m_Top + C_RECT[t].m_Height/2;
		}
	}
}
void CColor_Option::OnNMClickListColor(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;
}


void CColor_Option::OnNMDblclkListColor(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		return;
	}

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(pNMITEM->iSubItem == 0 || pNMITEM->iSubItem == -1 || pNMITEM->iItem == -1 ){return ;}
	
	iSavedItem = pNMITEM->iItem;
	iSavedSubitem = pNMITEM->iSubItem;
	
	if(pNMITEM->iItem != -1)
	{
		if(pNMITEM->iSubItem != 0)
		{		
			C_DATALIST.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			C_DATALIST.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_C_MOD))->SetWindowText(C_DATALIST.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_C_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
			((CEdit *)GetDlgItem(IDC_EDIT_C_MOD))->SetFocus();			
		}
	}

	

	*pResult = 0;
}
void CColor_Option::List_COLOR_Change(){
	bool FLAG = TRUE;
	int Check=0;

	for(int t=0;t<4; t++){
		if(ChangeItem[t]==0)Check++;
		if(ChangeItem[t]==1)Check++;
		if(ChangeItem[t]==2)Check++;
		if(ChangeItem[t]==3)Check++;
	}

	if(Check !=4){
		FLAG =FALSE;
		ChangeItem[changecount]=iSavedItem;
	}

	if(!FLAG){
		for(int t=0; t<changecount+1; t++){
			for(int k=0; k<changecount+1; k++){
				
				if(ChangeItem[t] == ChangeItem[k]){
					if(t==k){break;	}			
					ChangeItem[k] =-1;
				}
			
			
			}
		}
		//----------------데이터 정렬
		int temp=0;

		for(int i=0; i<4; i++)
		{
			for(int j=i+1; j<4; j++)
			{
				if(ChangeItem[i] < ChangeItem[j])  // 내림차순
				{
					temp = ChangeItem[i];
					ChangeItem[i] = ChangeItem[j];
					ChangeItem[j] = temp;
				}
			}
		}
		//--------------------------------
		for(int t=0; t<4; t++){

			if(ChangeItem[t] ==-1){
				changecount =t;
				break;
			}
		}
	}

	for(int t=0; t<4; t++){
		C_DATALIST.Update(t);
	}

}
void CColor_Option::OnEnKillfocusEditCMod()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	ChangeCheck=1;
	if((iSavedItem != -1)&&(iSavedSubitem != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_C_MOD, str));
			List_COLOR_Change();
		if(C_DATALIST.GetItemText(iSavedItem, iSavedSubitem) != str){
			Change_DATA();
			UploadList();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_C_MOD))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_C_MOD))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );
	iSavedItem = -1;
	iSavedSubitem = -1;
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
}



void CColor_Option::OnBnClickedButtonCRectSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Save_parameter();
	ChangeCheck =0;
	for(int t=0; t<4; t++){
		ChangeItem[t]=-1;
	}
	for(int t=0; t<4; t++){
		C_DATALIST.Update(t);
	}
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
}

void CColor_Option::Change_DATA(){

		CString str;
		
		((CEdit *)GetDlgItemText(IDC_EDIT_C_MOD, str));
		C_DATALIST.SetItemText(iSavedItem, iSavedSubitem, str);

		int num = _ttoi(str);
		double num2 = atof(str);
		if(num < 0){
			num =0;
		}

		if(iSavedSubitem ==1){
			C_RECT[iSavedItem].m_PosX = num;
			
		}
		if(iSavedSubitem ==2){
			C_RECT[iSavedItem].m_PosY = num;
			
		}
		if(iSavedSubitem ==3){
			C_RECT[iSavedItem].m_Left = num;
			C_RECT[iSavedItem].m_PosX=(C_RECT[iSavedItem].m_Right+1 +C_RECT[iSavedItem].m_Left) /2;
			C_RECT[iSavedItem].m_Width=C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;

		}
		if(iSavedSubitem ==4){
			C_RECT[iSavedItem].m_Top = num;
			C_RECT[iSavedItem].m_PosY=(C_RECT[iSavedItem].m_Top +C_RECT[iSavedItem].m_Bottom+1)/2;
			C_RECT[iSavedItem].m_Height=C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;

		}
		if(iSavedSubitem ==5){
			if(num == 0){
				C_RECT[iSavedItem].m_Right = 0;
			}
			else{
				C_RECT[iSavedItem].m_Right = num;
				C_RECT[iSavedItem].m_PosX=(C_RECT[iSavedItem].m_Right +C_RECT[iSavedItem].m_Left+1) /2;
				C_RECT[iSavedItem].m_Width=C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;
			}
		}
		if(iSavedSubitem ==6){
			if(num ==0){
				C_RECT[iSavedItem].m_Bottom =0;
			}
			else{
				C_RECT[iSavedItem].m_Bottom = num;
				C_RECT[iSavedItem].m_PosY=(C_RECT[iSavedItem].m_Top +C_RECT[iSavedItem].m_Bottom+1)/2;
				C_RECT[iSavedItem].m_Height=C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;
			}
		}
		if(iSavedSubitem ==7){
			C_RECT[iSavedItem].m_Width = num;
		}
		if(iSavedSubitem ==8){
			C_RECT[iSavedItem].m_Height = num;
		}
		if(iSavedSubitem ==9){
			C_RECT[iSavedItem].m_L = num;
		}
		if(iSavedSubitem ==10){
			C_RECT[iSavedItem].m_a = num2;
		}
		if(iSavedSubitem ==11){
			C_RECT[iSavedItem].m_b = num2;
		}
		if(iSavedSubitem ==12){
			C_RECT[iSavedItem].d_Thresold= num2;
		}
		/*if(iSavedSubitem ==13){
			C_RECT[iSavedItem].m_MinB= num;
		}
		if(iSavedSubitem ==14){
			C_RECT[iSavedItem].m_MaxB = num;
		}

		if(C_RECT[iSavedItem].m_MinR < 0){
			C_RECT[iSavedItem].m_MinR = 0;
			
		}
	
		if(C_RECT[iSavedItem].m_MinG < 0){
			C_RECT[iSavedItem].m_MinG = 0;
		}

		if(C_RECT[iSavedItem].m_MinB < 0){
			C_RECT[iSavedItem].m_MinB = 0;
		}

		if(C_RECT[iSavedItem].m_MaxR < 0){
			C_RECT[iSavedItem].m_MaxR = 0;
		}
	
		if(C_RECT[iSavedItem].m_MaxG < 0){
				C_RECT[iSavedItem].m_MaxG = 0;
		}

		if(C_RECT[iSavedItem].m_MaxB < 0){
				C_RECT[iSavedItem].m_MaxB = 0;
		}



		if(C_RECT[iSavedItem].m_MinR >255){
			C_RECT[iSavedItem].m_MinR = 255;
		}
	
		if(C_RECT[iSavedItem].m_MinG > 255){
			C_RECT[iSavedItem].m_MinG = 255;
		}

		if(C_RECT[iSavedItem].m_MinB > 255){
			C_RECT[iSavedItem].m_MinB = 255;
		}

		if(C_RECT[iSavedItem].m_MaxR > 255){
			C_RECT[iSavedItem].m_MaxR = 255;
		}
	
		if(C_RECT[iSavedItem].m_MaxG > 255){
			C_RECT[iSavedItem].m_MaxG = 255;
		}

		if(C_RECT[iSavedItem].m_MaxB > 255){
			C_RECT[iSavedItem].m_MaxB = 255;
		}*/




		
		C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
			
		if(C_RECT[iSavedItem].m_Left < 0){//////////////////////수정
			C_RECT[iSavedItem].m_Left = 0;
			C_RECT[iSavedItem].m_Width = C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Right >m_CAM_SIZE_WIDTH){
			C_RECT[iSavedItem].m_Right = m_CAM_SIZE_WIDTH;
			C_RECT[iSavedItem].m_Width = C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Top< 0){
			C_RECT[iSavedItem].m_Top = 0;
			C_RECT[iSavedItem].m_Height = C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Bottom >m_CAM_SIZE_HEIGHT){
			C_RECT[iSavedItem].m_Bottom = m_CAM_SIZE_HEIGHT;
			C_RECT[iSavedItem].m_Height = C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Height<= 0){
			C_RECT[iSavedItem].m_Height = 1;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Height >=m_CAM_SIZE_HEIGHT){
			C_RECT[iSavedItem].m_Height = m_CAM_SIZE_HEIGHT;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}

		if(C_RECT[iSavedItem].m_Width <= 0){
			C_RECT[iSavedItem].m_Width = 1;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Width >=m_CAM_SIZE_WIDTH){
			C_RECT[iSavedItem].m_Width = m_CAM_SIZE_WIDTH;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}



		C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);

	

		CString data = "";

		if(iSavedSubitem ==1){
			data.Format("%d",C_RECT[iSavedItem].m_PosX);			
		}
		else if(iSavedSubitem ==2){
			data.Format("%d",C_RECT[iSavedItem].m_PosY);
		}
		else if(iSavedSubitem ==3){
			data.Format("%d",C_RECT[iSavedItem].m_Left);			
		}
		else if(iSavedSubitem ==4){
			data.Format("%d",C_RECT[iSavedItem].m_Top);			
		}
		else if(iSavedSubitem ==5){
			data.Format("%d",C_RECT[iSavedItem].m_Right);			
		}
		else if(iSavedSubitem ==6){
			data.Format("%d",C_RECT[iSavedItem].m_Bottom);			
		}
		else if(iSavedSubitem ==7){
			data.Format("%d",C_RECT[iSavedItem].m_Width);			
		}
		else if(iSavedSubitem ==8){
			data.Format("%d",C_RECT[iSavedItem].m_Height);			
		}
		else if(iSavedSubitem ==9){
			data.Format("%0.2f",C_RECT[iSavedItem].m_L);			
		}
		else if(iSavedSubitem ==10){
			data.Format("%0.2f",C_RECT[iSavedItem].m_a);			
		}
		else if(iSavedSubitem ==11){
			data.Format("%0.2f",C_RECT[iSavedItem].m_b);			
		}
		else if(iSavedSubitem ==12){
			data.Format("%0.2f",C_RECT[iSavedItem].d_Thresold);			
		}
		//else if(iSavedSubitem ==13){
		//	data.Format("%d",C_RECT[iSavedItem].m_MinB);			
		//}
		//else if(iSavedSubitem ==14){
		//	data.Format("%d",C_RECT[iSavedItem].m_MaxB);			
		//}
		((CEdit *)GetDlgItem(IDC_EDIT_C_MOD))->SetWindowText(data);

}

//void CColor_Option::OnTimer(UINT_PTR nIDEvent)
//{
//	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
//
//	CDialog::OnTimer(nIDEvent);
//	switch(nIDEvent)	
//	{
//	case 110:
//		if(EnterState==1)
//		{
//	       
//			if(iSavedSubitem2 ==13){
//				if(iSavedItem == 0){
//					iSavedItem = 1; iSavedSubitem2 =1;
//				}
//				else if(iSavedItem == 1){
//					iSavedItem = 2; iSavedSubitem2 =1;
//				}
//				else if(iSavedItem == 2){
//					iSavedItem = 3; iSavedSubitem2 =1;
//				}
//				else if(iSavedItem == 3){
//					iSavedItem = 0; iSavedSubitem2 =1;
//				}
//			}
//			
//			C_DATALIST.GetSubItemRect(iSavedItem,iSavedSubitem2 , LVIR_BOUNDS, rect);
//			C_DATALIST.ClientToScreen(rect);
//			this->ScreenToClient(rect);
//
//			C_DATALIST.SetSelectionMark(iSavedItem);
//			C_DATALIST.SetFocus();
//			((CEdit *)GetDlgItem(IDC_EDIT_C_MOD))->SetWindowText(C_DATALIST.GetItemText(iSavedItem, iSavedSubitem2));
//			((CEdit *)GetDlgItem(IDC_EDIT_C_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
//			((CEdit *)GetDlgItem(IDC_EDIT_C_MOD))->SetFocus();
//
//			iSavedSubitem=iSavedSubitem2;
//
//			EnterState=0;
//			
//		}
//
//	break;
//
//	}
//
//}

BOOL CColor_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
			if(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_C_MOD))->GetSafeHwnd())//GetSafeHwnd() 자신의 핸들을 가져오는 함수
			{
				int bufSubitem = iSavedSubitem + 1;
				
				if(bufSubitem ==13){
					if(iSavedItem == 0){
						iSavedItem = 1; bufSubitem =1;
					}
					else if(iSavedItem == 1){
						iSavedItem = 2; bufSubitem =1;
					}
					else if(iSavedItem == 2){
						iSavedItem = 3; bufSubitem =1;
					}
					else if(iSavedItem == 3){
						iSavedItem =0; bufSubitem =1;
					}
					
				}
				int bufItem = iSavedItem;
				C_DATALIST.EnsureVisible(iSavedItem, FALSE);
				C_DATALIST.GetSubItemRect(iSavedItem,bufSubitem , LVIR_BOUNDS, rect);
				C_DATALIST.ClientToScreen(rect);
				this->ScreenToClient(rect);
				C_DATALIST.SetSelectionMark(iSavedItem);
				C_DATALIST.SetFocus(); //저장
				iSavedItem = bufItem;
				iSavedSubitem = bufSubitem;
				((CEdit *)GetDlgItem(IDC_EDIT_C_MOD))->SetWindowText(C_DATALIST.GetItemText(iSavedItem, iSavedSubitem));
				((CEdit *)GetDlgItem(IDC_EDIT_C_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
				((CEdit *)GetDlgItem(IDC_EDIT_C_MOD))->SetFocus();

			}
			
			if ((pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Color_PosX))->GetSafeHwnd())||  
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Color_PosY))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Color_Dis_W))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Color_Dis_H))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Color_Width))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Color_Height))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Color_Thresold))->GetSafeHwnd()))
			{	
				OnBnClickedButtonsetcolorzone();
			}

			return TRUE;
		}

		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CColor_Option::OnBnClickedButtonMasterC()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int nIndex = AVERAGE_INDEX;
	double db_L[AVERAGE_INDEX][4] = {0,};
	double db_a[AVERAGE_INDEX][4] = {0,};
	double db_b[AVERAGE_INDEX][4] = {0,};

	ChangeCheck=1;
	//		((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE_CHK(m_INFO.Number);
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY

	MasterMod =TRUE;

	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan =1;						

	for(int i =0;i<10;i++){
		if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0){
			break;
		}else{
			DoEvents(50);
		}
	}

	if(AutomationMod ==1){
		// 오토메이션 모드
		//C_CHECKING[] PosX,PosY 받은 거 체킹	

		//Automation_DATA_INPUT();// 성공하면 데이터 C_RECT에 넣기.
		//실패하면 그냥 진행...
		//Load_parameter();// 실패한 경우
		//		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	

		BOOL FLAG = TRUE;
		GetColorAreaCoordinate(m_RGBScanbuf);

		for(int lop=0; lop<4; lop++)
		{
			C_CHECKING[lop].m_PosX = CD_RECT[lop].m_resultX;
			C_CHECKING[lop].m_PosY = CD_RECT[lop].m_resultY;					
			C_CHECKING[lop].m_Success = CD_RECT[lop].m_Success;

			if(!C_CHECKING[lop].m_Success){
				FLAG = FALSE;
				break;
			}	
		}

		if(FLAG){
			Automation_DATA_INPUT();
		}else{Load_parameter();}


	}/*else{
	 Load_Original_pra();
	 }*/

	double SumL[4]={0,},SumA[4]={0,},SumB[4]={0,};

	for(int Loop=0; Loop<nIndex; Loop++)
	{
		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan =1;						

		for(int i =0;i<10;i++){
			if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0){
				break;
			}else{
				DoEvents(50);
			}
		} 	



		for(int lop=0; lop<4;lop++)
		{

			AveRGBGen(m_RGBScanbuf,lop);

			db_L[Loop][lop]=m_Result_L[lop];
			db_a[Loop][lop]=m_Result_a[lop];
			db_b[Loop][lop]=m_Result_b[lop];

			SumL[lop] += db_L[Loop][lop];
			SumA[lop] += db_a[Loop][lop];
			SumB[lop] += db_b[Loop][lop];
		}	
	}


	for(int lop=0; lop<4;lop++){
		
		C_RECT[lop].m_L = SumL[lop] / (double)nIndex;
		C_RECT[lop].m_a = SumA[lop] / (double)nIndex;
		C_RECT[lop].m_b = SumB[lop] / (double)nIndex;

	}

	UploadList();
	MasterMod =FALSE;

	for(int t=0; t<4; t++){
		ChangeItem[t]=t;
	}

	for(int t=0; t<4; t++){
		C_DATALIST.Update(t);
	}
	
}
void CColor_Option::Load_Original_pra(){


	for(int t=0; t<4; t++){
		C_RECT[t].m_PosX=C_Original[t].m_PosX;
		C_RECT[t].m_PosY=C_Original[t].m_PosY;
		C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);//////C
	}

}
void CColor_Option::Automation_DATA_INPUT(){
	for(int t=0; t<4; t++){
		C_RECT[t].m_PosX=C_CHECKING[t].m_PosX;
		C_RECT[t].m_PosY=C_CHECKING[t].m_PosY;
		C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);
	}

}

void CColor_Option::AveRGBGen(LPBYTE IN_RGB,int NUM){//AVE값 및 평균 RGB값 추출 및 그리기.
	
	//m_Success = FALSE;
	C_RECT[NUM].m_Success = FALSE;
	BYTE *BW;
	BYTE R = 0,G = 0,B = 0;
	DWORD	RGBPIX = 0;
	DWORD	Total_R = 0,Total_G = 0,Total_B = 0;
	int index=0;
//	if(RGBRect.Chkdata() == FALSE){return 0;}
		
	DWORD Total = C_RECT[NUM].Height()*C_RECT[NUM].Length();
	BW = new BYTE[Total];
	memset(BW,0,sizeof(BW));
	int startx = C_RECT[NUM].m_Left;
	int starty = C_RECT[NUM].m_Top;
	int endx = startx + C_RECT[NUM].Length();
	int endy = starty + C_RECT[NUM].Height();
	DWORD RGBLINE = starty * (m_CAM_SIZE_WIDTH *4);
		
	for(int lopy = starty;lopy < endy;lopy++)
	{
		RGBPIX = startx * 4;

		for(int lopx = startx;lopx < endx;lopx++)
		{
			B = IN_RGB[RGBLINE + RGBPIX];  
			G = IN_RGB[RGBLINE + RGBPIX + 1];
			R = IN_RGB[RGBLINE + RGBPIX + 2];

			/*if( G < 50 )
				G = G + 20;
			if( B < 50 )
				B = B + 20;*/

			Total_R += R;
			Total_G += G;
			Total_B += B;
			index++;
			RGBPIX+=4;
		}

		RGBLINE+=(m_CAM_SIZE_WIDTH *4);
	}

	m_AVER = (BYTE)(Total_R/index);
	m_AVEG = (BYTE)(Total_G/index);
	m_AVEB = (BYTE)(Total_B/index);

	if(m_AVER>90 && m_AVEG<50 && m_AVEB<50 ) {
		m_AVER = (m_AVER+130*2)/3.0;
		m_AVEG = (m_AVEG+15*2)/3.0;
		m_AVEB = (m_AVEB+15*2)/3.0;
	}


	double X = 0;
	double Y = 0;
	double Z = 0;

	double Xn = 0;
	double Yn = 0;
	double Zn = 0;

	double L = 0;
	double a = 0;
	double b = 0;

	X = 0.4306*m_AVER + 0.3415*m_AVEG + 0.1784*m_AVEB;
    Y = 0.2220*m_AVER + 0.7067*m_AVEG + 0.0713*m_AVEB;
    Z = 0.0202*m_AVER + 0.1295*m_AVEG + 0.9394*m_AVEB;

	Xn = 0.4306*255.0 + 0.3415*255.0 + 0.1784*255.0;
    Yn = 0.2220*255.0 + 0.7067*255.0 + 0.0713*255.0;
    Zn = 0.0202*255.0 + 0.1295*255.0 + 0.9394*255.0;

	if(Y/Yn > 0.008865)
	{
		L = 116.0*pow(Y/Yn, 1.0/3.0)-16.0;
		a = 500. * (pow(X/Xn, 1.0/3.0) - pow(Y/Yn, 1.0/3.0));
		b = 200. * (pow(Y/Yn, 1.0/3.0) - pow(Z/Zn, 1.0/3.0));
	}
	else
	{
		L = 903.3*Y/Yn;
		a = 500. * ( (7.787*(X/Xn) + 16.0/116.0) - (7.787*(Y/Yn) + 16.0/116.0) );
		b = 200. * ( (7.787*(Y/Yn) + 16.0/116.0) - (7.787*(Z/Zn) + 16.0/116.0) );
	}
	
	
	m_Result_L[NUM] = L;
	m_Result_a[NUM] = a;
	m_Result_b[NUM] = b;

	//// 최종 계산 - 합격 여부
	//double Result_Cal = 0;
	//double Squre_a = 0;
	//double Squre_b = 0;

	//Squre_a = m_Result_a[NUM] - C_RECT[NUM].m_a;
	//Squre_a *= Squre_a;

	//Squre_b = m_Result_b[NUM] - C_RECT[NUM].m_b;
	//Squre_b *= Squre_b;

	//Result_Cal = sqrt(Squre_a+Squre_b);

	//if(C_RECT[NUM].d_Thresold > Result_Cal)
	//{
	//	C_RECT[NUM].m_Success = TRUE;
	//}

	/*if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
	
		CString str="";
		str.Empty();
		str.Format("%0.2f",m_Result_L[NUM]);
		m_ColorList.SetItemText(InsertIndex,4+(NUM*4),str);
		str.Empty();
		str.Format("%0.2f",m_Result_a[NUM]);
		m_ColorList.SetItemText(InsertIndex,5+(NUM*4),str);
		str.Empty();
		str.Format("%0.2f",m_Result_b[NUM]);
		m_ColorList.SetItemText(InsertIndex,6+(NUM*4),str);
	}*/


	delete BW;
	//return C_RECT[NUM].m_Success;
}

bool CColor_Option::AveRGBPic(CDC *cdc,int NUM){//AVE값 및 평균 RGB값 추출 및 그리기.
	
	CPen	my_Pan,*old_pan;
	CString TEXTDATA ="";
	
	//if(C_RECT[NUM].Chkdata() == FALSE){return 0;}
	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	if(C_RECT[NUM].m_Success == TRUE){
		my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(WHITE_COLOR);
	}else{
		my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(WHITE_COLOR);
	}

	cdc->MoveTo(C_RECT[NUM].m_Left,C_RECT[NUM].m_Top);
	cdc->LineTo(C_RECT[NUM].m_Right,C_RECT[NUM].m_Top);
	cdc->LineTo(C_RECT[NUM].m_Right,C_RECT[NUM].m_Bottom);
	cdc->LineTo(C_RECT[NUM].m_Left,C_RECT[NUM].m_Bottom);
	cdc->LineTo(C_RECT[NUM].m_Left,C_RECT[NUM].m_Top);

	//if(MasterMod == TRUE){
		TEXTDATA.Format("L:%0.2f A:%0.2f B:%0.2f",m_Result_L[NUM],m_Result_a[NUM],m_Result_b[NUM]);
	//}
	//else{
	//	TEXTDATA.Format("R:%03d G:%03d B:%03d",C_RECT[NUM].m_resultR,C_RECT[NUM].m_resultG,C_RECT[NUM].m_resultB);	
	//}
	cdc->TextOut(C_RECT[NUM].m_Left + (int)(C_RECT[NUM].Height()/2)-60,C_RECT[NUM].m_Top - 20,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
	
	cdc->SelectObject(old_pan);
	old_pan->DeleteObject();
	my_Pan.DeleteObject();
	
	return TRUE;
}



void CModel_Create(CColor_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO){
	CRect OptionRect;
	if(pTabWnd != NULL){
		((CColor_Option *)*pWnd) = new CColor_Option(pMomWnd);
		((CColor_Option *)*pWnd)->Setup(pMomWnd,pEdit,INFO);
		((CColor_Option *)*pWnd)->Create(((CColor_Option *)*pWnd)->IDD,pTabWnd);//&m_CtrTab);
		((CColor_Option *)*pWnd)->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		((CColor_Option *)*pWnd)->MoveWindow(20,40,OptionRect.Width(),OptionRect.Height());
	}
}

void CModel_Delete(CColor_Option **pWnd){
	if(((CColor_Option *)*pWnd) != NULL){
//		((CColor_Option *)*pWnd)->KillTimer(110);
		((CColor_Option *)*pWnd)->DestroyWindow();
		delete ((CColor_Option *)*pWnd);
		((CColor_Option *)*pWnd) = NULL;	
	}
}

tResultVal CColor_Option::Run()
{		
	int nIndex = AVERAGE_INDEX_DET;
	b_StopFail = FALSE;
	double db_L[AVERAGE_INDEX_DET][4] = {0,};
	double db_a[AVERAGE_INDEX_DET][4] = {0,};
	double db_b[AVERAGE_INDEX_DET][4] = {0,};
		if(ChangeCheck == TRUE){
			OnBnClickedButtonCRectSave();
		}

		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			InsertList();
		}	
		int camcnt = 0;
		tResultVal retval = {0,};

		for (int lop = 0;lop<4;lop++)
		{
			m_Result_Cal[lop] = 0;
		}
		

		
		int count= 0;
		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan =1;						
			
		for(int i =0;i<10;i++){
			if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0){
				break;
			}else{
				DoEvents(50);
			}
		} 	
		
		Standard_CD_RECT[0].m_resultX = C_RECT[0].m_PosX;        //왼쪽위사각형
		Standard_CD_RECT[0].m_resultY = C_RECT[0].m_PosY;
		Standard_CD_RECT[1].m_resultX = C_RECT[1].m_PosX;        //오른쪽위사각형
		Standard_CD_RECT[1].m_resultY = C_RECT[1].m_PosY;
		Standard_CD_RECT[2].m_resultX = C_RECT[2].m_PosX;        //왼쪽아래사각형
		Standard_CD_RECT[2].m_resultY = C_RECT[2].m_PosY;
		Standard_CD_RECT[3].m_resultX = C_RECT[3].m_PosX;        //오른쪽아래사각형
		Standard_CD_RECT[3].m_resultY = C_RECT[3].m_PosY;
		

		CString stateDATA ="컬러_ ";
		CString str ="";

			if(1)//AutomationMod ==1)	//항상 실행하도록..LG 요청사항
			{
			// 오토메이션 모드
			//C_CHECKING[] PosX,PosY 받은 거 체킹	
			
				//Automation_DATA_INPUT();// 성공하면 데이터 C_RECT에 넣기.
				//실패하면 그냥 진행...
				GetColorAreaCoordinate(m_RGBScanbuf);

				BOOL FLAG = TRUE;

				for(int lop=0; lop<4; lop++)
				{
					C_CHECKING[lop].m_PosX = CD_RECT[lop].m_resultX;
					C_CHECKING[lop].m_PosY = CD_RECT[lop].m_resultY;					
					C_CHECKING[lop].m_Success = CD_RECT[lop].m_Success;					

					if(!C_CHECKING[lop].m_Success)
					{
						FLAG = FALSE;
						break;
					}
				}	
				
				if(FLAG)
					Automation_DATA_INPUT();
				else{Load_parameter();}
			}

			double SumL[4]={0,},SumA[4]={0,},SumB[4]={0,};

			for(int Loop=0; Loop<nIndex; Loop++)
			{
				((CImageTesterDlg *)m_pMomWnd)->m_ImgScan =1;						

				for(int i =0;i<10;i++){
					if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0){
						break;
					}else{
						DoEvents(50);
					}
				} 	



				for(int lop=0; lop<4;lop++)
				{

					AveRGBGen(m_RGBScanbuf,lop);

					db_L[Loop][lop]=m_Result_L[lop];
					db_a[Loop][lop]=m_Result_a[lop];
					db_b[Loop][lop]=m_Result_b[lop];

					SumL[lop] += db_L[Loop][lop];
					SumA[lop] += db_a[Loop][lop];
					SumB[lop] += db_b[Loop][lop];

					//if(C_RECT[lop].m_Success== TRUE){

					//	count++;
					//}
				}	
			}

			for(int lop=0; lop<4;lop++)
			{

				m_Result_L[lop] = SumL[lop] / (double)nIndex;
				m_Result_a[lop] = SumA[lop] / (double)nIndex;
				m_Result_b[lop] = SumB[lop] / (double)nIndex;

				// 최종 계산 - 합격 여부
				double Result_Cal = 0;
				double Squre_a = 0;
				double Squre_b = 0;

				Squre_a = m_Result_a[lop] - C_RECT[lop].m_a;
				Squre_a *= Squre_a;

				Squre_b = m_Result_b[lop] - C_RECT[lop].m_b;
				Squre_b *= Squre_b;

				Result_Cal = sqrt(Squre_a+Squre_b);
				m_Result_Cal[lop] = Result_Cal;

				if(C_RECT[lop].d_Thresold > Result_Cal)
				{
					C_RECT[lop].m_Success = TRUE;
				}
				
				if(C_RECT[lop].m_Success== TRUE){

					count++;
				}
			}


			// 최대 최소 제거 시 필요함..
			//for(int Loop=0; Loop<10; Loop++)
			//{

			//	for(int lop=0; lop<4;lop++)
			//	{

			//		SumL+=db_L[Loop][lop];
			//		SumA+=db_a[Loop][lop];
			//		SumB+=db_b[Loop][lop];
			//		
			//	}	
			//}
			
			
			str.Format("%0.2f,%0.2f,%0.2f Val:%0.3f",m_Result_L[0],m_Result_a[0],m_Result_b[0],m_Result_Cal[0]);
			stateDATA += ("LT _")+str+("\r\n");
			str.Format("%0.2f,%0.2f,%0.2f Val:%0.3f",m_Result_L[1],m_Result_a[1],m_Result_b[1],m_Result_Cal[1]);
			stateDATA += ("RT _")+str+("\r\n");
			str.Format("%0.2f,%0.2f,%0.2f Val:%0.3f",m_Result_L[2],m_Result_a[2],m_Result_b[2],m_Result_Cal[2]);
			stateDATA += ("LB _")+str+("\r\n");
			str.Format("%0.2f,%0.2f,%0.2f Val:%0.3f",m_Result_L[3],m_Result_a[3],m_Result_b[3],m_Result_Cal[3]);
			stateDATA += ("RB _")+str+("\r\n");
	
			//StatePrintf("컬러 측정 모드가 종료되었습니다");
		

			str.Format("%0.3f",m_Result_Cal[0]);
			((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->LT_VALUE2(str);
			if(C_RECT[0].m_Success == TRUE){
				((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->LT_VALUE("PASS");
			}else{
				((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->LT_VALUE("FAIL");
				
			}

			str.Format("%0.3f",m_Result_Cal[1]);
			((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->RT_VALUE2(str);
			if(C_RECT[1].m_Success == TRUE){
				((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->RT_VALUE("PASS");
			}else{
				((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->RT_VALUE("FAIL");
				
			}

			str.Format("%0.3f",m_Result_Cal[2]);
			((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->LB_VALUE2(str);
			if(C_RECT[2].m_Success == TRUE){
				((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->LB_VALUE("PASS");
			}else{
				((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->LB_VALUE("FAIL");
				
			}

			str.Format("%0.3f",m_Result_Cal[3]);
			((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->RB_VALUE2(str);
			if(C_RECT[3].m_Success == TRUE){
				((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->RB_VALUE("PASS");
			}else{
				((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->RB_VALUE("FAIL");
			}
	
		retval.m_ID = 0x0e;
		retval.ValString.Empty();
		retval.ValString.Format("4개중 %d 성공",count);
		if(count ==4){
			retval.m_Success = TRUE;
			Str_Mes[0] = "PASS";
			Str_Mes[1] = "1";	
			stateDATA += "_ PASS";
			((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->RV_VALUE(1,"PASS");
			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_COLOR,"PASS");
			m_ColorList.SetItemText(InsertIndex,20,"PASS");
			}
		}else{
			stateDATA += "_ FAIL";
			retval.m_Success = FALSE;
			Str_Mes[0] = "FAIL";
			Str_Mes[1] = "0";			
			((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->RV_VALUE(2,"FAIL");

			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_COLOR,"FAIL");
			m_ColorList.SetItemText(InsertIndex,20,"FAIL");
			}
		}

		((CImageTesterDlg *)m_pMomWnd)->StatePrintf(stateDATA);


		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

			str="";
			str.Empty();
			str.Format("%0.2f",m_Result_L[NUM]);
			m_ColorList.SetItemText(InsertIndex,4+(NUM*4),str);
			str.Empty();
			str.Format("%0.2f",m_Result_a[NUM]);
			m_ColorList.SetItemText(InsertIndex,5+(NUM*4),str);
			str.Empty();
			str.Format("%0.2f",m_Result_b[NUM]);
			m_ColorList.SetItemText(InsertIndex,6+(NUM*4),str);
			str.Empty();
			str.Format("%0.3f",m_Result_Cal[NUM]);
			m_ColorList.SetItemText(InsertIndex,7+(NUM*4),str);
			StartCnt++;
		}
		/*
		retval.m_Val[0] = S_Val.m_ResutX;
		retval.m_Val[1] = S_Val.m_ResutY;
		*/
		return retval;
	
}	

void CColor_Option::FAIL_UPLOAD(){
	if ((((CImageTesterDlg *)m_pMomWnd)->LotMod == 1) && (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)){

	InsertList();
	for (int t=0; t<16; t++)
	{
		m_ColorList.SetItemText(InsertIndex,t+4,"X");

	}
	m_ColorList.SetItemText(InsertIndex,20,"FAIL");
	m_ColorList.SetItemText(InsertIndex,21,"STOP");
	Str_Mes[0] = "FAIL";
	Str_Mes[1] = "0";	
	//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_COLOR,"STOP");
	StartCnt++;
	b_StopFail = TRUE;
	}
}

void CColor_Option::Pic(CDC *cdc)
{
	for(int lop=0;lop<4;lop++){
		AveRGBPic(cdc,lop);
	}
}

void CColor_Option::InitPrm()
{
	Load_parameter();
	UpdateData(FALSE);
	UploadList();

	for(int t=0; t<4;t++){
		C_RECT[t].m_Success = FALSE;
		C_RECT[t].m_R =0;
		C_RECT[t].m_G =0;
		C_RECT[t].m_B =0;

	}

}

void CColor_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

void CColor_Option::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",C_filename);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

//---------------------------------------------------------------------WORKLIST
void CColor_Option::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	InsertIndex = m_ColorList.InsertItem(StartCnt,"",0);
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_ColorList.SetItemText(InsertIndex,0,strCnt);
	
	m_ColorList.SetItemText(InsertIndex,1,((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_ColorList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_ColorList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");


}
void CColor_Option::Set_List(CListCtrl *List){
	

	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"1z_L",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"1z_a",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"1z_b",LVCFMT_CENTER, 80);

	List->InsertColumn(7,"1z_Val",LVCFMT_CENTER, 80);

	List->InsertColumn(8,"2z_L",LVCFMT_CENTER, 80);
	List->InsertColumn(9,"2z_a",LVCFMT_CENTER, 80);
	List->InsertColumn(10,"2z_b",LVCFMT_CENTER, 80);

	List->InsertColumn(11,"2z_Val",LVCFMT_CENTER, 80);

	List->InsertColumn(12,"3z_L",LVCFMT_CENTER, 80);
	List->InsertColumn(13,"3z_a",LVCFMT_CENTER, 80);
	List->InsertColumn(14,"3z_b",LVCFMT_CENTER, 80);

	List->InsertColumn(15,"3z_Val",LVCFMT_CENTER, 80);

	List->InsertColumn(16,"4z_L",LVCFMT_CENTER, 80);
	List->InsertColumn(17,"4z_a",LVCFMT_CENTER, 80);
	List->InsertColumn(18,"4z_b",LVCFMT_CENTER, 80);

	List->InsertColumn(19,"4z_Val",LVCFMT_CENTER, 80);

	List->InsertColumn(20,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(21,"비고",LVCFMT_CENTER, 80);
	ListItemNum=22;
	Copy_List(&m_ColorList,((CImageTesterDlg *)m_pMomWnd)->m_pWorkList,ListItemNum);
}

CString CColor_Option::Mes_Result()
{
	CString sz[12];
	int		nResult[12];

	//int nSel = m_ColorList.GetItemCount()-1;

	//// 좌상레드
	//sz[0] = m_ColorList.GetItemText(nSel, 4);

	//if(C_RECT[0].m_Success)
	//	nResult[0] = 1;
	//else
	//	nResult[0] = 0;

	//// 좌상그린
	//sz[1] = m_ColorList.GetItemText(nSel, 5);

	//if(C_RECT[0].m_Success)
	//	nResult[1] = 1;
	//else
	//	nResult[1] = 0;

	//// 좌상블루
	//sz[2] = m_ColorList.GetItemText(nSel, 6);

	//if(C_RECT[0].m_Success)
	//	nResult[2] = 1;
	//else
	//	nResult[2] = 0;

	//// 우상레드
	//sz[3] = m_ColorList.GetItemText(nSel, 7);

	//if(C_RECT[1].m_Success)
	//	nResult[3] = 1;
	//else
	//	nResult[3] = 0;

	//// 우상그린
	//sz[4] = m_ColorList.GetItemText(nSel, 8);

	//if(C_RECT[1].m_Success)
	//	nResult[4] = 1;
	//else
	//	nResult[4] = 0;

	//// 우상블루
	//sz[5] = m_ColorList.GetItemText(nSel, 9);

	//if(C_RECT[1].m_Success)
	//	nResult[5] = 1;
	//else
	//	nResult[5] = 0;

	//// 좌하레드
	//sz[6] = m_ColorList.GetItemText(nSel, 10);

	//if(C_RECT[2].m_Success)
	//	nResult[6] = 1;
	//else
	//	nResult[6] = 0;

	//// 좌하그린
	//sz[7] = m_ColorList.GetItemText(nSel, 11);

	//if(C_RECT[2].m_Success)
	//	nResult[7] = 1;
	//else
	//	nResult[7] = 0;

	//// 좌하블루
	//sz[8] = m_ColorList.GetItemText(nSel, 12);

	//if(C_RECT[2].m_Success)
	//	nResult[8] = 1;
	//else
	//	nResult[8] = 0;

	//// 우상레드
	//sz[9] = m_ColorList.GetItemText(nSel, 13);

	//if(C_RECT[3].m_Success)
	//	nResult[9] = 1;
	//else
	//	nResult[9] = 0;

	//// 우상그린
	//sz[10] = m_ColorList.GetItemText(nSel, 14);

	//if(C_RECT[3].m_Success)
	//	nResult[10] = 1;
	//else
	//	nResult[10] = 0;

	//// 우상블루
	//sz[11] = m_ColorList.GetItemText(nSel, 15);

	//if(C_RECT[3].m_Success)
	//	nResult[11] = 1;
	//else
	//	nResult[11] = 0;

	//m_szMesResult.Format(_T("%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d"), sz[0], nResult[0], sz[1], nResult[1], sz[2], nResult[2], sz[3], nResult[3], sz[4], nResult[4], sz[5], nResult[5], 
	//	sz[6], nResult[6], sz[7], nResult[7], sz[8], nResult[8], sz[9],	nResult[9], sz[10], nResult[10], sz[11], nResult[11]);
	
	m_szMesResult = _T("");
	if(b_StopFail == FALSE){
	m_szMesResult.Format(_T("%s:%s"),Str_Mes[0],Str_Mes[1]);
	}else{
		m_szMesResult.Format(_T(":1"));
	}
	return m_szMesResult;
}

void CColor_Option::EXCEL_SAVE(){
	CString Item[12]={"1z_R","1z_G","1z_B","2z_R","2z_G","2z_B","3z_R","3z_G","3z_B","4z_R","4z_G","4z_B"};

	CString Data ="";
	CString str="";
	str = "***********************컬러 검사***********************\n";
	Data += str;
	str.Empty();
	str.Format("Left Top ,Min,Max,,R,%d,%d,G,%d,%d,B,%d,%d,,/, ",C_RECT[0].m_MinR,C_RECT[0].m_MaxR,C_RECT[0].m_MinG,C_RECT[0].m_MaxG,C_RECT[0].m_MinB,C_RECT[0].m_MaxB);
	Data += str;
	str.Empty();
	str.Format("Right Top ,Min,Max,,R,%d,%d,G,%d,%d,B,%d,%d,,/, \n",C_RECT[1].m_MinR,C_RECT[1].m_MaxR,C_RECT[1].m_MinG,C_RECT[1].m_MaxG,C_RECT[1].m_MinB,C_RECT[1].m_MaxB);
	Data += str;
	str.Empty();
	str.Format("Left Bottom ,Min,Max,,R,%d,%d,G,%d,%d,B,%d,%d,,/,",C_RECT[2].m_MinR,C_RECT[2].m_MaxR,C_RECT[2].m_MinG,C_RECT[2].m_MaxG,C_RECT[2].m_MinB,C_RECT[2].m_MaxB);
	Data += str;
	str.Empty();
	str.Format("Right Bottom ,Min,Max,,R,%d,%d,G,%d,%d,B,%d,%d,,/, \n",C_RECT[3].m_MinR,C_RECT[3].m_MaxR,C_RECT[3].m_MinG,C_RECT[3].m_MaxG,C_RECT[3].m_MinB,C_RECT[3].m_MaxB);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;

	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("COLOR검사",Item,12,&m_ColorList,Data);
}

bool CColor_Option::EXCEL_UPLOAD(){
	m_ColorList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("COLOR검사",&m_ColorList,1, &StartCnt)==TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return TRUE;
}
void CColor_Option::OnBnClickedCheckColorAuto()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(AutomationMod ==0){
		
		AutomationMod =1;
		//C_DATALIST.EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Color_PosX)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Color_PosY)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Color_Dis_W)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Color_Dis_H)->EnableWindow(0);
		/*(CEdit *)GetDlgItem(IDC_Color_Width)->EnableWindow(0);
		
		(CEdit *)GetDlgItem(IDC_Color_Height)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Color_Thresold)->EnableWindow(0);
		*/
//		(CButton *)GetDlgItem(IDC_BUTTON_C_RECT_SAVE)->EnableWindow(0);
		//(CButton *)GetDlgItem(IDC_BUTTON_setColorZone)->EnableWindow(0);
		
		//////////////////////////////////////////////////////////////////////오토메이션 중심점으로 영역 잡는 소스 추가해야함.
	}
	else{
		AutomationMod =0;
	//	C_DATALIST.EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Color_PosX)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Color_PosY)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Color_Dis_W)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Color_Dis_H)->EnableWindow(1);
		/*(CEdit *)GetDlgItem(IDC_Color_Width)->EnableWindow(1);
		
		(CEdit *)GetDlgItem(IDC_Color_Height)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Color_Thresold)->EnableWindow(1);
		*/
//		(CButton *)GetDlgItem(IDC_BUTTON_C_RECT_SAVE)->EnableWindow(1);
	//	(CButton *)GetDlgItem(IDC_BUTTON_setColorZone)->EnableWindow(1);
	
	}

		for(int t=0; t<4; t++){
			C_DATALIST.Update(t);
		}

	CString strTitle;
	strTitle.Empty();
	strTitle="COLOR_INIT";
	if(AutomationMod ==1){
		WritePrivateProfileString(str_model,strTitle+"Auto","1",C_filename);}
	if(AutomationMod ==0){
		WritePrivateProfileString(str_model,strTitle+"Auto","0",C_filename);}

}

void CColor_Option::GetColorAreaCoordinate(LPBYTE IN_RGB)
{
	int checkPatternType[4] = {0, 0, 0, 0};

	CD_RECT[0].m_Success = FALSE;
	CD_RECT[1].m_Success = FALSE;
	CD_RECT[2].m_Success = FALSE;
	CD_RECT[3].m_Success = FALSE;

	IplImage *OriginImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);	
	IplImage *RGBResultImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3);

	CvRect centerPtRect = GetCenterPoint(IN_RGB);
	BYTE R,G,B;
	double Sum_Y;
	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	for (int y=0; y<m_CAM_SIZE_HEIGHT; y++)
	{
		for (int x=0; x<m_CAM_SIZE_WIDTH; x++)
		{
			B = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 2];
			
			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;
		}
	}
	
//	OriginImage = cvLoadImage("C:\\OriginalRGBImage.bmp", 0);

	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0); //객체 외곽의 Overshooting으로 인하여 한번 더 처리 함..
	
	cvCvtColor(OriginImage, RGBResultImage, CV_GRAY2BGR);
	
	cvCanny(SmoothImage, CannyImage, 0, 150);		
	
//	cvSaveImage("D:\\ColorAreaCannyResultImage.bmp", CannyImage);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	int distRect1=999999;
	int distRect2=999999;
	int distRect3=999999;
	int distRect4=999999;

	CvRect rect;
	double area, arcCount;

	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);		
		
		int curr_rect_center_x = rect.x + rect.width/2;
		int curr_rect_center_y = rect.y + rect.height/2;

		double circularity = (4.0*3.14*area)/(arcCount*arcCount);		
		double WHRatio = (double)rect.width / (double)rect.height;
		double HWRatio = (double)rect.height / (double)rect.width;
		
		if( centerPtRect.x < curr_rect_center_x && centerPtRect.x+centerPtRect.x+centerPtRect.width > curr_rect_center_x && centerPtRect.y < curr_rect_center_y && centerPtRect.y+centerPtRect.y+centerPtRect.height > curr_rect_center_y )
		{
		}
		else
		{
			if(circularity < 0.95)
			{			
				if(WHRatio < 1.5 && HWRatio < 1.5 )
				{
					if( (rect.width*rect.height) * 0.7 < area )
					{
						if(curr_rect_center_x < 360 && curr_rect_center_y < 240)
						{
						//	double st_rect_center_x = Standard_CD_RECT[0].m_resultX;
						//	double st_rect_center_y = Standard_CD_RECT[0].m_resultY;
							double st_rect_center_x = centerPtRect.x+centerPtRect.width/2;
							double st_rect_center_y = centerPtRect.y+centerPtRect.height/2;
							double curr_rect_center_x = rect.x + rect.width/2;
							double curr_rect_center_y = rect.y + rect.height/2;

							double dist = GetDistance(st_rect_center_x, st_rect_center_y, curr_rect_center_x, curr_rect_center_y);
							bool Color_Flag = FALSE;						
							
							B = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 0];
							G = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 1];
							R = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 2];
							
							if(C_RECT[0].m_MinB < B && C_RECT[0].m_MaxB > B && C_RECT[0].m_MinG < G && C_RECT[0].m_MaxG > G && C_RECT[0].m_MinR < R && C_RECT[0].m_MaxR > R)
								Color_Flag = TRUE;						
							
							if(dist < distRect1 && dist < 150 && Color_Flag)
							{
								CD_RECT[0].m_resultX = curr_rect_center_x;
								CD_RECT[0].m_resultY = curr_rect_center_y;
	//							CD_RECT[0].m_Success = TRUE;

						//		cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8);  

								distRect1 = dist;
								checkPatternType[0] = 1;
							}
						}

						if(curr_rect_center_x > 360 && curr_rect_center_y < 240)
						{
						//	double st_rect_center_x = Standard_CD_RECT[1].m_resultX;
						//	double st_rect_center_y = Standard_CD_RECT[1].m_resultY;
							double st_rect_center_x = centerPtRect.x+centerPtRect.width/2;
							double st_rect_center_y = centerPtRect.y+centerPtRect.height/2;
							double curr_rect_center_x = rect.x + rect.width/2;
							double curr_rect_center_y = rect.y + rect.height/2;
							
							double dist = GetDistance(st_rect_center_x, st_rect_center_y, curr_rect_center_x, curr_rect_center_y);
							bool Color_Flag = FALSE;						
							
							B = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 0];
							G = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 1];
							R = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 2];
							
							if(C_RECT[1].m_MinB < B && C_RECT[1].m_MaxB > B && C_RECT[1].m_MinG < G && C_RECT[1].m_MaxG > G && C_RECT[1].m_MinR < R && C_RECT[1].m_MaxR > R)
								Color_Flag = TRUE;

							if(dist < distRect2 && dist < 150 && Color_Flag)
							{
								CD_RECT[1].m_resultX = curr_rect_center_x;
								CD_RECT[1].m_resultY = curr_rect_center_y;
	//							CD_RECT[1].m_Success = TRUE;

						//		cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8);  

								distRect2 = dist;
								checkPatternType[1] = 1;
							}
						}					

						if(curr_rect_center_x < 360 && curr_rect_center_y > 240)
						{
						//	double st_rect_center_x = Standard_CD_RECT[2].m_resultX;
						//	double st_rect_center_y = Standard_CD_RECT[2].m_resultY;
							double st_rect_center_x = centerPtRect.x+centerPtRect.width/2;
							double st_rect_center_y = centerPtRect.y+centerPtRect.height/2;
							double curr_rect_center_x = rect.x + rect.width/2;
							double curr_rect_center_y = rect.y + rect.height/2;
							
							double dist = GetDistance(st_rect_center_x, st_rect_center_y, curr_rect_center_x, curr_rect_center_y);
							bool Color_Flag = FALSE;						
							
							B = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 0];
							G = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 1];
							R = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 2];
							
							if(C_RECT[2].m_MinB < B && C_RECT[2].m_MaxB > B && C_RECT[2].m_MinG < G && C_RECT[2].m_MaxG > G && C_RECT[2].m_MinR < R && C_RECT[2].m_MaxR > R)
								Color_Flag = TRUE;

							if(dist < distRect3 && dist < 150 && Color_Flag)
							{
								CD_RECT[2].m_resultX = curr_rect_center_x;
								CD_RECT[2].m_resultY = curr_rect_center_y;
	//							CD_RECT[2].m_Success = TRUE;

						//		cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8);  

								distRect3 = dist;
								checkPatternType[2] = 1;
							}
						}
						
						if(curr_rect_center_x > 360 && curr_rect_center_y > 240)
						{
						//	double st_rect_center_x = Standard_CD_RECT[3].m_resultX;
						//	double st_rect_center_y = Standard_CD_RECT[3].m_resultY;
							double st_rect_center_x = centerPtRect.x+centerPtRect.width/2;
							double st_rect_center_y = centerPtRect.y+centerPtRect.height/2;
							double curr_rect_center_x = rect.x + rect.width/2;
							double curr_rect_center_y = rect.y + rect.height/2;
							
							double dist = GetDistance(st_rect_center_x, st_rect_center_y, curr_rect_center_x, curr_rect_center_y);
							bool Color_Flag = FALSE;						
							
							B = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 0];
							G = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 1];
							R = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 2];
							
							if(C_RECT[3].m_MinB < B && C_RECT[3].m_MaxB > B && C_RECT[3].m_MinG < G && C_RECT[3].m_MaxG > G && C_RECT[3].m_MinR < R && C_RECT[3].m_MaxR > R)
								Color_Flag = TRUE;

							if(dist < distRect4 && dist < 150 && Color_Flag)
							{
								CD_RECT[3].m_resultX = curr_rect_center_x;
								CD_RECT[3].m_resultY = curr_rect_center_y;
	//							CD_RECT[3].m_Success = TRUE;

					//			cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8);  

								distRect4 = dist;
								checkPatternType[3] = 1;
							}
						}
					}
				}			
			}
		}
	}
	
	if(checkPatternType[0] == 1 && checkPatternType[1] == 1 && checkPatternType[2] == 1 && checkPatternType[3] == 0){
		CD_RECT[3].m_resultX = CD_RECT[1].m_resultX;
		CD_RECT[3].m_resultY = CD_RECT[2].m_resultY;
		for(int i=0; i<4; i++)
			CD_RECT[i].m_Success = TRUE;
	}
	else if(checkPatternType[0] == 1 && checkPatternType[1] == 1 && checkPatternType[2] == 0 && checkPatternType[3] == 1){
		CD_RECT[2].m_resultX = CD_RECT[0].m_resultX;
		CD_RECT[2].m_resultY = CD_RECT[3].m_resultY;
		for(int i=0; i<4; i++)
			CD_RECT[i].m_Success = TRUE;
	}
	else if(checkPatternType[0] == 0 && checkPatternType[1] == 1 && checkPatternType[2] == 1 && checkPatternType[3] == 1){
		CD_RECT[0].m_resultX = CD_RECT[2].m_resultX;
		CD_RECT[0].m_resultY = CD_RECT[1].m_resultY;
		for(int i=0; i<4; i++)
			CD_RECT[i].m_Success = TRUE;
	}		
	else if(checkPatternType[0] == 1 && checkPatternType[1] == 0 && checkPatternType[2] == 1 && checkPatternType[3] == 1){
		CD_RECT[1].m_resultX = CD_RECT[3].m_resultX;
		CD_RECT[1].m_resultY = CD_RECT[0].m_resultY;
		for(int i=0; i<4; i++)
			CD_RECT[i].m_Success = TRUE;
	}
	else if(checkPatternType[0] == 1 && checkPatternType[1] == 1 && checkPatternType[2] == 1 && checkPatternType[3] == 1)
	{
		for(int i=0; i<4; i++)
			CD_RECT[i].m_Success = TRUE;
	}
	else
	{
		for(int i=0; i<4; i++)
			CD_RECT[i].m_Success = FALSE;
	}
	
	for(int i=0; i<4; i++)
	{
		if(CD_RECT[i].m_Success == FALSE)
		{
			CD_RECT[i].m_resultX = Standard_CD_RECT[i].m_resultX;
			CD_RECT[i].m_resultY = Standard_CD_RECT[i].m_resultY;
		}
	}

//	cvSaveImage("D:\\ColorAreaResultImage.bmp", RGBResultImage);

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&CannyImage);	
	cvReleaseImage(&RGBResultImage);	

	//delete contour;
}

double CColor_Option::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt( (double)((x2-x1)*(x2-x1)) +  ((y2-y1)*(y2-y1)));

	return result;
}

void CColor_Option::OnNMCustomdrawListColor(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//*pResult = 0;

	 COLORREF text_color = 0;
        COLORREF bg_color = RGB(255, 255, 255);
     
        LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
		
	//	lplvcd->iPartId
        switch(lplvcd->nmcd.dwDrawStage){
            case CDDS_PREPAINT:
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
             
            // 배경 혹은 텍스트를 수정한다.
            case CDDS_ITEMPREPAINT:
                // 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
               /* if(lplvcd->nmcd.dwItemSpec == 0) text_color = RGB(255, 0, 0);
                else if(lplvcd->nmcd.dwItemSpec == 1) text_color = RGB(0, 255, 0);
                else text_color = RGB(0, 0, 255);
                lplvcd->clrText = text_color;*/
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
           
            // 서브 아이템의 배경 혹은 텍스트를 수정한다.
            case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
                if(lplvcd->iSubItem != 0){
                    // 1번째 행이라면...

					if(lplvcd->nmcd.dwItemSpec >=0 ||lplvcd->nmcd.dwItemSpec <4){
						if(AutomationMod ==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 7 ){
								text_color = RGB(0, 0, 0);
								bg_color = RGB(200, 200, 200);
							}
						}
						else{
							text_color = RGB(0, 0, 0);
							bg_color = RGB(255, 255, 255);
						
						}

						if(ChangeCheck==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 13 ){
								for(int t=0; t<4; t++){
									if(lplvcd->nmcd.dwItemSpec == ChangeItem[t]){
										text_color = RGB(0, 0, 0);
										bg_color = RGB(250, 230, 200);					
									}
								}
							}
						
						
						}
					
					}
					

				    lplvcd->clrText = text_color;
                    lplvcd->clrTextBk = bg_color;
					
                }
                *pResult = CDRF_NEWFONT;
                return;
        }
}
//void CColor_Option::OnSetFocus(CWnd* pOldWnd)
//{
//	CDialog::OnSetFocus(pOldWnd);
//
//	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
//}

void CColor_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);
	if(bShow == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
	}
}

void CColor_Option::OnBnClickedButtonCLoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Load_parameter();
	UpdateData(FALSE);
	UploadList();
}


void CColor_Option::OnEnChangeEditCMod()
{
	int num = iSavedItem;
	if((C_RECT[num].m_PosX >= 0)&&(C_RECT[num].m_PosX < m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_PosY >= 0)&&(C_RECT[num].m_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_RECT[num].m_Left>=0)&&(C_RECT[num].m_Left<m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_Top>=0)&&(C_RECT[num].m_Top<m_CAM_SIZE_HEIGHT)&&
		(C_RECT[num].m_Right>=0)&&(C_RECT[num].m_Right<m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_Bottom>=0)&&(C_RECT[num].m_Bottom<m_CAM_SIZE_HEIGHT)&&
		(C_RECT[num].m_Width>=0)&&(C_RECT[num].m_Width<m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_Height>=0)&&(C_RECT[num].m_Height<m_CAM_SIZE_HEIGHT)&&
		(C_RECT[num].m_MinR>=0)&&(C_RECT[num].m_MaxR<=255)&&
		(C_RECT[num].m_MinG>=0)&&(C_RECT[num].m_MaxG<=255)&&
		(C_RECT[num].m_MinB>=0)&&(C_RECT[num].m_MaxB<=255)){

			//((CButton *)GetDlgItem(IDC_BUTTON_C_RECT_SAVE))->EnableWindow(1);
		

	}else{
		//((CButton *)GetDlgItem(IDC_BUTTON_C_RECT_SAVE))->EnableWindow(0);
	}
}


		
void CColor_Option::OnEnChangeColorPosx()
{
	EditMinMaxIntSet(IDC_Color_PosX, &C_Total_PosX,0,m_CAM_SIZE_WIDTH);
	C_Total_PosX = GetDlgItemInt(IDC_Color_PosX);	
	C_Total_PosY = GetDlgItemInt(IDC_Color_PosY);
	C_Dis_Width	= GetDlgItemInt( IDC_Color_Dis_W );
	C_Dis_Height = GetDlgItemInt( IDC_Color_Dis_H);
	C_Total_Width = GetDlgItemInt(IDC_Color_Width);
	C_Total_Height = GetDlgItemInt(IDC_Color_Height);
	C_Thresold = GetDlgItemInt(IDC_Color_Thresold);

	if((C_Total_PosX >= 0)&&(C_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(C_Total_PosY >= 0)&&(C_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_Dis_Width>=0)&&(C_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(C_Dis_Height>=0)&&(C_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Total_Width>=0)&&(C_Total_Width<m_CAM_SIZE_WIDTH)&&
		(C_Total_Height>=0)&&(C_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Thresold>=0)&&(C_Thresold<100)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(0);
	}
}

void CColor_Option::OnEnChangeColorPosy()
{
	EditMinMaxIntSet(IDC_Color_PosY, &C_Total_PosY,0,m_CAM_SIZE_HEIGHT);
	C_Total_PosX = GetDlgItemInt(IDC_Color_PosX);	
	C_Total_PosY = GetDlgItemInt(IDC_Color_PosY);
	C_Dis_Width	= GetDlgItemInt( IDC_Color_Dis_W );
	C_Dis_Height = GetDlgItemInt( IDC_Color_Dis_H);
	C_Total_Width = GetDlgItemInt(IDC_Color_Width);
	C_Total_Height = GetDlgItemInt(IDC_Color_Height);
	C_Thresold = GetDlgItemInt(IDC_Color_Thresold);

	if((C_Total_PosX >= 0)&&(C_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(C_Total_PosY >= 0)&&(C_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_Dis_Width>=0)&&(C_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(C_Dis_Height>=0)&&(C_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Total_Width>=0)&&(C_Total_Width<m_CAM_SIZE_WIDTH)&&
		(C_Total_Height>=0)&&(C_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Thresold>=0)&&(C_Thresold<100)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(0);
	}
}

void CColor_Option::OnEnChangeColorDisW()
{

	EditMinMaxIntSet(IDC_Color_Dis_W, &C_Dis_Width,0,m_CAM_SIZE_WIDTH);
	C_Total_PosX = GetDlgItemInt(IDC_Color_PosX);	
	C_Total_PosY = GetDlgItemInt(IDC_Color_PosY);
	C_Dis_Width	= GetDlgItemInt( IDC_Color_Dis_W );
	C_Dis_Height = GetDlgItemInt( IDC_Color_Dis_H);
	C_Total_Width = GetDlgItemInt(IDC_Color_Width);
	C_Total_Height = GetDlgItemInt(IDC_Color_Height);
	C_Thresold = GetDlgItemInt(IDC_Color_Thresold);

	if((C_Total_PosX >= 0)&&(C_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(C_Total_PosY >= 0)&&(C_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_Dis_Width>=0)&&(C_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(C_Dis_Height>=0)&&(C_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Total_Width>=0)&&(C_Total_Width<m_CAM_SIZE_WIDTH)&&
		(C_Total_Height>=0)&&(C_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Thresold>=0)&&(C_Thresold<100)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(0);
	}
}

void CColor_Option::OnEnChangeColorDisH()
{
	EditMinMaxIntSet(IDC_Color_Dis_H, &C_Dis_Height,0,m_CAM_SIZE_HEIGHT);
	C_Total_PosX = GetDlgItemInt(IDC_Color_PosX);	
	C_Total_PosY = GetDlgItemInt(IDC_Color_PosY);
	C_Dis_Width	= GetDlgItemInt( IDC_Color_Dis_W );
	C_Dis_Height = GetDlgItemInt( IDC_Color_Dis_H);
	C_Total_Width = GetDlgItemInt(IDC_Color_Width);
	C_Total_Height = GetDlgItemInt(IDC_Color_Height);
	C_Thresold = GetDlgItemInt(IDC_Color_Thresold);

	if((C_Total_PosX >= 0)&&(C_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(C_Total_PosY >= 0)&&(C_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_Dis_Width>=0)&&(C_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(C_Dis_Height>=0)&&(C_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Total_Width>=0)&&(C_Total_Width<m_CAM_SIZE_WIDTH)&&
		(C_Total_Height>=0)&&(C_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Thresold>=0)&&(C_Thresold<100)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(0);
	}
}

void CColor_Option::OnEnChangeColorWidth()
{
	EditMinMaxIntSet(IDC_Color_Width, &C_Total_Width,0,m_CAM_SIZE_WIDTH);
	C_Total_PosX = GetDlgItemInt(IDC_Color_PosX);	
	C_Total_PosY = GetDlgItemInt(IDC_Color_PosY);
	C_Dis_Width	= GetDlgItemInt( IDC_Color_Dis_W );
	C_Dis_Height = GetDlgItemInt( IDC_Color_Dis_H);
	C_Total_Width = GetDlgItemInt(IDC_Color_Width);
	C_Total_Height = GetDlgItemInt(IDC_Color_Height);
	C_Thresold = GetDlgItemInt(IDC_Color_Thresold);

	if((C_Total_PosX >= 0)&&(C_Total_PosX < m_CAM_SIZE_WIDTH )&&
		(C_Total_PosY >= 0)&&(C_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_Dis_Width>=0)&&(C_Dis_Width<m_CAM_SIZE_WIDTH )&&
		(C_Dis_Height>=0)&&(C_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Total_Width>=0)&&(C_Total_Width<m_CAM_SIZE_WIDTH )&&
		(C_Total_Height>=0)&&(C_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Thresold>=0)&&(C_Thresold<100)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(0);
	}
}

void CColor_Option::OnEnChangeColorHeight()
{
	EditMinMaxIntSet(IDC_Color_Height, &C_Total_Height,0,m_CAM_SIZE_HEIGHT);
	C_Total_PosX = GetDlgItemInt(IDC_Color_PosX);	
	C_Total_PosY = GetDlgItemInt(IDC_Color_PosY);
	C_Dis_Width	= GetDlgItemInt( IDC_Color_Dis_W );
	C_Dis_Height = GetDlgItemInt( IDC_Color_Dis_H);
	C_Total_Width = GetDlgItemInt(IDC_Color_Width);
	C_Total_Height = GetDlgItemInt(IDC_Color_Height);
	C_Thresold = GetDlgItemInt(IDC_Color_Thresold);

	if((C_Total_PosX >= 0)&&(C_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(C_Total_PosY >= 0)&&(C_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_Dis_Width>=0)&&(C_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(C_Dis_Height>=0)&&(C_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Total_Width>=0)&&(C_Total_Width<m_CAM_SIZE_WIDTH)&&
		(C_Total_Height>=0)&&(C_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Thresold>=0)&&(C_Thresold<100)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(0);
	}
}

void CColor_Option::OnEnChangeColorThresold()
{
	EditMinMaxIntSet(IDC_Color_Thresold, &C_Thresold,0,100);
	C_Total_PosX = GetDlgItemInt(IDC_Color_PosX);	
	C_Total_PosY = GetDlgItemInt(IDC_Color_PosY);
	C_Dis_Width	= GetDlgItemInt( IDC_Color_Dis_W );
	C_Dis_Height = GetDlgItemInt( IDC_Color_Dis_H);
	C_Total_Width = GetDlgItemInt(IDC_Color_Width);
	C_Total_Height = GetDlgItemInt(IDC_Color_Height);
	C_Thresold = GetDlgItemInt(IDC_Color_Thresold);

	if((C_Total_PosX >= 0)&&(C_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(C_Total_PosY >= 0)&&(C_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_Dis_Width>=0)&&(C_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(C_Dis_Height>=0)&&(C_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Total_Width>=0)&&(C_Total_Width<m_CAM_SIZE_WIDTH)&&
		(C_Total_Height>=0)&&(C_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Thresold>=0)&&(C_Thresold<100)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setColorZone))->EnableWindow(0);
	}
}


BOOL CColor_Option::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	int znum = zDelta/120;
	CWnd *focusid;
	focusid = GetFocus();
	bool FLAG=0;
	if(znum > 0 ){
		FLAG = 1;
	}
	else if(znum < 0 ){
		FLAG =0;
	}
	else if(znum == 0){
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	}
	int casenum = focusid->GetDlgCtrlID();
	switch(casenum){
		case IDC_Color_PosX   :
			EditWheelIntSet(IDC_Color_PosX ,znum);
			OnEnChangeColorPosx();
			break;
		case IDC_Color_PosY :
			EditWheelIntSet(IDC_Color_PosY,znum);
			OnEnChangeColorPosy();
			break;
		case  IDC_Color_Dis_W :
			EditWheelIntSet( IDC_Color_Dis_W ,znum);
			OnEnChangeColorDisW();
			break;
		case IDC_Color_Dis_H :
			EditWheelIntSet(IDC_Color_Dis_H,znum);
			OnEnChangeColorDisH();
			break;
		case IDC_Color_Width :
			EditWheelIntSet(IDC_Color_Width,znum);
			OnEnChangeColorWidth();
			break;
		case IDC_Color_Height :
			EditWheelIntSet(IDC_Color_Height,znum);
			OnEnChangeColorHeight();
			break;

		case  IDC_Color_Thresold :
			EditWheelIntSet( IDC_Color_Thresold,znum);
			OnEnChangeColorThresold();
			break;
		
		case IDC_EDIT_C_MOD :
			if(FLAG == 1){
				if((Change_DATA_CHECK(1) == TRUE) /*|| (znum ==-1)*/){
					EditWheelIntSet(IDC_EDIT_C_MOD,znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditCMod();
				}
			}
			if(FLAG == 0){
				if((Change_DATA_CHECK(0) == TRUE) /*|| (znum == 1)*/){
					EditWheelIntSet(IDC_EDIT_C_MOD,znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditCMod();
				}
			}
			

			break;

	}

	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}


bool CColor_Option::Change_DATA_CHECK(bool FLAG){

		
	BOOL STAT = TRUE;
	//상한
	if(C_RECT[iSavedItem].m_Width > m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Height > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosX > m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosY > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Top > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Bottom > m_CAM_SIZE_HEIGHT  )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Left> m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Right> m_CAM_SIZE_WIDTH )
		STAT = FALSE;

	
	/*	if(C_RECT[iSavedItem].m_MinR < 0){
			STAT = FALSE;
		}
	
		if(C_RECT[iSavedItem].m_MinG < 0){
			STAT = FALSE;
		}

		if(C_RECT[iSavedItem].m_MinB < 0){
			STAT = FALSE;
		}

		if(C_RECT[iSavedItem].m_MaxR < 0){
			STAT = FALSE;
		}
	
		if(C_RECT[iSavedItem].m_MaxG < 0){
			STAT = FALSE;
		}

		if(C_RECT[iSavedItem].m_MaxB < 0){
			STAT = FALSE;
		}



		if(C_RECT[iSavedItem].m_MinR >255){
			STAT = FALSE;
		}
	
		if(C_RECT[iSavedItem].m_MinG > 255){
			STAT = FALSE;
		}

		if(C_RECT[iSavedItem].m_MinB > 255){
			STAT = FALSE;
		}

		if(C_RECT[iSavedItem].m_MaxR > 255){
			STAT = FALSE;
		}
	
		if(C_RECT[iSavedItem].m_MaxG > 255){
			STAT = FALSE;
		}

		if(C_RECT[iSavedItem].m_MaxB > 255){
			STAT = FALSE;
		}*/

	//하한

	if(C_RECT[iSavedItem].m_Width < 1 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Height < 1 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosX < 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosY < 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Top < 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Bottom < 0  )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Left< 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Right< 0 )
		STAT = FALSE;

	if(FLAG  == 1){
		if(C_RECT[iSavedItem].m_Width == 1 )
			STAT = FALSE;
		if(C_RECT[iSavedItem].m_Height == 1 )
			STAT = FALSE;
		
	}


	return STAT;
	

	
	

}
void CColor_Option::EditWheelIntSet(int nID,int Val)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = 0;
	}else{
		buf = GetDlgItemInt(nID);
		buf+= Val;
	}
	SetDlgItemInt(nID,buf,1);
	((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
}
void CColor_Option::EditMinMaxIntSet(int nID,int* Val,int Min,int Max)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = Min;
		retval = FALSE;
	}else{
		buf = GetDlgItemInt(nID);
		if(buf<Min){
			buf = *Val;
			retval = TRUE;
		}else if(buf > Max){
			buf = *Val;
			retval = TRUE;
		}else{
			retval = FALSE;
		}
	}
	*Val = buf;
	if(retval == TRUE){
		SetDlgItemInt(nID,buf,1);
		((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
	}
}
void CColor_Option::OnBnClickedButtontest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(0);
	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(1);

}

CvRect CColor_Option::GetCenterPoint(LPBYTE IN_RGB)
{	
	CvRect resultRect = cvRect(-99999, -99999, 0, 0);

	IplImage *OriginImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	IplImage *PreprecessedImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	
	IplImage *RGBOrgImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 3);

	BYTE R,G,B;
	double Sum_Y;

	for (int y=0; y<CAM_IMAGE_HEIGHT; y++)
	{
		for (int x=0; x<CAM_IMAGE_WIDTH; x++)
		{
		/*	B = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 2];
			
			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;		*/

			B = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 2];

			if( R < 100 && G < 100 && B < 100)
				OriginImage->imageData[y*OriginImage->widthStep+x] = 255;
			else
				OriginImage->imageData[y*OriginImage->widthStep+x] = 0;

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}	
	
//	cvSaveImage("C:\\CenterImage.bmp", OriginImage);
//	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
//	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);

	cvCopyImage(OriginImage, SmoothImage);
	
	cvCanny(SmoothImage, CannyImage, 0, 255);

//	cvDilate(CannyImage, DilateImage);
	
	cvCopyImage(CannyImage, temp_PatternImage);
	
	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);
	
//	cvSaveImage("C:\\CenterImage.bmp", CannyImage);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
		counter++;
	
	if(counter == 0){

		return resultRect;
	}
	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area=0, arcCount=0;
	counter = 0;
	double old_dist = 999999;
	
	int old_center_pos_x = 0;
	int old_center_pos_y = 0;
	int center_pt_x = 0;
	int center_pt_y = 0;
	int obj_Cnt = 0;
	int size=0, old_size = 0;	

	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;

		double circularity = (4.0*3.14*area)/(arcCount*arcCount);		

		if(circularity > 0.8)
		{
		
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;

			if(center_x > 270 && center_x < 450 && center_y > 160 && center_y < 320)
			{
				if(rect.width < CAM_IMAGE_WIDTH/2 && rect.height < CAM_IMAGE_HEIGHT/2 && rect.width > 10 && rect.height > 10)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
					
					/*center_pt_x = center_pt_x+ center_x;
					center_pt_y = center_pt_y+ center_y;
					obj_Cnt++;

					old_center_pos_x = center_pt_x/obj_Cnt;
					old_center_pos_y = center_pt_y/obj_Cnt;*/
					
					double distance = GetDistance(rect.x + rect.width/2, rect.y+rect.height/2, 360, 240);
					
					obj_Cnt++;

					size = rect.width * rect.height;
					
					if(distance < old_dist)
					{
						old_size = size;
						resultRect.x = rect.x;
						resultRect.y = rect.y;
						resultRect.width = rect.width;
						resultRect.height = rect.height;
						old_dist = distance;
					}

					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 0), 1, 8);  
				
				}
			}
		}
		
		counter++;
	}
//	cvSaveImage("D:\\RGBResultImage.bmp", RGBResultImage);
	cvReleaseMemStorage(&contour_storage);
	
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&PreprecessedImage);
	cvReleaseImage(&CannyImage);

	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
	cvReleaseImage(&RGBOrgImage);

	//delete contour;
	//delete temp_contour;
	delete []rectArray;
	delete []areaArray;
	
	/*if(obj_Cnt != 0)
	{
		resultPt.x = center_pt_x / obj_Cnt;
		resultPt.y = center_pt_y / obj_Cnt;
	}*/
/*	if(obj_Cnt != 0)
	{
		resultPt.x = center_pt_x;
		resultPt.y = center_pt_y;
	}*/
		
	return resultRect;	
}
#pragma region LOT관련 함수
void CColor_Option::LOT_Set_List(CListCtrl *List){

	while(List->DeleteColumn(0));
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT 명",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"1z_R",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"1z_G",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"1z_B",LVCFMT_CENTER, 80);
	List->InsertColumn(8,"2z_R",LVCFMT_CENTER, 80);
	List->InsertColumn(9,"2z_G",LVCFMT_CENTER, 80);
	List->InsertColumn(10,"2z_B",LVCFMT_CENTER, 80);
	List->InsertColumn(11,"3z_R",LVCFMT_CENTER, 80);
	List->InsertColumn(12,"3z_G",LVCFMT_CENTER, 80);
	List->InsertColumn(13,"3z_B",LVCFMT_CENTER, 80);
	List->InsertColumn(14,"4z_R",LVCFMT_CENTER, 80);
	List->InsertColumn(15,"4z_G",LVCFMT_CENTER, 80);
	List->InsertColumn(16,"4z_B",LVCFMT_CENTER, 80);    
	List->InsertColumn(17,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(18,"비고",LVCFMT_CENTER, 80);
	Lot_InsertNum =19;
	
}

void CColor_Option::LOT_InsertDataList(){
	CString strCnt="";

	int Index = m_Lot_ColorList.InsertItem(Lot_StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index+1);
	m_Lot_ColorList.SetItemText(Index,0,strCnt);

	int CopyIndex = m_ColorList.GetItemCount()-1;

	int count =0;
	for(int t=0; t< Lot_InsertNum;t++){
		if((t != 2)&&(t!=0)){
			m_Lot_ColorList.SetItemText(Index,t, m_ColorList.GetItemText(CopyIndex,count));
			count++;

		}else if(t==0){
			count++;
		}else{
			m_Lot_ColorList.SetItemText(Index,t,((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;

}
void CColor_Option::LOT_EXCEL_SAVE(){
	CString Item[12]={"1z_R","1z_G","1z_B","2z_R","2z_G","2z_B","3z_R","3z_G","3z_B","4z_R","4z_G","4z_B"};

	CString Data ="";
	CString str="";
	str = "***********************컬러 검사***********************\n";
	Data += str;
	str.Empty();
	str.Format("Left Top ,Min,Max,,R,%d,%d,G,%d,%d,B,%d,%d,,/, ",C_RECT[0].m_MinR,C_RECT[0].m_MaxR,C_RECT[0].m_MinG,C_RECT[0].m_MaxG,C_RECT[0].m_MinB,C_RECT[0].m_MaxB);
	Data += str;
	str.Empty();
	str.Format("Right Top ,Min,Max,,R,%d,%d,G,%d,%d,B,%d,%d,,/, \n",C_RECT[1].m_MinR,C_RECT[1].m_MaxR,C_RECT[1].m_MinG,C_RECT[1].m_MaxG,C_RECT[1].m_MinB,C_RECT[1].m_MaxB);
	Data += str;
	str.Empty();
	str.Format("Left Bottom ,Min,Max,,R,%d,%d,G,%d,%d,B,%d,%d,,/,",C_RECT[2].m_MinR,C_RECT[2].m_MaxR,C_RECT[2].m_MinG,C_RECT[2].m_MaxG,C_RECT[2].m_MinB,C_RECT[2].m_MaxB);
	Data += str;
	str.Empty();
	str.Format("Right Bottom ,Min,Max,,R,%d,%d,G,%d,%d,B,%d,%d,,/, \n",C_RECT[3].m_MinR,C_RECT[3].m_MaxR,C_RECT[3].m_MinG,C_RECT[3].m_MaxG,C_RECT[3].m_MinB,C_RECT[3].m_MaxB);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("COLOR검사",Item,12,&m_Lot_ColorList,Data);
}

bool CColor_Option::LOT_EXCEL_UPLOAD(){
	m_Lot_ColorList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("COLOR검사",&m_Lot_ColorList,1,&Lot_StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}

#pragma endregion 