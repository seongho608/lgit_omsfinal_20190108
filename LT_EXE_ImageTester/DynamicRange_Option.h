#pragma once
#include "afxcmn.h"
#include "ETCUSER.H"


// CDynamicRange_Option 대화 상자입니다.

typedef struct _ST_MES_DR
{
	CString szDataBW;
	CString szDataDR;

	int		nResultBW;
	int		nResultDR;

	_ST_MES_DR()
	{
		Reset();
	};

	void Reset()
	{
		szDataBW.Empty();
		szDataDR.Empty();

		nResultBW = TEST_NG;
		nResultDR = TEST_NG;
	}
}ST_MES_DR;

class CDynamicRange_Option : public CDialog
{
	DECLARE_DYNAMIC(CDynamicRange_Option)

public:
	CDynamicRange_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDynamicRange_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_DYNAMICRANGE };
	void	Setup(CWnd* IN_pMomWnd, CEdit	*pTEDIT, tINFO INFO);

	int m_CAM_SIZE_WIDTH;
	int m_CAM_SIZE_HEIGHT;

	CString DR_filename;
	void	InitPrm();
	void	SetList();
	void	UploadList();
	void	Change_DATA();
	void	List_COLOR_Change();
	void	EditWheelIntSet(int nID, int Val);
	bool	Change_DATA_CHECK(bool FLAG);
	int		ChangeItem[9];
	int		iSavedItem, iSavedSubitem;
	int		changecount;
	bool	ChangeCheck;
	CRect	rect;
	CRectData DR_RECT[6];
	int		m_Brightness[3];
	void	Save_parameter();
	void	Load_parameter();

	// TEST 관련
	tResultVal	Run();
	
	// mes
	CString Mes_Result();
	void	CopyMesData();
	BOOL	m_b_StopFail;
	CString m_szMesResult;
	ST_MES_DR	m_stMes[4];

	void	Pic(CDC *cdc);
	bool	DynamicRangePic(CDC *cdc, int NUM);
	bool	SNRGen_16bit(WORD *GRAYScanBuf, int NUM);
	void	SignalnNoiseGen(WORD *GRAYScanBuf, int NUM, int ROINUM);
	// LOG List
	void	Set_List(CListCtrl *List);
	int		ListItemNum;
	int		InsertIndex;
	int		StartCnt;
	void	InsertList();
	void	EXCEL_SAVE();
	void	FAIL_UPLOAD();
	bool	EXCEL_UPLOAD();

	// LOT LOG List
	void 	LOT_Set_List(CListCtrl *List);
	int		Lot_InsertNum;
	int		Lot_InsertIndex;
	int		Lot_StartCnt;
	void	LOT_InsertDataList();
	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();


private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CListCtrl m_DynamicRangeList;
	CListCtrl m_Lot_DynamicRangeList;
	CListCtrl DR_DATALIST;
	afx_msg void OnNMClickListDynamicrangelist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawListDynamicrangelist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListDynamicrangelist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnChangeEditDrMod();
	afx_msg void OnEnKillfocusEditDrMod();
	afx_msg void OnBnClickedButtonDrLoad();
	afx_msg void OnBnClickedButtonDrSavezone();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnBnClickedButtonTest();
	double SNRBW;
	afx_msg void OnBnClickedButtonDrSave();
	afx_msg void OnBnClickedButtonSetdrzone();
	afx_msg void OnEnChangeEditBrightness();
	afx_msg void OnEnChangeEditBrightness2();
	afx_msg void OnEnChangeEditBrightness3();
};
