#include "StdAfx.h"
#include "IoControl.h"

CIoControl::CIoControl(LPVOID p)
{
	memset(m_bInStatus, 0x00, sizeof(BOOL) * MAX_IO_PORT);
	memset(m_bOutTest, 0x00, sizeof(BOOL) * MAX_IO_PORT);

	m_pThread = NULL;
}

CIoControl::~CIoControl(void)
{
	if(m_pThread)
	{
		m_bRun = FALSE;
		DestroyThread();
	}
}

UINT CIoControl::Thread_Fun(LPVOID p)
{
	CIoControl* pMain = static_cast<CIoControl*>(p);

	int iReturn = pMain->ThreadFunction();

	return 0;
}


// INPUT 상태값 읽어오기
BOOL	CIoControl::GetIOInStatus(int nPort){	return m_bInStatus[nPort]; }
BOOL	CIoControl::GetIOOutStatus(int nPort){	return m_bOutTest[nPort]; }


// INPUT 체크 시작하기
BOOL CIoControl::StartIOMonitoring()
{
	m_pThread = AfxBeginThread(Thread_Fun, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);

	if(m_pThread == NULL)
		TRACE(_T("BOOL CIoControl::StartIOMonitoring() = Create Thread Fail!\r\n"));

	m_pThread->m_bAutoDelete = FALSE;
	m_pThread->ResumeThread();
	m_bRun = TRUE;
	return TRUE;
}

// OUTPUT 제어하기
BOOL CIoControl::SetIOOutPort(int nPort, BOOL bState)
{
	long		lBoardNo;
	long		lModulePos;
	DWORD		dwModuleID;

	if(AxdInfoGetModule(0, &lBoardNo, &lModulePos, &dwModuleID) == AXT_RT_SUCCESS)
	{
		AxdoWriteOutportBit(0, nPort, bState);
		m_bOutTest[nPort] = bState;
	}
	else
		return FALSE;

	return TRUE;
}

int CIoControl::ThreadFunction()
{
	int			iOffset;
	long		lBoardNo;
	long		lModulePos;
	DWORD		dwModuleID;
	DWORD		dwPortStatus;

	while(m_bRun)
	{
		AxdInfoGetModule(0, &lBoardNo, &lModulePos, &dwModuleID);

		switch(dwModuleID)
		{
			case AXT_SIO_DB32T:
				for(iOffset=0; iOffset<MAX_IO_PORT; iOffset++)
				{
					AxdiReadInportBit(0, iOffset, &dwPortStatus);

					if(dwPortStatus == 0x01)
						m_bInStatus[iOffset] = TRUE;
					else
						m_bInStatus[iOffset] = FALSE;
				}
			break;
		}

		DoEvents(10);
		
	}
	return 0;
}

BOOL CIoControl::DestroyThread()
{
	if(m_pThread)
	{
		m_bRun = FALSE;
		DWORD dwResult = ::WaitForSingleObject(m_pThread->m_hThread, INFINITE);

		if(dwResult == WAIT_TIMEOUT)
			TRACE(_T("BOOL CIoControl::DestroyThread() = Time Out!\r\n"));
		else if(dwResult == WAIT_OBJECT_0)
			TRACE(_T("BOOL CIoControl::DestroyThread() = Thread End\r\n"));

		delete m_pThread;
		m_pThread = NULL;
	}
	return TRUE;
}

void CIoControl::DoEvents()
{
	MSG msg;

	if(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		if(!AfxGetApp()->PreTranslateMessage(&msg)) 
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
	}

	::Sleep(0);
}

void CIoControl::DoEvents(DWORD dwMiliSeconds)
{
	clock_t start_tm = clock();
	do 
	{
		DoEvents();
		::Sleep(1);
	} while ((DWORD)(clock() - start_tm)<dwMiliSeconds);
}