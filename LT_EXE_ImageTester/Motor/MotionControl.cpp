#include "stdafx.h"
#include "MotionControl.h"

#include <math.h>

#include "AXM.h"
#include "AXL.h"
#include "AXHS.h"
#include "AXDev.h"

#if defined(_WIN64)
	#pragma comment(lib, "AxL_x64")
#else
	#pragma comment(lib, "AxL")
#endif

// Step : FALSE,    Servo : TRUE
BOOL m_AxisEncoder[TOTAL_NUM_AXIS]							= { TRUE,		TRUE,			TRUE,			TRUE,		TRUE		};
double m_AxisEncoderRatio[TOTAL_NUM_AXIS]					= { 1.0,		 1.0,			1.0,			1.0,		1.0			};
double m_AxisDirection[TOTAL_NUM_AXIS]						= { 1.0,		 1.0,			1.0,			1.0,		1.0			};
int m_AxisEncoderDirection[TOTAL_NUM_AXIS]					= { 1,			 1,				1,				1,			1			};
int m_AxisEncInputMethod[TOTAL_NUM_AXIS]					= { 2,			 2,				2,				7,			7			};	// 3 => 4, 7 => -4 체배
double m_AxisPulse[TOTAL_NUM_AXIS]							= { 10000.0,	 10000.0,		10000.0,		10000.0,	10000.0		};	// 1 회전당 엔코더 펄스 수 [pulse]
double m_AxisUnit[TOTAL_NUM_AXIS]							= { 10000.0,	 10000.0,		10000.0,		10000.0,	10000.0		};	// 1 회전당 이송 거리 [um]
double m_AxisStepMin[TOTAL_NUM_AXIS]						= { 0.001,		 0.001,			0.001,			0.001,		0.001		};
double m_AxisStepMax[TOTAL_NUM_AXIS]						= { 1000000.0,	 1000000.0,		1000000.0,		1000000.0,	1000000.0	};
int m_AxisHomeDir[TOTAL_NUM_AXIS]							= {	0,			 0,				1,				0,			0			};	//0:마이너스방향, 1:플러스방향 -1:미지정
int m_AxisHomeSig[TOTAL_NUM_AXIS]							= { 0,			 1,				0,				1,			1			};  //0:+Limit / 1:-Limit / 4:HomeSensor
int m_AxismZphas[TOTAL_NUM_AXIS]							= { 0,			 0,				0,				0,			0			};
AXT_MOTION_PULSE_OUTPUT m_AxisOutputMethod[TOTAL_NUM_AXIS]	= {TwoCwCcwHigh, TwoCwCcwHigh, TwoCwCcwHigh, TwoCwCcwHigh, TwoCcwCwHigh	};

#pragma warning(disable:4244)

CMotionControl::CMotionControl(void)
{
	m_pIoControl = new CIoControl(this);

	m_bRun = TRUE;
	m_bStop = FALSE;
	m_bMotorTest = FALSE;
	m_bMotorStandby = FALSE;
	m_bMotorOriginStandby = FALSE;

	m_bOriginAllStop = FALSE;

	m_bMotionBoardOpen = FALSE;

	m_iMode	= ACTION_NONE;

	m_nTestStep = 0;

	m_dbMotorWarningPos = LIMIT_CYL_WARNING;

	m_bMotorWarning = FALSE;

	m_OriginMode = -1;

	m_nAxisInitParam = 0;
	m_pAxisInitParam = NULL;
	
	memset(&m_Idx, 0x00, sizeof(int)*TOTAL_NUM_AXIS);
	memset(&m_dbPos, 0x00, sizeof(double)*TOTAL_NUM_AXIS);
	memset(&m_dbStandPos, 0x00, sizeof(double)*TOTAL_NUM_AXIS);

	memset(&param, 0x00, sizeof(MOTIONPARAM)*TOTAL_NUM_AXIS);
	memset(&m_OriginInfo, 0x00, sizeof(MORTORORGININFO)*TOTAL_NUM_AXIS);
}

CMotionControl::~CMotionControl(void)
{
	m_bStop = FALSE;

	if(m_pIoControl)
	{
		delete m_pIoControl;
		m_pIoControl = NULL;
	}
}

// 정보 로드
void CMotionControl::LoadParameter()
{
//	CString strPath = GetCurrentFilePath() + _T("\\INI\\Axis.ini");
	// [07/10/2014] Young 소스디버깅시 모터 위치정보가 섞일수 있으니 소스폴더와 분리하자.
	CString strPath = _T("");
	strPath.Format(_T("%s"), FILE_AXIS);
	LoadParameter(strPath);
}

void CMotionControl::LoadParameter(LPCTSTR szFile)
{
	LoadData(szFile);
}

void CMotionControl::LoadJogParameter()
{
//	CString strPath = GetCurrentFilePath() + _T("\\INI\\Jog.ini");
	// [07/10/2014] Young 소스디버깅시 모터 위치정보가 섞일수 있으니 소스폴더와 분리하자.
	CString strPath = _T("");
	strPath.Format(_T("%s"), FILE_JOG);
	LoadJogData(strPath);
}

void CMotionControl::LoadJogParameter(LPCTSTR szFile)
{
	LoadJogData(szFile);
}

void CMotionControl::LoadData(LPCTSTR szFile)
{
	TCHAR szApp[MAX_PATH] = {0, };
	TCHAR szKey[MAX_PATH] = {0, };
	TCHAR szBuf[MAX_PATH] = {0, };

	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
	{
		_stprintf(szApp, _T("MotorAxis #%d"), _a);
		_tcscpy(szKey, _T("MaxVel"));
		GetPrivateProfileString(szApp, szKey, _T("10000"), szBuf, MAX_PATH, szFile);
		param[_a].MaxVel = _ttoi(szBuf);

		_tcscpy(szKey, _T("MinVel"));
		GetPrivateProfileString(szApp, szKey, _T("10"), szBuf, MAX_PATH, szFile);
		param[_a].MinVel = _ttoi(szBuf);

		_tcscpy(szKey, _T("Acc"));
		GetPrivateProfileString(szApp, szKey, _T("25000"), szBuf, MAX_PATH, szFile);
		param[_a].Acc = _ttoi(szBuf);

		_tcscpy(szKey, _T("Dec"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].Dec = _ttoi(szBuf);

		_tcscpy(szKey, _T("CoarseVel"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].CoarseVel = _ttoi(szBuf);

		_tcscpy(szKey, _T("FineVel"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].FineVel = _ttoi(szBuf);

		_tcscpy(szKey, _T("OriginCoarseVel"));
		GetPrivateProfileString(szApp, szKey, _T("5000"), szBuf, MAX_PATH, szFile);
		param[_a].OriginCoarseVel = _ttoi(szBuf);

		_tcscpy(szKey, _T("OriginFineVel"));
		GetPrivateProfileString(szApp, szKey, _T("100"), szBuf, MAX_PATH, szFile);
		param[_a].OriginFineVel = _ttoi(szBuf);

		_tcscpy(szKey, _T("NegLimit"));
		GetPrivateProfileString(szApp, szKey, _T("1000000"), szBuf, MAX_PATH, szFile);
		param[_a].NegLimit = _ttoi(szBuf);

		_tcscpy(szKey, _T("PosLimit"));
		GetPrivateProfileString(szApp, szKey, _T("1000000"), szBuf, MAX_PATH, szFile);
		param[_a].PosLimit = _ttoi(szBuf);

		_tcscpy(szKey, _T("BufNegLimit"));
		GetPrivateProfileString(szApp, szKey, _T("1000000"), szBuf, MAX_PATH, szFile);
		param[_a].BufNegLimit = _ttoi(szBuf);

		_tcscpy(szKey, _T("BufPosLimit"));
		GetPrivateProfileString(szApp, szKey, _T("1000000"), szBuf, MAX_PATH, szFile);
		param[_a].BufPosLimit = _ttoi(szBuf);

		_tcscpy(szKey, _T("Numerator"));
		GetPrivateProfileString(szApp, szKey, _T("1"), szBuf, MAX_PATH, szFile);
		param[_a].Numerator = _ttoi(szBuf);

		_tcscpy(szKey, _T("Denominator"));
		GetPrivateProfileString(szApp, szKey, _T("1"), szBuf, MAX_PATH, szFile);
		param[_a].Denominator = _ttoi(szBuf);

		_tcscpy(szKey, _T("Unit"));
		GetPrivateProfileString(szApp, szKey, _T("360"), szBuf, MAX_PATH, szFile);
		param[_a].dbnit = _ttoi(szBuf);

		_tcscpy(szKey, _T("Pulse"));
		GetPrivateProfileString(szApp, szKey, _T("800"), szBuf, MAX_PATH, szFile);
		param[_a].Pulse = _ttoi(szBuf);

		_tcscpy(szKey, _T("GearRatio"));
		GetPrivateProfileString(szApp, szKey, _T("1"), szBuf, MAX_PATH, szFile);
		param[_a].GearRatio = _ttoi(szBuf);

		_tcscpy(szKey, _T("OriginOffset"));
		GetPrivateProfileString(szApp, szKey, _T("1000"), szBuf, MAX_PATH, szFile);
		param[_a].OriginOffset = _ttoi(szBuf);

		_tcscpy(szKey, _T("GantryOffset"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].GantryOffset = _ttoi(szBuf);

		_tcscpy(szKey, _T("OffsetRange"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].OffsetRange = _ttoi(szBuf);

		_tcscpy(szKey, _T("OutputMethod"));
		GetPrivateProfileString(szApp, szKey, _T("6"), szBuf, MAX_PATH, szFile);
		param[_a].OutPutMethod = _ttoi(szBuf);

		_tcscpy(szKey, _T("EncoderMethod"));
		GetPrivateProfileString(szApp, szKey, _T("3"), szBuf, MAX_PATH, szFile);
		param[_a].EncoderMethod = _ttoi(szBuf);

		_tcscpy(szKey, _T("CoordDirection"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].CoordDirection = _ttoi(szBuf);

		_tcscpy(szKey, _T("MotorType"));
		GetPrivateProfileString(szApp, szKey, _T("1"), szBuf, MAX_PATH, szFile);
		param[_a].MotorType = _ttoi(szBuf);

		_tcscpy(szKey, _T("AxisStatus"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].AxisStatus = _ttoi(szBuf);

		_tcscpy(szKey, _T("EncoderType"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].EncoderType = _ttoi(szBuf);

		_tcscpy(szKey, _T("StepMotorSmoothing"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].StepMotorSmoothing = _ttoi(szBuf);

		_tcscpy(szKey, _T("VelPulse"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].VelPulse = _ttoi(szBuf);

		_tcscpy(szKey, _T("MotorInpositionLevel"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].MotorInpositionLevel = _ttoi(szBuf);

		_tcscpy(szKey, _T("MotorAmpEnableLevel"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].MotorAmpEnableLevel = _ttoi(szBuf);

		_tcscpy(szKey, _T("MotorAmpResetLevel"));
		GetPrivateProfileString(szApp, szKey, _T("1"), szBuf, MAX_PATH, szFile);
		param[_a].MotorAmpResetLevel = _ttoi(szBuf);

		_tcscpy(szKey, _T("MotorAmpFaultLevel"));
		GetPrivateProfileString(szApp, szKey, _T("1"), szBuf, MAX_PATH, szFile);
		param[_a].MotorAmpFaultLevel = _ttoi(szBuf);

		_tcscpy(szKey, _T("MotorHomeSensorLevel"));
		GetPrivateProfileString(szApp, szKey, _T("1"), szBuf, MAX_PATH, szFile);
		param[_a].MotorHomeSensorLevel = _ttoi(szBuf);

		_tcscpy(szKey, _T("MotorNegaLimitLevel"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].MotorNegaLimitLevel = _ttoi(szBuf);

		_tcscpy(szKey, _T("MotorPosiLimitLevel"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].MotorPosiLimitLevel = _ttoi(szBuf);

		_tcscpy(szKey, _T("GantrySelect"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].GantrySelect = _ttoi(szBuf);

		_tcscpy(szKey, _T("StartSpeed"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].StartSpeed = _ttoi(szBuf);

		_tcscpy(szKey, _T("ServoAlarmLevel"));
		GetPrivateProfileString(szApp, szKey, _T("1"), szBuf, MAX_PATH, szFile);
		param[_a].ServoAlarmLevel = _ttoi(szBuf);

		_tcscpy(szKey, _T("StopLevel"));
		GetPrivateProfileString(szApp, szKey, _T("2"), szBuf, MAX_PATH, szFile);
		param[_a].StopLevel = _ttoi(szBuf);

		_tcscpy(szKey, _T("HomeDirection"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].nHomeDir = _ttoi(szBuf);

		_tcscpy(szKey, _T("LimitType"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].nLimitType = _ttoi(szBuf);

		_tcscpy(szKey, _T("MinPosLimit"));
		GetPrivateProfileString(szApp, szKey, _T("0"), szBuf, MAX_PATH, szFile);
		param[_a].dbMinPosLimit = _tstof(szBuf);

		_tcscpy(szKey, _T("MaxPosLimit"));
		GetPrivateProfileString(szApp, szKey, _T("999999"), szBuf, MAX_PATH, szFile);
		param[_a].dbMaxPosLimit = _tstof(szBuf);

		_tcscpy(szKey, _T("OFFSET_MM"));
		GetPrivateProfileString(szApp, szKey, _T("1"), szBuf, MAX_PATH, szFile);
		param[_a].nOffsetmm = _ttoi(szBuf);

		_tcscpy(szKey, _T("PULSE/MM"));
		GetPrivateProfileString(szApp, szKey, _T("500"), szBuf, MAX_PATH, szFile);
		param[_a].nMMperPulse = _ttoi(szBuf);
	}
	
	// 테스트 STEP 수
	_tcscpy(szApp, _T("MOTOR_TEST_STEP"));
	_tcscpy(szKey, _T("STEP_COUNT"));
	GetPrivateProfileString(szApp, szKey, _T(""), szBuf, MAX_PATH, szFile);
	m_nTestStep = _ttoi(szBuf);

	// 테스트 STEP
	for(int _a=0; _a<m_nTestStep; _a++)
	{
		_tcscpy(szApp, _T("MOTOR_STEP_INDEX"));
		_stprintf(szKey, _T("INDEX_%d"), _a);
		GetPrivateProfileString(szApp, szKey, _T(""), szBuf, MAX_PATH, szFile);
		m_Idx[_a] = _ttoi(szBuf);

		_tcscpy(szApp, _T("MOTOR_TEST_POS"));
		_stprintf(szKey, _T("STEP_%d"), _a);
		GetPrivateProfileString(szApp, szKey, _T(""), szBuf, MAX_PATH, szFile);
		m_dbPos[_a] = _ttoi(szBuf);

		_tcscpy(szApp, _T("MOTOR_STANDBY_POS"));
		_stprintf(szKey, _T("STEP_%d"), _a);
		GetPrivateProfileString(szApp, szKey, _T(""), szBuf, MAX_PATH, szFile);
		m_dbStandPos[_a] = _ttoi(szBuf);
	}
}

void CMotionControl::LoadJogData(LPCTSTR szFile)
{
	TCHAR szApp[MAX_PATH] = {0, };
	TCHAR szKey[MAX_PATH] = {0, };
	TCHAR szBuf[MAX_PATH] = {0, };

	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
	{
		_stprintf(szApp, _T("Axis_JOG CONFIG - #%d "), _a);
		_tcscpy(szKey, _T("STEP"));
		GetPrivateProfileString(szApp, szKey, _T(""), szBuf, MAX_PATH, szFile);
		param[_a].jog.JogStep = _ttoi(szBuf);

		_tcscpy(szKey, _T("SPEED"));
		GetPrivateProfileString(szApp, szKey, _T(""), szBuf, MAX_PATH, szFile);
		param[_a].jog.JogVel = _ttoi(szBuf);

		_tcscpy(szKey, _T("JOG MODE"));
		GetPrivateProfileString(szApp, szKey, _T(""), szBuf, MAX_PATH, szFile);
		param[_a].jog.JogMode = _ttoi(szBuf);

		_tcscpy(szApp, _T("GENRAL_JOG_CONFIG"));
		_tcscpy(szKey, _T("STEP"));
		GetPrivateProfileString(szApp, szKey, _T(""), szBuf, MAX_PATH, szFile);
		param[_a].jog.GeneralJogStep = _ttoi(szBuf);

		_tcscpy(szKey, _T("SPEED"));
		GetPrivateProfileString(szApp, szKey, _T(""), szBuf, MAX_PATH, szFile);
		param[_a].jog.GeneralJogVel = _ttoi(szBuf);

		_tcscpy(szKey, _T("JOG MODE"));
		GetPrivateProfileString(szApp, szKey, _T(""), szBuf, MAX_PATH, szFile);
		param[_a].jog.GeneralJogMode = _ttoi(szBuf);
	}
}

// 정보 저장
void CMotionControl::SaveParameter()
{

}

void CMotionControl::SaveParameter(int nAxisNum)
{
	SaveData(nAxisNum);
	SaveJogData(nAxisNum);
}

void CMotionControl::SaveData(int nAxisNum)
{
	TCHAR szApp[MAX_PATH] = {0, };
	TCHAR szKey[MAX_PATH] = {0, };
	TCHAR szBuf[MAX_PATH] = {0, };
	TCHAR szTemp[MAX_PATH] = {0, };
	TCHAR szFile[MAX_PATH] = {0, };

	_tcscpy(szFile, FILE_AXIS);

	_stprintf(szApp, _T("MotorAxis #%d"), nAxisNum);
	_tcscpy(szKey, _T("MaxVel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].MaxVel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("MinVel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].MinVel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);
	
	_tcscpy(szKey, _T("Acc"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].Acc);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("Dec"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].Dec);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("CoarseVel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].CoarseVel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("FineVel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].FineVel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("OriginCoarseVel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].OriginCoarseVel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("OriginFineVel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].OriginFineVel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("NegLimit"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].NegLimit);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("PosLimit"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].PosLimit);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("BufNegLimit"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].BufNegLimit);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("BufPosLimit"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].BufPosLimit);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("Numerator"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].Numerator);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("Denominator"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].Denominator);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("Unit"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].dbnit);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("Pulse"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].Pulse);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("GearRatio"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].GearRatio);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("OriginOffset"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].OriginOffset);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("GantryOffset"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].GantryOffset);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("OffsetRange"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].OffsetRange);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("OutputMethod"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].OutPutMethod);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("EncoderMethod"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].EncoderMethod);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("CoordDirection"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].CoordDirection);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("MotorType"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].MotorType);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("AxisStatus"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].AxisStatus);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("EncoderType"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].EncoderType);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("StepMotorSmoothing"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].StepMotorSmoothing);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("VelPulse"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].VelPulse);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("MotorInpositionLevel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].MotorInpositionLevel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("MotorAmpEnableLevel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].MotorAmpEnableLevel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("MotorAmpResetLevel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].MotorAmpResetLevel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("MotorAmpFaultLevel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].MotorAmpFaultLevel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("MotorHomeSensorLevel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].MotorHomeSensorLevel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("MotorNegaLimitLevel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].MotorNegaLimitLevel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("MotorPosiLimitLevel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].MotorPosiLimitLevel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("GantrySelect"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].GantrySelect);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("StartSpeed"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].StartSpeed);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("ServoAlarmLevel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].ServoAlarmLevel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("StopLevel"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].StopLevel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("HomeDirection"));
	_stprintf(szTemp, _T("%d"), param[nAxisNum].nHomeDir);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("LimitType"));
	_stprintf(szTemp, _T("%d"), param[nAxisNum].nLimitType);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("MinPosLimit"));
	_stprintf(szTemp, _T("%f"), param[nAxisNum].dbMinPosLimit);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("MaxPosLimit"));
	_stprintf(szTemp, _T("%f"), param[nAxisNum].dbMaxPosLimit);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("OFFSET_MM"));
	_stprintf(szTemp, _T("%d"), param[nAxisNum].nOffsetmm);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("PULSE/MM"));
	_stprintf(szTemp, _T("%d"), param[nAxisNum].nMMperPulse);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);
}

void CMotionControl::SaveJogData(int nAxisNum)
{
	TCHAR szApp[MAX_PATH] = {0, };
	TCHAR szKey[MAX_PATH] = {0, };
	TCHAR szBuf[MAX_PATH] = {0, };
	TCHAR szTemp[MAX_PATH] = {0, };
	TCHAR szFile[MAX_PATH] = {0, };

	_tcscpy(szFile, FILE_JOG);

	_stprintf(szApp, _T("Axis_JOG CONFIG - #%d "), nAxisNum);
	_tcscpy(szKey, _T("STEP"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].jog.JogStep);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);
	
	_tcscpy(szKey, _T("SPEED"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].jog.JogVel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("JOG MODE"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].jog.JogMode);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szApp, _T("GENRAL_JOG_CONFIG"));
	_tcscpy(szKey, _T("STEP"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].jog.GeneralJogStep);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("SPEED"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].jog.GeneralJogVel);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);

	_tcscpy(szKey, _T("JOG MODE"));
	_stprintf(szTemp, _T("%6.1f"), param[nAxisNum].jog.GeneralJogMode);
	WritePrivateProfileString(szApp, szKey, szTemp, szFile);
}

LPMOTIONPARAM CMotionControl::GetMotorParam(int nAxisNum)
{
	return &param[nAxisNum];
}

// 초기화
BOOL CMotionControl::Open()
{
	LoadParameter();
	LoadJogParameter();

	// Initialize AXT board 보드 초기화
 	if(!AxlIsOpened())
 	{
		char sz[100] = { 0, };

		AxlGetLibVersion(sz);

		CString str;
		str.Format(_T("Motion Version : [%s]"), sz);
		TRACE(str);
 		if ((AxlOpenNoReset(7) != AXT_RT_SUCCESS))
 		{
 			return FALSE;
 		}
 	}

	AmpOn();

	DoEvents(200);

	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
	{
		DWORD dwMode = 0;
		DWORD dwPLevel = 0;
		DWORD dwNLevel = 0;

		DoEvents(200);

		AxmMotSetEncInputMethod(_a, m_AxisEncInputMethod[_a]);	// Encoder 입력 method 설정 (13 -> -4체배)
		AxmMotSetPulseOutMethod(_a, m_AxisOutputMethod[_a]);

		// 제어 단위를 설정하는 함수
		AxmMotSetMoveUnitPerPulse(_a, m_AxisUnit[_a], (long)m_AxisPulse[_a]);

		AxmMotSetMaxVel(_a, param[_a].MaxVel);
		
		AxmSignalGetLimit(_a, &dwMode, &dwPLevel, &dwNLevel);
		AxmSignalSetLimit(_a, dwMode, param[_a].MotorNegaLimitLevel, param[_a].MotorPosiLimitLevel);

		AxmHomeSetSignalLevel(_a, param[_a].MotorHomeSensorLevel);

		// 서보팩의 위치결정완료 신호 사용여부 및 Active레벨을 설정하는 함수
		AxmSignalSetInpos(_a, param[_a].MotorInpositionLevel);
		
		AxmSignalSetServoAlarm(_a, param[_a].ServoAlarmLevel);

		AxmSignalSetStop(_a, 0, param[_a].StopLevel);

		AxmMotSetProfileMode(_a, 0);	// 서보팩의 위치결정완료 신호 사용여부 및 Active레벨을 설정하는 함수

		AxmSignalSetZphaseLevel(_a, 1);	// 서보팩의 위치결정완료 신호 사용여부 및 Active레벨을 설정하는 함수
	}

	m_bMotionBoardOpen = TRUE;
	return TRUE;
}


// 종료
void CMotionControl::Close()
{
	m_bStop = TRUE;
	m_bOriginAllStop = TRUE;

 	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
		AxmSignalServoOn(_a, ENABLE);

	AxlClose();

	m_bMotionBoardOpen = FALSE;
}

//  모터 이동
BOOL CMotionControl::MoveAxis(int nAxisNum, double pos, double acc, double dec, double vel, BOOL bWait, BOOL bOffsetUse)
{
	BOOL bRet = FALSE;

	if(!IsOpen()) return FALSE;

// 	double dbArea = GetCurrentPos(AXIS_R);
// 
// 	BOOL bWarningArea = FALSE;
// 
// 	if(CheckLimitPos(AXIS_R, dbArea) == MOVE_ERROR)
// 		bWarningArea = TRUE;
// 	
// 	// R축 제한 지역에 있을경우
// 	if(bWarningArea)
// 	{
// 		if(nAxisNum == AXIS_X || nAxisNum == AXIS_Z)
// 		{
// 			if(CheckLimitPos(nAxisNum, pos) == MOVE_ERROR)
// 			{
// 				if(m_pAxisInitParam)
// 					pos = m_pAxisInitParam[nAxisNum].dbPos;
// 				else
// 					pos = INIT_POS;
// 			}
// 		}
// 	}

// 	if(nAxisNum == AXIS_R)
// 		pos -= param[nAxisNum].OriginOffset;
	
	double dwRetCode = AxmMoveStartPos(nAxisNum, pos, vel, acc, dec);
	CString strData;

	if(dwRetCode != AXT_RT_SUCCESS)
	{
		strData.Format(_T("AxmMoveStartPos return error[Code:%04d]"), dwRetCode);
		AfxMessageBox(strData);
		return FALSE;
	}else
	{
		bRet = TRUE;
	}	

	if(bWait)
	{
		DoEvents(500);

		// 동작 완료 여부를 확인하자
		bRet = IsPulseEnd(nAxisNum);

		// 현재 위치 인지 확인
		double dbPos = GetCurrentPos(nAxisNum);
		
// 		if(nAxisNum == AXIS_R)
// 		{
// 			if(bOffsetUse == FALSE)
// 				dbPos -= param[AXIS_R].OriginOffset;
// 		}
// 		
// 		if(dbPos != pos)
// 			bRet = FALSE;
	}
	
	return bRet;
}

// 멀티축 움직이기
BOOL CMotionControl::MultiMoveAxis(LPAXISMOVEPARAM pAxisParam, int nAxisCount)
{
	CString str;
	double dbMutiPos[MAX_NUM_AXIS], dbMultiVel[MAX_NUM_AXIS], dbMultiAcc[MAX_NUM_AXIS];
	BOOL bRet = TRUE;
	for(int _a=0; _a<nAxisCount; _a++)
	{
		LPAXISMOVEPARAM p = &pAxisParam[_a];
		m_iMoveMultiAxis[_a] = p->nAxisNum;
		dbMutiPos[_a]  = p->dbPos;
		dbMultiVel[_a] = p->dbVel;
		dbMultiAcc[_a] = p->dbAcc;
	}

	for (int _a = 0; _a<nAxisCount; _a++)
		MoveAxis(m_iMoveMultiAxis[_a], dbMutiPos[_a], dbMultiAcc[_a], dbMultiAcc[_a], dbMultiVel[_a], FALSE);
	
	// 축별로 상태 확인하자.
	clock_t start_tm = clock();
	DWORD dwStatus;

	BOOL bStopCheck = TRUE;
	do 
	{
		bStopCheck = TRUE;
		// 동작 완료 여부를 확인하자
		for(int _a=0; _a<nAxisCount; _a++)
		{
			AxmStatusReadInMotion(m_iMoveMultiAxis[_a], &dwStatus);

			if(dwStatus)
				bStopCheck = FALSE;
		}

		if(bStopCheck)
			break;

		DoEvents(10);

	} while ((DWORD)(clock()-start_tm)<100000);

	// 현재 위치 인지 확인
	if(bStopCheck)
	{
		for(int _a=0; _a<nAxisCount; _a++)
		{
			double dbPos = GetCurrentPos(m_iMoveMultiAxis[_a]);

			if(!(dbPos == dbMutiPos[_a]))
				bRet = FALSE;
		}
	}else
	{
		bRet = FALSE;
	}
	
	return bRet;
}

// 조그 + 이동
int CMotionControl::JogMovePlus(int nAxisNum)
{
	if(nAxisNum < 0)
		return -1;
	
	int velocity = param[nAxisNum].jog.JogVel;
	velocity+=1;
	if(velocity <= 0) velocity=1;
	velocity = (velocity * param[nAxisNum].MaxVel)/10;

	int acc = param[nAxisNum].Acc;

	// 지정한 속도로 연속구동 함수
	DWORD ret = AxmMoveVel(nAxisNum, velocity * m_AxisDirection[nAxisNum], (double)acc, (double)acc);
	if(ret != AXT_RT_SUCCESS)
		return -1;
	
	return 1;
}

int CMotionControl::JogMovePlus(int nAxisNum, int nJogVel)
{
	if(nAxisNum < 0)
		return -1;

	int velocity = nJogVel;
	velocity+=1;
	if(velocity <= 0) velocity=1;
	velocity = (velocity * param[nAxisNum].MaxVel)/10;

	int acc = param[nAxisNum].Acc;

	// 지정한 속도로 연속구동 함수
	DWORD ret = AxmMoveVel(nAxisNum, velocity * m_AxisDirection[nAxisNum], (double)acc, (double)acc);
	if(ret != AXT_RT_SUCCESS)
		return -1;

	return 1;
}


// 조그 - 이동
int CMotionControl::JogMoveMinus(int nAxisNum)
{
	if(nAxisNum < 0)
		return -1;
	
	int velocity = param[nAxisNum].jog.JogVel;
	velocity+=1;
	if(velocity <= 0) velocity=1;
	velocity = (int)((velocity * param[nAxisNum].MaxVel)/10);

	int acc = param[nAxisNum].Acc;

	// 지정한 속도로 연속구동 함수
	DWORD ret = AxmMoveVel(nAxisNum, (-velocity) * m_AxisDirection[nAxisNum], (double)acc, (double)acc);
	if(ret != AXT_RT_SUCCESS)
		return -1;

	return 1;
}

int CMotionControl::JogMoveMinus(int nAxisNum, int nJogVel)
{
	if(nAxisNum < 0)
		return -1;

	int velocity = nJogVel;
	velocity+=1;
	if(velocity <= 0) velocity=1;
	velocity = (int)((velocity * param[nAxisNum].MaxVel)/10);

	int acc = param[nAxisNum].Acc;

	// 지정한 속도로 연속구동 함수
	DWORD ret = AxmMoveVel(nAxisNum, (-velocity) * m_AxisDirection[nAxisNum], (double)acc, (double)acc);
	if(ret != AXT_RT_SUCCESS)
		return -1;

	return 1;
}

// 조그 정지
int CMotionControl::JogMoveStop(int nAxisNum)
{
	if(nAxisNum < 0)
		return -1;
	
	DWORD dwStatus = AxmMoveSStop(nAxisNum);
	if(dwStatus != AXT_RT_SUCCESS)
		return -1;
	
	return 1;
}

// 알람초기화
int CMotionControl::AlarmClear(int nAxisNum)
{
	DWORD upStatus=0; 
	AxmSignalReadServoAlarm (nAxisNum, &upStatus);
	if(upStatus>0){
		AxmSignalServoAlarmReset(nAxisNum, ENABLE);
		Sleep(500);
		AxmSignalServoOn(nAxisNum, ENABLE);
		Sleep(500);
		AxmSignalServoOn(nAxisNum, DISABLE);
	}
	return 1;
}

// 펄스 동작중인지 상태를 확인하자(움직임여부 확인가능)
BOOL CMotionControl::IsPulseEnd(int nAxisNum)
{
	bool bRet = FALSE;

	DWORD dwStatus = FALSE;

	clock_t start_tm = clock();
	do 
	{
		AxmStatusReadInMotion(nAxisNum, &dwStatus);
		if(!dwStatus)
		{
			bRet = TRUE;
			break;
		}

		if(m_bStop)
			break;

		DoEvents();

	} while ((DWORD)(clock()-start_tm)<100000);

	return bRet;
}

BOOL CMotionControl::IsOpen()
{
	return m_bMotionBoardOpen;
}

BOOL CMotionControl::AmpCheck()
{
	DWORD dwStatus;

	m_bStop = TRUE;

	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
	{
		AxmSignalIsServoOn(_a, &dwStatus);
		DoEvents(100);
		if(!dwStatus)
		{
			CString str;
			str.Format(_T("%d servo on"), _a);
			AfxMessageBox(str);
		}else
		{
			CString str;
			str.Format(_T("%d servo off"), _a);
			AfxMessageBox(str);
		}
	}
	
	return TRUE;
}

void CMotionControl::AmpOn()
{
	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
		AmpOn(_a);
}

void CMotionControl::AmpOn(int nAxisNum)
{
	AxmSignalServoOn(nAxisNum, DISABLE);
	Sleep(100);
}

void CMotionControl::AmpOff(int nAxisNum)
{
	AxmSignalServoOn(nAxisNum, ENABLE);
	Sleep(100);
}

BOOL CMotionControl::OriginStandbyPosMove()		// 검사 종료 시에만 호출할것
{
	SetMotorModeStatus();

	if(m_bMotorOriginStandby)
		return FALSE;

	BOOL bRet = TRUE;
	m_bStop = FALSE;

	for (int _a = 0; _a < 3; _a++)
	{
		if (!GetOriginComplete(m_Idx[_a]))
		{
			AfxMessageBox(_T("원점을 잡은후 실행하시기 바랍니다."));
			break;
			return FALSE;
		}
	}
	
	if(GetIOInStatus(DIO_INPORT_CYL_IN))
	{
		if(!CylinderOut())
		{
			AfxMessageBox(_T("이물광원이 있습니다. 확인하시기 바랍니다."));
			return FALSE;
		}
	}
	
	int MoveStep[] = { AXIS_X, AXIS_Z, AXIS_R };
	double pos = 0;
	for (int _a = 0; _a < 3; _a++)
	{
		if (!GetOriginComplete(m_Idx[_a]))
		{
			AfxMessageBox(_T("원점을 잡은후 실행하시기 바랍니다."));
			break;
		}

// 		int nAxis = MoveStep[_a];
// 		bRet = MoveAxis(nAxis, INIT_POS, INIT_ACC, INIT_ACC, INIT_VEL);
	}

	AXISMOVEPARAM p[MAX_NUM_AXIS];

	for (int _a = 0; _a < MAX_NUM_AXIS; _a++)
	{
		p[_a].nAxisNum = _a;

		if (_a == AXIS_R)
			p[_a].dbPos = INIT_POS * -1;
		else
			p[_a].dbPos = INIT_POS;

		p[_a].dbAcc = INIT_ACC;
		p[_a].dbVel = INIT_VEL;
	}

// 	if (!MultiMoveAxis(p, MAX_NUM_AXIS))
// 	{
// 		bRet = FALSE;
// 	}

	MultiMoveAxis(p, AXIS_4); //!SH _180820: 셔터 따로 동작하도록
	//MultiMoveAxis(p, MAX_NUM_AXIS);
	MoveAxis(AXIS_S, p[AXIS_S].dbPos, p[AXIS_S].dbAcc, p[AXIS_S].dbAcc, p[AXIS_S].dbVel, FALSE);

	if(!GetIOInStatus(DIO_INPORT_CYL_OUT))
	{
		AfxMessageBox(_T("이물광원이 감지되지않았습니다. 센서를 확인바랍니다."));
		bRet = FALSE;
	}

	if(bRet)
	{
		m_iMode	= ACTION_ORIGIN;

		m_bMotorStandby = FALSE;
		m_bMotorTest = FALSE;
		m_bMotorOriginStandby = TRUE;
	}

	return bRet;
}

// 테스트용
BOOL CMotionControl::Standby1PosMove()
{
	SetMotorModeStatus();

	if(m_bMotorStandby)
		return FALSE;

	BOOL bRet = FALSE;
	m_bStop = FALSE;
	for(int _a=0; _a<3; _a++)
	{
		int nIdx = m_Idx[_a];
		bRet = MoveAxis(m_Idx[_a], m_dbStandPos[_a], param[nIdx].Acc, param[nIdx].Acc, param[nIdx].MaxVel);

		Sleep(100);

		if(!bRet)
			break;
	}
	
	if(bRet)
	{
		m_iMode	= ACTION_STANDBYPOS;

		m_bMotorStandby = TRUE;
		m_bMotorTest = FALSE;
	}
	
	return bRet;
}

// 이물 검사시 이동 위치
BOOL CMotionControl::TestPosMove()
{
	SetMotorModeStatus();

	if(m_bMotorTest)
		return TRUE;

	if(!CylinderOut())
		return FALSE;

	BOOL bRet = FALSE;
	m_bStop = FALSE;

	double dbMutiPos[MAX_NUM_AXIS], dbMultiVel[MAX_NUM_AXIS], dbMultiAcc[MAX_NUM_AXIS];

	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
	{
		if(!GetOriginComplete(m_Idx[_a]))
		{
			AfxMessageBox(_T("원점을 잡은후 실행하시기 바랍니다."));
			break;
		}

		dbMultiAcc[_a] = param[_a].Acc;
		dbMultiVel[_a] = param[_a].MaxVel;
		m_iMoveMultiAxis[_a] = _a;
	}

	dbMutiPos[AXIS_X] = m_dbPos[1];
	dbMutiPos[AXIS_Z] = m_dbPos[2];
	dbMutiPos[AXIS_R] = m_dbPos[0];

	// 이물 Z축 이동시 문짝에 부딪힐경우 방지
	double dbZPos = GetCurrentPos(AXIS_Z);

	if (dbZPos >= param[AXIS_Z].dbMaxPosLimit)		// Z축이 상단 Limit 범위 초과하였을 경우...
	{
		double dbMovePos = param[AXIS_Z].dbMaxPosLimit - 1000;
		BOOL bReturn = MoveAxis(AXIS_Z, dbMovePos, param[AXIS_Z].Acc, param[AXIS_Z].Acc, param[AXIS_Z].MaxVel);

		if (bReturn == FALSE)
		{
			AfxMessageBox(_T("Z축 이동 실패"));
			return FALSE;
		}
		else
		{

		}
	}

	AXISMOVEPARAM p[3];

	for(int _a=0; _a<3; _a++)
	{
		p[_a].nAxisNum = _a;
		p[_a].dbPos = dbMutiPos[_a];
		p[_a].dbAcc = dbMultiAcc[_a];
		p[_a].dbVel = dbMultiVel[_a];
	}

	if(!MultiMoveAxis(p, 3))
	{
		bRet = FALSE;
	}else
	{
		if(CylinderIn())
		{
			// 이물광원에 카메라 삽입
			int idx = m_Idx[3];
			if(MoveAxis(m_Idx[3], m_dbPos[3], param[idx].Acc, param[idx].Acc, param[idx].MaxVel))
				bRet = TRUE;
			else
				bRet = FALSE;
		}
	}

	if(bRet)
	{
		m_iMode	= ACTION_TESTPOS;
		m_bMotorTest = TRUE;
		m_bMotorStandby = FALSE;
	}
	
	return bRet;
}

// 테스트용
BOOL CMotionControl::Test1PosMove()
{
	SetMotorModeStatus();

	if(m_bMotorTest)
		return FALSE;

	BOOL bRet = FALSE;
	m_bStop = FALSE;
	for(int _a=0; _a<3; _a++)
	{
		int nIdx = m_Idx[_a];
		bRet = MoveAxis(m_Idx[_a], m_dbPos[_a], param[nIdx].Acc, param[nIdx].Acc, param[nIdx].MaxVel);

		Sleep(100);

		if(!bRet)
			break;
	}

	m_iMode	= ACTION_TESTPOS;
	m_bMotorTest = TRUE;
	m_bMotorStandby = FALSE;

	return TRUE;
}

BOOL CMotionControl::TestRun(int nStep)
{
	BOOL bRet = TRUE;

	int nIdx = m_Idx[nStep];

	switch(nStep)
	{
	case 0:			// R축 후진
		bRet = MoveAxis(m_Idx[nStep], m_dbPos[nStep], param[nIdx].Acc, param[nIdx].Acc, param[nIdx].MaxVel);
		break;

	case 1:			// X 축 이동, 실린더 IN
		SetMotorModeStatus();

		if(m_bMotorWarning)
		{
			AfxMessageBox(_T("R축이 진입되어 있습니다. R축을 이동하여 주시기 바랍니다."));
			return FALSE;
		}

		SetIOOutPort(DIO_OUTPORT_CYL_OUT, FALSE);
		SetIOOutPort(DIO_OUTPORT_CYL_IN, TRUE);

		bRet = MoveAxis(m_Idx[nStep], m_dbPos[nStep], param[nIdx].Acc, param[nIdx].Acc, param[nIdx].MaxVel);
		break;

	case 2:			// Z(Y) 축 올리기
		bRet = MoveAxis(m_Idx[nStep], m_dbPos[nStep], param[nIdx].Acc, param[nIdx].Acc, param[nIdx].MaxVel);
		break;

	case 3:			// R축 이물광원에 삽입
		if(GetIOInStatus(DIO_INPORT_CYL_IN))
		{
			bRet = MoveAxis(m_Idx[nStep], m_dbPos[nStep], param[nIdx].Acc, param[nIdx].Acc, param[nIdx].MaxVel);
		}else
		{
			if(!CylinderIn())
			{
				bRet = FALSE;
				AfxMessageBox(_T("이물광원이 삽입되지 않았습니다 확인바랍니다."));
				OriginStandbyPosMove();
			}else
			{
				bRet = MoveAxis(m_Idx[nStep], m_dbPos[nStep], param[nIdx].Acc, param[nIdx].Acc, param[nIdx].MaxVel);
			}
		}
		
		break;
	}

	return bRet;
}

BOOL CMotionControl::AllOrigin()
{
	BOOL bRet = TRUE;
	
	m_bStop = FALSE;
	m_bOriginAllStop = FALSE;
	m_OriginMode = -1;

	if(!IsOpen()) return FALSE;

	m_iMode = ACTION_ORIGIN;

	SetMotorModeStatus();

	// 모터 상태값 초기화
	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
	{
		m_OriginInfo[_a].nIdx = _a;
		m_OriginInfo[_a].bOriginComplete	= FALSE;
		m_OriginInfo[_a].bOrigining			= FALSE;
		m_OriginInfo[_a].bOriginStart		= FALSE;
	}

	CylinderOut();
	
	// 원점잡기 수행
	for(int _a=0; _a<2; _a++)
	{
		bRet = RunProcedure(_a);

		if(!bRet)
			break;
	}

// 	SetIOOutPort(DIO_OUTPORT_LIGHT, FALSE);
// 	SetIOOutPort(DIO_OUTPORT_IRLIGHT, FALSE);
	
	m_iMode					= ACTION_NONE;
	m_bMotorStandby			= FALSE;
	m_bMotorTest			= FALSE;
	m_bMotorOriginStandby	= TRUE;
	
	if(!bRet)
		AfxMessageBox(_T("AllOrigin 원점잡기에 실패하였습니다."));
	else
	{
		AfxMessageBox(_T("AllOrigin 원점잡기에 성공하였습니다."));
	}

	return bRet;
}

BOOL CMotionControl::RunProcedure(int nStep)
{
	BOOL bRet = TRUE;

	switch(nStep)
	{
	case 0:
		bRet = OriginXZ();
		break;

	case 1:
		bRet = OriginR();
		break;
	}
	
	return bRet;
}

BOOL CMotionControl::OriginXZ()
{
	BOOL bRet = TRUE;
	clock_t start_tm;

	AxisOrigin(AXIS_S);
	AxisOrigin(AXIS_X);
	AxisOrigin(AXIS_Z);

	//  대기
	start_tm = clock();
	do 
	{
		DoEvents();

		// 프로그램 종료시
		if(m_bStop || GetOriginStopFlag())
		{
			StopAllAxis();
			bRet = FALSE;
			break;
		}

		if((DWORD)(clock()-start_tm)>ORIGIN_WAIT_TIME)
		{
			StopAllAxis();
			bRet = FALSE;
			break;
		}
	} while (!GetOriginComplete(AXIS_X)||
		!GetOriginComplete(AXIS_Z) ||
		!GetOriginComplete(AXIS_S));
	
// 	if(m_pAxisInitParam)
// 	{
// 		if(bRet)
// 			MoveAxis(AXIS_X, m_pAxisInitParam[AXIS_X].dbPos, m_pAxisInitParam[AXIS_X].dbVel, m_pAxisInitParam[AXIS_X].dbAcc, m_pAxisInitParam[AXIS_X].dbAcc);
// 	}else
// 	{
// 		if(bRet)
// 			MoveAxis(AXIS_X, INIT_POS, INIT_ACC, INIT_ACC, INIT_VEL);
// 	}
	
	return bRet;
}

BOOL CMotionControl::OriginR()
{
	BOOL bRet = TRUE;
	clock_t start_tm;

	AxisOrigin(AXIS_R);

	//  대기
	start_tm = clock();
	do 
	{
		DoEvents();

		// 프로그램 종료시
		if(m_bStop || GetOriginStopFlag())
		{
			StopAllAxis();
			bRet = FALSE;
			break;
		}

		if((DWORD)(clock()-start_tm)>ORIGIN_WAIT_TIME)
		{
			StopAllAxis();
			bRet = FALSE;
			break;
		}
	} while (!GetOriginComplete(AXIS_R));

	return bRet;
}

BOOL CMotionControl::GetOriginComplete(int nAxisNum)
{
	return m_OriginInfo[nAxisNum].bOriginComplete;
}

//
BOOL CMotionControl::WaitMotorDone(int nAxisNum)
{
	BOOL bRet = TRUE;

	//  대기
	clock_t start_tm = clock();
	do 
	{
		if(m_bStop)
		{
			StopAllAxis();
			bRet = FALSE;
			break;
		}

		if((DWORD)(clock()-start_tm)>ORIGIN_WAIT_TIME)
		{
			StopAllAxis();
			bRet = FALSE;
			break;
		}

		DoEvents();

	} while (!GetOriginComplete(nAxisNum));

	return bRet;
}

int CMotionControl::AxisOrigin(int nAxisNum)
{	
	CWinThread* pThread;
	m_bRun = TRUE;

	THREADORIGINPARAMS* pNewParam = new THREADORIGINPARAMS;
	pNewParam->pVoid = (void*)this;
	pNewParam->bRun = &m_bRun;
	pNewParam->nAxisNo = nAxisNum;

	if(!(pThread = (CWinThread*)AfxBeginThread(ThreadFunc, pNewParam)))
	{
		TRACE(_T("Error - Create the Thread for Main"));
		return FALSE;
	}
	return 1;
}

UINT CMotionControl::ThreadFunc(LPVOID lpThreadParam)
{
	THREADORIGINPARAMS* pParam = (THREADORIGINPARAMS*)lpThreadParam;
	CMotionControl* pCtrl = (CMotionControl*)pParam->pVoid;

	pCtrl->m_bRun = *(BOOL*)pParam->bRun;
	int nAxisNo = pParam->nAxisNo;
	delete pParam;

	if(nAxisNo<0 && nAxisNo > MAX_NUM_AXIS)
		return 0;

	if(pCtrl->GetOriginStopFlag()) return 0;

	pCtrl->MotorOrigin(nAxisNo);

	return 1;
}

// 축별로 모터 원점 잡기
void CMotionControl::MotorOrigin(int nAxisNum)
{
	BOOL bRet = TRUE;

	int nFastSpeed, nSlowSpeed, nLowSpeed, nAcc;

	double OriginOffset = param[nAxisNum].OriginOffset;
	double dHomeClearTime = 1000.0;

	m_OriginInfo[nAxisNum].bOriginStart = TRUE;
	m_OriginInfo[nAxisNum].bOriginComplete = FALSE;
	m_OriginInfo[nAxisNum].bOrigining = TRUE;

	AxmSignalServoOn(nAxisNum, ENABLE);
	Sleep(200);
	AxmSignalServoOn(nAxisNum, DISABLE);

	double NegLimit = param[nAxisNum].NegLimit;
	double PosLimit = param[nAxisNum].PosLimit;

	nFastSpeed	= param[nAxisNum].OriginCoarseVel;
	nSlowSpeed	= param[nAxisNum].OriginFineVel;
	nLowSpeed	= param[nAxisNum].OriginFineVel/2;
	nAcc		= param[nAxisNum].Acc;

	AxmHomeSetVel(nAxisNum, nFastSpeed, nSlowSpeed, nLowSpeed, nLowSpeed, nAcc, nAcc);
	AxmHomeSetMethod(nAxisNum, param[nAxisNum].nHomeDir, m_AxisHomeSig[nAxisNum], m_AxismZphas[nAxisNum], dHomeClearTime, param[nAxisNum].OriginOffset);
	AxmHomeSetStart(nAxisNum);

	clock_t start_tm = clock();
	do 
	{
		DWORD dwHomeMainStepNumber, dwHomeStepNumber;
		AxmHomeGetRate(nAxisNum, &dwHomeMainStepNumber, &dwHomeStepNumber);

		if(dwHomeStepNumber == 100)
		{
			DWORD dwHomeResult;
			AxmHomeGetResult(nAxisNum, &dwHomeResult);

			if(dwHomeResult == TRUE)
			{
				m_OriginInfo[nAxisNum].bOriginStart		= FALSE;
				m_OriginInfo[nAxisNum].bOriginComplete	= TRUE;
			}else
			{
				m_OriginInfo[nAxisNum].bOriginComplete	= FALSE;
				m_OriginInfo[nAxisNum].bOriginStart		= FALSE;

//				AfxMessageBox(_T("원점검색에 실패했습니다."), MB_OK);
				break;
			}
		}

		DoEvents();

	} while (!GetOriginComplete(nAxisNum));
}

// + Limit 상태
int CMotionControl::GetPosSensorStatus(int nAxisNum)
{
	DWORD dwStatus = FALSE;
	int nSensorPos;

	AxmSignalReadLimit(nAxisNum, &dwStatus, NULL);

	nSensorPos = (int)dwStatus;

	return nSensorPos;
}

// - Limit 상태
int	 CMotionControl::GetNegSensorStatus(int nAxisNum)
{
	DWORD dwStatus = FALSE;
	AxmSignalReadLimit(nAxisNum, NULL, &dwStatus);
	return (int)dwStatus;
}

// Home 센서 상태
int	 CMotionControl::GetHomeSensorStatus(int nAxisNum)
{
	DWORD dwStatus = FALSE;
	AxmHomeReadSignal(nAxisNum, &dwStatus);
	return (int)dwStatus;
}

// Amp 상태
int	 CMotionControl::GetAmpStatus(int nAxisNum)
{
	DWORD dwStatus;
	AxmSignalIsServoOn(nAxisNum, &dwStatus);
	if(dwStatus) return TRUE;
	return FALSE;
}

double CMotionControl::GetCurrentPos(int nAxisNum)
{
	double dCnt = 0.;

	// 현재 지령펄스와, Encoder펄스를 확인하는 함수
	AxmStatusGetCmdPos(nAxisNum, &dCnt);

	dCnt = dCnt * param[nAxisNum].GearRatio;
	dCnt = dCnt * m_AxisDirection[nAxisNum];
	dCnt = dCnt * 1.0;

// 	if(nAxisNum == AXIS_R)
// 		dCnt += param[AXIS_R].OriginOffset;
	
	return dCnt;
}

BOOL CMotionControl::GetOriginStopFlag(){return m_bOriginAllStop;}

int		CMotionControl::GetMotorModeStatus(){return m_iMode;}
BOOL	CMotionControl::GetStandbyPos(){return m_bMotorStandby;}
BOOL	CMotionControl::GetTestPos(){return m_bMotorTest;}

void CMotionControl::SetMotorModeStatus()
{
	double dbPos = 0;

	m_iMode = ACTION_NONE;

	// 1. R축 모터가 진입되어 있는 경우
	dbPos = GetCurrentPos(AXIS_R);

	if(dbPos < m_dbMotorWarningPos)
		m_bMotorWarning = TRUE;
	else
		m_bMotorWarning = FALSE;

	
	// 2. TEST MODE 인경우(이물광원에 카메라 렌즈가 삽입된경우)
	BOOL   bOn	= GetIOInStatus(DIO_INPORT_CYL_IN);
	double dbR = GetCurrentPos(AXIS_R);

	if(bOn)
	{
		if(/*dbX == m_dbPos[1] && dbZ == m_dbPos[2] &&*/ dbR == m_dbPos[3])
		{
			m_bMotorTest = TRUE;
			m_iMode = ACTION_TESTPOS;
			m_bMotorOriginStandby = FALSE;
		}
		else
		{
			m_bMotorTest = FALSE;
		}
	}
	else
	{
		m_bMotorTest = FALSE;
	}

	// 3. 테스트 종료후 원점 잡을 경우
	if(/*dbX == 0 && dbZ == 0 &&*/ dbR == INIT_POS)
		m_bMotorOriginStandby = TRUE;
	else
		m_bMotorOriginStandby = FALSE;
}

int CMotionControl::AutoMove(int nAxisNum, double dPos, double dVel, long nAcc)
{
	return MoveAxis(nAxisNum,dPos, nAcc, nAcc, dVel);
}

BOOL  CMotionControl::StopAllAxis()
{
	BOOL bRet = TRUE;

	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
		bRet = StopAxis(_a);
	
	return bRet;
}

BOOL CMotionControl::StopAxis(int nAxisNum)
{
	BOOL bRet = TRUE;

	DWORD dwStatus = AxmMoveSStop(nAxisNum);

	m_bStop = TRUE;

	if(dwStatus != AXT_RT_SUCCESS)
	{
		// 에러 메시지창 띄우기
		CString str;
		str.Format(_T("%d Stop 에러"), nAxisNum);
		AfxMessageBox(str);
		bRet = FALSE;
	}else
	{
		m_OriginInfo[nAxisNum].bOriginComplete	= FALSE;
		m_OriginInfo[nAxisNum].bOrigining		= FALSE;
		m_OriginInfo[nAxisNum].bOriginStart		= FALSE;
	}

	return bRet;
}

void CMotionControl::DoEvents()
{
	MSG msg;

	if(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		if(!AfxGetApp()->PreTranslateMessage(&msg)) 
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
	}

	::Sleep(0);
}

void CMotionControl::DoEvents(DWORD dwMiliSeconds)
{
	clock_t start_tm = clock();
	do 
	{
		DoEvents();
		::Sleep(1);
	} while ((DWORD)(clock() - start_tm)<dwMiliSeconds);
}

void CMotionControl::SetStop(BOOL bStop)
{
	m_bStop = bStop;
}

CString CMotionControl::GetCurrentFilePath()
{
	TCHAR szFileName[MAX_PATH];

	GetModuleFileName(NULL, szFileName, MAX_PATH);

	CString strFilePath;
	strFilePath.Format(_T("%s"), szFileName);

	strFilePath = strFilePath.Left(strFilePath.ReverseFind('\\'));

	return strFilePath;
}


// IO 
BOOL CMotionControl::StartIOMonitoring()
{
	if(!IsOpen()) return FALSE;

	if(m_pIoControl)
		m_pIoControl->StartIOMonitoring();

	return TRUE;
}

BOOL CMotionControl::SetIOOutPort(int nPort, BOOL bState)
{
	if(!IsOpen()) return FALSE;

	BOOL bRet = FALSE;
	if(m_pIoControl)
	{
		bRet = TRUE;
		m_pIoControl->SetIOOutPort(nPort, bState);
	}

	return bRet;
}

BOOL CMotionControl::GetIOInStatus(int nPort)
{
	if(!IsOpen()) return FALSE;

	BOOL bRet = FALSE;
	if(m_pIoControl)
		bRet = m_pIoControl->GetIOInStatus(nPort);
	
	return bRet;
}

BOOL CMotionControl::GetIOOutStatus(int nPort)
{
	if(!IsOpen()) return FALSE;

	BOOL bRet = FALSE;
	if(m_pIoControl)
		bRet = m_pIoControl->GetIOOutStatus(nPort);

	return bRet;
}

// 실린더 인 제어
BOOL CMotionControl::CylinderIn()
{
	BOOL bOn = GetIOInStatus(DIO_INPORT_CYL_IN);
	BOOL bRet = TRUE;

	if(!bOn)
	{
		bRet = FALSE;
		SetMotorModeStatus();

		if(m_bMotorWarning)
		{
			AfxMessageBox(_T("R축이 진입되어 있습니다. R축을 이동하여 주시기 바랍니다."));
			return FALSE;
		}

		SetIOOutPort(DIO_OUTPORT_CYL_OUT, FALSE);
		SetIOOutPort(DIO_OUTPORT_CYL_IN, TRUE);

		// 이물광원 이동시간이 있으니 센서에 걸릴동안 대기 하여 준다.
		clock_t start_tm = clock();
		do 
		{
			bOn = GetIOInStatus(DIO_INPORT_CYL_IN);

			if(bOn)
			{
				SetIOOutPort(DIO_OUTPORT_CYL_IN, FALSE);
				bRet = TRUE;
				break;
			}

			DoEvents(1);

		} while ((DWORD)(clock()-start_tm)<20000);		// 5초동안대기
	}

	return bRet;
}

// 실린더 아웃 제어
BOOL CMotionControl::CylinderOut()
{
	BOOL bOn = GetIOInStatus(DIO_INPORT_CYL_OUT);
	BOOL bRet = TRUE;

	if(!bOn)
	{
		bRet = FALSE;
		SetMotorModeStatus();

// 		if(m_bMotorTest)		// 테스트 모드는 이물광원에 카메라가 삽입된 상태
// 		{
// 			SetIOOutPort(DIO_OUTPORT_CYL_IN, FALSE);
// 			SetIOOutPort(DIO_OUTPORT_CYL_OUT, TRUE);
// 			AfxMessageBox(_T("현재 이물광원에 카메라 렌즈가 삽입되어있습니다. 확인바랍니다."));
// 			return FALSE;
// 		}

		SetIOOutPort(DIO_OUTPORT_CYL_IN, FALSE);
		SetIOOutPort(DIO_OUTPORT_CYL_OUT, TRUE);

		// 이물광원 이동시간이 있으니 센서에 걸릴동안 대기 하여 준다.
		clock_t start_tm = clock();
		do 
		{
			bOn = GetIOInStatus(DIO_INPORT_CYL_OUT);

			if(bOn)
			{
				SetIOOutPort(DIO_OUTPORT_CYL_OUT, FALSE);
				bRet = TRUE;
				break;
			}

			DoEvents(1);

		} while ((DWORD)(clock()-start_tm)<20000);		// 5초동안대기
	}

	return bRet;
}

BOOL CMotionControl::CheckLimitPos(int nAxisNum, double dbPos)
{
	BOOL bRet =  MOVE_SUCCESS;

	int nLimitType = param[nAxisNum].nLimitType;

	double dbMin = (double)param[nAxisNum].dbMinPosLimit;
	double dbMax = (double)param[nAxisNum].dbMaxPosLimit;

	switch(nLimitType)
	{
	case LIMIT_NONE:
		break;

	case LIMIT_MINMAX:
		if(dbPos < dbMin || dbPos > dbMax)
			bRet = MOVE_ERROR;
		break;

	case LIMIT_AREA:
		if(dbPos > dbMin && dbPos < dbMax)
			bRet = MOVE_ERROR;
		break;
	}

	return bRet;
}