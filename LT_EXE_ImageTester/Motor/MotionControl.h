#pragma once

#include "IoControl.h"

#define TOTAL_NUM_AXIS	5


// 저장파일위치
#define		FILE_AXIS		_T("D:\\INI\\Axis.ini")
#define		FILE_JOG		_T("D:\\INI\\Jog.ini")


// 모터 축 이름
typedef enum __AXIS_NUM {
	AXIS_1 = 0, AXIS_2, AXIS_3, AXIS_4,	MAX_NUM_AXIS
} AXIS_NUM;

#define AXIS_X		AXIS_1
#define AXIS_Z		AXIS_2
#define AXIS_R		AXIS_3
#define AXIS_S		AXIS_4

// Default InitParam
#define		INIT_POS		1000
#define		INIT_VEL		100000
#define		INIT_ACC		100000

#define		LIMIT_CYL_WARNING	-35000		// INI 파일로 빼자


// ERROR
#define		MOVE_SUCCESS		1
#define		MOVE_ERROR			0


// LIMIT TYPE
#define		LIMIT_NONE			0
#define		LIMIT_MINMAX		1
#define		LIMIT_AREA			2


// 모터 상태
#define		ACTION_NONE			0
#define		ACTION_STANDBYPOS	1
#define		ACTION_TESTPOS		2
#define		ACTION_ORIGIN		3
#define		ACTION_MOVING		4


// 원점 스텝
#define		ORIGIN_FIRST_STEP	1
#define		ORIGIN_SECOND_STEP	2
#define		ORIGIN_THIRD_STEP	3

#define		MOVE_STANDBYPOS		4
#define		MOVE_TESTPOS		5

#define		ORIGIN_WAIT_TIME	600000


// color
#define	GREEN				RGB(0,255,0)
#define GRAY				RGB(128,128,128)
#define	RED					RGB(255,0,0)


#define COLOR_ON			GREEN
#define COLOR_OFF			GRAY
#define COLOR_ERR			RED

// INPORT 제어
#define		DIO_INPORT_EMS			0
#define		DIO_INPORT_START		1
#define		DIO_INPORT_STOP			2
#define		DIO_INPORT_DOOR			3
#define		DIO_INPORT_AREA			4
#define		DIO_INPORT_CYL_IN		7
#define		DIO_INPORT_CYL_OUT		8
#define		DIO_INPORT_SOCKETDOOR	9
#define		DIO_INPORT_SOCKETAREA	10

// OUTPORT 제어
#define		DIO_OUTPORT_LAMP_START		1
#define		DIO_OUTPORT_LAMP_STOP		2
#define		DIO_OUTPORT_CYL_IN			7
#define		DIO_OUTPORT_CYL_OUT			8
#define		DIO_OUTPORT_BRDPOWER		9
#define		DIO_OUTPORT_LAMP_RED		11
#define		DIO_OUTPORT_LAMP_YELLOW		12
#define		DIO_OUTPORT_LAMP_GREEN		13
#define		DIO_OUTPORT_LAMP_MELODY1	14
#define		DIO_OUTPORT_LAMP_MELODY2	15

// 모터축 움직임 정보
typedef struct _tagAXISMOVEPARAM
{
	int			nAxisNum;
	double		dbPos;
	double		dbVel;
	double		dbAcc;
	double		dbDec;
}AXISMOVEPARAM, *LPAXISMOVEPARAM;

// 스레드 모터 축 움직임 정보
typedef struct _tagTHREADAXISMOVEPARAM
{
	LPVOID				pVoid;
	LPAXISMOVEPARAM		pAxisParam;
	BOOL				bError;
	BOOL				bRun;
}THREADAXISMOVEPARAM, *LPTHREADAXISMOVEPARAM;

// ORIGIN 모터 정보 
typedef struct _tagMOTORORIGININFO
{
	int		nIdx;				// 모터축
	BOOL	bOriginStart;		// 원점잡기 시작
	BOOL	bOrigining;			// 원점잡기 수행중
	BOOL	bOriginComplete;	// 원점잡기 완료
}MORTORORGININFO, *LPMOTORORIGININFO;

typedef struct _tagTHREADORIGINPARAMS
{
	void	*pVoid;
	BOOL	*bRun;
	int		nAxisNo;
}THREADORIGINPARAMS;

typedef struct _tagJOGPARAM
{
	double JogStep;
	double GeneralJogStep;
	int JogVel;
	int GeneralJogVel;
	int JogMode;
	int GeneralJogMode;
}JOGPARAM, *LPJOGPARAM;

typedef struct _STMOTIONPARAM{
	double MaxVel;
	double MinVel;
	double Acc;
	double Dec;
	double CoarseVel;
	double FineVel;
	double OriginCoarseVel;			// 원점
	double OriginFineVel;			// 원점
	double NegLimit;
	double PosLimit;
	double BufNegLimit;
	double BufPosLimit;
	double Numerator;
	double Denominator;
	double dbnit;
	double Pulse;
	double GearRatio;	
	double OriginOffset;			// 원점
	double GantryOffset;
	double OffsetRange;
	double OutPutMethod;
	double EncoderMethod;
	double CoordDirection;
	double MotorType;
	double AxisStatus;
	double EncoderType;
	double StepMotorSmoothing;
	double VelPulse;
	double MotorInpositionLevel;
	double MotorAmpEnableLevel;
	double MotorAmpResetLevel;
	double MotorAmpFaultLevel;
	double MotorHomeSensorLevel;
	double MotorNegaLimitLevel;
	double MotorPosiLimitLevel;
	double GantrySelect;
	double StartSpeed;
	double ServoAlarmLevel;
	double StopLevel;
	int	   nHomeDir;
	int    nLimitType;
	double dbMinPosLimit;
	double dbMaxPosLimit;
	int	   nOffsetmm;
	int	   nMMperPulse;
	JOGPARAM	jog;
}MOTIONPARAM, *LPMOTIONPARAM;

class CMotionControl
{
public:
	CMotionControl(void);
	~CMotionControl(void);

	MOTIONPARAM	param[5];

	HANDLE		m_Handle[MAX_NUM_AXIS];

	long		m_iMoveMultiAxis[MAX_NUM_AXIS];

	// 정보 로드
	void LoadParameter();
	void LoadParameter(LPCTSTR szFile);
	void LoadJogParameter();
	void LoadJogParameter(LPCTSTR szFile);

	void LoadData(LPCTSTR szFile);
	void LoadJogData(LPCTSTR szFile);

	// 정보 저장
	void SaveParameter();
	void SaveParameter(int nAxisNum);
	void SaveData(int nAxisNum);
	void SaveJogData(int nAxisNum);

	LPMOTIONPARAM GetMotorParam(int nAxisNum);

	// 초기화
	BOOL Open();
	BOOL IsOpen();
	BOOL GetVersionLib();

	// 종료
	void Close();
		
	//  모터 이동
	BOOL MultiMoveAxis(LPAXISMOVEPARAM pAxisParam, int nAxisCount);
	BOOL MoveAxis(int nAxisNum, double pos, double acc, double dec, double vel, BOOL bWait = TRUE, BOOL bOffsetUse = TRUE);
	int  JogMovePlus(int nAxisNum);
	int  JogMovePlus(int nAxisNum, int nJogVel);
	int  JogMoveMinus(int nAxisNum);
	int	 JogMoveMinus(int nAxisNum, int nJogVel);
	int  JogMoveStop(int nAxisNum);
	int  AlarmClear(int nAxisNum);


	// 지정된 위치 도착여부 확인
	BOOL IsMoveEnd(int nAxisNum, double pos);
	BOOL IsPulseEnd(int nAxisNum);

	void DoEvents();
	void DoEvents(DWORD dwMiliSeconds);

	// 체크
	BOOL AmpCheck();

	void AmpOn();
	void AmpOn(int nAxisNum);
	void AmpOff(int nAxisNum);
	
	// 대기위치로 이동
	BOOL OriginStandbyPosMove();
	BOOL Standby1PosMove();

	// 검사위치로 이동
	BOOL TestPosMove();
	BOOL Test1PosMove();

	BOOL TestRun(int nStep);
	
	// 모든 축 원점잡기
	BOOL RunProcedure(int nStep);
	BOOL AllOrigin();
	BOOL OriginXZ();
	BOOL OriginR();
	
	// 한 축 원점잡기
	int  AxisOrigin(int nAxisNum);
	
	BOOL GetOriginStopFlag();
	int	 AutoMove(int nAxisNum, double dPos, double dVel, long nAcc);

	BOOL WaitMotorDone(int nAxisNum);
	
	//   센서 상태값 읽어오기 
	int  GetPosSensorStatus(int nAxisNum);
	int	 GetNegSensorStatus(int nAxisNum);
	int	 GetHomeSensorStatus(int nAxisNum);
	int	 GetAmpStatus(int nAxisNum);
	
	//  원점이동
	void MotorOrigin(int nAxisNum);

	// 모든 모터축 정지
	BOOL StopAxis(int nAxisNum);
	BOOL StopAllAxis();
	
	BOOL	GetOriginComplete(int nAxisNum);
	double	GetCurrentPos	(int nAxisNum);
	
	BOOL	GetStandbyPos();
	BOOL	GetTestPos();

	void SetMotorModeStatus();
	int	 GetMotorModeStatus();

	void SetStop(BOOL bStop);

	// 실린더 제어
	BOOL	CylinderIn();
	BOOL	CylinderOut();

	// 이동제한 	
	void	SetCylinderLimit(double dbPos){m_dbMotorWarningPos = dbPos;}
	BOOL	CheckLimitPos(int nAxisNum, double dbPos);

	// 현재폴더 PATH
	CString GetCurrentFilePath();

	void	SetInitMoveParam(LPAXISMOVEPARAM p){m_pAxisInitParam = p;}
	void	SetInitMoveCount(int nCount){m_nAxisInitParam = nCount; }


#pragma region		IO 체크 함수
	BOOL	StartIOMonitoring();
	BOOL	SetIOOutPort(int nPort, BOOL bState);
	BOOL	GetIOInStatus(int nPort);
	BOOL	GetIOOutStatus(int nPort);
#pragma endregion

	int m_nTestStep;
	int m_Idx[TOTAL_NUM_AXIS];

	double m_dbPos[TOTAL_NUM_AXIS];
	double m_dbStandPos[TOTAL_NUM_AXIS];

private:
	int m_iMode;			// 모션 상태

	int	m_OriginMode;		// 원점모드

	BOOL m_bRun;
	BOOL m_bStop;
	BOOL m_bMotorTest;		
	BOOL m_bMotorStandby;
	BOOL m_bMotorOriginStandby;
	BOOL m_bMotionBoardOpen;
	BOOL m_bMotorWarning;

	CIoControl*		m_pIoControl;

	double	m_dbMotorWarningPos;

	int					m_nAxisInitParam;
	LPAXISMOVEPARAM		m_pAxisInitParam;

	BOOL m_bOriginAllStop;		// 원점잡기 전체 중단
	MORTORORGININFO	m_OriginInfo[TOTAL_NUM_AXIS];	// 워점 잡기 축별 상태 관리

private:
	static UINT	ThreadFunc(LPVOID lpThreadParam);			// 모터 축 원점잡기 수행
};
