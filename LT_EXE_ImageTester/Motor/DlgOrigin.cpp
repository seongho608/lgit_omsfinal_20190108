// DlgOrigin.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "DlgOrigin.h"

// CDlgOrigin 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlgOrigin, CDialog)

CDlgOrigin::CDlgOrigin(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgOrigin::IDD, pParent)
{
	m_pMotion = NULL;
	m_nCurAxis = -1;
}

CDlgOrigin::~CDlgOrigin()
{
	m_pMotion = NULL;
}

void CDlgOrigin::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
		DDX_Control(pDX, IDC_PNL_CUR_POS_0+_a, m_ctrlCurPos[_a]);

	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
		DDX_Control(pDX, IDC_ORG_LED_ORIGIN_0+_a, m_ctrlOrigin[_a]);

	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
		DDX_Control(pDX, IDC_ORG_LED_POWER_0+_a, m_ctrlPower[_a]);

	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
		DDX_Control(pDX, IDC_PNL_PLUS_LIMIT_0+_a, m_ctrlPLimit[_a]);

	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
		DDX_Control(pDX, IDC_PNL_MINUS_LIMIT_0+_a, m_ctrlMLimit[_a]);

	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
		DDX_Control(pDX, IDC_PNL_HOME_0+_a, m_ctrlHLimit[_a]);
}

BEGIN_MESSAGE_MAP(CDlgOrigin, CDialog)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDOK, &CDlgOrigin::OnBnClickedOk)
	ON_WM_DESTROY()
	ON_WM_SHOWWINDOW()
	ON_WM_ACTIVATE()
END_MESSAGE_MAP()


// CDlgOrigin 메시지 처리기입니다.

BOOL CDlgOrigin::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	return TRUE;  // return TRUE unless you set the focus to a control
}


void CDlgOrigin::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch(nIDEvent)
	{
	case 100:
		if(m_pMotion)
			UpdateOriginStatus();
		break;

	case 110:
		if(m_pMotion)
			UpdateCurPos();
	
	case 120:
		if(m_pMotion)		
			UpdateSensorStatus();
		break;
	}

	CDialog::OnTimer(nIDEvent);
}

void CDlgOrigin::SetMotion(CMotionControl* pMotion)
{
	m_pMotion = pMotion;
}

CMotionControl* CDlgOrigin::GetMotion()
{
	return m_pMotion;
}

void CDlgOrigin::UpdateCurPos()
{
	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
	{
		if(m_pMotion->GetOriginComplete(_a))
			m_ctrlOrigin[_a].SetBackColorInterior(COLOR_ON);
		else
			m_ctrlOrigin[_a].SetBackColorInterior(COLOR_OFF);
	}
}

void CDlgOrigin::UpdateOriginStatus()
{
	CString str;

	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
	{
		str.Format(_T("%.0f"), m_pMotion->GetCurrentPos(_a));
		m_ctrlCurPos[_a].SetWindowText((LPCTSTR(str)));
	}
}

void CDlgOrigin::UpdateSensorStatus()
{
	if(m_pMotion == NULL) return;

	for(int _a=0; _a<MAX_NUM_AXIS; _a++)
	{
		// 파워
		if(m_pMotion->GetAmpStatus(_a))
			m_ctrlPower[_a].SetBackColorInterior(COLOR_ON);
		else
			m_ctrlPower[_a].SetBackColorInterior(COLOR_OFF);

		// + Limit 센서
		if(m_pMotion->GetPosSensorStatus(_a))
			m_ctrlPLimit[_a].SetBackColorInterior(COLOR_ERR);
		else
			m_ctrlPLimit[_a].SetBackColorInterior(COLOR_OFF);

		// - Limit 센서
		if(m_pMotion->GetNegSensorStatus(_a))
			m_ctrlMLimit[_a].SetBackColorInterior(COLOR_ERR);
		else
			m_ctrlMLimit[_a].SetBackColorInterior(COLOR_OFF);

		// Home 센서
		if(m_pMotion->GetHomeSensorStatus(_a))
			m_ctrlHLimit[_a].SetBackColorInterior(COLOR_ERR);
		else
			m_ctrlHLimit[_a].SetBackColorInterior(COLOR_OFF);
	}
}

void CDlgOrigin::OnBnClickedOk()
{
	OnOK();
}

void CDlgOrigin::OnDestroy()
{
	CDialog::OnDestroy();

	StopTimer();

	m_bShow = FALSE;
}

void CDlgOrigin::StartTimer()
{
	SetTimer(100, 100, NULL);
	SetTimer(110, 100, NULL);
	SetTimer(120, 100, NULL);
}

void CDlgOrigin::StopTimer()
{
	KillTimer(100);
	KillTimer(110);
	KillTimer(120);
}

BEGIN_EVENTSINK_MAP(CDlgOrigin, CDialog)
	ON_EVENT(CDlgOrigin, IDB_AXIS_MOVE_PLUS_0, DISPID_CLICK, CDlgOrigin::ClickAxisMovePlus0, VTS_NONE)
	ON_EVENT(CDlgOrigin, IDB_AXIS_MOVE_MINUS_0, DISPID_CLICK, CDlgOrigin::ClickAxisMoveMinus0, VTS_NONE)
	ON_EVENT(CDlgOrigin, IDB_AXIS_ALARM_CLEAR, DISPID_CLICK, CDlgOrigin::ClickAxisAlarmClear, VTS_NONE)
	ON_EVENT(CDlgOrigin, IDB_AXIS_ORIGIN_START, DISPID_CLICK, CDlgOrigin::ClickAxisOriginStart, VTS_NONE)
	ON_EVENT(CDlgOrigin, IDB_AXIS_ORIGIN_STOP, DISPID_CLICK, CDlgOrigin::ClickAxisOriginStop, VTS_NONE)
	ON_EVENT(CDlgOrigin, IDB_AXIS_NAME_0, DISPID_CLICK, CDlgOrigin::ClickAxisName0, VTS_NONE)
	ON_EVENT(CDlgOrigin, IDB_AXIS_NAME_1, DISPID_CLICK, CDlgOrigin::ClickAxisName1, VTS_NONE)
	ON_EVENT(CDlgOrigin, IDB_AXIS_NAME_2, DISPID_CLICK, CDlgOrigin::ClickAxisName2, VTS_NONE)
	ON_EVENT(CDlgOrigin, IDB_AXIS_NAME_3, DISPID_CLICK, CDlgOrigin::ClickAxisName3, VTS_NONE)
	ON_EVENT(CDlgOrigin, IDB_AXIS_NAME_4, DISPID_CLICK, CDlgOrigin::ClickAxisName4, VTS_NONE)

	ON_EVENT(CDlgOrigin, IDB_AXIS_MOVE_MINUS_0, DISPID_MOUSEDOWN,	CDlgOrigin::MouseDownAxisMoveMinus0, VTS_I2 VTS_I2 VTS_I4 VTS_I4)
	ON_EVENT(CDlgOrigin, IDB_AXIS_MOVE_MINUS_0, DISPID_MOUSEUP,		CDlgOrigin::MouseUpAxisMoveMinus0, VTS_I2 VTS_I2 VTS_I4 VTS_I4)
	ON_EVENT(CDlgOrigin, IDB_AXIS_MOVE_PLUS_0,	DISPID_MOUSEDOWN,	CDlgOrigin::MouseDownAxisMovePlus0, VTS_I2 VTS_I2 VTS_I4 VTS_I4)
	ON_EVENT(CDlgOrigin, IDB_AXIS_MOVE_PLUS_0,	DISPID_MOUSEUP,		CDlgOrigin::MouseUpAxisMovePlus0, VTS_I2 VTS_I2 VTS_I4 VTS_I4)
	
//	ON_EVENT(CDlgOrigin, IDB_AXIS_MOVE_STANDBYPOS, DISPID_CLICK, CDlgOrigin::ClickAxisMoveStandbypos, VTS_NONE)
//	ON_EVENT(CDlgOrigin, IDB_AXIS_MOVE_TESTPOS, DISPID_CLICK, CDlgOrigin::ClickAxisMoveTestpos, VTS_NONE)
//ON_EVENT(CDlgOrigin, IDB_DIALOG_CLOSE, DISPID_CLICK, CDlgOrigin::ClickDialogClose, VTS_NONE)
ON_EVENT(CDlgOrigin, IDB_DIALOG_CLOSE, DISPID_CLICK, CDlgOrigin::ClickDialogClose, VTS_NONE)
END_EVENTSINK_MAP()


// 조그이동 +
void CDlgOrigin::ClickAxisMovePlus0()
{
}

// 조그이동 -
void CDlgOrigin::ClickAxisMoveMinus0()
{
}

// 알람 초기화
void CDlgOrigin::ClickAxisAlarmClear()
{
	if(m_pMotion)
		m_pMotion->AlarmClear(m_nCurAxis);
}

// 원점이동 시작
void CDlgOrigin::ClickAxisOriginStart()
{
	if(m_pMotion)
		m_pMotion->AllOrigin();
}

// 원점이동 정지
void CDlgOrigin::ClickAxisOriginStop()
{
	if(m_pMotion)
		m_pMotion->StopAllAxis();
}

// 모터
void CDlgOrigin::ClickAxisName0(){ ClickAxisName(); }
void CDlgOrigin::ClickAxisName1(){ ClickAxisName(); }
void CDlgOrigin::ClickAxisName2(){ ClickAxisName(); }
void CDlgOrigin::ClickAxisName3(){ ClickAxisName(); }
void CDlgOrigin::ClickAxisName4(){ ClickAxisName(); }

void CDlgOrigin::ClickAxisName()
{
	UINT nID = GetFocus()->GetDlgCtrlID();

	switch(nID){
		case IDB_AXIS_NAME_0:	m_nCurAxis = AXIS_X;		break;
		case IDB_AXIS_NAME_1:	m_nCurAxis = AXIS_Z;		break;
		case IDB_AXIS_NAME_2:	m_nCurAxis = AXIS_R;		break;
		case IDB_AXIS_NAME_3:	m_nCurAxis = AXIS_S;		break;
	//	case IDB_AXIS_NAME_4:	m_nCurAxis = AXIS_CHART_Z;		break;
	}
}

void CDlgOrigin::MouseDownAxisMoveMinus0(short Button, short Shift, long x, long y)
{
	if(m_pMotion)
		m_pMotion->JogMoveMinus(m_nCurAxis);
}
 
void CDlgOrigin::MouseUpAxisMoveMinus0(short Button, short Shift, long x, long y)
{
	if(m_pMotion)
		m_pMotion->JogMoveStop(m_nCurAxis);
}

void CDlgOrigin::MouseDownAxisMovePlus0(short Button, short Shift, long x, long y)
{
	if(m_pMotion) 
		m_pMotion->JogMovePlus(m_nCurAxis);
} 

void CDlgOrigin::MouseUpAxisMovePlus0(short Button, short Shift, long x, long y)
{
	if(m_pMotion)
		m_pMotion->JogMoveStop(m_nCurAxis);
}

void CDlgOrigin::OnClose()
{
	StopTimer();

	if(m_pMotion)
		m_pMotion->SetStop(TRUE);

	m_bShow = FALSE;

	CDialog::OnClose();
}

void CDlgOrigin::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if(bShow)
	{
		StartTimer();
		m_bShow = TRUE;
	}
	else
	{
		StopTimer();
		m_bShow = FALSE;
	}
}

void CDlgOrigin::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);
}

void CDlgOrigin::ClickDialogClose()
{
	this->ShowWindow(SW_HIDE);
}
