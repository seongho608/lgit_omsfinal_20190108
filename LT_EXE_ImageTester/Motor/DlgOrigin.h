#pragma once


#include "btnenh.h"

#include "./MotionControl.h"

#include "../resource.h"



// CDlgOrigin 대화 상자입니다.

class CDlgOrigin : public CDialog
{
	DECLARE_DYNAMIC(CDlgOrigin)

public:
	CDlgOrigin(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlgOrigin();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_ORIGIN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CMotionControl* m_pMotion;

	int		m_nCurAxis;

	CBtnEnh		m_ctrlCurPos[TOTAL_NUM_AXIS];
	CBtnEnh		m_ctrlOrigin[TOTAL_NUM_AXIS];
	CBtnEnh		m_ctrlMLimit[TOTAL_NUM_AXIS];
	CBtnEnh		m_ctrlHLimit[TOTAL_NUM_AXIS];
	CBtnEnh		m_ctrlPLimit[TOTAL_NUM_AXIS];
	CBtnEnh		m_ctrlPower[TOTAL_NUM_AXIS];

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	void			SetMotion(CMotionControl* pMotion);
	CMotionControl*	GetMotion();

	void UpdateCurPos();
	void UpdateOriginStatus();
	void UpdateSensorStatus();

	void StartTimer();
	void StopTimer();

	afx_msg void OnBnClickedOk();
	afx_msg void OnDestroy();
	
	void ClickAxisName();

	void MouseDownAxisMoveMinus0(short Button, short Shift, long x, long y);
	void MouseUpAxisMoveMinus0(short Button, short Shift, long x, long y);
	void MouseDownAxisMovePlus0(short Button, short Shift, long x, long y);
	void MouseUpAxisMovePlus0(short Button, short Shift, long x, long y);

	DECLARE_EVENTSINK_MAP()
	
	void ClickAxisMovePlus0();
	void ClickAxisMoveMinus0();
	void ClickAxisAlarmClear();
	void ClickAxisOriginStart();
	void ClickAxisOriginStop();
	void ClickAxisName0();
	void ClickAxisName1();
	void ClickAxisName2();
	void ClickAxisName3();
	void ClickAxisName4();
	
	afx_msg void OnClose();
//	void ClickAxisMoveStandbypos();
//	void ClickAxisMoveTestpos();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

public:
	BOOL m_bShow;
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
//	void ClickDialogClose();
	void ClickDialogClose();
};
