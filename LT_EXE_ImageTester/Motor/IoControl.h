#pragma once

#include "AXD.h"

#define		MAX_IO_PORT		16

class CIoControl
{
public:
	CIoControl(LPVOID p);
	~CIoControl(void);

	BOOL	StartIOMonitoring();
	BOOL	SetIOOutPort(int nPort, BOOL bState);

	BOOL	GetIOInStatus(int nPort);
	BOOL	GetIOOutStatus(int nPort);

	int		ThreadFunction();
	BOOL	DestroyThread();

	void	SetThreadRun(BOOL bRun){m_bRun = bRun;}
	BOOL	GetThreadRun();

	void	DoEvents();
	void	DoEvents(DWORD dwMiliSeconds);

private:
	static UINT	Thread_Fun(LPVOID p);

private:
	BOOL	m_bInStatus[MAX_IO_PORT];
	BOOL	m_bOutTest[MAX_IO_PORT];

	CWinThread*		m_pThread;

	BOOL	m_bRun;
};
