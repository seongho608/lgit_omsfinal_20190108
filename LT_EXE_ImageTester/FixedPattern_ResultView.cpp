// FixedPattern_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "FixedPattern_ResultView.h"
#include "afxdialogex.h"


// CFixedPattern_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFixedPattern_ResultView, CDialog)

CFixedPattern_ResultView::CFixedPattern_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CFixedPattern_ResultView::IDD, pParent)
{

}

CFixedPattern_ResultView::~CFixedPattern_ResultView()
{
}

void CFixedPattern_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CFixedPattern_ResultView, CDialog)
	ON_WM_CTLCOLOR()
	ON_EN_SETFOCUS(IDC_EDIT_FIXEDPATTERNTXT, &CFixedPattern_ResultView::OnEnSetfocusEditFixedpatterntxt)
	ON_EN_SETFOCUS(IDC_EDIT_FIXEDPATTERNNUM, &CFixedPattern_ResultView::OnEnSetfocusEditFixedpatternnum)
	ON_EN_SETFOCUS(IDC_EDIT_FIXEDPATTERNRESULT, &CFixedPattern_ResultView::OnEnSetfocusEditFixedpatternresult)
END_MESSAGE_MAP()


// CFixedPattern_ResultView 메시지 처리기입니다.

void CFixedPattern_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd = IN_pMomWnd;
}
void CFixedPattern_ResultView::Font_Size_Change(int nID, CFont *font, LONG Weight, LONG Height)//나중에 전역변수로 간략화 할것임
{
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = Weight;

	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);

	GetDlgItem(nID)->SetFont(font);
}

BOOL CFixedPattern_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	Font_Size_Change(IDC_EDIT_FIXEDPATTERNTXT, &ft1, 40, 20);
	GetDlgItem(IDC_EDIT_FIXEDPATTERNNUM)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_FIXEDPATTERNRESULT)->SetFont(&ft1);

	Initstat();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void CFixedPattern_ResultView::Initstat(){

	RESULT_TEXT(0, "STAND BY");
	FIXEDPATTERNNUM_TEXT("");
	FIXEDPATTERN_TEXT("Fixed Pattern");

}
HBRUSH CFixedPattern_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if (pWnd->GetDlgCtrlID() == IDC_EDIT_FIXEDPATTERNTXT){
		pDC->SetTextColor(RGB(255, 255, 255));

		pDC->SetBkColor(RGB(86, 86, 86));
	}


	if (pWnd->GetDlgCtrlID() == IDC_EDIT_FIXEDPATTERNNUM){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if (pWnd->GetDlgCtrlID() == IDC_EDIT_FIXEDPATTERNRESULT){
		pDC->SetTextColor(txcol_TVal);
		pDC->SetBkColor(bkcol_TVal);
	}
	return hbr;
}

BOOL CFixedPattern_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}


void CFixedPattern_ResultView::OnEnSetfocusEditFixedpatterntxt()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}


void CFixedPattern_ResultView::OnEnSetfocusEditFixedpatternnum()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}


void CFixedPattern_ResultView::OnEnSetfocusEditFixedpatternresult()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}
void CFixedPattern_ResultView::FIXEDPATTERN_TEXT(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_FIXEDPATTERNTXT))->SetWindowText(lpcszString);
}
void CFixedPattern_ResultView::FIXEDPATTERNNUM_TEXT(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_FIXEDPATTERNNUM))->SetWindowText(lpcszString);
}

void CFixedPattern_ResultView::RESULT_TEXT(unsigned char col, LPCSTR lpcszString, ...)
{
	if (col == 0){//대기
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}
	else if (col == 1){//성공
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18, 69, 171);
	}
	else if (col == 2){//실패
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}
	else if (col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_FIXEDPATTERNRESULT))->SetWindowText(lpcszString);
}