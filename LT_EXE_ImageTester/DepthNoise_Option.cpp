
#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "DepthNoise_Option.h"

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
extern WORD	m_GRAYScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];
extern BYTE	m_OverlayArea[CAM_IMAGE_WIDTH][CAM_IMAGE_HEIGHT];

IMPLEMENT_DYNAMIC(CDepthNoise_Option, CDialog)

CDepthNoise_Option::CDepthNoise_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CDepthNoise_Option::IDD, pParent)
{
	m_CAM_SIZE_WIDTH  = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;

	m_iSavedItem	= 0;
	m_iSavedSubitem = 0;
	m_dwSlaveID		= 0;
	m_dwAddress		= 0;
	m_nMinSpec		= 0;
	m_nMaxSpec		= 1;
	StartCnt		= 0;
	m_nBright		= 0;
}

CDepthNoise_Option::~CDepthNoise_Option()
{
}

void CDepthNoise_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_DN,			m_ROI_List		);
	DDX_Control(pDX, IDC_LIST_WORK,			m_WorkList		);
	DDX_Control(pDX, IDC_LIST_LOT_WORKLIST, m_Lot_WorkList	);
}

BEGIN_MESSAGE_MAP(CDepthNoise_Option, CDialog)
	ON_WM_SHOWWINDOW()	
	ON_WM_MOUSEWHEEL()	
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK,	IDC_LIST_DN, &CDepthNoise_Option::OnNMClickListDepthNoise)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_DN, &CDepthNoise_Option::OnNMDblclkListDepthNoise)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_DN, &CDepthNoise_Option::OnNMCustomdrawListDepthNoise)
	ON_BN_CLICKED(IDC_BUTTON_DN_TEST, &CDepthNoise_Option::OnBnClickedButtonTest)
	ON_BN_CLICKED(IDC_BUTTON_DN_SAVE, &CDepthNoise_Option::OnBnClickedButtonSave)
	ON_EN_CHANGE(IDC_EDIT_DN_MOD, &CDepthNoise_Option::OnEnChangeEditTMod)
	ON_EN_KILLFOCUS(IDC_EDIT_DN_MOD, &CDepthNoise_Option::OnEnKillfocusEditTMod)
END_MESSAGE_MAP()

// CDepthNoise_Option 메시지 처리기입니다.
void CDepthNoise_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

//=============================================================================
// Method		: Setup
// Access		: public  
// Returns		: void
// Parameter	: CWnd * IN_pMomWnd
// Parameter	: CEdit * pTEDIT
// Parameter	: tINFO INFO
// Qualifier	:
// Last Update	: 2018/6/2 - 17:45
// Desc.		:
//=============================================================================
void CDepthNoise_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO; 

	str_model.Empty();
	str_model.Format(INFO.NAME);
}

//=============================================================================
// Method		: OnInitDialog
// Access		: virtual public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/6/2 - 17:45
// Desc.		:
//=============================================================================
BOOL CDepthNoise_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_CAM_SIZE_WIDTH  = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;

	OnSetList_Header();

	DepthNoise_filename =((CImageTesterDlg  *)m_pMomWnd)->modelfile_Name;

	Load_parameter();
	UpdateData(FALSE);

	OnSetList_Data();

	Set_List(&m_WorkList);
	LOT_Set_List(&m_Lot_WorkList);

	InitEVMS();

	return TRUE;  // return TRUE unless you set the focus to a control
}

//=============================================================================
// Method		: InitEVMS
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:13
// Desc.		:
//=============================================================================
void CDepthNoise_Option::InitEVMS()
{
}

//=============================================================================
// Method		: DepthNoisePic
// Access		: public  
// Returns		: bool
// Parameter	: CDC * cdc
// Parameter	: int NUM
// Qualifier	:
// Last Update	: 2018/6/2 - 18:13
// Desc.		:
//=============================================================================
bool CDepthNoise_Option::DepthNoisePic(CDC *cdc,int NUM)
{
	CPen	my_Pan;
	CPen	my_Pan2;
	CPen	*old_pan;
	CPen	*old_pan2;
	CFont	font;
	CString szText;

	::SetBkMode(cdc->m_hDC, TRANSPARENT);

	my_Pan.CreatePen(PS_SOLID, 2, RED_COLOR);
	old_pan = cdc->SelectObject(&my_Pan);

	if (m_bResult == TRUE)
	{
		cdc->SetTextColor(BLUE_COLOR);

		my_Pan2.CreatePen(PS_SOLID, 2, RGB(0, 0, 255));
		old_pan2 = cdc->SelectObject(&my_Pan2);
	}
	else
	{
		cdc->SetTextColor(RED_COLOR);

		my_Pan2.CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
		old_pan2 = cdc->SelectObject(&my_Pan2);
	}

	font.CreatePointFont(120, "Arial");
	cdc->SetTextAlign(TA_CENTER | TA_BASELINE);
	szText.Format("Bright : %d", m_nBright);

	cdc->TextOut(CAM_IMAGE_WIDTH / 2, CAM_IMAGE_HEIGHT - 100, szText.GetBuffer(0), szText.GetLength());

	cdc->MoveTo(m_rtROI.left,	m_rtROI.top		);
	cdc->LineTo(m_rtROI.right,	m_rtROI.top		);	
	cdc->LineTo(m_rtROI.right,	m_rtROI.bottom	);
	cdc->LineTo(m_rtROI.left,	m_rtROI.bottom	);
	cdc->LineTo(m_rtROI.left,	m_rtROI.top		);

	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();
	my_Pan2.DeleteObject();
	old_pan->DeleteObject();
	old_pan2->DeleteObject();

	font.DeleteObject();

	return TRUE;
}

//=============================================================================
// Method		: Pic
// Access		: public  
// Returns		: void
// Parameter	: CDC * cdc
// Qualifier	:
// Last Update	: 2018/6/2 - 18:25
// Desc.		:
//=============================================================================
void CDepthNoise_Option::Pic(CDC *cdc)
{
	DepthNoisePic(cdc, 0);
}

//=============================================================================
// Method		: InitPrm
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:25
// Desc.		:
//=============================================================================
void CDepthNoise_Option::InitPrm()
{
	Load_parameter();
	UpdateData(FALSE);
}

//=============================================================================
// Method		: Run
// Access		: public  
// Returns		: tResultVal
// Qualifier	:
// Last Update	: 2018/6/2 - 18:25
// Desc.		:
//=============================================================================
tResultVal CDepthNoise_Option::Run()
{	
	CString szValue="";
	b_StopFail = FALSE;

	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE)
	{
		InsertList();
	}

//	((CImageTesterDlg  *)m_pMomWnd)->m_pResDepthNoiseOptWnd->Initstat();
	
	int camcnt = 0;
	tResultVal retval = { 0, };

	DoEvents(500);

	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = 1;
	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;

	for (int i = 0; i < 10; i++)
	{
		if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0 && ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0)
		{
			break;
		}
		else
		{
			DoEvents(50);
		}
	}


	BOOL bReulst = TRUE;

	BYTE arbyBufRead[256] = { 0, };
	BOOL bResult = FALSE;

	// * Read :  Max 256 Byte
	if (TRUE == ((CImageTesterDlg *)m_pMomWnd)->m_LVDSVideo.I2C_Read_Repeat(0, (BYTE)m_dwSlaveID << 1, 2, m_dwAddress, 2, arbyBufRead))
	{
		CString strData;

		strData.Format(_T("%02x%02x"), arbyBufRead[0], arbyBufRead[1]);
		if (_T("0001") == strData)		{			((CImageTesterDlg  *)m_pMomWnd)->StatePrintf("BG MODE PASS");
			bResult = DepthNoiseGen_16bit(m_GRAYScanbuf);
		}
		else		{			bResult = FALSE;			((CImageTesterDlg  *)m_pMomWnd)->StatePrintf("BG MODE FAIL");
		}
	}	else	{		bResult = FALSE;		((CImageTesterDlg  *)m_pMomWnd)->StatePrintf("BG MODE FAIL");
	}

	//---------------------------
	retval.m_ID = 0x18;
	retval.ValString.Empty();

	CString stateDATA = "Depth Noise 검사_";
	szValue.Format("%d", m_nBright);

	if (bResult == TRUE)
	{
		m_Success = TRUE;
		m_WorkList.SetItemText(InsertIndex, 4, szValue);
		m_WorkList.SetItemText(InsertIndex, 5, "PASS");

		retval.m_Success = TRUE;
		Str_Mes[0] = szValue;
		Str_Mes[1] = "1";
	
		if (((CImageTesterDlg *)m_pMomWnd)->LotMod != 1)
		{
 			((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_DEPTHNOISE, "PASS");
		}

		stateDATA += "PASS";
		retval.ValString.Format("OK");
	}
	else
	{
		m_Success = FALSE;
		m_WorkList.SetItemText(InsertIndex, 4, szValue);
		m_WorkList.SetItemText(InsertIndex, 5, "FAIL");

		retval.m_Success = FALSE;
		Str_Mes[0] = szValue;
		Str_Mes[1] = "0";
	
		if (((CImageTesterDlg *)m_pMomWnd)->LotMod != 1)
		{
			((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_DEPTHNOISE, "FAIL");
		}

		stateDATA += "FAIL";
		retval.ValString.Format("FAIL");

	}

	((CImageTesterDlg  *)m_pMomWnd)->StatePrintf(stateDATA);
		
	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE)
	{
		StartCnt++;
	}
		
	return retval;
}	

//=============================================================================
// Method		: FAIL_UPLOAD
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:28
// Desc.		:
//=============================================================================
void CDepthNoise_Option::FAIL_UPLOAD()
{
 	if (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)
	{
		InsertList();

		m_WorkList.SetItemText(InsertIndex, 4, "X");//result
		m_WorkList.SetItemText(InsertIndex, 5, "FAIL");//result
		m_WorkList.SetItemText(InsertIndex, 6, "STOP");//비고

		Str_Mes[0] = "FAIL";
		Str_Mes[1] = "0";

		((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_DEPTHNOISE, "STOP");
		StartCnt++;
		b_StopFail = TRUE;
 	}
}

//=============================================================================
// Method		: CModel_Create
// Access		: public  
// Returns		: void
// Parameter	: CDepthNoise_Option * * pWnd
// Parameter	: CTabCtrl * pTabWnd
// Parameter	: CWnd * pMomWnd
// Parameter	: CEdit * pEdit
// Parameter	: tINFO INFO
// Qualifier	:
// Last Update	: 2018/6/2 - 18:29
// Desc.		:
//=============================================================================
void CModel_Create(CDepthNoise_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit *pEdit,tINFO INFO)
{
	CRect OptionRect;

	if(pTabWnd != NULL)
	{
		((CDepthNoise_Option *)*pWnd) = new CDepthNoise_Option(pMomWnd);
		((CDepthNoise_Option *)*pWnd)->Setup(pMomWnd,pEdit,INFO);
		((CDepthNoise_Option *)*pWnd)->Create(((CDepthNoise_Option *)*pWnd)->IDD,pTabWnd);//&m_CtrTab);
		((CDepthNoise_Option *)*pWnd)->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		((CDepthNoise_Option *)*pWnd)->MoveWindow(20,40,OptionRect.Width(),OptionRect.Height());
	}
}

//=============================================================================
// Method		: CModel_Delete
// Access		: public  
// Returns		: void
// Parameter	: CDepthNoise_Option * * pWnd
// Qualifier	:
// Last Update	: 2018/6/2 - 18:29
// Desc.		:
//=============================================================================
void CModel_Delete(CDepthNoise_Option **pWnd)
{
	if(((CDepthNoise_Option *)*pWnd) != NULL)
	{
		delete ((CDepthNoise_Option *)*pWnd);
		((CDepthNoise_Option *)*pWnd) = NULL;	
	}
}

//=============================================================================
// Method		: StatePrintf
// Access		: public  
// Returns		: void
// Parameter	: LPCSTR lpcszString
// Parameter	: ...
// Qualifier	:
// Last Update	: 2018/6/2 - 18:29
// Desc.		:
//=============================================================================
void CDepthNoise_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN)
	{
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

//=============================================================================
// Method		: Load_parameter
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 17:56
// Desc.		:
//=============================================================================
void CDepthNoise_Option::Load_parameter()
{
	CFileFind filefind;
	CString szTitle = "DEPTH_NOICES_INIT";

	m_dwSlaveID = GetPrivateProfileDouble(str_model, szTitle + "SlaveID", 0, DepthNoise_filename);
	m_dwAddress = GetPrivateProfileDouble(str_model, szTitle + "Address", 0, DepthNoise_filename);
	m_nMinSpec  = GetPrivateProfileDouble(str_model, szTitle + "MinSpec", 0, DepthNoise_filename);
	m_nMaxSpec  = GetPrivateProfileDouble(str_model, szTitle + "MaxSpec", 0, DepthNoise_filename);

	CString szData;
	Edit_SetValue(IDC_DN_SLAVEID, m_dwSlaveID);
	Edit_SetValue(IDC_DN_ADDRESS, m_dwAddress);

	szData.Format(_T("%d"), m_nMinSpec);
	((CEdit *)GetDlgItem(IDC_DN_MIN_SPEC))->SetWindowText(szData);

	szData.Format(_T("%d"), m_nMaxSpec);
	((CEdit *)GetDlgItem(IDC_DN_MAX_SPEC))->SetWindowText(szData);

	// ROI
	m_rtROI.left	= GetPrivateProfileInt(str_model, szTitle + "left",		-1, DepthNoise_filename);
	m_rtROI.top		= GetPrivateProfileInt(str_model, szTitle + "top",		-1, DepthNoise_filename);
	m_rtROI.right	= GetPrivateProfileInt(str_model, szTitle + "right",	-1, DepthNoise_filename);
	m_rtROI.bottom	= GetPrivateProfileInt(str_model, szTitle + "bottom",	-1, DepthNoise_filename);

	Save_parameter();
}

//=============================================================================
// Method		: Save_parameter
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 17:59
// Desc.		:
//=============================================================================
void CDepthNoise_Option::Save_parameter()
{
	CString szValue;
	CString szTitle = "DEPTH_NOICES_INIT";

	szValue.Format("%d", m_INFO.ID);
	WritePrivateProfileString(str_model, "ID", szValue, DepthNoise_filename);

	szValue.Format("%s", m_INFO.MODE);
	WritePrivateProfileString(str_model, "MODE", szValue, DepthNoise_filename);

	szValue.Format("%s", m_INFO.NAME);
	WritePrivateProfileString(str_model, "NAME", szValue, DepthNoise_filename);

	szValue.Format("%d", m_dwSlaveID);
	WritePrivateProfileString(str_model, szTitle + "SlaveID", szValue, DepthNoise_filename);

	szValue.Format("%d", m_dwAddress);
	WritePrivateProfileString(str_model, szTitle + "Address", szValue, DepthNoise_filename);

	szValue.Format("%d", m_nMinSpec);
	WritePrivateProfileString(str_model, szTitle + "MinSpec", szValue, DepthNoise_filename);

	szValue.Format("%d", m_nMaxSpec);
	WritePrivateProfileString(str_model, szTitle + "MaxSpec", szValue, DepthNoise_filename);

	szValue.Format("%d", m_rtROI.left);
	WritePrivateProfileString(str_model, szTitle + "left", szValue, DepthNoise_filename);

	szValue.Format("%d", m_rtROI.top);
	WritePrivateProfileString(str_model, szTitle + "top", szValue, DepthNoise_filename);

	szValue.Format("%d", m_rtROI.right);
	WritePrivateProfileString(str_model, szTitle + "right", szValue, DepthNoise_filename);

	szValue.Format("%d", m_rtROI.bottom);
	WritePrivateProfileString(str_model, szTitle + "bottom", szValue, DepthNoise_filename);

	UpdateData(FALSE);
}

//=============================================================================
// Method		: Save
// Access		: public  
// Returns		: void
// Parameter	: int NUM
// Qualifier	:
// Last Update	: 2018/6/2 - 18:29
// Desc.		:
//=============================================================================
void CDepthNoise_Option::Save(int NUM)
{
	WritePrivateProfileString(str_model,NULL,"",DepthNoise_filename);

	if(NUM != -1)
	{
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

//---------------------------------------------------------------------WORKLIST
//=============================================================================
// Method		: InsertList
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:29
// Desc.		:
//=============================================================================
void CDepthNoise_Option::InsertList()
{
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;
	
	InsertIndex = m_WorkList.InsertItem(StartCnt,"",0);
	
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_WorkList.SetItemText(InsertIndex,0,strCnt);
	
	m_WorkList.SetItemText(InsertIndex,1,((CImageTesterDlg  *)m_pMomWnd)->m_modelName);
	m_WorkList.SetItemText(InsertIndex,2,((CImageTesterDlg  *)m_pMomWnd)->strDay+"");
	m_WorkList.SetItemText(InsertIndex,3,((CImageTesterDlg  *)m_pMomWnd)->strTime+"");
}

//=============================================================================
// Method		: Set_List
// Access		: public  
// Returns		: void
// Parameter	: CListCtrl * List
// Qualifier	:
// Last Update	: 2018/6/2 - 18:29
// Desc.		:
//=============================================================================
void CDepthNoise_Option::Set_List(CListCtrl *List)
{

	while(List->DeleteColumn(0));
	((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);

	int LISTNUM = 4;

	List->InsertColumn(LISTNUM++, "Bright", LVCFMT_CENTER, 80);
	List->InsertColumn(LISTNUM++, "RESULT", LVCFMT_CENTER, 80);
	List->InsertColumn(LISTNUM++, "비고", LVCFMT_CENTER, 80);

	ListItemNum = LISTNUM;
	Copy_List(&m_WorkList, ((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList, ListItemNum);
}

//=============================================================================
// Method		: Mes_Result
// Access		: public  
// Returns		: CString
// Qualifier	:
// Last Update	: 2018/6/2 - 18:30
// Desc.		:
//=============================================================================
CString CDepthNoise_Option::Mes_Result()
{
	CString sz[9];
	int	nResult[9];

	// 제일 마지막 항목을 추가하자
	int nSel = m_WorkList.GetItemCount()-1;

	// C-DB
	CString szValue;
	CString strData;
	
	strData.Empty();
	m_szMesResult = _T("");

	if (b_StopFail == FALSE){
		m_szMesResult.Format(_T("%s:%s"), Str_Mes[0], Str_Mes[1]);
		m_szMesResult.Replace(" ", "");
	}
	else{
		m_szMesResult.Format(_T(":1"));
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_stNewMesData[NewMES_DepthNoise] = m_szMesResult;

	return m_szMesResult;
}

//=============================================================================
// Method		: EXCEL_SAVE
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:30
// Desc.		:
//=============================================================================
void CDepthNoise_Option::EXCEL_SAVE()
{
	CString Item[1] = { "VAL" };
	CString Data = "";
	CString szValue = "";
	szValue = "***********************Hot Pixel***********************\n";
	Data += szValue;
	szValue.Empty();
	szValue.Format("\n");
	Data += szValue;
	szValue.Empty();
	szValue.Format("\n");
	Data += szValue;
	szValue.Empty();
	szValue = "*************************************************\n\n";
	Data += szValue;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("DepthNoise", Item, 1, &m_WorkList, Data);
}

//=============================================================================
// Method		: EXCEL_UPLOAD
// Access		: public  
// Returns		: bool
// Qualifier	:
// Last Update	: 2018/6/2 - 18:30
// Desc.		:
//=============================================================================
bool CDepthNoise_Option::EXCEL_UPLOAD()
{
	m_WorkList.DeleteAllItems();
	
	if(((CImageTesterDlg  *)m_pMomWnd)->EXCEL_UPLOAD("DepthNoise",&m_WorkList,1,&StartCnt)== TRUE)
	{
		return TRUE;
	}
	else
	{
		StartCnt = 0;
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2018/6/2 - 18:30
// Desc.		:
//=============================================================================
BOOL CDepthNoise_Option::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}

		if (pMsg->wParam == VK_RETURN)
		{
			return TRUE;
		}
		
	}

	return CDialog::PreTranslateMessage(pMsg);
}

	
//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2018/6/2 - 18:30
// Desc.		:
//=============================================================================
void CDepthNoise_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if(bShow == TRUE)
	{
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;
	}
	else
	{
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = -1;
	}
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2018/6/3 - 11:47
// Desc.		:
//=============================================================================
BOOL CDepthNoise_Option::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	int iVal = zDelta / 120;
	CWnd *focusid;
	focusid = GetFocus();

	if (NULL == focusid)
		return FALSE;

	int casenum = focusid->GetDlgCtrlID();

	CString str;

	switch (casenum)
	{
	case IDC_EDIT_DN_MOD:
		OnSetChange_Data(iVal);
		break;
	}

	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: UploadList
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:30
// Desc.		:
//=============================================================================
void CDepthNoise_Option::UploadList()
{
}

//=============================================================================
// Method		: OnTimer
// Access		: public  
// Returns		: void
// Parameter	: UINT_PTR nIDEvent
// Qualifier	:
// Last Update	: 2018/6/2 - 18:30
// Desc.		:
//=============================================================================
void CDepthNoise_Option::OnTimer(UINT_PTR nIDEvent)
{
	CDialog::OnTimer(nIDEvent);
}

//=============================================================================
// Method		: DepthNoiseGen_16bit
// Access		: public  
// Returns		: BOOL
// Parameter	: WORD * GRAYScanBuf
// Qualifier	:
// Last Update	: 2018/4/24 - 20:07
// Desc.		:
//=============================================================================
BOOL CDepthNoise_Option::DepthNoiseGen_16bit(WORD *GRAYScanBuf)
{
	int iStart_X	= m_rtROI.left;
	int iStart_Y	= m_rtROI.top;
	int iEnd_X 		= m_rtROI.right + 1;
	int iEnd_Y		= m_rtROI.bottom + 1;
	int iImgWidth	= m_rtROI.Width();
	int iImgHeight	= m_rtROI.Height();

	int iTempData	= 0;
	int iData		= 0;
	int Signal		= 0;
	double dbNoise	= 0;


	for (int y = iStart_Y; y < iEnd_Y; y++)
	{
		for (int x = iStart_X; x < iEnd_X; x++)
		{
			iData += GRAYScanBuf[y * CAM_IMAGE_WIDTH + x];
		}
	}
	
	Signal = iData / (iImgWidth * iImgHeight);

	for (int y = iStart_Y; y < iEnd_Y; y++)
	{
		for (int x = iStart_X; x < iEnd_X; x++)
		{
			iData = abs(Signal - GRAYScanBuf[y * CAM_IMAGE_WIDTH + x]);
			iTempData += iData * iData;
		}
	}
	
	dbNoise = sqrt(iTempData / (iImgWidth * iImgHeight));

	m_nBright = Signal;

	CString szValue;

	szValue.Format(_T("%d"), m_nBright);

	if (m_nMinSpec <= m_nBright && m_nMaxSpec >= m_nBright)
	{
		((CImageTesterDlg  *)m_pMomWnd)->m_pResDepthNoiseOptWnd->RESULT_TEXT(1, "PASS");
		((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->TestState(0, "PASS");
		m_bResult = TRUE;
	}
	else
	{
		((CImageTesterDlg  *)m_pMomWnd)->m_pResDepthNoiseOptWnd->RESULT_TEXT(2, "FAIL");
		((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->TestState(1, "FAIL");
		m_bResult = FALSE;
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_pResDepthNoiseOptWnd->DepthNoiseNUM_TEXT(szValue);

 	return m_bResult;
}

//=============================================================================
// Method		: Edit_GetValue
// Access		: public  
// Returns		: UINT
// Parameter	: UINT nID
// Parameter	: BOOL bHex
// Qualifier	:
// Last Update	: 2018/6/3 - 14:28
// Desc.		:
//=============================================================================
UINT CDepthNoise_Option::Edit_GetValue(UINT nID, BOOL bHex /*= TRUE*/)
{
	CString text;
	((CEdit *)GetDlgItem(nID))->GetWindowText(text);

	//hex
	unsigned int n = 0;
	if (bHex)
	{
		if (text.Find(_T("0x")) == 0)
			_stscanf_s(text.GetBuffer(0) + 2, _T("%x"), &n);
		else
			_stscanf_s(text.GetBuffer(0), _T("%x"), &n);
	}
	else{
		_stscanf_s(text.GetBuffer(0), _T("%u"), &n);
	}

	return n;
}

//=============================================================================
// Method		: Edit_SetValue
// Access		: public  
// Returns		: void
// Parameter	: UINT nID
// Parameter	: DWORD wData
// Parameter	: BOOL bHex
// Qualifier	:
// Last Update	: 2018/6/3 - 14:28
// Desc.		:
//=============================================================================
void CDepthNoise_Option::Edit_SetValue(UINT nID, DWORD wData, BOOL bHex /*= TRUE*/)
{
	CString text;
	if (bHex)
	{
		text.Format(_T("0x%04x"), wData);
	}
	else{
		text.Format(_T("%d"), wData);

	}
	((CEdit *)GetDlgItem(nID))->SetWindowText(text);
}

//=============================================================================
// Method		: OnBnClickedButtonTest
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:31
// Desc.		:
//=============================================================================
void CDepthNoise_Option::OnBnClickedButtonTest()
{
	((CButton *)GetDlgItem(IDC_BUTTON_DN_TEST))->EnableWindow(0);

	((CImageTesterDlg  *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();

	((CButton *)GetDlgItem(IDC_BUTTON_DN_TEST))->EnableWindow(1);
}

#pragma region LOT관련 함수
//=============================================================================
// Method		: LOT_Set_List
// Access		: public  
// Returns		: void
// Parameter	: CListCtrl * List
// Qualifier	:
// Last Update	: 2018/6/2 - 18:31
// Desc.		:
//=============================================================================
void CDepthNoise_Option::LOT_Set_List(CListCtrl *List)
{
	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0, "NO", LVCFMT_CENTER, 30);
	List->InsertColumn(1, "모델명", LVCFMT_CENTER, 100);
	List->InsertColumn(2, "LOT 명", LVCFMT_CENTER, 80);
	List->InsertColumn(3, "Start DATE", LVCFMT_CENTER, 80);
	List->InsertColumn(4, "Start TIME", LVCFMT_CENTER, 80);
	List->InsertColumn(5, "Bright", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "result", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "비고", LVCFMT_CENTER, 80);

	Lot_InsertNum = 8;
	Copy_List(&m_Lot_WorkList, ((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList, Lot_InsertNum);
}

//=============================================================================
// Method		: LOT_InsertDataList
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:31
// Desc.		:
//=============================================================================
void CDepthNoise_Option::LOT_InsertDataList()
{
	CString strCnt="";

	int Index = m_Lot_WorkList.InsertItem(Lot_StartCnt, "", 0);
	Lot_InsertIndex = m_Lot_WorkList.InsertItem(Lot_StartCnt, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Lot_InsertIndex + 1);
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 0, strCnt);

	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 1, ((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 2, ((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);////////LOT 이름
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 3, ((CImageTesterDlg *)m_pMomWnd)->strDay + "");
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 4, ((CImageTesterDlg *)m_pMomWnd)->strTime + "");

	int CopyIndex = m_WorkList.GetItemCount() - 1;

	int count = 0;
	for (int t = 0; t < Lot_InsertNum; t++){
		if ((t != 2) && (t != 0)){
			m_Lot_WorkList.SetItemText(Index, t, m_WorkList.GetItemText(CopyIndex, count));
			count++;

		}
		else if (t == 0){
			count++;
		}
		else{
			m_Lot_WorkList.SetItemText(Index, t, ((CImageTesterDlg  *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;
}

//=============================================================================
// Method		: LOT_EXCEL_SAVE
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:32
// Desc.		:
//=============================================================================
void CDepthNoise_Option::LOT_EXCEL_SAVE()
{
	CString Item[1] = { "Bright" };
	CString Data = "";
	CString szValue = "";
	szValue = "***********************DepthNoise***********************\n";
	Data += szValue;
	szValue.Empty();
	szValue.Format("\n");
	Data += szValue;
	szValue.Empty();
	szValue.Format("\n");
	Data += szValue;
	szValue.Empty();
	szValue = "*************************************************\n\n";
	Data += szValue;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("DepthNoise", Item, 1, &m_Lot_WorkList, Data);
}

//=============================================================================
// Method		: LOT_EXCEL_UPLOAD
// Access		: public  
// Returns		: bool
// Qualifier	:
// Last Update	: 2018/6/2 - 18:32
// Desc.		:
//=============================================================================
bool CDepthNoise_Option::LOT_EXCEL_UPLOAD()
{
	m_Lot_WorkList.DeleteAllItems();

	if(((CImageTesterDlg  *)m_pMomWnd)->LOT_EXCEL_UPLOAD("DepthNoise",&m_Lot_WorkList,1,&Lot_StartCnt)==TRUE)
	{
		return TRUE;	
	}
	else
	{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}
 
#pragma endregion 
//=============================================================================
// Method		: OnBnClickedButtonSave
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:32
// Desc.		:
//=============================================================================
void CDepthNoise_Option::OnBnClickedButtonSave()
{
	CString szData;

	m_dwSlaveID = Edit_GetValue(IDC_DN_SLAVEID);
	m_dwAddress = Edit_GetValue(IDC_DN_ADDRESS);
	
	((CEdit *)GetDlgItem(IDC_DN_MIN_SPEC))->GetWindowText(szData);
	m_nMinSpec = _ttoi(szData);
	
	((CEdit *)GetDlgItem(IDC_DN_MAX_SPEC))->GetWindowText(szData);
	m_nMaxSpec = _ttoi(szData);

	Save_parameter();
}

//=============================================================================
// Method		: OnNMClickListDepthNoise
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2018/6/2 - 18:42
// Desc.		:
//=============================================================================
void CDepthNoise_Option::OnNMClickListDepthNoise(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMCustomdrawListDepthNoise
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2018/6/2 - 18:41
// Desc.		:
//=============================================================================
void CDepthNoise_Option::OnNMCustomdrawListDepthNoise(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//*pResult = 0;

	COLORREF text_color = 0;
	COLORREF bg_color = RGB(255, 255, 255);

	LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	//	lplvcd->iPartId
	switch (lplvcd->nmcd.dwDrawStage){
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;
		return;

		// 배경 혹은 텍스트를 수정한다.
	case CDDS_ITEMPREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;
		return;

		// 서브 아이템의 배경 혹은 텍스트를 수정한다.
	case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
		if (lplvcd->iSubItem != 0)
		{
			// 1번째 행이라면...
			if (lplvcd->nmcd.dwItemSpec >= 0 || lplvcd->nmcd.dwItemSpec <5)
			{
				text_color = RGB(0, 0, 0);
				bg_color = RGB(255, 255, 255);

			}

			lplvcd->clrText = text_color;
			lplvcd->clrTextBk = bg_color;
		}

		*pResult = CDRF_NEWFONT;
		return;
	}
}

//=============================================================================
// Method		: OnNMDblclkListDepthNoise
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2018/6/2 - 18:41
// Desc.		:
//=============================================================================
void CDepthNoise_Option::OnNMDblclkListDepthNoise(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
		return;

	if (pNMITEM->iSubItem == 0 || pNMITEM->iSubItem == -1 || pNMITEM->iItem == -1)
		return;

	m_iSavedItem	= pNMITEM->iItem;
	m_iSavedSubitem = pNMITEM->iSubItem;

	if (pNMITEM->iItem != -1)
	{
		if (pNMITEM->iSubItem != 0)
		{
			CRect rect;

			m_ROI_List.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			m_ROI_List.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_DN_MOD))->SetWindowText(m_ROI_List.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_DN_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW);
			((CEdit *)GetDlgItem(IDC_EDIT_DN_MOD))->SetFocus();
		}
	}

	*pResult = 0;
}

void CDepthNoise_Option::OnEnChangeEditTMod()
{

}

void CDepthNoise_Option::OnEnKillfocusEditTMod()
{
	CString szValue;
	int iValue = 0;
	int iTemp = 0;

	if ((m_iSavedItem != -1) && (m_iSavedSubitem != -1))
	{
		((CEdit *)GetDlgItemText(IDC_EDIT_DN_MOD, szValue));

		if (m_iSavedSubitem == 1)
		{
			iTemp = m_rtROI.left;
			iValue = atoi(szValue);

			if (iValue < 0)
				iValue = 0;

			if (iValue > CAM_IMAGE_WIDTH)
				iValue = CAM_IMAGE_WIDTH;
			
			m_rtROI.left   = iValue;
			m_rtROI.right += (m_rtROI.left - iTemp);
		}

		if (m_iSavedSubitem == 2)
		{
			iTemp = m_rtROI.top;
			iValue = atoi(szValue);

			if (iValue < 0)
			{
				iValue = 0;
				m_rtROI.top     = iValue;
				m_rtROI.bottom += (m_rtROI.top - iTemp);
			}
			else if (iValue > CAM_IMAGE_HEIGHT)
			{
				iValue = CAM_IMAGE_HEIGHT;
				m_rtROI.top = iValue;
				m_rtROI.bottom += (m_rtROI.top - iTemp);
			}
			else
			{
				m_rtROI.top = iValue;
				m_rtROI.bottom += (m_rtROI.top - iTemp);
			}
		}

		if (m_iSavedSubitem == 3)
		{
			iValue = atoi(szValue);

			if (iValue < 0)
			{
				iValue = 0;
				m_rtROI.right = m_rtROI.left + iValue;
			}
			else if (iValue > CAM_IMAGE_WIDTH)
			{
				iValue = CAM_IMAGE_WIDTH;
				m_rtROI.right = m_rtROI.left + iValue;
			}
			else
			{
				m_rtROI.right = m_rtROI.left + iValue;
			}
		}

		if (m_iSavedSubitem == 4)
		{
			iValue = atoi(szValue);

			if (iValue < 0)
			{
				iValue = 0;
				m_rtROI.bottom = m_rtROI.top + iValue;
			}
			else if (iValue > CAM_IMAGE_HEIGHT)
			{
				iValue = CAM_IMAGE_HEIGHT;
				m_rtROI.bottom = m_rtROI.top + iValue;
			}
			else
			{
				m_rtROI.bottom = m_rtROI.top + iValue;
			}
		}

		m_ROI_List.SetItemText(m_iSavedItem, m_iSavedSubitem, szValue);
	}
	
	((CEdit *)GetDlgItem(IDC_EDIT_DN_MOD))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_DN_MOD))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
	
	m_iSavedItem = -1;
	m_iSavedSubitem = -1;
	((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
}

//=============================================================================
// Method		: OnSetChange_Data
// Access		: public  
// Returns		: void
// Parameter	: int iVal
// Qualifier	:
// Last Update	: 2018/6/3 - 11:49
// Desc.		:
//=============================================================================
void CDepthNoise_Option::OnSetChange_Data(int iVal)
{
	CString szValue = "";
	int buf = 0;
	int retval = 0;

	GetDlgItemText(IDC_EDIT_DN_MOD, szValue);
	szValue.Remove(' ');

	int iValue		= 0;
	int iValueBuf	= 0;
	int iTemp		= 0;

	if (m_iSavedSubitem == 1)
	{
		iValueBuf = atoi(szValue);
		iValue = iValueBuf + iVal;

		iTemp = m_rtROI.left;

		if (iValue < 0)
		{
			iValue = 0;
			m_rtROI.left   = iValue;
			m_rtROI.right += (m_rtROI.left - iTemp);
		}
		else if (iValue > CAM_IMAGE_WIDTH)
		{
			iValue = CAM_IMAGE_WIDTH;
			m_rtROI.left   = iValue;
			m_rtROI.right += (m_rtROI.left - iTemp);
		}
		else
		{
			m_rtROI.left   = iValue;
			m_rtROI.right += (m_rtROI.left - iTemp);
		}
	}

	if (m_iSavedSubitem == 2)
	{
		iValueBuf = atoi(szValue);
		iValue = iValueBuf + iVal;

		iTemp = m_rtROI.top;

		if (iValue < 0)
		{
			iValue = 0;
			m_rtROI.top = iValue;
			m_rtROI.bottom += (m_rtROI.top - iTemp);
		}
		else if (iValue > CAM_IMAGE_HEIGHT)
		{
			iValue = CAM_IMAGE_HEIGHT;
			m_rtROI.top = iValue;
			m_rtROI.bottom += (m_rtROI.top - iTemp);
		}
		else
		{
			m_rtROI.top = iValue;
			m_rtROI.bottom += (m_rtROI.top - iTemp);
		}
	}

	if (m_iSavedSubitem == 3)
	{
		iValueBuf = atoi(szValue);
		iValue = iValueBuf + iVal;

		if (iValue < 0)
		{
			iValue = 0;
			m_rtROI.right = m_rtROI.left + iValue;
		}
		else if (iValue > CAM_IMAGE_WIDTH)
		{
			iValue = CAM_IMAGE_WIDTH;
			m_rtROI.right = m_rtROI.left + iValue;
		}
		else
		{
			m_rtROI.right = m_rtROI.left + iValue;
		}
	}

	if (m_iSavedSubitem == 4)
	{
		iValueBuf = atoi(szValue);
		iValue = iValueBuf + iVal;

		if (iValue < 0)
		{
			iValue = 0;
			m_rtROI.bottom = m_rtROI.top + iValue;
		}
		else if (iValue > CAM_IMAGE_HEIGHT)
		{
			iValue = CAM_IMAGE_HEIGHT;
			m_rtROI.bottom = m_rtROI.top + iValue;
		}
		else
		{
			m_rtROI.bottom = m_rtROI.top + iValue;
		}
	}

	szValue.Format("%d", iValue);
	m_ROI_List.SetItemText(m_iSavedItem, m_iSavedSubitem, szValue);
	((CEdit *)GetDlgItem(IDC_EDIT_DN_MOD))->SetWindowText(szValue);

	((CEdit *)GetDlgItem(IDC_EDIT_DN_MOD))->SetSel(-2, -1);
}

//=============================================================================
// Method		: OnSetList_Data
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 17:51
// Desc.		:
//=============================================================================
void CDepthNoise_Option::OnSetList_Header()
{
	m_ROI_List.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_ROI_List.InsertColumn(0, "",			LVCFMT_CENTER, 90);
	m_ROI_List.InsertColumn(1, "Pos X",		LVCFMT_CENTER, 100);
	m_ROI_List.InsertColumn(2, "Pos Y",		LVCFMT_CENTER, 100);
	m_ROI_List.InsertColumn(3, "Width",		LVCFMT_CENTER, 100);
	m_ROI_List.InsertColumn(4, "Height",	LVCFMT_CENTER, 100);
}

//=============================================================================
// Method		: OnSetList_Data
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:08
// Desc.		:
//=============================================================================
void CDepthNoise_Option::OnSetList_Data()
{
	CString szValue;

	m_ROI_List.DeleteAllItems();

	m_ROI_List.InsertItem(0, "", 0);
	m_ROI_List.SetItemText(0, 0, _T("ROI"));
	
	szValue.Format("%d", m_rtROI.left);
	m_ROI_List.SetItemText(0, 1, szValue);
	
	szValue.Format("%d", m_rtROI.top);
	m_ROI_List.SetItemText(0, 2, szValue);

	szValue.Format("%d", m_rtROI.Width());
	m_ROI_List.SetItemText(0, 3, szValue);

	szValue.Format("%d", m_rtROI.Height());
	m_ROI_List.SetItemText(0, 4, szValue);
}

