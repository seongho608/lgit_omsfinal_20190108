#pragma once


// CIRFilter_ResultView 대화 상자입니다.

class CIRFilter_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CIRFilter_ResultView)

public:
	CIRFilter_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CIRFilter_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_IR_Filter };

	CFont ft1;
	COLORREF txcol_TVal,bkcol_TVal;
	void	IRFILTER_TEXT(LPCSTR lpcszString, ...);
	void	IRFILTERNUM_TEXT(LPCSTR lpcszString, ...);
	void	RESULT_TEXT(unsigned char col,LPCSTR lpcszString, ...);
	void	Initstat();
	void	Setup(CWnd* IN_pMomWnd);
private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnSetfocusEditIrfilterTxt();
	afx_msg void OnEnSetfocusEditIrfilterValue();
	afx_msg void OnEnSetfocusEditIrfilterResult();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
