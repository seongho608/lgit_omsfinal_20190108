#pragma once
#include "afxcmn.h"
#include "ETCUSER.H"
#include "afxwin.h"
// CRotate_Option 대화 상자입니다.

class CRotate_Option : public CDialog
{
	DECLARE_DYNAMIC(CRotate_Option)

public:
	CRotate_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CRotate_Option();
	enum { IDD = IDD_OPTION_ROTATE };
	
	CRectData A_RECT[5];
	CRectData T_RECT[5];
	CRectData A_Original[5];
	void Load_Original_pra();
	CRectData C_CHECKING[5];
	tResultVal Run(void);
	void Pic(CDC *cdc);
	void InitPrm();
	void FAIL_UPLOAD();
	BOOL	b_StopFail;
	double m_fVer_Degree, m_fHor_Degree;

	CRectData SC_RECT[5];	//Side Circle
	double	  MasterDegree;
	CRectData Standard_SC_RECT[5];
	double	ResultDegree;
	
	void GetSideCircleCoordinate(LPBYTE m_RGBIn);
	double GetDistance(int x1, int y1, int x2, int y2);

// 대화 상자 데이터입니다.
	void	SETLIST();
	void	SETLIST_TILT();

	void	Setup(CWnd* IN_pMomWnd);
	void    Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
	void	StatePrintf(LPCSTR lpcszString, ...);

	void	UploadList();
	void	UploadList_Tilt();
	void	Save_parameter();
	void	Save(int NUM);
	CString A_filename;
	int PosX,PosY;
	int InIndex;
	int iSavedItem, iSavedSubitem;
	int iSavedItem_T, iSavedSubitem_T;
	int iSavedSubitem2;
	int state;
	int EnterState;
	int NewItem,NewSubitem;
	CRect rect;
	void	AngleSum();
	void Change_DATA();
	void Change_DATA_TILT();

	void Automation_DATA_INPUT();
	double disL,disR,disT,disB;
	int TestMod;
	bool AutomationMod;
	BYTE	m_InR;
	BYTE	m_InG;
	BYTE	m_InB;
	bool MasterMod;
	int	m_Comp_L_MAX,m_Comp_R_MAX,m_Comp_T_MAX,m_Comp_B_MAX;
	int	m_Comp_L_MIN,m_Comp_R_MIN,m_Comp_T_MIN,m_Comp_B_MIN;
	void Load_parameter();

	void GetSideCircleOnManualMode(LPBYTE IN_RGB);

	bool AngleGen(LPBYTE IN_RGB,int NUM);
	bool AnglePic(CDC *cdc,int NUM);
	int	m_CAM_SIZE_WIDTH;
	int	m_CAM_SIZE_HEIGHT;

	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void InsertList();
	int InsertIndex;
	int Lot_InsertIndex;
	int ListItemNum;
	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();

	bool b_RunEn;
	bool b_OptionEn;
	int	StartCnt;

	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();
	//--------
	void	InitStat();
	void List_COLOR_Change();
	void List_COLOR_Change_TILT();
	bool ChangeCheck;
	bool ChangeCheck_T;
	int ChangeItem[5];
	int ChangeItem_T[5];
	int changecount;
	int changecount_T;

	double horipointY1; 
	double horipointY2;

	double horipointX1; 
	double horipointX2;

	double vetipointX1;
	double vetipointX2;

	double vetipointY1;
	double vetipointY2;
	
	double m_dAngleOffset;
	double m_dRotationAngle_min;
	double m_dRotationAngle_max;

	double m_dOffset;
	double m_dOffset_M;

	void	EditWheelIntSet(int nID,int Val);
	void	EditMinMaxIntSet(int nID,int* Val,int Min,int Max);
	bool WheelCheck;
	bool	Change_DATA_CHECK(bool FLAG);
	bool	Change_DATA_CHECK_T(bool FLAG);

	#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

//	int Lot_StartCnt;
//	int Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

	void	InitEVMS();
#pragma endregion

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	CString		str_model;
	CString		strTitle;
//	int m_Number;
	tINFO	m_INFO;

	tResultVal m_retval;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListCtrl A_DATALIST;
	int A_Total_PosX;
	int A_Total_PosY;
	int A_Dis_Width;
	int A_Dis_Height;
	int A_Total_Width;
	int A_Total_Height;
	int A_Thresold;
	afx_msg void OnBnClickedButtonsetanglezone();
	afx_msg void OnBnClickedButtonARectSave();
	afx_msg void OnNMClickListAngle(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
//	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonMasterA();
	CListCtrl m_AngleList;
	afx_msg void OnBnClickedCheckVer();
	afx_msg void OnBnClickedCheckHor();
	afx_msg void OnBnClickedCheckAuto();
	afx_msg void OnNMCustomdrawListAngle(NMHDR *pNMHDR, LRESULT *pResult);
//	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnNMDblclkListAngle(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusEditAMod();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedButtonALoad();
	BOOL TestModALL;
	BOOL TestModVer;
	BOOL TestModHor;
	afx_msg void OnBnClickedCheck1();
	int m_dRotationAngle;
	
	BOOL m_bVerRotationCheck;
	BOOL m_bHorRotationCheck;
	BOOL m_bRotationCheck; 
	BOOL m_bTiltCheck;


	afx_msg void OnEnChangeRotationAngle();
	
	double Vertical_GetRotationCheck(double pt_x1, double pt_y1, double pt_x2, double pt_y2);
	double Horizontal_GetRotationCheck(double pt_x1, double pt_y1, double pt_x2, double pt_y2);
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnBnClickedButtonPicsave();
	BOOL b_CenterValueView;
	BOOL b_DegreeValueView;
	BOOL b_RectView;
	BOOL b_SetZone;
	afx_msg void OnBnClickedCheckCenter();
	afx_msg void OnBnClickedCheckDegree();
	afx_msg void OnBnClickedCheckRect();
	CString str_minDegree;
	CString str_maxDegree;
	afx_msg void OnEnChangeRotationAngle2();
	afx_msg void OnEnChangeAnglePosx();
	afx_msg void OnEnChangeAnglePosy();
	afx_msg void OnEnChangeAngleDisW();
	afx_msg void OnEnChangeAngleDisH();
	afx_msg void OnEnChangeAngleWidth();
	afx_msg void OnEnChangeAngleHeight();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnEnChangeEditAMod();
	afx_msg void OnEnChangeEdit1();
	CString str_MasterDegree;
	afx_msg void OnBnClickedButtontest();
//	CListCtrl m_Lot_RotateList;
	CString OFFSET;
	CString OFFSET_M;
	afx_msg void OnBnClickedButtonSave3();
	afx_msg void OnEnChangeEditMaster();
	BOOL bOffsetType;
	afx_msg void OnBnClickedCheckType();
	CListCtrl A_TILTLIST;
	afx_msg void OnEnChangeEditTMod();
	afx_msg void OnEnKillfocusEditTMod();
	afx_msg void OnNMClickListTilt(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawListTilt(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListTilt(NMHDR *pNMHDR, LRESULT *pResult);
	CString str_Angle_Offset;
	afx_msg void OnEnChangeRotationAngleOffset();
};


void CModel_Create(CRotate_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO);
void CModel_Delete(CRotate_Option **pWnd);
