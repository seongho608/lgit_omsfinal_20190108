// NewLot.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "NewLot.h"


// CNewLot 대화 상자입니다.

IMPLEMENT_DYNAMIC(CNewLot, CDialog)

CNewLot::CNewLot(CWnd* pParent /*=NULL*/)
	: CDialog(CNewLot::IDD, pParent)
	, m_LotOperator(_T(""))
	, m_LotInfo(_T(""))
	, SerialStartNum(0)
{
	m_combo_sel=0;
	b_ModelApply = FALSE;
}

CNewLot::~CNewLot()
{
}

void CNewLot::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_LOTNAME, m_LotInfo);
}


BEGIN_MESSAGE_MAP(CNewLot, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO_MODEL_LOT, &CNewLot::OnCbnSelchangeComboModelLot)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDOK, &CNewLot::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CNewLot::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_APPLY, &CNewLot::OnBnClickedButtonApply)
	ON_BN_CLICKED(IDC_BUTTON_modify, &CNewLot::OnBnClickedButtonmodify)
	ON_WM_CTLCOLOR()
	ON_EN_SETFOCUS(IDC_EDIT_MODEL_NAME, &CNewLot::OnEnSetfocusEditModelName)
END_MESSAGE_MAP()


// CNewLot 메시지 처리기입니다.
void CNewLot::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}
BOOL CNewLot::OnInitDialog()
{
	CDialog::OnInitDialog();

	pModelCombo = (CComboBox *)GetDlgItem(IDC_COMBO_MODEL_LOT);//다이알로그

	Font_Size_Change(IDC_EDIT_MODEL_NAME,&ft,100,25);
	Font_Size_Change(IDC_EDIT_LOTNAME,&ft1,100,18);
	//GetDlgItem(IDC_EDIT_BARCODE_VAL)->SetFont(&font4);

	

	((CImageTesterDlg  *)m_pMomWnd)->MainModelSel();

	backmodelcombo = ((CImageTesterDlg  *)m_pMomWnd)->m_combo_sel;



	MODELNAME = ((CImageTesterDlg  *)m_pMomWnd)->m_modelName;

	SetTimer(120,100,NULL); //InitProgram을 불러들이는데 쓰인다.

	Model_Select_Enable(0,MODELNAME);

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitEVMS();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CNewLot::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		b_ModelApply = TRUE;
		pModelCombo->EnableWindow(FALSE);
		((CButton *)GetDlgItem(IDC_BUTTON_APPLY))->EnableWindow(FALSE);
		((CButton *)GetDlgItem(IDC_BUTTON_modify))->EnableWindow(TRUE);
		Model_Select_Enable(1,MODELNAME);

	}
}

void CNewLot::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
void CNewLot::OnCbnSelchangeComboModelLot()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str ="";
	pModelCombo->GetLBText(pModelCombo->GetCurSel(),str);
	MODELNAME = str;
	Model_Select_Enable(0,MODELNAME);
	//((CMULTIDOWNLOAD_6CHDlg *)m_pMomWnd)->ModelFileSearch(str);
}

void CNewLot::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == 120){
		UpdateData(TRUE);
		if((pModelCombo->GetCurSel() != -1)&&(m_LotInfo != "")&&(b_ModelApply == TRUE)){
			((CButton *)GetDlgItem(IDOK))->EnableWindow(1);
		}else{
			((CButton *)GetDlgItem(IDOK))->EnableWindow(0);
		}
	}
}

void CNewLot::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//	MODELNAME = str;
	pModelCombo->GetLBText(pModelCombo->GetCurSel(),MODELNAME);
	((CImageTesterDlg  *)m_pMomWnd)->LOT_NUMBER = m_LotInfo;
	((CImageTesterDlg  *)m_pMomWnd)->m_pMainCombo->SetCurSel(pModelCombo->GetCurSel());
	((CImageTesterDlg  *)m_pMomWnd)->OnCbnSelchangeModelName();

	((CImageTesterDlg  *)m_pMomWnd)->LOT_RESET_LIST(0);
	if(((CImageTesterDlg  *)m_pMomWnd)->m_pResLotWnd != NULL)
		((CImageTesterDlg  *)m_pMomWnd)->m_pResLotWnd->LOTID_VAL(m_LotInfo);
	((CImageTesterDlg  *)m_pMomWnd)->LOT_CREATE_Folder(m_LotInfo,3);

	WritePrivateProfileString("LOT_NUMBER","NAME",m_LotInfo,((CImageTesterDlg  *)m_pMomWnd)->modelLotPath);

	((CImageTesterDlg  *)m_pMomWnd)->LotMod=1;
	OnOK();

}

void CNewLot::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->m_pMainCombo->SetCurSel(backmodelcombo);
	((CImageTesterDlg  *)m_pMomWnd)->OnCbnSelchangeModelName();
	((CImageTesterDlg  *)m_pMomWnd)->SETTING_USER_Switching();
	KillTimer(120);
	OnCancel();
}

void CNewLot::OnBnClickedButtonApply()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CPopUP2 subDlg;
	subDlg.Warning_TEXT = "  [  "+ MODELNAME +"  ] \r\n\r\n 모델을 적용하시겠습니까?";
	if(subDlg.DoModal() == IDOK){
		b_ModelApply = TRUE;
		pModelCombo->EnableWindow(FALSE);
		((CButton *)GetDlgItem(IDC_BUTTON_APPLY))->EnableWindow(FALSE);
		((CButton *)GetDlgItem(IDC_BUTTON_modify))->EnableWindow(TRUE);
		Model_Select_Enable(1,MODELNAME);

	}else{
		b_ModelApply = FALSE;
		pModelCombo->EnableWindow(TRUE);
		((CButton *)GetDlgItem(IDC_BUTTON_APPLY))->EnableWindow(TRUE);
		((CButton *)GetDlgItem(IDC_BUTTON_modify))->EnableWindow(FALSE);
		Model_Select_Enable(0,MODELNAME);

	}
	

}

void CNewLot::OnBnClickedButtonmodify()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	b_ModelApply = FALSE;
	pModelCombo->EnableWindow(TRUE);
	((CButton *)GetDlgItem(IDC_BUTTON_APPLY))->EnableWindow(TRUE);
	((CButton *)GetDlgItem(IDC_BUTTON_modify))->EnableWindow(FALSE);
	Model_Select_Enable(0,MODELNAME);

}

HBRUSH CNewLot::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_MODEL_NAME){
        pDC->SetTextColor(txcol);
		pDC->SetBkColor(bkcol);
    }
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CNewLot::Model_Select_Enable(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0){
		txcol = RGB(0, 0, 0);
		bkcol = RGB(255, 255, 0);
	}else if(col == 1){
		txcol = RGB(255, 255, 255);
		bkcol = RGB(86, 86, 86);
	}
	
	((CEdit *)GetDlgItem(IDC_EDIT_MODEL_NAME))->SetWindowText(lpcszString);
}

void CNewLot::OnEnSetfocusEditModelName()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	GotoDlgCtrl(((CButton *)GetDlgItem(IDC_BUTTON_APPLY)));
}
