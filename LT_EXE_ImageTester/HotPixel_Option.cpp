
#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "HotPixel_Option.h"

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
extern WORD	m_GRAYScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];
extern BYTE	m_OverlayArea[CAM_IMAGE_WIDTH][CAM_IMAGE_HEIGHT];

IMPLEMENT_DYNAMIC(CHotPixel_Option, CDialog)

CHotPixel_Option::CHotPixel_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CHotPixel_Option::IDD, pParent)
{
	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;
	StartCnt=0;

	m_dThreshold = 0.0;
	m_dLightCurr = 0.0;
	m_iLightDelay = 1000;
	m_iPassCount = 0;
	m_iClusterCount = 0;
	m_iFailCount = 0;

	b_StopFail = FALSE;
	m_b_FailCheck = FALSE;
}

CHotPixel_Option::~CHotPixel_Option()
{
}

void CHotPixel_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_WORK, m_WorkList);
	DDX_Control(pDX, IDC_LIST_LOT_WORKLIST, m_Lot_WorkList);
}


BEGIN_MESSAGE_MAP(CHotPixel_Option, CDialog)
	ON_WM_SHOWWINDOW()	
	ON_WM_MOUSEWHEEL()	
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_HOT_TEST, &CHotPixel_Option::OnBnClickedButtontest)
	ON_BN_CLICKED(IDC_BUTTON_HOT_SAVE, &CHotPixel_Option::OnBnClickedButtonSave)
END_MESSAGE_MAP()

// CHotPixel_Option 메시지 처리기입니다.

void CHotPixel_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CHotPixel_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO; 

	str_model.Empty();
	str_model.Format(INFO.NAME);;
}

BOOL CHotPixel_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_CAM_SIZE_WIDTH  = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;

	HotPixel_filename =((CImageTesterDlg  *)m_pMomWnd)->modelfile_Name;

	UpdateData(TRUE);
	Load_parameter();
	UpdateData(FALSE);
	Set_List(&m_WorkList);
	LOT_Set_List(&m_Lot_WorkList);

	InitEVMS();

	return TRUE;  // return TRUE unless you set the focus to a control
}

void CHotPixel_Option::InitEVMS()
{
}


bool CHotPixel_Option::HotPixelPic(CDC *cdc,int NUM)
{
	CString TEXTDATA = "";

	::SetBkMode(cdc->m_hDC, TRANSPARENT);

	if (m_b_FailCheck == TRUE)
	{
		if (TRUE == m_bCluster[NUM])
		{
			cdc->SetPixel(m_ptCenter[NUM].x, m_ptCenter[NUM].y, RED_COLOR);
			m_GRAYScanbuf[CAM_IMAGE_WIDTH * m_ptCenter[NUM].y + m_ptCenter[NUM].x] = RED_COLOR;
		}
		else
		{
			cdc->SetPixel(m_ptCenter[NUM].x, m_ptCenter[NUM].y, YELLOW_COLOR);
			m_GRAYScanbuf[CAM_IMAGE_WIDTH * m_ptCenter[NUM].y + m_ptCenter[NUM].x] = 0;
		}
	}

	return 1;
}

void CHotPixel_Option::Pic(CDC *cdc)
{
	for (int lop = 0; lop < m_iFailCount; lop++)
	{
		HotPixelPic(cdc, lop);
	}
}

void CHotPixel_Option::InitPrm()
{
	Load_parameter();
	UpdateData(FALSE);
}


tResultVal CHotPixel_Option::Run()
{	
	CString str="";
	b_StopFail = FALSE;

	m_b_FailCheck = FALSE;

	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE)
	{
		InsertList();
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_pResHotPixelOptWnd->Initstat();
	
	int camcnt = 0;
	tResultVal retval = { 0, };

	// 이물광원 밝기 변경

	((CImageTesterDlg *)m_pMomWnd)->Control_Current_SLOT(3, m_dLightCurr);

	DoEvents(m_iLightDelay);

	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = 1;
	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;

	for (int i = 0; i < 10; i++)
	{
		if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0 && ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0)
		{
			break;
		}
		else
		{
			DoEvents(50);
		}
	}
				
	BOOL bResult = HotPixelGen_16bit(m_GRAYScanbuf);

	//---------------------------
	retval.m_ID = 0x17;
	retval.ValString.Empty();

	CString stateDATA = "Hot Pixel 검사_ ";
	str.Format("%d", m_iClusterCount);

	if (bResult == TRUE)
	{
		m_Success = TRUE;
		m_WorkList.SetItemText(InsertIndex, 4, str);
		m_WorkList.SetItemText(InsertIndex, 5, "PASS");

		retval.m_Success = TRUE;
		Str_Mes[0] = str;
		Str_Mes[1] = "1";
		if (((CImageTesterDlg *)m_pMomWnd)->LotMod != 1)
		{
			((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_HOTPIXEL, "PASS");
		}
		stateDATA += "_ PASS";
		retval.ValString.Format("OK");
	}
	else
	{
		m_Success = FALSE;
		m_WorkList.SetItemText(InsertIndex, 4, str);
		m_WorkList.SetItemText(InsertIndex, 5, "FAIL");

		retval.m_Success = FALSE;
		Str_Mes[0] = str;
		Str_Mes[1] = "0";
		if (((CImageTesterDlg *)m_pMomWnd)->LotMod != 1)
		{
			((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_HOTPIXEL, "FAIL");
		}
		stateDATA += "_ FAIL";
		retval.ValString.Format("FAIL");

	}

	((CImageTesterDlg  *)m_pMomWnd)->StatePrintf(stateDATA);
		
	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE)
	{
		StartCnt++;
	}
		
	return retval;
}	

void CHotPixel_Option::FAIL_UPLOAD()
{
 	if (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)
	{
		InsertList();

		m_WorkList.SetItemText(InsertIndex, 4, "X");//result
		m_WorkList.SetItemText(InsertIndex, 5, "FAIL");//result
		m_WorkList.SetItemText(InsertIndex, 6, "STOP");//비고

		Str_Mes[0] = "FAIL";
		Str_Mes[1] = "0";

		((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_HOTPIXEL, "STOP");
		StartCnt++;
		b_StopFail = TRUE;
 	}
}

void CModel_Create(CHotPixel_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit *pEdit,tINFO INFO)
{
	CRect OptionRect;
	if(pTabWnd != NULL){
		((CHotPixel_Option *)*pWnd) = new CHotPixel_Option(pMomWnd);
		((CHotPixel_Option *)*pWnd)->Setup(pMomWnd,pEdit,INFO);
		((CHotPixel_Option *)*pWnd)->Create(((CHotPixel_Option *)*pWnd)->IDD,pTabWnd);//&m_CtrTab);
		((CHotPixel_Option *)*pWnd)->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		((CHotPixel_Option *)*pWnd)->MoveWindow(20,40,OptionRect.Width(),OptionRect.Height());
	}
}

void CModel_Delete(CHotPixel_Option **pWnd)
{
	if(((CHotPixel_Option *)*pWnd) != NULL)
	{
		delete ((CHotPixel_Option *)*pWnd);
		((CHotPixel_Option *)*pWnd) = NULL;	
	}
}

void CHotPixel_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN)
	{
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

void CHotPixel_Option::Load_parameter()
{
	CFileFind filefind;
	CString str = "";
	CString strTitle = "";
	strTitle.Empty();
	strTitle = "HOT_INIT";

	m_dThreshold  = GetPrivateProfileDouble(str_model, strTitle + "Threshold", 0, HotPixel_filename);
	m_dLightCurr  = GetPrivateProfileDouble(str_model, strTitle + "LightCurr", 0, HotPixel_filename);
	m_iPassCount  = GetPrivateProfileInt(str_model, strTitle + "PassCnt", 50, HotPixel_filename);
	m_iLightDelay = GetPrivateProfileInt(str_model, strTitle + "LightDelay", 1000, HotPixel_filename);
	
	CString strData;
	strData.Format(_T("%6.2f"), m_dThreshold);
	((CEdit *)GetDlgItem(IDC_HOT_Thresold))->SetWindowText(strData);

	strData.Format(_T("%.f"), m_dLightCurr);
	((CEdit *)GetDlgItem(IDC_HOT_Light_Curr))->SetWindowText(strData);

	strData.Format(_T("%d"), m_iPassCount);
	((CEdit *)GetDlgItem(IDC_HOT_PASS_CNT))->SetWindowText(strData);

	strData.Format(_T("%d"), m_iLightDelay);
	((CEdit *)GetDlgItem(IDC_HOT_Light_Delay))->SetWindowText(strData);

	Save_parameter();
}

void CHotPixel_Option::Save_parameter()
{
	CString str = "";
	CString strTitle = "";
	str.Empty();
	str.Format("%d", m_INFO.ID);
	WritePrivateProfileString(str_model, "ID", str, HotPixel_filename);
	str.Empty();
	str.Format("%s", m_INFO.MODE);
	WritePrivateProfileString(str_model, "MODE", str, HotPixel_filename);
	str.Empty();
	str.Format("%s", m_INFO.NAME);
	WritePrivateProfileString(str_model, "NAME", str, HotPixel_filename);

	strTitle.Empty();
	strTitle = "HOT_INIT";

	str.Empty();
	str.Format("%6.2f", m_dThreshold);
	WritePrivateProfileString(str_model, strTitle + "Threshold", str, HotPixel_filename);

	str.Empty();
	str.Format("%.f", m_dLightCurr);
	WritePrivateProfileString(str_model, strTitle + "LightCurr", str, HotPixel_filename);

	str.Empty();
	str.Format("%d", m_iPassCount);
	WritePrivateProfileString(str_model, strTitle + "PassCnt", str, HotPixel_filename);

	str.Empty();
	str.Format("%d", m_iLightDelay);
	WritePrivateProfileString(str_model, strTitle + "LightDelay", str, HotPixel_filename);
}
void CHotPixel_Option::Save(int NUM)
{
	WritePrivateProfileString(str_model,NULL,"",HotPixel_filename);

	if(NUM != -1)
	{
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

//---------------------------------------------------------------------WORKLIST
void CHotPixel_Option::InsertList()
{
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;
	
	InsertIndex = m_WorkList.InsertItem(StartCnt,"",0);
	
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_WorkList.SetItemText(InsertIndex,0,strCnt);
	
	m_WorkList.SetItemText(InsertIndex,1,((CImageTesterDlg  *)m_pMomWnd)->m_modelName);
	m_WorkList.SetItemText(InsertIndex,2,((CImageTesterDlg  *)m_pMomWnd)->strDay+"");
	m_WorkList.SetItemText(InsertIndex,3,((CImageTesterDlg  *)m_pMomWnd)->strTime+"");
}

void CHotPixel_Option::Set_List(CListCtrl *List)
{

	while(List->DeleteColumn(0));
	((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);

	int LISTNUM = 4;

	List->InsertColumn(LISTNUM++, "VAL", LVCFMT_CENTER, 80);
	List->InsertColumn(LISTNUM++, "RESULT", LVCFMT_CENTER, 80);
	List->InsertColumn(LISTNUM++, "비고", LVCFMT_CENTER, 80);

	ListItemNum = LISTNUM;
	Copy_List(&m_WorkList, ((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList, ListItemNum);
}

CString CHotPixel_Option::Mes_Result()
{
	CString sz[9];
	int	nResult[9];

	// 제일 마지막 항목을 추가하자
	int nSel = m_WorkList.GetItemCount()-1;

	// C-DB
	CString str;
	CString strData;

	
	strData.Empty();
	m_szMesResult = _T("");

	if (b_StopFail == FALSE){
		m_szMesResult.Format(_T("%s:%s"), Str_Mes[0], Str_Mes[1]);
		m_szMesResult.Replace(" ", "");
	}
	else{
		m_szMesResult.Format(_T(":1"));
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_stNewMesData[NewMES_HotPixel] = m_szMesResult;

	return m_szMesResult;
}

void CHotPixel_Option::EXCEL_SAVE()
{
	CString Item[1] = { "VAL" };
	CString Data = "";
	CString str = "";
	str = "***********************Hot Pixel***********************\n";
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("HotPixel", Item, 1, &m_WorkList, Data);
}

bool CHotPixel_Option::EXCEL_UPLOAD()
{
	m_WorkList.DeleteAllItems();
	
	if(((CImageTesterDlg  *)m_pMomWnd)->EXCEL_UPLOAD("HotPixel",&m_WorkList,1,&StartCnt)== TRUE)
	{
		return TRUE;
	}
	else
	{
		StartCnt = 0;
		return FALSE;
	}

	return TRUE;
}


BOOL CHotPixel_Option::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}

		if (pMsg->wParam == VK_RETURN)
		{
			return TRUE;
		}
		
	}

	return CDialog::PreTranslateMessage(pMsg);
}

	
void CHotPixel_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if(bShow == TRUE)
	{
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;
	}
	else
	{
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = -1;
	}
}

void CHotPixel_Option::UploadList()
{
	
}

void CHotPixel_Option::OnTimer(UINT_PTR nIDEvent)
{
	CDialog::OnTimer(nIDEvent);
}


//=============================================================================
// Method		: HotPixelGen_16bit
// Access		: public  
// Returns		: bool
// Parameter	: WORD * GRAYScanBuf
// Qualifier	:
// Last Update	: 2018/4/24 - 20:07
// Desc.		:
//=============================================================================
bool CHotPixel_Option::HotPixelGen_16bit(WORD *GRAYScanBuf)
{
	CTI_HotPixel HotPixel;

	m_iClusterCount = 0;

	for (UINT nIdx = 0; nIdx < 100; nIdx++)
	{
		m_sPointX[nIdx] = 0;
		m_sPointY[nIdx] = 0;
	}
	m_iFailCount = 0;
	HotPixel.HotPixel_Test(m_dThreshold, m_iFailCount, m_sPointX, m_sPointY, m_fConcen, GRAYScanBuf);

	// 덩어리 판정 
	HotPixel_Cluster();

	CString szValue;

	szValue.Format("%d", m_iClusterCount);

	if (m_iClusterCount <= m_iPassCount)
	{
		((CImageTesterDlg  *)m_pMomWnd)->m_pResHotPixelOptWnd->RESULT_TEXT(1, "PASS");
		((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->TestState(0, "PASS");
		m_b_FailCheck	= FALSE;
		m_Success		= TRUE;
	}
	else
	{
		((CImageTesterDlg  *)m_pMomWnd)->m_pResHotPixelOptWnd->RESULT_TEXT(2, "FAIL");
		((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->TestState(1, "FAIL");
		m_b_FailCheck	= TRUE;
		m_Success		= FALSE;
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_pResHotPixelOptWnd->HotPixelNUM_TEXT(szValue);

	return m_Success;
}

//=============================================================================
// Method		: HotPixel_Cluster
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/8/16 - 17:34
// Desc.		:
//=============================================================================
void CHotPixel_Option::HotPixel_Cluster()
{	
	// (0,0) ~ (3,3) 을 제외 한 불량 수량
	UINT nFailCnt = 0;

	m_iClusterCount = 0;

	for (UINT nCnt = 0; nCnt < m_iFailCount; nCnt++)
	{
		if (m_sPointX[nCnt] <= 3 && m_sPointY[nCnt] <= 3)
		{
			// (0,0) ~ (3,3) 까지의 불량 데이터 무시
		}
		else
		{
			m_ptCenter[nFailCnt].x = m_sPointX[nCnt];
			m_ptCenter[nFailCnt].y = m_sPointY[nCnt];

			nFailCnt++;
		}
	}

	m_iFailCount = nFailCnt;

	/* Cluster */
	IplImage* PixelClusterIMG = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage* CalImg = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);

	// 0으로 초기화
	cvSetZero(PixelClusterIMG);

	//DeadPixel 점 찍어주기
	for (int i = 0; i < m_iFailCount; i++)
	{
		cvLine(PixelClusterIMG, cvPoint(m_ptCenter[i].x, m_ptCenter[i].y), cvPoint(m_ptCenter[i].x, m_ptCenter[i].y), CV_RGB(255, 255, 255), 1);
	}

	cvCopy(PixelClusterIMG, CalImg);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;

	cvFindContours(CalImg, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	CvRect* Cluster = new CvRect[100];

	CvRect rect;
	// 크기가 2 Pixel 이상이 되는 DeadPixel들을 [Cluster 뭉치]로 저장하기
	int Cluster_Count = 0;
	for (; contour != 0; contour = contour->h_next)
	{
		rect = cvBoundingRect(contour, 1);

		if (!(rect.width == 1 && rect.height == 1))
		{
			//cvRectangle(PixelClusterIMG, cvPoint(rect.x - 1, rect.y - 1), cvPoint(rect.x + rect.width + 1, rect.y + rect.height + 1),CV_RGB(180,180,180),1);
			Cluster[Cluster_Count] = rect;
			Cluster_Count++;
		}
	}

	// BOOL 변수에 클러스터로 추출된 녀석에 인식표 붙히기
	for (int i = 0; i < m_iFailCount; i++)
	{
		BOOL bClusterFlag = FALSE;

		// DeadPixel이 [Cluster 뭉치]들 중에 포함이 됬는지 확인하고 깃발 올리기
		for (int _c = 0; _c < Cluster_Count; _c++)
		{
			if (m_ptCenter[i].x >= Cluster[_c].x && m_ptCenter[i].y >= Cluster[_c].y && m_ptCenter[i].x < Cluster[_c].x + Cluster[_c].width && m_ptCenter[i].y < Cluster[_c].y + Cluster[_c].height)
			{
				bClusterFlag = TRUE;
				break;
			}
		}

		// 깃발이 올라가면 해당 순번의 DeadPixel은 [Cluster 이름표] 붙히기
		if (bClusterFlag == TRUE)
		{
			m_bCluster[i] = TRUE;
		}
		else
		{
			m_bCluster[i] = FALSE;
		}
	}

	m_iClusterCount = Cluster_Count;

	delete[]Cluster;
	cvReleaseImage(&PixelClusterIMG);
	cvReleaseImage(&CalImg);
}

//=============================================================================
// Method		: OnBnClickedButtontest
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/8/16 - 17:34
// Desc.		:
//=============================================================================
void CHotPixel_Option::OnBnClickedButtontest()
{
	((CButton *)GetDlgItem(IDC_BUTTON_HOT_TEST))->EnableWindow(0);
	((CImageTesterDlg  *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	((CButton *)GetDlgItem(IDC_BUTTON_HOT_TEST))->EnableWindow(1);
}

#pragma region LOT관련 함수
void CHotPixel_Option::LOT_Set_List(CListCtrl *List){

	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0, "NO", LVCFMT_CENTER, 30);
	List->InsertColumn(1, "모델명", LVCFMT_CENTER, 100);
	List->InsertColumn(2, "LOT 명", LVCFMT_CENTER, 80);
	List->InsertColumn(3, "Start DATE", LVCFMT_CENTER, 80);
	List->InsertColumn(4, "Start TIME", LVCFMT_CENTER, 80);
	List->InsertColumn(5, "VAL", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "result", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "비고", LVCFMT_CENTER, 80);

	Lot_InsertNum = 8;
	Copy_List(&m_Lot_WorkList, ((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList, Lot_InsertNum);
}

void CHotPixel_Option::LOT_InsertDataList()
{
	CString strCnt="";

	int Index = m_Lot_WorkList.InsertItem(Lot_StartCnt, "", 0);
	Lot_InsertIndex = m_Lot_WorkList.InsertItem(Lot_StartCnt, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Lot_InsertIndex + 1);
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 0, strCnt);

	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 1, ((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 2, ((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);////////LOT 이름
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 3, ((CImageTesterDlg *)m_pMomWnd)->strDay + "");
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 4, ((CImageTesterDlg *)m_pMomWnd)->strTime + "");

	int CopyIndex = m_WorkList.GetItemCount() - 1;

	int count = 0;
	for (int t = 0; t < Lot_InsertNum; t++){
		if ((t != 2) && (t != 0)){
			m_Lot_WorkList.SetItemText(Index, t, m_WorkList.GetItemText(CopyIndex, count));
			count++;

		}
		else if (t == 0){
			count++;
		}
		else{
			m_Lot_WorkList.SetItemText(Index, t, ((CImageTesterDlg  *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;
}

void CHotPixel_Option::LOT_EXCEL_SAVE()
{
	CString Item[1] = { "VAL" };
	CString Data = "";
	CString str = "";
	str = "***********************HotPixel***********************\n";
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("HotPixel", Item, 1, &m_Lot_WorkList, Data);

}

bool CHotPixel_Option::LOT_EXCEL_UPLOAD()
{
	m_Lot_WorkList.DeleteAllItems();
	if(((CImageTesterDlg  *)m_pMomWnd)->LOT_EXCEL_UPLOAD("HotPixel",&m_Lot_WorkList,1,&Lot_StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}
 
#pragma endregion 

void CHotPixel_Option::OnBnClickedButtonSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strData;
	((CEdit *)GetDlgItem(IDC_HOT_Thresold))->GetWindowText(strData);
	m_dThreshold = _ttof(strData);

	((CEdit *)GetDlgItem(IDC_HOT_Light_Curr))->GetWindowText(strData);
	m_dLightCurr = _ttof(strData);

	((CEdit *)GetDlgItem(IDC_HOT_PASS_CNT))->GetWindowText(strData);
	m_iPassCount = _ttoi(strData);

	((CEdit *)GetDlgItem(IDC_HOT_Light_Delay))->GetWindowText(strData);
	m_iLightDelay = _ttoi(strData);

	strData.Format(_T("%.2f"), m_dThreshold);
	((CEdit *)GetDlgItem(IDC_HOT_Thresold))->SetWindowText(strData);

	strData.Format(_T("%.f"), m_dLightCurr);
	((CEdit *)GetDlgItem(IDC_HOT_Light_Curr))->SetWindowText(strData);

	strData.Format(_T("%d"), m_iPassCount);
	((CEdit *)GetDlgItem(IDC_HOT_PASS_CNT))->SetWindowText(strData);

	strData.Format(_T("%d"), m_iLightDelay);
	((CEdit *)GetDlgItem(IDC_HOT_Light_Delay))->SetWindowText(strData);

	Save_parameter();
}
