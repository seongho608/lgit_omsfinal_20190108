// NTSC_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "NTSC_ResultView.h"


// CNTSC_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CNTSC_ResultView, CDialog)

CNTSC_ResultView::CNTSC_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CNTSC_ResultView::IDD, pParent)
{

}

CNTSC_ResultView::~CNTSC_ResultView()
{
}

void CNTSC_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CNTSC_ResultView, CDialog)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CNTSC_ResultView 메시지 처리기입니다.

void CNTSC_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CNTSC_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
BOOL CNTSC_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_EDIT_VPPTXT,&ft1,40,20);
	GetDlgItem(IDC_EDIT_VPPNUM)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_VPPRESULT)->SetFont(&ft1);	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	GetDlgItem(IDC_EDIT_SYNCTXT)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_SYNCNUM)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_SYNCRESULT)->SetFont(&ft1);	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Initstat();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void	CNTSC_ResultView::Initstat(){
	VPP_RESULT_TEXT(0,"STAND BY");
	

	VPPNUM_TEXT("");
	VPP_TEXT("VPP");

	SYNC_RESULT_TEXT(0,"STAND BY");
	

	SYNCNUM_TEXT("");
	SYNC_TEXT("SYNC");
}
HBRUSH CNTSC_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VPPTXT){
			pDC->SetTextColor(RGB(255, 255, 255));

			pDC->SetBkColor(RGB(86,86,86));
	}


	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VPPNUM){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255,255,255));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VPPRESULT){
		pDC->SetTextColor(txcol_TVal);
		pDC->SetBkColor(bkcol_TVal);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_SYNCTXT){
			pDC->SetTextColor(RGB(255, 255, 255));

			pDC->SetBkColor(RGB(86,86,86));
	}


	if(pWnd->GetDlgCtrlID() == IDC_EDIT_SYNCNUM){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255,255,255));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_SYNCRESULT){
		pDC->SetTextColor(txcol_TVal2);
		pDC->SetBkColor(bkcol_TVal2);
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}
void CNTSC_ResultView::VPP_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_VPPTXT))->SetWindowText(lpcszString);
}
void CNTSC_ResultView::VPPNUM_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_VPPNUM))->SetWindowText(lpcszString);
}

void CNTSC_ResultView::VPP_RESULT_TEXT(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0){//대기
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18,  69,171);
	}else if(col == 2){//실패
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_VPPRESULT))->SetWindowText(lpcszString);
}
void CNTSC_ResultView::SYNC_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_SYNCTXT))->SetWindowText(lpcszString);
}
void CNTSC_ResultView::SYNCNUM_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_SYNCNUM))->SetWindowText(lpcszString);
}

void CNTSC_ResultView::SYNC_RESULT_TEXT(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0){//대기
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(18,  69,171);
	}else if(col == 2){//실패
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal2 = RGB(0, 0, 0);
		bkcol_TVal2 = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_SYNCRESULT))->SetWindowText(lpcszString);
}
BOOL CNTSC_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}
