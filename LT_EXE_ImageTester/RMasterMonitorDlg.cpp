// RMasterMonitorDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "RMasterMonitorDlg.h"


extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4]; 

// CRMasterMonitorDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CRMasterMonitorDlg, CDialog)

CRMasterMonitorDlg::CRMasterMonitorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRMasterMonitorDlg::IDD, pParent)
{

}

CRMasterMonitorDlg::~CRMasterMonitorDlg()
{

}

void CRMasterMonitorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CAM_FRAME, m_stCenterDisplay);
}


BEGIN_MESSAGE_MAP(CRMasterMonitorDlg, CDialog)
//	ON_BN_CLICKED(IDC_BTN_CHARTMOVE, &CRMasterMonitorDlg::OnBnClickedBtnChartmove)
//ON_BN_CLICKED(IDC_BTN_POWER_ON2, &CRMasterMonitorDlg::OnBnClickedBtnPowerOn2)
ON_BN_CLICKED(IDC_BTN_POWER_ON2, &CRMasterMonitorDlg::OnBnClickedBtnPowerOn2)
ON_BN_CLICKED(IDC_BTN_POWER_OFF, &CRMasterMonitorDlg::OnBnClickedBtnPowerOff)
ON_BN_CLICKED(IDC_BTN_OVERLAYON, &CRMasterMonitorDlg::OnBnClickedBtnOverlayon)
ON_BN_CLICKED(IDC_BTN_OVERLAYOFF, &CRMasterMonitorDlg::OnBnClickedBtnOverlayoff)
ON_BN_CLICKED(IDC_BTN_AUTOSET, &CRMasterMonitorDlg::OnBnClickedBtnAutoset)
ON_EN_CHANGE(IDC_EDIT_MOFFSETX, &CRMasterMonitorDlg::OnEnChangeEditMoffsetx)
ON_EN_CHANGE(IDC_EDIT_MOFFSETY, &CRMasterMonitorDlg::OnEnChangeEditMoffsety)
ON_BN_CLICKED(IDC_BTN_POSITION_STAND, &CRMasterMonitorDlg::OnBnClickedBtnPositionStand)
ON_BN_CLICKED(IDC_BTN_POSITION_MOVE, &CRMasterMonitorDlg::OnBnClickedBtnPositionMove)
ON_BN_CLICKED(IDC_BTN_POSITION_SET, &CRMasterMonitorDlg::OnBnClickedBtnPositionSet)
ON_EN_CHANGE(IDC_POSITION_VALX, &CRMasterMonitorDlg::OnEnChangePositionValx)
ON_EN_CHANGE(IDC_POSITION_VALY, &CRMasterMonitorDlg::OnEnChangePositionValy)
ON_BN_CLICKED(IDC_BTN_CENTERDETECT, &CRMasterMonitorDlg::OnBnClickedBtnCenterdetect)
ON_WM_CTLCOLOR()
ON_EN_CHANGE(IDC_EDIT_PIXELRATE, &CRMasterMonitorDlg::OnEnChangeEditPixelrate)
ON_BN_CLICKED(IDC_BUTTON_USER_EXIT, &CRMasterMonitorDlg::OnBnClickedButtonUserExit)
ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// CRMasterMonitorDlg 메시지 처리기입니다.
void CRMasterMonitorDlg::Focus_move_start()
{
	GotoDlgCtrl(((CStatic *)GetDlgItem(IDC_STATIC)));
}

void CRMasterMonitorDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CRMasterMonitorDlg::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT)
{
	pTStat		= pTEDIT;
	Setup(IN_pMomWnd);
}

BOOL CRMasterMonitorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	pDirectModeCombo = (CComboBox *)GetDlgItem(IDC_OPT_WRMODE);
	Font_Size_Change(IDC_STATE_MASTERINIT,&font1,500,23);
	InitSet();
	b_Running = FALSE;
	UpdateMaState();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CRMasterMonitorDlg::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

void CRMasterMonitorDlg::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight;
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}


void CRMasterMonitorDlg::SubBmpPic(UINT nIDResource)//나중에 전역변수로 간략화 할것임
{	
	CPen  my_Pan,my_Pan2,*old_pan,*old_pan2;
	CFont m_font,m_font2;
	CString TEXTDATA ="";

	CRect rect;
	CDC *pcDC = m_stCenterDisplay.GetDC();
	m_stCenterDisplay.GetClientRect(&rect);
			
	CDC MemDC;
	CBitmap image;
	BITMAP bmpinfo;

	MemDC.CreateCompatibleDC(pcDC);
	image.LoadBitmap(nIDResource);
	image.GetBitmap(&bmpinfo);
	CBitmap*pOldBitmap = (CBitmap*)MemDC.SelectObject(&image);


	::SetBkMode(MemDC.m_hDC,TRANSPARENT);	
	my_Pan.CreatePen(PS_SOLID,2,BLACK_COLOR);
	old_pan = MemDC.SelectObject(&my_Pan);
		
	if(ClickNUM == TRUE){
		m_font2.CreatePointFont(700,"Arial");
		MemDC.SelectObject(&m_font2);
		MemDC.SetTextAlign(TA_CENTER|TA_BASELINE);
		MemDC.SetTextColor(BLUE_COLOR);
		TEXTDATA.Format("SUCCESS");
		MemDC.TextOut(360,300,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
	}

	MemDC.SetTextColor(BLACK_COLOR);
	m_font.CreatePointFont(200,"Arial");  
	MemDC.SelectObject(&m_font);
	MemDC.SetTextAlign(TA_CENTER|TA_BASELINE);
	TEXTDATA.Format("MASTER MODE");
	MemDC.TextOut(580,40,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	pcDC->SetStretchBltMode(COLORONCOLOR);
	pcDC->StretchBlt(0, 0, rect.Width(), rect.Height(), &MemDC, 0, 0, bmpinfo.bmWidth, bmpinfo.bmHeight, SRCCOPY);
	MemDC.SelectObject(pOldBitmap);
	image.DeleteObject();
	MemDC.DeleteDC();
	ReleaseDC(pcDC);
}

void CRMasterMonitorDlg::SubBmpPic(LPCSTR lpBitmapName)//나중에 전역변수로 간략화 할것임
{	
	CRect rect;
	CDC *pcDC = m_stCenterDisplay.GetDC();
	m_stCenterDisplay.GetClientRect(&rect);
			
	CDC MemDC;
	CBitmap image;
	BITMAP bmpinfo;

	MemDC.CreateCompatibleDC(pcDC);
	image.LoadBitmap(lpBitmapName);
	image.GetBitmap(&bmpinfo);
	CBitmap*pOldBitmap = (CBitmap*)MemDC.SelectObject(&image);
	pcDC->SetStretchBltMode(COLORONCOLOR);
	pcDC->StretchBlt(0, 0, rect.Width(), rect.Height(), &MemDC, 0, 0, bmpinfo.bmWidth, bmpinfo.bmHeight, SRCCOPY);
	MemDC.SelectObject(pOldBitmap);
	image.DeleteObject();
	MemDC.DeleteDC();
	ReleaseDC(pcDC);
}

void CRMasterMonitorDlg::OnCamDisplay(CDC *cdc)
{
	
	CPen  my_Pan,my_Pan2,*old_pan,*old_pan2;
	CFont m_font,m_font2;
	CString RESULTDATA ="";
	CString TEXTDATA ="";

	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	/*if(ClickNUM == TRUE){
		m_font2.CreatePointFont(700,"Arial");
		cdc->SelectObject(&m_font2);
		cdc->SetTextAlign(TA_CENTER|TA_BASELINE);
		cdc->SetTextColor(BLUE_COLOR);
		TEXTDATA.Format("SUCCESS");
		cdc->TextOut(360,300,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
	}*/
		
	my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
	old_pan = cdc->SelectObject(&my_Pan);
	cdc->SetTextColor(BLUE_COLOR);
	
	m_font.CreatePointFont(200,"Arial");  
	cdc->SelectObject(&m_font);
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);
	TEXTDATA.Format("MASTER MODE");
	cdc->TextOut(580,40,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	if((CenterData.x < 0)&&(CenterData.y < 0)){
		TEXTDATA.Format("Detect FAIL");
		cdc->SetTextColor(RED_COLOR);
		cdc->TextOut(360,440,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
	}else if((CenterData.x != 0)&&(CenterData.y != 0)){
		TEXTDATA.Format("X:%d Y:%d",CenterData.x,CenterData.y);
		cdc->TextOut(360,440,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
		my_Pan2.CreatePen(PS_SOLID,2,WHITE_COLOR);
		old_pan2 = cdc->SelectObject(&my_Pan2);
		cdc->SetTextColor(WHITE_COLOR);
		cdc->MoveTo((720-CenterData.x)-10,CenterData.y);
		cdc->LineTo((720-CenterData.x)+10,CenterData.y);
		cdc->MoveTo((720-CenterData.x),CenterData.y-10);
		cdc->LineTo((720-CenterData.x),CenterData.y+10);
	}
	
	CDC *pcDC;
	pcDC = m_stCenterDisplay.GetDC();

	::SetStretchBltMode(pcDC->m_hDC, COLORONCOLOR);
	::StretchBlt(pcDC->m_hDC,
				0,  0,
				CAM_IMAGE_WIDTH,
				CAM_IMAGE_HEIGHT,
				cdc->m_hDC,
				0, 0,
				CAM_IMAGE_WIDTH,
				CAM_IMAGE_HEIGHT,SRCCOPY);
	m_stCenterDisplay.ReleaseDC(pcDC);		
}

void CRMasterMonitorDlg::UpdateMaState()
{
	if(ClickNUM == TRUE){//마스터셋 진행을 성공하였을 때
		MasterState_Val(1,"Success");
	}else{
		MasterState_Val(0,"Stand By");
	}	
}

void CRMasterMonitorDlg::InitSet()
{
	CString str = "";

	if(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd != NULL){
		OrgStandX = ((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.StandX;
		OrgStandY = ((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.StandY;
	}else{
		OrgStandX = 360;
		OrgStandY = 240;	
	}
	b_TESTMOD = 1;

	MasterStandX = OrgStandX;
	MasterStandY = OrgStandY;

	str.Format("%d",MasterStandX);
	Master_AxisX_Txt(str);
	str.Format("%d",MasterStandY);
	Master_AxisY_Txt(str);

	m_RatePix = 5;
	CenterData.x =0;
	CenterData.y =0;

	ACenterData.x =0;
	ACenterData.y =0;

	MasterOffsetX = 0;
	Master_OffsetX_Txt("0");	
	MasterOffsetY = 0;
	Master_OffsetY_Txt("0");	

	pDirectModeCombo->SetCurSel(((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterPrm.WriteMode);

	m_dXModuleDistance = ((CImageTesterDlg  *)m_pMomWnd)->m_dXModuleDistance;//0.1mm단위라 가정
	m_dYModuleDistance = ((CImageTesterDlg  *)m_pMomWnd)->m_dYModuleDistance;//0.1mm단위라 가정
	UpdatePositionVal();
	Pixel_Rate_Set();
}
void CRMasterMonitorDlg::UpdatePositionVal()
{
	CString str;
	str.Format("%3.1f",(double)((CImageTesterDlg *)m_pMomWnd)->m_dXModuleDistance/10);
	PositionX_Txt(str);
	str.Format("%3.1f",(double)((CImageTesterDlg *)m_pMomWnd)->m_dYModuleDistance/10);
	PositionY_Txt(str);
	str.Format("%3.1f",(double)m_dXModuleDistance/10);	
	PositionValX_Txt(str);
	str.Format("%3.1f",(double)m_dYModuleDistance/10);
	PositionValY_Txt(str);
}


void CRMasterMonitorDlg::MasterState_Val(int col,LPCSTR lpcszString, ...)
{	
	if(col == 0){
		Matxtcol = RGB(0, 0, 0);
		MaBkcol = RGB(255, 255, 0);
	}else if(col  ==1){
		Matxtcol = RGB(255, 255, 255);
		MaBkcol = RGB(50, 82, 152);
	}else{
		Matxtcol = RGB(255,255,0);
		MaBkcol = RGB(0,0,0);
	}
	((CEdit *)GetDlgItem(IDC_STATE_MASTERINIT))->SetWindowText(lpcszString);
}

void CRMasterMonitorDlg::PositionX_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_POSITION_BVALX))->SetWindowText(lpcszString);
}
void CRMasterMonitorDlg::PositionY_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_POSITION_BVALY))->SetWindowText(lpcszString);
}

void CRMasterMonitorDlg::PositionValX_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_POSITION_VALX))->SetWindowText(lpcszString);
}
void CRMasterMonitorDlg::PositionValY_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_POSITION_VALY))->SetWindowText(lpcszString);
}

void CRMasterMonitorDlg::Master_OffsetX_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_MOFFSETX))->SetWindowText(lpcszString);
}
void CRMasterMonitorDlg::Master_OffsetY_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_MOFFSETY))->SetWindowText(lpcszString);
}

void CRMasterMonitorDlg::Master_AxisX_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_MAXISX))->SetWindowText(lpcszString);
}
void CRMasterMonitorDlg::Master_AxisY_Txt(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_MAXISY))->SetWindowText(lpcszString);
}

void CRMasterMonitorDlg::Pixel_Rate_Set()
{	
	CString str;
	str.Format("%d",m_RatePix);
	((CEdit *)GetDlgItem(IDC_EDIT_PIXELRATE))->SetWindowText(str);
}

CvPoint CRMasterMonitorDlg::ChartPointSet()
{
	double centx,centy;
	CvPoint pt = {0,0};
	CenterData.x = 0;
	CenterData.y = 0;
	ACenterData.x = 0;
	ACenterData.y = 0;
	int camcnt =0;

	while((!((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())&&(camcnt < 100)){
		DoEvents(10);
		camcnt++;
	}
	if (camcnt >= 100) {//카메라가 연결되어 있지 않으면 카메라를 연결
		return pt;
	}


	BOOL	bstate = 1;
	int		errcnt = 0;
	int		wrmode = pDirectModeCombo->GetCurSel();
	while(bstate){
		memset(m_RGBScanbuf,0,sizeof(m_RGBScanbuf));
		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = TRUE;
		
		for(int i =0;i<10;i++){
			if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == FALSE){
				break;
			}else{
				DoEvents(50);
			}
		}

		pt = GetCenterPoint(m_RGBScanbuf);
		if	((pt.x <= MasterStandX + 200)&&
			(pt.x >= MasterStandX - 200)&&
			(pt.y <= MasterStandY + 200)&&
			(pt.y >= MasterStandY - 200)){
			bstate = 0;
			CenterData.x = 720-pt.x;
			CenterData.y = pt.y;		
			switch(wrmode){
				case 0:
					ACenterData.x = CenterData.x;	//좌우반전
					ACenterData.y = CenterData.y;
					break;
				case 1:
					ACenterData.x = 720 - CenterData.x;	//상하반전
					ACenterData.y = 480-CenterData.y;	
					break;
				case 2:
					ACenterData.x = 720 - CenterData.x;	//오리지날
					ACenterData.y = CenterData.y;	
					break;
				case 3:
					ACenterData.x = CenterData.x;	//로테이트
					ACenterData.y = 480-CenterData.y;	
					break;
			}
		}else{
			pt.x = 0;
			pt.y = 0;
			errcnt++;
			if(errcnt >4){
				bstate = 0;
				CenterData.x = -9999;
				CenterData.y = -9999;
				ACenterData.x = -9999;
				ACenterData.y = -9999;
			}
		}
	}
	return CenterData;
}

CvPoint CRMasterMonitorDlg::GetCenterPoint(LPBYTE IN_RGB)
{	
	double Cam_PosX = (CAM_IMAGE_WIDTH/2);
	double Cam_PosY = (CAM_IMAGE_HEIGHT/2);
	CvPoint resultPt = cvPoint(-99999, -99999);

	IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *PreprecessedImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	
	IplImage *RGBOrgImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);

	BYTE R,G,B;
	double Sum_Y;

	for (int y=0; y<CAM_IMAGE_HEIGHT; y++)
	{
		for (int x=0; x<CAM_IMAGE_WIDTH; x++)
		{
			B = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 2];

			if(( R < 60 )&& (G < 60) && (B < 60)&& (abs(R-B) < 20)&& (abs(R-G) < 20))
				OriginImage->imageData[y*OriginImage->widthStep+x] = 255;
			else
				OriginImage->imageData[y*OriginImage->widthStep+x] = 0;

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}	
	
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);

	cvCopyImage(OriginImage, SmoothImage);
	
	cvCanny(SmoothImage, CannyImage, 0, 255);

	cvCopyImage(CannyImage, temp_PatternImage);
	
	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);
	
	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
		counter++;
	
	if(counter == 0){
			cvReleaseImage(&OriginImage);
		cvReleaseImage(&CannyImage);
		cvReleaseImage(&PreprecessedImage);
		
		cvReleaseImage(&DilateImage);
		cvReleaseImage(&SmoothImage);
		cvReleaseImage(&RGBResultImage);
		cvReleaseImage(&temp_PatternImage);
		cvReleaseImage(&RGBOrgImage);
		return resultPt;
	}

	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area=0, arcCount=0;
	counter = 0;
	double old_dist = 999999;
	
	int old_center_pos_x = 0;
	int old_center_pos_y = 0;
	int center_pt_x = 0;
	int center_pt_y = 0;
	int obj_Cnt = 0;
	int size=0, old_size = 0;	

	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;

		double circularity = (4.0*3.14*area)/(arcCount*arcCount);		

		if(circularity > 0.8)
		{
		
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;

			if(center_x > (int)(Cam_PosX*0.60) && center_x < (int)(Cam_PosX*1.40) && center_y > (int)(Cam_PosY*0.600) && center_y < (int)(Cam_PosY*1.400))
			{
				if(rect.width < Cam_PosX && rect.height < Cam_PosY && rect.width > 20 && rect.height > 20)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
					
					double distance = GetDistance(rect.x + rect.width/2, rect.y+rect.height/2, 360, 240);
					
					obj_Cnt++;

					size = rect.width * rect.height;
					
					if(distance < old_dist)
					{
						old_size = size;
						resultPt.x = center_x;
						resultPt.y = center_y;
						old_dist = distance;
					}

					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 0), 1, 8);  
				
				}
			}
		}
		
		counter++;
	}
	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&CannyImage);
	cvReleaseImage(&PreprecessedImage);
	
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
	cvReleaseImage(&RGBOrgImage);

	delete []rectArray;
	delete []areaArray;

//	delete contour;
//	delete temp_contour;
		
	return resultPt;	
}

double CRMasterMonitorDlg::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt( (double)((x2-x1)*(x2-x1)) +  ((y2-y1)*(y2-y1)));

	return result;
}


void CRMasterMonitorDlg::AutosetRunning()
{
	ClickNUM = 0;
	UpdateMaState();

	int offset_x = 0;
	int offset_y = 0;
	BOOL b_off_x = 0;
	BOOL b_off_y = 0;
	
	m_dXModuleDistance = ((CImageTesterDlg  *)m_pMomWnd)->m_dXModuleDistance;//0.1mm단위라 가정
	m_dYModuleDistance = ((CImageTesterDlg  *)m_pMomWnd)->m_dYModuleDistance;//0.1mm단위라 가정
	int back_dXModuleDistance = m_dXModuleDistance;
	int back_dYModuleDistance = m_dYModuleDistance;
	
	UpdatePositionVal();

	CenterData.x = 0;
	CenterData.y = 0;
	CvPoint OrgData = {0,0};
	CvPoint CurData = {0,0};

	int wrmode = pDirectModeCombo->GetCurSel();

	if(!((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE()){
		StatePrintf("JIG MOVE FAIL");
		ClickNUM = 0;
		UpdateMaState();	
		return;
	}

	((CImageTesterDlg *)m_pMomWnd)->Power_On(); //전원을 넣는다.
// 	DoEvents(1000);
// 	((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(0);
// 	DoEvents(500);

	int m_OrgdifX = 0;
	int m_OrgdifY = 0;

	BOOL b_OrgdifX = 0;
	BOOL b_OrgdifY = 0;

	m_RatePix = GetDlgItemInt(IDC_EDIT_PIXELRATE);
	if(m_RatePix <= 0){
		m_RatePix = 1;
	}else if(m_RatePix > 20){
		m_RatePix = 20;
	}
	Pixel_Rate_Set();


	for(int lop = 0;lop<10;lop++){
		CenterData = ChartPointSet();

		if((CenterData.x != 0)&&(CenterData.y != 0)){//중심점 검출을 성공하면
			if((CenterData.x == MasterStandX)&&(CenterData.y == MasterStandY)){//중심점이 합격기준에 들어가면
				((CImageTesterDlg *)m_pMomWnd)->SavePositionSet(m_dXModuleDistance,m_dYModuleDistance);//현재 모터 위치를 저장한다.
				//UpdateChartVal();
				StatePrintf("Auto Detect Success");
				ClickNUM = 1;
				UpdateMaState();
				return;
			}
			offset_x = MasterStandX - CenterData.x;
			offset_y = MasterStandY - CenterData.y;
			if(offset_x >= 0){
				b_off_x = TRUE;
			}else{
				b_off_x = FALSE;
			}

			if(offset_y >= 0){
				b_off_y = TRUE;
			}else{
				b_off_y = FALSE;
			}
			if(lop != 0){
				if(((b_off_x == b_OrgdifX)&&(abs(offset_x) > abs(m_OrgdifX)))||((b_off_y == b_OrgdifY)&&(abs(offset_y) > abs(m_OrgdifY)))){
					m_dXModuleDistance = ((CImageTesterDlg  *)m_pMomWnd)->m_dXModuleDistance;//0.1mm단위라 가정
					m_dYModuleDistance = ((CImageTesterDlg  *)m_pMomWnd)->m_dYModuleDistance;//0.1mm단위라 가정
					((CImageTesterDlg *)m_pMomWnd)->SavePositionSet(m_dXModuleDistance,m_dYModuleDistance);//현재 모터 위치를 저장한다.
					ClickNUM = 0;
					UpdateMaState();
					DoEvents(1000);
					StatePrintf("카메라 방향설정이 틀립니다. 확인해 주세요.");
					if(!((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE()){
						StatePrintf("JIG MOVE FAIL");
						ClickNUM = 0;
						UpdateMaState();	
						return;
					}
					return;
				}
			}
			m_OrgdifX = offset_x;
			m_OrgdifY = offset_y;
			b_OrgdifX = b_off_x;
			b_OrgdifY = b_off_y;
		}else{//검출 자체를 실패한 경우
			//((COptical_CenterDlg *)m_pMomWnd)->SaveChartCellPrm();
			//UpdateChartVal();
			m_dXModuleDistance = ((CImageTesterDlg  *)m_pMomWnd)->m_dXModuleDistance;//0.1mm단위라 가정
			m_dYModuleDistance = ((CImageTesterDlg  *)m_pMomWnd)->m_dYModuleDistance;//0.1mm단위라 가정
			((CImageTesterDlg *)m_pMomWnd)->SavePositionSet(m_dXModuleDistance,m_dYModuleDistance);//현재 모터 위치를 저장한다.
			ClickNUM = 0;
			UpdateMaState();
			AfxMessageBox("Auto Detect Fail->중심점 검출 실패");
			if(!((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE()){
				StatePrintf("JIG MOVE FAIL");
				ClickNUM = 0;
				UpdateMaState();	
				return;
			}
			
			return;
		}

		//m_dXModuleDistance -= offset_x * m_RatePix;	//좌우반전
		//m_dYModuleDistance -= offset_y * m_RatePix;

		switch(wrmode){
			case 0:
				m_dXModuleDistance -= offset_x * m_RatePix;	//좌우반전
				m_dYModuleDistance += offset_y * m_RatePix;	
				break;
			case 1:
				m_dXModuleDistance += offset_x * m_RatePix;	//상하반전
				m_dYModuleDistance -= offset_y * m_RatePix;		
				break;
			case 2:
				m_dXModuleDistance += offset_x * m_RatePix;	//오리지날
				m_dYModuleDistance += offset_y * m_RatePix;	
				break;
			case 3:
				m_dXModuleDistance -= offset_x * m_RatePix;	//로테이트
				m_dYModuleDistance -= offset_y * m_RatePix;	
				break;
		}

		//switch(wrmode){
		//	case 0:
		//		m_dXModuleDistance -= offset_x * m_RatePix;	//좌우반전
		//		m_dYModuleDistance -= offset_y * m_RatePix;	
		//		break;
		//	case 1:
		//		m_dXModuleDistance += offset_x * m_RatePix;	//상하반전
		//		m_dYModuleDistance += offset_y * m_RatePix;		
		//		break;
		//	case 2:
		//		m_dXModuleDistance += offset_x * m_RatePix;	//오리지날
		//		m_dYModuleDistance -= offset_y * m_RatePix;	
		//		break;
		//	case 3:
		//		m_dXModuleDistance -= offset_x * m_RatePix;	//로테이트
		//		m_dYModuleDistance += offset_y * m_RatePix;	
		//		break;
		//}

		if(m_dXModuleDistance < 0){
			m_dXModuleDistance = 0;
			lop = 10;
		}else if(m_dXModuleDistance > 280){
			m_dXModuleDistance = 280;
			lop = 10;
		}

		if(m_dYModuleDistance < 0){
			m_dYModuleDistance = 0;
			lop = 10;
		}else if(m_dYModuleDistance > 300){
			m_dYModuleDistance = 300;
			lop = 10;
		}
		UpdatePositionVal();

		back_dXModuleDistance = ((CImageTesterDlg  *)m_pMomWnd)->m_dXModuleDistance;//기존 위치를 백업한다.
		back_dYModuleDistance = ((CImageTesterDlg  *)m_pMomWnd)->m_dYModuleDistance;

		((CImageTesterDlg  *)m_pMomWnd)->m_dXModuleDistance = m_dXModuleDistance;
		((CImageTesterDlg  *)m_pMomWnd)->m_dYModuleDistance = m_dYModuleDistance;

		if(!((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE()){
			StatePrintf("JIG MOVE FAIL");
			ClickNUM = 0;
			UpdateMaState();
			((CImageTesterDlg  *)m_pMomWnd)->m_dXModuleDistance = back_dXModuleDistance;
			((CImageTesterDlg  *)m_pMomWnd)->m_dYModuleDistance = back_dYModuleDistance;
			return;
		}
		((CImageTesterDlg  *)m_pMomWnd)->m_dXModuleDistance = back_dXModuleDistance;
		((CImageTesterDlg  *)m_pMomWnd)->m_dYModuleDistance = back_dYModuleDistance;//현재 위치를 백업한다.
	}
}

BOOL CRMasterMonitorDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		if(pMsg->wParam == VK_ADD){
			return TRUE;
		}
		if (pMsg->wParam == VK_SUBTRACT){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CRMasterMonitorDlg::OnBnClickedBtnPowerOn2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Power_On();
}

void CRMasterMonitorDlg::OnBnClickedBtnPowerOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Power_Off();
}

void CRMasterMonitorDlg::OnBnClickedBtnOverlayon()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(1);
}

void CRMasterMonitorDlg::OnBnClickedBtnOverlayoff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(0);
}

void CRMasterMonitorDlg::OnBnClickedBtnAutoset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	RUN_MODE_CHK(1);
	AutosetRunning();
	RUN_MODE_CHK(0);
}

void CRMasterMonitorDlg::RUN_MODE_CHK(bool Mode){//
	if(Mode == TRUE){
		((CButton *)GetDlgItem(IDC_BTN_AUTOSET))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_AUTOSET2))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_POWER_ON2))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_OVERLAYON))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_POWER_OFF))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_OVERLAYOFF))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_USER_EXIT))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_POSITION_STAND))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_POSITION_MOVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_POSITION_SET))->EnableWindow(0);

		((CEdit *)GetDlgItem(IDC_POSITION_VALX))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_POSITION_VALY))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_PIXELRATE))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MOFFSETX))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MOFFSETY))->EnableWindow(0);
		((CComboBox *)GetDlgItem(IDC_OPT_WRMODE))->EnableWindow(0);
		b_Running = TRUE;
	}else{
		((CButton *)GetDlgItem(IDC_BTN_AUTOSET))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BTN_AUTOSET2))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BTN_POWER_ON2))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BTN_OVERLAYON))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BTN_POWER_OFF))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BTN_OVERLAYOFF))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BUTTON_USER_EXIT))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BTN_POSITION_STAND))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BTN_POSITION_MOVE))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BTN_POSITION_SET))->EnableWindow(1);

		((CEdit *)GetDlgItem(IDC_POSITION_VALX))->EnableWindow(1);
		((CEdit *)GetDlgItem(IDC_POSITION_VALY))->EnableWindow(1);
		((CEdit *)GetDlgItem(IDC_EDIT_PIXELRATE))->EnableWindow(1);
		((CEdit *)GetDlgItem(IDC_EDIT_MOFFSETX))->EnableWindow(1);
		((CEdit *)GetDlgItem(IDC_EDIT_MOFFSETY))->EnableWindow(1);
		((CComboBox *)GetDlgItem(IDC_OPT_WRMODE))->EnableWindow(1);
		b_Running = FALSE;
		PositionValChk();
	}
}
void CRMasterMonitorDlg::OnEnChangeEditMoffsetx()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	CString str;
	MasterOffsetX = GetDlgItemInt(IDC_EDIT_MOFFSETX);

	MasterStandX = OrgStandX + MasterOffsetX;
	if(MasterStandX > 719){
		MasterStandX = 719;
	}else if(MasterStandX < 0){
		MasterStandX = 0;
	}
	str.Format("%d",MasterStandX);
	Master_AxisX_Txt(str);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CRMasterMonitorDlg::OnEnChangeEditMoffsety()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	CString str;
	MasterOffsetY = GetDlgItemInt(IDC_EDIT_MOFFSETY);

	MasterStandY = OrgStandY + MasterOffsetY;
	if(MasterStandY > 479){
		MasterStandY = 479;
	}else if(MasterStandY< 0){
		MasterStandY = 0;
	}
	str.Format("%d",MasterStandY);
	Master_AxisY_Txt(str);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CRMasterMonitorDlg::OnBnClickedBtnPositionStand()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	RUN_MODE_CHK(1);
	ClickNUM = 0;
	UpdateMaState();
	m_dXModuleDistance = 150;
	m_dYModuleDistance = 150;
	((CImageTesterDlg *)m_pMomWnd)->SavePositionSet(m_dXModuleDistance,m_dYModuleDistance);//현재 모터 위치를 저장한다.
	UpdatePositionVal();
	if(!((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE()){
		StatePrintf("JIG MOVE FAIL");
	}
	RUN_MODE_CHK(0);
}

void CRMasterMonitorDlg::OnBnClickedBtnPositionMove()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	RUN_MODE_CHK(1);

	int back_dXModuleDistance = m_dXModuleDistance;
	int back_dYModuleDistance = m_dYModuleDistance;

	ClickNUM = 0;
	UpdateMaState();

	CString str = "";
	double buf_data;
	GetDlgItemText(IDC_POSITION_VALX,str);
	buf_data = atof(str) + 0.05;
	m_dXModuleDistance = (int)(buf_data*10);

	GetDlgItemText(IDC_POSITION_VALY,str);
	buf_data = atof(str) + 0.05;
	m_dYModuleDistance = (int)(buf_data*10);

	if((m_dXModuleDistance < 0)||(m_dXModuleDistance >280)||(m_dYModuleDistance <0)||(m_dYModuleDistance > 300)){
		m_dXModuleDistance = back_dXModuleDistance;
		m_dYModuleDistance = back_dYModuleDistance;
		RUN_MODE_CHK(0);
		return;
	}
	int backModuleDistanceX = ((CImageTesterDlg *)m_pMomWnd)->m_dXModuleDistance;
	int backModuleDistanceY = ((CImageTesterDlg *)m_pMomWnd)->m_dYModuleDistance;

	((CImageTesterDlg *)m_pMomWnd)->m_dXModuleDistance = m_dXModuleDistance;
	((CImageTesterDlg *)m_pMomWnd)->m_dYModuleDistance = m_dYModuleDistance;
	if(!((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE()){
		StatePrintf("JIG MOVE FAIL");
	}
	((CImageTesterDlg *)m_pMomWnd)->m_dXModuleDistance = backModuleDistanceX;
	((CImageTesterDlg *)m_pMomWnd)->m_dYModuleDistance = backModuleDistanceY;
	UpdatePositionVal();
	RUN_MODE_CHK(0);

}

void CRMasterMonitorDlg::OnBnClickedBtnPositionSet()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	RUN_MODE_CHK(1);
	int back_dXModuleDistance = m_dXModuleDistance;
	int back_dYModuleDistance = m_dYModuleDistance;

	CString str = "";
	double buf_data;
	GetDlgItemText(IDC_POSITION_VALX,str);
	buf_data = atof(str) + 0.05;
	m_dXModuleDistance = (int)(buf_data*10);

	GetDlgItemText(IDC_POSITION_VALY,str);
	buf_data = atof(str) + 0.05;
	m_dYModuleDistance = (int)(buf_data*10);

	if((m_dXModuleDistance < 0)||(m_dXModuleDistance >280)||(m_dYModuleDistance <0)||(m_dYModuleDistance > 300)){
		m_dXModuleDistance = back_dXModuleDistance;
		m_dYModuleDistance = back_dYModuleDistance;
		RUN_MODE_CHK(0);
		return;
	}
	
	((CImageTesterDlg *)m_pMomWnd)->SavePositionSet(m_dXModuleDistance,m_dYModuleDistance);//현재 모터 위치를 저장한다.
	UpdatePositionVal();
	if(!((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE()){
		StatePrintf("JIG MOVE FAIL");
	}else{
		ClickNUM = 1;
		UpdateMaState();
		StatePrintf("Manual Set Success");
	}
	RUN_MODE_CHK(0);
}

void CRMasterMonitorDlg::PositionValChk()
{
	CString str = "";
	double buf_data;
	int buf_X = -1;
	int	buf_Y = -1;

	GetDlgItemText(IDC_POSITION_VALX,str);
	str.Replace(" ","");
	if(str != ""){
		buf_data = atof(str) + 0.05;
		buf_X = (int)(buf_data*10);
	}

	GetDlgItemText(IDC_POSITION_VALY,str);
	str.Replace(" ","");
	if(str != ""){
		buf_data = atof(str) + 0.05;
		buf_Y = (int)(buf_data*10);
	}
	if((buf_X < 0)||(buf_X >280)||(buf_Y <0)||(buf_Y > 300)){
		((CButton *)GetDlgItem(IDC_BTN_POSITION_MOVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_POSITION_SET))->EnableWindow(0);	
	}else{
		if(b_Running == TRUE){
			((CButton *)GetDlgItem(IDC_BTN_POSITION_MOVE))->EnableWindow(0);
			((CButton *)GetDlgItem(IDC_BTN_POSITION_SET))->EnableWindow(0);
		}else{
			((CButton *)GetDlgItem(IDC_BTN_POSITION_MOVE))->EnableWindow(1);
			((CButton *)GetDlgItem(IDC_BTN_POSITION_SET))->EnableWindow(1);
		}
	}
}



void CRMasterMonitorDlg::OnEnChangePositionValx()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	PositionValChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CRMasterMonitorDlg::OnEnChangePositionValy()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	PositionValChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CRMasterMonitorDlg::OnBnClickedBtnCenterdetect()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	RUN_MODE_CHK(1);
	int camcnt =0;

	if(((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK() == FALSE){
		((CImageTesterDlg *)m_pMomWnd)->Power_On(); //전원을 넣는다.
		DoEvents(1000);
		if(((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK() == FALSE){
			RUN_MODE_CHK(0);
			return;
		}
	}
// 	((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(0);
// 	DoEvents(500);
	ChartPointSet();
	DoEvents(500);
	RUN_MODE_CHK(0);
}

HBRUSH CRMasterMonitorDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() ==IDC_STATE_MASTERINIT){
		pDC->SetTextColor(Matxtcol);
		pDC->SetBkColor(MaBkcol);
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CRMasterMonitorDlg::OnEnChangeEditPixelrate()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CRMasterMonitorDlg::OnBnClickedButtonUserExit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(((ClickNUM == 1)))
	{
		OnCancel();
	}else if(((CImageTesterDlg *)m_pMomWnd)->MatchPassWord == ""){
		b_TESTMOD =0;
		OnCancel();
	}
	else{
		CUserSwitching subdlg;
		subdlg.Setup((CImageTesterDlg *)m_pMomWnd);
	
		if(subdlg.DoModal() == IDOK){
			b_TESTMOD =0;
			OnCancel();		
		}
		else{
			ShowWindow(SW_HIDE);
			ShowWindow(SW_SHOW);
		}
		delete subdlg;
	}
}

void CRMasterMonitorDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(b_TESTMOD == 1){
		int skip=0; 
		
		if(ClickNUM != 1){
			skip = 100;
		}

		if(skip == 100){
			AfxMessageBox("마스터 세팅이 되지 않았습니다. 다시 세팅 해주세요.");
			ShowWindow(SW_HIDE);
			ShowWindow(SW_SHOW);
			return;
		}
	}

	b_TESTMOD =0; 
	((CImageTesterDlg *)m_pMomWnd)->Power_Off();
	((CImageTesterDlg *)m_pMomWnd)->b_MasterDlg = FALSE;
	CDialog::OnCancel();
}

void CRMasterMonitorDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == 1){
		InitSet();
	}
}
