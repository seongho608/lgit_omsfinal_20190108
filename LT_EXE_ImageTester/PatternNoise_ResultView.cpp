// PatternNoise_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "PatternNoise_ResultView.h"


// CPatternNoise_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPatternNoise_ResultView, CDialog)

CPatternNoise_ResultView::CPatternNoise_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CPatternNoise_ResultView::IDD, pParent)
{

}

CPatternNoise_ResultView::~CPatternNoise_ResultView()
{
}

void CPatternNoise_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_RESULT, m_ResultList);
}


BEGIN_MESSAGE_MAP(CPatternNoise_ResultView, CDialog)
	ON_WM_CTLCOLOR()
	ON_EN_SETFOCUS(IDC_EDIT_Y_STAT, &CPatternNoise_ResultView::OnEnSetfocusEditYStat)
END_MESSAGE_MAP()


// CPatternNoise_ResultView 메시지 처리기입니다.
void CPatternNoise_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CPatternNoise_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
BOOL CPatternNoise_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	Font_Size_Change(IDC_STATIC_Y,&ft,100,50);
	GetDlgItem(IDC_Thresold_Y)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_Y_STAT)->SetFont(&ft);/*
	GetDlgItem(IDC_STATIC_V)->SetFont(&ft);	
	GetDlgItem(IDC_Thresold_V)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_V_STAT)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_D)->SetFont(&ft);	
	GetDlgItem(IDC_Thresold_D)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_D_STAT)->SetFont(&ft);*/
	// TODO:  여기에 추가 초기화 작업을 추가합니다.


	m_ResultList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_ResultList.InsertColumn(0,"LOCATION",LVCFMT_CENTER, 110);
	m_ResultList.InsertColumn(1,"STD",LVCFMT_CENTER, 80);
	m_ResultList.InsertColumn(2,"RESULT",LVCFMT_CENTER, 80);
	m_ResultList.InsertColumn(3,"비고",LVCFMT_CENTER, 80);
	
	
	initstat();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPatternNoise_ResultView::initstat()
{
	Y_TEXT("STD");
	Y_VALUE("");
	Y_RESULT(0,"STAND BY");

	V_TEXT("산포");
	V_VALUE("");
	V_RESULT(0,"STAND BY");

	D_TEXT("NoiseValue");
	D_VALUE("");
	D_RESULT(0,"STAND BY");
	m_ResultList.DeleteAllItems();
}

HBRUSH CPatternNoise_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_Y){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_Thresold_Y){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}

	//if(pWnd->GetDlgCtrlID() == IDC_STATIC_V){
	//		pDC->SetTextColor(RGB(255, 255, 255));
	//		// 텍스트의 배경색상을 설정한다.
	//		//pDC->SetBkColor(RGB(138, 75, 36));
	//		pDC->SetBkColor(RGB(86,86,86));
	//		// 바탕색상을 설정한다.
	//}
	//if(pWnd->GetDlgCtrlID() == IDC_Thresold_V){
	//		pDC->SetTextColor(RGB(86,86,86));
	//		// 텍스트의 배경색상을 설정한다.
	//		pDC->SetBkColor(RGB(255, 255, 255));
	//		// 바탕색상을 설정한다.
	//}

	//if(pWnd->GetDlgCtrlID() == IDC_STATIC_D){
	//		pDC->SetTextColor(RGB(255, 255, 255));
	//		// 텍스트의 배경색상을 설정한다.
	//		//pDC->SetBkColor(RGB(138, 75, 36));
	//		pDC->SetBkColor(RGB(86,86,86));
	//		// 바탕색상을 설정한다.
	//}
	//if(pWnd->GetDlgCtrlID() == IDC_Thresold_D){
	//		pDC->SetTextColor(RGB(86,86,86));
	//		// 텍스트의 배경색상을 설정한다.
	//		pDC->SetBkColor(RGB(255, 255, 255));
	//		// 바탕색상을 설정한다.
	//}

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_Y_STAT){
		pDC->SetTextColor(txcol_TVal);
		pDC->SetBkColor(bkcol_TVal);
	}
	//if(pWnd->GetDlgCtrlID() ==IDC_EDIT_V_STAT){
	//	pDC->SetTextColor(txcol_TVal2);
	//	pDC->SetBkColor(bkcol_TVal2);
	//}

	//if(pWnd->GetDlgCtrlID() ==IDC_EDIT_D_STAT){
	//	pDC->SetTextColor(txcol_TVal3);
	//	pDC->SetBkColor(bkcol_TVal3);
	//}


	
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CPatternNoise_ResultView::Y_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_Y))->SetWindowText(lpcszString);
}
void CPatternNoise_ResultView::V_TEXT(LPCSTR lpcszString, ...){
	//((CEdit *)GetDlgItem(IDC_STATIC_V))->SetWindowText(lpcszString);
}
void CPatternNoise_ResultView::D_TEXT(LPCSTR lpcszString, ...){
	//((CEdit *)GetDlgItem(IDC_STATIC_D))->SetWindowText(lpcszString);
}

void CPatternNoise_ResultView::Y_VALUE(LPCSTR lpcszString, ...){
	//((CEdit *)GetDlgItem(IDC_Thresold_Y))->SetWindowText(lpcszString);
}
void CPatternNoise_ResultView::V_VALUE(LPCSTR lpcszString, ...){
	//((CEdit *)GetDlgItem(IDC_Thresold_V))->SetWindowText(lpcszString);
}
void CPatternNoise_ResultView::D_VALUE(LPCSTR lpcszString, ...){
	//((CEdit *)GetDlgItem(IDC_Thresold_D))->SetWindowText(lpcszString);
}

void CPatternNoise_ResultView::Y_RESULT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}

	((CEdit *)GetDlgItem(IDC_EDIT_Y_STAT))->SetWindowText(lpcszString);
}

void CPatternNoise_ResultView::V_RESULT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal2 = RGB(0, 0, 0);
		bkcol_TVal2 = RGB(255, 255, 0);
	}

	//((CEdit *)GetDlgItem(IDC_EDIT_V_STAT))->SetWindowText(lpcszString);
}

void CPatternNoise_ResultView::D_RESULT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_TVal3 = RGB(255, 255, 255);
		bkcol_TVal3 = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal3 = RGB(255, 255, 255);
		bkcol_TVal3 = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_TVal3 = RGB(255, 255, 255);
		bkcol_TVal3 = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal3 = RGB(0, 0, 0);
		bkcol_TVal3 = RGB(255, 255, 0);
	}

	//((CEdit *)GetDlgItem(IDC_EDIT_D_STAT))->SetWindowText(lpcszString);
}

BOOL CPatternNoise_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CPatternNoise_ResultView::OnEnSetfocusEditYStat()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}
