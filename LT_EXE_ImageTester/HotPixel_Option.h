#pragma once
#include "afxcmn.h"
#include "ETCUSER.H"
#include "resource.h"
#include <vector>
#include "TI_HotPixel.h"
// CHotPixel_Option 대화 상자입니다.

class CHotPixel_Option : public CDialog
{
	DECLARE_DYNAMIC(CHotPixel_Option)

public:
	CHotPixel_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CHotPixel_Option();
    

	int		m_CAM_SIZE_WIDTH;
	int		m_CAM_SIZE_HEIGHT;
	int		StartCnt;
	bool    m_Success;
	tResultVal Run();
	void	Pic(CDC *cdc);
	void	InitPrm();
	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
	void	StatePrintf(LPCSTR lpcszString, ...);
	bool	HotPixelPic(CDC *cdc,int NUM);


	CString HotPixel_filename;
	void	Save(int NUM);

	void	Save_parameter();
	void	Load_parameter();
	void	UploadList();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_HotPixel };

	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void InsertList();
	int InsertIndex;
	int Lot_InsertIndex;
	int ListItemNum;
	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();
	void FAIL_UPLOAD();
	BOOL	b_StopFail;

	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();

	#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	int		Lot_StartCnt;
	int		Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

	void	InitEVMS();

	CRectData P_RECT[256];

	// Hot Pixel의 X좌표
	CPoint			m_ptCenter[100];
	BOOL			m_bCluster[100];
	unsigned short	m_sPointX[100];		// Hot Pixel의 X좌표
	unsigned short	m_sPointY[100];		// Hot Pixel의 Y좌표
	float			m_fConcen[100];		// 농도

#pragma endregion

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:

	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	bool HotPixelGen_16bit(WORD *GRAYScanBuf);

	void HotPixel_Cluster();

	afx_msg void OnBnClickedButtontest();
	afx_msg void OnBnClickedButtonSave();

	double  m_dThreshold;
	double  m_dLightCurr;
	int		m_iLightDelay;
	int		m_iPassCount;
	int		m_iFailCount;
	int		m_iClusterCount;

	CListCtrl m_WorkList;
	CListCtrl m_Lot_WorkList;

	BOOL	m_b_FailCheck;
};

void CModel_Create	(CHotPixel_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO);
void CModel_Delete	(CHotPixel_Option **pWnd);