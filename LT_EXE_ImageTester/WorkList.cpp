// WorkList.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "WorkList.h"


// CWorkList 대화 상자입니다.

IMPLEMENT_DYNAMIC(CWorkList, CDialog)

CWorkList::CWorkList(CWnd* pParent /*=NULL*/)
	: CDialog(CWorkList::IDD, pParent)
{

}

CWorkList::~CWorkList()
{
}

void CWorkList::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_SEL_TEST, m_TEST_Item);
	DDX_Control(pDX, IDC_WORKLIST, m_WorkList);
}


BEGIN_MESSAGE_MAP(CWorkList, CDialog)
	ON_WM_DESTROY()
	ON_NOTIFY(NM_CLICK, IDC_LIST_SEL_TEST, &CWorkList::OnNMClickListSelTest)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_SEL_TEST, &CWorkList::OnLvnItemchangedListSelTest)
END_MESSAGE_MAP()


// CWorkList 메시지 처리기입니다.
void CWorkList::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}
BOOL CWorkList::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_TEST_Item.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);//|LVS_EX_HEADERDRAGDROP
	m_TEST_Item.InsertColumn(0,"TEST",LVCFMT_CENTER,155);
	m_TEST_Item.SetSelectionMark(0);
	m_TEST_Item.SetFocus();
	((CImageTesterDlg *)m_pMomWnd)->Set_List(&m_WorkList);
	Copy_List(&((CImageTesterDlg *)m_pMomWnd)->MainWork_List,&m_WorkList,((CImageTesterDlg *)m_pMomWnd)->ListItemNum);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CWorkList::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		if(pMsg->wParam == VK_ADD){
			return TRUE;
		}
		if (pMsg->wParam == VK_SUBTRACT){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CWorkList::OnDestroy()
{
	CDialog::OnDestroy();
	
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CWorkList::UpdateRect(int parm_edit_id)
{
	 CRect r;
	 GetDlgItem(parm_edit_id)->GetWindowRect(r);
	 // 에디트 좌표를 대화상자 기준으로 변환한다.
	 ScreenToClient(r);
	 // 다이얼로그의 에디트 컨트롤 영역을 갱신한다.
	 InvalidateRect(r);
}

void CWorkList::UpdateRect()
{
	UpdateRect(IDC_LIST_SEL_TEST);
	UpdateRect(IDC_WORKLIST);
}

void CWorkList::OnNMClickListSelTest(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItem= reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int Item=pNMItem->iItem;

	if( pNMItem->iSubItem == -1 || pNMItem->iItem == -1 )
		return ;
	if(Item ==0){
		((CImageTesterDlg *)m_pMomWnd)->Set_List(&m_WorkList);
// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == TRUE)
// 			Copy_List(&((CImageTesterDlg *)m_pMomWnd)->m_Lot_MainWorkList, &m_WorkList, ((CImageTesterDlg *)m_pMomWnd)->ListItemNum);
// 		else
			Copy_List(&((CImageTesterDlg *)m_pMomWnd)->MainWork_List, &m_WorkList, ((CImageTesterDlg *)m_pMomWnd)->ListItemNum);
	}
	else{
		if(((CImageTesterDlg *)m_pMomWnd)->b_Chk_CurrentChkEn == TRUE){
			Item=Item-1;
	// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == TRUE)
	// 			((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[Item]->Upload_Lot_List(&m_WorkList);
	// 		else
			((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[Item]->Upload_List(&m_WorkList);
		}else{
			Item=Item-1;
// 			if (((CImageTesterDlg *)m_pMomWnd)->LotMod == TRUE)
// 				((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[Item + 1]->Upload_Lot_List(&m_WorkList);
// 			else
				((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[Item+1]->Upload_List(&m_WorkList);
		}
	}
	*pResult = 0;
}

void CWorkList::OnLvnItemchangedListSelTest(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}
void CWorkList::SearchList(CString TestNAME){
	/*for(int t=0; t<20;t++){
		if(((CCenterAFocus_DetectDlg *)m_pMomWnd)->m_psubmodel[t] != NULL){
			if(((CCenterAFocus_DetectDlg *)m_pMomWnd)->m_psubmodel[t]->m_ID == 0x01){
				if(TestNAME == "FOCUS"){
					((CCenterAFocus_DetectDlg *)m_pMomWnd)->m_psubmodel[t]->Upload_List(&m_WorkList,0);
					break;
				}
				if(TestNAME == "CENTER_DETECT"){
					((CCenterAFocus_DetectDlg *)m_pMomWnd)->m_psubmodel[t]->Upload_List(&m_WorkList,1);
					break;
				}			
			}
			else if(((CCenterAFocus_DetectDlg *)m_pMomWnd)->m_psubmodel[t]->m_ID == 0x02){
				if(TestNAME == "ROTATE"){
					((CCenterAFocus_DetectDlg *)m_pMomWnd)->m_psubmodel[t]->Upload_List(&m_WorkList,0);
					break;
				}
				if(TestNAME == "TILT"){
					((CCenterAFocus_DetectDlg *)m_pMomWnd)->m_psubmodel[t]->Upload_List(&m_WorkList,1);
					break;
				}			
			}
			else {
				((CCenterAFocus_DetectDlg *)m_pMomWnd)->m_psubmodel[t]->Upload_List(&m_WorkList,0);
			}
		}
	}*/
}
