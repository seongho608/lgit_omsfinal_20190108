// Option_ResultWStand.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Option_ResultWStand.h"


// COption_ResultWStand 대화 상자입니다.

IMPLEMENT_DYNAMIC(COption_ResultWStand, CDialog)

COption_ResultWStand::COption_ResultWStand(CWnd* pParent /*=NULL*/)
	: CDialog(COption_ResultWStand::IDD, pParent)
{

}

COption_ResultWStand::~COption_ResultWStand()
{
}

void COption_ResultWStand::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(COption_ResultWStand, CDialog)
	ON_WM_CTLCOLOR()
	ON_EN_SETFOCUS(IDC_VOLT_NAME, &COption_ResultWStand::OnEnSetfocusVoltName)
	ON_EN_SETFOCUS(IDC_VOLT_VALUE, &COption_ResultWStand::OnEnSetfocusVoltValue)
	ON_EN_SETFOCUS(IDC_MODEL_LNAME, &COption_ResultWStand::OnEnSetfocusModelLname)
	ON_EN_SETFOCUS(IDC_EDIT_IPNAME, &COption_ResultWStand::OnEnSetfocusEditIpname)
	ON_EN_SETFOCUS(IDC_EDIT_IPVAL, &COption_ResultWStand::OnEnSetfocusEditIpval)
	ON_EN_SETFOCUS(IDC_EDIT_PORTNAME4, &COption_ResultWStand::OnEnSetfocusEditPortname4)
	ON_EN_SETFOCUS(IDC_EDIT_PORTVAL, &COption_ResultWStand::OnEnSetfocusEditPortval)
	ON_EN_SETFOCUS(IDC_EDIT_LOTID_NAME, &COption_ResultWStand::OnEnSetfocusEditLotidName)
	ON_EN_SETFOCUS(IDC_EDIT_LOTID_VAL, &COption_ResultWStand::OnEnSetfocusEditLotidVal)
	ON_EN_SETFOCUS(IDC_EDIT_TESTNUM_NAME, &COption_ResultWStand::OnEnSetfocusEditTestnumName)
	ON_EN_SETFOCUS(IDC_EDIT_TESTNUM_VAL, &COption_ResultWStand::OnEnSetfocusEditTestnumVal)
	ON_EN_SETFOCUS(IDC_EDIT_BARCODE_NAME, &COption_ResultWStand::OnEnSetfocusEditBarcodeName)
	ON_EN_SETFOCUS(IDC_EDIT_BARCODE_VAL, &COption_ResultWStand::OnEnSetfocusEditBarcodeVal)
END_MESSAGE_MAP()


// COption_ResultWStand 메시지 처리기입니다.
void COption_ResultWStand::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void COption_ResultWStand::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
BOOL COption_ResultWStand::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_VOLT_NAME,&font1,500,28);
	GetDlgItem(IDC_VOLT_VALUE)->SetFont(&font1);
	Font_Size_Change(IDC_MODEL_LNAME,&font2,620,35);
	
	Font_Size_Change(IDC_EDIT_IPNAME,&font3,30,28);
	GetDlgItem(IDC_EDIT_IPNAME)->SetFont(&font3);
	GetDlgItem(IDC_EDIT_IPVAL)->SetFont(&font3);
	GetDlgItem(IDC_EDIT_PORTNAME4)->SetFont(&font3);
	GetDlgItem(IDC_EDIT_PORTVAL)->SetFont(&font3);

	GetDlgItem(IDC_EDIT_LOTID_NAME)->SetFont(&font3);
	GetDlgItem(IDC_EDIT_LOTID_VAL)->SetFont(&font3);
	GetDlgItem(IDC_EDIT_TESTNUM_NAME)->SetFont(&font3);
	GetDlgItem(IDC_EDIT_TESTNUM_VAL)->SetFont(&font3);

	Font_Size_Change(IDC_EDIT_BARCODE_NAME,&font4,30,35);
	GetDlgItem(IDC_EDIT_BARCODE_NAME)->SetFont(&font4);
	GetDlgItem(IDC_EDIT_BARCODE_VAL)->SetFont(&font4);

	Voltage_text("VOLTAGE");

	MESIP_TEXT("IP");
	MESPORT_TEXT("Port");
	MESBARCODE_TEXT("BarCode");

	m_strIp = ((CImageTesterDlg*)m_pMomWnd)->m_szIp;
	MESIP_VIEW(m_strIp);

	m_strPort = ((CImageTesterDlg*)m_pMomWnd)->m_szPort;
	MESPORT_VIEW(m_strPort);
	MESBARCODE_VIEW("");

	LOTID_TEXT("LOTID");
	TESTNUM_TEXT("NUM");


	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL COption_ResultWStand::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		if(pMsg->wParam == VK_ADD){
//			::PostMessage(((CCenterPointModify_4CHDlg *)m_pMomWnd)->hMainWnd, WM_COMM_START, 0, 0 );
			return TRUE;
		}
		if (pMsg->wParam == VK_SUBTRACT){
//			::PostMessage(((CCenterPointModify_4CHDlg *)m_pMomWnd)->hMainWnd, WM_COMM_STOP, 0, 0 );
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

HBRUSH COption_ResultWStand::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() ==IDC_VOLT_NAME){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}
	if(pWnd->GetDlgCtrlID() ==IDC_VOLT_VALUE){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() ==IDC_MODEL_LNAME){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	//MES 관련
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_IPNAME)
	{
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB( 86,86,86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_PORTNAME4)
	{
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB( 86,86,86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_IPVAL)
	{
		pDC->SetTextColor(RGB( 86,86,86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_PORTVAL)
	{
		pDC->SetTextColor(RGB( 86,86,86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_BARCODE_NAME)
	{
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB( 86,86,86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_BARCODE_VAL){
		pDC->SetTextColor(RGB( 86,86,86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_LOTID_NAME)
	{
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB( 86,86,86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_LOTID_VAL){
		pDC->SetTextColor(RGB( 86,86,86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_TESTNUM_NAME)
	{
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB( 86,86,86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_TESTNUM_VAL){
		pDC->SetTextColor(RGB( 86,86,86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void COption_ResultWStand::UpdateEditRect(int parm_edit_id)
{
	CRect r;
	GetDlgItem(parm_edit_id)->GetWindowRect(r);
	// 에디트 좌표를 대화상자 기준으로 변환한다.
	ScreenToClient(r);
	// 다이얼로그의 에디트 컨트롤 영역을 갱신한다.
	InvalidateRect(r);
}

void COption_ResultWStand::Voltage_text(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_VOLT_NAME))->SetWindowText(lpcszString);
}

void COption_ResultWStand::Voltage_Value(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_VOLT_VALUE))->SetWindowText(lpcszString);
}

void COption_ResultWStand::Modelname_text(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_MODEL_LNAME))->SetWindowText(lpcszString);
}

void COption_ResultWStand::MESIP_TEXT(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_IPNAME))->SetWindowText(lpcszString);
}

void COption_ResultWStand::MESIP_VIEW(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_IPVAL))->SetWindowText(lpcszString);
}

void COption_ResultWStand::MESPORT_TEXT(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_PORTNAME4))->SetWindowText(lpcszString);
}

void COption_ResultWStand::MESPORT_VIEW(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_PORTVAL))->SetWindowText(lpcszString);
}

void COption_ResultWStand::MESBARCODE_TEXT(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_BARCODE_NAME))->SetWindowText(lpcszString);
}

void COption_ResultWStand::MESBARCODE_VIEW(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_BARCODE_VAL))->SetWindowText(lpcszString);
}

void COption_ResultWStand::LOTID_TEXT(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_LOTID_NAME))->SetWindowText(lpcszString);
}

void COption_ResultWStand::LOTID_VAL(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_LOTID_VAL))->SetWindowText(lpcszString);
}

void COption_ResultWStand::TESTNUM_TEXT(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_TESTNUM_NAME))->SetWindowText(lpcszString);
}

void COption_ResultWStand::TESTNUM_VAL(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_TESTNUM_VAL))->SetWindowText(lpcszString);
}

void COption_ResultWStand::OnEnSetfocusVoltName()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_ResultWStand::OnEnSetfocusVoltValue()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_ResultWStand::OnEnSetfocusModelLname()		{ ((CImageTesterDlg *)m_pMomWnd)->Focus_move_start(); }
void COption_ResultWStand::OnEnSetfocusEditIpname()		{ ((CImageTesterDlg *)m_pMomWnd)->Focus_move_start(); }
void COption_ResultWStand::OnEnSetfocusEditIpval()		{ ((CImageTesterDlg *)m_pMomWnd)->Focus_move_start(); }
void COption_ResultWStand::OnEnSetfocusEditPortname4()	{ ((CImageTesterDlg *)m_pMomWnd)->Focus_move_start(); }
void COption_ResultWStand::OnEnSetfocusEditPortval()	{ ((CImageTesterDlg *)m_pMomWnd)->Focus_move_start(); }
void COption_ResultWStand::OnEnSetfocusEditLotidName()	{ ((CImageTesterDlg *)m_pMomWnd)->Focus_move_start(); }
void COption_ResultWStand::OnEnSetfocusEditLotidVal()	{ ((CImageTesterDlg *)m_pMomWnd)->Focus_move_start(); }
void COption_ResultWStand::OnEnSetfocusEditTestnumName(){ ((CImageTesterDlg *)m_pMomWnd)->Focus_move_start(); }
void COption_ResultWStand::OnEnSetfocusEditTestnumVal() { ((CImageTesterDlg *)m_pMomWnd)->Focus_move_start(); }
void COption_ResultWStand::OnEnSetfocusEditBarcodeName(){ ((CImageTesterDlg *)m_pMomWnd)->Focus_move_start(); }
void COption_ResultWStand::OnEnSetfocusEditBarcodeVal() { ((CImageTesterDlg *)m_pMomWnd)->Focus_move_start(); }
