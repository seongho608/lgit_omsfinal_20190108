// Option_ResultOverlay.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Option_ResultOverlay.h"

// COption_ResultOverlay 대화 상자입니다.

IMPLEMENT_DYNAMIC(COption_ResultOverlay, CDialog)

COption_ResultOverlay::COption_ResultOverlay(CWnd* pParent /*=NULL*/)
	: CDialog(COption_ResultOverlay::IDD, pParent)
{

}

COption_ResultOverlay::~COption_ResultOverlay()
{
}

void COption_ResultOverlay::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LIST_CH1_OVERLAY_RESULT, m_listCh1OverlayResult);
	
}


BEGIN_MESSAGE_MAP(COption_ResultOverlay, CDialog)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_CH1_OVERLAY_RESULT, &COption_ResultOverlay::OnLvnItemchangedListCh1OverlayResult)
END_MESSAGE_MAP()

void COption_ResultOverlay::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}


// COption_ResultOverlay 메시지 처리기입니다.

BOOL COption_ResultOverlay::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_listCh1OverlayResult.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_listCh1OverlayResult.InsertColumn(0, "구분", LVCFMT_CENTER, 70);
	m_listCh1OverlayResult.InsertColumn(1, "결과", LVCFMT_CENTER, 40);
	m_listCh1OverlayResult.InsertColumn(2, "매칭률", LVCFMT_CENTER, 60);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL COption_ResultOverlay::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void COption_ResultOverlay::InitStat(){

	m_listCh1OverlayResult.DeleteAllItems();

}
void COption_ResultOverlay::OnLvnItemchangedListCh1OverlayResult(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}
