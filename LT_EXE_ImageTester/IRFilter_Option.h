#pragma once
#include "ETCUSER.H"
#include "afxcmn.h"

// CIRFilter_Option 대화 상자입니다.

class CIRFilter_Option : public CDialog
{
	DECLARE_DYNAMIC(CIRFilter_Option)

public:
	CIRFilter_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CIRFilter_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_IR_Filter };
	int m_CAM_SIZE_WIDTH;
	int m_CAM_SIZE_HEIGHT;

	CString C_filename;
	CRectData C_RECT[1];
	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
	void	StatePrintf(LPCSTR lpcszString, ...);
	tResultVal Run(void);
	void Pic(CDC *cdc);
	bool IR_TEST;

	bool AvePic(CDC *cdc,int NUM);
	bool AveGen(LPBYTE IN_RGB,int NUM);
	
	void InitPrm();
	void FAIL_UPLOAD();
	BOOL	b_StopFail;

	void	Save_parameter();
	void	Load_parameter();
	void	Save(int NUM);
	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void InsertList();
	int InsertIndex;
	int ListItemNum;
	int StartCnt;

	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();

	double d_R_Threshold;

	int m_FontSize;
	int m_PosX;
	int m_PosY;	

	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();

	int m_R_MaxThreshold;
	int m_R_MinThreshold;
	void EditRThrmChk();

	void	InitEVMS();
#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	int Lot_StartCnt;
	int Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

#pragma endregion

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
	CString		strTitle;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_IRFilterList;
	virtual BOOL OnInitDialog();
	CString str_R_Threshold;
	afx_msg void OnBnClickedButtonRthresave();
	afx_msg void OnEnChangeEditRThre();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnEnChangeEditFonti();
	afx_msg void OnEnChangeEditPosxi();
	afx_msg void OnEnChangeEditPosyi();
	afx_msg void OnBnClickedButtonSavei();
	CString str_Fontsize;
	CString str_PosX;
	CString str_PosY;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButtontest();
	CListCtrl m_Lot_IRlist;
	CString str_R_MinThreshold;
	afx_msg void OnEnChangeEditRThrmin();
	afx_msg void OnBnClickedButtontest2();
	afx_msg void OnBnClickedBtnLightRayOn();
	afx_msg void OnBnClickedBtnLightAllOff();
};

void CModel_Create(CIRFilter_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO);
void CModel_Delete(CIRFilter_Option **pWnd);
