// ModSelDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "ModSelDlg.h"


// CModSelDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CModSelDlg, CDialog)

CModSelDlg::CModSelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModSelDlg::IDD, pParent)
{
	m_DListCnt = -1;
	DSTItemIndex = -1;
	SRCItemIndex = -1;
	b_fullselct = 0;
}

CModSelDlg::~CModSelDlg()
{
}

void CModSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CModSelDlg, CDialog)
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_LIST_SRC, &CModSelDlg::OnNMClickListSrc)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_SRC, &CModSelDlg::OnNMDblclkListSrc)
	ON_BN_CLICKED(IDC_BUTTON_SELSAVE, &CModSelDlg::OnBnClickedButtonSelsave)
END_MESSAGE_MAP()


// CModSelDlg 메시지 처리기입니다.
void CModSelDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

BOOL CModSelDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);//|LVS_EX_HEADERDRAGDROP
	((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->InsertColumn(0,"",LVCFMT_CENTER,20);
	((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->InsertColumn(1,"",LVCFMT_CENTER,40);
	((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->InsertColumn(2,"TEST",LVCFMT_CENTER,200);

	
	((CListCtrl*)GetDlgItem(IDC_LIST_DST))->SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);//|LVS_EX_CHECKBOXES);
	((CListCtrl*)GetDlgItem(IDC_LIST_DST))->InsertColumn(0,"",LVCFMT_CENTER,40);
	((CListCtrl*)GetDlgItem(IDC_LIST_DST))->InsertColumn(1,"TEST",LVCFMT_CENTER,200);
	//((CListCtrl*)GetDlgItem(IDC_LIST_DST))->InsertColumn(2,"MES순서",LVCFMT_CENTER,65);

	for(int i= 0;i<20;i++){
		m_ModeSrcInfo[i].ID = 0;
		m_ModeSrcInfo[i].MODE = "";
		m_ModeSrcInfo[i].NAME = "";
		m_ModeSrcInfo[i].nMesIdx = 0;
		m_ModeSrcInfo[i].nMesNum = -1;
		m_ModeSrcInfo[i].ENABLE = FALSE;
	}

	for(int i= 0;i<20;i++){
		m_ModeDstInfo[i].ID = ((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[i].ID;
		m_ModeDstInfo[i].MODE = ((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[i].MODE;
		m_ModeDstInfo[i].NAME = ((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[i].NAME;
		m_ModeDstInfo[i].nMesIdx = ((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[i].nMesIdx;
		m_ModeDstInfo[i].nMesNum = ((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[i].nMesNum;
		m_ModeDstInfo[i].ENABLE = FALSE;
	}

	InitDstSet();
	InitEVMS();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CModSelDlg::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->EnableWindow(0);
	//	((CListCtrl*)GetDlgItem(IDC_LIST_DST))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_SELSAVE))->EnableWindow(0);
	}
}

BOOL CModSelDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		
		if (pMsg->wParam == VK_RETURN){
			if(((CButton *)GetDlgItem(IDC_BUTTON_SELSAVE))->IsWindowEnabled() == TRUE){
				OnBnClickedButtonSelsave();
			}
			return TRUE;
		}
		if(pMsg->wParam == VK_ADD){
//			::PostMessage(((CCenterPointModify_4CHDlg *)m_pMomWnd)->hMainWnd, WM_COMM_START, 0, 0 );
			return TRUE;
		}
		if (pMsg->wParam == VK_SUBTRACT){
//			::PostMessage(((CCenterPointModify_4CHDlg *)m_pMomWnd)->hMainWnd, WM_COMM_STOP, 0, 0 );
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CModSelDlg::OnBnClickedButtonSelsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	for(int lop= 0;lop<20;lop++){
		((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[lop].NAME = m_ModeDstInfo[lop].NAME;
		((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[lop].ID = m_ModeDstInfo[lop].ID;
		((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[lop].MODE = m_ModeDstInfo[lop].MODE;
		((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[lop].Number = m_ModeDstInfo[lop].Number;
		((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[lop].nMesIdx = m_ModeDstInfo[lop].nMesIdx;
		((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[lop].nMesNum = m_ModeDstInfo[lop].nMesNum;
	}
	((CImageTesterDlg *)m_pMomWnd)->ModelSave();
	((CImageTesterDlg *)m_pMomWnd)->ModelGen();
	((CButton *)GetDlgItem(IDC_BUTTON_SELSAVE))->EnableWindow(0);
	GotoDlgCtrl(((CButton *)GetDlgItem(IDC_BUTTON_SELSAVE)));
((CImageTesterDlg *)m_pMomWnd)->OnBnClickedBtnTestSetting();

	((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->ShowWindow(SW_HIDE);
	((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->ShowWindow(SW_SHOW);
}

void CModSelDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == 150){
		KillTimer(150);
		DstSet();
	}else if(nIDEvent == 160){
		KillTimer(160);
		DstSet();
	}
	CDialog::OnTimer(nIDEvent);
}

void CModSelDlg::InitSrcData()
{
	int ncnt = 0;	

	//-------------1----------------------
 		m_ModeSrcInfo[ncnt].ID = 0x03;
 		m_ModeSrcInfo[ncnt].MODE = _T("보임량 광축");
 		m_ModeSrcInfo[ncnt].NAME = _T("보임량 광축");
//  		m_ModeSrcInfo[ncnt].MODE = _T("SW광축보정");
//  		m_ModeSrcInfo[ncnt].NAME = _T("SW광축보정");
 		m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST2;
 		m_ModeSrcInfo[ncnt].nMesNum = 2;//4,5
 		ncnt++;	
	//-------------2----------------------
	m_ModeSrcInfo[ncnt].ID = 0x15;
	m_ModeSrcInfo[ncnt].MODE = "해상도 검사(SFR)";
	m_ModeSrcInfo[ncnt].NAME = "해상도 검사(SFR)";
	m_ModeSrcInfo[ncnt].nMesNum = 3;//4,5
	ncnt++;
	//-------------3----------------------
 	m_ModeSrcInfo[ncnt].ID = 0x04;
	m_ModeSrcInfo[ncnt].NAME = _T("Rotate 검사");
	m_ModeSrcInfo[ncnt].MODE = _T("Rotate 검사");
	m_ModeSrcInfo[ncnt].nMesNum = 4;//4,5
	//  	m_ModeSrcInfo[ncnt].NAME = _T("Rotate/Tilt 검사");
//  	m_ModeSrcInfo[ncnt].MODE = _T("Rotate/Tilt 검사");
 	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST7;
 	//m_ModeSrcInfo[ncnt].nMesNum = 11; //21
 	ncnt++;
	//-------------4----------------------
	m_ModeSrcInfo[ncnt].ID = 0x06;
	m_ModeSrcInfo[ncnt].MODE = _T("화각검사");
	m_ModeSrcInfo[ncnt].NAME = _T("화각검사");
	m_ModeSrcInfo[ncnt].nMesNum = 5;//4,5
	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST5;
	//m_ModeSrcInfo[ncnt].nMesNum = 9; //18,19
	ncnt++;
	//-------------5----------------------
	m_ModeSrcInfo[ncnt].ID = 0x0d;
	m_ModeSrcInfo[ncnt].NAME = _T("Distortion");
	m_ModeSrcInfo[ncnt].MODE = _T("Distortion");
	m_ModeSrcInfo[ncnt].nMesNum = 6;//4,5
	ncnt++;

	//-------------6----------------------
	m_ModeSrcInfo[ncnt].ID = 0x16;
	m_ModeSrcInfo[ncnt].NAME = _T("3D_Depth");
	m_ModeSrcInfo[ncnt].MODE = _T("3D_Depth");
	m_ModeSrcInfo[ncnt].nMesNum = 7;//4,5
	ncnt++;

	//-------------7----------------------
    m_ModeSrcInfo[ncnt].ID = 0x0a;//루리이물검사
//	m_ModeSrcInfo[ncnt].ID = 0x10;//이물검사_LG
	m_ModeSrcInfo[ncnt].MODE = _T("이물검사");
	m_ModeSrcInfo[ncnt].NAME = _T("이물검사");
	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST8;
	//m_ModeSrcInfo[ncnt].nMesNum = 4;//A //6
	//m_ModeSrcInfo[ncnt].nMesNum = 5;//B
	m_ModeSrcInfo[ncnt].nMesNum = 7;//4,5
	ncnt++;
	//-------------8----------------------
	m_ModeSrcInfo[ncnt].ID = 0x0b;
	m_ModeSrcInfo[ncnt].MODE = _T("광량비 검사");
	m_ModeSrcInfo[ncnt].NAME = _T("광량비 검사");
	//m_ModeSrcInfo[ncnt].nMesNum = 10;
	m_ModeSrcInfo[ncnt].nMesNum = 8;//4,5
	ncnt++;
	//-------------9----------------------
	m_ModeSrcInfo[ncnt].ID = 0x08;//AMH
	m_ModeSrcInfo[ncnt].MODE = _T("이물광원 SNR");
	m_ModeSrcInfo[ncnt].NAME = _T("이물광원 SNR");
	m_ModeSrcInfo[ncnt].nMesNum = 9;//4,5
	ncnt++;
	//-------------10----------------------
	m_ModeSrcInfo[ncnt].ID = 0x13;
	m_ModeSrcInfo[ncnt].NAME = _T("Dynamic Range");
	m_ModeSrcInfo[ncnt].MODE = _T("Dynamic Range");
	m_ModeSrcInfo[ncnt].nMesNum = 11;//4,5
	ncnt++;
	//-------------11----------------------//
	m_ModeSrcInfo[ncnt].ID = 0x0c;
	m_ModeSrcInfo[ncnt].MODE = _T("Fixed Pattern");
	m_ModeSrcInfo[ncnt].NAME = _T("Fixed Pattern");
	m_ModeSrcInfo[ncnt].nMesNum = 12;//4,5
	ncnt++;
	//-------------12----------------------//
	m_ModeSrcInfo[ncnt].ID = 0x17;
	m_ModeSrcInfo[ncnt].MODE = _T("HotPixel");
	m_ModeSrcInfo[ncnt].NAME = _T("HotPixel");
	m_ModeSrcInfo[ncnt].nMesNum = 7;//4,5
	ncnt++;
	//-------------13----------------------//
	m_ModeSrcInfo[ncnt].ID = 0x18;
	m_ModeSrcInfo[ncnt].MODE = _T("DepthNoise");
	m_ModeSrcInfo[ncnt].NAME = _T("DepthNoise");
	m_ModeSrcInfo[ncnt].nMesNum = 7;//4,5
	ncnt++;
	//-------------14----------------------//
	m_ModeSrcInfo[ncnt].ID = 0x19;
	m_ModeSrcInfo[ncnt].MODE = _T("TemperatureSensor");
	m_ModeSrcInfo[ncnt].NAME = _T("TemperatureSensor");
	m_ModeSrcInfo[ncnt].nMesNum = 2;//4,5
	ncnt++;


	//-------------8----------------------
	// 	m_ModeSrcInfo[ncnt].ID = 0x08;
	// 	m_ModeSrcInfo[ncnt].MODE = _T("저조도검사");//파이날 테스트의 기존 저조도 검사
	// 	m_ModeSrcInfo[ncnt].NAME = _T("저조도검사");
	// 	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST11;
	// 	//m_ModeSrcInfo[ncnt].nMesNum = 6; //8
	// 	ncnt++;
	//-------------2----------------------
	//m_ModeSrcInfo[ncnt].ID = 0x0e;
	//m_ModeSrcInfo[ncnt].MODE = _T("Color Reproduction");
	//m_ModeSrcInfo[ncnt].NAME = _T("Color Reproduction");
	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST3;
	//m_ModeSrcInfo[ncnt].nMesNum = 7; //9
	//ncnt++;
	//-------------3----------------------
	//m_ModeSrcInfo[ncnt].ID = 0x05;
	//m_ModeSrcInfo[ncnt].MODE = _T("De-Focus");
	//m_ModeSrcInfo[ncnt].NAME = _T("De-Focus");
	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST4;
	//m_ModeSrcInfo[ncnt].nMesNum = 8; //10~17
	//ncnt++;
	//-------------3----------------------
	//m_ModeSrcInfo[ncnt].ID = 0x11;
	//m_ModeSrcInfo[ncnt].MODE = _T("Overlay Position");
	//m_ModeSrcInfo[ncnt].NAME = _T("Overlay Position");
	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST6;
	//m_ModeSrcInfo[ncnt].nMesNum = 10; //20
	//ncnt++;
	//-------------8----------------------
	//m_ModeSrcInfo[ncnt].ID = 0x0f;
	//m_ModeSrcInfo[ncnt].MODE = "조향연동";
	//m_ModeSrcInfo[ncnt].NAME = "조향연동";
	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST9;
	//m_ModeSrcInfo[ncnt].nMesNum = 14;
	//ncnt++;
	//-------------9----------------------
	//m_ModeSrcInfo[ncnt].ID = 0x1f;
	//m_ModeSrcInfo[ncnt].MODE = "경고문구";
	//m_ModeSrcInfo[ncnt].NAME = "경고문구";
	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST10;
	//m_ModeSrcInfo[ncnt].nMesNum = 15;
	//ncnt++;
	//-------------11---------------------
	//m_ModeSrcInfo[ncnt].ID = 0x12;
	//m_ModeSrcInfo[ncnt].MODE = _T("저조도검사");//LG 추가된 검사
	//m_ModeSrcInfo[ncnt].NAME = _T("저조도검사");
	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST12;
	//m_ModeSrcInfo[ncnt].nMesNum = 1;//2
	//ncnt++;
	//-------------12----------------------//MCNEX 영상신호검사 수정
	//m_ModeSrcInfo[ncnt].ID = 0x01;
	//m_ModeSrcInfo[ncnt].MODE = _T("Peak검사");//영상신호검사
	//m_ModeSrcInfo[ncnt].NAME = _T("Peak검사");
	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST13;
	//m_ModeSrcInfo[ncnt].nMesNum = 5;//A //7
	////m_ModeSrcInfo[ncnt].nMesNum = 4;//B
	//ncnt++;
	//-------------13----------------------
	//m_ModeSrcInfo[ncnt].ID = 0x09;
	//m_ModeSrcInfo[ncnt].MODE = _T("IR filter 검사");
	//m_ModeSrcInfo[ncnt].NAME = _T("IR filter 검사");
	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST14;
	//m_ModeSrcInfo[ncnt].nMesNum = 2;//3
	//ncnt++;
	//-------------14----------------------//NEW
	//m_ModeSrcInfo[ncnt].ID = 0x1C;
	//m_ModeSrcInfo[ncnt].MODE = _T("버전 검사");
	//m_ModeSrcInfo[ncnt].NAME = _T("버전 검사");
	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST15;
	//m_ModeSrcInfo[ncnt].nMesNum = 12; //22
	//ncnt++;
	//-------------15----------------------//MCNEX 수정
	//m_ModeSrcInfo[ncnt].ID = 0x1b;
	//m_ModeSrcInfo[ncnt].MODE = "Mfgdata Write/Read";//시스템 진단
	//m_ModeSrcInfo[ncnt].NAME = "Mfgdata Write/Read";//시스템 진단
	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST16;
	//m_ModeSrcInfo[ncnt].nMesNum = 13; //23
	//ncnt++;
	//-------------16----------------------//MCNEX 수정
	//m_ModeSrcInfo[ncnt].ID = 0x1a;
	//m_ModeSrcInfo[ncnt].MODE = "DTC Clear";
	//m_ModeSrcInfo[ncnt].NAME = "DTC Clear";
	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST17;
	//m_ModeSrcInfo[ncnt].nMesNum = 16;
	//ncnt++;
	//-------------17----------------------//MCNEX 수정
	//m_ModeSrcInfo[ncnt].ID = 0x2f;
	//m_ModeSrcInfo[ncnt].MODE = "VERIFY";
	//m_ModeSrcInfo[ncnt].NAME = "VERIFY";
	//m_ModeSrcInfo[ncnt].nMesIdx = MES_TEST18;
	//m_ModeSrcInfo[ncnt].nMesNum = 17;
	//ncnt++;

	for(int lop= 0;lop<20;lop++)
	{
		if(m_ModeSrcInfo[lop].ID == 0)
		{
			m_ModeSrcInfo[lop].ENABLE = FALSE;
		}else
		{
			m_ModeSrcInfo[lop].Number = lop;
			m_ModeSrcInfo[lop].ENABLE = TRUE;
		}
	}
}

void CModSelDlg::InitDstSet()
{
	InitSrcData(); //기본 소스데이터를 검출한다. 소스들의 ENABLE을 모두 활성화한다. 

	for(int i= 0;i<20;i++){
		m_ModeDstInfo[i].ID = ((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[i].ID;
		m_ModeDstInfo[i].MODE = ((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[i].MODE;
		m_ModeDstInfo[i].NAME = ((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[i].NAME;
		m_ModeDstInfo[i].Number = ((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[i].Number;
		m_ModeDstInfo[i].nMesIdx = ((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[i].nMesIdx;
		m_ModeDstInfo[i].nMesNum = ((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[i].nMesNum;
		m_ModeDstInfo[i].ENABLE = FALSE;
		for(int lop2 = 0;lop2 < 20;lop2++){ //현재 쓰는것과 같은 ID가 있으면 ENABLE을 FALSE로 만든다. 
			if(m_ModeSrcInfo[lop2].ID == m_ModeDstInfo[i].ID ){
				m_ModeSrcInfo[lop2].ENABLE = FALSE;
				//if(m_ModeDstInfo[i].nMesNum < 1){
					m_ModeDstInfo[i].nMesNum = m_ModeSrcInfo[lop2].nMesNum;
					((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[i].nMesNum = m_ModeDstInfo[i].nMesNum;
				//}
			}
		}
	}
	InitSrcSet();
	DstSet();
}

void CModSelDlg::InitSrcSet()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->DeleteAllItems();
	m_SListCnt = -1;
	for(int lop = 0;lop <20;lop++){
		if(m_ModeSrcInfo[lop].ID != 0){
			m_SListCnt++;
			((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->InsertItem(m_SListCnt,"");
			str.Format("%d",lop+1);
			((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->SetItemText(m_SListCnt,1,str);
			str = m_ModeSrcInfo[lop].MODE;
			((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->SetItemText(m_SListCnt,2,str);
			if(m_ModeSrcInfo[lop].ENABLE == TRUE){
				((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->SetItemState(m_SListCnt,0x1000,LVIS_STATEIMAGEMASK);
			}else{
				((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->SetItemState(m_SListCnt,0x2000,LVIS_STATEIMAGEMASK);
			}
		}
	}
	SRCItemIndex = -1;
}

void CModSelDlg::DstSet()
{
	CString str = "";
	InitSrcData();
	m_DListCnt = -1;
	((CListCtrl*)GetDlgItem(IDC_LIST_DST))->DeleteAllItems();
	for(int lop = 0;lop <20;lop++){
		if((m_ModeSrcInfo[lop].ID != 0)&&(((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->GetItemState(lop,LVIS_STATEIMAGEMASK)) == 0x2000){
			m_DListCnt++;
			m_ModeDstInfo[m_DListCnt].ID = m_ModeSrcInfo[lop].ID;
			m_ModeDstInfo[m_DListCnt].MODE = m_ModeSrcInfo[lop].MODE;
			m_ModeDstInfo[m_DListCnt].NAME = m_ModeSrcInfo[lop].NAME;
			m_ModeDstInfo[m_DListCnt].nMesIdx = m_ModeSrcInfo[lop].nMesIdx;
			m_ModeDstInfo[m_DListCnt].nMesNum = m_ModeSrcInfo[lop].nMesNum;
			m_ModeDstInfo[m_DListCnt].Number = m_DListCnt;
			m_ModeSrcInfo[lop].ENABLE = FALSE;

			((CListCtrl*)GetDlgItem(IDC_LIST_DST))->InsertItem(m_DListCnt,"");
			str.Format("%d",m_DListCnt+1);
			((CListCtrl*)GetDlgItem(IDC_LIST_DST))->SetItemText(m_DListCnt,0,str);
			str = m_ModeDstInfo[m_DListCnt].NAME;
			((CListCtrl*)GetDlgItem(IDC_LIST_DST))->SetItemText(m_DListCnt,1,str);
			//str.Format("%d",m_ModeDstInfo[m_DListCnt].nMesNum);
			//((CListCtrl*)GetDlgItem(IDC_LIST_DST))->SetItemText(m_DListCnt,2,str);
		}
	}
	
	for(int lop2 = m_DListCnt+1;lop2<20;lop2++){
		m_ModeDstInfo[lop2].ID = 0;
		m_ModeDstInfo[lop2].MODE = "";
		m_ModeDstInfo[lop2].NAME = "";
		m_ModeDstInfo[lop2].nMesIdx = 0;
		m_ModeDstInfo[lop2].Number = -1;
		m_ModeDstInfo[lop2].nMesNum = -1;
	}
	

	InitSrcSet();

	if(ModelSettingChk() == TRUE){
		((CButton *)GetDlgItem(IDC_BUTTON_SELSAVE))->EnableWindow(0);
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_SELSAVE))->EnableWindow(1);
	}
	GotoDlgCtrl(((CListCtrl*)GetDlgItem(IDC_LIST_SRC)));
}

bool CModSelDlg::ModelSettingChk()//모두 같으면 TRUE 틀리면 FALSE
{
	bool retval = TRUE;
	for(int lop= 0;lop<20;lop++){
		if((((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[lop].NAME != m_ModeDstInfo[lop].NAME)||
		(((CImageTesterDlg *)m_pMomWnd)->m_ModelInfo[lop].ID != m_ModeDstInfo[lop].ID)){
			retval = FALSE;
		}
	}
	return retval;
}

void CModSelDlg::OnNMClickListSrc(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	SRCItemIndex = pNMItemActivate->iItem;
	if(SRCItemIndex != -1){
		UINT BUF = ((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->GetItemState(SRCItemIndex,LVIS_STATEIMAGEMASK);
		if(pNMItemActivate->iSubItem != 0){
			if( BUF == 0x2000){
				((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->SetItemState(SRCItemIndex,0x1000,LVIS_STATEIMAGEMASK);
			}else if(BUF == 0x1000){
				((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->SetItemState(SRCItemIndex,0x2000,LVIS_STATEIMAGEMASK);
			}
		}
		b_fullselct = 0;
	}else{
		if(b_fullselct == 0){
			((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->SetItemState(SRCItemIndex,0x2000,LVIS_STATEIMAGEMASK);
			b_fullselct = 1;
		}else if(b_fullselct == 1){
			((CListCtrl*)GetDlgItem(IDC_LIST_SRC))->SetItemState(SRCItemIndex,0x1000,LVIS_STATEIMAGEMASK);
			b_fullselct = 0;
		}
	}
	SetTimer(150,5,NULL); //InitProgram을 불러들이는데 쓰인다. 	
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}

void CModSelDlg::OnNMDblclkListSrc(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SRCItemIndex = pNMItemActivate->iItem;
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(SRCItemIndex != -1){
		if(pNMItemActivate->iSubItem == 0){
			SetTimer(160,100,NULL); //InitProgram을 불러들이는데 쓰인다. 
		}
	}
	*pResult = 0;
}
