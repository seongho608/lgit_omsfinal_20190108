// Particle_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Particle_Option.h"

// CParticle_Option 대화 상자입니다.

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
extern WORD	m_GRAYScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];
IMPLEMENT_DYNAMIC(CParticle_Option, CDialog)

CParticle_Option::CParticle_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CParticle_Option::IDD, pParent)
	, b_FailCheck(FALSE)
	, str_CaptureNum(_T(""))
	, str_Black_STEP(_T(""))
	, str_PassNum(_T(""))
	, str_CamBrightness(_T(""))
	, str_CamContrast(_T(""))
	, str_Separation_V(_T(""))
	, str_FailCnt(_T(""))
	, str_ImageSavePath(_T(""))
	, str_ExpGain(_T(""))
	, str_Concentration(_T(""))
	, b_ManualTestMode(FALSE)
{
	iSavedItem=0;
	iSavedSubitem=0;
	for(int t=0; t<3; t++){
		ChangeItem[t]=-1;
	}
	counter = 0;
	rect.x = 0;
	rect.y = 0;
	rect.height = 0;
	rect.width = 0;

	changecount=0;
	iSavedItem_click=0;
	i_DustDis[0] = 4;
	i_DustDis[1] = 4;
	i_DustDis[2] = 4;
	particle_start = FALSE;
	b_StopFail = FALSE;
}

CParticle_Option::~CParticle_Option()
{
}

void CParticle_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_RECT, m_RectList);
	DDX_Control(pDX, IDC_LIST_PARTICLE, m_ParticleList);
	DDX_Control(pDX, IDC_LIST_PARTICLE_LOT, m_Lot_ParticleList);
	DDX_Check(pDX, IDC_CHECK_FAIL, b_FailCheck);
	DDX_Text(pDX, IDC_EDIT_capture, str_CaptureNum);
	DDX_Text(pDX, IDC_P_STEP, str_Black_STEP);
	DDX_Text(pDX, IDC_EDIT_PASSNUM, str_PassNum);
	DDX_Text(pDX, IDC_EDIT_CAM_Bright2, str_CamBrightness);
	DDX_Text(pDX, IDC_EDIT_CAM_Contrast, str_CamContrast);
	DDX_Text(pDX, IDC_P_Separation, str_Separation_V);
	DDX_Text(pDX, IDC_P_FAIL, str_FailCnt);
	DDX_Control(pDX, IDC_COMBO_IMAGESAVE, m_Combo_ImageSave);
	DDX_Text(pDX, IDC_EDIT_IMAGESAVE_PATH, str_ImageSavePath);
	DDX_Text(pDX, IDC_EDIT_EXPGAIN, str_ExpGain);
	DDX_Text(pDX, IDC_EDIT_CONCENT, str_Concentration);
	DDX_Check(pDX, IDC_CHECK_MANUALTEST, b_ManualTestMode);
	DDX_Control(pDX, IDC_COMBO_IMAGE_SAVE, m_Cb_ImageSave);
}


BEGIN_MESSAGE_MAP(CParticle_Option, CDialog)
	ON_WM_CTLCOLOR()
	ON_NOTIFY(NM_CLICK, IDC_LIST_RECT, &CParticle_Option::OnNMClickListRect)
	ON_WM_TIMER()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_DEFAULT, &CParticle_Option::OnBnClickedButtonDefault)
	ON_BN_CLICKED(IDC_BUTTON_STEPDEFAULT, &CParticle_Option::OnBnClickedButtonStepdefault)
	ON_EN_SETFOCUS(IDC_EDIT_RECTLIST, &CParticle_Option::OnEnSetfocusEditRectlist)
	ON_EN_KILLFOCUS(IDC_EDIT_RECTLIST, &CParticle_Option::OnEnKillfocusEditRectlist)
	ON_BN_CLICKED(IDC_BUTTON_P_SAVE, &CParticle_Option::OnBnClickedButtonPSave)
	ON_WM_MOUSEWHEEL()
	ON_BN_CLICKED(IDC_BUTTON_LOAD, &CParticle_Option::OnBnClickedButtonLoad)
	ON_BN_CLICKED(IDC_BUTTON_LIST_SAVE, &CParticle_Option::OnBnClickedButtonListSave)
	ON_BN_CLICKED(IDC_BUTTON_CAM, &CParticle_Option::OnBnClickedButtonCam)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_RECT, &CParticle_Option::OnNMDblclkListRect)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_RECT, &CParticle_Option::OnNMCustomdrawListRect)
	ON_BN_CLICKED(IDC_CHECK_FAIL, &CParticle_Option::OnBnClickedCheckFail)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_OP, &CParticle_Option::OnBnClickedButtonSaveOp)
	ON_BN_CLICKED(IDC_BUTTON_PATHOPEN, &CParticle_Option::OnBnClickedButtonPathopen)
	ON_BN_CLICKED(IDC_BUTTON_GAINSAVE, &CParticle_Option::OnBnClickedButtonGainsave)
	ON_BN_CLICKED(IDC_BUTTON_CANCLE, &CParticle_Option::OnBnClickedButtonCancle)
	ON_BN_CLICKED(IDC_BUTTON_GAINAPPLY, &CParticle_Option::OnBnClickedButtonGainapply)
	ON_BN_CLICKED(IDC_BUTTON_CAMSET_PARTI, &CParticle_Option::OnBnClickedButtonCamsetParti)
	ON_BN_CLICKED(IDC_CHECK_MANUALTEST, &CParticle_Option::OnBnClickedCheckManualtest)
	ON_CBN_SELENDOK(IDC_COMBO_IMAGE_SAVE, &CParticle_Option::OnCbnSelendokComboImageSave)
END_MESSAGE_MAP()


// CParticle_Option 메시지 처리기입니다.
BOOL CParticle_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

//	Font_Size_Change(IDC_EDIT_OVERLAYNAME,&ft,100,30);
//	H_filename =((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	InitPrm();
	Set_List(&m_ParticleList);
	LOT_Set_List(&m_Lot_ParticleList);

	((CEdit *) GetDlgItem(IDC_EDIT_RECTLIST))->ShowWindow(FALSE);

	Rect_SETLIST();
	Load_parameter();
	Rect_InsertList();

	InitEVMS();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CParticle_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
		UpdateData(FALSE);
	}
}

BOOL CParticle_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}


HBRUSH CParticle_Option::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	// TODO:  여기서 DC의 특성을 변경합니다.
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CParticle_Option::OnNMClickListRect(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(pNMItemActivate->iSubItem == 0){
		iSavedItem_click = pNMItemActivate->iItem;
		ChangeCheck=1;
		SetTimer(150,5,NULL);  
		List_COLOR_Change(iSavedItem_click);
		UpdateData(FALSE);
	}
	*pResult = 0;
}

void CParticle_Option::OnBnClickedButtonDefault()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	for(int k=0; k<3; k++){
		Test_RECT[k].m_PosX = (CAM_IMAGE_WIDTH/2);
		Test_RECT[k].m_PosY = (CAM_IMAGE_HEIGHT/2);
		Test_RECT[k].m_Width = CAM_IMAGE_WIDTH-20;
		Test_RECT[k].m_Height = CAM_IMAGE_HEIGHT-20;
		Test_RECT[k].EX_RECT_SET(
			Test_RECT[k].m_PosX,
			Test_RECT[k].m_PosY,
			Test_RECT[k].m_Width,
			Test_RECT[k].m_Height
		);
	}
	Rect_InsertList();
}

void CParticle_Option::OnBnClickedButtonStepdefault()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Test_RECT[0].m_PosX = (CAM_IMAGE_WIDTH/2);
	Test_RECT[0].m_PosY = (CAM_IMAGE_HEIGHT/2);
	Test_RECT[0].m_Width = CAM_IMAGE_WIDTH - 20;
	Test_RECT[0].m_Height = CAM_IMAGE_HEIGHT - 20;

	Test_RECT[1].m_PosX = (CAM_IMAGE_WIDTH/2);
	Test_RECT[1].m_PosY = (CAM_IMAGE_HEIGHT/2);
	Test_RECT[1].m_Width = CAM_IMAGE_WIDTH *2/3;
	Test_RECT[1].m_Height = CAM_IMAGE_HEIGHT * 2 / 3;

	Test_RECT[2].m_PosX = (CAM_IMAGE_WIDTH/2);
	Test_RECT[2].m_PosY = (CAM_IMAGE_HEIGHT/2);
	Test_RECT[2].m_Width = CAM_IMAGE_WIDTH / 2;
	Test_RECT[2].m_Height = CAM_IMAGE_HEIGHT / 2;




	for(int k=0; k<3; k++){
		Test_RECT[k].EX_RECT_SET(
		Test_RECT[k].m_PosX,
		Test_RECT[k].m_PosY,
		Test_RECT[k].m_Width,
		Test_RECT[k].m_Height);
	}
	Rect_InsertList();
}

void CParticle_Option::OnEnSetfocusEditRectlist()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int num = iSavedItem;
	if ((Test_RECT[num].m_PosX >= 0) && (Test_RECT[num].m_PosX < CAM_IMAGE_WIDTH) &&
		(Test_RECT[num].m_PosY >= 0) && (Test_RECT[num].m_PosY < CAM_IMAGE_HEIGHT) &&
		(Test_RECT[num].m_Left >= 0) && (Test_RECT[num].m_Left < CAM_IMAGE_WIDTH) &&
		(Test_RECT[num].m_Top >= 0) && (Test_RECT[num].m_Top  < CAM_IMAGE_HEIGHT) &&
		(Test_RECT[num].m_Right >= 0) && (Test_RECT[num].m_Right< CAM_IMAGE_WIDTH) &&
		(Test_RECT[num].m_Bottom >= 0) && (Test_RECT[num].m_Bottom<CAM_IMAGE_HEIGHT) &&
		(Test_RECT[num].m_Width >= 0) && (Test_RECT[num].m_Width<CAM_IMAGE_WIDTH) &&
		(Test_RECT[num].m_Height >= 0) && (Test_RECT[num].m_Height<CAM_IMAGE_HEIGHT) &&
		(m_i_DustNumOfSector[num]>=0)&&(m_i_DustNumOfSector[num]<=50)&&
		(m_d_Thr[num] >=0)&&(m_d_Thr[num]<=50)&&
		(m_d_OvershootingThr[num]>=0)&&
		(m_d_OvershootingThr[num]<=999)&&
		(m_stainBlemish[num] >= 0) && (m_stainBlemish[num] <= 100)){
			((CButton *)GetDlgItem(IDC_BUTTON_LIST_SAVE))->EnableWindow(1);
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_LIST_SAVE))->EnableWindow(0);
	}
}

void CParticle_Option::OnEnKillfocusEditRectlist()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	ChangeCheck=1;
	if((iSavedItem != -1)&&(iSavedSubitem != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_RECTLIST, str));
			List_COLOR_Change(iSavedItem);
		if(m_RectList.GetItemText(iSavedItem, iSavedSubitem) != str){
			Change_DATA();
			Rect_InsertList();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );
	iSavedItem = -1;
	iSavedSubitem = -1;
}

void CParticle_Option::OnBnClickedButtonPSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	CString str_buf = "";
	CString str_buf2 = "";
	CString str = "";
	int buf = 0;
	double buf2 = 0.0;
	
	buf = atoi(str_CaptureNum);
	m_CaptureNum = buf;

	buf = atoi(str_Black_STEP);
	m_Black_STEP = buf;

	buf = atoi(str_PassNum);
	m_PASSNUM = buf;

	/*buf = atoi(str_Separation_V);
	m_Separation_V = buf;*/

	buf = atoi(str_FailCnt);
	m_Fail_Retry_Cnt = buf;

	/*buf2 = atof(str_Concentration);
	m_stainBlemish = buf2;*/


	str_Black_STEP.Format("%d",m_Black_STEP);
	str_PassNum.Format("%d",m_PASSNUM);
	str_Separation_V.Format("%d",m_Separation_V);
	str_CaptureNum.Format("%d",m_CaptureNum);
	str_FailCnt.Format("%d",m_Fail_Retry_Cnt);
//	str_Concentration.Format("%.1f",m_stainBlemish);

	UpdateData(FALSE);

	str.Empty();
	str.Format("%d",m_Black_STEP);
	WritePrivateProfileString("PARTICLEOPT","BK_STEP",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);	
	
	str.Empty();
	str.Format("%d",m_Separation_V);
	WritePrivateProfileString("PARTICLEOPT","Separation",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	
	str.Empty();
	str.Format("%d",m_PASSNUM);
	WritePrivateProfileString("PARTICLEOPT","PassNum",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	
	str.Empty();
	str.Format("%d",m_CaptureNum);
	WritePrivateProfileString("PARTICLEOPT","Capture",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	str.Empty();
	str.Format("%d",m_Fail_Retry_Cnt);
	WritePrivateProfileString("PARTICLEOPT","Fail_Retry",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	/*str.Empty();
	str.Format("%.1f",m_stainBlemish);
	WritePrivateProfileString("PARTICLEOPT","Concentration",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);*/

	FULL_RegionSUM();
	DustFilterSum();
}



void CParticle_Option::OnBnClickedButtonLoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Load_parameter();
	UpdateData(FALSE);
	Rect_InsertList();
	ChangeCheck =0;
	for(int t=0; t<3; t++){
		ChangeItem[t]=-1;
	}
	for(int t=0; t<3; t++){
		m_RectList.Update(t);
	}
}

void CParticle_Option::OnBnClickedButtonListSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Save_parameter();
	Rect_InsertList();

	ChangeCheck =0;
	for(int t=0; t<3; t++){
		ChangeItem[t]=-1;
	}
	for(int t=0; t<3; t++){
		m_RectList.Update(t);
	}
}

void CParticle_Option::OnBnClickedButtonCam()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int bright= atoi(str_CamBrightness);

	if(bright >=255){
		bright = 255;
		str_CamBrightness.Format("%d",bright);
	}
	if(bright <0){
		bright =0;
		str_CamBrightness.Format("%d",bright);
	}

	str_CamBrightness.Format("%d",bright);

	int Contrast= atoi(str_CamContrast);

	if(Contrast >=255){
		Contrast = 255;
		str_CamContrast.Format("%d",Contrast);
	}
	if(Contrast <0){
		Contrast =0;
		str_CamContrast.Format("%d",Contrast);
	}

	str_CamContrast.Format("%d",Contrast);

	m_Brightness = bright;
	m_Contrast = Contrast;
	
	
	CString str ="";


	
	str.Empty();
	str.Format("%d",m_Brightness);
	WritePrivateProfileString("PARTICLEOPT","Bright",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	
	str.Empty();
	str.Format("%d",m_Contrast);
	WritePrivateProfileString("PARTICLEOPT","Contrast",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	//((CImageTesterDlg *)m_pMomWnd)->ComArtWnd ->ViewChange(BYTE(m_Brightness),BYTE(m_Contrast));
	UpdateData(FALSE);
}

void CParticle_Option::OnNMDblclkListRect(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(pNMITEM->iSubItem == 0 || pNMITEM->iSubItem == -1 || pNMITEM->iItem == -1 ){return ;}
	
	iSavedItem = pNMITEM->iItem;
	iSavedSubitem = pNMITEM->iSubItem;
	
	CRect rect;
	if(pNMITEM->iItem != -1)
	{
		if(pNMITEM->iSubItem != 0)
		{		
			m_RectList.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			m_RectList.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowText(m_RectList.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
			((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetFocus();			
		}
	}
	*pResult = 0;
}

void CParticle_Option::OnNMCustomdrawListRect(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//*pResult = 0;
	COLORREF text_color = 0;
	COLORREF bg_color = RGB(255, 255, 255);

	LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
	
	switch(lplvcd->nmcd.dwDrawStage){
        case CDDS_PREPAINT:
            *pResult = CDRF_NOTIFYITEMDRAW;
            return;
         
        // 배경 혹은 텍스트를 수정한다.
        case CDDS_ITEMPREPAINT:
            // 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
            *pResult = CDRF_NOTIFYITEMDRAW;
            return;
       
        // 서브 아이템의 배경 혹은 텍스트를 수정한다.
        case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
            if(lplvcd->iSubItem != 0){
                // 1번째 행이라면...

				if(lplvcd->nmcd.dwItemSpec >=0 ||lplvcd->nmcd.dwItemSpec <3){
					

					if(ChangeCheck==1){
						if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 13 ){
							for(int t=0; t<3; t++){
								if(lplvcd->nmcd.dwItemSpec == ChangeItem[t]){
									text_color = RGB(0, 0, 0);
									bg_color = RGB(250, 230, 200);					
								}
							}
						}
					
					
					}
				
				}
				

			    lplvcd->clrText = text_color;
                lplvcd->clrTextBk = bg_color;
				
            }
            *pResult = CDRF_NEWFONT;
            return;
    }
}

BOOL CParticle_Option::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// 이 기능을 사용하려면 Windows Vista 이상이 있어야 합니다.
	// _WIN32_WINNT 기호는 0x0600보다 크거나 같아야 합니다.
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	int znum = zDelta/120;
	CWnd *focusid;
	focusid = GetFocus();
	bool FLAG=0;
	if(znum > 0 ){
		FLAG = 1;
	}
	else if(znum < 0 ){
		FLAG =0;
	}
	else if(znum == 0){
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	}
	int casenum = focusid->GetDlgCtrlID();
	switch(casenum){
		case IDC_EDIT_RECTLIST :
			if(FLAG == 1){
				if((Change_DATA_CHECK(1) == TRUE) /*|| (znum ==-1)*/){
					EditWheelIntSet(IDC_EDIT_RECTLIST,znum);
					Change_DATA();
					Rect_InsertList();
					OnEnSetfocusEditRectlist();
				}
			}
			if(FLAG == 0){
				if((Change_DATA_CHECK(0) == TRUE) /*|| (znum == 1)*/){
					EditWheelIntSet(IDC_EDIT_RECTLIST,znum);
					Change_DATA();
					Rect_InsertList();
					OnEnSetfocusEditRectlist();
				}
			}
			break;
	}
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

void CParticle_Option::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	UINT BUF = 0;
	if(nIDEvent == 150){
		KillTimer(150);
		BUF = m_RectList.GetItemState(iSavedItem_click,LVIS_STATEIMAGEMASK);
		if( BUF == 0x2000){
			m_b_Ellipse[iSavedItem_click] = TRUE;
		}else if(BUF == 0x1000){
			m_b_Ellipse[iSavedItem_click] = FALSE;
		}
	}else if(nIDEvent == 160){
		KillTimer(160);
		BUF = m_RectList.GetItemState(iSavedItem_click,LVIS_STATEIMAGEMASK);
		if(BUF == 0x2000){
			m_b_Ellipse[iSavedItem_click] = TRUE;
		}else if(BUF == 0x1000){
			m_b_Ellipse[iSavedItem_click] = FALSE;
		}
	}
	CDialog::OnTimer(nIDEvent);
}

void CParticle_Option::OnBnClickedCheckFail()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	b_FailCheck = b_FailCheck;
}


CString CParticle_Option::Mes_Result()
{
	CString sz;
	m_szMesResult = _T("");
	if(b_StopFail == FALSE){
		m_szMesResult.Format(_T("%s:%s"),Str_Mes[0],Str_Mes[1]);
		m_szMesResult.Replace(" ","");
	}else{
		m_szMesResult.Format(_T(":1"));
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_stNewMesData[NewMES_Particle] = m_szMesResult;

	return m_szMesResult;
}

void CParticle_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CButton *)GetDlgItem(IDC_BUTTON_LOAD))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_LIST_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_DEFAULT))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_STEPDEFAULT))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_P_SAVE))->EnableWindow(0);
	
		((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_capture))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_PASSNUM))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_P_STEP))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_P_Separation))->EnableWindow(0);
	}
}

void CParticle_Option::FAIL_UPLOAD(){
//	if (((CImageTesterDlg  *)m_pMomWnd)->LotMod == 1){
// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 			LOT_InsertDataList();
// 			m_Lot_ParticleList.SetItemText(Lot_InsertIndex, 6, "FAIL");
// 			m_Lot_ParticleList.SetItemText(Lot_InsertIndex, 7, "STOP");
// 			Lot_StartCnt++;
// 			b_StopFail = TRUE;
// 		}
// 		else{
	
			InsertList();
			m_ParticleList.SetItemText(InsertIndex, 4, "X");
			m_ParticleList.SetItemText(InsertIndex, 5, "FAIL");
			m_ParticleList.SetItemText(InsertIndex, 6, "STOP");
			Str_Mes[0] = "FAIL";
			Str_Mes[1] = "0";
			((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_DUST, "STOP");
			StartCnt++;
			b_StopFail = TRUE;
/*		}*/
//	}
	
}

bool CParticle_Option::EXCEL_UPLOAD(){
	m_ParticleList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("이물검사",&m_ParticleList,1,&StartCnt)==TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
}

void CParticle_Option::Set_List(CListCtrl *List){
	
	CString str ="";
	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
//	List->InsertColumn(2, "LOT CARD", LVCFMT_CENTER, 100);
	List->InsertColumn(2, "Start DATE", LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"VAL",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"result",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"비고",LVCFMT_CENTER, 80);
	
	ListItemNum=7;
	Copy_List(&m_ParticleList, ((CImageTesterDlg *)m_pMomWnd)->m_pWorkList, ListItemNum);
}

void CParticle_Option::EXCEL_SAVE()
{
	CString Item[1]={"VAL"};
	CString Data ="";
	CString str="";
	str = "***********************이물 검사***********************\n";
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("이물검사",Item,1,&m_ParticleList,Data);
}

void CParticle_Option::InsertList()
{
	CString strCnt;

	InsertIndex = m_ParticleList.InsertItem(StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_ParticleList.SetItemText(InsertIndex,0,strCnt);
	
	m_ParticleList.SetItemText(InsertIndex, 1, ((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_ParticleList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_ParticleList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");
}

void CParticle_Option::LOT_Set_List(CListCtrl *List){

	/*CString testD[18] = {"[BS]DefectCount","[BS]SingleDefectNum","[BS]MaxValue","[BS]PosX","[BS]PosY","[BS]Result",
							"[ST]SingleDefectCount","[ST]BlobCount","[ST]CenterCount","[ST]MaxValue","[ST]PosX","[ST]PosY"
							,"[ST]EdgeCount","[ST]MaxValue","[ST]PosX","[ST]PosY","[ST]DefectCount","[ST]Result"}; */

	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT 명",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"VAL",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"result",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"비고",LVCFMT_CENTER, 80);
	
	Lot_InsertNum =8;
	//Copy_List(&m_Lot_ParticleList, ((CImageTesterDlg *)m_pMomWnd)->m_pWorkList, Lot_InsertNum);
}

void CParticle_Option::LOT_InsertDataList()
{
	CString strCnt = "";

	int Index = m_Lot_ParticleList.InsertItem(Lot_StartCnt, "", 0);
	//Lot_InsertIndex = m_Lot_ParticleList.InsertItem(Lot_StartCnt, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Lot_InsertIndex + 1);
	m_Lot_ParticleList.SetItemText(Lot_InsertIndex, 0, strCnt);

// 	m_Lot_ParticleList.SetItemText(Lot_InsertIndex, 1, ((CImageTesterDlg *)m_pMomWnd)->m_modelName);
// 	m_Lot_ParticleList.SetItemText(Lot_InsertIndex, 2, ((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);////////LOT 이름
// 	m_Lot_ParticleList.SetItemText(Lot_InsertIndex, 3, ((CImageTesterDlg *)m_pMomWnd)->strDay + "");
// 	m_Lot_ParticleList.SetItemText(Lot_InsertIndex, 4, ((CImageTesterDlg *)m_pMomWnd)->strTime + "");

	int CopyIndex = m_ParticleList.GetItemCount() - 1;

	int count = 0;
	for (int t = 0; t< Lot_InsertNum; t++){
		if ((t != 2) && (t != 0)){
			m_Lot_ParticleList.SetItemText(Index, t, m_ParticleList.GetItemText(CopyIndex, count));
			count++;

		}
		else if (t == 0){
			count++;
		}
		else{
			m_Lot_ParticleList.SetItemText(Index, t, ((CImageTesterDlg  *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;

}
void CParticle_Option::LOT_EXCEL_SAVE()
{
	CString Item[1]={"VAL"};
	CString Data ="";
	CString str="";
	str = "***********************이물 검사***********************\n";
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("이물검사",Item,1,&m_Lot_ParticleList,Data);
}

bool CParticle_Option::LOT_EXCEL_UPLOAD()
{
	m_Lot_ParticleList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("이물검사",&m_Lot_ParticleList,1,&Lot_StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}

void CParticle_Option::InitPrm()
{
	Load_parameter();
	UpdateData(FALSE);
}

void CParticle_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째라인의앞부분을선택한다.  
		pTStat -> Clear();			   //라인하나를지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

tResultVal CParticle_Option::Run()
{	
	CString str_Data = "";
	CString str_Pos = "";
	CString str_OutData = "";

	tResultVal retval = {0,};
	BOOL FLAG = FALSE;
	b_StopFail = FALSE;
	particle_start = FALSE;
	((CImageTesterDlg *)m_pMomWnd)->b_Particle_fail = FALSE;

	BOOL b_FailMode = TRUE;

	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

		InsertList();
	}

	
	int FailCount =0;

	for(int i=0; i<256; i++)
	{
		P_RECT[i].m_Left =0;
		P_RECT[i].m_Right =0;
		P_RECT[i].m_Top =0;
		P_RECT[i].m_Bottom =0;
		P_RECT[i].m_PosX =0;
		P_RECT[i].m_PosY =0;
	}

	while(b_FailMode){


		

		ParticleCnt = 0; 
		FLAG = FALSE;
#if 0 //1: 8bit
		CaptureImage();//영상을 추출한다. 
		FLAG = ParticleGen(m_CaptureBuf, 0);
#else
		CaptureImage_16bit();//영상을 추출한다. 
		FLAG = ParticleGen_16bit(m_CaptureBuf_16bit,m_CaptureBuf, 0);
#endif

		particle_start = TRUE;

		for(int i=0; i<ParticleCnt; i++)
		{
			if(i>=256){
				i = 255;
			}
			
			int x = (P_RECT[i].m_Left+P_RECT[i].m_Right)/2;
			int y = (P_RECT[i].m_Top+P_RECT[i].m_Bottom)/2;

			str_Pos.Format("X_%d, Y_%d", x, y);
			str_Data.Format("%0.3f", P_RECT[i].m_Concentration);
			str_OutData = "이물 위치 : "+str_Pos+" 변화량 : "+str_Data+"\n"; 

			((CImageTesterDlg *)m_pMomWnd)->StatePrintf(str_OutData);
		}
// 		if(FLAG == FALSE){ //이물 FAIL 시 
// 
// 			str =((CImageTesterDlg *)m_pMomWnd)->m_szCode+_T("_")+((CImageTesterDlg *)m_pMomWnd)->m_strdegre;//생성 폴더명
// 			((CImageTesterDlg *)m_pMomWnd)->CREATE_Folder(str,5);
// 			SAVEDATA("RAW_DATA");//DATA저장
// 			((CImageTesterDlg *)m_pMomWnd)->Original_ImageCapture();//ori,pic,UI 캡처
// 			((CImageTesterDlg *)m_pMomWnd)->b_Particle_fail = TRUE; // MES 데이터 복사
// 		}
		((CImageTesterDlg *)m_pMomWnd)->TestImageSaveMode(m_nImageSaveMode, m_CaptureBuf_16bit, _T("Particle"));

		if ((b_ManualTestMode == 1))
		{

			((CImageTesterDlg *)m_pMomWnd)->b_Particle = TRUE;
			if (R_RECT[0].m_Success == TRUE)
			{
				retval.ValString.Format("OK");
				retval.m_Success = TRUE;

				if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

					m_ParticleList.SetItemText(InsertIndex, 5, "PASS");
				}
				//	StatePrintf("이물 측정 PASS");

			}
			else
			{
				if (((CImageTesterDlg *)m_pMomWnd)->b_UserMode == 0){
					retval.ValString.Format("이물 있음");

				}
				else{
					retval.ValString.Format("이물이 %d개 발견 ", ParticleCnt);

				}
				//	StatePrintf("이물이 %d개 발견되었습니다. ",ParticleCnt);


				retval.m_Success = FALSE;
				if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

					m_ParticleList.SetItemText(InsertIndex, 5, "FAIL");
				}
			}



			if (retval.m_Success == TRUE)
			{
				((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->TestState(0, "PASS");
			}
			else
			{
				((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->TestState(1, "FAIL");
			}

			((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->TextView(retval.ValString);
			((CImageTesterDlg *)m_pMomWnd)->b_Particle = TRUE;
			((CImageTesterDlg *)m_pMomWnd)->m_ParticleView = new CParticle_View(((CImageTesterDlg *)m_pMomWnd));
			((CImageTesterDlg *)m_pMomWnd)->m_ParticleView->Setup(((CImageTesterDlg *)m_pMomWnd));
			if (((CImageTesterDlg *)m_pMomWnd)->m_ParticleView->DoModal() == IDOK){
				FLAG = TRUE;
				((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptWnd->RESULT_TEXT(1, "PASS");
			}
			else{
				FLAG = FALSE;
				((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptWnd->RESULT_TEXT(2, "FAIL");
			}
			delete ((CImageTesterDlg *)m_pMomWnd)->m_ParticleView;
			((CImageTesterDlg *)m_pMomWnd)->m_ParticleView = NULL;

			m_ParticleList.SetItemText(InsertIndex, 6, "수동모드");

		}


		CString str;
		str = "";
// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 
// 			if (FLAG == TRUE){
// 				retval.m_Success = TRUE;
// 				retval.ValString.Empty();
// 				retval.ValString.Format("TEST SUCCESS");
// 				((CImageTesterDlg  *)m_pMomWnd)->m_pResParticleOptWnd->RESULT_TEXT(1, "PASS");
// 				str.Format("%d", ParticleCnt);
// 				((CImageTesterDlg  *)m_pMomWnd)->m_pResParticleOptWnd->PARTICLENUM_TEXT(str);
// 				m_Lot_ParticleList.SetItemText(Lot_InsertIndex, 5, str);
// 				m_Lot_ParticleList.SetItemText(Lot_InsertIndex, 6, "PASS");
// 			}
// 			else{
// 				retval.m_Success = FALSE;
// 				retval.ValString.Empty();
// 				retval.ValString.Format("TEST FAIL");
// 				((CImageTesterDlg  *)m_pMomWnd)->m_pResParticleOptWnd->RESULT_TEXT(2, "FAIL");
// 				str.Format("%d", ParticleCnt);
// 				((CImageTesterDlg  *)m_pMomWnd)->m_pResParticleOptWnd->PARTICLENUM_TEXT(str);
// 				m_Lot_ParticleList.SetItemText(Lot_InsertIndex, 5, str);
// 				m_Lot_ParticleList.SetItemText(Lot_InsertIndex, 6, "FAIL");
// 			}
// 			Lot_StartCnt++;
// 		}
// 		else{
		
			if (FLAG == TRUE){
				retval.m_Success = TRUE;
				retval.ValString.Empty();
				retval.ValString.Format("TEST SUCCESS");
				((CImageTesterDlg  *)m_pMomWnd)->m_pResParticleOptWnd->RESULT_TEXT(1, "PASS");
				str.Format("%d", ParticleCnt);
				((CImageTesterDlg  *)m_pMomWnd)->m_pResParticleOptWnd->PARTICLENUM_TEXT(str);
				if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
					m_ParticleList.SetItemText(InsertIndex, 4, str);
					m_ParticleList.SetItemText(InsertIndex, 5, "PASS");
					((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_DUST, "PASS");
					Str_Mes[0] = str;
					Str_Mes[1] = "1";
				}
			}
			else{
				retval.m_Success = FALSE;
				retval.ValString.Empty();
				retval.ValString.Format("TEST FAIL");
				((CImageTesterDlg  *)m_pMomWnd)->m_pResParticleOptWnd->RESULT_TEXT(2, "FAIL");
				str.Format("%d", ParticleCnt);
				((CImageTesterDlg  *)m_pMomWnd)->m_pResParticleOptWnd->PARTICLENUM_TEXT(str);
				if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
					m_ParticleList.SetItemText(InsertIndex, 4, str);
					m_ParticleList.SetItemText(InsertIndex, 5, "FAIL");
					((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_DUST, "FAIL");
					Str_Mes[0] = str;
					Str_Mes[1] = "0";
				}
			}
			StartCnt++;
//		}
		


		Image_Save(m_ImageSaveMode, FLAG);


		if (FLAG == TRUE || m_Fail_Retry_Cnt == 0)
		{
			b_FailMode = FALSE;

			break;
		}else{
			b_FailMode = TRUE;
			
		}
		FailCount++;

		if (FailCount == m_Fail_Retry_Cnt || m_Fail_Retry_Cnt == 0)
		{
			b_FailMode = TRUE;
			break;
		}
	}



	return retval;
	
}	
void CParticle_Option::SAVEDATA(LPCSTR lpcszString, ...)
{
	CString str_Data = "";
	CString str_Pos = "";
	CString str_OutData = "";
	CStdioFile File;
	CString strFilename ="";
	CString strOut ="";
	CFileFind filefind;

	CString str="";int length=0;
	TCHAR			lpszBuf[256];

	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);

	strFilename = ((CImageTesterDlg *)m_pMomWnd)->MODEL_FOLDERPATH_P +("\\") + ("%s",lpszBuf)+(".csv");

	CFileException e;


	bool FLAG = TRUE;

	if(!File.Open((LPCTSTR)strFilename,CFile::modeCreate|CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
		CString strFilename2 = ((CImageTesterDlg *)m_pMomWnd)->MODEL_FOLDERPATH_P +("\\") + ("%s",lpszBuf)+("_bak.csv");
		if(!File.Open((LPCTSTR)strFilename2,CFile::modeCreate|CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
			return;

		}
	}

	strOut ="";

	str.Empty();
	str= L"NO ,";
	strOut+= str;
	str.Empty();
	str=L"이물위치_X ,";
	strOut+= str;
	str.Empty();
	str=L"이물위치_Y ,";
	strOut+= str;
	str.Empty();
	str=L"변화량\n";
	strOut+= str;

	for(int i=0; i<ParticleCnt; i++)
	{
		if(i>=256){
			i = 255;
		}
		str.Empty();
		str.Format("%d ,",i+1);
		strOut+= str;
		str.Empty();
		str.Format("%d ,",P_RECT[i].m_PosX);
		strOut+= str;
		str.Empty();
		str.Format("%d ,",P_RECT[i].m_PosY);
		strOut+= str;
		str.Empty();
		str.Format("%0.3f", P_RECT[i].m_Concentration);
		strOut+= str;
		str.Empty();
		str=L"\n";
		strOut+= str;
	}

	File.WriteString(LPCTSTR(strOut));

	File.Close();
}
void CParticle_Option::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}


void CParticle_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
	
}

void CParticle_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
	strTitle.Empty();
	strTitle="PARTICLE_";
}

/////////////////////////////////////////////////////////////////////
void CParticle_Option::Rect_SETLIST(){

	m_RectList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);
	m_RectList.InsertColumn(0, "구분", LVCFMT_CENTER, 90);
	m_RectList.InsertColumn(1, "PosX", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(2, "PosY", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(3, "St X", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(4, "St Y", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(5, "Ex X", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(6, "Ex Y", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(7, "W", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(8, "H", LVCFMT_CENTER, 40);

	/*m_RectList.InsertColumn(9, "Dust of Sector", LVCFMT_CENTER, 90);*/
	m_RectList.InsertColumn(9, "Threshold(흑점)", LVCFMT_CENTER, 130);
	//m_RectList.InsertColumn(10, "Overshoot Thr", LVCFMT_CENTER, 90);
	m_RectList.InsertColumn(10, "농도(멍)", LVCFMT_CENTER, 60);
	m_RectList.InsertColumn(11, "크기(멍)", LVCFMT_CENTER, 60);
}

void CParticle_Option::Change_DATA(){

		CString str;
		
		((CEdit *)GetDlgItemText(IDC_EDIT_RECTLIST, str));
		m_RectList.SetItemText(iSavedItem, iSavedSubitem, str);

		double num = atof(str);
		if(num < 0){
			num =0;
		}

		if(iSavedSubitem ==1){
			Test_RECT[iSavedItem].m_PosX = (int)num;
			
		}
		if(iSavedSubitem ==2){
			Test_RECT[iSavedItem].m_PosY = (int)num;
			
		}
		if(iSavedSubitem ==3){
			Test_RECT[iSavedItem].m_Left = (int)num;
			Test_RECT[iSavedItem].m_PosX=(Test_RECT[iSavedItem].m_Right+1 + Test_RECT[iSavedItem].m_Left) /2;
			Test_RECT[iSavedItem].m_Width=Test_RECT[iSavedItem].m_Right+1 - Test_RECT[iSavedItem].m_Left;

		}
		if(iSavedSubitem ==4){
			Test_RECT[iSavedItem].m_Top = (int)num;
			Test_RECT[iSavedItem].m_PosY=(Test_RECT[iSavedItem].m_Top + Test_RECT[iSavedItem].m_Bottom+1)/2;
			Test_RECT[iSavedItem].m_Height= Test_RECT[iSavedItem].m_Bottom+1 - Test_RECT[iSavedItem].m_Top;

		}
		if(iSavedSubitem ==5){
			if(num == 0){
				Test_RECT[iSavedItem].m_Right = 0;
			}
			else{
				Test_RECT[iSavedItem].m_Right = (int)num;
				Test_RECT[iSavedItem].m_PosX = (Test_RECT[iSavedItem].m_Right + Test_RECT[iSavedItem].m_Left+1) /2;
				Test_RECT[iSavedItem].m_Width = Test_RECT[iSavedItem].m_Right+1 - Test_RECT[iSavedItem].m_Left;
			}
		}
		if(iSavedSubitem ==6){
			if(num ==0){
				Test_RECT[iSavedItem].m_Bottom =0;
			}
			else{
				Test_RECT[iSavedItem].m_Bottom = (int)num;
				Test_RECT[iSavedItem].m_PosY = (Test_RECT[iSavedItem].m_Top + Test_RECT[iSavedItem].m_Bottom+1)/2;
				Test_RECT[iSavedItem].m_Height = Test_RECT[iSavedItem].m_Bottom+1 - Test_RECT[iSavedItem].m_Top;
			}
		}
		if(iSavedSubitem ==7){
			Test_RECT[iSavedItem].m_Width = (int)num;
		}
		if(iSavedSubitem ==8){
			Test_RECT[iSavedItem].m_Height = (int)num;
		}
		/*if(iSavedSubitem ==9){
			m_i_DustNumOfSector[iSavedItem] = (int)num;
		}*/
		if(iSavedSubitem ==9){
			m_d_Thr[iSavedItem] = num;
		}
		/*if(iSavedSubitem ==10){
			m_d_OvershootingThr[iSavedItem] = num;
		}*/
		if(iSavedSubitem == 10){
			m_stainBlemish[iSavedItem] = num;
		}
		if(iSavedSubitem == 11){
			m_Separation_V[iSavedItem] = num;
		}
		
		Test_RECT[iSavedItem].EX_RECT_SET(
		Test_RECT[iSavedItem].m_PosX,
		Test_RECT[iSavedItem].m_PosY,
		Test_RECT[iSavedItem].m_Width,
		Test_RECT[iSavedItem].m_Height);
			
		if(Test_RECT[iSavedItem].m_Left < 0){//////////////////////수정
			Test_RECT[iSavedItem].m_Left = 0;
			Test_RECT[iSavedItem].m_Width = Test_RECT[iSavedItem].m_Right+1 -Test_RECT[iSavedItem].m_Left;
			Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
		}
		if(Test_RECT[iSavedItem].m_Right >CAM_IMAGE_WIDTH){
			Test_RECT[iSavedItem].m_Right = CAM_IMAGE_WIDTH;
			Test_RECT[iSavedItem].m_Width = Test_RECT[iSavedItem].m_Right+1 -Test_RECT[iSavedItem].m_Left;
			Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
		}
		if(Test_RECT[iSavedItem].m_Top< 0){
			Test_RECT[iSavedItem].m_Top = 0;
			Test_RECT[iSavedItem].m_Height = Test_RECT[iSavedItem].m_Bottom+1 - Test_RECT[iSavedItem].m_Top;
			Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
		}
		if(Test_RECT[iSavedItem].m_Bottom >CAM_IMAGE_HEIGHT){
			Test_RECT[iSavedItem].m_Bottom = CAM_IMAGE_HEIGHT;
			Test_RECT[iSavedItem].m_Height = 
			Test_RECT[iSavedItem].m_Bottom+1 -
			Test_RECT[iSavedItem].m_Top;
			Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
		}
		if(Test_RECT[iSavedItem].m_Height<= 0){
			Test_RECT[iSavedItem].m_Height = 1;
			Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
		}
		if(Test_RECT[iSavedItem].m_Height >=CAM_IMAGE_HEIGHT){
			Test_RECT[iSavedItem].m_Height = CAM_IMAGE_HEIGHT;
			Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
		}

		if(Test_RECT[iSavedItem].m_Width <= 0){
			Test_RECT[iSavedItem].m_Width = 1;
			Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
		}
		if(Test_RECT[iSavedItem].m_Width >=CAM_IMAGE_WIDTH){
			Test_RECT[iSavedItem].m_Width = CAM_IMAGE_WIDTH;
			Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
		}

		if((int)m_i_DustNumOfSector[iSavedItem] < 0){
			m_i_DustNumOfSector[iSavedItem] = 0;
		}

		if((int)m_i_DustNumOfSector[iSavedItem] > 50){
			m_i_DustNumOfSector[iSavedItem] = 50;
		}

		if((int)m_d_Thr[iSavedItem] < 0){
			m_d_Thr[iSavedItem] =0;
		}

		if((int)m_d_Thr[iSavedItem] > 50){
			m_d_Thr[iSavedItem] = 50;
		}
		
		
		if((int)m_d_OvershootingThr[iSavedItem] < 0){
			m_d_OvershootingThr[iSavedItem] = 0;
		}

		if((int)m_d_OvershootingThr[iSavedItem] > 999){
			m_d_OvershootingThr[iSavedItem] = 999;
		}

		if((double)m_stainBlemish[iSavedItem] < 0){
			m_stainBlemish[iSavedItem] = 0;
		}

		if((double)m_stainBlemish[iSavedItem] > 100){
			m_stainBlemish[iSavedItem] = 100;
		}


		Test_RECT[iSavedItem].EX_RECT_SET(
		Test_RECT[iSavedItem].m_PosX,
		Test_RECT[iSavedItem].m_PosY,
		Test_RECT[iSavedItem].m_Width,
		Test_RECT[iSavedItem].m_Height);

		CString data = "";

		if(iSavedSubitem ==1){
			data.Format("%d",Test_RECT[iSavedItem].m_PosX);			
		}
		else if(iSavedSubitem ==2){
			data.Format("%d",Test_RECT[iSavedItem].m_PosY);
		}
		else if(iSavedSubitem ==3){
			data.Format("%d",Test_RECT[iSavedItem].m_Left);			
		}
		else if(iSavedSubitem ==4){
			data.Format("%d",Test_RECT[iSavedItem].m_Top);			
		}
		else if(iSavedSubitem ==5){
			data.Format("%d",Test_RECT[iSavedItem].m_Right);			
		}
		else if(iSavedSubitem ==6){
			data.Format("%d",Test_RECT[iSavedItem].m_Bottom);			
		}
		else if(iSavedSubitem ==7){
			data.Format("%d",Test_RECT[iSavedItem].m_Width);			
		}
		else if(iSavedSubitem ==8){
			data.Format("%d",Test_RECT[iSavedItem].m_Height);			
		}
		/*else if(iSavedSubitem ==9){
			data.Format("%d",m_i_DustNumOfSector[iSavedItem]);
		}*/
		else if(iSavedSubitem ==9){
			data.Format("%6.2f",m_d_Thr[iSavedItem]);
		}
		/*else if(iSavedSubitem ==11){
			data.Format("%6.2f",m_d_OvershootingThr[iSavedItem]);
		}*/
		else if(iSavedSubitem == 10){
			data.Format("%6.2f", m_stainBlemish[iSavedItem]);
		}
		else if(iSavedSubitem == 11){
			data.Format("%d", m_Separation_V[iSavedItem]);
		}
		((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowText(data);
}

void CParticle_Option::List_COLOR_Change(int itemnum)
{
	bool FLAG = TRUE;
	int Check=0;

	for(int t=0;t<3; t++){
		if(ChangeItem[t]==0)Check++;
		if(ChangeItem[t]==1)Check++;
		if(ChangeItem[t]==2)Check++;
	}

	if(Check !=3){
		FLAG =FALSE;
		ChangeItem[changecount]=itemnum;
	}

	if(!FLAG){
		for(int t=0; t<changecount+1; t++){
			for(int k=0; k<changecount+1; k++){
				
				if(ChangeItem[t] == ChangeItem[k]){
					if(t==k){break;	}			
					ChangeItem[k] =-1;
				}
			}
		}
		//----------------데이터 정렬
		int temp=0;

		for(int i=0; i<3; i++)
		{
			for(int j=i+1; j<3; j++)
			{
				if(ChangeItem[i] < ChangeItem[j])  // 내림차순
				{
					temp = ChangeItem[i];
					ChangeItem[i] = ChangeItem[j];
					ChangeItem[j] = temp;
				}
			}
		}
		//--------------------------------
		for(int t=0; t<3; t++){

			if(ChangeItem[t] ==-1){
				changecount =t;
				break;
			}
		}
	}

	for(int t=0; t<3; t++){
		m_RectList.Update(t);
	}
}

void CParticle_Option::EditWheelIntSet(int nID,int Val)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = 0;
	}else{
		buf = GetDlgItemInt(nID);
		buf+= Val;
	}
	SetDlgItemInt(nID,buf,1);
	((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
}

bool CParticle_Option::Change_DATA_CHECK(bool FLAG){

		
	BOOL STAT = TRUE;
	//상한
	if (Test_RECT[iSavedItem].m_Width > CAM_IMAGE_WIDTH)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Height > CAM_IMAGE_HEIGHT)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_PosX > CAM_IMAGE_WIDTH)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_PosY > CAM_IMAGE_HEIGHT)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Top > CAM_IMAGE_HEIGHT)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Bottom > CAM_IMAGE_HEIGHT)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Left> CAM_IMAGE_WIDTH)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Right> CAM_IMAGE_WIDTH)
		STAT = FALSE;

	
	if(m_i_DustNumOfSector[iSavedItem] < 0){
		STAT = FALSE;
	}

	if(m_d_Thr[iSavedItem] < 0){
		STAT = FALSE;
	}

	if(m_d_OvershootingThr[iSavedItem] < 0){
		STAT = FALSE;
	}

	if(m_i_DustNumOfSector[iSavedItem] > 50){
		STAT = FALSE;
	}

	if((int)m_d_Thr[iSavedItem] > 50){
		STAT = FALSE;
	}

	if((int)m_d_OvershootingThr[iSavedItem] > 999){
		STAT = FALSE;
	}

	if((double)m_stainBlemish[iSavedItem] < 0){
		STAT = FALSE;
	}

	if((double)m_stainBlemish[iSavedItem] > 100){
		STAT = FALSE;
	}
	//하한

	if(Test_RECT[iSavedItem].m_Width < 1 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Height < 1 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_PosX < 0 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_PosY < 0 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Top < 0 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Bottom < 0  )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Left< 0 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Right< 0 )
		STAT = FALSE;

	if(FLAG  == 1){
		if(Test_RECT[iSavedItem].m_Width == 1 )
			STAT = FALSE;
		if(Test_RECT[iSavedItem].m_Height == 1 )
			STAT = FALSE;
		
	}


	return STAT;
}

void CParticle_Option::Rect_InsertList()
{
	int InIndex=0;
	CString str="";
	
	m_RectList.DeleteAllItems();

	for(int t=0; t<3; t++){
		InIndex = m_RectList.InsertItem(t,"초기",0);
		
 	
		if(t ==0){
			str.Empty();str="카메라영역";
		}
		if(t ==1){
			str.Empty();str="Side ";
		}
		if(t ==2){
			str.Empty();str="Center";
		}
		
		m_RectList.SetItemText(InIndex,0,str);
		str.Empty();str.Format("%d", Test_RECT[t].m_PosX);
		m_RectList.SetItemText(InIndex,1,str);
		str.Empty();str.Format("%d", Test_RECT[t].m_PosY);
		m_RectList.SetItemText(InIndex,2,str);
		
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Left);
		m_RectList.SetItemText(InIndex,3,str);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Top);
		m_RectList.SetItemText(InIndex,4,str);
	
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Right+1);
		m_RectList.SetItemText(InIndex,5,str);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Bottom+1);
		m_RectList.SetItemText(InIndex,6,str);

		str.Empty();
		str.Format("%d", Test_RECT[t].m_Width);
		m_RectList.SetItemText(InIndex,7,str);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Height);
		m_RectList.SetItemText(InIndex,8,str);

		//-----------test parameter
		/*str.Empty();
		str.Format("%d",m_i_DustNumOfSector[t]);
		m_RectList.SetItemText(InIndex,9,str);*/
		str.Empty();
		str.Format("%6.2f",m_d_Thr[t]);
		m_RectList.SetItemText(InIndex,9,str);
		/*str.Empty();
		str.Format("%6.2f",m_d_OvershootingThr[t]);
		m_RectList.SetItemText(InIndex,11,str);*/
		str.Empty();
		str.Format("%6.2f",m_stainBlemish[t]);
		m_RectList.SetItemText(InIndex,10,str);
		str.Empty();
		str.Format("%d",m_Separation_V[t]);
		m_RectList.SetItemText(InIndex,11,str);
		

		if(m_b_Ellipse[t] == TRUE){
			m_RectList.SetItemState(t,0x2000,LVIS_STATEIMAGEMASK);
		}else{
			m_RectList.SetItemState(t,0x1000,LVIS_STATEIMAGEMASK);
		}
		
	}
}

void CParticle_Option::Load_parameter(){
	CString str="";
	CString strTitle2 = "";
	
	b_FailCheck = GetPrivateProfileInt("PARTICLEOPT","FailCheck",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(b_FailCheck == -1){
		b_FailCheck = 1;
		str.Empty();
		str.Format("%d",b_FailCheck);
		WritePrivateProfileString("PARTICLEOPT","FailCheck",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}
	b_FailCheck = b_FailCheck;

	m_Black_STEP = GetPrivateProfileInt("PARTICLEOPT","BK_STEP",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_Black_STEP == -1){
		m_Black_STEP = 3;
		str.Empty();
		str.Format("%d",m_Black_STEP);
		WritePrivateProfileString("PARTICLEOPT","BK_STEP",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	/*m_Separation_V = GetPrivateProfileInt("PARTICLEOPT","Separation",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_Separation_V == -1){
		m_Separation_V = 30;
		str.Empty();
		str.Format("%d",m_Separation_V);
		WritePrivateProfileString("PARTICLEOPT","Separation",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}*/

	m_PASSNUM = GetPrivateProfileInt("PARTICLEOPT","PassNum",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_PASSNUM == -1){
		m_PASSNUM = 0;
		str.Empty();
		str.Format("%d",m_PASSNUM);
		WritePrivateProfileString("PARTICLEOPT","PassNum",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	m_RemovePic = GetPrivateProfileInt("PARTICLEOPT","REMOVE",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_RemovePic == -1){
		m_RemovePic = 5;
		str.Empty();
		str.Format("%d",m_RemovePic);
		WritePrivateProfileString("PARTICLEOPT","REMOVE",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	m_CaptureNum = GetPrivateProfileInt("PARTICLEOPT","Capture",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_CaptureNum == -1){
		m_CaptureNum = 30;
		str.Empty();
		str.Format("%d",m_CaptureNum);
		WritePrivateProfileString("PARTICLEOPT","Capture",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	/*m_stainBlemish = GetPrivateProfileDouble("PARTICLEOPT","Concentration",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_stainBlemish == -1){
		m_stainBlemish = 2.0;
		str.Empty();
		str.Format("%.1f",m_stainBlemish);
		WritePrivateProfileString("PARTICLEOPT","Concentration",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}*/

	m_Brightness = GetPrivateProfileInt("PARTICLEOPT","Bright",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_Brightness == -1){
		m_Brightness = 110;
		str.Empty();
		str.Format("%d",m_Brightness);
		WritePrivateProfileString("PARTICLEOPT","Bright",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	m_Contrast = GetPrivateProfileInt("PARTICLEOPT","Contrast",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_Contrast == -1){
		m_Contrast = 110;
		str.Empty();
		str.Format("%d",m_Contrast);
		WritePrivateProfileString("PARTICLEOPT","Contrast",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	m_Fail_Retry_Cnt = GetPrivateProfileInt("PARTICLEOPT","Fail_Retry",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_Fail_Retry_Cnt == -1){
		m_Fail_Retry_Cnt = 0;
		str.Empty();
		str.Format("%d",m_Fail_Retry_Cnt);
		WritePrivateProfileString("PARTICLEOPT","Fail_Retry",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	m_ExpGain = GetPrivateProfileCString("PARTICLEOPT","EXP_GAIN",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_ExpGain.IsEmpty()){
		m_ExpGain = "10";
		WritePrivateProfileString("PARTICLEOPT","EXP_GAIN",m_ExpGain,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	str_ImageSavePath = GetPrivateProfileCString("PARTICLEOPT","ImageSavePath",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	if (str_ImageSavePath.IsEmpty())
	{
		str_ImageSavePath = "D:\\ParticleImage";
		folder_gen(str_ImageSavePath);
		WritePrivateProfileString("PARTICLEOPT","ImageSavePath",str_ImageSavePath,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	m_ImageSaveMode = GetPrivateProfileInt("PARTICLEOPT","ImageSaveMode",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_ImageSaveMode == -1){
		m_ImageSaveMode = 0;
		str.Empty();
		str.Format("%d",m_ImageSaveMode);
		WritePrivateProfileString("PARTICLEOPT","ImageSaveMode",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);	
	}

	b_ManualTestMode = GetPrivateProfileInt("PARTICLEOPT", "TESTMODE", -1, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if (b_ManualTestMode == -1){
		b_ManualTestMode = 0;
		str.Empty();
		str.Format("%d", b_ManualTestMode);
		WritePrivateProfileString("PARTICLEOPT", "TESTMODE", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	int nFLAG = GetPrivateProfileInt(str_model, "Image_SaveMode", -1, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if (nFLAG == -1)
	{
		nFLAG = 1;
		str.Format("%d", nFLAG);
		WritePrivateProfileString(str_model, "Image_SaveMode", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	m_nImageSaveMode = nFLAG;

	m_Cb_ImageSave.SetCurSel(m_nImageSaveMode);

	((CImageTesterDlg *)m_pMomWnd)->b_ManualTestMode = b_ManualTestMode;
	((CImageTesterDlg *)m_pMomWnd)->b_ParticleCaptureMode = FALSE;
	((CImageTesterDlg *)m_pMomWnd)->i_CapturePaticleMode = m_ImageSaveMode;


	m_Combo_ImageSave.SetCurSel(m_ImageSaveMode);
	

//	str_CaptureNum.Format("%d",m_CaptureNum);
//	str_RemovePic.Format("%d",P_RemovePic);

	str_ExpGain = m_ExpGain;
	str_FailCnt.Format("%d",m_Fail_Retry_Cnt);
	str_Black_STEP.Format("%d",m_Black_STEP);
	str_PassNum.Format("%d",m_PASSNUM);
	str_Separation_V.Format("%d",m_Separation_V);
	str_CaptureNum.Format("%d",m_CaptureNum);
	str_Concentration.Format("%.1f",m_stainBlemish);
	str_CamBrightness.Format("%d",m_Brightness);
	str_CamContrast.Format("%d",m_Contrast);
	//-----------------------------------------------------------------영역 추가
		for(int t=0; t<3; t++){
			if(t ==0){
				strTitle2.Empty();
				strTitle2="CAM_";
			}
			if(t ==1){
				strTitle2.Empty();
				strTitle2="SIDE_";		
			}
			if(t ==2){
				strTitle2.Empty();
				strTitle2="CENTER_";
			}
			Test_RECT[t].m_PosX = GetPrivateProfileInt("PARTICLEOPT",strTitle2+"PosX",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
			if(Test_RECT[t].m_PosX == -1){
				for(int k=0; k<3; k++){
					Test_RECT[k].m_PosX = (CAM_IMAGE_WIDTH/2);
					Test_RECT[k].m_PosY = (CAM_IMAGE_HEIGHT/2);
					Test_RECT[k].m_Width = CAM_IMAGE_WIDTH - 20;
					Test_RECT[k].m_Height = CAM_IMAGE_HEIGHT - 20;
					Test_RECT[k].EX_RECT_SET(
					Test_RECT[k].m_PosX,
					Test_RECT[k].m_PosY,
					Test_RECT[k].m_Width,
					Test_RECT[k].m_Height);
					m_i_DustNumOfSector[k]=3;	
					m_d_Thr[k] = 10;
					m_d_OvershootingThr[k] = 30;
					m_b_Ellipse[k] = 0;
					m_stainBlemish[k] = 2.0;
					m_Separation_V[k] = 15;
				}
				Save_parameter();
			}
			Test_RECT[t].m_PosY	= GetPrivateProfileInt("PARTICLEOPT",strTitle2+"PosY",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
			Test_RECT[t].m_Left	= GetPrivateProfileInt("PARTICLEOPT",strTitle2+"StartX",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
			Test_RECT[t].m_Top	= GetPrivateProfileInt("PARTICLEOPT",strTitle2+"StartY",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
			Test_RECT[t].m_Right = GetPrivateProfileInt("PARTICLEOPT",strTitle2+"ExitX",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
			Test_RECT[t].m_Bottom = GetPrivateProfileInt("PARTICLEOPT",strTitle2+"ExitY",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
			Test_RECT[t].m_Width = GetPrivateProfileInt("PARTICLEOPT",strTitle2+"Width",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
			Test_RECT[t].m_Height = GetPrivateProfileInt("PARTICLEOPT",strTitle2+"Height",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);	

			m_i_DustNumOfSector[t] = GetPrivateProfileInt("PARTICLEOPT",strTitle2+"DustNumOfSector",3,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);	
			m_d_Thr[t] = GetPrivateProfileDouble("PARTICLEOPT",strTitle2+"Thresold",11,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
			m_d_OvershootingThr[t] = GetPrivateProfileDouble("PARTICLEOPT",strTitle2+"OverShootingThresold",30,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
			m_b_Ellipse[t] = GetPrivateProfileDouble("PARTICLEOPT",strTitle2+"m_Ellipse",0,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

			m_stainBlemish[t] = GetPrivateProfileDouble("PARTICLEOPT",strTitle2+"Concentration",2.0,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
			m_Separation_V[t] = GetPrivateProfileInt("PARTICLEOPT",strTitle2+"Size",15,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);	
	}
	//Save_parameter();
	FULL_RegionSUM();
	DustFilterSum();

	UpdateData(FALSE);
}

void CParticle_Option::Save_parameter(){
	CString str="";
	CString strTitle2 = "";

	b_FailCheck = b_FailCheck;

	str.Empty();
	str.Format("%d",b_FailCheck);
	WritePrivateProfileString("PARTICLEOPT","FailCheck",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	
	str.Empty();
	str.Format("%d",m_Black_STEP);
	WritePrivateProfileString("PARTICLEOPT","BK_STEP",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);	
	
	str.Empty();
	str.Format("%d",m_Separation_V);
	WritePrivateProfileString("PARTICLEOPT","Separation",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	
	str.Empty();
	str.Format("%d",m_PASSNUM);
	WritePrivateProfileString("PARTICLEOPT","PassNum",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	
	str.Empty();
	str.Format("%d",m_RemovePic);
	WritePrivateProfileString("PARTICLEOPT","REMOVE",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	
	str.Empty();
	str.Format("%d",m_CaptureNum);
	WritePrivateProfileString("PARTICLEOPT","Capture",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	str.Empty();
	str.Format("%d", b_ManualTestMode);
	WritePrivateProfileString("PARTICLEOPT", "TESTMODE", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	((CImageTesterDlg *)m_pMomWnd)->b_ManualTestMode = b_ManualTestMode;
	/*str.Empty();
	str.Format("%.1f",m_stainBlemish);
	WritePrivateProfileString("PARTICLEOPT","Concentration",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);*/
	
	for(int t=0; t<3; t++){

		if(t ==0){
			strTitle2.Empty();
			strTitle2="CAM_";
		}
		if(t ==1){
			strTitle2.Empty();
			strTitle2="SIDE_";		
		}
		if(t ==2){
			strTitle2.Empty();
			strTitle2="CENTER_";
		}

		str.Empty();
		str.Format("%d",Test_RECT[t].m_PosX);
		WritePrivateProfileString("PARTICLEOPT",strTitle2+"PosX",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%d",Test_RECT[t].m_PosY);
		WritePrivateProfileString("PARTICLEOPT",strTitle2+"PosY",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

		str.Empty();
		str.Format("%d",Test_RECT[t].m_Left);
		WritePrivateProfileString("PARTICLEOPT",strTitle2+"StartX",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%d",Test_RECT[t].m_Top);
		WritePrivateProfileString("PARTICLEOPT",strTitle2+"StartY",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%d",Test_RECT[t].m_Right);
		WritePrivateProfileString("PARTICLEOPT",strTitle2+"ExitX",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%d",Test_RECT[t].m_Bottom);
		WritePrivateProfileString("PARTICLEOPT",strTitle2+"ExitY",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%d",Test_RECT[t].m_Width);
		WritePrivateProfileString("PARTICLEOPT",strTitle2+"Width",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%d",Test_RECT[t].m_Height);
		WritePrivateProfileString("PARTICLEOPT",strTitle2+"Height",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

		str.Empty();
		str.Format("%d",m_i_DustNumOfSector[t]);
		WritePrivateProfileString("PARTICLEOPT",strTitle2+"DustNumOfSector",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%6.2f",m_d_Thr[t]);
		WritePrivateProfileString("PARTICLEOPT",strTitle2+"Thresold",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%6.2f",m_d_OvershootingThr[t]);
		WritePrivateProfileString("PARTICLEOPT",strTitle2+"OverShootingThresold",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%d",m_b_Ellipse[t]);
		WritePrivateProfileString("PARTICLEOPT",strTitle2+"m_Ellipse",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%.1f",m_stainBlemish[t]);
		WritePrivateProfileString("PARTICLEOPT",strTitle2+"Concentration",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%d",m_Separation_V[t]);
		WritePrivateProfileString("PARTICLEOPT",strTitle2+"Size",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	}
	FULL_RegionSUM();
	DustFilterSum();
}

void CParticle_Option::FULL_RegionSUM(){
	
	IplImage *Image2 = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);
	unsigned int Cnt;
	int Step=3, i,j;
	int ApplyZero = 1;

	for (int x = 0; x < CAM_IMAGE_WIDTH; x++){
		for (int y = 0; y < CAM_IMAGE_HEIGHT; y++){
			m_Expand_OverlayArea[x][y] = 1;
		}
	}


	//데이터 넣기
	
	FULL_RECT[0].m_PosX = Test_RECT[0].m_PosX;
	FULL_RECT[0].m_PosY = Test_RECT[0].m_PosY;
	FULL_RECT[0].m_Left = Test_RECT[0].m_Left;
	FULL_RECT[0].m_Right =  Test_RECT[0].m_Right;
	FULL_RECT[0].m_Top =  Test_RECT[0].m_Top;
	FULL_RECT[0].m_Bottom =  Test_RECT[0].m_Bottom;

	FULL_RECT[0].m_Width =  Test_RECT[0].m_Width;
	FULL_RECT[0].m_Height =  Test_RECT[0].m_Height;

	m_DustNumOfSector_C = m_i_DustNumOfSector[0];
	m_Lens_Shading_W_C = m_d_Thr[0];	
	P_Black_Threshold_C = m_d_OvershootingThr[0];	

///////////////////////////////   MIDDLE_ RECT   //////////////////////////////////////////////////
	m_Ellipse_MIDDLE =  m_b_Ellipse[1];
	MIDDLE_RECT[0].m_PosX =  Test_RECT[1].m_PosX;
	MIDDLE_RECT[0].m_PosY =  Test_RECT[1].m_PosY;
	MIDDLE_RECT[0].m_Left =  Test_RECT[1].m_Left;
	MIDDLE_RECT[0].m_Right =   Test_RECT[1].m_Right;
	MIDDLE_RECT[0].m_Top =   Test_RECT[1].m_Top;
	MIDDLE_RECT[0].m_Bottom =   Test_RECT[1].m_Bottom;

	MIDDLE_RECT[0].m_Width =  Test_RECT[1].m_Width;
	MIDDLE_RECT[0].m_Height =  Test_RECT[1].m_Height;

	m_DustNumOfSector_B = m_i_DustNumOfSector[1];
	m_Lens_Shading_W_B = m_d_Thr[1];	
	P_Black_Threshold_B =m_d_OvershootingThr[1];	

///////////////////////////////   CENTER_RECT   ////////////////////////////////////////////////
	m_Ellipse = m_b_Ellipse[2];
	R_RECT[0].m_PosX = Test_RECT[2].m_PosX;
	R_RECT[0].m_PosY = Test_RECT[2].m_PosY;
	R_RECT[0].m_Left = Test_RECT[2].m_Left;
	R_RECT[0].m_Right =  Test_RECT[2].m_Right;
	R_RECT[0].m_Top =  Test_RECT[2].m_Top;
	R_RECT[0].m_Bottom =  Test_RECT[2].m_Bottom;

	R_RECT[0].m_Width =  Test_RECT[2].m_Width;
	R_RECT[0].m_Height =  Test_RECT[2].m_Height;

	m_DustNumOfSector = m_i_DustNumOfSector[2];
	m_Lens_Shading_W = m_d_Thr[2];	
	P_Black_Threshold= m_d_OvershootingThr[2];	
//////////////////////////////////////////////////////////////////////////////


	LogWriteToFile(_T("FULL_RegionSUM() 함수 4"));
	for (int y = 0; y < CAM_IMAGE_HEIGHT; y++){
		for (int x = 0; x < CAM_IMAGE_WIDTH; x++){
			Image2->imageData[y * Image2->widthStep + x * 3 + 0] = 255;
			Image2->imageData[y * Image2->widthStep + x * 3 + 1] = 255;
			Image2->imageData[y * Image2->widthStep + x * 3 + 2] = 255;
			m_Area[x][y] =0;
			////////////// 최외각 라인에 안 들어온
			if((FULL_RECT[0].m_Top <= y) && (FULL_RECT[0].m_Bottom >= y) ){
				if((FULL_RECT[0].m_Left <= x) && (FULL_RECT[0].m_Right >= x) ){
					////////////////외각 MIDDLE 라인에 안 들어온
					if((MIDDLE_RECT[0].m_Top <= y) && (MIDDLE_RECT[0].m_Bottom >= y) ){
						if((MIDDLE_RECT[0].m_Left <= x) && (MIDDLE_RECT[0].m_Right >= x) ){
								//////////////////////////////  MIDDLE_RECT의 사각, 타원에 따라 m_Area 설정 :: 영역 안 = 2, 영역 밖 = 3  //////////////////////////////////////////////////
							if(m_Ellipse_MIDDLE == 1){ // m_Ellipse -> ROI 영역 체크박스를 타원으로 했을 때.
								if(EllipseDistanceSum(MIDDLE_RECT[0].m_PosX,   MIDDLE_RECT[0].m_PosY,  ((double)MIDDLE_RECT[0].m_Width/2.0),  ((double)MIDDLE_RECT[0].m_Height/2.0),  x,  y) <=1){//X, Y 가 타원 안에 들어온다면
									m_Area[x][y] =2; // B영역
									Image2->imageData[y * Image2->widthStep + x * 3 + 0] = 0;
									Image2->imageData[y * Image2->widthStep + x * 3 + 1] = 255;
									Image2->imageData[y * Image2->widthStep + x * 3 + 2] = 255;
								}
								else{
									m_Area[x][y] =3; // C영역
									Image2->imageData[y * Image2->widthStep + x * 3 + 0] = 0;
									Image2->imageData[y * Image2->widthStep + x * 3 + 1] = 255;
									Image2->imageData[y * Image2->widthStep + x * 3 + 2] = 0;

								}
							}// ROI 영역이 사각형일때
							else{
								m_Area[x][y] =2; // B영역
								Image2->imageData[y * Image2->widthStep + x * 3 + 0] = 0;
								Image2->imageData[y * Image2->widthStep + x * 3 + 1] = 255;
								Image2->imageData[y * Image2->widthStep + x * 3 + 2] = 255;
							}
								///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							////////////////CENTER 라인에 들어온다면
							if((R_RECT[0].m_Top <= y) && (R_RECT[0].m_Bottom >= y) ){
								if((R_RECT[0].m_Left <= x) && (R_RECT[0].m_Right >= x) ){
									//////////////////////////////  R_RECT의 사각, 타원에 따라 m_Area 설정 :: 영역 안 = 1, 영역 밖 = 2  //////////////////////////////////////////////////
									if(m_Ellipse == 1){ // m_Ellipse -> ROI 영역 체크박스를 타원으로 했을 때.
										if(EllipseDistanceSum(R_RECT[0].m_PosX,   R_RECT[0].m_PosY,  ((double)R_RECT[0].m_Width/2.0),  ((double)R_RECT[0].m_Height/2.0),  x,  y) <=1){//X, Y 가 타원 안에 들어온다면
											m_Area[x][y] =1; // A영역
											Image2->imageData[y * Image2->widthStep + x * 3 + 0] = 100;
											Image2->imageData[y * Image2->widthStep + x * 3 + 1] = 0;
											Image2->imageData[y * Image2->widthStep + x * 3 + 2] = 0;
										}
										else{
											m_Area[x][y] =2; // B영역
											Image2->imageData[y * Image2->widthStep + x * 3 + 0] = 0;
											Image2->imageData[y * Image2->widthStep + x * 3 + 1] = 255;
											Image2->imageData[y * Image2->widthStep + x * 3 + 2] = 255;

										}
									}// ROI 영역이 사각형일때
									else{
										m_Area[x][y] =1; // A영역
										Image2->imageData[y * Image2->widthStep + x * 3 + 0] = 100;
										Image2->imageData[y * Image2->widthStep + x * 3 + 1] = 0;
										Image2->imageData[y * Image2->widthStep + x * 3 + 2] = 0;
									}
									///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
								}else{
									
								}
							}else{
								
							}
						}else{
							m_Area[x][y] =3;//C영역
							Image2->imageData[y * Image2->widthStep + x * 3 + 0] = 0;
							Image2->imageData[y * Image2->widthStep + x * 3 + 1] = 255;
							Image2->imageData[y * Image2->widthStep + x * 3 + 2] = 0;
						}
					}else{
						m_Area[x][y] =3;//C영역
						Image2->imageData[y * Image2->widthStep + x * 3 + 0] = 0;
						Image2->imageData[y * Image2->widthStep + x * 3 + 1] = 255;
						Image2->imageData[y * Image2->widthStep + x * 3 + 2] = 0;
					}
				}
			}
		}
	}

	//if(((CFINAL_TEST_MDlg *)m_pMomWnd)->b_SecretOption ==1){
	//	cvShowImage("Region",Image2);  // 이 윈도우에 내가 만든 ipl_Img 이미지를 보이고
	//	cvWaitKey(0);                           // 키입력을 받은후에
	//	cvDestroyWindow("Region");        // 윈도우를 종료
	//}
	cvReleaseImage(&Image2);
}

void CParticle_Option::DustFilterSum(){

	int dis = (i_DustDis[0]+i_DustDis[1]+i_DustDis[2])*2;
	DustRoiX = dis;
	DustRoiY = dis;
	int Donut1 =i_DustDis[0];
	int Donut2 =i_DustDis[0]+i_DustDis[1];
	int Donut3 =i_DustDis[0]+i_DustDis[1]+i_DustDis[2];
	IplImage *Image2 = cvCreateImage(cvSize(DustRoiX+1, DustRoiY+1), IPL_DEPTH_8U, 3);
	for(int y = 0;y < DustRoiY+1;y++){
		for(int x = 0;x < DustRoiX+1;x++){
				Image2->imageData[y * Image2->widthStep + x * 3 + 0] = 255;
				Image2->imageData[y * Image2->widthStep + x * 3 + 1] = 255;
				Image2->imageData[y * Image2->widthStep + x * 3 + 2] = 255;
				m_DustFilter[x][y] =3;	
				if(EllipseDistanceSum(DustRoiX/2,DustRoiY/2,(double)Donut3,(double)Donut3,x,y) <=1){
				
					m_DustFilter[x][y] =2;	
					Image2->imageData[y * Image2->widthStep + x * 3 + 0] = 255;
					Image2->imageData[y * Image2->widthStep + x * 3 + 1] = 0;
					Image2->imageData[y * Image2->widthStep + x * 3 + 2] = 0;
					if(EllipseDistanceSum(DustRoiX/2,DustRoiY/2,(double)Donut2,(double)Donut2,x,y) <=1){
				
						m_DustFilter[x][y] =1;	
						Image2->imageData[y * Image2->widthStep + x * 3 + 0] = 0;
						Image2->imageData[y * Image2->widthStep + x * 3 + 1] = 255;
						Image2->imageData[y * Image2->widthStep + x * 3 + 2] = 0;
						if(EllipseDistanceSum(DustRoiX/2,DustRoiY/2,(double)Donut1,(double)Donut1,x,y) <=1){
							m_DustFilter[x][y] =0;	
							Image2->imageData[y * Image2->widthStep + x * 3 + 0] = 0;
							Image2->imageData[y * Image2->widthStep + x * 3 + 1] = 0;
							Image2->imageData[y * Image2->widthStep + x * 3 + 2] = 255;
						}
					}
				}
		}
	}
	
	//if(b_SHOWBtn ==1){
	//	cvShowImage("Region",Image2);  // 이 윈도우에 내가 만든 ipl_Img 이미지를 보이고
	//	cvWaitKey(0);                           // 키입력을 받은후에
	//	cvDestroyWindow("Region");        // 윈도우를 종료
	//}
	cvReleaseImage(&Image2);
}

double CParticle_Option::EllipseDistanceSum(int XC,int YC,double A,double B,int X1,int Y1){
	double DistX,DistY,x,y;

	x=(double)(X1-XC);
	y=(double)(Y1-YC);
	DistX=(x*x) / (double)((A*A));
	DistY=(y*y) / (double)((B*B));

	double Dist = DistX + DistY;

	return Dist;
}
double  CParticle_Option::Distance(double x1, double y1 , double x2, double y2)
{
	double Dist,x,y;

	x=(double)(x1-x2);
	y=(double)(y1-y2);
	Dist=sqrt(x*x+y*y);
	return Dist;
}

void CParticle_Option::CaptureImage()
{
	DWORD RGBPIX = 0;
	DWORD RGBLINE=0;
	
	memset(m_CaptureBuf,0,sizeof(m_CaptureBuf));

	unsigned int	m_SumBuf_R[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT] = { 0, };
	unsigned int	m_SumBuf_G[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT] = { 0, };
	unsigned int	m_SumBuf_B[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT] = { 0, };

	for (int t = 0; t<(CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT); t++){
		m_SumBuf_R[t]=0;
		m_SumBuf_G[t]=0;
		m_SumBuf_B[t]=0;
	}

	int CaptureSet = m_CaptureNum;///CaptureSet 캡쳐 수 옵션 최하 1
	for(int Cnt =0; Cnt < CaptureSet;  Cnt++)
	{
		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan =1;					
		for(int i =0;i<10;i++){
			if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0){
				break;
			}else{
				DoEvents(50);
			}
		}

		RGBPIX = 0;
		RGBLINE=0;
		for (int lopy = 0; lopy < CAM_IMAGE_HEIGHT; lopy++){
			RGBPIX = 0;
			for (int lopx = 0; lopx < CAM_IMAGE_WIDTH; lopx++){

				m_SumBuf_B[RGBLINE + RGBPIX] += m_RGBScanbuf[(RGBLINE * 3) + (RGBPIX * 3)];
				m_SumBuf_G[RGBLINE + RGBPIX] += m_RGBScanbuf[(RGBLINE * 3) + (RGBPIX * 3) + 1];
				m_SumBuf_R[RGBLINE + RGBPIX] += m_RGBScanbuf[(RGBLINE * 3) + (RGBPIX * 3) + 2];
				RGBPIX+=1;
			}
			RGBLINE += CAM_IMAGE_WIDTH;
		}
	}
	
	RGBPIX = 0;
	RGBLINE=0;
	for (int lopy = 0; lopy < CAM_IMAGE_HEIGHT; lopy++)
	{
		RGBPIX = 0;
		for (int lopx = 0; lopx < CAM_IMAGE_WIDTH; lopx++){
			m_CaptureBuf[(RGBLINE*3) + (RGBPIX*3)] = (BYTE)(m_SumBuf_B[RGBLINE + RGBPIX]/CaptureSet);  
			m_CaptureBuf[(RGBLINE*3) + (RGBPIX*3)+1] = (BYTE)(m_SumBuf_G[RGBLINE + RGBPIX]/CaptureSet); 
			m_CaptureBuf[(RGBLINE*3) + (RGBPIX*3)+2] = (BYTE)(m_SumBuf_R[RGBLINE + RGBPIX]/CaptureSet); 
			RGBPIX+=1;
		}
		RGBLINE += CAM_IMAGE_WIDTH;
	}
}
void CParticle_Option::CaptureImage_16bit()
{
	DWORD RGBPIX = 0;
	DWORD RGBLINE = 0;

	memset(m_CaptureBuf, 0, sizeof(m_CaptureBuf));
	memset(m_CaptureBuf_16bit, 0, sizeof(m_CaptureBuf_16bit));

	unsigned int	m_SumBuf_R[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT] = { 0, };
	unsigned int	m_SumBuf_G[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT] = { 0, };
	unsigned int	m_SumBuf_B[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT] = { 0, };

	unsigned int	m_SumBuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT] = { 0, };

	for (int t = 0; t < (CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT); t++){
		m_SumBuf_R[t] = 0;
		m_SumBuf_G[t] = 0;
		m_SumBuf_B[t] = 0;

		m_SumBuf[t] = 0;
	}

	int CaptureSet = m_CaptureNum;///CaptureSet 캡쳐 수 옵션 최하 1
	for (int Cnt = 0; Cnt < CaptureSet; Cnt++){
		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = 1;
		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;
		for (int i = 0; i < 10; i++){
			if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0&&((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0){
				break;
			}
			else{
				DoEvents(50);
			}
		}

		RGBPIX = 0;
		RGBLINE = 0;
		for (int lopy = 0; lopy < CAM_IMAGE_HEIGHT; lopy++){
			RGBPIX = 0;
			for (int lopx = 0; lopx < CAM_IMAGE_WIDTH; lopx++){

				m_SumBuf_B[RGBLINE + RGBPIX] += m_RGBScanbuf[(RGBLINE * 3) + (RGBPIX * 3)];
				m_SumBuf_G[RGBLINE + RGBPIX] += m_RGBScanbuf[(RGBLINE * 3) + (RGBPIX * 3) + 1];
				m_SumBuf_R[RGBLINE + RGBPIX] += m_RGBScanbuf[(RGBLINE * 3) + (RGBPIX * 3) + 2];

				m_SumBuf[RGBLINE + RGBPIX] += m_GRAYScanbuf[RGBLINE + RGBPIX];

				RGBPIX += 1;
			}
			RGBLINE += CAM_IMAGE_WIDTH;
		}
	}

	RGBPIX = 0;
	RGBLINE = 0;
	for (int lopy = 0; lopy < CAM_IMAGE_HEIGHT; lopy++){
		RGBPIX = 0;
		for (int lopx = 0; lopx < CAM_IMAGE_WIDTH; lopx++){

			m_CaptureBuf[(RGBLINE * 3) + (RGBPIX * 3)] = (BYTE)(m_SumBuf_B[RGBLINE + RGBPIX] / CaptureSet);
			m_CaptureBuf[(RGBLINE * 3) + (RGBPIX * 3) + 1] = (BYTE)(m_SumBuf_G[RGBLINE + RGBPIX] / CaptureSet);
			m_CaptureBuf[(RGBLINE * 3) + (RGBPIX * 3) + 2] = (BYTE)(m_SumBuf_R[RGBLINE + RGBPIX] / CaptureSet);

			m_CaptureBuf_16bit[RGBLINE + RGBPIX ] = (int)(m_SumBuf[RGBLINE + RGBPIX] / CaptureSet);
			RGBPIX += 1;
		}
		RGBLINE += CAM_IMAGE_WIDTH;
	}
}

bool CParticle_Option::ParticleGen(LPBYTE IN_RGB,int NUM){
	
//	R_RECT[0].m_Success = FALSE;
	int Step = m_Black_STEP;
	
	BYTE R,G,B;
	DWORD	RGBPIX = 0;
	int index=0;
	float **Data, **Data1, **Data2;

	int startx = 0, starty = 0, endx = CAM_IMAGE_WIDTH, endy = CAM_IMAGE_HEIGHT;
	int HEIGHT = CAM_IMAGE_HEIGHT, WIDTH = CAM_IMAGE_WIDTH, Xc, Yc, Radius;
	long i,*IndexLS, Index;
	int x, y;
	int Group =0;	
	long count =0;

	unsigned int DeadPixel[55][256][4];
	double CenterPoint[55][4];
	unsigned int DeadPixelLowData[2000][4]={0,};

	unsigned int IndexG[55];
	long k,j;
	unsigned int Cell=30;

	// 선언
	Data = (float **)malloc(sizeof(float *)*CAM_IMAGE_HEIGHT);
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)Data[i] = (float *)malloc(sizeof(float)* CAM_IMAGE_WIDTH);

	Data1 = (float **)malloc(sizeof(float *)*CAM_IMAGE_HEIGHT);
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)Data1[i] = (float *)malloc(sizeof(float)* CAM_IMAGE_WIDTH);

	Data2 = (float **)malloc(sizeof(float *)*CAM_IMAGE_HEIGHT);
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)Data2[i] = (float *)malloc(sizeof(float)* CAM_IMAGE_WIDTH);


	DWORD RGBLINE = starty * CAM_IMAGE_WIDTH*4;
	
	Xc = WIDTH/2; Yc=HEIGHT/2;

	count =0;	Group = 0;
	IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *DSTImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	
//  	IplImage *Temp_LoadImage = cvLoadImage("D:\\Test_Stain\\D_Zone_8\\16263H120540_001_Origin.bmp");
// // 	IplImage *Temp_disOverlay = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);
// // 
// // 	cvCopy(Temp_LoadImage,Temp_disOverlay);
// // 	BYTE Temp_B = 0;
// // 	BYTE Temp_G = 0;
// // 	BYTE Temp_R = 0;
// // 	//cvSmooth(Temp_LoadImage,Temp_LoadImage);
// 	cvSaveImage("D:\\Test\\Stain_SRC.bmp",Temp_LoadImage);
// // 
// // 
// // 
//  	for(int i = 0; i < CAM_IMAGE_WIDTH;i++){
//  		for(int j = 0; j < CAM_IMAGE_HEIGHT; j++){
//  			IN_RGB[j * (CAM_IMAGE_WIDTH * 4) + 4*i] = Temp_LoadImage->imageData[j * Temp_LoadImage->widthStep + i * 3 + 0];
//  			IN_RGB[j * (CAM_IMAGE_WIDTH * 4) + 4*i + 1] = Temp_LoadImage->imageData[j * Temp_LoadImage->widthStep + i * 3 + 1];
//  			IN_RGB[j * (CAM_IMAGE_WIDTH * 4) + 4*i + 2] = Temp_LoadImage->imageData[j * Temp_LoadImage->widthStep + i * 3 + 2];
//  		}
//  	}



	double AVG_Y = 0.0;
	double Temp_THR = 0.0;
	UINT _c = 0;
	int Increase_RageX = 15;
	int Increase_RageY = 10;

	for(int lopy = starty;lopy < endy;lopy++){
		RGBPIX = startx * 3;
		for(int lopx = startx;lopx < endx;lopx++){


			if((lopx >= endx - Increase_RageX) || (lopx <= startx + Increase_RageX)   ){
				if(lopx >= CAM_IMAGE_WIDTH/2 ){
					B = IN_RGB[lopy * (CAM_IMAGE_WIDTH * 3) + 3 * (endx - Increase_RageX)];
					G = IN_RGB[lopy * (CAM_IMAGE_WIDTH * 3) + 3 * (endx - Increase_RageX) + 1];
					R = IN_RGB[lopy * (CAM_IMAGE_WIDTH * 3) + 3 * (endx - Increase_RageX) + 2];
				}
				else{
					B = IN_RGB[lopy * (CAM_IMAGE_WIDTH * 3) + 3 * (startx + Increase_RageX)];
					G = IN_RGB[lopy * (CAM_IMAGE_WIDTH * 3) + 3 * (startx + Increase_RageX) + 1];
					R = IN_RGB[lopy * (CAM_IMAGE_WIDTH * 3) + 3 * (startx + Increase_RageX) + 2];
				}
				Data[lopy][lopx] = (float)((0.29900*R)+(0.58700*G)+(0.11400*B));
				
			}else{
				B = IN_RGB[lopy * (CAM_IMAGE_WIDTH * 3) + 3 * lopx];
				G = IN_RGB[lopy * (CAM_IMAGE_WIDTH * 3) + 3 * lopx + 1];
				R = IN_RGB[lopy * (CAM_IMAGE_WIDTH * 3) + 3 * lopx + 2];
				Data[lopy][lopx] = (float)((0.29900*R)+(0.58700*G)+(0.11400*B));
			}

			AVG_Y += Data[lopy][lopx];
			_c++;
			
			//if(m_Area[lopx][lopy] == 1){
			//	Index=(long)Distance(lopx,lopy,Xc,Yc);
			//}

			

			OriginImage->imageData[(lopy*OriginImage->widthStep) + ( lopx) ] = Data[lopy][lopx];

			//RGBPIX+=4;
		}
		//RGBLINE+=2880;
	}
	if(_c){
		AVG_Y = AVG_Y/_c;
	}
	for(int lopy = starty;lopy < endy;lopy++){
		//RGBPIX = startx * 4;
		for(int lopx = startx;lopx < endx;lopx++){
			if(lopy >= (endy-Increase_RageY)){
				B = OriginImage->imageData[((endy-Increase_RageY)*OriginImage->widthStep) + ( lopx) ];
				G = OriginImage->imageData[((endy-Increase_RageY)*OriginImage->widthStep) + ( lopx) +1];
				R = OriginImage->imageData[((endy-Increase_RageY)*OriginImage->widthStep) + ( lopx) +2];
				Data[lopy][lopx] = (float)((0.29900*R)+(0.58700*G)+(0.11400*B));
				
			}
			OriginImage->imageData[(lopy*OriginImage->widthStep) + ( lopx) ] = Data[lopy][lopx];

		}
	}
	for(int i = 0; i < CAM_IMAGE_WIDTH;i++){
  		for(int j = 0; j < CAM_IMAGE_HEIGHT; j++){
			IN_RGB[j * (CAM_IMAGE_WIDTH * 3) + 3 * i] = OriginImage->imageData[j * OriginImage->widthStep + i + 0];
			IN_RGB[j * (CAM_IMAGE_WIDTH * 3) + 3 * i + 1] = OriginImage->imageData[j * OriginImage->widthStep + i + 1];
			IN_RGB[j * (CAM_IMAGE_WIDTH * 3) + 3 * i + 2] = OriginImage->imageData[j * OriginImage->widthStep + i + 2];
  		}
  	}

	cvSaveImage("D:\\test\\RGBResultImage_org.bmp", OriginImage);

	//if(Crush_detection(IN_RGB) == FALSE)
	//	return FALSE;


	//THR_OFFSET = 0;
	Lump_detection(IN_RGB);

	for (int i = 0; i < counter; i++){
		P_RECT[i].m_Left = rectArray[i].x;
		P_RECT[i].m_Top = rectArray[i].y;
		P_RECT[i].m_Right = rectArray[i].x + rectArray[i].width;
		P_RECT[i].m_Bottom = rectArray[i].y + rectArray[i].height;

		count++;
	}

	delete[]rectArray;






	cvReleaseImage(&DSTImage);
	cvReleaseImage(&OriginImage);

	IplImage *Image = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
    bool skip =0;
	int BigCnt=0;
	int BigDustPosition[10000][3]={0,};

	int THR_OFFSET = /*(AVG_Y - 112)/(double) 5*/ 0; //유동 Threshold 제거
	count=counter;
	double ThrPattern = m_Lens_Shading_W + THR_OFFSET; //멍 이물 검출을 위한 한계값. -> Center
	double ThrPattern_B = m_Lens_Shading_W_B + THR_OFFSET; //멍 이물 검출을 위한 한계값. -> Side
	double ThrPattern_C = m_Lens_Shading_W_C + THR_OFFSET; //멍 이물 검출을 위한 한계값. -> 카메라영역 (최외각)

	double ThrHigh=P_Black_Threshold; //밝기의 변화량이 너무 크면 Overlay일 확률이 크므로... 제외하기 위한 한계값 일반적으로 30 정도 사용
	double ThrHigh_B=P_Black_Threshold_B; //
	double ThrHigh_C=P_Black_Threshold_C;// -> 최외각
	

	int CntNegative=0, CntPositive=0 ,EvaluationStart=0,NegativeStart=0;
	double MeanData=0, SumPoint[2000]={0,}, SumTemp=0, SumPoint2[2000]={0,}, SumTemp2=0;

	int Mg=3,N=2,MatchingFilter=10, Cnt=0,OvSet=0;
	int Mgin=5; //오버레이 근처 영역 제거 마진
	int ModifyEnd = 0;
	double Sum[3][CAM_IMAGE_HEIGHT] = { 0, }, stdSum[3][CAM_IMAGE_HEIGHT] = { 0, }, MeanX[3][CAM_IMAGE_HEIGHT] = { 0, }, stdX[3][CAM_IMAGE_HEIGHT] = { 0, };

	/////////////////////////  밝기 변화 형태로 변환 X 방향 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	LogWriteToFile(_T("밝기 변환 형태로 변환 X 방향"));
	for (int y = 0; y<CAM_IMAGE_HEIGHT; y++){
		for (int x = N; x<CAM_IMAGE_WIDTH - N; x++){
			Data1[y][x]=0;
			for(i=0;(i<N);i++){
				if((x-N+i<0) ||(x-N+i>719)||(x+i<0) ||(x+i>719))break;
				Data1[y][x] += (float)((Data[y][x+i]) -(Data[y][x-N+i]));  //khk
			}
			Data1[y][x]/=(float)N;
		}

		for (int x = N; x<CAM_IMAGE_WIDTH - N; x++){
			if(m_Expand_OverlayArea[x][y] == 0){
				int i50;
				if ((ModifyEnd<x) && (x<CAM_IMAGE_WIDTH-10)){
					for (i50 = x; i50<CAM_IMAGE_WIDTH - N - Mgin; i50++){
						if(m_Expand_OverlayArea[i50][y] == 1 ){
							ModifyEnd = i50;
							break;
						}
					}
					if (i50<CAM_IMAGE_WIDTH - N){
						double Start =0, End= 0;
						for(i50=0;i50<6;i50++){
							if((x-Mgin-i50<0) ||(x-Mgin-i50>719)||(ModifyEnd+Mgin+i50<0) ||(ModifyEnd+Mgin+i50>719))break;
							Start += Data1[y][x-Mgin-i50];
							End  +=  Data1[y][ModifyEnd+Mgin+i50];
						}
						Start /= 6.0;
						End   /= 6.0;

						for(i50=0;i50<ModifyEnd-x+Mgin*2;i50++){
							if((x-Mgin-i50<0) ||(x-Mgin-i50>719)||(ModifyEnd-x+Mgin*2<0) ||(ModifyEnd-x+Mgin*2>719))break;
							Data1[y][x-Mgin+i50] = (float)((End-Start)/(double)(ModifyEnd-x+Mgin*2)*i50)+Start;
						}
					}
				}
			}
		}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		for (int x = MatchingFilter; x<CAM_IMAGE_WIDTH - MatchingFilter; x++)
		{
			Data2[y][x]=0;
			for(i=0;i<MatchingFilter;i++){
				if((x+i<0) ||(x+i>719)||(x-MatchingFilter+i<0) ||(x-MatchingFilter+i>719))break;
				Data2[y][x] += (float)((Data1[y][x+i]) -(Data1[y][x-MatchingFilter+i]));  //khk
			}
			Data2[y][x]/=MatchingFilter;
		}
		ModifyEnd = 0;
	}
	///////////////////////////////각 라인별 평균값 구하기////////////////////////////////////////////////////////////////
	LogWriteToFile(_T("각 라인별 평균값 구하기"));
	Cnt=0;
	for (int y = 0; y<CAM_IMAGE_HEIGHT; y++){
		for (int x = 20; x<CAM_IMAGE_WIDTH - 20; x++){
			Sum[0][y] += Data[y][x];
			stdSum[0][y] += Data[y][x]*Data[y][x];
			Sum[1][y] += Data1[y][x];
			stdSum[1][y] += Data1[y][x]*Data1[y][x];
			Sum[2][y] += Data2[y][x];
			stdSum[2][y] += Data2[y][x]*Data2[y][x];
			Cnt++;
		}
		for(i=0;i<3;i++){
			MeanX[i][y] = Sum[i][y]/(double)Cnt;
			stdX[i][y] = stdSum[i][y]/(double)Cnt - MeanX[i][y]*MeanX[i][y];
		}
		Cnt=0;
	}

	///////////////////////////////각 라인별 멍이물 특징점 구하기////////////////////////////////////////////////////////////////

	//	count=0;
	CntNegative=0; CntPositive=0; EvaluationStart=0;NegativeStart=0;
	MeanData=0;// SumPoint[1000]={0,}, 
	SumTemp=0;
	double Max_Concen = 0;
	LogWriteToFile(_T("각 라인별 멍이물 특징점 구하기"));
	BOOL Flag = FALSE;
	int Range_Stain = 10;
	int defect_Step = 3;

	for (int y = 1; y<CAM_IMAGE_HEIGHT; y++){
		CntNegative=0;

		for (int x = 20; x<CAM_IMAGE_WIDTH - 20; x++){
			if (((Data[y][x] - Data[y][x + 3]) > ThrPattern) && (m_Area[x][y] == 1) && y < CAM_IMAGE_HEIGHT - 5 && x < CAM_IMAGE_WIDTH){

				DeadPixelLowData[count][0] = x;	
				DeadPixelLowData[count][1] = y;
				P_RECT[count].m_Left = x - Range_Stain + defect_Step;
				P_RECT[count].m_Top = y - Range_Stain + defect_Step;
				P_RECT[count].m_Right = x + (Range_Stain)+defect_Step;
				P_RECT[count].m_Bottom = y + Range_Stain + defect_Step;
				
				

				P_RECT[count].m_Concentration = Data[y][x] - Data[y][x + defect_Step];

				if (P_RECT[count].m_Concentration > Max_Concen){
					Max_Concen = P_RECT[count].m_Concentration;
				}

				Flag = TRUE;
				count++;
				y += 10;
				break;
			}
			if (((Data[y][x] - Data[y][x + 3]) > ThrPattern_B) && (m_Area[x][y] == 2) && y < CAM_IMAGE_HEIGHT - 5 && x < CAM_IMAGE_WIDTH){

				DeadPixelLowData[count][0] = x;	
				DeadPixelLowData[count][1] = y;
				P_RECT[count].m_Left = x - Range_Stain + defect_Step;
				P_RECT[count].m_Top = y - Range_Stain + defect_Step;
				P_RECT[count].m_Right = x + (Range_Stain)+defect_Step;
				P_RECT[count].m_Bottom = y + Range_Stain + defect_Step;
				
				

				P_RECT[count].m_Concentration = Data[y][x] - Data[y][x + defect_Step];

				if (P_RECT[count].m_Concentration > Max_Concen){
					Max_Concen = P_RECT[count].m_Concentration;
				}

				Flag = TRUE;
				count++;
				y += 10;
				break;
			}
			if (((Data[y][x] - Data[y][x + 3]) > ThrPattern_C) && (m_Area[x][y] == 3) && y < CAM_IMAGE_HEIGHT - 5 && x < CAM_IMAGE_WIDTH){

				DeadPixelLowData[count][0] = x;	
				DeadPixelLowData[count][1] = y;
				P_RECT[count].m_Left = x - Range_Stain + defect_Step;
				P_RECT[count].m_Top = y - Range_Stain + defect_Step;
				P_RECT[count].m_Right = x + (Range_Stain)+defect_Step;
				P_RECT[count].m_Bottom = y + Range_Stain + defect_Step;
				
				

				P_RECT[count].m_Concentration = Data[y][x] - Data[y][x + defect_Step];

				if (P_RECT[count].m_Concentration > Max_Concen){
					Max_Concen = P_RECT[count].m_Concentration;
				}

				Flag = TRUE;
				count++;
				y += 10;
				break;
			}
			if((double)Data2[y][x]-MeanX[2][y]<0){

				if(EvaluationStart==1){
					EvaluationStart=2;
					if(CntPositive>=10){  
						// 이물 검출 알고리즘
						EvaluationStart=0;
						SumTemp=0; SumPoint[count]=0;
						SumTemp2=0; SumPoint2[count]=0;

						for(i=0;i<CntNegative;i++){
							if((x-CntPositive-1-i>0)&&(x-CntPositive-1-i<719)){
								SumTemp += (Data2[y][x-CntPositive-1-i]-MeanX[2][y]);
							}
						}

						SumPoint[count]=SumTemp; 

						SumTemp=0; 
						for(i=0;i<CntNegative;i++){
							if((x-CntPositive+i>0)&&(x-CntPositive+i<719)){
								SumTemp += (Data2[y][x-CntPositive+i]-MeanX[2][y]);
							}
						}
						SumPoint[count]=SumTemp-SumPoint[count];

						for(i=0;i<CntNegative;i++){
							if((x-CntPositive-1-i>0)&&(x-CntPositive-1-i<719)){
								SumTemp2 += (Data2[y-1][x-CntPositive-1-i]-MeanX[2][y-1]);

							}
						}

						SumPoint2[count]=SumTemp2; //   /(double)CntNegative;

						SumTemp2=0; 

						for(i=0;i<CntNegative;i++){
							if((x-CntPositive+i>0)&&(x-CntPositive+i<719)){
								SumTemp2 += (Data2[y-1][x-CntPositive+i]-MeanX[2][y-1]);

							}
						}
						SumPoint2[count]=SumTemp2-SumPoint2[count];

						///////////////////////////////  A 존 검출  /////////////////////////////중앙 노란색
// 						if((m_Area[x][y]==1)&&(SumPoint[count]>ThrPattern)&&(SumPoint[count]<ThrHigh)/*&&(SumPoint2[count]>ThrPattern)&&(SumPoint2[count]<ThrHigh)*/&&((x>23)&&(x<697))){//이물이 맞으면
// 							if(x-CntPositive-10>0){
// 
// 								if(count < 1000)
// 								{
// 									DeadPixelLowData[count][0]=x-CntPositive-10;	
// 									DeadPixelLowData[count][1]=y;
// 								}else
// 								{
// 								//	AfxMessageBox(_T("Error A Zone"));
// 								}
// 								
// 								if(count<1000)count++;
// 							}
// 						}
// 						///////////////////////////////  B 존 검출  /////////////////////////////바깥쪽 초록색
// 						if((m_Area[x][y]==2)&&(SumPoint[count]>ThrPattern_B)&&(SumPoint[count]<ThrHigh_B)/*&&(SumPoint2[count]>ThrPattern_B)&&(SumPoint2[count]<ThrHigh_B)*/&&((x>23)&&(x<697))){//이물이 맞으면
// 							if(x-CntPositive-10>0){
// 								if(count < 1000)
// 								{
// 									DeadPixelLowData[count][0]=x-CntPositive-10;	
// 									DeadPixelLowData[count][1]=y;
// 								}else
// 								{
// 								//	AfxMessageBox(_T("Error B Zone"));
// 								}
// 								if(count<1000)count++;
// 							}
// 						}
// 						////////////////////////////////  C 존 검출  ///////////////////////////////외각
// 						if((m_Area[x][y]==3)&&(SumPoint[count]>ThrPattern_C)&&(SumPoint[count]<ThrHigh_C)/*&&(SumPoint2[count]>ThrPattern_C)&&(SumPoint2[count]<ThrHigh_C)*/&&((x>23)&&(x<697))){//이물이 맞으면
// 							if(x-CntPositive-10>0){
// 
// 								if(count < 1000)
// 								{
// 									DeadPixelLowData[count][0]=x-CntPositive-10;	
// 									DeadPixelLowData[count][1]=y;
// 								}else
// 								{
// 								//	AfxMessageBox(_T("Error C Zone"));
// 								}
// 
// 								if(count<1000)count++;
// 							}
// 						}
					}
					CntPositive=0; CntNegative=0;//초기화
				}
				else if(CntPositive != 0) CntPositive=0;

				CntNegative ++;

			}
			else {
				if(EvaluationStart == 2 ){
					EvaluationStart = 0 ;
					CntNegative=0;
				}


				if((CntNegative>=10)&&(EvaluationStart == 0)){
					EvaluationStart = 1;
				}
				CntPositive++;
			}
		}
	}


	for(int i = 0 ; i < count ; i++){
		P_RECT[i].m_Concentration = Max_Concen;
	}

//
//	/////////////////////////한줄단위 그래프 그리기////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	if(((CFINAL_TEST_MDlg *)m_pMomWnd)->b_SecretOption == TRUE){
//		IplImage *temp_image = cvCreateImage(cvSize(720, 255), IPL_DEPTH_8U, 3);
//		IplImage *temp_image1 = cvCreateImage(cvSize(720, 255), IPL_DEPTH_8U, 3);
//		IplImage *temp_image2 = cvCreateImage(cvSize(720, 255), IPL_DEPTH_8U, 3);
//
//		cvSetZero(temp_image);
//		cvSetZero(temp_image1);
//		cvSetZero(temp_image2);
//
//		BYTE Sum_Y, Sum_Y1,Sum_Y2;
//
//		int Y_POS = GetDlgItemInt(IDC_EDIT3);
//
//
//		for(int x=0; x<720; x++)
//		{
//			/////////////
//			if(Data2[Y_POS][x]*16+100>255){
//				Sum_Y2=255;
//			}
//			else if(Data2[Y_POS][x]*16+100<0){
//				Sum_Y2=0;
//			}
//			else{
//				Sum_Y2 = (BYTE)(Data2[Y_POS][x]*16+100); 
//			}
//			//////////////
//			if(Data1[Y_POS][x]*16+100>255){
//				Sum_Y1=255;
//			}
//			else if(Data1[Y_POS][x]*16+100<0){
//				Sum_Y1=0;
//			}
//			else{
//				Sum_Y1 = (BYTE)(Data1[Y_POS][x]*16+100); 
//			}
//			///////////////		
//			Sum_Y = (BYTE)(Data[Y_POS][x]); 
//
//			cvLine(temp_image1, cvPoint(x, temp_image1->height), cvPoint(x, temp_image1->height - Sum_Y1), CV_RGB(255, 255, 255), 1, 8);
//			cvLine(temp_image, cvPoint(x, temp_image->height), cvPoint(x, temp_image->height - Sum_Y), CV_RGB(255, 255, 255), 1, 8);
//			cvLine(temp_image2, cvPoint(x, temp_image2->height), cvPoint(x, temp_image2->height - Sum_Y2), CV_RGB(255, 255, 255), 1, 8);
//		}
//		cvLine(temp_image1, cvPoint(1, temp_image1->height - (MeanX[1][Y_POS]+100)), cvPoint(718, temp_image1->height - (MeanX[1][Y_POS]*16+100)), CV_RGB(255, 0, 0), 1, 8);
//		cvLine(temp_image, cvPoint(1, temp_image1->height - (MeanX[0][Y_POS])), cvPoint(718, temp_image->height - (MeanX[0][Y_POS])), CV_RGB(255, 0, 0), 1, 8);
//		cvLine(temp_image2, cvPoint(1,temp_image1->height - (MeanX[2][Y_POS]+100)), cvPoint(718, temp_image2->height - (MeanX[2][Y_POS]*16+100)), CV_RGB(255, 0, 0), 1, 8);
//
//		cvShowImage("SCAN LINE 3", temp_image);
//		cvShowImage("SCAN LINE 4", temp_image1);
//		cvShowImage("SCAN LINE 5", temp_image2);
//
//		cvReleaseImage(&temp_image2);
//		cvReleaseImage(&temp_image1);
//		cvReleaseImage(&temp_image);
//	}
/////////////////////////  밝기 변화 형태로 변환 Y 방향////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Mg=3;N=2;MatchingFilter=10; Cnt=0;OvSet=0;
	Mgin=5; //오버레이 근처 영역 제거 마진
	ModifyEnd = 0;

	LogWriteToFile(_T("밝기 변환 형태로 변환 Y 방향"));
	double SumY[3][CAM_IMAGE_WIDTH] = { 0, }, stdSumY[3][CAM_IMAGE_WIDTH] = { 0, }, MeanY[3][CAM_IMAGE_WIDTH] = { 0, }, stdY[3][CAM_IMAGE_WIDTH] = { 0, };

	
	for (int x = 0; x < CAM_IMAGE_WIDTH; x++){
		for (int y = N + 10; y<CAM_IMAGE_HEIGHT - N - 10; y++){
 			if(y < 0)
 				break;
			
			Data1[y][x]=0;
			for(i=0;(i<N);i++){
				if ((y - N + i<0) || (y - N + i>CAM_IMAGE_WIDTH - 1) || (y + i<0) || (y + i>CAM_IMAGE_WIDTH-1))break;
				Data1[y][x] += (float)((Data[y+i][x]) -(Data[y-N+i][x]));  //khk
			}
			Data1[y][x]/=(float)N;
		}
		
		if(MatchingFilter < 0)
			break;

		for (int y = MatchingFilter; y<CAM_IMAGE_HEIGHT - MatchingFilter; y++)
		{
			Data2[y][x]=0;
			for(i=0;i<MatchingFilter;i++){
				if((y+i<0) ||(y+i>479)||(y-MatchingFilter+i<0) ||(y-MatchingFilter+i>479))break;
				Data2[y][x] += (float)((Data1[y+i][x]) -(Data1[y-MatchingFilter+i][x]));  //khk
			}
			Data2[y][x]/=MatchingFilter;
		}
		ModifyEnd = 0;
	}
//	///////////////////////////////각 라인별 평균값 구하기////////////////////////////////////////////////////////////////
	LogWriteToFile(_T("각 라인별 평균값 구하기"));
	Cnt=0;
	for (int x = 0; x<CAM_IMAGE_WIDTH; x++){
		if (x == CAM_IMAGE_WIDTH/2)
			x=x;
		SumY[0][x]=0; SumY[1][x]=0; SumY[2][x]=0;
		for (int y = 40; y<CAM_IMAGE_HEIGHT - 40; y++){
			SumY[0][x] += Data[y][x];
			stdSumY[0][x] += Data[y][x]*Data[y][x];
			SumY[1][x] += Data1[y][x];
			stdSumY[1][x] += Data1[y][x]*Data1[y][x];
			SumY[2][x] += Data2[y][x];
			stdSumY[2][x] += Data2[y][x]*Data2[y][x];
			Cnt++;
		}
		for(i=0;i<3;i++){
			MeanY[i][x] = SumY[i][x]/(double)Cnt;
			stdY[i][x] = stdSumY[i][x]/(double)Cnt - MeanY[i][x]*MeanY[i][x];
		}
		Cnt=0;
	}

	///////////////////////////////각 라인별 멍이물 특징점 구하기////////////////////////////////////////////////////////////////
	CntNegative=0; CntPositive=0;EvaluationStart=0;NegativeStart=0;
	MeanData=0;  SumTemp=0;

	Sleep(50);
	LogWriteToFile(_T("각 라인별 멍이물 특징점 구하기"));
// 	for(int x=1; x<720; x++){
// 		CntNegative=0;
// 
// 		for(int y=0+20;y<480-20;y++){
// 			if(count >= 2000)
// 			{
// 				LogWriteToFile(_T("Count 2000 초과"));
// 				AfxMessageBox(_T("Count 2000 초과"));
// 			}else if((double)Data2[y][x]-MeanY[2][x]<0)
// 			{
// 				if(EvaluationStart==1)
// 				{  
// 					EvaluationStart=2;
// 					if(CntPositive>=10)
// 					{
// 						// 이물 검출 알고리즘
// 						EvaluationStart=0;
// 						SumTemp=0; SumPoint[count]=0;
// 						SumTemp2=0; SumPoint2[count]=0;
// 						for(i=0;i<CntNegative;i++)
// 						{
// 							if((y-CntPositive-1-i>0)&&(y-CntPositive-1-i<479))
// 							{
// 								SumTemp += (Data2[y-CntPositive-1-i][x]-MeanY[2][x]);
// 							}
// 						}
// 
// 						SumPoint[count]=SumTemp; //   /(double)CntNegative;
// 
// 						SumTemp=0; 
// 						for(i=0;i<CntNegative;i++)
// 						{
// 							if((y-CntPositive+i>0)&&(y-CntPositive+i<479))
// 							{
// 								SumTemp += (Data2[y-CntPositive+i][x]-MeanY[2][x]);
// 							}
// 						}
// 
// 						SumPoint[count]=SumTemp-SumPoint[count];
// 
// 
// 						for(i=0;i<CntNegative;i++)
// 						{
// 							if((y-CntPositive-1-i>0)&&(y-CntPositive-1-i<479))
// 							{
// 								SumTemp2 += (Data2[y-CntPositive-1-i][x-1]-MeanY[2][x-1]);
// 							}
// 						}
// 
// 						SumPoint2[count]=SumTemp2;
// 
// 						SumTemp2=0; 
// 
// 						for(i=0;i<CntNegative;i++)
// 						{
// 							if((y-CntPositive+i>0)&&(y-CntPositive+i<479))
// 							{
// 								SumTemp2 += (Data2[y-CntPositive+i][x-1]-MeanY[2][x-1]);
// 							}
// 						}
// 
// 						SumPoint2[count]=SumTemp2-SumPoint2[count];
// 						if(x==17&&y==190)
// 							x=x;
// 
// 						if((m_Area[x][y]==1)&&(SumPoint[count]>ThrPattern)/*&&(SumPoint2[count]>ThrPattern)*/&&((((x>12)&&(x<=70)))||((x>=650)&&(x<708))))	//이물이 맞으면
// 						{
// 							if(y-CntPositive-10>0)
// 							{
// 								DeadPixelLowData[count][0]=x;	
// 								DeadPixelLowData[count][1]=y-CntPositive-10;
// 
// 								if(count<1000)count++;
// 							}
// 						}
// 
// 						if(count >= 2000)
// 						{
// 							LogWriteToFile(_T("Count 2000 초과"));
// 							AfxMessageBox(_T("Count 2000 초과"));
// 						}
// 
// 						if((m_Area[x][y]==2)&&(SumPoint[count]>ThrPattern_B)/*&&(SumPoint2[count]>ThrPattern_B)*/&&((((x>12)&&(x<=70)))||((x>=650)&&(x<708))))	//이물이 맞으면
// 						{
// 							if(y-CntPositive-10>0)
// 							{
// 								DeadPixelLowData[count][0]=x;	
// 								DeadPixelLowData[count][1]=y-CntPositive-10;
// 								if(count<1000)count++;
// 							}
// 						}
// 
// 						if(count >= 2000)
// 						{
// 							LogWriteToFile(_T("Count 2000 초과"));
// 							AfxMessageBox(_T("Count 2000 초과"));
// 						}
// 						if((m_Area[x][y]==3)&&(SumPoint[count]>ThrPattern_C)/*&&(SumPoint2[count]>ThrPattern_C)*/&&((((x>12)&&(x<=70)))||((x>=650)&&(x<708))))	//이물이 맞으면
// 						{
// 							if(y-CntPositive-10>0)
// 							{
// 								DeadPixelLowData[count][0]=x;	
// 								DeadPixelLowData[count][1]=y-CntPositive-10;
// 
// 								if(count<1000)count++;
// 							}
// 						}
// 
// 						if(count >= 2000)
// 						{
// 							LogWriteToFile(_T("Count 2000 초과"));
// 							AfxMessageBox(_T("Count 2000 초과"));
// 						}
// 					}
// 
// 					CntPositive=0; CntNegative=0;//초기화
// 				}
// 				else if(CntPositive != 0) CntPositive=0;
// 
// 				CntNegative ++;
// 
// 			}
// 			else {
// 				if(EvaluationStart == 2 )
// 				{
// 					EvaluationStart = 0 ;
// 					CntNegative=0;
// 				}
// 
// 				if((CntNegative>=10)&&(EvaluationStart == 0)){
// 					EvaluationStart = 1;
// 				}
// 				CntPositive++;
// 			}
// 		}
// 	}

	LogWriteToFile(_T("한줄단위 그래프 그리기"));

	//if(((CFINAL_TEST_MDlg *)m_pMomWnd)->b_SecretOption ==1){
	//	cvShowImage("Region",Image);  // 이 윈도우에 내가 만든 ipl_Img 이미지를 보이고
	//	cvWaitKey(0);                           // 키입력을 받은후에
	//	cvDestroyWindow("Region");        // 윈도우를 종료
	//}
	cvReleaseImage(&Image);

	int ExitLoop=0;
	int SkipCnt=0;
	ParticleCnt=0;

	if(count>=1){
		double Dist1, MinDist, SumX,SumY;
		double DistMoveCenter[2];
		long IndexMin;
		int MoveCheck=0,ExitGrouping=0;
		double DistMoveCenterX=0, DistMoveCenterY=0;
		double NewCenterX, NewCenterY ;
		int IndexAdd=0;
		
		double SplitFactor=10;
		int CheckExit=0;

		Group=2;
		DeadPixel[0][0][0]=DeadPixelLowData[0][0]; DeadPixel[0][0][1]=DeadPixelLowData[0][1];  // 2개 Group의 초기값 설정
		CenterPoint[0][0]=(double)DeadPixelLowData[0][0]; CenterPoint[0][1]=(double)DeadPixelLowData[0][1];  // 2개 Group의 초기값 설정

		DeadPixel[1][0][0]=DeadPixelLowData[1][0]; DeadPixel[1][0][1]=DeadPixelLowData[1][1];  // 2개 Group의 초기값 설정
		CenterPoint[1][0]=(double)DeadPixelLowData[1][0]; CenterPoint[1][1]=(double)DeadPixelLowData[1][1];  // 2개 Group의 초기값 설정
		
		LogWriteToFile(_T("가장 가까운 Group 찾기"));
		for(int Loop=0;Loop<500 && ExitLoop==0;Loop++){
			ExitGrouping=0;
			for(i=0;i<1000 && ExitGrouping==0;i++){
				for(k=0;k<50;k++)IndexG[k]=0;
				for(j=0;j<count;j++){
					if (DeadPixelLowData[j][0]>CAM_IMAGE_WIDTH || DeadPixelLowData[j][0] < 0 || DeadPixelLowData[j][1] >CAM_IMAGE_HEIGHT || DeadPixelLowData[j][1] < 0) continue;

					////////////////////////////// 가장 가까운 Group 찾기///////////////////////
					MinDist=999999; IndexMin=0; //초기화
					for(k=0;k<Group;k++){
						Dist1=Distance(CenterPoint[k][0],CenterPoint[k][1], DeadPixelLowData[j][0], DeadPixelLowData[j][1]);
						if(Dist1<MinDist){
							IndexMin = k;
							MinDist = Dist1;
						}
					}
					if(Group>48)
						Group=Group;
					DeadPixel[IndexMin][IndexG[IndexMin]][0]=DeadPixelLowData[j][0];
					DeadPixel[IndexMin][IndexG[IndexMin]][1]=DeadPixelLowData[j][1];
					DeadPixel[IndexMin][IndexG[IndexMin]][2]=j;
					IndexG[IndexMin]++;
				}
				//////////////////////////////  각 Group의 중심점 구하기 ///////////////////////
				
				ExitGrouping=1;
				
				for(k=0;k<Group;k++){
					if(IndexG[k]==0) {
						if(k<Group-1){
							for(int m = 0;m<Group-1;m++){

								IndexG[k+m] = IndexG[k+m+1];
								for(int p=0;p<4;p++)CenterPoint[k+m][p] = CenterPoint[k+m+1][p];

								for(int n = 0;n<IndexG[k+m];n++){
									DeadPixel[k+m][n][0] = DeadPixel[k+m+1][n][0];
									DeadPixel[k+m][n][1] = DeadPixel[k+m+1][n][1];
								}
							}
						}
						Group--;
						SplitFactor ++ ;
						CheckExit++;
						continue;
					}

					SumX=0;SumY=0;

					for(j=0;j<IndexG[k];j++){
						SumX += (double)DeadPixel[k][j][0];
						SumY += (double)DeadPixel[k][j][1];
					}
				
					NewCenterX = ((double) SumX / (double) IndexG[k]);
					NewCenterY = ((double) SumY / (double) IndexG[k]);

					DistMoveCenterX = fabs(NewCenterX - (double)CenterPoint[k][0]);
					DistMoveCenterY = fabs(NewCenterY - (double)CenterPoint[k][1]);

					if(DistMoveCenterX>0.1 || DistMoveCenterY > 0.1){
						ExitGrouping=0; 
						CenterPoint[k][0] = NewCenterX;
						CenterPoint[k][1] = NewCenterY;
					}
					
				}
			} //  할당된 Group개수 만큼 분류 완료

			//////////////////////////////  Group 분리 및 종료 Check ///////////////////////
			IndexAdd=0;
			int Split=0;
			double SumDarkness=0;

			for(k=0;k<Group&&Split==0&&CheckExit<2;k++){
				SumX=0;SumY=0;SumDarkness=0;
				for(j=0;j<IndexG[k];j++){

					SumX += (double)DeadPixel[k][j][0]*(double)DeadPixel[k][j][0];
					SumY += (double)DeadPixel[k][j][1]*(double)DeadPixel[k][j][1];
					SumDarkness += SumPoint[DeadPixel[k][j][2]];	// Group 별 Threshold 값 계산////////////////////////////////////////////////////

				
				}
				SumX = sqrt(SumX/(double)IndexG[k]-CenterPoint[k][0]*CenterPoint[k][0]); CenterPoint[k][2]=SumX;
				SumY = sqrt(SumY/(double)IndexG[k]-CenterPoint[k][1]*CenterPoint[k][1]); CenterPoint[k][3]=SumY;
				SumDarkness /= (double) IndexG[k]; 
				//P_RECT[k].m_Concentration = SumPoint[k];
				if(sqrt(SumX*SumX+SumY*SumY)> (double)30){ // Group의 산포가 너무 크면 2개로 분할

					if(Group<49&&Split==0){
						IndexAdd ++;

						CenterPoint[k][0] = DeadPixel[k][0][0] ;
						CenterPoint[k][1] = DeadPixel[k][0][1] ;

						CenterPoint[Group][0] = DeadPixel[k][1][0] ;
						CenterPoint[Group][1] = DeadPixel[k][1][1] ;
						
					}
					Split=1;
				}				
			}
			Group += IndexAdd; // Group 확장
			SkipCnt=0;
			int DustNumOfSector=m_DustNumOfSector; // 작은 멍은 제거할수 있도록 설정
			int DustNumOfSector_B=m_DustNumOfSector_B; 
			//int DustNumOfSector_C = m_DustNumOfSector_C;
			int x1,y1;

			if(IndexAdd==0){      //  Grouping 종료 설정 및 이물 영역 출력 설정 
				ParticleCnt = Group;
				
				for(k=0;k<Group;k++){
					x1= (int)(CenterPoint[k][0]);			y1= (int)(CenterPoint[k][1]);

					//if(((m_Area[x1][y1]==1)&&(IndexG[k]<=DustNumOfSector))||((m_Area[x1][y1]==2)&&(IndexG[k]<=DustNumOfSector_B))
					//	/*||  ((m_Area[x1][y1]==3)&&(IndexG[k]<=DustNumOfSector_C))*/ ) {//Option 처리
					//	P_RECT[k-SkipCnt].m_PosX=0;P_RECT[k-SkipCnt].m_PosY=0;
					//	P_RECT[k-SkipCnt].EX_RECT_SET(P_RECT[k-SkipCnt].m_PosX,P_RECT[k-SkipCnt].m_PosY,0,0);//

					//	ParticleCnt--;
					//	SkipCnt++;

					//	continue;
					//}

					//P_RECT[k-SkipCnt].m_PosX=CenterPoint[k][0];
					//P_RECT[k-SkipCnt].m_PosY=CenterPoint[k][1];
					//P_RECT[k-SkipCnt].EX_RECT_SET(P_RECT[k-SkipCnt].m_PosX,P_RECT[k-SkipCnt].m_PosY,(unsigned int)Cell+CenterPoint[k][2]*1.5,(unsigned int)Cell+CenterPoint[k][3]*1.5);//

					//P_RECT[k-SkipCnt].m_Concentration = /*SumPoint[k-SkipCnt]*/;
				}
				ExitLoop=1;
			}
			ParticleCnt += BigCnt;
			CString str="";
			str.Empty();
			str.Format("%d",ParticleCnt);
			/*m_ParticleList.SetItemText(InsertIndex,5,str);
			((CFINAL_TEST_MDlg *)m_pMomWnd)->PQMS_TESTRESULT[9][0] = str;
			((CFINAL_TEST_MDlg *)m_pMomWnd)->m_pResParticleOptWnd->PARTICLENUM_TEXT(str);*/

		}

		ParticleCnt =  counter;
		if(Group>50) {Group=50; BigCnt=0; ParticleCnt=50;  }
		if(Group + BigCnt>50) { BigCnt= 50- Group; ParticleCnt=50;  }

		for(k=Group;k<Group+BigCnt;k++){
			P_RECT[k-SkipCnt].m_PosX=BigDustPosition[k-Group][0];P_RECT[k-SkipCnt].m_PosY=BigDustPosition[k-Group][1];
			P_RECT[k-SkipCnt].EX_RECT_SET(P_RECT[k-SkipCnt].m_PosX,P_RECT[k-SkipCnt].m_PosY,BigDustPosition[k-Group][2],BigDustPosition[k-Group][2]);//
		}
	}

	///신규 농도 추가 1005
	Group = ParticleCnt;

	int Mid_x = 0;
	int Mid_y = 0;

	for (int lop = 0; lop < ParticleCnt ; lop++)
	{
		int centerSum = 0, centerCount = 0;	double center;
		int side1_Sum = 0, side1_Count = 0;	double side;
		int side2_Sum = 0, side2_Count = 0;
		//Mid_x = P_RECT[lop].m_Left + m_pstParticleData->ErrRegionList[lop].RegionList.right) / 2;
		//Mid_y = (m_pstParticleData->ErrRegionList[lop].RegionList.top + m_pstParticleData->ErrRegionList[lop].RegionList.bottom) / 2;
		////for (int y = Mid_y - 1; y <= Mid_y + 1; y++)
		////{
		////	for (int x = Mid_x - 1; x <= Mid_x + 1; x++)
		////	{
		////		if (y > 0 && y < 480 && x > 0 && x < 720)
		////		{
		////			centerSum += Data[y][x];
		////			centerCount++;
		////		}
		////	}
		////}
		
		int size_x = P_RECT[lop].m_Right - P_RECT[lop].m_Left;
		int size_y = P_RECT[lop].m_Bottom - P_RECT[lop].m_Top;
		
		for (int y = P_RECT[lop].m_Top - size_y; y <=  P_RECT[lop].m_Bottom + size_y; y++)
		{
			for (int x =  P_RECT[lop].m_Left - size_x; x <= P_RECT[lop].m_Right + size_x; x++)
			{
				if (y > 0 && y < CAM_IMAGE_HEIGHT && x > 0 && x < CAM_IMAGE_WIDTH)
				{
					side1_Sum += Data[y][x];
					side1_Count++;
				}

			}
		}

		for (int y = P_RECT[lop].m_Top ; y <= P_RECT[lop].m_Bottom ; y++)
		{
			for (int x = P_RECT[lop].m_Left ; x <= P_RECT[lop].m_Right ; x++)
			{
				if (y > 0 && y < CAM_IMAGE_HEIGHT && x > 0 && x < CAM_IMAGE_WIDTH)
				{
					side2_Sum += Data[y][x];
					side2_Count++;
				}
			}
		}

		for (int y = P_RECT[lop].m_Top ; y <= P_RECT[lop].m_Bottom ; y++)
		{
			for (int x = P_RECT[lop].m_Left ; x <= P_RECT[lop].m_Right ; x++)
			{
				if (y > 0 && y < CAM_IMAGE_HEIGHT && x > 0 && x < CAM_IMAGE_WIDTH)
				{
					centerSum += Data[y][x];
					centerCount++;
				}
			}
		}

		center = (double)centerSum / (double)centerCount;
		side = (double)(side1_Sum-side2_Sum) / (double)(side1_Count-side2_Count);

		//m_pstParticleData->ErrRegionList[lop].stain_Blemish = (center / AVG_Y) * 100;
		P_RECT[lop].m_Concentration = 100 - (center / side) * 100;

		if (P_RECT[lop].m_Concentration > 100)
			P_RECT[lop].m_Concentration = 100;
		else if (P_RECT[lop].m_Concentration < 0)
			P_RECT[lop].m_Concentration = 0;



		/*if (center < AVG_Y)
			m_pstParticleData->ErrRegionList[lop].stain_Blemish = (center / AVG_Y) * 100;
		else
			m_pstParticleData->ErrRegionList[lop].stain_Blemish = (AVG_Y / center) * 100;*/


	}


	for (int lop = 0; lop < Group; lop++)
	{
		for (int lop3 = 0; lop3 < ParticleCnt; lop3++)
		{
			int Center_X = (P_RECT[lop3].m_Left + P_RECT[lop3].m_Right)/2;
			int Center_Y = (P_RECT[lop3].m_Top + P_RECT[lop3].m_Bottom)/2;;
			//// 중심 -> A
			if (P_RECT[lop3].m_Concentration <= m_stainBlemish[2] && m_Area[Center_X][Center_Y] ==1 && P_RECT[lop3].m_Concentration >= 0)
			{
				for (int lop2 = lop3; lop2 < ParticleCnt; lop2++)
				{

					
					//m_pstParticleData->ErrRegionList[lop2].RegionList.CenterPoint().x = m_pstParticleData->ErrRegionList[lop2 +1 ].RegionList.CenterPoint().x;
					//m_pstParticleData->ErrRegionList[lop2].RegionList.CenterPoint().y = m_pstParticleData->ErrRegionList[lop2 + 1].RegionList.CenterPoint().y;
				
					P_RECT[lop2].m_Top = P_RECT[lop2+1].m_Top;
					P_RECT[lop2].m_Bottom = P_RECT[lop2+1].m_Bottom;
					P_RECT[lop2].m_Left = P_RECT[lop2+1].m_Left;
					P_RECT[lop2].m_Right = P_RECT[lop2+1].m_Right;
					

				//	P_RECT[lop2].Y_result = P_RECT[lop2 + 1].Y_result;
					P_RECT[lop2].m_Concentration = P_RECT[lop2+1].m_Concentration;
				}

				ParticleCnt--;
				break;
			}

			//// 중간 -> B
			if (P_RECT[lop3].m_Concentration <= m_stainBlemish[1]  && m_Area[Center_X][Center_Y] ==2 && P_RECT[lop3].m_Concentration >= 0)
			{
				for (int lop2 = lop3; lop2 < ParticleCnt; lop2++)
				{

					
					//m_pstParticleData->ErrRegionList[lop2].RegionList.CenterPoint().x = m_pstParticleData->ErrRegionList[lop2 +1 ].RegionList.CenterPoint().x;
					//m_pstParticleData->ErrRegionList[lop2].RegionList.CenterPoint().y = m_pstParticleData->ErrRegionList[lop2 + 1].RegionList.CenterPoint().y;
				
					P_RECT[lop2].m_Top = P_RECT[lop2+1].m_Top;
					P_RECT[lop2].m_Bottom = P_RECT[lop2+1].m_Bottom;
					P_RECT[lop2].m_Left = P_RECT[lop2+1].m_Left;
					P_RECT[lop2].m_Right = P_RECT[lop2+1].m_Right;
					

				//	P_RECT[lop2].Y_result = P_RECT[lop2 + 1].Y_result;
					P_RECT[lop2].m_Concentration = P_RECT[lop2+1].m_Concentration;
				}

				ParticleCnt--;
				break;
			}
			//// 바깥 -> C
			if (P_RECT[lop3].m_Concentration <= m_stainBlemish[0]  && m_Area[Center_X][Center_Y] ==3 && P_RECT[lop3].m_Concentration >= 0)
			{
				for (int lop2 = lop3; lop2 < ParticleCnt; lop2++)
				{

					
					//m_pstParticleData->ErrRegionList[lop2].RegionList.CenterPoint().x = m_pstParticleData->ErrRegionList[lop2 +1 ].RegionList.CenterPoint().x;
					//m_pstParticleData->ErrRegionList[lop2].RegionList.CenterPoint().y = m_pstParticleData->ErrRegionList[lop2 + 1].RegionList.CenterPoint().y;
				
					P_RECT[lop2].m_Top = P_RECT[lop2+1].m_Top;
					P_RECT[lop2].m_Bottom = P_RECT[lop2+1].m_Bottom;
					P_RECT[lop2].m_Left = P_RECT[lop2+1].m_Left;
					P_RECT[lop2].m_Right = P_RECT[lop2+1].m_Right;
					

				//	P_RECT[lop2].Y_result = P_RECT[lop2 + 1].Y_result;
					P_RECT[lop2].m_Concentration = P_RECT[lop2+1].m_Concentration;
				}

				ParticleCnt--;
				break;
			}
		}

	}

	for (int i = 0; i < ParticleCnt; i++)
	{
		if (P_RECT[i].m_Concentration > 100)
			P_RECT[i].m_Concentration = 100;
		else if (P_RECT[i].m_Concentration < 0)
			P_RECT[i].m_Concentration = 0;
	}




	if(ParticleCnt > m_PASSNUM){
	//	((CFINAL_TEST_MDlg *)m_pMomWnd)->m_pResParticleOptWnd->RESULT_TEXT(2,"FAIL");
		R_RECT[0].m_Success = FALSE;
	}	
	else{
	//	((CFINAL_TEST_MDlg *)m_pMomWnd)->m_pResParticleOptWnd->RESULT_TEXT(1,"PASS");
		R_RECT[0].m_Success = TRUE;
	}
//
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++){
		if(i==76)
			i=i;
		free(Data1[i]);
	}
	free(Data1);

	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)free(Data[i]);	free(Data);
	
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)free(Data2[i]);	free(Data2);
//
//	((CFINAL_TEST_MDlg *)m_pMomWnd)->Wait2(1000);
	return R_RECT[0].m_Success;
}
bool CParticle_Option::ParticleGen_16bit(LPWORD IN_GRAY, LPBYTE IN_RGB, int NUM){

	//	R_RECT[0].m_Success = FALSE;
	int Step = m_Black_STEP;

	BYTE R, G, B;
	//DWORD	RGBPIX = 0;
	int index = 0;
	float **Data, **Data1, **Data2;

	int startx = 0, starty = 0, endx = CAM_IMAGE_WIDTH, endy = CAM_IMAGE_HEIGHT;
	int HEIGHT = CAM_IMAGE_HEIGHT, WIDTH = CAM_IMAGE_WIDTH, Xc, Yc, Radius;
	long i, *IndexLS, Index;
	int x, y;
	int Group = 0;
	long count = 0;

	unsigned int DeadPixel[55][256][4];
	double CenterPoint[55][4];
	unsigned int DeadPixelLowData[2000][4] = { 0, };

	unsigned int IndexG[55];
	long k, j;
	unsigned int Cell = 30;

	// 선언
	Data = (float **)malloc(sizeof(float *)*CAM_IMAGE_HEIGHT);
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)Data[i] = (float *)malloc(sizeof(float)* CAM_IMAGE_WIDTH);

	Data1 = (float **)malloc(sizeof(float *)*CAM_IMAGE_HEIGHT);
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)Data1[i] = (float *)malloc(sizeof(float)* CAM_IMAGE_WIDTH);

	Data2 = (float **)malloc(sizeof(float *)*CAM_IMAGE_HEIGHT);
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)Data2[i] = (float *)malloc(sizeof(float)* CAM_IMAGE_WIDTH);


	DWORD RGBLINE = starty * CAM_IMAGE_WIDTH;

	Xc = WIDTH / 2; Yc = HEIGHT / 2;

	count = 0;	Group = 0;

 	for (int lopy = starty; lopy < endy; lopy++){
 		for (int lopx = startx; lopx < endx; lopx++){

			Data[lopy][lopx] = (float)IN_GRAY[lopy * CAM_IMAGE_WIDTH + lopx];
		}
	}

	Lump_detection(IN_RGB);

	for (int i = 0; i < counter; i++){
		P_RECT[i].m_Left = rectArray[i].x;
		P_RECT[i].m_Top = rectArray[i].y;
		P_RECT[i].m_Right = rectArray[i].x + rectArray[i].width;
		P_RECT[i].m_Bottom = rectArray[i].y + rectArray[i].height;

		count++;
	}

	delete[]rectArray;

// 	cvReleaseImage(&DSTImage);
// 	cvReleaseImage(&OriginImage);

//	IplImage *Image = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_16U, 1);
	bool skip = 0;
	int BigCnt = 0;
	int BigDustPosition[10000][3] = { 0, };

	int THR_OFFSET = /*(AVG_Y - 112)/(double) 5*/ 0; //유동 Threshold 제거
	count = counter;
	double ThrPattern = m_Lens_Shading_W + THR_OFFSET; //멍 이물 검출을 위한 한계값. -> Center
	double ThrPattern_B = m_Lens_Shading_W_B + THR_OFFSET; //멍 이물 검출을 위한 한계값. -> Side
	double ThrPattern_C = m_Lens_Shading_W_C + THR_OFFSET; //멍 이물 검출을 위한 한계값. -> 카메라영역 (최외각)

	double ThrHigh = P_Black_Threshold; //밝기의 변화량이 너무 크면 Overlay일 확률이 크므로... 제외하기 위한 한계값 일반적으로 30 정도 사용
	double ThrHigh_B = P_Black_Threshold_B; //
	double ThrHigh_C = P_Black_Threshold_C;// -> 최외각


	int CntNegative = 0, CntPositive = 0, EvaluationStart = 0, NegativeStart = 0;
	double MeanData = 0, SumPoint[2000] = { 0, }, SumTemp = 0, SumPoint2[2000] = { 0, }, SumTemp2 = 0;

	int Mg = 3, N = 2, MatchingFilter = 10, Cnt = 0, OvSet = 0;
	int Mgin = 5; //오버레이 근처 영역 제거 마진
	int ModifyEnd = 0;
	double Sum[3][CAM_IMAGE_HEIGHT] = { 0, }, stdSum[3][CAM_IMAGE_HEIGHT] = { 0, }, MeanX[3][CAM_IMAGE_HEIGHT] = { 0, }, stdX[3][CAM_IMAGE_HEIGHT] = { 0, };

	/////////////////////////  밝기 변화 형태로 변환 X 방향 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	LogWriteToFile(_T("밝기 변환 형태로 변환 X 방향"));
	for (int y = 0; y<CAM_IMAGE_HEIGHT; y++){
		for (int x = N; x<CAM_IMAGE_WIDTH - N; x++){
			Data1[y][x] = 0;
			for (i = 0; (i<N); i++){
				if ((x - N + i<0) || (x - N + i>CAM_IMAGE_WIDTH - 1) || (x + i<0) || (x + i>CAM_IMAGE_WIDTH-1))break;
				Data1[y][x] += (float)((Data[y][x + i]) - (Data[y][x - N + i]));  //khk
			}
			Data1[y][x] /= (float)N;
		}

		for (int x = N; x<CAM_IMAGE_WIDTH - N; x++){
			if (m_Expand_OverlayArea[x][y] == 0){
				int i50;
				if ((ModifyEnd<x) && (x<CAM_IMAGE_WIDTH - 10)){
					for (i50 = x; i50<CAM_IMAGE_WIDTH - N - Mgin; i50++){
						if (m_Expand_OverlayArea[i50][y] == 1){
							ModifyEnd = i50;
							break;
						}
					}
					if (i50<CAM_IMAGE_WIDTH - N){
						double Start = 0, End = 0;
						for (i50 = 0; i50<6; i50++){
							if ((x - Mgin - i50<0) || (x - Mgin - i50>CAM_IMAGE_WIDTH - 1) || (ModifyEnd + Mgin + i50<0) || (ModifyEnd + Mgin + i50>CAM_IMAGE_WIDTH - 1))break;
							Start += Data1[y][x - Mgin - i50];
							End += Data1[y][ModifyEnd + Mgin + i50];
						}
						Start /= 6.0;
						End /= 6.0;

						for (i50 = 0; i50<ModifyEnd - x + Mgin * 2; i50++){
							if ((x - Mgin - i50<0) || (x - Mgin - i50>CAM_IMAGE_WIDTH - 1) || (ModifyEnd - x + Mgin * 2<0) || (ModifyEnd - x + Mgin * 2>CAM_IMAGE_WIDTH - 1))break;
							Data1[y][x - Mgin + i50] = (float)((End - Start) / (double)(ModifyEnd - x + Mgin * 2)*i50) + Start;
						}
					}
				}
			}
		}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		for (int x = MatchingFilter; x<CAM_IMAGE_WIDTH - MatchingFilter; x++)
		{
			Data2[y][x] = 0;
			for (i = 0; i<MatchingFilter; i++){
				if ((x + i<0) || (x + i>CAM_IMAGE_WIDTH - 1) || (x - MatchingFilter + i<0) || (x - MatchingFilter + i>CAM_IMAGE_WIDTH - 1))break;
				Data2[y][x] += (float)((Data1[y][x + i]) - (Data1[y][x - MatchingFilter + i]));  //khk
			}
			Data2[y][x] /= MatchingFilter;
		}
		ModifyEnd = 0;
	}
	///////////////////////////////각 라인별 평균값 구하기////////////////////////////////////////////////////////////////
	LogWriteToFile(_T("각 라인별 평균값 구하기"));
	Cnt = 0;
	for (int y = 0; y<CAM_IMAGE_HEIGHT; y++){
		for (int x = 20; x<CAM_IMAGE_WIDTH - 20; x++){
			Sum[0][y] += Data[y][x];
			stdSum[0][y] += Data[y][x] * Data[y][x];
			Sum[1][y] += Data1[y][x];
			stdSum[1][y] += Data1[y][x] * Data1[y][x];
			Sum[2][y] += Data2[y][x];
			stdSum[2][y] += Data2[y][x] * Data2[y][x];
			Cnt++;
		}
		for (i = 0; i<3; i++){
			MeanX[i][y] = Sum[i][y] / (double)Cnt;
			stdX[i][y] = stdSum[i][y] / (double)Cnt - MeanX[i][y] * MeanX[i][y];
		}
		Cnt = 0;
	}

	///////////////////////////////각 라인별 멍이물 특징점 구하기////////////////////////////////////////////////////////////////

	//	count=0;
	CntNegative = 0; CntPositive = 0; EvaluationStart = 0; NegativeStart = 0;
	MeanData = 0;// SumPoint[1000]={0,}, 
	SumTemp = 0;
	double Max_Concen = 0;
	LogWriteToFile(_T("각 라인별 멍이물 특징점 구하기"));
	BOOL Flag = FALSE;
	int Range_Stain = 10;
	int defect_Step = 3;

	for (int y = 1; y<CAM_IMAGE_HEIGHT; y++){
		CntNegative = 0;

		for (int x = 20; x<CAM_IMAGE_WIDTH - 20; x++){
			if (((Data[y][x] - Data[y][x + 3]) > ThrPattern) && (m_Area[x][y] == 1) && y < CAM_IMAGE_HEIGHT - 5 && x < CAM_IMAGE_WIDTH){

				DeadPixelLowData[count][0] = x;
				DeadPixelLowData[count][1] = y;
				P_RECT[count].m_Left = x - Range_Stain + defect_Step;
				P_RECT[count].m_Top = y - Range_Stain + defect_Step;
				P_RECT[count].m_Right = x + (Range_Stain)+defect_Step;
				P_RECT[count].m_Bottom = y + Range_Stain + defect_Step;



				P_RECT[count].m_Concentration = Data[y][x] - Data[y][x + defect_Step];

				if (P_RECT[count].m_Concentration > Max_Concen){
					Max_Concen = P_RECT[count].m_Concentration;
				}

				Flag = TRUE;
				count++;
				y += 10;
				break;
			}
			if (((Data[y][x] - Data[y][x + 3]) > ThrPattern_B) && (m_Area[x][y] == 2) && y < CAM_IMAGE_HEIGHT - 5 && x < CAM_IMAGE_WIDTH){

				DeadPixelLowData[count][0] = x;
				DeadPixelLowData[count][1] = y;
				P_RECT[count].m_Left = x - Range_Stain + defect_Step;
				P_RECT[count].m_Top = y - Range_Stain + defect_Step;
				P_RECT[count].m_Right = x + (Range_Stain)+defect_Step;
				P_RECT[count].m_Bottom = y + Range_Stain + defect_Step;



				P_RECT[count].m_Concentration = Data[y][x] - Data[y][x + defect_Step];

				if (P_RECT[count].m_Concentration > Max_Concen){
					Max_Concen = P_RECT[count].m_Concentration;
				}

				Flag = TRUE;
				count++;
				y += 10;
				break;
			}
			if (((Data[y][x] - Data[y][x + 3]) > ThrPattern_C) && (m_Area[x][y] == 3) && y < CAM_IMAGE_HEIGHT - 5 && x < CAM_IMAGE_WIDTH){

				DeadPixelLowData[count][0] = x;
				DeadPixelLowData[count][1] = y;
				P_RECT[count].m_Left = x - Range_Stain + defect_Step;
				P_RECT[count].m_Top = y - Range_Stain + defect_Step;
				P_RECT[count].m_Right = x + (Range_Stain)+defect_Step;
				P_RECT[count].m_Bottom = y + Range_Stain + defect_Step;



				P_RECT[count].m_Concentration = Data[y][x] - Data[y][x + defect_Step];

				if (P_RECT[count].m_Concentration > Max_Concen){
					Max_Concen = P_RECT[count].m_Concentration;
				}

				Flag = TRUE;
				count++;
				y += 10;
				break;
			}
			if ((double)Data2[y][x] - MeanX[2][y]<0){

				if (EvaluationStart == 1){
					EvaluationStart = 2;
					if (CntPositive >= 10){
						// 이물 검출 알고리즘
						EvaluationStart = 0;
						SumTemp = 0; SumPoint[count] = 0;
						SumTemp2 = 0; SumPoint2[count] = 0;

						for (i = 0; i<CntNegative; i++){
							if ((x - CntPositive - 1 - i>0) && (x - CntPositive - 1 - i<719)){
								SumTemp += (Data2[y][x - CntPositive - 1 - i] - MeanX[2][y]);
							}
						}

						SumPoint[count] = SumTemp;

						SumTemp = 0;
						for (i = 0; i<CntNegative; i++){
							if ((x - CntPositive + i>0) && (x - CntPositive + i<719)){
								SumTemp += (Data2[y][x - CntPositive + i] - MeanX[2][y]);
							}
						}
						SumPoint[count] = SumTemp - SumPoint[count];

						for (i = 0; i<CntNegative; i++){
							if ((x - CntPositive - 1 - i>0) && (x - CntPositive - 1 - i<719)){
								SumTemp2 += (Data2[y - 1][x - CntPositive - 1 - i] - MeanX[2][y - 1]);

							}
						}

						SumPoint2[count] = SumTemp2; //   /(double)CntNegative;

						SumTemp2 = 0;

						for (i = 0; i<CntNegative; i++){
							if ((x - CntPositive + i>0) && (x - CntPositive + i<CAM_IMAGE_WIDTH - 1)){
								SumTemp2 += (Data2[y - 1][x - CntPositive + i] - MeanX[2][y - 1]);

							}
						}
						SumPoint2[count] = SumTemp2 - SumPoint2[count];

						///////////////////////////////  A 존 검출  /////////////////////////////중앙 노란색
						// 						if((m_Area[x][y]==1)&&(SumPoint[count]>ThrPattern)&&(SumPoint[count]<ThrHigh)/*&&(SumPoint2[count]>ThrPattern)&&(SumPoint2[count]<ThrHigh)*/&&((x>23)&&(x<697))){//이물이 맞으면
						// 							if(x-CntPositive-10>0){
						// 
						// 								if(count < 1000)
						// 								{
						// 									DeadPixelLowData[count][0]=x-CntPositive-10;	
						// 									DeadPixelLowData[count][1]=y;
						// 								}else
						// 								{
						// 								//	AfxMessageBox(_T("Error A Zone"));
						// 								}
						// 								
						// 								if(count<1000)count++;
						// 							}
						// 						}
						// 						///////////////////////////////  B 존 검출  /////////////////////////////바깥쪽 초록색
						// 						if((m_Area[x][y]==2)&&(SumPoint[count]>ThrPattern_B)&&(SumPoint[count]<ThrHigh_B)/*&&(SumPoint2[count]>ThrPattern_B)&&(SumPoint2[count]<ThrHigh_B)*/&&((x>23)&&(x<697))){//이물이 맞으면
						// 							if(x-CntPositive-10>0){
						// 								if(count < 1000)
						// 								{
						// 									DeadPixelLowData[count][0]=x-CntPositive-10;	
						// 									DeadPixelLowData[count][1]=y;
						// 								}else
						// 								{
						// 								//	AfxMessageBox(_T("Error B Zone"));
						// 								}
						// 								if(count<1000)count++;
						// 							}
						// 						}
						// 						////////////////////////////////  C 존 검출  ///////////////////////////////외각
						// 						if((m_Area[x][y]==3)&&(SumPoint[count]>ThrPattern_C)&&(SumPoint[count]<ThrHigh_C)/*&&(SumPoint2[count]>ThrPattern_C)&&(SumPoint2[count]<ThrHigh_C)*/&&((x>23)&&(x<697))){//이물이 맞으면
						// 							if(x-CntPositive-10>0){
						// 
						// 								if(count < 1000)
						// 								{
						// 									DeadPixelLowData[count][0]=x-CntPositive-10;	
						// 									DeadPixelLowData[count][1]=y;
						// 								}else
						// 								{
						// 								//	AfxMessageBox(_T("Error C Zone"));
						// 								}
						// 
						// 								if(count<1000)count++;
						// 							}
						// 						}
					}
					CntPositive = 0; CntNegative = 0;//초기화
				}
				else if (CntPositive != 0) CntPositive = 0;

				CntNegative++;

			}
			else {
				if (EvaluationStart == 2){
					EvaluationStart = 0;
					CntNegative = 0;
				}


				if ((CntNegative >= 10) && (EvaluationStart == 0)){
					EvaluationStart = 1;
				}
				CntPositive++;
			}
		}
	}


	for (int i = 0; i < count; i++){
		P_RECT[i].m_Concentration = Max_Concen;
	}

	//
	//	/////////////////////////한줄단위 그래프 그리기////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	if(((CFINAL_TEST_MDlg *)m_pMomWnd)->b_SecretOption == TRUE){
	//		IplImage *temp_image = cvCreateImage(cvSize(720, 255), IPL_DEPTH_8U, 3);
	//		IplImage *temp_image1 = cvCreateImage(cvSize(720, 255), IPL_DEPTH_8U, 3);
	//		IplImage *temp_image2 = cvCreateImage(cvSize(720, 255), IPL_DEPTH_8U, 3);
	//
	//		cvSetZero(temp_image);
	//		cvSetZero(temp_image1);
	//		cvSetZero(temp_image2);
	//
	//		BYTE Sum_Y, Sum_Y1,Sum_Y2;
	//
	//		int Y_POS = GetDlgItemInt(IDC_EDIT3);
	//
	//
	//		for(int x=0; x<720; x++)
	//		{
	//			/////////////
	//			if(Data2[Y_POS][x]*16+100>255){
	//				Sum_Y2=255;
	//			}
	//			else if(Data2[Y_POS][x]*16+100<0){
	//				Sum_Y2=0;
	//			}
	//			else{
	//				Sum_Y2 = (BYTE)(Data2[Y_POS][x]*16+100); 
	//			}
	//			//////////////
	//			if(Data1[Y_POS][x]*16+100>255){
	//				Sum_Y1=255;
	//			}
	//			else if(Data1[Y_POS][x]*16+100<0){
	//				Sum_Y1=0;
	//			}
	//			else{
	//				Sum_Y1 = (BYTE)(Data1[Y_POS][x]*16+100); 
	//			}
	//			///////////////		
	//			Sum_Y = (BYTE)(Data[Y_POS][x]); 
	//
	//			cvLine(temp_image1, cvPoint(x, temp_image1->height), cvPoint(x, temp_image1->height - Sum_Y1), CV_RGB(255, 255, 255), 1, 8);
	//			cvLine(temp_image, cvPoint(x, temp_image->height), cvPoint(x, temp_image->height - Sum_Y), CV_RGB(255, 255, 255), 1, 8);
	//			cvLine(temp_image2, cvPoint(x, temp_image2->height), cvPoint(x, temp_image2->height - Sum_Y2), CV_RGB(255, 255, 255), 1, 8);
	//		}
	//		cvLine(temp_image1, cvPoint(1, temp_image1->height - (MeanX[1][Y_POS]+100)), cvPoint(718, temp_image1->height - (MeanX[1][Y_POS]*16+100)), CV_RGB(255, 0, 0), 1, 8);
	//		cvLine(temp_image, cvPoint(1, temp_image1->height - (MeanX[0][Y_POS])), cvPoint(718, temp_image->height - (MeanX[0][Y_POS])), CV_RGB(255, 0, 0), 1, 8);
	//		cvLine(temp_image2, cvPoint(1,temp_image1->height - (MeanX[2][Y_POS]+100)), cvPoint(718, temp_image2->height - (MeanX[2][Y_POS]*16+100)), CV_RGB(255, 0, 0), 1, 8);
	//
	//		cvShowImage("SCAN LINE 3", temp_image);
	//		cvShowImage("SCAN LINE 4", temp_image1);
	//		cvShowImage("SCAN LINE 5", temp_image2);
	//
	//		cvReleaseImage(&temp_image2);
	//		cvReleaseImage(&temp_image1);
	//		cvReleaseImage(&temp_image);
	//	}
	/////////////////////////  밝기 변화 형태로 변환 Y 방향////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Mg = 3; N = 2; MatchingFilter = 10; Cnt = 0; OvSet = 0;
	Mgin = 5; //오버레이 근처 영역 제거 마진
	ModifyEnd = 0;

	LogWriteToFile(_T("밝기 변환 형태로 변환 Y 방향"));
	double SumY[3][CAM_IMAGE_WIDTH] = { 0, }, stdSumY[3][CAM_IMAGE_WIDTH] = { 0, }, MeanY[3][CAM_IMAGE_WIDTH] = { 0, }, stdY[3][CAM_IMAGE_WIDTH] = { 0, };


	for (int x = 0; x < CAM_IMAGE_WIDTH; x++){
		for (int y = N + 10; y<CAM_IMAGE_HEIGHT - N - 10; y++){
			if (y < 0)
				break;

			Data1[y][x] = 0;
			for (i = 0; (i<N); i++){
				if ((y - N + i<0) || (y - N + i>CAM_IMAGE_WIDTH - 1) || (y + i<0) || (y + i>CAM_IMAGE_WIDTH - 1))break;
				Data1[y][x] += (float)((Data[y + i][x]) - (Data[y - N + i][x]));  //khk
			}
			Data1[y][x] /= (float)N;
		}

		if (MatchingFilter < 0)
			break;

		for (int y = MatchingFilter; y<CAM_IMAGE_HEIGHT - MatchingFilter; y++)
		{
			Data2[y][x] = 0;
			for (i = 0; i<MatchingFilter; i++){
				if ((y + i<0) || (y + i>CAM_IMAGE_WIDTH - 1) || (y - MatchingFilter + i<0) || (y - MatchingFilter + i>CAM_IMAGE_WIDTH - 1))break;
				Data2[y][x] += (float)((Data1[y + i][x]) - (Data1[y - MatchingFilter + i][x]));  //khk
			}
			Data2[y][x] /= MatchingFilter;
		}
		ModifyEnd = 0;
	}
	//	///////////////////////////////각 라인별 평균값 구하기////////////////////////////////////////////////////////////////
	LogWriteToFile(_T("각 라인별 평균값 구하기"));
	Cnt = 0;
	for (int x = 0; x<CAM_IMAGE_WIDTH; x++){
		if (x == CAM_IMAGE_WIDTH / 2)
			x = x;
		SumY[0][x] = 0; SumY[1][x] = 0; SumY[2][x] = 0;
		for (int y = 40; y<CAM_IMAGE_HEIGHT - 40; y++){
			SumY[0][x] += Data[y][x];
			stdSumY[0][x] += Data[y][x] * Data[y][x];
			SumY[1][x] += Data1[y][x];
			stdSumY[1][x] += Data1[y][x] * Data1[y][x];
			SumY[2][x] += Data2[y][x];
			stdSumY[2][x] += Data2[y][x] * Data2[y][x];
			Cnt++;
		}
		for (i = 0; i<3; i++){
			MeanY[i][x] = SumY[i][x] / (double)Cnt;
			stdY[i][x] = stdSumY[i][x] / (double)Cnt - MeanY[i][x] * MeanY[i][x];
		}
		Cnt = 0;
	}

	///////////////////////////////각 라인별 멍이물 특징점 구하기////////////////////////////////////////////////////////////////
	CntNegative = 0; CntPositive = 0; EvaluationStart = 0; NegativeStart = 0;
	MeanData = 0;  SumTemp = 0;

	Sleep(50);
	LogWriteToFile(_T("각 라인별 멍이물 특징점 구하기"));
	// 	for(int x=1; x<720; x++){
	// 		CntNegative=0;
	// 
	// 		for(int y=0+20;y<480-20;y++){
	// 			if(count >= 2000)
	// 			{
	// 				LogWriteToFile(_T("Count 2000 초과"));
	// 				AfxMessageBox(_T("Count 2000 초과"));
	// 			}else if((double)Data2[y][x]-MeanY[2][x]<0)
	// 			{
	// 				if(EvaluationStart==1)
	// 				{  
	// 					EvaluationStart=2;
	// 					if(CntPositive>=10)
	// 					{
	// 						// 이물 검출 알고리즘
	// 						EvaluationStart=0;
	// 						SumTemp=0; SumPoint[count]=0;
	// 						SumTemp2=0; SumPoint2[count]=0;
	// 						for(i=0;i<CntNegative;i++)
	// 						{
	// 							if((y-CntPositive-1-i>0)&&(y-CntPositive-1-i<479))
	// 							{
	// 								SumTemp += (Data2[y-CntPositive-1-i][x]-MeanY[2][x]);
	// 							}
	// 						}
	// 
	// 						SumPoint[count]=SumTemp; //   /(double)CntNegative;
	// 
	// 						SumTemp=0; 
	// 						for(i=0;i<CntNegative;i++)
	// 						{
	// 							if((y-CntPositive+i>0)&&(y-CntPositive+i<479))
	// 							{
	// 								SumTemp += (Data2[y-CntPositive+i][x]-MeanY[2][x]);
	// 							}
	// 						}
	// 
	// 						SumPoint[count]=SumTemp-SumPoint[count];
	// 
	// 
	// 						for(i=0;i<CntNegative;i++)
	// 						{
	// 							if((y-CntPositive-1-i>0)&&(y-CntPositive-1-i<479))
	// 							{
	// 								SumTemp2 += (Data2[y-CntPositive-1-i][x-1]-MeanY[2][x-1]);
	// 							}
	// 						}
	// 
	// 						SumPoint2[count]=SumTemp2;
	// 
	// 						SumTemp2=0; 
	// 
	// 						for(i=0;i<CntNegative;i++)
	// 						{
	// 							if((y-CntPositive+i>0)&&(y-CntPositive+i<479))
	// 							{
	// 								SumTemp2 += (Data2[y-CntPositive+i][x-1]-MeanY[2][x-1]);
	// 							}
	// 						}
	// 
	// 						SumPoint2[count]=SumTemp2-SumPoint2[count];
	// 						if(x==17&&y==190)
	// 							x=x;
	// 
	// 						if((m_Area[x][y]==1)&&(SumPoint[count]>ThrPattern)/*&&(SumPoint2[count]>ThrPattern)*/&&((((x>12)&&(x<=70)))||((x>=650)&&(x<708))))	//이물이 맞으면
	// 						{
	// 							if(y-CntPositive-10>0)
	// 							{
	// 								DeadPixelLowData[count][0]=x;	
	// 								DeadPixelLowData[count][1]=y-CntPositive-10;
	// 
	// 								if(count<1000)count++;
	// 							}
	// 						}
	// 
	// 						if(count >= 2000)
	// 						{
	// 							LogWriteToFile(_T("Count 2000 초과"));
	// 							AfxMessageBox(_T("Count 2000 초과"));
	// 						}
	// 
	// 						if((m_Area[x][y]==2)&&(SumPoint[count]>ThrPattern_B)/*&&(SumPoint2[count]>ThrPattern_B)*/&&((((x>12)&&(x<=70)))||((x>=650)&&(x<708))))	//이물이 맞으면
	// 						{
	// 							if(y-CntPositive-10>0)
	// 							{
	// 								DeadPixelLowData[count][0]=x;	
	// 								DeadPixelLowData[count][1]=y-CntPositive-10;
	// 								if(count<1000)count++;
	// 							}
	// 						}
	// 
	// 						if(count >= 2000)
	// 						{
	// 							LogWriteToFile(_T("Count 2000 초과"));
	// 							AfxMessageBox(_T("Count 2000 초과"));
	// 						}
	// 						if((m_Area[x][y]==3)&&(SumPoint[count]>ThrPattern_C)/*&&(SumPoint2[count]>ThrPattern_C)*/&&((((x>12)&&(x<=70)))||((x>=650)&&(x<708))))	//이물이 맞으면
	// 						{
	// 							if(y-CntPositive-10>0)
	// 							{
	// 								DeadPixelLowData[count][0]=x;	
	// 								DeadPixelLowData[count][1]=y-CntPositive-10;
	// 
	// 								if(count<1000)count++;
	// 							}
	// 						}
	// 
	// 						if(count >= 2000)
	// 						{
	// 							LogWriteToFile(_T("Count 2000 초과"));
	// 							AfxMessageBox(_T("Count 2000 초과"));
	// 						}
	// 					}
	// 
	// 					CntPositive=0; CntNegative=0;//초기화
	// 				}
	// 				else if(CntPositive != 0) CntPositive=0;
	// 
	// 				CntNegative ++;
	// 
	// 			}
	// 			else {
	// 				if(EvaluationStart == 2 )
	// 				{
	// 					EvaluationStart = 0 ;
	// 					CntNegative=0;
	// 				}
	// 
	// 				if((CntNegative>=10)&&(EvaluationStart == 0)){
	// 					EvaluationStart = 1;
	// 				}
	// 				CntPositive++;
	// 			}
	// 		}
	// 	}

	LogWriteToFile(_T("한줄단위 그래프 그리기"));

	//if(((CFINAL_TEST_MDlg *)m_pMomWnd)->b_SecretOption ==1){
	//	cvShowImage("Region",Image);  // 이 윈도우에 내가 만든 ipl_Img 이미지를 보이고
	//	cvWaitKey(0);                           // 키입력을 받은후에
	//	cvDestroyWindow("Region");        // 윈도우를 종료
	//}
	//cvReleaseImage(&Image);

	int ExitLoop = 0;
	int SkipCnt = 0;
	ParticleCnt = 0;

	if (count >= 1){
		double Dist1, MinDist, SumX, SumY;
		double DistMoveCenter[2];
		long IndexMin;
		int MoveCheck = 0, ExitGrouping = 0;
		double DistMoveCenterX = 0, DistMoveCenterY = 0;
		double NewCenterX, NewCenterY;
		int IndexAdd = 0;

		double SplitFactor = 10;
		int CheckExit = 0;

		Group = 2;
		DeadPixel[0][0][0] = DeadPixelLowData[0][0]; DeadPixel[0][0][1] = DeadPixelLowData[0][1];  // 2개 Group의 초기값 설정
		CenterPoint[0][0] = (double)DeadPixelLowData[0][0]; CenterPoint[0][1] = (double)DeadPixelLowData[0][1];  // 2개 Group의 초기값 설정

		DeadPixel[1][0][0] = DeadPixelLowData[1][0]; DeadPixel[1][0][1] = DeadPixelLowData[1][1];  // 2개 Group의 초기값 설정
		CenterPoint[1][0] = (double)DeadPixelLowData[1][0]; CenterPoint[1][1] = (double)DeadPixelLowData[1][1];  // 2개 Group의 초기값 설정

		LogWriteToFile(_T("가장 가까운 Group 찾기"));
		for (int Loop = 0; Loop<500 && ExitLoop == 0; Loop++){
			ExitGrouping = 0;
			for (i = 0; i<1000 && ExitGrouping == 0; i++){
				for (k = 0; k<50; k++)IndexG[k] = 0;
				for (j = 0; j<count; j++){
					if (DeadPixelLowData[j][0]>CAM_IMAGE_WIDTH || DeadPixelLowData[j][0] < 0 || DeadPixelLowData[j][1] >CAM_IMAGE_HEIGHT || DeadPixelLowData[j][1] < 0) continue;

					////////////////////////////// 가장 가까운 Group 찾기///////////////////////
					MinDist = 999999; IndexMin = 0; //초기화
					for (k = 0; k<Group; k++){
						Dist1 = Distance(CenterPoint[k][0], CenterPoint[k][1], DeadPixelLowData[j][0], DeadPixelLowData[j][1]);
						if (Dist1<MinDist){
							IndexMin = k;
							MinDist = Dist1;
						}
					}
					if (Group>48)
						Group = Group;
					DeadPixel[IndexMin][IndexG[IndexMin]][0] = DeadPixelLowData[j][0];
					DeadPixel[IndexMin][IndexG[IndexMin]][1] = DeadPixelLowData[j][1];
					DeadPixel[IndexMin][IndexG[IndexMin]][2] = j;
					IndexG[IndexMin]++;
				}
				//////////////////////////////  각 Group의 중심점 구하기 ///////////////////////

				ExitGrouping = 1;

				for (k = 0; k<Group; k++){
					if (IndexG[k] == 0) {
						if (k<Group - 1){
							for (int m = 0; m<Group - 1; m++){

								IndexG[k + m] = IndexG[k + m + 1];
								for (int p = 0; p<4; p++)CenterPoint[k + m][p] = CenterPoint[k + m + 1][p];

								for (int n = 0; n<IndexG[k + m]; n++){
									DeadPixel[k + m][n][0] = DeadPixel[k + m + 1][n][0];
									DeadPixel[k + m][n][1] = DeadPixel[k + m + 1][n][1];
								}
							}
						}
						Group--;
						SplitFactor++;
						CheckExit++;
						continue;
					}

					SumX = 0; SumY = 0;

					for (j = 0; j<IndexG[k]; j++){
						SumX += (double)DeadPixel[k][j][0];
						SumY += (double)DeadPixel[k][j][1];
					}

					NewCenterX = ((double)SumX / (double)IndexG[k]);
					NewCenterY = ((double)SumY / (double)IndexG[k]);

					DistMoveCenterX = fabs(NewCenterX - (double)CenterPoint[k][0]);
					DistMoveCenterY = fabs(NewCenterY - (double)CenterPoint[k][1]);

					if (DistMoveCenterX>0.1 || DistMoveCenterY > 0.1){
						ExitGrouping = 0;
						CenterPoint[k][0] = NewCenterX;
						CenterPoint[k][1] = NewCenterY;
					}

				}
			} //  할당된 Group개수 만큼 분류 완료

			//////////////////////////////  Group 분리 및 종료 Check ///////////////////////
			IndexAdd = 0;
			int Split = 0;
			double SumDarkness = 0;

			for (k = 0; k<Group&&Split == 0 && CheckExit<2; k++){
				SumX = 0; SumY = 0; SumDarkness = 0;
				for (j = 0; j<IndexG[k]; j++){

					SumX += (double)DeadPixel[k][j][0] * (double)DeadPixel[k][j][0];
					SumY += (double)DeadPixel[k][j][1] * (double)DeadPixel[k][j][1];
					SumDarkness += SumPoint[DeadPixel[k][j][2]];	// Group 별 Threshold 값 계산////////////////////////////////////////////////////


				}
				SumX = sqrt(SumX / (double)IndexG[k] - CenterPoint[k][0] * CenterPoint[k][0]); CenterPoint[k][2] = SumX;
				SumY = sqrt(SumY / (double)IndexG[k] - CenterPoint[k][1] * CenterPoint[k][1]); CenterPoint[k][3] = SumY;
				SumDarkness /= (double)IndexG[k];
				//P_RECT[k].m_Concentration = SumPoint[k];
				if (sqrt(SumX*SumX + SumY*SumY)> (double)30){ // Group의 산포가 너무 크면 2개로 분할

					if (Group<49 && Split == 0){
						IndexAdd++;

						CenterPoint[k][0] = DeadPixel[k][0][0];
						CenterPoint[k][1] = DeadPixel[k][0][1];

						CenterPoint[Group][0] = DeadPixel[k][1][0];
						CenterPoint[Group][1] = DeadPixel[k][1][1];

					}
					Split = 1;
				}
			}
			Group += IndexAdd; // Group 확장
			SkipCnt = 0;
			int DustNumOfSector = m_DustNumOfSector; // 작은 멍은 제거할수 있도록 설정
			int DustNumOfSector_B = m_DustNumOfSector_B;
			//int DustNumOfSector_C = m_DustNumOfSector_C;
			int x1, y1;

			if (IndexAdd == 0){      //  Grouping 종료 설정 및 이물 영역 출력 설정 
				ParticleCnt = Group;

				for (k = 0; k<Group; k++){
					x1 = (int)(CenterPoint[k][0]);			y1 = (int)(CenterPoint[k][1]);

					//if(((m_Area[x1][y1]==1)&&(IndexG[k]<=DustNumOfSector))||((m_Area[x1][y1]==2)&&(IndexG[k]<=DustNumOfSector_B))
					//	/*||  ((m_Area[x1][y1]==3)&&(IndexG[k]<=DustNumOfSector_C))*/ ) {//Option 처리
					//	P_RECT[k-SkipCnt].m_PosX=0;P_RECT[k-SkipCnt].m_PosY=0;
					//	P_RECT[k-SkipCnt].EX_RECT_SET(P_RECT[k-SkipCnt].m_PosX,P_RECT[k-SkipCnt].m_PosY,0,0);//

					//	ParticleCnt--;
					//	SkipCnt++;

					//	continue;
					//}

					//P_RECT[k-SkipCnt].m_PosX=CenterPoint[k][0];
					//P_RECT[k-SkipCnt].m_PosY=CenterPoint[k][1];
					//P_RECT[k-SkipCnt].EX_RECT_SET(P_RECT[k-SkipCnt].m_PosX,P_RECT[k-SkipCnt].m_PosY,(unsigned int)Cell+CenterPoint[k][2]*1.5,(unsigned int)Cell+CenterPoint[k][3]*1.5);//

					//P_RECT[k-SkipCnt].m_Concentration = /*SumPoint[k-SkipCnt]*/;
				}
				ExitLoop = 1;
			}
			ParticleCnt += BigCnt;
			CString str = "";
			str.Empty();
			str.Format("%d", ParticleCnt);
			/*m_ParticleList.SetItemText(InsertIndex,5,str);
			((CFINAL_TEST_MDlg *)m_pMomWnd)->PQMS_TESTRESULT[9][0] = str;
			((CFINAL_TEST_MDlg *)m_pMomWnd)->m_pResParticleOptWnd->PARTICLENUM_TEXT(str);*/

		}

		ParticleCnt = counter;
		if (Group>50) { Group = 50; BigCnt = 0; ParticleCnt = 50; }
		if (Group + BigCnt>50) { BigCnt = 50 - Group; ParticleCnt = 50; }

		for (k = Group; k<Group + BigCnt; k++){
			P_RECT[k - SkipCnt].m_PosX = BigDustPosition[k - Group][0]; P_RECT[k - SkipCnt].m_PosY = BigDustPosition[k - Group][1];
			P_RECT[k - SkipCnt].EX_RECT_SET(P_RECT[k - SkipCnt].m_PosX, P_RECT[k - SkipCnt].m_PosY, BigDustPosition[k - Group][2], BigDustPosition[k - Group][2]);//
		}
	}

	///신규 농도 추가 1005
	Group = ParticleCnt;

	int Mid_x = 0;
	int Mid_y = 0;

	for (int lop = 0; lop < ParticleCnt; lop++)
	{
		int centerSum = 0, centerCount = 0;	double center;
		int side1_Sum = 0, side1_Count = 0;	double side;
		int side2_Sum = 0, side2_Count = 0;
		//Mid_x = P_RECT[lop].m_Left + m_pstParticleData->ErrRegionList[lop].RegionList.right) / 2;
		//Mid_y = (m_pstParticleData->ErrRegionList[lop].RegionList.top + m_pstParticleData->ErrRegionList[lop].RegionList.bottom) / 2;
		////for (int y = Mid_y - 1; y <= Mid_y + 1; y++)
		////{
		////	for (int x = Mid_x - 1; x <= Mid_x + 1; x++)
		////	{
		////		if (y > 0 && y < 480 && x > 0 && x < 720)
		////		{
		////			centerSum += Data[y][x];
		////			centerCount++;
		////		}
		////	}
		////}

		int size_x = P_RECT[lop].m_Right - P_RECT[lop].m_Left;
		int size_y = P_RECT[lop].m_Bottom - P_RECT[lop].m_Top;

		for (int y = P_RECT[lop].m_Top - size_y; y <= P_RECT[lop].m_Bottom + size_y; y++)
		{
			for (int x = P_RECT[lop].m_Left - size_x; x <= P_RECT[lop].m_Right + size_x; x++)
			{
				if (y > 0 && y < CAM_IMAGE_HEIGHT && x > 0 && x < CAM_IMAGE_WIDTH)
				{
					side1_Sum += Data[y][x];
					side1_Count++;
				}

			}
		}

		for (int y = P_RECT[lop].m_Top; y <= P_RECT[lop].m_Bottom; y++)
		{
			for (int x = P_RECT[lop].m_Left; x <= P_RECT[lop].m_Right; x++)
			{
				if (y > 0 && y < CAM_IMAGE_HEIGHT && x > 0 && x < CAM_IMAGE_WIDTH)
				{
					side2_Sum += Data[y][x];
					side2_Count++;
				}
			}
		}

		for (int y = P_RECT[lop].m_Top; y <= P_RECT[lop].m_Bottom; y++)
		{
			for (int x = P_RECT[lop].m_Left; x <= P_RECT[lop].m_Right; x++)
			{
				if (y > 0 && y < CAM_IMAGE_HEIGHT && x > 0 && x < CAM_IMAGE_WIDTH)
				{
					centerSum += Data[y][x];
					centerCount++;
				}
			}
		}

		center = (double)centerSum / (double)centerCount;
		side = (double)(side1_Sum - side2_Sum) / (double)(side1_Count - side2_Count);

		//m_pstParticleData->ErrRegionList[lop].stain_Blemish = (center / AVG_Y) * 100;
		P_RECT[lop].m_Concentration = 100 - (center / side) * 100;

		if (P_RECT[lop].m_Concentration > 100)
			P_RECT[lop].m_Concentration = 100;
		else if (P_RECT[lop].m_Concentration < 0)
			P_RECT[lop].m_Concentration = 0;



		/*if (center < AVG_Y)
		m_pstParticleData->ErrRegionList[lop].stain_Blemish = (center / AVG_Y) * 100;
		else
		m_pstParticleData->ErrRegionList[lop].stain_Blemish = (AVG_Y / center) * 100;*/


	}


	for (int lop = 0; lop < Group; lop++)
	{
		for (int lop3 = 0; lop3 < ParticleCnt; lop3++)
		{
			int Center_X = (P_RECT[lop3].m_Left + P_RECT[lop3].m_Right) / 2;
			int Center_Y = (P_RECT[lop3].m_Top + P_RECT[lop3].m_Bottom) / 2;;
			//// 중심 -> A
			if (P_RECT[lop3].m_Concentration <= m_stainBlemish[2] && m_Area[Center_X][Center_Y] == 1 && P_RECT[lop3].m_Concentration >= 0)
			{
				for (int lop2 = lop3; lop2 < ParticleCnt; lop2++)
				{


					//m_pstParticleData->ErrRegionList[lop2].RegionList.CenterPoint().x = m_pstParticleData->ErrRegionList[lop2 +1 ].RegionList.CenterPoint().x;
					//m_pstParticleData->ErrRegionList[lop2].RegionList.CenterPoint().y = m_pstParticleData->ErrRegionList[lop2 + 1].RegionList.CenterPoint().y;

					P_RECT[lop2].m_Top = P_RECT[lop2 + 1].m_Top;
					P_RECT[lop2].m_Bottom = P_RECT[lop2 + 1].m_Bottom;
					P_RECT[lop2].m_Left = P_RECT[lop2 + 1].m_Left;
					P_RECT[lop2].m_Right = P_RECT[lop2 + 1].m_Right;


					//	P_RECT[lop2].Y_result = P_RECT[lop2 + 1].Y_result;
					P_RECT[lop2].m_Concentration = P_RECT[lop2 + 1].m_Concentration;
				}

				ParticleCnt--;
				break;
			}

			//// 중간 -> B
			if (P_RECT[lop3].m_Concentration <= m_stainBlemish[1] && m_Area[Center_X][Center_Y] == 2 && P_RECT[lop3].m_Concentration >= 0)
			{
				for (int lop2 = lop3; lop2 < ParticleCnt; lop2++)
				{


					//m_pstParticleData->ErrRegionList[lop2].RegionList.CenterPoint().x = m_pstParticleData->ErrRegionList[lop2 +1 ].RegionList.CenterPoint().x;
					//m_pstParticleData->ErrRegionList[lop2].RegionList.CenterPoint().y = m_pstParticleData->ErrRegionList[lop2 + 1].RegionList.CenterPoint().y;

					P_RECT[lop2].m_Top = P_RECT[lop2 + 1].m_Top;
					P_RECT[lop2].m_Bottom = P_RECT[lop2 + 1].m_Bottom;
					P_RECT[lop2].m_Left = P_RECT[lop2 + 1].m_Left;
					P_RECT[lop2].m_Right = P_RECT[lop2 + 1].m_Right;


					//	P_RECT[lop2].Y_result = P_RECT[lop2 + 1].Y_result;
					P_RECT[lop2].m_Concentration = P_RECT[lop2 + 1].m_Concentration;
				}

				ParticleCnt--;
				break;
			}
			//// 바깥 -> C
			if (P_RECT[lop3].m_Concentration <= m_stainBlemish[0] && m_Area[Center_X][Center_Y] == 3 && P_RECT[lop3].m_Concentration >= 0)
			{
				for (int lop2 = lop3; lop2 < ParticleCnt; lop2++)
				{


					//m_pstParticleData->ErrRegionList[lop2].RegionList.CenterPoint().x = m_pstParticleData->ErrRegionList[lop2 +1 ].RegionList.CenterPoint().x;
					//m_pstParticleData->ErrRegionList[lop2].RegionList.CenterPoint().y = m_pstParticleData->ErrRegionList[lop2 + 1].RegionList.CenterPoint().y;

					P_RECT[lop2].m_Top = P_RECT[lop2 + 1].m_Top;
					P_RECT[lop2].m_Bottom = P_RECT[lop2 + 1].m_Bottom;
					P_RECT[lop2].m_Left = P_RECT[lop2 + 1].m_Left;
					P_RECT[lop2].m_Right = P_RECT[lop2 + 1].m_Right;


					//	P_RECT[lop2].Y_result = P_RECT[lop2 + 1].Y_result;
					P_RECT[lop2].m_Concentration = P_RECT[lop2 + 1].m_Concentration;
				}

				ParticleCnt--;
				break;
			}
		}

	}

	for (int i = 0; i < ParticleCnt; i++)
	{
		if (P_RECT[i].m_Concentration > 100)
			P_RECT[i].m_Concentration = 100;
		else if (P_RECT[i].m_Concentration < 0)
			P_RECT[i].m_Concentration = 0;
	}




	if (ParticleCnt > m_PASSNUM){
		//	((CFINAL_TEST_MDlg *)m_pMomWnd)->m_pResParticleOptWnd->RESULT_TEXT(2,"FAIL");
		R_RECT[0].m_Success = FALSE;
	}
	else{
		//	((CFINAL_TEST_MDlg *)m_pMomWnd)->m_pResParticleOptWnd->RESULT_TEXT(1,"PASS");
		R_RECT[0].m_Success = TRUE;
	}
	//
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++){
		if (i == 76)
			i = i;
		free(Data1[i]);
	}
	free(Data1);

	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)free(Data[i]);	free(Data);

	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)free(Data2[i]);	free(Data2);
	//
	//	((CFINAL_TEST_MDlg *)m_pMomWnd)->Wait2(1000);
	return R_RECT[0].m_Success;
}
bool CParticle_Option::ParticlePic(CDC *cdc,int NUM)
{
	CPen	my_Pan,*old_pan;
	CString TEXTDATA ="";
	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	if (b_FailCheck == TRUE)
	{
		if(R_RECT[0].m_Success == FALSE){
			my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
			old_pan = cdc->SelectObject(&my_Pan);
			cdc->SetTextColor(WHITE_COLOR);

			cdc->MoveTo(P_RECT[NUM].m_Left,P_RECT[NUM].m_Top);
			cdc->LineTo(P_RECT[NUM].m_Right,P_RECT[NUM].m_Top);
			cdc->LineTo(P_RECT[NUM].m_Right,P_RECT[NUM].m_Bottom);
			cdc->LineTo(P_RECT[NUM].m_Left,P_RECT[NUM].m_Bottom);
			cdc->LineTo(P_RECT[NUM].m_Left,P_RECT[NUM].m_Top);

			int temp_left = P_RECT[NUM].m_Left;

			if(temp_left > 640)
				temp_left = 600;

			TEXTDATA.Format("B : %.1f", P_RECT[NUM].m_Concentration);
			if(P_RECT[NUM].m_Top - 40 <= 0)
				cdc->TextOut(temp_left, P_RECT[NUM].m_Bottom+20, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());
			else
				cdc->TextOut(temp_left, P_RECT[NUM].m_Top-40, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());

			TEXTDATA.Format("S : %dx%d", 
				P_RECT[NUM].m_Right - P_RECT[NUM].m_Left, P_RECT[NUM].m_Bottom - P_RECT[NUM].m_Top);
			if(P_RECT[NUM].m_Top - 40 <= 0)
				cdc->TextOut(temp_left, P_RECT[NUM].m_Bottom+40, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());
			else
				cdc->TextOut(temp_left, P_RECT[NUM].m_Top-20, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());

			cdc->SelectObject(old_pan);
			old_pan->DeleteObject();
			my_Pan.DeleteObject();
		}
	}
	return 1;
}

bool CParticle_Option::ParticlePic_Lump(CDC *cdc,int NUM)
{
	CPen	my_Pan,*old_pan;
	CString TEXTDATA ="";
	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	if (b_FailCheck == TRUE)
	{
		if(R_RECT[0].m_Success == FALSE){
			my_Pan.CreatePen(PS_SOLID,2,YELLOW_COLOR);
			old_pan = cdc->SelectObject(&my_Pan);
			cdc->SetTextColor(WHITE_COLOR);

			cdc->MoveTo(rectArray[NUM].x,rectArray[NUM].y);
			cdc->LineTo(rectArray[NUM].x + rectArray[NUM].width,rectArray[NUM].y);
			cdc->LineTo(rectArray[NUM].x + rectArray[NUM].width,rectArray[NUM].y+rectArray[NUM].height);
			cdc->LineTo(rectArray[NUM].x,rectArray[NUM].y+rectArray[NUM].height);
			cdc->LineTo(rectArray[NUM].x,rectArray[NUM].y);

			cdc->SelectObject(old_pan);
			old_pan->DeleteObject();
			my_Pan.DeleteObject();
		}
	}
	return 1;
}

bool CParticle_Option::ParticlePic_New(CDC *cdc,int NUM){

	//if(EIJARect.Chkdata() == FALSE){return 0;}//재대로 된 데이터가 아니면 바로 빠져나간다.

	CPen	my_Pan,*old_pan;
	CString TEXTDATA ="";
	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	
	if(NUM ==0){
		my_Pan.CreatePen(PS_SOLID,2,WHITE_COLOR);
	}else if(NUM ==1){
		my_Pan.CreatePen(PS_SOLID,2,GREEN_COLOR);
	}else{
		my_Pan.CreatePen(PS_SOLID,2,YELLOW_COLOR);
	}
	
	if(m_b_Ellipse[NUM] == TRUE){
		old_pan = cdc->SelectObject(&my_Pan);

		CBrush oldBrush;
		oldBrush.CreateStockObject(NULL_BRUSH);
		CBrush *poldBrush = cdc->SelectObject(&oldBrush);
		cdc->Ellipse(Test_RECT[NUM].m_Left,
			Test_RECT[NUM].m_Top,
			Test_RECT[NUM].m_Right,
			Test_RECT[NUM].m_Bottom);
		
		cdc->SelectObject(old_pan);
		cdc->SelectObject(oldBrush);

		old_pan->DeleteObject();
		poldBrush->DeleteObject();
		my_Pan.DeleteObject();
		oldBrush.DeleteObject();
	}
	else{
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->MoveTo(Test_RECT[NUM].m_Left,Test_RECT[NUM].m_Top);
		cdc->LineTo(Test_RECT[NUM].m_Right,Test_RECT[NUM].m_Top);
		cdc->LineTo(Test_RECT[NUM].m_Right,Test_RECT[NUM].m_Bottom);
		cdc->LineTo(Test_RECT[NUM].m_Left,Test_RECT[NUM].m_Bottom);
		cdc->LineTo(Test_RECT[NUM].m_Left,Test_RECT[NUM].m_Top);

		old_pan->DeleteObject();
		my_Pan.DeleteObject();
	}
		

	return 1;
}

void CParticle_Option::Pic(CDC *cdc)
{
	if(ParticleCnt==0){
		ParticlePic(cdc,0);
	}else if(ParticleCnt > 255){
		ParticleCnt = 255;
	}
	
	if(particle_start == TRUE)
	{
		for(int lop=0;lop<ParticleCnt;lop++){
			ParticlePic(cdc,lop);
		}

		/*for(int i = 0; i < counter ; i++){
			ParticlePic_Lump(cdc,i);
		}*/
	}
	
	for(int t=0;t<3; t++){
		ParticlePic_New(cdc,t);
	}
}
void CParticle_Option::OnBnClickedButtonSaveOp()
{

	CString str;
	m_ImageSaveMode = m_Combo_ImageSave.GetCurSel();

	str.Empty();
	str.Format("%d",m_ImageSaveMode);
	WritePrivateProfileString("PARTICLEOPT","ImageSaveMode",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);	
}

void CParticle_Option::OnBnClickedButtonPathopen()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	ITEMIDLIST        *pidlBrowse;
	char    SaveFolderPath[2046] = {0,}; 
	BROWSEINFO BrInfo;
	BrInfo.hwndOwner = NULL; 
	BrInfo.pidlRoot = NULL;
	memset( &BrInfo, 0, sizeof(BrInfo) );
	BrInfo.pszDisplayName = SaveFolderPath;
	BrInfo.lpszTitle = "저장할 디렉토리를 선택하세요";
	BrInfo.ulFlags = BIF_RETURNONLYFSDIRS;

	// Show Dialog
	pidlBrowse = ::SHBrowseForFolder(&BrInfo);    

	if( pidlBrowse != NULL)
	{
		//   Get Path
		UpdateData(TRUE);
		::SHGetPathFromIDList(pidlBrowse, SaveFolderPath);   
		str_ImageSavePath = SaveFolderPath;
	
		UpdateData(FALSE);
		WritePrivateProfileString("PARTICLEOPT","ImageSavePath",str_ImageSavePath,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);	
	}
}

void CParticle_Option::Image_Save(UINT MODE, BOOL Result){

	if (P_Image_NoSave == MODE)
	{
		return;
	}

	((CImageTesterDlg *)m_pMomWnd)->b_Particle_Result = Result;

	CString str_Failpath;
	str_Failpath.Format("%s\\%s",str_ImageSavePath,g_szPath[1]);

	folder_gen(str_Failpath);

	CString str_Passpath;
	str_Passpath.Format("%s\\%s",str_ImageSavePath,g_szPath[0]);
	folder_gen(str_Passpath);

	CString str,str_ori;
	CString str_Pass_Daypath;
	CString str_Fail_Daypath;

	CTime thetime = CTime::GetCurrentTime();
	int y=thetime.GetYear(); int m=thetime.GetMonth(); int d=thetime.GetDay();
	str.Format("%04d%02d%02d",y,m,d);

	str_Fail_Daypath =str_Failpath  +(("\\")+str);
	str_Pass_Daypath = str_Passpath + (("\\")+str);

	folder_gen(str_Fail_Daypath);
	folder_gen(str_Pass_Daypath);
	int hour=thetime.GetHour(); int minute=thetime.GetMinute(); int second=thetime.GetSecond();

	if (((CImageTesterDlg *)m_pMomWnd)->b_UserMode == TRUE)
	{
		str.Empty();
		str.Format("%02dh_%02dm_%02ds.bmp",hour,minute,second);
		str_ori.Format("%02dh_%02dm_%02ds_Origin.bmp",hour,minute,second);
	}else{
		CString lotid = ((CImageTesterDlg *)m_pMomWnd)->m_strLotIdFull;

		if (lotid == "")
		{
			lotid.Format("NoLotId_%02dh_%02dm_%02ds",hour,minute,second);
		}
		str.Empty();
		str.Format("%s_%02dh_%02dm_%02ds.bmp",lotid,hour,minute,second);
		str_ori.Format("%s_%02dh_%02dm_%02ds_Origin.bmp",lotid,hour,minute,second);
	}



	CString str_FailFullPath,str_PassFullPath;

	if (MODE != P_Image_NoSave)
	{
		((CImageTesterDlg *)m_pMomWnd)->imagepic_Failcopy = str_Fail_Daypath +"\\"+ str;
		((CImageTesterDlg *)m_pMomWnd)->imagepic_Passcopy = str_Pass_Daypath +"\\"+ str;

		((CImageTesterDlg *)m_pMomWnd)->i_CapturePaticleMode = MODE;
		((CImageTesterDlg *)m_pMomWnd)->b_ParticleCaptureMode = TRUE;
		DoEvents(300);
		IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);
		BYTE R,G,B;
		double Sum_Y;

		for (int y = 0; y<CAM_IMAGE_HEIGHT; y++)
		{
			for (int x = 0; x<CAM_IMAGE_WIDTH; x++)
			{
				B = m_CaptureBuf[y*(CAM_IMAGE_WIDTH * 3) + x * 3];
				G = m_CaptureBuf[y*(CAM_IMAGE_WIDTH * 3) + x * 3 + 1];
				R = m_CaptureBuf[y*(CAM_IMAGE_WIDTH * 3) + x * 3 + 2];

				OriginImage->imageData[y*OriginImage->widthStep+x* 3 + 0] = B;
				OriginImage->imageData[y*OriginImage->widthStep+x* 3 + 1] = G;
				OriginImage->imageData[y*OriginImage->widthStep+x* 3 + 2] = R;
			}
		}
		if (Result == FALSE)
		{
			cvSaveImage(str_Fail_Daypath + "\\"+str_ori,OriginImage);
		}

		if(MODE == P_Image_ALLSave){
			if (Result == TRUE)
			{
				cvSaveImage(str_Pass_Daypath + "\\"+str_ori,OriginImage);
			}
		}
		cvReleaseImage(&OriginImage);

		DoEvents(300);
	}
}

//160922 KDY 추가 /////////////////

void CParticle_Option::OnBnClickedButtonCancle()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_ExpGain = GetPrivateProfileCString("PARTICLEOPT","EXP_GAIN",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(m_ExpGain.IsEmpty()){
		m_ExpGain = "10";
		WritePrivateProfileString("PARTICLEOPT","EXP_GAIN",m_ExpGain,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}

	str_ExpGain = m_ExpGain;

	UpdateData(FALSE);
}

void CParticle_Option::OnBnClickedButtonGainapply()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	CString str_buf = "";
	CString str_buf2 = "";
	CString str = "";
	int buf = 0;

	buf = _ttoi(str_ExpGain);

	if(buf < 0 || buf > 480)
	{
		AfxMessageBox("Gain조절 허용범위를 확인하세요.");
		return;
	}



	BOOL ret;

	ret = ((CImageTesterDlg *)m_pMomWnd)->Manual_Exp_Mode();

	if(ret)
	{
		((CImageTesterDlg *)m_pMomWnd)->Manual_Exp_Gain_Ctrl(str_ExpGain);
	}
	
}

void CParticle_Option::OnBnClickedButtonGainsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	CString str_buf = "";
	CString str_buf2 = "";
	CString str = "";
	int buf = 0;

	buf = _ttoi(str_ExpGain);

	if(buf < 0 || buf > 480)
	{
		AfxMessageBox("Gain조절 허용범위를 확인하세요.");
		return;
	}


	m_ExpGain = str_ExpGain;

	str_ExpGain = m_ExpGain;

	UpdateData(FALSE);


	WritePrivateProfileString("PARTICLEOPT","EXP_GAIN",str_ExpGain,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);	

	BOOL ret;

	ret = ((CImageTesterDlg *)m_pMomWnd)->Manual_Exp_Mode();

	if(ret)
	{
		((CImageTesterDlg *)m_pMomWnd)->Manual_Exp_Gain_Ctrl(m_ExpGain);
	}
}

//////////////////////추가끝////////////////////

void CParticle_Option::Lump_detection(LPBYTE IN_RGB){

	IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);
	IplImage *tempOriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);

	BYTE R,G,B;
	double Sum_Y;

	for (int y=0; y<CAM_IMAGE_HEIGHT; y++)
	{
		for (int x=0; x<CAM_IMAGE_WIDTH; x++)
		{
			B = IN_RGB[y*(CAM_IMAGE_WIDTH*3) + x*3    ];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH*3) + x*3 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH*3) + x*3 + 2];

			if(y == 0 || y == (CAM_IMAGE_HEIGHT -1) ){
				tempOriginImage->imageData[y * tempOriginImage->widthStep + x*3 ] = 0;
				tempOriginImage->imageData[y * tempOriginImage->widthStep + x*3 +1] =0;
				tempOriginImage->imageData[y * tempOriginImage->widthStep + x*3 +2] =0;
			}else{
				tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3] = B;
				tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3 + 1] = G;
				tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3 + 2] = R;
			}
		

			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));


			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;
		}
	}	
	cvSaveImage("D:\\test\\OriginImage.bmp", OriginImage);
	int Particle_Size_Side = m_Separation_V[0];
	int Particle_Size_Middle = m_Separation_V[1];
	int Particle_Size_Center = m_Separation_V[2];

	double Param1 =m_Black_STEP;
	cvAdaptiveThreshold(OriginImage,DilateImage,255,CV_ADAPTIVE_THRESH_MEAN_C,CV_THRESH_BINARY,71,Param1);
	cvSaveImage("D:\\test\\DilateImage.bmp", DilateImage);
	cvNot(DilateImage, DilateImage);
	cvErode(DilateImage,DilateImage);
	cvDilate(DilateImage, DilateImage);

	cvSaveImage("D:\\test\\DilateImage_Erode.bmp", DilateImage);



	cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	cvSaveImage("D:\\test\\DilateImage_bContours.bmp", DilateImage);
	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	

	temp_contour = contour;

	

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
		counter++;

	rectArray = new CvRect[counter];
	//	double *areaArray = new double[counter];

	double area=0, arcCount=0;
	counter = 0;
	double old_dist = 999999;

	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);		
		double circularity = (4.0*3.14*area)/(arcCount*arcCount);

		rect = cvContourBoundingRect(contour, 1);

		int center_x, center_y;
		center_x = rect.x + rect.width/2;
		center_y = rect.y + rect.height/2;
		cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(255, 0, 0), 1, 8);

		BYTE R, G, B;
		B = IN_RGB[center_y*(CAM_IMAGE_WIDTH * 3) + center_x * 3];
		G = IN_RGB[center_y*(CAM_IMAGE_WIDTH * 3) + center_x * 3 + 1];
		R = IN_RGB[center_y*(CAM_IMAGE_WIDTH * 3) + center_x * 3 + 2];


		BOOL Rate_Part = FALSE;
		double  Rate = 0;
		if (rect.width > rect.height){
			Rate = (double)rect.width / rect.height;
		}
		else{
			Rate = (double)rect.height / rect.width;
		}
		if (Rate >= 9){
			Rate_Part = TRUE;
		}

		//Size -> 

		if(rect.width < (CAM_IMAGE_WIDTH/4) && rect.height < (CAM_IMAGE_HEIGHT/4))
		{
			int Center_X = rect.x + rect.width/2 ;
			int Center_Y = rect.y + rect.height/2 ;

			////중심 = A
			if(rect.width > Particle_Size_Center && rect.height > Particle_Size_Center && m_Area[Center_X][Center_Y] == 1  && Rate_Part == FALSE)
			{				
				rectArray[counter] = rect;


				counter++;				

				double dist = GetDistance(center_x, center_y,  CAM_IMAGE_WIDTH/2, CAM_IMAGE_HEIGHT/2);							

				if(old_dist > dist)
				{					
					m_currPt.x = center_x - (CAM_IMAGE_WIDTH/2);
					m_currPt.y = center_y - (CAM_IMAGE_HEIGHT/2);

					m_currPt.x = m_currPt.x/2;
					m_currPt.y = m_currPt.y/2;

					old_dist = dist;
				}

				cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
				//cvRectangle(RGBResultImage, cvPoint(Center_X-1, Center_Y-1), cvPoint(Center_X+1, Center_Y+1), CV_RGB(0, 255, 0), 1, 8);  

			}

			//// 중간 = B
			if(rect.width > Particle_Size_Middle && rect.height > Particle_Size_Middle && m_Area[Center_X][Center_Y] == 2   && Rate_Part == FALSE)
			{				
				rectArray[counter] = rect;


				counter++;				

				double dist = GetDistance(center_x, center_y,  CAM_IMAGE_WIDTH/2, CAM_IMAGE_HEIGHT/2);							

				if(old_dist > dist)
				{					
					m_currPt.x = center_x - (CAM_IMAGE_WIDTH/2);
					m_currPt.y = center_y - (CAM_IMAGE_HEIGHT/2);

					m_currPt.x = m_currPt.x/2;
					m_currPt.y = m_currPt.y/2;

					old_dist = dist;
				}

				//cvRectangle(RGBResultImage, cvPoint(Center_X-1, Center_Y-1), cvPoint(Center_X+1, Center_Y+1), CV_RGB(0, 255, 0), 1, 8);  
				cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
			}

			//// 바깥 = C

			if(rect.width > Particle_Size_Side && rect.height > Particle_Size_Side && m_Area[Center_X][Center_Y] == 3 && Rate_Part == FALSE)
			{				
				rectArray[counter] = rect;


				counter++;				

				double dist = GetDistance(center_x, center_y,  CAM_IMAGE_WIDTH/2, CAM_IMAGE_HEIGHT/2);							

				if(old_dist > dist)
				{					
					m_currPt.x = center_x - (CAM_IMAGE_WIDTH/2);
					m_currPt.y = center_y - (CAM_IMAGE_HEIGHT/2);

					m_currPt.x = m_currPt.x/2;
					m_currPt.y = m_currPt.y/2;

					old_dist = dist;
				}

				//cvRectangle(RGBResultImage, cvPoint(Center_X-1, Center_Y-1), cvPoint(Center_X+1, Center_Y+1), CV_RGB(0, 255, 0), 1, 8);  
				cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
			}

		}
	}


	cvSaveImage("D:\\test\\RGBResultImage.bmp", RGBResultImage);
	cvSaveImage("D:\\test\\DilateImage2.bmp", DilateImage);
	//cvShowImage("Test_Lump",RGBResultImage);

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);	
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&tempOriginImage);





}
void CParticle_Option::Lump_detection_16bit(LPWORD IN_RGB){

	IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_16U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_16U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_16U, 3);
	IplImage *tempOriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_16U, 3);

	BYTE R, G, B;
	DWORD Sum_Y;
	memcpy(OriginImage->imageData, IN_RGB, CAM_IMAGE_WIDTH*CAM_IMAGE_HEIGHT * 2);
// 	for (int y = 0; y < CAM_IMAGE_HEIGHT; y++)
// 	{
// 		for (int x = 0; x < CAM_IMAGE_WIDTH; x++)
// 		{
// 			B = IN_RGB[y*(CAM_IMAGE_WIDTH * 3) + x * 3];
// 			G = IN_RGB[y*(CAM_IMAGE_WIDTH * 3) + x * 3 + 1];
// 			R = IN_RGB[y*(CAM_IMAGE_WIDTH * 3) + x * 3 + 2];
// 
// 			if (y == 0 || y == (CAM_IMAGE_HEIGHT - 1)){
// 				tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3] = 0;
// 				tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3 + 1] = 0;
// 				tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3 + 2] = 0;
// 			}
// 			else{
// 				tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3] = B;
// 				tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3 + 1] = G;
// 				tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3 + 2] = R;
// 			}
// 
// 
// 			Sum_Y = (BYTE)((0.29900*R) + (0.58700*G) + (0.11400*B));
// 			Sum_Y = IN_RGB[y*CAM_IMAGE_WIDTH + x];
// 
// 			OriginImage->imageData[y*CAM_IMAGE_WIDTH + x] = Sum_Y;
// 		}
// 	}
	cvSaveImage("D:\\test\\RGBResultImage_Test_Lump.bmp", OriginImage);
	int Particle_Size_Side = m_Separation_V[0];
	int Particle_Size_Middle = m_Separation_V[1];
	int Particle_Size_Center = m_Separation_V[2];

	double Param1 = m_Black_STEP;
	//cvShowImage("Test_Lump", OriginImage);
	cvAdaptiveThreshold(OriginImage, DilateImage, 65535, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 51, Param1);

	cvNot(DilateImage, DilateImage);
	//cvDilate(DilateImage,DilateImage);
	cvErode(DilateImage, DilateImage);




	cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;



	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
		counter++;

	rectArray = new CvRect[counter];
	//	double *areaArray = new double[counter];

	double area = 0, arcCount = 0;
	counter = 0;
	double old_dist = 999999;

	for (; contour != 0; contour = contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);
		double circularity = (4.0*3.14*area) / (arcCount*arcCount);

		rect = cvContourBoundingRect(contour, 1);

		int center_x, center_y;
		center_x = rect.x + rect.width / 2;
		center_y = rect.y + rect.height / 2;

		BYTE R, G, B;
		B = IN_RGB[center_y*(CAM_IMAGE_WIDTH * 3) + center_x * 3];
		G = IN_RGB[center_y*(CAM_IMAGE_WIDTH * 3) + center_x * 3 + 1];
		R = IN_RGB[center_y*(CAM_IMAGE_WIDTH * 3) + center_x * 3 + 2];


		BOOL Rate_Part = FALSE;
		double  Rate = 0;
		if (rect.width > rect.height){
			Rate = (double)rect.width / rect.height;
		}
		else{
			Rate = (double)rect.height / rect.width;
		}
		if (Rate >= 9){
			Rate_Part = TRUE;
		}

		//Size -> 

		if (rect.width < (CAM_IMAGE_WIDTH / 4) && rect.height < (CAM_IMAGE_HEIGHT / 4))
		{
			int Center_X = rect.x + rect.width / 2;
			int Center_Y = rect.y + rect.height / 2;

			////중심 = A
			if (rect.width > Particle_Size_Center && rect.height > Particle_Size_Center && m_Area[Center_X][Center_Y] == 1 && Rate_Part == FALSE)
			{
				rectArray[counter] = rect;


				counter++;

				double dist = GetDistance(center_x, center_y, CAM_IMAGE_WIDTH / 2, CAM_IMAGE_HEIGHT / 2);

				if (old_dist > dist)
				{
					m_currPt.x = center_x - (CAM_IMAGE_WIDTH / 2);
					m_currPt.y = center_y - (CAM_IMAGE_HEIGHT / 2);

					m_currPt.x = m_currPt.x / 2;
					m_currPt.y = m_currPt.y / 2;

					old_dist = dist;
				}

				cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);
				//cvRectangle(RGBResultImage, cvPoint(Center_X-1, Center_Y-1), cvPoint(Center_X+1, Center_Y+1), CV_RGB(0, 255, 0), 1, 8);  

			}

			//// 중간 = B
			if (rect.width > Particle_Size_Middle && rect.height > Particle_Size_Middle && m_Area[Center_X][Center_Y] == 2 && Rate_Part == FALSE)
			{
				rectArray[counter] = rect;


				counter++;

				double dist = GetDistance(center_x, center_y, CAM_IMAGE_WIDTH / 2, CAM_IMAGE_HEIGHT / 2);

				if (old_dist > dist)
				{
					m_currPt.x = center_x - (CAM_IMAGE_WIDTH / 2);
					m_currPt.y = center_y - (CAM_IMAGE_HEIGHT / 2);

					m_currPt.x = m_currPt.x / 2;
					m_currPt.y = m_currPt.y / 2;

					old_dist = dist;
				}

				//cvRectangle(RGBResultImage, cvPoint(Center_X-1, Center_Y-1), cvPoint(Center_X+1, Center_Y+1), CV_RGB(0, 255, 0), 1, 8);  
				cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);
			}

			//// 바깥 = C

			if (rect.width > Particle_Size_Side && rect.height > Particle_Size_Side && m_Area[Center_X][Center_Y] == 3 && Rate_Part == FALSE)
			{
				rectArray[counter] = rect;


				counter++;

				double dist = GetDistance(center_x, center_y, CAM_IMAGE_WIDTH / 2, CAM_IMAGE_HEIGHT / 2);

				if (old_dist > dist)
				{
					m_currPt.x = center_x - (CAM_IMAGE_WIDTH / 2);
					m_currPt.y = center_y - (CAM_IMAGE_HEIGHT / 2);

					m_currPt.x = m_currPt.x / 2;
					m_currPt.y = m_currPt.y / 2;

					old_dist = dist;
				}

				//cvRectangle(RGBResultImage, cvPoint(Center_X-1, Center_Y-1), cvPoint(Center_X+1, Center_Y+1), CV_RGB(0, 255, 0), 1, 8);  
				cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);
			}

		}
	}


	cvSaveImage("D:\\test\\RGBResultImage.bmp", RGBResultImage);
	//cvShowImage("Test_Lump",RGBResultImage);

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&tempOriginImage);





}
double CParticle_Option::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt( (double)((x2-x1)*(x2-x1)) +  ((y2-y1)*(y2-y1)));

	return result;
}

bool CParticle_Option::Crush_detection(LPBYTE IN_RGB)
{
	BYTE R1,G1,B1;
	BYTE R2,G2,B2;
	long int Cnt=0;
	double Sum = 0;

	for (int y=0; y<80; y++)
	{
		for (int x = 0; x<CAM_IMAGE_WIDTH; x++)
		{
			B1 = IN_RGB[y*(CAM_IMAGE_WIDTH*3) + x*3    ];
			G1 = IN_RGB[y*(CAM_IMAGE_WIDTH*3) + x*3 + 1];
			R1 = IN_RGB[y*(CAM_IMAGE_WIDTH*3) + x*3 + 2];

			B2 = IN_RGB[(y + 160)*(CAM_IMAGE_WIDTH * 3) + x * 3];
			G2 = IN_RGB[(y + 160)*(CAM_IMAGE_WIDTH * 3) + x * 3 + 1];
			R2 = IN_RGB[(y + 160)*(CAM_IMAGE_WIDTH * 3) + x * 3 + 2];

			if(R2>R1) Sum += R2-R1;		else Sum += R1-R2; //밝기 차이를 절대값으로 합산함. --> 3번째 반복되는 부분에서 1번째 반복 되는 부분을 빼기함. 
			if(G2>G1) Sum += G2-G1;		else Sum += G1-G2; //밝기 차이를 절대값으로 합산함.
			if(B2>B1) Sum += B2-B1;		else Sum += B1-B2; //밝기 차이를 절대값으로 합산함.		

			Cnt++;
		}
	}	

	Sum /= Cnt;	Sum /= 3;	// 평균 밝기 차이 ( R+G+B 이므로 [나누기 3] )


	if(Sum<5.0)	{ // 이상한 이미지 임을 확인함.
		((CImageTesterDlg *)m_pMomWnd)->Power_Rst();
		//AfxMessageBox(_T("Fail"));
		return FALSE;
	}
	else {
		//AfxMessageBox(_T("ok"));
		return TRUE;
	}
}


void CParticle_Option::OnBnClickedButtonCamsetParti()
{
	((CImageTesterDlg *)m_pMomWnd)->CLGE_CamSet_Parti_ShowWindow();
}


void CParticle_Option::OnBnClickedCheckManualtest()
{
	CString str;
	if (b_ManualTestMode == 0){
		b_ManualTestMode = 1;
	}
	else{
		b_ManualTestMode = 0;
	}
	UpdateData(FALSE);

	str.Empty();
	str.Format("%d", b_ManualTestMode);
	WritePrivateProfileString("PARTICLEOPT", "TESTMODE", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	((CImageTesterDlg *)m_pMomWnd)->b_ManualTestMode = b_ManualTestMode;
}


void CParticle_Option::OnCbnSelendokComboImageSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_nImageSaveMode = m_Cb_ImageSave.GetCurSel();

	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);

	CString str;
	str.Empty();
	str.Format("%d", m_nImageSaveMode);
	WritePrivateProfileString("PARTICLEOPT", "Image_SaveMode", str, str_ModelPath);
}
