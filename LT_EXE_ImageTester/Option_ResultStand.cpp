// Option_ResultStand.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Option_ResultStand.h"


// COption_ResultStand 대화 상자입니다.

IMPLEMENT_DYNAMIC(COption_ResultStand, CDialog)

COption_ResultStand::COption_ResultStand(CWnd* pParent /*=NULL*/)
	: CDialog(COption_ResultStand::IDD, pParent)
{
	m_voltstat = 2;
}

COption_ResultStand::~COption_ResultStand()
{
}

void COption_ResultStand::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(COption_ResultStand, CDialog)
	ON_WM_CTLCOLOR()
//	ON_EN_CHANGE(IDC_EDIT_NAME1, &COption_ResultStand::OnEnChangeEditName1)
ON_EN_SETFOCUS(IDC_VOLT_NAME, &COption_ResultStand::OnEnSetfocusVoltName)
ON_EN_SETFOCUS(IDC_EDIT_VOLT_ON, &COption_ResultStand::OnEnSetfocusEditVoltOn)
ON_EN_SETFOCUS(IDC_VOLT_VALUE, &COption_ResultStand::OnEnSetfocusVoltValue)
ON_EN_SETFOCUS(IDC_EDIT_VOLT_OFF, &COption_ResultStand::OnEnSetfocusEditVoltOff)
ON_EN_SETFOCUS(IDC_EDIT_NAME1, &COption_ResultStand::OnEnSetfocusEditName1)
ON_EN_SETFOCUS(IDC_EDIT_NAME2, &COption_ResultStand::OnEnSetfocusEditName2)
ON_EN_SETFOCUS(IDC_EDIT_NAME3, &COption_ResultStand::OnEnSetfocusEditName3)
ON_EN_SETFOCUS(IDC_EDIT_VAL1, &COption_ResultStand::OnEnSetfocusEditVal1)
ON_EN_SETFOCUS(IDC_EDIT_VAL2, &COption_ResultStand::OnEnSetfocusEditVal2)
ON_EN_SETFOCUS(IDC_EDIT_VAL3, &COption_ResultStand::OnEnSetfocusEditVal3)
ON_EN_SETFOCUS(IDC_EDIT_VOLT_ON2, &COption_ResultStand::OnEnSetfocusEditVoltOn2)
END_MESSAGE_MAP()


// COption_ResultStand 메시지 처리기입니다.
void COption_ResultStand::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void COption_ResultStand::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
BOOL COption_ResultStand::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_VOLT_NAME,&font1,500,23);
	GetDlgItem(IDC_VOLT_VALUE)->SetFont(&font1);
	GetDlgItem(IDC_EDIT_VOLT_ON)->SetFont(&font1);
	GetDlgItem(IDC_EDIT_VOLT_OFF)->SetFont(&font1);
	GetDlgItem(IDC_EDIT_NAME1)->SetFont(&font1);
	GetDlgItem(IDC_EDIT_VAL1)->SetFont(&font1);
	GetDlgItem(IDC_EDIT_NAME2)->SetFont(&font1);
	GetDlgItem(IDC_EDIT_VAL2)->SetFont(&font1);
	GetDlgItem(IDC_EDIT_NAME3)->SetFont(&font1);
	GetDlgItem(IDC_EDIT_VAL3)->SetFont(&font1);
	GetDlgItem(IDC_EDIT_VOLT_ON2)->SetFont(&font1);
	

	Voltage_text("VOLTAGE");
	((CEdit *)GetDlgItem(IDC_EDIT_VOLT_ON))->SetWindowText("VOLT ON");
	((CEdit *)GetDlgItem(IDC_EDIT_VOLT_ON2))->SetWindowText("VOLT ON[ 광원 Register ]");
	((CEdit *)GetDlgItem(IDC_EDIT_VOLT_OFF))->SetWindowText("VOLT OFF");
	m_voltstat = 1;

	CurrState_text("Current Value");
	Test2_Name("EEPROM READ");
	Val1col = RGB(0, 0, 0);
	Val2col = RGB(0, 0, 0);
	Val3col = RGB(0, 0, 0);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL COption_ResultStand::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		if(pMsg->wParam == VK_ADD){

			return TRUE;
		}
		if (pMsg->wParam == VK_SUBTRACT){

			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

HBRUSH COption_ResultStand::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() ==IDC_VOLT_NAME){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() ==IDC_VOLT_VALUE){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_NAME1){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_NAME2){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_NAME3){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_VAL1){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Val1col);
	}

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_VAL2){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Val2col);
	}

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_VAL3){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Val3col);
	}

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_VOLT_ON){
		if(m_voltstat == 0){
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(50, 82, 152));
		}else{
			pDC->SetTextColor(RGB(150, 150, 150));
			pDC->SetBkColor(RGB(200, 200, 200));
		}
	}
	if (pWnd->GetDlgCtrlID() == IDC_EDIT_VOLT_ON2){
		if (m_voltstat == 0){
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(50, 82, 152));
		}
		else{
			pDC->SetTextColor(RGB(150, 150, 150));
			pDC->SetBkColor(RGB(200, 200, 200));
		}
	}
	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_VOLT_OFF){
		if(m_voltstat == 1){
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(50, 82, 152));
		}else{
			pDC->SetTextColor(RGB(150, 150, 150));
			pDC->SetBkColor(RGB(200, 200, 200));
		}
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


void COption_ResultStand::Voltage_text(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_VOLT_NAME))->SetWindowText(lpcszString);
}

void COption_ResultStand::Voltage_Value(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_VOLT_VALUE))->SetWindowText(lpcszString);
}

void COption_ResultStand::UpdateEdit(){
	UpdateEditRect(IDC_EDIT_VOLT_ON);
	UpdateEditRect(IDC_EDIT_VOLT_ON2);
	UpdateEditRect(IDC_EDIT_VOLT_OFF);
}

void COption_ResultStand::UpdateEditRect(int parm_edit_id)
{
	CRect r;
	GetDlgItem(parm_edit_id)->GetWindowRect(r);
	// 에디트 좌표를 대화상자 기준으로 변환한다.
	ScreenToClient(r);
	// 다이얼로그의 에디트 컨트롤 영역을 갱신한다.
	InvalidateRect(r);
}

void COption_ResultStand::InitStat(){
	Test1_Val(0,"");
	Test2_Val(0,"");
	Test3_Val(0,"");
}

void COption_ResultStand::CurrState_text(LPCSTR lpcszString, ...)
{	
	Test1_Name(lpcszString);
}

void COption_ResultStand::CurrState_Val(int col,LPCSTR lpcszString, ...)
{	
	Test1_Val(col,lpcszString);
}

void COption_ResultStand::Test1_Name(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_NAME1))->SetWindowText(lpcszString);
}

void COption_ResultStand::Test1_Val(int col,LPCSTR lpcszString, ...)
{	
	COLORREF Col;
	if(col == 0){
		Col = RGB(18,69,171);
	}else if(col  ==1){
		Col = RGB(201,0,0);
	}else{
		Col = RGB(255,255,0);
	}
	Val1col = Col;
	((CEdit *)GetDlgItem(IDC_EDIT_VAL1))->SetWindowText(lpcszString);
}


void COption_ResultStand::Test2_Name(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_NAME2))->SetWindowText(lpcszString);
}

void COption_ResultStand::Test2_Val(int col,LPCSTR lpcszString, ...)
{	
	COLORREF Col;
	if(col == 0){
		Col = RGB(18,69,171);
	}else if(col  ==1){
		Col = RGB(201,0,0);
	}else{
		Col = RGB(255,255,255);
	}
	Val2col = Col;
	((CEdit *)GetDlgItem(IDC_EDIT_VAL2))->SetWindowText(lpcszString);
}


void COption_ResultStand::Test3_Name(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_NAME1))->SetWindowText(lpcszString);
}

void COption_ResultStand::Test3_Val(int col,LPCSTR lpcszString, ...)
{	
	COLORREF Col;
	if(col == 0){
		Col = RGB(18,69,171);
	}else if(col  ==1){
		Col = RGB(201,0,0);
	}else{
		Col = RGB(255,255,0);
	}
	Val3col = Col;
	((CEdit *)GetDlgItem(IDC_EDIT_VAL3))->SetWindowText(lpcszString);
}

//void COption_ResultStand::OnEnChangeEditName1()
//{
//	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
//	// CDialog::OnInitDialog() 함수를 재지정 
//	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
//	// 이 알림 메시지를 보내지 않습니다.
//
//	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
//}

void COption_ResultStand::OnEnSetfocusVoltName()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();

}

void COption_ResultStand::OnEnSetfocusEditVoltOn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
	if (!((CImageTesterDlg  *)m_pMomWnd)->Power_On()){//파워on을 실패할 경우
		((CImageTesterDlg  *)m_pMomWnd)->StatePrintf("Power On Fail!");
	}


}

void COption_ResultStand::OnEnSetfocusVoltValue()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();

}

void COption_ResultStand::OnEnSetfocusEditVoltOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(!((CImageTesterDlg  *)m_pMomWnd)->Power_Off()){//파워on을 실패할 경우
		((CImageTesterDlg  *)m_pMomWnd)->StatePrintf("Power Off Fail!");
	}
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();

}

void COption_ResultStand::OnEnSetfocusEditName1()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
// 	CString str_buf;
// 	if(((CImageTesterDlg  *)m_pMomWnd)->Current_Detect() == TRUE){
// 		str_buf.Format("%3.1f mA",((CImageTesterDlg  *)m_pMomWnd)->m_CurrentVal);
// 		if((((CImageTesterDlg  *)m_pMomWnd)->m_CurrentVal >= ((CImageTesterDlg  *)m_pMomWnd)->m_MinAmp) && (((CImageTesterDlg  *)m_pMomWnd)->m_CurrentVal <= ((CImageTesterDlg  *)m_pMomWnd)->m_MaxAmp)){
// 			((CImageTesterDlg  *)m_pMomWnd)->m_pResStandOptWnd->CurrState_Val(0,str_buf);//PASS 판란색
// 		}else{
// 			((CImageTesterDlg  *)m_pMomWnd)->m_pResStandOptWnd->CurrState_Val(1,str_buf);//FAIL 빨강색
// 		}
// 	}else{//전류 측정 실패
// 		((CImageTesterDlg  *)m_pMomWnd)->StatePrintf("Current Detect Fail!");
// 		((CImageTesterDlg  *)m_pMomWnd)->m_pResStandOptWnd->CurrState_Val(1,"FAIL");//FAIL 빨강색
// 	}
	
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_ResultStand::OnEnSetfocusEditName2()
{
	((CImageTesterDlg  *)m_pMomWnd)->PreEEPROM_CHECK();
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_ResultStand::OnEnSetfocusEditName3()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_ResultStand::OnEnSetfocusEditVal1()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_ResultStand::OnEnSetfocusEditVal2()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_ResultStand::OnEnSetfocusEditVal3()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void COption_ResultStand::OnEnSetfocusEditVoltOn2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
	if (!((CImageTesterDlg  *)m_pMomWnd)->Power_On(VR_PO_REGISTER_2)){//파워on을 실패할 경우
		((CImageTesterDlg  *)m_pMomWnd)->StatePrintf("Power On Fail!");
	}
}
