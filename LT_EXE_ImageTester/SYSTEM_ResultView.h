#pragma once


// CSYSTEM_ResultView 대화 상자입니다.

class CSYSTEM_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CSYSTEM_ResultView)

public:
	CSYSTEM_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSYSTEM_ResultView();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_SYSTEM };
	void	Setup(CWnd* IN_pMomWnd);

	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	CFont	ft,ft2;

	COLORREF tx_st_col,bk_st_col,Valcol[5];
	void	SYS_RESULT(int num,int col,LPCSTR lpcszString, ...);
	void	SYS_STATE(int col,LPCSTR lpcszString, ...);
	void	InitStat();

private :
	CWnd	*m_pMomWnd;	

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnSetfocusEditSysState();
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
