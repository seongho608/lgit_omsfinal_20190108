#pragma once
#include "afxcmn.h"


// CCENTER_DETECT_ResultView 대화 상자입니다.

class CCENTER_DETECT_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CCENTER_DETECT_ResultView)

public:
	CCENTER_DETECT_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCENTER_DETECT_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_CENTER };

	CFont	font1,font2;
	void	Setup(CWnd* IN_pMomWnd);
	
	CEdit	*pRunEdit;
	CEdit	*pFileEdit;
	CEdit	*pWrModeEdit;

	void	AxisX_Txt(int ch,LPCSTR lpcszString, ...);
	void	AxisY_Txt(int ch,LPCSTR lpcszString, ...);
	void	OffsetX_Txt(int ch,LPCSTR lpcszString, ...);
	void	OffsetY_Txt(int ch,LPCSTR lpcszString, ...);
	void	InitStat();
	void	SetOptMode(int num);
private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CProgressCtrl m_CtrFileProgress;
	afx_msg void OnEnSetfocusFileName();
	afx_msg void OnEnSetfocusWrmode();
	afx_msg void OnEnSetfocusEditXCh1();
	afx_msg void OnEnSetfocusEditYCh1();
	afx_msg void OnEnSetfocusEditOffsetxCh1();
	afx_msg void OnEnSetfocusEditOffsetyCh1();
	afx_msg void OnEnSetfocusEditXCh2();
	afx_msg void OnEnSetfocusEditYCh2();
	afx_msg void OnEnSetfocusEditOffsetxCh2();
	afx_msg void OnEnSetfocusEditOffsetyCh2();
	afx_msg void OnEnSetfocusEditXCh3();
	afx_msg void OnEnSetfocusEditYCh3();
	afx_msg void OnEnSetfocusEditOffsetxCh3();
	afx_msg void OnEnSetfocusEditOffsetyCh3();
	afx_msg void OnEnSetfocusEditXCh4();
	afx_msg void OnEnSetfocusEditYCh4();
	afx_msg void OnEnSetfocusEditOffsetxCh4();
	afx_msg void OnEnSetfocusEditOffsetyCh4();
	afx_msg void OnEnSetfocusRunMode();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
