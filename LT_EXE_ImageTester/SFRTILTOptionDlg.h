#pragma once
#include "afxcmn.h"
#include "afxwin.h"
//
//static LPCTSTR g_szPath[] =
//{
//	_T("PASS"),				// RC_UnknownError = 0,
//	_T("FAIL"),					// RC_OK = 1,
//	NULL
//};
typedef enum enSFR_Grouping
{
	SFR_GROUP_01F = 0,
	SFR_GROUP_03F,
	SFR_GROUP_05F_TL,
	SFR_GROUP_05F_TR,
	SFR_GROUP_05F_BL,
	SFR_GROUP_05F_BR,
	SFR_GROUP_07F_TL,
	SFR_GROUP_07F_TR,
	SFR_GROUP_07F_BL,
	SFR_GROUP_07F_BR,
	SFR_GROUP_MAX,
};
static LPCTSTR g_szSFR_Grouping[] =
{
	_T("0.1F"),
	_T("0.3F"),
	_T("0.5F(TL)"),
	_T("0.5F(TR)"),
	_T("0.5F(BL)"),
	_T("0.5F(BR)"),
	_T("0.7F(TL)"),
	_T("0.7F(TR)"),
	_T("0.7F(BL)"),
	_T("0.7F(BR)"),
	NULL
};
#define MAX_SFR_ROI_NUM 34

typedef struct _ST_MES_SFR
{
	CString szData[37];
	int		nResult[37];
	
	_ST_MES_SFR()
	{
		Reset();
	};

	void Reset()
	{
		for (int _a = 0; _a < 37; _a++)
		{
			szData[_a].Empty();
			nResult[_a] = TEST_NG;
		}
	}
}ST_MES_SFR;

typedef struct _SFR_Data
{
	BOOL bEnable;
	
	CString NAME;
	int nIndex;
	int	nPosX;
	int	nPosY;
	int	nWidth;
	int	nHeight;

	//◁▶ = 0
	//◀▷ = 1
	//△▼ = 2
	//▲▽ = 3
	int nEdgeDir;
	int nFont;

	double dThreshold_MIN;
	double dThreshold_MAX;

	double dResultData;//결과값
	double dTotalData;
	double dOffset;//옵셋
	double dFieldData;
	double dLinepare;
	int		MTF;
	int		nGrouping;

	BYTE	m_CaptureBuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];

	int nResultType;

	BOOL	m_Success; // Pass, Fail

}tSFR_Data;

// CSFRTILTOptionDlg 대화 상자입니다.

class CSFRTILTOptionDlg : public CDialog
{
	DECLARE_DYNAMIC(CSFRTILTOptionDlg)

public:
	CSFRTILTOptionDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSFRTILTOptionDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_SFRTILT };
	
	tSFR_Data SFR_DATA_AVR[MAX_SFR_ROI_NUM];

	void	Setup(CWnd* IN_pMomWnd);
	void Setup(CWnd* IN_pMomWnd,CEdit *pTEDIT, tINFO INFO);
	void	Set_List();
	void Set_GroupList();
	void	Init_Dlg();
	void	Upload_List();
	void	Load_Parameter();
	void	Save_Parameter();
	void	Change_Data(CString str_Value);
	void	Change_Data_Val(int Val);
	//***  SFR Value , Function ***//
	void	SearchROIArea(LPBYTE IN_RGB, int tempWidth);
	void	SFR_Gen(BYTE *RGBScanBuf, int loopCnt, int SFRMODE);
	void	SFR_Gen_16bit(WORD *GRAYScanBuf, int loopCnt, int SFRMODE);

	double	GetSFRValue(BYTE *atemp, int width, int height, int ROI_NUM, int SFRMODE);
	double	GetSFRValue_16bit(WORD *atemp, int width, int height, int ROI_NUM, int SFRMODE);//Ver.2
	double	GetSFRValue_16bit(WORD *atemp, int width, int height, int ROI_NUM, double dPixelSize, int SFRMODE);//Ver.3
	void	MulMatrix(double **A, double **B, double **C, unsigned int Row, unsigned int Col, unsigned int n, unsigned int Mode);
	void	pInverse(double **A, unsigned int Row, unsigned int Col, double **RtnMat);
	void	Inverse(double **dataMat, unsigned int n, double **MatRtn);
	double	GetDistance(int x1, int y1, int x2, int y2);
	CvPoint	m_currPt;
	//***  Tilt Value , Function ***//
	void	Mid_XTilt_Pair();
	void	Mid_YTilt_Pair();
	void	Side_XTilt_Pair();
	void	Side_YTilt_Pair();
	
	double Tilt_x;
	double Tilt_y;

	int ID_XTiltPoint1;
	int ID_XTiltPoint2;
	int ID_YTiltPoint1;
	int ID_YTiltPoint2;

	int Low_SFRDATA_X_Tilt;
	int Low_SFRDATA_Y_Tilt;

	///161017 KDY 추가
	// SFR 주변부 Delta 구하기 
	double ComputeDelta(double side[]);
	double delta_V;
	double delta_H;
	BOOL   delta_V_Success;
	BOOL   delta_H_Success;
	double delta_V_Thre;
	double delta_H_Thre;

	//170809 AMH 추가
	// SFR DATA 산출방식 변경 기능
	int	m_bSFRmode;
	CComboBox	*pSFRMode;
	void	CHANGESELLIST(BOOL MODE);
	void	SETLIST(BOOL MODE);

	void InitStat();
	void InitPrm();
	tSFR_Data		SFR_DATA[MAX_SFR_ROI_NUM];
	tSFR_Data		INIT_SFR_DATA[34];
	double m_dPixelSize;
	double m_dLinePerPixel;
	double m_dThreshold;
	double m_dLinePerPixel_Side;
	double m_dThreshold_Side;
	int m_Capture_Count;
	int m_DefaultSet_PosX;
	int m_DefaultSet_PosY;
	int m_DefaultSet_Width;
	int m_DefaultSet_Height;
	//double m_DefaultSet_Threshold;
	BOOL	b_OverShoot;
	int POSITIONDATA;

	tResultVal Run();
	tResultVal ETC_Run();
	void Pic(CDC *cdc);
	void SFR_Pic(CDC *cdc, int nCnt);
	void Delta_Pic(CDC *cdc);

	int m_ETC_State;

	BOOL EXCEL_UPLOAD();
	void EXCEL_SAVE();
	void Set_List(CListCtrl *List);
	void FAIL_UPLOAD();
	void	FINAL_UPLOAD(LPCSTR lpcszString);
	void InsertList();
	bool Insertcheck;
	int InsertIndex;
	int Lot_InsertIndex;

	int StartCnt;
		
	int iSavedItem;
	int iSavedSubitem;
	
	// MES 정보저장
	CString m_szMesResult;
	CString Mes_Result();
	void	CopyMesData();
	bool	b_StopFail;
	ST_MES_SFR	m_stMes;
	UINT m_nIdex;
#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	int Lot_StartCnt;
	int Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

#pragma endregion
private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
	CString		strTitle;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_ROIList;
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnNMDblclkListRoi(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBtnSave();
	afx_msg void OnBnClickedBtnLoad();
	afx_msg void OnEnKillfocusEditRdMod();
	afx_msg void OnCbnKillfocusComboRdMode();
	afx_msg void OnCbnKillfocusComboFontMode();
	afx_msg void OnCbnKillfocusComboGroupMode();
	afx_msg void OnBnClickedBtnEtcSave();
	afx_msg void OnBnClickedBtnDefaultSet();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedChkOvershoot();
	BOOL b_Smooth;
	CListCtrl m_SFRList;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnNMClickListRoi(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBtnAutoCheck();
	afx_msg void OnBnClickedButtonSet();
	afx_msg void OnBnClickedButtonSet2();
	CListCtrl m_Lot_SFRList;

	afx_msg void OnBnClickedCheckEdgeoff();
	BOOL b_Edge_Off;
	afx_msg void OnBnClickedButtontest();
	afx_msg void OnBnClickedDeltaSave();
	CComboBox m_Combo_ImageSave;
	int m_ImageSaveMode;
	CString str_ImageSavePath;
	afx_msg void OnBnClickedButtonSaveOp();
	afx_msg void OnBnClickedButtonPathopen();
	void Image_Save(UINT MODE, BOOL Result);


	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnCbnSelchangeComboSfrmode();
	BOOL m_bAutomation;
	afx_msg void OnBnClickedAutomationCheck();
	void Reload_Rect();
	CComboBox m_Cb_ImageSave;
	afx_msg void OnCbnSelendokComboImageSave();
private:
	int m_nImageSaveMode;
public:
	CComboBox m_Cb_XTilt1;
	CComboBox m_Cb_XTilt2;
	CComboBox m_Cb_Y1Tilt1;
	CComboBox m_Cb_Y1Tilt2;
	CComboBox m_Cb_Y2Tilt1;
	CComboBox m_Cb_Y2Tilt2;

	int m_nXtilt1;
	int m_nXtilt2;
	int m_nY1tilt1;
	int m_nY1tilt2;
	int m_nY2tilt1;
	int m_nY2tilt2;

	double m_dXtilt;
	double m_dY1tilt;
	double m_dY2tilt;

	double m_dXtilt_result;
	double m_dY1tilt_result;
	double m_dY2tilt_result;
	
	// Grouping [1/8/2019 Seongho.Lee]

	int m_n05FAspec_Min;
	int m_n05FAspec_Max;
	int m_n07FAspec_Min;
	int m_n07FAspec_Max;

	double	m_dAvr01F_Group;
	double	m_dAvr03F_Group;
	double	m_dAvr05F_Group;
	double	m_dAvr07F_Group;

	double m_dMaxTL;
	double m_dMaxTR;
	double m_dMaxBL;
	double m_dMaxBR;

	double m_dAvr05FA_Group;			//	A(%) = Max(TL:TR:BL:BR) - Min(TL:TR:BL:BR) / Average(TL:TR:BL:BR)
	double m_dAvr07FA_Group;			//	A(%) = Max(TL:TR:BL:BR) - Min(TL:TR:BL:BR) / Average(TL:TR:BL:BR)
	//////////////////////////////////////////////////////////////////////////
	int EachTiltResult[3];


	afx_msg void OnCbnSelendokComboXtilt1();
	afx_msg void OnCbnSelendokComboXtilt2();
	afx_msg void OnCbnSelendokComboY1tilt1();
	afx_msg void OnCbnSelendokComboY1tilt2();
	afx_msg void OnCbnSelendokComboY2tilt1();
	afx_msg void OnCbnSelendokComboY2tilt2();
	afx_msg void OnBnClickedButtonTiltSave();
	afx_msg void OnBnClickedButtonCaptureCountSave();
	CListCtrl m_GroupList;
};
void CModel_Create(CSFRTILTOptionDlg **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO);
void CModel_Delete(CSFRTILTOptionDlg **pWnd);