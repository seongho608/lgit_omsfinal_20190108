// DynamicRange_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "DynamicRange_Option.h"
#include "afxdialogex.h"


// CDynamicRange_Option 대화 상자입니다.

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
extern WORD	m_GRAYScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];

IMPLEMENT_DYNAMIC(CDynamicRange_Option, CDialog)

CDynamicRange_Option::CDynamicRange_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CDynamicRange_Option::IDD, pParent)
	, SNRBW(0)
{
	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;
	ListItemNum = 0; 
	InsertIndex = 0;
	StartCnt = 0;
	Lot_InsertNum = 0;
	Lot_InsertIndex = 0;
	Lot_StartCnt = 0;
	iSavedItem = 0;
	iSavedSubitem=0 ;
	changecount = 0;
	for (int a = 0; a < 6; a++){
		for (int b = 0; b < 3; b++){
			DR_RECT[a].Signal[b] = 0;
			DR_RECT[a].Noise[b] = 0;
		}
	}
	for (int lop = 0; lop < 3; lop++){
		m_Brightness[lop] = 0;
	}

	m_b_StopFail = FALSE;
}

CDynamicRange_Option::~CDynamicRange_Option()
{
}

void CDynamicRange_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_DYNAMICRANGE, m_DynamicRangeList);
	DDX_Control(pDX, IDC_LIST_DYNAMICRANGE_LOT, m_Lot_DynamicRangeList);
	DDX_Control(pDX, IDC_LIST_DynamicRangeList, DR_DATALIST);
	DDX_Text(pDX, IDC_Thresold_SRNBW, SNRBW);
	DDX_Text(pDX, IDC_EDIT_BRIGHTNESS, m_Brightness[0]);
	DDX_Text(pDX, IDC_EDIT_BRIGHTNESS2, m_Brightness[1]);
	DDX_Text(pDX, IDC_EDIT_BRIGHTNESS3, m_Brightness[2]);
}


BEGIN_MESSAGE_MAP(CDynamicRange_Option, CDialog)
	ON_WM_SHOWWINDOW()
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_LIST_DynamicRangeList, &CDynamicRange_Option::OnNMClickListDynamicrangelist)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_DynamicRangeList, &CDynamicRange_Option::OnNMCustomdrawListDynamicrangelist)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_DynamicRangeList, &CDynamicRange_Option::OnNMDblclkListDynamicrangelist)
	ON_EN_CHANGE(IDC_EDIT_DR_MOD, &CDynamicRange_Option::OnEnChangeEditDrMod)
	ON_EN_KILLFOCUS(IDC_EDIT_DR_MOD, &CDynamicRange_Option::OnEnKillfocusEditDrMod)
	ON_BN_CLICKED(IDC_BUTTON_DR_LOAD, &CDynamicRange_Option::OnBnClickedButtonDrLoad)
	ON_BN_CLICKED(IDC_BUTTON_DR_SAVEZONE, &CDynamicRange_Option::OnBnClickedButtonDrSavezone)
	ON_WM_MOUSEWHEEL()
	ON_BN_CLICKED(IDC_BUTTON_TEST_, &CDynamicRange_Option::OnBnClickedButtonTest)
	ON_BN_CLICKED(IDC_BUTTON_DR_SAVE, &CDynamicRange_Option::OnBnClickedButtonDrSave)
	ON_BN_CLICKED(IDC_BUTTON_SETDRZone, &CDynamicRange_Option::OnBnClickedButtonSetdrzone)
	ON_EN_CHANGE(IDC_EDIT_BRIGHTNESS, &CDynamicRange_Option::OnEnChangeEditBrightness)
	ON_EN_CHANGE(IDC_EDIT_BRIGHTNESS2, &CDynamicRange_Option::OnEnChangeEditBrightness2)
	ON_EN_CHANGE(IDC_EDIT_BRIGHTNESS3, &CDynamicRange_Option::OnEnChangeEditBrightness3)
END_MESSAGE_MAP()


// CDynamicRange_Option 메시지 처리기입니다.
void CDynamicRange_Option::Setup(CWnd* IN_pMomWnd, CEdit	*pTEDIT, tINFO INFO)
{
	m_pMomWnd = IN_pMomWnd;
	pTStat = pTEDIT;
	m_INFO = INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);;
}

BOOL CDynamicRange_Option::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;

	DR_filename = ((CImageTesterDlg  *)m_pMomWnd)->modelfile_Name;
	SetList();
	Load_parameter();
	UpdateData(FALSE);

	InitPrm();

	Set_List(&m_DynamicRangeList);
	LOT_Set_List(&m_Lot_DynamicRangeList);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDynamicRange_Option::SetList(){
	DR_DATALIST.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_CHECKBOXES);
	DR_DATALIST.InsertColumn(0, "Zone", LVCFMT_CENTER, 95);
	DR_DATALIST.InsertColumn(1, "PosX", LVCFMT_CENTER, 60);
	DR_DATALIST.InsertColumn(2, "PosY", LVCFMT_CENTER, 60);
	DR_DATALIST.InsertColumn(3, "START X", LVCFMT_CENTER, 60);
	DR_DATALIST.InsertColumn(4, "START Y", LVCFMT_CENTER, 60);
	DR_DATALIST.InsertColumn(5, "EXIT X", LVCFMT_CENTER, 60);
	DR_DATALIST.InsertColumn(6, "EXIT Y", LVCFMT_CENTER, 60);
	DR_DATALIST.InsertColumn(7, "Width", LVCFMT_CENTER,50);
	DR_DATALIST.InsertColumn(8, "Height", LVCFMT_CENTER, 50);
	DR_DATALIST.InsertColumn(9, "SNR_BW", LVCFMT_CENTER, 60);
	DR_DATALIST.InsertColumn(10, "Dynamic Range", LVCFMT_CENTER, 60);


}

bool CDynamicRange_Option::Change_DATA_CHECK(bool FLAG){

	BOOL STAT = TRUE;
	//상한

	if (DR_RECT[iSavedItem].m_Width > m_CAM_SIZE_WIDTH)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].m_Height > m_CAM_SIZE_HEIGHT)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].m_PosX > m_CAM_SIZE_WIDTH)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].m_PosY > m_CAM_SIZE_HEIGHT)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].m_Top > m_CAM_SIZE_HEIGHT)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].m_Bottom > m_CAM_SIZE_HEIGHT)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].m_Left> m_CAM_SIZE_WIDTH)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].m_Right> m_CAM_SIZE_WIDTH)
		STAT = FALSE;

	
	//하한

	if (DR_RECT[iSavedItem].m_Width < 1)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].m_Height < 1)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].m_PosX < 0)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].m_PosY < 0)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].m_Top < 0)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].m_Bottom < 0)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].m_Left< 0)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].m_Right< 0)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].d_Thresold < 0)
		STAT = FALSE;
	if (DR_RECT[iSavedItem].d_Thresold_BW < 0)
		STAT = FALSE;

	if (FLAG == 1){
		if (DR_RECT[iSavedItem].m_Width == 1)
			STAT = FALSE;
		if (DR_RECT[iSavedItem].m_Height == 1)
			STAT = FALSE;

	}

	return STAT;



}
void CDynamicRange_Option::EditWheelIntSet(int nID, int Val)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID, str_buf);
	str_buf.Remove(' ');
	if (str_buf == ""){
		buf = 0;
	}
	else{
		buf = GetDlgItemInt(nID);
		buf += Val;
	}
	SetDlgItemInt(nID, buf, 1);
	((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
}

void CDynamicRange_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if (bShow == TRUE){
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;

		if (((CImageTesterDlg  *)m_pMomWnd)->b_SecretOption == 1){

		}
		else{

		}

	}
	else{
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = -1;

	}
}


void CDynamicRange_Option::OnTimer(UINT_PTR nIDEvent)
{
	UINT BUF = 0;
	int _a = 0;
	if (nIDEvent == 150){
		KillTimer(150);
		BUF = DR_DATALIST.GetItemState(iSavedItem, LVIS_STATEIMAGEMASK);

		if (BUF == 0x2000){

			DR_RECT[iSavedItem].ENABLE = TRUE;

		}else if (BUF == 0x1000){

			DR_RECT[iSavedItem].ENABLE = FALSE;

		}

		UploadList();

		InitPrm();
	}
	else if (nIDEvent == 160){
		KillTimer(160);
		BUF = DR_DATALIST.GetItemState(iSavedItem, LVIS_STATEIMAGEMASK);
	}

	CDialog::OnTimer(nIDEvent);
}

BOOL CDynamicRange_Option::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){

			return TRUE;
		}

		if (pMsg->wParam == VK_RETURN){

			return TRUE;
		}

	}

	return CDialog::PreTranslateMessage(pMsg);
}
void CDynamicRange_Option::InitPrm()
{
	for (int ROINUM = 0; ROINUM < 4; ROINUM++)
	{
		if (DR_RECT[ROINUM].ENABLE == FALSE)
		{
			((CImageTesterDlg  *)m_pMomWnd)->m_pResDynamicRangeOptWnd->DYNAMICRANGENUM_TEXT(ROINUM,"X");
			((CImageTesterDlg  *)m_pMomWnd)->m_pResDynamicRangeOptWnd->DYNAMICRANGENUM_TEXT(ROINUM+4, "X");
			((CImageTesterDlg  *)m_pMomWnd)->m_pResDynamicRangeOptWnd->RESULT_TEXT(ROINUM,3, "Disable");
		}
		else
		{
			((CImageTesterDlg  *)m_pMomWnd)->m_pResDynamicRangeOptWnd->DYNAMICRANGENUM_TEXT(ROINUM,"");
			((CImageTesterDlg  *)m_pMomWnd)->m_pResDynamicRangeOptWnd->DYNAMICRANGENUM_TEXT(ROINUM+4, "");
			((CImageTesterDlg  *)m_pMomWnd)->m_pResDynamicRangeOptWnd->RESULT_TEXT(ROINUM,0, "STAND BY");
		}

		DR_RECT[ROINUM].m_result = 0.0;
		DR_RECT[ROINUM].m_result_BW = 0.0;
	}
}
tResultVal CDynamicRange_Option::Run()
{
	tResultVal retval = { 0, };
	CString str = "";
	BOOL m_Success = TRUE;

	m_b_StopFail = FALSE;

	UploadList();
	InitPrm();


	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

		InsertList();
	}

	//TEST 알고리즘
// 	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = 1;
// 	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;
// 
// 	for (int i = 0; i < 10; i++){
// 		if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0 && ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0){
// 			break;
// 		}
// 		else{
// 			DoEvents(50);
// 		}
// 	}
// 
// 
// 	for (int lop = 0; lop < 2; lop++){
// 		if (lop == 1)
// 			lop = 3;
// 		if (DR_RECT[lop].ENABLE == TRUE){
// 			DR_RECT[lop].m_Success = SNRGen_16bit(m_GRAYScanbuf, lop);
// 			m_Success &= DR_RECT[lop].m_Success;
// 		}
// 
// 		for (int NUM = 0; NUM < 3; NUM++){
// 			DR_RECT[NUM + lop].m_Success = DR_RECT[lop].m_Success;
// 		}
// 
// 	}

	for (int lop = 0; lop < 3; lop++){
		// 광원 밝기 조정
		((CImageTesterDlg *)m_pMomWnd)->Control_Current_SLOT(3, m_Brightness[lop]);
		// 광원및 영상 안정화 시간
		DoEvents(((CImageTesterDlg *)m_pMomWnd)->i_Lightondelay);

 		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = 1;
 		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;
 		 
 		for (int i = 0; i < 10; i++){
 		 	if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0 && ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0){
 		 		break;
 		 	}
 		 	else{
 		 		DoEvents(50);
 		 	}
 		}
		for (int ROINUM = 0; ROINUM < 4; ROINUM++){
			SignalnNoiseGen(m_GRAYScanbuf, lop, ROINUM);


		}
	}

	int Totalcnt = 0;
	int Passcnt = 0;
	for (int ROINUM = 0; ROINUM < 4; ROINUM++){

		if (DR_RECT[ROINUM].ENABLE == TRUE)
		{
			Totalcnt++;

			if ((DR_RECT[ROINUM].Signal[0] > DR_RECT[ROINUM].Signal[2]) && (DR_RECT[ROINUM].Noise[1] != 0))
				DR_RECT[ROINUM].m_result = 20 * log10l((DR_RECT[ROINUM].Signal[0] - DR_RECT[ROINUM].Signal[2]) / DR_RECT[ROINUM].Noise[1]); // SNR_BW
			else
				DR_RECT[ROINUM].m_result = 0; // 정상적인 TEST 환경이 아님


			if ((DR_RECT[ROINUM].Signal[0] > DR_RECT[ROINUM].Signal[2]) && (DR_RECT[ROINUM].Signal[2] != 0))
				DR_RECT[ROINUM].m_result_BW = 20 * log10l(DR_RECT[ROINUM].Signal[0] / DR_RECT[ROINUM].Signal[2]); // Dynamic Range
			else if ((DR_RECT[ROINUM].Signal[2] == 0))
				DR_RECT[ROINUM].m_result_BW = 100; // Noise Zero ALL BLACK 
			else
				DR_RECT[ROINUM].m_result_BW = 0; // 정상적인 TEST 환경이 아님

			
			if ((DR_RECT[ROINUM].m_result >= DR_RECT[ROINUM].d_Thresold) && (DR_RECT[ROINUM].m_result_BW >= DR_RECT[ROINUM].d_Thresold_BW)){
				Passcnt++;
				DR_RECT[ROINUM].m_Success = TRUE;
			}
			else{
				DR_RECT[ROINUM].m_Success = FALSE;
			}
		}

	}
	if (Totalcnt == Passcnt){
		m_Success = TRUE;
	}
	else{
		m_Success = FALSE;
	}
	//---------------------------

	retval.m_ID = 0x13;
	retval.ValString.Empty();
	CString stateDATA = "DynamicRange_ ";
	BOOL b_environment = TRUE;
	//if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE)
	{
		for (int ROINUM = 0; ROINUM < 4; ROINUM++){

			if (ROINUM == 0){
				str.Empty(); str = "LEFT_TOP_";
			}
			if (ROINUM == 1){
				str.Empty(); str = "LEFT_BOTTOM_";
			}
			if (ROINUM == 2){
				str.Empty(); str = "RIGHT_TOP_";
			}
			if (ROINUM == 3){
				str.Empty(); str = "RIGHT_BOTTOM_";
			}
			stateDATA += str;

			if (DR_RECT[ROINUM].ENABLE == TRUE)
			{
				str.Empty();
				str.Format("%6.2f", DR_RECT[ROINUM].m_result);
				m_DynamicRangeList.SetItemText(InsertIndex, 4 + ROINUM, str);
				str.Format("SNR_BW_%6.2f", DR_RECT[ROINUM].m_result);
				stateDATA += str;
				((CImageTesterDlg  *)m_pMomWnd)->m_pResDynamicRangeOptWnd->DYNAMICRANGENUM_TEXT(ROINUM,str);
				str.Empty();
				str.Format("%6.2f", DR_RECT[ROINUM].m_result_BW);
				m_DynamicRangeList.SetItemText(InsertIndex, 8 + ROINUM, str);
				str.Format("D.R_%6.2f", DR_RECT[ROINUM].m_result_BW);
				stateDATA += str;
				((CImageTesterDlg  *)m_pMomWnd)->m_pResDynamicRangeOptWnd->DYNAMICRANGENUM_TEXT(ROINUM + 4,str);

				if (DR_RECT[ROINUM].m_Success == true){
					((CImageTesterDlg  *)m_pMomWnd)->m_pResDynamicRangeOptWnd->RESULT_TEXT(ROINUM,1, "PASS");
				}
				else{
					((CImageTesterDlg  *)m_pMomWnd)->m_pResDynamicRangeOptWnd->RESULT_TEXT(ROINUM,2, "FAIL");
				}

				if ((DR_RECT[ROINUM].m_result == 0) || (DR_RECT[ROINUM].m_result_BW == 0))
					b_environment &= FALSE;
			}
			else{
				str.Empty();
				str.Format("X");
				m_DynamicRangeList.SetItemText(InsertIndex, 4 + ROINUM, str);
				m_DynamicRangeList.SetItemText(InsertIndex, 8 + ROINUM, str);
				stateDATA += str;
				((CImageTesterDlg  *)m_pMomWnd)->m_pResDynamicRangeOptWnd->DYNAMICRANGENUM_TEXT(ROINUM, str);
				((CImageTesterDlg  *)m_pMomWnd)->m_pResDynamicRangeOptWnd->DYNAMICRANGENUM_TEXT(ROINUM + 4, str);
			}

		}

	}

	if (m_Success == TRUE){ // 최종 결과출력
		retval.m_Success = TRUE;

		if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			m_DynamicRangeList.SetItemText(InsertIndex, 12, "PASS");////////
			((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_DYNAMICRANGE, "PASS");
		}

		stateDATA += "_ PASS";
		retval.ValString.Format("OK");

	}
	else{
		retval.m_Success = FALSE;

		if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			m_DynamicRangeList.SetItemText(InsertIndex, 12, "FAIL");////////
			((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_DYNAMICRANGE, "FAIL");
		}
		stateDATA += "_ FAIL";
		retval.ValString.Format("FAIL");

	}

	if (b_environment == FALSE){
		retval.ValString.Format("TEST 환경이 아닙니다.");
	}
	((CImageTesterDlg  *)m_pMomWnd)->StatePrintf(stateDATA);

	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
		StartCnt++;
	}

	CopyMesData();
	return retval;
}

CString CDynamicRange_Option::Mes_Result()
{
	m_szMesResult = _T("");
	
	// LT_BW:DR,RT_BW:DR,LB_BW:DR, RB_BW:DR
	if (m_b_StopFail == FALSE)
	{
		m_szMesResult.Format(_T("%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d"), m_stMes[0].szDataBW, m_stMes[0].nResultBW, m_stMes[0].szDataDR, m_stMes[0].nResultDR,
																					m_stMes[1].szDataBW, m_stMes[1].nResultBW, m_stMes[1].szDataDR, m_stMes[1].nResultDR,
																					m_stMes[2].szDataBW, m_stMes[2].nResultBW, m_stMes[2].szDataDR, m_stMes[2].nResultDR,
																					m_stMes[3].szDataBW, m_stMes[3].nResultBW, m_stMes[3].szDataDR, m_stMes[3].nResultDR);
	}
	else
	{
		m_szMesResult.Format(_T(":1,:1,:1,:1,:1,:1,:1,:1"));
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_stNewMesData[NewMES_DynamicRange] = m_szMesResult;

	return m_szMesResult;
}

void CDynamicRange_Option::CopyMesData()
{
	for (int _a = 0; _a < 4; _a++)
	{
		if (DR_RECT[_a].m_result >= DR_RECT[_a].d_Thresold)
			m_stMes[_a].nResultBW = TEST_OK;
		else
			m_stMes[_a].nResultBW = TEST_NG;
					
		if (DR_RECT[_a].m_result_BW >= DR_RECT[_a].d_Thresold_BW)
			m_stMes[_a].nResultDR = TEST_OK;
		else
			m_stMes[_a].nResultDR = TEST_NG;
		
		m_stMes[_a].szDataBW.Format(_T("%6.2f"), DR_RECT[_a].m_result);
		m_stMes[_a].szDataDR.Format(_T("%6.2f"), DR_RECT[_a].m_result_BW);
	}
}

void CDynamicRange_Option::Pic(CDC *cdc)
{
	for (int lop = 0; lop < 4; lop++){
		DynamicRangePic(cdc, lop);
	}
}
bool CDynamicRange_Option::DynamicRangePic(CDC *cdc, int NUM){

	//if(EIJARect.Chkdata() == FALSE){return 0;}//재대로 된 데이터가 아니면 바로 빠져나간다.

	CPen	my_Pan, *old_pan;
	CString TEXTDATA = "";
	::SetBkMode(cdc->m_hDC, TRANSPARENT);

	if (DR_RECT[NUM].ENABLE == TRUE)
	{


		if (DR_RECT[NUM].m_Success == TRUE){
			my_Pan.CreatePen(PS_SOLID, 2, BLUE_COLOR);
			cdc->SetTextColor(BLUE_COLOR);
		}
		else{
			my_Pan.CreatePen(PS_SOLID, 2, RED_COLOR);
			cdc->SetTextColor(RED_COLOR);
		}



		old_pan = cdc->SelectObject(&my_Pan);

		cdc->SetTextAlign(TA_CENTER | TA_BASELINE);

		cdc->MoveTo(DR_RECT[NUM].m_Left, DR_RECT[NUM].m_Top);
		cdc->LineTo(DR_RECT[NUM].m_Right, DR_RECT[NUM].m_Top);
		cdc->LineTo(DR_RECT[NUM].m_Right, DR_RECT[NUM].m_Bottom);
		cdc->LineTo(DR_RECT[NUM].m_Left, DR_RECT[NUM].m_Bottom);
		cdc->LineTo(DR_RECT[NUM].m_Left, DR_RECT[NUM].m_Top);

//		if ((NUM == 0) || (NUM == 3))
//		{
			TEXTDATA.Format("SNR_BW: %6.2f", DR_RECT[NUM].m_result);
			cdc->TextOut(DR_RECT[NUM].m_PosX, DR_RECT[NUM].m_Top - 15, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());

			TEXTDATA.Format("DR: %6.2f", DR_RECT[NUM].m_result_BW);
			cdc->TextOut(DR_RECT[NUM].m_PosX, DR_RECT[NUM].m_Top - 4, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());
//		}

// 		cdc->SetTextColor(GREEN_DK_COLOR);
// 		if ((NUM == 1) || (NUM == 4)){
// 			TEXTDATA.Format("STD: %6.6f", DR_RECT[NUM].Noise);
// 			cdc->TextOut(DR_RECT[NUM].m_PosX, DR_RECT[NUM].m_Top - 15, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());
// 		}
// 		else{
// 			TEXTDATA.Format("AVE: %d", DR_RECT[NUM].Signal);
// 			cdc->TextOut(DR_RECT[NUM].m_PosX, DR_RECT[NUM].m_Bottom + 15, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());
// 		}
		


		
		cdc->SelectObject(old_pan);

		old_pan->DeleteObject();
		my_Pan.DeleteObject();

	}

	return 1;
}
void CDynamicRange_Option::Set_List(CListCtrl *List){

	while (List->DeleteColumn(0));
	((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0, "NO", LVCFMT_CENTER, 30);
	List->InsertColumn(1, "모델명", LVCFMT_CENTER, 100);
	//List->InsertColumn(2, "LOT CARD", LVCFMT_CENTER, 100);
	List->InsertColumn(2, "Start DATE", LVCFMT_CENTER, 80);
	List->InsertColumn(3, "Start TIME", LVCFMT_CENTER, 80);
	List->InsertColumn(4, "LT_SNRBW", LVCFMT_CENTER, 80);
	List->InsertColumn(5, "LB_SNRBW", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "RT_SNRBW", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "RB_SNRBW", LVCFMT_CENTER, 80);
	List->InsertColumn(8, "LT_DR", LVCFMT_CENTER, 80);
	List->InsertColumn(9, "LB_DR", LVCFMT_CENTER, 80);
	List->InsertColumn(10, "RT_DR", LVCFMT_CENTER, 80);
	List->InsertColumn(11, "RB_DR", LVCFMT_CENTER, 80);
	List->InsertColumn(12, "RESULT", LVCFMT_CENTER, 80);
	List->InsertColumn(13, "비고", LVCFMT_CENTER, 80);
	ListItemNum = 14;
	Copy_List(&m_DynamicRangeList, ((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList, ListItemNum);
}
void CDynamicRange_Option::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt, strStartMode;


	InsertIndex = m_DynamicRangeList.InsertItem(StartCnt, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex + 1);
	m_DynamicRangeList.SetItemText(InsertIndex, 0, strCnt);

	m_DynamicRangeList.SetItemText(InsertIndex, 1, ((CImageTesterDlg  *)m_pMomWnd)->m_modelName);
	m_DynamicRangeList.SetItemText(InsertIndex, 2, ((CImageTesterDlg  *)m_pMomWnd)->strDay + "");
	m_DynamicRangeList.SetItemText(InsertIndex, 3, ((CImageTesterDlg  *)m_pMomWnd)->strTime + "");



}
void CDynamicRange_Option::EXCEL_SAVE()
{
	CString Item[8] = { "LT_SNRBW", "LB_SNRBW", "RT_SNRBW", "RB_SNRBW","LT_DR" ,"LB_DR" ,"RT_DR", "RB_DR" };
	
	CString Data = "";
	CString str = "";
	str = "***********************DynamicRange [ 단위 : dB ] ***********************\n\n";
	Data += str;
	str.Empty();
	str.Format("SNR_BW Threshold, LT_SNRBW, %0.1f,LB_SNRBW, %0.1f, RT_SNRBW, %0.1f, RB_SNRBW, %0.1f,  \n", DR_RECT[0].d_Thresold, DR_RECT[1].d_Thresold, DR_RECT[2].d_Thresold, DR_RECT[3].d_Thresold);//Threshold
	Data += str;
	str.Empty();
	str.Format("D.R Threshold, LT_DR, %0.1f,LB_DR, %0.1f, RT_DR, %0.1f, RB_DR, %0.1f,  \n", DR_RECT[0].d_Thresold_BW, DR_RECT[1].d_Thresold_BW, DR_RECT[2].d_Thresold_BW, DR_RECT[3].d_Thresold_BW);//d_Thresold_BW
	Data += str;
	str.Empty();
	str = "*************************************************\n";
	Data += str;

	((CImageTesterDlg  *)m_pMomWnd)->SAVE_EXCEL("DynamicRange", Item, 8, &m_DynamicRangeList, Data);
}

void CDynamicRange_Option::FAIL_UPLOAD(){

//	if (((CImageTesterDlg  *)m_pMomWnd)->LotMod == 1){
// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 			LOT_InsertDataList();
// 			for (int t = 0; t < 9; t++)
// 			{
// 				m_Lot_DynamicRangeList.SetItemText(Lot_InsertIndex, t + 5, "X");//result
// 
// 			}
// 			m_Lot_DynamicRangeList.SetItemText(Lot_InsertIndex, 14, "FAIL");//result
// 			m_Lot_DynamicRangeList.SetItemText(Lot_InsertIndex, 15, "STOP");//비고
// 			Lot_StartCnt++;
// 		}
// 		else{
			InsertList();
			for (int t = 0; t < 9; t++)
			{
				m_DynamicRangeList.SetItemText(InsertIndex, t + 4, "X");//result

			}
			m_DynamicRangeList.SetItemText(InsertIndex, 12, "FAIL");//result
			m_DynamicRangeList.SetItemText(InsertIndex, 13, "STOP");//비고
			((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_DYNAMICRANGE, "STOP");
			StartCnt++;

			m_b_StopFail = TRUE;
		//}
//	}
}
bool CDynamicRange_Option::EXCEL_UPLOAD(){
	m_DynamicRangeList.DeleteAllItems();
	if (((CImageTesterDlg  *)m_pMomWnd)->EXCEL_UPLOAD("DynamicRange", &m_DynamicRangeList, 1, &StartCnt) == TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}

	return TRUE;
}


void CDynamicRange_Option::LOT_Set_List(CListCtrl *List){

	while (List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0, "NO", LVCFMT_CENTER, 30);
	List->InsertColumn(1, "모델명", LVCFMT_CENTER, 100);
	List->InsertColumn(2, "LOT 명", LVCFMT_CENTER, 80);
	List->InsertColumn(3, "Start DATE", LVCFMT_CENTER, 80);
	List->InsertColumn(4, "Start TIME", LVCFMT_CENTER, 80);
	List->InsertColumn(5, "LT_SNRBW", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "LB_SNRBW", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "RT_SNRBW", LVCFMT_CENTER, 80);
	List->InsertColumn(8, "RB_SNRBW", LVCFMT_CENTER, 80);
	List->InsertColumn(9, "LT_DR", LVCFMT_CENTER, 80);
	List->InsertColumn(10, "LB_DR", LVCFMT_CENTER, 80);
	List->InsertColumn(11, "RT_DR", LVCFMT_CENTER, 80);
	List->InsertColumn(12, "RB_DR", LVCFMT_CENTER, 80);
	List->InsertColumn(13, "RESULT", LVCFMT_CENTER, 80);
	List->InsertColumn(14, "비고", LVCFMT_CENTER, 80);
	Lot_InsertNum = 15;
	//Copy_List(&m_Lot_DynamicRangeList, ((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList, Lot_InsertNum);
}

void CDynamicRange_Option::LOT_InsertDataList(){
	CString strCnt = "";

	int Index = m_Lot_DynamicRangeList.InsertItem(Lot_StartCnt,"",0);
//	Lot_InsertIndex = m_Lot_DynamicRangeList.InsertItem(Lot_StartCnt, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Lot_InsertIndex + 1);
	m_Lot_DynamicRangeList.SetItemText(Lot_InsertIndex, 0, strCnt);

// 	m_Lot_DynamicRangeList.SetItemText(Lot_InsertIndex, 1, ((CImageTesterDlg *)m_pMomWnd)->m_modelName);
// 	m_Lot_DynamicRangeList.SetItemText(Lot_InsertIndex, 2, ((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);////////LOT 이름
// 	m_Lot_DynamicRangeList.SetItemText(Lot_InsertIndex, 3, ((CImageTesterDlg *)m_pMomWnd)->strDay + "");
// 	m_Lot_DynamicRangeList.SetItemText(Lot_InsertIndex, 4, ((CImageTesterDlg *)m_pMomWnd)->strTime + "");

	int CopyIndex = m_DynamicRangeList.GetItemCount() - 1;

	int count = 0;
	for (int t = 0; t < Lot_InsertNum; t++){
		if ((t != 2) && (t != 0)){
			m_Lot_DynamicRangeList.SetItemText(Index, t, m_DynamicRangeList.GetItemText(CopyIndex, count));
			count++;

		}
		else if (t == 0){
			count++;
		}
		else{
			m_Lot_DynamicRangeList.SetItemText(Index, t, ((CImageTesterDlg  *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;

}
void CDynamicRange_Option::LOT_EXCEL_SAVE(){

	CString Item[8] = { "LT_SNRBW", "LB_SNRBW", "RT_SNRBW", "RB_SNRBW", "LT_DR", "LB_DR", "RT_DR", "RB_DR" };

	CString Data = "";
	CString str = "";
	str = "***********************DynamicRange [ 단위 : dB ] ***********************\n\n";
	Data += str;
	str.Empty();
	str.Format("SNR_BW Threshold, LT_SNRBW, %0.1f,LB_SNRBW, %0.1f, RT_SNRBW, %0.1f, RB_SNRBW, %0.1f,  \n", DR_RECT[0].d_Thresold, DR_RECT[1].d_Thresold, DR_RECT[2].d_Thresold, DR_RECT[3].d_Thresold);//Threshold
	Data += str;
	str.Empty();
	str.Format("D.R Threshold, LT_DR, %0.1f,LB_DR, %0.1f, RT_DR, %0.1f, RB_DR, %0.1f,  \n", DR_RECT[0].d_Thresold_BW, DR_RECT[1].d_Thresold_BW, DR_RECT[2].d_Thresold_BW, DR_RECT[3].d_Thresold_BW);//d_Thresold_BW
	Data += str;
	str.Empty();
	str = "*************************************************\n";
	Data += str;

	((CImageTesterDlg  *)m_pMomWnd)->LOT_SAVE_EXCEL("DynamicRange", Item, 8, &m_Lot_DynamicRangeList, Data);
}
bool CDynamicRange_Option::LOT_EXCEL_UPLOAD(){
	m_Lot_DynamicRangeList.DeleteAllItems();
	if (((CImageTesterDlg  *)m_pMomWnd)->LOT_EXCEL_UPLOAD("DynamicRange", &m_Lot_DynamicRangeList, 1, &Lot_StartCnt) == TRUE){
		return TRUE;
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;
	}
	return TRUE;
}
void CDynamicRange_Option::Save_parameter(){
	CString str = "";
	CString strTitle = "";

	str.Empty();
	str.Format("%d", m_INFO.ID);
	WritePrivateProfileString(str_model, "ID", str, DR_filename);
	str.Empty();
	str.Format("%s", m_INFO.MODE);
	WritePrivateProfileString(str_model, "MODE", str, DR_filename);
	str.Empty();
	str.Format("%s", m_INFO.NAME);
	WritePrivateProfileString(str_model, "NAME", str, DR_filename);

	str.Empty();
	str.Format("%0.2f", SNRBW);
	WritePrivateProfileString(str_model, "SNRBW", str, DR_filename);


	for (int t = 0; t < 4; t++){

		if (t == 0){
			strTitle.Empty(); strTitle = "LEFT_TOP";
		}
		if (t == 1){
			strTitle.Empty(); strTitle = "LEFT_BOTTOM";
		}
		if (t == 2){
			strTitle.Empty(); strTitle = "RIGHT_TOP";
		}
		if (t == 3){
			strTitle.Empty(); strTitle = "RIGHT_BOTTOM";
		}

		str.Empty();
		str.Format("%d", DR_RECT[t].m_PosX);
		WritePrivateProfileString(str_model, strTitle + "PosX", str, DR_filename);
		str.Empty();
		str.Format("%d", DR_RECT[t].m_PosY);
		WritePrivateProfileString(str_model, strTitle + "PosY", str, DR_filename);
		str.Empty();
		str.Format("%d", DR_RECT[t].m_Left);
		WritePrivateProfileString(str_model, strTitle + "StartX", str, DR_filename);
		str.Empty();
		str.Format("%d", DR_RECT[t].m_Top);
		WritePrivateProfileString(str_model, strTitle + "StartY", str, DR_filename);
		str.Empty();
		str.Format("%d", DR_RECT[t].m_Right);
		WritePrivateProfileString(str_model, strTitle + "ExitX", str, DR_filename);
		str.Empty();
		str.Format("%d", DR_RECT[t].m_Bottom);
		WritePrivateProfileString(str_model, strTitle + "ExitY", str, DR_filename);
		str.Empty();
		str.Format("%d", DR_RECT[t].m_Width);
		WritePrivateProfileString(str_model, strTitle + "Width", str, DR_filename);
		str.Empty();
		str.Format("%d", DR_RECT[t].m_Height);
		WritePrivateProfileString(str_model, strTitle + "Height", str, DR_filename);

		str.Empty();
		str.Format("%d", DR_RECT[t].ENABLE);
		WritePrivateProfileString(str_model, strTitle + "Enable", str, DR_filename);

		
		str.Empty();
		str.Format("%0.1f", DR_RECT[t].d_Thresold);
		WritePrivateProfileString(str_model, strTitle + "Thresold", str, DR_filename);
		str.Empty();
		str.Format("%0.1f", DR_RECT[t].d_Thresold_BW);
		WritePrivateProfileString(str_model, strTitle + "Thresold_BW", str, DR_filename);

	}
	for (int t = 0; t < 3; t++){

		strTitle.Empty(); strTitle.Format("%d_", t + 1);

		str.Empty();
		str.Format("%d", m_Brightness[t]);
		WritePrivateProfileString(str_model, strTitle+"BRIGHTNESS", str, DR_filename);

	}

	UploadList();
}
void CDynamicRange_Option::Load_parameter(){
	CFileFind filefind;
	CString str = "";
	CString strTitle = "";

	int rect_Width = 20;// ((m_CAM_SIZE_WIDTH + m_CAM_SIZE_HEIGHT) / 2) * 0.065;
	int rect_Height = 20; // ((m_CAM_SIZE_WIDTH + m_CAM_SIZE_HEIGHT) / 2) * 0.065;

	if ((GetPrivateProfileCString(str_model, "NAME", DR_filename) != m_INFO.NAME) || (GetPrivateProfileCString(str_model, "MODE", DR_filename) != m_INFO.MODE)){//ini파일이 없으면 파일을 생성한다.

		SNRBW = 20;

		//left top
		DR_RECT[0].m_PosX = m_CAM_SIZE_WIDTH*0.222;;
		DR_RECT[0].m_PosY = m_CAM_SIZE_HEIGHT*0.2916;

		//left bottom
		DR_RECT[1].m_PosX = m_CAM_SIZE_WIDTH*0.222;;
		DR_RECT[1].m_PosY = m_CAM_SIZE_HEIGHT*0.7083;

		//right top
		DR_RECT[2].m_PosX = m_CAM_SIZE_WIDTH*0.777;
		DR_RECT[2].m_PosY = m_CAM_SIZE_HEIGHT*0.2916;

		//right bottom
		DR_RECT[3].m_PosX = m_CAM_SIZE_WIDTH*0.777;
		DR_RECT[3].m_PosY = m_CAM_SIZE_HEIGHT*0.7083;
		 
		 
		for(int t=0; t<4; t++){
		 			
		 	DR_RECT[t].m_Width=rect_Width;
		 	DR_RECT[t].m_Height=rect_Height;
		 	DR_RECT[t].EX_RECT_SET(DR_RECT[t].m_PosX,DR_RECT[t].m_PosY,DR_RECT[t].m_Width,DR_RECT[t].m_Height);//////C
		 	DR_RECT[t].ENABLE = 1;
			DR_RECT[t].d_Thresold = 20.0;
			DR_RECT[t].d_Thresold_BW = 20.0;
		 
		}
		for (int t = 0; t < 3; t++){
				m_Brightness[t] = 500 * (3 - t);
		}

		Save_parameter();
		UpdateData(FALSE);
	}
	else{

		SNRBW = GetPrivateProfileDouble(str_model, "SNRBW", -1, DR_filename);
		if (SNRBW == -1){
			SNRBW = 20;
			str.Empty();
			str.Format("%0.2f", SNRBW);
			WritePrivateProfileString(str_model, "SNRBW", str, DR_filename);

		}

		for (int t = 0; t < 4; t++){

			if (t == 0){
				strTitle.Empty(); strTitle = "LEFT_TOP";
			}
			if (t == 1){
				strTitle.Empty(); strTitle = "LEFT_BOTTOM";
			}
			if (t == 2){
				strTitle.Empty(); strTitle = "RIGHT_TOP";
			}
			if (t == 3){
				strTitle.Empty(); strTitle = "RIGHT_BOTTOM";
			}

			DR_RECT[t].m_PosX = GetPrivateProfileInt(str_model, strTitle + "PosX", -1, DR_filename);

			if (DR_RECT[t].m_PosX == -1){

				//left top
				DR_RECT[0].m_PosX = m_CAM_SIZE_WIDTH*0.222;;
				DR_RECT[0].m_PosY = m_CAM_SIZE_HEIGHT*0.2916;

				//left bottom
				DR_RECT[1].m_PosX = m_CAM_SIZE_WIDTH*0.222;;
				DR_RECT[1].m_PosY = m_CAM_SIZE_HEIGHT*0.7083;

				//right top
				DR_RECT[2].m_PosX = m_CAM_SIZE_WIDTH*0.777;
				DR_RECT[2].m_PosY = m_CAM_SIZE_HEIGHT*0.2916;

				//right bottom
				DR_RECT[3].m_PosX = m_CAM_SIZE_WIDTH*0.777;
				DR_RECT[3].m_PosY = m_CAM_SIZE_HEIGHT*0.7083;


				for (int t = 0; t < 4; t++){

					DR_RECT[t].m_Width = rect_Width;
					DR_RECT[t].m_Height = rect_Height;
					DR_RECT[t].EX_RECT_SET(DR_RECT[t].m_PosX, DR_RECT[t].m_PosY, DR_RECT[t].m_Width, DR_RECT[t].m_Height);//////C
					DR_RECT[t].ENABLE = 1;
					DR_RECT[t].d_Thresold = 20.0;
					DR_RECT[t].d_Thresold_BW =20.0;
				}
				break;
			}
			DR_RECT[t].m_PosY = GetPrivateProfileInt(str_model, strTitle + "PosY", -1, DR_filename);
			DR_RECT[t].m_Left = GetPrivateProfileInt(str_model, strTitle + "StartX", -1, DR_filename);
			DR_RECT[t].m_Top = GetPrivateProfileInt(str_model, strTitle + "StartY", -1, DR_filename);
			DR_RECT[t].m_Right = GetPrivateProfileInt(str_model, strTitle + "ExitX", -1, DR_filename);
			DR_RECT[t].m_Bottom = GetPrivateProfileInt(str_model, strTitle + "ExitY", -1, DR_filename);
			DR_RECT[t].m_Width = GetPrivateProfileInt(str_model, strTitle + "Width", -1, DR_filename);
			DR_RECT[t].m_Height = GetPrivateProfileInt(str_model, strTitle + "Height", -1, DR_filename);
			DR_RECT[t].ENABLE = GetPrivateProfileInt(str_model, strTitle + "Enable", -1, DR_filename);
			DR_RECT[t].d_Thresold = GetPrivateProfileDouble(str_model, strTitle + "Thresold", -1, DR_filename);
			DR_RECT[t].d_Thresold_BW = GetPrivateProfileDouble(str_model, strTitle + "Thresold_BW", -1, DR_filename);

		}
		for (int t = 0; t < 3; t++){

			strTitle.Empty(); strTitle.Format("%d_", t+1);


			m_Brightness[t] = GetPrivateProfileInt(str_model, strTitle+ "BRIGHTNESS", -1, DR_filename);
			if (m_Brightness[t] == -1){
				m_Brightness[t] = 500 * (3 - t);
				str.Empty();
				str.Format("%d", m_Brightness[t]);
				WritePrivateProfileString(str_model, strTitle + "BRIGHTNESS", str, DR_filename);
			}
		}
	}

	UploadList();
	UpdateData(FALSE);
}
void CDynamicRange_Option::UploadList(){
	CString str = "";
	int InIndex = 0;
	DR_DATALIST.DeleteAllItems();

	//for (int t = 0; t < 6; t++){
	for (int t = 0; t < 4; t++){
		InIndex = DR_DATALIST.InsertItem(t, "", 0);


		if (t == 0){
			str.Empty(); str = "LEFT_TOP";
		}
		if (t == 1){
			str.Empty(); str = "LEFT_BOTTOM";
		}
		if (t == 2){
			str.Empty(); str = "RIGHT_TOP";
		}
		if (t == 3){
			str.Empty(); str = "RIGHT_BOTTOM";
		}

		DR_DATALIST.SetItemText(InIndex, 0, str);
 
 	 	str.Empty();str.Format("%d", DR_RECT[t].m_PosX);
 	 	DR_DATALIST.SetItemText(InIndex,1,str);
 	 	str.Empty();str.Format("%d", DR_RECT[t].m_PosY);
 	 	DR_DATALIST.SetItemText(InIndex,2,str);
 	 
 	 	str.Empty();
 	 	str.Format("%d", DR_RECT[t].m_Left);
 	 	DR_DATALIST.SetItemText(InIndex,3,str);
 	 	str.Empty();
 	 	str.Format("%d", DR_RECT[t].m_Top);
 	 	DR_DATALIST.SetItemText(InIndex,4,str);
 	 
 	 	str.Empty();
 	 	str.Format("%d", DR_RECT[t].m_Right+1);
 	 	DR_DATALIST.SetItemText(InIndex,5,str);
 	 	str.Empty();
 	 	str.Format("%d", DR_RECT[t].m_Bottom+1);
 	 	DR_DATALIST.SetItemText(InIndex,6,str);
 	 
 	 	str.Empty();
 	 	str.Format("%d", DR_RECT[t].m_Width);
 	 	DR_DATALIST.SetItemText(InIndex,7,str);
 	 	str.Empty();
 	 	str.Format("%d",DR_RECT[t].m_Height);
 	 	DR_DATALIST.SetItemText(InIndex,8,str);
		str.Empty();
		str.Format("%0.1f", DR_RECT[t].d_Thresold);
		DR_DATALIST.SetItemText(InIndex, 9, str);
		str.Empty();
		str.Format("%0.1f", DR_RECT[t].d_Thresold_BW);
		DR_DATALIST.SetItemText(InIndex, 10, str);

		if (DR_RECT[t].ENABLE == TRUE){
			DR_DATALIST.SetItemState(t, 0x2000, LVIS_STATEIMAGEMASK);
		}
		else{
			DR_DATALIST.SetItemState(t, 0x1000, LVIS_STATEIMAGEMASK);
		}
	}
}

void CDynamicRange_Option::OnNMClickListDynamicrangelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	iSavedItem = pNMItemActivate->iItem;
	iSavedSubitem = pNMItemActivate->iSubItem;

	if (iSavedSubitem == 0){
		SetTimer(150, 5, NULL); //InitProgram을 불러들이는데 쓰인다. 
		UpdateData(FALSE);
	}
	*pResult = 0;
}


void CDynamicRange_Option::OnNMCustomdrawListDynamicrangelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	COLORREF text_color = 0;
	COLORREF bg_color = RGB(255, 255, 255);
	/////////////////////////////////////////////////////////////default 컬러

	LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	//	lplvcd->iPartId
	switch (lplvcd->nmcd.dwDrawStage){
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;
		return;

		// 배경 혹은 텍스트를 수정한다.
	case CDDS_ITEMPREPAINT:
		// 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
		/* if(lplvcd->nmcd.dwItemSpec == 0) text_color = RGB(255, 0, 0);
		else if(lplvcd->nmcd.dwItemSpec == 1) text_color = RGB(0, 255, 0);
		else text_color = RGB(0, 0, 255);
		lplvcd->clrText = text_color;*/
		*pResult = CDRF_NOTIFYITEMDRAW;
		return;

		// 서브 아이템의 배경 혹은 텍스트를 수정한다.
	case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
		if (lplvcd->iSubItem != 0){
			// 1번째 행이라면...

			if (lplvcd->nmcd.dwItemSpec >= 0 || lplvcd->nmcd.dwItemSpec <9){

				text_color = RGB(0, 0, 0);
				bg_color = RGB(255, 255, 255);

				if (ChangeCheck == 1){
					if (lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 9){
						for (int t = 0; t<9; t++){
							if (lplvcd->nmcd.dwItemSpec == ChangeItem[t]){
								text_color = RGB(0, 0, 0);
								bg_color = RGB(250, 230, 200);
							}
						}
					}
				}
			}

			lplvcd->clrText = text_color;
			lplvcd->clrTextBk = bg_color;

		}
		*pResult = CDRF_NEWFONT;
		return;
	}
	//*pResult = 0;
}


void CDynamicRange_Option::OnNMDblclkListDynamicrangelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		return;
	}

	if (/*pNMITEM->iSubItem == 0 ||*/ pNMITEM->iSubItem == -1 || pNMITEM->iItem == -1)
		return;
	iSavedItem = pNMITEM->iItem;
	iSavedSubitem = pNMITEM->iSubItem;

	if (iSavedSubitem == 0){
		SetTimer(150, 5, NULL); //InitProgram을 불러들이는데 쓰인다. 
		UpdateData(FALSE);
	}

	if (pNMITEM->iItem != -1)
	{
		if (pNMITEM->iSubItem != 0)
		{
			DR_DATALIST.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			DR_DATALIST.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_DR_MOD))->SetWindowText(DR_DATALIST.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_DR_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW);
			((CEdit *)GetDlgItem(IDC_EDIT_DR_MOD))->SetFocus();

		}
		else{
			SetTimer(160, 5, NULL); //InitProgram을 불러들이는데 쓰인다.
		}
	}
	List_COLOR_Change();
	UpdateData(FALSE);

	*pResult = 0;
}


void CDynamicRange_Option::OnEnChangeEditDrMod()
{
	int num = iSavedItem;
	if ((DR_RECT[num].m_PosX >= 0) && (DR_RECT[num].m_PosX < m_CAM_SIZE_WIDTH) &&
		(DR_RECT[num].m_PosY >= 0) && (DR_RECT[num].m_PosY < m_CAM_SIZE_HEIGHT) &&
		(DR_RECT[num].m_Left >= 0) && (DR_RECT[num].m_Left<m_CAM_SIZE_WIDTH) &&
		(DR_RECT[num].m_Top >= 0) && (DR_RECT[num].m_Top<m_CAM_SIZE_HEIGHT) &&
		(DR_RECT[num].m_Right >= 0) && (DR_RECT[num].m_Right<m_CAM_SIZE_WIDTH) &&
		(DR_RECT[num].m_Bottom >= 0) && (DR_RECT[num].m_Bottom<m_CAM_SIZE_HEIGHT) &&
		(DR_RECT[num].m_Width >= 0) && (DR_RECT[num].m_Width<m_CAM_SIZE_WIDTH) &&
		(DR_RECT[num].m_Height >= 0) && (DR_RECT[num].m_Height<m_CAM_SIZE_HEIGHT)){
		((CButton *)GetDlgItem(IDC_BUTTON_DR_SAVEZONE))->EnableWindow(1);

	}
	else{
		((CButton *)GetDlgItem(IDC_BUTTON_DR_SAVEZONE))->EnableWindow(0);
	}
}


void CDynamicRange_Option::OnEnKillfocusEditDrMod()
{
	CString str = "";
	ChangeCheck = 1;
	if ((iSavedItem != -1) && (iSavedSubitem != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_DR_MOD, str));
		List_COLOR_Change();
		if (DR_DATALIST.GetItemText(iSavedItem, iSavedSubitem) != str){
			Change_DATA();
			UploadList();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_DR_MOD))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_DR_MOD))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
	iSavedItem = -1;
	iSavedSubitem = -1;
	((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
}
void CDynamicRange_Option::Change_DATA(){

	CString str;

	((CEdit *)GetDlgItemText(IDC_EDIT_DR_MOD, str));
	DR_DATALIST.SetItemText(iSavedItem, iSavedSubitem, str);

	int num = _ttoi(str);
	double d_num = _ttof(str);
	if (num < 0){
		num = 0;
	}
	if (d_num < 0){
		d_num = 0.0;
	}

	if (iSavedSubitem == 1){
		DR_RECT[iSavedItem].m_PosX = num;

	}
	if (iSavedSubitem == 2){
		DR_RECT[iSavedItem].m_PosY = num;
	}
	if (iSavedSubitem == 3){

		DR_RECT[iSavedItem].m_Left = num;
		DR_RECT[iSavedItem].m_PosX = (DR_RECT[iSavedItem].m_Right + 1 + DR_RECT[iSavedItem].m_Left) / 2;
		DR_RECT[iSavedItem].m_Width = DR_RECT[iSavedItem].m_Right + 1 - DR_RECT[iSavedItem].m_Left;

	}
	if (iSavedSubitem == 4){

		DR_RECT[iSavedItem].m_Top = num;
		DR_RECT[iSavedItem].m_PosY=(DR_RECT[iSavedItem].m_Top +DR_RECT[iSavedItem].m_Bottom+1)/2;
		DR_RECT[iSavedItem].m_Height=DR_RECT[iSavedItem].m_Bottom+1 -DR_RECT[iSavedItem].m_Top;

	}
	if (iSavedSubitem == 5){

		if (num == 0){
			DR_RECT[iSavedItem].m_Right = 0;
		}
		else{
			DR_RECT[iSavedItem].m_Right = num;
			DR_RECT[iSavedItem].m_PosX = (DR_RECT[iSavedItem].m_Right + DR_RECT[iSavedItem].m_Left + 1) / 2;
			DR_RECT[iSavedItem].m_Width = DR_RECT[iSavedItem].m_Right + 1 - DR_RECT[iSavedItem].m_Left;
		}
	}
	if (iSavedSubitem == 6){

		if(num ==0){
			DR_RECT[iSavedItem].m_Bottom =0;
		}
		else{
			DR_RECT[iSavedItem].m_Bottom = num;
			DR_RECT[iSavedItem].m_PosY=(DR_RECT[iSavedItem].m_Top +DR_RECT[iSavedItem].m_Bottom+1)/2;
			DR_RECT[iSavedItem].m_Height=DR_RECT[iSavedItem].m_Bottom+1 -DR_RECT[iSavedItem].m_Top;
		}
	}

 
 	 	if(iSavedSubitem ==7){
 	 		DR_RECT[iSavedItem].m_Width = num;
 	 	}
 	 	if(iSavedSubitem ==8){
 	 		DR_RECT[iSavedItem].m_Height = num;
 	 	}
		if (iSavedSubitem == 9){
			DR_RECT[iSavedItem].d_Thresold = d_num;
		}
		if (iSavedSubitem == 10){
			DR_RECT[iSavedItem].d_Thresold_BW = d_num;
		}
 	 	DR_RECT[iSavedItem].EX_RECT_SET(DR_RECT[iSavedItem].m_PosX,DR_RECT[iSavedItem].m_PosY,DR_RECT[iSavedItem].m_Width,DR_RECT[iSavedItem].m_Height);
 	 
 	 
 	 	if(DR_RECT[iSavedItem].m_Left < 0){//////////////////////수정
 	 		DR_RECT[iSavedItem].m_Left = 0;
 	 		DR_RECT[iSavedItem].m_Width = DR_RECT[iSavedItem].m_Right+1 -DR_RECT[iSavedItem].m_Left;
 	 		DR_RECT[iSavedItem].EX_RECT_SET(DR_RECT[iSavedItem].m_PosX,DR_RECT[iSavedItem].m_PosY,DR_RECT[iSavedItem].m_Width,DR_RECT[iSavedItem].m_Height);
 	 	}
 	 	if(DR_RECT[iSavedItem].m_Right >m_CAM_SIZE_WIDTH){
 	 		DR_RECT[iSavedItem].m_Right = m_CAM_SIZE_WIDTH;
 	 		DR_RECT[iSavedItem].m_Width = DR_RECT[iSavedItem].m_Right+1 -DR_RECT[iSavedItem].m_Left;
 	 		DR_RECT[iSavedItem].EX_RECT_SET(DR_RECT[iSavedItem].m_PosX,DR_RECT[iSavedItem].m_PosY,DR_RECT[iSavedItem].m_Width,DR_RECT[iSavedItem].m_Height);
 	 	}
 	 	if(DR_RECT[iSavedItem].m_Top< 0){
 	 		DR_RECT[iSavedItem].m_Top = 0;
 	 		DR_RECT[iSavedItem].m_Height = DR_RECT[iSavedItem].m_Bottom+1 -DR_RECT[iSavedItem].m_Top;
 	 		DR_RECT[iSavedItem].EX_RECT_SET(DR_RECT[iSavedItem].m_PosX,DR_RECT[iSavedItem].m_PosY,DR_RECT[iSavedItem].m_Width,DR_RECT[iSavedItem].m_Height);
 	 	}
 	 	if(DR_RECT[iSavedItem].m_Height >m_CAM_SIZE_HEIGHT){
 	 		DR_RECT[iSavedItem].m_Height = m_CAM_SIZE_HEIGHT;
 	 		DR_RECT[iSavedItem].m_Width = DR_RECT[iSavedItem].m_Bottom+1 -DR_RECT[iSavedItem].m_Top;
 	 		DR_RECT[iSavedItem].EX_RECT_SET(DR_RECT[iSavedItem].m_PosX,DR_RECT[iSavedItem].m_PosY,DR_RECT[iSavedItem].m_Width,DR_RECT[iSavedItem].m_Height);
 	 	}
 	 
 	 	if(DR_RECT[iSavedItem].m_Height<= 0){
 	 		DR_RECT[iSavedItem].m_Height = 1;
 	 		DR_RECT[iSavedItem].EX_RECT_SET(DR_RECT[iSavedItem].m_PosX,DR_RECT[iSavedItem].m_PosY,DR_RECT[iSavedItem].m_Width,DR_RECT[iSavedItem].m_Height);
 	 	}
 	 	if(DR_RECT[iSavedItem].m_Height >=m_CAM_SIZE_HEIGHT){
 	 		DR_RECT[iSavedItem].m_Height = m_CAM_SIZE_HEIGHT;
 	 		DR_RECT[iSavedItem].EX_RECT_SET(DR_RECT[iSavedItem].m_PosX,DR_RECT[iSavedItem].m_PosY,DR_RECT[iSavedItem].m_Width,DR_RECT[iSavedItem].m_Height);
 	 	}
 	 
 	 	if(DR_RECT[iSavedItem].m_Width <= 0){
 	 		DR_RECT[iSavedItem].m_Width = 1;
 	 		DR_RECT[iSavedItem].EX_RECT_SET(DR_RECT[iSavedItem].m_PosX,DR_RECT[iSavedItem].m_PosY,DR_RECT[iSavedItem].m_Width,DR_RECT[iSavedItem].m_Height);
 	 	}
 	 	if(DR_RECT[iSavedItem].m_Width >=m_CAM_SIZE_WIDTH){
 	 		DR_RECT[iSavedItem].m_Width = m_CAM_SIZE_WIDTH;
 	 		DR_RECT[iSavedItem].EX_RECT_SET(DR_RECT[iSavedItem].m_PosX,DR_RECT[iSavedItem].m_PosY,DR_RECT[iSavedItem].m_Width,DR_RECT[iSavedItem].m_Height);
 	 	}
 	 
 	 
 	 
 	 	DR_RECT[iSavedItem].EX_RECT_SET(DR_RECT[iSavedItem].m_PosX,DR_RECT[iSavedItem].m_PosY,DR_RECT[iSavedItem].m_Width,DR_RECT[iSavedItem].m_Height);


	CString data = "";

	if (iSavedSubitem == 1){
		data.Format("%d", DR_RECT[iSavedItem].m_PosX);
	}
	else if (iSavedSubitem == 2){
		data.Format("%d", DR_RECT[iSavedItem].m_PosY);
	}
	else if (iSavedSubitem == 3){
		data.Format("%d", DR_RECT[iSavedItem].m_Left);
	}
	else if (iSavedSubitem == 4){
		data.Format("%d", DR_RECT[iSavedItem].m_Top);
	}
	else if (iSavedSubitem == 5){
		data.Format("%d", DR_RECT[iSavedItem].m_Right);
	}
	else if (iSavedSubitem == 6){
		data.Format("%d", DR_RECT[iSavedItem].m_Bottom);
	}
	else if (iSavedSubitem == 7){
		data.Format("%d", DR_RECT[iSavedItem].m_Width);
	}
	else if (iSavedSubitem == 8){
		data.Format("%d", DR_RECT[iSavedItem].m_Height);
	}
	else if (iSavedSubitem == 9){
		data.Format("%0.1f", DR_RECT[iSavedItem].d_Thresold);
	}
	else if (iSavedSubitem == 10){
		data.Format("%0.1f", DR_RECT[iSavedItem].d_Thresold_BW);
	}

	((CEdit *)GetDlgItem(IDC_EDIT_DR_MOD))->SetWindowText(data);

}
void CDynamicRange_Option::List_COLOR_Change(){
	bool FLAG = TRUE;
	int Check = 0;


	for (int t = 0; t < 9; t++){
		if (ChangeItem[t] == 0)Check++;
		if (ChangeItem[t] == 1)Check++;
		if (ChangeItem[t] == 2)Check++;
		if (ChangeItem[t] == 3)Check++;
		if (ChangeItem[t] == 4)Check++;
		if (ChangeItem[t] == 5)Check++;
		if (ChangeItem[t] == 6)Check++;
		if (ChangeItem[t] == 7)Check++;
		if (ChangeItem[t] == 8)Check++;
	}
	if (Check != 8){
		FLAG = FALSE;
		ChangeItem[changecount] = iSavedItem;
	}

	if (!FLAG){
		for (int t = 0; t < changecount + 1; t++){
			for (int k = 0; k < changecount + 1; k++){

				if (ChangeItem[t] == ChangeItem[k]){
					if (t == k){ break; }
					ChangeItem[k] = -1;
				}


			}
		}
		//----------------데이터 정렬
		int temp = 0;

		for (int i = 0; i < 9; i++)
		{
			for (int j = i + 1; j < 9; j++)
			{
				if (ChangeItem[i] < ChangeItem[j])  // 내림차순
				{
					temp = ChangeItem[i];
					ChangeItem[i] = ChangeItem[j];
					ChangeItem[j] = temp;
				}
			}
		}
		//--------------------------------
		for (int t = 0; t < 9; t++){

			if (ChangeItem[t] == -1){
				changecount = t;
				break;
			}
		}
	}
	for (int t = 0; t < 9; t++){
		DR_DATALIST.Update(t);
	}

}

void CDynamicRange_Option::OnBnClickedButtonDrLoad()
{
	Load_parameter();
	UpdateData(FALSE);
	UploadList();
}


void CDynamicRange_Option::OnBnClickedButtonDrSavezone()
{
	UpdateData(TRUE);
	Save_parameter();
	UpdateData(FALSE);

	ChangeCheck = 0;
	for (int t = 0; t < 9; t++){
		ChangeItem[t] = -1;
	}
	for (int t = 0; t < 9; t++){
		DR_DATALIST.Update(t);
	}
}


BOOL CDynamicRange_Option::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	int znum = zDelta / 120;
	CWnd *focusid;
	focusid = GetFocus();
	bool FLAG = 0;
	if (znum > 0){
		FLAG = 1;
	}
	else if (znum < 0){
		FLAG = 0;
	}
	else if (znum == 0){
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	}
	int casenum = focusid->GetDlgCtrlID();
	switch (casenum){
		// 	case IDC_PN_PosX:
		// 		EditWheelIntSet(IDC_PN_PosX, znum);
		// 		OnEnChangePnPosx();
		// 		break;
		// 	case IDC_PN_PosY:
		// 		EditWheelIntSet(IDC_PN_PosY, znum);
		// 		OnEnChangePnPosy();
		// 		break;
		// 	case IDC_PN_Dis_W:
		// 		EditWheelIntSet(IDC_PN_Dis_W, znum);
		// 		OnEnChangePnDisW();
		// 		break;
		// 	case IDC_PN_Dis_H:
		// 		EditWheelIntSet(IDC_PN_Dis_H, znum);
		// 		OnEnChangePnDisH();
		// 		break;
		// 	case  IDC_PN_Width:
		// 		EditWheelIntSet(IDC_PN_Width, znum);
		// 		OnEnChangePnWidth();
		// 		break;
		// 	case IDC_PN_Height:
		// 		EditWheelIntSet(IDC_PN_Height, znum);
		// 		OnEnChangePnHeight();
		// 		break;

	case IDC_EDIT_DR_MOD:
		if (/*(iSavedSubitem != 9) &&*/ (iSavedSubitem != 11)){
			if (FLAG == 1){
				if ((Change_DATA_CHECK(1) == TRUE) /*|| (znum ==-1)*/){
					EditWheelIntSet(IDC_EDIT_DR_MOD, znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditDrMod();
				}
			}
			if (FLAG == 0){
				if ((Change_DATA_CHECK(0) == TRUE) /*|| (znum == 1)*/){
					EditWheelIntSet(IDC_EDIT_DR_MOD, znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditDrMod();
				}
			}

		}

		break;
	}

	return TRUE;
}


void CDynamicRange_Option::OnBnClickedButtonTest()
{
	((CButton *)GetDlgItem(IDC_BUTTON_TEST_))->EnableWindow(0);
	((CImageTesterDlg  *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	((CButton *)GetDlgItem(IDC_BUTTON_TEST_))->EnableWindow(1);
}
void CDynamicRange_Option::SignalnNoiseGen(WORD *GRAYScanBuf, int NUM, int ROINUM){

	CvScalar MeanValue, StdValue;
	bool m_Success = false;
	int count = 0;

	int startx = 0;
	int starty = 0;
	int endx = 0;
	int endy = 0;
	int ImgWidth = 0;
	int ImgHeight = 0;

	int data = 0;
	int Signal = 0;
	double Noise = 0;
	unsigned int Val = 0;



	startx = DR_RECT[ROINUM].m_Left;
	starty = DR_RECT[ROINUM].m_Top;
	endx = DR_RECT[ROINUM].m_Right + 1;
	endy = DR_RECT[ROINUM].m_Bottom + 1;
	ImgWidth = DR_RECT[ROINUM].m_Width;
	ImgHeight = DR_RECT[ROINUM].m_Height;

	for (int y = starty; y < endy; y++)
	{
		for (int x = startx; x < endx; x++)
		{
			data += GRAYScanBuf[y*CAM_IMAGE_WIDTH + x];
		}
	}
	Signal = data / (ImgWidth*ImgHeight);
	for (int y = starty; y < endy; y++)
	{
		for (int x = startx; x < endx; x++)
		{
			data = abs(Signal - GRAYScanBuf[y*CAM_IMAGE_WIDTH + x]);
			Val += data*data;
		}
	}
	Noise = sqrt(Val / (ImgWidth*ImgHeight));

	DR_RECT[ROINUM].Signal[NUM] = Signal;
	DR_RECT[ROINUM].Noise[NUM] = Noise;

}
bool CDynamicRange_Option::SNRGen_16bit(WORD *GRAYScanBuf, int NUM){

	CvScalar MeanValue, StdValue;
	bool m_Success = false;
// 	int count = 0;
// 
// 	int startx = 0;
// 	int starty = 0;
// 	int endx = 0;
// 	int endy = 0;
// 	int ImgWidth = 0;
// 	int ImgHeight = 0;
// 
// 	int data = 0;
// 	int Signal[3] = { 0, };
// 	double Noise[3] = { 0.0, };
// 	unsigned int Val = 0;
// 
// 	for (int num = 0; num < 3; num++){
// 
// 		startx = DR_RECT[num + NUM].m_Left;
// 		starty = DR_RECT[num + NUM].m_Top;
// 		endx = DR_RECT[num + NUM].m_Right + 1;
// 		endy = DR_RECT[num + NUM].m_Bottom + 1;
// 		ImgWidth = DR_RECT[num + NUM].m_Width;
// 		ImgHeight = DR_RECT[num + NUM].m_Height;
// 
// 		for (int y = starty; y < endy; y++)
// 		{
// 			for (int x = startx; x < endx; x++)
// 			{
// 				data += GRAYScanBuf[y*CAM_IMAGE_WIDTH + x];
// 			}
// 		}
// 		Signal[num] = data / (ImgWidth*ImgHeight);
// 		for (int y = starty; y < endy; y++)
// 		{
// 			for (int x = startx; x < endx; x++)
// 			{
// 				data = abs(Signal[num] - GRAYScanBuf[y*CAM_IMAGE_WIDTH + x]);
// 				Val += data*data;
// 			}
// 		}
// 		Noise[num] = sqrt(Val / (ImgWidth*ImgHeight));
// 
// 		DR_RECT[num + NUM].Signal = Signal[num];
// 		DR_RECT[num + NUM].Noise = Noise[num];
// 	}
// 
// 	if ((Signal[0]>Signal[2]) && (Noise[1] != 0))
// 		DR_RECT[NUM].m_result = 20 * log10l((Signal[0] - Signal[2]) / Noise[1]);
// 	else
// 		DR_RECT[NUM].m_result = 0; // 정상적인 TEST 환경이 아님
// 
// 	if (DR_RECT[NUM].m_result >= SNRBW){
// 		m_Success = TRUE;
// 		DR_RECT[NUM].m_Success = TRUE;
// 
// 	}
// 	else{
// 		m_Success = FALSE;
// 		DR_RECT[NUM].m_Success = FALSE;
// 
// 	}

	return m_Success;

}

void CDynamicRange_Option::OnBnClickedButtonDrSave()
{
	UpdateData(TRUE);


	CString str = "";
	CString strTitle = "";
	// 	str.Empty();
// 	str.Format("%0.2f", SNRBW);
// 	WritePrivateProfileString(str_model, "SNRBW", str, DR_filename);

	for (int t = 0; t < 3; t++){

		strTitle.Empty(); strTitle.Format("%d_", t + 1);

		str.Empty();
		str.Format("%d", m_Brightness[t]);
		WritePrivateProfileString(str_model, strTitle +"BRIGHTNESS", str, DR_filename);

	}

	UpdateData(FALSE);
	((CButton *)GetDlgItem(IDC_BUTTON_DR_SAVE))->EnableWindow(0);
}


void CDynamicRange_Option::OnBnClickedButtonSetdrzone()
{
	ChangeCheck = 1;
	UpdateData(TRUE);

	int rect_Width = 20;// ((m_CAM_SIZE_WIDTH + m_CAM_SIZE_HEIGHT) / 2) * 0.065;
	int rect_Height = 20;//((m_CAM_SIZE_WIDTH + m_CAM_SIZE_HEIGHT) / 2) * 0.065;

	//Top White
	DR_RECT[0].m_PosX = 100;
	DR_RECT[0].m_PosY = m_CAM_SIZE_HEIGHT / 6;

	//Top Gray
	DR_RECT[1].m_PosX = 130;
	DR_RECT[1].m_PosY = m_CAM_SIZE_HEIGHT / 6;

	//Top Black
	DR_RECT[2].m_PosX = 160;
	DR_RECT[2].m_PosY = m_CAM_SIZE_HEIGHT / 6;

	//Bottom White
	DR_RECT[3].m_PosX = m_CAM_SIZE_WIDTH - 160;
	DR_RECT[3].m_PosY = (m_CAM_SIZE_HEIGHT * 5) / 6;

	//Bottom Gray
	DR_RECT[4].m_PosX = m_CAM_SIZE_WIDTH - 130;
	DR_RECT[4].m_PosY = (m_CAM_SIZE_HEIGHT * 5) / 6;

	//Bottom Black
	DR_RECT[5].m_PosX = m_CAM_SIZE_WIDTH - 100;
	DR_RECT[5].m_PosY = (m_CAM_SIZE_HEIGHT * 5) / 6;

	for (int t = 0; t < 6; t++){

		DR_RECT[t].m_Width = rect_Width;
		DR_RECT[t].m_Height = rect_Height;
		DR_RECT[t].EX_RECT_SET(DR_RECT[t].m_PosX, DR_RECT[t].m_PosY, DR_RECT[t].m_Width, DR_RECT[t].m_Height);//////C
		DR_RECT[t].ENABLE = 1;

	}

	UpdateData(FALSE);

	UploadList();//리스트컨트롤의 입력


	for (int t = 0; t < 9; t++){
		ChangeItem[t] = t;
	}

	for (int t = 0; t < 9; t++){
		DR_DATALIST.Update(t);
	}

	((CButton *)GetDlgItem(IDC_BUTTON_SETDRZone))->EnableWindow(1);
}


void CDynamicRange_Option::OnEnChangeEditBrightness()
{
	UpdateData(TRUE);
}


void CDynamicRange_Option::OnEnChangeEditBrightness2()
{
	UpdateData(TRUE);
}


void CDynamicRange_Option::OnEnChangeEditBrightness3()
{
	UpdateData(TRUE);
}
