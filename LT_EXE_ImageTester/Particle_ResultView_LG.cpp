// Particle_ResultView_LG.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Particle_ResultView_LG.h"


// CParticle_ResultView_LG 대화 상자입니다.

IMPLEMENT_DYNAMIC(CParticle_ResultView_LG, CDialog)

CParticle_ResultView_LG::CParticle_ResultView_LG(CWnd* pParent /*=NULL*/)
	: CDialog(CParticle_ResultView_LG::IDD, pParent)
{

}

CParticle_ResultView_LG::~CParticle_ResultView_LG()
{
}

void CParticle_ResultView_LG::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CParticle_ResultView_LG, CDialog)
	ON_WM_CTLCOLOR()
	ON_EN_SETFOCUS(IDC_EDIT_PARTICLETXT, &CParticle_ResultView_LG::OnEnSetfocusEditParticletxt)
	ON_EN_SETFOCUS(IDC_EDIT_PARTICLENUM, &CParticle_ResultView_LG::OnEnSetfocusEditParticlenum)
	ON_EN_SETFOCUS(IDC_EDIT_PARTICLERESULT, &CParticle_ResultView_LG::OnEnSetfocusEditParticleresult)
	ON_EN_SETFOCUS(IDC_EDIT_PARTICLETXT2, &CParticle_ResultView_LG::OnEnSetfocusEditParticletxt2)
	ON_EN_SETFOCUS(IDC_EDIT_PARTICLENUM2, &CParticle_ResultView_LG::OnEnSetfocusEditParticlenum2)
	ON_EN_SETFOCUS(IDC_EDIT_PARTICLERESULT2, &CParticle_ResultView_LG::OnEnSetfocusEditParticleresult2)
	ON_EN_SETFOCUS(IDC_EDIT_PARTICLETXT3, &CParticle_ResultView_LG::OnEnSetfocusEditParticletxt3)
	ON_EN_SETFOCUS(IDC_EDIT_PARTICLENUM3, &CParticle_ResultView_LG::OnEnSetfocusEditParticlenum3)
	ON_EN_SETFOCUS(IDC_EDIT_PARTICLERESULT3, &CParticle_ResultView_LG::OnEnSetfocusEditParticleresult3)
END_MESSAGE_MAP()


// CParticle_ResultView_LG 메시지 처리기입니다.

void CParticle_ResultView_LG::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CParticle_ResultView_LG::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
BOOL CParticle_ResultView_LG::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_EDIT_PARTICLETXT,&ft1,40,20);
	GetDlgItem(IDC_EDIT_PARTICLERESULT)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_PARTICLENUM)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_PARTICLERESULT2)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_PARTICLENUM2)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_PARTICLETXT2)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_PARTICLENUM3)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_PARTICLERESULT3)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_PARTICLETXT3)->SetFont(&ft1);
	Initstat();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

HBRUSH CParticle_ResultView_LG::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_PARTICLETXT){
			pDC->SetTextColor(RGB(255, 255, 255));

			pDC->SetBkColor(RGB(86,86,86));
	}


	if(pWnd->GetDlgCtrlID() == IDC_EDIT_PARTICLENUM){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255,255,255));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_PARTICLERESULT){
		pDC->SetTextColor(txcol_TVal);
		pDC->SetBkColor(bkcol_TVal);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_PARTICLETXT2){
			pDC->SetTextColor(RGB(255, 255, 255));

			pDC->SetBkColor(RGB(86,86,86));
	}


	if(pWnd->GetDlgCtrlID() == IDC_EDIT_PARTICLENUM2){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255,255,255));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_PARTICLERESULT2){
		pDC->SetTextColor(txcol_TVal2);
		pDC->SetBkColor(bkcol_TVal2);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_PARTICLETXT3){
			pDC->SetTextColor(RGB(255, 255, 255));

			pDC->SetBkColor(RGB(86,86,86));
	}


	if(pWnd->GetDlgCtrlID() == IDC_EDIT_PARTICLENUM3){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255,255,255));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_PARTICLERESULT3){
		pDC->SetTextColor(txcol_TVal3);
		pDC->SetBkColor(bkcol_TVal3);
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CParticle_ResultView_LG::PARTICLE_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_PARTICLETXT))->SetWindowText(lpcszString);
}
void CParticle_ResultView_LG::PARTICLENUM_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_PARTICLENUM))->SetWindowText(lpcszString);
}

void CParticle_ResultView_LG::RESULT_TEXT(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0){//대기
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18,  69,171);
	}else if(col == 2){//실패
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_PARTICLERESULT))->SetWindowText(lpcszString);
}

void CParticle_ResultView_LG::PARTICLE_TEXT2(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_PARTICLETXT2))->SetWindowText(lpcszString);
}
void CParticle_ResultView_LG::PARTICLENUM_TEXT2(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_PARTICLENUM2))->SetWindowText(lpcszString);
}

void CParticle_ResultView_LG::RESULT_TEXT2(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0){//대기
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(18,  69,171);
	}else if(col == 2){//실패
		txcol_TVal2 = RGB(255, 255, 255);
		bkcol_TVal2 = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal2 = RGB(0, 0, 0);
		bkcol_TVal2 = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_PARTICLERESULT2))->SetWindowText(lpcszString);
}

void CParticle_ResultView_LG::PARTICLE_TEXT3(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_PARTICLETXT3))->SetWindowText(lpcszString);
}
void CParticle_ResultView_LG::PARTICLENUM_TEXT3(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_PARTICLENUM3))->SetWindowText(lpcszString);
}

void CParticle_ResultView_LG::RESULT_TEXT3(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0){//대기
		txcol_TVal3 = RGB(255, 255, 255);
		bkcol_TVal3 = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal3 = RGB(255, 255, 255);
		bkcol_TVal3 = RGB(18,  69,171);
	}else if(col == 2){//실패
		txcol_TVal3 = RGB(255, 255, 255);
		bkcol_TVal3 = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_PARTICLERESULT3))->SetWindowText(lpcszString);
}
void	CParticle_ResultView_LG::Initstat(){
	RESULT_TEXT(0,"STAND BY");
	PARTICLENUM_TEXT("");
	PARTICLE_TEXT("TOTAL");

	RESULT_TEXT2(0,"STAND BY");
	PARTICLENUM_TEXT2("");
	PARTICLE_TEXT2("BLACK SPOT");

	RESULT_TEXT3(0,"STAND BY");
	PARTICLENUM_TEXT3("");
	PARTICLE_TEXT3("STAIN");
}
void CParticle_ResultView_LG::OnEnSetfocusEditParticletxt()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_ResultView_LG::OnEnSetfocusEditParticlenum()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_ResultView_LG::OnEnSetfocusEditParticleresult()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

BOOL CParticle_ResultView_LG::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CParticle_ResultView_LG::OnEnSetfocusEditParticletxt2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CParticle_ResultView_LG::OnEnSetfocusEditParticlenum2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CParticle_ResultView_LG::OnEnSetfocusEditParticleresult2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CParticle_ResultView_LG::OnEnSetfocusEditParticletxt3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CParticle_ResultView_LG::OnEnSetfocusEditParticlenum3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CParticle_ResultView_LG::OnEnSetfocusEditParticleresult3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}
