#pragma once
#include "afxcmn.h"


// COption_FixedPattern 대화 상자입니다.

class COption_FixedPattern : public CDialog
{
	DECLARE_DYNAMIC(COption_FixedPattern)

public:
	COption_FixedPattern(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COption_FixedPattern();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_FIXEDPATTERN };

	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd, CEdit	*pTEDIT, tINFO INFO);

	void	InitPrm();
	void	Load_parameter();
	void	Save_parameter();

	int		InsertIndex;
	int		Lot_InsertIndex;
	int		ListItemNum;
	int		StartCnt;
	int		Lot_StartCnt;
	int		Lot_InsertNum;

	void	Set_List(CListCtrl *List);
	void	FAIL_UPLOAD();
	bool	EXCEL_UPLOAD();
	void	EXCEL_SAVE();
	void	InsertList();

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();
	void	LOT_InsertDataList();

	int		iSavedItem_click;
	int		iSavedItem, iSavedSubitem;
	bool	ChangeCheck;
	int		ChangeItem[4];
	int		changecount;

	CRectData Test_RECT[3];
	BOOL	m_b_Ellipse[3];
	void	Change_DATA();
	void	Rect_InsertList();
	void	Rect_SETLIST();
	void	List_COLOR_Change(int itemnum);
	bool	Change_DATA_CHECK(bool FLAG);
	void	EditWheelIntSet(int nID, int Val);


	void	Pic(CDC *cdc);
	bool	FIXEDPATTERNPic_New(CDC *cdc, int NUM);
	tResultVal	Run();

	// mes
	CString		Mes_Result();
	tResultVal	m_retval;
	CString		m_szMesResult;
	BOOL		m_b_StopFail;

	BOOL	PatternNoiseGen(LPBYTE IN_RGB, int NUM);

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
	CString		strTitle;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_FixedPatternList;
	CListCtrl m_Lot_FixedPatternList;
	CListCtrl m_RectList;
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnNMClickListRect(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawListRect(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListRect(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusEditRectlist();
	afx_msg void OnEnSetfocusEditRectlist();
	afx_msg void OnBnClickedButtonDefault();
	afx_msg void OnBnClickedButtonLoad();
	afx_msg void OnBnClickedButtonListSave();
	int ThrNoise;
	int ThrDark;
	afx_msg void OnBnClickedButtonFpnSave();
	afx_msg void OnEnChangeThresoldV();
	afx_msg void OnEnChangeThresoldY();
};
