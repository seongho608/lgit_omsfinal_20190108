#pragma once


// COption_SubStand 대화 상자입니다.

class COption_SubStand : public CDialog
{
	DECLARE_DYNAMIC(COption_SubStand)

public:
	COption_SubStand(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COption_SubStand();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_STAND };

	int	m_CAM_SIZE_WIDTH;
	int m_CAM_SIZE_HEIGHT;
	void	Focus_move_start();

	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT);//,tINFO INFO);
	void	Save_parameter();
	void	Load_parameter();
	void	Position_Set();

	void	StatePrintf(LPCSTR lpcszString, ...);
	void	UpdateEditRect(int parm_edit_id);
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);

	double		m_MinAmp;
	double		m_MaxAmp;
	double	m_Offset;
	double		m_MinAmp2;
	double		m_MaxAmp2;
	double	m_Offset2;
	double		m_MinAmp3;
	double		m_MaxAmp3;
	double	m_Offset3;
	double		m_MinAmp4;
	double		m_MaxAmp4;
	double	m_Offset4;
	double		m_MinAmp5;
	double		m_MaxAmp5;
	double	m_Offset5;
	int	m_Align;

	double	d_Volt_Val;

	int m_dChartDistance;
	int m_dXModuleDistance;
	int m_dYModuleDistance;
	int m_dSModuleDistance;
	void	Enable_Overlay(bool stat);
	CString OVERLAYNAME;
	void	OVERLAY_NAMEVIEW(LPCSTR lpcszString, ...);
	CFont ft;
//	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	void	GetOverlayImage(CString MODELNAME, bool STATE);
	double d_OVThr;
	BYTE highlevel_Search(BYTE N1,BYTE N2,BYTE N3);

	int i_Alarmdelay;
	int i_Buzzerdelay;
	int i_Lightondelay;
	int i_Rayondelay;
	int i_Lightoffdelay;
	int i_VoltageDelay;

//	BOOL	m_SAEStat;
	unsigned int m_SShut;
	unsigned int m_SSensor;
	unsigned int m_SISP;
	BYTE m_SRed;
	BYTE m_SBlue;
	
//	BOOL	m_LAEStat;
	unsigned int m_LShut;
	unsigned int m_LSensor;
	unsigned int m_LISP;
	BYTE m_LRed;
	BYTE m_LBlue;

	CString ModeName;

	void TESTMOVEBTN_Enable(bool MODE);
	CString str_Oldsshutter;
	CString str_Oldssensor;
	CString str_OldsISP;
	CString str_OldsRED;
	CString str_OldsBLUE;

	CString str_Oldlshutter;
	CString str_Oldlsensor;
	CString str_OldlISP;
	CString str_OldlRED;
	CString str_OldlBLUE;
	CComboBox	*pHuTypeCombo;
	void	hModeSetup();
	void	InitEVMS();
private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	CString		str_model;
	CString		str_ModelPath;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButtonAmpsave();
//	afx_msg void OnBnClickedButtonContrlsave();
	CString m_VOLT_Change;
	CString m_VOLT_VALUE;
	afx_msg void OnEnChangeInputVvalue();
	afx_msg void OnBnClickedBtnVoltsave();
	CString str_MinAmp;
	CString str_MaxAmp;
	afx_msg void OnEnChangeEditMinamp();
	afx_msg void OnEnChangeEditMaxamp();
	BOOL b_Chk_Volton;
	BOOL b_Chk_CurrentChkEn;
	afx_msg void OnBnClickedCheckVoltageon();
	afx_msg void OnBnClickedCheckChkcurrent();
	afx_msg void OnEnChangeEditChartDistance();
	afx_msg void OnEnChangeEditModuleXdistance();
	afx_msg void OnEnChangeEditModuleYdistance();
	afx_msg void OnEnChangeEditModuleSdistance();
	afx_msg void OnBnClickedBtnChartMoveSave();
	afx_msg void OnBnClickedBtnModuleXSave();
	afx_msg void OnBnClickedBtnModuleYSave();
	afx_msg void OnBnClickedBtnChartTestmove();
	afx_msg void OnBnClickedBtnModulexTestmove();
	afx_msg void OnBnClickedBtnModuleyTestmove();
	afx_msg void OnBnClickedBtnLightAllOff();
	afx_msg void OnBnClickedBtnLightRayOn();
	afx_msg void OnBnClickedBtnLightLiOn();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedButtonOrigin();
	afx_msg void OnBnClickedButtonOriginStop();
	afx_msg void OnBnClickedButtonCylIn();
	afx_msg void OnBnClickedButtonCylOut();
	afx_msg void OnEnSetfocusDataVvalue();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButtonTestPos();
	afx_msg void OnBnClickedButtonStandby();
	afx_msg void OnEnChangeEditLighton();
	afx_msg void OnEnChangeEditLighton2();
	afx_msg void OnEnChangeEditLighton3();
	afx_msg void OnBnClickedButtonLightondelaysave();
	afx_msg void OnBnClickedButtonLightondelaysave2();
	afx_msg void OnBnClickedButtonLightondelaysave3();
	CString str_LightonDelay;
	CString str_RayonDelay;
	CString str_LightoffDelay;
	CString str_VoltageDelay;
	afx_msg void OnEnChangeEditVoltagedelay();
	afx_msg void OnBnClickedButtonAlarmRun();
	afx_msg void OnBnClickedButtonBuzzerPass();
	afx_msg void OnBnClickedButtonBuzzerFail();
	afx_msg void OnBnClickedButtonBuzzerOff();
	afx_msg void OnBnClickedButtonAlarmPass();
	afx_msg void OnBnClickedButtonAlarmFail();
	afx_msg void OnBnClickedButtonAlarmOff();
	afx_msg void OnBnClickedButtonAlarnsave();
	CString str_AlarmDelay;
	afx_msg void OnEnChangeEditAlarm();
	afx_msg void OnEnChangeEditAlarm2();
	CString str_Buzzerdelay;
	afx_msg void OnBnClickedButtonAlarnsave2();
	afx_msg void OnBnClickedBtnVoltsave2();
	BOOL b_SAEStat;
	BOOL b_LAEStat;
	afx_msg void OnBnClickedCheckSaeoff();
	afx_msg void OnBnClickedCheckLaeoff();
	afx_msg void OnEnChangeEditSshutter();
	CString str_sshutter;
	CString str_ssensor;
	CString str_sISP;
	CString str_sRED;
	CString str_sBLUE;
	CString str_lshutter;
	CString str_lsensor;
	CString str_lISP;
	CString str_lRED;
	CString str_lBLUE;
	afx_msg void OnEnChangeEditSsensor();
	afx_msg void OnEnChangeEditSisp();
	afx_msg void OnEnChangeEditSred();
	afx_msg void OnEnChangeEditSblue();
	afx_msg void OnEnChangeEditLshutter();
	afx_msg void OnEnChangeEditLsensor();
	afx_msg void OnEnChangeEditLisp();
	afx_msg void OnEnChangeEditLred();
	afx_msg void OnEnChangeEditLblue();
	afx_msg void OnEnSetfocusEditSshutter();
	afx_msg void OnEnSetfocusEditSsensor();
	afx_msg void OnEnSetfocusEditSisp();
	afx_msg void OnEnSetfocusEditSred();
	afx_msg void OnEnSetfocusEditSblue();
	afx_msg void OnEnSetfocusEditLshutter();
	afx_msg void OnEnSetfocusEditLsensor();
	afx_msg void OnEnSetfocusEditLisp();
	afx_msg void OnEnSetfocusEditLred();
	afx_msg void OnEnSetfocusEditLblue();
	afx_msg void OnBnClickedButtonSaetest();
	afx_msg void OnBnClickedButtonLaetest();
	afx_msg void OnBnClickedButtonSaesave();
	afx_msg void OnBnClickedButtonLaesave();
	afx_msg void OnBnClickedButtonReset();
	afx_msg void OnCbnSelchangeComboHutype();
	afx_msg void OnBnClickedButtonHutypesave();
	afx_msg void OnBnClickedButtonProperty();
	afx_msg void OnEnChangeEditLighton4();
	BOOL b_Chk_TestSkip;
	afx_msg void OnBnClickedCheckTestskip();
	CString FILE_PATH;
	afx_msg void OnBnClickedButtonRegisteropen();
	afx_msg void OnBnClickedButtonControl();
	CString str_MinAmp2;
	CString str_MinAmp3;
	CString str_MinAmp4;
	CString str_MinAmp5;
	CString str_MaxAmp2;
	CString str_MaxAmp3;
	CString str_MaxAmp4;
	CString str_MaxAmp5;
	CString str_Offset;
	CString str_Offset2;
	CString str_Offset3;
	CString str_Offset4;
	CString str_Offset5;
	afx_msg void OnEnChangeEditOffset();
	afx_msg void OnEnChangeEditOffset2();
	afx_msg void OnEnChangeEditOffset3();
	afx_msg void OnEnChangeEditOffset4();
	afx_msg void OnEnChangeEditOffset5();
	CString str_Align;
	BOOL b_Chk_Align;
	afx_msg void OnBnClickedCheckAlign();
	afx_msg void OnBnClickedBtnAlign();
	afx_msg void OnBnClickedButtonRegisteropen2();
	CString FILE_PATH_SECOND;
	BOOL B_Check_RegisterChange;
	afx_msg void OnBnClickedCheckRegisterchange();
	void Disable_RegisterChange(BOOL bMode);
	afx_msg void OnBnClickedButtonReadSave();
	UINT Edit_GetValue(UINT nID, BOOL bHex = TRUE);

	void Edit_SetValue(UINT nID, DWORD wData, BOOL bHex = TRUE);
	void Save_parameter_EEPROM();
	BOOL B_Check_Read;
	BOOL B_Check_Checksum;
	afx_msg void OnBnClickedCheckRead();
	afx_msg void OnBnClickedCheckChecksum();
	afx_msg void OnEnChangeEditMinamp2();
	afx_msg void OnEnChangeEditMaxamp2();
	afx_msg void OnEnChangeEditMinamp3();
	afx_msg void OnEnChangeEditMaxamp3();
	afx_msg void OnEnChangeEditMinamp4();
	afx_msg void OnEnChangeEditMaxamp4();
	afx_msg void OnEnChangeEditMinamp5();
	afx_msg void OnEnChangeEditMaxamp5();
	BOOL B_CHECK_ZERO;
	afx_msg void OnBnClickedCheckZero();
	afx_msg void OnBnClickedBtnModuleSSave();
	afx_msg void OnBnClickedBtnModulesTestmove();
	afx_msg void OnBnClickedBtnChartMoveSave2();
	BOOL b_Chk_AreaSensor;
	afx_msg void OnBnClickedCheckAreaSensor();
};
