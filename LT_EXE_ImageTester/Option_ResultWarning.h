#pragma once


// COption_ResultWarning 대화 상자입니다.

class COption_ResultWarning : public CDialog
{
	DECLARE_DYNAMIC(COption_ResultWarning)

public:
	COption_ResultWarning(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COption_ResultWarning();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_WARNINIG };
	void	Setup(CWnd* IN_pMomWnd);
	void InitStat();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	CWnd	*m_pMomWnd;	

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListCtrl m_listCh1WarningResult;
};
