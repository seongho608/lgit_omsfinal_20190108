// CENTER_DETECT.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "OPTIC_DETECT_Option.h"


extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4]; 

extern BYTE	m_ChartArea[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];

// COPTIC_DETECT_Option 대화 상자입니다.

IMPLEMENT_DYNAMIC(COPTIC_DETECT_Option, CDialog)

COPTIC_DETECT_Option::COPTIC_DETECT_Option(CWnd* pParent /*=NULL*/)
	: CDialog(COPTIC_DETECT_Option::IDD, pParent)
//	, m_channel_Number(0)
	, m_iStandX(0)
	, m_iStandY(0)
	, m_dPassDeviatX(0)
	, m_dPassDeviatY(0)
	, m_dRateX(0)
	, m_dRateY(0)
	, m_iEnPixX(0)
	, m_iEnPixY(0)
	, m_iMaxCount(0)
	, m_iWRMODE(0)
	, m_dPassMaxDevX(0)
	, m_dPassMaxDevY(0)
	, b_Chk_Img(FALSE)
	, m_Optical_radius(0)
	//, m_Sensitivity(0)
	, str_Sensitivity(_T(""))
{
	BinfileName = "";
	oldBinfileName = "";
	g_stat=0;
	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;
	pTStat = NULL;
	m_filemode = 0;
	m_EtcNumber = 0;
	m_Running = FALSE;
	for(int lop = 0;lop<4;lop++){
		EtcPt[lop].x = -1;
		EtcPt[lop].y = -1;
	}
	CPKcount =0;

	for(int t=0; t<200; t++){
		CPK_DATA[0][t]=0;
		CPK_DATA[1][t]=0;
	}
		b_TESTResult = FALSE;
			g_pImage = NULL;
	b_Chk_Img=0;

	ChangeCheck=0;
	for(int t=0; t<4; t++){
		ChangeItem[t]=-1;
	}
	changecount=0;
	iSavedSubitem=0;
	iSavedItem=0;
				g_pImage = NULL;

	StartCnt=0;
	b_StopFail = FALSE;
}

COPTIC_DETECT_Option::~COPTIC_DETECT_Option()
{
}

void COPTIC_DETECT_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//	DDX_CBIndex(pDX, IDC_COMBO_SelChNum, m_channel_Number);
	//DDX_Control(pDX, IDC_CAM_FRAME, m_stCenterDisplay);
	DDX_Text(pDX, IDC_STAND_OFFSETX, m_iStandX);
	DDV_MinMaxUInt(pDX, m_iStandX, 0, CAM_IMAGE_WIDTH);
	DDX_Text(pDX, IDC_STAND_OFFSETY, m_iStandY);
	DDV_MinMaxUInt(pDX, m_iStandY, 0, CAM_IMAGE_HEIGHT);
	DDX_Text(pDX, IDC_PASS_DEVIATX, m_dPassDeviatX);
	double DevX = CAM_IMAGE_WIDTH/2;
	DDV_MinMaxDouble(pDX, m_dPassDeviatX, 0.0, DevX);
	DDX_Text(pDX, IDC_PASS_DEVIATY, m_dPassDeviatY);
	double DevY = CAM_IMAGE_HEIGHT/2;
	DDV_MinMaxDouble(pDX, m_dPassDeviatY, 0.0, DevY);
	DDX_Text(pDX, IDC_RATE_X, m_dRateX);
	DDV_MinMaxDouble(pDX, m_dRateX, 0, 10.0);
	DDX_Text(pDX, IDC_RATE_Y, m_dRateY);
	DDV_MinMaxDouble(pDX, m_dRateY, 0, 10.0);
	DDX_Text(pDX, IDC_MAXCONTROL_NUMBER, m_iMaxCount);
	DDV_MinMaxUInt(pDX, m_iMaxCount, 0, 10);
	DDX_CBIndex(pDX, IDC_OPT_WRMODE, m_iWRMODE);
	//	DDX_Control(pDX, IDC_COMBO_SelChNum, m_cbSelMasterChannel);
	DDX_Control(pDX, IDC_LIST_CENTERWORK, m_CenterWorklist);
	DDX_Text(pDX, IDC_PASS_MAXDEVX, m_dPassMaxDevX);
	DDV_MinMaxDouble(pDX, m_dPassMaxDevX, 0.0, DevX);
	DDX_Text(pDX, IDC_PASS_MAXDEVY, m_dPassMaxDevY);
	DDV_MinMaxDouble(pDX, m_dPassMaxDevY, 0.0, DevY);
	DDX_Check(pDX, IDC_CHK_IMG, b_Chk_Img);
	DDX_Text(pDX, IDC_EDIT_radius, m_Optical_radius);
	//	DDX_Text(pDX, IDC_EDIT_sensing, m_Sensitivity);
	DDX_Text(pDX, IDC_EDIT_sensing, str_Sensitivity);
	DDX_Control(pDX, IDC_LIST_RECT, C_DATALIST);
	DDX_Control(pDX, IDC_SUB_FRAME, m_SubFrame);
	DDX_Control(pDX, IDC_LIST_CENTERWORK_LOT, m_Lot_OpticalCenterList);
}


BEGIN_MESSAGE_MAP(COPTIC_DETECT_Option, CDialog)
//	ON_CBN_SELCHANGE(IDC_COMBO2, &COPTIC_DETECT_Option::OnCbnSelchangeCombo2)
//ON_CBN_SELCHANGE(IDC_COMBO_SelChNum, &COPTIC_DETECT_Option::OnCbnSelchangeComboSelchnum)

ON_BN_CLICKED(IDC_OPTICALSTAT_SAVE, &COPTIC_DETECT_Option::OnBnClickedOpticalstatSave)
ON_CBN_SELCHANGE(IDC_COMBO_MODE, &COPTIC_DETECT_Option::OnCbnSelchangeComboMode)
//ON_BN_CLICKED(IDC_BTN_AUTOSET, &COPTIC_DETECT_Option::OnBnClickedBtnAutoset)
//ON_BN_CLICKED(IDC_BTN_CHARTSET, &COPTIC_DETECT_Option::OnBnClickedBtnChartset)
//ON_CBN_SELCHANGE(IDC_COMOB_BIN, &COPTIC_DETECT_Option::OnCbnSelchangeComobBin)
//ON_CBN_SELCHANGE(IDC_OPT_WRMODE, &COPTIC_DETECT_Option::OnCbnSelchangeOptWrmode)
//ON_CBN_SELCHANGE(IDC_OPT_WRMODE, &COPTIC_DETECT_Option::OnCbnSelchangeOptWrmode)
ON_BN_CLICKED(IDC_OPTICALSTAT_SAVE2, &COPTIC_DETECT_Option::OnBnClickedOpticalstatSave2)

ON_WM_SHOWWINDOW()
ON_BN_CLICKED(IDC_CHK_IMG, &COPTIC_DETECT_Option::OnBnClickedChkImg)
ON_BN_CLICKED(IDC_BUTTON_PRMSAVE, &COPTIC_DETECT_Option::OnBnClickedButtonPrmsave)
ON_BN_CLICKED(IDC_BUTTON_DEFAULTRECT, &COPTIC_DETECT_Option::OnBnClickedButtonDefaultrect)
ON_BN_CLICKED(IDC_BUTTON_LOADPRM, &COPTIC_DETECT_Option::OnBnClickedButtonLoadprm)
ON_BN_CLICKED(IDC_BUTTON_SAVEPRM, &COPTIC_DETECT_Option::OnBnClickedButtonSaveprm)
ON_NOTIFY(NM_DBLCLK, IDC_LIST_RECT, &COPTIC_DETECT_Option::OnNMDblclkListRect)
ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_RECT, &COPTIC_DETECT_Option::OnNMCustomdrawListRect)
ON_EN_KILLFOCUS(IDC_EDIT_RECT_OPTIC, &COPTIC_DETECT_Option::OnEnKillfocusEditRectOptic)
ON_WM_MOUSEWHEEL()
ON_EN_CHANGE(IDC_EDIT_RECT_OPTIC, &COPTIC_DETECT_Option::OnEnChangeEditRectOptic)
ON_BN_CLICKED(IDC_BUTTON_MICOM, &COPTIC_DETECT_Option::OnBnClickedButtonMicom)
ON_BN_CLICKED(IDC_BUTTON_test, &COPTIC_DETECT_Option::OnBnClickedButtontest)
END_MESSAGE_MAP()


// COPTIC_DETECT_Option 메시지 처리기입니다.
void COPTIC_DETECT_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
	str_model.Empty();
	str_model.Format("OpticDetect");
}

void COPTIC_DETECT_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
}


BOOL COPTIC_DETECT_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;
	((CEdit *) GetDlgItem(IDC_EDIT_RECT_OPTIC))->ShowWindow(FALSE);

	//((CImageTesterDlg  *)m_pMomWnd)->OpticEnable = 1;
	str_ModelPath=((CImageTesterDlg  *)m_pMomWnd)->modelfile_Name;


	SETLIST();
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
//	pFileCombo = (CComboBox *)GetDlgItem(IDC_COMOB_BIN);
	pRunCombo = (CComboBox *)GetDlgItem(IDC_COMBO_MODE);
	Load_parameter();
//	pINTWROPTCombo->SetCurSel(m_CheckWrite);
//	pWROPTCombo->SetCurSel(m_ChkSector);
	Set_List(&m_CenterWorklist);
	LOT_Set_List(&m_Lot_OpticalCenterList);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

tResultVal COPTIC_DETECT_Option::Run()
{

	tResultVal retval = {0,};
	b_StopFail = FALSE;
	m_dCurrCenterCoord = cvPoint(0, 0);
	Initgen(&CenterVal);

	if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == TRUE){
	
		InsertList();
	}

	m_RunMode=1;
	if(m_RunMode == 0){//광축 조정 모드
		OpticalAsixModify(&CenterVal);
	}else if(m_RunMode == 1){//광축 측정 모드
		Find_offset(&CenterVal);
	}
	
	retval.m_Success = CenterVal.m_success;

	CString stateDATA ="왜곡 광축_ ";
	CString str = "";
	if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == TRUE){
		if((abs(CenterVal.m_Init_X) < 0) ||(abs(CenterVal.m_Init_X) > m_CAM_SIZE_WIDTH))
		{
			str="0";
		}else{
			str.Empty();
			str.Format("%d",CenterVal.m_Init_X);
		}
		m_CenterWorklist.SetItemText(InsertIndex,5,str);
		//초기 옵셋
		if((abs(CenterVal.m_Init_Y) < 0) ||(abs(CenterVal.m_Init_Y) > m_CAM_SIZE_HEIGHT))
		{
			str="0";
		}else{
			str.Empty();
			str.Format("%d",CenterVal.m_Init_Y);
		}
		m_CenterWorklist.SetItemText(InsertIndex,6,str);
		//초기 옵셋

		if((fabs(CenterVal.m_X_ASIX) < 0) ||(fabs(CenterVal.m_X_ASIX) > m_CAM_SIZE_WIDTH))
		{
			str="0";
		}else{
			str.Empty();
			str.Format("%3.1f",CenterVal.m_X_ASIX);
		}
		m_CenterWorklist.SetItemText(InsertIndex,7,str);


		if((fabs(CenterVal.m_Y_ASIX) < 0) ||(fabs(CenterVal.m_Y_ASIX) > m_CAM_SIZE_HEIGHT))
		{
			str="0";
		}else{
			str.Empty();
			str.Format("%3.1f",CenterVal.m_Y_ASIX);
		}
		m_CenterWorklist.SetItemText(InsertIndex,8,str);

		if(m_RunMode == 0){//광축 조정 모드
			str.Empty();
			str.Format("%d",CenterVal.m_offsetX);
			m_CenterWorklist.SetItemText(InsertIndex,9,str);

			str.Empty();
			str.Format("%d",CenterVal.m_offsetY);
			m_CenterWorklist.SetItemText(InsertIndex,10,str);

		}else{
			m_CenterWorklist.SetItemText(InsertIndex,9,"");
			m_CenterWorklist.SetItemText(InsertIndex,10,"");

		}
		str.Empty();
		str.Format("%d",CenterVal.m_X_OFFSET);
		m_CenterWorklist.SetItemText(InsertIndex,11,str);

		str.Empty();
		str.Format("%d",CenterVal.m_Y_OFFSET);
		m_CenterWorklist.SetItemText(InsertIndex,12,str);

	}


	str.Empty();
	str.Format("%3.1f",CenterVal.m_X_ASIX);
	stateDATA += "X_ "+str;
	str.Empty();
	str.Format("%3.1f",CenterVal.m_Y_ASIX);
	stateDATA += " Y_ "+str;
	str.Empty();
	str.Format("%d",CenterVal.m_X_OFFSET);
	stateDATA += "  offsetX_ "+str;
	str.Empty();
	str.Format("%d",CenterVal.m_Y_OFFSET);
	stateDATA += " Y_ "+str;

	if(retval.m_Success == TRUE){
		b_TESTResult = TRUE;
		stateDATA += " _ PASS";
		if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == TRUE){
		m_CenterWorklist.SetItemText(InsertIndex,13,"PASS");
		//((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex,7,"PASS");
		}
		retval.ValString = "OK";
		
		
	}else{
		b_TESTResult = FALSE;
		stateDATA += " _ FAIL";
		if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == TRUE){

			m_CenterWorklist.SetItemText(InsertIndex,13,"FAIL");
			//((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex,7,"FAIL");
		}
		retval.ValString = "FAIL";

		
		
	}
	((CImageTesterDlg  *)m_pMomWnd)->StatePrintf(stateDATA);

	if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == TRUE){
		StartCnt++;
	}

	return retval;
}


void COPTIC_DETECT_Option::CPK_DATA_SUM(){
	
	double AveX =0;
	double AveY =0;
	double VarX =0;
	double VarY =0;

	double PassX = CenterPrm.PassDeviatX;
	double PassY = CenterPrm.PassDeviatY;

	double USL_X = PassX;
	double LSL_X = (PassX * -1.0);

	double USL_Y = PassY;
	double LSL_Y = (PassY * -1.0);



	for(int t=0; t<CPKcount;t++){
		//--------------------------------X
		AveX += CPK_DATA[0][t];
		//--------------------------------Y
		AveY += CPK_DATA[1][t];
	}

	AveX /= (double)CPKcount;
	AveY /= (double)CPKcount;

	for(int t=0; t<CPKcount;t++){
		//--------------------------------X
		VarX += pow(((double)CPK_DATA[0][t] - AveX),2);
		//--------------------------------Y
		VarY += pow(((double)CPK_DATA[1][t] - AveY),2);
	}
	VarX /= (double)CPKcount;
	VarY /= (double)CPKcount;

	double SqrtX = sqrt(VarX);
	double SqrtY = sqrt(VarY);

	//--------------------------------X
	double K_X=0;
	if(fabs(USL_X - AveX) >fabs(LSL_X - AveX)){
		K_X = fabs(0 - AveX) / PassX;
	}
	else{
		K_X = fabs(0 - AveX) / PassX;
	}

	double CP_X = 0;
	double sum_X = (SqrtX*6.0);


	CP_X = (PassX*2.0)/(SqrtX*6.0);
	
	double CPK_X=(1.0-K_X)*CP_X;
	if(sum_X == 0){
		CPK_X =100;		
	}
	if(sum_X < 0){
		CPK_X =0;		
	}
	//--------------------------------Y
	double K_Y=0;
	if(fabs(USL_Y - AveY) >fabs(LSL_Y - AveY)){
		K_Y = fabs(0 - AveY) / PassY;
	}
	else{
		K_Y = fabs(0 - AveY) / PassY;
	}

	double CP_Y = 0;
	

	double sum_Y = (SqrtY*6.0);

	
	double CPK_Y=(1.0-K_Y)*CP_Y;


	if(sum_Y == 0){
		CPK_Y =100;
	}
	if(sum_Y < 0){
		CPK_Y =0;
	}
	CString str_X="";
	str_X.Format("%6.2f",CPK_X);
	((CImageTesterDlg  *)m_pMomWnd)->m_pStandWnd->CPK_X(str_X);
	CString str_Y="";
	str_Y.Format("%6.2f",CPK_Y);
	((CImageTesterDlg  *)m_pMomWnd)->m_pStandWnd->CPK_Y(str_Y);

	SaveCPK_X = CPK_X;
	SaveCPK_Y = CPK_Y;
	
}

CvPoint COPTIC_DETECT_Option::GetCenterPoint(LPBYTE IN_RGB)
{	
	CvPoint resultPt = cvPoint(-99999, -99999);

	IplImage *OriginImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *PreprecessedImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	
	IplImage *RGBOrgImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3);

	BYTE R,G,B;
	double Sum_Y;
	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	for (int y=0; y<m_CAM_SIZE_HEIGHT; y++)
	{
		for (int x=0; x<m_CAM_SIZE_WIDTH; x++)
		{
		/*	B = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 2];
			
			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;		*/

			B = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 2];

			if( R < 100 && G < 100 && B < 100)
				OriginImage->imageData[y*OriginImage->widthStep+x] = 255;
			else
				OriginImage->imageData[y*OriginImage->widthStep+x] = 0;

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}	
	
//	cvSaveImage("C:\\CenterImage.bmp", OriginImage);
//	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
//	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);

	cvCopyImage(OriginImage, SmoothImage);
	
	cvCanny(SmoothImage, CannyImage, 0, 255);

//	cvDilate(CannyImage, DilateImage);
	
	cvCopyImage(CannyImage, temp_PatternImage);
	
	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);
	
//	cvSaveImage("C:\\CenterImage.bmp", CannyImage);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
		counter++;
	
	if(counter == 0)
		return resultPt;

	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area=0, arcCount=0;
	counter = 0;
	double old_dist = 999999;
	
	int old_center_pos_x = 0;
	int old_center_pos_y = 0;
	int center_pt_x = 0;
	int center_pt_y = 0;
	int obj_Cnt = 0;
	int size=0, old_size = 0;	

	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;

		double circularity = (4.0*3.14*area)/(arcCount*arcCount);		

		if(circularity > 0.8)
		{
		
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;

			if(center_x > (int)(Cam_PosX*0.44) && center_x < (int)(Cam_PosX*1.56) && center_y > (int)(Cam_PosY*0.16) && center_y < (int)(Cam_PosY*1.84))
			{
				if(rect.width < Cam_PosX && rect.height < Cam_PosY)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
					
					/*center_pt_x = center_pt_x+ center_x;
					center_pt_y = center_pt_y+ center_y;
					obj_Cnt++;

					old_center_pos_x = center_pt_x/obj_Cnt;
					old_center_pos_y = center_pt_y/obj_Cnt;*/
					
					double distance = GetDistance(rect.x + rect.width/2, rect.y+rect.height/2, Cam_PosX, Cam_PosY);
					
					obj_Cnt++;

					size = rect.width * rect.height;
					
					if(distance < old_dist)
					{
						old_size = size;
						resultPt.x = center_x;
						resultPt.y = center_y;
						old_dist = distance;
					}

					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 0), 1, 8);  
				
				}
			}
		}
		
		counter++;
	}
//	cvSaveImage("C:\\RGBResultImage.bmp", RGBResultImage);
	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
	cvReleaseImage(&RGBOrgImage);

	delete []rectArray;
	delete []areaArray;
	
	/*if(obj_Cnt != 0)
	{
		resultPt.x = center_pt_x / obj_Cnt;
		resultPt.y = center_pt_y / obj_Cnt;
	}*/
/*	if(obj_Cnt != 0)
	{
		resultPt.x = center_pt_x;
		resultPt.y = center_pt_y;
	}*/
		
	return resultPt;	
}

//void COPTIC_DETECT_Option::OnCbnSelchangeComboSelchnum()
//{
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	int sel = m_cbSelMasterChannel.GetCurSel();
//	CString str;
//	m_cbSelMasterChannel.GetLBText(sel, str);
//	int currCh=0;
//	
//	if(str == "채널 1")
//		currCh = 0;
//	else if(str == "채널 2")
//		currCh = 1;
//	else if(str == "채널 3")
//		currCh = 2;
//	else if(str == "채널 4")
//		currCh = 3;
//	else
//		currCh = 0;
//		
//	((CImageTesterDlg  *)m_pMomWnd)->m_pChartWnd->ChartReset();
//	((CImageTesterDlg  *)m_pMomWnd)->m_pChartWnd->DrawCircle(m_ptMasterCoord[currCh].x, m_ptMasterCoord[currCh].y, 75, 75);
//	
//	SetDlgItemInt(IDC_Chart_X, m_ptMasterCoord[currCh].x);
//	SetDlgItemInt(IDC_CHART_Y, m_ptMasterCoord[currCh].y);
//	
//	DoEvents(300);
//
//	if(((CImageTesterDlg  *)m_pMomWnd)->m_SelCh == 0)
//		((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan = 1;
//
//	for(int i =0;i<10;i++){
//		if(((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan == 0){
//			break;
//		}else{
//			DoEvents(100);
//		}
//	}	
//
//	ShowCenterMasterImage(currCh);
//	
//}
/*
void COPTIC_DETECT_Option::ShowCenterMasterImage(int ch)
{
	unsigned char *RGBBuf = new unsigned char [CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];

	BITMAPINFO		m_biInfo;
	ZeroMemory(&m_biInfo, sizeof(m_biInfo));
	m_biInfo.bmiHeader.biSize		= sizeof(BITMAPINFOHEADER);
	m_biInfo.bmiHeader.biWidth		= CAM_IMAGE_WIDTH;
	m_biInfo.bmiHeader.biHeight		= CAM_IMAGE_HEIGHT;
	m_biInfo.bmiHeader.biPlanes		= 1;
	m_biInfo.bmiHeader.biBitCount	= 32;
	m_biInfo.bmiHeader.biCompression	= 0;
	m_biInfo.bmiHeader.biSizeImage	= CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4;	

	CDC *pcDC;

	//-----------가상메모리를 만든다.------------------
	CDC	*pcMemDC = new CDC;
	CBitmap	*pcDIB	= new CBitmap;

//	CSPACE_UYVY_TO_RGB32(ComArtWnd->m_YUVIn[m_Channel], CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT,m_RGBScanbuf[m_Channel], 0);
	pcDC = m_stCenterDisplay.GetDC();
	memcpy(RGBBuf,m_RGBScanbuf[ch],sizeof(m_RGBScanbuf[ch]));

	pcMemDC->CreateCompatibleDC(pcDC);
	pcDIB->CreateCompatibleBitmap(pcDC,CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT);
	CBitmap	*pcOldDIB = pcMemDC->SelectObject(pcDIB);
	
	

	//---------------------------------------------
	::SetStretchBltMode(pcMemDC->m_hDC, COLORONCOLOR);//MemDC.m_hDC//pcDC->m_hDC
	::StretchDIBits(pcMemDC->m_hDC, 
					0,  CAM_IMAGE_HEIGHT-1,
					CAM_IMAGE_WIDTH, -CAM_IMAGE_HEIGHT,
					0, 0,
					m_biInfo.bmiHeader.biWidth,
					m_biInfo.bmiHeader.biHeight,
					RGBBuf, &m_biInfo, DIB_RGB_COLORS, SRCCOPY);

	CPen my_Pan, my_Pan2, *old_pan;
	
	my_Pan.CreatePen(PS_SOLID,3,RGB(255, 255, 255));
	old_pan = pcMemDC->SelectObject(&my_Pan);
	pcMemDC->MoveTo(270, 160);
	pcMemDC->LineTo(450,160);
	pcMemDC->LineTo(450,320);
	pcMemDC->LineTo(270,320);
	pcMemDC->LineTo(270,160);
	my_Pan.DeleteObject();
	
	int x = m_ptCenterPointCoord[ch].x;
	int y = m_ptCenterPointCoord[ch].y;

	my_Pan2.CreatePen(PS_SOLID,3,RGB(255, 255, 255));
	old_pan = pcMemDC->SelectObject(&my_Pan2);
	pcMemDC->MoveTo(x-10, y);
	pcMemDC->LineTo(x+10, y);
	pcMemDC->MoveTo(x, y-10);
	pcMemDC->LineTo(x, y+10);
	my_Pan2.DeleteObject();

	CString TEXTDATA ="";
	CFont font;
	font.CreatePointFont(240, "Arial");  
	pcMemDC->SelectObject(&font);
	
	if(m_ptCenterPointCoord[ch].x == -99999 && m_ptCenterPointCoord[ch].y == -99999)
	{
		TEXTDATA.Format("중심점을 찾지 못하였습니다.");
		pcMemDC->SetTextColor(RGB(255, 0, 0));
	}
	else
	{
		TEXTDATA.Format("X = %d   Y = %d", m_ptCenterPointCoord[ch].x, m_ptCenterPointCoord[ch].y);
		pcMemDC->SetTextColor(RGB(0, 0, 255));
	}

	pcMemDC->TextOut(0 , CAM_IMAGE_HEIGHT - 80,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	font.DeleteObject();


	//--------------모니터 창에 맞추어 RGB정보를 할당한다.-------------------------
	::SetStretchBltMode(pcDC->m_hDC, COLORONCOLOR);
	::StretchBlt(	pcDC->m_hDC,
					0,  0,
					//720, 480,//1080, 720,
					360,228,
					pcMemDC->m_hDC,
					0, 0,
					CAM_IMAGE_WIDTH,
					CAM_IMAGE_HEIGHT,SRCCOPY);
	

	m_stCenterDisplay.ReleaseDC(pcDC);

	delete pcDIB;
	delete pcMemDC;	
	delete []RGBBuf;
	
//	return TRUE;
}
*/

void COPTIC_DETECT_Option::OnBnClickedBtnBinadd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	CString GetFileName,GetFilepath,GetFolderPath;
	CString DstFile = "";

	CFile file;	
	CFileDialog dlg(TRUE,"bin","*.bin",OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT|OFN_ALLOWMULTISELECT, 
	"bin 파일 (*.bin)|*.bin|모든 파일 (*.*)|*.*|");

	if(dlg.DoModal() == IDOK)
	{	
		GetFileName = dlg.GetFileTitle();
		GetFilepath = dlg.GetPathName();//폴더 및 파일경로를 가져온다.			
		DstFile = str_BinPath + ("\\") + GetFileName + ".bin";
	
		if(GetFilepath != DstFile){
			BinFiledel();//기존에 있던 파일을 지운다.
			CopyFile(GetFilepath,DstFile,false);
			DoEvents(100);
		}
		WritePrivateProfileString(str_model,"FILENAME",GetFileName,str_ModelPath);//BIN파일 경로를 저장한다.
		DoEvents(100);
		SetFileSel();
	}
}

void COPTIC_DETECT_Option::BinFiledel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CFileFind  finder;
	CString fname = "";
	CString strFile = str_BinPath +("\\*.bin");

	BOOL bWorking = finder.FindFile(strFile);
	while(bWorking)
	{
		bWorking = finder.FindNextFile();
		if(!finder.IsDots() && !finder.IsDirectory()){
			fname.Empty();
			fname = finder.GetFileTitle();
			DelFile(str_BinPath + "\\" + fname + ".bin");
		}
	}
}

void COPTIC_DETECT_Option::SetFileSel()//bin파일이 있는지 확인후에 경로 지정 및 이름지정하고 존재할 경우 binfilechk를 실행한다.
{
	//CFileFind  finder;
	////------------------------------------------- 폴더에 ini파일이 있으면 각불러올 파일을 갱신한다. 
	//CString	   loadfile = "";
	//char cload[100] ={0,};
	//int frnum;//폴더이름갯수
	//
	//if(finder.FindFile(str_ModelPath) == TRUE){//ini파일이 있으면 읽어들여서 선택된 BIN파일이 있는지 체크한다. 
	//	frnum = GetPrivateProfileString(str_model,"FILENAME","",cload,100,str_ModelPath);
	//	loadfile.Empty();
	//	for(int i=0; i<frnum; i++){
	//		loadfile += cload[i];
	//	}

	//	if(loadfile != ""){
	//		CString binPath = str_BinPath + "\\" + loadfile + (".bin"); 
	//		if(finder.FindFile(binPath) == TRUE){
	//			model_bin_path = binPath;
	//			BinfileName = loadfile;
	//			if(oldBinfileName != BinfileName){
	//				oldBinfileName = BinfileName;
	//				m_filemode = BinFileCHK();
	//			}
	//		}else{
	//			WritePrivateProfileString(str_model,"FILENAME","",str_ModelPath);//현재 선택된 셀을 저장한다.
	//			model_bin_path = "";
	//			BinfileName = "";
	//		}
	//	}else{
	//		model_bin_path = "";
	//		BinfileName = "";
	//	}
	//}else{
	//	model_bin_path = "";
	//	BinfileName = "";
	//}

	//((CEdit *)GetDlgItem(IDC_EDIT_BINFILENAME))->SetWindowText(BinfileName);
	//if(((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd != NULL){
	//	((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd->pFileEdit->SetWindowText(BinfileName);
	//}
//	((CImageTesterDlg  *)m_pMomWnd)->m_pResWorkerOptWnd->Filename_Value(BinfileName);

}

/*
void COPTIC_DETECT_Option::FileSearch(CString compdata)//폴더검색
{
	//----------------------------------------------------------------------------------------
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CFileFind  finder;
	//-----------------------------
	int mcnt = -1;
	int conum = -1;
	CString fname = "";
	CString strFile =  str_BinPath +("\\*.BIN"); 
	CString str="";

	pFileCombo->ResetContent();
	if(((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd != NULL){
		((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd->pFileEdit->SetWindowText("");
	}
	BOOL bWorking = finder.FindFile(strFile);
	while(bWorking)
	{
		bWorking = finder.FindNextFile();
		if(!finder.IsDots() && !finder.IsDirectory()){
			fname.Empty();
			fname = finder.GetFileTitle();
			pFileCombo->AddString(fname);
			mcnt++;	
			if(fname == compdata){
				conum = mcnt;
			}
						
		}
	}
	
	if((conum >= 0)&&(mcnt>=conum)){
		m_file_sel = conum;
	}else{
		m_file_sel = mcnt;
	}
	
	if(m_file_sel != -1){
		pFileCombo->SetCurSel(m_file_sel);
		pFileCombo->GetLBText(m_file_sel,str);
		if(((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd != NULL){
			((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd->pFileEdit->SetWindowText(str);
		}
		SetFileSave();
	}else{//지정된 파일이 없을 경우 
		CenterPrm.MaxCount = 0;
		CenterPrm.RateX = 0;
		CenterPrm.RateY = 0;
		CenterPrm.EnPixX = 0;
		CenterPrm.EnPixY = 0;
		CenterPrm.WriteMode = 0;
		SetFileSave();
	}
}
*/
void COPTIC_DETECT_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

BYTE COPTIC_DETECT_Option::BinFileCHK()
{
	CString BufFile;
	
	int stat = BinFileCHK(0); //일반 BIN파일인지 체크한다.
	if(stat == 0){//BIN파일에서 CHK데이터를 찾지 못했을때
		stat = BinFileCHK(1);	//	
	}else if(stat == 1){//현재 BIN파일이 맞을때
		BufFile.Empty();
		BufFile.Format("[%s.bin] FILE READ&OFFSET CHECK SUCCESS",BinfileName);
		StatePrintf(BufFile);
		return 1;
	}else{//BIN파일이 없거나 읽는걸 실패할때
		return 0;
	}

	if(stat == 0){//BIN파일에서 CHK데이터를 찾지 못했을때
		BufFile.Empty();
		BufFile.Format("[%s.bin] FILE READ&OFFSET CHECK FAILS",BinfileName);
		StatePrintf(BufFile);
	}else if(stat == 1){//현재 BIN파일이 맞을때
		BufFile.Empty();
		BufFile.Format("[%s.bin] FILE MEGA READ&OFFSET CHECK SUCCESS",BinfileName);
		StatePrintf(BufFile);
		return 2;
	}
	return 0;
}

int COPTIC_DETECT_Option::BinFileCHK(int stat)
{
	// TODO: Add your control notification handler code here	
	CString GetFileName;
	CString BufFile;
	int n[40]={0};		

	CFile BINFILE;
	CFileStatus BINFILESTATUS;
	CFileException BINEX;

	if(stat == 0){//
		xoffdata = 0x10;
		yoffdata = 0x0c;
		m_DChkSum1 = 0xde;
		m_DChkSum2 = 0xac;
	}else{
		xoffdata = 0;
		yoffdata = 0;
		m_DChkSum1 = 0xEC;
		m_DChkSum2 = 0x9C;
	}
	
	unsigned long nRet = 0, i,k,m,IndexOffset, AddressPage=0;
	unsigned long SIZEofBUFF; //MAXSIZE,CNTSIZE,
	char buffer[32][256];
	unsigned char EndOfRead=0;//RD_BUFFER_SIZE를 결정함으로서 한번에 읽을 버퍼사이즈를 결정한다. 
	unsigned long MAXSIZE,MAXADDR;
	if(BinfileName == ""){
		StatePrintf("BIN FILE EMPTY");
		return 2;
	}
	GetFileName = str_BinPath +"\\"+BinfileName+".bin";
	
	//------------------------------------------------------------------------------------ 읽을 파일을 결정한다.
	if(!BINFILE.Open(GetFileName, CFile::modeRead, &BINEX)){ //오류가 발생하면
		StatePrintf("BIN FILE READ FAIL");
		return 2;
	}
	else {
		BINFILE.GetStatus(BINFILESTATUS);
		MAXSIZE = BINFILESTATUS.m_size;
		MAXADDR = (unsigned long)(MAXSIZE/256);
		
		SIZEofBUFF=sizeof(buffer[0]);
		for(i=0;i<32;i++) ZeroMemory(buffer[i], SIZEofBUFF); 

		EndOfRead=0;EndPage==0;
		for(AddressPage=0;(AddressPage<=MAXADDR)&&(EndOfRead!=2);AddressPage++){
			if(((nRet = BINFILE.Read(buffer[AddressPage%32],SIZEofBUFF))) != 0){
				for(i=0;(i<nRet)&&(EndOfRead==0);i++){    //AddOfOffset[4]={X,Y,DE,AC};
					if(AddOfOffset[2] == 0){              //  DE 를 찾기 전까지 만... 

						if(buffer[AddressPage%32][i]==m_DChkSum1){ //우선 ec 만 찾은경우..
							if(i==(SIZEofBUFF-1)){ //ec가 버퍼의 마지막 [256번째]에 있어 AC를 다음 block 에서 찾아야 하는 경우
								AddOfOffset[2]=AddressPage*SIZEofBUFF + i ;
							}
							else {
								if(buffer[AddressPage%32][i+1]==m_DChkSum2){   // ec 9c 를 찾은경우..
									AddOfOffset[2]=AddressPage*SIZEofBUFF +  i  ;
									AddOfOffset[3]=AddressPage*SIZEofBUFF + i+1 ;

									if(AddOfOffset[3]+ 16 < (AddressPage+1)*SIZEofBUFF ){ // X, Y offset 변수 위치가 동일 Page 에 있을때
										if((buffer[AddressPage%32][AddOfOffset[3]%SIZEofBUFF+8]==xoffdata)&&(buffer[AddressPage%32][AddOfOffset[3]%SIZEofBUFF+16]==yoffdata)){
											AddOfOffset[0]=AddOfOffset[3]+ 8;
											AddOfOffset[1]=AddOfOffset[3]+16;

											EndOfRead=1;
										}
										else AddOfOffset[2]=AddOfOffset[3]=0;
									} 
									else {
										if(AddOfOffset[3]+8 < (AddressPage+1)*SIZEofBUFF){
											if(buffer[AddressPage%2][AddOfOffset[3]%SIZEofBUFF+8]==xoffdata){
												AddOfOffset[0]=AddOfOffset[3]+ 8;
											}
											else AddOfOffset[2]=AddOfOffset[3]=0;
										}
									}

								}
								else {   // DE를 찾았으나, 연이어 AC가 오지 않은 경우
									AddOfOffset[2]=AddOfOffset[3]=0;
								}
							}
						}
					}
					else {
						if(AddOfOffset[0]==0x00){
							if((i==0)&&(AddOfOffset[3]==0x00)){
								if(buffer[AddressPage%2][i]==m_DChkSum2) AddOfOffset[3]=AddressPage*SIZEofBUFF +  i;
								else AddOfOffset[2]=AddOfOffset[3]=0;
							}
							if(AddOfOffset[3]!=0x00){
								if(AddOfOffset[3]+ 16 < (AddressPage+1)*SIZEofBUFF ){ // X, Y offset 변수 위치가 동일 Page 에 있을때
									if((buffer[AddressPage%2][AddOfOffset[3]%SIZEofBUFF+8]==xoffdata)&&(buffer[AddressPage%2][AddOfOffset[3]%SIZEofBUFF+16]==yoffdata)){
										AddOfOffset[0]=AddOfOffset[3]+ 8;
										AddOfOffset[1]=AddOfOffset[3]+16;

										EndOfRead=1;
									}
									else AddOfOffset[2]=AddOfOffset[3]=0;
								} 
								else {
									if(AddOfOffset[3]+8 < (AddressPage+1)*SIZEofBUFF){
										if(buffer[AddressPage%2][AddOfOffset[3]%SIZEofBUFF+8]==xoffdata){
											AddOfOffset[0]=AddOfOffset[3]+ 8;
										}
										else AddOfOffset[2]=AddOfOffset[3]=0;
									}
								}
							}
						}
						else {
							if(buffer[AddressPage%2][(AddOfOffset[0]+8)%SIZEofBUFF]==yoffdata){
								AddOfOffset[1]=AddOfOffset[0]+8;
								EndOfRead=1;
							}
							else AddOfOffset[2]=AddOfOffset[3]=0;
						}
					}
				}
				if((EndOfRead==1)&&(AddressPage%16==0))EndOfRead=2;
			}
		}

		if((AddOfOffset[1]&0x01000) != 0x00 ) IndexOffset = 16; //현재 위치가 1k byte 이상인 위치에 있으면...
		else IndexOffset = 0;

		for(k=0;k<16;k++){  
			
			for(m=0;m<256;m++) {
				OffsetBuffer[16+k][m]=buffer[   IndexOffset     +k][m];
				OffsetBuffer[   k][m]=buffer[(IndexOffset+16)%32+k][m];
			}
		}

		BINFILE.Close();
		if (EndOfRead == 2){
			B_FileChk = TRUE;
			return 1;
		}else{
			B_FileChk = FALSE;
			return 0;
		}
	}		
}

void COPTIC_DETECT_Option::Save_parameter(){
	CString	   str = "";
	WritePrivateProfileString(str_model,"FILENAME",BinfileName,str_ModelPath);//BIN파일 경로를 저장한다.
	str.Empty();
	str.Format("%d",CenterPrm.MaxCount);
	WritePrivateProfileString(str_model,"MAXCONTROLCOUNT",str,str_ModelPath);
	str.Empty();
	str.Format("%d",CenterPrm.StandX);
	WritePrivateProfileString(str_model,"STANDOFFSETX",str,str_ModelPath);
	str.Empty();
	str.Format("%d",CenterPrm.StandY);
	WritePrivateProfileString(str_model,"STANDOFFSETY",str,str_ModelPath);
	str.Empty();
	str.Format("%6.2f",CenterPrm.PassDeviatX);
	WritePrivateProfileString(str_model,"PASSDEVIATX",str,str_ModelPath);
	str.Empty();
	str.Format("%6.2f",CenterPrm.PassDeviatY);
	WritePrivateProfileString(str_model,"PASSDEVIATY",str,str_ModelPath);
	str.Empty();
	str.Format("%6.2f",CenterPrm.PassMaxDevX);
	WritePrivateProfileString(str_model,"MAXPASSDEVX",str,str_ModelPath);
	str.Empty();
	str.Format("%6.2f",CenterPrm.PassMaxDevY);
	WritePrivateProfileString(str_model,"MAXPASSDEVY",str,str_ModelPath);

	str.Empty();
	str.Format("%6.2f",CenterPrm.RateX);
	WritePrivateProfileString(str_model,"RATEX",str,str_ModelPath);
	str.Empty();
	str.Format("%6.2f",CenterPrm.RateY);
	WritePrivateProfileString(str_model,"RATEY",str,str_ModelPath);
	str.Empty();
	str.Format("%d",CenterPrm.EnPixX);
	WritePrivateProfileString(str_model,"ENPIXX",str,str_ModelPath);
	str.Empty();
	str.Format("%d",CenterPrm.EnPixY);
	WritePrivateProfileString(str_model,"ENPIXY",str,str_ModelPath);
	str.Empty();
	str.Format("%d",CenterPrm.WriteMode);
	WritePrivateProfileString(str_model,"WRITEMODE",str,str_ModelPath);

	str.Empty();
	str.Format("%6.2f",CenterPrm.Optical_Radius);
	WritePrivateProfileString(str_model,"OpticalRadius",str,str_ModelPath);

	str.Empty();
	str.Format("%d",i_Sensitivity);
	WritePrivateProfileString(str_model,"Sensitivity",str,str_ModelPath);

	
	/*str.Empty();
	str.Format("%d",m_ChkSector);
	WritePrivateProfileString(str_model,"SectorWrite",str,str_ModelPath);
	str.Empty();
	str.Format("%d",m_CheckWrite);
	WritePrivateProfileString(str_model,"initWrite",str,str_ModelPath);*/

	
}


void COPTIC_DETECT_Option::Load_parameter(){
	CString	   str = "";
	CString	   loadfile = "";
	char cload[100] ={0.};
	int frnum;//폴더이름갯수
	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);
	
	CenterPrm.MaxCount = GetPrivateProfileInt(str_model,"MAXCONTROLCOUNT",-1,str_ModelPath);//테스트를 진행할 갯수
	if(CenterPrm.MaxCount == -1){
		CenterPrm.MaxCount = 3;
		str.Empty();
		str.Format("%d",CenterPrm.MaxCount);
		WritePrivateProfileString(str_model,"MAXCONTROLCOUNT",str,str_ModelPath);
	}

	CenterPrm.StandX = GetPrivateProfileInt(str_model,"STANDOFFSETX",-1,str_ModelPath);
	if(CenterPrm.StandX == -1){
		CenterPrm.StandX = Cam_PosX;
		str.Empty();
		str.Format("%d",CenterPrm.StandX);
		WritePrivateProfileString(str_model,"STANDOFFSETX",str,str_ModelPath);
	}
	CenterPrm.StandY = GetPrivateProfileInt(str_model,"STANDOFFSETY",-1,str_ModelPath);
	if(CenterPrm.StandY == -1){
		CenterPrm.StandY = Cam_PosY;
		str.Empty();
		str.Format("%d",CenterPrm.StandY);
		WritePrivateProfileString(str_model,"STANDOFFSETY",str,str_ModelPath);
	}
	CenterPrm.PassDeviatX = GetPrivateProfileDouble(str_model,"PASSDEVIATX",-1,str_ModelPath);
	if(CenterPrm.PassDeviatX == -1){
		CenterPrm.PassDeviatX = 3.5;
		str.Empty();
		str.Format("%6.2f",CenterPrm.PassDeviatX);
		WritePrivateProfileString(str_model,"PASSDEVIATX",str,str_ModelPath);
	}
	CenterPrm.PassDeviatY = GetPrivateProfileDouble(str_model,"PASSDEVIATY",-1,str_ModelPath);
	if(CenterPrm.PassDeviatY == -1){
		CenterPrm.PassDeviatY = 3.5;
		str.Empty();
		str.Format("%6.2f",CenterPrm.PassDeviatY);
		WritePrivateProfileString(str_model,"PASSDEVIATY",str,str_ModelPath);
	}
	
	CenterPrm.PassMaxDevX = GetPrivateProfileDouble(str_model,"MAXPASSDEVX",-1,str_ModelPath);
	if(CenterPrm.PassMaxDevX == -1){
		CenterPrm.PassMaxDevX = 1.0;
		str.Empty();
		str.Format("%6.2f",CenterPrm.PassMaxDevX);
		WritePrivateProfileString(str_model,"MAXPASSDEVX",str,str_ModelPath);
	}
	CenterPrm.PassMaxDevY = GetPrivateProfileDouble(str_model,"MAXPASSDEVY",-1,str_ModelPath);
	if(CenterPrm.PassMaxDevY == -1){
		CenterPrm.PassMaxDevY = 1.0;
		str.Empty();
		str.Format("%6.2f",CenterPrm.PassMaxDevY);
		WritePrivateProfileString(str_model,"MAXPASSDEVY",str,str_ModelPath);
	}

	CenterPrm.RateX = GetPrivateProfileDouble(str_model,"RATEX",-1,str_ModelPath);
	if(CenterPrm.RateX == -1){
		CenterPrm.RateX = 1.0;
		str.Empty();
		str.Format("%6.2f",CenterPrm.RateX);
		WritePrivateProfileString(str_model,"RATEX",str,str_ModelPath);
	}
	CenterPrm.RateY = GetPrivateProfileDouble(str_model,"RATEY",-1,str_ModelPath);
	if(CenterPrm.RateY == -1){
		CenterPrm.RateY = 1.0;
		str.Empty();
		str.Format("%6.2f",CenterPrm.RateY);
		WritePrivateProfileString(str_model,"RATEY",str,str_ModelPath);
	}
	CenterPrm.EnPixX = GetPrivateProfileInt(str_model,"ENPIXX",-1,str_ModelPath);
	if(CenterPrm.EnPixX == -1){
		CenterPrm.EnPixX = 10;
		str.Empty();
		str.Format("%d",CenterPrm.EnPixX);
		WritePrivateProfileString(str_model,"ENPIXX",str,str_ModelPath);
	}
	CenterPrm.EnPixY = GetPrivateProfileInt(str_model,"ENPIXY",-1,str_ModelPath);
	if(CenterPrm.EnPixY == -1){
		CenterPrm.EnPixY = 10;
		str.Empty();
		str.Format("%d",CenterPrm.EnPixY);
		WritePrivateProfileString(str_model,"ENPIXY",str,str_ModelPath);
	}
	CenterPrm.WriteMode = GetPrivateProfileInt(str_model,"WRITEMODE",-1,str_ModelPath);
	if(CenterPrm.WriteMode == -1){
		CenterPrm.WriteMode = 0;
		str.Empty();
		str.Format("%d",CenterPrm.WriteMode);
		WritePrivateProfileString(str_model,"WRITEMODE",str,str_ModelPath);
	}
	
	if(((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd != NULL){
		((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd->pFileEdit->SetWindowText(BinfileName);
	}
//	((CImageTesterDlg  *)m_pMomWnd)->m_pResWorkerOptWnd->Filename_Value(BinfileName);
	



	
	//=================================================================================
	m_RunMode = GetPrivateProfileInt(str_model,"RunMode",-1,str_ModelPath);
	if(m_RunMode == -1){
		m_RunMode = 1;
		str.Empty();
		str.Format("%d",m_RunMode);
		WritePrivateProfileString(str_model,"RunMode",str,str_ModelPath);
	}
	m_RunMode=1;

	pRunCombo->SetCurSel(m_RunMode);
	pRunCombo->GetLBText(m_RunMode,str);
	ModeName = str;
	if(((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd != NULL){
		((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd->pRunEdit->SetWindowText(str);
		((CComboBox *)GetDlgItem(IDC_OPT_WRMODE))->GetLBText(CenterPrm.WriteMode,str);
		((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd->pWrModeEdit->SetWindowText(str);
	}

	CenterPrm.Optical_Radius = GetPrivateProfileDouble(str_model,"OpticalRadius",-1,str_ModelPath);
	if(CenterPrm.Optical_Radius == -1){
		CenterPrm.Optical_Radius = 1440;
	str.Empty();
	str.Format("%6.2f",CenterPrm.Optical_Radius);
	WritePrivateProfileString(str_model,"OpticalRadius",str,str_ModelPath);
	}
	i_Sensitivity = GetPrivateProfileInt(str_model,"Sensitivity",-1,str_ModelPath);
	if(i_Sensitivity == -1){
		i_Sensitivity = 997;
		str.Empty();
		str.Format("%f",i_Sensitivity);
		WritePrivateProfileString(str_model,"Sensitivity",str,str_ModelPath);

	}

	m_Sensitivity = double((1000-i_Sensitivity))/1000.0;
	CenterPrm.Sensitivity  = m_Sensitivity;


	CString strTitle="";

	for(int t=0; t<4; t++){
		if(t ==0){
			strTitle.Empty();
			strTitle="LEFT_TOP_";
		}
		if(t ==1){
			strTitle.Empty();
			strTitle="RIGHT_TOP_";		
		}
		if(t ==2){
			strTitle.Empty();
			strTitle="LEFT_BOTTOM_";
		}
		if(t ==3){
			strTitle.Empty();
			strTitle="RIGHT_BOTTOM_";
		}
		C_RECT[t].m_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,str_ModelPath);
		if(C_RECT[t].m_PosX == -1){
			OnBnClickedButtonDefaultrect();
		}else{
			//C_Original[t].m_PosX = C_RECT[t].m_PosX;
			C_RECT[t].m_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,str_ModelPath);
			//C_Original[t].m_PosY = C_RECT[t].m_PosY;
			C_RECT[t].m_Left=GetPrivateProfileInt(str_model,strTitle+"StartX",-1,str_ModelPath);
			C_RECT[t].m_Top=GetPrivateProfileInt(str_model,strTitle+"StartY",-1,str_ModelPath);
			C_RECT[t].m_Right=GetPrivateProfileInt(str_model,strTitle+"ExitX",-1,str_ModelPath);
			C_RECT[t].m_Bottom=GetPrivateProfileInt(str_model,strTitle+"ExitY",-1,str_ModelPath);
			C_RECT[t].m_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,str_ModelPath);
			C_RECT[t].m_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,str_ModelPath);	
		}
	}
	UploadList();
	UpdateData(TRUE);
	m_iStandX = CenterPrm.StandX;
	m_iStandY = CenterPrm.StandY;
	m_dPassDeviatX = CenterPrm.PassDeviatX;
	m_dPassDeviatY = CenterPrm.PassDeviatY;
	m_iMaxCount = CenterPrm.MaxCount;
	m_iStandX = CenterPrm.StandX;
	m_iStandY = CenterPrm.StandY;
	m_dPassDeviatX = CenterPrm.PassDeviatX;
	m_dPassDeviatY = CenterPrm.PassDeviatY;
	m_dPassMaxDevX = CenterPrm.PassMaxDevX;
	m_dPassMaxDevY = CenterPrm.PassMaxDevY;
	m_dRateX = CenterPrm.RateX;
	m_dRateY = CenterPrm.RateY;
	m_iEnPixX = CenterPrm.EnPixX;
	m_iEnPixY = CenterPrm.EnPixY;
	m_iWRMODE = CenterPrm.WriteMode;

	m_Optical_radius = CenterPrm.Optical_Radius;
	str_Sensitivity.Format("%d",i_Sensitivity);
	UpdateData(FALSE);

	SetFileSel();//bin 파일을 읽어들인후에 경로를 확인한다.
	
//	SetDlgItemInt(IDC_Chart_X, m_ptMasterCoord[0].x);
//	SetDlgItemInt(IDC_CHART_Y, m_ptMasterCoord[0].y);
}
void COPTIC_DETECT_Option::OnBnClickedOpticalstatSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	CenterPrm.MaxCount= m_iMaxCount;
	CenterPrm.RateX= m_dRateX;
	CenterPrm.RateY= m_dRateY;
	Save_parameter();
	
}

void COPTIC_DETECT_Option::OnBnClickedOpticalstatSave2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	CenterPrm.StandX= m_iStandX;
	CenterPrm.StandY= m_iStandY;
	CenterPrm.PassDeviatX= m_dPassDeviatX;
	CenterPrm.PassDeviatY= m_dPassDeviatY;
	CenterPrm.PassMaxDevX= m_dPassMaxDevX;
	CenterPrm.PassMaxDevY= m_dPassMaxDevY;

	CenterPrm.WriteMode= m_iWRMODE;

	CString str = "";
	if(((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd != NULL){
		((CComboBox *)GetDlgItem(IDC_OPT_WRMODE))->GetLBText(CenterPrm.WriteMode,str);
		((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd->pWrModeEdit->SetWindowText(str);
	}
	Save_parameter();
}

void COPTIC_DETECT_Option::OnCbnSelchangeComboMode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	m_RunMode = pRunCombo->GetCurSel();
	pRunCombo->GetLBText(m_RunMode,str);

	if(((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd != NULL){
		((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd->pRunEdit->SetWindowText(str);
	}
	/*if(m_EtcNumber != 0){
		((CImageTesterDlg  *)m_pMomWnd)->m_psubmodel[m_EtcNumber]->NameChange(str);
	}*/

	str.Format("%d",m_RunMode);
	WritePrivateProfileString(str_model,"RunMode",str,str_ModelPath);
	if(m_RunMode == 0){

	}else{

	}
}

int COPTIC_DETECT_Option::OnSendSerial(_EtcWrdata * INDATA){
	int ret = 0;
	ret = MicomOnSendSerial(INDATA);
	return ret;
				}
				
int COPTIC_DETECT_Option::OffsetWrite(_TOTWrdata * INDATA)
{

	unsigned long nRet = 0, i,j,k,AddressPage, ApplyOffset[4]={0,0,0,0};// ApplyOffset[4]={X,Y,DE,AC}
	unsigned long BuffSize=256;//MAXSIZE,CNTSIZE,
	unsigned char EndOfWrite=0, LOOP = 32;
	unsigned int loopnum = 0, knum = 0, CRC=0;

	DoEvents(1000);

	BYTE SerialDATA[20] ={'!',0x07, 0,0,0,0, 0,0,0,0,'@'};

	((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd->m_CtrFileProgress.SetPos(0);
//	((CImageTesterDlg  *)m_pMomWnd)->m_pResWorkerOptWnd->m_CtrFileProgress.SetPos(0);

	SerialDATA[1] =0x07; SerialDATA[2] =0x90; SerialDATA[3]=0x03;SerialDATA[6]=0xFF;SerialDATA[7]=0xFF;SerialDATA[8]=0xFF;SerialDATA[9]=0xFF;
	if(!SendData8Byte(SerialDATA, 11,INDATA)){  
		return FALSE;	
	}  //  1ms  BL Mode 진입
	DoEvents(60);

	SerialDATA[1] =0x07;   SerialDATA[2] =0x91; 
	if(!SendData8Byte(SerialDATA, 11,INDATA)){  
		return FALSE;	
	}  //  2ms   Get Info
	DoEvents(60);

	SerialDATA[1] =0x07;     SerialDATA[2] =0x92; SerialDATA[3]=0xa5; SerialDATA[4]=0xF1; SerialDATA[5]=0xFF;//DATA[6]=0xFF;DATA[7]=0xFF;DATA[8]=0xFF;DATA[9]=0xFF;
	if(!SendData8Byte(SerialDATA, 11,INDATA)){  
		return FALSE;	
	}  //  2ms  Flash_Keys
	DoEvents(60);

	if((AddOfOffset[1] >> 12)==0x00){
		AddressPage = 0;
		LOOP = 16;
	}
	else {
		AddressPage = (AddOfOffset[1] >> 8)&0xFFFFFFF0;
		LOOP = 32;
	}

	for(i=0;i<LOOP;i++, AddressPage++){//
		OffsetBuffer[i+32-LOOP][256];
		memcpy(INDATA->t_EtcCh[0].buffer,OffsetBuffer[i+32-LOOP],256);
		memcpy(INDATA->t_EtcCh[1].buffer,OffsetBuffer[i+32-LOOP],256);
		memcpy(INDATA->t_EtcCh[2].buffer,OffsetBuffer[i+32-LOOP],256);
		memcpy(INDATA->t_EtcCh[3].buffer,OffsetBuffer[i+32-LOOP],256);

		if((B_FileChk==TRUE)&&(m_filemode != 0)){//파일의 옵셋값이 존재할때만 적용된다.
			if((AddOfOffset[0]>=AddressPage*BuffSize)&&(AddOfOffset[0]<(AddressPage+1)*BuffSize)){				
				for(int lop = 0;lop<4;lop++){	
					INDATA->t_EtcCh[lop].buffer[AddOfOffset[0]%BuffSize] =  xoffdata + INDATA->t_EtcCh[lop].m_offsetX;
				}
			}

			if((AddOfOffset[1]>=AddressPage*BuffSize)&&(AddOfOffset[1]<(AddressPage+1)*BuffSize)){			
				for(int lop = 0;lop<4;lop++){	
					INDATA->t_EtcCh[lop].buffer[AddOfOffset[1]%BuffSize] =  yoffdata + INDATA->t_EtcCh[lop].m_offsetY;
				}
			}

			if((AddOfOffset[2]>=AddressPage*BuffSize)&&(AddOfOffset[2]<(AddressPage+1)*BuffSize)){				
				if(m_filemode == 2){		//return = 2이면 메가이다. 
					for(int lop = 0;lop<4;lop++){
						if(INDATA->t_EtcCh[lop].m_offsetX+xoffdata < 0){
							if(INDATA->t_EtcCh[lop].m_offsetY+yoffdata < 0){
								INDATA->t_EtcCh[lop].buffer[AddOfOffset[2]%BuffSize] =  m_DChkSum1 -2; //둘다 -일때
							}else{
								INDATA->t_EtcCh[lop].buffer[AddOfOffset[2]%BuffSize] =  m_DChkSum1 -1; //둘중 하나만 -일때
							}
						}else{
							if(INDATA->t_EtcCh[lop].m_offsetY+yoffdata < 0){
								INDATA->t_EtcCh[lop].buffer[AddOfOffset[2]%BuffSize] =  m_DChkSum1 -1; //둘중 하나만 -일때
							}else{
								INDATA->t_EtcCh[lop].buffer[AddOfOffset[2]%BuffSize] =  m_DChkSum1; //둘다 +일때
							}
						}
					}
				}else{ //메가가 아닌 일반일경우 
					for(int lop = 0;lop<4;lop++){	
						INDATA->t_EtcCh[lop].buffer[AddOfOffset[2]%BuffSize] =  m_DChkSum1; //둘다 +일때
					}
				}
			}
			
			if((AddOfOffset[3]>=AddressPage*BuffSize)&&(AddOfOffset[3]<(AddressPage+1)*BuffSize)){				
				for(int lop = 0;lop<4;lop++){
					INDATA->t_EtcCh[lop].buffer[AddOfOffset[3]%BuffSize] =  m_DChkSum2 - (INDATA->t_EtcCh[lop].m_offsetX) - (INDATA->t_EtcCh[lop].m_offsetY);
				}
			}
		}

		SerialDATA[1] =0x07; SerialDATA[2] =0x93; SerialDATA[3] =0x00; SerialDATA[4] =0x00;SerialDATA[5] =0x00;SerialDATA[6] =0x00;
		SerialDATA[3] = 0x00;
		SerialDATA[5] = (AddressPage>> 0x00) & 0x000000FF ;
		SerialDATA[6] = (AddressPage>> 0x08) & 0x000000FF ;
		SerialDATA[4] = (AddressPage>> 0x10) & 0x000000FF ;  //??

		SerialDATA[8] =0x00;SerialDATA[9] =0x00;

		if(!SendData8Byte(SerialDATA, 11,INDATA)){  
			return FALSE;	
		}//set Address
		((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd->m_CtrFileProgress.SetPos((unsigned int)(((i+1)*1000)/LOOP));
//		((CImageTesterDlg  *)m_pMomWnd)->m_pResWorkerOptWnd->m_CtrFileProgress.SetPos((unsigned int)(((i+1)*1000)/LOOP));
		DoEvents(30); // 2ms

		SerialDATA[1] = 0x08;  // Set Data Mode  0x08

		for(j=0;j < 32; j++){ //1 page = 32 * 1 packet = 32* 8 = 256 //1 packet = 8 byte
			for(k=0;k<8;k++){ 
				for(k=0;k<8;k++){
					for(int lop = 0;lop<4;lop++){
						INDATA->t_EtcCh[lop].DATA[k+2] = INDATA->t_EtcCh[lop].buffer[j*8+k];
					}
				}
				if(!EtcSendData8Byte(INDATA, 11)){  
					return FALSE;	
				}
			}
		}
	//	DoEvents(150);
		DoEvents(m_dwd1pdelay);	
		if(AddressPage%16==0){
//			DATA[1] =0x07; DATA[2] =0x94; DATA[3]=0x00;
//			if(!SendData8Byte(DATA, 11,INDATA)){  
//				return FALSE;	
//			}
//			DoEvents(400);
			DoEvents(m_dwd16pdelay);
		}
	}

	SerialDATA[1] =0x07; SerialDATA[2] =0x97; SerialDATA[3]=0x3D; SerialDATA[4]=0xC2; SerialDATA[5]=0xA5; SerialDATA[6]=0x1B; SerialDATA[7]=0xFF; SerialDATA[8]=0xFF; SerialDATA[9]=0xFF;
	if(!SendData8Byte(SerialDATA, 11,INDATA)){  
		return FALSE;	
	}  //  2ms  Write Signature
	DoEvents(60);

	SerialDATA[1] =0x07; SerialDATA[2] =0x92; SerialDATA[3]=0x00; SerialDATA[4]=0x00; //DATA[5]=0xA5; DATA[6]=0x1B; DATA[7]=0xFF; DATA[8]=0xFF; DATA[9]=0xFF;
	if(!SendData8Byte(SerialDATA, 11,INDATA)){  
		return FALSE;	
	}  //  2ms  Flash_Keys
	DoEvents(60);

	SerialDATA[1] =0x07;  SerialDATA[2] =0x98; SerialDATA[3]=0x00;
	if(!SendData8Byte(SerialDATA, 11,INDATA)){  
		return FALSE;	
	}  //  100ms  Software Reset
	DoEvents(500);
	((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd->m_CtrFileProgress.SetPos(0);
//	((CImageTesterDlg  *)m_pMomWnd)->m_pResWorkerOptWnd->m_CtrFileProgress.SetPos(0);
	DoEvents(500);

	return TRUE;
}



BOOL COPTIC_DETECT_Option::SendData8Byte(unsigned char * TxData, int Num,_TOTWrdata * INDATA)
{
	
	BOOL b_Ret;

	return b_Ret;
}

BOOL COPTIC_DETECT_Option::EtcSendData8Byte(_TOTWrdata * INDATA,int Num)
{
	char ret = 0;
	BOOL b_Ret = FALSE;
	for(int lop = 0;lop<4;lop++){
		if(INDATA->t_EtcCh[lop].m_Enable == TRUE){
			INDATA->t_EtcCh[lop].DATA[0] = '!';
			INDATA->t_EtcCh[lop].DATA[1] = 0x08;
			INDATA->t_EtcCh[lop].DATA[10] = '@';
//			ret = ((CImageTesterDlg  *)m_pMomWnd)->m_pEtcCh[lop]->SendData8Byte(INDATA->t_EtcCh[lop].DATA,Num);
			if(ret == FALSE){
				INDATA->t_EtcCh[lop].m_Enable = FALSE;
				INDATA->t_EtcCh[lop].m_success = FALSE;
			}else{
				b_Ret = TRUE;
			}
		}
	}
	return b_Ret;
}

void COPTIC_DETECT_Option::centergen(double centx,double centy,_EtcWrdata *EtcDATA)
{
//	int mbuf;
	double TmpX, TmpY;
	double bufx, bufy;
	char m_Offsetx_buf;
	char m_Offsety_buf;
	CString str="";
	char m_F_Offsetx_buf;
	char m_F_Offsety_buf;
	
	long bufcenterx = (long)((centx+0.005) *100);
	long bufcentery = (long)((centy+0.005) *100);

	double d_F_X_ASIX = (double)bufcenterx/100;
	double d_F_Y_ASIX = m_CAM_SIZE_HEIGHT-(double)bufcentery/100;
	
	EtcDATA->m_F_X_ASIX = d_F_X_ASIX;
	EtcDATA->m_F_Y_ASIX = d_F_Y_ASIX;

	if(CenterPrm.WriteMode == 0){ //좌우반전이므로 계측된 x축은 반대다.
		bufx = m_CAM_SIZE_WIDTH - d_F_X_ASIX;//계측 광축 X 360근처
		bufy = d_F_Y_ASIX;//계측 광축 Y 240근처
	}else if(CenterPrm.WriteMode == 1){//상하반전이므로 계측된 y축은 반대다. 
		bufx = d_F_X_ASIX;
		bufy = m_CAM_SIZE_HEIGHT - d_F_Y_ASIX;//계측 광축 Y 240근처
	}else if(CenterPrm.WriteMode == 2){//무반전이므로 계측된 x와 y를 그대로 적용한다. 
		bufx = d_F_X_ASIX;
		bufy = d_F_Y_ASIX;
	}else if(CenterPrm.WriteMode == 3){//로테이트이므로 x와 y 둘다 반대로 적용된다. 
		bufx = m_CAM_SIZE_WIDTH - d_F_X_ASIX;
		bufy = m_CAM_SIZE_HEIGHT - d_F_Y_ASIX;
	}else{
		bufx = m_CAM_SIZE_WIDTH - d_F_X_ASIX;//이 부분에 모드에 따라 표현되는 방식이 틀려진다.
		bufy = d_F_Y_ASIX;
	}
	
	if(EtcDATA->m_Init_X == 99999 && EtcDATA->m_Init_Y == 99999)
	{
		EtcDATA->m_Init_X = bufx;
		EtcDATA->m_Init_Y= bufy;
	}

	EtcDATA->m_X_ASIX = bufx;
	EtcDATA->m_Y_ASIX = bufy;

	TmpX = (bufx - CenterPrm.StandX); //물리적으로 광축이 벗어난 정도 
	if((TmpX) > 0.0)	m_Offsetx_buf = (char)((TmpX)+0.5) ;//+ 0x10  ; //+ 0x1E/2 = 15[dec]
	else m_Offsetx_buf = (char)((TmpX)-0.5) ;//+ 0x10  ; //+ 0x1E/2 = 15[dec]
	
	TmpY = (bufy - CenterPrm.StandY);
	if((TmpY) > 0.0) m_Offsety_buf = (char)((TmpY)+0.5); //+ 0x16/2 = 11[dec]
	else  m_Offsety_buf = (char)((TmpY) - 0.5); //+ 0x16/2 = 11[dec]

	char m_Offsetx_buf2 = (unsigned char)(m_Offsetx_buf * CenterPrm.RateX);
	char m_Offsety_buf2 = (unsigned char)(m_Offsety_buf * CenterPrm.RateY);
	
	EtcDATA->m_X_OFFSET = m_Offsetx_buf;
	EtcDATA->m_Y_OFFSET = m_Offsety_buf;
	
	if(EtcDATA->m_Init_Offset_X == 99999 && EtcDATA->m_Init_Offset_Y == 99999)
	{
		EtcDATA->m_Init_Offset_X = m_Offsetx_buf;
		EtcDATA->m_Init_Offset_Y = m_Offsety_buf;
	}


	if(((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd != NULL){
		str.Format("%d",m_Offsetx_buf);
		((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd->OffsetX_Txt(EtcDATA->m_number,str);
		str.Format("%d",m_Offsety_buf);
		((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd->OffsetY_Txt(EtcDATA->m_number,str);
		str.Format("%5.2f",EtcDATA->m_X_ASIX);
		((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd->AxisX_Txt(EtcDATA->m_number,str);
		str.Format("%5.2f",EtcDATA->m_Y_ASIX);
		((CImageTesterDlg  *)m_pMomWnd)->m_pResCOpticPointOptWnd->AxisY_Txt(EtcDATA->m_number,str);
	}
	
	//중심점 검출후 합격 범위안에 들어오면 성공표시를 하고 테스트를 스톱한다.
	if(EtcDATA->m_X_ASIX <= (CenterPrm.StandX + CenterPrm.PassDeviatX)&&
	   EtcDATA->m_X_ASIX >= (CenterPrm.StandX - CenterPrm.PassDeviatX)&&
	   EtcDATA->m_Y_ASIX >= (CenterPrm.StandY - CenterPrm.PassDeviatY)&&
	   EtcDATA->m_Y_ASIX <= (CenterPrm.StandY + CenterPrm.PassDeviatY)){
	   EtcDATA->m_success = TRUE;
		if((EtcDATA->m_count >= CenterPrm.MaxCount)||
			(EtcDATA->m_X_ASIX <= (CenterPrm.StandX + CenterPrm.PassMaxDevX)&&
			EtcDATA->m_X_ASIX >= (CenterPrm.StandX - CenterPrm.PassMaxDevX)&&
			EtcDATA->m_Y_ASIX >= (CenterPrm.StandY - CenterPrm.PassMaxDevY)&&
			EtcDATA->m_Y_ASIX <= (CenterPrm.StandY + CenterPrm.PassMaxDevY)))
		{
			EtcDATA->m_Enable = FALSE;
		}else{
			EtcDATA->m_count++;
			EtcDATA->m_oldoffsetX = EtcDATA->m_offsetX;
			EtcDATA->m_oldoffsetY = EtcDATA->m_offsetY;
			//EtcDATA->m_offsetX += (-1)*(m_Offsetx_buf2);//펌웨어 다운로드의 경우 검출값을 그대로 쓴다.
			//EtcDATA->m_offsetY += (-1)*(m_Offsety_buf2);
			EtcDATA->m_offsetX = (-1)*(m_Offsetx_buf2);//펌웨어 다운로드의 경우 검출값을 그대로 쓴다.
			EtcDATA->m_offsetY = (-1)*(m_Offsety_buf2);
			EtcDATA->m_Enable = TRUE;
		}
	}else{//실패할경우 write에 적용할 수치를 적용하고 끝낸다. 
		if(EtcDATA->m_count >= CenterPrm.MaxCount){
			EtcDATA->m_success = FALSE;
			EtcDATA->m_Enable = FALSE;
		}else{
			EtcDATA->m_count++;
			EtcDATA->m_oldoffsetX = EtcDATA->m_offsetX;
			EtcDATA->m_oldoffsetY = EtcDATA->m_offsetY;
			//EtcDATA->m_offsetX += (-1)*(m_Offsetx_buf2);
			//EtcDATA->m_offsetY += (-1)*(m_Offsety_buf2);
			EtcDATA->m_offsetX += (-1)*(m_Offsetx_buf2);//펌웨어 다운로드의 경우 검출값을 그대로 쓴다.
			EtcDATA->m_offsetY += (-1)*(m_Offsety_buf2);
		}
	}
}

void COPTIC_DETECT_Option::Initgen(_EtcWrdata *EtcDATA)
{
	memset(EtcDATA->buffer,0,sizeof(EtcDATA->buffer));
	memset(EtcDATA->DATA,0,sizeof(EtcDATA->DATA));
	EtcDATA->m_Enable = TRUE;
	EtcDATA->m_success = FALSE;
	EtcDATA->m_X_ASIX = 0;
	EtcDATA->m_Y_ASIX = 0;
	EtcDATA->m_F_X_ASIX = 0;
	EtcDATA->m_F_Y_ASIX = 0;
	EtcDATA->m_X_OFFSET = 0;
	EtcDATA->m_Y_OFFSET = 0;
	EtcDATA->m_offsetX = 0;
	EtcDATA->m_offsetY = 0;
	EtcDATA->m_oldoffsetX = 0;
	EtcDATA->m_oldoffsetY = 0;
	EtcDATA->str_state = "";
	EtcDATA->m_Init_Offset_X = 99999;
	EtcDATA->m_Init_Offset_Y = 99999;
	
	EtcDATA->m_Init_X = 99999;
	EtcDATA->m_Init_Y = 99999;
	EtcDATA->m_count = 0;

	EtcPt[0].x= 0;
	EtcPt[0].y= 0;
}

void COPTIC_DETECT_Option::MoveCenter(void) 
{
	StatePrintf("오프셋 기본값으로 셋팅합니다.");
	((CImageTesterDlg  *)m_pMomWnd)->m_pMicomOpticWnd->DefaultInitwr();
}

int COPTIC_DETECT_Option::MicomOnSendSerial(_EtcWrdata * INDATA) 
{	

	((CImageTesterDlg  *)m_pMomWnd)->m_pMicomOpticWnd->edit_offsetX.Format("%d",INDATA->m_offsetX);
	((CImageTesterDlg  *)m_pMomWnd)->m_pMicomOpticWnd->edit_offsetY.Format("%d",INDATA->m_offsetY);
	((CImageTesterDlg  *)m_pMomWnd)->m_pMicomOpticWnd->UpdateData(FALSE);

	if(!((CImageTesterDlg  *)m_pMomWnd)->m_pMicomOpticWnd->SettingPosWrite()){
		return FALSE;
	}

	return TRUE;
}



void COPTIC_DETECT_Option::OpticalAsixModify(_EtcWrdata * INDATA)
{
	bool mloop = 0;
	Initgen(INDATA);
	//	나중에 중간에 정지 시킬수 잇는 스톱 버튼을 만든다.
	if(((CImageTesterDlg  *)m_pMomWnd)->b_StopCommand == TRUE){
		StatePrintf("강제 종료 되었습니다.");
		INDATA->m_Enable = FALSE;
		INDATA->m_success = FALSE;
		INDATA->str_state = "강제 종료";
		return;
	}
	
	MoveCenter();//명령이 입력되면 카메라가 알아서 리셋되는가?
	DoEvents(300);

	if(!((CImageTesterDlg  *)m_pMomWnd)->CAMERA_STAT_CHK()){
		INDATA->m_Enable = FALSE;
		INDATA->m_success = FALSE;
		INDATA->str_state = "CAMERA OFF";
		return;
	}

	//카메라 리셋을 추가한다.
	int lopcnt = 0;
	int rebuf = 0;
	int TSTCNT = 0;
	BOOL DLSTAT = TRUE;
	while(lopcnt <= (CenterPrm.MaxCount)){//4개 포트가 모두 TRUE일때 계속해서 돈다. 
		DLSTAT = Find_offset(INDATA);
		if(((CImageTesterDlg  *)m_pMomWnd)->b_StopCommand == TRUE){
			StatePrintf("강제 종료 되었습니다.");
			INDATA->m_Enable = FALSE;
			INDATA->m_success = FALSE;
			INDATA->str_state = "강제 종료";
			return;
		}
		
		if(INDATA->m_Enable == TRUE){//아직 테스트를 계속진행해야 할경우
			if(DLSTAT == TRUE){
				TSTCNT++;
				StatePrintf("%d 번째 광축조정(왜곡광축)을 진행합니다",TSTCNT);
				rebuf = MicomOnSendSerial(INDATA);//펌웨어 다운로드 알고리즘을 넣는다. 
				DoEvents(300);
				if(!rebuf){//write를 진행한다.
					if(((CImageTesterDlg  *)m_pMomWnd)->b_StopCommand == TRUE){
						if(INDATA->m_Enable == TRUE){
							INDATA->m_Enable = FALSE;
							INDATA->m_success = FALSE;
							INDATA->str_state = "강제 종료";
						}

						return;
					}else{
						if(lopcnt == (CenterPrm.MaxCount)){
							StatePrintf("광축값 적용을 실패하였습니다.");
							if(INDATA->m_Enable == TRUE){
								INDATA->m_Enable = FALSE;
								INDATA->m_success = FALSE;
								INDATA->str_state = "OFFSET WRITE 실패";
							}
							return;
						}else{
							StatePrintf("광축값 적용을 실패하였습니다. 다시 시도합니다.");

						}
					}
				}
				DoEvents(300);
			}else if(lopcnt == (CenterPrm.MaxCount)){
				StatePrintf("광축값 적용을 실패하였습니다.");
				if(INDATA->m_Enable == TRUE){
					INDATA->m_Enable = FALSE;
					INDATA->m_success = FALSE;
					INDATA->str_state = "OFFSET WRITE 실패";
				}
				return;
			}else{
				StatePrintf("광축값 측정을 실패하였습니다. 다시 시도합니다.");

			}
//			((CImageTesterDlg  *)m_pMomWnd)->Camera_Reset();
		}else{//테스트가 모두 끝났을 경우
			lopcnt = 100;
		}
		lopcnt++;
	}
}

BOOL COPTIC_DETECT_Option::ChkTstEnd(_TOTWrdata * INDATA)
{
	for(int lop = 0;lop<4;lop++){
		if(INDATA->t_EtcCh[lop].m_Enable == TRUE){
			return TRUE;
		}
	}
	return FALSE;
}

BOOL COPTIC_DETECT_Option::ChkTstEnd(_EtcWrdata * INDATA)
{
	if(INDATA->m_Enable == TRUE){
		return TRUE;
	}
	return FALSE;
}
void COPTIC_DETECT_Option::Center_Pic(CDC* cdc){
	for(int t=0; t<4; t++){
		Center_Pic(cdc,t);
	}
	if(g_pImage != NULL){
		if(g_stat == 0){	
			CDC *pcDC2 = m_SubFrame.GetDC();//m_pStandWnd->m_SubFrame.GetDC();
			ShowIplImage(g_pImage, pcDC2->m_hDC);
			m_SubFrame.ReleaseDC(pcDC2);
		}
	}
}
void COPTIC_DETECT_Option::ShowIplImage(IplImage *src, HDC hDCDst)
{
   uchar buffer[sizeof(BITMAPINFOHEADER) + 1024];
   BITMAPINFO* bmi = (BITMAPINFO*)buffer;
   
   ::SetStretchBltMode(hDCDst, COLORONCOLOR);//MemDC.m_hDC//pcDC->m_hDC1
	FillBitmapInfo(bmi, m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT, (src->depth & 255) * src->nChannels); //에러가 나는 이유?>
   ::StretchDIBits(hDCDst,0,  0,180, 120,0, 0,m_CAM_SIZE_WIDTH,m_CAM_SIZE_HEIGHT,
						src->imageData, bmi, DIB_RGB_COLORS, SRCCOPY);
}	

void  COPTIC_DETECT_Option::FillBitmapInfo( BITMAPINFO* bmi,int width, int height, int bpp )
{
	int g_nColorbytes = 3;

    BITMAPINFOHEADER* bmih = &(bmi->bmiHeader);
    if (!bpp) bpp = g_nColorbytes * 8;

    memset( bmih, 0, sizeof(*bmih));
    bmih->biSize   = sizeof(BITMAPINFOHEADER); 
    bmih->biWidth  = width;
    bmih->biHeight = -abs(height);
    bmih->biPlanes = 1; 
    bmih->biBitCount = bpp;
    bmih->biCompression = BI_RGB;

    if( bpp == 8 )
    { // 팔레트를 매번 초기화하는 게 불합리하나 여기 과거 공개된(찾기 귀찮아 참조 생략하니 용서를..) 소스를 따름.
        RGBQUAD* palette = bmi->bmiColors;
        int i;
        for( i = 0; i < 256; i++ )
        {
            palette[i].rgbBlue = palette[i].rgbGreen = palette[i].rgbRed = (BYTE)i;
            palette[i].rgbReserved = 0;
        }
    }
}
void COPTIC_DETECT_Option::Center_Pic(CDC* cdc,int NUM)
{
	
	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);


	::SetBkMode(cdc->m_hDC,TRANSPARENT);

	//******************************************************************************
	CPen my_Pan, my_Pan2, *old_pan;
//***********************초기 원점 탐색 영역******************************//
	my_Pan.CreatePen(PS_SOLID,3,RGB(255, 255, 255));
	old_pan = cdc->SelectObject(&my_Pan);
	//cdc->MoveTo((Cam_PosX*0.75), (Cam_PosY*0.625));
	//cdc->LineTo((Cam_PosX*1.25),(Cam_PosY*0.625));
	//cdc->LineTo((Cam_PosX*1.25),(Cam_PosY*1.375));
	//cdc->LineTo((Cam_PosX*0.75),(Cam_PosY*1.375));
	//cdc->LineTo((Cam_PosX*0.75),(Cam_PosY*0.625));

	
	cdc->MoveTo(C_RECT[NUM].m_Left,C_RECT[NUM].m_Top);

	cdc->LineTo(C_RECT[NUM].m_Right,C_RECT[NUM].m_Top);
	cdc->LineTo(C_RECT[NUM].m_Right,C_RECT[NUM].m_Bottom);
	cdc->LineTo(C_RECT[NUM].m_Left,C_RECT[NUM].m_Bottom);
	cdc->LineTo(C_RECT[NUM].m_Left,C_RECT[NUM].m_Top);
	cdc->SelectObject(old_pan);
	old_pan->DeleteObject();
	my_Pan.DeleteObject();
	
	
	int x1 = EtcPt[0].x;
	int y1 = EtcPt[0].y;

	int x= CenterVal.m_X_OFFSET;
	int y= CenterVal.m_Y_OFFSET;
	CString TEXTDATA ="";

	CFont font,*old_font;
	font.CreatePointFont(220, "Arial"); 
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);


	old_font = cdc->SelectObject(&font);
	if((EtcPt[0].x >0)||(EtcPt[0].y >0)){
	
		if((x1 == 0)&&(y1 == 0)){
			TEXTDATA.Format("중심점을 찾지 못하였습니다.");
			cdc->SetTextColor(RGB(255, 0, 0));
		}else{
			TEXTDATA.Format("OFFSET X = %d   OFFSET Y = %d", x, y);
			if(b_TESTResult == TRUE){
				cdc->SetTextColor(RGB(0, 0, 255));
			}else{
				cdc->SetTextColor(RGB(255, 0, 0));
			}
		}

	}else{
		TEXTDATA.Format("중심점을 찾지 못하였습니다.");
		cdc->SetTextColor(RGB(255, 0, 0));
	}
	cdc->TextOut(Cam_PosX , (Cam_PosY+200),TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	cdc->SelectObject(old_font);
	old_font->DeleteObject();
	font.DeleteObject();
//*********************************************************************

}

bool COPTIC_DETECT_Option::Find_offset(_EtcWrdata * INDATA)
{
	INDATA->m_number = 0;

	double centx,centy;
	//((CImageTesterDlg  *)m_pMomWnd)->REG_STANDARD_SETUP();

	if(!((CImageTesterDlg  *)m_pMomWnd)->CAMERA_STAT_CHK()){
		INDATA->m_Enable = FALSE;
		INDATA->m_success = FALSE;
		INDATA->str_state = "CAMERA OFF";
		return FALSE;
	}

	if(INDATA->m_Enable == TRUE){//동작이 허가되었을때만 시도한다.			
		//여기에 광축 측정 알고리즘을 넣는

		double OldOpticCenterX = 0;
		double OldOpticCenterY = 0;
		double ***xs, ***ms,***Hs, ***Rs ;
		double **RtnH, **RtnRs;
		double p[9], *Err, Theta_incoming[N_Point],phi_incoming[N_Point];
		int	   ErrorCount=0,ErrorPicture=0;
		long i,j;
		int img_change = 0;
		double Finalp[15],***FinalRs;
		double **Hs1, RtnTs[3],**Ts,**FinalTs;
		int camcnt = 0;

		while((!((CImageTesterDlg  *)m_pMomWnd)->CAMERA_STAT_CHK())&&(camcnt < 100)){
			DoEvents(10);
			camcnt++;
		}
			
		if (camcnt >= 100) {//카메라가 연결되어 있지 않으면 카메라를 연결
			StatePrintf("카메라가 연결되어 있지 않습니다");
			return FALSE;
		}

		BYTE   buf_ori[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT] = {'0', }; //y영역을 저장할 임시 버퍼	
		//IplImage* img_yuv = 0;

		Hs = (double ***)malloc(sizeof(double **)*1); //   
		for(i=0;i<1;i++){
			Hs[i]=(double **)malloc(sizeof(double *)* 3);
			for(j=0;j<3;j++)
				Hs[i][j]=(double *)malloc(sizeof(double )* 3);
		}

		Rs = (double ***)malloc(sizeof(double **)*1);//   
		for(i=0;i<1;i++){
			Rs[i]=(double **)malloc(sizeof(double *)* 3);
			for(j=0;j<3;j++)
				Rs[i][j]=(double *)malloc(sizeof(double )* 3);
		}

		FinalRs = (double ***)malloc(sizeof(double **)*1);//   
		for(i=0;i<1;i++){
			FinalRs[i]=(double **)malloc(sizeof(double *)* 3);
			for(j=0;j<3;j++)
				FinalRs[i][j]=(double *)malloc(sizeof(double )* 3);
		}

		Err = (double *)malloc(sizeof(double )*1); 

		FinalTs = (double **)malloc(sizeof(double *)*1);                     //   
		for(i=0;i<1;i++)FinalTs[i]=(double *)malloc(sizeof(double )* 3);

		RtnH = (double **)malloc(sizeof(double *)*3);                     //   
		for(i=0;i<3;i++)RtnH[i]=(double *)malloc(sizeof(double )* 3);

		Ts = (double **)malloc(sizeof(double *)*1);                     //   
		for(i=0;i<1;i++)Ts[i]=(double *)malloc(sizeof(double )* 3);

		RtnRs = (double **)malloc(sizeof(double *)*3);                     //   
		for(i=0;i<3;i++)RtnRs[i]=(double *)malloc(sizeof(double )* 3);

		//////////////////////////동적 메모리할당////////////////////////////////////////////////////////
		xs = (double ***)malloc(sizeof(double **)*1);                     //   
		ms = (double ***)malloc(sizeof(double **)*1);                     //   

		for(int i=0;i<1;i++){
			xs[i]=(double **)malloc(sizeof(double *)* N_Point);
			ms[i]=(double **)malloc(sizeof(double *)* N_Point);
			for(j=0;j<N_Point;j++){
				xs[i][j]=(double *)malloc(sizeof(double )* 3);
				ms[i][j]=(double *)malloc(sizeof(double )* 2);
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////

		//-----------------
		// Capture
		//-----------------
		OldOpticCenterX = OpticCenterX;
		OldOpticCenterY = OpticCenterY;
		OpticCenterX = 0;
		OpticCenterY = 0;

		/*CCHART	*m_pChartWnd = ((CImageTesterDlg  *)m_pMomWnd)->m_pChartWnd;

		if(m_pChartWnd == NULL){
			//차트가 없으면 만드는 알고리즘을 추가?
			return FALSE;
		}

		m_pChartWnd->ChartImgClr();//차트 이미지 지움 테스트

		DoEvents(800);
		((CImageTesterDlg  *)m_pMomWnd)->m_YUVSCAN = 1; //OnMs_VCaptEvent에서 m_YUVORG버퍼에 들어온 영상 데이터를 저장한다.
		DoEvents(200);
		PreProcessor(m_YUVIn); //차트 클리어 상태에서 흑백이미지를 추출한다.*/
		//for(i=0;i<1;i++){   ////khk

			//----------------------------------------------------------------------------
		/*	int imgcnt = 0;
			m_pChartWnd->ChartChange(img_change,0,1); 
			DoEvents(800);
			((CImageTesterDlg  *)m_pMomWnd)->m_YUVSCAN = 1;//OnMs_VCaptEvent에서 m_YUVORG버퍼에 들어온 영상 데이터를 저장한다.
			DoEvents(200);
			EtcPreProcessor(m_YUVIn);//이미지가 들어올 영역을 채크한다.	

			m_pChartWnd->ChartChange(img_change,1,1); 
			DoEvents(800);
			((CImageTesterDlg  *)m_pMomWnd)->m_YUVSCAN = 1; //OnMs_VCaptEvent에서 m_YUVORG버퍼에 들어온 영상 데이터를 저장한다. 
			DoEvents(200);

			EnImgProcessor(m_YUVIn);
			img_yuv=cvCreateImage(cvSize(720,480), 8, 1); //iplImage 구조체 선언 향후 버퍼가 들어갈 곳
			
			img_change++;
			for(j=0; j<720*480;j++) {
				buf_ori[j]=m_EtcORGBUF[j*2+1];//Y값 만 추출하여 저장
			}
			make_iplimage(img_yuv, buf_ori, 720, 480);//iplImage 구조체에 이미지로 변신 
			*/
			IplImage* GrayScaleROIImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
			
		//		if( (GrayScaleROIImage=cvLoadImage("c:\\ROIImage.bmp", CV_LOAD_IMAGE_GRAYSCALE)) == NULL )return 0;//"Snapshot_007.jpg"

			for(int i=0;i<1;i++){
				((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan =1;						
						
				for(int k =0;k<10;k++){
					if(((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan == 0){
						break;
					}else{
						DoEvents(50);
					}
				}	
			
			BYTE R,G,B;
			double Sum_Y;

			for (int y=0; y<m_CAM_SIZE_HEIGHT; y++)
			{
				for (int x=0; x<m_CAM_SIZE_WIDTH; x++)
				{
					B = m_RGBScanbuf[y*(m_CAM_SIZE_WIDTH*4) + x*4    ];
					G = m_RGBScanbuf[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 1];
					R = m_RGBScanbuf[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 2];
					
				
					Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
					
					GrayScaleROIImage->imageData[y*GrayScaleROIImage->widthStep+x] = Sum_Y;						
				}
			}	

		//	GrayScaleROIImage = GetROIImage(m_RGBScanbuf); 
		//		cvSaveImage("C:\\ROIImage.bmp", GrayScaleROIImage);
		//	centerPt; //보임량 광축
		//	centerPt.m_resultX = x좌표
		//	centerPt.m_resultY = y좌표

			

			g_stat = 1;
			if(g_pImage !=	NULL){
				cvReleaseImage(&g_pImage);
				g_pImage = NULL;
			}
			g_pImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH,m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3 );
			g_stat = 0;
			cvCvtColor(GrayScaleROIImage, g_pImage, CV_GRAY2BGR );


		//	if( (GrayScaleROIImage=cvLoadImage("c:\\ROIImage.bmp", CV_LOAD_IMAGE_GRAYSCALE)) == NULL )return 0;//"Snapshot_007.jpg"
			ReadPicturePoint(GrayScaleROIImage, ms[i], xs[i], &ErrorCount) ; // Read Cell Point & Error Check

			if(ErrorCount>1){
				i--;
ErrorDetect :  // 에러 발생시 본 Capture Image Skip 
				ErrorPicture++;
				if(ErrorPicture>7){
					ErrorPicture=100;
					for(i=0;i<1;i++){
						for(j=0;j<N_Point;j++){
							free(ms[i][j]);   
							free(xs[i][j]);   
						}
						free(ms[i]);   
						free(xs[i]);   
					} 

					for(i=0;i<1;i++){
						for(j=0;j<3;j++) free(Hs[i][j]);   
						free(Hs[i]);   
					} free(Hs); 
					
					for(i=0;i<1;i++){
						for(j=0;j<3;j++) free(Rs[i][j]);   
						free(Rs[i]);   
					} free(Rs); 

					for(i=0;i<1;i++){
						for(j=0;j<3;j++) free(FinalRs[i][j]);   
						free(FinalRs[i]);   
					} free(FinalRs); 

					for(i=0;i<3;i++)free(RtnH[i]);   free(RtnH); 
					for(i=0;i<3;i++)free(RtnRs[i]);   free(RtnRs);
					for(i=0;i<1;i++)free(FinalTs[i]);   free(FinalTs);
					for(i=0;i<1;i++)free(Ts[i]);   free(Ts);

					free(ms); 
					free(xs); 
					free(Err);
					cvReleaseImage(&GrayScaleROIImage);
					cvReleaseImage(&g_pImage);
					

					EtcPt[0].x = 0;
					EtcPt[0].y = 0;

					return FALSE;
				}
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			ThetaMax = ViewField/2.0*pi/180.0  ;
			CalCoefficient(&p[0]);  // Return 값 :  A[1],A[0],mres,rmax,mu,mv,u0,v0; 

			RadialProjection(&p[0]) ; // Evaluate ThetaMax and ThetaFirstMax  //2.8885, 332번째 

		//		p[0] = -0.0517;   p[1] = 1.2695; p[2] = 3.5039; p[3] = 1.5459; p[4] = 474.9879; p[5] = 474.9879;
		//		p[6] = 640.0; p[7] = 360.0;



		///////////////////////Find Hs : 각 사진별 최소 에러 변환 행렬(3x3) 찾기 ////////////////////
		//이부분
			for(int Loop=0;Loop<1;Loop++){
				for(i=0;i<N_Point;i++){

					xs[Loop][i][2]=1.0;

				}
				Err[Loop]=BackProjectGeneric(RtnH, ms[Loop], xs[Loop], &p[0], ThetaMax, &Theta_incoming[0], &phi_incoming[0] , N_Point);

				Hs1=RtnH;
				InitialiseExternalp(RtnRs, RtnTs, Hs1);

				for(i=0;i<3;i++){
					for(j=0;j<3;j++){
						Hs[Loop][i][j]=RtnH[i][j];
						Rs[Loop][i][j]=RtnRs[i][j];
					}
					Ts[Loop][i]=RtnTs[i];
				}
			}

		///////////////////////Find Hs : 각 사진별 최소 에러 변환 행렬(3x3) 찾기 ////////////////////

		MinimiseProjErrs(FinalRs, FinalTs, Finalp, ms, xs,Rs, Ts, &p[0]); //***FinalRs, **FinalTs, *Finalp

		if(fabs(Finalp[4]-m_CAM_SIZE_WIDTH/2)>30||fabs(Finalp[5]-m_CAM_SIZE_HEIGHT/2)>30) 
			goto ErrorDetect;
		OpticCenterX=Finalp[4]; OpticCenterY=Finalp[5];  //좌우반전 등 영상 출력 형태 고려한 광축 변환 전에, 추출된 원 영상의 광축값

		EtcPt[0].x = int(OpticCenterX);
		EtcPt[0].y = int(OpticCenterY);	
		centergen(OpticCenterX,OpticCenterY,INDATA);//검출된 광축 X,Y를 외부에 표시하고 write할 시정수로 변경한다.

		for(i=0;i<1;i++){
			for(j=0;j<N_Point;j++){
				free(ms[i][j]);   
				free(xs[i][j]);   
			}
			free(ms[i]);   
			free(xs[i]);   
		} 

		for(i=0;i<1;i++){
			for(j=0;j<3;j++) free(Hs[i][j]);   
			free(Hs[i]);   
		} free(Hs); 

		for(i=0;i<1;i++){
			for(j=0;j<3;j++) free(Rs[i][j]);   
			free(Rs[i]);   
		} free(Rs); 

		for(i=0;i<1;i++){
			for(j=0;j<3;j++) free(FinalRs[i][j]);   
			free(FinalRs[i]);   
		} free(FinalRs); 

		for(i=0;i<3;i++)free(RtnH[i]);   free(RtnH); 
		for(i=0;i<3;i++)free(RtnRs[i]);   free(RtnRs);
		for(i=0;i<1;i++)free(FinalTs[i]);   free(FinalTs);
		for(i=0;i<1;i++)free(Ts[i]);   free(Ts);

		free(ms); 
		free(xs); 
		free(Err);
		cvReleaseImage(&GrayScaleROIImage);
		cvReleaseImage(&g_pImage);
	/*	memset(m_RGBScanbuf,0,sizeof(m_RGBScanbuf));
		((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan = TRUE;
		
		for(int i =0;i<10;i++){
			if(((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan == FALSE){
				break;
			}else{
				DoEvents(50);
			}
		}

		EtcPt[0] = GetCenterPoint(m_RGBScanbuf);
		centx = EtcPt[0].x;
		centy = EtcPt[0].y;
		m_dCurrCenterCoord.x = centx;
		m_dCurrCenterCoord.y =  centy;
		
		centergen(centx,centy,INDATA);
		CenterVal.str_state.Format("X- %3.1f ,Y- %3.1f",centx,centy);
		if	((centx > CenterPrm.StandX + 200)||
			(centx < CenterPrm.StandX - 200)||
			(centy > CenterPrm.StandY + 200)||
			(centy < CenterPrm.StandY - 200)){
			if(((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd != NULL){
				((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd->OffsetX_Txt(0,"");
				((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd->OffsetY_Txt(0,"");
				((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd->AxisX_Txt(0,"측정실패");
				((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd->AxisY_Txt(0,"측정실패");
				EtcPt[0].x = 0;
				EtcPt[0].y = 0;
				INDATA->str_state = "광축측정실패";
				INDATA->m_success = FALSE;
				INDATA->m_Enable = FALSE;
			}
		}	*/
	}
}

/*
void COPTIC_DETECT_Option::Find_offset(_TOTWrdata * INDATA)
{
	double centx,centy;
	//
	//for(int lop = 0;lop<4;lop++){//카메라가 켜져있는지 확인한다.
	//	if(!((CImageTesterDlg  *)m_pMomWnd)->CAMERA_STAT_CHK(lop)){
	//		if(((CImageTesterDlg  *)m_pMomWnd)->CameraState[lop] == TRUE)
	//		{
	//			((CImageTesterDlg  *)m_pMomWnd)->CameraState[lop] = TRUE;			
	//		}
	//		else{
	//			((CImageTesterDlg  *)m_pMomWnd)->CameraState[lop] = FALSE;
	//		}
	//		if(INDATA->t_EtcCh[lop].m_Enable == TRUE){
	//			INDATA->t_EtcCh[lop].m_Enable = FALSE;
	//			INDATA->t_EtcCh[lop].m_success = FALSE;
	//			INDATA->t_EtcCh[lop].str_state = "CAMERA OFF";
	//		}
	//	}
	//	else{
	//		((CImageTesterDlg  *)m_pMomWnd)->CameraState[lop] = TRUE;
	//	}
	//}
	//
	for(int lop = 0;lop<4;lop++){
		if(INDATA->t_EtcCh[lop].m_Enable == TRUE){//동작이 허가되었을때만 시도한다.			
			//여기에 광축 측정 알고리즘을 넣는다.
			((CImageTesterDlg  *)m_pMomWnd)->m_pChartWnd->ChartReset();
			((CImageTesterDlg  *)m_pMomWnd)->m_pChartWnd->DrawCircle(m_ptMasterCoord[lop].x, m_ptMasterCoord[lop].y, 75, 75);
			((CImageTesterDlg  *)m_pMomWnd)->m_pChartWnd->UpdateWindow();
			DoEvents(500);
			
			for(int lop2 = 0;lop2 < 4;lop2++){
				//((CImageTesterDlg  *)m_pMomWnd)->m_SelCh = lop;
				memset(m_RGBScanbuf,0,sizeof(m_RGBScanbuf));
				((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan = TRUE;
				INDATA->t_EtcCh[lop].m_Enable = TRUE;
				for(int i =0;i<10;i++){
					if(((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan == FALSE){
						break;
					}else{
						DoEvents(50);
					}
				}
				
				EtcPt[lop] = GetCenterPoint(m_RGBScanbuf);
				centx = EtcPt[lop].x;
				centy = EtcPt[lop].y;

//				m_dCurrCenterCoord[lop].x = centx;
//				m_dCurrCenterCoord[lop].y = centy;

				centergen(centx,centy,&INDATA->t_EtcCh[lop]);
				if	((centx <= CenterPrm.StandX + 200)&&
					(centx >= CenterPrm.StandX - 200)&&
					(centy <= CenterPrm.StandY + 200)&&
					(centy >= CenterPrm.StandY - 200)){
					break;	
				}
			}
			if	((centx > CenterPrm.StandX + 200)||
				(centx < CenterPrm.StandX - 200)||
				(centy > CenterPrm.StandY + 200)||
				(centy < CenterPrm.StandY - 200)){
				if(((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd != NULL){
					((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd->OffsetX_Txt(lop,"");
					((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd->OffsetY_Txt(lop,"");
					((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd->AxisX_Txt(lop,"측정실패");
					((CImageTesterDlg  *)m_pMomWnd)->m_pResCPointOptWnd->AxisY_Txt(lop,"측정실패");
					EtcPt[lop].x = 0;
					EtcPt[lop].y = 0;
					INDATA->t_EtcCh[lop].str_state = "광축측정실패";
					INDATA->t_EtcCh[lop].m_success = FALSE;
					INDATA->t_EtcCh[lop].m_Enable = FALSE;
				}
			}
			
		}
	}
	//((CImageTesterDlg  *)m_pMomWnd)->m_pChartWnd->ChartReset();
}
*/

//---------------------------------------------------------------------WORKLIST
void COPTIC_DETECT_Option::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	InsertIndex = m_CenterWorklist.InsertItem(StartCnt,"",0);
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_CenterWorklist.SetItemText(InsertIndex,0,strCnt);

	m_CenterWorklist.SetItemText(InsertIndex,1,((CImageTesterDlg  *)m_pMomWnd)->m_modelName);
	m_CenterWorklist.SetItemText(InsertIndex,2,((CImageTesterDlg  *)m_pMomWnd)->strDay+"");
	m_CenterWorklist.SetItemText(InsertIndex,3,((CImageTesterDlg  *)m_pMomWnd)->strTime+"");




	
	CString str ="";
	if(CenterPrm.WriteMode == 0)
	{
		str = "좌우반전";
	}
	if(CenterPrm.WriteMode == 1)
	{
		str = "상하반전";
	}
	if(CenterPrm.WriteMode == 2)
	{
		str ="오리지날";	
	}
	if(CenterPrm.WriteMode == 3)
	{
		str ="로테이트";	
	}

	m_CenterWorklist.SetItemText(InsertIndex,4,str);
	



}
void COPTIC_DETECT_Option::FAIL_UPLOAD(){
	if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand==TRUE)
	{
			InsertList();
			for (int t=0; t<9; t++)
			{
				m_CenterWorklist.SetItemText(InsertIndex,t+5,"X");

			}
			m_CenterWorklist.SetItemText(InsertIndex,13,"FAIL");
			m_CenterWorklist.SetItemText(InsertIndex,14,"STOP");
			//((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex,7,"STOP");

			StartCnt++;
			b_StopFail = TRUE;

	}
}


void COPTIC_DETECT_Option::Set_List(CListCtrl *List){
	
	int count=0;
	while(List->DeleteColumn(0));
	((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"카메라상태",LVCFMT_CENTER, 80);
	
	List->InsertColumn(5,"init X",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"init Y",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"FINAL X",LVCFMT_CENTER, 80);
	List->InsertColumn(8,"FINAL Y",LVCFMT_CENTER, 80);
	List->InsertColumn(9,"적용 수치 X",LVCFMT_CENTER, 80);
	List->InsertColumn(10,"적용 수치 Y",LVCFMT_CENTER, 80);
	List->InsertColumn(11,"Offset X",LVCFMT_CENTER, 80);
	List->InsertColumn(12,"Offset Y",LVCFMT_CENTER, 80);
	List->InsertColumn(13,"RESULT",LVCFMT_CENTER, 80);

	List->InsertColumn(14,"비고",LVCFMT_CENTER, 80);

	ListItemNum=15;
	Copy_List(&m_CenterWorklist,((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList,ListItemNum);
}

CString COPTIC_DETECT_Option::Mes_Result()
{
	CString sz[8];
	int		nResult[8];

	int nSel = m_CenterWorklist.GetItemCount()-1;

	// 초기측정 X좌표
	sz[0] = m_CenterWorklist.GetItemText(nSel, 5);

	// 초기측정 Y좌표
	sz[1] = m_CenterWorklist.GetItemText(nSel, 6);

	// 초기 측정 offset X
	sz[4] = m_CenterWorklist.GetItemText(nSel, 9);

	// 초기 측정 offset Y
	sz[5] = m_CenterWorklist.GetItemText(nSel, 10);

	if(m_RunMode == 0)				// 광축 조정모드
	{
		// 마지막 측정 X좌표
		sz[2] = m_CenterWorklist.GetItemText(nSel, 7);

		// 마지막 측정 Y좌표
		sz[3] = m_CenterWorklist.GetItemText(nSel, 8);

		// 마지막 측정 offset X
		sz[6] = m_CenterWorklist.GetItemText(nSel, 11);

		// 마지막 측정 offset Y
		sz[7] = m_CenterWorklist.GetItemText(nSel, 12);

	}else if(m_RunMode == 1)		// 광축 측정모드
	{
		// 마지막 측정 X좌표
		sz[2] = m_CenterWorklist.GetItemText(nSel, 5);

		// 마지막 측정 Y좌표
		sz[3] = m_CenterWorklist.GetItemText(nSel, 6);

		// 마지막 측정 offset X
		sz[6] = m_CenterWorklist.GetItemText(nSel, 9);

		// 마지막 측정 offset Y
		sz[7] = m_CenterWorklist.GetItemText(nSel, 10);
	}

	for(int _a=0; _a<8; _a++)
	{
		if(CenterVal.m_success)
			nResult[_a] = 1;
		else
			nResult[_a] = 0;
	}	
	if(b_StopFail == FALSE){
	m_szMesResult.Format(_T("%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d"), sz[0], nResult[0], sz[1], nResult[1], sz[2], nResult[2], sz[3], nResult[3], 
		sz[4], nResult[4], sz[5], nResult[5], sz[6], nResult[6], sz[7], nResult[7]);
	}else{
		m_szMesResult.Format(_T(":1,:1,:1,:1,:1,:1,:1,:1"), sz[0], nResult[0], sz[1], nResult[1], sz[2], nResult[2], sz[3], nResult[3], 
			sz[4], nResult[4], sz[5], nResult[5], sz[6], nResult[6], sz[7], nResult[7]);
	}
	return m_szMesResult;
}

void COPTIC_DETECT_Option::EXCEL_SAVE(){
	CString Item[9]={"카메라상태","init X","init Y","FINAL X","FINAL Y","적용 수치 X","적용 수치 Y","Offset X","Offset Y"};//수정해야할 곳 0203

	CString Data ="";
	CString str="";
	str = "***********************왜곡광축 검사***********************\n";
	Data += str;
	
	str.Empty();
	str.Format("기준광축 X, %d,,,기준광축 Y, %d\n ",CenterPrm.StandX,CenterPrm.StandY);
	Data += str;
	str.Empty();
	str.Format("합격편차 X, %3.1f,,,합격편차 Y, %3.1f\n\n ",CenterPrm.PassMaxDevX,CenterPrm.PassMaxDevY);
	Data += str;
//	str.Empty();
//	str.Format("CPK X, %6.2f,,,CPK Y, %6.2f\n ",SaveCPK_X,SaveCPK_Y);
//	Data += str;
	str.Empty();
	str = "*************************************************\n";
	Data += str;
	((CImageTesterDlg  *)m_pMomWnd)->SAVE_EXCEL("왜곡광축",Item,9,&m_CenterWorklist,Data);
}

bool COPTIC_DETECT_Option::EXCEL_UPLOAD(){
	m_CenterWorklist.DeleteAllItems();
	//if(((CImageTesterDlg  *)m_pMomWnd)->EXCEL_UPLOAD("왜곡광축",&m_CenterWorklist,1) == TRUE){
	//	
	//	for(int t=0; t<((CImageTesterDlg  *)m_pMomWnd)->Totalint; t++){
	//		
	//		//int X = atoi(m_CenterWorklist.GetItemText(t,12));
	//		//int Y = atoi(m_CenterWorklist.GetItemText(t,13));

	//		//CPK_DATA[0][CPKcount]= X;
	//		//CPK_DATA[1][CPKcount]= Y;
	//		
	//		//CPKcount++;
	//		//CPK_DATA_SUM();
	//	}
	//	return TRUE;
	//}
	//else{
	//	return FALSE;
	//}
	//return 0;

	if(((CImageTesterDlg  *)m_pMomWnd)->EXCEL_UPLOAD("왜곡광축",&m_CenterWorklist,1, &StartCnt) == TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return TRUE;
	
}

void COPTIC_DETECT_Option::Setup_List(){
	
	/*CString strCnt ="";
	for(int NUM=0; NUM<4; NUM++){
		if(((CImageTesterDlg  *)m_pMomWnd)->TestData[0][NUM].m_Enable == 1){
			int count = NUM + ((CImageTesterDlg  *)m_pMomWnd)->Totalint;
			
			InsertIndex = m_CenterWorklist.InsertItem(count,"",0);
			strCnt.Empty();
			strCnt.Format(_T("%d"), InsertIndex+1);
			m_CenterWorklist.SetItemText(InsertIndex,0,strCnt);
			
			for(int t=0; t<18; t++){
				m_CenterWorklist.SetItemText(InsertIndex,t+1,TESTLOT[NUM][t+1]);
			}			
		}
	}*/
}

void COPTIC_DETECT_Option::OnCbnSelchangeComboIntwropt()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
/*	m_CheckWrite = pINTWROPTCombo->GetCurSel();
	CString str="";
	str.Format("%d",m_CheckWrite);
	WritePrivateProfileString(str_model,"initWrite",str,str_ModelPath);*/
}
BOOL COPTIC_DETECT_Option::CheckSettingDetect_1()//모두 같으면 TRUE 틀리면 FALSE
{
	/*
	int buf = 0;
	buf = pINTWROPTCombo->GetCurSel();
	if(buf != m_CheckWrite){
		return FALSE; 
	}
	buf = pWROPTCombo->GetCurSel();
	if(buf != m_ChkSector){
		return FALSE;
	}

	buf = ((CComboBox *)GetDlgItem(IDC_OPT_WRMODE))->GetCurSel();
	if(buf != CenterPrm.WriteMode){
		return FALSE;
	}
	
	buf = GetDlgItemInt(IDC_MAXCONTROL_NUMBER);
	if(buf != CenterPrm.MaxCount){
		return FALSE;
	}

	buf = GetDlgItemInt(IDC_RATE_X);
	if(buf != CenterPrm.MaxCount){
		return FALSE;
	}
	*/


	return TRUE;//모두 같으면 PASS
}

void COPTIC_DETECT_Option::OnCbnSelchangeComboWropt2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	/*m_ChkSector = pWROPTCombo->GetCurSel();
	CString str="";
	str.Format("%d",m_ChkSector);
	WritePrivateProfileString(str_model,"SectorWrite",str,str_ModelPath);*/
}

void COPTIC_DETECT_Option::OnBnClickedOpticalstatSave3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	/*CString	   str = "";
	UpdateData(TRUE);
	m_dwd1pdelay = m_dwd1pdelay_buf;
	m_dwd16pdelay = m_dwd16pdelay_buf;
	
	str.Empty();
	str.Format("%d",m_dwd1pdelay);
	WritePrivateProfileString("WRITETIMING","1PAGE",str,((CImageTesterDlg  *)m_pMomWnd)->inifilename);
	str.Empty();
	str.Format("%d",m_dwd16pdelay);
	WritePrivateProfileString("WRITETIMING","16PAGE",str,((CImageTesterDlg  *)m_pMomWnd)->inifilename);
	((CButton *)GetDlgItem(IDC_OPTICALSTAT_SAVE3))->EnableWindow(0);*/
}

void COPTIC_DETECT_Option::OnEnChangeEdit1pdelay()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	//// 이 알림 메시지를 보내지 않습니다.
	//CString str_buf = "";
	//CString str_buf2 = "";
	//GetDlgItemText(IDC_EDIT_1PDELAY,str_buf);
	//DWORD buf_1pdelay = atol(str_buf);
	//GetDlgItemText(IDC_EDIT_16PDELAY,str_buf2);
	//DWORD buf_16pdelay = atol(str_buf2);
	//if((buf_1pdelay == m_dwd1pdelay)&&(buf_16pdelay == m_dwd16pdelay)){
	//	((CButton *)GetDlgItem(IDC_OPTICALSTAT_SAVE3))->EnableWindow(0);
	//}else{
	//	if((str_buf == "")||(str_buf2 == "")){
	//		((CButton *)GetDlgItem(IDC_OPTICALSTAT_SAVE3))->EnableWindow(0);
	//	}else{
	//		((CButton *)GetDlgItem(IDC_OPTICALSTAT_SAVE3))->EnableWindow(1);
	//	}
	//}


	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COPTIC_DETECT_Option::OnEnChangeEdit16pdelay()
{
	//// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	//// CDialog::OnInitDialog() 함수를 재지정 
	////하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	//// 이 알림 메시지를 보내지 않습니다.
	//CString str_buf = "";
	//CString str_buf2 = "";
	//GetDlgItemText(IDC_EDIT_1PDELAY,str_buf);
	//DWORD buf_1pdelay = atol(str_buf);
	//GetDlgItemText(IDC_EDIT_16PDELAY,str_buf2);
	//DWORD buf_16pdelay = atol(str_buf2);
	//if((buf_1pdelay == m_dwd1pdelay)&&(buf_16pdelay == m_dwd16pdelay)){
	//	((CButton *)GetDlgItem(IDC_OPTICALSTAT_SAVE3))->EnableWindow(0);
	//}else{
	//	if((str_buf == "")||(str_buf2 == "")){
	//		((CButton *)GetDlgItem(IDC_OPTICALSTAT_SAVE3))->EnableWindow(0);
	//	}else{
	//		((CButton *)GetDlgItem(IDC_OPTICALSTAT_SAVE3))->EnableWindow(1);
	//	}
	//}
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COPTIC_DETECT_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if(bShow == TRUE){
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = -1;

	}
}

double COPTIC_DETECT_Option::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt( (double)((x2-x1)*(x2-x1)) +  ((y2-y1)*(y2-y1)));

	return result;
}
BOOL COPTIC_DETECT_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
			if(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_RECT_OPTIC))->GetSafeHwnd())//GetSafeHwnd() 자신의 핸들을 가져오는 함수
			{
				int bufSubitem = iSavedSubitem + 1;
				
				if(bufSubitem ==9){
					if(iSavedItem == 0){
						iSavedItem = 1; bufSubitem =1;
					}
					else if(iSavedItem == 1){
						iSavedItem = 2; bufSubitem =1;
					}
					else if(iSavedItem == 2){
						iSavedItem = 3; bufSubitem =1;
					}
					else if(iSavedItem == 3){
						iSavedItem =0; bufSubitem =1;
					}
					
				}
				int bufItem = iSavedItem;
				C_DATALIST.EnsureVisible(iSavedItem, FALSE);
				C_DATALIST.GetSubItemRect(iSavedItem,bufSubitem , LVIR_BOUNDS, rect);
				C_DATALIST.ClientToScreen(rect);
				this->ScreenToClient(rect);
				C_DATALIST.SetSelectionMark(iSavedItem);
				C_DATALIST.SetFocus(); //저장
				iSavedItem = bufItem;
				iSavedSubitem = bufSubitem;
				((CEdit *)GetDlgItem(IDC_EDIT_RECT_OPTIC))->SetWindowText(C_DATALIST.GetItemText(iSavedItem, iSavedSubitem));
				((CEdit *)GetDlgItem(IDC_EDIT_RECT_OPTIC))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
				((CEdit *)GetDlgItem(IDC_EDIT_RECT_OPTIC))->SetFocus();
			}
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}

unsigned int COPTIC_DETECT_Option::ReadPicturePoint(IplImage *srcImage, double **ms1, double **xs1, int *ErrorCount)
{
//	IplImage    *srcImage;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	


	IplImage* dstImage = cvCreateImage( cvGetSize(srcImage), IPL_DEPTH_8U, 3 );
	IplImage* eig_image = cvCreateImage( cvGetSize(srcImage), IPL_DEPTH_32F, 1 );
	IplImage* temp_image = cvCreateImage( cvGetSize(srcImage), IPL_DEPTH_32F, 1 );
	CvPoint2D32f corners[500];
	int    nCornerCount=0; //초기화되지 않으면 오류

	double quality_level = CenterPrm.Sensitivity;// 격자점 인식 민감도 작을수록 많은 점을 찾음.
	int Thr = 30; //화면의 바깥쪽 부분 제외
	double min_distance  = 10;
	int    block_size = 3;
	int    use_harris=1;
	double k=0.04;

	double    fValue;
	double    MaxErr, Err, PredictPoint[2];
	double    maxValue;
	double	  CenterPoint[2];

	double     ***DataTmp;
	int TestIndex;
	uchar *dst, LRTB[2][2];
	
	int       *MinPointIndex, ErrorFlag=0;
	int       x, y, i, j, *PointIndex;
	int       m, n, k1,k2;
	bool      bChange;
	int       nSize = 5; // Window size for finding local maxima

	int aperture_size=3, Count=5, Index,Shift=0, Exit=0;;
	double  **msTmp, **xsTmp, Dx,DDx,DDy, Dy,***DataS;

	int Width_Of_Cross_Clearzone = 40, Hight_Of_Chart = CAM_IMAGE_HEIGHT/2;

#if 1


	
	double ChartPoint[144][2]=
	{   {380,100}, {380,140}, {380,180}, {380,220}, {380,260}, {380,300}, {380,420}, {380,460}, {380,500}, {380,540}, {380,580}, {380,620},
		{420,100}, {420,140}, {420,180}, {420,220}, {420,260}, {420,300}, {420,420}, {420,460}, {420,500}, {420,540}, {420,580}, {420,620},
		{460,100}, {460,140}, {460,180}, {460,220}, {460,260}, {460,300}, {460,420}, {460,460}, {460,500}, {460,540}, {460,580}, {460,620},
		{500,100}, {500,140}, {500,180}, {500,220}, {500,260}, {500,300}, {500,420}, {500,460}, {500,500}, {500,540}, {500,580}, {500,620},
		{540,100}, {540,140}, {540,180}, {540,220}, {540,260}, {540,300}, {540,420}, {540,460}, {540,500}, {540,540}, {540,580}, {540,620},
		{580,100}, {580,140}, {580,180}, {580,220}, {580,260}, {580,300}, {580,420}, {580,460}, {580,500}, {580,540}, {580,580}, {580,620},
		{700,100}, {700,140}, {700,180}, {700,220}, {700,260}, {700,300}, {700,420}, {700,460}, {700,500}, {700,540}, {700,580}, {700,620},
		{740,100}, {740,140}, {740,180}, {740,220}, {740,260}, {740,300}, {740,420}, {740,460}, {740,500}, {740,540}, {740,580}, {740,620},
		{780,100}, {780,140}, {780,180}, {780,220}, {780,260}, {780,300}, {780,420}, {780,460}, {780,500}, {780,540}, {780,580}, {780,620},
		{820,100}, {820,140}, {820,180}, {820,220}, {820,260}, {820,300}, {820,420}, {820,460}, {820,500}, {820,540}, {820,580}, {820,620},
		{860,100}, {860,140}, {860,180}, {860,220}, {860,260}, {860,300}, {860,420}, {860,460}, {860,500}, {860,540}, {860,580}, {860,620},
		{900,100}, {900,140}, {900,180}, {900,220}, {900,260}, {900,300}, {900,420}, {900,460}, {900,500}, {900,540}, {900,580}, {900,620} };

#else


//	double ChartPoint[144][2]=
//	{{    425.00,     305.00},{    425.00,    295.00},{    425.00,    285.00},{    425.00,    275.00},{    425.00,    265.00},{    425.00,    255.00},  /**/ { 425.00,    225.00},{    425.00,    215.00},{    425.00,    205.00},{    425.00,    195.00},{    425.00,    185.00},{    425.00,    175.00},
//	{    415.00,     305.00},{    415.00,    295.00},{    415.00,    285.00},{    415.00,    275.00},{    415.00,    265.00},{    415.00,    255.00},    { 415.00,    225.00},{    415.00,    215.00},{    415.00,    205.00},{    415.00,    195.00},{    415.00,    185.00},{    415.00,    175.00},
//	{    405.00,     305.00},{    405.00,    295.00},{    405.00,    285.00},{    405.00,    275.00},{    405.00,    265.00},{    405.00,    255.00},    { 405.00,    225.00},{    405.00,    215.00},{    405.00,    205.00},{    405.00,    195.00},{    405.00,    185.00},{    405.00,    175.00},
//	{    395.00,     305.00},{    395.00,    295.00},{    395.00,    285.00},{    395.00,    275.00},{    395.00,    265.00},{    395.00,    255.00},    { 395.00,    225.00},{    395.00,    215.00},{    395.00,    205.00},{    395.00,    195.00},{    395.00,    185.00},{    395.00,    175.00},
//	{    385.00,     305.00},{    385.00,    295.00},{    385.00,    285.00},{    385.00,    275.00},{    385.00,    265.00},{    385.00,    255.00},    { 385.00,    225.00},{    385.00,    215.00},{    385.00,    205.00},{    385.00,    195.00},{    385.00,    185.00},{    385.00,    175.00},
//	{    375.00,     305.00},{    375.00,    295.00},{    375.00,    285.00},{    375.00,    275.00},{    375.00,    265.00},{    375.00,    255.00},    { 375.00,    225.00},{    375.00,    215.00},{    375.00,    205.00},{    375.00,    195.00},{    375.00,    185.00},{    375.00,    175.00},
//
//	{    345.00,     305.00},{    345.00,    295.00},{    345.00,    285.00},{    345.00,    275.00},{    345.00,    265.00},{    345.00,    255.00},    { 345.00,    225.00},{    345.00,    215.00},{    345.00,    205.00},{    345.00,    195.00},{    345.00,    185.00},{    345.00,    175.00},
//	{    335.00,     305.00},{    335.00,    295.00},{    335.00,    285.00},{    335.00,    275.00},{    335.00,    265.00},{    335.00,    255.00},    { 335.00,    225.00},{    335.00,    215.00},{    335.00,    205.00},{    335.00,    195.00},{    335.00,    185.00},{    335.00,    175.00},
//	{    325.00,     305.00},{    325.00,    295.00},{    325.00,    285.00},{    325.00,    275.00},{    325.00,    265.00},{    325.00,    255.00},    { 325.00,    225.00},{    325.00,    215.00},{    325.00,    205.00},{    325.00,    195.00},{    325.00,    185.00},{    325.00,    175.00},
//	{    315.00,     305.00},{    315.00,    295.00},{    315.00,    285.00},{    315.00,    275.00},{    315.00,    265.00},{    315.00,    255.00},    { 315.00,    225.00},{    315.00,    215.00},{    315.00,    205.00},{    315.00,    195.00},{    315.00,    185.00},{    315.00,    175.00},
//	{    305.00,     305.00},{    305.00,    295.00},{    305.00,    285.00},{    305.00,    275.00},{    305.00,    265.00},{    305.00,    255.00},    { 305.00,    225.00},{    305.00,    215.00},{    305.00,    205.00},{    305.00,    195.00},{    305.00,    185.00},{    305.00,    175.00},
//	{    295.00,     305.00},{    295.00,    295.00},{    295.00,    285.00},{    295.00,    275.00},{    295.00,    265.00},{    295.00,    255.00},    { 295.00,    225.00},{    295.00,    215.00},{    295.00,    205.00},{    295.00,    195.00},{    295.00,    185.00},{    295.00,    175.00}} ;


	double ChartPoint[144][2]=
	 {{    343.00,     63.00},{    345.00,    103.00},{    343.00,    134.00},{    345.00,    174.00},{    343.00,    205.00},{    345.00,    245.00},{    345.00,    475.00},{    343.00,    515.00},{    345.00,    546.00},{    343.00,    586.00},{    345.00,    617.00},{    343.00,    657.00},
		{    383.00,     65.00},{    384.00,    104.00},{    383.00,    136.00},{    384.00,    175.00},{    383.00,    206.00},{    384.00,    246.00},{    384.00,    474.00},{    383.00,    514.00},{    384.00,    545.00},{    383.00,    585.00},{    384.00,    616.00},{    383.00,    655.00},
		{    414.00,     63.00},{    416.00,    103.00},{    414.00,    134.00},{    415.00,    174.00},{    414.00,    205.00},{    416.00,    245.00},{    416.00,    475.00},{    414.00,    515.00},{    416.00,    546.00},{    414.00,    586.00},{    416.00,    617.00},{    414.00,    657.00},
		{    454.00,     65.00},{    455.00,    104.00},{    454.00,    136.00},{    455.00,    175.00},{    454.00,    206.00},{    455.00,    246.00},{    455.00,    474.00},{    454.00,    514.00},{    455.00,    545.00},{    454.00,    585.00},{    455.00,    616.00},{    454.00,    655.00},
		{    485.00,     63.00},{    486.00,    103.00},{    485.00,    134.00},{    486.00,    174.00},{    485.00,    205.00},{    485.00,    288.00},{    485.00,    432.00},{    485.00,    515.00},{    486.00,    546.00},{    485.00,    586.00},{    486.00,    617.00},{    485.00,    657.00},
		{    524.00,     65.00},{    526.00,    104.00},{    524.00,    136.00},{    526.00,    175.00},{    568.00,    205.00},{    568.00,    288.00},{    568.00,    432.00},{    568.00,    515.00},{    526.00,    545.00},{    524.00,    585.00},{    526.00,    616.00},{    524.00,    655.00},
		{    755.00,     65.00},{    754.00,    104.00},{    755.00,    136.00},{    754.00,    175.00},{    712.00,    205.00},{    712.00,    288.00},{    712.00,    432.00},{    712.00,    515.00},{    754.00,    545.00},{    755.00,    585.00},{    754.00,    616.00},{    755.00,    655.00},
		{    795.00,     63.00},{    794.00,    103.00},{    795.00,    134.00},{    794.00,    174.00},{    795.00,    205.00},{    795.00,    288.00},{    795.00,    432.00},{    795.00,    515.00},{    794.00,    546.00},{    795.00,    586.00},{    794.00,    617.00},{    795.00,    657.00},
		{    826.00,     65.00},{    825.00,    104.00},{    826.00,    136.00},{    825.00,    175.00},{    826.00,    206.00},{    825.00,    246.00},{    825.00,    474.00},{    826.00,    514.00},{    825.00,    545.00},{    826.00,    585.00},{    825.00,    616.00},{    826.00,    655.00},
		{    866.00,     63.00},{    864.00,    103.00},{    866.00,    134.00},{    865.00,    174.00},{    866.00,    205.00},{    865.00,    245.00},{    864.00,    475.00},{    866.00,    515.00},{    865.00,    546.00},{    866.00,    586.00},{    864.00,    617.00},{    866.00,    657.00},
		{    897.00,     65.00},{    896.00,    104.00},{    897.00,    136.00},{    896.00,    175.00},{    897.00,    206.00},{    896.00,    246.00},{    896.00,    474.00},{    897.00,    514.00},{    896.00,    545.00},{    897.00,    585.00},{    896.00,    616.00},{    897.00,    655.00},
		{    937.00,     63.00},{    935.00,    103.00},{    937.00,    134.00},{    935.00,    174.00},{    937.00,    205.00},{    935.00,    245.00},{    935.00,    475.00},{    937.00,    515.00},{    935.00,    546.00},{    937.00,    586.00},{    935.00,    617.00},{    937.00,    657.00}} ;

	
#endif	
	
#if 1	
	centerPt = GetOpticCenterCoordinate(m_RGBScanbuf); //보임량 광축 실시간 측정일때..
	if((fabs(centerPt.m_resultX-CAM_IMAGE_WIDTH/2)>80)||(fabs(centerPt.m_resultY-CAM_IMAGE_HEIGHT/2)>80)){
		*ErrorCount=5;
		cvReleaseImage(&dstImage);
		cvReleaseImage(&eig_image);
		cvReleaseImage(&temp_image);
		return ErrorFlag;
	}
#else
//	centerPt.m_resultX = 580; centerPt.m_resultY = 388; //보임량광축 Test 이미지 일때..
	centerPt.m_resultX = 10; centerPt.m_resultY = 58; //보임량광축 Test 이미지 일때..
	centerPt.m_resultX = 640; centerPt.m_resultY = 360; //보임량광축 Reference Chart 이미지 일때..
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	cvGoodFeaturesToTrack(srcImage, eig_image, temp_image, corners, &nCornerCount, quality_level, min_distance, NULL,
                               block_size, use_harris, k );
	if(nCornerCount<144) {
		*ErrorCount=5;
		cvReleaseImage(&dstImage);
		cvReleaseImage(&eig_image);
		cvReleaseImage(&temp_image);
		return ErrorFlag;
	}

	cvReleaseImage(&temp_image);	cvReleaseImage(&eig_image);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	cvCvtColor(srcImage, dstImage, CV_GRAY2BGR );
	PointIndex=(int *)malloc(sizeof(int)*nCornerCount);
	MinPointIndex=(int *)malloc(sizeof(int)*2);

	msTmp=(double **)malloc(sizeof(double*)*nCornerCount);
	for(i=0;i<nCornerCount;i++)msTmp[i]=(double *)malloc(sizeof(double)*4);
	
	DataTmp = (double ***)malloc(sizeof(double **)*(12+12));                     //   
	for(i=0;i<12+12;i++){
		DataTmp[i]=(double **)malloc(sizeof(double *)* (13+12));
		for(j=0;j<13+12;j++){
			DataTmp[i][j]=(double *)malloc(sizeof(double )* 4);
			DataTmp[i][j][0]=DataTmp[i][j][1]=0;DataTmp[i][j][2]=0;DataTmp[i][j][3]=0;
		}
	}

	DataS = (double ***)malloc(sizeof(double **)*(4));                     // aa  
	for(i=0;i<4;i++){
		DataS[i]=(double **)malloc(sizeof(double *)* (36*2*4));
		for(j=0;j<36*2*4;j++){
			DataS[i][j]=(double *)malloc(sizeof(double )* 4);
			DataS[i][j][0]=DataS[i][j][1]=0;DataS[i][j][2]=0;DataS[i][j][3]=0;
		}
	}

	xsTmp=(double **)malloc(sizeof(double*)*n_Cluster);
	for(i=0;i<n_Cluster;i++)xsTmp[i]=(double *)malloc(sizeof(double)*2);

////////////////////////////////////////////////////////////////////////////////////////

	for(i=0;i<nCornerCount;i++){
		msTmp[i][0] = corners[i].x;
		msTmp[i][1] = corners[i].y;
	}

	MakeCornerMap(msTmp,nCornerCount,CenterPrm.Optical_Radius);//왜곡 보정하여 Data[i][0]~[1]에 넣고, 원래 점들은 Data[i][2]~[3]에 보관

	//int Width_Of_Cross_Clearzone = 30, Hight_Of_Chart = CAM_IMAGE_HEIGHT/2;
	//x = (int)centerPt.m_resultX; y = (int)centerPt.m_resultY; //보임량광축
	
	x = (int)centerPt.m_resultX; y = (int)centerPt.m_resultY; //보임량광축
	Hight_Of_Chart = CAM_IMAGE_HEIGHT - y;
	if(Hight_Of_Chart>(CAM_IMAGE_HEIGHT/2)) {
		Hight_Of_Chart = (CAM_IMAGE_HEIGHT - Hight_Of_Chart) ;  // 보임량 광축을 기준으로, 화면의 작은쪽 y의 길이를 선택
	}

	
	for(i=0,j=0;i<nCornerCount;i++){
		if(((msTmp[i][0] > (x - Width_Of_Cross_Clearzone)) && (msTmp[i][0] < (x + Width_Of_Cross_Clearzone)))  ||
			  ((msTmp[i][1] > (y - Width_Of_Cross_Clearzone)) && (msTmp[i][1] < (y + Width_Of_Cross_Clearzone)))) {
					msTmp[i][0] = 0;	msTmp[i][1] = 0;
		}
		if(((msTmp[i][2] < (x - Hight_Of_Chart)) || (msTmp[i][2] > (x + Hight_Of_Chart)))  ||
			  ((msTmp[i][3] < (y - Hight_Of_Chart)) || (msTmp[i][3] > (y + Hight_Of_Chart)))) {
					msTmp[i][0] = 0;	msTmp[i][1] = 0;
		}
		if((msTmp[i][3] < Thr) || (msTmp[i][3] > (CAM_IMAGE_HEIGHT - Thr))) {
					msTmp[i][0] = 0;	msTmp[i][1] = 0;
		}
	}

	//  0인 점들 제거 알고리즘 // 옵션에 Clear 영역 Pixel 수, Thr 추가 요청  ##1

	CenterPoint[0] = centerPt.m_resultX; CenterPoint[1] = centerPt.m_resultY; //보임량광축

	FixClusterPoint(CenterPoint, msTmp, nCornerCount, n_Cluster);//Center 로부터, 가장 가까운 좌표부터 Data[0]에 저장 

//	CenterPoint[0]=msTmp[0][0]; CenterPoint[1]=msTmp[0][1];
//	FixClusterPoint(CenterPoint, msTmp, nCornerCount, n_Cluster);//Center좌표 찾고, 가장 가까운 좌표부터 Data[0]에 저장 



	PredictPoint[0]=msTmp[0][0]; PredictPoint[1]=msTmp[0][1];
	double TiltCheckData[4][4];
	FindMinDistPoint1(&Index, msTmp, PredictPoint, nCornerCount,1);//Mode 0. 전체중에 가장 가까운점 찾기, Mode 1. 자기자신은 제외하고 가장 가까운점찾기  
	for(i=0;i<4;i++) CopyPoint4(&TiltCheckData[i][0],msTmp[i]);
	for(i=0;i<3;i++) 
		for(j=0;j<3-i;j++) 
			if(TiltCheckData[j][0]>TiltCheckData[j+1][0]) SwapPoint4(&TiltCheckData[j][0],TiltCheckData[j+1]); //msTmp[1]~msTmp[4]까지 x좌표에 대해 오름차순 정렬

	int Tilt_Clockwise=0;
	(TiltCheckData[0][1]+TiltCheckData[1][1]) > (TiltCheckData[2][1]+TiltCheckData[3][1]) ? 
		Tilt_Clockwise =1 : 
		Tilt_Clockwise = 0; //Chart오른쪽이 더 아래로 기울어진 상태(카메라가 시계반대방향으로 Tilt 상태)


	//FindMinDistPoint1(int *Index, double **DataTmp, PredictPoint, nCornerCount);//RefPoint로부터 가장 가까운점 찾기, 최소거리 값 Return

/////////////////////////////////////////  센터에서 제일 가까운점으로부터 좌표 찾기시작 /////////////////////////////////////////////////////////////////////

	double Tmp_Distance=0,dx=0,dy=0;
	int LowCount=0;

	if(Index >=nCornerCount) Index=0;
	if(fabs(msTmp[0][0]-msTmp[Index][0]) > fabs(msTmp[0][1]-msTmp[Index][1])){
		Dx = -fabs(msTmp[0][0]-msTmp[Index][0]);
		Tilt_Clockwise != 1 ? dy = -fabs(msTmp[0][1]-msTmp[Index][1]) : dy = fabs(msTmp[0][1]-msTmp[Index][1]);
	}
	else{
		Dx = -fabs(msTmp[0][1]-msTmp[Index][1]);
		Tilt_Clockwise != 1 ? dy = -fabs(msTmp[0][0]-msTmp[Index][0]) : dy = fabs(msTmp[0][0]-msTmp[Index][0]);
	}
	// 보임량 광축을 기준으로 4개의 Sector로 분류(1~4사분면)
	*ErrorCount=0;
	if(SeparateSector(CenterPoint, msTmp, DataS,nCornerCount)==ERROR) { //void SeparateSector(double CenterPoint ,double **msTmp, double ***DataS, int n);
		*ErrorCount=5;
//*////////////////////////////////////////  출력 ///////////////////////////////////////////
		
		if(b_Chk_Img == 1){
		
			cvCircle(dstImage, cvPoint((int)CenterPoint[0], (int)CenterPoint[1]), 5, CV_RGB(0, 0, 255), 4);
			for(j=0;j<4;j++){
				for(i=0; i<36; i++){  //154  N_Point+1
				
					x = cvRound( DataS[j][i][0]);
					y = cvRound( DataS[j][i][1]);
					cvCircle(dstImage, cvPoint(x, y), 5, CV_RGB(255, 0, 0), 4);
					cvNamedWindow("dstImage", CV_WINDOW_AUTOSIZE);
				cvShowImage("dstImage", dstImage);
				cvWaitKey(0);

				}
				
			}
//				cvNamedWindow("dstImage", CV_WINDOW_AUTOSIZE);
//			cvShowImage("dstImage", dstImage);
//			cvWaitKey(0);
		}
		cvDestroyWindow("dstImage");  

//*////////////////////////////////////////////////////////////////////////////////////////////
	}
	else {
		MakeChessboard(CenterPoint ,msTmp, DataS,144, srcImage);  // 

	//*////////////////////////////////////////  좌표 테이블  ///////////////////////////////////////////


		//double **ms1, double **xs1, 


		for(i=0;i<N_Point;i++){  // 원 차트의 좌표 설정
			xs1[i][0]=ChartPoint[i+15][0];
			xs1[i][1]=ChartPoint[i+15][1];
			ms1[i][0]=msTmp[i+15][2];
			ms1[i][1]=msTmp[i+15][3];
	//		xs1[i][0] = (double)(((i+1)%11)*50);
	//		xs1[i][1] = (double)(((i+1)/11)*50);
		}

	////////////////////////////////////////  출력 ///////////////////////////////////////////
		
		if(b_Chk_Img == 1){
			cvCircle(dstImage, cvPoint((int)CenterPoint[0], (int)CenterPoint[1]), 5, CV_RGB(0, 0, 255), 4);
			for(j=0;j<4;j++){
				for(i=0; i<36; i++){  //154  N_Point+1
				
					x = cvRound( DataS[j][i][0]);
					y = cvRound( DataS[j][i][1]);
					cvCircle(dstImage, cvPoint(x, y), 5, CV_RGB(255, 0, 0), 4);
					cvNamedWindow("dstImage", CV_WINDOW_AUTOSIZE);
					cvShowImage("dstImage", dstImage);
					cvWaitKey(0);

				}
				
			}
//			cvShowImage("dstImage", dstImage);
//			cvWaitKey(0);
			cvDestroyWindow("dstImage");  
		}
	//*////////////////////////////////////////////////////////////////////////////////////////////

	/*////////////////////////////////////////  출력 ///////////////////////////////////////////
		cvCircle(dstImage, cvPoint((int)CenterPoint[0], (int)CenterPoint[1]), 5, CV_RGB(0, 0, 255), 4);
		for(i=0; i<N_Point; i++){  //154  N_Point+1
		
			x = cvRound( ms1[i][0]);
			y = cvRound( ms1[i][1]);
			cvCircle(dstImage, cvPoint(x, y), 5, CV_RGB(0, 0, 255), 4);
			cvNamedWindow("dstImage", CV_WINDOW_AUTOSIZE);
			cvShowImage("dstImage", dstImage);
			cvWaitKey(0);

			x = cvRound( xs1[i][0]);
			y = cvRound( xs1[i][1]);
			cvCircle(dstImage, cvPoint(x, y), 5, CV_RGB(0, 255, 0), 4);
			cvNamedWindow("dstImage", CV_WINDOW_AUTOSIZE);
			cvShowImage("dstImage", dstImage);
			cvWaitKey(0);
		

		}
		 
		cvDestroyWindow("dstImage");  

	//*////////////////////////////////////////////////////////////////////////////////////////////


		g_stat = 1;
		if(g_pImage !=	NULL){
			cvReleaseImage(&g_pImage);
			g_pImage = NULL;
		}
		g_pImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH,m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3 );
		g_stat = 0;
		cvCopyImage(dstImage,g_pImage);
	}
 
	cvReleaseImage(&dstImage); 
	cvReleaseImage(&g_pImage); 

	free(PointIndex); free(MinPointIndex);

	for(i=0;i<12+12;i++){
		for(j=0;j<13+12;j++) free(DataTmp[i][j]);   
		free(DataTmp[i]);   
	} free(DataTmp);

	for(i=0;i<4;i++){
		for(j=0;j<36*2*4;j++) free(DataS[i][j]);   
		free(DataS[i]);   
	} free(DataS); 
	
	for(i=0;i<nCornerCount;i++)free(msTmp[i]); free(msTmp);
	for(i=0;i<n_Cluster;i++)free(xsTmp[i]); free(xsTmp);
	return ErrorFlag;
}


char  COPTIC_DETECT_Option::SeparateSector(double *CenterPoint ,double **Data, double ***DataS, int n)
{
	int i,j, index[4]={0,0,0,0} ;
#if 1
	for(i=0;i<n;i++){  //2134

		if((Data[i][2]>C_RECT[1].m_Left)&&(Data[i][2]<C_RECT[1].m_Right)
			    &&(Data[i][3]<C_RECT[1].m_Bottom)&&(Data[i][3]>C_RECT[1].m_Top)){ 
			// 1사 분면, x가 Center 보다 크고, y가 Center보다 작으면...
			for(j=0;j<4;j++) DataS[0][index[0]][j]=Data[i][j];
			index[0]++;
		}
		if((Data[i][2]>C_RECT[3].m_Left)&&(Data[i][2]<C_RECT[3].m_Right)
			    &&(Data[i][3]<C_RECT[3].m_Bottom)&&(Data[i][3]>C_RECT[3].m_Top)){   
			// 4사 분면, x가 Center 보다 크고, y가 Center보다 크면...
			for(j=0;j<4;j++) DataS[3][index[1]][j]=Data[i][j];
			index[1]++;
		}
		if((Data[i][2]>C_RECT[0].m_Left)&&(Data[i][2]<C_RECT[0].m_Right)
			    &&(Data[i][3]<C_RECT[0].m_Bottom)&&(Data[i][3]>C_RECT[0].m_Top)){ 
			 // 2사 분면, x가 Center 보다 작고, y가 Center보다 작으면...
			for(j=0;j<4;j++) DataS[1][index[2]][j]=Data[i][j];
			index[2]++;
		}
		if((Data[i][2]>C_RECT[2].m_Left)&&(Data[i][2]<C_RECT[2].m_Right)
			    &&(Data[i][3]<C_RECT[2].m_Bottom)&&(Data[i][3]>C_RECT[2].m_Top)){ 
			// 3사 분면, x가 Center 보다 작고, y가 Center보다 크면...
			for(j=0;j<4;j++) DataS[2][index[3]][j]=Data[i][j];
			index[3]++;
		}
	}
#else
	for(i=0;i<n;i++){
		if(Data[i][0]>CenterPoint[0]){
			if(Data[i][1]<CenterPoint[1]){  // 1사 분면, x가 Center 보다 크고, y가 Center보다 작으면...
				for(j=0;j<4;j++) DataS[0][index[0]][j]=Data[i][j];
				index[0]++;
			}
			else {  // 4사 분면, x가 Center 보다 크고, y가 Center보다 크면...
				for(j=0;j<4;j++) DataS[3][index[1]][j]=Data[i][j];
				index[1]++;
			}
		}
		else { 
			if(Data[i][1]<CenterPoint[1]){  // 2사 분면, x가 Center 보다 작고, y가 Center보다 작으면...
				for(j=0;j<4;j++) DataS[1][index[2]][j]=Data[i][j];
				index[2]++;
			}
			else {  // 3사 분면, x가 Center 보다 작고, y가 Center보다 크면...
				for(j=0;j<4;j++) DataS[2][index[3]][j]=Data[i][j];
				index[3]++;
			}
		}
	}
#endif

//	return 1;
	if((index[0]>=37)&&(index[1]>=37)&&(index[2]>=37)&&(index[3]>=37)) {return TRUE;}
	else {return ERROR ;}
}

void  COPTIC_DETECT_Option::MakeChessboard(double *CenterPoint ,double **Data, double ***DataS, int n,IplImage *srcImage )
{
	int i,j,k, index[4]={0,0,0,0};
	int IndexX, IndexY, Direction_Scan_X[4], Direction_Scan_Y[4];
	double **DataSub,TempData[6][6][4], DataAll[12][12][4];
//	DataSub=DataS[0];
	double Dx=0,Dy=0, Dx_Ref,Dy_Ref,DirectionX,DirectionY, PredictPoint[2]; 
	int Index=0;
	int x,y;
	FILE *fp;

	IplImage* dstImage = cvCreateImage( cvGetSize(srcImage), IPL_DEPTH_8U, 3 );
	

	for(i=0;i<4;i++){
		
		DataSub=DataS[i]; Dx=0; Dy=0;// 변수 초기화


//*////////////////////////////////////////////////////////////////////////////////////////////

		switch (i){ // x, y의 증가 방향, Center로 부터 멀어지는 점들의 좌표값이 증가 또는 감소하는 것을 각 4 분면에 대해 정의
			case 0 : 
				DirectionX = 1.0 ;  DirectionY = -1.0; 
				break;
			case 1 :
				DirectionX = -1.0 ;  DirectionY = -1.0; 
				break;
			case 2 : 
				DirectionX = -1.0 ;  DirectionY = 1.0; 
				break;
			case 3 : 
				DirectionX = 1.0 ;  DirectionY = 1.0; 
				break;
		}
		/////////// Dx, Dy 구하기 ///////////////
		Dx = fabs(DataSub[0][0] - DataSub[1][0]) ;
		if ( Dx < fabs(DataSub[0][0] - DataSub[2][0])) Dx = fabs(DataSub[0][0] - DataSub[2][0]);

		Dy = fabs(DataSub[0][1] - DataSub[1][1]) ;
		if ( Dy < fabs(DataSub[0][1] - DataSub[2][1])) Dy = fabs(DataSub[0][1] - DataSub[2][1]);

		Dx *= DirectionX;	Dy *= DirectionY;
		
//
		/////////// Dx, Dy 구하기 ///////////////
		//TempData[6][6][4]
		CopyPoint4(TempData[0][0],DataSub[0]); 
		PredictPoint[0]=DataSub[0][0]; PredictPoint[1]=DataSub[0][1];  // Start Point

		PredictPoint[0] =DataSub[0][0]+ Dx;	PredictPoint[1] = DataSub[0][1];
		FindMinDistPoint1(&Index, DataSub, PredictPoint, 36,0); 
		CopyPoint4(TempData[1][0],DataSub[Index]);   // (x,y) <==> (1,0) Data[1,0]

		PredictPoint[0] = DataSub[0][0]; PredictPoint[1] =  DataSub[0][1]+Dy; 
		FindMinDistPoint1(&Index, DataSub, PredictPoint, 36,0); 
		CopyPoint4(TempData[0][1],DataSub[Index]);   // (x,y) <==> (0,1) Data[0,1]

		PredictPoint[0] = TempData[1][0][0]; PredictPoint[1] = TempData[0][1][1]; 
		FindMinDistPoint1(&Index, DataSub, PredictPoint, 36,0); 
		CopyPoint4(TempData[1][1],DataSub[Index]);   // (x,y) <==> (1,1) Data[1,1]

		Dx_Ref = (TempData[1][0][0] - TempData[0][0][0]) ;
		Dy_Ref = (TempData[0][1][1] - TempData[0][0][1]) ;
//		Dx_Ref = (TempData[1][0][0] - TempData[0][0][0]) /2;
//		Dy_Ref = (TempData[0][1][1] - TempData[0][0][1]) /2;

		PredictPoint[0] = TempData[0][1][0]; PredictPoint[1] = TempData[0][1][1] + Dy_Ref; 
		FindMinDistPoint1(&Index, DataSub, PredictPoint, 36,0); 
		CopyPoint4(TempData[0][2],DataSub[Index]);   // (x,y) <==> (0,2) Data[0,2]

		PredictPoint[0] = TempData[1][1][0]+ (TempData[1][1][0]-TempData[1][0][0]); PredictPoint[1] = TempData[0][2][1]; 
		FindMinDistPoint1(&Index, DataSub, PredictPoint, 36,0); 
		CopyPoint4(TempData[1][2],DataSub[Index]);   // (x,y) <==> (1,2) Data[1,2]

		PredictPoint[0] = TempData[0][2][0]; PredictPoint[1] = TempData[0][2][1]+(TempData[0][2][1]-TempData[0][1][1]); 
		FindMinDistPoint1(&Index, DataSub, PredictPoint, 36,0); 
		CopyPoint4(TempData[0][3],DataSub[Index]);   // (x,y) <==> (0,3) Data[0,3]
		
		PredictPoint[0] = TempData[1][2][0]+ (TempData[1][2][0]-TempData[1][1][0]); PredictPoint[1] = TempData[1][2][1]+ (TempData[1][2][1]-TempData[1][1][1]); 
		FindMinDistPoint1(&Index, DataSub, PredictPoint, 36,0); 
		CopyPoint4(TempData[1][3],DataSub[Index]);   // (x,y) <==> (1,3) Data[1,3]

		//////////
		PredictPoint[0] = TempData[0][3][0]+ (TempData[0][3][0]-TempData[0][2][0]); PredictPoint[1] = TempData[0][3][1]+ (TempData[0][3][1]-TempData[0][2][1]);
		FindMinDistPoint1(&Index, DataSub, PredictPoint, 36,0); 
		CopyPoint4(TempData[0][4],DataSub[Index]);   // (x,y) <==> (0,4) Data[0,4]
		
		PredictPoint[0] = TempData[1][3][0]+ (TempData[1][3][0]-TempData[1][2][0]); PredictPoint[1] = TempData[1][3][1]+ (TempData[1][3][1]-TempData[1][2][1]); 
		FindMinDistPoint1(&Index, DataSub, PredictPoint, 36,0); 
		CopyPoint4(TempData[1][4],DataSub[Index]);   // (x,y) <==> (1,4) Data[1,4]

		PredictPoint[0] = TempData[0][4][0]+ (TempData[0][4][0]-TempData[0][3][0]); PredictPoint[1] = TempData[0][4][1]+ (TempData[0][4][1]-TempData[0][3][1]);
		FindMinDistPoint1(&Index, DataSub, PredictPoint, 36,0); 
		CopyPoint4(TempData[0][5],DataSub[Index]);   // (x,y) <==> (0,5) Data[0,5]
		
		PredictPoint[0] = TempData[1][4][0]+ (TempData[1][4][0]-TempData[1][3][0]); PredictPoint[1] = TempData[1][4][1]+ (TempData[1][4][1]-TempData[1][3][1]); 
		FindMinDistPoint1(&Index, DataSub, PredictPoint, 36,0); 
		CopyPoint4(TempData[1][5],DataSub[Index]);   // (x,y) <==> (1,5) Data[1,5]
		/////////////

		for(j=2;j<6;j++){
			for(k=0;k<6;k++){
				switch (k){
					case 0 : 
						if(j==2) {
							PredictPoint[0] = TempData[1][1][0]+(TempData[1][2][0]-TempData[0][2][0]); PredictPoint[1] = TempData[1][1][1]-(TempData[1][2][1]-TempData[1][1][1]);; 
						}
						else {
							PredictPoint[0] = TempData[j-1][0][0]+(TempData[j-1][0][0]-TempData[j-2][0][0]); PredictPoint[1] = TempData[j-1][0][1]; 
						}
						break;

					case 1 : 
						PredictPoint[0] = TempData[j][k-1][0]; PredictPoint[1] = TempData[j][k-1][1]+(TempData[1][2][1]-TempData[1][1][1]); 
						break;

					default : 
						PredictPoint[0] = TempData[j][k-1][0]+(TempData[j][k-1][0]-TempData[j][k-2][0]); PredictPoint[1] = TempData[j][k-1][1]+(TempData[j][k-1][1]-TempData[j][k-2][1]); 
				}
				FindMinDistPoint1(&Index, DataSub, PredictPoint, 36,0); 
				CopyPoint4(TempData[j][k],DataSub[Index]);   // (x,y) <==> (j,k) Data[j,k]
			}
		}

		switch(i){
			case 0 :
				IndexX=6;	IndexY=0;
				break;
			case 1 :
				IndexX=0;	IndexY=0;				
				break;
			case 2 :
				IndexX=0;	IndexY=6;
				break;
			case 3 :
				IndexX=6;	IndexY=6;
				break;
		}

		// DataSub=DataS[i]; Dx=0; Dy=0;// 변수 초기화
		for(j=0;j<6;j++) {
			for(k=0;k<6;k++) {

				switch(i){
					case 0 : 
						CopyPoint4(DataSub[j*6+k],TempData[j][6-k-1]);
						break;
					case 1 : 
						CopyPoint4(DataSub[(6-j-1)*6+k],TempData[j][6-k-1]);
						break;
					case 2 : 
						CopyPoint4(DataSub[(6-j-1)*6+k],TempData[j][k]);
						break;
					case 3 : 
						CopyPoint4(DataSub[j*6+k],TempData[j][k]);
						break;
					default : 
						break;
				}
			}
		}
			
		
		for(j=0;j<6;j++){        // 복사 
			for(k=0;k<6;k++){
				CopyPoint4(TempData[j][k], DataSub[j*6+k] );
				//CopyPoint4(Data[i*36+j*6+k],TempData[i][j]);
			}
		}

	}

/////////////////////////// Chessboard Merge //////////////////////////////////////////////	
	Index=0;
	for(j=0;j<6;j++){
		DataSub=DataS[1];
		for(k=0;k<6;k++){
			CopyPoint4(Data[Index],DataSub[j*6+k]);
			Index++;
		}
		DataSub=DataS[2];
		for(k=0;k<6;k++){
			CopyPoint4(Data[Index],DataSub[j*6+k]);
			Index++;
		}
	}
	for(j=0;j<6;j++){
		DataSub=DataS[0];
		for(k=0;k<6;k++){
			CopyPoint4(Data[Index],DataSub[j*6+k]);
			Index++;
		}
		DataSub=DataS[3];
		for(k=0;k<6;k++){
			CopyPoint4(Data[Index],DataSub[j*6+k]);
			Index++;
		}
	}

#if 0	
	fp=fopen("c:\\test.txt","w");
	fprintf(fp, "{");
	for(i=0;i<144;i++){
		fprintf(fp, "{");
		for(j=0;j<2;j++){
			
			fprintf(fp, "%6.1f",Data[i][j+2]);
			if(j==0) fprintf(fp, ",");
			else fprintf(fp, "},");
		}
		//fprintf(fp, "\n");
		if(i==143){
			fprintf(fp, "}");
			break;
		}
		if(i%12==11)fprintf(fp, "\n");
		
	}
	fprintf(fp, "};");
	fclose(fp);

#endif
	cvReleaseImage(&dstImage); 
}

CRectData COPTIC_DETECT_Option::GetOpticCenterCoordinate(LPBYTE IN_RGB)
{
	CRectData OC_RECTData;
	
	IplImage *OriginImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);

	BYTE R,G,B;
	double Sum_Y;

	for (int y=0; y<m_CAM_SIZE_HEIGHT; y++)
	{
		for (int x=0; x<m_CAM_SIZE_WIDTH; x++)
		{
			B = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 2];
			
			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;		
		}
	}	
	
//	OriginImage = cvLoadImage("C:\\OriginImage.bmp", 0);

//	Capture_Gray_SAVE("C:\\RGBIMAGE", IN_RGB, 720, 480);	
//	cvSaveImage("C:\\Default_Image.bmp", OriginImage);

	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
//	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0); //객체 외곽의 Overshooting으로 인하여 한번 더 처리 함..

	cvCanny(SmoothImage, CannyImage, 0, 255);
	
//	cvSaveImage("C:\\CannyImage.bmp", CannyImage);

	cvDilate(CannyImage, DilateImage);
	cvCopyImage(DilateImage, temp_PatternImage);
	
	cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);////오버플로우//
	
	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
		counter++;
	
	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area=0, arcCount=0;
	counter = 0;
	double old_dist = 999999;

	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;
		double circularity = (4.0*3.14*area)/(arcCount*arcCount);
		
		if(circularity > 0.8)
		{			
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;

			if(center_x > (m_CAM_SIZE_WIDTH/2) - (m_CAM_SIZE_WIDTH/8) && center_x < (m_CAM_SIZE_WIDTH/2) + (m_CAM_SIZE_WIDTH/8) && center_y > (m_CAM_SIZE_HEIGHT/2) - (m_CAM_SIZE_WIDTH/6) && center_y < (m_CAM_SIZE_HEIGHT/2) + (m_CAM_SIZE_WIDTH/6))
			{
				if(rect.width < (m_CAM_SIZE_WIDTH/2) && rect.height < (m_CAM_SIZE_HEIGHT/2))
				{
				//	cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
					
					double dist = GetDistance(center_x, center_y, (m_CAM_SIZE_WIDTH/2), (m_CAM_SIZE_HEIGHT/2));
					
					if(dist < old_dist)
					{
						OC_RECTData.m_Left = rect.width/4+rect.x;
						OC_RECTData.m_Top = rect.height/4+rect.y;
						OC_RECTData.m_Width = rect.width/2;
						OC_RECTData.m_Height = rect.height/2;
						
						OC_RECTData.m_resultX = rect.x + rect.width/2;
						OC_RECTData.m_resultY= rect.y + rect.height/2;

						OC_RECTData.m_Right	= OC_RECTData.m_Left + OC_RECTData.m_Width;
						OC_RECTData.m_Bottom = OC_RECTData.m_Top + OC_RECTData.m_Height;
						

						old_dist = dist;
					}
				}
			}
			else
			{
			//	cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8);  
			}
		}

		counter++;
	}

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&CannyImage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);

//	delete contour;
//	delete temp_contour;
	
	delete []rectArray;
	delete []areaArray;

	return OC_RECTData;
}

void COPTIC_DETECT_Option::FixClusterPoint(double *CenterPoint, double **Data, int NumInputData, int NumOutputData)
{

	int i, j, Iter, NumData;
	double CenterX, CenterY, DistX, DistY, *DistOfPoint;

///////////////////////////////////////////////////
	DistOfPoint=(double *)malloc(sizeof(double)*NumInputData);

	NumData = NumInputData;
#if 1
		for(i=0; i<NumInputData; i++){
			DistOfPoint[i] = Distance(CenterPoint,Data[i]);
		}

		for(i=0; i<NumInputData; i++){
			for(j=i;j<NumInputData;j++){
				if(DistOfPoint[i]>DistOfPoint[j]){
					Swapdata(&DistOfPoint[i], &DistOfPoint[j]);
					SwapPoint4(Data[i],Data[j] );
				}
			}
		}
		NumData = NumOutputData;
////////////////////////////////////////////////////

///////////////////////////////////////////////////
#else
	for(Iter=0;Iter<2;Iter++){
		CenterX=0; CenterY=0;
		for(i=0; i<NumData; i++){
			CenterX += Data[i][0];	CenterY += Data[i][1];
		}
	
		CenterX /= NumData; CenterY /= NumData;

		for(i=0; i<NumInputData; i++){
			DistOfPoint[i] = Distance(CenterPoint,Data[i]);
	
//			DistX = fabs(Data[i][0] - CenterX);
//			DistY = fabs(Data[i][1] - CenterY);
//			if(DistX>DistY)MaxDistOfPoint[i] = DistX;
//			else MaxDistOfPoint[i] = DistY;
		}

		for(i=0; i<NumInputData; i++){
			for(j=i;j<NumInputData;j++){
				if(DistOfPoint[i]>DistOfPoint[j]){
					Swapdata(&DistOfPoint[i], &DistOfPoint[j]);
					SwapPoint4(Data[i],Data[j] );
				}
			}
		}
		NumData = NumOutputData;
	}
////////////////////////////////////////////////////

///////////////////////////////////////////////////
	CenterPoint[0]=0; CenterPoint[1]=0;
	for(i=0; i<NumOutputData; i++){
		CenterPoint[0] += Data[i][0];	CenterPoint[1] += Data[i][1];
	}
	CenterPoint[0] /= NumOutputData; CenterPoint[1] /= NumOutputData;
#endif
	free(DistOfPoint);
}

double COPTIC_DETECT_Option::Distance(double *A, double *B)
{
	return sqrt((A[0]-B[0])*(A[0]-B[0])+(A[1]-B[1])*(A[1]-B[1]));
}

void COPTIC_DETECT_Option::SwapPoint(double *A, double *B)
{
	double Temp[2];

	Temp[0] = A[0];    Temp[1] = A[1];
	A[0]    = B[0];    A[1]    = B[1];
	B[0]    = Temp[0]; B[1]    = Temp[1];
}

double COPTIC_DETECT_Option::FindMinDistPoint1(int *Index, double **DataTmp, double *RefPoint, int N, int Mode)
//Find MinDistance Point 배열에서 
{
	int i;
	double Dist, MinDist, SkipIndex, Thr=0.000000001;
//
	SkipIndex=N+1;

	*Index=0;
	MinDist = Distance(RefPoint, DataTmp[0]);
	if((Mode==1)&&(MinDist<Thr)){			//Mode 0. 전체중에 가장 가까운점 찾기, Mode 1. 자기자신은 제외하고 가장 가까운점찾기  
		SkipIndex=0;
		MinDist = Distance(RefPoint, DataTmp[1]);
		*Index=1;
	}
	
	for(i=0;i<N;i++){
		if(i == SkipIndex)	continue;

		Dist = Distance(RefPoint, DataTmp[i]);
		if(Mode==1 && Dist<Thr){
			SkipIndex=i;
			continue;
		}

		if(MinDist>Dist) {
			MinDist = Dist; 
			*Index = i; 
		}
	}
	return MinDist;
}

void COPTIC_DETECT_Option::Swapdata(double *A, double *B)
{
	double Temp;
	Temp = A[0];    
	A[0]   = B[0];
	B[0]   = Temp; 
}

void COPTIC_DETECT_Option::SwapPoint4(double *A, double *B)
{
	double Temp[4];

	Temp[0] = A[0];    Temp[1] = A[1];		Temp[2] = A[2];    Temp[3] = A[3];
	A[0]    = B[0];    A[1]    = B[1];		A[2]    = B[2];    A[3]    = B[3];
	B[0]    = Temp[0]; B[1]    = Temp[1];	B[2]    = Temp[2]; B[3]    = Temp[3];
}

int COPTIC_DETECT_Option::CheckArea(int x, int y, int n) // 격자점이 x,y 좌표에 존재할수 있는 영역인지 확인 n은 주변+-n pixel까지 같은 영역인지 확인
{
	if((y-n < 0) || (x-n < 0) || (y+n >= m_CAM_SIZE_HEIGHT) || (x+n >= m_CAM_SIZE_WIDTH)) return 0;

	for(int i=0;i<n*2+1;i++)
	for(int j=0;j<n*2+1;j++){
		if(m_ChartArea[((int)y+i-n)*m_CAM_SIZE_WIDTH+(int)x+j-n] != 0x01) return 0;
	}
	return 1;
}

void COPTIC_DETECT_Option::MakeCornerMap(double **Data,int N, double R)
{
	long i,j,k;
	double c;//왜곡의 중심및 촬영된 좌표를 잇는 직선과, 왜곡함수와의 교점(a,b,c)에서 Z좌표인 c  
	
	for(i=0;i<N;i++){
		c=sqrt(R*R-(Data[i][0]-(m_CAM_SIZE_WIDTH/2))*(Data[i][0]-(m_CAM_SIZE_WIDTH/2))-(Data[i][1]-(m_CAM_SIZE_HEIGHT/2))*(Data[i][1]-(m_CAM_SIZE_HEIGHT/2)))-R;
		Data[i][2]=(R/(c+R))*(Data[i][0]-(m_CAM_SIZE_WIDTH/2))+(m_CAM_SIZE_WIDTH/2);
		Data[i][3]=(R/(c+R))*(Data[i][1]-(m_CAM_SIZE_HEIGHT/2))+(m_CAM_SIZE_HEIGHT/2);
	}

	for(i=0;i<N-1;i++)
	for(j=0;j<N-i-1;j++){
		if(Data[j][2]>Data[j+1][2])	SwapPoint4(Data[j],Data[j+1]);
	}

	for(k=0;k<12;k++){
		for(i=0;i<12-1;i++)
		for(j=0;j<12-i-1;j++){
			if(Data[k*12+j][3]>Data[k*12+j+1][3])	SwapPoint4(Data[k*12+j],Data[k*12+j+1]);
		}
	}

	for(i=0;i<N;i++) SwapPoint(Data[i],Data[i]+2);
}

void COPTIC_DETECT_Option::CopyPoint4(double *Dest, double *Src)
{
	Dest[0] = Src[0];   Dest[1] = Src[1];   Dest[2] = Src[2];   Dest[3] = Src[3]; //에러
}

void COPTIC_DETECT_Option::CalCoefficient( double *ReturnVal )
{
	unsigned int i, NumTheta;
	double A[2],**X, **y, **TempM2,**Temp2M, **Temp22, Theta=0, DeltaTheta = 0.1/180.0*pi;
	double Norm=0, Sum=0,mres, rmax, u0, v0, mu, mv;


	NumTheta = (unsigned int)(ThetaMax/DeltaTheta)+1;

	X = (double **)malloc(sizeof(double *)*NumTheta);                     //   X[960][2]   X[Row][Col]
	for(i=0;i<NumTheta;i++)X[i]=(double *)malloc(sizeof(double )* 2);   

	y = (double **)malloc(sizeof(double *)*NumTheta);
	for(i=0;i<NumTheta;i++)y[i]=(double *)malloc(sizeof(double )* 1);     //   y[960][1]

	Temp2M = (double **)malloc(sizeof(double *)*2);
	for(i=0;i<2;i++)Temp2M[i]=(double *)malloc(sizeof(double )* NumTheta);     //   Temp2M[2][960] 

	TempM2 = (double **)malloc(sizeof(double *)*NumTheta);
	for(i=0;i<NumTheta;i++)TempM2[i]=(double *)malloc(sizeof(double )* 2);     //   TempM2[960][2] 

	Temp22 = (double **)malloc(sizeof(double *)*2);
	for(i=0;i<2;i++)Temp22[i]=(double *)malloc(sizeof(double )* 2);            //   Temp22[2][2] 

	
	for( i=0, Theta=0 ; i<NumTheta ; Theta+=DeltaTheta, i++ ) {
		X[i][0]=Theta;
		X[i][1]=Theta*Theta*Theta;
		y[i][0]=2*Focal*sin(Theta/2.0); 
	}

	MulMatrix(X, X, Temp22, 2, 2, NumTheta, 1);  // Temp22 = X' x X (Mode 1)
	InvMatrix22(Temp22, Temp22);
	MulMatrix(Temp22, X, Temp2M, 2, NumTheta, 2, 2);  // Temp2M = Temp22 x X' (Mode 2)
	MulMatrix(Temp2M, y, Temp22, 2, 2, NumTheta, 0);  // Temp22 = Temp2M x y (Mode 0)	

	A[0]=Temp22[0][0]; A[1]=Temp22[1][0];  // A[0] = 1.2688, A[1] = -0.0509  계수 값
    
	MulMatrix(X, Temp22, TempM2, NumTheta, 1, 2, 0);  // X x A (Mode 0)	

	for(i=0;i<NumTheta;i++){
		Sum += (y[i][0]-TempM2[i][0])*(y[i][0]-TempM2[i][0]);  // Sum[(y-X*a)^2]
	}
	Norm = sqrt(Sum);

	mres = 1.0/NumTheta*Norm;

	rmax = A[0]*ThetaMax+A[1]*ThetaMax*ThetaMax*ThetaMax;
	u0 = m_CAM_SIZE_WIDTH/2; v0=m_CAM_SIZE_HEIGHT/2;
	mu = sqrt(u0*u0+v0*v0)/rmax;
	mv = mu;

    ReturnVal[0]=A[1]; ReturnVal[1]=A[0]; ReturnVal[2]=mres; ReturnVal[3]=rmax; 
	ReturnVal[4]=mu  ; ReturnVal[5]=mv  ; ReturnVal[6]=u0  ; ReturnVal[7]=v0; //??

	// Return 값 :  A[1],A[0],mres,rmax,mu,mv,u0,v0; 

	for(i=0;i<NumTheta;i++){
		free (X[i]);   
		free (y[i]);
		free (TempM2[i]);     
	}
	free(X);       //   X[960][2]     
	free(y);       //   y[960][1]
	free(TempM2);  //   TempM2[960][2] 
	
	for(i=0;i<2;i++){
		free (Temp2M[i]);
		free (Temp22[i]);     
	}
	free(Temp22);  //   Temp22[2][2] 
	free(Temp2M);  //   Temp2M[2][960] 
}


void COPTIC_DETECT_Option::RadialProjection(double *p)  // 인수 p :  A[1],A[0],mres,rmax,mu,mv,u0,v0 
{                                       // A[0] = 1.2688, A[1] = -0.0509  계수 값
	unsigned int i, NumAlpha, MaxPosition;
	double rc[4], dc[4], *ts, *rs, *ds ;  

    NumAlpha=(unsigned int)(180.0/0.5+1);

	ts=(double *)malloc(sizeof(double)*NumAlpha);
	rs=(double *)malloc(sizeof(double)*NumAlpha);
	ds=(double *)malloc(sizeof(double)*NumAlpha);

	for(i=0;i<NumAlpha;i++)	ts[i] = 0.5*((double)(i))/180.0*pi;
    
	
	rc[0]=p[0];  rc[1]=   0 ; rc[2]=p[1]; rc[3]=  0 ; 
	dc[0]=  0 ; dc[1]=3*p[0]; dc[2]=  0 ; dc[3]=p[1]; 


	polyval3(ts, rs, &rc[0], NumAlpha);
	polyval3(ts, ds, &dc[0], NumAlpha);
	Max(rs, &MaxPosition, NumAlpha);
	ThetaMax = ts[MaxPosition];
	
	for(i=0;i<NumAlpha;i++)	if(ds[i]<0) break; 
	ThetaFirstMax = ts[i];  //2.8885, 332번째 

	free(ts);	free(rs); free(ds);
}

void COPTIC_DETECT_Option::MulMatrix(double **A, double **B, double **C, unsigned int Row, unsigned int Col, unsigned int n, unsigned int Mode)
{

////  A[Row][n] x B[n][Col] = [Row x n] x [n x Col] = [ Row x Col ] = C[Row][Col]   ///////
////  Mode :  [ Mode 0 : A x B , Mode 1 : A' x B, Mode 2 : A x B' ] 

	unsigned int i,j,k;


	for(i=0;i<Row;i++){
		for(j=0;j<Col;j++){
			C[i][j]=0;
			for(k=0;k<n;k++){
				if(Mode==0)	     C[i][j] += A[i][k]*B[k][j];   // Mode 0 : A  x B
				else if(Mode==1) C[i][j] += A[k][i]*B[k][j];   // Mode 1 : A' x B
				else if(Mode==2) C[i][j] += A[i][k]*B[j][k];   // Mode 2 : A  x B' 
			}
		}
	}
}

void COPTIC_DETECT_Option::InvMatrix22(double **A, double **B)
{
	double Det, TempMatrix[2][2];

	Det = 1.0/(A[0][0]*A[1][1]-A[0][1]*A[1][0]);
	TempMatrix[0][0] = A[1][1];
	TempMatrix[1][1] = A[0][0];
	TempMatrix[0][1] = -1*A[0][1];
	TempMatrix[1][0] = -1*A[1][0];

	B[0][0] = TempMatrix[0][0]*Det;
	B[0][1] = TempMatrix[0][1]*Det;
	B[1][0] = TempMatrix[1][0]*Det;
	B[1][1] = TempMatrix[1][1]*Det;
}

void COPTIC_DETECT_Option::polyval3(double *x, double *y, double *K, unsigned int NumAlpha)
{
	unsigned int i,j;
	for(i=0;i<NumAlpha;i++)	y[i]=K[0];
	for(i=0;i<3;i++){
		for(j=0;j<NumAlpha;j++){
			y[j] = y[j]*x[j] + K[i+1];
		}
	}
}

void COPTIC_DETECT_Option::Max(double *data, unsigned int *MaxPosition, unsigned int count)
{
	unsigned int i;
	double MaxData;

	MaxData=data[0]; *MaxPosition=0;
	for(i=1;i<count;i++){
		if(MaxData<data[i]) {
			MaxData=data[i]; *MaxPosition = i;
		}
	}
}

void COPTIC_DETECT_Option::poly3roots(double *a, double *result)
{
//	unsigned int i;
	double tmpa, p, q, D, tmp1[2], tmp2[2], u[2], v[2], tmp3, tmp4;
	tmpa=a[2]/a[3];
	p=a[1]/a[3]-1.0/3.0*tmpa*tmpa;
	q=2.0/27.0*tmpa*tmpa*tmpa-1.0/3.0*(a[1]*a[2])/(a[3]*a[3])+a[0]/a[3];
	D=1.0/27.0*p*p*p+1.0/4.0*q*q;

	if(D>=0) {
		tmp1[0]=-1.0/2.0*q+sqrt(D); tmp1[1]=0;
	    tmp2[0]=-1.0/2.0*q-sqrt(D); tmp2[1]=0;
		
		u[0]=pow(tmp1[0], 1.0/3.0); u[1]=0;
		v[0]=pow(tmp2[0], 1.0/3.0); v[1]=0;
	}
	else {
		tmp1[0]=-1.0/2.0*q;  tmp1[1]= sqrt(-1.0*D);
	    tmp2[0]=-1.0/2.0*q;  tmp2[1]= -1.0*sqrt(-1.0*D); 

        ///// tmp1^(1/3) , 복소수 이므로....	
		
		tmp3=sqrt(tmp1[0]*tmp1[0]+tmp1[1]*tmp1[1]);   // r(cos[a]+i*sin[a])=abs(tmp1)
		tmp4=(atan(tmp1[1]/tmp1[0])-2*pi)/3.0;               // [a]/3 ;

		u[0]=-1*pow(tmp3,1.0/3.0)*cos(tmp4);
		u[1]=-1*pow(tmp3,1.0/3.0)*sin(tmp4);
			
		tmp3=sqrt(tmp2[0]*tmp2[0]+tmp2[1]*tmp2[1]);   // r(cos[a]+i*sin[a])=abs(tmp1)
		tmp4=(atan(tmp2[1]/tmp2[0])-4*pi)/3.0;               // [a]/3 ;		
		v[0]=-1*pow(tmp3,1.0/3.0)*cos(tmp4);
		v[1]=-1*pow(tmp3,1.0/3.0)*sin(tmp4);

	}


	result[0] = -1.0/3.0*tmpa+u[0]+v[0]; // real part of the Root1
    result[1] =  u[1]+v[1];    // image part of the Root1
	result[2] = -1.0/3.0*tmpa-1.0/2.0*(u[0]+v[0])-(sqrt(3.0)/2.0)*(u[1]-v[1]);  // real part of the Root2
	result[3] = -1.0/2.0*(u[1]+v[1])+(sqrt(3.0)/2.0)*(u[0]-v[0]);  // image part of the Root2
	result[4] = -1.0/3.0*tmpa-1.0/2.0*(u[0]+v[0])+(sqrt(3.0)/2.0)*(u[1]-v[1]);  // real part of the Root3
	result[5] = -1.0/2.0*(u[1]+v[1])-(sqrt(3.0)/2.0)*(u[0]-v[0]);   // image part of the Root3

}

void COPTIC_DETECT_Option::SVD(double *dataA, unsigned int row, unsigned int col, double **MatU, double **MatW, double **MatVT)
{

	unsigned int i,j;
	CvMat matA = cvMat( row, col, CV_64F, dataA );
	CvMat *pMatW = cvCreateMat(row, col, CV_64F);
	CvMat *pMatU = cvCreateMat(row, row, CV_64F);
	CvMat *pMatVT = cvCreateMat(col, col, CV_64F);

	CvMat *pMatA = cvCreateMat(row, col, CV_64F);
//	PrintMat(&matA, "pMatA = Source");

	cvSVD(&matA, pMatW, pMatU, pMatVT, CV_SVD_V_T);

	for(i=0;i<col;i++)
		for(j=0;j<col;j++)
			MatU[i][j]=cvGetReal2D(pMatU, i, j);  // A = U * W * VT  ,  MatRtn : VT
	
	for(i=0;i<col;i++)
		for(j=0;j<col;j++)
			MatW[i][j]=cvGetReal2D(pMatW, i, j);  // A = U * W * VT  ,  MatRtn : VT
			
	for(i=0;i<col;i++)
		for(j=0;j<col;j++)
			MatVT[i][j]=cvGetReal2D(pMatVT, i, j);  // A = U * W * VT  ,  MatRtn : VT

//    PrintMat(&matA, "MatA =  ");

	cvReleaseMat(&pMatU);
	cvReleaseMat(&pMatVT);
    cvReleaseMat(&pMatW);
	cvReleaseMat(&pMatA);
}

double COPTIC_DETECT_Option::BackProjectGeneric(double **RtnH, double **m, double **xs, double *p, double ThetaMax1, double *Theta, double *phi, unsigned int NumPoint)
{
	unsigned int i,j,k, I1,ExitFlag,SuccessfulStep, done;//, ii, PassCnt, gcnt;
	double k1, k2, mu, mv, u0, v0, SumTmp=0;
	double *x, *y, *r, **xp, **A, *B, a[4], **root, **SphereX, tol=0.0001;
	double *SphereXNorm, *xpNorm, **MatRtn, **AugJac, *AugRes;//, **pInvAugJac;
	double **H, **CurrentH, *VecD, *trialCostFun;
	double **MatW, **MatU;
	double DiffMinChange, DiffMaxChange;
	double DeltaX[9], relFactor; //*fplus, *fCurrent, 
	double gradF[9], step[9], **JAC;
	double SqrtEps = 1.4901e-8 ; //SqrtEps = sqrt(eps) = 1.49e(-8), eps = 2^(-52
	double tolOpt = 1.0e-8;//tolOpt = 1e-4 * tolFun; % tolFun = 1.0e-04
	double lambda =0.01, **trialX,trialSumSq, **XOUT,*costFun, SumSq =0, temp, RtnErr=0;// Xout[9];

	DiffMinChange = 1.0e-8; DiffMaxChange = 0.1;

	x = (double *)malloc(sizeof(double)*NumPoint);
	y = (double *)malloc(sizeof(double)*NumPoint);
	r = (double *)malloc(sizeof(double)*NumPoint);
	xpNorm= (double *)malloc(sizeof(double)*NumPoint);
	SphereXNorm = (double *)malloc(sizeof(double)*NumPoint);

	A = (double **)malloc(sizeof(double*)*(NumPoint*3));
	for(i=0;i<NumPoint*3;i++)A[i]=(double *)malloc(sizeof(double)*9);

	B = (double *)malloc(sizeof(double)*(NumPoint*3*9));

	SphereX = (double **)malloc(sizeof(double*)*NumPoint);
	for(i=0;i<NumPoint;i++)SphereX[i]=(double *)malloc(sizeof(double)*3);

    root = (double **)malloc(sizeof(double*)*NumPoint);
	for(i=0;i<NumPoint;i++)root[i]=(double *)malloc(sizeof(double)*6);

	xp = (double **)malloc(sizeof(double*)*NumPoint);
	for(i=0;i<NumPoint;i++)xp[i]=(double *)malloc(sizeof(double)*3);

	
	k1=p[1]; k2=p[0]; mu=p[4]; mv=p[5]; u0=p[6]; v0=p[7];

	for(i=0;i<NumPoint;i++){
		x[i]=1.0/mu*(m[i][0]-u0); 	
		y[i]=1.0/mv*(m[i][1]-v0); 
		r[i]= sqrt(x[i]*x[i]+y[i]*y[i]);

		if(x[i] == 0.0){
			phi[i]= pi/2.0;
			if(y[i]<0.0) phi[i] *= -1.0;
		}
		else {
			phi[i]=atan(y[i]/x[i]);
			if(x[i]<0.0) phi[i] += pi;
			if(phi[i]<0.0) phi[i] += 2*pi;
		}
	}

    a[3] = p[0]; a[2] =0; a[1] = p[1];  // a3=-0.0509, a2 = 0, a1 = 1.2688, a0 = -r'; 
	                                    //3차식 근을 구하기 위한 다항식의 계수

	for(i=0;i<NumPoint;i++){ 
		a[0] = -1*r[i]; 
		poly3roots(&a[0], root[i]);
	}

    for(i=0;i<NumPoint;i++){
		for(j=0;j<3;j++){
			if(root[i][j*2+1]<tol && root[i][j*2]>0 && root[i][j*2]<ThetaMax){
				Theta[i]=root[i][j*2];
				break;
			}
		}
	}

	for(i=0;i<NumPoint;i++){
		SphereX[i][0] = cos(phi[i])*sin(Theta[i]);
		SphereX[i][1] = sin(phi[i])*sin(Theta[i]);
		SphereX[i][2] = cos(Theta[i]);
		SphereXNorm[i] = sqrt(SphereX[i][0]*SphereX[i][0]+SphereX[i][1]*SphereX[i][1]+SphereX[i][2]*SphereX[i][2]);
		SphereX[i][0] /= SphereXNorm[i];
		SphereX[i][1] /= SphereXNorm[i];
		SphereX[i][2] /= SphereXNorm[i];
	}

	for(i=0;i<NumPoint;i++){
		//
		xp[i][0]=xs[i][0];		xp[i][1]=xs[i][1];		xp[i][2]=1.0;
		xpNorm[i]=sqrt(xp[i][0]*xp[i][0] + xp[i][1]*xp[i][1] + xp[i][2]*xp[i][2]);
		for(j=0;j<3;j++)xp[i][j] /= xpNorm[i];
	}

	for(I1=0;I1<NumPoint;I1++)
		for(i=0;i<3;i++)
			for(j=0;j<3;j++)
				for(k=0;k<3;k++){
					if(i==j) A[I1*3+i][j*3+k]=0;
					else {
						A[I1*3+i][j*3+k]=SphereX[I1][3-(i+j)] * xp[I1][k];
						if(i<j) A[I1*3+i][j*3+k] *= (-1.0);
						if((i+j)%2==0) A[I1*3+i][j*3+k] *= (-1.0);
					}
				}

/************ SVD 를 이용하여 WT 함수의 값 구함 ************/
	for(i=0;i<3*NumPoint;i++)
		for(j=0;j<9;j++)
			B[i*9+j]=A[i][j];
	
	MatRtn = (double **)malloc(sizeof(double*)*9);
	for(i=0;i<9;i++)MatRtn[i]=(double *)malloc(sizeof(double)*9);
	
	MatW = (double **)malloc(sizeof(double*)*(NumPoint*3));
	for(i=0;i<(NumPoint*3);i++)MatW[i]=(double *)malloc(sizeof(double)*9);

	MatU = (double **)malloc(sizeof(double*)*(NumPoint*3));
	for(i=0;i<NumPoint*3;i++)MatU[i]=(double *)malloc(sizeof(double)*(NumPoint*3));

	SVD(B, 3*NumPoint, 9, MatU, MatW, MatRtn);  //  A = U*W*VT --> MatRtn : VT

	for(i=0;i<NumPoint*3;i++)free(MatU[i]);   free(MatU);
	for(i=0;i<NumPoint*3;i++)free(MatW[i]);   free(MatW);

	H = (double **)malloc(sizeof(double*)*3);
	for(i=0;i<3;i++)H[i]=(double *)malloc(sizeof(double)*3);

	CurrentH = (double **)malloc(sizeof(double*)*3);
	for(i=0;i<3;i++)CurrentH[i]=(double *)malloc(sizeof(double)*3);

	JAC = (double **)malloc(sizeof(double*)*NumPoint);
	for(i=0;i<NumPoint;i++)JAC[i]=(double *)malloc(sizeof(double)*9);
	
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			H[i][j] = (-1.0)*MatRtn[8][i*3+j];  // j와 i가 바뀌어 있슴  ??
		}
	}
	VecD = (double *)malloc(sizeof(double)*NumPoint);

	HomAngleErr(VecD, H, SphereX, xs , NumPoint);  //초기 costFun=VecD 구하기  //?!

	FindJAC(JAC, DeltaX,  VecD, H, SphereX, xs, NumPoint ); // JAC & DeltaX 구하기 

	FindGradF(gradF, JAC, VecD,  NumPoint, 9);  //get gradF 구하기 

	relFactor = MaxAbs(gradF, 9);

	if(relFactor<SqrtEps) relFactor=SqrtEps; //SqrtEps = sqrt(eps) = 1.49e(-8), eps = 2^(-52)  

	///////////// Set AugJac and AugRes ///////////////////////////

	AugJac = (double **)malloc(sizeof(double*)*(NumPoint+9));
	for(i=0;i<NumPoint+9;i++)AugJac[i]=(double *)malloc(sizeof(double)*9);

	trialX = (double **)malloc(sizeof(double*)*3);
	for(i=0;i<3;i++)trialX[i]=(double *)malloc(sizeof(double)*3);

	XOUT = (double **)malloc(sizeof(double*)*3);
	for(i=0;i<3;i++)XOUT[i]=(double *)malloc(sizeof(double)*3);

	costFun=(double *)malloc(sizeof(double)*NumPoint);
	trialCostFun=(double *)malloc(sizeof(double)*NumPoint);

	AugRes = (double *)malloc(sizeof(double)*(NumPoint+9));

	ExitFlag=0;SuccessfulStep=1;done=0;

	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			XOUT[i][j]=H[i][j];  //XOUT = H'
		}
	}
	SumSq = Norm(VecD,NumPoint);   // levenbergMarquardt.m
	SumSq *= SumSq;

	for(i=0;i<NumPoint;i++){
		costFun[i]=VecD[i];

	}

	for(I1=0;(I1<40)&&(done != 1); I1++){
		if(SuccessfulStep==1){
			for(i=0;i<NumPoint;i++){
				for(j=0;j<9;j++){
					AugJac[i][j]=JAC[i][j];
				}
				AugRes[i] = -1.0*costFun[i]; 
			}
			for(i=0;i<9;i++){
				for(j=0;j<9;j++){
					if(i==j) AugJac[i+NumPoint][j]=sqrt(lambda);
					else AugJac[i+NumPoint][j]=0;
				}
				AugRes[i+NumPoint] = 0;  // AugRes = [-costFun; zeroPad]; % Augmented residual
			}
		}
		else {
			for(i=0;i<9;i++)
				AugJac[i+NumPoint][i]=lambda;
		}

		GetStep(AugJac, AugRes, step, NumPoint+9, 9);

		for(i=0;i<3;i++)
			for(j=0;j<3;j++)
				trialX[j][i]=XOUT[j][i]+step[i*3+j];  //  row 먼저 col 나중....

		HomAngleErr(trialCostFun, trialX, SphereX, xs , NumPoint);

		trialSumSq = Norm(trialCostFun, NumPoint);
		trialSumSq *= trialSumSq ;

		if(trialSumSq<SumSq){
			for(j=0;j<NumPoint;j++)costFun[j]=trialCostFun[j];
			for(j=0;j<3;j++)
				for(k=0;k<3;k++)
					XOUT[k][j]=trialX[k][j];  
			if(SuccessfulStep==1){
				lambda *=0.1;
				if(lambda<2.2204e-16)lambda=2.2204e-16;
			}
		
			FindJAC(JAC, DeltaX,  costFun, XOUT, SphereX, xs, NumPoint ); // JAC & DeltaX 구하기 

			SuccessfulStep=1;
			FindGradF(gradF, JAC, costFun, NumPoint, 9);
			
////////////////////////////////// Check  탈출조건 //////////////////
			done= 0;

			temp = MaxAbs(gradF, 9);
			//if(Norm(gradF, 9) < (tolOpt+relFactor)){
			if(temp < (tolOpt*relFactor)){
				ExitFlag=1;  done=1;
			}
			else if (I1>=0){
				SumTmp=0;
				for(j=0;j<3;j++)SumTmp+=Norm(XOUT[j],3);
				if(Norm(step,9)<((1.0e-4)*(1.4901e-8 + SumTmp))){
					ExitFlag=4; done=1;
				}
			}
//////////////////////////////////////////////////////////////////////

			SumSq=trialSumSq;
		}
		else {
			lambda *=10;
			SuccessfulStep=0;
			if(lambda>1.0e16) ExitFlag =3;
		}
	}
//
    RtnErr=0;
	for(i=0;i<NumPoint;i++){
		RtnErr += fabs(trialCostFun[i]);
	}
	RtnErr /= (double)NumPoint;
	for(i=0;i<3;i++)
		for(j=0;j<3;j++)
			RtnH[i][j]=XOUT[i][j];

	free(x); free(y);free(r); free(SphereXNorm), free(xpNorm); 
	free(VecD); free(AugRes);

	for(i=0;i<3;i++)free(XOUT[i]);   free(XOUT);
	for(i=0;i<3;i++)free(trialX[i]);   free(trialX);
	for(i=0;i<NumPoint+9;i++)free(AugJac[i]);   free(AugJac);
	for(i=0;i<3;i++)free(CurrentH[i]);   free(CurrentH);
	for(i=0;i<NumPoint;i++)free(JAC[i]);   free(JAC);
	for(i=0;i<3;i++)free(H[i]);   free(H);

	for(i=0;i<NumPoint*3;i++)free(A[i]);   free(A);       
	for(i=0;i<9;i++)free(MatRtn[i]);   free(MatRtn);
	for(i=0;i<NumPoint;i++)free(xp[i]);   free(xp);       // 
	for(i=0;i<NumPoint;i++)free(SphereX[i]);   free(SphereX);
	for(i=0;i<NumPoint;i++)free(root[i]);   free(root); 
	free(B);
	free(trialCostFun);
	free(costFun);

	return RtnErr;
}



double COPTIC_DETECT_Option::Norm(double * data, unsigned int n)
{
	unsigned int i;
	double Sum=0;
	for(i=0;i<n;i++)
		Sum += data[i]*data[i];
	return sqrt(Sum);
}

void COPTIC_DETECT_Option::HomAngleErr(double *VecD, double **H, double **SphereX, double **xs , unsigned int NumPoint)
{
	unsigned int i,j;
	double **TmpC, TmpD;//, **HT;

/************ SVD 를 이용하여 WT 함수의 값 구함 ************/
	// x[1090][3] : SphereX, M : NumPoint(109) , xs[109][3] : 공간상의 좌표 중 x, y, z=1


	TmpC = (double **)malloc(sizeof(double*)*3);
	for(i=0;i<3;i++)TmpC[i]=(double *)malloc(sizeof(double)*NumPoint);
	
	MulMatrix(H, xs, TmpC, 3, NumPoint, 3, 2);   

	NormalizeMat(TmpC, 3, NumPoint, 1);  // Mode ( 0 : Row방향, 1: Col 방향 )
	


	
	for(i=0;i<NumPoint;i++){
		TmpD=0;
		for(j=0;j<3;j++){
			TmpD += TmpC[j][i]*SphereX[i][j];
		}
		VecD[i] = sin(acos(TmpD));
	}
	for(i=0;i<3;i++)free(TmpC[i]);   free(TmpC); 

}

void COPTIC_DETECT_Option::FindJAC(double **JAC,  double *DeltaX,  double *VecD, double **H, double **SphereX, double **xs, unsigned int NumPoint )
{
	unsigned int i, PassCnt=0, gcnt=0;
	double SumSq=0, TypicalX[9], **CurrentH, VecH[9], *fplus;
	double DiffMinChange = 1.0e-8, DiffMaxChange = 0.1, *fCurrent;



	CurrentH = (double **)malloc(sizeof(double*)*3);
	for(i=0;i<3;i++)CurrentH[i]=(double *)malloc(sizeof(double)*3);	

	fplus = (double *)malloc(sizeof(double)*NumPoint);

	for(i=0;i<9;i++){
		VecH[i]=H[i%3][i/3];
		TypicalX[i] = 1;
	}

	SetDeltaX(DeltaX, VecH, TypicalX, DiffMinChange, DiffMaxChange, 9);

	PassCnt=0; fCurrent=VecD;
									/// finitedifferences.m
	for(gcnt=0;gcnt<9;gcnt++){
		for(i=0;i<9;i++) CurrentH[i%3][i/3]=VecH[i];
		CurrentH[gcnt%3][gcnt/3] += DeltaX[gcnt];
		HomAngleErr(fplus, CurrentH, SphereX, xs , NumPoint);

		for(i=0;i<NumPoint;i++){
			JAC[i][gcnt]=(fplus[i]-fCurrent[i])/DeltaX[gcnt];
		}
		PassCnt++;
	}

	free(fplus); 
	for(i=0;i<3;i++)free(CurrentH[i]);   free(CurrentH);
}

void COPTIC_DETECT_Option::FindGradF(double *gradF, double **JAC, double *costFun,  unsigned int JacRow, unsigned int JacCol)
{
	unsigned int i,j;
	double Sum=0;

	for(i=0;i<JacCol;i++){
		Sum=0;
		for(j=0;j<JacRow;j++){
			Sum += JAC[j][i]*costFun[j];
		}
		gradF[i]=Sum;
	}
}

double COPTIC_DETECT_Option::MaxAbs(double * data, unsigned int n)
{
	double Max, temp;
	unsigned int i;

	Max = fabs(data[0]);
	for(i=1;i<n;i++){
		temp=fabs(data[i]);
		if(Max<temp) Max = temp;
	}
	return Max;
}	

void COPTIC_DETECT_Option::GetStep(double **AugJac, double *AugRes1D, double *step1D, unsigned int row, unsigned int col)
{

	unsigned int i;//,j;
	double **pInvAugJac, **AugRes, **step;

	pInvAugJac = (double **)malloc(sizeof(double*)*col);
	for(i=0;i<col;i++)pInvAugJac[i]=(double *)malloc(sizeof(double)*row);

	AugRes = (double **)malloc(sizeof(double*)*row);
	for(i=0;i<row;i++)AugRes[i]=(double *)malloc(sizeof(double)*1);

	step = (double **)malloc(sizeof(double*)*row);
	for(i=0;i<row;i++)step[i]=(double *)malloc(sizeof(double)*1);

	pInverse(AugJac, row, col, pInvAugJac);

	for(i=0;i<row;i++)
		AugRes[i][0]=AugRes1D[i];
	
	MulMatrix(pInvAugJac, AugRes, step, col, 1, row, 0);  

	for(i=0;i<col;i++){
		step1D[i]= step[i][0];
	}

	for(i=0;i<col;i++)free(pInvAugJac[i]);   free(pInvAugJac);
	for(i=0;i<row;i++)free(AugRes[i]);   free(AugRes);
	for(i=0;i<row;i++)free(step[i]);   free(step);

}

void COPTIC_DETECT_Option::NormalizeMat(double **MatA, unsigned int row , unsigned int col, unsigned int Mode)  // Mode ( 0 : Row방향, 1: Col 방향 )
{
	unsigned int i,j;
	double Norm =0;

	if(Mode==0){
		for(i=0;i<row;i++){
			Norm=0;
			for(j=0;j<col;j++)
				Norm += MatA[i][j]*MatA[i][j];

			Norm = sqrt(Norm);

			for(j=0;j<col;j++)
				MatA[i][j] /= Norm; 
		}
	}
	else {
		for(i=0;i<col;i++){
			Norm=0;
			for(j=0;j<row;j++)
				Norm += MatA[j][i]*MatA[j][i];

			Norm = sqrt(Norm);

			for(j=0;j<row;j++)
				MatA[j][i] /= Norm; 
		}
	}
}

void COPTIC_DETECT_Option::SetDeltaX(double *DeltaX, double *VecH, double *TypicalX, double Min, double Max , unsigned int N)
{
	unsigned int i;

	if(fwdFindDiff==1){
		for(i=0;i<N;i++){
			if(AbsDouble(VecH[i])>AbsDouble(TypicalX[i]))
				DeltaX[i]= 1.49*pow(10.0,-8.0) * VecH[i] ;//1.49*pow10(-8) : sqrt(eps)
			else 
				DeltaX[i]= 1.49*pow(10.0,-8.0) * TypicalX[i] ;//1.49*pow10(-8) : sqrt(eps)
		}
	}
	else {
		for(i=0;i<N;i++){
			if(AbsDouble(VecH[i])>AbsDouble(TypicalX[i]))
				DeltaX[i]= 6.0555*pow(10.0,-6.0) * AbsDouble(VecH[i]) ;//1.49*pow10(-8) : sqrt(eps)
			else 
				DeltaX[i]= 6.0555*pow(10.0,-6.0) * AbsDouble(TypicalX[i]) ;//1.49*pow10(-8) : sqrt(eps)
		}
	}
	for(i=0;i<N;i++){
		if(AbsDouble(DeltaX[i])<Min) DeltaX[i]= Min;			
		if(AbsDouble(DeltaX[i])>Max) DeltaX[i]= Max;			
	}
	for(i=0;i<N;i++)
		if(VecH[i]<0) DeltaX[i] *= (-1.0);		
}

void COPTIC_DETECT_Option::pInverse(double **A, unsigned int Row, unsigned int Col, double **RtnMat)
{
	unsigned int i;//, j;
	double **AxA, **InvAxA;

	AxA = (double **)malloc(sizeof(double*)*Col);
	for(i=0;i<Col;i++)AxA[i]=(double *)malloc(sizeof(double)*Col);	

	InvAxA = (double **)malloc(sizeof(double*)*Col);
	for(i=0;i<Col;i++) InvAxA[i]=(double *)malloc(sizeof(double)*Col);
	
	MulMatrix(A, A, AxA, Col, Col, Row, 1);  // Mode 1 : A' * B

	Inverse(AxA, Col, InvAxA);

	MulMatrix(InvAxA, A, RtnMat, Col, Row, Col, 2);

	for(i=0;i<Col;i++)free(InvAxA[i]);   free(InvAxA);
	for(i=0;i<Col;i++)free(AxA[i]);   free(AxA);

}

void COPTIC_DETECT_Option::MinimiseProjErrs(double ***Rs, double **Ts, double *p, double ***ms, double *** xs,double ***Rs0, double **Ts0, double *p0)
{
	unsigned int i,j, N_Par;

	double RotVect[3], *Par0, *Par;

	N_Par = 9+1*6;
	Par0=(double *)malloc(sizeof(double)*N_Par);
	Par=(double *)malloc(sizeof(double)*N_Par);


	Par0[0] = p0[1]; Par0[1] = p0[0]; Par0[2] = p0[4]; Par0[3] = p0[5]; 
	Par0[4] = p0[6]; Par0[5] = p0[7]; 
	Par0[6] = Par0[7] = Par0[8] = 1.0e-7;

	for(i=0;i<1;i++){
		RotationPars(RotVect, Rs0[i]);
		for(j=0;j<3;j++){
			Par0[9+6*i+j]   = RotVect[j]; 
			Par0[9+6*i+3+j] = Ts0[i][j];
		}
	}

	Lsqnonlin(Par, Par0, ms, xs);//,  double ThetaMax1, double *Theta, double *phi, unsigned int NumPoint);

	for(i=0;i<9;i++) p[i]=Par[i];

	for(i=0;i<1;i++){
		for(j=0;j<3;j++){
			RotVect[j] = Par0[9+6*i+j]; 
			Ts[i][j] = Par0[9+6*i+3+j];
		}
		RotationMat(Rs[i], RotVect); // 향후 왜곡보정시 필요..
	}
	free(Par0); free(Par);
}

double COPTIC_DETECT_Option::AbsDouble(double data)
{
	if(data<0)
		data *= (-1.0);
	return data;
}

void COPTIC_DETECT_Option::Inverse(double **dataMat, unsigned int n, double **MatRtn)
{

	unsigned int i,j;
	double *dataA;

	CvMat matA;
	CvMat *pMatB = cvCreateMat(n, n, CV_64F);

	dataA = (double *)malloc(sizeof(double)*n*n);

	for(i=0;i<n;i++)
		for(j=0;j<n;j++)
			dataA[i*n+j]=dataMat[i][j];

	matA = cvMat( n, n, CV_64F, dataA );

	cvInvert(&matA, pMatB);
//	PrintMat(&matA, "pMatA = Source");    // Mat print

	for(i=0;i<n;i++)
		for(j=0;j<n;j++)
			MatRtn[i][j]=cvGetReal2D(pMatB, i, j);  // A = U * W * VT  ,  MatRtn : VT

    cvReleaseMat(&pMatB);
	free(dataA);
}

void COPTIC_DETECT_Option::RotationPars(double *RtnT, double **R)
{
	unsigned int i,j;
	double Data[9], **MatU, **MatW, **MatVT;
	double TraceR=0, CosPhi, SinPhi, Phi;

	MatU = (double **)malloc(sizeof(double *)*3);                     //   
	for(i=0;i<3;i++)MatU[i]=(double *)malloc(sizeof(double )* 3);


	MatW = (double **)malloc(sizeof(double *)*3);                     //   
	for(i=0;i<3;i++)MatW[i]=(double *)malloc(sizeof(double )* 3);

	MatVT = (double **)malloc(sizeof(double *)*3);                     //   
	for(i=0;i<3;i++)MatVT[i]=(double *)malloc(sizeof(double )* 3);

	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			Data[i*3+j]=R[i][j];
			if(i==j){
				TraceR += R[i][j];
				Data[i*3+j] -= 1.0;
			}
		}
	}

	SVD(Data, 3, 3, MatU, MatW, MatVT);  //  A = U*W*VT --> MatRtn : VT

	CosPhi = 0.5*(TraceR-1.0);
	SinPhi = -0.5*( MatU[2][0]*(R[2][1]-R[1][2]) +  MatU[2][1]*(R[0][2]-R[2][0]) + MatU[2][2]*(R[1][0]-R[0][1]));
	Phi=atan(SinPhi/CosPhi);

    for(i=0;i<3;i++) RtnT[i] = Phi * MatU[i][2];

	for(i=0;i<3;i++)free(MatU[i]);   free(MatU); 
	for(i=0;i<3;i++)free(MatW[i]);   free(MatW); 
	for(i=0;i<3;i++)free(MatVT[i]);   free(MatVT); 
}

void COPTIC_DETECT_Option::Lsqnonlin(double *Par, double *Par0, double ***ms, double ***xs)
{

	double *d;

	d = (double *)malloc(sizeof(double )*1*N_Point*2); 

	ProjErr(d, Par0, ms, xs);//, unsigned int NumPictur);
	LevenbergMarquardt( Par, Par0, d, ms, xs);

	free(d);
}

void COPTIC_DETECT_Option::RotationMat(double **RtnH, double *RotVect)
{

	unsigned int i,j;
	double **Thx, **ThxTh, Phi;

	Thx = (double **)malloc(sizeof(double *)*3);                     //   
	for(i=0;i<3;i++)Thx[i]=(double *)malloc(sizeof(double )* 3);

	ThxTh = (double **)malloc(sizeof(double *)*3);                     //   
	for(i=0;i<3;i++)ThxTh[i]=(double *)malloc(sizeof(double )* 3);

	Phi = sqrt(RotVect[0]*RotVect[0]+RotVect[1]*RotVect[1]+RotVect[2]*RotVect[2]);

	Thx[0][0] = Thx[1][1] = Thx[2][2] = 0;
	Thx[0][1] = -1.0*RotVect[2]/Phi;
	Thx[0][2] =  RotVect[1]/Phi;
	Thx[1][0] =  RotVect[2]/Phi;
	Thx[1][2] =  -1.0*RotVect[0]/Phi;
	Thx[2][0] =  -1.0*RotVect[1]/Phi;
	Thx[2][1] =  RotVect[0]/Phi;

	MulMatrix(Thx, Thx, ThxTh, 3, 3, 3, 0);
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			RtnH[i][j] = sin(Phi)*Thx[i][j]+(1.0-cos(Phi))*ThxTh[i][j];
			if(i==j)RtnH[i][j] += 1.0;
		}
	}
	for(i=0;i<3;i++)free(Thx[i]);   free(Thx); 
	for(i=0;i<3;i++)free(ThxTh[i]);   free(ThxTh);
}

void COPTIC_DETECT_Option::ProjErr(double *d, double *Par, double ***ms, double ***xs)
{
	unsigned int i,j,k;
	double *RotVect, Ti[3], **Ri, **mh, **Error, *Err;


	Error = (double **)malloc(sizeof(double *)*N_Point*1);                     //   ?? 개수 확인필요
	for(i=0;i<N_Point*1;i++)Error[i]=(double *)malloc(sizeof(double )* 2);

	Err = (double *)malloc(sizeof(double )* 1*1); //   ?? 개수 확인필요

	RotVect = (double *)malloc(sizeof(double )* 3);

	Ri = (double **)malloc(sizeof(double *)*3);                     //   
	for(i=0;i<3;i++)Ri[i]=(double *)malloc(sizeof(double )* 3);

	mh = (double **)malloc(sizeof(double *)*N_Point);                     //   
	for(i=0;i<N_Point;i++)mh[i]=(double *)malloc(sizeof(double )* 3);

	for(i=0;i<1;i++){
		for(j=0;j<3;j++){
			RotVect[j] = Par[9+6*i+j]; 
			Ti[j] = Par[9+6*i+3+j];
		}
		RotationMat(Ri, RotVect); //

		GenericProjExtended(mh, xs[i], Par, Ri, Ti);

		Err[i]=0;

		for(j=0;j<N_Point;j++){
			for(k=0;k<2;k++){
				d[i*N_Point*2+j*2+k] = Error[j][k] = ms[i][j][k]-mh[j][k];
			}
			Err[i] += sqrt(Error[j][0]*Error[j][0]+Error[j][1]*Error[j][1]); 
		}
		Err[i] /= (double) N_Point;
	}

	free(RotVect);	free(Err);

	for(i=0;i<N_Point*1;i++)free(Error[i]);   free(Error); 
	for(i=0;i<N_Point;i++)free(mh[i]);   free(mh); 
	for(i=0;i<3;i++)free(Ri[i]);   free(Ri); 
	
}

void COPTIC_DETECT_Option::LevenbergMarquardt(double *XOUT, double *Par0 , double *CostFun, double ***ms, double ***xs)
{
	unsigned int i,j,I1,ExitFlag,SuccessfulStep, done, n_costFun, n_par;//, ii, PassCnt, gcnt;k, 
	double    SumTmp=0;//mu,mv, k1, k2,u0, v0,
	double  tol=0.0001;//**SphereX, a[4],**A,  **root,**xp, *x, *y, 
	double  **AugJac, *AugRes;//, **pInvAugJac;**MatRtn, *B, *r, *xpNorm, *SphereXNorm,
	double *trialCostFun;//**CurrentH,*VecD,**H,   
	double   **JAC1, *costFun; //**MatU,**MatW;
	double DiffMinChange, DiffMaxChange;
	double *DeltaX, relFactor; //*fplus, *fCurrent, 
	double *gradF,  *step, *TypicalX;
	double SqrtEps = 1.4901e-15 ; //SqrtEps = sqrt(eps) = 1.49e(-8), eps = 2^(-52
	//double SqrtEps = 1.4901e-8 ; //SqrtEps = sqrt(eps) = 1.49e(-8), eps = 2^(-52
	double tolOpt = 1.0e-8;//tolOpt = 1e-4 * tolFun; % tolFun = 1.0e-04
	double lambda =0.01, *trialX,trialSumSq, SumSq =0, temp, RtnErr=0;// Xout[9];**XOUT,*costFun, 
	n_costFun = 1*N_Point*2;	n_par = 9+6*1;
	
	DiffMinChange = 1.0e-8; DiffMaxChange = 0.1;

	JAC1 = (double **)malloc(sizeof(double*)*n_costFun);
	for(i=0;i<n_costFun;i++) JAC1[i]=(double *)malloc(sizeof(double)*n_par);
	
	gradF=(double *)malloc(sizeof(double)*n_par);

	DeltaX = (double *)malloc(sizeof(double)*n_par);
	
	TypicalX = (double *)malloc(sizeof(double)*n_par);

	SumSq = 0;
	for(i=0;i<n_costFun;i++)   //6540 = 109*30*2 : NumCostFun
		SumSq += CostFun[i]*CostFun[i];

	for(i=0;i<n_par;i++) TypicalX[i]=1.0;

	SetDeltaX(DeltaX, Par0, TypicalX, DiffMinChange, DiffMaxChange, n_par);


	FindJAC1(JAC1,  DeltaX, CostFun, Par0, ms, xs, n_costFun, n_par); //get gradF 구하기 
	FindGradF(gradF, JAC1, CostFun,  n_costFun, n_par);

	relFactor = MaxAbs(gradF, n_par);

	if(relFactor<SqrtEps) relFactor=SqrtEps; //SqrtEps = sqrt(eps) = 1.49e(-8), eps = 2^(-52)  

	///////////// Set AugJac and AugRes ///////////////////////////

	AugJac = (double **)malloc(sizeof(double*)*(n_costFun+n_par));
	for(i=0;i<(n_costFun+n_par);i++)AugJac[i]=(double *)malloc(sizeof(double)*n_par);

	trialX = (double *)malloc(sizeof(double)*n_par);

	costFun=(double *)malloc(sizeof(double)*n_costFun);
	trialCostFun=(double *)malloc(sizeof(double)*n_costFun);

	AugRes = (double *)malloc(sizeof(double)*(n_costFun+n_par));

	step = (double *)malloc(sizeof(double)*n_par);
	

	ExitFlag=0;SuccessfulStep=1;done=0;
	
	SumSq = Norm(CostFun, n_costFun);   // levenbergMarquardt.m
	SumSq *= SumSq;

	for(i=0;i<n_costFun;i++) costFun[i]=CostFun[i];
	for(i=0;i<n_par;i++) XOUT[i]=Par0[i];

	
	for(I1=0;(I1<40)&&(done != 1); I1++){
		if(SuccessfulStep==1){
			for(i=0;i<n_costFun;i++){
				for(j=0;j<n_par;j++){
					AugJac[i][j]=JAC1[i][j];
				}
				AugRes[i] = -1.0*costFun[i]; 
			}
			for(i=0;i<n_par;i++){
				for(j=0;j<n_par;j++){
					if(i==j) AugJac[i+n_costFun][j]=sqrt(lambda);
					else AugJac[i+n_costFun][j]=0;
				}
				AugRes[i+n_costFun] = 0;  // AugRes = [-costFun; zeroPad]; % Augmented residual
			}
		}
		else {
			for(i=0;i<n_par;i++)
				AugJac[i+n_costFun][i]=lambda;
		}


		GetStep(AugJac, AugRes, step, n_costFun+n_par, n_par);

		for(i=0;i<n_par;i++) trialX[i]=XOUT[i]+step[i]; 
		
		ProjErr(trialCostFun, trialX, ms, xs);

		trialSumSq = Norm(trialCostFun, n_costFun);
		trialSumSq *= trialSumSq ;

//		printf("trSum : %20.8lf, Sum : %20.8lf\n", trialSumSq, SumSq);

		if(trialSumSq<SumSq){
			for(j=0;j<n_costFun;j++)costFun[j]=trialCostFun[j];
			for(j=0;j<n_par;j++) XOUT[j]=trialX[j];  
			if(SuccessfulStep==1){
				lambda *=0.1;
				if(lambda<2.2204e-16)lambda=2.2204e-16;
			}
		
			FindJAC1(JAC1,  DeltaX, costFun, XOUT, ms, xs, n_costFun, n_par); //get gradF 구하기 
			
			SuccessfulStep=1;

			FindGradF(gradF, JAC1, costFun,  n_costFun, n_par);
			
////////////////////////////////// Check  탈출조건 //////////////////
			done= 0;

			temp = MaxAbs(gradF, n_par);
			//if(Norm(gradF, 9) < (tolOpt+relFactor)){
			if(temp < (tolOpt*relFactor)){
				ExitFlag=1;  done=1;
			}
			else if (I1>=0){
				SumTmp = Norm(XOUT,n_par);
				//if(Norm(step,n_par)<((1.0e-4)*(1.4901e-8 + SumTmp))){//SqrtEps
				if(Norm(step,n_par)<((1.0e-4)*(SqrtEps + SumTmp))){//SqrtEps
					ExitFlag=4; done=1;
				}
			}
//////////////////////////////////////////////////////////////////////

			SumSq=trialSumSq;
		}
		else {
			lambda *=10;
			SuccessfulStep=0;
			if(lambda>1.0e16) ExitFlag =3;
		}
	}

    free(DeltaX); 	free(TypicalX); free(gradF);  free(trialX);
	free(trialCostFun); free(AugRes); free(costFun); free(step);
		
	for(i=0;i<n_costFun;i++)free(JAC1[i]);   free(JAC1); 
	for(i=0;i<n_costFun+n_par;i++)free(AugJac[i]);   free(AugJac); 
}

void COPTIC_DETECT_Option::InitialiseExternalp(double **RtnRs, double *RtnTs, double **Hs)
{
	unsigned int i,j;
	double  sum1=0, sum2=0, rate, Ra[3][3], Rb[3][3], Ca[3];//Rt[3][3],
	double Q[3][3], Ta[3], Tb[3], T[3];
	double **MatVT, **MatW, **MatU, Q1D[9];

	for(i=0;i<2;i++){
		sum1 += Hs[i][0]*Hs[i][0];	sum2 += Hs[i][1]*Hs[i][1];
	}

	rate = 2.0/(sqrt(sum1) + sqrt(sum2));

	for(i=0;i<3;i++){
		Ra[i][0] = rate*Hs[i][0]; Ra[i][1] = rate*Hs[i][1];
		Rb[i][0] = -1.0*Ra[i][0]; Rb[i][1] = -1.0*Ra[i][1];

		Ta[i] = rate*Hs[i][2];
		Tb[i] = -1.0*Ta[i];
	}
	Ra[0][2] = Ra[1][0]*Ra[2][1]-Ra[2][0]*Ra[1][1];
	Ra[1][2] = Ra[2][0]*Ra[0][1]-Ra[0][0]*Ra[2][1];
	Ra[2][2] = Ra[0][0]*Ra[1][1]-Ra[1][0]*Ra[0][1];
	for(i=0;i<3;i++) Rb[i][2] = Ra[i][2];
	for(i=0;i<3;i++){
		Ca[i]=0;
		for(j=0;j<3;j++){
			Ca[i] += Ra[j][i]*Ta[j];
		}
		Ca[i] *= -1.0;
	}

	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			if(Ca[2]<0)	Q[i][j]=Ra[i][j];
			else Q[i][j]=Rb[i][j];
		}
		if(Ca[2]<0)	T[i]=Ta[i];
		else T[i]=Tb[i];
	}

	MatVT = (double **)malloc(sizeof(double*)*3);
	for(i=0;i<3;i++)MatVT[i]=(double *)malloc(sizeof(double)*3);
	
	MatW = (double **)malloc(sizeof(double*)*3);
	for(i=0;i<3;i++)MatW[i]=(double *)malloc(sizeof(double)*3);

	MatU = (double **)malloc(sizeof(double*)*3);
	for(i=0;i<3;i++)MatU[i]=(double *)malloc(sizeof(double)*3);

	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			Q1D[i*3+j]=Q[i][j];
		}
	}

	SVD(Q1D, 3, 3, MatU, MatW, MatVT);  //  A = U*W*VT --> MatRtn : VT

	MulMatrix(MatU, MatVT, RtnRs, 3, 3, 3, 0);	

	for(i=0;i<3;i++) RtnTs[i]=T[i];

	for(i=0;i<3;i++)free(MatU[i]);   free(MatU);
	for(i=0;i<3;i++)free(MatW[i]);   free(MatW);
	for(i=0;i<3;i++)free(MatVT[i]);   free(MatVT);
}

void COPTIC_DETECT_Option::GenericProjExtended(double **Rtn, double **xs, double *p, double **R, double *t)
{
	unsigned int i, j; // N_Picture   N_Point
	double k[5], **Xc, Theta[N_Point], rXc[N_Point];
	double CosPhi[N_Point], SinPhi[N_Point] ,r[N_Point];
	double x[N_Point], y[N_Point];

	Xc = (double **)malloc(sizeof(double *)*3);                     //   
	for(i=0;i<3;i++)Xc[i]=(double *)malloc(sizeof(double )* N_Point);

	k[0] = p[0];  k[1] = p[1]; 
	k[2] = p[6];
	k[3] = p[7];
	k[4] = p[8];

	for(i=0;i<N_Point;i++)xs[i][2]=0;

	MulMatrix(R, xs, Xc, 3, N_Point , 3, 2);  //A x B'

	
	for(i=0;i<3;i++)
		for(j=0;j<N_Point;j++)
			Xc[i][j] += t[i];

	for(i=0;i<N_Point;i++){
		Theta[i]=acos(Xc[2][i]/sqrt(Xc[0][i]*Xc[0][i]+Xc[1][i]*Xc[1][i]+Xc[2][i]*Xc[2][i])); // 소수부분 확인 필요 
		rXc[i] = sqrt(Xc[0][i]*Xc[0][i]+Xc[1][i]*Xc[1][i]);

		if(rXc[i] == 0.0) rXc[i] =1.0;

		CosPhi[i] = Xc[0][i]/rXc[i];
		SinPhi[i] = Xc[1][i]/rXc[i];

		r[i]=0;
		for(j=0;j<5;j++){
			r[i] += k[j] * pow( Theta[i], ((double)(j)*2.0 + 1.0 ));
		}

		x[i] = r[i]*CosPhi[i];
		y[i] = r[i]*SinPhi[i];
		Rtn[i][0] = p[2]*x[i] + p[4];
		Rtn[i][1] = p[3]*y[i] + p[5];
	}
	for(i=0;i<3;i++)free(Xc[i]);   free(Xc); 
}

void COPTIC_DETECT_Option::FindJAC1(double **GradF,  double *DeltaX, double *VecD, double *Par0,  double ***ms, double ***xs,unsigned int n_vecD,unsigned int n_par)
{
	unsigned int i, PassCnt=0, gcnt=0;
	double SumSq=0,  *fplus, *xCurrent;// **CurrentH, VecH[9],*TypicalX,
	double DiffMinChange = 1.0e-8, DiffMaxChange = 0.1, *fCurrent;
	double TmpHold;//Err[N_Point], 

	fCurrent = (double *)malloc(sizeof(double)*n_vecD);

	fplus = (double *)malloc(sizeof(double)*n_vecD);


 	for(i=0;i<n_vecD;i++) fCurrent[i]=VecD[i];

	PassCnt=0; xCurrent=Par0;
									/// finitedifferences.m
	for(gcnt=0;gcnt<n_par;gcnt++){

		TmpHold = xCurrent[gcnt];

		xCurrent[gcnt] += DeltaX[gcnt];

		ProjErr(fplus, xCurrent, ms, xs);

		for(i=0;i<n_vecD;i++){
			GradF[i][gcnt]=(fplus[i]-fCurrent[i])/DeltaX[gcnt];
		}

		xCurrent[gcnt] = TmpHold;
	}
	free(fplus);  free(fCurrent);
}

void COPTIC_DETECT_Option::OnBnClickedChkImg()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(b_Chk_Img == 0){
		b_Chk_Img = 1;
	}else{
		b_Chk_Img =0;
	}
	UpdateData(FALSE);
}

void COPTIC_DETECT_Option::OnBnClickedButtonPrmsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str ="";
	UpdateData(TRUE);

	CenterPrm.Optical_Radius  = m_Optical_radius;

	i_Sensitivity = atoi(str_Sensitivity);

	m_Sensitivity = double((1000-i_Sensitivity))/1000.0;
	CenterPrm.Sensitivity  = m_Sensitivity;

	str_Sensitivity.Format("%d",i_Sensitivity);
	UpdateData(FALSE);
	str.Empty();
	str.Format("%6.2f",CenterPrm.Optical_Radius);
	WritePrivateProfileString(str_model,"OpticalRadius",str,str_ModelPath);

	str.Empty();
	str.Format("%d",i_Sensitivity);
	WritePrivateProfileString(str_model,"Sensitivity",str,str_ModelPath);
}

void COPTIC_DETECT_Option::OnBnClickedButtonDefaultrect()
{
	int PosX=(m_CAM_SIZE_WIDTH/2);
	int PosY=(m_CAM_SIZE_HEIGHT/2);
	int C_Dis_Width =m_CAM_SIZE_WIDTH * 0.27;
	int C_Dis_Height =m_CAM_SIZE_HEIGHT*0.3125;

	C_RECT[0].m_PosX = PosX-C_Dis_Width;
	C_RECT[0].m_PosY = PosY-C_Dis_Height;
	C_RECT[1].m_PosX = PosX+C_Dis_Width;
	C_RECT[1].m_PosY = PosY-C_Dis_Height;
	C_RECT[2].m_PosX = PosX-C_Dis_Width;
	C_RECT[2].m_PosY = PosY+C_Dis_Height;
	C_RECT[3].m_PosX = PosX+C_Dis_Width;
	C_RECT[3].m_PosY = PosY+C_Dis_Height;

	for(int t=0; t<4; t++){
		C_RECT[t].m_Width= ((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.25;
		C_RECT[t].m_Height=((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.25;
		C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);//////C

	}
	UpdateData(FALSE);
	ChangeCheck =1;
	
	for(int t=0; t<4; t++){
		ChangeItem[t]=t;
	}

	for(int t=0; t<4; t++){
		C_DATALIST.Update(t);
	}

	UploadList();//리스트컨트롤의 입력

}
void COPTIC_DETECT_Option::SETLIST(){
	C_DATALIST.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	C_DATALIST.InsertColumn(0,"Zone",LVCFMT_CENTER, 80);
	C_DATALIST.InsertColumn(1,"PosX",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(2,"PosY",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(3,"ST_X",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(4,"ST_Y",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(5,"EX_X",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(6,"EX_Y",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(7,"W",LVCFMT_CENTER, 40);
	C_DATALIST.InsertColumn(8,"H",LVCFMT_CENTER, 40);

}

void COPTIC_DETECT_Option::UploadList(){
	CString str="";
	
	C_DATALIST.DeleteAllItems();

	for(int t=0; t<4; t++){
		InIndex = C_DATALIST.InsertItem(t,"초기",0);
		
 	
		if(t ==0){
			str.Empty();str="LEFT_TOP";
		}
		if(t ==1){
			str.Empty();str="RIGHT_TOP";
		}
		if(t ==2){
			str.Empty();str="LEFT_BOTTOM";
		}
		if(t ==3){
			str.Empty();str="RIGHT_BOTTOM";
		}
		
		C_DATALIST.SetItemText(InIndex,0,str);
		str.Empty();str.Format("%d", C_RECT[t].m_PosX);
		C_DATALIST.SetItemText(InIndex,1,str);
		str.Empty();str.Format("%d", C_RECT[t].m_PosY);
		C_DATALIST.SetItemText(InIndex,2,str);
		
		str.Empty();
		str.Format("%d", C_RECT[t].m_Left);
		C_DATALIST.SetItemText(InIndex,3,str);
		str.Empty();
		str.Format("%d", C_RECT[t].m_Top);
		C_DATALIST.SetItemText(InIndex,4,str);
	
		str.Empty();
		str.Format("%d", C_RECT[t].m_Right+1);
		C_DATALIST.SetItemText(InIndex,5,str);
		str.Empty();
		str.Format("%d", C_RECT[t].m_Bottom+1);
		C_DATALIST.SetItemText(InIndex,6,str);

		str.Empty();
		str.Format("%d", C_RECT[t].m_Width);
		C_DATALIST.SetItemText(InIndex,7,str);
		str.Empty();
		str.Format("%d",C_RECT[t].m_Height);
		C_DATALIST.SetItemText(InIndex,8,str);
		
	}
}
void COPTIC_DETECT_Option::OnBnClickedButtonLoadprm()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	Load_parameter();
	UpdateData(FALSE);
	UploadList();
}

void COPTIC_DETECT_Option::OnBnClickedButtonSaveprm()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Save_parameter();

	ChangeCheck =0;
	for(int t=0; t<4; t++){
		ChangeItem[t]=-1;
	}
	for(int t=0; t<4; t++){
		C_DATALIST.Update(t);
	}
}
void COPTIC_DETECT_Option::List_COLOR_Change(){
	
	bool FLAG = TRUE;
	int Check=0;
	for(int t=0;t<4; t++){
		if(ChangeItem[t]==0)Check++;
		if(ChangeItem[t]==1)Check++;
		if(ChangeItem[t]==2)Check++;
		if(ChangeItem[t]==3)Check++;
	}
	if(Check !=4){
		FLAG =FALSE;
		ChangeItem[changecount]=iSavedItem;
	}

	if(!FLAG){
		for(int t=0; t<changecount+1; t++){
			for(int k=0; k<changecount+1; k++){
				
				if(ChangeItem[t] == ChangeItem[k]){
					if(t==k){break;	}			
					ChangeItem[k] =-1;
				}
			
			
			}
		}
		//----------------데이터 정렬
		int temp=0;

		for(int i=0; i<4; i++)
		{
			for(int j=i+1; j<4; j++)
			{
				if(ChangeItem[i] < ChangeItem[j])  // 내림차순
				{
					temp = ChangeItem[i];
					ChangeItem[i] = ChangeItem[j];
					ChangeItem[j] = temp;
				}
			}
		}
		//--------------------------------
		for(int t=0; t<4; t++){

			if(ChangeItem[t] ==-1){
				changecount =t;
				break;
			}
		}
	}
	/*if(FLAG){
		changecount++;
	}*/

	for(int t=0; t<4; t++){
		C_DATALIST.Update(t);
	}

}
void COPTIC_DETECT_Option::OnNMDblclkListRect(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Invalidate();

	
	iSavedItem = pNMITEM->iItem;
	iSavedSubitem = pNMITEM->iSubItem;

	

	if(pNMITEM->iItem != -1)
	{
		if(pNMITEM->iSubItem != 0 && pNMITEM->iSubItem <9)
		{		
			C_DATALIST.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			C_DATALIST.ClientToScreen(rect);
			this->ScreenToClient(rect);
			ChangeItem[iSavedItem]=iSavedItem;
			((CEdit *)GetDlgItem(IDC_EDIT_RECT_OPTIC))->SetWindowText(C_DATALIST.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_RECT_OPTIC))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
			((CEdit *)GetDlgItem(IDC_EDIT_RECT_OPTIC))->SetFocus();
			
		}
	}
}

void COPTIC_DETECT_Option::OnNMCustomdrawListRect(NMHDR *pNMHDR, LRESULT *pResult)
{
	 COLORREF text_color = 0;
        COLORREF bg_color = RGB(255, 255, 255);
     
        LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
		
	//	lplvcd->iPartId
        switch(lplvcd->nmcd.dwDrawStage){
            case CDDS_PREPAINT:
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
             
            // 배경 혹은 텍스트를 수정한다.
            case CDDS_ITEMPREPAINT:
                // 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
               /* if(lplvcd->nmcd.dwItemSpec == 0) text_color = RGB(255, 0, 0);
                else if(lplvcd->nmcd.dwItemSpec == 1) text_color = RGB(0, 255, 0);
                else text_color = RGB(0, 0, 255);
                lplvcd->clrText = text_color;*/
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
           
            // 서브 아이템의 배경 혹은 텍스트를 수정한다.
            case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
                if(lplvcd->iSubItem != 0){
                    // 1번째 행이라면...

					if(lplvcd->nmcd.dwItemSpec >=0 ||lplvcd->nmcd.dwItemSpec <4){
						if(ChangeCheck==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 10 ){
								for(int t=0; t<4; t++){
									if(lplvcd->nmcd.dwItemSpec == ChangeItem[t]){
										text_color = RGB(0, 0, 0);
										bg_color = RGB(250, 230, 200);					
									}
								}
							}
						
						
						}
					}

				    lplvcd->clrText = text_color;
                    lplvcd->clrTextBk = bg_color;
					
                }
                *pResult = CDRF_NEWFONT;
                return;
        }
}

void COPTIC_DETECT_Option::OnEnKillfocusEditRectOptic()
{

	CString str = "";
	ChangeCheck=1;
	if((iSavedItem != -1)&&(iSavedSubitem != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_RECT_OPTIC, str));
		List_COLOR_Change();
		if(C_DATALIST.GetItemText(iSavedItem, iSavedSubitem) != str){
			Change_DATA();
			UploadList();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_RECT_OPTIC))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_RECT_OPTIC))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );
	iSavedItem = -1;
	iSavedSubitem = -1;
	((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COPTIC_DETECT_Option::Change_DATA(){

		CString str;
		
		((CEdit *)GetDlgItemText(IDC_EDIT_RECT_OPTIC, str));
		C_DATALIST.SetItemText(iSavedItem, iSavedSubitem, str);

		int num = _ttoi(str);
		if(num < 0){
			num =0;
		}

		if(iSavedSubitem ==1){
			C_RECT[iSavedItem].m_PosX = num;
			
		}
		if(iSavedSubitem ==2){
			C_RECT[iSavedItem].m_PosY = num;
			
		}
		if(iSavedSubitem ==3){
			C_RECT[iSavedItem].m_Left = num;
			C_RECT[iSavedItem].m_PosX=(C_RECT[iSavedItem].m_Right+1 +C_RECT[iSavedItem].m_Left) /2;
			C_RECT[iSavedItem].m_Width=C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;

		}
		if(iSavedSubitem ==4){
			C_RECT[iSavedItem].m_Top = num;
			C_RECT[iSavedItem].m_PosY=(C_RECT[iSavedItem].m_Top +C_RECT[iSavedItem].m_Bottom+1)/2;
			C_RECT[iSavedItem].m_Height=C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;

		}
		if(iSavedSubitem ==5){
			if(num == 0){
				C_RECT[iSavedItem].m_Right = 0;
			}
			else{
				C_RECT[iSavedItem].m_Right = num;
				C_RECT[iSavedItem].m_PosX=(C_RECT[iSavedItem].m_Right +C_RECT[iSavedItem].m_Left+1) /2;
				C_RECT[iSavedItem].m_Width=C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;
			}
		}
		if(iSavedSubitem ==6){
			if(num ==0){
				C_RECT[iSavedItem].m_Bottom =0;
			}
			else{
				C_RECT[iSavedItem].m_Bottom = num;
				C_RECT[iSavedItem].m_PosY=(C_RECT[iSavedItem].m_Top +C_RECT[iSavedItem].m_Bottom+1)/2;
				C_RECT[iSavedItem].m_Height=C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;
			}
		}
		if(iSavedSubitem ==7){
			C_RECT[iSavedItem].m_Width = num;
		}
		if(iSavedSubitem ==8){
			C_RECT[iSavedItem].m_Height = num;
		}
		
		C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
			
		if(C_RECT[iSavedItem].m_Left < 0){//////////////////////수정
			C_RECT[iSavedItem].m_Left = 0;
			C_RECT[iSavedItem].m_Width = C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Right >m_CAM_SIZE_WIDTH){
			C_RECT[iSavedItem].m_Right = m_CAM_SIZE_WIDTH;
			C_RECT[iSavedItem].m_Width = C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Top< 0){
			C_RECT[iSavedItem].m_Top = 0;
			C_RECT[iSavedItem].m_Height = C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Bottom >m_CAM_SIZE_HEIGHT){
			C_RECT[iSavedItem].m_Bottom = m_CAM_SIZE_HEIGHT;
			C_RECT[iSavedItem].m_Height = C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Height<= 0){
			C_RECT[iSavedItem].m_Height = 1;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Height >=m_CAM_SIZE_HEIGHT){
			C_RECT[iSavedItem].m_Height = m_CAM_SIZE_HEIGHT;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}

		if(C_RECT[iSavedItem].m_Width <= 0){
			C_RECT[iSavedItem].m_Width = 1;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Width >=m_CAM_SIZE_WIDTH){
			C_RECT[iSavedItem].m_Width = m_CAM_SIZE_WIDTH;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}



		C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);

	

		CString data = "";

		if(iSavedSubitem ==1){
			data.Format("%d",C_RECT[iSavedItem].m_PosX);			
		}
		else if(iSavedSubitem ==2){
			data.Format("%d",C_RECT[iSavedItem].m_PosY);
		}
		else if(iSavedSubitem ==3){
			data.Format("%d",C_RECT[iSavedItem].m_Left);			
		}
		else if(iSavedSubitem ==4){
			data.Format("%d",C_RECT[iSavedItem].m_Top);			
		}
		else if(iSavedSubitem ==5){
			data.Format("%d",C_RECT[iSavedItem].m_Right);			
		}
		else if(iSavedSubitem ==6){
			data.Format("%d",C_RECT[iSavedItem].m_Bottom);			
		}
		else if(iSavedSubitem ==7){
			data.Format("%d",C_RECT[iSavedItem].m_Width);			
		}
		else if(iSavedSubitem ==8){
			data.Format("%d",C_RECT[iSavedItem].m_Height);			
		}		
		((CEdit *)GetDlgItem(IDC_EDIT_RECT_OPTIC))->SetWindowText(data);

}
BOOL COPTIC_DETECT_Option::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	int znum = zDelta/120;
	CWnd *focusid;
	focusid = GetFocus();
	bool FLAG=0;
	if(znum > 0 ){
		FLAG = 1;
	}
	else if(znum < 0 ){
		FLAG =0;
	}
	else if(znum == 0){
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	}
	int casenum = focusid->GetDlgCtrlID();
	switch(casenum){
	
		case IDC_EDIT_RECT_OPTIC :
			
			if(FLAG == 1){
				if((Change_DATA_CHECK(1) == TRUE) /*|| (znum ==-1)*/){
					EditWheelIntSet(IDC_EDIT_RECT_OPTIC,znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditRectOptic();
				}
			}
			if(FLAG == 0){
				if((Change_DATA_CHECK(0) == TRUE)/* || (znum == 1)*/){
					EditWheelIntSet(IDC_EDIT_RECT_OPTIC,znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditRectOptic();
				}
			}

			break;

	}

	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

bool COPTIC_DETECT_Option::Change_DATA_CHECK(bool FLAG){

		
	BOOL STAT = TRUE;
	//상한
	if(C_RECT[iSavedItem].m_Width > m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Height > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosX > m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosY > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Top > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Bottom > m_CAM_SIZE_HEIGHT  )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Left> m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Right> m_CAM_SIZE_WIDTH )
		STAT = FALSE;

	//하한

	if(C_RECT[iSavedItem].m_Width < 1 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Height < 1 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosX < 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosY < 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Top < 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Bottom < 0  )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Left< 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Right< 0 )
		STAT = FALSE;

	if(FLAG  == 1){
		if(C_RECT[iSavedItem].m_Width == 1 )
			STAT = FALSE;
		if(C_RECT[iSavedItem].m_Height == 1 )
			STAT = FALSE;
		
	}


	return STAT;
	

	
	

}
void COPTIC_DETECT_Option::OnEnChangeEditRectOptic()
{
	int num = iSavedItem;
	if((C_RECT[num].m_PosX >= 0)&&(C_RECT[num].m_PosX < m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_PosY >= 0)&&(C_RECT[num].m_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_RECT[num].m_Left>=0)&&(C_RECT[num].m_Left<m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_Top>=0)&&(C_RECT[num].m_Top<m_CAM_SIZE_HEIGHT)&&
		(C_RECT[num].m_Right>=0)&&(C_RECT[num].m_Right<m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_Bottom>=0)&&(C_RECT[num].m_Bottom<m_CAM_SIZE_HEIGHT)&&
		(C_RECT[num].m_Width>=0)&&(C_RECT[num].m_Width<m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_Height>=0)&&(C_RECT[num].m_Height<m_CAM_SIZE_HEIGHT)){

			((CButton *)GetDlgItem(IDC_BUTTON_SAVEPRM))->EnableWindow(1);
		

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_SAVEPRM))->EnableWindow(0);
	}
}

void COPTIC_DETECT_Option::EditWheelIntSet(int nID,int Val)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = 0;
	}else{
		buf = GetDlgItemInt(nID);
		buf+= Val;
	}
	SetDlgItemInt(nID,buf,1);
	((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
}
void COPTIC_DETECT_Option::OnBnClickedButtonMicom()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->MicomOptic_Popup();
}

void COPTIC_DETECT_Option::OnBnClickedButtontest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(0);
	((CImageTesterDlg  *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(1);

}



#pragma region LOT관련 함수
void COPTIC_DETECT_Option::LOT_Set_List(CListCtrl *List){

	while(List->DeleteColumn(0));
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT 명",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"카메라상태",LVCFMT_CENTER, 80);	
	List->InsertColumn(6,"init X",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"init Y",LVCFMT_CENTER, 80);
	List->InsertColumn(8,"FINAL X",LVCFMT_CENTER, 80);
	List->InsertColumn(9,"FINAL Y",LVCFMT_CENTER, 80);
	List->InsertColumn(10,"적용 수치 X",LVCFMT_CENTER, 80);
	List->InsertColumn(11,"적용 수치 Y",LVCFMT_CENTER, 80);
	List->InsertColumn(12,"Offset X",LVCFMT_CENTER, 80);
	List->InsertColumn(13,"Offset Y",LVCFMT_CENTER, 80);
	List->InsertColumn(14,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(15,"비고",LVCFMT_CENTER, 80);
	Lot_InsertNum =16;
	
}

void COPTIC_DETECT_Option::LOT_InsertDataList(){
	CString strCnt="";

	int Index = m_Lot_OpticalCenterList.InsertItem(Lot_StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index+1);
	m_Lot_OpticalCenterList.SetItemText(Index,0,strCnt);

	int CopyIndex = m_CenterWorklist.GetItemCount()-1;

	int count =0;
	for(int t=0; t< Lot_InsertNum;t++){
		if((t != 2)&&(t!=0)){
			m_Lot_OpticalCenterList.SetItemText(Index,t, m_CenterWorklist.GetItemText(CopyIndex,count));
			count++;

		}else if(t==0){
			count++;
		}else{
			m_Lot_OpticalCenterList.SetItemText(Index,t,((CImageTesterDlg  *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;

}
void COPTIC_DETECT_Option::LOT_EXCEL_SAVE(){
	CString Item[9]={"카메라상태","init X","init Y","FINAL X","FINAL Y","적용 수치 X","적용 수치 Y","Offset X","Offset Y"};//수정해야할 곳 0203

	CString Data ="";
	CString str="";
	str = "***********************왜곡광축 검사***********************\n";
	Data += str;
	
	str.Empty();
	str.Format("기준광축 X, %d,,,기준광축 Y, %d\n ",CenterPrm.StandX,CenterPrm.StandY);
	Data += str;
	str.Empty();
	str.Format("합격편차 X, %3.1f,,,합격편차 Y, %3.1f\n\n ",CenterPrm.PassMaxDevX,CenterPrm.PassMaxDevY);
	Data += str;

	str.Empty();
	str = "*************************************************\n";
	Data += str;
	((CImageTesterDlg  *)m_pMomWnd)->LOT_SAVE_EXCEL("왜곡광축",Item,9,&m_Lot_OpticalCenterList,Data);
}

bool COPTIC_DETECT_Option::LOT_EXCEL_UPLOAD(){
	m_Lot_OpticalCenterList.DeleteAllItems();
	if(((CImageTesterDlg  *)m_pMomWnd)->LOT_EXCEL_UPLOAD("왜곡광축",&m_Lot_OpticalCenterList,1,&Lot_StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}

#pragma endregion 