#pragma once
#include "ETCUSER.H"
#include "afxcmn.h"

// CVER_Option 대화 상자입니다.

class CVER_Option : public CDialog
{
	DECLARE_DYNAMIC(CVER_Option)

public:
	CVER_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CVER_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_VERSION };

	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);

	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	CFont	ft,ft2;

	tResultVal Run(void);
	void	Pic(CDC *cdc);
	bool	VerPic(CDC *cdc);
	BOOL	m_Etc_State;
	void	InitPrm();
	void	Save(int NUM);
	void	initstat();

	void	Set_List(CListCtrl *List);
	void	LOT_Set_List(CListCtrl *List);
	int	Lot_InsertNum;
	void	EXCEL_SAVE();
	void	LOT_EXCEL_SAVE();
	bool	EXCEL_UPLOAD();
	bool	LOT_EXCEL_UPLOAD();
	void	InsertList();
	int		InsertIndex;
	int		ListItemNum;

	void	Load_parameter();
	void	Save_parameter();	

	void	FAIL_UPLOAD();
	BOOL	b_StopFail;
	int		StartCnt;
	int		Lot_StartCnt;

	void	LOT_InsertDataList();

	unsigned long m_SetVerDat[2];//0:app data 1:bl data
	CString str_SetVerDat[2];//0:app data 1:bl data
	BOOL	b_Match[2];
	BOOL	b_tMatch;

	void	InitEVMS();
	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();

	COLORREF st_col,Valcol[4];
	void	VER_RESULT(int num,int col,LPCSTR lpcszString, ...);
	void	VER_STATE(int col,LPCSTR lpcszString, ...);
	void	VER_RESULT_STATE(int col,LPCSTR lpcszString, ...);
	void	VER_ST_VALUE(int num,LPCSTR lpcszString, ...);
	void	VER_ST_VALUE_SET();

	CString Old_str_Wrdata1;
	CString Old_str_Wrdata2;

	BOOL	VER_READ();
	
private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
	CString		strTitle;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CListCtrl m_VERList;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEditSetverWr1();
	afx_msg void OnEnChangeEditSetverWr2();
	CString str_Wrdata1;
	CString str_Wrdata2;
//	afx_msg void OnBnClickedBtnDtcsave();
	afx_msg void OnBnClickedBtnVerread();
	afx_msg void OnBnClickedBtnVersave();
	CListCtrl m_Lot_VERList;
};
