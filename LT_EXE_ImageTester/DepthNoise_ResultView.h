#pragma once


// CDepthNoise_ResultView 대화 상자입니다.

class CDepthNoise_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CDepthNoise_ResultView)

public:
	CDepthNoise_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDepthNoise_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_DepthNoise };
	CFont ft1;
	COLORREF txcol_TVal,bkcol_TVal;
	void	DepthNoise_TEXT(LPCSTR lpcszString, ...);
	void	DepthNoiseNUM_TEXT(LPCSTR lpcszString, ...);
	void	RESULT_TEXT(unsigned char col,LPCSTR lpcszString, ...);
	void	Setup(CWnd* IN_pMomWnd);
void	Initstat();
private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnSetfocusEditDepthNoisetxt();
	afx_msg void OnEnSetfocusEditDepthNoisenum();
	afx_msg void OnEnSetfocusEditDepthNoiseresult();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
