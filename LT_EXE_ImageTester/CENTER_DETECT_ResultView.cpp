// Option_ResultCDetect.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "CENTER_DETECT_ResultView.h"


// CCENTER_DETECT_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCENTER_DETECT_ResultView, CDialog)

CCENTER_DETECT_ResultView::CCENTER_DETECT_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CCENTER_DETECT_ResultView::IDD, pParent)
{

}

CCENTER_DETECT_ResultView::~CCENTER_DETECT_ResultView()
{
}

void CCENTER_DETECT_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FILE_TRANS, m_CtrFileProgress);
}


BEGIN_MESSAGE_MAP(CCENTER_DETECT_ResultView, CDialog)
	ON_EN_SETFOCUS(IDC_FILE_NAME, &CCENTER_DETECT_ResultView::OnEnSetfocusFileName)
	ON_EN_SETFOCUS(IDC_WRMODE, &CCENTER_DETECT_ResultView::OnEnSetfocusWrmode)
	ON_EN_SETFOCUS(IDC_EDIT_X_CH1, &CCENTER_DETECT_ResultView::OnEnSetfocusEditXCh1)
	ON_EN_SETFOCUS(IDC_EDIT_Y_CH1, &CCENTER_DETECT_ResultView::OnEnSetfocusEditYCh1)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETX_CH1, &CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetxCh1)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETY_CH1, &CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetyCh1)
	ON_EN_SETFOCUS(IDC_EDIT_X_CH2, &CCENTER_DETECT_ResultView::OnEnSetfocusEditXCh2)
	ON_EN_SETFOCUS(IDC_EDIT_Y_CH2, &CCENTER_DETECT_ResultView::OnEnSetfocusEditYCh2)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETX_CH2, &CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetxCh2)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETY_CH2, &CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetyCh2)
	ON_EN_SETFOCUS(IDC_EDIT_X_CH3, &CCENTER_DETECT_ResultView::OnEnSetfocusEditXCh3)
	ON_EN_SETFOCUS(IDC_EDIT_Y_CH3, &CCENTER_DETECT_ResultView::OnEnSetfocusEditYCh3)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETX_CH3, &CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetxCh3)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETY_CH3, &CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetyCh3)
	ON_EN_SETFOCUS(IDC_EDIT_X_CH4, &CCENTER_DETECT_ResultView::OnEnSetfocusEditXCh4)
	ON_EN_SETFOCUS(IDC_EDIT_Y_CH4, &CCENTER_DETECT_ResultView::OnEnSetfocusEditYCh4)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETX_CH4, &CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetxCh4)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETY_CH4, &CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetyCh4)
	ON_EN_SETFOCUS(IDC_RUN_MODE, &CCENTER_DETECT_ResultView::OnEnSetfocusRunMode)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CCENTER_DETECT_ResultView 메시지 처리기입니다.
void CCENTER_DETECT_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CCENTER_DETECT_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}


BOOL CCENTER_DETECT_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	Font_Size_Change(IDC_RUN_MODE,&font1,35,20);
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	pRunEdit = (CEdit *)GetDlgItem(IDC_RUN_MODE);
	pFileEdit = (CEdit *)GetDlgItem(IDC_FILE_NAME);
	pWrModeEdit = (CEdit *)GetDlgItem(IDC_WRMODE);
	((CEdit *)GetDlgItem(IDC_EDIT_NAME))->SetWindowText("BIN 파일");

	Font_Size_Change(IDC_EDIT_NAME,&font2,30,18);
	GetDlgItem(IDC_FILE_NAME)->SetFont(&font2);
	GetDlgItem(IDC_WRMODE)->SetFont(&font2);

	GetDlgItem(IDC_EDIT_X_CH1)->SetFont(&font2);
	GetDlgItem(IDC_EDIT_Y_CH1)->SetFont(&font2);
	GetDlgItem(IDC_EDIT_OFFSETX_CH1)->SetFont(&font2);
	GetDlgItem(IDC_EDIT_OFFSETY_CH1)->SetFont(&font2);


	m_CtrFileProgress.SetRange(0,1000);
	m_CtrFileProgress.SetPos(0);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CCENTER_DETECT_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	/*	if(pMsg->wParam == VK_ADD){
			((CImageTesterDlg *)m_pMomWnd)->ExBtnStart();
			return TRUE;
		}
		if (pMsg->wParam == VK_SUBTRACT){
			((CImageTesterDlg *)m_pMomWnd)->ExBtnStop();
			return TRUE;
		}*/
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CCENTER_DETECT_ResultView::AxisX_Txt(int ch,LPCSTR lpcszString, ...)
{	
	switch(ch){
		case 0:((CEdit *)GetDlgItem(IDC_EDIT_X_CH1))->SetWindowText(lpcszString);
			break;
		case 1:((CEdit *)GetDlgItem(IDC_EDIT_X_CH2))->SetWindowText(lpcszString);
			break;
		case 2:((CEdit *)GetDlgItem(IDC_EDIT_X_CH3))->SetWindowText(lpcszString);
			break;
		case 3:((CEdit *)GetDlgItem(IDC_EDIT_X_CH4))->SetWindowText(lpcszString);
			break;
	}
	
}
void CCENTER_DETECT_ResultView::AxisY_Txt(int ch,LPCSTR lpcszString, ...)
{	
	switch(ch){
		case 0:((CEdit *)GetDlgItem(IDC_EDIT_Y_CH1))->SetWindowText(lpcszString);
			break;
		case 1:((CEdit *)GetDlgItem(IDC_EDIT_Y_CH2))->SetWindowText(lpcszString);
			break;
		case 2:((CEdit *)GetDlgItem(IDC_EDIT_Y_CH3))->SetWindowText(lpcszString);
			break;
		case 3:((CEdit *)GetDlgItem(IDC_EDIT_Y_CH4))->SetWindowText(lpcszString);
			break;
	}
}
void CCENTER_DETECT_ResultView::OffsetX_Txt(int ch,LPCSTR lpcszString, ...)
{	
	switch(ch){
		case 0:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETX_CH1))->SetWindowText(lpcszString);
			break;
		case 1:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETX_CH2))->SetWindowText(lpcszString);
			break;
		case 2:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETX_CH3))->SetWindowText(lpcszString);
			break;
		case 3:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETX_CH4))->SetWindowText(lpcszString);
			break;
	}
}
void CCENTER_DETECT_ResultView::OffsetY_Txt(int ch,LPCSTR lpcszString, ...)
{	
	switch(ch){
		case 0:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETY_CH1))->SetWindowText(lpcszString);
			break;
		case 1:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETY_CH2))->SetWindowText(lpcszString);
			break;
		case 2:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETY_CH3))->SetWindowText(lpcszString);
			break;
		case 3:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETY_CH4))->SetWindowText(lpcszString);
			break;
	}
}

void CCENTER_DETECT_ResultView::InitStat(){

	for(int t=0; t<4; t++){
		AxisX_Txt(t,"");
		AxisY_Txt(t,"");
		OffsetX_Txt(t,"");
		OffsetY_Txt(t,"");
	}
}
void CCENTER_DETECT_ResultView::OnEnSetfocusFileName()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusWrmode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditXCh1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditYCh1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetxCh1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetyCh1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditXCh2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditYCh2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetxCh2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetyCh2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditXCh3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditYCh3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetxCh3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetyCh3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditXCh4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditYCh4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetxCh4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusEditOffsetyCh4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::OnEnSetfocusRunMode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CCENTER_DETECT_ResultView::SetOptMode(int num)
{
	if(num == 1){
		pFileEdit->ShowWindow(SW_HIDE);
		m_CtrFileProgress.ShowWindow(SW_HIDE);
		((CEdit *)GetDlgItem(IDC_EDIT_NAME))->ShowWindow(SW_HIDE);
	}else{
		pFileEdit->ShowWindow(SW_SHOW);
		m_CtrFileProgress.ShowWindow(SW_SHOW);
		((CEdit *)GetDlgItem(IDC_EDIT_NAME))->ShowWindow(SW_SHOW);
	}

	
}

HBRUSH CCENTER_DETECT_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_NAME){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() ==IDC_FILE_NAME){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() ==IDC_WRMODE){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_X_CH1){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_Y_CH1){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_OFFSETX_CH1){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_OFFSETY_CH1){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}
