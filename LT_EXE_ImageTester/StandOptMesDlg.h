#pragma once
#include "afxcmn.h"
#include "resource.h"

// StandOptMesDlg 대화 상자입니다.

class CStandOptMesDlg : public CDialog
{
	DECLARE_DYNAMIC(CStandOptMesDlg)

public:
	CStandOptMesDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CStandOptMesDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_STAND_OPT_MES_DIALOG };

	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	CFont ft;
	void	MODE_EditView(LPCSTR lpcszString, ...);

	void	InitEVMS();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

protected:
	BOOL	m_bUseSocket;

	unsigned int m_socket;

	CString m_szMachineCode;
	CString	m_szIp;
	CString m_szPort;
	CString m_szMesLocation;
	CString m_szModelID;

	CWnd		*m_pMomWnd;
public:
	void	InitUI();
	void	Setup(CWnd* IN_pMomWnd);
	void	EnableWindow(BOOL bEnable);

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedCheckSocket();
	afx_msg void OnBnClickedButtonSaveCode();
	afx_msg void OnBnClickedButtonUdpSave();
	afx_msg void OnBnClickedButtonMesLocationSave();
	CIPAddressCtrl m_ctrllp_IP;
	afx_msg void OnEnChangeUdpRxPortEdit();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnEnChangeEditCode();
	afx_msg void OnBnClickedButtonLotidSave();
	afx_msg void OnEnChangeEditLotid();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedLotreportpathsave();
	CString REPORT_PATH;
	afx_msg void OnEnSetfocusEditMode();
};
