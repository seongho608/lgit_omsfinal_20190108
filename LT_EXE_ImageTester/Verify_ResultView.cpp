// Verify_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Verify_ResultView.h"


// CVerify_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CVerify_ResultView, CDialog)

CVerify_ResultView::CVerify_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CVerify_ResultView::IDD, pParent)
{

}

CVerify_ResultView::~CVerify_ResultView()
{
}

void CVerify_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS_TESTRES, m_ProgressTestRes);
}


BEGIN_MESSAGE_MAP(CVerify_ResultView, CDialog)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CVerify_ResultView 메시지 처리기입니다.
void CVerify_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

HBRUSH CVerify_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VERIFY_STATE){
		pDC->SetBkColor(bk_st_col);
		pDC->SetTextColor(tx_st_col);
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}
BOOL CVerify_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	Font_Size_Change(IDC_EDIT_VERIFY_STATE,&ft2,100,38);
	m_ProgressTestRes.SetRange(0,1000);
	VER_STATE(0,"Stand By");
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CVerify_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;

	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);

	GetDlgItem(nID)->SetFont(font);
}
void CVerify_ResultView::VER_STATE(int col,LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_VERIFY_STATE))->SetWindowText(lpcszString);
	if(col == 0){//스탠드 바이
		tx_st_col = RGB(255, 255, 255);
		bk_st_col = RGB(47, 157, 39);
	}else if(col == 1){//성공
		tx_st_col = RGB(255, 255, 255);
		bk_st_col = RGB(18, 69, 171);
	}else if(col == 2){//실패
		tx_st_col = RGB(255, 255, 255);
		bk_st_col = RGB(201, 0, 0);
	}else if(col == 3){
		tx_st_col = RGB(0, 0, 0);
		bk_st_col = RGB(255, 255, 0);
	}
}

void CVerify_ResultView::InitStat()
{
	
	VER_STATE(0,"Stand By");
}
BOOL CVerify_ResultView::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}
