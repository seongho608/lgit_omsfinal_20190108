#pragma once
#include "afxcmn.h"
#include "ETCUSER.H"
#include "resource.h"
#include <vector>
// CDepthNoise_Option 대화 상자입니다.

class CDepthNoise_Option : public CDialog
{
	DECLARE_DYNAMIC(CDepthNoise_Option)

public:
	CDepthNoise_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDepthNoise_Option();
 
	int		m_CAM_SIZE_WIDTH;
	int		m_CAM_SIZE_HEIGHT;
	int		StartCnt;
	bool    m_Success;
	tResultVal Run();
	void	Pic(CDC *cdc);
	void	InitPrm();
	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
	void	StatePrintf(LPCSTR lpcszString, ...);
	bool	DepthNoisePic(CDC *cdc,int NUM);


	CString DepthNoise_filename;
	void	Save(int NUM);

	void	Save_parameter();
	void	Load_parameter();
	void	UploadList();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_DepthNoise };

	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void InsertList();
	int InsertIndex;
	int Lot_InsertIndex;
	int ListItemNum;
	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();
	void FAIL_UPLOAD();
	BOOL	b_StopFail;

	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();

	#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	int		Lot_StartCnt;
	int		Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

	void	InitEVMS();

#pragma endregion

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:

	virtual BOOL OnInitDialog					();
	virtual BOOL PreTranslateMessage			(MSG* pMsg);
	afx_msg void OnShowWindow					(BOOL bShow, UINT nStatus);
	afx_msg BOOL OnMouseWheel					(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnTimer						(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonTest			();
	afx_msg void OnBnClickedButtonSave			();
	afx_msg void OnNMClickListDepthNoise		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawListDepthNoise	(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListDepthNoise		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnChangeEditTMod				();
	afx_msg void OnEnKillfocusEditTMod			();

	void	OnSetChange_Data	(int iVal);
	void	OnSetList_Header	();
	void	OnSetList_Data		();

	BOOL DepthNoiseGen_16bit	(WORD *GRAYScanBuf);

	UINT Edit_GetValue	(UINT nID, BOOL bHex = TRUE);
	void Edit_SetValue	(UINT nID, DWORD wData, BOOL bHex = TRUE);
	
	int		m_iSavedItem;
	int		m_iSavedSubitem;

	DWORD	m_dwSlaveID;
	DWORD	m_dwAddress;
	UINT	m_nMinSpec;
	UINT	m_nMaxSpec;

	CRect	m_rtROI;

	CListCtrl m_ROI_List;
	CListCtrl m_WorkList;
	CListCtrl m_Lot_WorkList;

	BOOL	m_bResult;
	UINT    m_nBright;
};

void CModel_Create	(CDepthNoise_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO);
void CModel_Delete	(CDepthNoise_Option **pWnd);