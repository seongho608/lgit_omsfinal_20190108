// Pogo_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Pogo_Option.h"


// CPogo_Option 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPogo_Option, CDialog)

CPogo_Option::CPogo_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CPogo_Option::IDD, pParent)
	, str_PogoGroupNewName(_T(""))
	, str_PogoCount(_T(""))
{

}

CPogo_Option::~CPogo_Option()
{
}

void CPogo_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_POGOGROUPNAME, str_PogoGroupNewName);
	DDX_Text(pDX, IDC_EDIT_POGOGROUPNAME2, str_PogoCount);
}


BEGIN_MESSAGE_MAP(CPogo_Option, CDialog)
	ON_EN_CHANGE(IDC_EDIT_POGOGROUPNAME, &CPogo_Option::OnEnChangeEditPogogroupname)
	ON_BN_CLICKED(IDC_BUTTON_GROUPSAVE, &CPogo_Option::OnBnClickedButtonGroupsave)
	ON_CBN_SELCHANGE(IDC_COMBO_GROUP, &CPogo_Option::OnCbnSelchangeComboGroup)
	ON_BN_CLICKED(IDC_BUTTON_GROUPDEL, &CPogo_Option::OnBnClickedButtonGroupdel)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON_CNTSAVE, &CPogo_Option::OnBnClickedButtonCntsave)
	ON_EN_CHANGE(IDC_EDIT_POGOGROUPNAME2, &CPogo_Option::OnEnChangeEditPogogroupname2)
	ON_BN_CLICKED(IDC_BTN_POGOUP, &CPogo_Option::OnBnClickedBtnPogoup)
	ON_BN_CLICKED(IDC_BUTTON_CNTINIT, &CPogo_Option::OnBnClickedButtonCntinit)
	ON_BN_CLICKED(IDC_BTN_POGODOWN, &CPogo_Option::OnBnClickedBtnPogodown)
	ON_EN_SETFOCUS(IDC_EDIT_GROUPVIEW, &CPogo_Option::OnEnSetfocusEditGroupview)
END_MESSAGE_MAP()


// CPogo_Option 메시지 처리기입니다.

void CPogo_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CPogo_Option::OnEnChangeEditPogogroupname()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	CString str = str_PogoGroupNewName;
	str.Remove(' ');
	if(str != ""){
		((CButton *)GetDlgItem(IDC_BUTTON_GROUPSAVE))->EnableWindow(1);
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_GROUPSAVE))->EnableWindow(0);
	}

	
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CPogo_Option::OnCbnSelchangeComboGroup()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SetPogoSet();
}

BOOL CPogo_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	pModelCombo = (CComboBox *)GetDlgItem(IDC_COMBO_GROUP);//모델
	Font_Size_Change(IDC_EDIT_GROUPVIEW,&ft1,100,30);

	InitEVMS();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPogo_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CComboBox *)GetDlgItem(IDC_COMBO_GROUP))->EnableWindow(0);

		((CEdit *)GetDlgItem(IDC_EDIT_POGOGROUPNAME))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_POGOGROUPNAME2))->EnableWindow(0);

		((CButton *)GetDlgItem(IDC_BUTTON_GROUPSAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_GROUPDEL))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_CNTSAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_CNTINIT))->EnableWindow(0);

		((CButton *)GetDlgItem(IDC_BTN_POGOUP))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_POGODOWN))->EnableWindow(0);
	}
}

void CPogo_Option::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}

void CPogo_Option::InitUI()
{
	CString str = "";
	
	i_PogoPinSET = ((CImageTesterDlg *)m_pMomWnd)->i_PogoPinSET;
	str_PogoCount.Format("%d",i_PogoPinSET);
	i_PogoCnt = ((CImageTesterDlg *)m_pMomWnd)->i_PogoCnt;
	str.Format("%d",i_PogoCnt);
	CNT_VIEW(str);
	str_PogoGroupNewName = "";

	((CButton *)GetDlgItem(IDC_BUTTON_GROUPSAVE))->EnableWindow(0);
	((CButton *)GetDlgItem(IDC_BUTTON_CNTSAVE))->EnableWindow(0);
	UpdateData(FALSE);
}


BOOL CPogo_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

HBRUSH CPogo_Option::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_GROUPVIEW){
        pDC->SetTextColor(RGB(255,255,255));
		pDC->SetBkColor(RGB(86,86,86));
    }
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


void CPogo_Option::OnEnChangeEditPogogroupname2()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	str_PogoCount.Remove(' ');
	if(str_PogoCount == ""){
		UpdateData(FALSE);
		((CButton *)GetDlgItem(IDC_BUTTON_CNTSAVE))->EnableWindow(0);
		return;
	}
	int ibuf = atoi(str_PogoCount);
	str_PogoCount.Format("%d",ibuf);
	if(ibuf != ((CImageTesterDlg *)m_pMomWnd)->i_PogoPinSET){
		((CButton *)GetDlgItem(IDC_BUTTON_CNTSAVE))->EnableWindow(1);
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_CNTSAVE))->EnableWindow(0);
	}
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_POGOGROUPNAME2))->GetSel();
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_POGOGROUPNAME2))->SetSel(SELCURSER,SELCURSER);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CPogo_Option::OnBnClickedButtonGroupdel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString ModelPath = "";
	int index = pModelCombo->GetCurSel();
	CString delstr;
	if(index < 0){
		return;
	}
	pModelCombo->GetLBText(pModelCombo->GetCurSel(),delstr);
	ModelPath = (((CImageTesterDlg *)m_pMomWnd)->Pogo_path) + ("\\") + delstr +(".ini"); 

	CPopUP2 subDlg;
	subDlg.Warning_TEXT = "  [  "+ delstr +"  ] 모델을 삭제하시겠습니까?";
	if(subDlg.DoModal() == IDOK){
		if(((CImageTesterDlg *)m_pMomWnd)->MatchPassWord == ""){
			DeleteDir(ModelPath);
			if(delstr == "DEFAULT"){
				CString example;
				example.Empty();
				example="TOTAL";
				WritePrivateProfileString(example,NULL,"",ModelPath);
				((CImageTesterDlg *)m_pMomWnd)->Pogo_Load_Prm();
			}
		}else{
			CUserSwitching subdlg2;
			subdlg2.Setup((CImageTesterDlg *)m_pMomWnd);
			if(subdlg2.DoModal()){
				DeleteDir(ModelPath);
				if(delstr == "DEFAULT"){
					CString example;
					example.Empty();
					example="TOTAL";
					WritePrivateProfileString(example,NULL,"",ModelPath);
					((CImageTesterDlg *)m_pMomWnd)->Pogo_Load_Prm();
				}
			}else{
				AfxMessageBox("패스워드가 틀렸으므로 삭제 되지 않습니다");
			}
		}
	}

	((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_HIDE);
	((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_SHOW);
	((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->OnShowSel(2);
	MainGroupSel();
}

void CPogo_Option::MainGroupSel()//초기 모델명에 따른 폴더가 있는지 확인한다. 
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//--------------------초기화파일을 읽어 과거에 저장된 folder이름을 불러온다.-------------
	CString	   loadmodel = "";
	char cload[100] ={0,};
	int frnum;//폴더이름갯수
	((CImageTesterDlg *)m_pMomWnd)->Pogo_Name.Empty();
	frnum = GetPrivateProfileString("POGO","GROUP","",cload,100,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	for(int i=0; i<frnum; i++){
		((CImageTesterDlg *)m_pMomWnd)->Pogo_Name += cload[i];
	}
	if(((CImageTesterDlg *)m_pMomWnd)->Pogo_Name == ""){
		((CImageTesterDlg *)m_pMomWnd)->Pogo_Name = "DEFAULT";
		WritePrivateProfileString("POGO","GROUP","DEFAULT",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}
	//---------------------------------------------------------------------------------------
	ModelFileSearch(((CImageTesterDlg *)m_pMomWnd)->Pogo_Name);
}

void CPogo_Option::ModelFileSearch(CString compdata){
	CFileFind  finder;
	int exnum = -1;
	int conum = -1;
	int ret = -1;
	
	CString	fname = "";
	CString	strFile = ("\\*.ini"); 
	pModelCombo->ResetContent();//콤보를 초기화한다.
	((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->pPogoCombo->ResetContent();

	BOOL bWorking = finder.FindFile(((CImageTesterDlg *)m_pMomWnd)->Pogo_path + strFile);
	
	while(bWorking)
	{
		bWorking = finder.FindNextFile();
		if(!finder.IsDots() && !finder.IsDirectory()){
			fname.Empty();
			fname = finder.GetFileTitle();
						
			pModelCombo->AddString(fname);
			((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->pPogoCombo->AddString(fname);
			exnum++;
			if(fname == compdata){
				conum = exnum;
			}
		}
	}
	if((conum >= 0)&&(exnum>=conum)){
		m_combo_sel = conum;
	}else{
		m_combo_sel = exnum;
	}

	if(m_combo_sel != -1){
		pModelCombo->SetCurSel(m_combo_sel);
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->pPogoCombo->SetCurSel(m_combo_sel);
		SetPogoSet();
	}else{//폴더가 하나도 없을때 기본 폴더를 생성한다. 
		((CImageTesterDlg *)m_pMomWnd)->PogoSelect_path = ((CImageTesterDlg *)m_pMomWnd)->Pogo_path +"\\DEFAULT.ini";
		m_combo_sel =0;
		((CImageTesterDlg *)m_pMomWnd)->Pogo_Name = "DEFAULT";
		pModelCombo->AddString("DEFAULT");
		pModelCombo->SetCurSel(0);
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->pPogoCombo->AddString("DEFAULT");
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->pPogoCombo->SetCurSel(0);
		CString example;
		example.Empty();
		example="TOTAL";
		WritePrivateProfileString(example,NULL,"",((CImageTesterDlg *)m_pMomWnd)->PogoSelect_path);
		WritePrivateProfileString("POGO","GROUP","DEFAULT",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		SetPogoSet();
	}
} 

void CPogo_Option::SetPogoSet()
{
	CString str = "";
	m_combo_sel = pModelCombo->GetCurSel();
	
	if(m_combo_sel < 0){
		return;
	}
	pModelCombo->GetLBText(pModelCombo->GetCurSel(),str);

	if(str == (""))     
	{         
		return;     
	}
	((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->pPogoCombo->SetCurSel(m_combo_sel);
	pogoName = str;
	if(pogoName != ((CImageTesterDlg *)m_pMomWnd)->Pogo_Name){
		WritePrivateProfileString("POGO","GROUP",pogoName,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		((CImageTesterDlg *)m_pMomWnd)->Pogo_Name = pogoName;
	}
	POGONAME_VIEW(str);
	((CImageTesterDlg *)m_pMomWnd)->POGOGROUPNAME(str);
	((CImageTesterDlg *)m_pMomWnd)->PogoSelect_path = ((CImageTesterDlg *)m_pMomWnd)->Pogo_path +"\\"+pogoName+".ini";
	((CImageTesterDlg *)m_pMomWnd)->Pogo_Load_Prm();
}

void CPogo_Option::POGONAME_VIEW(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_GROUPVIEW))->SetWindowText(lpcszString);
}

void CPogo_Option::OnBnClickedButtonGroupsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString example = "";
	CFileFind modelfind;
	
	if (str_PogoGroupNewName == ""){
		return;
	}

	CString pogopath = ((CImageTesterDlg *)m_pMomWnd)->Pogo_path + "\\" +str_PogoGroupNewName +".ini";
	CString str_buf = str_PogoGroupNewName;
	if(!modelfind.FindFile(pogopath)){ //원하는 폴더가 없으면 만든다.
		example.Empty();
		example="TOTAL";
		WritePrivateProfileString(example,NULL,"",pogopath);//모델을 만든다. 	

		DoEvents(100);
		ModelFileSearch(str_PogoGroupNewName);
		AfxMessageBox("[ "+str_buf+" ] 그룹이 생성되었습니다");
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_SHOW);
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->OnShowSel(2);

	}else{/////////////////동일한 모델이 있을 경우
		AfxMessageBox("동일한 POGO 그룹이 있습니다. 다시 입력해주세요.");
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_SHOW);
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->OnShowSel(2);
	}
	str_PogoGroupNewName = "";
	UpdateData(FALSE);
}

void CPogo_Option::OnBnClickedButtonCntsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	i_PogoPinSET = atoi(str_PogoCount);
	str_PogoCount.Format("%d",i_PogoPinSET);
	((CImageTesterDlg *)m_pMomWnd)->i_PogoPinSET =i_PogoPinSET;
	UpdateData(FALSE);
	WritePrivateProfileString("COUNT","PogoSetting",str_PogoCount,((CImageTesterDlg *)m_pMomWnd)->PogoSelect_path);
	((CImageTesterDlg *)m_pMomWnd)->SetCountUpdate();
}

void CPogo_Option::OnBnClickedBtnPogoup()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->i_PogoCnt++;
	((CImageTesterDlg *)m_pMomWnd)->SetCountUpdate();
}

void CPogo_Option::OnBnClickedBtnPogodown()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(((CImageTesterDlg *)m_pMomWnd)->i_PogoCnt > 0){
		((CImageTesterDlg *)m_pMomWnd)->i_PogoCnt--;
	}
	((CImageTesterDlg *)m_pMomWnd)->SetCountUpdate();
}

void CPogo_Option::OnBnClickedButtonCntinit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(((CImageTesterDlg *)m_pMomWnd)->MatchPassWord == ""){
		AfxMessageBox("POGO PIN 카운트를 0으로 초기화 합니다.");
		((CImageTesterDlg *)m_pMomWnd)->i_PogoCnt = 0;
		((CImageTesterDlg *)m_pMomWnd)->SetCountUpdate();
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_SHOW);
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->OnShowSel(2);
	}else{
		CUserSwitching subdlg;
		subdlg.Setup(m_pMomWnd);
		if(subdlg.DoModal() == IDOK){
			AfxMessageBox("POGO PIN 카운트를 0으로 초기화 합니다.");
			((CImageTesterDlg *)m_pMomWnd)->i_PogoCnt = 0;
			((CImageTesterDlg *)m_pMomWnd)->SetCountUpdate();
			((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_HIDE);
			((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_SHOW);
			((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->OnShowSel(2);
		}
	}
}

void CPogo_Option::SetCountUpdate()
{
	CString str = "";
	str.Format("%d",((CImageTesterDlg *)m_pMomWnd)->i_PogoCnt);
	if(i_PogoCnt != ((CImageTesterDlg *)m_pMomWnd)->i_PogoCnt){
		i_PogoCnt = ((CImageTesterDlg *)m_pMomWnd)->i_PogoCnt;
		WritePrivateProfileString("COUNT","PogoCnt",str,((CImageTesterDlg *)m_pMomWnd)->PogoSelect_path);
	}
	CNT_VIEW(str);
}

void CPogo_Option::CNT_VIEW(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_POGOGROUPNAME3))->SetWindowText(lpcszString);
}

void CPogo_Option::OnEnSetfocusEditGroupview()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	GotoDlgCtrl(((CEdit *)GetDlgItem(IDC_EDIT_POGOGROUPNAME2)));

}
