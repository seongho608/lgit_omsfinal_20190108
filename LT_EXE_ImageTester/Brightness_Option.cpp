// Brightness_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Brightness_Option.h"


extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
extern WORD	m_GRAYScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];
extern BYTE	m_OverlayArea[CAM_IMAGE_WIDTH][CAM_IMAGE_HEIGHT];

// CBrightness_Option 대화 상자입니다.

IMPLEMENT_DYNAMIC(CBrightness_Option, CDialog)

CBrightness_Option::CBrightness_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CBrightness_Option::IDD, pParent)
	, B_Total_PosX(0)
	, B_Total_PosY(0)
	, B_Dis_Width(0)
	, B_Dis_Height(0)
	, B_Total_Width(0)
	, B_Total_Height(0)
	, B_Thresold(0)
	, b_BlockDataSave(FALSE)
	, b_MasterDev(FALSE)
	, str_MasterPath(_T(""))
{

	iSavedItem=0;
	iSavedSubitem=0;
	iSavedSubitem2=0;
	state=0;
	EnterState=0;
	NewItem =0;NewSubitem=0;
m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;
	InsertIndex=0;
	//pTStat = NULL;
	ListItemNum=0;
	ChangeCheck=0;
	for(int t=0; t<5; t++){
		ChangeItem[t]=-1;
	}
	changecount=0;
	StartCnt=0;
	b_StopFail = FALSE;

}

CBrightness_Option::~CBrightness_Option()
{
}

void CBrightness_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_BRIGHTNESS, B_DATALIST);
	DDX_Control(pDX, IDC_LIST_BRIGHTNESS_WORK, m_BrightnessList);
	DDX_Text(pDX, IDC_EDIT_BRIGHTNESS_POSX, B_Total_PosX);
	DDX_Text(pDX, IDC_EDIT_BRIGHTNESS_POSY, B_Total_PosY);
	DDX_Text(pDX, IDC_EDIT_BRIGHTNESS_DISW, B_Dis_Width);
	DDX_Text(pDX, IDC_EDIT_BRIGHTNESS_DISH, B_Dis_Height);
	DDX_Text(pDX, IDC_EDIT_BRIGHTNESS_W, B_Total_Width);
	DDX_Text(pDX, IDC_EDIT_BRIGHTNESS_H, B_Total_Height);
	DDX_Text(pDX, IDC_EDIT_Bright_Thresold, B_Thresold);
	DDX_Control(pDX, IDC_LIST_BRIGHTNESS_LOT, m_Lot_BrightnessList);
	DDX_Check(pDX, IDC_CHK_BLOCK_DATA_SAVE, b_BlockDataSave);
	DDX_Check(pDX, IDC_CHK_MASTERMODE, b_MasterDev);
	DDX_Text(pDX, IDC_EDIT_MasterPath, str_MasterPath);
	DDX_Control(pDX, IDC_COMBO_IMAGE_SAVE, m_Cb_ImageSave);
}


BEGIN_MESSAGE_MAP(CBrightness_Option, CDialog)
//	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_BRIGHTNESS_SAVE, &CBrightness_Option::OnBnClickedButtonBrightnessSave)
	ON_BN_CLICKED(IDC_BUTTON_BRIGHTNESS_SETZONE, &CBrightness_Option::OnBnClickedButtonBrightnessSetzone)
	ON_NOTIFY(NM_CLICK, IDC_LIST_BRIGHTNESS, &CBrightness_Option::OnNMClickListBrightness)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_BRIGHTNESS, &CBrightness_Option::OnNMDblclkListBrightness)
	ON_WM_SHOWWINDOW()
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_BRIGHTNESS, &CBrightness_Option::OnNMCustomdrawListBrightness)
	ON_EN_KILLFOCUS(IDC_EDIT_BRIGHT, &CBrightness_Option::OnEnKillfocusEditBright)
	ON_WM_MOUSEWHEEL()
	ON_EN_CHANGE(IDC_EDIT_BRIGHTNESS_POSX, &CBrightness_Option::OnEnChangeEditBrightnessPosx)
	ON_EN_CHANGE(IDC_EDIT_BRIGHTNESS_POSY, &CBrightness_Option::OnEnChangeEditBrightnessPosy)
	ON_EN_CHANGE(IDC_EDIT_BRIGHTNESS_DISW, &CBrightness_Option::OnEnChangeEditBrightnessDisw)
	ON_EN_CHANGE(IDC_EDIT_BRIGHTNESS_DISH, &CBrightness_Option::OnEnChangeEditBrightnessDish)
	ON_EN_CHANGE(IDC_EDIT_BRIGHTNESS_W, &CBrightness_Option::OnEnChangeEditBrightnessW)
	ON_EN_CHANGE(IDC_EDIT_BRIGHTNESS_H, &CBrightness_Option::OnEnChangeEditBrightnessH)
	ON_EN_CHANGE(IDC_EDIT_Bright_Thresold, &CBrightness_Option::OnEnChangeEditBrightThresold)
	ON_EN_CHANGE(IDC_EDIT_BRIGHT, &CBrightness_Option::OnEnChangeEditBright)
	ON_BN_CLICKED(IDC_BUTTON_test, &CBrightness_Option::OnBnClickedButtontest)
	ON_BN_CLICKED(IDC_BUTTON_BRIGHTNESS_LOAD, &CBrightness_Option::OnBnClickedButtonBrightnessLoad)
	ON_BN_CLICKED(IDC_BUTTON_BRIGHTNESS_MASTER, &CBrightness_Option::OnBnClickedButtonBrightnessMaster)
	ON_BN_CLICKED(IDC_CHK_BLOCK_DATA_SAVE, &CBrightness_Option::OnBnClickedChkBlockDataSave)
	ON_BN_CLICKED(IDC_CHK_MASTERMODE, &CBrightness_Option::OnBnClickedChkMastermode)
	ON_BN_CLICKED(IDC_BUTTON_MASTERLOAD, &CBrightness_Option::OnBnClickedButtonMasterload)
	ON_CBN_SELENDOK(IDC_COMBO_IMAGE_SAVE, &CBrightness_Option::OnCbnSelendokComboImageSave)
END_MESSAGE_MAP()


void CBrightness_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}



void CBrightness_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
}

// CBrightness_Option 메시지 처리기입니다.

BOOL CBrightness_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;

	((CEdit *) GetDlgItem(IDC_EDIT_BRIGHT))->ShowWindow(FALSE);
	SETLIST();
	
	//C_filename=((CImageTesterDlg *)m_pMomWnd)->genfolderpath +"\\ColorTEST.ini";
	C_filename=((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	Load_parameter();
	UpdateData(FALSE);
	UploadList();
	//SetTimer(110,100,NULL);

	Set_List(&m_BrightnessList);
	LOT_Set_List(&m_Lot_BrightnessList);
	InitEVMS();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CBrightness_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SAVE))->EnableWindow(0);
		
		((CEdit *)GetDlgItem(IDC_EDIT_BRIGHTNESS_POSX))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_BRIGHTNESS_POSY))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_BRIGHTNESS_DISW))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_BRIGHTNESS_DISH))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_BRIGHTNESS_W))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_BRIGHTNESS_H))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_Bright_Thresold))->EnableWindow(0);
	}
}

BOOL CBrightness_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
			if(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_BRIGHT))->GetSafeHwnd())//GetSafeHwnd() 자신의 핸들을 가져오는 함수
			{
				int bufSubitem = iSavedSubitem + 1;
				
				if(bufSubitem ==10){
					if(iSavedItem == 0){
						iSavedItem = 1; bufSubitem =1;
					}
					else if(iSavedItem == 1){
						iSavedItem = 2; bufSubitem =1;
					}
					else if(iSavedItem == 2){
						iSavedItem = 3; bufSubitem =1;
					}
					else if(iSavedItem == 3){
						iSavedItem = 4; bufSubitem =1;
					}
					else if(iSavedItem == 4){
						iSavedItem = 0; bufSubitem =1;
					}
				}
				int bufItem = iSavedItem;
				B_DATALIST.EnsureVisible(iSavedItem, FALSE);
				B_DATALIST.GetSubItemRect(iSavedItem,bufSubitem , LVIR_BOUNDS, rect);
				B_DATALIST.ClientToScreen(rect);
				this->ScreenToClient(rect);
				B_DATALIST.SetSelectionMark(iSavedItem);
				B_DATALIST.SetFocus(); //저장
				iSavedItem = bufItem;
				iSavedSubitem = bufSubitem;
				((CEdit *)GetDlgItem(IDC_EDIT_BRIGHT))->SetWindowText(B_DATALIST.GetItemText(iSavedItem, iSavedSubitem));
				((CEdit *)GetDlgItem(IDC_EDIT_BRIGHT))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
				((CEdit *)GetDlgItem(IDC_EDIT_BRIGHT))->SetFocus();

			}
			
			if ((pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_BRIGHTNESS_POSX))->GetSafeHwnd())||  
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_BRIGHTNESS_POSY))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_BRIGHTNESS_DISW))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_BRIGHTNESS_DISH))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_BRIGHTNESS_W))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_BRIGHTNESS_H))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_Bright_Thresold))->GetSafeHwnd()))
			{	
				OnBnClickedButtonBrightnessSetzone();
			}

			return TRUE;
		}

		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		/*if(pMsg->wParam == VK_ADD){
			((CImageTesterDlg *)m_pMomWnd)->ExBtnStart();
			return TRUE;
		}
		if (pMsg->wParam == VK_SUBTRACT){
			((CImageTesterDlg *)m_pMomWnd)->ExBtnStop();
			return TRUE;
		}*/

	}


	return CDialog::PreTranslateMessage(pMsg);
}

void CBrightness_Option::SETLIST(){

	CString str;
	B_DATALIST.DeleteAllItems();

	B_DATALIST.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	B_DATALIST.InsertColumn(0, "Zone", LVCFMT_CENTER, 120);
	for (int lop = 0; lop < BRPoint_MaxNum-1; lop++){
		str.Format("Line_%d", lop + 1);
		B_DATALIST.InsertColumn(lop + 1, str, LVCFMT_CENTER, 100);
	}
}
void CBrightness_Option::SETLIST_ITEM(){
	CString str;
	int MaxLinecnt = 0;

	B_DATALIST.DeleteAllItems();

	for (int lop = 0; lop < BRPoint_MaxNum-1; lop++){
		if (MaxLinecnt <= st_BRMaster.pt_StartPoint_PixPosList[lop].size()){
			MaxLinecnt = st_BRMaster.pt_StartPoint_PixPosList[lop].size();// 각 라인 Block 최대 갯수 
		}
	}
	MaxLinecnt = MaxLinecnt/ 2; // 각 라인 Center 까지의 Block 갯수 

	for (int t = 0; t < MaxLinecnt + 3; t++){

		if (t == 0){
			str.Empty(); str = "STANDARD_BLOCK";
		}
		else if (t == 1){
			str.Empty(); str = "THRESHOLD";
		}
		else if (t == 2){
			str.Empty(); str = "Block_Center";
		}
		else{
			str.Empty(); str.Format("Block_%d", t - 2);
		}
		B_DATALIST.InsertItem(t, str, 0);
		//B_DATALIST.SetItemText(InIndex, 0, str);
	}
}
// void CBrightness_Option::SETLIST(){
// 	B_DATALIST.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
// 	B_DATALIST.InsertColumn(0,"Zone",LVCFMT_CENTER, 100);
// // 	B_DATALIST.InsertColumn(1,"PosX",LVCFMT_CENTER, 60);
// // 	B_DATALIST.InsertColumn(2,"PosY",LVCFMT_CENTER, 60);
// 	B_DATALIST.InsertColumn(1,"START X",LVCFMT_CENTER, 60);
// 	B_DATALIST.InsertColumn(2,"START Y",LVCFMT_CENTER, 60);
// 	B_DATALIST.InsertColumn(3,"EXIT X",LVCFMT_CENTER, 60);
// 	B_DATALIST.InsertColumn(4,"EXIT Y",LVCFMT_CENTER, 60);
// 	B_DATALIST.InsertColumn(5,"Width",LVCFMT_CENTER, 60);
// 	B_DATALIST.InsertColumn(6,"Height",LVCFMT_CENTER, 60);
// 	B_DATALIST.InsertColumn(7, "Master", LVCFMT_CENTER, 60);
// 	B_DATALIST.InsertColumn(8,"Thresold",LVCFMT_CENTER, 60);
// 
// 
// }
void CBrightness_Option::UploadList(){
	CString str;

	for (int lop = 0; lop < BRPoint_MaxNum-1; lop++){
		str.Empty(); str.Format("%d", C_RECT[lop].m_iStdBlock); // 기준 블럭
		B_DATALIST.SetItemText(0, lop+1, str);
		str.Empty(); str.Format("%.1f", C_RECT[lop].d_Thresold); // 양불 범위
		B_DATALIST.SetItemText(1, lop+1, str);
	}
	
	
	for (int lop = 0; lop < BRPoint_MaxNum-1; lop++){
		for (int t = 0; t < (st_BRMaster.pt_StartPoint_PixPosList[lop].size() / 2)+1; t++){
			str.Empty(); str.Format("%.1f", C_RECT[lop].d_StdThre[t]); // Block Master //0 번이 센터
			B_DATALIST.SetItemText(t + 2, lop + 1, str);
		}
	}

// 	for(int t=0; t<5; t++){
// 
// 		str.Empty(); str.Format("%d", C_RECT[t].m_startX);
// 		B_DATALIST.SetItemText(t,1,str);
// 		str.Empty(); str.Format("%d", C_RECT[t].m_startY);
// 		B_DATALIST.SetItemText(t,2,str);
// 		
// 		str.Empty();
// 		str.Format("%d", C_RECT[t].m_endX);
// 		B_DATALIST.SetItemText(t,3,str);
// 		str.Empty();
// 		str.Format("%d", C_RECT[t].m_endY);
// 		B_DATALIST.SetItemText(t,4,str);
// 	
// 		str.Empty();
// 		str.Format("%d", C_RECT[t].m_sizeX);
// 		B_DATALIST.SetItemText(t,5,str);
// 		str.Empty();
// 		str.Format("%d", C_RECT[t].m_sizeY);
// 		B_DATALIST.SetItemText(t,6,str);
// 
// 		str.Empty();
// 		str.Format("%d", C_RECT[t].m_Master);
// 		B_DATALIST.SetItemText(t,7,str);
// 		str.Empty();
// 		str.Format("%d",C_RECT[t].m_Thresold);
// 		B_DATALIST.SetItemText(t, 8, str);
// 
// 	}
}
void CBrightness_Option::Save_parameter(){

	CString str = "";
	CString strTitle = "";

 	//WritePrivateProfileString(str_model,NULL,"",C_filename);
	str.Empty();
	str.Format("%d", m_INFO.ID);
	WritePrivateProfileString(str_model, "ID", str, C_filename);
	str.Empty();
	str.Format("%s", m_INFO.MODE);
	WritePrivateProfileString(str_model, "MODE", str, C_filename);
	str.Empty();
	str.Format("%s", m_INFO.NAME);
	WritePrivateProfileString(str_model, "NAME", str, C_filename);

	strTitle.Empty();
	strTitle = "BRIGHTNESS_INIT";

	str.Empty();
	str.Format("%6.2f", m_dOffset);
	WritePrivateProfileString(str_model, strTitle + "Offset", str, C_filename);
	str.Empty();
	str.Format("%s", str_MasterPath);
	WritePrivateProfileString(str_model, "MASTERPATH", str, C_filename);

	str.Empty();
	str.Format("%d", m_nImageSaveMode);
	WritePrivateProfileString(str_model, "ImageSaveMode", str, C_filename);

	



	int MaxLinecnt = 0;

	for (int lop = 0; lop < BRPoint_MaxNum-1; lop++){
		if (MaxLinecnt <= st_BRMaster.pt_StartPoint_PixPosList[lop].size()){
			MaxLinecnt = st_BRMaster.pt_StartPoint_PixPosList[lop].size();// 각 라인 Block 최대 갯수 
		}
	}
	MaxLinecnt = MaxLinecnt / 2; // 각 라인 Center 까지의 Block 갯수

	for (int lop = 0; lop < BRPoint_MaxNum-1; lop++){

		str.Empty(); str.Format("%d", C_RECT[lop].m_iStdBlock); // 기준 블럭
		WritePrivateProfileString(str_model, strTitle + "STANDARD_BLOCK", str, C_filename);
		str.Empty(); str.Format("%.1f", C_RECT[lop].d_Thresold); // 양불 범위
		WritePrivateProfileString(str_model, strTitle + "THRESHOLD", str, C_filename);

		for (int t = 0; t < MaxLinecnt + 1; t++){
			if (t == 0){
				strTitle.Empty();
				strTitle.Format("Block_Center_%d", lop);
			}
			else{
				strTitle.Empty();
				strTitle.Format("Block_%d_%d", lop, t);
			}
			str.Empty(); str.Format("%.1f", C_RECT[lop].d_StdThre[t]); // Block Master //0 번이 센터
			WritePrivateProfileString(str_model, strTitle + "MASTER", str, C_filename);
		}
	}

}
// void CBrightness_Option::Save_parameter(){
// 
// 	CString str="";
// 	CString strTitle="";

	//WritePrivateProfileString(str_model,NULL,"",C_filename);
// 	str.Empty();
// 	str.Format("%d", b_BlockDataSave);
// 	WritePrivateProfileString(str_model, "BLOCK_DATA_SAVE_B", str, C_filename);
// 	str.Empty();
// 	str.Format("%d", b_MasterDev);
// 	WritePrivateProfileString(str_model, "MASTER_DEV", str, C_filename);
// 	str.Empty();
// 	str.Format("%d",m_INFO.ID);
// 	WritePrivateProfileString(str_model,"ID",str,C_filename);
// 	str.Empty();
// 	str.Format("%s",m_INFO.MODE);
// 	WritePrivateProfileString(str_model,"MODE",str,C_filename);
// 	str.Empty();
// 	str.Format("%s",m_INFO.NAME);
// 	WritePrivateProfileString(str_model,"NAME",str,C_filename);
// 
// 	strTitle.Empty();
// 	strTitle="BRIGHTNESS_INIT";
// 
// 	str.Empty();
// 	str.Format("%6.2f", m_dOffset);
// 	WritePrivateProfileString(str_model, strTitle + "Offset", str, C_filename);
// 
// 
// 	str.Empty();
// 	str.Format("%d",B_Total_PosX);
// 	WritePrivateProfileString(str_model,strTitle+"PosX",str,C_filename);
// 	str.Empty();
// 	str.Format("%d",B_Total_PosY);
// 	WritePrivateProfileString(str_model,strTitle+"PosY",str,C_filename);
// 	str.Empty();
// 	str.Format("%d",B_Dis_Width);
// 	WritePrivateProfileString(str_model,strTitle+"DisW",str,C_filename);
// 	str.Empty();
// 	str.Format("%d",B_Dis_Height);
// 	WritePrivateProfileString(str_model,strTitle+"DisH",str,C_filename);
// 	str.Empty();
// 	str.Format("%d",B_Total_Width);
// 	WritePrivateProfileString(str_model,strTitle+"Width",str,C_filename);
// 	str.Empty();
// 	str.Format("%d",B_Total_Height);
// 	WritePrivateProfileString(str_model,strTitle+"Height",str,C_filename);
// 	str.Empty();
// 	str.Format("%d",B_Thresold);
// 	WritePrivateProfileString(str_model,strTitle+"Thresold",str,C_filename);

// 	for(int t=0; t<5; t++){
// 	
// 		if(t ==0){
// 			strTitle.Empty();
// 			strTitle="LEFT_TOP_";
// 		}
// 		if(t ==1){
// 			strTitle.Empty();
// 			strTitle="RIGHT_TOP_";		
// 		}
// 		if(t ==2){
// 			strTitle.Empty();
// 			strTitle="CENTER";		
// 		}
// 		if(t ==3){
// 			strTitle.Empty();
// 			strTitle="LEFT_BOTTOM_";
// 		}
// 		if(t ==4){
// 			strTitle.Empty();
// 			strTitle="RIGHT_BOTTOM_";
// 		}
// 		str.Empty();
// 		str.Format("%d", C_RECT[t].m_startX);
// 		WritePrivateProfileString(str_model, strTitle + "LEFT", str, C_filename);
// 		str.Empty();
// 		str.Format("%d", C_RECT[t].m_startY);
// 		WritePrivateProfileString(str_model, strTitle + "TOP", str, C_filename);
// 		str.Empty();
// 		str.Format("%d", C_RECT[t].m_endX);
// 		WritePrivateProfileString(str_model, strTitle + "RIGHT", str, C_filename);
// 		str.Empty();
// 		str.Format("%d", C_RECT[t].m_endY);
// 		WritePrivateProfileString(str_model, strTitle + "BOTTOM", str, C_filename);
// 		str.Empty();
// 		str.Format("%d", C_RECT[t].m_sizeX);
// 		WritePrivateProfileString(str_model, strTitle + "SIZEX", str, C_filename);
// 		str.Empty();
// 		str.Format("%d", C_RECT[t].m_sizeY);
// 		WritePrivateProfileString(str_model, strTitle + "SIZEY", str, C_filename);
// 		str.Empty();
// 		str.Format("%d", C_RECT[t].m_Master);
// 		WritePrivateProfileString(str_model, strTitle + "MASTER", str, C_filename);
// 		str.Empty();
// 		str.Format("%d", C_RECT[t].m_Thresold);
// 		WritePrivateProfileString(str_model, strTitle + "Thresold", str, C_filename);
// 
// 		str.Empty();
// 		str.Format("%d",C_RECT[t].m_PosX);
// 		WritePrivateProfileString(str_model,strTitle+"PosX",str,C_filename);
// 		str.Empty();
// 		str.Format("%d",C_RECT[t].m_PosY);
// 		WritePrivateProfileString(str_model,strTitle+"PosY",str,C_filename);
// 		str.Empty();
// 		str.Format("%d",C_RECT[t].m_Left);
// 		WritePrivateProfileString(str_model,strTitle+"StartX",str,C_filename);
// 		str.Empty();
// 		str.Format("%d",C_RECT[t].m_Top);
// 		WritePrivateProfileString(str_model,strTitle+"StartY",str,C_filename);
// 		str.Empty();
// 		str.Format("%d",C_RECT[t].m_Right);
// 		WritePrivateProfileString(str_model,strTitle+"ExitX",str,C_filename);
// 		str.Empty();
// 		str.Format("%d",C_RECT[t].m_Bottom);
// 		WritePrivateProfileString(str_model,strTitle+"ExitY",str,C_filename);
// 		str.Empty();
// 		str.Format("%d",C_RECT[t].m_Width);
// 		WritePrivateProfileString(str_model,strTitle+"Width",str,C_filename);
// 		str.Empty();
// 		str.Format("%d",C_RECT[t].m_Height);
// 		WritePrivateProfileString(str_model,strTitle+"Height",str,C_filename);
// 
// 	}
//}

void CBrightness_Option::Load_parameter(){
	CFileFind filefind;
	CString str = "";
	CString strTitle = "";

	if ((GetPrivateProfileCString(str_model, "NAME", C_filename) != m_INFO.NAME) || (GetPrivateProfileCString(str_model, "MODE", C_filename) != m_INFO.MODE)){//ini파일이 없으면 파일을 생성한다.

		m_dOffset = 1.0;

		str_MasterPath = "";
		st_BRMaster.str_MasterPath = str_MasterPath;
		Master_Data_Load();
		SETLIST_ITEM();
		
		int MaxLinecnt = 0;

		for (int lop = 0; lop < BRPoint_MaxNum-1; lop++){
			if (MaxLinecnt <= st_BRMaster.pt_StartPoint_PixPosList[lop].size()){
				MaxLinecnt = st_BRMaster.pt_StartPoint_PixPosList[lop].size();// 각 라인 Block 최대 갯수 
			}
		}
		MaxLinecnt = MaxLinecnt / 2; // 각 라인 Center 까지의 Block 갯수

		for (int lop = 0; lop < BRPoint_MaxNum-1; lop++){

			C_RECT[lop].m_iStdBlock = 0;// 기준 블럭
			C_RECT[lop].d_Thresold = 2.0; // 양불 범위

			for (int t = 0; t < MaxLinecnt + 1; t++){

				C_RECT[lop].d_StdThre[t] = 100; // Block Master //0 번이 센터
			}
		}
		m_nImageSaveMode = 1;
		Save_parameter();
	}
	else{

		strTitle.Empty();
		strTitle = "BRIGHTNESS_INIT";
	 	m_dOffset = GetPrivateProfileDouble(str_model, strTitle + "Offset", -999, C_filename);
	 	if (m_dOffset == -999)
	 	{
	 		m_dOffset = 1.0;
	 		str.Format("%6.2f", m_dOffset);
	 		WritePrivateProfileString(str_model, strTitle + "Offset", str, C_filename);
	 	}

		str_MasterPath = GetPrivateProfileCString(str_model, "MASTERPATH", C_filename);

		st_BRMaster.str_MasterPath = str_MasterPath;

// 		int nFLAG = GetPrivateProfileInt(str_model, "Image_SaveMode", -1, C_filename);
// 		if (nFLAG == -1)
// 		{
// 			nFLAG = 1;
// 			str.Format("%d", nFLAG);
// 			WritePrivateProfileString(str_model, "Image_SaveMode", str, C_filename);
// 		}

		m_nImageSaveMode = Image_NoSave;


		Master_Data_Load();
		SETLIST_ITEM();

		int MaxLinecnt = 0;

		for (int lop = 0; lop < BRPoint_MaxNum-1; lop++){
			if (MaxLinecnt <= st_BRMaster.pt_StartPoint_PixPosList[lop].size()){
				MaxLinecnt = st_BRMaster.pt_StartPoint_PixPosList[lop].size();// 각 라인 Block 최대 갯수 
			}
		}
		MaxLinecnt = MaxLinecnt / 2; // 각 라인 Center 까지의 Block 갯수

		for (int lop2 = 0; lop2 < BRPoint_MaxNum-1; lop2++){

			C_RECT[lop2].m_iStdBlock = GetPrivateProfileInt(str_model, strTitle + "STANDARD_BLOCK", -1, C_filename);
			if (C_RECT[lop2].m_iStdBlock == -1){
				for (int a = 0; a < BRPoint_MaxNum-1; a++){

					C_RECT[a].m_iStdBlock = st_BRMaster.pt_StartPoint_PixPosList[a].size()/2 - 1;// 기준 블럭
					C_RECT[a].d_Thresold = 2.0; // 양불 범위

					for (int t = 0; t < MaxLinecnt + 1; t++){

						C_RECT[a].d_StdThre[t] = 100; // Block Master //0 번이 센터
					}
				}
				Save_parameter();
			}
			if (C_RECT[lop2].m_iStdBlock > st_BRMaster.pt_StartPoint_PixPosList[lop2].size() / 2){
				C_RECT[lop2].m_iStdBlock = st_BRMaster.pt_StartPoint_PixPosList[lop2].size() / 2 - 1;
				Save_parameter();
			}
			C_RECT[lop2].d_Thresold = GetPrivateProfileDouble(str_model, strTitle + "THRESHOLD", -1, C_filename);
			for (int t = 0; t < MaxLinecnt + 1; t++){
				if (t == 0){
					strTitle.Empty();
					strTitle.Format("Block_Center_%d", lop2);
				}
				else{
					strTitle.Empty();
					strTitle.Format("Block_%d_%d", lop2, t);
				}
				C_RECT[lop2].d_StdThre[t] = GetPrivateProfileDouble(str_model, strTitle + "MASTER", -1, C_filename);
			}
		}

	}
	str.Format(_T("%6.2f"), m_dOffset);
	m_Cb_ImageSave.SetCurSel(m_nImageSaveMode);

	((CEdit *)GetDlgItem(IDC_EDIT_BRIGHT_OFFSET))->SetWindowText(str);
	UploadList();
}
// void CBrightness_Option::Load_parameter(){
// 	CFileFind filefind;
// 	CString str="";
// 	CString strTitle="";
// 
// 	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
// 	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);
// 
// 	if((GetPrivateProfileCString(str_model,"NAME",C_filename) != m_INFO.NAME)||(GetPrivateProfileCString(str_model,"MODE",C_filename) != m_INFO.MODE)){//ini파일이 없으면 파일을 생성한다.
// 		B_Total_PosX=Cam_PosX;
// 		B_Total_PosY=Cam_PosY;
// 
// 		//B_Dis_Width=200;
// 		//B_Dis_Height=100;
// 		//B_Total_Width=80;
// 		//B_Total_Height=80;
// 		//B_Thresold =80;
// 		b_BlockDataSave = 1;
// 		b_MasterDev = 1;
// 		B_Dis_Width = m_CAM_SIZE_WIDTH * 0.27;
// 		B_Dis_Height = m_CAM_SIZE_HEIGHT * 0.20;
// 		B_Total_Width = ((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.13;
// 		B_Total_Height = ((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.13;
// 		B_Thresold = 30;
// 		m_dOffset = 1.0;
// 
// 		for (int t = 0; t < 5; t++){
// 			C_RECT[t].m_sizeX = 4;
// 			C_RECT[t].m_sizeY = 4;
// 		}
// 
// 		C_RECT[0].m_startX = BLOCK_WIDTH / 4 - C_RECT[0].m_sizeX / 2;
// 		C_RECT[0].m_startY = BLOCK_HEIGHT / 2 - 7 - C_RECT[0].m_sizeY / 2;
// 
// 		C_RECT[1].m_startX = BLOCK_WIDTH * 3 / 4 - C_RECT[1].m_sizeX / 2;
// 		C_RECT[1].m_startY = BLOCK_HEIGHT / 2 - 7 - C_RECT[1].m_sizeY / 2;
// 
// 		C_RECT[2].m_startX = BLOCK_WIDTH / 2 - C_RECT[2].m_sizeX / 2;
// 		C_RECT[2].m_startY = BLOCK_HEIGHT / 2 - C_RECT[2].m_sizeY / 2;
// 
// 		C_RECT[3].m_startX = BLOCK_WIDTH / 4 - C_RECT[3].m_sizeX / 2;
// 		C_RECT[3].m_startY = BLOCK_HEIGHT / 2 + 7 - C_RECT[3].m_sizeY / 2;
// 
// 		C_RECT[4].m_startX = BLOCK_WIDTH * 3 / 4 - C_RECT[4].m_sizeX / 2;
// 		C_RECT[4].m_startY = BLOCK_HEIGHT / 2 + 7 - C_RECT[4].m_sizeY / 2;
// 
// 
// 
// 		
// 		for(int t=0; t<5; t++){
// 
// 			C_RECT[t].m_endX = C_RECT[t].m_startX + C_RECT[t].m_sizeX;
// 			C_RECT[t].m_endY = C_RECT[t].m_startY + C_RECT[t].m_sizeY;
// 
// 			Rect_Set(C_RECT, t);
// 
// 			C_RECT[t].m_Master = 255;
// 			C_RECT[t].m_Thresold = 30;
// 
// 		}
// 
// 		str_MasterPath = "";
// 		st_BRMaster.str_MasterPath = str_MasterPath;
// 
// 		Master_Data_Load();
// 		SETLIST_ITEM();
// 		Save_parameter();
// 	}else{
// 
// 		strTitle.Empty();
// 		strTitle="BRIGHTNESS_INIT";
// 
// 
// 
// 		str_MasterPath = GetPrivateProfileCString(str_model, "MASTERPATH", C_filename);
// 
// 		st_BRMaster.str_MasterPath = str_MasterPath;
// 
// 		Master_Data_Load();
// 		SETLIST_ITEM();
// 		m_dOffset = GetPrivateProfileDouble(str_model, strTitle + "Offset", -999, C_filename);
// 		if (m_dOffset == -999)
// 		{
// 			m_dOffset = 1.0;
// 			str.Format("%6.2f", m_dOffset);
// 			WritePrivateProfileString(str_model, strTitle + "Offset", str, C_filename);
// 		}
// 
// 
// 
// 		b_BlockDataSave = GetPrivateProfileInt(str_model, "BLOCK_DATA_SAVE_B", -1, C_filename);
// 		if (b_BlockDataSave == -1)
// 		{
// 			b_BlockDataSave = 1;
// 			str.Format("%d", b_BlockDataSave);
// 			WritePrivateProfileString(str_model, "BLOCK_DATA_SAVE_B", str, C_filename);
// 		}
// 
// 		b_MasterDev = GetPrivateProfileInt(str_model, "MASTER_DEV", -1, C_filename);
// 		if (b_MasterDev == -1)
// 		{
// 			b_MasterDev = 1;
// 			str.Format("%d", b_MasterDev);
// 			WritePrivateProfileString(str_model, "MASTER_DEV", str, C_filename);
// 		}
// 
// 		B_Total_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,C_filename);
// 		if(B_Total_PosX == -1){
// 			B_Total_PosX =Cam_PosX;
// 			str.Empty();
// 			str.Format("%d",B_Total_PosX);
// 			WritePrivateProfileString(str_model,strTitle+"PosX",str,C_filename);
// 		}
// 		B_Total_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,C_filename);
// 		if(B_Total_PosY == -1){
// 			B_Total_PosY =Cam_PosY;
// 			str.Empty();
// 			str.Format("%d",B_Total_PosY);
// 			WritePrivateProfileString(str_model,strTitle+"PosY",str,C_filename);
// 		}
// 		B_Dis_Width=GetPrivateProfileInt(str_model,strTitle+"DisW",-1,C_filename);
// 		if(B_Dis_Width == -1){
// 			B_Dis_Width =m_CAM_SIZE_WIDTH * 0.27;
// 			str.Empty();
// 			str.Format("%d",B_Dis_Width);
// 			WritePrivateProfileString(str_model,strTitle+"DisW",str,C_filename);
// 		}
// 		B_Dis_Height=GetPrivateProfileInt(str_model,strTitle+"DisH",-1,C_filename);
// 		if(B_Dis_Height == -1){
// 			B_Dis_Height =m_CAM_SIZE_HEIGHT * 0.20;
// 			str.Empty();
// 			str.Format("%d",B_Dis_Height);
// 			WritePrivateProfileString(str_model,strTitle+"DisH",str,C_filename);
// 		}
// 		B_Total_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,C_filename);
// 		if(B_Total_Width == -1){
// 			B_Total_Width =Cam_PosX;
// 			str.Empty();
// 			str.Format("%d",B_Total_Width);
// 			WritePrivateProfileString(str_model,strTitle+"Width",str,C_filename);
// 		}
// 		B_Total_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,C_filename);
// 		if(B_Total_Height == -1){
// 			B_Total_Height =((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.13;
// 			str.Empty();
// 			str.Format("%d",B_Total_Height);
// 			WritePrivateProfileString(str_model,strTitle+"Height",str,C_filename);
// 		}
// 		B_Thresold=GetPrivateProfileInt(str_model,strTitle+"Thresold",-1,C_filename);
// 		if(B_Thresold == -1){
// 			B_Thresold =60;
// 			str.Empty();
// 			str.Format("%d",B_Thresold);
// 			WritePrivateProfileString(str_model,strTitle+"Thresold",str,C_filename);
// 		}
// 
// 		for(int t=0; t<5; t++){
// 			if(t ==0){
// 			strTitle.Empty();
// 			strTitle="LEFT_TOP_";
// 			}
// 			if(t ==1){
// 				strTitle.Empty();
// 				strTitle="RIGHT_TOP_";		
// 			}
// 			if(t ==2){
// 				strTitle.Empty();
// 				strTitle="CENTER";		
// 			}
// 			if(t ==3){
// 				strTitle.Empty();
// 				strTitle="LEFT_BOTTOM_";
// 			}
// 			if(t ==4){
// 				strTitle.Empty();
// 				strTitle="RIGHT_BOTTOM_";
// 			}
// 			
// 			C_RECT[t].m_startX = GetPrivateProfileInt(str_model, strTitle + "LEFT", -1, C_filename);
// 			if (C_RECT[t].m_startX == -1){
// 				for (int t = 0; t < 5; t++){
// 					C_RECT[t].m_sizeX = 4;
// 					C_RECT[t].m_sizeY = 4;
// 				}
// 
// 				C_RECT[0].m_startX = BLOCK_WIDTH / 4 - C_RECT[0].m_sizeX / 2;
// 				C_RECT[0].m_startY = BLOCK_HEIGHT / 2 - 7 - C_RECT[0].m_sizeY / 2;
// 
// 				C_RECT[1].m_startX = BLOCK_WIDTH * 3 / 4 - C_RECT[1].m_sizeX / 2;
// 				C_RECT[1].m_startY = BLOCK_HEIGHT / 2 - 7 - C_RECT[1].m_sizeY / 2;
// 
// 				C_RECT[2].m_startX = BLOCK_WIDTH / 2 - C_RECT[2].m_sizeX / 2;
// 				C_RECT[2].m_startY = BLOCK_HEIGHT / 2 - C_RECT[2].m_sizeY / 2;
// 
// 				C_RECT[3].m_startX = BLOCK_WIDTH / 4 - C_RECT[3].m_sizeX / 2;
// 				C_RECT[3].m_startY = BLOCK_HEIGHT / 2 + 7 - C_RECT[3].m_sizeY / 2;
// 
// 				C_RECT[4].m_startX = BLOCK_WIDTH * 3 / 4 - C_RECT[4].m_sizeX / 2;
// 				C_RECT[4].m_startY = BLOCK_HEIGHT / 2 + 7 - C_RECT[4].m_sizeY / 2;
// 
// 
// 				for (int t = 0; t < 5; t++){
// 
// 					C_RECT[t].m_endX = C_RECT[t].m_startX + C_RECT[t].m_sizeX;
// 					C_RECT[t].m_endY = C_RECT[t].m_startY + C_RECT[t].m_sizeY;
// 
// 					Rect_Set(C_RECT, t);
// 
// 					C_RECT[t].m_Master = 255;
// 					C_RECT[t].m_Thresold = 30;
// 
// 				}
// 
// 				Save_parameter();
// 			}
// 
// 			C_RECT[t].m_startY = GetPrivateProfileInt(str_model, strTitle + "TOP", -1, C_filename);
// 			C_RECT[t].m_endX = GetPrivateProfileInt(str_model, strTitle + "RIGHT", -1, C_filename);
// 			C_RECT[t].m_endY = GetPrivateProfileInt(str_model, strTitle + "BOTTOM", -1, C_filename);
// 			C_RECT[t].m_sizeX = GetPrivateProfileInt(str_model, strTitle + "SIZEX", -1, C_filename);
// 			C_RECT[t].m_sizeY = GetPrivateProfileInt(str_model, strTitle + "SIZEY", -1, C_filename);
// 			C_RECT[t].m_Master = GetPrivateProfileInt(str_model, strTitle + "MASTER", -1, C_filename);
// 			C_RECT[t].m_Thresold=GetPrivateProfileInt(str_model,strTitle+"Thresold",-1,C_filename);	
// 		
// 			Rect_Set(C_RECT, t);
// 
// 		}
// 	}
// 
// 	str.Format(_T("%6.2f"), m_dOffset);
// 
// 	((CEdit *)GetDlgItem(IDC_EDIT_BRIGHT_OFFSET))->SetWindowText(str);
// 
// 
// }
bool CBrightness_Option::AvePic(CDC *cdc,int NUM){//AVE값 및 평균 RGB값 추출 및 그리기.
	
// 	CPen	my_Pan,*old_pan;
// 	CString TEXTDATA ="";
// 	
// 	//if(C_RECT[NUM].Chkdata() == FALSE){return 0;}
// 	::SetBkMode(cdc->m_hDC,TRANSPARENT);
// 	if(C_RECT[NUM].m_Success == TRUE){
// 		my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
// 		old_pan = cdc->SelectObject(&my_Pan);
// 		cdc->SetTextColor(BLUE_COLOR);
// 	}else{
// 		my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
// 		old_pan = cdc->SelectObject(&my_Pan);
// 		cdc->SetTextColor(RED_COLOR);
// 	}
// 
// 	cdc->MoveTo(C_RECT[NUM].m_Left,C_RECT[NUM].m_Top);
// 	cdc->LineTo(C_RECT[NUM].m_Right,C_RECT[NUM].m_Top);
// 	cdc->LineTo(C_RECT[NUM].m_Right,C_RECT[NUM].m_Bottom);
// 	cdc->LineTo(C_RECT[NUM].m_Left,C_RECT[NUM].m_Bottom);
// 	cdc->LineTo(C_RECT[NUM].m_Left,C_RECT[NUM].m_Top);
// 
// 	/*if(MasterMod == TRUE){
// 		TEXTDATA.Format("R:%03d G:%03d B:%03d",C_RECT[NUM].m_R,C_RECT[NUM].m_G,C_RECT[NUM].m_B);
// 	}
// 	else{
// 		TEXTDATA.Format("R:%03d G:%03d B:%03d",C_RECT[NUM].m_resultR,C_RECT[NUM].m_resultG,C_RECT[NUM].m_resultB);	
// 	}*/
// 
// 	TEXTDATA.Format("%6.1f%%",C_RECT[NUM].m_result);
// 	cdc->TextOut(C_RECT[NUM].m_Left + (int)(C_RECT[NUM].Height()/2)-10,C_RECT[NUM].m_Top - 20,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
// 
// 	cdc->SetTextColor(GREEN_DK_COLOR); // 각 영역의 Y 평균값 표시
// 	TEXTDATA.Format("AVE :%6.1f", C_RECT[NUM].BrightData_Ave);
// 	cdc->TextOut(C_RECT[NUM].m_Left , C_RECT[NUM].m_Top - 35, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());
// 	//cdc->TextOut(360,240,checkView.GetBuffer(0),checkView.GetLength());
// 	
// 	cdc->SelectObject(old_pan);
// 	old_pan->DeleteObject();
// 	my_Pan.DeleteObject();
// 	
	return TRUE;
}



void CModel_Create(CBrightness_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO){
	CRect OptionRect;
	if(pTabWnd != NULL){
		((CBrightness_Option *)*pWnd) = new CBrightness_Option(pMomWnd);
		((CBrightness_Option *)*pWnd)->Setup(pMomWnd,pEdit,INFO);
		((CBrightness_Option *)*pWnd)->Create(((CBrightness_Option *)*pWnd)->IDD,pTabWnd);//&m_CtrTab);
		((CBrightness_Option *)*pWnd)->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		((CBrightness_Option *)*pWnd)->MoveWindow(20,40,OptionRect.Width(),OptionRect.Height());
	}
}

void CModel_Delete(CBrightness_Option **pWnd){
	if(((CBrightness_Option *)*pWnd) != NULL){
		//((CBrightness_Option *)*pWnd)->KillTimer(110);
		((CBrightness_Option *)*pWnd)->DestroyWindow();
		delete ((CBrightness_Option *)*pWnd);
		((CBrightness_Option *)*pWnd) = NULL;	
	}
}
bool CBrightness_Option::AveGen(LPBYTE IN_RGB,int NUM){//AVE값 및 평균 RGB값 추출 및 그리기.
	
	//m_Success = FALSE;
	C_RECT[NUM].m_Success = FALSE;
	BYTE *BW;
	BYTE R = 0,G = 0,B = 0;
	DWORD	RGBPIX = 0;
	DWORD	Total_R = 0,Total_G = 0,Total_B = 0;
	int index=0;
//	if(RGBRect.Chkdata() == FALSE){return 0;}
	double Sum =0;	
	DWORD Total = C_RECT[NUM].Height()*C_RECT[NUM].Length();
	BW = new BYTE[Total];
	memset(BW,0,sizeof(BW));
	int startx = C_RECT[NUM].m_Left;
	int starty = C_RECT[NUM].m_Top;
	int endx = startx + C_RECT[NUM].Length()-2;
	int endy = starty + C_RECT[NUM].Height()-2;
	DWORD RGBLINE = starty * (m_CAM_SIZE_WIDTH*3);
		
	for(int lopy = starty;lopy < endy;lopy++){
		RGBPIX = startx * 3;
		for(int lopx = startx;lopx < endx;lopx++){
			//if(m_OverlayArea[lopx][lopy] == 1){
				B = IN_RGB[RGBLINE + RGBPIX];  
				G = IN_RGB[RGBLINE + RGBPIX + 1];
				R = IN_RGB[RGBLINE + RGBPIX + 2];
				BW[index] = (0.29900*R)+(0.58700*G)+(0.11400*B);
				index++;
			//}
			RGBPIX+=3;
		}
		RGBLINE+=(m_CAM_SIZE_WIDTH*3);
	}

	Sum=0;
	for(int t=0; t<(index-1); t++){
		Sum += BW[t];	
	}

	C_RECT[NUM].BrightData_Ave = Sum / (double)index;

	delete BW;
	return C_RECT[NUM].m_Success;
}
bool CBrightness_Option::AveGen_16bit(LPWORD IN_RGB, int NUM)
{

	C_RECT[NUM].m_Success = FALSE;
	WORD *BW;
	DWORD	RGBPIX = 0;
	int index = 0;
	double Sum = 0;
	DWORD Total = C_RECT[NUM].Height()*C_RECT[NUM].Length();
	BW = new WORD[Total];
	memset(BW, 0, sizeof(BW));
	int startx = C_RECT[NUM].m_Left;
	int starty = C_RECT[NUM].m_Top;
	int endx = startx + C_RECT[NUM].Length() - 2;
	int endy = starty + C_RECT[NUM].Height() - 2;

	for (int lopy = starty; lopy < endy; lopy++){
		for (int lopx = startx; lopx < endx; lopx++){

			BW[index] = IN_RGB[lopy*m_CAM_SIZE_WIDTH + lopx];
			index++;
		}
	}

	Sum = 0;
	for (int t = 0; t < (index - 1); t++){
		Sum += BW[t];
	}

	double Ave = Sum / (double)index;

	Ave *= m_dOffset;
	C_RECT[NUM].BrightData_Ave = Ave;

	delete BW;
	return C_RECT[NUM].m_Success;
}
bool CBrightness_Option::AveGen_16bit_New(LPWORD IN_RGB, int nMode, int NUM)
{

	bool bResult =  false;
	WORD *BW;
	DWORD	RGBPIX = 0;
	int index = 0;
	double Sum = 0;
	DWORD Total = st_BRMaster.nMaster_Detect_X*st_BRMaster.nMaster_Detect_Y;
	BW = new WORD[Total];
	memset(BW, 0, sizeof(BW));

	int startx = st_BRMaster.pt_StartPoint_PixPosList[nMode][NUM].x;
	int starty = st_BRMaster.pt_StartPoint_PixPosList[nMode][NUM].y;
	int endx = startx + st_BRMaster.nMaster_Detect_X;
	int endy = starty + st_BRMaster.nMaster_Detect_Y;

	for (int lopy = starty; lopy < endy; lopy++){
		for (int lopx = startx; lopx < endx; lopx++){

			BW[index] = IN_RGB[lopy*m_CAM_SIZE_WIDTH + lopx];
			index++;
		}
	}

	Sum = 0;
	for (int t = 0; t < (index - 1); t++){
		Sum += BW[t];
	}

	double Ave = Sum / (double)index;

	Ave *= m_dOffset;
	st_BRMaster.v_dBrightNess[nMode].push_back(Ave);

	delete BW;
	return true;
}
bool CBrightness_Option::SNRGen_16bit(WORD *GRAYScanBuf, int nMode, int NUM){

	CvScalar MeanValue, StdValue;
	bool m_Success = false;
	int count = 0;

	int startx = 0;
	int starty = 0;
	int endx = 0;
	int endy = 0;

	int data = 0;
	int Signal;
	double Noise;
	unsigned int Val = 0;
	int index = 0;
	data = 0;
	Val = 0;


	startx = st_BRMaster.pt_StartPoint_PixPosList[nMode][NUM].x;
	starty = st_BRMaster.pt_StartPoint_PixPosList[nMode][NUM].y;
	endx = startx + st_BRMaster.nMaster_Detect_X;
	endy = starty + st_BRMaster.nMaster_Detect_Y;

	for (int y = starty; y < endy; y++)
	{
		for (int x = startx; x < endx; x++)
		{
			data += GRAYScanBuf[y*CAM_IMAGE_WIDTH + x];
			index++;
		}
	}
	Signal = data / (double)index;

	index = 0;
	for (int y = starty; y < endy; y++)
	{
		for (int x = startx; x < endx; x++)
		{
			data = abs(Signal - GRAYScanBuf[y*CAM_IMAGE_WIDTH + x]);
			Val += data*data;
			index++;
		}
	}
	Noise = sqrt(Val / (double)index);


	st_BRMaster.v_iSignal[nMode].push_back(Signal);
	st_BRMaster.v_dNoise[nMode].push_back(Noise);


	if (Noise == 0)
	{
		st_BRMaster.v_dResult[nMode].push_back(0);
	}
	else{
		double data = 20 * log10l(Signal / Noise);
		st_BRMaster.v_dResult[nMode].push_back(data);
	}


	return m_Success;

}
tResultVal CBrightness_Option::Run()
{
	bool checkResult = 0;
	b_StopFail = FALSE;
	CString str = "";
	CString stateDATA = "광량비_ ";
// 	if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 		LOT_InsertDataList();
// 	}
// 	else{
// 		if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
// 			InsertList();
// 		}
// 	}
	
	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

		InsertList();
	}
	int camcnt = 0;
	tResultVal retval = { 0, };
	retval.m_Success = TRUE;
	retval.ValString = "Success";
	int count = 0;
	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = 1;
	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;

	for (int i = 0; i < 10; i++){
		if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0 && ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0){
			break;
		}
		else{
			DoEvents(50);
		}
	}

	// 		PBYTE pRGBz;
	// 		if (!Get_TestImageBuffer(&pRGBz))
	// 		{
	// 			AfxMessageBox(_T("카메라 ON 해주세요"));
	// 			//return;
	// 		}
	// 		if (b_BlockDataSave == TRUE){
	// 			for (int Y = 0; Y < BLOCK_HEIGHT; Y++){
	// 				for (int X = 0; X < BLOCK_WIDTH; X++){
	// 					//AveGen_Block(m_RGBScanbuf, FULL_RECT, X, Y);
	// 					AveGen_Block_16bit(m_GRAYScanbuf, FULL_RECT, X, Y);
	// 				}
	// 			}
	// 			SAVE_FILE_AVE(FULL_RECT, BLOCK_WIDTH, BLOCK_HEIGHT);
	// 		}

	// 		for (int lop = 0; lop < 5; lop++){
	// 			AveGen_16bit(m_GRAYScanbuf, lop);
	// 		}

	// 		if (b_MasterDev == TRUE){
	// 			for (int lop = 0; lop < 5; lop++){
	// 				C_RECT[lop].m_result = Compute_Result(lop);//Master 대비편차
	// 			}
	// 		}
	// 		else{
	//  		 	if (C_RECT[2].BrightData_Ave != 0)
	//  		 	{
	//  		 		C_RECT[0].m_result = C_RECT[0].BrightData_Ave / C_RECT[2].BrightData_Ave * 100;
	//  		 		C_RECT[1].m_result = C_RECT[1].BrightData_Ave / C_RECT[2].BrightData_Ave * 100;
	//  		 		C_RECT[2].m_result = 100;
	//  		 		C_RECT[3].m_result = C_RECT[3].BrightData_Ave / C_RECT[2].BrightData_Ave * 100;
	//  		 		C_RECT[4].m_result = C_RECT[4].BrightData_Ave / C_RECT[2].BrightData_Ave * 100;
	//  		 	}
	//  		 	else{
	//  		 		C_RECT[0].m_result = 0;
	//  		 		C_RECT[1].m_result = 0;
	//  		 		C_RECT[2].m_result = 100;
	//  		 		C_RECT[3].m_result = 0;
	//  		 		C_RECT[4].m_result = 0;
	//  		 	}
	// 		}

	// 		str.Empty();
	// 		str.Format("%3.1f", C_RECT[0].m_result);
	// 		((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->VALUE_TEXT(str);
	// 		stateDATA += ("C_") + str + (", ");
	// 		str.Empty();
	// 		str.Format("%3.1f", C_RECT[1].m_result);
	// 		stateDATA += ("LT_") + str + (", ");
	// 		((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->VALUE_TEXT2(str);
	// 		str.Empty();
	// 		str.Format("%3.1f", C_RECT[2].m_result);
	// 		stateDATA += ("RT_") + str + (", ");
	// 		((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->VALUE_TEXT3(str);
	// 		str.Empty();
	// 		str.Format("%3.1f", C_RECT[3].m_result);
	// 		stateDATA += ("LB_") + str + (", ");
	// 		((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->VALUE_TEXT4(str);
	// 		str.Empty();
	// 		str.Format("%3.1f", C_RECT[4].m_result);
	// 		stateDATA += ("RB_") + str;
	// 		((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->VALUE_TEXT5(str);

	// 		for (int lop = 0; lop<5; lop++){
	// 			CString str1 = "";
	// 			str1.Format("%6.1f", C_RECT[lop].m_result);
	// 			if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
	// 				if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
	// 
	// 					m_Lot_BrightnessList.SetItemText(Lot_InsertIndex, 5 + lop, str1);
	// 				}
	// 			}
	// 			else{
	// 				m_BrightnessList.SetItemText(InsertIndex, 4 + lop, str1);
	// 			}
	// 
	// 			BOOL FLAG = TRUE;
	// 			if (b_MasterDev == TRUE){
	// 				if (abs(C_RECT[lop].m_result) < C_RECT[lop].m_Thresold){//Master 대비편차
	// 					FLAG = TRUE;
	// 				}
	// 				else
	// 				{
	// 					FLAG = FALSE;
	// 				}
	// 			}
	// 			else{
	// 				if (C_RECT[lop].m_result >C_RECT[lop].m_Thresold){
	// 					FLAG = TRUE;
	// 				}
	// 				else
	// 				{
	// 					FLAG = FALSE;
	// 				}
	// 			}
	// 			
	// 			if (FLAG == TRUE){
	// 				C_RECT[lop].m_Success = TRUE;
	// 
	// 				if (C_RECT[lop].m_result > 100){
	// 					C_RECT[lop].m_Success = FALSE;
	// 					FLAG = FALSE;
	// 					checkResult = 1;
	// 				}
	// 			}
	// 			else if (C_RECT[2].BrightData_Ave == 0){
	// 				C_RECT[lop].m_Success = FALSE;
	// 				FLAG = FALSE;
	// 				checkResult = 1;
	// 			}
	// 			else{
	// 				C_RECT[lop].m_Success = FALSE;
	// 				FLAG = FALSE;
	// 			}
	// 
	// 			if (C_RECT[lop].m_Success == TRUE){
	// 				count++;
	// 			}
	// 			CString stat1 = "";
	// 			unsigned char col = 0;
	// 			if (FLAG == FALSE){
	// 				stat1 = "FAIL";
	// 				col = 2;
	// 			}
	// 			else{
	// 				stat1 = "PASS";
	// 				col = 1;
	// 			}
	// 			if (lop == 0){
	// 				((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->RESULT_TEXT(col, stat1);
	// 			}
	// 			else if (lop == 1){
	// 				((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->RESULT_TEXT2(col, stat1);
	// 			}
	// 			else if (lop == 2){
	// 				((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->RESULT_TEXT3(col, stat1);
	// 			}
	// 			else if (lop == 3){
	// 				((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->RESULT_TEXT4(col, stat1);
	// 			}
	// 			else if (lop == 4){
	// 				((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->RESULT_TEXT5(col, stat1);
	// 			}
	// 		}
	// 		
	// 		//StatePrintf("광량비 측정 모드가 종료되었습니다");
	// 		
	// 		retval.m_ID = 0x0b;
	// 		retval.ValString.Empty();
	// 		if(checkResult==0){
	// 			retval.ValString.Format("5개중 %d 성공",count);
	// 		}
	// 		else{
	// 			retval.ValString.Format("TEST환경이 아닙니다.");
	// 		}
	// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
	// 			if (count == 5){
	// 				retval.m_Success = TRUE;
	// 				stateDATA += ("_ PASS");
	// 				if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
	// 					m_Lot_BrightnessList.SetItemText(Lot_InsertIndex, 10, "PASS");
	// 				}
	// 			}
	// 			else{
	// 				retval.m_Success = FALSE;
	// 				stateDATA += ("_ FAIL");
	// 				if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
	// 					m_Lot_BrightnessList.SetItemText(Lot_InsertIndex, 10, "FAIL");
	// 				}
	// 			}
	// 			Lot_StartCnt++;
	// 		}
	// 		else{
	// 			if (count == 5){
	// 				retval.m_Success = TRUE;
	// 				stateDATA += ("_ PASS");
	// 
	// 				((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_BRIGHTNESS, "PASS");
	// 				m_BrightnessList.SetItemText(InsertIndex, 9, "PASS");
	// 			}
	// 			else{
	// 				retval.m_Success = FALSE;
	// 				stateDATA += ("_ FAIL");
	// 				((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_BRIGHTNESS, "FAIL");
	// 				m_BrightnessList.SetItemText(InsertIndex, 9, "FAIL");
	// 			}
	// 			StartCnt++;
	// 		}
	// 
	// 		((CImageTesterDlg *)m_pMomWnd)->StatePrintf(stateDATA);

	//- New
	if (st_BRMaster.pt_StartPoint_PixPosList[BRPoint_Center].size() != 0)
	{

		//개별적인 밝기 평균
		for (int lop = 0; lop < BRPoint_MaxNum; lop++){
			st_BRMaster.v_dBrightNess[lop].clear();
			st_BRMaster.v_iSignal[lop].clear();
			st_BRMaster.v_dNoise[lop].clear();
			st_BRMaster.v_dResult[lop].clear();
			v_B_Result[lop].clear();
			for (int t = 0; t < st_BRMaster.pt_StartPoint_PixPosList[lop].size(); t++)
			{
				AveGen_16bit_New(m_GRAYScanbuf, lop, t);
				SNRGen_16bit(m_GRAYScanbuf, lop, t);
			}
		}

		// 			//중심값 편자 퍼센트
		// 			double dPercent = 0;
		// 			for (int lop = 0; lop < BRPoint_MaxNum - 1; lop++){
		//			st_BRMaster.v_dCenterGap[lop].clear();
		// 				for (int t = 0; t < st_BRMaster.pt_StartPoint_PixPosList[lop].size(); t++)
		// 				{
		// 					dPercent = st_BRMaster.v_dBrightNess[lop][t] / st_BRMaster.v_dBrightNess[BRPoint_Center][0] * 100.0; //센터대비
		// 
		// 					st_BRMaster.v_dCenterGap[lop].push_back(dPercent);
		// 
		// 				}
		// 			}

		//기준대비 퍼센트
		double dPercent = 0;
		for (int lop = 0; lop < BRPoint_MaxNum - 1; lop++){
			st_BRMaster.v_dStandardGap[lop].clear();
			for (int t = 0; t < st_BRMaster.pt_StartPoint_PixPosList[lop].size() + 1; t++)
			{
				if (t == 0){
					dPercent = st_BRMaster.v_dBrightNess[BRPoint_Center][0] / st_BRMaster.v_dBrightNess[lop][C_RECT[lop].m_iStdBlock - 1] * 100.0; //설정 기준 값 대비
				}
				else
				{
					int m_HalfLine_Cnt = (st_BRMaster.pt_StartPoint_PixPosList[lop].size() / 2); // 중심까지의 갯수
					int m_HalfLine_Frontnum = C_RECT[lop].m_iStdBlock - 1;
					int m_HalfLine_Backnum = st_BRMaster.pt_StartPoint_PixPosList[lop].size() - C_RECT[lop].m_iStdBlock;
					if (t - 1 <= m_HalfLine_Cnt)
						dPercent = st_BRMaster.v_dBrightNess[lop][t - 1] / st_BRMaster.v_dBrightNess[lop][m_HalfLine_Frontnum] * 100.0; //설정 기준 값 대비
					else
						dPercent = st_BRMaster.v_dBrightNess[lop][t - 1] / st_BRMaster.v_dBrightNess[lop][m_HalfLine_Backnum] * 100.0; //설정 기준 값 대비
				}

				st_BRMaster.v_dStandardGap[lop].push_back(dPercent);

			}
		}

		//FailCount
		for (int lop = 0; lop < BRPoint_MaxNum - 1; lop++){
			int m_FailCnt = 0;
			for (int t = 0; t < st_BRMaster.pt_StartPoint_PixPosList[lop].size() + 1; t++)
			{
				int m_HalfLine_Cnt = (st_BRMaster.pt_StartPoint_PixPosList[lop].size() / 2); // 중심까지의 갯수
				double BlockData = st_BRMaster.v_dStandardGap[lop][t];
				double Threshold;

				if (t - 1 < m_HalfLine_Cnt)
					Threshold = C_RECT[lop].d_StdThre[t] - C_RECT[lop].d_Thresold;
				else
					Threshold = C_RECT[lop].d_StdThre[st_BRMaster.pt_StartPoint_PixPosList[lop].size() - t + 1] - C_RECT[lop].d_Thresold;

				if (Threshold > BlockData){
					m_FailCnt++;
					//C_RECT[lop].m_Success = FALSE;

					v_B_Result[lop].push_back(FALSE);
				}
				else{
					//C_RECT[lop].m_Success = TRUE;
					v_B_Result[lop].push_back(TRUE);
				}
			}
			m_LineFailcnt[lop] = m_FailCnt;
			if (m_LineFailcnt[lop] != 0){
				retval.m_Success &= FALSE;
				retval.ValString = "FAIL";
			}
		}


		//결과 저장
		CString TEXTDATA;

		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){

			CString ListName;
			int ListNUM = 5;


			for (int lop = 0; lop < BRPoint_MaxNum - 1; lop++){
				int m_FailCnt = 0;
				for (int t = 0; t < st_BRMaster.pt_StartPoint_PixPosList[lop].size() ; t++)
				{
					TEXTDATA.Format("%.0f", st_BRMaster.v_dStandardGap[lop][t+1]);
					m_Lot_BrightnessList.SetItemText(Lot_InsertIndex, ListNUM, TEXTDATA);
					ListNUM++;
					TEXTDATA.Format("%.0f", st_BRMaster.v_dBrightNess[lop][t]);
					m_Lot_BrightnessList.SetItemText(Lot_InsertIndex, ListNUM, TEXTDATA);
					ListNUM++;
					

				}
			}
			// 			for (int k = 0; k < BRPoint_MaxNum - 1; k++)
// 			{
// 				ListName.Format("%d", m_LineFailcnt[k]);
// 				m_Lot_BrightnessList.SetItemText(Lot_InsertIndex, ListNUM, ListName);
// 				ListNUM++;
// 			}
			if (retval.m_Success == FALSE)
				m_Lot_BrightnessList.SetItemText(Lot_InsertIndex, ListNUM, "FAIL");
			else
				m_Lot_BrightnessList.SetItemText(Lot_InsertIndex, ListNUM, "PASS");

		}
		else{

			CString ListName;
			int ListNUM = 4;


			for (int lop = 0; lop < BRPoint_MaxNum - 1; lop++){
				int m_FailCnt = 0;
				for (int t = 0; t < st_BRMaster.pt_StartPoint_PixPosList[lop].size() ; t++)
				{
					TEXTDATA.Format("%.0f", st_BRMaster.v_dStandardGap[lop][t+1]);
					m_BrightnessList.SetItemText(InsertIndex, ListNUM, TEXTDATA);
					ListNUM++;
					TEXTDATA.Format("%.0f", st_BRMaster.v_dBrightNess[lop][t]);
					m_BrightnessList.SetItemText(InsertIndex, ListNUM, TEXTDATA);
					ListNUM++;
					

				}
			}

			TEXTDATA.Format("%.0f", st_BRMaster.v_dBrightNess[4][0]);
			m_BrightnessList.SetItemText(InsertIndex, ListNUM, TEXTDATA);
			ListNUM++;


			if (retval.m_Success == FALSE)
				m_BrightnessList.SetItemText(InsertIndex, ListNUM, "FAIL");
			else
				m_BrightnessList.SetItemText(InsertIndex, ListNUM, "PASS");
		}
	}
	else{
		retval.m_Success = FALSE;
		retval.ValString = "Setting File X";
	}

	if (retval.m_Success)
	{
		st_BRMaster.bResult = TRUE;
		((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_BRIGHTNESS, "PASS");
	}
	else{
		st_BRMaster.bResult = FALSE;
		((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_BRIGHTNESS, "FAIL");
	}



	((CImageTesterDlg *)m_pMomWnd)->TestImageSaveMode(m_nImageSaveMode, m_GRAYScanbuf, _T("광량비"));

	for (int lop = 0; lop < BRPoint_MaxNum - 1; lop++)
	{
		str.Empty();
		str.Format("%d", m_LineFailcnt[BRPoint_1st]);
		((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->VALUE_TEXT(str);
		str.Empty();
		str.Format("%d", m_LineFailcnt[BRPoint_2nd]);
		((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->VALUE_TEXT2(str);
		str.Empty();
		str.Format("%d", m_LineFailcnt[BRPoint_3rd]);
		((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->VALUE_TEXT3(str);
		str.Empty();
		str.Format("%d", m_LineFailcnt[BRPoint_4th]);
		((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->VALUE_TEXT4(str);

		CString stat1 = "";
		unsigned char col = 0;
		if (m_LineFailcnt[lop] > 0){
			stat1 = "FAIL";
			col = 2;
		}
		else{
			stat1 = "PASS";
			col = 1;
		}
		if (lop == 0){
			((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->RESULT_TEXT(col, stat1);
		}
		else if (lop == 1){
			((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->RESULT_TEXT2(col, stat1);
		}
		else if (lop == 2){
			((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->RESULT_TEXT3(col, stat1);
		}
		else if (lop == 3){
			((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->RESULT_TEXT4(col, stat1);
		}
	}
// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 			if (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == TRUE){
// 				Lot_StartCnt++;
// 			}
// 		}
// 		else{
// 			StartCnt++;
// 		}
		if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			StartCnt++;
		}
		return retval;
	
}	

void CBrightness_Option::FAIL_UPLOAD(){

	if (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1){

		InsertList();

		int ListNUM = 4;

		for (int lop = 0; lop < BRPoint_MaxNum; lop++){
			int m_FailCnt = 0;
			for (int t = 0; t < st_BRMaster.pt_StartPoint_PixPosList[lop].size(); t++)
			{
				if (lop != 4)
				{
					m_BrightnessList.SetItemText(InsertIndex, ListNUM++, "X");
				}
				m_BrightnessList.SetItemText(InsertIndex, ListNUM++, "X");

			}
		}
		m_BrightnessList.SetItemText(InsertIndex, ListNUM++, "FAIL");
		m_BrightnessList.SetItemText(InsertIndex, ListNUM++, "STOP");


		((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_BRIGHTNESS, "STOP");
		StartCnt++;
		b_StopFail = TRUE;

	}

}
void CBrightness_Option::Pic(CDC *cdc)
{
// 	for(int lop=0;lop<5;lop++){
// 		AvePic(cdc,lop);
// 	}
	New_Brightness_Pic(cdc);
}
void CBrightness_Option::New_Brightness_Pic(CDC *cdc){
	CPen	my_Pan, *old_pan;
	CString TEXTDATA = "";
	::SetBkMode(cdc->m_hDC, TRANSPARENT);

	my_Pan.CreatePen(PS_SOLID, 2, WHITE_COLOR);
	cdc->SetTextColor(RED_COLOR);
	old_pan = cdc->SelectObject(&my_Pan);
	int posX = 0;
	for (int t = 0; t < st_BRMaster.nMaster_Block_X; t++)
	{
		if (t == 0)
		{
			posX += st_BRMaster.nMaster_Detect_X / 2;
		}
		else{
			posX += st_BRMaster.nMaster_Detect_X;
		}
		cdc->MoveTo(posX, 0);
		cdc->LineTo(posX, CAM_IMAGE_HEIGHT);
	}

	int posY = 0;
	for (int t = 0; t < st_BRMaster.nMaster_Block_Y; t++)
	{
		if (t == 0)
		{
			posY += st_BRMaster.nMaster_Detect_Y / 2;
		}
		else{
			posY += st_BRMaster.nMaster_Detect_Y;
		}
		cdc->MoveTo(0, posY);
		cdc->LineTo(CAM_IMAGE_WIDTH, posY);
	}

	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();

	//-Data 위치
	CFont font, *old_font;
	font.CreatePointFont(70, "Arial");
	cdc->SetTextAlign(TA_CENTER | TA_BASELINE);
	old_font = cdc->SelectObject(&font);

	for (int k = 0; k < BRPoint_MaxNum; k++)
	{
		for (int t = 0; t < st_BRMaster.pt_StartPoint_PixPosList[k].size(); t++)
		{
			if (st_BRMaster.v_dStandardGap[k].size() != 0)
			{
				int m_HalfLine_Cnt = (st_BRMaster.pt_StartPoint_PixPosList[k].size() / 2) - 1; // 중심까지의 갯수
				double BlockData = st_BRMaster.v_dStandardGap[k][t + 1];
				double Threshold;

				if (t - 1 < m_HalfLine_Cnt)
					Threshold = C_RECT[k].d_StdThre[t + 1] - C_RECT[k].d_Thresold;
				else
					Threshold = C_RECT[k].d_StdThre[st_BRMaster.pt_StartPoint_PixPosList[k].size() - t] - C_RECT[k].d_Thresold;

				if (Threshold > BlockData){
					my_Pan.CreatePen(PS_SOLID, 2, RED_COLOR);
					cdc->SetTextColor(RED_COLOR);
				}
				else{
					switch (k)
					{
					case BRPoint_1st:
						my_Pan.CreatePen(PS_SOLID, 2, RGB(255, 255, 0));
						cdc->SetTextColor(RGB(255, 255, 0));
						break;
					case BRPoint_2nd:
						my_Pan.CreatePen(PS_SOLID, 2, RGB(71, 200, 62));
						cdc->SetTextColor(RGB(71, 200, 62));
						break;
					case BRPoint_3rd:
						my_Pan.CreatePen(PS_SOLID, 2, RGB(255, 94, 0));
						cdc->SetTextColor(RGB(255, 94, 0));
						break;
					case BRPoint_4th:
						my_Pan.CreatePen(PS_SOLID, 2, RGB(67, 116, 217));
						cdc->SetTextColor(RGB(67, 116, 217));
						break;
					case BRPoint_Center:
						my_Pan.CreatePen(PS_SOLID, 2, BLACK_COLOR);
						cdc->SetTextColor(BLACK_COLOR);
						break;
					default:
						break;
					}
				}
			}

			old_pan = cdc->SelectObject(&my_Pan);

			cdc->SetTextAlign(TA_CENTER | TA_BASELINE);

			cdc->MoveTo(st_BRMaster.pt_StartPoint_PixPosList[k][t].x, st_BRMaster.pt_StartPoint_PixPosList[k][t].y);
			cdc->LineTo(st_BRMaster.pt_StartPoint_PixPosList[k][t].x + st_BRMaster.nMaster_Detect_X, st_BRMaster.pt_StartPoint_PixPosList[k][t].y);
			cdc->LineTo(st_BRMaster.pt_StartPoint_PixPosList[k][t].x + st_BRMaster.nMaster_Detect_X, st_BRMaster.pt_StartPoint_PixPosList[k][t].y + st_BRMaster.nMaster_Detect_Y);
			cdc->LineTo(st_BRMaster.pt_StartPoint_PixPosList[k][t].x, st_BRMaster.pt_StartPoint_PixPosList[k][t].y + st_BRMaster.nMaster_Detect_Y);
			cdc->LineTo(st_BRMaster.pt_StartPoint_PixPosList[k][t].x, st_BRMaster.pt_StartPoint_PixPosList[k][t].y);

		//	if (!((st_BRMaster.pt_Min.x == st_BRMaster.pt_StartPoint_PixPosList[k][t].x) && (st_BRMaster.pt_Min.y == st_BRMaster.pt_StartPoint_PixPosList[k][t].y)))
			{
				if (st_BRMaster.v_dBrightNess[k].size() != 0)
				{
					TEXTDATA.Format("%.0f", st_BRMaster.v_dBrightNess[k][t]);
					cdc->TextOut(st_BRMaster.pt_StartPoint_PixPosList[k][t].x + st_BRMaster.nMaster_Detect_X / 2, st_BRMaster.pt_StartPoint_PixPosList[k][t].y + st_BRMaster.nMaster_Detect_Y / 2, 
						TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());

				}
// 				if (st_BRMaster.v_dNoise[k].size() != 0)
// 				{
// 					TEXTDATA.Format("%6.1f", st_BRMaster.v_dNoise[k][t]);
// 					cdc->TextOut(st_BRMaster.pt_StartPoint_PixPosList[k][t].x + st_BRMaster.nMaster_Detect_X / 2, st_BRMaster.pt_StartPoint_PixPosList[k][t].y + st_BRMaster.nMaster_Detect_Y / 2+9,
// 						TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());
// 
// 				}
// 				if (st_BRMaster.v_dCenterGap[k].size() != 0)
// 				{
// 					TEXTDATA.Format("%6.1f", st_BRMaster.v_dCenterGap[k][t]);
// 					cdc->TextOut(st_BRMaster.pt_StartPoint_PixPosList[k][t].x + st_BRMaster.nMaster_Detect_X / 2, st_BRMaster.pt_StartPoint_PixPosList[k][t].y + st_BRMaster.nMaster_Detect_Y / 2 + 9,
// 						TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());
// 
// 				}
				if (st_BRMaster.v_dStandardGap[k].size() != 0)
				{
					TEXTDATA.Format("%6.1f", st_BRMaster.v_dStandardGap[k][t+1]);
					cdc->TextOut(st_BRMaster.pt_StartPoint_PixPosList[k][t].x + st_BRMaster.nMaster_Detect_X / 2, st_BRMaster.pt_StartPoint_PixPosList[k][t].y + st_BRMaster.nMaster_Detect_Y / 2 + 9,
						TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());

				}
			}
			cdc->SelectObject(old_pan);
			my_Pan.DeleteObject();
		}
	}
	cdc->SelectObject(old_font);
	font.DeleteObject();
}

void CBrightness_Option::InitPrm()  
{
	Load_parameter();
	UpdateData(FALSE);
	UploadList();

	for(int t=0; t<5;t++){
		C_RECT[t].m_Success = FALSE;
		C_RECT[t].m_result =0;

	}

}


void CBrightness_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

void CBrightness_Option::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",C_filename);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

//---------------------------------------------------------------------WORKLIST
void CBrightness_Option::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	InsertIndex = m_BrightnessList.InsertItem(StartCnt,"",0);
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_BrightnessList.SetItemText(InsertIndex,0,strCnt);
	
	m_BrightnessList.SetItemText(InsertIndex,1,((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_BrightnessList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_BrightnessList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");


}
void CBrightness_Option::Set_List(CListCtrl *List){
	

	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	//List->InsertColumn(2, "LOT CARD", LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
// 	List->InsertColumn(4,"LT",LVCFMT_CENTER, 80);
// 	List->InsertColumn(5,"RT",LVCFMT_CENTER, 80);
// 	List->InsertColumn(6,"C",LVCFMT_CENTER, 80);
// 	List->InsertColumn(7,"LD",LVCFMT_CENTER, 80);
// 	List->InsertColumn(8,"RD",LVCFMT_CENTER, 80);

// 	List->InsertColumn(4, "Center-Ave", LVCFMT_CENTER, 80);
// 	List->InsertColumn(5, "Center-Std", LVCFMT_CENTER, 80);
// 	List->InsertColumn(6, "Center-%", LVCFMT_CENTER, 80);
// 
// 	CString ListName;
// 	int ListNUM = 7;
// 	for (int k = 0; k < BRPoint_MaxNum-1; k++)
// 	{
// 		for (int t = 0; t < 40; t++)
// 		{
// 			ListName.Format("[%d][%d]-Ave", k + 1, t + 1);
// 			List->InsertColumn(ListNUM, ListName, LVCFMT_CENTER, 80);
// 			ListNUM++;
// 			ListName.Format("[%d][%d]-Std", k + 1, t + 1);
// 			List->InsertColumn(ListNUM, ListName, LVCFMT_CENTER, 80);
// 			ListNUM++;
// 			ListName.Format("[%d][%d]-%%", k + 1, t + 1);
// 			List->InsertColumn(ListNUM, ListName, LVCFMT_CENTER, 80);
// 			ListNUM++;
// 
// 		}
// 	}
	CString ListName;
	int ListNUM = 4;


	for (int k = 0; k < BRPoint_MaxNum; k++)
	{
		for (int t = 0; t < st_BRMaster.pt_StartPoint_PixPosList[k].size(); t++)
		{
			if (k != 4)
			{
				ListName.Format("[%d-%d]-Percent", k + 1, t + 1);
				List->InsertColumn(ListNUM, ListName, LVCFMT_CENTER, 80);
				ListNUM++;
			}
		
			ListName.Format("[%d-%d]-Brightness", k + 1, t+1);
			List->InsertColumn(ListNUM, ListName, LVCFMT_CENTER, 80);
			ListNUM++;
		}
	}
// 	for (int k = 0; k < BRPoint_MaxNum - 1; k++)
// 	{
// 		ListName.Format("[%d]-FAILCnt", k + 1);
// 		List->InsertColumn(ListNUM, ListName, LVCFMT_CENTER, 80);
// 		ListNUM++;
// 	}

	List->InsertColumn(ListNUM, "RESULT", LVCFMT_CENTER, 80);
	List->InsertColumn(ListNUM + 1, "비고", LVCFMT_CENTER, 80);
	
	ListItemNum = ListNUM + 2;
	Copy_List(&m_BrightnessList, ((CImageTesterDlg *)m_pMomWnd)->m_pWorkList, ListItemNum);

}

CString CBrightness_Option::Mes_Result()
{
	CString sz[5];
	int		nResult[5];

	// 제일 마지막 항목을 추가하자
	int nSel = m_BrightnessList.GetItemCount() - 1;

	int ListNUM = 4;

	CString strData;

	CString str;

	strData.Empty();
	int nCount = 1;
	if (b_StopFail == FALSE){


		for (int k = 0; k < BRPoint_MaxNum-1; k++)
		{
			for (int t = 0; t < st_BRMaster.pt_StartPoint_PixPosList[k].size(); t++)
			{

				str = m_BrightnessList.GetItemText(nSel, ListNUM++);


				strData += str + ("_");
				str = m_BrightnessList.GetItemText(nSel, ListNUM++);
				strData += str + (":");


				str.Format(_T("%d"), v_B_Result[k][t + 1]);

				strData += str + (",");

				nCount++;
			

			}
		}

		str = m_BrightnessList.GetItemText(nSel, ListNUM++);
		strData += str;
		str.Format(_T(":%d"), st_BRMaster.bResult);
		strData += str + ("");


		if (nCount < 73)
		{

			strData += (",");
			for (int t = nCount; t < 72; t++)
			{
				strData += (":1,");
			}
			strData += (":1");
		}
	


		m_szMesResult = strData;
	
	}
	else{

		for (int k = 0; k < BRPoint_MaxNum-1; k++)
		{
			for (int t = 0; t < st_BRMaster.pt_StartPoint_PixPosList[k].size(); t++)
			{
				strData += (":1,");
				nCount++;
			}
		}
		strData += (":1");


		if (nCount < 73)
		{

			strData += (",");
			for (int t = nCount; t < 72; t++)
			{
				strData += (":1,");
			}
			strData += (":1");
		}



		m_szMesResult = strData;
	}
	m_szMesResult.Replace(" ", "");
	((CImageTesterDlg  *)m_pMomWnd)->m_stNewMesData[NewMES_Brightness] = m_szMesResult;

	return m_szMesResult;
}

void CBrightness_Option::EXCEL_SAVE(){
	//CString Item[5]={"LT","RT","C","LD","RD"};

// 	CString Item[483] = { "Center-Ave", "Center-Std", "Center-%", };
// 
// 
// 	CString ListName;
// 	int ListNUM = 3;
// 	for (int k = 0; k < BRPoint_MaxNum-1; k++)
// 	{
// 		for (int t = 0; t < 40; t++)
// 		{
// 			ListName.Format("[%d][%d]-Ave", k + 1, t + 1);
// 			Item[ListNUM] = ListName;
// 			ListNUM++;
// 			ListName.Format("[%d][%d]-Std", k + 1, t + 1);
// 			Item[ListNUM] = ListName;
// 			ListNUM++;
// 			ListName.Format("[%d][%d]-%%", k + 1, t + 1);
// 			Item[ListNUM] = ListName;
// 			ListNUM++;
// 		}
// 	}

	CString ListName;


	CString Item[500];

	int ListNUM = 0;
	for (int k = 0; k < BRPoint_MaxNum; k++)
	{
		for (int t = 0; t < st_BRMaster.pt_StartPoint_PixPosList[k].size(); t++)
		{
			if (k != 4)
			{
				ListName.Format("[%d-%d]-Percent", k + 1, t + 1);
				Item[ListNUM];
				ListNUM++;
			}

			ListName.Format("[%d-%d]-Brightness", k + 1, t + 1);
			Item[ListNUM];
			ListNUM++;
		}
	}
	
	// 	int ListNUM = 0;
// 	for (int k = 0; k < BRPoint_MaxNum - 1; k++)
// 	{
// 		ListName.Format("[%d]-FAILCnt", k + 1);
// 		Item[ListNUM] = ListName;
// 		ListNUM++;
// 	}

	CString Data ="";
	CString str="";
	str = "***********************광량비 검사 [ 단위 : 밝기값, Center 대비편차 (%) ] ***********************\n";
	Data += str;
	str.Empty();
	str.Format("Left Top ,%d ,Threshold,  %d ,, ", C_RECT[0].m_Master,C_RECT[0].m_Thresold);
	Data += str;
	str.Empty();
	str.Format("Right Top ,%d ,Threshold,  %d, ,", C_RECT[1].m_Master, C_RECT[1].m_Thresold);
	Data += str;
	str.Empty();
	str.Format("Center ,%d ,Threshold,  %d, \n", C_RECT[2].m_Master, C_RECT[2].m_Thresold);
	Data += str;
	str.Empty();
	str.Format("Left Bottom ,%d ,Threshold, %d, ,", C_RECT[3].m_Master, C_RECT[3].m_Thresold);
	Data += str;
	str.Empty();
	str.Format("Right Bottom ,%d ,Threshold, %d,\n ", C_RECT[4].m_Master, C_RECT[4].m_Thresold);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("광량비검사", Item, ListNUM, &m_BrightnessList, Data);
}

bool CBrightness_Option::EXCEL_UPLOAD(){
	m_BrightnessList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("광량비검사",&m_BrightnessList,1,&StartCnt) == TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return TRUE;
}
void CBrightness_Option::Change_DATA(){

	CString str;
	CString data = "";

	((CEdit *)GetDlgItemText(IDC_EDIT_BRIGHT, str));
	B_DATALIST.SetItemText(iSavedItem, iSavedSubitem, str);

	int num = _ttoi(str);
	double d_num = _ttof(str);

	if (num < 0){
		num = 0;
	}
	if (d_num < 0){
		d_num = 0;
	}

	if (iSavedItem == 0){
		C_RECT[iSavedSubitem-1].m_iStdBlock = num;
		data.Format("%d", C_RECT[iSavedSubitem-1].m_iStdBlock);
	}
	else if (iSavedItem == 1){
		C_RECT[iSavedSubitem - 1].d_Thresold = d_num;
		data.Format("%.1f", C_RECT[iSavedSubitem-1].d_Thresold);
	}
	else{
		C_RECT[iSavedSubitem - 1].d_StdThre[iSavedItem - 2] = d_num;
		data.Format("%.1f", C_RECT[iSavedSubitem-1].d_StdThre[iSavedItem - 2]);
	}
	
	((CEdit *)GetDlgItem(IDC_EDIT_BRIGHT))->SetWindowText(data);
	
}

// void CBrightness_Option::Change_DATA(){
// 
// 		CString str;
// 		
// 		((CEdit *)GetDlgItemText(IDC_EDIT_BRIGHT, str));
// 		B_DATALIST.SetItemText(iSavedItem, iSavedSubitem, str);
// 
// 		int num = _ttoi(str);
// 
// 		if(num < 0){
// 			num =0;
// 		}
// 
// 		if(iSavedSubitem ==1){
// 			C_RECT[iSavedItem].m_startX = num;
// 			C_RECT[iSavedItem].m_endX = num + C_RECT[iSavedItem].m_sizeX;
// //			C_RECT[iSavedItem].m_PosX = num;
// 			
// 		}
// 		if(iSavedSubitem ==2){
// 			C_RECT[iSavedItem].m_startY = num;
// 			C_RECT[iSavedItem].m_endY = num + C_RECT[iSavedItem].m_sizeY;
// //			C_RECT[iSavedItem].m_PosY = num;
// 		}
// 		if(iSavedSubitem ==3){
// 			C_RECT[iSavedItem].m_endX = num;
// 			C_RECT[iSavedItem].m_sizeX = C_RECT[iSavedItem].m_endX - C_RECT[iSavedItem].m_startX;
// // 			C_RECT[iSavedItem].m_Left = num;
// // 			C_RECT[iSavedItem].m_PosX=(C_RECT[iSavedItem].m_Right+1 +C_RECT[iSavedItem].m_Left) /2;
// // 			C_RECT[iSavedItem].m_Width=C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;
// 
// 		}
// 		if(iSavedSubitem ==4){
// 			C_RECT[iSavedItem].m_endY = num;
// 			C_RECT[iSavedItem].m_sizeY = C_RECT[iSavedItem].m_endY - C_RECT[iSavedItem].m_startY;
// // 			C_RECT[iSavedItem].m_Top = num;
// // 			C_RECT[iSavedItem].m_PosY=(C_RECT[iSavedItem].m_Top +C_RECT[iSavedItem].m_Bottom+1)/2;
// // 			C_RECT[iSavedItem].m_Height=C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;
// 
// 		}
// 		if(iSavedSubitem ==5){
// 			C_RECT[iSavedItem].m_sizeX = num;
// 			C_RECT[iSavedItem].m_endX = C_RECT[iSavedItem].m_startX + num;
// 
// // 			if(num == 0){
// // 				C_RECT[iSavedItem].m_Right = 0;
// // 			}
// // 			else{
// // 				C_RECT[iSavedItem].m_Right = num;
// // 				C_RECT[iSavedItem].m_PosX=(C_RECT[iSavedItem].m_Right +C_RECT[iSavedItem].m_Left+1) /2;
// // 				C_RECT[iSavedItem].m_Width=C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;
// // 			}
// 		}
// 		if(iSavedSubitem ==6){
// 			C_RECT[iSavedItem].m_sizeY = num;
// 			C_RECT[iSavedItem].m_endY = C_RECT[iSavedItem].m_startY + num;
// 
// // 			if(num ==0){
// // 				C_RECT[iSavedItem].m_Bottom =0;
// // 			}
// // 			else{
// // 				C_RECT[iSavedItem].m_Bottom = num;
// // 				C_RECT[iSavedItem].m_PosY=(C_RECT[iSavedItem].m_Top +C_RECT[iSavedItem].m_Bottom+1)/2;
// // 				C_RECT[iSavedItem].m_Height=C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;
// // 			}
// 		}
// 		if(iSavedSubitem ==7){
// 			C_RECT[iSavedItem].m_Master = num;
// //			C_RECT[iSavedItem].m_Width = num;
// 
// 		}
// 		if(iSavedSubitem ==8){
// 			C_RECT[iSavedItem].m_Thresold = num;
// //			C_RECT[iSavedItem].m_Height = num;
// 		}
// 		
// // 		if(iSavedSubitem ==9){
// // 			C_RECT[iSavedItem].m_Thresold = num;
// // 		}
// 
// 		Rect_Set(C_RECT, iSavedItem);
// 
// 
// // 		C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
// // 			
// // 		if(C_RECT[iSavedItem].m_Left < 0){//////////////////////수정
// // 			C_RECT[iSavedItem].m_Left = 0;
// // 			C_RECT[iSavedItem].m_Width = C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;
// // 			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
// // 		}
// // 		if(C_RECT[iSavedItem].m_Right >m_CAM_SIZE_WIDTH){
// // 			C_RECT[iSavedItem].m_Right = m_CAM_SIZE_WIDTH;
// // 			C_RECT[iSavedItem].m_Width = C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;
// // 			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
// // 		}
// // 		if(C_RECT[iSavedItem].m_Top< 0){
// // 			C_RECT[iSavedItem].m_Top = 0;
// // 			C_RECT[iSavedItem].m_Height = C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;
// // 			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
// // 		}
// // 		if(C_RECT[iSavedItem].m_Bottom >m_CAM_SIZE_HEIGHT){
// // 			C_RECT[iSavedItem].m_Bottom = m_CAM_SIZE_HEIGHT;
// // 			C_RECT[iSavedItem].m_Height = C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;
// // 			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
// // 		}
// // 
// // 		if(C_RECT[iSavedItem].m_Height<= 0){
// // 			C_RECT[iSavedItem].m_Height = 1;
// // 			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
// // 		}
// // 		if(C_RECT[iSavedItem].m_Height >=m_CAM_SIZE_HEIGHT){
// // 			C_RECT[iSavedItem].m_Height = m_CAM_SIZE_HEIGHT;
// // 			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
// // 		}
// // 
// // 		if(C_RECT[iSavedItem].m_Width <= 0){
// // 			C_RECT[iSavedItem].m_Width = 1;
// // 			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
// // 		}
// // 		if(C_RECT[iSavedItem].m_Width >=m_CAM_SIZE_WIDTH){
// // 			C_RECT[iSavedItem].m_Width = m_CAM_SIZE_WIDTH;
// // 			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
// // 		}
// //
// // 		C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
// 
// 	
// 
// 		CString data = "";
// 
// 		if(iSavedSubitem ==1){
// 			data.Format("%d",C_RECT[iSavedItem].m_startX);			
// 		}
// 		else if(iSavedSubitem ==2){
// 			data.Format("%d",C_RECT[iSavedItem].m_startY);
// 		}
// 		else if(iSavedSubitem ==3){
// 			data.Format("%d",C_RECT[iSavedItem].m_endX);			
// 		}
// 		else if(iSavedSubitem ==4){
// 			data.Format("%d", C_RECT[iSavedItem].m_endY);
// 		}
// 		else if(iSavedSubitem ==5){
// 			data.Format("%d",C_RECT[iSavedItem].m_sizeX);			
// 		}
// 		else if(iSavedSubitem ==6){
// 			data.Format("%d", C_RECT[iSavedItem].m_sizeY);
// 		}
// 		else if(iSavedSubitem ==7){
// 			data.Format("%d",C_RECT[iSavedItem].m_Master);			
// 		}
// 		else if(iSavedSubitem ==8){
// 			data.Format("%d",C_RECT[iSavedItem].m_Thresold);			
// 		}
// 		
// 		
// 		//((CEdit *)SetDlgItemText(IDC_EDIT_BRIGHT, data));
// 		((CEdit *)GetDlgItem(IDC_EDIT_BRIGHT))->SetWindowText(data);
// 		
// 		
// 		//((CEdit *)GetDlgItem(IDC_EDIT_L_MOD))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );	
// 
// }

void CBrightness_Option::OnBnClickedButtonBrightnessSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str_Buf;

	((CEdit *)GetDlgItem(IDC_EDIT_BRIGHT_OFFSET))->GetWindowText(str_Buf);
	m_dOffset = atof(str_Buf);



	Save_parameter();
	
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	
	ChangeCheck =0;
	for(int t=0; t<5; t++){
		ChangeItem[t]=-1;
	}
	for(int t=0; t<5; t++){
		B_DATALIST.Update(t);
	}
}

void CBrightness_Option::OnBnClickedButtonBrightnessSetzone()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeCheck=1;
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	UpdateData(TRUE);

	
	for (int t = 0; t < 5; t++){
		C_RECT[t].m_sizeX = 4;
		C_RECT[t].m_sizeY = 4;
	}

	C_RECT[0].m_startX = BLOCK_WIDTH / 4 - C_RECT[1].m_sizeX / 2;
	C_RECT[0].m_startY = BLOCK_HEIGHT / 2 - 7 - C_RECT[1].m_sizeY / 2;

	C_RECT[1].m_startX = BLOCK_WIDTH * 3 / 4 - C_RECT[2].m_sizeX / 2;
	C_RECT[1].m_startY = BLOCK_HEIGHT / 2 - 7 - C_RECT[2].m_sizeY / 2;

	C_RECT[2].m_startX = BLOCK_WIDTH / 2 - C_RECT[0].m_sizeX / 2;
	C_RECT[2].m_startY = BLOCK_HEIGHT / 2 - C_RECT[0].m_sizeY / 2;

	C_RECT[3].m_startX = BLOCK_WIDTH * 3 / 4 - C_RECT[3].m_sizeX / 2;
	C_RECT[3].m_startY = BLOCK_HEIGHT / 2 + 7 - C_RECT[3].m_sizeY / 2;

	C_RECT[4].m_startX = BLOCK_WIDTH / 4 - C_RECT[4].m_sizeX / 2;
	C_RECT[4].m_startY = BLOCK_HEIGHT / 2 + 7 - C_RECT[4].m_sizeY / 2;


	for(int t=0; t<5; t++){

		C_RECT[t].m_endX = C_RECT[t].m_startX + C_RECT[t].m_sizeX;
		C_RECT[t].m_endY = C_RECT[t].m_startY + C_RECT[t].m_sizeY;

		Rect_Set(C_RECT, t);

	}
// 	PosX=B_Total_PosX;
// 	PosY=B_Total_PosY;
// 	
// 	C_RECT[0].m_PosX = PosX-B_Dis_Width;
// 	C_RECT[0].m_PosY = PosY-B_Dis_Height;
// 	C_RECT[1].m_PosX = PosX+B_Dis_Width;
// 	C_RECT[1].m_PosY = PosY-B_Dis_Height;
// 
// 	C_RECT[2].m_PosX = PosX;
// 	C_RECT[2].m_PosY = PosY;
// 	C_RECT[3].m_PosX = PosX-B_Dis_Width;
// 	C_RECT[3].m_PosY = PosY+B_Dis_Height;
// 	C_RECT[4].m_PosX = PosX+B_Dis_Width;
// 	C_RECT[4].m_PosY = PosY+B_Dis_Height;
// 
// 	for(int t=0; t<5; t++){
// 		C_RECT[t].m_Width= B_Total_Width;
// 		C_RECT[t].m_Height=B_Total_Height;
// 		C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);//////C
// 		C_RECT[t].m_Thresold = B_Thresold;
// 	}
	UpdateData(FALSE);

	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	
	
// 	PBYTE pRGBz;
// 	if (!Get_TestImageBuffer(&pRGBz))
// 	{
// 		AfxMessageBox(_T("카메라 ON 해주세요"));
// 		//return;
// 	}
// 	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = 1;
// 
// 	for (int i = 0; i < 10; i++){
// 		if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0){
// 			break;
// 		}
// 		else{
// 			DoEvents(50);
// 		}
// 	}
// 
// 	for(int lop=0; lop<5;lop++){
// 		AveGen(m_RGBScanbuf, lop);
// 	}	
	UploadList();

	for(int t=0; t<5; t++){
		ChangeItem[t]=t;
	}
	
}

void CBrightness_Option::OnNMClickListBrightness(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;
}

void CBrightness_Option::OnNMDblclkListBrightness(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		return;
	}

	if(pNMITEM->iSubItem == 0 || pNMITEM->iSubItem == -1 || pNMITEM->iItem == -1 )
	return ;
	iSavedItem = pNMITEM->iItem;
	iSavedSubitem = pNMITEM->iSubItem;
	
	

	if(pNMITEM->iItem != -1)
	{
		if(pNMITEM->iSubItem != 0)
		{		
			B_DATALIST.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			B_DATALIST.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_BRIGHT))->SetWindowText(B_DATALIST.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_BRIGHT))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
			((CEdit *)GetDlgItem(IDC_EDIT_BRIGHT))->SetFocus();
			
		}
	}


	*pResult = 0;
}
void CBrightness_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if(bShow == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
	}
}

void CBrightness_Option::OnNMCustomdrawListBrightness(NMHDR *pNMHDR, LRESULT *pResult)
{
 COLORREF text_color = 0;
        COLORREF bg_color = RGB(255, 255, 255);
     
        LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
		
	//	lplvcd->iPartId
        switch(lplvcd->nmcd.dwDrawStage){
            case CDDS_PREPAINT:
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
             
            // 배경 혹은 텍스트를 수정한다.
            case CDDS_ITEMPREPAINT:
                // 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
               /* if(lplvcd->nmcd.dwItemSpec == 0) text_color = RGB(255, 0, 0);
                else if(lplvcd->nmcd.dwItemSpec == 1) text_color = RGB(0, 255, 0);
                else text_color = RGB(0, 0, 255);
                lplvcd->clrText = text_color;*/
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
           
            // 서브 아이템의 배경 혹은 텍스트를 수정한다.
            case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
                if(lplvcd->iSubItem != 0){
                    // 1번째 행이라면...

					if(lplvcd->nmcd.dwItemSpec >=0 ||lplvcd->nmcd.dwItemSpec <5){
						/*if(AutomationMod ==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 7 ){
								text_color = RGB(0, 0, 0);
								bg_color = RGB(200, 200, 200);
							}
						}
						else{
							text_color = RGB(0, 0, 0);
							bg_color = RGB(255, 255, 255);
						
						}*/

						if(ChangeCheck==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 13 ){
								for(int t=0; t<5; t++){
									if(lplvcd->nmcd.dwItemSpec == ChangeItem[t]){
										text_color = RGB(0, 0, 0);
										bg_color = RGB(250, 230, 200);					
									}
								}
							}
						
						
						}
					
					}
					

				    lplvcd->clrText = text_color;
                    lplvcd->clrTextBk = bg_color;
					
                }
                *pResult = CDRF_NEWFONT;
                return;
        }
}
void CBrightness_Option::List_COLOR_Change(){
	bool FLAG = TRUE;
	int Check=0;

	for(int t=0;t<5; t++){
		if(ChangeItem[t]==0)Check++;
		if(ChangeItem[t]==1)Check++;
		if(ChangeItem[t]==2)Check++;
		if(ChangeItem[t]==3)Check++;
		if(ChangeItem[t]==4)Check++;
	}

	if(Check !=5){
		FLAG =FALSE;
		ChangeItem[changecount]=iSavedItem;
	}

	if(!FLAG){
		for(int t=0; t<changecount+1; t++){
			for(int k=0; k<changecount+1; k++){
				
				if(ChangeItem[t] == ChangeItem[k]){
					if(t==k){break;	}			
					ChangeItem[k] =-1;
				}
			
			
			}
		}
			//----------------데이터 정렬
		int temp=0;

		for(int i=0; i<5; i++)
		{
			for(int j=i+1; j<5; j++)
			{
				if(ChangeItem[i] < ChangeItem[j])  // 내림차순
				{
					temp = ChangeItem[i];
					ChangeItem[i] = ChangeItem[j];
					ChangeItem[j] = temp;
				}
			}
		}
		//--------------------------------
		for(int t=0; t<5; t++){

			if(ChangeItem[t] ==-1){
				changecount =t;
				break;
			}
		}
	}

	for(int t=0; t<5; t++){
		B_DATALIST.Update(t);
	}


}
void CBrightness_Option::OnEnKillfocusEditBright()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	ChangeCheck=1;
	if((iSavedItem != -1)&&(iSavedSubitem != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_BRIGHT, str));
		List_COLOR_Change();
		if(B_DATALIST.GetItemText(iSavedItem, iSavedSubitem) != str){
			Change_DATA();
			UploadList();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_BRIGHT))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_BRIGHT))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );
	iSavedItem = -1;
	iSavedSubitem = -1;
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
}

BOOL CBrightness_Option::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	int znum = zDelta/120;
	CWnd *focusid;
	focusid = GetFocus();
	int casenum = focusid->GetDlgCtrlID();
	bool FLAG=0;
	if(znum > 0 ){
		FLAG = 1;
	}
	else if(znum < 0 ){
		FLAG =0;
	}
	else if(znum == 0){
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	}
	switch(casenum){
		case IDC_EDIT_BRIGHTNESS_POSX :
			EditWheelIntSet(IDC_EDIT_BRIGHTNESS_POSX,znum);
			OnEnChangeEditBrightnessPosx();
			break;
		case IDC_EDIT_BRIGHTNESS_POSY :
			EditWheelIntSet(IDC_EDIT_BRIGHTNESS_POSY,znum);
			OnEnChangeEditBrightnessPosy();
			break;
		case IDC_EDIT_BRIGHTNESS_DISW :
			EditWheelIntSet(IDC_EDIT_BRIGHTNESS_DISW,znum);
			OnEnChangeEditBrightnessDisw();
			break;
		case IDC_EDIT_BRIGHTNESS_DISH :
			EditWheelIntSet(IDC_EDIT_BRIGHTNESS_DISH,znum);
			OnEnChangeEditBrightnessDish();
			break;
		case IDC_EDIT_BRIGHTNESS_W :
			EditWheelIntSet(IDC_EDIT_BRIGHTNESS_W,znum);
			OnEnChangeEditBrightnessW();
			break;
		case IDC_EDIT_BRIGHTNESS_H :
			EditWheelIntSet(IDC_EDIT_BRIGHTNESS_H,znum);
			OnEnChangeEditBrightnessH();
			break;
		case IDC_EDIT_Bright_Thresold :
			EditWheelIntSet(IDC_EDIT_Bright_Thresold,znum);
			OnEnChangeEditBrightThresold();
			break;
		case IDC_EDIT_BRIGHT :
			//if((C_RECT[iSavedItem].m_Width != 0) ||(C_RECT[iSavedItem].m_Height != 0)){
			if(FLAG == 1){
				if((Change_DATA_CHECK(1) == TRUE)/* || (znum ==-1)*/){
					EditWheelIntSet(IDC_EDIT_BRIGHT,znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditBright();
				}
			}
			if(FLAG == 0){
				if((Change_DATA_CHECK(0) == TRUE) /*|| (znum == 1)*/){
					EditWheelIntSet(IDC_EDIT_BRIGHT,znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditBright();
				}
			}
			break;

	}
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

bool CBrightness_Option::Change_DATA_CHECK(bool FLAG){

	BOOL STAT = TRUE;
	//상한
	if (C_RECT[iSavedItem].m_startX > BLOCK_WIDTH-1)
		STAT = FALSE;
	if (C_RECT[iSavedItem].m_startY > BLOCK_HEIGHT-1)
		STAT = FALSE;
	if (C_RECT[iSavedItem].m_endX > BLOCK_WIDTH)
		STAT = FALSE;
	if (C_RECT[iSavedItem].m_endY > BLOCK_HEIGHT)
		STAT = FALSE;
	if (C_RECT[iSavedItem].m_sizeX > BLOCK_WIDTH)
		STAT = FALSE;
	if (C_RECT[iSavedItem].m_sizeY > BLOCK_HEIGHT)
		STAT = FALSE;


	if(C_RECT[iSavedItem].m_Width > m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Height > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosX > m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosY > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Top > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Bottom > m_CAM_SIZE_HEIGHT  )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Left> m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Right> m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Thresold> 100 )
		STAT = FALSE;

	//하한
	if (C_RECT[iSavedItem].m_startX < 0)
		STAT = FALSE;
	if (C_RECT[iSavedItem].m_startY < 0)
		STAT = FALSE;
	if (C_RECT[iSavedItem].m_endX < 1)
		STAT = FALSE;
	if (C_RECT[iSavedItem].m_endY < 1)
		STAT = FALSE;
	if (C_RECT[iSavedItem].m_sizeX < 1)
		STAT = FALSE;
	if (C_RECT[iSavedItem].m_sizeY < 1)
		STAT = FALSE;

	if(C_RECT[iSavedItem].m_Width < 1 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Height < 1 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosX < 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosY < 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Top < 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Bottom < 0  )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Left< 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Right< 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Thresold< 0 )
		STAT = FALSE;


	if(FLAG  == 1){
		if(C_RECT[iSavedItem].m_Width == 1 )
			STAT = FALSE;
		if(C_RECT[iSavedItem].m_Height == 1 )
			STAT = FALSE;
		
	}


	return STAT;
	
}

void CBrightness_Option::OnEnChangeEditBrightnessPosx()
{EditMinMaxIntSet(IDC_EDIT_BRIGHTNESS_POSX,&B_Total_PosX,0,m_CAM_SIZE_WIDTH);
	B_Total_PosX = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_POSX);	
	B_Total_PosY = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_POSY);
	B_Dis_Width	= GetDlgItemInt(IDC_EDIT_BRIGHTNESS_DISW);
	B_Dis_Height = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_DISH);
	B_Total_Width = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_W);
	B_Total_Height = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_H);
	B_Thresold = GetDlgItemInt(IDC_EDIT_Bright_Thresold);

	if((B_Total_PosX >= 0)&&(B_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(B_Total_PosY >= 0)&&(B_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(B_Dis_Width>=0)&&(B_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(B_Dis_Height>=0)&&(B_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(B_Total_Width>=0)&&(B_Total_Width<m_CAM_SIZE_WIDTH)&&
		(B_Total_Height>=0)&&(B_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(B_Thresold>=0)&&(B_Thresold<100)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
	}
}


void CBrightness_Option::OnEnChangeEditBrightnessPosy()
{
	EditMinMaxIntSet(IDC_EDIT_BRIGHTNESS_POSY,&B_Total_PosY,0,m_CAM_SIZE_HEIGHT);
	B_Total_PosX = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_POSX);	
	B_Total_PosY = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_POSY);
	B_Dis_Width	= GetDlgItemInt(IDC_EDIT_BRIGHTNESS_DISW);
	B_Dis_Height = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_DISH);
	B_Total_Width = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_W);
	B_Total_Height = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_H);
	B_Thresold = GetDlgItemInt(IDC_EDIT_Bright_Thresold);

	if((B_Total_PosX >= 0)&&(B_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(B_Total_PosY >= 0)&&(B_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(B_Dis_Width>=0)&&(B_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(B_Dis_Height>=0)&&(B_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(B_Total_Width>=0)&&(B_Total_Width<m_CAM_SIZE_WIDTH)&&
		(B_Total_Height>=0)&&(B_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(B_Thresold>=0)&&(B_Thresold<100)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
	}
}

void CBrightness_Option::OnEnChangeEditBrightnessDisw()
{
		EditMinMaxIntSet(IDC_EDIT_BRIGHTNESS_DISW,&B_Dis_Width,0,m_CAM_SIZE_WIDTH);
	B_Total_PosX = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_POSX);	
	B_Total_PosY = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_POSY);
	B_Dis_Width	= GetDlgItemInt(IDC_EDIT_BRIGHTNESS_DISW);
	B_Dis_Height = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_DISH);
	B_Total_Width = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_W);
	B_Total_Height = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_H);
	B_Thresold = GetDlgItemInt(IDC_EDIT_Bright_Thresold);

	if((B_Total_PosX >= 0)&&(B_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(B_Total_PosY >= 0)&&(B_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(B_Dis_Width>=0)&&(B_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(B_Dis_Height>=0)&&(B_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(B_Total_Width>=0)&&(B_Total_Width<m_CAM_SIZE_WIDTH)&&
		(B_Total_Height>=0)&&(B_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(B_Thresold>=0)&&(B_Thresold<100)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
	}
}

void CBrightness_Option::OnEnChangeEditBrightnessDish()
{
	EditMinMaxIntSet(IDC_EDIT_BRIGHTNESS_DISH,&B_Dis_Height,0,m_CAM_SIZE_HEIGHT);
	B_Total_PosX = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_POSX);	
	B_Total_PosY = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_POSY);
	B_Dis_Width	= GetDlgItemInt(IDC_EDIT_BRIGHTNESS_DISW);
	B_Dis_Height = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_DISH);
	B_Total_Width = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_W);
	B_Total_Height = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_H);
	B_Thresold = GetDlgItemInt(IDC_EDIT_Bright_Thresold);

	if((B_Total_PosX >= 0)&&(B_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(B_Total_PosY >= 0)&&(B_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(B_Dis_Width>=0)&&(B_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(B_Dis_Height>=0)&&(B_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(B_Total_Width>=0)&&(B_Total_Width<m_CAM_SIZE_WIDTH)&&
		(B_Total_Height>=0)&&(B_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(B_Thresold>=0)&&(B_Thresold<100)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
	}
}

void CBrightness_Option::OnEnChangeEditBrightnessW()
{
		EditMinMaxIntSet(IDC_EDIT_BRIGHTNESS_W,&B_Total_Width,0,m_CAM_SIZE_WIDTH);
	B_Total_PosX = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_POSX);	
	B_Total_PosY = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_POSY);
	B_Dis_Width	= GetDlgItemInt(IDC_EDIT_BRIGHTNESS_DISW);
	B_Dis_Height = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_DISH);
	B_Total_Width = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_W);
	B_Total_Height = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_H);
	B_Thresold = GetDlgItemInt(IDC_EDIT_Bright_Thresold);

	if((B_Total_PosX >= 0)&&(B_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(B_Total_PosY >= 0)&&(B_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(B_Dis_Width>=0)&&(B_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(B_Dis_Height>=0)&&(B_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(B_Total_Width>=0)&&(B_Total_Width<m_CAM_SIZE_WIDTH)&&
		(B_Total_Height>=0)&&(B_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(B_Thresold>=0)&&(B_Thresold<100)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
	}
}

void CBrightness_Option::OnEnChangeEditBrightnessH()
{
	EditMinMaxIntSet(IDC_EDIT_BRIGHTNESS_H,&B_Total_Height,0,m_CAM_SIZE_HEIGHT);
	B_Total_PosX = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_POSX);	
	B_Total_PosY = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_POSY);
	B_Dis_Width	= GetDlgItemInt(IDC_EDIT_BRIGHTNESS_DISW);
	B_Dis_Height = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_DISH);
	B_Total_Width = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_W);
	B_Total_Height = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_H);
	B_Thresold = GetDlgItemInt(IDC_EDIT_Bright_Thresold);

	if((B_Total_PosX >= 0)&&(B_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(B_Total_PosY >= 0)&&(B_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(B_Dis_Width>=0)&&(B_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(B_Dis_Height>=0)&&(B_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(B_Total_Width>=0)&&(B_Total_Width<m_CAM_SIZE_WIDTH)&&
		(B_Total_Height>=0)&&(B_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(B_Thresold>=0)&&(B_Thresold<100)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
	}
}

void CBrightness_Option::OnEnChangeEditBrightThresold()
{
	EditMinMaxIntSet(IDC_EDIT_Bright_Thresold,&B_Thresold,0,100);
	B_Total_PosX = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_POSX);	
	B_Total_PosY = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_POSY);
	B_Dis_Width	= GetDlgItemInt(IDC_EDIT_BRIGHTNESS_DISW);
	B_Dis_Height = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_DISH);
	B_Total_Width = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_W);
	B_Total_Height = GetDlgItemInt(IDC_EDIT_BRIGHTNESS_H);
	B_Thresold = GetDlgItemInt(IDC_EDIT_Bright_Thresold);

	if((B_Total_PosX >= 0)&&(B_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(B_Total_PosY >= 0)&&(B_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(B_Dis_Width>=0)&&(B_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(B_Dis_Height>=0)&&(B_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(B_Total_Width>=0)&&(B_Total_Width<m_CAM_SIZE_WIDTH)&&
		(B_Total_Height>=0)&&(B_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(B_Thresold>=0)&&(B_Thresold<100)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
	}
}
void CBrightness_Option::EditWheelIntSet(int nID,int Val)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = 0;
	}else{
		buf = GetDlgItemInt(nID);
		buf+= Val;
	}
	SetDlgItemInt(nID,buf,1);
	((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
}
void CBrightness_Option::EditMinMaxIntSet(int nID,int* Val,int Min,int Max)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = Min;
		retval = FALSE;
	}else{
		buf = GetDlgItemInt(nID);
		if(buf<Min){
			buf = *Val;
			retval = TRUE;
		}else if(buf > Max){
			buf = *Val;
			retval = TRUE;
		}else{
			retval = FALSE;
		}
	}
	*Val = buf;
	if(retval == TRUE){
		SetDlgItemInt(nID,buf,1);
		((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
	}
}

void CBrightness_Option::OnEnChangeEditBright()
{
	int num = iSavedItem;
	if((C_RECT[num].m_PosX >= 0)&&(C_RECT[num].m_PosX < m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_PosY >= 0)&&(C_RECT[num].m_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_RECT[num].m_Left>=0)&&(C_RECT[num].m_Left<m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_Top>=0)&&(C_RECT[num].m_Top<m_CAM_SIZE_HEIGHT)&&
		(C_RECT[num].m_Right>=0)&&(C_RECT[num].m_Right<m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_Bottom>=0)&&(C_RECT[num].m_Bottom<m_CAM_SIZE_HEIGHT)&&
		(C_RECT[num].m_Width>=0)&&(C_RECT[num].m_Width<m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_Height>=0)&&(C_RECT[num].m_Height<m_CAM_SIZE_HEIGHT)&&
		(C_RECT[num].m_Thresold>=0)&&(C_RECT[num].m_Thresold<100)){

			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SAVE))->EnableWindow(1);
		

	}else{
		//((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SAVE))->EnableWindow(0);
	}
}

void CBrightness_Option::OnBnClickedButtontest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(0);
	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(1);
}

#pragma region LOT관련 함수
void CBrightness_Option::LOT_Set_List(CListCtrl *List){

	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT 명",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);
// 	List->InsertColumn(5,"LT",LVCFMT_CENTER, 80);
// 	List->InsertColumn(6,"RT",LVCFMT_CENTER, 80);
// 	List->InsertColumn(7,"C",LVCFMT_CENTER, 80);
// 	List->InsertColumn(8,"LD",LVCFMT_CENTER, 80);
// 	List->InsertColumn(9,"RD",LVCFMT_CENTER, 80);
// 	List->InsertColumn(10,"RESULT",LVCFMT_CENTER, 80);
// 	List->InsertColumn(11,"비고",LVCFMT_CENTER, 80);

// 	List->InsertColumn(5, "Center-Ave", LVCFMT_CENTER, 80);
// 	List->InsertColumn(6, "Center-Std", LVCFMT_CENTER, 80);
// 	List->InsertColumn(7, "Center-%%", LVCFMT_CENTER, 80);
// 
// 	CString ListName;
// 	int ListNUM = 8;
// 	for (int k = 0; k < BRPoint_MaxNum-1; k++)
// 	{
// 		for (int t = 0; t < 40; t++)
// 		{
// 			ListName.Format("[%d][%d]-Ave", k + 1, t + 1);
// 			List->InsertColumn(ListNUM, ListName, LVCFMT_CENTER, 80);
// 			ListNUM++;
// 			ListName.Format("[%d][%d]-Std", k + 1, t + 1);
// 			List->InsertColumn(ListNUM, ListName, LVCFMT_CENTER, 80);
// 			ListNUM++;
// 			ListName.Format("[%d][%d]-%%", k + 1, t + 1);
// 			List->InsertColumn(ListNUM, ListName, LVCFMT_CENTER, 80);
// 			ListNUM++;
// 			
// 		}
// 	}

	CString ListName;
	int ListNUM = 5;


	for (int k = 0; k < BRPoint_MaxNum; k++)
	{
		for (int t = 0; t < st_BRMaster.pt_StartPoint_PixPosList[k].size(); t++)
		{
			if (k != 4)
			{
				ListName.Format("[%d-%d]-Percent", k + 1, t + 1);
				List->InsertColumn(ListNUM, ListName, LVCFMT_CENTER, 80);
				ListNUM++;
			}

			ListName.Format("[%d-%d]-Brightness", k + 1, t + 1);
			List->InsertColumn(ListNUM, ListName, LVCFMT_CENTER, 80);
			ListNUM++;
		}
	}

	List->InsertColumn(ListNUM, "RESULT", LVCFMT_CENTER, 80);
	List->InsertColumn(ListNUM + 1, "비고", LVCFMT_CENTER, 80);

	Lot_InsertNum = ListNUM + 2;
	Copy_List(&m_Lot_BrightnessList, ((CImageTesterDlg *)m_pMomWnd)->m_pWorkList, Lot_InsertNum);
}

void CBrightness_Option::LOT_InsertDataList(){
	CString strCnt="";

	//int Index = m_Lot_BrightnessList.InsertItem(Lot_StartCnt,"",0);
	Lot_InsertIndex = m_Lot_BrightnessList.InsertItem(Lot_StartCnt, "", 0);
	strCnt.Empty();
	strCnt.Format(_T("%d"), Lot_InsertIndex + 1);
	m_Lot_BrightnessList.SetItemText(Lot_InsertIndex, 0, strCnt);

	m_Lot_BrightnessList.SetItemText(Lot_InsertIndex, 1, ((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_Lot_BrightnessList.SetItemText(Lot_InsertIndex, 2, ((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);////////LOT 이름
	m_Lot_BrightnessList.SetItemText(Lot_InsertIndex, 3, ((CImageTesterDlg *)m_pMomWnd)->strDay + "");
	m_Lot_BrightnessList.SetItemText(Lot_InsertIndex, 4, ((CImageTesterDlg *)m_pMomWnd)->strTime + "");

// 	int CopyIndex = m_BrightnessList.GetItemCount()-1;
// 
// 	int count =0;
// 	for(int t=0; t< Lot_InsertNum;t++){
// 		if((t != 2)&&(t!=0)){
// 			m_Lot_BrightnessList.SetItemText(Index,t, m_BrightnessList.GetItemText(CopyIndex,count));
// 			count++;
// 
// 		}else if(t==0){
// 			count++;
// 		}else{
// 			m_Lot_BrightnessList.SetItemText(Index,t,((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
// 		}
// 	}
// 
// 	Lot_StartCnt++;

}
void CBrightness_Option::LOT_EXCEL_SAVE(){
	//CString Item[5]={"LT","RT","C","LD","RD"};

// 	CString Item[483] = { "Center-Ave", "Center-Std", "Center-%", };
// 
// 
// 	CString ListName;
// 	int ListNUM = 3;
// 	for (int k = 0; k < BRPoint_MaxNum-1; k++)
// 	{
// 		for (int t = 0; t < 40; t++)
// 		{
// 			ListName.Format("[%d][%d]-Ave", k + 1, t + 1);
// 			Item[ListNUM] = ListName;
// 			ListNUM++;
// 			ListName.Format("[%d][%d]-Std", k + 1, t + 1);
// 			Item[ListNUM] = ListName;
// 			ListNUM++;
// 			ListName.Format("[%d][%d]-%", k + 1, t + 1);
// 			Item[ListNUM] = ListName;
// 			ListNUM++;
// 		}
// 	}

	CString Item[BRPoint_MaxNum - 1];

	CString ListName;
	int ListNUM = 0;
	for (int k = 0; k < BRPoint_MaxNum - 1; k++)
	{
		ListName.Format("[%d]-FAILCnt", k + 1);
		Item[ListNUM] = ListName;
		ListNUM++;
	}

	CString Data ="";
	CString str="";
	str = "***********************광량비 검사 [ 단위 : 밝기값, Center 대비편차 (%) ]***********************\n";
	Data += str;
	str.Empty();
	str.Format("Left Top ,Threshold,  %d ,, ",C_RECT[0].m_Thresold);
	Data += str;
	str.Empty();
	str.Format("Right Top ,Threshold,  %d, \n",C_RECT[1].m_Thresold);
	Data += str;
	str.Empty();
	str.Format("Left Bottom ,Threshold, %d, ,",C_RECT[3].m_Thresold);
	Data += str;
	str.Empty();
	str.Format("Right Bottom ,Threshold, %d,\n ",C_RECT[4].m_Thresold);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("광량비검사", Item, ListNUM, &m_Lot_BrightnessList, Data);
}

bool CBrightness_Option::LOT_EXCEL_UPLOAD(){
	m_Lot_BrightnessList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("광량비검사",&m_Lot_BrightnessList,1,&Lot_StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}

#pragma endregion 

void CBrightness_Option::Rect_Set(CRectData *Rect, int Num){

	Rect[Num].m_Left = Rect[Num].m_startX * BLOCK_SIZE;
	Rect[Num].m_Right = Rect[Num].m_endX * BLOCK_SIZE;
	Rect[Num].m_Top = Rect[Num].m_startY*BLOCK_SIZE;
	Rect[Num].m_Bottom = Rect[Num].m_endY*BLOCK_SIZE;
	Rect[Num].m_Width = Rect[Num].m_sizeX * BLOCK_SIZE;
	Rect[Num].m_Height = Rect[Num].m_sizeY * BLOCK_SIZE;

}

void CBrightness_Option::OnBnClickedButtonBrightnessLoad()
{
	Load_parameter();

	UploadList();

	for (int t = 0; t < 5; t++){
		ChangeItem[t] = t;
	}

}


void CBrightness_Option::OnBnClickedButtonBrightnessMaster()
{
	if (!((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK()){
		AfxMessageBox("Please turn on the camera and try again.");
		return;
	}

	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = 1;
	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;

	for (int i = 0; i < 10; i++){
		if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0 && ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0){
			break;
		}
		else{
			DoEvents(50);
		}
	}

	for (int lop = 0; lop < 5; lop++){
		//AveGen(m_RGBScanbuf, lop);
		AveGen_16bit(m_GRAYScanbuf, lop);
	}

	for (int lop = 0; lop<5; lop++){
		if (abs((int)C_RECT[lop].BrightData_Ave) > 0){
			C_RECT[lop].m_Master = (int)C_RECT[lop].BrightData_Ave;
		}
		else{
			AfxMessageBox("Not a test environment.");
			break;
		}
	}
	UpdateData(FALSE);
	UploadList();
}
double CBrightness_Option::Compute_Result(int NUM){
	double Result;

	Result = ((C_RECT[NUM].m_Master - C_RECT[NUM].BrightData_Ave) * 100) / C_RECT[NUM].m_Master;

	return Result;
}
void CBrightness_Option::AveGen_Block(LPBYTE IN_RGB, CRectData *Rect, int X, int Y){//이미지 전체 블록당 밝기 평균값

	BYTE *BW;
	DWORD R = 0, G = 0, B = 0;
	DWORD	RGBPIX = 0;
	DWORD	Total_R = 0, Total_G = 0, Total_B = 0;
	int index = 0;
	double Sum = 0;
	DWORD Total = BLOCK_SIZE*BLOCK_SIZE;
	BW = new BYTE[Total];
	memset(BW, 0, sizeof(BW));
	int startx = X*BLOCK_SIZE;
	int starty = Y*BLOCK_SIZE;
	int endx = startx + BLOCK_SIZE-1;
	int endy = starty + BLOCK_SIZE-1;
	DWORD RGBLINE = starty * (m_CAM_SIZE_WIDTH * 3);

	for (int lopy = starty; lopy < endy; lopy++){
		RGBPIX = startx * 3;
		for (int lopx = startx; lopx < endx; lopx++){

			B = IN_RGB[RGBLINE + RGBPIX];
			G = IN_RGB[RGBLINE + RGBPIX + 1];
			R = IN_RGB[RGBLINE + RGBPIX + 2];
			BW[index] = (0.29900*R) + (0.58700*G) + (0.11400*B);
			index++;

			RGBPIX += 3;
		}
		RGBLINE += (m_CAM_SIZE_WIDTH * 3);
	}

	Sum = 0;
	for (int t = 0; t < (index - 1); t++){
		Sum += BW[t];
	}

	Rect[Y*BLOCK_WIDTH + X].BrightData_Ave = Sum / (double)index;

	delete BW;
}
void CBrightness_Option::AveGen_Block_16bit(LPWORD IN_RGB, CRectData *Rect, int X, int Y){//이미지 전체 블록당 밝기 평균값

	WORD *BW;
	int index = 0;
	double Sum = 0;
	DWORD Total = BLOCK_SIZE*BLOCK_SIZE;
	BW = new WORD[Total];
	memset(BW, 0, sizeof(BW));
	int startx = X*BLOCK_SIZE;
	int starty = Y*BLOCK_SIZE;
	int endx = startx + BLOCK_SIZE - 1;
	int endy = starty + BLOCK_SIZE - 1;

	for (int lopy = starty; lopy < endy; lopy++){
		for (int lopx = startx; lopx < endx; lopx++){

			BW[index] = IN_RGB[lopy*m_CAM_SIZE_WIDTH + lopx];
			index++;
		}
	}

	Sum = 0;
	for (int t = 0; t < (index - 1); t++){
		Sum += BW[t];
	}

	Rect[Y*BLOCK_WIDTH + X].BrightData_Ave = Sum / (double)index;

	delete BW;
}
void CBrightness_Option::SAVE_FILE_AVE(CRectData *Rect, int X, int Y)
{ 
	CTime time = CTime::GetTickCount();
	int y = time.GetYear();
	int m = time.GetMonth();
	int d = time.GetDay();

	CTime thetime = CTime::GetCurrentTime();

	int hour = 0, minute = 0, second = 0, hour2 = 0;
	hour = thetime.GetHour(); minute = thetime.GetMinute(); second = thetime.GetSecond();

	CString strFilename;
	strFilename = ((CImageTesterDlg *)m_pMomWnd)->m_modelName;
	((CImageTesterDlg *)m_pMomWnd)->CREATE_Folder(strFilename, 5);
	CString DataSave_Path = ((CImageTesterDlg *)m_pMomWnd)->MODEL_FOLDERPATH_P;
	
	strFilename.Format("%s\\AVE_%04d_%02d_%02d_%02dh_%02dm_%02ds.csv", DataSave_Path,y,m,d ,hour, minute, second);

	CStdioFile File;
	CFileException e;
	if (!File.Open((LPCTSTR)strFilename, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
		return;

	}

	CString strOut = "";
	CString str = "";


	for (int t = 0; t < Y; t++){
		for (int i = 0; i < X; i++){
			str.Empty();
			str.Format("%0.3f,", Rect[X*t + i].BrightData_Ave);
			strOut += str;
			if (i == X - 1){
				strOut += "\n";
			}
		}

	}
	File.WriteString(LPCTSTR(strOut));
	File.Close();
}

void CBrightness_Option::OnBnClickedChkBlockDataSave()
{
	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);

	CString str;
	str.Empty();
	str.Format("%d", b_BlockDataSave);
	WritePrivateProfileString(str_model, "BLOCK_DATA_SAVE_B", str, str_ModelPath);

	b_BlockDataSave = GetPrivateProfileInt(str_model, "BLOCK_DATA_SAVE_B", 1, str_ModelPath);

	UpdateData(FALSE);
}


void CBrightness_Option::OnBnClickedChkMastermode()
{
	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);

	CString str;
	str.Empty();
	str.Format("%d", b_MasterDev);
	WritePrivateProfileString(str_model, "MASTER_DEV", str, str_ModelPath);

	b_MasterDev = GetPrivateProfileInt(str_model, "MASTER_DEV", 1, str_ModelPath);

	UpdateData(FALSE);
}


void CBrightness_Option::OnBnClickedButtonMasterload()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), _T("txt"), _T("txt"));
	CString strFileSel;
	strFileSel.Format(_T("*.%s"), _T("txt"));


	TCHAR szExePath[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, szExePath, MAX_PATH);

	TCHAR drive[_MAX_DRIVE];
	TCHAR dir[_MAX_DIR];
	TCHAR file[_MAX_FNAME];
	TCHAR ext[_MAX_EXT];
	_tsplitpath_s(szExePath, drive, _MAX_DRIVE, dir, _MAX_DIR, file, _MAX_FNAME, ext, _MAX_EXT);

	CString szProgramPath;
	szProgramPath.Format(_T("%s%s"), drive, dir);


	// 파일 불러오기
	CFileDialog fileDlg(TRUE, _T("txt"), strFileSel, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = szProgramPath;

	if (fileDlg.DoModal() == IDOK)
	{
		strFullPath = fileDlg.GetPathName();
		strFileTitle = fileDlg.GetFileTitle();

		str_MasterPath = strFullPath;
		st_BRMaster.str_MasterPath = str_MasterPath;
		WritePrivateProfileString(str_model, "MASTERPATH", str_MasterPath, C_filename);
		Load_parameter();
	}
	UpdateData(FALSE);
}


BOOL CBrightness_Option::Master_Data_Load()
{
	st_BRMaster.Reset();

	CString str;
	str.Format(_T("%d"), st_BRMaster.nMaster_Block_X);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_X))->SetWindowText(str);
	str.Format(_T("%d"), st_BRMaster.nMaster_Block_Y);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_Y))->SetWindowText(str);
	str.Format(_T("%d"), st_BRMaster.nMaster_Detect_X);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_X2))->SetWindowText(str);
	str.Format(_T("%d"), st_BRMaster.nMaster_Detect_Y);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_Y2))->SetWindowText(str);

	CStdioFile File;

	CString filePath;  //파일 경로 저장
	CString tmp = "";        // 한줄씩 읽은 문자열 저장
	CString pb;         // 문자열에서 잘라낸 로그값 저장
	CString strFilename = "";

	CFileException e;
	TCHAR			lpszBuf[256];

	int count = 0;

	if (!File.Open((LPCTSTR)st_BRMaster.str_MasterPath, CFile::modeRead | CFile::shareDenyNone, &e)){
		//AfxMessageBox("해당 csv파일을 찾을 수 없습니다. 처음부터 작업시작합니다.");
		return FALSE;
	}
	File.SeekToBegin();

	// - Master Block Area 찾기

	// 첫번째 줄.
	File.ReadString(tmp);

	CString szData;
	int length = 0;
	CString szDataTemp;
	int space_index = 0;
	szData = tmp;

	//첫번째 줄
	while (true)
	{
		length = szData.GetLength();
		space_index = szData.Find('\t');

		if (space_index == -1)
		{
			st_BRMaster.nMaster_Block_X = _ttoi(szData);
			break;
		}
		szData = szData.Mid(space_index + 1, length - space_index);
	}
	POINT pt_Pix;
	//두번째 줄
	while (File.ReadString(szData))
	{
		length = szData.GetLength();
		space_index = szData.Find('\t');

		if (space_index != -1)
		{
			//	szDataTemp = szData.Mid(space_index + 1, length - space_index);
			//	int Tapspace_index = szDataTemp.Find('\t');
// 			for (int lop = 0; lop < BRPoint_MaxNum; lop++){
// 				st_BRMaster.pt_StartPointList[lop].clear();
// 			}

			szDataTemp = szData.Mid(0, space_index);

			st_BRMaster.nMaster_Block_Y = _ttoi(szDataTemp);

			for (int t = 0; t < st_BRMaster.nMaster_Block_X; t++)
			{
				szDataTemp = szData.Mid(space_index + 1, length - space_index);
				int Tapspace_index = szDataTemp.Find('\t');

				szDataTemp = szDataTemp.Mid(0, Tapspace_index);

				int idata = _ttoi(szDataTemp);

				space_index += (Tapspace_index + 1);

				if (idata != 0)
				{
					pt_Pix.x = t;
					pt_Pix.y = st_BRMaster.nMaster_Block_Y - 1;

					switch (idata-1)
					{
					case BRPoint_1st:
						st_BRMaster.pt_StartPointList[BRPoint_1st].push_back(pt_Pix);
						break;
					case BRPoint_2nd:
						st_BRMaster.pt_StartPointList[BRPoint_2nd].push_back(pt_Pix);
						break;
					case BRPoint_3rd:
						st_BRMaster.pt_StartPointList[BRPoint_3rd].push_back(pt_Pix);
						break;
					case BRPoint_4th:
						st_BRMaster.pt_StartPointList[BRPoint_4th].push_back(pt_Pix);
						break;
					case BRPoint_Center:
						st_BRMaster.pt_StartPointList[BRPoint_Center].push_back(pt_Pix);
						break;
					default:
						break;
					}

				
				}
			}
		}
	}



	if (st_BRMaster.nMaster_Block_Y < 2)
	{
		return FALSE;
	}
	if (st_BRMaster.nMaster_Block_X < 2)
	{
		return FALSE;
	}
	//한 블럭 당 Pix 수

	int nVerCnt = CAM_IMAGE_HEIGHT / (st_BRMaster.nMaster_Block_Y - 1);
	int nHorCnt = CAM_IMAGE_WIDTH / (st_BRMaster.nMaster_Block_X - 1);

	st_BRMaster.nMaster_Detect_X = nHorCnt;
	st_BRMaster.nMaster_Detect_Y = nVerCnt;
	POINT pt_PixData;
	int nDataCnt = 0;
	for (int t = 0; t < BRPoint_MaxNum; t++)
	{
		//st_BRMaster.pt_StartPoint_PixPosList[t].clear();
		nDataCnt = st_BRMaster.pt_StartPointList[t].size();
		for (int k = 0; k< nDataCnt;k++)
		{
			pt_Pix = st_BRMaster.pt_StartPointList[t][k];

			pt_PixData.x = (pt_Pix.x * st_BRMaster.nMaster_Detect_X) - st_BRMaster.nMaster_Detect_X / 2;
			pt_PixData.y = (pt_Pix.y * st_BRMaster.nMaster_Detect_Y) - st_BRMaster.nMaster_Detect_Y / 2;
			st_BRMaster.pt_StartPoint_PixPosList[t].push_back(pt_PixData);
		}

	}
	

	str.Format(_T("%d"), st_BRMaster.nMaster_Block_X);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_X))->SetWindowText(str);
	str.Format(_T("%d"), st_BRMaster.nMaster_Block_Y);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_Y))->SetWindowText(str);
	str.Format(_T("%d"), st_BRMaster.nMaster_Detect_X);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_X2))->SetWindowText(str);
	str.Format(_T("%d"), st_BRMaster.nMaster_Detect_Y);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_Y2))->SetWindowText(str);

	File.Close();
	return TRUE;
}


void CBrightness_Option::OnCbnSelendokComboImageSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_nImageSaveMode = m_Cb_ImageSave.GetCurSel();

	CString str_ModelPath = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);

	CString str;
	str.Empty();
	str.Format("%d", m_nImageSaveMode);
	WritePrivateProfileString(str_model, "Image_SaveMode", str, str_ModelPath);

	
}
