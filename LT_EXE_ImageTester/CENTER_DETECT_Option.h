#pragma once

#include "EtcModelDlg.h"
#include "afxwin.h"
#include "afxcmn.h"

// CCENTER_DETECT_Option 대화 상자입니다.

class CCENTER_DETECT_Option : public CDialog
{
	DECLARE_DYNAMIC(CCENTER_DETECT_Option)

public:
	CCENTER_DETECT_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCENTER_DETECT_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_CENTER };

	_EtcWrdata CenterVal;
	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
	tResultVal Run(void);

	bool b_TESTResult;
	
	double GetDistance(int x1, int y1, int x2, int y2);

	int	m_CAM_SIZE_WIDTH;
	int	m_CAM_SIZE_HEIGHT;

	CvPoint GetCenterPoint(LPBYTE IN_RGB);
	//---------------sdy-------------------------
	CComboBox	*pRunCombo;
	CComboBox	*pWrModeCombo;
	CComboBox	*pINTWROPTCombo;
	CComboBox	*pWROPTCombo;

	int		m_iSelWrMode;
	void	SetOptMode(int num);

	_OptPr	CenterPrm;
	int		m_EtcNumber;
	int		m_file_sel;
	void	BinFiledel();
	void	SetFileSel();
	void	Save_parameter();
	void	Load_parameter();
	void	StatePrintf(LPCSTR lpcszString, ...);

	
	CString		BinfileName;
	CString		oldBinfileName;
	CString		model_bin_path;

	int		BinFileCHK(int stat);
	BYTE	BinFileCHK();
	BYTE	BINBUF[1024*512];
	char	OffsetBuffer[32][256];

	BYTE	m_filemode;
	unsigned long	AddOfOffset[4];
	unsigned long	m_BINSIZE;
	unsigned long	IndexOffset;
	unsigned long	IndexOffset2;

	char	xoffdata;
	char	yoffdata;
	char	m_DChkSum1;
	char	m_DChkSum2;

	BYTE	m_ChkSumMode;
	BOOL	B_FileChk;
	BOOL	B_FileRead;
	int		m_binmod;

	BOOL	m_Running;
	int		m_RunMode;
	CString ModeName;
	int		OffsetWrite(_TOTWrdata * INDATA);
	BOOL	SendData8Byte(unsigned char * TxData, int Num,_TOTWrdata * INDATA);
	BOOL	EtcSendData8Byte(_TOTWrdata * INDATA,int Num);

	void	centergen(double centx,double centy,_EtcWrdata *EtcDATA);
	void	OpticalAsixModify(_EtcWrdata * INDATA);
	void	MoveCenter(_EtcWrdata * INDATA);
	int		OnSendSerial(_EtcWrdata * INDATA);
	int		MicomOnSendSerial(_EtcWrdata * INDATA);
	int		CanOnSendSerial(_EtcWrdata * INDATA,bool stat); 
	int		CanOffsetWrite(_EtcWrdata * INDATA,bool stat);
	int		CanDLOffsetWr(_EtcWrdata * INDATA,bool stat); 

	int		CanOnSendSerial(_EtcWrdata * INDATA);
	int		CanOffsetWrite(_EtcWrdata * INDATA);

	//2017.08.29
	void	CanInitUpdate();


	CvPoint EtcPt[4];
	void	Find_offset(_EtcWrdata * INDATA);
	
//	BOOL	ChkTstEnd(_TOTWrdata * INDATA);
	BOOL	ChkTstEnd(_EtcWrdata * INDATA);
	
	void	Initgen(_EtcWrdata *EtcDATA);

	void	Center_Pic(CDC* cdc);
	CvPoint	m_dCurrCenterCoord;
	int		m_CheckWrite;
	int		m_ChkSector;
	
	

	void	EtcModelNameChange(int num);
	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void InsertList();
	int InsertIndex;
	int ListItemNum;

	BOOL CheckSettingDetect_1();

	CString TESTLOT[4][30];
	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();
	void FAIL_UPLOAD();
	BOOL	b_StopFail;
	void Setup_List();
	void CPK_DATA_SUM();
	int CPK_DATA[2][200];
	int CPKcount;
	int StartCnt;
	
	double SaveCPK_X;
	double SaveCPK_Y;

	// MES 정보저장
	CString Str_Mes[2][2];
	CString m_szMesResult;
	CString Mes_Result();

	CString Mes_Result_InitData();
	void	InitEVMS();

#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	int Lot_StartCnt;
	int Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

#pragma endregion

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	CString		str_model;
	CString		strTitle;
	CString		str_ModelPath;
	CString		str_BinPath;
	tINFO		m_INFO;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnBinadd();
	virtual BOOL OnInitDialog();
	UINT m_iStandX;
	UINT m_iStandY;
	double m_dPassDeviatX;
	double m_dPassDeviatY;
	double m_dRateX;
	double m_dRateY;
	UINT m_iEnPixX;
	UINT m_iEnPixY;
	UINT m_iMaxCount;
	int m_iWRMODE;
	afx_msg void OnBnClickedOpticalstatSave();
	afx_msg void OnCbnSelchangeComboMode();

	CvPoint m_ptMasterCoord[4];
	CvPoint m_ptCenterPointCoord[4];
	CListCtrl m_CenterWorklist;
	double m_dPassMaxDevX;
	double m_dPassMaxDevY;
	afx_msg void OnCbnSelchangeComboIntwropt();
	afx_msg void OnCbnSelchangeComboWropt2();
	afx_msg void OnBnClickedOpticalstatSave2();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButtonMicom();
	afx_msg void OnBnClickedButtontest();
	CListCtrl m_Lot_CenterList;
	afx_msg void OnCbnSelchangeComobWrmode();
	afx_msg void OnCbnSelchangeOptWrmode();
	afx_msg void OnCbnSelchangeCbOptCenRetry();
	CComboBox m_cb_RetryCnt;
    afx_msg void OnBnClickedCheckCan();
    BOOL CheckOnOverlay(LPBYTE IN_RGB);
    BOOL OverlayCheck_TEST();
    BOOL b_CanOverlayCheck;
    afx_msg void OnBnClickedButtonOverlaytest();
};
