// Particle_Option_LG.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Particle_Option_LG.h"

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
extern BYTE m_pYOut[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];
extern BYTE	m_YUVIn[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 2];
extern BYTE	m_YUVOut[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 2];
// CParticle_Option_LG 대화 상자입니다.

IMPLEMENT_DYNAMIC(CParticle_Option_LG, CDialog)

CParticle_Option_LG::CParticle_Option_LG(CWnd* pParent /*=NULL*/)
	: CDialog(CParticle_Option_LG::IDD, pParent)
	, str_Block_W(_T(""))
	, str_Block_H(_T(""))
	, str_DF_Ratio(_T(""))
	, str_Cluster_Size(_T(""))
	, str_DF_inCluster(_T(""))
	, str_DefectNum(_T(""))
	, str_RoiRadius(_T(""))
	, b_BlackSpotTESTEn(FALSE)
	, b_StainTESTEn(FALSE)
	, str_EdgeSize(_T(""))
	, str_Center_Thr(_T(""))
	, str_Edge_Thr(_T(""))
	, str_Stain_Block_Width(_T(""))
	, str_Stain_ROIRadius(_T(""))
	, b_ManualTestMode(FALSE)
//	, m_uTestMode(_T(""))
, m_uTestMode(0)
, Stain_Debug(FALSE)
{
// 	m_BlackSpot = NULL;
// 	m_Stain = NULL;
	StartCnt = 0;
	Stain_ParticleCnt=0;
	Black_ParticleCnt=0;
	
}

CParticle_Option_LG::~CParticle_Option_LG()
{
// 	if(m_Stain != NULL){
// 		delete m_Stain;
// 		m_Stain = NULL;
// 	}
// 	if(m_BlackSpot != NULL){
// 		delete m_BlackSpot;
// 		m_BlackSpot = NULL;
// 
// 	}
}

void CParticle_Option_LG::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_PARTICLE_LG, m_ParticleList);
	DDX_Control(pDX, IDC_LIST_PARTICLE_LOT_LG, m_Lot_ParticleList);
	DDX_Text(pDX, IDC_EDIT_Black_W, str_Block_W);
	DDX_Text(pDX, IDC_EDIT_Black_H, str_Block_H);
	DDX_Text(pDX, IDC_EDIT_DF_Ratio, str_DF_Ratio);
	DDX_Text(pDX, IDC_EDIT_ClusterSize, str_Cluster_Size);
	DDX_Text(pDX, IDC_EDIT_DefectInCluster, str_DF_inCluster);
	DDX_Text(pDX, IDC_EDIT_DefectNum, str_DefectNum);
	DDX_Text(pDX, IDC_EDIT_ROIRadius, str_RoiRadius);
	DDX_Check(pDX, IDC_CHECK_Black_Spot, b_BlackSpotTESTEn);
	DDX_Check(pDX, IDC_CHECK_StainTEST, b_StainTESTEn);
	DDX_Text(pDX, IDC_EDIT_EdgeSize, str_EdgeSize);
	DDX_Text(pDX, IDC_EDIT_Center_Thr, str_Center_Thr);
	DDX_Text(pDX, IDC_EDIT_Edge_Thr, str_Edge_Thr);
	DDX_Text(pDX, IDC_EDIT_BlockWidth, str_Stain_Block_Width);
	DDX_Text(pDX, IDC_EDIT_STAIN_ROIRADIUS, str_Stain_ROIRadius);
	DDX_Check(pDX, IDC_CHECK_MANUALTEST_LG, b_ManualTestMode);
	//	DDX_CBString(pDX, IDC_COMBO_TESTMODE, m_uTestMode);
	DDX_CBIndex(pDX, IDC_COMBO_TESTMODE, m_uTestMode);
	DDX_Check(pDX, IDC_CHECK_DEBUG, Stain_Debug);
}


BEGIN_MESSAGE_MAP(CParticle_Option_LG, CDialog)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_CHECK_MANUALTEST_LG, &CParticle_Option_LG::OnBnClickedCheckManualtestLg)
	ON_BN_CLICKED(IDC_BUTTON_BlockSpot_SAVE, &CParticle_Option_LG::OnBnClickedButtonBlockspotSave)
	ON_EN_CHANGE(IDC_EDIT_Black_W, &CParticle_Option_LG::OnEnChangeEditBlackW)
	ON_EN_CHANGE(IDC_EDIT_Black_H, &CParticle_Option_LG::OnEnChangeEditBlackH)
	ON_EN_CHANGE(IDC_EDIT_DF_Ratio, &CParticle_Option_LG::OnEnChangeEditDfRatio)
	ON_EN_CHANGE(IDC_EDIT_ClusterSize, &CParticle_Option_LG::OnEnChangeEditClustersize)
	ON_EN_CHANGE(IDC_EDIT_DefectInCluster, &CParticle_Option_LG::OnEnChangeEditDefectincluster)
	ON_EN_CHANGE(IDC_EDIT_DefectNum, &CParticle_Option_LG::OnEnChangeEditDefectnum)
	ON_EN_CHANGE(IDC_EDIT_ROIRadius, &CParticle_Option_LG::OnEnChangeEditRoiradius)
	ON_BN_CLICKED(IDC_CHECK_Black_Spot, &CParticle_Option_LG::OnBnClickedCheckBlackSpot)
	ON_BN_CLICKED(IDC_CHECK_StainTEST, &CParticle_Option_LG::OnBnClickedCheckStaintest)
	ON_EN_CHANGE(IDC_EDIT_EdgeSize, &CParticle_Option_LG::OnEnChangeEditEdgesize)
	ON_EN_CHANGE(IDC_EDIT_Center_Thr, &CParticle_Option_LG::OnEnChangeEditCenterThr)
	ON_EN_CHANGE(IDC_EDIT_Edge_Thr, &CParticle_Option_LG::OnEnChangeEditEdgeThr)
	ON_EN_CHANGE(IDC_EDIT_STAIN_ROIRADIUS, &CParticle_Option_LG::OnEnChangeEditStainRoiradius)
	ON_EN_CHANGE(IDC_EDIT_BlockWidth, &CParticle_Option_LG::OnEnChangeEditBlockwidth)
	ON_BN_CLICKED(IDC_BUTTON_STAIN_SAVE, &CParticle_Option_LG::OnBnClickedButtonStainSave)
	ON_CBN_SELCHANGE(IDC_COMBO_TESTMODE, &CParticle_Option_LG::OnCbnSelchangeComboTestmode)
	ON_BN_CLICKED(IDC_CHECK_DEBUG, &CParticle_Option_LG::OnBnClickedCheckDebug)
	ON_WM_SHOWWINDOW()
	ON_EN_SETFOCUS(IDC_EDIT_MODE_STAT_LG, &CParticle_Option_LG::OnEnSetfocusEditModeStatLg)
END_MESSAGE_MAP()


// CParticle_Option_LG 메시지 처리기입니다.
void CParticle_Option_LG::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CParticle_Option_LG::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO      = INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
	strTitle.Empty();
	strTitle="PARTICLE_LG_";
}

void CParticle_Option_LG::Pic(CDC *cdc)
{
	ParticlePic(cdc,0);
}
  

void CParticle_Option_LG::InitPrm()
{
	UpdateData(TRUE);
	Load_parameter();
	UpdateData(FALSE);
	/*for(int t=0; t<255;t++){
		P_RECT[t].m_Left =0;
		P_RECT[t].m_Right =0;
		P_RECT[t].m_Top =0;
		P_RECT[t].m_Bottom =0;
		P_RECT[t].m_PosX =0;
		P_RECT[t].m_PosY =0;
	}

	R_RECT[0].m_Success = FALSE;
	ParticleCnt = 0;*/

// 	if(m_Stain != NULL){
// 		delete m_Stain;
// 		m_Stain = NULL;
// 	}
// 	if(m_BlackSpot != NULL){
// 		delete m_BlackSpot;
// 		m_BlackSpot = NULL;
// 	}

	//((CButton *)GetDlgItem(IDC_BUTTON_P_SAVE))->EnableWindow(0);
}


void CParticle_Option_LG::TESTMODE_NAME(bool MODE){
	CString str ="";
	if(MODE == 0){
		str.Format("자동 검사 모드");
		((CComboBox *)GetDlgItem(IDC_COMBO_TESTMODE))->EnableWindow(FALSE);
	}else{
		str.Format("검사 확인 모드");
		((CComboBox *)GetDlgItem(IDC_COMBO_TESTMODE))->EnableWindow(TRUE);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_MODE_STAT_LG))->SetWindowText(str);
}
void CParticle_Option_LG::Load_parameter(){

	CString str ="";
	strTitle="PARTICLE_LG_";

	m_uTestMode = GetPrivateProfileInt(str_model,strTitle+"USERTESTMODE",0,R_filename);
	str.Empty();
	str.Format("%d",m_uTestMode);
	WritePrivateProfileString(str_model,strTitle+"USERTESTMODE",str,R_filename);
	
	b_ManualTestMode = GetPrivateProfileInt(str_model,strTitle+"TESTMODE",0,R_filename);
	str.Empty();
	str.Format("%d",b_ManualTestMode);
	WritePrivateProfileString(str_model,strTitle+"TESTMODE",str,R_filename);
	TESTMODE_NAME(b_ManualTestMode);

	b_BlackSpotTESTEn  = GetPrivateProfileInt(str_model,strTitle+"BlackSpotEn",1,R_filename);
	str.Empty();
	str.Format("%d",b_BlackSpotTESTEn );
	WritePrivateProfileString(str_model,strTitle+"BlackSpotEn",str,R_filename);

	b_StainTESTEn   = GetPrivateProfileInt(str_model,strTitle+"StainEn",1,R_filename);
	str.Empty();
	str.Format("%d",b_StainTESTEn);
	WritePrivateProfileString(str_model,strTitle+"StainEn",str,R_filename);

	m_bBlackSpotEn = b_BlackSpotTESTEn;
	m_bSTAINEn = b_StainTESTEn;

	Edit_En_BlackSpot(b_BlackSpotTESTEn);
	Edit_En_Stain(b_StainTESTEn);
	Load_parameter_BlockSpot();
	Load_parameter_Stain();

	BlockspotChangeChk();
	StainChangeChk();
}

void CParticle_Option_LG::Save_parameter(){
	CString str;

	Save_parameter_BlockSpot();
	Save_parameter_Stain();

	strTitle="PARTICLE_LG_";
	str.Empty();
	str.Format("%d",b_BlackSpotTESTEn);
	WritePrivateProfileString(str_model,strTitle+"BlackSpotEn",str,R_filename);

	str.Empty();
	str.Format("%d",b_StainTESTEn);
	WritePrivateProfileString(str_model,strTitle+"StainEn",str,R_filename);
}

void CParticle_Option_LG::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",R_filename);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

void CParticle_Option_LG::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	InsertIndex = m_ParticleList.InsertItem(StartCnt,"",0);
	
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_ParticleList.SetItemText(InsertIndex,0,strCnt);
	
	m_ParticleList.SetItemText(InsertIndex,1,((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_ParticleList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_ParticleList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");
}

void CParticle_Option_LG::FAIL_UPLOAD(){
	if ((((CImageTesterDlg *)m_pMomWnd)->LotMod == 1) && (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)){
		InsertList();
		for(int t=0; t<18; t++){
			m_ParticleList.SetItemText(InsertIndex,4+t,"X");
		}
		m_ParticleList.SetItemText(InsertIndex,22,"FAIL");
		m_ParticleList.SetItemText(InsertIndex,23,"STOP");
		((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_DUST,"STOP");
		StartCnt++;
	}
}

void CParticle_Option_LG::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}

void CParticle_Option_LG::Focus_move_start()
{
	if(((CButton *)GetDlgItem(IDC_BUTTON_P_SAVE))->IsWindowEnabled() == TRUE){
		GotoDlgCtrl(((CButton *)GetDlgItem(IDC_BUTTON_P_SAVE)));
	}else{
		GotoDlgCtrl(((CStatic *)GetDlgItem(IDC_STATIC)));
	}
}



tResultVal CParticle_Option_LG::Run()
{	
	tResultVal retval = {0,};

	Stain_ParticleCnt=0;
	Black_ParticleCnt=0;
	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
		InsertList();
	}

	for(int lop = 0;lop<6;lop++){
		ResultString[lop] = "";
	}

	retval.m_ID = 0x10;
	retval.ValString.Empty();


// 	if(m_Stain != NULL){
// 		delete m_Stain;
// 		m_Stain = NULL;
// 	}
// 	if(m_BlackSpot != NULL){
// 		delete m_BlackSpot;
// 		m_BlackSpot = NULL;
// 	}
	DoEvents(2000);
	if(CaptureImage() == FALSE){
		retval.ValString.Format("영상추출실패");
		retval.m_Success = FALSE;
		((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->TestState(1,"FAIL");
		return retval;//이미지 캡쳐 실패. 
	}

	


	BOOL STATBS = TRUE;
	BOOL STATST2 = TRUE;
	m_STATBS = 0;
	m_STATST2 = 0;
	m_ST2DefectCount =0;
	m_BSDefectCount =0;

	m_bBlackSpotEn = b_BlackSpotTESTEn;
	m_bSTAINEn = b_StainTESTEn;
	if((m_bBlackSpotEn == FALSE)&&(m_bSTAINEn == FALSE)){
		
	}
	

	if(m_bBlackSpotEn == TRUE){
		STATBS = InspectionBlackSpot(m_pYOut,CAM_IMAGE_WIDTH,CAM_IMAGE_HEIGHT);
		m_STATBS = STATBS +1;
	}

	if(m_bSTAINEn == TRUE){
		STATST2 = InspectionStain2(m_pYOut,CAM_IMAGE_WIDTH,CAM_IMAGE_HEIGHT);
		m_STATST2 = STATST2 +1;
	}
	DoEvents(1000);
	((CImageTesterDlg *)m_pMomWnd)->str_pCsvdata = CSVMODELGEN();

	if((STATBS == TRUE)&&(STATST2 == TRUE)){
		retval.ValString.Format("OK");
		retval.m_Success = TRUE;
		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			m_ParticleList.SetItemText(InsertIndex,22,"PASS");
		}
	}else{
		retval.ValString.Format("이물 있음");
		retval.m_Success = FALSE;
		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			m_ParticleList.SetItemText(InsertIndex,22,"FAIL");
		}
	}
	if((b_ManualTestMode == 1)/*&&(R_RECT[0].m_Success ==TRUE)*/){
		if(m_uTestMode == 2){
			((CImageTesterDlg *)m_pMomWnd)->m_ParticleView = NULL;
			((CImageTesterDlg *)m_pMomWnd)->b_Particle =TRUE;
			((CImageTesterDlg *)m_pMomWnd)->m_ParticleView = new CParticle_View(((CImageTesterDlg *)m_pMomWnd));
			((CImageTesterDlg *)m_pMomWnd)->m_ParticleView->Setup(((CImageTesterDlg *)m_pMomWnd));
			if(((CImageTesterDlg *)m_pMomWnd)->m_ParticleView->DoModal() == IDOK){
				retval.m_Success = TRUE;
				retval.ValString.Format("유저 확인 PASS");
				if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
					m_ParticleList.SetItemText(InsertIndex,22,"PASS");
				}
			}else{
				retval.m_Success = FALSE;
				retval.ValString.Format("유저 확인 FAIL");
				if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
					m_ParticleList.SetItemText(InsertIndex,22,"FAIL");
				}
			}
			delete ((CImageTesterDlg  *)m_pMomWnd)->m_ParticleView;
			((CImageTesterDlg  *)m_pMomWnd)->m_ParticleView =NULL;
		}else if((retval.m_Success == TRUE)&&(m_uTestMode == 1)){
			((CImageTesterDlg *)m_pMomWnd)->CParticleViewLG_Show(0);
		}else{
			//((CImageTesterDlg *)m_pMomWnd)->NameUpdate(LPCSTR Name,LPCSTR Path)
			((CImageTesterDlg *)m_pMomWnd)->CParticleViewLG_Show(1);
			((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
			while(((CImageTesterDlg *)m_pMomWnd)->m_ParticleViewLG->IsWindowVisible() == TRUE){
				DoEvents(100);
			}
		}
	}else{
		((CImageTesterDlg *)m_pMomWnd)->CParticleViewLG_Show(0);
	}

	int buf_ST2DefectCount,buf_BSDefectCount;
	if(m_ST2DefectCount == -1){
		buf_ST2DefectCount = 0;
	}else{
		buf_ST2DefectCount = m_ST2DefectCount;
	}

	if(m_BSDefectCount == -1){
		buf_BSDefectCount = 0;
	}else{
		buf_BSDefectCount = m_BSDefectCount;
	}

	ParticleCnt =( buf_ST2DefectCount + buf_BSDefectCount);

	CString str;
	str.Format("%d",ParticleCnt);
	((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptLGWnd->PARTICLENUM_TEXT(str);

	if(retval.m_Success == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptLGWnd->RESULT_TEXT(1,"PASS");
		((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->TestState(0,"PASS");
		//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_DUST,"PASS");
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptLGWnd->RESULT_TEXT(2,"FAIL");
		((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->TestState(1,"FAIL");
		//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_DUST,"FAIL");
	}

// 	if(m_Stain != NULL){
// 		delete m_Stain;
// 		m_Stain = NULL;
// 	}
// 	if(m_BlackSpot != NULL){
// 		delete m_BlackSpot;
// 		m_BlackSpot = NULL;
// 	}
	((CImageTesterDlg *)m_pMomWnd)->str_pCsvdata = "";
	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE)
	{
		if(retval.m_Success == TRUE)
		{
			((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_DUST,"PASS");
		}
		else
		{
			((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_DUST,"FAIL");
		}
		StartCnt++;
	}
	return retval;
}



CString CParticle_Option_LG::CSVMODELGEN()
{
	CString strOut ="";
	CString str = "";

	strOut.Empty();

	str.Empty();
	str= L"TESTMODE ,";
	strOut+= str;
	str.Empty();
	str=L"NO ,";
	strOut+= str;
	str.Empty();
	str=L"X ,";
	strOut+= str;
	str.Empty();
	str=L"Y ,";
	strOut+= str;
	str=L"RESULT \n";
	strOut+= str;
	
// 	if(m_bBlackSpotEn == TRUE){	
// 		if( m_BSDefectCount > 0 )
// 		{
// 			for( int i = 0 ; i < (int)m_BlackSpot->GetDefectBlobCount() ; i++ )
// 			{
// 				str.Empty();
// 				str= L"BLACKSPOT ,";
// 				strOut+= str;
// 				str.Empty();
// 				str.Format("%d ,",i+1);
// 				strOut+= str;
// 				const RECT* rt = m_BlackSpot->GetDefectBlobRect(i);	
// 				str.Empty();
// 				str.Format("%d ,",rt->left);
// 				strOut+= str;
// 				str.Empty();
// 				str.Format("%d ,",rt->top);
// 				strOut+= str;
// 				str.Empty();
// 				str.Format("%d \n",m_BlackSpot->GetDefectResult(EIMAGEREGION_CENTER,i)->nSize);
// 				strOut+= str;
// 			}
// 		}
// 	}
// 
// 	if(m_bSTAINEn == TRUE){
// 		int nCenterCount = m_Stain->GetDefectCount(EIMAGEREGION_CENTER);
// 		for( int i = 0 ; i < nCenterCount ; i++ )
// 		{
// 			stain = m_Stain->GetDefectResult(EIMAGEREGION_CENTER, i);
// 
// 			str.Empty();
// 			str= L"STAIN_CENTER ,";
// 			strOut+= str;
// 			str.Empty();
// 			str.Format("%d ,",i+1);
// 			strOut+= str;
// 			str.Empty();
// 			str.Format("%d ,",stain->ptPos.x);
// 			strOut+= str;
// 			str.Empty();
// 			str.Format("%d ,",stain->ptPos.y);
// 			strOut+= str;
// 			str.Empty();
// 			str.Format("%.2f \n",stain->dValue);
// 			strOut+= str;
// 		}
// 
// 		int nEdgeCount = m_Stain->GetDefectCount(EIMAGEREGION_EDGE);
// 		for( int i = 0 ; i < nEdgeCount ; i++ )
// 		{
// 			stain = m_Stain->GetDefectResult(EIMAGEREGION_EDGE, i);
// 			str.Empty();
// 			str= L"STAIN_EDGE ,";
// 			strOut+= str;
// 			str.Empty();
// 			str.Format("%d ,",i+1);
// 			strOut+= str;
// 			str.Empty();
// 			str.Format("%d ,",stain->ptPos.x);
// 			strOut+= str;
// 			str.Empty();
// 			str.Format("%d ,",stain->ptPos.y);
// 			strOut+= str;
// 			str.Empty();
// 			str.Format("%.2f \n",stain->dValue);
// 			strOut+= str;
// 		}
// 	}
	return strOut;
}



void CParticle_Option_LG::CdcDrawText(CDC *cdc,int x,int y,COLORREF rgb,int fontsize,LPCSTR format, ... )
{
	CFont font;
	font.CreatePointFont(fontsize, "Arial"); 
	CString TEXTDATA = format;
	cdc->SetTextAlign(TA_LEFT|TA_BASELINE);
	cdc->SelectObject(&font);
	cdc->SetTextColor(rgb);
	cdc->TextOut(x , y,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
	font.DeleteObject();
}

void CParticle_Option_LG::CdcDrawRect(CDC *cdc,int x,int y,COLORREF rgb,int wegih,int height,int mode)//사각형을 그린다. 0:x,y가 중심;1:x,y가 시작 2:x
{
	CPen	my_Pan;
	my_Pan.CreatePen(PS_SOLID,2,rgb);
	cdc->SelectObject(&my_Pan);

	int m_Left,m_Right,m_Top,m_Bottom;
	if((mode == 0)||(mode == 2)){
		m_Left = x - (wegih/2);
		m_Right = x + (wegih/2);
		m_Top = y-(height/2);
		m_Bottom = y+(height/2);
	}else{
		m_Left = x;
		m_Right = x + wegih;
		m_Top = y;
		m_Bottom = y + height;
	}

	if(mode == 2){//x표를 그린다. 
		cdc->MoveTo(m_Left,m_Top);
		cdc->LineTo(m_Right,m_Bottom);
		cdc->MoveTo(m_Right,m_Top);
		cdc->LineTo(m_Left,m_Bottom);
	}else{//사각형을 그린다. 
		cdc->MoveTo(m_Left,m_Top);
		cdc->LineTo(m_Right,m_Top);
		cdc->LineTo(m_Right,m_Bottom);
		cdc->LineTo(m_Left,m_Bottom);
		cdc->LineTo(m_Left,m_Top);
	}
	my_Pan.DeleteObject();
}

void CParticle_Option_LG::CdcDrawCircle(CDC *cdc,int x,int y,COLORREF rgb,int radio,int size,int mode)//원을 그린다. 0:x,y가 중심;1:x,y가 시작 2:x
{
	CPen	my_Pan;
	my_Pan.CreatePen(PS_SOLID,size,rgb);
	cdc->SelectObject(&my_Pan);
	cdc->SelectStockObject(HOLLOW_BRUSH);
	int m_Left,m_Right,m_Top,m_Bottom;

	if(mode == 0){
		m_Left = x - radio;
		m_Right = x + radio;
		m_Top = y- radio;
		m_Bottom = y+radio;
	}else{
		m_Left = x;
		m_Right = x + radio*2;
		m_Top = y;
		m_Bottom = y + radio*2;
	}

	cdc->Ellipse(m_Left,m_Top,m_Right,m_Bottom);
	my_Pan.DeleteObject();
}


BOOL CParticle_Option_LG::ParticlePic(CDC *cdc,int NUM){

	//if(EIJARect.Chkdata() == FALSE){return 0;}//재대로 된 데이터가 아니면 바로 빠져나간다.
	
	CPen	my_Pan,*old_pan;
	CString TEXTDATA ="";
	::SetBkMode(cdc->m_hDC,TRANSPARENT);	
// 	if(m_bSTAINEn == TRUE){
// 		CdcDrawCircle(cdc,CAM_IMAGE_WIDTH/2,CAM_IMAGE_HEIGHT/2,MCYAN,Stain2Config.ROIRadius,4,0);
// 	}
	CString str = "";
// 	if(m_bBlackSpotEn == TRUE){
// 		CdcDrawCircle(cdc,CAM_IMAGE_WIDTH/2,CAM_IMAGE_HEIGHT/2,YELLOW_COLOR,BlackSpotConfig.ROIRadius,3,0);
// 		if(m_BlackSpot != NULL){
// 			if( m_BSDefectCount > 0 )
// 			{
// 				for( int i = 0 ; i < (int)m_BlackSpot->GetDefectBlobCount() ; i++ )
// 				{
// 					const RECT* rt = m_BlackSpot->GetDefectBlobRect(i);	
// 					CdcDrawRect(cdc,rt->left,rt->top,RED_COLOR,rt->right - rt->left,rt->bottom - rt->top,1);
// 					str.Format("%d",m_BlackSpot->GetDefectResult(EIMAGEREGION_CENTER,i)->nSize);
// 					CdcDrawText(cdc,rt->left,rt->top -5,RED_COLOR,60,str);
// 				}
// 			}
// 		}
// 	}

// 	if(m_bSTAINEn == TRUE){
// 		if(m_Stain != NULL){
// 			int nCenterCount = m_Stain->GetDefectCount(EIMAGEREGION_CENTER);
// 			for( int i = 0 ; i < nCenterCount ; i++ )
// 			{
// 				stain = m_Stain->GetDefectResult(EIMAGEREGION_CENTER, i);
// 				CdcDrawRect(cdc,stain->ptPos.x,stain->ptPos.y,MCYAN,stain->nSize,stain->nSize,0);
// 				str.Format("%.2f",stain->dValue);
// 				CdcDrawText(cdc,stain->ptPos.x,stain->ptPos.y -5,MCYAN,60,str);
// 			}
// 
// 			int nEdgeCount = m_Stain->GetDefectCount(EIMAGEREGION_EDGE);
// 			for( int i = 0 ; i < nEdgeCount ; i++ )
// 			{
// 				stain = m_Stain->GetDefectResult(EIMAGEREGION_EDGE, i);
// 				CdcDrawRect(cdc,stain->ptPos.x,stain->ptPos.y,YELLOW_COLOR,stain->nSize,stain->nSize,0);
// 				str.Format("%.2f",stain->dValue);
// 				CdcDrawText(cdc,stain->ptPos.x,stain->ptPos.y -5,YELLOW_COLOR,60,str);
// 			}
// 		}
// 	}


	
	
	int leng = 0;
	for(int lop = 0;lop<6;lop++){
		if(ResultString[lop] != ""){
			leng += 25;
			if(lop < 2){
				if(m_STATBS == 1){
					CdcDrawText(cdc,10,leng,RED_DK_COLOR,100,ResultString[lop]);
				}else{
					CdcDrawText(cdc,10,leng,BLUE_DK_COLOR,100,ResultString[lop]);
				}
			}else{
				if(m_STATST2 == 1){
					CdcDrawText(cdc,10,leng,RED_DK_COLOR,100,ResultString[lop]);
				}else{
					CdcDrawText(cdc,10,leng,BLUE_DK_COLOR,100,ResultString[lop]);
				}
			}
		}
	}
	
	//if(R_RECT[0].m_Success == FALSE){
	//	my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
	//	old_pan = cdc->SelectObject(&my_Pan);
	//	cdc->SetTextColor(WHITE_COLOR);

	//	cdc->MoveTo(P_RECT[NUM].m_Left,P_RECT[NUM].m_Top);
	//	cdc->LineTo(P_RECT[NUM].m_Right,P_RECT[NUM].m_Top);
	//	cdc->LineTo(P_RECT[NUM].m_Right,P_RECT[NUM].m_Bottom);
	//	cdc->LineTo(P_RECT[NUM].m_Left,P_RECT[NUM].m_Bottom);
	//	cdc->LineTo(P_RECT[NUM].m_Left,P_RECT[NUM].m_Top);

	//	my_Pan.DeleteObject();
	//}
	//
	//
	//my_Pan.CreatePen(PS_SOLID,2,WHITE_COLOR);
	//old_pan = cdc->SelectObject(&my_Pan);
	//cdc->SetTextColor(WHITE_COLOR);

	//
	//cdc->MoveTo(FULL_RECT[0].m_Left,FULL_RECT[0].m_Top);
	//cdc->LineTo(FULL_RECT[0].m_Right,FULL_RECT[0].m_Top);
	//cdc->LineTo(FULL_RECT[0].m_Right,FULL_RECT[0].m_Bottom);
	//cdc->LineTo(FULL_RECT[0].m_Left,FULL_RECT[0].m_Bottom);
	//cdc->LineTo(FULL_RECT[0].m_Left,FULL_RECT[0].m_Top);
	//my_Pan.DeleteObject();


	//my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
	//old_pan = cdc->SelectObject(&my_Pan);
	//if(m_Ellipse == 1){
	//
	//	CBrush oldBrush;
	//	oldBrush.CreateStockObject(NULL_BRUSH);
	//	CBrush *poldBrush = cdc->SelectObject(&oldBrush);
	//	cdc->Ellipse(R_RECT[0].m_Left,R_RECT[0].m_Top,R_RECT[0].m_Right,R_RECT[0].m_Bottom);
	//	
	//	cdc->SelectObject(old_pan);
	//	cdc->SelectObject(oldBrush);
	//}
	//
	//else{
	//	old_pan = cdc->SelectObject(&my_Pan);
	//	//cdc->SetBkMode(TRANSPARENT);
	//	cdc->SetTextColor(WHITE_COLOR);

	//	cdc->MoveTo(R_RECT[0].m_Left,R_RECT[0].m_Top);
	//	cdc->LineTo(R_RECT[0].m_Right,R_RECT[0].m_Top);
	//	cdc->LineTo(R_RECT[0].m_Right,R_RECT[0].m_Bottom);
	//	cdc->LineTo(R_RECT[0].m_Left,R_RECT[0].m_Bottom);
	//	cdc->LineTo(R_RECT[0].m_Left,R_RECT[0].m_Top);

	//}

	////my_Pan.CreatePen(PS_SOLID,2,WHITE_COLOR);
	////cdc->MoveTo(FULL_RECT[0].m_Left,FULL_RECT[0].m_Top);
	////cdc->LineTo(FULL_RECT[0].m_Right,FULL_RECT[0].m_Top);
	////cdc->LineTo(FULL_RECT[0].m_Right,FULL_RECT[0].m_Bottom);
	////cdc->LineTo(FULL_RECT[0].m_Left,FULL_RECT[0].m_Bottom);
	////cdc->LineTo(FULL_RECT[0].m_Left,FULL_RECT[0].m_Top);
	////cdc->SelectObject(old_pan);
	//	
	//my_Pan.DeleteObject();

	//cdc->SelectObject(old_pan);
	return 1;
}

BOOL CParticle_Option_LG::CaptureImage(){
	if(!((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK()){
		return FALSE;
	}

	((CImageTesterDlg *)m_pMomWnd)->m_YUVSCAN = TRUE;
		
	for(int i =0;i<10;i++){
		if(((CImageTesterDlg *)m_pMomWnd)->m_YUVSCAN == FALSE){
			break;
		}else{
			DoEvents(50);
		}
	}

	DWORD	dwk = 0,dwi = 0,dwnx = 0;
	DWORD	dwd=691200;

	if(((CImageTesterDlg *)m_pMomWnd)->m_YUVSCAN == FALSE){
		dwd=0;
		for (dwk=0; dwk<691200; dwk+=4)//khk
			{	
			dwd = dwk/2;
			m_pYOut[dwd]= m_YUVIn[dwk+1];//y0
			m_pYOut[dwd+1]= m_YUVIn[dwk+3];//y1

			m_YUVOut[dwk+1]= m_YUVIn[dwk];//cb
			m_YUVOut[dwk]= m_YUVIn[dwk+1];//y0
			m_YUVOut[dwk+3]= m_YUVIn[dwk+2];//cr
			m_YUVOut[dwk+2]= m_YUVIn[dwk+3];//y1
		}
		return TRUE;
	}else{
		return FALSE;
	}
	
}



BOOL CParticle_Option_LG::InspectionBlackSpot(BYTE *m_pFrame8BitBuffer, int width, int height)
{
// 	if(m_BlackSpot != NULL){
// 		delete m_BlackSpot;
// 		m_BlackSpot = NULL;
// 	}
// 	m_BlackSpot = new CBlemish;
	CString str;


	const BYTE* pRaw = (BYTE*)m_pFrame8BitBuffer;
	int nWidth = width;
	int nHeight = height;
	
	//blackspot관련 설정값 옵션처리해야 함.
	/*BlackSpotConfig.nBlockWidth = 16;
	BlackSpotConfig.nBlockHeight = 16;
	BlackSpotConfig.dDefectRatio = 0.1;
	BlackSpotConfig.nClusterSize = 5;
	BlackSpotConfig.nDefectInCluster = 1;
	BlackSpotConfig.nMaxSingleDefectNum = 1000;
	BlackSpotConfig.ROIRadius = 550;*/


	//g_OvrDisp.DrawCircle(width/2, height/2, Config.ROIRadius,MCYAN); //화면에 표시

	// Pre-processing
	// 1. AE/AWB completed
	// 2. LSC(optional)
	//if(stSpecCommon.bEnableLSC==TRUE)
	//	xLensShadingCorrection(pRaw, nWidth, nHeight, stSpecCommon.nLensShadingScaleLevel );

	// Inspection
	//	int nSingleDefectCount = m_BlackSpot->MakeDefectiveMap(const_cast<BYTE*>(pRaw), nWidth, nHeight, nBlockWidth, dDefectRatio, nClusterSize, static_cast<CAtomImageBlemish::EBlemishMethod>(nFindDPMethod));
	
// 	m_BSDefectCount = m_BlackSpot->Inspect(const_cast<BYTE*>(pRaw), nWidth, nHeight, BlackSpotConfig );
// 	m_BSSingleDefectCount = m_BlackSpot->GetSingleDefectCount();

	//---- Log 이부분을 남겨둘지 나중으로 할지 나중에 판단한다. 
	ResultString[0].Format(_T("[BlackSpot]DefectCount=%d, SingleDefectNum=%d"), m_BSDefectCount, m_BSSingleDefectCount );
	((CImageTesterDlg *)m_pMomWnd)->StatePrintf(ResultString[0]);
	//----

	//Black_ParticleCnt += m_BSDefectCount;
	//Black_ParticleCnt += m_BSSingleDefectCount;
	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
		str.Format("%d",m_BSDefectCount);
		m_ParticleList.SetItemText(InsertIndex,4,str);
		str.Format("%d",m_BSSingleDefectCount);
		m_ParticleList.SetItemText(InsertIndex,5,str);
	}

	// Judge
// 	if( m_BSSingleDefectCount > BlackSpotConfig.nMaxSingleDefectNum ){
// 		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
// 			m_ParticleList.SetItemText(InsertIndex,9,"FAIL");
// 		}
// 		str.Format("%d",m_BSDefectCount);
// 		((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptLGWnd->PARTICLENUM_TEXT2(str);
// 		((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptLGWnd->RESULT_TEXT2(2,"FAIL");
// 		return FALSE;	// NOISE
// 	}

	// Display Graphic
// 	int nCenterCount = m_BlackSpot->GetDefectCount(EIMAGEREGION_CENTER);
// 	for( int i = 0 ; i < nCenterCount ; i++ )
// 	{
// 		blackSpot = m_BlackSpot->GetDefectResult(EIMAGEREGION_CENTER, i);
// 	}
// 
// 	blackSpot = m_BlackSpot->GetMaxDefectResult(EIMAGEREGION_CENTER);
// 	ResultString[1].Format(_T("[BlackSpot]MaxValue=%.02f, PosX=%d, PosY=%d") ,blackSpot->dValue, blackSpot->ptPos.x, blackSpot->ptPos.y);
	((CImageTesterDlg *)m_pMomWnd)->StatePrintf(ResultString[1]);

	//g_OvrDisp.DrawText(10,10,MGREEN,40,strTemp);//화면에 표시


	////-----------
// 	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
// 		
// 		str.Format("%.02f",blackSpot->dValue);
// 		m_ParticleList.SetItemText(InsertIndex,6,str);
// 		str.Format("%d",blackSpot->ptPos.x);
// 		m_ParticleList.SetItemText(InsertIndex,7,str);
// 		str.Format("%d",blackSpot->ptPos.y);
// 		m_ParticleList.SetItemText(InsertIndex,8,str);
// 
// 	}




	///----------

	str.Format("%d",m_BSDefectCount);
	((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptLGWnd->PARTICLENUM_TEXT2(str);
	if( m_BSDefectCount > 0 )
	{
		// rectangles at stain positions
		//for( int i = 0 ; i < (int)m_BlackSpot.GetDefectBlobCount() ; i++ )
		//{
		//	const RECT* rt = m_BlackSpot.GetDefectBlobRect(i);			
		////	g_OvrDisp.DrawRect(rt->left, rt->top, rt->right, rt->bottom, MRED, 50, "%.2f" , m_BlackSpot.GetDefectResult(EIMAGEREGION_CENTER,i)->dValue);
		//}
		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
			m_ParticleList.SetItemText(InsertIndex,9,"FAIL");
		}
		((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptLGWnd->RESULT_TEXT2(2,"FAIL");

		return FALSE;
	}
	
	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
		m_ParticleList.SetItemText(InsertIndex,9,"PASS");
	}
	((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptLGWnd->RESULT_TEXT2(1,"PASS");


	return TRUE;

}

BOOL CParticle_Option_LG::InspectionStain2(BYTE *m_pFrame8BitBuffer, int width, int height)
{
	//BYTE* pRaw = (BYTE*)m_YImageObject;
// 	if(m_Stain != NULL){
// 		delete m_Stain;
// 		m_Stain = NULL;
// 	}
// 	m_Stain = new CRU_Ymean;
		CString str;

	BYTE* pRaw = (BYTE*)m_pFrame8BitBuffer;
	int nWidth = width;
	int nHeight = height;

	// Spec.(from UI)
	/*Stain2Config.nEdgeSize = 50;
	Stain2Config.fCenterThreshold = 2.5;
	Stain2Config.fEdgeThreshold = 3.0;
	Stain2Config.nBlockWidth = 16;
	Stain2Config.ROIRadius = 550;*/

//	g_OvrDisp.DrawCircle(width/2, height/2, Stain2Config.ROIRadius,MCYAN);
	
	// Inspection
// 	m_ST2DefectCount = m_Stain->Inspect(pRaw, nWidth, nHeight, Stain2Config);
// 	m_ST2SingleDefectCount = m_Stain->GetSingleDefectCount();
	//AddLog(_T("[RU_Ymean] SingleDefectCount=%d, BlobCount=%d"), nDefectCount, m_pStainYmean->GetDefectBlobCount());

	//ResultString[2].Format(_T("[Stain2]Single Defect Count =%d, BlobCount=%d"), m_ST2SingleDefectCount, m_Stain->GetDefectBlobCount());
	((CImageTesterDlg *)m_pMomWnd)->StatePrintf(ResultString[2]);

	//Stain_ParticleCnt += m_ST2SingleDefectCount;
	//Stain_ParticleCnt += m_Stain->GetDefectBlobCount();

	// Display Graphic
	//TDefectResult stain;
	
	//--------------------------------------------------------------pic으로 넘김 
//	int nCenterCount = m_Stain.GetDefectCount(EIMAGEREGION_CENTER);
//	for( int i = 0 ; i < nCenterCount ; i++ )
//	{
//		stain = m_Stain.GetDefectResult(EIMAGEREGION_CENTER, i);
//		CRectEx rt;
//		rt.SetRectCentered(stain->ptPos.x,stain->ptPos.y, stain->nSize, stain->nSize);
//		g_OvrDisp.DrawRect(rt, MYELLOW, 20, "%.2f" , stain->dValue);
//	}
//
//	int nEdgeCount = m_Stain.GetDefectCount(EIMAGEREGION_EDGE);
//	for( int i = 0 ; i < nEdgeCount ; i++ )
//	{
//		stain = m_Stain.GetDefectResult(EIMAGEREGION_EDGE, i);
//		CRectEx rt;
//		rt.SetRectCentered(stain->ptPos.x,stain->ptPos.y, stain->nSize, stain->nSize);
//		g_OvrDisp.DrawRect(rt, MYELLOW, 20, "%.2f" , stain->dValue);
//	}
	//--------------------------------------------------------------

	// Defect & information
// 	stain = m_Stain->GetMaxDefectResult(EIMAGEREGION_CENTER);
// 	// [Draw]Defect(PDC_SKYBLUE, stain.ptPos, 100 );
// 	ResultString[3].Format(_T("[Stain2]Center Count=%d, MaxValue=%.02f, PosX=%d, PosY=%d"), m_Stain->GetDefectCount(EIMAGEREGION_CENTER),stain->dValue, stain->ptPos.x, stain->ptPos.y);
	((CImageTesterDlg *)m_pMomWnd)->StatePrintf(ResultString[3]);
//	g_OvrDisp.DrawText(10,50,MGREEN,40,strTemp);
	//m_LogListBox.AddString(strTemp);

	//Stain_ParticleCnt += m_Stain->GetDefectCount(EIMAGEREGION_CENTER);

// 	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
// 		str.Format("%d",m_ST2SingleDefectCount);
// 		m_ParticleList.SetItemText(InsertIndex,10,str);
// 		str.Format("%d",m_Stain->GetDefectBlobCount());
// 		m_ParticleList.SetItemText(InsertIndex,11,str);
// 		str.Format("%d",m_Stain->GetDefectCount(EIMAGEREGION_EDGE));
// 		m_ParticleList.SetItemText(InsertIndex,12,str);
// 		str.Format("%.02f",stain->dValue);
// 		m_ParticleList.SetItemText(InsertIndex,13,str);
// 		str.Format("%d",stain->ptPos.x);
// 		m_ParticleList.SetItemText(InsertIndex,14,str);
// 		str.Format("%d",stain->ptPos.y);
// 		m_ParticleList.SetItemText(InsertIndex,15,str);
// 	}
// 
// 	stain = m_Stain->GetMaxDefectResult(EIMAGEREGION_EDGE);
	// [Draw]Defect(PDC_SLATEBLUE, stain.ptPos, 100 );
	//ResultString[4].Format(_T("[Stain2]Edge Count=%d, MaxValue=%.02f, PosX=%d, PosY=%d"), m_Stain->GetDefectCount(EIMAGEREGION_EDGE),stain->dValue, stain->ptPos.x, stain->ptPos.y);
	((CImageTesterDlg *)m_pMomWnd)->StatePrintf(ResultString[4]);
//	g_OvrDisp.DrawText(10,90,MGREEN,40,strTemp);
	//Stain_ParticleCnt += m_Stain->GetDefectCount(EIMAGEREGION_EDGE);

	ResultString[5].Format(_T("[Stain2]Defect Count=%d"), m_ST2DefectCount );
	((CImageTesterDlg *)m_pMomWnd)->StatePrintf(ResultString[5]);
	//Stain_ParticleCnt += m_ST2DefectCount;


// 	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
// 		str.Format("%d",m_Stain->GetDefectCount(EIMAGEREGION_EDGE));
// 		m_ParticleList.SetItemText(InsertIndex,16,str);
// 		str.Format("%.02f",stain->dValue);
// 		m_ParticleList.SetItemText(InsertIndex,17,str);
// 		str.Format("%d",stain->ptPos.x);
// 		m_ParticleList.SetItemText(InsertIndex,18,str);
// 		str.Format("%d",stain->ptPos.y);
// 		m_ParticleList.SetItemText(InsertIndex,19,str);
// 		str.Format("%d",m_ST2DefectCount);
// 		m_ParticleList.SetItemText(InsertIndex,20,str);
// 
// 
// 
// 	}

	if(m_ST2DefectCount == -1){
		str.Format("검출실패");
	}else{
		str.Format("%d",m_ST2DefectCount);
	}
	((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptLGWnd->PARTICLENUM_TEXT3(str);

	if( m_ST2DefectCount > 0 )
	{
		// rectangles at stain positions
		for( int i = 0 ; i < m_ST2DefectCount ; i++ )
		{

		}

		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
			m_ParticleList.SetItemText(InsertIndex,21,"FAIL");
		}

		((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptLGWnd->RESULT_TEXT3(2,"FAIL");

		return FALSE;
	}else if(m_ST2DefectCount == -1){
		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
			m_ParticleList.SetItemText(InsertIndex,21,"FAIL");
		}

		((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptLGWnd->RESULT_TEXT3(2,"FAIL");

		return FALSE;
	}


	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
		m_ParticleList.SetItemText(InsertIndex,21,"PASS");
	}
	((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptLGWnd->RESULT_TEXT3(1,"PASS");

	return TRUE;
}


void CParticle_Option_LG::Set_List(CListCtrl *List)
{

	CString testD[18] = {"[BS]DefectCount","[BS]SingleDefectNum","[BS]MaxValue","[BS]PosX","[BS]PosY","[BS]Result",
							"[ST]SingleDefectCount","[ST]BlobCount","[ST]CenterCount","[ST]MaxValue","[ST]PosX","[ST]PosY"
							,"[ST]EdgeCount","[ST]MaxValue","[ST]PosX","[ST]PosY","[ST]DefectCount","[ST]Result"}; 
	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	for(int t=0; t<18; t++){
		List->InsertColumn(4+t,testD[t],LVCFMT_CENTER, 80);
	}
	List->InsertColumn(22,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(23,"비고",LVCFMT_CENTER, 80);

	ListItemNum=24;
	Copy_List(&m_ParticleList,((CImageTesterDlg *)m_pMomWnd)->m_pWorkList,ListItemNum);
}

CString CParticle_Option_LG::Mes_Result()
{
	CString szResult;
	CString sz[19];
	int		nResult[3];
	
	int		nResultT;
	int		nSel = m_ParticleList.GetItemCount()-1;
	szResult = m_ParticleList.GetItemText(nSel, 22);
	if(szResult.Compare(_T("PASS")) == 0){
		nResultT = 1;
	}else{
		nResultT = 0;
	}
	m_szMesResult.Format("%s:%d",szResult,nResultT);

	//// 제일 마지막 항목을 추가하자
	//int nSel = m_ParticleList.GetItemCount()-1;

	//for(int lop = 0;lop<19;lop++){
	//	sz[lop] = m_ParticleList.GetItemText(nSel, lop+4);
	//}
	//
	//if(sz[5].Compare(_T("PASS")) == 0){
	//	nResult[0] = 1;
	//}else{
	//	nResult[0] = 0;
	//}

	//if(sz[17].Compare(_T("PASS")) == 0){
	//	nResult[1] = 1;
	//}else{
	//	nResult[1] = 0;
	//}

	//if(sz[18].Compare(_T("PASS")) == 0){
	//	nResult[2] = 1;
	//}else{
	//	nResult[2] = 0;
	//}

	//for(int lop = 0;lop<19 ;lop++){
	//	if(sz[lop] == ""){
	//		sz[lop] = 'X';
	//		if(lop == 0){
	//			nResult[0] = 1;
	//		}else if(lop == 6){
	//			nResult[1] = 1;
	//		}else if(lop == 18){
	//			nResult[2] = 1;
	//		}
	//	}	
	//}

	//CString strbuf = "";
	//m_szMesResult = "";
	//int bufnum = 0;
	//for(int lop = 0;lop<19;lop++){
	//	strbuf = "";
	//	if(lop != 0){
	//		m_szMesResult += ",";
	//	}
	//	if(lop < 6){
	//		bufnum = 0;
	//	}else if(lop < 18){
	//		bufnum = 1;
	//	}else{
	//		bufnum = 2;
	//	}
	//	strbuf.Format("%s:%d",sz[lop],nResult[bufnum]);
	//	m_szMesResult += strbuf;
	//}
	return m_szMesResult;
}

void CParticle_Option_LG::EXCEL_SAVE(){
	//CString Item[1]={"갯수"};

	CString Item[18] = {"[BS]DefectCount","[BS]SingleDefectNum","[BS]MaxValue","[BS]PosX","[BS]PosY","[BS]Result",
							"[ST]SingleDefectCount","[ST]BlobCount","[ST]CenterCount","[ST]MaxValue","[ST]PosX","[ST]PosY"
							,"[ST]EdgeCount","[ST]MaxValue","[ST]PosX","[ST]PosY","[ST]DefectCount","[ST]Result"}; 
	CString Data ="";
	CString str="";
	str = "***********************LG이물 검사***********************\n\n";
	Data += str;
	str.Empty();
	str.Format("BlackSpot, BlockWidth,%d, BlockHeight, %d ,DefectRatio,%6.3f,ClusterSize,%d,DefectInCluster,%d,MaxSingleDefectNum,%d,ROIRadius,%d ,\n",i_Block_W,i_Block_H,d_DF_Ratio,i_Cluster_Size,i_DF_inCluster,i_DefectNum,i_RoiRadius);
	Data += str;
	str.Empty();
	str.Format("Stain, EdgeSize,%d,CenterThreshold,%6.3f,EdgeThreshold,%6.3f,BlockWidth,%d,ROIRadius,%d,\n",i_EdgeSize,f_Center_Thr,f_Edge_Thr,i_Stain_Block_Width,i_Stain_ROIRadius);
	Data += str;
	str.Empty();
	str = "*************************************************\n";
	Data += str;
	

	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("LG이물검사",Item,18,&m_ParticleList,Data);
}

bool CParticle_Option_LG::EXCEL_UPLOAD(){
	m_ParticleList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("LG이물검사",&m_ParticleList,1, &StartCnt)==TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return TRUE;
}

#pragma region LOT관련 함수
void CParticle_Option_LG::LOT_Set_List(CListCtrl *List){

	CString testD[18] = {"[BS]DefectCount","[BS]SingleDefectNum","[BS]MaxValue","[BS]PosX","[BS]PosY","[BS]Result",
							"[ST]SingleDefectCount","[ST]BlobCount","[ST]CenterCount","[ST]MaxValue","[ST]PosX","[ST]PosY"
							,"[ST]EdgeCount","[ST]MaxValue","[ST]PosX","[ST]PosY","[ST]DefectCount","[ST]Result"}; 

	while(List->DeleteColumn(0));
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT 명",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);
	for(int t=0; t<18; t++){
		List->InsertColumn(5+t,testD[t],LVCFMT_CENTER, 80);
	}
	List->InsertColumn(23,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(24,"비고",LVCFMT_CENTER, 80);

	
	Lot_InsertNum =25;
	
}

void CParticle_Option_LG::LOT_InsertDataList(){
	CString strCnt="";

	int Index = m_Lot_ParticleList.InsertItem(Lot_StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index+1);
	m_Lot_ParticleList.SetItemText(Index,0,strCnt);

	int CopyIndex = m_ParticleList.GetItemCount()-1;

	int count =0;
	for(int t=0; t< Lot_InsertNum;t++){
		if((t != 2)&&(t!=0)){
			m_Lot_ParticleList.SetItemText(Index,t, m_ParticleList.GetItemText(CopyIndex,count));
			count++;

		}else if(t==0){
			count++;
		}else{
			m_Lot_ParticleList.SetItemText(Index,t,((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;

}
void CParticle_Option_LG::LOT_EXCEL_SAVE(){
	CString Item[18] = {"[BS]DefectCount","[BS]SingleDefectNum","[BS]MaxValue","[BS]PosX","[BS]PosY","[BS]Result",
						"[ST]SingleDefectCount","[ST]BlobCount","[ST]CenterCount","[ST]MaxValue","[ST]PosX","[ST]PosY"
						,"[ST]EdgeCount","[ST]MaxValue","[ST]PosX","[ST]PosY","[ST]DefectCount","[ST]Result"}; 
	CString Data ="";
	CString str="";
	str = "***********************LG이물 검사***********************\n\n";
	Data += str;
	str.Empty();
	str.Format("BlackSpot, BlockWidth,%d, BlockHeight, %d ,DefectRatio,%6.3f,ClusterSize,%d,DefectInCluster,%d,MaxSingleDefectNum,%d,ROIRadius,%d ,\n",i_Block_W,i_Block_H,d_DF_Ratio,i_Cluster_Size,i_DF_inCluster,i_DefectNum,i_RoiRadius);
	Data += str;
	str.Empty();
	str.Format("Stain, EdgeSize,%d,CenterThreshold,%6.3f,EdgeThreshold,%6.3f,BlockWidth,%d,ROIRadius,%d,\n",i_EdgeSize,f_Center_Thr,f_Edge_Thr,i_Stain_Block_Width,i_Stain_ROIRadius);
	Data += str;
	str.Empty();
	str = "*************************************************\n";
	Data += str;
	
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("LG이물검사",Item,18,&m_Lot_ParticleList,Data);
}

bool CParticle_Option_LG::LOT_EXCEL_UPLOAD(){
	m_Lot_ParticleList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("LG이물검사",&m_Lot_ParticleList,1,&Lot_StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}

#pragma endregion 
void CParticle_Option_LG::OnBnClickedCheckManualtestLg()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str;
	/*if(b_ManualTestMode == 0){
		b_ManualTestMode =1;
	}else{
		b_ManualTestMode =0;
	}
	UpdateData(FALSE);*/
	UpdateData(TRUE);
	TESTMODE_NAME(b_ManualTestMode);
	strTitle="PARTICLE_LG_";
	str.Empty();
	str.Format("%d",b_ManualTestMode);
	WritePrivateProfileString(str_model,strTitle+"TESTMODE",str,R_filename);
}


BOOL CParticle_Option_LG::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	Font_Size_Change(IDC_EDIT_MODE_STAT_LG ,&ft1,40,28);
	Font_Size_Change(IDC_CHECK_Black_Spot ,&ft2,40,20);
	GetDlgItem(IDC_CHECK_StainTEST)->SetFont(&ft2);



	R_filename=((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;

	UpdateData(TRUE);
	Load_parameter();
	UpdateData(FALSE);

	Set_List(&m_ParticleList);
	LOT_Set_List(&m_Lot_ParticleList);

	InitEVMS();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CParticle_Option_LG::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CComboBox *)GetDlgItem(IDC_COMBO_TESTMODE))->EnableWindow(0);

		((CButton *)GetDlgItem(IDC_CHECK_MANUALTEST_LG))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_Black_Spot))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_BlockSpot_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_StainTEST))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_STAIN_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_DEBUG))->EnableWindow(0);

		((CEdit *)GetDlgItem(IDC_EDIT_Black_W))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_Black_H))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_DF_Ratio))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_ClusterSize))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_DefectInCluster))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_DefectNum))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_ROIRadius))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_EdgeSize))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_Center_Thr))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_Edge_Thr))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_BlockWidth))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_STAIN_ROIRADIUS))->EnableWindow(0);
	}
}

HBRUSH CParticle_Option_LG::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_MODE_STAT_LG){
		pDC->SetTextColor(RGB(255, 255, 255));
		// 텍스트의 배경색상을 설정한다.
		//pDC->SetBkColor(RGB(138, 75, 36));
		pDC->SetBkColor(RGB(86,86,86));
		// 바탕색상을 설정한다.
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


void CParticle_Option_LG::OnBnClickedButtonBlockspotSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	
	i_Block_W = atoi(str_Block_W);
	i_Block_H = atoi(str_Block_H);
	d_DF_Ratio = atof(str_DF_Ratio);
	i_Cluster_Size = atoi(str_Cluster_Size);
	i_DF_inCluster= atoi(str_DF_inCluster);
	i_DefectNum= atoi(str_DefectNum);
	i_RoiRadius= atoi(str_RoiRadius);

	str_Block_W.Format("%d",i_Block_W);
	str_Block_H.Format("%d",i_Block_H);
	str_DF_Ratio.Format("%6.3f",d_DF_Ratio);
	str_Cluster_Size.Format("%d",i_Cluster_Size);
	str_DF_inCluster.Format("%d",i_DF_inCluster);
	str_DefectNum.Format("%d",i_DefectNum);
	str_RoiRadius.Format("%d",i_RoiRadius);

	UpdateData(FALSE);
	Save_parameter_BlockSpot();
	InputData_BlockSpot();
	BlockspotChangeChk();
}

void CParticle_Option_LG::BlockspotChangeChk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	
	int ibuf_Block_W = atoi(str_Block_W);
	int ibuf_Block_H = atoi(str_Block_H);
	double dbuf_DF_Ratio = atof(str_DF_Ratio);
	int ibuf_Cluster_Size = atoi(str_Cluster_Size);
	int ibuf_DF_inCluster= atoi(str_DF_inCluster);
	int ibuf_DefectNum= atoi(str_DefectNum);
	int ibuf_RoiRadius= atoi(str_RoiRadius);

	if( (ibuf_Block_W != i_Block_W)||
		(ibuf_Block_H != i_Block_H)||
		(dbuf_DF_Ratio != d_DF_Ratio)||
		(ibuf_Cluster_Size != i_Cluster_Size)||
		(ibuf_DF_inCluster != i_DF_inCluster)||
		(ibuf_DefectNum != i_DefectNum)||
		(ibuf_RoiRadius != i_RoiRadius)){
			if(b_BlackSpotTESTEn == TRUE){
				((CButton *)GetDlgItem(IDC_BUTTON_BlockSpot_SAVE))->EnableWindow(1);
			}else{
				((CButton *)GetDlgItem(IDC_BUTTON_BlockSpot_SAVE))->EnableWindow(0);
			}
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_BlockSpot_SAVE))->EnableWindow(0);
	}
}

void CParticle_Option_LG::InputData_BlockSpot(){

	str_Block_W.Format("%d",i_Block_W);
	str_Block_H.Format("%d",i_Block_H);
	str_DF_Ratio.Format("%6.3f",d_DF_Ratio);
	str_Cluster_Size.Format("%d",i_Cluster_Size);
	str_DF_inCluster.Format("%d",i_DF_inCluster);
	str_DefectNum.Format("%d",i_DefectNum);
	str_RoiRadius.Format("%d",i_RoiRadius);


// 	BlackSpotConfig.nBlockWidth = i_Block_W;
// 	BlackSpotConfig.nBlockHeight = i_Block_H;
// 	BlackSpotConfig.dDefectRatio = d_DF_Ratio;
// 	BlackSpotConfig.nClusterSize = i_Cluster_Size;
// 	BlackSpotConfig.nDefectInCluster = i_DF_inCluster;
// 	BlackSpotConfig.nMaxSingleDefectNum = i_DefectNum;
// 	BlackSpotConfig.ROIRadius = i_RoiRadius;

	UpdateData(FALSE);

}

void CParticle_Option_LG::Save_parameter_BlockSpot(){

	CString str = "";

	strTitle="PARTICLE_LG_";
	str.Empty();
	str.Format("%d",i_Block_W);
	WritePrivateProfileString(str_model,strTitle+"Black_W",str,R_filename);
	str.Empty();
	str.Format("%d",i_Block_H);
	WritePrivateProfileString(str_model,strTitle+"Black_H",str,R_filename);
	str.Empty();
	str.Format("%6.3f",d_DF_Ratio);
	WritePrivateProfileString(str_model,strTitle+"d_DF_Ratio",str,R_filename);

	str.Empty();
	str.Format("%d",i_Cluster_Size);
	WritePrivateProfileString(str_model,strTitle+"i_Cluster_Size",str,R_filename);

	str.Empty();
	str.Format("%d",i_DF_inCluster);
	WritePrivateProfileString(str_model,strTitle+"i_DF_inCluster",str,R_filename);

	str.Empty();
	str.Format("%d",i_DefectNum);
	WritePrivateProfileString(str_model,strTitle+"i_DefectNum",str,R_filename);

	str.Empty();
	str.Format("%d",i_RoiRadius);
	WritePrivateProfileString(str_model,strTitle+"i_RoiRadius",str,R_filename);

}

void CParticle_Option_LG::Load_parameter_BlockSpot(){

	CString str = "";

	strTitle="PARTICLE_LG_";

	i_Block_W = GetPrivateProfileInt(str_model,strTitle+"Black_W",-9999,R_filename);

	if(i_Block_W == -9999){
		i_Block_W = 16;
		str.Empty();
		str.Format("%d",i_Block_W);
		WritePrivateProfileString(str_model,strTitle+"Black_W",str,R_filename);
	}

	i_Block_H = GetPrivateProfileInt(str_model,strTitle+"Black_H",-9999,R_filename);

	if(i_Block_H == -9999){
		i_Block_H = 16;
		str.Empty();
		str.Format("%d",i_Block_H);
		WritePrivateProfileString(str_model,strTitle+"Black_H",str,R_filename);
	}


	d_DF_Ratio = GetPrivateProfileDouble(str_model,strTitle+"d_DF_Ratio",-9999,R_filename);

	if(d_DF_Ratio == -9999){
		d_DF_Ratio = 0.2;
		str.Empty();
		str.Format("%6.3f",d_DF_Ratio);
		WritePrivateProfileString(str_model,strTitle+"d_DF_Ratio",str,R_filename);
	}

	i_Cluster_Size = GetPrivateProfileInt(str_model,strTitle+"i_Cluster_Size",-9999,R_filename);

	if(i_Cluster_Size == -9999){
		i_Cluster_Size = 5;
		str.Empty();
		str.Format("%d",i_Cluster_Size);
		WritePrivateProfileString(str_model,strTitle+"i_Cluster_Size",str,R_filename);
	}



	i_DF_inCluster = GetPrivateProfileInt(str_model,strTitle+"i_DF_inCluster",-9999,R_filename);

	if(i_DF_inCluster == -9999){
		i_DF_inCluster = 1;
		str.Empty();
		str.Format("%d",i_DF_inCluster);
		WritePrivateProfileString(str_model,strTitle+"i_DF_inCluster",str,R_filename);
	}


	i_DefectNum = GetPrivateProfileInt(str_model,strTitle+"i_DefectNum",-9999,R_filename);

	if(i_DefectNum == -9999){
		i_DefectNum = 500;
		str.Empty();
		str.Format("%d",i_DefectNum);
		WritePrivateProfileString(str_model,strTitle+"i_DefectNum",str,R_filename);
	}

	i_RoiRadius = GetPrivateProfileInt(str_model,strTitle+"i_RoiRadius",-9999,R_filename);

	if(i_RoiRadius == -9999){
		i_RoiRadius = 250;
		str.Empty();
		str.Format("%d",i_RoiRadius);
		WritePrivateProfileString(str_model,strTitle+"i_RoiRadius",str,R_filename);
	}

	InputData_BlockSpot();

	
}


void CParticle_Option_LG::OnEnChangeEditBlackW()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	BlockspotChangeChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_Option_LG::OnEnChangeEditBlackH()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	UpdateData(TRUE);
	BlockspotChangeChk();
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_Option_LG::OnEnChangeEditDfRatio()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	UpdateData(TRUE);
	BlockspotChangeChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_Option_LG::OnEnChangeEditClustersize()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	BlockspotChangeChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_Option_LG::OnEnChangeEditDefectincluster()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	UpdateData(TRUE);
	BlockspotChangeChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_Option_LG::OnEnChangeEditDefectnum()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	UpdateData(TRUE);
	BlockspotChangeChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_Option_LG::OnEnChangeEditRoiradius()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	BlockspotChangeChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_Option_LG::OnBnClickedCheckBlackSpot()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	

	CString str;
	if(b_BlackSpotTESTEn == 0){
		b_BlackSpotTESTEn =1;
	}else{
		b_BlackSpotTESTEn =0;
	}

	Edit_En_BlackSpot(b_BlackSpotTESTEn);
	UpdateData(FALSE);
	strTitle="PARTICLE_LG_";
	str.Empty();
	str.Format("%d",b_BlackSpotTESTEn);
	WritePrivateProfileString(str_model,strTitle+"BlackSpotEn",str,R_filename);
	BlockspotChangeChk();
}
void CParticle_Option_LG::Edit_En_BlackSpot(bool MODE){
	((CEdit *)GetDlgItem(IDC_EDIT_Black_W))->EnableWindow(MODE);
	((CEdit *)GetDlgItem(IDC_EDIT_Black_H))->EnableWindow(MODE);
	((CEdit *)GetDlgItem(IDC_EDIT_DF_Ratio))->EnableWindow(MODE);
	((CEdit *)GetDlgItem(IDC_EDIT_ClusterSize))->EnableWindow(MODE);
	((CEdit *)GetDlgItem(IDC_EDIT_DefectInCluster))->EnableWindow(MODE);
	((CEdit *)GetDlgItem(IDC_EDIT_DefectNum))->EnableWindow(MODE);
	((CEdit *)GetDlgItem(IDC_EDIT_ROIRadius))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BUTTON_BlockSpot_SAVE))->EnableWindow(MODE);

	InitEVMS();

}
void CParticle_Option_LG::OnBnClickedCheckStaintest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str;
	if(b_StainTESTEn == 0){
		b_StainTESTEn =1;
	}else{
		b_StainTESTEn =0;
	}
	UpdateData(FALSE);
	strTitle="PARTICLE_LG_";
	str.Empty();
	str.Format("%d",b_StainTESTEn);
	WritePrivateProfileString(str_model,strTitle+"StainEn",str,R_filename);

	Edit_En_Stain(b_StainTESTEn);
	StainChangeChk();
	
}

void CParticle_Option_LG::Edit_En_Stain(bool MODE){
	((CEdit *)GetDlgItem(IDC_EDIT_EdgeSize))->EnableWindow(MODE);
	((CEdit *)GetDlgItem(IDC_EDIT_Center_Thr))->EnableWindow(MODE);
	((CEdit *)GetDlgItem(IDC_EDIT_Edge_Thr))->EnableWindow(MODE);
	((CEdit *)GetDlgItem(IDC_EDIT_BlockWidth))->EnableWindow(MODE);
	((CEdit *)GetDlgItem(IDC_EDIT_STAIN_ROIRADIUS))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BUTTON_STAIN_SAVE))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_CHECK_DEBUG))->EnableWindow(MODE);

	InitEVMS();
}


void CParticle_Option_LG::OnEnChangeEditEdgesize()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	StainChangeChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_Option_LG::OnEnChangeEditCenterThr()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	StainChangeChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_Option_LG::OnEnChangeEditEdgeThr()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	StainChangeChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_Option_LG::OnEnChangeEditStainRoiradius()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	StainChangeChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_Option_LG::OnEnChangeEditBlockwidth()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	StainChangeChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CParticle_Option_LG::OnBnClickedButtonStainSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	i_EdgeSize = atoi(str_EdgeSize);
	f_Center_Thr = atof(str_Center_Thr);
	f_Edge_Thr = atof(str_Edge_Thr);
	i_Stain_Block_Width = atoi(str_Stain_Block_Width);
	i_Stain_ROIRadius = atoi(str_Stain_ROIRadius);
	i_Stain_Debug = Stain_Debug;

	str_EdgeSize.Format("%d",i_EdgeSize);
	str_Center_Thr.Format("%6.3f",f_Center_Thr);
	str_Edge_Thr.Format("%6.3f",f_Edge_Thr);
	str_Stain_Block_Width.Format("%d",i_Stain_Block_Width);
	str_Stain_ROIRadius.Format("%d",i_Stain_ROIRadius);

	UpdateData(FALSE);

	Save_parameter_Stain();

	InputData_Stain();
	StainChangeChk();
	
}

void CParticle_Option_LG::StainChangeChk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int ibuf_EdgeSize = atoi(str_EdgeSize);
	float fbuf_Center_Thr = atof(str_Center_Thr);
	float fbuf_Edge_Thr = atof(str_Edge_Thr);
	int ibuf_Stain_Block_Width = atoi(str_Stain_Block_Width);
	int ibuf_Stain_ROIRadius = atoi(str_Stain_ROIRadius);
	int ibuf_Stain_Debug = Stain_Debug;

	if( (ibuf_EdgeSize != i_EdgeSize)||
		(fbuf_Center_Thr != f_Center_Thr)||
		(fbuf_Edge_Thr != f_Edge_Thr)||
		(ibuf_Stain_Block_Width != i_Stain_Block_Width)||
		(ibuf_Stain_ROIRadius != i_Stain_ROIRadius)||
		(ibuf_Stain_Debug != i_Stain_Debug)){
			if(b_StainTESTEn == 1){
				((CButton *)GetDlgItem(IDC_BUTTON_STAIN_SAVE))->EnableWindow(1);
			}else{
				((CButton *)GetDlgItem(IDC_BUTTON_STAIN_SAVE))->EnableWindow(0);
			}
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_STAIN_SAVE))->EnableWindow(0);
	}
}


void CParticle_Option_LG::InputData_Stain(){

	str_EdgeSize.Format("%d",i_EdgeSize);
	str_Center_Thr.Format("%6.3f",f_Center_Thr);
	str_Edge_Thr.Format("%6.3f",f_Edge_Thr);
	str_Stain_Block_Width.Format("%d",i_Stain_Block_Width);
	str_Stain_ROIRadius.Format("%d",i_Stain_ROIRadius);
	Stain_Debug = i_Stain_Debug;
	
// 	Stain2Config.nEdgeSize = i_EdgeSize;
// 	Stain2Config.fCenterThreshold = f_Center_Thr;
// 	Stain2Config.fEdgeThreshold = f_Edge_Thr;
// 	Stain2Config.nBlockWidth = i_Stain_Block_Width;
// 	Stain2Config.ROIRadius = i_Stain_ROIRadius;
// 	Stain2Config.debug = i_Stain_Debug;
	UpdateData(FALSE);

}

void CParticle_Option_LG::Save_parameter_Stain(){

	CString str = "";

	strTitle="PARTICLE_LG_";
	str.Empty();
	str.Format("%d",i_EdgeSize);
	WritePrivateProfileString(str_model,strTitle+"i_EdgeSize",str,R_filename);
	str.Empty();
	str.Format("%6.3f",f_Center_Thr);
	WritePrivateProfileString(str_model,strTitle+"f_Center_Thr",str,R_filename);
	str.Empty();
	str.Format("%6.3f",f_Edge_Thr);
	WritePrivateProfileString(str_model,strTitle+"f_Edge_Thr",str,R_filename);

	str.Empty();
	str.Format("%d",i_Stain_Block_Width);
	WritePrivateProfileString(str_model,strTitle+"i_Stain_Block_Width",str,R_filename);

	str.Empty();
	str.Format("%d",i_Stain_ROIRadius);
	WritePrivateProfileString(str_model,strTitle+"i_Stain_ROIRadius",str,R_filename);

	str.Empty();
	str.Format("%d",i_Stain_Debug);
	WritePrivateProfileString(str_model,strTitle+"i_Stain_Debug",str,R_filename);

}

void CParticle_Option_LG::Load_parameter_Stain(){

	CString str = "";

	strTitle="PARTICLE_LG_";

	i_EdgeSize = GetPrivateProfileInt(str_model,strTitle+"i_EdgeSize",-9999,R_filename);

	if(i_EdgeSize == -9999){
		i_EdgeSize = 50;
		str.Empty();
		str.Format("%d",i_EdgeSize);
		WritePrivateProfileString(str_model,strTitle+"i_EdgeSize",str,R_filename);
	}

	double DATA=0;
	DATA= GetPrivateProfileDouble(str_model,strTitle+"f_Center_Thr",-9999,R_filename);

	if(DATA == -9999){
		DATA = 1.0;
		str.Empty();
		str.Format("%6.3f",DATA);
		WritePrivateProfileString(str_model,strTitle+"f_Center_Thr",str,R_filename);
	}

	f_Center_Thr = (float)DATA;


	DATA= GetPrivateProfileDouble(str_model,strTitle+"f_Edge_Thr",-9999,R_filename);

	if(DATA == -9999){
		DATA = 3.0;
		str.Empty();
		str.Format("%6.3f",DATA);
		WritePrivateProfileString(str_model,strTitle+"f_Edge_Thr",str,R_filename);
	}

	f_Edge_Thr = (float)DATA;




	i_Stain_Block_Width = GetPrivateProfileInt(str_model,strTitle+"i_Stain_Block_Width",-9999,R_filename);

	if(i_Stain_Block_Width == -9999){
		i_Stain_Block_Width =16;
		str.Empty();
		str.Format("%d",i_Stain_Block_Width);
		WritePrivateProfileString(str_model,strTitle+"i_Stain_Block_Width",str,R_filename);
	}



	i_Stain_ROIRadius = GetPrivateProfileInt(str_model,strTitle+"i_Stain_ROIRadius",-9999,R_filename);

	if(i_Stain_ROIRadius == -9999){
		i_Stain_ROIRadius = 250;
		str.Empty();
		str.Format("%d",i_Stain_ROIRadius);
		WritePrivateProfileString(str_model,strTitle+"i_Stain_ROIRadius",str,R_filename);
	}

	i_Stain_Debug = GetPrivateProfileInt(str_model,strTitle+"i_Stain_Debug",-9999,R_filename);

	if(i_Stain_Debug == -9999){
		i_Stain_Debug = 1;
		str.Empty();
		str.Format("%d",i_Stain_Debug);
		WritePrivateProfileString(str_model,strTitle+"i_Stain_Debug",str,R_filename);
	}

	InputData_Stain();

	
}

void CParticle_Option_LG::OnCbnSelchangeComboTestmode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str;
	/*if(b_ManualTestMode == 0){
		b_ManualTestMode =1;
	}else{
		b_ManualTestMode =0;
	}
	UpdateData(FALSE);*/
	UpdateData(TRUE);
	strTitle="PARTICLE_LG_";
	str.Empty();
	str.Format("%d",m_uTestMode);
	WritePrivateProfileString(str_model,strTitle+"USERTESTMODE",str,R_filename);
	GotoDlgCtrl(((CStatic *)GetDlgItem(IDC_STATIC)));

}

void CParticle_Option_LG::OnBnClickedCheckDebug()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	
}
void CParticle_Option_LG::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == TRUE){
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = -1;

	}
}

BOOL CParticle_Option_LG::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

		if (pMsg->wParam == VK_RETURN){
			if ((pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_Black_W))->GetSafeHwnd())||  
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_Black_H))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_DF_Ratio))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_ClusterSize))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_DefectInCluster))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_DefectNum))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_ROIRadius))->GetSafeHwnd()))
			{	
				if(((CButton *)GetDlgItem(IDC_BUTTON_BlockSpot_SAVE))->IsWindowEnabled() == TRUE){
					OnBnClickedButtonBlockspotSave();
				}
			}

			if ((pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_EdgeSize))->GetSafeHwnd())||  
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_Center_Thr))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_Edge_Thr))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_BlockWidth))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_STAIN_ROIRADIUS))->GetSafeHwnd()))
			{	
				if(((CButton *)GetDlgItem(IDC_BUTTON_STAIN_SAVE))->IsWindowEnabled() == TRUE){
					OnBnClickedButtonStainSave();
				}
			}
			
			return TRUE;
		}
		
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CParticle_Option_LG::OnEnSetfocusEditModeStatLg()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	GotoDlgCtrl(((CButton *)GetDlgItem(IDC_CHECK_MANUALTEST_LG)));

}
