// Dlg_Emergency.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Dlg_Emergency.h"


#define IDC_EDIT_WARNINGTEXT		1000
#define IDC_EDIT_WARNINGTEXT2		1001
#define IDC_EDIT_WARNINGBK1			1002
#define IDC_EDIT_WARNINGBK2			1003
#define IDC_BUTTON_OK				2000

// CDlg_Emergency 대화 상자입니다.




IMPLEMENT_DYNAMIC(CDlg_Emergency, CDialog)

CDlg_Emergency::CDlg_Emergency(CWnd* pParent /*=NULL*/)
	: CDialog(CDlg_Emergency::IDD, pParent)
{
	m_iflash = -1;

	VERIFY(m_font.CreateFont(
		140,						// nHeight
		0,						// nWidth	
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font3.CreateFont(
		260,						// nHeight
		0,						// nWidth	
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font1.CreateFont(
		100,						// nHeight
		0,						// nWidth	
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
	
	VERIFY(m_font2.CreateFont(
		500,						// nHeight
		0,						// nWidth	
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CDlg_Emergency::~CDlg_Emergency()
{

	m_font.DeleteObject();
	m_font1.DeleteObject();
	m_font2.DeleteObject();
	m_font3.DeleteObject();

	
}

void CDlg_Emergency::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlg_Emergency, CDialog)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON_OK, OnBnClickedBtnOK)

END_MESSAGE_MAP()


// CDlg_Emergency 메시지 처리기입니다.

int CDlg_Emergency::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_edText.Create(dwStyle | ES_CENTER | ES_READONLY , rectDummy, this, IDC_EDIT_WARNINGTEXT);
	m_edText.SetFont(&m_font);

	m_edText1.Create(dwStyle | ES_CENTER | ES_READONLY , rectDummy, this, IDC_EDIT_WARNINGTEXT2);
	m_edText1.SetFont(&m_font3);
	//m_edText.SetWindowText("안전 센서 감지!!");

	m_edBk1.Create(dwStyle | ES_CENTER | ES_READONLY, rectDummy, this, IDC_EDIT_WARNINGBK1);
	m_edBk1.SetFont(&m_font2);
	m_edBk1.SetWindowText("");

	m_edBk2.Create(dwStyle | ES_CENTER | ES_READONLY, rectDummy, this, IDC_EDIT_WARNINGBK2);
	m_edBk2.SetFont(&m_font2);
	m_edBk2.SetWindowText("");

	m_btOK.Create("확인", dwStyle | WS_BORDER | BS_PUSHBUTTON , rectDummy, this, IDC_BUTTON_OK);
	m_btOK.SetFont(&m_font1);

	this->ShowWindow(SW_MAXIMIZE);

	
	SetTimer(100, 500, NULL);

	return 0;
}

void CDlg_Emergency::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin = 1;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iMagrin;
	int iTop = iMagrin;
	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	int iBtH = iHeight / 6;
	int iEdH = iHeight - iBtH;
	int iEdTxtH = iEdH / 7;
	int iEdBkH = iEdH * 2 / 7;


	m_edBk1.MoveWindow(iLeft, iTop, iWidth, iEdBkH);
	m_edText.MoveWindow(iLeft, iEdBkH, iWidth, iEdTxtH);

	iEdBkH+=iEdTxtH;
	m_edText1.MoveWindow(iLeft, iEdBkH, iWidth, iEdTxtH *13/ 7);
	m_edBk2.MoveWindow(iLeft, iEdH * 4 / 7  , iWidth, iEdBkH);

	iTop += iHeight * 5 / 6;
	m_btOK.MoveWindow(iLeft, iTop, iWidth, iBtH);

}

BOOL CDlg_Emergency::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialog::PreCreateWindow(cs);
}

BOOL CDlg_Emergency::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN)
		{
			return TRUE;
		}

		if (pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}


void CDlg_Emergency::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch(nIDEvent)
	{
	case 100:
		
		m_iflash *= -1;
		m_edText.SetWindowText("안전 센서 감지!");
		m_edText1.SetWindowText("주의!!!!");
		
		break;
	
	default:
		break;
	}

	CDialog::OnTimer(nIDEvent);
}

HBRUSH CDlg_Emergency::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	if(pWnd->GetDlgCtrlID() == m_edText.GetDlgCtrlID() || pWnd->GetDlgCtrlID() == m_edText1.GetDlgCtrlID())
	{
		if (m_iflash == -1)
		{
			// 텍스트의 배경색상을 설정한다.
			pDC->SetTextColor(RGB(255, 255, 255));

			// 바탕색상을 설정한다.
			pDC->SetBkColor(RGB(201, 0, 0));
		}
		else
		{
			// 텍스트의 배경색상을 설정한다.
			pDC->SetTextColor(RGB(201, 0, 0));

			// 바탕색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
		}
	}


	if(pWnd->GetDlgCtrlID() == m_edBk1.GetDlgCtrlID()
		|| pWnd->GetDlgCtrlID() == m_edBk2.GetDlgCtrlID())
	{
		// 텍스트의 배경색상을 설정한다.
			pDC->SetTextColor(RGB(255, 255, 255));

			// 바탕색상을 설정한다.
			pDC->SetBkColor(RGB(201, 0, 0));
	}
	
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


void CDlg_Emergency::OnBnClickedBtnOK()
{
	KillTimer(100);
	OnOK();
		
}