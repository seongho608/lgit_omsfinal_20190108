#pragma once
#include "afxcmn.h"

#include "ETCUSER.H"
#include "afxwin.h"
// CColorReversal_Option 대화 상자입니다.

class CColorReversal_Option : public CDialog
{
	DECLARE_DYNAMIC(CColorReversal_Option)

public:
	CColorReversal_Option(CWnd* pParent = NULL);   // 표A준 생성자입니다.
	virtual ~CColorReversal_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_COLOR_REVERSAL };

	void	Focus_move_start();
	int m_CAM_SIZE_WIDTH;
	int m_CAM_SIZE_HEIGHT;

	CRectData C_RECT[4];
	CRectData C_CHECKING[4];
	CRectData C_Original[4];
	void Load_Original_pra();

	CRectData CD_RECT[4];	//Color Detection
	CRectData Standard_CD_RECT[4];
	void GetColorAreaCoordinate(LPBYTE m_RGBIn);

	tResultVal m_retval;
	void FAIL_UPLOAD();
	BOOL	b_StopFail;
	void Automation_DATA_INPUT();
	tResultVal Run(void);
	void Pic(CDC *cdc);
	void InitPrm();

	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
	void	StatePrintf(LPCSTR lpcszString, ...);

	void	SETLIST();
	void	UploadList();
	void	Save_parameter();
	void	Load_parameter();
	void	Save(int NUM);
	void	Change_DATA();

	int StartCnt;

	bool AutomationMod;
	bool ChangeCheck;
	int ChangeItem[4];
	int changecount;
	double GetDistance(int x1, int y1, int x2, int y2);

	int PosX,PosY;
	int InIndex;
	int iSavedItem, iSavedSubitem;
	int iSavedSubitem2;
	int state;
	int EnterState;
	int NewItem,NewSubitem;
	CRect rect;

	bool MasterMod;

	CString C_filename;
	int	m_AVER,m_AVEG,m_AVEB;
	int	m_Comp_AVER_MAX,m_Comp_AVEG_MAX,m_Comp_AVEB_MAX;
	int	m_Comp_AVER_MIN,m_Comp_AVEG_MIN,m_Comp_AVEB_MIN;
	bool AveRGBPic(CDC *cdc,int NUM);
	bool AveRGBGen(LPBYTE IN_RGB,int NUM);
	void ColorReversal_TEST(int R, int G ,int B, int NUM);
	void AveRGB(LPBYTE IN_RGB,int NUM);
	void RGB_Judgment();
	int ColorReversal_DATA[4];
	CString checkView;
	CString checkViewResult;
	void Check_RGBVIEW(CString check);
	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void InsertList();
	int InsertIndex;
	int ListItemNum;

	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();
	void List_COLOR_Change();
	CFont f1;
	unsigned char	Master_text;
	void Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	void MasterState(unsigned char col,LPCSTR lpcszString, ...);
	void	EditWheelIntSet(int nID,int Val);
	void	EditMinMaxIntSet(int nID,int* Val,int Min,int Max);
	bool	Change_DATA_CHECK(bool FLAG);
CString		GetColorNum(BYTE R, BYTE G, BYTE B);
	CvRect GetCenterPoint(LPBYTE IN_RGB);

	// MES 정보저장
	CString m_szMesResult;
	CString Mes_Result();

	void InitEVMS();

#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	int Lot_StartCnt;
	int Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

#pragma endregion

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl C_DATALIST;
//	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CListCtrl m_ColorReversalList;
	int C_Total_PosX;
	int C_Total_PosY;
	int C_Dis_Width;
	int C_Dis_Height;
	int C_Total_Width;
	int C_Total_Height;

	afx_msg void OnBnClickedButtonSetzone();
	afx_msg void OnBnClickedButtonReversalSave();
	afx_msg void OnBnClickedButtonReversalMaster();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedCheckReversalAuto();
	afx_msg void OnNMClickListReversal(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawListReversal(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnNMDblclkListReversal(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusEditColorReversal();
	afx_msg void OnBnClickedButtonCrload();
	afx_msg void OnBnClickedCheckColorreversal();
	CButton btn_ColorReversalKeepRun;
	BOOL ColorReversalKeepRun;
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnEnChangeEditReversalPosx();
	afx_msg void OnEnChangeEditReversalPosy();
	afx_msg void OnEnChangeEditReversalDisw();
	afx_msg void OnEnChangeEditReversalDish();
	afx_msg void OnEnChangeEditReversalW();
	afx_msg void OnEnChangeEditReversalH();
	afx_msg void OnEnChangeEditColorReversal();
	afx_msg void OnEnSetfocusEditMasterstate();

	
	afx_msg void OnBnClickedButtontest();
	CListCtrl m_Lot_ColorReversalList;
	afx_msg void OnLvnItemchangedListReversal(NMHDR *pNMHDR, LRESULT *pResult);
};


void CModel_Create(CColorReversal_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO);
void CModel_Delete(CColorReversal_Option **pWnd);