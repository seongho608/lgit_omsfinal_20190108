#pragma once
#include "resource.h"

// CStandOptDlg 대화 상자입니다.

class CStandOptDlg : public CDialog
{
	DECLARE_DYNAMIC(CStandOptDlg)

public:
	CStandOptDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CStandOptDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_STAND_OPT_DIALOG };
	void	Setup(CWnd* IN_pMomWnd);
	CComboBox	*pModelCombo;
	CComboBox	*pConCombo;
	CComboBox	*pSubCombo;
	CComboBox	*pCamCombo;

	CComboBox	*pBaudCombo;
	CComboBox	*pDataCombo;
	CComboBox	*pStopCombo;
	CComboBox	*pParityCombo;

	void PortUpdate();

	int i_MSTime_H;
	int i_MSTime_M;

	void	InitEVMS();
private:
	CWnd		*m_pMomWnd;
	BOOL	PortSettingChk_PORT();
	BOOL	PortSettingChk_HW();
	BOOL	PortSettingChk_CONPORT();
	BOOL	PortSettingChk_SUBPORT();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedSerialstatSave();
	CString REPORT_PATH;
		CString str_MSTime_H;
	CString str_MSTime_M;
	CString m_str_cpyname;
	CString m_str_name;
	CString m_PW_Change;
	afx_msg void OnEnChangeModelname();
	afx_msg void OnBnClickedBtnModelsave();
	afx_msg void OnEnChangeCopymodel();
	afx_msg void OnBnClickedBtnModelcopy();
	afx_msg void OnBnClickedBtnModeldel();
	afx_msg void OnBnClickedReportpathsave();
	afx_msg void OnEnChangePassedit();
	afx_msg void OnBnClickedPasswordChange();
	afx_msg void OnCbnSelchangePortCon();
	afx_msg void OnCbnSelchangePortSub();
	afx_msg void OnCbnSelchangePortCam();
	afx_msg void OnCbnSelchangeBordrate();
	afx_msg void OnCbnSelchangeDataBit();
	afx_msg void OnCbnSelchangeParity();
	afx_msg void OnCbnSelchangeStopBit();
	afx_msg void OnBnClickedSerialstatSave2();
//	afx_msg void OnCbnSelchangeComobModel();
	afx_msg void OnEnChangeEditMsTimeH();
	afx_msg void OnEnChangeEditMsTimeM();
	afx_msg void OnBnClickedButtonMsTime();
	BOOL b_InterLock_Enable;
	afx_msg void OnBnClickedCheckInterlock();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedReportpathsaveP();
	CString REPORT_PATH_P;
	CString IMAGE_PATH;
	afx_msg void OnBnClickedImagesave();
};
