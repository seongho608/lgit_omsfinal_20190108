// ColorReversal_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "ColorReversal_Option.h"

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];

// CColorReversal_Option 대화 상자입니다.

IMPLEMENT_DYNAMIC(CColorReversal_Option, CDialog)

CColorReversal_Option::CColorReversal_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CColorReversal_Option::IDD, pParent)
	, C_Total_PosX(0)
	, C_Total_PosY(0)
	, C_Dis_Width(0)
	, C_Dis_Height(0)
	, C_Total_Width(0)
	, C_Total_Height(0)

	, ColorReversalKeepRun(FALSE)
{
	MasterMod=0;
	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;AutomationMod=0;
	iSavedItem=0;
	iSavedSubitem=0;
	iSavedSubitem2=0;
	state=0;
	EnterState=0;
	NewItem =0;NewSubitem=0;
	ChangeCheck=0;
	for(int t=0; t<4; t++){
		ChangeItem[t]=-1;
	}
	changecount=0;
	StartCnt=0;
	b_StopFail = FALSE;
	
//****************Color Area 기준 탐색 점 설정*******************//
	//Standard_CD_RECT[0].m_resultX = 240;	//왼쪽 Side circle
	//Standard_CD_RECT[0].m_resultY = 140;
	//Standard_CD_RECT[1].m_resultX = 490;	//오른쪽 Side circle
	//Standard_CD_RECT[1].m_resultY = 140;
	//Standard_CD_RECT[2].m_resultX = 240;	//위쪽 Side circle
	//Standard_CD_RECT[2].m_resultY = 350;
	//Standard_CD_RECT[3].m_resultX = 490;	//아래쪽 Side circle
	//Standard_CD_RECT[3].m_resultY = 350;
//****************************************************************//

	Standard_CD_RECT[0].m_resultX = (CAM_IMAGE_WIDTH/2) - ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);        //왼쪽위사각형
    Standard_CD_RECT[0].m_resultY = (CAM_IMAGE_HEIGHT/2) - ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);
    Standard_CD_RECT[1].m_resultX = (CAM_IMAGE_WIDTH/2) + ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);        //오른쪽위사각형
    Standard_CD_RECT[1].m_resultY = (CAM_IMAGE_HEIGHT/2) - ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);
    Standard_CD_RECT[2].m_resultX = (CAM_IMAGE_WIDTH/2) - ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);;        //왼쪽아래사각형
    Standard_CD_RECT[2].m_resultY = (CAM_IMAGE_HEIGHT/2) + ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);
    Standard_CD_RECT[3].m_resultX = (CAM_IMAGE_WIDTH/2) + ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);;        //오른쪽아래사각형
    Standard_CD_RECT[3].m_resultY = (CAM_IMAGE_HEIGHT/2) + ((CAM_IMAGE_WIDTH+CAM_IMAGE_HEIGHT)/12);
}

CColorReversal_Option::~CColorReversal_Option()
{
}

void CColorReversal_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_REVERSAL, C_DATALIST);
	DDX_Control(pDX, IDC_LIST_COLOR_REVERSAL_LIST, m_ColorReversalList);
	DDX_Text(pDX, IDC_EDIT_REVERSAL_POSX, C_Total_PosX);
	DDV_MinMaxInt(pDX, C_Total_PosX, 0, m_CAM_SIZE_WIDTH);
	DDX_Text(pDX, IDC_EDIT_REVERSAL_POSY, C_Total_PosY);
	DDV_MinMaxInt(pDX, C_Total_PosY, 0, m_CAM_SIZE_HEIGHT);
	DDX_Text(pDX, IDC_EDIT_REVERSAL_DISW, C_Dis_Width);
	DDX_Text(pDX, IDC_EDIT_REVERSAL_DISH, C_Dis_Height);
	DDX_Text(pDX, IDC_EDIT_REVERSAL_W, C_Total_Width);
	DDX_Text(pDX, IDC_EDIT_REVERSAL_H, C_Total_Height);
	DDX_Control(pDX, IDC_CHECK_COLORREVERSAL, btn_ColorReversalKeepRun);
	DDX_Check(pDX, IDC_CHECK_COLORREVERSAL, ColorReversalKeepRun);
	DDX_Control(pDX, IDC_LIST_COLOR_REVERSAL_LOT, m_Lot_ColorReversalList);
}


BEGIN_MESSAGE_MAP(CColorReversal_Option, CDialog)
//	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_SETZONE, &CColorReversal_Option::OnBnClickedButtonSetzone)
	ON_BN_CLICKED(IDC_BUTTON_REVERSAL_SAVE, &CColorReversal_Option::OnBnClickedButtonReversalSave)
	ON_BN_CLICKED(IDC_BUTTON_REVERSAL_MASTER, &CColorReversal_Option::OnBnClickedButtonReversalMaster)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_CHECK_REVERSAL_AUTO, &CColorReversal_Option::OnBnClickedCheckReversalAuto)
	ON_NOTIFY(NM_CLICK, IDC_LIST_REVERSAL, &CColorReversal_Option::OnNMClickListReversal)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_REVERSAL, &CColorReversal_Option::OnNMCustomdrawListReversal)
	ON_WM_SHOWWINDOW()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_REVERSAL, &CColorReversal_Option::OnNMDblclkListReversal)
	ON_EN_KILLFOCUS(IDC_EDIT_COLOR_REVERSAL, &CColorReversal_Option::OnEnKillfocusEditColorReversal)
	ON_BN_CLICKED(IDC_BUTTON_CR_load, &CColorReversal_Option::OnBnClickedButtonCrload)
	ON_BN_CLICKED(IDC_CHECK_COLORREVERSAL, &CColorReversal_Option::OnBnClickedCheckColorreversal)
	ON_WM_MOUSEWHEEL()
	ON_EN_CHANGE(IDC_EDIT_REVERSAL_POSX, &CColorReversal_Option::OnEnChangeEditReversalPosx)
	ON_EN_CHANGE(IDC_EDIT_REVERSAL_POSY, &CColorReversal_Option::OnEnChangeEditReversalPosy)
	ON_EN_CHANGE(IDC_EDIT_REVERSAL_DISW, &CColorReversal_Option::OnEnChangeEditReversalDisw)
	ON_EN_CHANGE(IDC_EDIT_REVERSAL_DISH, &CColorReversal_Option::OnEnChangeEditReversalDish)
	ON_EN_CHANGE(IDC_EDIT_REVERSAL_W, &CColorReversal_Option::OnEnChangeEditReversalW)
	ON_EN_CHANGE(IDC_EDIT_REVERSAL_H, &CColorReversal_Option::OnEnChangeEditReversalH)
	ON_EN_CHANGE(IDC_EDIT_COLOR_REVERSAL, &CColorReversal_Option::OnEnChangeEditColorReversal)
	ON_EN_SETFOCUS(IDC_EDIT_MasterState, &CColorReversal_Option::OnEnSetfocusEditMasterstate)
	ON_BN_CLICKED(IDC_BUTTON_test, &CColorReversal_Option::OnBnClickedButtontest)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_REVERSAL, &CColorReversal_Option::OnLvnItemchangedListReversal)
END_MESSAGE_MAP()


// CColorReversal_Option 메시지 처리기입니다.

void CColorReversal_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
	
}



void CColorReversal_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);;
}
BOOL CColorReversal_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;

	((CEdit *) GetDlgItem(IDC_EDIT_COLOR_REVERSAL))->ShowWindow(FALSE);
	SETLIST();
	
	//C_filename=((CImageTesterDlg *)m_pMomWnd)->genfolderpath +"\\ColorTEST.ini";
	C_filename=((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	Font_Size_Change(IDC_EDIT_MasterState,&f1,400,23);


	Load_parameter();
	MasterState(0,checkView);
	UpdateData(FALSE);
	UploadList();
//	SetTimer(110,100,NULL);

	Set_List(&m_ColorReversalList);

	LOT_Set_List(&m_Lot_ColorReversalList);
	if(AutomationMod == 1){
		AutomationMod =0;	
		OnBnClickedCheckReversalAuto();
		CheckDlgButton(IDC_CHECK_REVERSAL_AUTO,TRUE);
	}
	
	//((CButton *)GetDlgItem(IDC_BUTTON_SETZONE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_SAVE))->EnableWindow(0);

	InitEVMS();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CColorReversal_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_MASTER))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_CR_load))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_SETZONE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_REVERSAL_AUTO))->EnableWindow(0);
		
		((CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_POSX))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_POSY))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_DISW))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_DISH))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_W))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_H))->EnableWindow(0);
	}
}


void CColorReversal_Option::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight;
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}


//void CColorReversal_Option::OnTimer(UINT_PTR nIDEvent)
//{
//	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
//
//	CDialog::OnTimer(nIDEvent);
//	switch(nIDEvent)	
//	{
//	case 110:
//		if(EnterState==1)
//		{
//	       
//			if(iSavedSubitem2 ==9){//////////////////////0913수정
//				if(iSavedItem == 0){
//					iSavedItem = 1; iSavedSubitem2 =1;
//				}
//				else if(iSavedItem == 1){
//					iSavedItem = 2; iSavedSubitem2 =1;
//				}
//				else if(iSavedItem == 2){
//					iSavedItem = 3; iSavedSubitem2 =1;
//				}
//				else if(iSavedItem == 3){
//					iSavedItem = 0; iSavedSubitem2 =1;
//				}
//			}
//			
//			C_DATALIST.GetSubItemRect(iSavedItem,iSavedSubitem2 , LVIR_BOUNDS, rect);
//			C_DATALIST.ClientToScreen(rect);
//			this->ScreenToClient(rect);
//
//			C_DATALIST.SetSelectionMark(iSavedItem);
//			C_DATALIST.SetFocus();
//			((CEdit *)GetDlgItem(IDC_EDIT_COLOR_REVERSAL))->SetWindowText(C_DATALIST.GetItemText(iSavedItem, iSavedSubitem2));
//			((CEdit *)GetDlgItem(IDC_EDIT_COLOR_REVERSAL))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
//			((CEdit *)GetDlgItem(IDC_EDIT_COLOR_REVERSAL))->SetFocus();
//
//			iSavedSubitem=iSavedSubitem2;
//
//			EnterState=0;
//			
//		}
//
//	break;
//
//	}
//
//	CDialog::OnTimer(nIDEvent);
//}


BOOL CColorReversal_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
			if(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_COLOR_REVERSAL))->GetSafeHwnd())//GetSafeHwnd() 자신의 핸들을 가져오는 함수
			{
				int bufSubitem = iSavedSubitem + 1;
				
				if(bufSubitem ==9){
					if(iSavedItem == 0){
						iSavedItem = 1; bufSubitem =1;
					}
					else if(iSavedItem == 1){
						iSavedItem = 2; bufSubitem =1;
					}
					else if(iSavedItem == 2){
						iSavedItem = 3; bufSubitem =1;
					}
					else if(iSavedItem == 3){
						iSavedItem =0; bufSubitem =1;
					}
					
				}
				int bufItem = iSavedItem;
				C_DATALIST.EnsureVisible(iSavedItem, FALSE);
				C_DATALIST.GetSubItemRect(iSavedItem,bufSubitem , LVIR_BOUNDS, rect);
				C_DATALIST.ClientToScreen(rect);
				this->ScreenToClient(rect);
				C_DATALIST.SetSelectionMark(iSavedItem);
				C_DATALIST.SetFocus(); //저장
				iSavedItem = bufItem;
				iSavedSubitem = bufSubitem;
				((CEdit *)GetDlgItem(IDC_EDIT_COLOR_REVERSAL))->SetWindowText(C_DATALIST.GetItemText(iSavedItem, iSavedSubitem));
				((CEdit *)GetDlgItem(IDC_EDIT_COLOR_REVERSAL))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
				((CEdit *)GetDlgItem(IDC_EDIT_COLOR_REVERSAL))->SetFocus();
			}
			
			if ((pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_POSX))->GetSafeHwnd())||  
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_POSY))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_DISW))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_DISH))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_W))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_H))->GetSafeHwnd()))
			{	
				OnBnClickedButtonSetzone();
			}

			return TRUE;
		}
		
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CColorReversal_Option::SETLIST(){
	C_DATALIST.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	C_DATALIST.InsertColumn(0,"Zone",LVCFMT_CENTER, 100);
	C_DATALIST.InsertColumn(1,"PosX",LVCFMT_CENTER, 60);
	C_DATALIST.InsertColumn(2,"PosY",LVCFMT_CENTER, 60);
	C_DATALIST.InsertColumn(3,"START X",LVCFMT_CENTER, 60);
	C_DATALIST.InsertColumn(4,"START Y",LVCFMT_CENTER, 60);
	C_DATALIST.InsertColumn(5,"EXIT X",LVCFMT_CENTER, 60);
	C_DATALIST.InsertColumn(6,"EXIT Y",LVCFMT_CENTER, 60);
	C_DATALIST.InsertColumn(7,"Width",LVCFMT_CENTER, 60);
	C_DATALIST.InsertColumn(8,"Height",LVCFMT_CENTER, 60);
	C_DATALIST.InsertColumn(9,"COLOR",LVCFMT_CENTER, 60);

}

void CColorReversal_Option::UploadList(){
	CString str="";
	
	C_DATALIST.DeleteAllItems();

	for(int t=0; t<4; t++){
		InIndex = C_DATALIST.InsertItem(t,"초기",0);
		
 	
		if(t ==0){
			str.Empty();str="LEFT_TOP";
		}
		if(t ==1){
			str.Empty();str="RIGHT_TOP";
		}
		if(t ==2){
			str.Empty();str="LEFT_BOTTOM";
		}
		if(t ==3){
			str.Empty();str="RIGHT_BOTTOM";
		}
		
		C_DATALIST.SetItemText(InIndex,0,str);
		str.Empty();str.Format("%d", C_RECT[t].m_PosX);
		C_DATALIST.SetItemText(InIndex,1,str);
		str.Empty();str.Format("%d", C_RECT[t].m_PosY);
		C_DATALIST.SetItemText(InIndex,2,str);
		
		str.Empty();
		str.Format("%d", C_RECT[t].m_Left);
		C_DATALIST.SetItemText(InIndex,3,str);
		str.Empty();
		str.Format("%d", C_RECT[t].m_Top);
		C_DATALIST.SetItemText(InIndex,4,str);
	
		str.Empty();
		str.Format("%d", C_RECT[t].m_Right+1);
		C_DATALIST.SetItemText(InIndex,5,str);
		str.Empty();
		str.Format("%d", C_RECT[t].m_Bottom+1);
		C_DATALIST.SetItemText(InIndex,6,str);

		str.Empty();
		str.Format("%d", C_RECT[t].m_Width);
		C_DATALIST.SetItemText(InIndex,7,str);
		str.Empty();
		str.Format("%d",C_RECT[t].m_Height);
		C_DATALIST.SetItemText(InIndex,8,str);
		
		C_DATALIST.SetItemText(InIndex,9,C_RECT[t].COLOR);
	}
}

void CColorReversal_Option::Save_parameter(){

	CString str="";
	CString strTitle="";

	//WritePrivateProfileString(str_model,NULL,"",C_filename);
	str.Empty();
	str.Format("%d",m_INFO.ID);
	WritePrivateProfileString(str_model,"ID",str,C_filename);
	str.Empty();
	str.Format("%s",m_INFO.MODE);
	WritePrivateProfileString(str_model,"MODE",str,C_filename);
	str.Empty();
	str.Format("%s",m_INFO.NAME);
	WritePrivateProfileString(str_model,"NAME",str,C_filename);

	strTitle.Empty();
	strTitle="COLOR_REVERSAL_INIT";

	str.Empty();
	str.Format("%d",C_Total_PosX);
	WritePrivateProfileString(str_model,strTitle+"PosX",str,C_filename);
	str.Empty();
	str.Format("%d",C_Total_PosY);
	WritePrivateProfileString(str_model,strTitle+"PosY",str,C_filename);
	str.Empty();
	str.Format("%d",C_Dis_Width);
	WritePrivateProfileString(str_model,strTitle+"DisW",str,C_filename);
	str.Empty();
	str.Format("%d",C_Dis_Height);
	WritePrivateProfileString(str_model,strTitle+"DisH",str,C_filename);
	str.Empty();
	str.Format("%d",C_Total_Width);
	WritePrivateProfileString(str_model,strTitle+"Width",str,C_filename);
	str.Empty();
	str.Format("%d",C_Total_Height);
	WritePrivateProfileString(str_model,strTitle+"Height",str,C_filename);

	WritePrivateProfileString(str_model,strTitle+"MasterState",checkView,C_filename);

	if(AutomationMod ==1){
		WritePrivateProfileString(str_model,strTitle+"Auto","1",C_filename);}
	if(AutomationMod ==0){
		WritePrivateProfileString(str_model,strTitle+"Auto","0",C_filename);}
	

	for(int t=0; t<4; t++){
	
		if(t ==0){
			strTitle.Empty();
			strTitle="LEFT_TOP_";
		}
		if(t ==1){
			strTitle.Empty();
			strTitle="RIGHT_TOP_";		
		}
		if(t ==2){
			strTitle.Empty();
			strTitle="LEFT_BOTTOM_";
		}
		if(t ==3){
			strTitle.Empty();
			strTitle="RIGHT_BOTTOM_";
		}


		
		str.Empty();
		str.Format("%d",C_RECT[t].m_PosX);
		WritePrivateProfileString(str_model,strTitle+"PosX",str,C_filename);
		C_Original[t].m_PosX = C_RECT[t].m_PosX;
		str.Empty();
		str.Format("%d",C_RECT[t].m_PosY);
		WritePrivateProfileString(str_model,strTitle+"PosY",str,C_filename);
		C_Original[t].m_PosY = C_RECT[t].m_PosY;
		str.Empty();
		str.Format("%d",C_RECT[t].m_Left);
		WritePrivateProfileString(str_model,strTitle+"StartX",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_Top);
		WritePrivateProfileString(str_model,strTitle+"StartY",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_Right);
		WritePrivateProfileString(str_model,strTitle+"ExitX",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_Bottom);
		WritePrivateProfileString(str_model,strTitle+"ExitY",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_Width);
		WritePrivateProfileString(str_model,strTitle+"Width",str,C_filename);
		str.Empty();
		str.Format("%d",C_RECT[t].m_Height);
		WritePrivateProfileString(str_model,strTitle+"Height",str,C_filename);
		WritePrivateProfileString(str_model,strTitle+"COLOR",C_RECT[t].COLOR,C_filename);

	}

	
}


void CColorReversal_Option::Load_parameter(){
	CFileFind filefind;
	CString str="";
	CString strTitle="";

	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	if((GetPrivateProfileCString(str_model,"NAME",C_filename) != m_INFO.NAME)||(GetPrivateProfileCString(str_model,"MODE",C_filename) != m_INFO.MODE)){//ini파일이 없으면 파일을 생성한다.
		C_Total_PosX=Cam_PosX;
		C_Total_PosY=Cam_PosY;
		C_Dis_Width=90;
		C_Dis_Height=90;
		C_Total_Width=((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.015;
		C_Total_Height=((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.015;


		C_RECT[0].m_PosX=Cam_PosX-C_Dis_Width;
		C_RECT[0].m_PosY=Cam_PosY-C_Dis_Height;
		C_RECT[0].m_Width=C_Total_Width;
		C_RECT[0].m_Height=C_Total_Height;

		C_RECT[1].m_PosX=Cam_PosX+C_Dis_Width;
		C_RECT[1].m_PosY=Cam_PosY-C_Dis_Height;
		C_RECT[1].m_Width=C_Total_Width;
		C_RECT[1].m_Height=C_Total_Height;

		C_RECT[2].m_PosX=Cam_PosX-C_Dis_Width;
		C_RECT[2].m_PosY=Cam_PosY+C_Dis_Height;
		C_RECT[2].m_Width=C_Total_Width;
		C_RECT[2].m_Height=C_Total_Height;

		C_RECT[3].m_PosX=Cam_PosX+C_Dis_Width;
		C_RECT[3].m_PosY=Cam_PosY+C_Dis_Height;
		C_RECT[3].m_Width=C_Total_Width;
		C_RECT[3].m_Height=C_Total_Height;

		//-----------------------------
		C_Original[0].m_PosX=Cam_PosX-C_Dis_Width;
		C_Original[0].m_PosY=Cam_PosY-C_Dis_Height;
		C_Original[1].m_PosX=Cam_PosX+C_Dis_Width;
		C_Original[1].m_PosY=Cam_PosY-C_Dis_Height;
		C_Original[2].m_PosX=Cam_PosX-C_Dis_Width;
		C_Original[2].m_PosY=Cam_PosY+C_Dis_Height;
		C_Original[3].m_PosX=Cam_PosX+C_Dis_Width;
		C_Original[3].m_PosY=Cam_PosY+C_Dis_Height;

		AutomationMod =0;
		
		for(int t=0; t<4; t++){
			C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);//////C
			C_RECT[t].COLOR = "BL";

		}

		Save_parameter();
	}else{

		strTitle.Empty();
		strTitle="COLOR_REVERSAL_INIT";
		C_Total_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,C_filename);
		if(C_Total_PosX == -1){
			C_Total_PosX =Cam_PosX;
		}
		C_Total_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,C_filename);
		if(C_Total_PosY == -1){
			C_Total_PosY =Cam_PosY;
		}
		C_Dis_Width=GetPrivateProfileInt(str_model,strTitle+"DisW",-1,C_filename);
		if(C_Dis_Width == -1){
			C_Dis_Width =90;
		}
		C_Dis_Height=GetPrivateProfileInt(str_model,strTitle+"DisH",-1,C_filename);
		if(C_Dis_Height == -1){
			C_Dis_Height =90;
		}
		C_Total_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,C_filename);
		if(C_Total_Width == -1){
			C_Total_Width =((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.015;
		}
		C_Total_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,C_filename);
		if(C_Total_Height == -1){
			C_Total_Height =((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.015;
		}
		checkView = GetPrivateProfileCString(str_model,strTitle+"MasterState",C_filename);
		CString Mod = GetPrivateProfileCString(str_model,strTitle+"Auto",C_filename);
		if("1" == Mod){
			AutomationMod = 1;
		}
		if(("0" == Mod)||("" == Mod)){
			AutomationMod = 0;
		}
		
		for(int t=0; t<4; t++){
			if(t ==0){
				strTitle.Empty();
				strTitle="LEFT_TOP_";
			}
			if(t ==1){
				strTitle.Empty();
				strTitle="RIGHT_TOP_";		
			}
			if(t ==2){
				strTitle.Empty();
				strTitle="LEFT_BOTTOM_";
			}
			if(t ==3){
				strTitle.Empty();
				strTitle="RIGHT_BOTTOM_";
			}
			C_RECT[t].m_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,C_filename);
			if(C_RECT[t].m_PosX == -1){
				C_Total_PosX=Cam_PosX;
				C_Total_PosY=Cam_PosY;
				C_Dis_Width=90;
				C_Dis_Height=90;
				C_Total_Width=((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.015;
				C_Total_Height=((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.015;


				C_RECT[0].m_PosX=Cam_PosX-C_Dis_Width;
				C_RECT[0].m_PosY=Cam_PosY-C_Dis_Height;
				C_RECT[0].m_Width=C_Total_Width;
				C_RECT[0].m_Height=C_Total_Height;

				C_RECT[1].m_PosX=Cam_PosX+C_Dis_Width;
				C_RECT[1].m_PosY=Cam_PosY-C_Dis_Height;
				C_RECT[1].m_Width=C_Total_Width;
				C_RECT[1].m_Height=C_Total_Height;

				C_RECT[2].m_PosX=Cam_PosX-C_Dis_Width;
				C_RECT[2].m_PosY=Cam_PosY+C_Dis_Height;
				C_RECT[2].m_Width=C_Total_Width;
				C_RECT[2].m_Height=C_Total_Height;

				C_RECT[3].m_PosX=Cam_PosX+C_Dis_Width;
				C_RECT[3].m_PosY=Cam_PosY+C_Dis_Height;
				C_RECT[3].m_Width=C_Total_Width;
				C_RECT[3].m_Height=C_Total_Height;

				//-----------------------------
				C_Original[0].m_PosX=Cam_PosX-C_Dis_Width;
				C_Original[0].m_PosY=Cam_PosY-C_Dis_Height;
				C_Original[1].m_PosX=Cam_PosX+C_Dis_Width;
				C_Original[1].m_PosY=Cam_PosY-C_Dis_Height;
				C_Original[2].m_PosX=Cam_PosX-C_Dis_Width;
				C_Original[2].m_PosY=Cam_PosY+C_Dis_Height;
				C_Original[3].m_PosX=Cam_PosX+C_Dis_Width;
				C_Original[3].m_PosY=Cam_PosY+C_Dis_Height;

				for(int t=0; t<4; t++){
					C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);//////C
					C_RECT[t].COLOR = "BL";
				}

				Save_parameter();
			}
			C_Original[t].m_PosX = C_RECT[t].m_PosX;
			C_RECT[t].m_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,C_filename);
			C_Original[t].m_PosY = C_RECT[t].m_PosY;
			C_RECT[t].m_Left=GetPrivateProfileInt(str_model,strTitle+"StartX",-1,C_filename);
			C_RECT[t].m_Top=GetPrivateProfileInt(str_model,strTitle+"StartY",-1,C_filename);
			C_RECT[t].m_Right=GetPrivateProfileInt(str_model,strTitle+"ExitX",-1,C_filename);
			C_RECT[t].m_Bottom=GetPrivateProfileInt(str_model,strTitle+"ExitY",-1,C_filename);
			C_RECT[t].m_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,C_filename);
			C_RECT[t].m_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,C_filename);	
			C_RECT[t].COLOR = GetPrivateProfileCString(str_model,strTitle+"COLOR",C_filename);
			
			Standard_CD_RECT[t].m_resultX = C_RECT[t].m_Left + C_RECT[t].m_Width/2;
			Standard_CD_RECT[t].m_resultY = C_RECT[t].m_Top + C_RECT[t].m_Height/2;
		}
	}

	Save_parameter();
}
void CColorReversal_Option::Change_DATA(){

		CString str;
		
		((CEdit *)GetDlgItemText(IDC_EDIT_COLOR_REVERSAL, str));
		C_DATALIST.SetItemText(iSavedItem, iSavedSubitem, str);

		int num = _ttoi(str);
		if(num < 0){
			num =0;
		}

		if(iSavedSubitem ==1){
			C_RECT[iSavedItem].m_PosX = num;
			
		}
		if(iSavedSubitem ==2){
			C_RECT[iSavedItem].m_PosY = num;
			
		}
		if(iSavedSubitem ==3){
			C_RECT[iSavedItem].m_Left = num;
			C_RECT[iSavedItem].m_PosX=(C_RECT[iSavedItem].m_Right+1 +C_RECT[iSavedItem].m_Left) /2;
			C_RECT[iSavedItem].m_Width=C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;

		}
		if(iSavedSubitem ==4){
			C_RECT[iSavedItem].m_Top = num;
			C_RECT[iSavedItem].m_PosY=(C_RECT[iSavedItem].m_Top +C_RECT[iSavedItem].m_Bottom+1)/2;
			C_RECT[iSavedItem].m_Height=C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;

		}
		if(iSavedSubitem ==5){
			if(num == 0){
				C_RECT[iSavedItem].m_Right = 0;
			}
			else{
				C_RECT[iSavedItem].m_Right = num;
				C_RECT[iSavedItem].m_PosX=(C_RECT[iSavedItem].m_Right +C_RECT[iSavedItem].m_Left+1) /2;
				C_RECT[iSavedItem].m_Width=C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;
			}
		}
		if(iSavedSubitem ==6){
			if(num ==0){
				C_RECT[iSavedItem].m_Bottom =0;
			}
			else{
				C_RECT[iSavedItem].m_Bottom = num;
				C_RECT[iSavedItem].m_PosY=(C_RECT[iSavedItem].m_Top +C_RECT[iSavedItem].m_Bottom+1)/2;
				C_RECT[iSavedItem].m_Height=C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;
			}
		}
		if(iSavedSubitem ==7){
			C_RECT[iSavedItem].m_Width = num;
		}
		if(iSavedSubitem ==8){
			C_RECT[iSavedItem].m_Height = num;
		}
		
		C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
			
		if(C_RECT[iSavedItem].m_Left < 0){//////////////////////수정
			C_RECT[iSavedItem].m_Left = 0;
			C_RECT[iSavedItem].m_Width = C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Right >m_CAM_SIZE_WIDTH){
			C_RECT[iSavedItem].m_Right = m_CAM_SIZE_WIDTH;
			C_RECT[iSavedItem].m_Width = C_RECT[iSavedItem].m_Right+1 -C_RECT[iSavedItem].m_Left;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Top< 0){
			C_RECT[iSavedItem].m_Top = 0;
			C_RECT[iSavedItem].m_Height = C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Bottom >m_CAM_SIZE_HEIGHT){
			C_RECT[iSavedItem].m_Bottom = m_CAM_SIZE_HEIGHT;
			C_RECT[iSavedItem].m_Height = C_RECT[iSavedItem].m_Bottom+1 -C_RECT[iSavedItem].m_Top;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Height<= 0){
			C_RECT[iSavedItem].m_Height = 1;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Height >=m_CAM_SIZE_HEIGHT){
			C_RECT[iSavedItem].m_Height = m_CAM_SIZE_HEIGHT;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}

		if(C_RECT[iSavedItem].m_Width <= 0){
			C_RECT[iSavedItem].m_Width = 1;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}
		if(C_RECT[iSavedItem].m_Width >=m_CAM_SIZE_WIDTH){
			C_RECT[iSavedItem].m_Width = m_CAM_SIZE_WIDTH;
			C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);
		}



		C_RECT[iSavedItem].EX_RECT_SET(C_RECT[iSavedItem].m_PosX,C_RECT[iSavedItem].m_PosY,C_RECT[iSavedItem].m_Width,C_RECT[iSavedItem].m_Height);

	

		CString data = "";

		if(iSavedSubitem ==1){
			data.Format("%d",C_RECT[iSavedItem].m_PosX);			
		}
		else if(iSavedSubitem ==2){
			data.Format("%d",C_RECT[iSavedItem].m_PosY);
		}
		else if(iSavedSubitem ==3){
			data.Format("%d",C_RECT[iSavedItem].m_Left);			
		}
		else if(iSavedSubitem ==4){
			data.Format("%d",C_RECT[iSavedItem].m_Top);			
		}
		else if(iSavedSubitem ==5){
			data.Format("%d",C_RECT[iSavedItem].m_Right);			
		}
		else if(iSavedSubitem ==6){
			data.Format("%d",C_RECT[iSavedItem].m_Bottom);			
		}
		else if(iSavedSubitem ==7){
			data.Format("%d",C_RECT[iSavedItem].m_Width);			
		}
		else if(iSavedSubitem ==8){
			data.Format("%d",C_RECT[iSavedItem].m_Height);			
		}		
		((CEdit *)GetDlgItem(IDC_EDIT_COLOR_REVERSAL))->SetWindowText(data);

}


bool CColorReversal_Option::AveRGBGen(LPBYTE IN_RGB,int NUM){//AVE값 및 평균 RGB값 추출 및 그리기.
	
	//m_Success = FALSE;
	C_RECT[NUM].m_Success = FALSE;
	BYTE *BW;
	BYTE R = 0,G = 0,B = 0;
	DWORD	RGBPIX = 0;
	DWORD	Total_R = 0,Total_G = 0,Total_B = 0;
	int index=0;
//	if(RGBRect.Chkdata() == FALSE){return 0;}
		
	DWORD Total = C_RECT[NUM].Height()*C_RECT[NUM].Length();
	BW = new BYTE[Total];
	memset(BW,0,sizeof(BW));
	int startx = C_RECT[NUM].m_Left;
	int starty = C_RECT[NUM].m_Top;
	int endx = startx + C_RECT[NUM].Length();
	int endy = starty + C_RECT[NUM].Height();
	DWORD RGBLINE = starty * (m_CAM_SIZE_WIDTH*4);
		
	for(int lopy = starty;lopy < endy;lopy++){
		RGBPIX = startx * 4;
		for(int lopx = startx;lopx < endx;lopx++){
			B = IN_RGB[RGBLINE + RGBPIX];  
			G = IN_RGB[RGBLINE + RGBPIX + 1];
			R = IN_RGB[RGBLINE + RGBPIX + 2];
			Total_R += R;
			Total_G += G;
			Total_B += B;
			index++;
			RGBPIX+=4;
		}
		RGBLINE+=(m_CAM_SIZE_WIDTH*4);
	}
	m_AVER = (BYTE)(Total_R/index);
	m_AVEG = (BYTE)(Total_G/index);
	m_AVEB = (BYTE)(Total_B/index);

	C_RECT[NUM].m_R=m_AVER;
	C_RECT[NUM].m_G=m_AVEG;
	C_RECT[NUM].m_B=m_AVEB;

	//if(C_RECT[NUM].m_R <50 && C_RECT[NUM].m_G<50&& C_RECT[NUM].m_B<50){
	//	C_RECT[NUM].m_Success = FALSE;
	//	delete BW;
	//	return C_RECT[NUM].m_Success;
	//}
	ColorReversal_TEST(C_RECT[NUM].m_R ,C_RECT[NUM].m_G,C_RECT[NUM].m_B, NUM);/////////컬러 판단

	if(MasterMod == FALSE){
		if(C_RECT[NUM].COLOR ==C_RECT[NUM].COLORresult){
			C_RECT[NUM].m_Success = TRUE;
		} 
		else{
			C_RECT[NUM].m_Success = FALSE;
		}
	}
////////////////////////////////////////////////////////////////////////////////////////LOT




	delete BW;
	return C_RECT[NUM].m_Success;
}


void CColorReversal_Option::AveRGB(LPBYTE IN_RGB,int NUM){//AVE값 및 평균 RGB값 추출 및 그리기.
	
	//m_Success = FALSE;
	C_RECT[NUM].m_Success = FALSE;
	BYTE *BW;
	BYTE R = 0,G = 0,B = 0;
	DWORD	RGBPIX = 0;
	DWORD	Total_R = 0,Total_G = 0,Total_B = 0;
	int index=0;
//	if(RGBRect.Chkdata() == FALSE){return 0;}
		
	DWORD Total = C_RECT[NUM].Height()*C_RECT[NUM].Length();
	BW = new BYTE[Total];
	memset(BW,0,sizeof(BW));
	int startx = C_RECT[NUM].m_Left;
	int starty = C_RECT[NUM].m_Top;
	int endx = startx + C_RECT[NUM].Length();
	int endy = starty + C_RECT[NUM].Height();
	DWORD RGBLINE = starty * (m_CAM_SIZE_WIDTH*4);
		
	for(int lopy = starty;lopy < endy;lopy++){
		RGBPIX = startx * 4;
		for(int lopx = startx;lopx < endx;lopx++){
			B = IN_RGB[RGBLINE + RGBPIX];  
			G = IN_RGB[RGBLINE + RGBPIX + 1];
			R = IN_RGB[RGBLINE + RGBPIX + 2];
			Total_R += R;
			Total_G += G;
			Total_B += B;
			index++;
			RGBPIX+=4;
		}
		RGBLINE+=(m_CAM_SIZE_WIDTH*4);
	}
	m_AVER = (BYTE)(Total_R/index);
	m_AVEG = (BYTE)(Total_G/index);
	m_AVEB = (BYTE)(Total_B/index);
	

	C_RECT[NUM].m_resultR=m_AVER;
	C_RECT[NUM].m_resultG=m_AVEG;
	C_RECT[NUM].m_resultB=m_AVEB;
	delete BW;

}
void CColorReversal_Option::ColorReversal_TEST(int R, int G ,int B, int NUM){ ///빨강은 1, 파랑은 2, 노랑색은 3, 초록색은 4


	
	if(R > G && R > B){
		if(R-G>20 && R-B>20){
			//////////////////////////레드
			ColorReversal_DATA[NUM]=1;// 레드
			if(MasterMod == TRUE){
				C_RECT[NUM].COLOR = "R";
			}
			else{
				C_RECT[NUM].COLORresult = "R";		
			}
		}		
	}
	if(B>R && B>G){
		if(B-R>30 && B-G>30){
			//////////////////////////블루
			ColorReversal_DATA[NUM]=2;// 블루
			if(MasterMod == TRUE){
				C_RECT[NUM].COLOR = "B";
			}
			else{
				C_RECT[NUM].COLORresult = "B";		
			}
		}
	}
	if(G>R &&G>B/*&& ((R>B) ||(R==B))*/){
		//if(G-R>50 &&G-B>50){
			//////////////////////////그린
			ColorReversal_DATA[NUM]=4;// 그린
			if(MasterMod == TRUE){
				C_RECT[NUM].COLOR = "G";
			}
			else{
				C_RECT[NUM].COLORresult = "G";		
			}
		//}
	}


	if((R<60)&&(G<60)&&(B<60)){
		if(abs(B-R)<20 && abs(B-G)<30){
			ColorReversal_DATA[NUM]=3;// 검정
			if(MasterMod == TRUE){
				C_RECT[NUM].COLOR = "BL";
			}
			else{
				C_RECT[NUM].COLORresult = "BL";		
			}
		}
	}
	

	
}
void CColorReversal_Option::Check_RGBVIEW(CString check){

	int ch1=0,ch2=0,ch3=0,ch4=0;
	
		ch1 = ColorReversal_DATA[0];
		ch2 = ColorReversal_DATA[1];
		ch3 = ColorReversal_DATA[2];
		ch4 = ColorReversal_DATA[3];

		if(ch1==1&&ch2==2&&ch3==3&&ch4==4){
			check.Empty();
			check= "좌우반전";
		}
		else if(ch1==2&&ch2==1&&ch3==4&&ch4==3){
			check.Empty();
			check= "오리지널";
		}
		else if(ch1==3&&ch2==4&&ch3==1&&ch4==2){
			check.Empty();
			check= "로테이트";
		}
		else if(ch1==4&&ch2==3&&ch3==2&&ch4==1){
			check.Empty();
			check= "상하반전";
		}
		else{
			check.Empty();
			check= "에러";
		}

		if(MasterMod == TRUE){
			checkView=check;
			MasterState(0,check);

		}
		else{
			checkViewResult=check;

		}
		
	
}

bool CColorReversal_Option::AveRGBPic(CDC *cdc,int NUM){//AVE값 및 평균 RGB값 추출 및 그리기.
	
	CPen	my_Pan,*old_pan;
	CString TEXTDATA ="";

	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);
	
	//if(C_RECT[NUM].Chkdata() == FALSE){return 0;}
	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	if(C_RECT[NUM].m_Success == TRUE){
		my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(BLUE_COLOR);
	}else{
		my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(RED_COLOR);
	}

	cdc->MoveTo(C_RECT[NUM].m_Left,C_RECT[NUM].m_Top);
	cdc->LineTo(C_RECT[NUM].m_Right,C_RECT[NUM].m_Top);
	cdc->LineTo(C_RECT[NUM].m_Right,C_RECT[NUM].m_Bottom);
	cdc->LineTo(C_RECT[NUM].m_Left,C_RECT[NUM].m_Bottom);
	cdc->LineTo(C_RECT[NUM].m_Left,C_RECT[NUM].m_Top);

	
	CFont m_font, *old_font;
	m_font.CreatePointFont(100,"Arial");
	old_font = cdc->SelectObject(&m_font);
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);


	TEXTDATA.Format("R:%03d G:%03d B:%03d",C_RECT[NUM].m_R,C_RECT[NUM].m_G,C_RECT[NUM].m_B);
	cdc->TextOut(C_RECT[NUM].m_PosX,C_RECT[NUM].m_PosY-(C_RECT[NUM].m_Height/2)-5,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	cdc->SelectObject(old_font);
	old_font->DeleteObject();
	m_font.DeleteObject();

	m_font.CreatePointFont(300,"Arial");
	old_font = cdc->SelectObject(&m_font);
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);
	if(MasterMod == TRUE){
		cdc->TextOut(Cam_PosX,(Cam_PosY+200),checkView.GetBuffer(0),checkView.GetLength());
	}
	else{
		cdc->TextOut(Cam_PosX,(Cam_PosY+200),checkViewResult.GetBuffer(0),checkViewResult.GetLength());
		
	}
	cdc->SelectObject(old_pan);
	old_pan->DeleteObject();
	my_Pan.DeleteObject();

	cdc->SelectObject(old_font);
	old_font->DeleteObject();
	m_font.DeleteObject();
	
	return TRUE;
}



void CModel_Create(CColorReversal_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO){
	CRect OptionRect;
	if(pTabWnd != NULL){
		((CColorReversal_Option *)*pWnd) = new CColorReversal_Option(pMomWnd);
		((CColorReversal_Option *)*pWnd)->Setup(pMomWnd,pEdit,INFO);
		((CColorReversal_Option *)*pWnd)->Create(((CColorReversal_Option *)*pWnd)->IDD,pTabWnd);//&m_CtrTab);
		((CColorReversal_Option *)*pWnd)->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		((CColorReversal_Option *)*pWnd)->MoveWindow(20,40,OptionRect.Width(),OptionRect.Height());
	}
}

void CModel_Delete(CColorReversal_Option **pWnd){
	if(((CColorReversal_Option *)*pWnd) != NULL){
//		((CColorReversal_Option *)*pWnd)->KillTimer(110);
		((CColorReversal_Option *)*pWnd)->DestroyWindow();
		delete ((CColorReversal_Option *)*pWnd);
		((CColorReversal_Option *)*pWnd) = NULL;	
	}
}

tResultVal CColorReversal_Option::Run()
{	
	CString str="";
	b_StopFail = FALSE;
		if(ChangeCheck == TRUE){
			OnBnClickedButtonReversalSave(); 
		}
		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			InsertList();
		}
			int camcnt = 0;
		tResultVal retval = {0,};
	
		//StatePrintf("컬러 반전 검사를 시작합니다");

		int count = 0;



		/*int loop_cnt=0;

		ColorReversalKeepRun =btn_ColorReversalKeepRun.GetCheck();

		while(loop_cnt < 1 || ColorReversalKeepRun)
		{*/
			 count = 0;
			((CImageTesterDlg *)m_pMomWnd)->m_ImgScan =1;						
				
			for(int i =0;i<10;i++){
				if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0){
					break;
				}else{
					DoEvents(50);
				}
			}	


				if(AutomationMod ==1){
				// 오토메이션 모드
				//C_CHECKING[] PosX,PosY 받은 거 체킹	
				
					//Automation_DATA_INPUT();// 성공하면 데이터 C_RECT에 넣기.
					//실패하면 그냥 진행...

					GetColorAreaCoordinate(m_RGBScanbuf);

					BOOL FLAG = TRUE;

					for(int lop=0; lop<4; lop++)
					{
						C_CHECKING[lop].m_PosX = CD_RECT[lop].m_resultX;
						C_CHECKING[lop].m_PosY = CD_RECT[lop].m_resultY;					
						C_CHECKING[lop].m_Success = CD_RECT[lop].m_Success;					

						if(!C_CHECKING[lop].m_Success)
						{
							FLAG = FALSE;
							break;
						}
					}	
					
					if(FLAG)
						Automation_DATA_INPUT();
					else{Load_parameter();}
				}

				for(int lop=0; lop<4;lop++){
					AveRGBGen(m_RGBScanbuf,lop);
					
					if(C_RECT[lop].m_Success== TRUE){
						count++;
					}
				}	
			

				CString stateDATA ="역상_ ";
			Check_RGBVIEW(checkViewResult);

		
			/*ColorReversalKeepRun =btn_ColorReversalKeepRun.GetCheck();
			loop_cnt++;

		}*/
		//StatePrintf("컬러 반전 검사가 종료되었습니다");

		////////
		((CImageTesterDlg *)m_pMomWnd)->m_pResColorRvsOptWnd->MASTER_VALUE(checkView);
		((CImageTesterDlg *)m_pMomWnd)->m_pResColorRvsOptWnd->TEST_VALUE(checkViewResult);
		((CImageTesterDlg *)m_pMomWnd)->m_pResColorRvsOptWnd->LT_VALUE(C_RECT[0].COLOR);
		((CImageTesterDlg *)m_pMomWnd)->m_pResColorRvsOptWnd->RT_VALUE(C_RECT[1].COLOR);
		((CImageTesterDlg *)m_pMomWnd)->m_pResColorRvsOptWnd->LB_VALUE(C_RECT[2].COLOR);
		((CImageTesterDlg *)m_pMomWnd)->m_pResColorRvsOptWnd->RB_VALUE(C_RECT[3].COLOR);

		CString master = _T(checkView);
		CString test = _T(checkViewResult);
		if(test != "에러"){
			if(master.CompareNoCase(test) == 0){
				((CImageTesterDlg *)m_pMomWnd)->m_pResColorRvsOptWnd->RV_VALUE(1,"PASS");
			}else if(master.CompareNoCase(test) == -1){
				((CImageTesterDlg *)m_pMomWnd)->m_pResColorRvsOptWnd->RV_VALUE(2,"FAIL");
			}
		}else{
			((CImageTesterDlg *)m_pMomWnd)->m_pResColorRvsOptWnd->RV_VALUE(2,"FAIL");
		
		}


		////////

		retval.m_ID = 0x07;
		retval.ValString.Empty();
		
		retval.ValString.Format("Master_"+checkView +"-> "+checkViewResult);
		stateDATA += retval.ValString;
		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
		m_ColorReversalList.SetItemText(InsertIndex,4,retval.ValString);
		}
		
		if(checkView == "" || checkViewResult == "에러"){
			stateDATA += " _ FAIL";
			retval.m_Success = FALSE;
			m_ColorReversalList.SetItemText(InsertIndex,5,"FAIL");
			((CImageTesterDlg *)m_pMomWnd)->StatePrintf(stateDATA);

			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
				StartCnt++;
			}
			return retval;
		}

	
		
		if(count ==4){
			retval.m_Success = TRUE;
			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,9,"PASS");
			m_ColorReversalList.SetItemText(InsertIndex,5,"PASS");
			}
			stateDATA += " _ PASS";
			
		}else{
			stateDATA += " _ FAIL";
			retval.m_Success = FALSE;
			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,9,"FAIL");
			m_ColorReversalList.SetItemText(InsertIndex,5,"FAIL");
			}
			
		}
		((CImageTesterDlg *)m_pMomWnd)->StatePrintf(stateDATA);
		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			StartCnt++;
		}
	
		m_retval = retval;

		return retval;
	
}	
void CColorReversal_Option::FAIL_UPLOAD(){
	if ((((CImageTesterDlg *)m_pMomWnd)->LotMod == 1) && (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)){
	
		InsertList();
	
		m_ColorReversalList.SetItemText(InsertIndex,4,"X");

		m_ColorReversalList.SetItemText(InsertIndex,5,"FAIL");
		
		m_ColorReversalList.SetItemText(InsertIndex,6,"STOP");
		//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,9,"STOP");
		StartCnt++;
		b_StopFail = TRUE;
	}
}


void CColorReversal_Option::Automation_DATA_INPUT(){

	for(int t=0; t<4; t++){
		C_RECT[t].m_PosX=C_CHECKING[t].m_PosX;
		C_RECT[t].m_PosY=C_CHECKING[t].m_PosY;
		C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);

		if(C_RECT[t].m_Left < 0){//////////////////////수정
			C_RECT[t].m_Left = 0;
			C_RECT[t].m_Width = C_RECT[t].m_Right+1 -C_RECT[t].m_Left;
			C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);
		}
		if(C_RECT[t].m_Right >m_CAM_SIZE_WIDTH){
			C_RECT[t].m_Right = m_CAM_SIZE_WIDTH;
			C_RECT[t].m_Width = C_RECT[t].m_Right+1 -C_RECT[t].m_Left;
			C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);
		}
		if(C_RECT[t].m_Top< 0){
			C_RECT[t].m_Top = 0;
			C_RECT[t].m_Height = C_RECT[t].m_Bottom+1 -C_RECT[t].m_Top;
			C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);
		}
		if(C_RECT[t].m_Bottom >m_CAM_SIZE_HEIGHT){
			C_RECT[t].m_Bottom = m_CAM_SIZE_HEIGHT;
			C_RECT[t].m_Height = C_RECT[t].m_Bottom+1 -C_RECT[t].m_Top;
			C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);
		}
		C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);
	}


}
void CColorReversal_Option::Pic(CDC *cdc)
{
	for(int lop=0;lop<4;lop++){
		AveRGBPic(cdc,lop);
	}
}

void CColorReversal_Option::InitPrm()  
{
	Load_parameter();
	MasterState(0,checkView);
	UpdateData(FALSE);
	UploadList();

	for(int t=0; t<4;t++){
		C_RECT[t].m_Success = FALSE;
		C_RECT[t].m_R =0;
		C_RECT[t].m_G =0;
		C_RECT[t].m_B =0;

	}
	checkViewResult ="";
}


void CColorReversal_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

void CColorReversal_Option::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",C_filename);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

//---------------------------------------------------------------------WORKLIST
void CColorReversal_Option::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	InsertIndex = m_ColorReversalList.InsertItem(StartCnt,"",0);
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_ColorReversalList.SetItemText(InsertIndex,0,strCnt);
	
	m_ColorReversalList.SetItemText(InsertIndex,1,((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_ColorReversalList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_ColorReversalList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");


}
void CColorReversal_Option::Set_List(CListCtrl *List){
	

	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"역상 상태",LVCFMT_CENTER, 80);
    List->InsertColumn(5,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"비고",LVCFMT_CENTER, 80);

	ListItemNum=7;
	Copy_List(&m_ColorReversalList,((CImageTesterDlg *)m_pMomWnd)->m_pWorkList,ListItemNum);
}

CString CColorReversal_Option::Mes_Result()
{
	CString sz;

	int		nData = 0;
	int		nResult = 0;

	int nSel = m_ColorReversalList.GetItemCount()-1;

	// 역상상태
	sz = m_ColorReversalList.GetItemText(nSel, 5);
	
	if(m_retval.m_Success)
		nResult = 1;
	else 
		nResult = 0;

	if(b_StopFail == FALSE){
	m_szMesResult.Format(_T("%s:%d"), sz, nResult);
	}else{
		m_szMesResult.Format(_T(":1"));	
	}

	return m_szMesResult;
}

void CColorReversal_Option::EXCEL_SAVE(){
	CString Item[1]={"역상 상태"};

	CString Data ="";
	CString str="";
	str = "***********************컬러 반전 검사***********************\n";
	Data += str;
	str.Empty();
	str= "Master 모드, "+checkView+",\n";
	Data += str;
	str.Empty();
	str.Format("Left Top ,"+C_RECT[0].COLOR+",/,Right Top ,"+C_RECT[1].COLOR+",/,Left Bottom,"+C_RECT[2].COLOR+",/,Right Bottom,"+C_RECT[3].COLOR+",\n");
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("반전검사",Item,1,&m_ColorReversalList,Data);
}

bool CColorReversal_Option::EXCEL_UPLOAD(){
	m_ColorReversalList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("반전검사",&m_ColorReversalList,1,&StartCnt)){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return TRUE;
}

void CColorReversal_Option::OnBnClickedButtonSetzone()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeCheck=1;
	UpdateData(TRUE);
	if(AutomationMod ==0){
	PosX=C_Total_PosX;
	PosY=C_Total_PosY;
	
	C_RECT[0].m_PosX = PosX-C_Dis_Width;
	C_RECT[0].m_PosY = PosY-C_Dis_Height;
	C_RECT[1].m_PosX = PosX+C_Dis_Width;
	C_RECT[1].m_PosY = PosY-C_Dis_Height;
	C_RECT[2].m_PosX = PosX-C_Dis_Width;
	C_RECT[2].m_PosY = PosY+C_Dis_Height;
	C_RECT[3].m_PosX = PosX+C_Dis_Width;
	C_RECT[3].m_PosY = PosY+C_Dis_Height;
	}
	for(int t=0; t<4; t++){
		C_RECT[t].m_Width= C_Total_Width;
		C_RECT[t].m_Height=C_Total_Height;
		C_RECT[t].EX_RECT_SET(C_RECT[t].m_PosX,C_RECT[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);//////C

	}
	UpdateData(FALSE);

	UploadList();//리스트컨트롤의 입력

	for(int t=0; t<4; t++){
		ChangeItem[t]=t;
	}

	for(int t=0; t<4; t++){
		C_DATALIST.Update(t);
	}
		((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_SAVE))->EnableWindow(1);


//	Save_parameter();

}

void CColorReversal_Option::OnBnClickedButtonReversalSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Save_parameter();
	ChangeCheck =0;
	for(int t=0; t<4; t++){
		ChangeItem[t]=-1;
	}
	for(int t=0; t<4; t++){
		C_DATALIST.Update(t);
	}
}

void CColorReversal_Option::OnBnClickedButtonReversalMaster()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE_CHK(m_INFO.Number);
//	((CImageTesterDlg *)m_pMomWnd)->OverLay_Off();



	ChangeCheck=1;
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;
	MasterMod =TRUE;

	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan =1;						
			
	for(int i =0;i<10;i++){
		if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0){
			break;
		}else{
			DoEvents(50);
		}
	}		
		
	if(AutomationMod ==1){
	// 오토메이션 모드
	//C_CHECKING[] PosX,PosY 받은 거 체킹	
	//Automation_DATA_INPUT();// 성공하면 데이터 C_RECT에 넣기.
	//실패하면 그냥 진행...
		GetColorAreaCoordinate(m_RGBScanbuf);
		
		BOOL FLAG = TRUE;

		for(int lop=0; lop<4; lop++)
		{
			C_CHECKING[lop].m_PosX = CD_RECT[lop].m_resultX;
			C_CHECKING[lop].m_PosY = CD_RECT[lop].m_resultY;					
			C_CHECKING[lop].m_Success = CD_RECT[lop].m_Success;					

			if(!C_CHECKING[lop].m_Success)
			{
				FLAG = FALSE;
				break;
			}
		}	
		
		if(FLAG)
			Automation_DATA_INPUT();
		else{Load_parameter();}
	}/*else{
		Load_Original_pra();
	}*/


	for(int lop=0; lop<4;lop++){
		C_RECT[lop].COLOR="";
		AveRGBGen(m_RGBScanbuf,lop);	
	}	
	
	Check_RGBVIEW(checkView);
	UploadList();

	MasterMod =FALSE;
	

	for(int t=0; t<4; t++){
		ChangeItem[t]=t;
	}
	for(int t=0; t<4; t++){
		C_DATALIST.Update(t);
	}
	((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_SAVE))->EnableWindow(1);


}

void CColorReversal_Option::Load_Original_pra(){

	for(int t=0; t<4; t++){
		C_RECT[t].EX_RECT_SET(C_Original[t].m_PosX,C_Original[t].m_PosY,C_RECT[t].m_Width,C_RECT[t].m_Height);//////C
	}

}

void CColorReversal_Option::RGB_Judgment(){
	int R=0,G=0,B=0;
	
	for(int t =0; t< 4; t++){
		R=C_RECT[t].m_resultR;
		G=C_RECT[t].m_resultG;
		B=C_RECT[t].m_resultB;
		if(R>B && G>B){
			if(R-G<50 && B<50){
				ColorReversal_DATA[t]=3;// 빨강
				if(MasterMod == TRUE){
					C_RECT[t].COLOR = "Y";
				}
			}
			if(G-R<50 && B<50){
				ColorReversal_DATA[t]=3;// 빨강
				if(MasterMod == TRUE){
					C_RECT[t].COLOR = "Y";
				}
			}
		}
		
		if(R > G && R > B){
			if(R-G>50 && R-B>50){
				//////////////////////////레드
				ColorReversal_DATA[t]=1;// 빨강
				if(MasterMod == TRUE){
					C_RECT[t].COLOR = "R";
				}
			}		
		}
		if(B>R && B>G){
			if(B-R>50 && B-G>50){
				//////////////////////////블루
				ColorReversal_DATA[t]=2;// 빨강
				if(MasterMod == TRUE){
					C_RECT[t].COLOR = "B";
				}
			}
		}
		if(G>R &&G>B){
			if(G-R>50 &&G-B>50){
				//////////////////////////그린
				ColorReversal_DATA[t]=4;// 빨강
				if(MasterMod == TRUE){
					C_RECT[t].COLOR = "G";
				}
			}
		}
		
	}


}
HBRUSH CColorReversal_Option::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_MasterState){
		if (Master_text == 0){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
		}
	}
	return hbr;
}

void CColorReversal_Option::MasterState(unsigned char col,LPCSTR lpcszString, ...){

	Master_text = col;
	((CEdit *)GetDlgItem(IDC_EDIT_MasterState))->SetWindowText(lpcszString);
}

void CColorReversal_Option::OnBnClickedCheckReversalAuto()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(AutomationMod ==0){
		
		AutomationMod =1;
		//C_DATALIST.EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_POSX)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_POSY)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_DISW)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_DISH)->EnableWindow(0);
		//(CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_W)->EnableWindow(0);
		//(CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_H)->EnableWindow(0);
//		(CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_SAVE)->EnableWindow(0);
		//(CButton *)GetDlgItem(IDC_BUTTON_SETZONE)->EnableWindow(0);
		
		//////////////////////////////////////////////////////////////////////오토메이션 중심점으로 영역 잡는 소스 추가해야함.
	}
	else{
		AutomationMod =0;
		//C_DATALIST.EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_POSX)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_POSY)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_DISW)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_DISH)->EnableWindow(1);
		//(CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_W)->EnableWindow(1);
		//(CEdit *)GetDlgItem(IDC_EDIT_REVERSAL_H)->EnableWindow(1);
//		(CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_SAVE)->EnableWindow(1);
		//(CButton *)GetDlgItem(IDC_BUTTON_SETZONE)->EnableWindow(1);
	
	}
	for(int t=0; t<4; t++){
			C_DATALIST.Update(t);
		}
	CString strTitle;
	strTitle.Empty();
	strTitle="COLOR_REVERSAL_INIT";
	if(AutomationMod ==1){
		WritePrivateProfileString(str_model,strTitle+"Auto","1",C_filename);}
	if(AutomationMod ==0){
		WritePrivateProfileString(str_model,strTitle+"Auto","0",C_filename);}
}
void CColorReversal_Option::OnNMDblclkListReversal(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		return;
	}
	
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Invalidate();

	
		iSavedItem = pNMITEM->iItem;
		iSavedSubitem = pNMITEM->iSubItem;

	

	if(pNMITEM->iItem != -1)
	{
		if(pNMITEM->iSubItem != 0 && pNMITEM->iSubItem <9)
		{		
			C_DATALIST.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			C_DATALIST.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_COLOR_REVERSAL))->SetWindowText(C_DATALIST.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_COLOR_REVERSAL))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
			((CEdit *)GetDlgItem(IDC_EDIT_COLOR_REVERSAL))->SetFocus();
			
		}
	}

	

	
	*pResult = 0;
}


void CColorReversal_Option::OnNMClickListReversal(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}

void CColorReversal_Option::GetColorAreaCoordinate(LPBYTE IN_RGB)
{
	int checkPatternType[4] = {0, 0, 0, 0};

	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	CD_RECT[0].m_Success = FALSE;
	CD_RECT[1].m_Success = FALSE;
	CD_RECT[2].m_Success = FALSE;
	CD_RECT[3].m_Success = FALSE;

	IplImage *OriginImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);	
	IplImage *RGBResultImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3);

CvRect centerPtRect = GetCenterPoint(IN_RGB);

	BYTE R,G,B;
	double Sum_Y;

	for (int y=0; y<m_CAM_SIZE_HEIGHT; y++)
	{
		for (int x=0; x<m_CAM_SIZE_WIDTH; x++)
		{
			B = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 2];
			
			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;
		}
	}
	
	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0); //객체 외곽의 Overshooting으로 인하여 한번 더 처리 함..
	
	cvCvtColor(OriginImage, RGBResultImage, CV_GRAY2BGR);
	
	cvCanny(SmoothImage, CannyImage, 0, 150);		

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	int distRect1=999999;
	int distRect2=999999;
	int distRect3=999999;
	int distRect4=999999;

	CvRect rect;
	double area, arcCount;

	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);		
		
		int curr_rect_center_x = rect.x + rect.width/2;
		int curr_rect_center_y = rect.y + rect.height/2;

		double circularity = (4.0*3.14*area)/(arcCount*arcCount);		
		double WHRatio = (double)rect.width / (double)rect.height;
		double HWRatio = (double)rect.height / (double)rect.width;
		
			if( centerPtRect.x < curr_rect_center_x && centerPtRect.x+centerPtRect.x+centerPtRect.width > curr_rect_center_x && centerPtRect.y < curr_rect_center_y && centerPtRect.y+centerPtRect.y+centerPtRect.height > curr_rect_center_y )
		{
		}
		else
		{
			if(circularity < 0.95)
			{			
				if(WHRatio < 1.5 && HWRatio < 1.5 )
				{
					if( (rect.width*rect.height) * 0.7 < area )
					{
						if(curr_rect_center_x < 360 && curr_rect_center_y < 240)
						{
							double st_rect_center_x = centerPtRect.x+centerPtRect.width/2;
							double st_rect_center_y = centerPtRect.y+centerPtRect.height/2;
							double curr_rect_center_x = rect.x + rect.width/2;
							double curr_rect_center_y = rect.y + rect.height/2;

							double dist = GetDistance(st_rect_center_x, st_rect_center_y, curr_rect_center_x, curr_rect_center_y);
							B = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 0];
							G = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 1];
							R = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 2];
							
							int Color_Flag = FALSE;
							CString ColorID = GetColorNum(R, G, B);
							
							if(ColorID == C_RECT[0].COLOR)
								Color_Flag = TRUE;

							if(dist < distRect1 && dist < 150 && Color_Flag)
							{
								CD_RECT[0].m_resultX = curr_rect_center_x;
								CD_RECT[0].m_resultY = curr_rect_center_y;
	//							CD_RECT[0].m_Success = TRUE;

					//			cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8);  

								distRect1 = dist;
								checkPatternType[0] = 1;
							}
						}

						if(curr_rect_center_x > 360 && curr_rect_center_y < 240)
						{
							double st_rect_center_x = centerPtRect.x+centerPtRect.width/2;
							double st_rect_center_y = centerPtRect.y+centerPtRect.height/2;
							double curr_rect_center_x = rect.x + rect.width/2;
							double curr_rect_center_y = rect.y + rect.height/2;
							
							double dist = GetDistance(st_rect_center_x, st_rect_center_y, curr_rect_center_x, curr_rect_center_y);
							B = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 0];
							G = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 1];
							R = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 2];
							
							int Color_Flag = FALSE;
							CString ColorID = GetColorNum(R, G, B);
							
							if(ColorID == C_RECT[1].COLOR)
								Color_Flag = TRUE;

							if(dist < distRect2 && dist < 150 && Color_Flag)
							{
								CD_RECT[1].m_resultX = curr_rect_center_x;
								CD_RECT[1].m_resultY = curr_rect_center_y;
	//							CD_RECT[1].m_Success = TRUE;

						//		cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8);  

								distRect2 = dist;
								checkPatternType[1] = 1;
							}
						}					

						if(curr_rect_center_x < 360 && curr_rect_center_y > 240)
						{
							double st_rect_center_x = centerPtRect.x+centerPtRect.width/2;
							double st_rect_center_y = centerPtRect.y+centerPtRect.height/2;
							double curr_rect_center_x = rect.x + rect.width/2;
							double curr_rect_center_y = rect.y + rect.height/2;
							
							double dist = GetDistance(st_rect_center_x, st_rect_center_y, curr_rect_center_x, curr_rect_center_y);
							B = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 0];
							G = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 1];
							R = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 2];
							
							int Color_Flag = FALSE;
							CString ColorID = GetColorNum(R, G, B);
							
							if(ColorID == C_RECT[2].COLOR)
								Color_Flag = TRUE;

							if(dist < distRect3 && dist < 150 && Color_Flag)
							{
								CD_RECT[2].m_resultX = curr_rect_center_x;
								CD_RECT[2].m_resultY = curr_rect_center_y;
	//							CD_RECT[2].m_Success = TRUE;

						//		cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8);  

								distRect3 = dist;
								checkPatternType[2] = 1;
							}
						}
						
						if(curr_rect_center_x > 360 && curr_rect_center_y > 240)
						{
							double st_rect_center_x = centerPtRect.x+centerPtRect.width/2;
							double st_rect_center_y = centerPtRect.y+centerPtRect.height/2;
							double curr_rect_center_x = rect.x + rect.width/2;
							double curr_rect_center_y = rect.y + rect.height/2;
							
							double dist = GetDistance(st_rect_center_x, st_rect_center_y, curr_rect_center_x, curr_rect_center_y);
							B = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 0];
							G = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 1];
							R = IN_RGB[(int)curr_rect_center_y * 2880 + (int)curr_rect_center_x*4 + 2];
							
							int Color_Flag = FALSE;
							CString ColorID = GetColorNum(R, G, B);
							
							if(ColorID == C_RECT[3].COLOR)
								Color_Flag = TRUE;

							if(dist < distRect4 && dist < 150 && Color_Flag)
							{
								CD_RECT[3].m_resultX = curr_rect_center_x;
								CD_RECT[3].m_resultY = curr_rect_center_y;
	//							CD_RECT[3].m_Success = TRUE;

						//		cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8);  

								distRect4 = dist;
								checkPatternType[3] = 1;
							}
						}
					}
				}			
			}
		}
	}
	
	
	if(checkPatternType[0] == 1 && checkPatternType[1] == 1 && checkPatternType[2] == 1 && checkPatternType[3] == 0){
		CD_RECT[3].m_resultX = CD_RECT[1].m_resultX;
		CD_RECT[3].m_resultY = CD_RECT[2].m_resultY;
		for(int i=0; i<4; i++)
			CD_RECT[i].m_Success = TRUE;
	}
	else if(checkPatternType[0] == 1 && checkPatternType[1] == 1 && checkPatternType[2] == 0 && checkPatternType[3] == 1){
		CD_RECT[2].m_resultX = CD_RECT[0].m_resultX;
		CD_RECT[2].m_resultY = CD_RECT[3].m_resultY;
		for(int i=0; i<4; i++)
			CD_RECT[i].m_Success = TRUE;
	}
	else if(checkPatternType[0] == 0 && checkPatternType[1] == 1 && checkPatternType[2] == 1 && checkPatternType[3] == 1){
		CD_RECT[0].m_resultX = CD_RECT[2].m_resultX;
		CD_RECT[0].m_resultY = CD_RECT[1].m_resultY;
		for(int i=0; i<4; i++)
			CD_RECT[i].m_Success = TRUE;
	}		
	else if(checkPatternType[0] == 1 && checkPatternType[1] == 0 && checkPatternType[2] == 1 && checkPatternType[3] == 1){
		CD_RECT[1].m_resultX = CD_RECT[3].m_resultX;
		CD_RECT[1].m_resultY = CD_RECT[0].m_resultY;
		for(int i=0; i<4; i++)
			CD_RECT[i].m_Success = TRUE;
	}
	else if(checkPatternType[0] == 1 && checkPatternType[1] == 1 && checkPatternType[2] == 1 && checkPatternType[3] == 1){
		for(int i=0; i<4; i++)
			CD_RECT[i].m_Success = TRUE;
	}	
	else{
		for(int i=0; i<4; i++)
			CD_RECT[i].m_Success = FALSE;
	}

	for(int i=0; i<4; i++)
	{
		if(CD_RECT[i].m_Success == FALSE)
		{
			CD_RECT[i].m_resultX = Standard_CD_RECT[i].m_resultX;
			CD_RECT[i].m_resultY = Standard_CD_RECT[i].m_resultY;
		}
	}

//	cvSaveImage("C:\\ColorAreaResultImage.bmp", RGBResultImage);

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&CannyImage);	
	cvReleaseImage(&RGBResultImage);

	//delete contour;
}

double CColorReversal_Option::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt( (double)((x2-x1)*(x2-x1)) +  ((y2-y1)*(y2-y1)));

	return result;
}

void CColorReversal_Option::OnNMCustomdrawListReversal(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//*pResult = 0;

	 COLORREF text_color = 0;
        COLORREF bg_color = RGB(255, 255, 255);
     
        LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
		
	//	lplvcd->iPartId
        switch(lplvcd->nmcd.dwDrawStage){
            case CDDS_PREPAINT:
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
             
            // 배경 혹은 텍스트를 수정한다.
            case CDDS_ITEMPREPAINT:
                // 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
               /* if(lplvcd->nmcd.dwItemSpec == 0) text_color = RGB(255, 0, 0);
                else if(lplvcd->nmcd.dwItemSpec == 1) text_color = RGB(0, 255, 0);
                else text_color = RGB(0, 0, 255);
                lplvcd->clrText = text_color;*/
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
           
            // 서브 아이템의 배경 혹은 텍스트를 수정한다.
            case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
                if(lplvcd->iSubItem != 0){
                    // 1번째 행이라면...

					if(lplvcd->nmcd.dwItemSpec >=0 ||lplvcd->nmcd.dwItemSpec <4){
						if(AutomationMod ==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 7 ){
								text_color = RGB(0, 0, 0);
								bg_color = RGB(200, 200, 200);
							}
						}
						else{
							text_color = RGB(0, 0, 0);
							bg_color = RGB(255, 255, 255);
						
						}
					
					

						if(ChangeCheck==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 10 ){
								for(int t=0; t<4; t++){
									if(lplvcd->nmcd.dwItemSpec == ChangeItem[t]){
										text_color = RGB(0, 0, 0);
										bg_color = RGB(250, 230, 200);					
									}
								}
							}
						
						
						}
					}

				    lplvcd->clrText = text_color;
                    lplvcd->clrTextBk = bg_color;
					
                }
                *pResult = CDRF_NEWFONT;
                return;
        }
}

void CColorReversal_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
		ColorReversalKeepRun = FALSE;
		UpdateData(FALSE);
	}
}

void CColorReversal_Option::List_COLOR_Change(){
	
	bool FLAG = TRUE;
	int Check=0;
	for(int t=0;t<4; t++){
		if(ChangeItem[t]==0)Check++;
		if(ChangeItem[t]==1)Check++;
		if(ChangeItem[t]==2)Check++;
		if(ChangeItem[t]==3)Check++;
	}
	if(Check !=4){
		FLAG =FALSE;
		ChangeItem[changecount]=iSavedItem;
	}

	if(!FLAG){
		for(int t=0; t<changecount+1; t++){
			for(int k=0; k<changecount+1; k++){
				
				if(ChangeItem[t] == ChangeItem[k]){
					if(t==k){break;	}			
					ChangeItem[k] =-1;
				}
			
			
			}
		}
		//----------------데이터 정렬
		int temp=0;

		for(int i=0; i<4; i++)
		{
			for(int j=i+1; j<4; j++)
			{
				if(ChangeItem[i] < ChangeItem[j])  // 내림차순
				{
					temp = ChangeItem[i];
					ChangeItem[i] = ChangeItem[j];
					ChangeItem[j] = temp;
				}
			}
		}
		//--------------------------------
		for(int t=0; t<4; t++){

			if(ChangeItem[t] ==-1){
				changecount =t;
				break;
			}
		}
	}
	/*if(FLAG){
		changecount++;
	}*/

	for(int t=0; t<4; t++){
		C_DATALIST.Update(t);
	}

}

void CColorReversal_Option::OnEnKillfocusEditColorReversal()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	ChangeCheck=1;
	if((iSavedItem != -1)&&(iSavedSubitem != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_COLOR_REVERSAL, str));
		List_COLOR_Change();
		if(C_DATALIST.GetItemText(iSavedItem, iSavedSubitem) != str){
			Change_DATA();
			UploadList();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_COLOR_REVERSAL))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_COLOR_REVERSAL))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );
	iSavedItem = -1;
	iSavedSubitem = -1;
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
}

void CColorReversal_Option::OnBnClickedButtonCrload()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Load_parameter();
	MasterState(0,checkView);
	UpdateData(FALSE);
	UploadList();
}

void CColorReversal_Option::OnBnClickedCheckColorreversal()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	ColorReversalKeepRun =btn_ColorReversalKeepRun.GetCheck();
	UpdateData(FALSE);
	if(ColorReversalKeepRun == TRUE){
		AutomationMod =1;
		OnBnClickedCheckReversalAuto();
		(CButton *)GetDlgItem(IDC_CHECK_REVERSAL_AUTO)->EnableWindow(0);
		CheckDlgButton(IDC_CHECK_REVERSAL_AUTO,FALSE);
//		((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->KeepRun();
	}
	else{
		(CButton *)GetDlgItem(IDC_CHECK_REVERSAL_AUTO)->EnableWindow(1);
//		((CImageTesterDlg *)m_pMomWnd)->keepRun = FALSE;
	}
}

BOOL CColorReversal_Option::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	int znum = zDelta/120;
	CWnd *focusid;
	focusid = GetFocus();
	bool FLAG=0;
	if(znum > 0 ){
		FLAG = 1;
	}
	else if(znum < 0 ){
		FLAG =0;
	}
	else if(znum == 0){
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	}
	int casenum = focusid->GetDlgCtrlID();
	switch(casenum){
		case IDC_EDIT_REVERSAL_POSX   :
			EditWheelIntSet(IDC_EDIT_REVERSAL_POSX ,znum);
			OnEnChangeEditReversalPosx();
			break;
		case IDC_EDIT_REVERSAL_POSY :
			EditWheelIntSet(IDC_EDIT_REVERSAL_POSY,znum);
			OnEnChangeEditReversalPosy();
			break;
		case IDC_EDIT_REVERSAL_DISW :
			EditWheelIntSet(IDC_EDIT_REVERSAL_DISW ,znum);
			OnEnChangeEditReversalDisw();
			break;
		case IDC_EDIT_REVERSAL_DISH :
			EditWheelIntSet(IDC_EDIT_REVERSAL_DISH,znum);
			OnEnChangeEditReversalDish();
			break;
		case IDC_EDIT_REVERSAL_W :
			EditWheelIntSet(IDC_EDIT_REVERSAL_W,znum);
			OnEnChangeEditReversalW();
			break;
		case IDC_EDIT_REVERSAL_H :
			EditWheelIntSet(IDC_EDIT_REVERSAL_H,znum);
			OnEnChangeEditReversalH();
			break;
		
		case IDC_EDIT_COLOR_REVERSAL :
			
			if(FLAG == 1){
				if((Change_DATA_CHECK(1) == TRUE) /*|| (znum ==-1)*/){
					EditWheelIntSet(IDC_EDIT_COLOR_REVERSAL,znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditColorReversal();
				}
			}
			if(FLAG == 0){
				if((Change_DATA_CHECK(0) == TRUE)/* || (znum == 1)*/){
					EditWheelIntSet(IDC_EDIT_COLOR_REVERSAL,znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditColorReversal();
				}
			}

			break;

	}
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

bool CColorReversal_Option::Change_DATA_CHECK(bool FLAG){

		
	BOOL STAT = TRUE;
	//상한
	if(C_RECT[iSavedItem].m_Width > m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Height > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosX > m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosY > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Top > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Bottom > m_CAM_SIZE_HEIGHT  )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Left> m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Right> m_CAM_SIZE_WIDTH )
		STAT = FALSE;

	//하한

	if(C_RECT[iSavedItem].m_Width < 1 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Height < 1 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosX < 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_PosY < 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Top < 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Bottom < 0  )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Left< 0 )
		STAT = FALSE;
	if(C_RECT[iSavedItem].m_Right< 0 )
		STAT = FALSE;

	if(FLAG  == 1){
		if(C_RECT[iSavedItem].m_Width == 1 )
			STAT = FALSE;
		if(C_RECT[iSavedItem].m_Height == 1 )
			STAT = FALSE;
		
	}


	return STAT;
	

	
	

}

void CColorReversal_Option::EditWheelIntSet(int nID,int Val)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = 0;
	}else{
		buf = GetDlgItemInt(nID);
		buf+= Val;
	}
	SetDlgItemInt(nID,buf,1);
	((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
}
void CColorReversal_Option::EditMinMaxIntSet(int nID,int* Val,int Min,int Max)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = Min;
		retval = FALSE;
	}else{
		buf = GetDlgItemInt(nID);
		if(buf<Min){
			buf = *Val;
			retval = TRUE;
		}else if(buf > Max){
			buf = *Val;
			retval = TRUE;
		}else{
			retval = FALSE;
		}
	}
	*Val = buf;
	if(retval == TRUE){
		SetDlgItemInt(nID,buf,1);
		((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
	}
}


void CColorReversal_Option::OnEnChangeEditReversalPosx()
{
	EditMinMaxIntSet(IDC_EDIT_REVERSAL_POSX,&C_Total_PosX,0,m_CAM_SIZE_WIDTH);
	C_Total_PosX = GetDlgItemInt(IDC_EDIT_REVERSAL_POSX);	
	C_Total_PosY = GetDlgItemInt(IDC_EDIT_REVERSAL_POSY);
	C_Dis_Width	= GetDlgItemInt(IDC_EDIT_REVERSAL_DISW);
	C_Dis_Height = GetDlgItemInt(IDC_EDIT_REVERSAL_DISH);
	C_Total_Width = GetDlgItemInt(IDC_EDIT_REVERSAL_W);
	C_Total_Height = GetDlgItemInt(IDC_EDIT_REVERSAL_H);
	

	if((C_Total_PosX >= 0)&&(C_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(C_Total_PosY >= 0)&&(C_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_Dis_Width>=0)&&(C_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(C_Dis_Height>=0)&&(C_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Total_Width>=0)&&(C_Total_Width<m_CAM_SIZE_WIDTH)&&
		(C_Total_Height>=0)&&(C_Total_Height<m_CAM_SIZE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_SETZONE))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_SETZONE))->EnableWindow(0);
	}
}

void CColorReversal_Option::OnEnChangeEditReversalPosy()
{
	EditMinMaxIntSet(IDC_EDIT_REVERSAL_POSY,&C_Total_PosY,0,m_CAM_SIZE_HEIGHT);
	C_Total_PosX = GetDlgItemInt(IDC_EDIT_REVERSAL_POSX);	
	C_Total_PosY = GetDlgItemInt(IDC_EDIT_REVERSAL_POSY);
	C_Dis_Width	= GetDlgItemInt(IDC_EDIT_REVERSAL_DISW);
	C_Dis_Height = GetDlgItemInt(IDC_EDIT_REVERSAL_DISH);
	C_Total_Width = GetDlgItemInt(IDC_EDIT_REVERSAL_W);
	C_Total_Height = GetDlgItemInt(IDC_EDIT_REVERSAL_H);
	

	if((C_Total_PosX >= 0)&&(C_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(C_Total_PosY >= 0)&&(C_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_Dis_Width>=0)&&(C_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(C_Dis_Height>=0)&&(C_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Total_Width>=0)&&(C_Total_Width<m_CAM_SIZE_WIDTH)&&
		(C_Total_Height>=0)&&(C_Total_Height<m_CAM_SIZE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_SETZONE))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_SETZONE))->EnableWindow(0);
	}
}

void CColorReversal_Option::OnEnChangeEditReversalDisw()
{
	EditMinMaxIntSet(IDC_EDIT_REVERSAL_DISW,&C_Dis_Width,0,m_CAM_SIZE_WIDTH);
	C_Total_PosX = GetDlgItemInt(IDC_EDIT_REVERSAL_POSX);	
	C_Total_PosY = GetDlgItemInt(IDC_EDIT_REVERSAL_POSY);
	C_Dis_Width	= GetDlgItemInt(IDC_EDIT_REVERSAL_DISW);
	C_Dis_Height = GetDlgItemInt(IDC_EDIT_REVERSAL_DISH);
	C_Total_Width = GetDlgItemInt(IDC_EDIT_REVERSAL_W);
	C_Total_Height = GetDlgItemInt(IDC_EDIT_REVERSAL_H);
	

	if((C_Total_PosX >= 0)&&(C_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(C_Total_PosY >= 0)&&(C_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_Dis_Width>=0)&&(C_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(C_Dis_Height>=0)&&(C_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Total_Width>=0)&&(C_Total_Width<m_CAM_SIZE_WIDTH)&&
		(C_Total_Height>=0)&&(C_Total_Height<m_CAM_SIZE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_SETZONE))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_SETZONE))->EnableWindow(0);
	}
}

void CColorReversal_Option::OnEnChangeEditReversalDish()
{
	EditMinMaxIntSet(IDC_EDIT_REVERSAL_DISH,&C_Dis_Height,0,m_CAM_SIZE_HEIGHT);
	C_Total_PosX = GetDlgItemInt(IDC_EDIT_REVERSAL_POSX);	
	C_Total_PosY = GetDlgItemInt(IDC_EDIT_REVERSAL_POSY);
	C_Dis_Width	= GetDlgItemInt(IDC_EDIT_REVERSAL_DISW);
	C_Dis_Height = GetDlgItemInt(IDC_EDIT_REVERSAL_DISH);
	C_Total_Width = GetDlgItemInt(IDC_EDIT_REVERSAL_W);
	C_Total_Height = GetDlgItemInt(IDC_EDIT_REVERSAL_H);
	

	if((C_Total_PosX >= 0)&&(C_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(C_Total_PosY >= 0)&&(C_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_Dis_Width>=0)&&(C_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(C_Dis_Height>=0)&&(C_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Total_Width>=0)&&(C_Total_Width<m_CAM_SIZE_WIDTH)&&
		(C_Total_Height>=0)&&(C_Total_Height<m_CAM_SIZE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_SETZONE))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_SETZONE))->EnableWindow(0);
	}
}

void CColorReversal_Option::OnEnChangeEditReversalW()
{
	EditMinMaxIntSet(IDC_EDIT_REVERSAL_W,&C_Total_Width,0,m_CAM_SIZE_WIDTH);
	C_Total_PosX = GetDlgItemInt(IDC_EDIT_REVERSAL_POSX);	
	C_Total_PosY = GetDlgItemInt(IDC_EDIT_REVERSAL_POSY);
	C_Dis_Width	= GetDlgItemInt(IDC_EDIT_REVERSAL_DISW);
	C_Dis_Height = GetDlgItemInt(IDC_EDIT_REVERSAL_DISH);
	C_Total_Width = GetDlgItemInt(IDC_EDIT_REVERSAL_W);
	C_Total_Height = GetDlgItemInt(IDC_EDIT_REVERSAL_H);
	

	if((C_Total_PosX >= 0)&&(C_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(C_Total_PosY >= 0)&&(C_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_Dis_Width>=0)&&(C_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(C_Dis_Height>=0)&&(C_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Total_Width>=0)&&(C_Total_Width<m_CAM_SIZE_WIDTH)&&
		(C_Total_Height>=0)&&(C_Total_Height<m_CAM_SIZE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_SETZONE))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_SETZONE))->EnableWindow(0);
	}
}

void CColorReversal_Option::OnEnChangeEditReversalH()
{
	EditMinMaxIntSet(IDC_EDIT_REVERSAL_H,&C_Total_Height,0,m_CAM_SIZE_HEIGHT);
	C_Total_PosX = GetDlgItemInt(IDC_EDIT_REVERSAL_POSX);	
	C_Total_PosY = GetDlgItemInt(IDC_EDIT_REVERSAL_POSY);
	C_Dis_Width	= GetDlgItemInt(IDC_EDIT_REVERSAL_DISW);
	C_Dis_Height = GetDlgItemInt(IDC_EDIT_REVERSAL_DISH);
	C_Total_Width = GetDlgItemInt(IDC_EDIT_REVERSAL_W);
	C_Total_Height = GetDlgItemInt(IDC_EDIT_REVERSAL_H);
	

	if((C_Total_PosX >= 0)&&(C_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(C_Total_PosY >= 0)&&(C_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_Dis_Width>=0)&&(C_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(C_Dis_Height>=0)&&(C_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(C_Total_Width>=0)&&(C_Total_Width<m_CAM_SIZE_WIDTH)&&
		(C_Total_Height>=0)&&(C_Total_Height<m_CAM_SIZE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_SETZONE))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_SETZONE))->EnableWindow(0);
		
	}
}

void CColorReversal_Option::OnEnChangeEditColorReversal()
{
	int num = iSavedItem;
	if((C_RECT[num].m_PosX >= 0)&&(C_RECT[num].m_PosX < m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_PosY >= 0)&&(C_RECT[num].m_PosY < m_CAM_SIZE_HEIGHT)&&
		(C_RECT[num].m_Left>=0)&&(C_RECT[num].m_Left<m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_Top>=0)&&(C_RECT[num].m_Top<m_CAM_SIZE_HEIGHT)&&
		(C_RECT[num].m_Right>=0)&&(C_RECT[num].m_Right<m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_Bottom>=0)&&(C_RECT[num].m_Bottom<m_CAM_SIZE_HEIGHT)&&
		(C_RECT[num].m_Width>=0)&&(C_RECT[num].m_Width<m_CAM_SIZE_WIDTH)&&
		(C_RECT[num].m_Height>=0)&&(C_RECT[num].m_Height<m_CAM_SIZE_HEIGHT)){

			((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_SAVE))->EnableWindow(1);
		

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_SAVE))->EnableWindow(0);
	}
}

void CColorReversal_Option::OnEnSetfocusEditMasterstate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
}

void CColorReversal_Option::Focus_move_start()
{
	if(((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_MASTER))->IsWindowEnabled() == TRUE){
		GotoDlgCtrl(((CButton *)GetDlgItem(IDC_BUTTON_REVERSAL_MASTER)));
	}else{
		GotoDlgCtrl(((CStatic *)GetDlgItem(IDC_STATIC)));
	}
}
void CColorReversal_Option::OnBnClickedButtontest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(0);
	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(1);
}
CString	CColorReversal_Option::GetColorNum(BYTE R, BYTE G, BYTE B)
{
	CString resultNum = "";

	if(R > G && R > B){
		if(R-G>20 && R-B>20){
			//////////////////////////레드
			resultNum="R";// 레드			
		}		
	}
	if(B>R && B>G){
		if(B-R>30 && B-G>30){
			//////////////////////////블루
			resultNum="B";// 블루			
		}
	}
	if(G>R &&G>B/*&& ((R>B) ||(R==B))*/){
		//if(G-R>50 &&G-B>50){
			//////////////////////////그린
			resultNum="G";// 그린			
		//}
	}


	if((R<60)&&(G<60)&&(B<60)&&(abs(R-B)<20)&&(abs(R-G)<20)){
			resultNum="BL";// 검정			
	}

	return resultNum;
}

CvRect CColorReversal_Option::GetCenterPoint(LPBYTE IN_RGB)
{	
	CvRect resultRect = cvRect(-99999, -99999, 0, 0);

	IplImage *OriginImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	IplImage *PreprecessedImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	
	IplImage *RGBOrgImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 3);	

	BYTE R,G,B;
	double Sum_Y;

	for (int y=0; y<CAM_IMAGE_HEIGHT; y++)
	{
		for (int x=0; x<CAM_IMAGE_WIDTH; x++)
		{
		/*	B = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 2];
			
			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;		*/

			B = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 2];

			if( R < 100 && G < 100 && B < 100)
				OriginImage->imageData[y*OriginImage->widthStep+x] = 255;
			else
				OriginImage->imageData[y*OriginImage->widthStep+x] = 0;

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}	
	
//	cvSaveImage("C:\\CenterImage.bmp", OriginImage);
//	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
//	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);

	cvCopyImage(OriginImage, SmoothImage);
	
	cvCanny(SmoothImage, CannyImage, 0, 255);

//	cvDilate(CannyImage, DilateImage);
	
	cvCopyImage(CannyImage, temp_PatternImage);
	
	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);
	
//	cvSaveImage("C:\\CenterImage.bmp", CannyImage);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
		counter++;
	
	if(counter == 0){

		return resultRect;
	}
	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area=0, arcCount=0;
	counter = 0;
	double old_dist = 999999;
	
	int old_center_pos_x = 0;
	int old_center_pos_y = 0;
	int center_pt_x = 0;
	int center_pt_y = 0;
	int obj_Cnt = 0;
	int size=0, old_size = 0;	

	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);
				
		rectArray[counter] = rect;
		areaArray[counter] = area;

		double circularity = (4.0*3.14*area)/(arcCount*arcCount);		
		
		if(circularity > 0.8)
		{
		
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;

			if(center_x > 270 && center_x < 450 && center_y > 160 && center_y < 320)
			{
				if(rect.width < CAM_IMAGE_WIDTH/2 && rect.height < CAM_IMAGE_HEIGHT/2 && rect.width > 10 && rect.height > 10)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
					
					/*center_pt_x = center_pt_x+ center_x;
					center_pt_y = center_pt_y+ center_y;
					obj_Cnt++;

					old_center_pos_x = center_pt_x/obj_Cnt;
					old_center_pos_y = center_pt_y/obj_Cnt;*/
					
					double distance = GetDistance(rect.x + rect.width/2, rect.y+rect.height/2, 360, 240);
					
					obj_Cnt++;

					size = rect.width * rect.height;
					
					if(distance < old_dist)
					{
						old_size = size;
						resultRect.x = rect.x;
						resultRect.y = rect.y;
						resultRect.width = rect.width;
						resultRect.height = rect.height;
						old_dist = distance;
					}

					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 0), 1, 8);  
				
				}
			}
		}
		
		
		counter++;
	}
//	cvSaveImage("D:\\RGBResultImage.bmp", RGBResultImage);
	cvReleaseMemStorage(&contour_storage);
	
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&PreprecessedImage);
	cvReleaseImage(&CannyImage);

	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
	cvReleaseImage(&RGBOrgImage);

//	delete contour;
//	delete temp_contour;
	delete []rectArray;
	delete []areaArray;
	
	/*if(obj_Cnt != 0)
	{
		resultPt.x = center_pt_x / obj_Cnt;
		resultPt.y = center_pt_y / obj_Cnt;
	}*/
/*	if(obj_Cnt != 0)
	{
		resultPt.x = center_pt_x;
		resultPt.y = center_pt_y;
	}*/
		
	return resultRect;	
}

#pragma region LOT관련 함수
void CColorReversal_Option::LOT_Set_List(CListCtrl *List){

	while(List->DeleteColumn(0));
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT 명",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);	
	List->InsertColumn(5,"역상 상태",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"비고",LVCFMT_CENTER, 80);
	Lot_InsertNum =8;
	
}

void CColorReversal_Option::LOT_InsertDataList(){
	CString strCnt="";

	int Index = m_Lot_ColorReversalList.InsertItem(Lot_StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index+1);
	m_Lot_ColorReversalList.SetItemText(Index,0,strCnt);

	int CopyIndex = m_ColorReversalList.GetItemCount()-1;

	int count =0;
	for(int t=0; t< Lot_InsertNum;t++){
		if((t != 2)&&(t!=0)){
			m_Lot_ColorReversalList.SetItemText(Index,t, m_ColorReversalList.GetItemText(CopyIndex,count));
			count++;

		}else if(t==0){
			count++;
		}else{
			m_Lot_ColorReversalList.SetItemText(Index,t,((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;

}
void CColorReversal_Option::LOT_EXCEL_SAVE(){
	CString Item[1]={"역상 상태"};

	CString Data ="";
	CString str="";
	str = "***********************컬러 반전 검사***********************\n";
	Data += str;
	str.Empty();
	str= "Master 모드, "+checkView+",\n";
	Data += str;
	str.Empty();
	str.Format("Left Top ,"+C_RECT[0].COLOR+",/,Right Top ,"+C_RECT[1].COLOR+",/,Left Bottom,"+C_RECT[2].COLOR+",/,Right Bottom,"+C_RECT[3].COLOR+",\n");
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("반전검사",Item,1,&m_Lot_ColorReversalList,Data);
}

bool CColorReversal_Option::LOT_EXCEL_UPLOAD(){
	m_Lot_ColorReversalList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("반전검사",&m_Lot_ColorReversalList,1,&Lot_StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}

#pragma endregion 
void CColorReversal_Option::OnLvnItemchangedListReversal(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}
