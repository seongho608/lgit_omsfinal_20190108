#pragma once


// CUserSwitching 대화 상자입니다.

class CUserSwitching : public CDialog
{
	DECLARE_DYNAMIC(CUserSwitching)

public:
	CUserSwitching(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CUserSwitching();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_USER };

	void	Setup(CWnd* IN_pMomWnd);
	int		User_Cursel;
private:
	CWnd		*m_pMomWnd;	

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CString m_PassWord;
};
