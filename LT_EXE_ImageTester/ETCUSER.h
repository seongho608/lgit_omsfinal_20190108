#pragma once


#define WHITE_COLOR	RGB(255,255,255)
#define BLACK_COLOR	RGB(0,0,0)
#define BLUE_COLOR	RGB(0,0,255)
#define BLUE_DK_COLOR	RGB(0,0,128)
#define RED_COLOR	RGB(255,0,0)
#define RED_DK_COLOR	RGB(128,0,0)
#define GREEN_COLOR	RGB(0,255,0)
#define GREEN_DK_COLOR	RGB(0,128,0)
#define GRAY_COLOR	RGB(192,192,192)
#define GRAY_DK_COLOR	RGB(128,128,128)
#define GRAY_LT_COLOR	RGB(230,230,230)
#define YELLOW_COLOR	RGB(255,255,0)
#define MINOR_COLOR		RGB(255,128,64)
#define PINK_COLOR		RGB(255,0,255)
#define MCYAN	RGB(0,255,255)


#define TX_MODE			'0'
#define RX_MODE			'1'
#define END_MODE		'2'
#define ERR_MODE		'3'
#define READ_FRAME_TIMER		9
#define READ_FRAME_PERIOD		10
#define TX_COLOR					RED_COLOR
#define RX_COLOR					BLUE_COLOR

#define MAX_LOG_LEN		1024*50
#define MAX_BUF_LEN		0xFFF

#define TEST_OK		1
#define TEST_NG		0


//전역변수

BYTE Hex2Ascii(BYTE HEX);
BYTE Ascii2Hex(BYTE Ascii);
int Ascii2int(BYTE DATA);
CString StringMoc(LPCSTR lpcszString, ...);//cstring 값을 읽어 숫자모드의 경우 숫자만 남기고 헥사모드의 경우 헥사값만 남긴다.
DWORD StringHEX2DWORD(LPCSTR lpcszString, ...);

void		DoEvents(DWORD dwMiliSeconds);
void		DoEvents();
void		Wait(DWORD dwmillisecont);
void		Wait2(DWORD dwmillisecont);
//void		DoEvents(DWORD dwmillisecont);

CString		CurrntDay();
CString		CStringIntgen(int stat,int number);
CString		ExePathSearch();
CString		ExeDrvSearch();

CString		BackPathSearch(CString strPath);
CString		FinalPathSearch(CString strPath);

BOOL		DelFile(CString dir);
BOOL		DeleteDir(CString dir);
BOOL		CopyFolder(CString strFromFolder, CString strToFolder);
BOOL		FileFolder_Copy(CString Original_path, CString Copy_path);

CString		SysPathSearch();

#ifdef _UNICODE
	double		GetPrivateProfileDouble(LPCTSTR lpAppName,LPCTSTR lpKeyName,double nDefault,LPCTSTR lpFileName);
	CString		GetPrivateProfileCString(LPCTSTR lpAppName,LPCTSTR lpKeyName,LPCTSTR lpFileName);
#else
	double		GetPrivateProfileDouble(LPCSTR lpAppName,LPCSTR lpKeyName,double nDefault,LPCSTR lpFileName);
	CString		GetPrivateProfileCString(LPCSTR lpAppName,LPCSTR lpKeyName,LPCSTR lpFileName);
#endif

DWORD		GetHddSerial(LPCTSTR Driver);

BOOL		RegChk(unsigned int m_ID);
CString		RemoveExtension(LPCTSTR lpcszString, ...);
CString		folder_gen(CString Path,LPCTSTR lpcszString, ...);
CString		folder_gen(CString Path);

DWORD		regfilechk(LPCTSTR regfile);
void		regfileSave(LPCTSTR regpath,DWORD regdata);
DWORD		PWChangeGen(DWORD Reg);

void DrawOutlineText(CDC* pDC, int x, int y, LPCTSTR lpText,COLORREF clrText, COLORREF clrLine, int sizeLine);

void Copy_List(CListCtrl *SrcList,CListCtrl *List,int ListItemNum);
void LogWriteToFile(CString szMsg);
void LogWriteToFile_Center(CString szMsg);

typedef struct _INFO{
	BYTE ID;
	int RunNum;
	int Number;
	int nMesIdx;
	int nMesNum;
	double VER;
	bool	ENABLE;
	CString NAME;
	CString MODE;
	//int use;
}tINFO;

typedef struct _SetPoint{
	CPoint Set_Pos_Start;
	CPoint Set_Pos_End;
	CPoint Old_Pos_Start;
	CPoint Old_Pos_End;
	CPoint Default_Pos_Start;
	CPoint Default_Pos_End;
}tSetPoint;



typedef struct _PCBINFO{
	BYTE ID;
	int Number;
	double VoltCH1;
	double VoltCH2;
	bool  VoltCH1_Chk;
	bool  VoltCH2_Chk;
	bool  VoltChk;
	
	bool CHK_Enable;

	bool enable;
	int Erase;
	int Download;
	int Verify;
	int VoltageOn;
	int VoltageOff;
	
	int testResult;
	CString strResult;
	CString TESTMODE;
}tPcbINFO;

typedef struct _ECPDATA{
	int m_oldoffX;
	int m_oldoffY;
	int m_offsetX;
	int m_offsetY;
	int m_totaloffsetX;
	int m_totaloffsetY;
	double OpticCenterX;
	double OpticCenterY;
	BOOL m_Enable;
	BOOL m_success;
}tECPDATA;

typedef struct _EtcWrdata{
	int m_number;
	int m_count;
	
	int m_offsetX;   //파일 다운로드시 적용되는 수치 X	//누적 OFFSET
	int m_offsetY;   //파일 다운로드시 적용되는 수치 Y
	int m_oldoffsetX;   //파일 다운로드시 적용되는 수치 과거값X
	int m_oldoffsetY;   //파일 다운로드시 적용되는 수치 과거값Y
	
	int m_Init_Offset_X;	//초기 OFFSET
	int m_Init_Offset_Y;

	int m_Init_X;	//초기 광축
	int m_Init_Y;
	
	int m_X_OFFSET;  //중심으로 부터 OFFSET X	//마지막 옵셋
	int m_Y_OFFSET;  //중심으로 부터 OFFSET Y
	double m_X_ASIX; //계측광축
	double m_Y_ASIX; //계측광축
	double m_F_X_ASIX;//실제 측정 광축 X
	double m_F_Y_ASIX;//실제 측정 광축 Y
	int m_F_X_OFFSET;  //중심으로 부터 OFFSET X	//마지막 옵셋
	int m_F_Y_OFFSET;  //중심으로 부터 OFFSET Y
	
	CString str_state;
	bool m_Enable;
	bool m_success;

	BYTE buffer[256];
	BYTE DATA[20];
}tEtcWrdata;

typedef struct _TOTWrdata{
	tEtcWrdata t_EtcCh[4];
}tTOTWrdata;


typedef struct _OptPr{
	int HWMODE;
	int INITWRSEL;
	int MIDWRSEL;
	int MaxCount;
	int StandX;
	int StandY;
	double PassDeviatX;
	double PassDeviatY;
	double PassMaxDevX;
	double PassMaxDevY;
	int WriteMode;
	double RateX;
	double RateY;
	int EnPixX;
	int EnPixY;
	double Optical_Radius;
	double Sensitivity;

	int		RetryCnt_Write;	// 2016.09.01 수정 : 심재원
}tOptPr;


typedef struct _ResultVal{
	int m_ID;
	int m_Success;
	double m_Val[10];
	CString ValString;
	int resultNUM;
	double d_resultNUM;

	void Reset()
	{
		m_ID;
		m_Success = 0;
		for (int _a = 0; _a < 10; _a++)
		{
			m_Val[_a] = 0.0;
		}

		ValString.Empty();
		resultNUM = 0;
		d_resultNUM = 0.0;
	}
}tResultVal;


typedef struct _MOTOR{ //
	double POS1_X;
	double POS1_Z;
	double POS1_R;

	double POS2_X;
	double POS2_Z;
	double POS2_R;

	double POS3_X;
	double POS3_Z;
	double POS3_R;

	double MOVE_STABLE_TIME;
	double STOPPER_UP_DELAY;
	double STOPPER_DN_DELAY;
	
}tMOTOR;

class CLGE_LIB_Data
{

private:

public:
	int m_ImgSize;// 1 : half(320x240), 0 : full ( 640x480)
	int m_Projection;// 1: projection distance , 0: radial distance
	int m_Offset;
	int m_Slope;
	int m_Alpha;
	bool b_IRMode;// 1: on , 0 : off
	int m_IRGain;//1,2,4,8,16,32
	int m_Removal; // 64
	bool b_Exposure; // 1: on , 0 : off
	bool b_Emission;
	bool b_ViwerGain;
	int m_ViwerGain;

};

class CRectData
{
public:
	CRectData();
private:
	void	ClassBegin(void);
	void	ClassEnd(  void);
public:
	int m_Left,m_Right,m_Top,m_Bottom;
	int m_PosX,m_PosY,m_Height,m_Width;
	int m_R,m_G,m_B;
	int m_startX, m_startY, m_endX, m_endY;
	int m_sizeX, m_sizeY;
	int m_Master;
	CString COLOR;
	CString COLORresult;
	BOOL	ENABLE;
	double BrightData_Ave;
	int m_resultR,m_resultG,m_resultB;

	int m_MinR,m_MinG,m_MinB;
	int m_MaxR,m_MaxG,m_MaxB;
	double m_L, m_a, m_b;
	double	m_resultDis;
	double	m_resultDegree;
	double	m_Degree;
	//-------------------------------eija
	int		m_ConstDataPosition;
	int		m_ResultDataPosition;
	int		m_bDirect;
	int		m_bmode;
	int		m_EN_RUN;
	int		m_CheckLine;
	int		m_ResultData;//최종 데이터
	int		m_ResultLine;//최종 데이터
	int		m_StDev;
	int		m_offset1,m_offset2;
	double		MTFRatio;
	int		m_dir;
	int		m_checkLine_Buffer[30];
	int		m_checkLine_Buffer_cnt;
	CPoint	m_dPatternStartPos, m_dPatternEndPos;
	int		m_ThresholdPosition;
	float m_Concentration;

	int font;
	double m_resultX,m_resultY;
	double m_result;
	bool m_Success;
	int m_Thresold;
	int m_Thresold_Peak; 

	int Signal[3];//SNR_BW
	double Noise[3];//SNR_BW
	double m_result_BW;//D.R
	double d_Thresold_BW;//D.R

	double d_Thresold;
	long Length();
	long Height();
	bool Chkdata();
	void MOVE(int x,int y);
	void Clear(void);
	void RECT_SET(int x,int y,int width,int height);
	void EX_RECT_SET(int half_x,int half_y,UINT width,UINT height);
	//-------------------------------eija
	void EX_EIJA_SET(int HALF_X,int HALF_Y,int Width,int Height,bool Mode,bool Direct,int ConstDataPosition,int BaseLineData);
	void EX_RESOLUTION_SET(int HALF_X,int HALF_Y,int Width,int Height,bool Mode,bool Direct);

	int		SFR_Width, SFR_Height;
	double	*SFR_MTF_Value;
	double  *SFR_MTF_LineFitting_Value;
	double  *SFR_BWRatio;
	int EdgeDelta;

	//-------------------------------광량비
	int m_iStdBlock;
	double d_StdThre[30];//SNR_BW
};

void SaveChartSet(CRectData *buf_Rect,CString Filename);
void LoadChartSet(CRectData *buf_Rect,CString Filename);

class CData
{
public:
	CData()
	{
		memset(data, 0x00, MAX_BUF_LEN); 
		len = 0;
		sendtime = 0;
		retrycnt = 0;
	}

public:
	BYTE	data[MAX_BUF_LEN];
	int		len;
	clock_t sendtime;
	int		retrycnt;
};
