// DTC_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "DTC_ResultView.h"


// CDTC_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDTC_ResultView, CDialog)

CDTC_ResultView::CDTC_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CDTC_ResultView::IDD, pParent)
{

}

CDTC_ResultView::~CDTC_ResultView()
{
}

void CDTC_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDTC_ResultView, CDialog)
	ON_EN_SETFOCUS(IDC_EDIT_DTC_STATE, &CDTC_ResultView::OnEnSetfocusEditDtcState)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CDTC_ResultView 메시지 처리기입니다.
void CDTC_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CDTC_ResultView::OnEnSetfocusEditDtcState()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

BOOL CDTC_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_EDIT_DTC_NAME1,&ft,100,20);
	GetDlgItem(IDC_EDIT_DTC_NAME2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_DTC_NAME3)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_DTC_NAME4)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_DTC1)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_DTC2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_DTC3)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_DTC4)->SetFont(&ft);
	Font_Size_Change(IDC_EDIT_DTC_STATE,&ft2,100,38);

	((CEdit *)GetDlgItem(IDC_EDIT_DTC_NAME1))->SetWindowText("DTC 1");
	((CEdit *)GetDlgItem(IDC_EDIT_DTC_NAME2))->SetWindowText("DTC 2");
	((CEdit *)GetDlgItem(IDC_EDIT_DTC_NAME3))->SetWindowText("DTC 3");
	((CEdit *)GetDlgItem(IDC_EDIT_DTC_NAME4))->SetWindowText("DTC 4");

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

HBRUSH CDTC_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_DTC_NAME1){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_DTC_NAME2){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_DTC_NAME3){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_DTC_NAME4){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_DTC1){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[0]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_DTC2){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[1]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_DTC3){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[2]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_DTC4){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[3]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_DTC_STATE){
		pDC->SetBkColor(bk_st_col);
		pDC->SetTextColor(tx_st_col);
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CDTC_ResultView::DTC_RESULT(int num,int col,LPCSTR lpcszString, ...)
{	
	switch(num){
		case 0:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC1))->SetWindowText(lpcszString);
			break;
		case 1:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC2))->SetWindowText(lpcszString);
			break;
		case 2:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC3))->SetWindowText(lpcszString);
			break;
		case 3:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC4))->SetWindowText(lpcszString);
			break;
		default:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC1))->SetWindowText(lpcszString);
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC2))->SetWindowText(lpcszString);
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC3))->SetWindowText(lpcszString);
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC4))->SetWindowText(lpcszString);
	}

	if(num <=3){
		if(col == 0){
			Valcol[num] = RGB(86,86,86);
		}else if(col  ==1){
			Valcol[num] = RGB(18,69,171);
		}else{
			Valcol[num] = RGB(201,0,0);
		}
	}else{
		for(int lop = 0;lop<4;lop++){
			if(col == 0){
				Valcol[lop] = RGB(86,86,86);
			}else if(col  ==1){
				Valcol[lop] = RGB(18,69,171);
			}else{
				Valcol[lop] = RGB(201,0,0);
			}
		}
	}
}

void CDTC_ResultView::DTC_STATE(int col,LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_DTC_STATE))->SetWindowText(lpcszString);
	if(col == 0){//스탠드 바이
		tx_st_col = RGB(255, 255, 255);
		bk_st_col = RGB(47, 157, 39);
	}else if(col == 1){//성공
		tx_st_col = RGB(255, 255, 255);
		bk_st_col = RGB(18, 69, 171);
	}else if(col == 2){//실패
		tx_st_col = RGB(255, 255, 255);
		bk_st_col = RGB(201, 0, 0);
	}else if(col == 3){
		tx_st_col = RGB(0, 0, 0);
		bk_st_col = RGB(255, 255, 0);
	}
}

void CDTC_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}

void CDTC_ResultView::InitStat()
{
	DTC_RESULT(4,0,"");//모두 NONE표시
	DTC_STATE(0,"Stand By");

}