// Particle_View.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Particle_View.h"


// CParticle_View 대화 상자입니다.

IMPLEMENT_DYNAMIC(CParticle_View, CDialog)

CParticle_View::CParticle_View(CWnd* pParent /*=NULL*/)
	: CDialog(CParticle_View::IDD, pParent)
{

}

CParticle_View::~CParticle_View()
{
}

void CParticle_View::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_ParticleView, m_PaticleViewFrame);
}


BEGIN_MESSAGE_MAP(CParticle_View, CDialog)
	ON_BN_CLICKED(IDOK, &CParticle_View::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CParticle_View::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &CParticle_View::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON4, &CParticle_View::OnBnClickedButton4)
END_MESSAGE_MAP()


// CParticle_View 메시지 처리기입니다.
void CParticle_View::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}
void CParticle_View::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->b_Particle =FALSE;
	OnOK();
}

void CParticle_View::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->b_Particle =FALSE;
	OnCancel();
}

void CParticle_View::OnBnClickedButton1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->b_Particle =FALSE;
	//((CImageTesterDlg  *)m_pMomWnd)->Particle_TEST=1;

}

void CParticle_View::OnBnClickedButton4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->b_Particle =FALSE;
	//((CPATTERN_NOISE_TESTDlg *)m_pMomWnd)->Particle_TEST=2;
}
void CParticle_View::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}