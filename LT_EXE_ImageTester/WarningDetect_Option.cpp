// WarningDetect_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "WarningDetect_Option.h"

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];

IMPLEMENT_DYNAMIC(CWarningDetect_Option, CDialog)

CWarningDetect_Option::CWarningDetect_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CWarningDetect_Option::IDD, pParent)
	, m_dWarningStart_x(0)
	, m_dWarningStart_y(0)
	, m_dWarningEnd_x(0)
	, m_dWarningEnd_y(0)
	, b_Chk_ResEx(FALSE)
	, b_Chk_EndRst(FALSE)
	, str_List_End(_T(""))
{
	m_WNum = -1;
	str_WMode = "";
	StartCnt = 0;
	Lot_StartCnt = 0;
	b_StopFail = FALSE;
}

CWarningDetect_Option::~CWarningDetect_Option()
{

}

void CWarningDetect_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_WARNINGWORKLIST, m_WarningWorkList);
	DDX_Control(pDX, IDC_LIST_WARNING, m_lsWarningCheckList);
	DDX_Control(pDX, IDC_LIST_WARNING_CANDIDATE, m_lsWarningExtrCandidate);
	DDX_Control(pDX, IDC_LIST_WARNING_TOTAL, m_lsWarningTotal);
	DDX_Control(pDX, IDC_WARNING_FRAME, m_stWarningFrame);
	DDX_Check(pDX, IDC_CHECK_RESEX, b_Chk_ResEx);
	DDX_Check(pDX, IDC_CHECK_RESET, b_Chk_EndRst);
	DDX_Text(pDX, IDC_EDIT_END_CODE, str_List_End);
	DDX_Control(pDX, IDC_LIST_WARNINGWORKLIST_LOT, m_Lot_WarningWorkList);
}


BEGIN_MESSAGE_MAP(CWarningDetect_Option, CDialog)
	ON_LBN_SELCHANGE(IDC_LIST_WARNING_TOTAL, &CWarningDetect_Option::OnLbnSelchangeListWarningTotal)
	ON_BN_CLICKED(IDC_BTN_TOTAL2CANDIDATE, &CWarningDetect_Option::OnBnClickedBtnTotal2candidate)
	ON_BN_CLICKED(IDC_BTN_CANDIDATE2TOTAL, &CWarningDetect_Option::OnBnClickedBtnCandidate2total)
	ON_BN_CLICKED(IDC_BTN_WARNING_MASTER_EXTACT, &CWarningDetect_Option::OnBnClickedBtnWarningMasterExtact)
	ON_LBN_SELCHANGE(IDC_LIST_WARNING, &CWarningDetect_Option::OnLbnSelchangeListWarning)
	ON_BN_CLICKED(IDC_BTN_WARNING_CHECKLIST_DELETE, &CWarningDetect_Option::OnBnClickedBtnWarningChecklistDelete)
	ON_BN_CLICKED(IDC_BTN_WARNING_SAVE, &CWarningDetect_Option::OnBnClickedBtnWarningSave)
	ON_BN_CLICKED(IDC_BTN_SET_WARNING_AREA, &CWarningDetect_Option::OnBnClickedBtnSetWarningArea)
	ON_CBN_SELCHANGE(IDC_COMBO_RES, &CWarningDetect_Option::OnCbnSelchangeComboRes)
	ON_BN_CLICKED(IDC_CHECK_RESEX, &CWarningDetect_Option::OnBnClickedCheckResex)
	ON_LBN_SELCHANGE(IDC_LIST_WARNING_CANDIDATE, &CWarningDetect_Option::OnLbnSelchangeListWarningCandidate)
	ON_EN_CHANGE(IDC_EDIT_TEST_NAME, &CWarningDetect_Option::OnEnChangeEditTestName)
//	ON_CBN_SELCHANGE(IDC_COMBO_WMODE2, &CWarningDetect_Option::OnCbnSelchangeComboWmode2)
	ON_BN_CLICKED(IDC_BTN_TOTAL2CANDIDATE2, &CWarningDetect_Option::OnBnClickedBtnTotal2candidate2)
	ON_BN_CLICKED(IDC_BTN_CANDIDATE2TOTAL2, &CWarningDetect_Option::OnBnClickedBtnCandidate2total2)
	ON_BN_CLICKED(IDC_CHECK_RESET, &CWarningDetect_Option::OnBnClickedCheckReset)
	ON_BN_CLICKED(IDC_BTN_WARNIG_ADD, &CWarningDetect_Option::OnBnClickedBtnWarnigAdd)
	ON_BN_CLICKED(IDC_BTN_WARNIG_DEL, &CWarningDetect_Option::OnBnClickedBtnWarnigDel)
	ON_BN_CLICKED(IDC_BTN_WARNING_INIT, &CWarningDetect_Option::OnBnClickedBtnWarningInit)
	ON_BN_CLICKED(IDC_BTN_WARNING_TEST, &CWarningDetect_Option::OnBnClickedBtnWarningTest)
	ON_BN_CLICKED(IDC_BTN_CANDIDATE2CLR, &CWarningDetect_Option::OnBnClickedBtnCandidate2clr)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// CWarningDetect_Option 메시지 처리기입니다.
void CWarningDetect_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
	str_ModelPath=((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	strSelectedModelPath = ((CImageTesterDlg *)m_pMomWnd)->Overlay_path;
	str_model.Empty();
	str_model.Format("WARNING OPTION");
}

void CWarningDetect_Option::Setup(CWnd* IN_pMomWnd,CEdit *pTEDIT,tINFO INFO)
{
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	Setup(IN_pMomWnd);

//	m_ShowMode = 0;
}

BOOL CWarningDetect_Option::OnInitDialog()
{
	CDialog::OnInitDialog();
	DefaultCodeSet();//파일명 코드를 초기화한다. 

	pResCombo = (CComboBox *)GetDlgItem(IDC_COMBO_RES);
	UpateMasterWarningFileList_ORG();
	Load_parameter();
	UpdateData(FALSE);	
	m_lsWarningCheckList.ResetContent();
	for(int _a=0; _a<m_nWarningListCnt; _a++)
	{
		m_lsWarningCheckList.AddString(m_szWarningListFile[_a]);
	}

	SETTING_DATA();
	
	for(int k=0; k<100; k++){
		TESTLOT[k]="";
	}

	m_Resum = 0;
	b_Chk_ResEx = FALSE;
	pResCombo->SetCurSel(m_Resum);
	WarningList_Set();
	Set_List(&m_WarningWorkList);
	LOT_Set_List(&m_Lot_WarningWorkList);
	OnCbnSelchangeComboRes();
	
	InitEVMS();
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CWarningDetect_Option::Save_parameter()
{
	CString str="";
	CString strTitle="";

	str.Empty();
	str.Format("%d",m_INFO.ID);
	WritePrivateProfileString(str_model,"ID",str,str_ModelPath);
	str.Empty();
	str.Format("%s",m_INFO.MODE);
	WritePrivateProfileString(str_model,"MODE",str,str_ModelPath);
	str.Empty();
	str.Format("%s",m_INFO.NAME);
	WritePrivateProfileString(str_model,"NAME",str,str_ModelPath);

	str.Empty();
	GetDlgItemText(IDC_EDIT_WARNING_START_X, str);
	WritePrivateProfileString(str_model,"WARNING_START_X",str,str_ModelPath);	
	str.Empty();
	GetDlgItemText(IDC_EDIT_WARNING_START_Y, str);
	WritePrivateProfileString(str_model,"WARNING_START_Y",str,str_ModelPath);
	str.Empty();
	GetDlgItemText(IDC_EDIT_WARNING_END_X, str);
	WritePrivateProfileString(str_model,"WARNING_END_X",str,str_ModelPath);
	str.Empty();
	GetDlgItemText(IDC_EDIT_WARNING_END_Y, str);
	WritePrivateProfileString(str_model,"WARNING_END_Y",str,str_ModelPath);
	str.Empty();
	GetDlgItemText(IDC_EDIT_MATCHING_RATIO, str);
	WritePrivateProfileString(str_model,"WARNING_MATCHING_RATIO",str,str_ModelPath);
}

void CWarningDetect_Option::Load_parameter()
{
	CFileFind filefind;
	CString str="";
	CString strTitle="";

	b_Chk_EndRst = GetPrivateProfileInt(str_model,"WARNING_ENDRSTEN",-1,str_ModelPath);
	if(b_Chk_EndRst == -1){
		b_Chk_EndRst = 1;
		str.Empty();
		str.Format("%d",b_Chk_EndRst);
		WritePrivateProfileString(str_model,"WARNING_ENDRSTEN",str,str_ModelPath);
	}
	EnableEndRststate(b_Chk_EndRst);

	str_List_End = GetPrivateProfileCString(str_model,"WARNING_LIST_END",str_ModelPath);


	m_dWarningStart_x = GetPrivateProfileInt(str_model,"WARNING_START_X",-1,str_ModelPath);	
	if(m_dWarningStart_x == -1){
		m_dWarningStart_x = 50;
		str.Empty();
		str.Format("%d",m_dWarningStart_x);
		WritePrivateProfileString(str_model,"WARNING_START_X",str,str_ModelPath);
	}

	m_dWarningStart_y = GetPrivateProfileInt(str_model,"WARNING_START_Y",-1,str_ModelPath);	
	if(m_dWarningStart_y == -1){
		m_dWarningStart_y = 5;
		str.Empty();
		str.Format("%d",m_dWarningStart_y);
		WritePrivateProfileString(str_model,"WARNING_START_Y",str,str_ModelPath);
	}

	m_dWarningEnd_x = GetPrivateProfileInt(str_model,"WARNING_END_X",-1,str_ModelPath);	
	if(m_dWarningEnd_x == -1){
		m_dWarningEnd_x = 700;
		str.Empty();
		str.Format("%d",m_dWarningEnd_x);
		WritePrivateProfileString(str_model,"WARNING_END_X",str,str_ModelPath);
	}

	m_dWarningEnd_y = GetPrivateProfileInt(str_model,"WARNING_END_Y",-1,str_ModelPath);	
	if(m_dWarningEnd_y == -1){
		m_dWarningEnd_y = 100;
		str.Empty();
		str.Format("%d",m_dWarningEnd_y);
		WritePrivateProfileString(str_model,"WARNING_END_Y",str,str_ModelPath);
	}
	
	m_dMatchingRatio = GetPrivateProfileInt(str_model,"WARNING_MATCHING_RATIO",-1,str_ModelPath);	
	if(m_dMatchingRatio == -1){
		m_dMatchingRatio = 90;
		str.Empty();
		str.Format("%d",m_dMatchingRatio);
		WritePrivateProfileString(str_model,"WARNING_MATCHING_RATIO",str,str_ModelPath);
	}

	CString szTemp = _T("");
	m_nWarningListCnt = GetPrivateProfileInt(str_model,"WARNING_LIST_COUNT",0,str_ModelPath);	
	if(m_nWarningListCnt > 60){
		m_nWarningListCnt = 60;
		str.Empty();
		str.Format("%d",m_nWarningListCnt);
		WritePrivateProfileString(str_model,"WARNING_LIST_COUNT",str,str_ModelPath);
	}else if(m_nWarningListCnt == 0){
		if(m_lsWarningCheckList.GetCount() > 0){//과거 버전의 이미지가 남아있다면
			m_nWarningListCnt = m_lsWarningCheckList.GetCount();
			if(m_nWarningListCnt > 60){m_nWarningListCnt = 60;}
			for(int _a=0; _a<m_nWarningListCnt; _a++)
			{
				m_lsWarningCheckList.GetText(_a, str);
				szTemp.Format(_T("WARNING_LIST_%d"), _a);
				WritePrivateProfileString(str_model,szTemp,str,str_ModelPath);
			}
			str.Empty();
			str.Format("%d",m_nWarningListCnt);
			WritePrivateProfileString(str_model,"WARNING_LIST_COUNT",str,str_ModelPath);
		}
	}

	for(int _a=0; _a<m_nWarningListCnt; _a++)
	{
		szTemp.Format(_T("WARNING_LIST_%d"), _a);
		m_szWarningListFile[_a] = GetPrivateProfileCString(str_model,szTemp,str_ModelPath);	
		if(m_szWarningListFile[_a].GetLength() == 0){
			str.Empty();
			str.Format("%s",m_szWarningListFile[_a]);
			WritePrivateProfileString(str_model,szTemp,str,str_ModelPath);
		}
	}

	SetDlgItemInt(IDC_EDIT_WARNING_START_X, m_dWarningStart_x);
	SetDlgItemInt(IDC_EDIT_WARNING_START_Y, m_dWarningStart_y);
	SetDlgItemInt(IDC_EDIT_WARNING_END_X, m_dWarningEnd_x);
	SetDlgItemInt(IDC_EDIT_WARNING_END_Y, m_dWarningEnd_y);
	SetDlgItemInt(IDC_EDIT_MATCHING_RATIO, m_dMatchingRatio);
}

void CWarningDetect_Option::LOT_InsertDataList()
{
	CString strCnt="";

	int Index = m_Lot_WarningWorkList.InsertItem(Lot_StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index+1);
	m_Lot_WarningWorkList.SetItemText(Index,0,strCnt);

	int CopyIndex = m_WarningWorkList.GetItemCount()-1;

	int count =0;
	for(int t=0; t< Lot_InsertNum;t++){
		if((t != 2)&&(t!=0)){
			m_Lot_WarningWorkList.SetItemText(Index,t, m_WarningWorkList.GetItemText(CopyIndex,count));
			count++;

		}else if(t==0){
			count++;
		}else{
			m_Lot_WarningWorkList.SetItemText(Index,t,((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;
}

void CWarningDetect_Option::DefaultCodeSet()
{	
	for(int lop = 0;lop<16;lop++){
		ResCode[lop].m_Code = lop;
		ResCode[lop].str_DATA = "";
		ResCode[lop].str_Name = "";
	}
	ResCode[0].str_Name = "DOM";
	ResCode[1].str_Name = "NA";
	ResCode[2].str_Name = "EU";
	ResCode[3].str_Name = "AUS";
	ResCode[4].str_Name = "CHN";
	ResCode[5].str_Name = "GEN";
	for(int lop = 0;lop<16;lop++){
		if(ResCode[lop].str_Name != ""){
			ResCode[lop].str_DATA = "_"+ ResCode[lop].str_Name;		
		}
	}

	for(int lop = 0;lop<31;lop++){
		LangCode[lop].m_Code = lop+1;
		LangCode[lop].str_Name.Format("문구%02d",lop+1);
		LangCode[lop].str_DATA = "";
	}
	LangCode[0].str_DATA = "Korean";
	LangCode[1].str_DATA = "English(US)";
	LangCode[2].str_DATA = "English(UK)";
	LangCode[3].str_DATA = "Chinese(Mandarin)";
	LangCode[4].str_DATA = "Chinese(Cantonese)";
	LangCode[5].str_DATA = "French";
	LangCode[6].str_DATA = "Spanish";
	LangCode[7].str_DATA = "Arabic";
	LangCode[8].str_DATA = "Czech";
	LangCode[9].str_DATA = "Danish";
	LangCode[10].str_DATA = "German";
	LangCode[11].str_DATA = "Italian";
	LangCode[12].str_DATA = "Dutch";
	LangCode[13].str_DATA = "Polish";
	LangCode[14].str_DATA = "Portuguese";
	LangCode[15].str_DATA = "Russian";
	LangCode[16].str_DATA = "Slovak";
	LangCode[17].str_DATA = "Swedish";
	LangCode[18].str_DATA = "Turkish";
	LangCode[19].str_DATA = "French(US)";
	LangCode[20].str_DATA = "Spain(US)";
	LangCode[21].str_DATA = "reserved";
	LangCode[22].str_DATA = "reserved";
	LangCode[23].str_DATA = "reserved";
	LangCode[24].str_DATA = "reserved";
	LangCode[25].str_DATA = "reserved";
	LangCode[26].str_DATA = "reserved";
	LangCode[27].str_DATA = "reserved";
	LangCode[28].str_DATA = "reserved";
	LangCode[29].str_DATA = "reserved";
	LangCode[30].str_DATA = "Invalid";
	return;
}

void CWarningDetect_Option::WarningList_Set()
{
	CString str_buf = "";
	m_lsWarningTotal.ResetContent();
	if(b_Chk_ResEx == TRUE){
		for(int lop=0;lop<31;lop++){
			str_buf.Format("문구%02d",lop+1);
			str_buf += ResCode[m_Resum].str_DATA;
			m_lsWarningTotal.AddString(str_buf);
		}
	}else if(m_Resum == 0){
		str_buf.Format("문구01_DOM");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구02_DOM");
		m_lsWarningTotal.AddString(str_buf);
	}else if(m_Resum == 1){
		str_buf.Format("문구01_NA");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구02_NA");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구20_NA");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구21_NA");
		m_lsWarningTotal.AddString(str_buf);
	}else if(m_Resum == 2){
		str_buf.Format("문구02_EU");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구06_EU");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구07_EU");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구09_EU");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구10_EU");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구11_EU");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구12_EU");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구13_EU");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구14_EU");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구15_EU");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구16_EU");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구17_EU");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구18_EU");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구19_EU");
		m_lsWarningTotal.AddString(str_buf);
	}else if(m_Resum == 3){
		str_buf.Format("문구02_AUS");
		m_lsWarningTotal.AddString(str_buf);
	}else if(m_Resum == 4){
		str_buf.Format("문구04_CHN");
		m_lsWarningTotal.AddString(str_buf);
	}else if(m_Resum == 5){
		str_buf.Format("문구02_GEN");
		m_lsWarningTotal.AddString(str_buf);
		str_buf.Format("문구15_GEN");
		m_lsWarningTotal.AddString(str_buf);
	}
}

void CWarningDetect_Option::OnLbnSelchangeListWarningTotal()
{
	CString str;
	CString str1;
	CString str2;
	CString strCode = "";

	int sel;
	int lennum = 31;

	sel = m_lsWarningTotal.GetCurSel();
	m_lsWarningTotal.GetText(sel, str);

	for(int lop = 30;lop>=0;lop--){
		str1 = "";
		str2 = "";
		str1.Format("경고문구 %d",lop+1);
		str2.Format("문구%02d",lop+1);
		if(strstr(str, str1)||strstr(str, str2)){
			lennum = lop;
			break;
		}
	}

	if(lennum < 31){
		strCode.Format("0x%02X",LangCode[lennum].m_Code);
		SetDlgItemText(IDC_EDIT_WARNING_CODE,strCode);
		SetDlgItemText(IDC_EDIT_TINDEX_NAME,LangCode[lennum].str_DATA);
	}else{
		SetDlgItemText(IDC_EDIT_WARNING_CODE,"");
		SetDlgItemText(IDC_EDIT_TINDEX_NAME,"");
	}
}

void CWarningDetect_Option::OnLbnSelchangeListWarningCandidate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str;
	CString str1;
	CString str2;
	CString strCode = "";

	int sel;
	int lennum = 31;

	sel = m_lsWarningExtrCandidate.GetCurSel();
	m_lsWarningExtrCandidate.GetText(sel, str);

	for(int lop = 30;lop>=0;lop--){
		str1 = "";
		str2 = "";
		str1.Format("경고문구 %d",lop+1);
		str2.Format("문구%02d",lop+1);
		if(strstr(str, str1)||strstr(str, str2)){
			lennum = lop;
			break;
		}
	}
	if(lennum < 31){
		SetDlgItemText(IDC_EDIT_DINDEX_NAME,LangCode[lennum].str_DATA);
	}else{
		SetDlgItemText(IDC_EDIT_DINDEX_NAME,"");
	}
}

void CWarningDetect_Option::OnLbnSelchangeListWarning()
{
	CString str;
	CString str1;
	CString str2;

	int lennum = 31;
	int sel = m_lsWarningCheckList.GetCurSel();
	m_lsWarningCheckList.GetText(sel, str);
	
	for(int lop = 30;lop>=0;lop--){
		str1 = "";
		str2 = "";
		str1.Format("경고문구 %d",lop+1);
		str2.Format("문구%02d",lop+1);
		if(strstr(str, str1)||strstr(str, str2)){
			lennum = lop;
			break;
		}
	}

	if(lennum < 31){
		SetDlgItemText(IDC_EDIT_TEST_NAME,LangCode[lennum].str_DATA);
	}else{
		SetDlgItemText(IDC_EDIT_TEST_NAME,"");
	}

	IplImage *srcImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);
	
	srcImage = cvLoadImage(strSelectedModelPath + "\\Warning\\" + str + ".bmp");
	
	ShowWarningImage(srcImage);

	cvReleaseImage(&srcImage);
}



void CWarningDetect_Option::OnBnClickedBtnTotal2candidate()
{
	CString str;
	
	int nCount = m_lsWarningTotal.GetCount();
	for(int lop = 0;lop<nCount;lop++){
		if(m_lsWarningTotal.GetSel(lop) != 0){
			m_lsWarningTotal.GetText(lop, str);
			bool txtFlag = TRUE;
			CString compareStr;
			for(int i=0; i<m_lsWarningExtrCandidate.GetCount(); i++)
			{
				m_lsWarningExtrCandidate.GetText(i, compareStr);
				if(str == compareStr){
					txtFlag = FALSE;
				}
			}
			
			if(txtFlag){
				m_lsWarningExtrCandidate.AddString(str);
			}
			m_lsWarningTotal.SetSel(lop,0);
		}
	}
}

void CWarningDetect_Option::OnBnClickedBtnCandidate2total()
{
	CString str;
	int nCount = m_lsWarningExtrCandidate.GetCount();
	for(int lop = nCount-1;lop>=0;lop--){
		if(m_lsWarningExtrCandidate.GetSel(lop) != 0){
			m_lsWarningExtrCandidate.DeleteString(lop);
			SetDlgItemText(IDC_EDIT_DINDEX_NAME,"");
		}
	}
}

void CWarningDetect_Option::OnBnClickedBtnWarningMasterExtact()//추출
{
	if(m_lsWarningExtrCandidate.GetCount() <= 0){
		AfxMessageBox("추출할 항목이 없습니다.");
		((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->ShowWindow(SW_SHOW);
		return;
	}

	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->JIG_MOVE_CHK();
	
	((CImageTesterDlg *)m_pMomWnd)->Power_On();
// 	Wait(1000);
// 	((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(1);
// 	Wait(500);
	if(!((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK()){
		AfxMessageBox("카메라가 연결되어 있지 않습니다.");
		((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->ShowWindow(SW_SHOW);
		return;
	}

	if((m_lsWarningCheckList.GetCount() > 0)&&(m_lsWarningExtrCandidate.GetCount() > 0)){
		CString Wpath = strSelectedModelPath + "\\Warning";
		DeleteDir(Wpath);
		folder_gen(Wpath); 
	}

	BYTE inputCode;
	BYTE resCode;
	BOOL ret = FALSE;
	
	((CImageTesterDlg *)m_pMomWnd)->Overlay_Start();
	for(int i=0; i<m_lsWarningExtrCandidate.GetCount(); i++)
	{
		CString currStr;
		
		m_lsWarningExtrCandidate.GetText(i, currStr);
		
		if(strstr(currStr,"_DOM")){
			resCode = 0x00;
		}else if(strstr(currStr,"_NA")){
			resCode = 0x01;
		}else if(strstr(currStr,"_EU")){
			resCode = 0x02;
		}else if(strstr(currStr,"_AUS")){
			resCode = 0x03;
		}else if(strstr(currStr,"_CHN")){
			resCode = 0x04;
		}else if(strstr(currStr,"_GEN")){
			resCode = 0x05;
		}else{
			resCode = 0x00;
		}

		if(strstr(currStr, "경고문구 10")||strstr(currStr, "문구10"))
			inputCode = 0x0A;
		else if(strstr(currStr, "경고문구 11")||strstr(currStr, "문구11"))
			inputCode = 0x0B;
		else if(strstr(currStr, "경고문구 12")||strstr(currStr, "문구12"))
			inputCode = 0x0C;
		else if(strstr(currStr, "경고문구 13")||strstr(currStr, "문구13"))
			inputCode = 0x0D;
		else if(strstr(currStr, "경고문구 14")||strstr(currStr, "문구14"))
			inputCode = 0x0E;
		else if(strstr(currStr, "경고문구 15")||strstr(currStr, "문구15"))
			inputCode = 0x0F;
		else if(strstr(currStr, "경고문구 16")||strstr(currStr, "문구16"))
			inputCode = 0x10;
		else if(strstr(currStr, "경고문구 17")||strstr(currStr, "문구17"))
			inputCode = 0x11;
		else if(strstr(currStr, "경고문구 18")||strstr(currStr, "문구18"))
			inputCode = 0x12;
		else if(strstr(currStr, "경고문구 19")||strstr(currStr, "문구19"))
			inputCode = 0x13;
		else if(strstr(currStr, "경고문구 20")||strstr(currStr, "문구20"))
			inputCode = 0x14;
		else if(strstr(currStr, "경고문구 21")||strstr(currStr, "문구21"))
			inputCode = 0x15;
		else if(strstr(currStr, "경고문구 22")||strstr(currStr, "문구22"))
			inputCode = 0x16;
		else if(strstr(currStr, "경고문구 23")||strstr(currStr, "문구23"))
			inputCode = 0x17;
		else if(strstr(currStr, "경고문구 24")||strstr(currStr, "문구24"))
			inputCode = 0x18;
		else if(strstr(currStr, "경고문구 25")||strstr(currStr, "문구25"))
			inputCode = 0x19;
		else if(strstr(currStr, "경고문구 26")||strstr(currStr, "문구26"))
			inputCode = 0x1A;
		else if(strstr(currStr, "경고문구 27")||strstr(currStr, "문구27"))
			inputCode = 0x1B;
		else if(strstr(currStr, "경고문구 28")||strstr(currStr, "문구28"))
			inputCode = 0x1C;
		else if(strstr(currStr, "경고문구 29")||strstr(currStr, "문구29"))
			inputCode = 0x1D;
		else if(strstr(currStr, "경고문구 30")||strstr(currStr, "문구30"))
			inputCode = 0x1E;
		else if(strstr(currStr, "경고문구 31")||strstr(currStr, "문구31"))
			inputCode = 0x1F;
		else if(strstr(currStr, "경고문구 1")||strstr(currStr, "문구01"))
			inputCode = 0x01;
		else if(strstr(currStr, "경고문구 2")||strstr(currStr, "문구02"))
			inputCode = 0x02;
		else if(strstr(currStr, "경고문구 3")||strstr(currStr, "문구03"))
			inputCode = 0x03;
		else if(strstr(currStr, "경고문구 4")||strstr(currStr, "문구04"))
			inputCode = 0x04;
		else if(strstr(currStr, "경고문구 5")||strstr(currStr, "문구05"))
			inputCode = 0x05;
		else if(strstr(currStr, "경고문구 6")||strstr(currStr, "문구06"))
			inputCode = 0x06;
		else if(strstr(currStr, "경고문구 7")||strstr(currStr, "문구07"))
			inputCode = 0x07;
		else if(strstr(currStr, "경고문구 8")||strstr(currStr, "문구08"))
			inputCode = 0x08;
		else if(strstr(currStr, "경고문구 9")||strstr(currStr, "문구09"))
			inputCode = 0x09;

		GetWarningImage(inputCode,resCode,currStr);	
	}
	EtcCamInit();
	UpateMasterWarningFileList();
	//================
	SETTING_DATA();
	Set_List(&m_WarningWorkList);
}

void CWarningDetect_Option::EtcCamInit()
{
	BOOL ret = FALSE;
	for(int lop =0;lop<2;lop++){//우선 경고문구가 없는 번지로 바꾼다.
		ret = ((CImageTesterDlg *)m_pMomWnd)->Overlay_Move(0,0,0); 
		if(ret == TRUE){
			break;
		}
	}
	for(int lop = 0;lop<3;lop++){//초기화를 진행한다.
		ret = ((CImageTesterDlg *)m_pMomWnd)->Overlay_Init();
		if(ret == TRUE){
			break;
		}
	}
	//=====================================================
	((CImageTesterDlg *)m_pMomWnd)->Power_Off();
	DoEvents(1000);
	((CImageTesterDlg *)m_pMomWnd)->Power_On();
	DoEvents(500);
	//=====================================================
}

BOOL CWarningDetect_Option::GetWarningImage(BYTE lencode,BYTE rescod,CString Filename)
{
	int ret = FALSE;
	memset(m_RGBScanbuf,0,sizeof(m_RGBScanbuf));
	for(int lop =0;lop<3;lop++){
		ret = ((CImageTesterDlg *)m_pMomWnd)->Overlay_Move(lencode,rescod);//추출이 안될 경우 2초를 기다린다. 
		if(ret == TRUE){
			if(((CImageTesterDlg *)m_pMomWnd)->Buf_IMG_SUC == TRUE){
				break;
			}
		}
	}
	
	if((ret == FALSE)||(((CImageTesterDlg *)m_pMomWnd)->Buf_IMG_SUC == FALSE)){
		return FALSE;
	}

	IplImage *testImage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 3);

	for(int y = 0; y<480; y++)
	{
		for(int x=0; x<720; x++)
		{
			testImage->imageData[y * testImage->widthStep + x * 3 + 0] = m_RGBScanbuf[y * 2880 + x * 4 + 0];
			testImage->imageData[y * testImage->widthStep + x * 3 + 1] = m_RGBScanbuf[y * 2880 + x * 4 + 1];
			testImage->imageData[y * testImage->widthStep + x * 3 + 2] = m_RGBScanbuf[y * 2880 + x * 4 + 2];
		}
	}

	CString savePath;
	savePath = Filename += ".bmp";
	
	cvSaveImage(strSelectedModelPath + "\\Warning\\" + savePath, testImage);

	ShowWarningImage(testImage);
	
	cvReleaseImage(&testImage);

	return	TRUE;
}

void CWarningDetect_Option::UpateMasterWarningFileList()
{
	CFileFind folderfind;

	if(!folderfind.FindFile(strSelectedModelPath + "\\Warning")){ //원하는 폴더가 없으면 만든다.
		CreateDirectory(strSelectedModelPath + "\\Warning", NULL); 
	}

	m_lsWarningCheckList.ResetContent();
	
	int nCount = m_lsWarningExtrCandidate.GetCount();
	CString str;
	CString szTemp;
	CString strFileName;

	CFileFind finder;
	BOOL bWorking = FALSE;

	int nFailCount = 0;

	for(int _a=0; _a<nCount; _a++)
	{
		m_lsWarningExtrCandidate.GetText(_a, str);

		if(!str.IsEmpty())
		{
			strFileName.Format("%s.bmp", str);
			bWorking = finder.FindFile(strSelectedModelPath + "\\Warning\\" + strFileName);

			if(bWorking)
			{
				m_lsWarningCheckList.AddString(str);

				szTemp.Format(_T("WARNING_LIST_%d"), _a - nFailCount);
				WritePrivateProfileString(str_model,szTemp,str,str_ModelPath);
			}else
			{
				// 파일이 없을 경우
				nFailCount++;
			}
		}
	}

	str.Empty();
	str.Format("%d",nCount - nFailCount);
	WritePrivateProfileString(str_model,"WARNING_LIST_COUNT",str,str_ModelPath);

}

void CWarningDetect_Option::UpateMasterWarningFileList_ORG()
{
	CFileFind folderfind;

	if(!folderfind.FindFile(strSelectedModelPath + "\\Warning")){ //원하는 폴더가 없으면 만든다.
		CreateDirectory(strSelectedModelPath + "\\Warning", NULL); 
	}

	m_lsWarningCheckList.ResetContent();
	
	int nCount = m_lsWarningExtrCandidate.GetCount();
	
	CFileFind finder;

	BOOL bWorking = finder.FindFile(strSelectedModelPath + "\\Warning\\" +  "*.bmp");
		
	while( bWorking )
	{
		bWorking = finder.FindNextFile();
		CString str = finder.GetFileTitle();
		m_lsWarningCheckList.AddString(str);
	}
}

void CWarningDetect_Option::ShowWarningImage(IplImage *srcImage)
{
	if (srcImage == NULL)
	{
		return;
	}
	unsigned char *RGBBuf = new unsigned char [CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];

	BITMAPINFO		m_biInfo;
	ZeroMemory(&m_biInfo, sizeof(m_biInfo));
	m_biInfo.bmiHeader.biSize		= sizeof(BITMAPINFOHEADER);
	m_biInfo.bmiHeader.biWidth		= CAM_IMAGE_WIDTH;
	m_biInfo.bmiHeader.biHeight		= CAM_IMAGE_HEIGHT;
	m_biInfo.bmiHeader.biPlanes		= 1;
	m_biInfo.bmiHeader.biBitCount	= 32;
	m_biInfo.bmiHeader.biCompression	= 0;
	m_biInfo.bmiHeader.biSizeImage	= CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4;	

	CDC *pcDC;

	//-----------가상메모리를 만든다.------------------
	CDC	*pcMemDC = new CDC;
	CBitmap	*pcDIB	= new CBitmap;

	pcDC = m_stWarningFrame.GetDC();

	for(int y=0; y<CAM_IMAGE_HEIGHT; y++)
		for(int x=0; x<CAM_IMAGE_WIDTH; x++)
		{
			RGBBuf[y*(CAM_IMAGE_WIDTH*4) + x*4 + 0] = srcImage->imageData[y*srcImage->widthStep + x*3 + 0];
			RGBBuf[y*(CAM_IMAGE_WIDTH*4) + x*4 + 1] = srcImage->imageData[y*srcImage->widthStep + x*3 + 1];
			RGBBuf[y*(CAM_IMAGE_WIDTH*4) + x*4 + 2] = srcImage->imageData[y*srcImage->widthStep + x*3 + 2];
		}

	pcMemDC->CreateCompatibleDC(pcDC);
	pcDIB->CreateCompatibleBitmap(pcDC,CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT);
	CBitmap	*pcOldDIB = pcMemDC->SelectObject(pcDIB);
	
	//---------------------------------------------
	::SetStretchBltMode(pcMemDC->m_hDC, COLORONCOLOR);//MemDC.m_hDC//pcDC->m_hDC
	::StretchDIBits(pcMemDC->m_hDC, 
					0,  CAM_IMAGE_HEIGHT-1,
					CAM_IMAGE_WIDTH, -CAM_IMAGE_HEIGHT,
					0, 0,
					m_biInfo.bmiHeader.biWidth,
					m_biInfo.bmiHeader.biHeight,
					RGBBuf, &m_biInfo, DIB_RGB_COLORS, SRCCOPY);
//******************************************************************************

	CPen	my_Pan, *old_pan;

	::SetBkMode(pcMemDC->m_hDC,TRANSPARENT);
	my_Pan.CreatePen(PS_SOLID,3,RGB(255, 255, 255));
	old_pan = pcMemDC->SelectObject(&my_Pan);
	pcMemDC->MoveTo(m_dWarningStart_x,	m_dWarningStart_y);
	pcMemDC->LineTo(m_dWarningEnd_x,	m_dWarningStart_y);
	pcMemDC->LineTo(m_dWarningEnd_x,	m_dWarningEnd_y);
	pcMemDC->LineTo(m_dWarningStart_x,	m_dWarningEnd_y);
	pcMemDC->LineTo(m_dWarningStart_x,	m_dWarningStart_y);

	my_Pan.DeleteObject();
//******************************************************************************

	//--------------모니터 창에 맞추어 RGB정보를 할당한다.-------------------------
	::SetStretchBltMode(pcDC->m_hDC, COLORONCOLOR);
	::StretchBlt(	pcDC->m_hDC,
					0,  0,
					//720, 480,//1080, 720,
					360,228,
					pcMemDC->m_hDC,
					0, 0,
					CAM_IMAGE_WIDTH,
					CAM_IMAGE_HEIGHT,SRCCOPY);
	

	m_stWarningFrame.ReleaseDC(pcDC);

	delete pcDIB;
	delete pcMemDC;	
	delete []RGBBuf;
	
//	return TRUE;
}



void CWarningDetect_Option::OnBnClickedBtnWarningChecklistDelete()
{
	CString str;
	int sel;
	int cnt;

	cnt = m_lsWarningCheckList.GetCount();
	sel = m_lsWarningCheckList.GetCurSel();

	for(int lop = cnt-1;lop>=0;lop--){
		if(m_lsWarningCheckList.GetSel(lop) != 0){
			m_lsWarningCheckList.GetText(lop, str);
			DeleteFile(strSelectedModelPath + "\\Warning\\" + str + ".bmp");
			m_lsWarningCheckList.DeleteString(lop);
		}
	}

	int nCount = m_lsWarningCheckList.GetCount();
	CString szTemp;
	CString strFileName;

	CFileFind finder;
	BOOL bWorking = FALSE;

	int nFailCount = 0;

	for(int _a=0; _a<nCount; _a++)
	{
		m_lsWarningCheckList.GetText(_a, str);

		if(!str.IsEmpty())
		{
			strFileName.Format("%s.bmp", str);
			bWorking = finder.FindFile(strSelectedModelPath + "\\Warning\\" + strFileName);

			if(bWorking)
			{
				szTemp.Format(_T("WARNING_LIST_%d"), _a - nFailCount);
				WritePrivateProfileString(str_model,szTemp,str,str_ModelPath);
			}else
			{
				nFailCount++;
			}
		}
	}

	str.Empty();
	str.Format("%d",nCount - nFailCount);
	WritePrivateProfileString(str_model,"WARNING_LIST_COUNT",str,str_ModelPath);

	SetDlgItemText(IDC_EDIT_TEST_NAME,"");
}

void CWarningDetect_Option::OnBnClickedBtnWarningSave()
{
	Save_parameter();
}

bool CWarningDetect_Option::CompareWarning(LPBYTE IN_RGBIn_Target, IplImage *master_image,WarningData *sWarningData, int Cnt)
{
	int start_x, start_y;
	int end_x, end_y;

	start_x = GetDlgItemInt(IDC_EDIT_WARNING_START_X);
	start_y = GetDlgItemInt(IDC_EDIT_WARNING_START_Y);
	end_x = GetDlgItemInt(IDC_EDIT_WARNING_END_X);
	end_y = GetDlgItemInt(IDC_EDIT_WARNING_END_Y);
	
	double threshold = GetDlgItemInt(IDC_EDIT_MATCHING_RATIO);		
	threshold = threshold / 100.0;

	bool ret = CompareWarning(IN_RGBIn_Target,master_image,start_x, start_y, end_x, end_y, threshold, sWarningData, Cnt);
	return ret;
}

bool CWarningDetect_Option::CompareWarning(LPBYTE IN_RGBIn_Target, IplImage *master_image, int start_x, int start_y, int end_x, int end_y, double threshold, WarningData *sWarningData, int Cnt)
{
	int width = end_x - start_x;
	int height = end_y - start_y;

	IplImage *src_image = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	IplImage *target_image = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	
	BYTE R, G, B;
	double Sum_Y, src_Sum_Y;
	double total_Sum_Y=0;
	double srcImage_total_Sum_Y=0;

	for(int y=start_y; y<end_y; y++)
	{
		for(int x=start_x; x<end_x; x++)
		{
			B = master_image->imageData[y*master_image->widthStep + x*3 + 0];
			G = master_image->imageData[y*master_image->widthStep + x*3 + 1];
			R = master_image->imageData[y*master_image->widthStep + x*3 + 2];						

			if(R < 80 && G < 80 && B < 80)
				Sum_Y = 0;
			else
				Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
			total_Sum_Y += Sum_Y;

			src_image->imageData[(y-start_y)*src_image->widthStep+(x-start_x)] = Sum_Y;			

			B = IN_RGBIn_Target[y*(720*4) + x*4    ];
			G = IN_RGBIn_Target[y*(720*4) + x*4 + 1];
			R = IN_RGBIn_Target[y*(720*4) + x*4 + 2];
			
			if(R < 80 && G < 80 && B < 80)
				Sum_Y = 0;
			else
				Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));			
			
			srcImage_total_Sum_Y += Sum_Y;

			target_image->imageData[(y-start_y)*target_image->widthStep+(x-start_x)] = Sum_Y;
		}
	}	
	
	double min_val, max_val;

	IplImage *temp = cvCreateImage(cvSize(src_image->width - target_image->width+1, src_image->height - target_image->height+1), IPL_DEPTH_32F, 1);
	cvMatchTemplate(target_image, src_image, temp, CV_TM_CCOEFF_NORMED);
	cvMinMaxLoc(temp, &min_val, &max_val, 0, 0, 0);
	
	cvReleaseImage(&temp);
	cvReleaseImage(&src_image);
	cvReleaseImage(&target_image);
	
	sWarningData->templateRatio[Cnt] = max_val * 100.0;
	
	if(srcImage_total_Sum_Y < 1000 && total_Sum_Y < 1000)
		max_val = 100.0;
	else if(total_Sum_Y < 1000)
	{
		max_val = 0.0;
		sWarningData->templateRatio[Cnt] = 0;
	}

	if(max_val > threshold)
		return TRUE;
	else
		return FALSE;
}

void CWarningDetect_Option::SETTING_DATA(){
	TEST_LIST_COUNT=0;	//검사항목 수
	TEST_LIST_COUNT = m_lsWarningCheckList.GetCount();
	
	int AttrCnt = 0;
	for(int i=0; i<m_lsWarningCheckList.GetCount(); i++)
	{	
		CString str;
		m_lsWarningCheckList.GetText(i, str);
		TESTNAME[AttrCnt] = str;
		AttrCnt++;
	}

	for(int j=0; j<50; j++)
	{
		m_dTemplateRatio[j] = 0;
		m_dResult[j] = FALSE;
	}
}

void CWarningDetect_Option::OnBnClickedBtnSetWarningArea()
{
	if(m_lsWarningCheckList.GetCount() == 0)
		return;
	
	m_dWarningStart_x = GetDlgItemInt(IDC_EDIT_WARNING_START_X);
	m_dWarningStart_y = GetDlgItemInt(IDC_EDIT_WARNING_START_Y);
	m_dWarningEnd_x = GetDlgItemInt(IDC_EDIT_WARNING_END_X);
	m_dWarningEnd_y = GetDlgItemInt(IDC_EDIT_WARNING_END_Y);	

	CString str;
	int sel = m_lsWarningCheckList.GetCurSel();

	if(sel == -1)
		sel = 0;

	m_lsWarningCheckList.GetText(sel, str);

	
	IplImage *srcImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);
	srcImage = cvLoadImage(strSelectedModelPath + "\\Warning\\" + str + ".bmp");
	
	if(!srcImage)
		return;

	ShowWarningImage(srcImage);

	cvReleaseImage(&srcImage);
}


//----------------worklist
void CWarningDetect_Option::Set_List(CListCtrl *List){
	
	int count=0;
	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);

	for(int t=0; t<TEST_LIST_COUNT; t++){
		int k = t*2;
		List->InsertColumn(4+k,TESTNAME[t],LVCFMT_CENTER, 80);
		List->InsertColumn(5+k,TESTNAME[t]+"_RESULT",LVCFMT_CENTER, 80);
		count+=2;
	}
	List->InsertColumn(count+4,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(count+5,"비고",LVCFMT_CENTER, 80);

	ListItemNum=count+6;
	Copy_List(&m_WarningWorkList,((CImageTesterDlg *)m_pMomWnd)->m_pWorkList,ListItemNum);


}

void CWarningDetect_Option::LOT_Set_List(CListCtrl *List){
	
	int count=0;
	while(List->DeleteColumn(0));
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);

	for(int t=0; t<TEST_LIST_COUNT; t++){
		int k = t*2;
		List->InsertColumn(4+k,TESTNAME[t],LVCFMT_CENTER, 80);
		List->InsertColumn(5+k,TESTNAME[t]+"_RESULT",LVCFMT_CENTER, 80);
		count+=2;
	}
	List->InsertColumn(count+4,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(count+5,"비고",LVCFMT_CENTER, 80);

	Lot_InsertNum=count+6;


}

void CWarningDetect_Option::InsertList(){
	
	for(int k=0; k<100; k++){
		TESTLOT[k] = "";
	}		
	
	TESTLOT[1]=((CImageTesterDlg *)m_pMomWnd)->modelName;
	TESTLOT[2]=((CImageTesterDlg *)m_pMomWnd)->strDay;
	TESTLOT[3]=((CImageTesterDlg *)m_pMomWnd)->strTime;
}

void CWarningDetect_Option::FAIL_UPLOAD(){
	if ((((CImageTesterDlg *)m_pMomWnd)->LotMod == 1) && (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)){
		InsertList();
		TESTLOT[ListItemNum-2]="FAIL";
		TESTLOT[ListItemNum-1]="STOP";
		Setup_List();
		//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_WARNNING,"STOP");//토탈 result
		StartCnt++;
		b_StopFail = TRUE;
	}
}

void CWarningDetect_Option::Setup_List(){
	
	CString strCnt ="";
	int count = StartCnt;
	InsertIndex = m_WarningWorkList.InsertItem(count,"",0);
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_WarningWorkList.SetItemText(InsertIndex,0,strCnt);
	for(int t=0; t<ListItemNum-1; t++){
		m_WarningWorkList.SetItemText(InsertIndex,t+1,TESTLOT[t+1]);
	}			
}

void CWarningDetect_Option::EXCEL_SAVE(){
	CString Item[100]={"",};//수정해야할 곳 0203
	int count =0;
	for(int t=0; t<TEST_LIST_COUNT; t++){
		int k = t*2;
		Item[k]=TESTNAME[t];
		Item[k+1]=TESTNAME[t]+"_RESULT";
		count+=2;
	}
	CString Data ="";
	CString str="";
	str = "***********************경고문구 검사***********************\n\n";
	Data += str;
	
	str.Empty();
	str.Format("매칭율, %d,\n ", m_dMatchingRatio);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("경고문구검사",Item,count,&m_WarningWorkList,Data);
}

void CWarningDetect_Option::LOT_EXCEL_SAVE(){
	CString Item[100]={"",};//수정해야할 곳 0203
	int count =0;
	for(int t=0; t<TEST_LIST_COUNT; t++){
		int k = t*2;
		Item[k]=TESTNAME[t];
		Item[k+1]=TESTNAME[t]+"_RESULT";
		count+=2;
	}
	CString Data ="";
	CString str="";
	str = "***********************경고문구 검사***********************\n\n";
	Data += str;
	
	str.Empty();
	str.Format("매칭율, %d,\n ", m_dMatchingRatio);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("경고문구검사",Item,count,&m_Lot_WarningWorkList,Data);
}

bool CWarningDetect_Option::EXCEL_UPLOAD(){
	m_WarningWorkList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("경고문구검사",&m_WarningWorkList,1,&StartCnt) == TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return 0;
}

bool CWarningDetect_Option::LOT_EXCEL_UPLOAD(){
	m_WarningWorkList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("경고문구검사",&m_Lot_WarningWorkList,1,&StartCnt) == TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return 0;
}

void CWarningDetect_Option::Pic(CDC *cdc)
{
	
}
void CWarningDetect_Option::InitPrm(){
	Load_parameter();
	UpdateData(FALSE);	
	SETTING_DATA();
}


void CWarningDetect_Option::OnCbnSelchangeComboRes()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	m_Resum = pResCombo->GetCurSel();
	if(m_Resum != -1){
		str.Format("0x%02X",ResCode[m_Resum].m_Code);
		SetDlgItemText(IDC_EDIT_RES_CODE,str);
	}else{
		SetDlgItemText(IDC_EDIT_RES_CODE,"");
	}
	WarningList_Set();
	SetDlgItemText(IDC_EDIT_TINDEX_NAME,"");
	SetDlgItemText(IDC_EDIT_WARNING_CODE,"");

}


void CWarningDetect_Option::OnBnClickedCheckResex()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	WarningList_Set();
}

void CWarningDetect_Option::OnEnChangeEditTestName()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CWarningDetect_Option::OnBnClickedBtnTotal2candidate2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str;
	CString compareStr;
	bool txtFlag = TRUE;
	for(int t=0; t<m_lsWarningTotal.GetCount(); t++)
	{

		m_lsWarningTotal.GetText(t,str);
		
		txtFlag = TRUE;
		for(int i=0; i<m_lsWarningExtrCandidate.GetCount(); i++)
		{
			m_lsWarningExtrCandidate.GetText(i, compareStr);

			if(str == compareStr){
				txtFlag = FALSE;
			}
		}
		
		if(txtFlag){
			m_lsWarningExtrCandidate.AddString(str);
		}
	}
}

void CWarningDetect_Option::OnBnClickedBtnCandidate2total2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str;
	CString compareStr;
	bool txtFlag = TRUE;
	for(int t=0; t<m_lsWarningTotal.GetCount(); t++)
	{
		m_lsWarningTotal.GetText(t,str);
		txtFlag = TRUE;
		for(int i=0; i<m_lsWarningExtrCandidate.GetCount(); i++)
		{
			m_lsWarningExtrCandidate.GetText(i, compareStr);
			if(str == compareStr){
				m_lsWarningExtrCandidate.DeleteString(i);
				break;
			}
		}
	}
}

bool CWarningDetect_Option::CompareWarningOnUltimateImage(LPBYTE IN_RGBIn_Target, IplImage *master_image, int start_x, int start_y, int end_x, int end_y, double threshold)
{
	int width = end_x - start_x;
	int height = end_y - start_y;

	IplImage *src_image = cvCreateImage(cvSize(master_image->width, master_image->height), IPL_DEPTH_8U, 1);
	IplImage *target_image = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 1);
	IplImage *Master_target_image;// = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);

	BYTE R, G, B;
	double Sum_Y;
	double total_Sum_Y=0;
	
	for(int y=0; y<master_image->height; y++)
	{
		for(int x=0; x<master_image->width; x++)
		{
			B = master_image->imageData[y*master_image->widthStep + x*3 + 0];
			G = master_image->imageData[y*master_image->widthStep + x*3 + 1];
			R = master_image->imageData[y*master_image->widthStep + x*3 + 2];
			
			if(R < 80 && G < 80 && B < 80)
				Sum_Y = 0;
			else
				Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
			total_Sum_Y += Sum_Y;

			src_image->imageData[(y)*src_image->widthStep+(x)] = Sum_Y;			
		}
	}
	
	for(int y=0; y<480; y++)
	{
		for(int x=0; x<720; x++)
		{
			B = IN_RGBIn_Target[y*(720*4) + x*4    ];
			G = IN_RGBIn_Target[y*(720*4) + x*4 + 1];
			R = IN_RGBIn_Target[y*(720*4) + x*4 + 2];
			
			if(R < 80 && G < 80 && B < 80)
				Sum_Y = 0;
			else
				Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));

			target_image->imageData[y*target_image->widthStep+x] = Sum_Y;
		}
	}

	Master_target_image = GetWarningArea(src_image);

	double min_val, max_val;	

	IplImage *temp = cvCreateImage(cvSize(target_image->width - Master_target_image->width+1, target_image->height - Master_target_image->height+1), IPL_DEPTH_32F, 1);
	cvMatchTemplate(Master_target_image, target_image, temp, CV_TM_CCOEFF_NORMED);
	cvMinMaxLoc(temp, &min_val, &max_val, 0, 0, 0);

	cvReleaseImage(&temp);
	cvReleaseImage(&src_image);
	cvReleaseImage(&target_image);
	cvReleaseImage(&Master_target_image );
	
	if(total_Sum_Y < 1000)
	{
		max_val = 0.0;
	}

	if(max_val > threshold){
		return TRUE;
	}else{
		return FALSE;
	}
}

IplImage* CWarningDetect_Option::GetWarningArea(IplImage *srcImage)
{
	IplImage *GrayImage = cvCreateImage(cvSize(srcImage->width, srcImage->height), IPL_DEPTH_8U, 1);
	IplImage *BinImage = cvCreateImage(cvSize(srcImage->width, srcImage->height), IPL_DEPTH_8U, 1);
	IplImage *DummyImage = cvCreateImage(cvSize(srcImage->width, srcImage->height), IPL_DEPTH_8U, 1);
	IplImage *WordCandidateImage = cvCreateImage(cvSize(srcImage->width, srcImage->height), IPL_DEPTH_8U, 1);
	IplImage *WordArea=NULL;
	
	cvSetZero(WordCandidateImage);

	BYTE R, G, B;
	BYTE Sum_Y;
	
	for(int y=0; y<GrayImage->height; y++)
	{
		for(int x=0; x<GrayImage->width; x++)
		{

			Sum_Y = srcImage->imageData[y * srcImage->widthStep + x];

			GrayImage->imageData[y * GrayImage->widthStep + x] = Sum_Y;
		}
	}
	
	cvThreshold(GrayImage, BinImage, 0, 255, CV_THRESH_OTSU);

	cvDilate(BinImage, BinImage);
	cvDilate(BinImage, BinImage);
	
	cvCopyImage(BinImage, DummyImage);

	CvSeq *contour = 0;	
	CvMemStorage* contour_storage = cvCreateMemStorage(0);	

	cvFindContours(BinImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);		

	for( ; contour != 0; contour=contour->h_next)
	{	
		CvRect rect = cvContourBoundingRect(contour, 1);
		
		int centerX, centerY;
		centerX = rect.x + rect.width/2;
		centerY = rect.y + rect.height/2;

		cvDrawContours(WordCandidateImage, contour, CV_RGB(255, 255, 255), CV_RGB(255, 255, 255), 1, CV_FILLED, 8);
		cvLine(WordCandidateImage, cvPoint(centerX-rect.width, centerY), cvPoint(centerX+rect.width, centerY), CV_RGB(255, 255, 255), 3, 8);
	}
	
	CvSeq *Wordcontour = 0;
	CvMemStorage* Wordcontour_storage = cvCreateMemStorage(0);
	int ObjCnt = 0;
	
	cvFindContours(WordCandidateImage, Wordcontour_storage, &Wordcontour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);		

	for( ; Wordcontour != 0; Wordcontour=Wordcontour->h_next)
	{	
		CvRect rect = cvContourBoundingRect(Wordcontour, 1);
		
		int centerX, centerY;
		centerX = rect.x + rect.width/2;
		centerY = rect.y + rect.height/2;		
		
		if(rect.y < srcImage->height/2)
		{
			int startX = rect.x, endX = rect.x+rect.width;
			
			for(int x=rect.x; x<rect.x+rect.width; x++)
			{
				BOOL WhiteFlag = FALSE;

				for(int y=rect.y; y<rect.y+rect.height; y++)
				{
					if((unsigned char)DummyImage->imageData[y * DummyImage->widthStep + x] == 255 )
					{
						startX = x;
						WhiteFlag = TRUE;
						break;
					}					
				}
				if(WhiteFlag)
					break;
			}
			for(int x=rect.x+rect.width; x>rect.x; x--)
			{
				BOOL WhiteFlag = FALSE;

				for(int y=rect.y; y<rect.y+rect.height; y++)
				{
					if((unsigned char)DummyImage->imageData[y * DummyImage->widthStep + x] == 255)
					{
						endX = x;
						WhiteFlag = TRUE;
						break;
					}					
				}
				if(WhiteFlag)
					break;
			}
			
			int X_margin = (startX - rect.x) + ((rect.x+rect.width) - endX);

			WordArea = cvCreateImage(cvSize(rect.width - X_margin, rect.height), IPL_DEPTH_8U, 1);
			cvSetImageROI(GrayImage, cvRect(startX, rect.y, rect.width - X_margin, rect.height));
			cvCopyImage(GrayImage, WordArea);
			cvResetImageROI(GrayImage);
			
			ObjCnt++;
			
			CString str;
			str.Format("%d", ObjCnt);
		}
	}	

	cvReleaseImage(&GrayImage);
	cvReleaseImage(&BinImage);
	cvReleaseImage(&DummyImage);
	cvReleaseMemStorage(&contour_storage);
	cvReleaseMemStorage(&Wordcontour_storage);

	return WordArea;
}
BOOL CWarningDetect_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CWarningDetect_Option::OnBnClickedCheckReset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);


	CString str="";
	str.Empty();
	str.Format("%d",b_Chk_EndRst);
	WritePrivateProfileString(str_model,"WARNING_ENDRSTEN",str,str_ModelPath);

	EnableEndRststate(b_Chk_EndRst);

}

void CWarningDetect_Option::EnableEndRststate(BOOL state)
{
	BOOL MODE;
	if(state == 1){
		MODE = 0;
	}else{
		MODE = 1;
	}
	((CButton *)GetDlgItem(IDC_BTN_WARNIG_ADD))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BTN_WARNIG_DEL))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BTN_WARNING_TEST))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BTN_WARNING_INIT))->EnableWindow(MODE);
}

void CWarningDetect_Option::OnBnClickedBtnWarnigAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str;
	int sel;
	sel = m_lsWarningTotal.GetCurSel();
	if(sel != -1)
	{
		m_lsWarningTotal.GetText(sel, str_List_End);
		WritePrivateProfileString(str_model,"WARNING_LIST_END",str_List_End,str_ModelPath);
	}
	UpdateData(FALSE);
}

void CWarningDetect_Option::OnBnClickedBtnWarnigDel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	str_List_End = "";
	WritePrivateProfileString(str_model,"WARNING_LIST_END",str_List_End,str_ModelPath);
	UpdateData(FALSE);
}

void CWarningDetect_Option::OnBnClickedBtnWarningInit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	EnableEndRststate(1);
	EtcCamInit();
	EnableEndRststate(0);
}

void CWarningDetect_Option::OnBnClickedBtnWarningTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	EnableEndRststate(1);
	DoEvents(100);
	m_sWarningData.enable = TRUE;
	((CImageTesterDlg *)m_pMomWnd)->Overlay_Start();
	Warning_END_TEST();
	EnableEndRststate(0);
}

void CWarningDetect_Option::OnBnClickedBtnCandidate2clr()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int TCNT = m_lsWarningExtrCandidate.GetCount();

	for(int t=0; t<TCNT; t++)
	{
		m_lsWarningExtrCandidate.DeleteString(0);
	}
}

BOOL CWarningDetect_Option::WarningChange(CString FName)
{
	BOOL ret = FALSE;
	BYTE resCode = 0x00;
	BYTE inputCode = 0x00;
	
	CString currStr = FName;

	if(strstr(currStr,"_DOM")){
		resCode = 0x00;
	}else if(strstr(currStr,"_NA")){
		resCode = 0x01;
	}else if(strstr(currStr,"_EU")){
		resCode = 0x02;
	}else if(strstr(currStr,"_AUS")){
		resCode = 0x03;
	}else if(strstr(currStr,"_CHN")){
		resCode = 0x04;
	}else if(strstr(currStr,"_GEN")){
		resCode = 0x05;
	}else{
		resCode = 0x00;
	}

	if(strstr(currStr, "경고문구 10")||strstr(currStr, "문구10"))
		inputCode = 0x0A;
	else if(strstr(currStr, "경고문구 11")||strstr(currStr, "문구11"))
		inputCode = 0x0B;
	else if(strstr(currStr, "경고문구 12")||strstr(currStr, "문구12"))
		inputCode = 0x0C;
	else if(strstr(currStr, "경고문구 13")||strstr(currStr, "문구13"))
		inputCode = 0x0D;
	else if(strstr(currStr, "경고문구 14")||strstr(currStr, "문구14"))
		inputCode = 0x0E;
	else if(strstr(currStr, "경고문구 15")||strstr(currStr, "문구15"))
		inputCode = 0x0F;
	else if(strstr(currStr, "경고문구 16")||strstr(currStr, "문구16"))
		inputCode = 0x10;
	else if(strstr(currStr, "경고문구 17")||strstr(currStr, "문구17"))
		inputCode = 0x11;
	else if(strstr(currStr, "경고문구 18")||strstr(currStr, "문구18"))
		inputCode = 0x12;
	else if(strstr(currStr, "경고문구 19")||strstr(currStr, "문구19"))
		inputCode = 0x13;
	else if(strstr(currStr, "경고문구 20")||strstr(currStr, "문구20"))
		inputCode = 0x14;
	else if(strstr(currStr, "경고문구 21")||strstr(currStr, "문구21"))
		inputCode = 0x15;
	else if(strstr(currStr, "경고문구 22")||strstr(currStr, "문구22"))
		inputCode = 0x16;
	else if(strstr(currStr, "경고문구 23")||strstr(currStr, "문구23"))
		inputCode = 0x17;
	else if(strstr(currStr, "경고문구 24")||strstr(currStr, "문구24"))
		inputCode = 0x18;
	else if(strstr(currStr, "경고문구 25")||strstr(currStr, "문구25"))
		inputCode = 0x19;
	else if(strstr(currStr, "경고문구 26")||strstr(currStr, "문구26"))
		inputCode = 0x1A;
	else if(strstr(currStr, "경고문구 27")||strstr(currStr, "문구27"))
		inputCode = 0x1B;
	else if(strstr(currStr, "경고문구 28")||strstr(currStr, "문구28"))
		inputCode = 0x1C;
	else if(strstr(currStr, "경고문구 29")||strstr(currStr, "문구29"))
		inputCode = 0x1D;
	else if(strstr(currStr, "경고문구 30")||strstr(currStr, "문구30"))
		inputCode = 0x1E;
	else if(strstr(currStr, "경고문구 31")||strstr(currStr, "문구31"))
		inputCode = 0x1F;
	else if(strstr(currStr, "경고문구 1")||strstr(currStr, "문구01"))
		inputCode = 0x01;
	else if(strstr(currStr, "경고문구 2")||strstr(currStr, "문구02"))
		inputCode = 0x02;
	else if(strstr(currStr, "경고문구 3")||strstr(currStr, "문구03"))
		inputCode = 0x03;
	else if(strstr(currStr, "경고문구 4")||strstr(currStr, "문구04"))
		inputCode = 0x04;
	else if(strstr(currStr, "경고문구 5")||strstr(currStr, "문구05"))
		inputCode = 0x05;
	else if(strstr(currStr, "경고문구 6")||strstr(currStr, "문구06"))
		inputCode = 0x06;
	else if(strstr(currStr, "경고문구 7")||strstr(currStr, "문구07"))
		inputCode = 0x07;
	else if(strstr(currStr, "경고문구 8")||strstr(currStr, "문구08"))
		inputCode = 0x08;
	else if(strstr(currStr, "경고문구 9")||strstr(currStr, "문구09"))
		inputCode = 0x09;

	for(int lop =0;lop<3;lop++){
		ret = ((CImageTesterDlg *)m_pMomWnd)->Overlay_Move(inputCode,resCode);//추출이 안될 경우 2초를 기다린다. 
		if(ret == TRUE){
			if(((CImageTesterDlg *)m_pMomWnd)->Buf_IMG_SUC == TRUE){
				break;
			}
		}
	}
	
	if((ret == FALSE)||(((CImageTesterDlg *)m_pMomWnd)->Buf_IMG_SUC == FALSE)){
		return FALSE;
	}else{
		return TRUE;
	}

}

void CWarningDetect_Option::Warning_END_TEST() //오버레이 조향각 Angle만큼 움직임 
{
	if(b_Chk_EndRst == TRUE){
		EtcCamInit();	
		return;
	}  
	if(str_List_End == ""){
		return;
	}
	
	WarningChange(str_List_End);//경고문구를 변경하고 이미지를 추출한다. 

	DoEvents(500);
	((CImageTesterDlg *)m_pMomWnd)->Power_Off();
	DoEvents(1000);
	((CImageTesterDlg *)m_pMomWnd)->Power_On();
	DoEvents(500);
}

void CWarningDetect_Option::Warning_INIT_TEST() //오버레이 조향각 Angle만큼 움직임 
{
	CString str = "";
	CString RES = "";
	CString NUM = "";

	m_lsWarningCheckList.GetText(0, str);
	if(str == ""){
		return;
	}
	CString tstbuf = str;
	
	((CImageTesterDlg *)m_pMomWnd)->Overlay_Start();

	((CImageTesterDlg *)m_pMomWnd)->Get_Wait = FALSE;
	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = FALSE;	
	
}

BOOL CWarningDetect_Option::Warning_Etc_Compare(int num) //오버레이 조향각 Angle만큼 움직임 
{
	CString str = "";
	CString RES = "";
	CString NUM = "";
	BOOL ret = FALSE;

	m_lsWarningCheckList.GetText(num, str);

	IplImage *MasterImage = cvLoadImage(strSelectedModelPath + "\\Warning\\" + str + ".bmp", 1);
	if(!MasterImage){
		return FALSE;
	}
	CString tstbuf = str;

	
	ret = WarningChange(tstbuf);//경고문구를 변경하고 이미지를 추출한다. 

//	((CImageTesterDlg *)m_pMomWnd)->Get_Wait = FALSE;
//	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = FALSE;	

	bool result = FALSE;

	if(ret == FALSE){
		m_sWarningData.templateRatio[num] = 0.0;
		result = FALSE;
	}else{
		if(m_sWarningData.enable == TRUE){
			result = CompareWarning(m_RGBScanbuf, MasterImage, &m_sWarningData, num);
		}
	}

	m_sWarningData.testName[num] = str;
	m_sWarningData.WarningCheckResult[num] = result;
	NUM.Empty();
	NUM.Format("%3.1f",m_sWarningData.templateRatio[num]);
	RES.Empty();
	if(result == TRUE){
		RES = "PASS";	
	}else{
		RES = "FAIL";			
	}
	int k = num*2;
	TESTLOT[4+k]=NUM;
	TESTLOT[5+k]=RES;

	if( m_sWarningData.enable == TRUE)
	{	
		((CImageTesterDlg *)m_pMomWnd)->m_pResWarning_CAN_OptWnd->m_listCh1WarningResult.InsertItem(num, "");
		((CImageTesterDlg *)m_pMomWnd)->m_pResWarning_CAN_OptWnd->m_listCh1WarningResult.SetItemText(num, 0, m_sWarningData.testName[num]);

		if(m_sWarningData.WarningCheckResult[num] == TRUE)
		{
			((CImageTesterDlg *)m_pMomWnd)->m_pResWarning_CAN_OptWnd->m_listCh1WarningResult.SetItemText(num, 1, "OK");
		}
		else
		{
			((CImageTesterDlg *)m_pMomWnd)->m_pResWarning_CAN_OptWnd->m_listCh1WarningResult.SetItemText(num, 1, "FAIL");
		}

		((CImageTesterDlg *)m_pMomWnd)->m_pResWarning_CAN_OptWnd->m_listCh1WarningResult.SetItemText(num, 2, NUM);					
	}
	
	((CImageTesterDlg *)m_pMomWnd)->Get_Wait = FALSE;
	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = FALSE;	

	cvReleaseImage(&MasterImage);

	return TRUE;
}


tResultVal CWarningDetect_Option::Run(){


	tResultVal retval = {0,};
	b_StopFail = FALSE;
	((CImageTesterDlg *)m_pMomWnd)->m_pResWarning_CAN_OptWnd->m_listCh1WarningResult.DeleteAllItems();

	int warningCnt = m_lsWarningCheckList.GetCount();
	int totalCheckCnt = warningCnt+1;

	if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand==TRUE){
		for(int k=0; k<100; k++){
			TESTLOT[k]="";
		}
		InsertList();
	}
	
	for(int j=0; j<50; j++){
		m_sWarningData.templateRatio[j] = 0.0;
	}
	m_sWarningData.enable = TRUE;

	for(int i=0; i<totalCheckCnt; i++)
	{
		m_sWarningData.WarningCheckResult[i] = FALSE;
	}


	if(warningCnt > 0){
		Warning_INIT_TEST();
		for(int i=0; i<warningCnt; i++)
		{
			Warning_Etc_Compare(i);
		}
	}
	
	totalCheckCnt = warningCnt;

	bool FLAG = TRUE;
	if(warningCnt > 0){
		for(int i=0; i<warningCnt; i++)
		{
			if(m_sWarningData.WarningCheckResult[i] == TRUE)
			{

			}else{
				FLAG = FALSE;
			}
		}
	}
	
	retval.m_Success = 0;
	retval.ValString = "경고문구 검사 실패";

	if(FLAG == TRUE){
		retval.m_Success = 1;
		retval.ValString = "경고문구 검사 성공";
		TESTLOT[(TEST_LIST_COUNT*2+4)] ="PASS";
		Str_Mes[0] = "PASS";
		Str_Mes[1] = "1";
	}else if(FLAG == FALSE){
		retval.m_Success = 0;
		retval.ValString = "경고문구 검사 실패";
		TESTLOT[(TEST_LIST_COUNT*2+4)] ="FAIL";
		Str_Mes[0] = "FAIL";
		Str_Mes[1] = "0";	
	}
	Warning_END_TEST();

	if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand==TRUE){
		if(retval.m_Success == TRUE)
		{
		//	((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_WARNNING,"TRUE");
		}
		else
		{	
		//	((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_WARNNING,"FAIL");
		}
		Setup_List();
		StartCnt++;
	}
	//================================================================================================

	return retval;
}

void CWarningDetect_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if(bShow == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
	
	}
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

CString CWarningDetect_Option ::Mes_Result()
{
	CString sz;
	m_szMesResult = _T("");
	if(b_StopFail == FALSE){
	m_szMesResult.Format(_T("%s:%s"),Str_Mes[0],Str_Mes[1]);
	}else{
		m_szMesResult.Format(_T(":1"));
	}
	return m_szMesResult;
}

void CWarningDetect_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CButton *)GetDlgItem(IDC_CHECK_RESET))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_WARNIG_ADD))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_WARNIG_DEL))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_WARNING_TEST))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_WARNING_INIT))->EnableWindow(0);
//		((CButton *)GetDlgItem(IDC_CHECK_RESEX))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_WARNING_CHECKLIST_DELETE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_WARNING_MASTER_EXTACT))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_CANDIDATE2CLR))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_TOTAL2CANDIDATE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_CANDIDATE2TOTAL))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_TOTAL2CANDIDATE2))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_CANDIDATE2TOTAL2))->EnableWindow(0);

		((CButton *)GetDlgItem(IDC_BTN_SET_WARNING_AREA))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_WARNING_SAVE))->EnableWindow(0);
		
		((CEdit *)GetDlgItem(IDC_EDIT_WARNING_START_X))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_WARNING_START_Y))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_WARNING_END_X))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_WARNING_END_Y))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MATCHING_RATIO))->EnableWindow(0);
		
	//	m_lsWarningCheckList.EnableWindow(0);
	//	m_lsWarningExtrCandidate.EnableWindow(0);
	//	m_lsWarningTotal.EnableWindow(0);
		
	//	pResCombo->EnableWindow(0);
	}
}