#pragma once


// CNewLot 대화 상자입니다.

class CNewLot : public CDialog
{
	DECLARE_DYNAMIC(CNewLot)

public:
	CNewLot(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CNewLot();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_LOT };
	CComboBox	*pModelCombo;
	CString MODELNAME;
	void	Setup(CWnd* IN_pMomWnd);
	int m_combo_sel;
	bool b_ModelApply;
	COLORREF txcol,bkcol;
	void	Model_Select_Enable(unsigned char col,LPCSTR lpcszString, ...);
	void Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	CFont ft,ft1;
	void	InitEVMS();

private:
	CWnd		*m_pMomWnd;
	int		backmodelcombo;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnCbnSelchangeComboModelLot();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CString m_LotOperator;
	CString m_LotInfo;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	int SerialStartNum;
	afx_msg void OnBnClickedButtonApply();
	afx_msg void OnBnClickedButtonmodify();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnSetfocusEditModelName();
};
