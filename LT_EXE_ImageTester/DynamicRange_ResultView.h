#pragma once


// CDynamicRange_ResultView 대화 상자입니다.

class CDynamicRange_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CDynamicRange_ResultView)

public:
	CDynamicRange_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDynamicRange_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_DYNAMICRANGE };
	void	Setup(CWnd* IN_pMomWnd);
	void	Initstat();
	void	Font_Size_Change(int nID, CFont *font, LONG Weight, LONG Height);

	void	DYNAMICRANGE_TEXT(LPCSTR lpcszString, ...);
	void	DYNAMICRANGENUM_TEXT(LPCSTR lpcszString, ...);
	void	DYNAMICRANGENUM_TEXT5(LPCSTR lpcszString, ...);
	void	RESULT_TEXT(unsigned char col, LPCSTR lpcszString, ...);
	void	DYNAMICRANGE_TEXT2(LPCSTR lpcszString, ...);
	void	DYNAMICRANGENUM_TEXT2(LPCSTR lpcszString, ...);
	void	DYNAMICRANGENUM_TEXT6(LPCSTR lpcszString, ...);
	void	RESULT_TEXT2(unsigned char col, LPCSTR lpcszString, ...);
	void	DYNAMICRANGE_TEXT3(LPCSTR lpcszString, ...);
	void	DYNAMICRANGENUM_TEXT3(LPCSTR lpcszString, ...);
	void	DYNAMICRANGENUM_TEXT7(LPCSTR lpcszString, ...);
	void	RESULT_TEXT3(unsigned char col, LPCSTR lpcszString, ...);
	void	DYNAMICRANGE_TEXT4(LPCSTR lpcszString, ...);
	void	DYNAMICRANGENUM_TEXT4(LPCSTR lpcszString, ...);
	void	DYNAMICRANGENUM_TEXT8(LPCSTR lpcszString, ...);
	void	RESULT_TEXT4(unsigned char col, LPCSTR lpcszString, ...);

	void	DYNAMICRANGENUM_TEXT(int NUM, LPCSTR lpcszString, ...);
	void	RESULT_TEXT(int NUM, unsigned char col, LPCSTR lpcszString, ...);
	CFont ft1;
	COLORREF txcol_TVal, bkcol_TVal;
	COLORREF txcol_TVal2, bkcol_TVal2;
	COLORREF txcol_TVal3, bkcol_TVal3;
	COLORREF txcol_TVal4, bkcol_TVal4;
private:
	CWnd	*m_pMomWnd;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnEnSetfocusEditDynamicrangetxt();
	afx_msg void OnEnSetfocusEditDynamicrangenum();
	afx_msg void OnEnSetfocusEditDynamicrangenum5();
	afx_msg void OnEnSetfocusEditDynamicrangeresult();
	afx_msg void OnEnSetfocusEditDynamicrangetxt2();
	afx_msg void OnEnSetfocusEditDynamicrangenum2();
	afx_msg void OnEnSetfocusEditDynamicrangenum6();
	afx_msg void OnEnSetfocusEditDynamicrangeresult2();
	afx_msg void OnEnSetfocusEditDynamicrangetxt3();
	afx_msg void OnEnSetfocusEditDynamicrangenum3();
	afx_msg void OnEnSetfocusEditDynamicrangenum7();
	afx_msg void OnEnSetfocusEditDynamicrangeresult3();
	afx_msg void OnEnSetfocusEditDynamicrangetxt4();
	afx_msg void OnEnSetfocusEditDynamicrangenum4();
	afx_msg void OnEnSetfocusEditDynamicrangenum8();
	afx_msg void OnEnSetfocusEditDynamicrangeresult4();
};
