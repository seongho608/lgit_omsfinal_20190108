#pragma once
#include "resource.h"

// COption_ResultWStand 대화 상자입니다.

class COption_ResultWStand : public CDialog
{
	DECLARE_DYNAMIC(COption_ResultWStand)

public:
	COption_ResultWStand(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COption_ResultWStand();

	CString m_strPort;
	CString m_strIp;


// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_WSTAND };
	CFont	font1,font2,font3,font4;
	void	Setup(CWnd* IN_pMomWnd);
	void	UpdateEditRect(int parm_edit_id);
	void	UpdateEdit();
	void	Voltage_text(LPCSTR lpcszString, ...);
	void	Voltage_Value(LPCSTR lpcszString, ...);
	void	Modelname_text(LPCSTR lpcszString, ...);

	void	MESIP_TEXT(LPCSTR lpcszString, ...);
	void	MESIP_VIEW(LPCSTR lpcszString, ...);
	void	MESPORT_TEXT(LPCSTR lpcszString, ...);
	void	MESPORT_VIEW(LPCSTR lpcszString, ...);
	void	MESBARCODE_TEXT(LPCSTR lpcszString, ...);
	void	MESBARCODE_VIEW(LPCSTR lpcszString, ...);

	void	LOTID_TEXT(LPCSTR lpcszString, ...);
	void	LOTID_VAL(LPCSTR lpcszString, ...);

	void	TESTNUM_TEXT(LPCSTR lpcszString, ...);
	void	TESTNUM_VAL(LPCSTR lpcszString, ...);
private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

	afx_msg void OnEnSetfocusVoltName();
	afx_msg void OnEnSetfocusVoltValue();
	afx_msg void OnEnSetfocusModelLname();
	afx_msg void OnEnSetfocusEditIpname();
	afx_msg void OnEnSetfocusEditIpval();
	afx_msg void OnEnSetfocusEditPortname4();
	afx_msg void OnEnSetfocusEditPortval();
	afx_msg void OnEnSetfocusEditLotidName();
	afx_msg void OnEnSetfocusEditLotidVal();
	afx_msg void OnEnSetfocusEditTestnumName();
	afx_msg void OnEnSetfocusEditTestnumVal();
	afx_msg void OnEnSetfocusEditBarcodeName();
	afx_msg void OnEnSetfocusEditBarcodeVal();
};
