#pragma once


// COverlayPosition_ResultView 대화 상자입니다.

class COverlayPosition_ResultView : public CDialog
{
	DECLARE_DYNAMIC(COverlayPosition_ResultView)

public:
	COverlayPosition_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COverlayPosition_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_OVERLAYPOSITION };
	
	void	Setup(CWnd* IN_pMomWnd);
	CFont	ft1;
	void	Initstat();

	COLORREF txcol_TVal[2],bkcol_TVal[2];
	
	void	OVERLAY_LEFT_TEXT(LPCSTR lpcszString, ...);
	void	OVERLAY_LEFT_NUM_TEXT(LPCSTR lpcszString, ...);
	void	RESULT_LEFT_TEXT(unsigned char col,LPCSTR lpcszString, ...);

	void	OVERLAY_RIGHT_TEXT(LPCSTR lpcszString, ...);
	void	OVERLAY_RIGHT_NUM_TEXT(LPCSTR lpcszString, ...);
	void	RESULT_RIGHT_TEXT(unsigned char col,LPCSTR lpcszString, ...);
	
private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
//	afx_msg void OnEnChangeEditOverlayresult();
};
