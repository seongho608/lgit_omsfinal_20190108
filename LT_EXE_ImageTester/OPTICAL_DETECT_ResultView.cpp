// OPTICAL_DETECT_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "OPTICAL_DETECT_ResultView.h"


// COPTICAL_DETECT_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(COPTICAL_DETECT_ResultView, CDialog)

COPTICAL_DETECT_ResultView::COPTICAL_DETECT_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(COPTICAL_DETECT_ResultView::IDD, pParent)
{

}

COPTICAL_DETECT_ResultView::~COPTICAL_DETECT_ResultView()
{
}

void COPTICAL_DETECT_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FILE_TRANS, m_CtrFileProgress);
}


BEGIN_MESSAGE_MAP(COPTICAL_DETECT_ResultView, CDialog)
	ON_EN_SETFOCUS(IDC_FILE_NAME, &COPTICAL_DETECT_ResultView::OnEnSetfocusFileName)
	ON_EN_SETFOCUS(IDC_WRMODE, &COPTICAL_DETECT_ResultView::OnEnSetfocusWrmode)
	ON_EN_SETFOCUS(IDC_EDIT_X_CH1, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditXCh1)
	ON_EN_SETFOCUS(IDC_EDIT_Y_CH1, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditYCh1)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETX_CH1, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetxCh1)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETY_CH1, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetyCh1)
	ON_EN_SETFOCUS(IDC_EDIT_X_CH2, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditXCh2)
	ON_EN_SETFOCUS(IDC_EDIT_Y_CH2, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditYCh2)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETX_CH2, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetxCh2)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETY_CH2, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetyCh2)
	ON_EN_SETFOCUS(IDC_EDIT_X_CH3, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditXCh3)
	ON_EN_SETFOCUS(IDC_EDIT_Y_CH3, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditYCh3)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETX_CH3, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetxCh3)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETY_CH3, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetyCh3)
	ON_EN_SETFOCUS(IDC_EDIT_X_CH4, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditXCh4)
	ON_EN_SETFOCUS(IDC_EDIT_Y_CH4, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditYCh4)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETX_CH4, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetxCh4)
	ON_EN_SETFOCUS(IDC_EDIT_OFFSETY_CH4, &COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetyCh4)
	ON_EN_SETFOCUS(IDC_RUN_MODE, &COPTICAL_DETECT_ResultView::OnEnSetfocusRunMode)
END_MESSAGE_MAP()


// COPTICAL_DETECT_ResultView 메시지 처리기입니다.
void COPTICAL_DETECT_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void COPTICAL_DETECT_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}


BOOL COPTICAL_DETECT_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	Font_Size_Change(IDC_RUN_MODE,&font1,35,20);
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	pRunEdit = (CEdit *)GetDlgItem(IDC_RUN_MODE);
	pFileEdit = (CEdit *)GetDlgItem(IDC_FILE_NAME);
	pWrModeEdit = (CEdit *)GetDlgItem(IDC_WRMODE);

	m_CtrFileProgress.SetRange(0,1000);
	m_CtrFileProgress.SetPos(0);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL COPTICAL_DETECT_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	/*	if(pMsg->wParam == VK_ADD){
			((CImageTesterDlg  *)m_pMomWnd)->ExBtnStart();
			return TRUE;
		}
		if (pMsg->wParam == VK_SUBTRACT){
			((CImageTesterDlg  *)m_pMomWnd)->ExBtnStop();
			return TRUE;
		}*/
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void COPTICAL_DETECT_ResultView::AxisX_Txt(int ch,LPCSTR lpcszString, ...)
{	
	switch(ch){
		case 0:((CEdit *)GetDlgItem(IDC_EDIT_X_CH1))->SetWindowText(lpcszString);
			break;
		case 1:((CEdit *)GetDlgItem(IDC_EDIT_X_CH2))->SetWindowText(lpcszString);
			break;
		case 2:((CEdit *)GetDlgItem(IDC_EDIT_X_CH3))->SetWindowText(lpcszString);
			break;
		case 3:((CEdit *)GetDlgItem(IDC_EDIT_X_CH4))->SetWindowText(lpcszString);
			break;
	}
	
}
void COPTICAL_DETECT_ResultView::AxisY_Txt(int ch,LPCSTR lpcszString, ...)
{	
	switch(ch){
		case 0:((CEdit *)GetDlgItem(IDC_EDIT_Y_CH1))->SetWindowText(lpcszString);
			break;
		case 1:((CEdit *)GetDlgItem(IDC_EDIT_Y_CH2))->SetWindowText(lpcszString);
			break;
		case 2:((CEdit *)GetDlgItem(IDC_EDIT_Y_CH3))->SetWindowText(lpcszString);
			break;
		case 3:((CEdit *)GetDlgItem(IDC_EDIT_Y_CH4))->SetWindowText(lpcszString);
			break;
	}
}
void COPTICAL_DETECT_ResultView::OffsetX_Txt(int ch,LPCSTR lpcszString, ...)
{	
	switch(ch){
		case 0:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETX_CH1))->SetWindowText(lpcszString);
			break;
		case 1:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETX_CH2))->SetWindowText(lpcszString);
			break;
		case 2:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETX_CH3))->SetWindowText(lpcszString);
			break;
		case 3:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETX_CH4))->SetWindowText(lpcszString);
			break;
	}
}
void COPTICAL_DETECT_ResultView::OffsetY_Txt(int ch,LPCSTR lpcszString, ...)
{	
	switch(ch){
		case 0:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETY_CH1))->SetWindowText(lpcszString);
			break;
		case 1:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETY_CH2))->SetWindowText(lpcszString);
			break;
		case 2:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETY_CH3))->SetWindowText(lpcszString);
			break;
		case 3:((CEdit *)GetDlgItem(IDC_EDIT_OFFSETY_CH4))->SetWindowText(lpcszString);
			break;
	}
}

void COPTICAL_DETECT_ResultView::InitStat(){

	for(int t=0; t<4; t++){
		AxisX_Txt(t,"");
		AxisY_Txt(t,"");
		OffsetX_Txt(t,"");
		OffsetY_Txt(t,"");
	}
}
void COPTICAL_DETECT_ResultView::OnEnSetfocusFileName()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusWrmode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditXCh1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditYCh1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetxCh1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetyCh1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditXCh2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditYCh2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetxCh2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetyCh2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditXCh3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditYCh3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetxCh3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetyCh3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditXCh4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditYCh4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetxCh4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusEditOffsetyCh4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}

void COPTICAL_DETECT_ResultView::OnEnSetfocusRunMode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
}
