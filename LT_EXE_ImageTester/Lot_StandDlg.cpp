// Lot_StandDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Lot_StandDlg.h"


// CLot_StandDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLot_StandDlg, CDialog)

CLot_StandDlg::CLot_StandDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLot_StandDlg::IDD, pParent)
{

}

CLot_StandDlg::~CLot_StandDlg()
{
}

void CLot_StandDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CLot_StandDlg, CDialog)
	ON_WM_CTLCOLOR()
	ON_WM_DRAWITEM()
	ON_BN_CLICKED(IDC_BUTTON_LOTSTART, &CLot_StandDlg::OnBnClickedButtonLotstart)
	ON_BN_CLICKED(IDC_BUTTON_LOTEXIT, &CLot_StandDlg::OnBnClickedButtonLotexit)
	ON_EN_SETFOCUS(IDC_MODEL_LNAME, &CLot_StandDlg::OnEnSetfocusModelLname)
	ON_EN_SETFOCUS(IDC_VOLT_NAME, &CLot_StandDlg::OnEnSetfocusVoltName)
	ON_EN_SETFOCUS(IDC_VOLT_VALUE, &CLot_StandDlg::OnEnSetfocusVoltValue)
	ON_EN_SETFOCUS(IDC_EDIT_TESTNUM_NAME, &CLot_StandDlg::OnEnSetfocusEditTestnumName)
	ON_EN_SETFOCUS(IDC_EDIT_TESTNUM_VAL, &CLot_StandDlg::OnEnSetfocusEditTestnumVal)
	ON_EN_SETFOCUS(IDC_EDIT_LOTID_VAL, &CLot_StandDlg::OnEnSetfocusEditLotidVal)
	ON_EN_SETFOCUS(IDC_EDIT_LOTID_NAME, &CLot_StandDlg::OnEnSetfocusEditLotidName)
END_MESSAGE_MAP()


// CLot_StandDlg 메시지 처리기입니다.
void CLot_StandDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CLot_StandDlg::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
BOOL CLot_StandDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	Font_Size_Change(IDC_VOLT_NAME,&font1,500,28);
	GetDlgItem(IDC_VOLT_VALUE)->SetFont(&font1);
	Font_Size_Change(IDC_MODEL_LNAME,&font2,620,35);

	Font_Size_Change(IDC_EDIT_LOTID_NAME,&font3,500,28);
	GetDlgItem(IDC_EDIT_LOTID_VAL)->SetFont(&font3);
	GetDlgItem(IDC_EDIT_TESTNUM_NAME)->SetFont(&font3);
	GetDlgItem(IDC_EDIT_TESTNUM_VAL)->SetFont(&font3);

	Font_Size_Change(IDC_BUTTON_LOTSTART,&font4,600,28);
	GetDlgItem(IDC_BUTTON_LOTEXIT)->SetFont(&font4);



	Voltage_text("VOLT");
	LOTID_TEXT("LOTID");
	TESTNUM_TEXT("COUNT");

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void CLot_StandDlg::Voltage_text(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_VOLT_NAME))->SetWindowText(lpcszString);
}

void CLot_StandDlg::Voltage_Value(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_VOLT_VALUE))->SetWindowText(lpcszString);
}

void CLot_StandDlg::Modelname_text(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_MODEL_LNAME))->SetWindowText(lpcszString);
}
void CLot_StandDlg::LOTID_TEXT(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_LOTID_NAME))->SetWindowText(lpcszString);
}

void CLot_StandDlg::LOTID_VAL(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_LOTID_VAL))->SetWindowText(lpcszString);
}

void CLot_StandDlg::TESTNUM_TEXT(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_TESTNUM_NAME))->SetWindowText(lpcszString);
}

void CLot_StandDlg::TESTNUM_VAL(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_TESTNUM_VAL))->SetWindowText(lpcszString);
}

HBRUSH CLot_StandDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
		// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() ==IDC_VOLT_NAME){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}
	if(pWnd->GetDlgCtrlID() ==IDC_VOLT_VALUE){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() ==IDC_MODEL_LNAME){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_LOTID_NAME)
	{
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB( 86,86,86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_LOTID_VAL){
		pDC->SetTextColor(RGB( 86,86,86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_TESTNUM_NAME)
	{
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB( 86,86,86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_TESTNUM_VAL){
		pDC->SetTextColor(RGB( 86,86,86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}


	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CLot_StandDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDCtl==IDC_BUTTON_LOTSTART ||nIDCtl==IDC_BUTTON_LOTEXIT)
	{
		// DRAWITEMSTRUCT 구조체의 정보중에서 HDC 형식의 핸들값을 CDC 객체로 변환한다.
		CDC *p_dc = CDC::FromHandle(lpDrawItemStruct->hDC);
		int t=0;
		// RECT 형식의 구조체값을 이용하여 CRect 객체를 생성한다.
		CRect r(lpDrawItemStruct->rcItem); 
		CString str;

		if(nIDCtl==IDC_BUTTON_LOTSTART)
		{
			str.Empty();
			str.Format(_T("LOT START"));
			t=1;	
		}
	
		if(nIDCtl==IDC_BUTTON_LOTEXIT)
		{
			str.Empty();
			str.Format(_T("LOT EXIT"));
			t=4;
		}
	

        // 글자를 출력할 때 적용할 배경을 투명으로 설정한다.
        int old_mode = p_dc->SetBkMode(TRANSPARENT);
 
        // 버튼을 사용할 수 없는 상태일 경우
        if(lpDrawItemStruct->itemState & ODS_DISABLED)
		{
            // 버튼 영역을 회색으로 채운다.
            p_dc->FillSolidRect(r, RGB(200, 200, 200));
			
			p_dc->Draw3dRect(r, RGB(150, 150, 150),  RGB(150, 150, 150));
            // 진한 회색으로 글자색을 설정한다.
            p_dc->SetTextColor(RGB(128, 128, 128));
            // 버튼의 캡션을 출력한다.
            p_dc->DrawText(str, r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

        }else 
		{	
			//버튼을 사용할수 있을 경우 
            // 버튼 영역을 배경색으로 채운다.
            p_dc->FillSolidRect(r, RGB(240, 240, 240));
                
            // 버튼이 포커스를 얻은 경우
            if(lpDrawItemStruct->itemState & ODS_FOCUS)
			{
				p_dc->FillSolidRect(r,RGB(50, 82, 152));
                // 분홍색으로 테두리를 그리고, 글자색도 분홍색으로 설정한다.
                p_dc->Draw3dRect(r, RGB(50, 82, 152),  RGB(50, 82, 152));
				p_dc->SetTextColor(RGB(255, 255, 255));
            } else 
			{
                // 회색으로 테두리를 그리고, 글자색을 흰색으로 설정한다.
		
					p_dc->Draw3dRect(r, RGB(240, 240, 240),RGB(240, 240, 240));
					p_dc->SetTextColor(RGB(0, 0, 0));
		
			}
            // 영역을 한픽셀씩 내부 방향으로 줄인다.
            r.left++;
            r.top++;
            r.right--;
            r.bottom--;
 
            // 버튼을 선택한 경우
            if(lpDrawItemStruct->itemState & ODS_SELECTED)
			{
                // 테두리의 색상을 지정한다.
                p_dc->Draw3dRect(r,  RGB(50, 82, 152),  RGB(50, 82, 152));
				
               // 버튼의 캡션을 오른쪽 아래 방향으로 한픽셀씩 이동시켜 출력한다.
                p_dc->DrawText(str, r + CPoint(1, 1), DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			}else 
			{
                // 테두리의 색상을 지정한다.
            	p_dc->Draw3dRect(r, RGB(110, 130, 142), RGB(110, 130, 142));
				// 버튼의 캡션을 출력한다.
                p_dc->DrawText(str, r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
            }
        }
	
        // 배경을 이전 모드로 설정한다.
		p_dc->SetBkMode(old_mode);
	}
	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CLot_StandDlg::OnBnClickedButtonLotstart()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg*)m_pMomWnd)->ClickedBtnLotStart();
}

void CLot_StandDlg::OnBnClickedButtonLotexit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg*)m_pMomWnd)->ClickedBtnLotExit();
}

void CLot_StandDlg::LotBtn_Enable(bool Enable){
	(CButton *)GetDlgItem(IDC_BUTTON_LOTSTART)->EnableWindow(Enable);
	(CButton *)GetDlgItem(IDC_BUTTON_LOTEXIT)->EnableWindow(1-Enable);
}

void CLot_StandDlg::LotexitBtn_Enable(bool Enable){
	(CButton *)GetDlgItem(IDC_BUTTON_LOTEXIT)->EnableWindow(Enable);
}
void CLot_StandDlg::OnEnSetfocusModelLname()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg*)m_pMomWnd)->Focus_move_start();
}

void CLot_StandDlg::OnEnSetfocusVoltName()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg*)m_pMomWnd)->Focus_move_start();
}

void CLot_StandDlg::OnEnSetfocusVoltValue()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg*)m_pMomWnd)->Focus_move_start();
}

void CLot_StandDlg::OnEnSetfocusEditTestnumName()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg*)m_pMomWnd)->Focus_move_start();
}

void CLot_StandDlg::OnEnSetfocusEditTestnumVal()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg*)m_pMomWnd)->Focus_move_start();
}

void CLot_StandDlg::OnEnSetfocusEditLotidVal()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg*)m_pMomWnd)->Focus_move_start();
}

void CLot_StandDlg::OnEnSetfocusEditLotidName()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg*)m_pMomWnd)->Focus_move_start();
}
