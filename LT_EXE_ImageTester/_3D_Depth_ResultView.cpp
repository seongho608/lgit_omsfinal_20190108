// _3D_Depth_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "_3D_Depth_ResultView.h"


// C_3D_Depth_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(C_3D_Depth_ResultView, CDialog)

C_3D_Depth_ResultView::C_3D_Depth_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(C_3D_Depth_ResultView::IDD, pParent)
{

}

C_3D_Depth_ResultView::~C_3D_Depth_ResultView()
{
}

void C_3D_Depth_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(C_3D_Depth_ResultView, CDialog)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// C_3D_Depth_ResultView 메시지 처리기입니다.
void C_3D_Depth_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void C_3D_Depth_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
BOOL C_3D_Depth_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();
	Font_Size_Change(IDC_STATIC_LEFT_C,&ft,100,20);
	GetDlgItem(IDC_VALUE_LEFT_C)->SetFont(&ft);
	GetDlgItem(IDC_RESULT_LEFT_C)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_RIGHT_C)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_RIGHT_C)->SetFont(&ft);
	GetDlgItem(IDC_RESULT_RIGHT_C)->SetFont(&ft);
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	initstat();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void C_3D_Depth_ResultView::initstat()
{
	AVE_TEXT("평균");
	STDEV_TEXT("표준 편차");

	AVE_VALUE("");
	STDEV_VALUE("");

	AVE_RESULT(0,"STAND BY");
	STDEV_RESULT(0,"STAND BY");
}

HBRUSH C_3D_Depth_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_LEFT_C){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_LEFT_C){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() ==IDC_RESULT_LEFT_C){
		pDC->SetTextColor(txcol_LTVal);
		pDC->SetBkColor(bkcol_LTVal);
	}

	if(pWnd->GetDlgCtrlID() == IDC_STATIC_RIGHT_C){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_RIGHT_C){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() ==IDC_RESULT_RIGHT_C){
		pDC->SetTextColor(txcol_RTVal);
		pDC->SetBkColor(bkcol_RTVal);
	}


	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void C_3D_Depth_ResultView::AVE_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_LEFT_C))->SetWindowText(lpcszString);
}
void C_3D_Depth_ResultView::STDEV_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_RIGHT_C))->SetWindowText(lpcszString);
}

void C_3D_Depth_ResultView::AVE_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_LEFT_C))->SetWindowText(lpcszString);
}
void C_3D_Depth_ResultView::STDEV_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_RIGHT_C))->SetWindowText(lpcszString);
}

void C_3D_Depth_ResultView::AVE_RESULT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_LTVal = RGB(255, 255, 255);
		bkcol_LTVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_LTVal = RGB(255, 255, 255);
		bkcol_LTVal = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_LTVal = RGB(255, 255, 255);
		bkcol_LTVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_LTVal = RGB(0, 0, 0);
		bkcol_LTVal = RGB(255, 255, 0);
	}

	((CEdit *)GetDlgItem(IDC_RESULT_LEFT_C))->SetWindowText(lpcszString);
}
void C_3D_Depth_ResultView::STDEV_RESULT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_RTVal = RGB(255, 255, 255);
		bkcol_RTVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_RTVal = RGB(255, 255, 255);
		bkcol_RTVal = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_RTVal = RGB(255, 255, 255);
		bkcol_RTVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_RTVal = RGB(0, 0, 0);
		bkcol_RTVal = RGB(255, 255, 0);
	}

	((CEdit *)GetDlgItem(IDC_RESULT_RIGHT_C))->SetWindowText(lpcszString);
}
BOOL C_3D_Depth_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}
