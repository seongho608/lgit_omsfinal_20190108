#pragma once
#include "afxcmn.h"


// CControlDlg 대화 상자입니다.

class CControlDlg : public CDialog
{
	DECLARE_DYNAMIC(CControlDlg)

public:
	CControlDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CControlDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_CONTROL };

	bool	Change_DATA_CHECK(bool FLAG);
	void	EditWheelIntSet(int nID,int Val);
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);//나중에 전역변수로 간략화 할것임
	CFont ft,ft1;
	void	PORT_STATE(bool MODE);
	COLORREF tx,bk;
	int Power_Ctl_Data[5]; 
	int Current_Ctl_Data[5];
	void Save_Parameter();
	void Load_Parameter();
	void SETLIST();
	void InsertList();
	int iSavedItem, iSavedSubitem;
	CRect rect;
	void	Change_DATA();

	void List_COLOR_Change();
	bool ChangeCheck;
	int ChangeItem[5];
	int changecount;

	void	Enable_Button(bool MODE);


	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT);//,tINFO INFO);
private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	CString		str_model;
	CString		str_ModelPath;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CListCtrl m_DataList;
	afx_msg void OnBnClickedButtonTotalOn();
	afx_msg void OnBnClickedButtonLeftOn();
	afx_msg void OnBnClickedButtonCenterOn();
	afx_msg void OnBnClickedButtonRightOn();
	afx_msg void OnBnClickedButtonDustOn();
	afx_msg void OnBnClickedButtonIrOn();
	afx_msg void OnBnClickedButtonApply();
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnBnClickedButtonLoad();
	afx_msg void OnNMDblclkListData(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusEditData();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	CSpinButtonCtrl m_SpinCtrl[5];
	afx_msg void OnDeltaposSpinLeft(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinCenter(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinRight(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinDust(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinIr(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnSetfocusEditStatus();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnNMCustomdrawListData(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonTotalOff();
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnBnClickedButtonLeftOff();
	afx_msg void OnBnClickedButtonCenterOff();
	afx_msg void OnBnClickedButtonRightOff();
	afx_msg void OnBnClickedButtonDustOff();
	afx_msg void OnBnClickedButtonIrOff();
protected:
	virtual void OnCancel();
};
