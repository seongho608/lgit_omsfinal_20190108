// VER_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "VER_ResultView.h"


// CVER_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CVER_ResultView, CDialog)

CVER_ResultView::CVER_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CVER_ResultView::IDD, pParent)
{

}

CVER_ResultView::~CVER_ResultView()
{
}

void CVER_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CVER_ResultView, CDialog)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CVER_ResultView 메시지 처리기입니다.
void CVER_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

BOOL CVER_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_EDIT_VER_NAME1,&ft,100,20);
	GetDlgItem(IDC_EDIT_VER_NAME2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_VER1)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_VER2)->SetFont(&ft);
	Font_Size_Change(IDC_EDIT_VER_STATE,&ft2,100,38);

	((CEdit *)GetDlgItem(IDC_EDIT_VER_NAME1))->SetWindowText("App Version");
	((CEdit *)GetDlgItem(IDC_EDIT_VER_NAME2))->SetWindowText("BL Version");

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

HBRUSH CVER_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VER_NAME1){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VER_NAME2){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_VER1){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[0]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_VER2){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[1]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VER_STATE){
		pDC->SetBkColor(bk_st_col);
		pDC->SetTextColor(tx_st_col);
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CVER_ResultView::VER_RESULT(int num,int col,LPCSTR lpcszString, ...)
{	
	switch(num){
		case 0:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_VER1))->SetWindowText(lpcszString);
			break;
		case 1:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_VER2))->SetWindowText(lpcszString);
			break;
		default:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_VER1))->SetWindowText(lpcszString);
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_VER2))->SetWindowText(lpcszString);
	}

	if(num <=3){
		if(col == 0){
			Valcol[num] = RGB(86,86,86);
		}else if(col  ==1){
			Valcol[num] = RGB(18,69,171);
		}else{
			Valcol[num] = RGB(201,0,0);
		}
	}else{
		for(int lop = 0;lop<4;lop++){
			if(col == 0){
				Valcol[lop] = RGB(86,86,86);
			}else if(col  ==1){
				Valcol[lop] = RGB(18,69,171);
			}else{
				Valcol[lop] = RGB(201,0,0);
			}
		}
	}
}

void CVER_ResultView::VER_STATE(int col,LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_VER_STATE))->SetWindowText(lpcszString);
	if(col == 0){//스탠드 바이
		tx_st_col = RGB(255, 255, 255);
		bk_st_col = RGB(47, 157, 39);
	}else if(col == 1){//성공
		tx_st_col = RGB(255, 255, 255);
		bk_st_col = RGB(18, 69, 171);
	}else if(col == 2){//실패
		tx_st_col = RGB(255, 255, 255);
		bk_st_col = RGB(201, 0, 0);
	}else if(col == 3){
		tx_st_col = RGB(0, 0, 0);
		bk_st_col = RGB(255, 255, 0);
	}
}

void CVER_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}

void CVER_ResultView::InitStat()
{
	VER_RESULT(2,0,"");//모두 NONE표시
	VER_STATE(0,"Stand By");
}