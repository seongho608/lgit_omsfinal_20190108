// ModOptDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "ModOptDlg.h"


// CModOptDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CModOptDlg, CDialog)

CModOptDlg::CModOptDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModOptDlg::IDD, pParent)
{

}

CModOptDlg::~CModOptDlg()
{
}

void CModOptDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_SETTING, m_Tab_Setting);
	DDX_Control(pDX, IDC_TAB_TESTMANAGER, m_Tab_Manager);
}


BEGIN_MESSAGE_MAP(CModOptDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_SUB_MODEL, &CModOptDlg::OnCbnSelchangeSubModel)
	ON_WM_SHOWWINDOW()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_SETTING, &CModOptDlg::OnTcnSelchangeTabSetting)
	ON_CBN_SELCHANGE(IDC_SUB_POGO, &CModOptDlg::OnCbnSelchangeSubPogo)
END_MESSAGE_MAP()


// CModOptDlg 메시지 처리기입니다.
void CModOptDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

BOOL CModOptDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_Tab_Setting.DeleteAllItems();
	m_Tab_Setting.InsertItem(0, _T("시리얼옵션"));
	m_Tab_Setting.InsertItem(1, _T("MES 옵션"));
	m_Tab_Setting.InsertItem(2, _T("POGO 옵션"));
	m_Tab_Setting.SetCurSel(0);
	m_Tab_Setting.ShowWindow(TRUE);

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	pModelCombo = (CComboBox *)GetDlgItem(IDC_SUB_MODEL);//모델
	pPogoCombo = (CComboBox *)GetDlgItem(IDC_SUB_POGO);//모델

	InitEVMS();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CModOptDlg::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		pModelCombo->EnableWindow(0);
		pPogoCombo->EnableWindow(0);
	}
}

void CModOptDlg::OnCbnSelchangeSubModel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->m_combo_sel = pModelCombo->GetCurSel();

	((CImageTesterDlg *)m_pMomWnd)->m_pMainCombo->SetCurSel(((CImageTesterDlg *)m_pMomWnd)->m_combo_sel);

	if(((CImageTesterDlg *)m_pMomWnd)->m_pStandOptWnd != NULL)
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptWnd->pModelCombo->SetCurSel(((CImageTesterDlg *)m_pMomWnd)->m_combo_sel);
	
	if(((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd != NULL)
		((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->pModelCombo->SetCurSel(((CImageTesterDlg *)m_pMomWnd)->m_combo_sel);
	

	((CImageTesterDlg *)m_pMomWnd)->SetModelSet();
	((CImageTesterDlg *)m_pMomWnd)->ModelSet(); //luri 파일을 읽어 각 모델이 무엇인지 파악한다.
	((CImageTesterDlg *)m_pMomWnd)->ModelGen(); //파악된 모델을 외부에 표시한다.
	ShowWindow(SW_HIDE);
	ShowWindow(SW_SHOW);
}

void CModOptDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if(bShow)
	{
		m_Tab_Setting.SetCurSel(0);
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptWnd->ShowWindow(SW_SHOW);
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptMesWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pOptPogoDlgWnd->ShowWindow(SW_HIDE);
	}
}

void CModOptDlg::OnShowSel(int Num)
{
	if(Num == 0){
		m_Tab_Setting.SetCurSel(0);
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptWnd->ShowWindow(SW_SHOW);
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptMesWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pOptPogoDlgWnd->ShowWindow(SW_HIDE);
	}
	else if (Num == 1){
		m_Tab_Setting.SetCurSel(1);
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptMesWnd->ShowWindow(SW_SHOW);
		((CImageTesterDlg *)m_pMomWnd)->m_pOptPogoDlgWnd->ShowWindow(SW_HIDE);
	}else{
		m_Tab_Setting.SetCurSel(2);
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptMesWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pOptPogoDlgWnd->ShowWindow(SW_SHOW);
	}
}

void CModOptDlg::OnTcnSelchangeTabSetting(NMHDR *pNMHDR, LRESULT *pResult)
{
	int itab = m_Tab_Setting.GetCurSel();

	if(itab == 0)
	{
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptWnd->ShowWindow(SW_SHOW);
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptMesWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pOptPogoDlgWnd->ShowWindow(SW_HIDE);
	}else if(itab == 1)
	{
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptMesWnd->ShowWindow(SW_SHOW);
		((CImageTesterDlg *)m_pMomWnd)->m_pOptPogoDlgWnd->ShowWindow(SW_HIDE);
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pStandOptMesWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pOptPogoDlgWnd->ShowWindow(SW_SHOW);
	}
	*pResult = 0;
}

void CModOptDlg::OnCbnSelchangeSubPogo()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SetPogoSet();
}

void CModOptDlg::SetPogoSet()
{
	CString str = "";
	int m_pogo_sel = pPogoCombo->GetCurSel(); 
	
	if(m_pogo_sel < 0){
		return;
	}
	pPogoCombo->GetLBText(m_pogo_sel,str);

	if(str == (""))     
	{         
		return;     
	}
	((CImageTesterDlg *)m_pMomWnd)->m_pOptPogoDlgWnd->pModelCombo->SetCurSel(m_pogo_sel);
	CString pogoName = str;
	if(pogoName != ((CImageTesterDlg *)m_pMomWnd)->Pogo_Name){
		WritePrivateProfileString("POGO","GROUP",pogoName,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		((CImageTesterDlg *)m_pMomWnd)->Pogo_Name = pogoName;
	}
	((CImageTesterDlg *)m_pMomWnd)->m_pOptPogoDlgWnd->POGONAME_VIEW(str);
	((CImageTesterDlg *)m_pMomWnd)->POGOGROUPNAME(str);
	((CImageTesterDlg *)m_pMomWnd)->PogoSelect_path = ((CImageTesterDlg *)m_pMomWnd)->Pogo_path +"\\"+pogoName+ ".ini";
	((CImageTesterDlg *)m_pMomWnd)->Pogo_Load_Prm();
	OnShowSel(2);
}

