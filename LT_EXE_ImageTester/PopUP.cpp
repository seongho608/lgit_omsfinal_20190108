// PopUP.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "PopUP.h"


// CPopUP 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPopUP, CDialog)

CPopUP::CPopUP(CWnd* pParent /*=NULL*/)
	: CDialog(CPopUP::IDD, pParent)
{
	ILMode = 0;
	b_SuccessTrue =	FALSE;
}

CPopUP::~CPopUP()
{
}

void CPopUP::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CPopUP, CDialog)
	ON_BN_CLICKED(IDOK, &CPopUP::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPopUP::OnBnClickedCancel)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CPopUP 메시지 처리기입니다.
void CPopUP::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CPopUP::TEXT_CHANGE(LPCSTR lpcszString, ...){
	
	((CEdit *)GetDlgItem(IDC_EDIT_WARNIGTEXT))->SetWindowText(lpcszString);
}

BOOL CPopUP::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_EDIT_WARNIGTEXT,&Font,300,20);
	TEXT_CHANGE(Warning_TEXT);
	//if(ILMode != 0){
	//	SetTimer(121, 400, NULL); //영상입력 타이머 파라메터
	//}
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CPopUP::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			OnBnClickedOk();
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CPopUP::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight;
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}

void CPopUP::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	
//	KillTimer(121);
	OnOK();
}

void CPopUP::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	return;
	OnCancel();
}

void CPopUP::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//if(nIDEvent == 121){
	//	if(b_SuccessTrue == FALSE){
	//		if((ILMode == 1)||(ILMode == 3)){
	//			if(((CFINAL_TEST_MDlg *)m_pMomWnd)->Sensing_Wait == FALSE){
	//				if(((CFINAL_TEST_MDlg *)m_pMomWnd)->DoorCHKSTATE == 0){
	//					TEXT_CHANGE("불량함에카메라를 넣어주세요.\r\n\r\nOK를 두번누르시면 강제로 넘어갑니다.\r\n");
	//					ILMode = 2;
	//				}else{
	//					((CFINAL_TEST_MDlg *)m_pMomWnd)->Sensing_Wait =TRUE;
	//				}
	//			}
	//		}else if(((CFINAL_TEST_MDlg *)m_pMomWnd)->m_ErrBoxEn > 0){//불량함에 불량이 들어오면 
	//			b_SuccessTrue = TRUE;
	//			OnBnClickedOk();//불량함에 들어오면 자동으로 해제된다. 
	//		}
	//	}
	//}
	CDialog::OnTimer(nIDEvent);
}
