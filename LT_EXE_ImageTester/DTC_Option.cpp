// DTC_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "DTC_Option.h"

// CDTC_Option 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDTC_Option, CDialog)

CDTC_Option::CDTC_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CDTC_Option::IDD, pParent)
{
	InsertIndex = 0;
	ListItemNum = 0;
	b_AutoErase = FALSE;
	StartCnt = 0;
	m_Etc_State = 0;
	Lot_StartCnt=0;
	b_StopFail = FALSE;
}

CDTC_Option::~CDTC_Option()
{
}

void CDTC_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_DTCLIST, m_DTCList);
	DDX_Control(pDX, IDC_LIST_DTCLIST_LOT, m_Lot_DTCList);
}


BEGIN_MESSAGE_MAP(CDTC_Option, CDialog)
	ON_BN_CLICKED(IDC_BTN_DTCREAD, &CDTC_Option::OnBnClickedBtnDtcread)
	ON_BN_CLICKED(IDC_BTN_DTCERASE, &CDTC_Option::OnBnClickedBtnDtcerase)
	ON_WM_CTLCOLOR()
	ON_CBN_SELCHANGE(IDC_COMBO_DTCMODE, &CDTC_Option::OnCbnSelchangeComboDtcmode)
	ON_BN_CLICKED(IDC_BTN_DTCSAVE, &CDTC_Option::OnBnClickedBtnDtcsave)
END_MESSAGE_MAP()


// CDTC_Option 메시지 처리기입니다.
BOOL CDTC_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_EDIT_DTC_DATA,&ft,100,20);
	GetDlgItem(IDC_EDIT_DTC_NAME1)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_DTC_NAME2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_DTC_NAME3)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_DTC_NAME4)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_DTC1)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_DTC2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_DTC3)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_DTC4)->SetFont(&ft);

	Font_Size_Change(IDC_EDIT_DTC_MODE,&ft2,200,40);


	((CEdit *)GetDlgItem(IDC_EDIT_DTC_NAME1))->SetWindowText("DTC 1");
	((CEdit *)GetDlgItem(IDC_EDIT_DTC_NAME2))->SetWindowText("DTC 2");
	((CEdit *)GetDlgItem(IDC_EDIT_DTC_NAME3))->SetWindowText("DTC 3");
	((CEdit *)GetDlgItem(IDC_EDIT_DTC_NAME4))->SetWindowText("DTC 4");
	DTC_RESULT(4,0,"");//모두 NONE표시
	DTC_STATE(0,"Stand By");
	DTC_RESULT_STATE(0,"STAND BY");

	Set_List(&m_DTCList);
	LOT_Set_List(&m_Lot_DTCList);

	pComboMode = (CComboBox *)GetDlgItem(IDC_COMBO_DTCMODE);
	Load_parameter();

	InitEVMS();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDTC_Option::LOT_InsertDataList()
{
	CString strCnt="";

	int Index = m_Lot_DTCList.InsertItem(Lot_StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index+1);
	m_Lot_DTCList.SetItemText(Index,0,strCnt);

	int CopyIndex = m_DTCList.GetItemCount()-1;

	int count =0;
	for(int t=0; t< Lot_InsertNum;t++){
		if((t != 2)&&(t!=0)){
			m_Lot_DTCList.SetItemText(Index,t, m_DTCList.GetItemText(CopyIndex,count));
			count++;

		}else if(t==0){
			count++;
		}else{
			m_Lot_DTCList.SetItemText(Index,t,((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;
}

void CDTC_Option::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}

void CDTC_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CDTC_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
	strTitle.Empty();
	strTitle="DTCOPT_";
}

void CDTC_Option::Pic(CDC *cdc)
{
	DTCPic(cdc);
}

bool CDTC_Option::DTCPic(CDC *cdc){
	
	CPen	my_Pan,*old_pan;
	CString TEXTNAME[4] = {"  DTC_1  : ","  DTC_2  : ","  DTC_3  : ","  DTC_4  : "};
	CString TEXTDATA = "";
	CString str = "";
	CFont m_font;

	for(int lop = 0;lop<4;lop++){
		if(m_Etc_State == 0){
			return TRUE;
		}
	}
	
	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	m_font.CreatePointFont(200,"Arial");
	cdc->SelectObject(&m_font);
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);
	if(m_Etc_State == 1){
		my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(BLUE_COLOR);
	}else{
		my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(RED_COLOR);
	}
	for(int lop = 0;lop<4;lop++){
		str.Format("0x%02X",((CImageTesterDlg *)m_pMomWnd)->DTCRDDATA[lop]);
		TEXTDATA = TEXTNAME[lop] + str;
		cdc->TextOut(150,60+(lop*30),TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
	}
	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();
	m_font.DeleteObject();
	return TRUE;
}


void CDTC_Option::InitPrm()
{
	Load_parameter();
	UpdateData(FALSE);
}

void CDTC_Option::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

void CDTC_Option::Save_parameter(){
	CString str = "";
	
	WritePrivateProfileString(str_model,NULL,"",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%d",m_INFO.ID);
	WritePrivateProfileString(str_model,"ID",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%s",m_INFO.MODE);
	WritePrivateProfileString(str_model,"MODE",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%s",m_INFO.NAME);
	WritePrivateProfileString(str_model,"NAME",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	
	str.Empty();
	str.Format("%d",b_DTCMODE);
	WritePrivateProfileString(str_model,strTitle+"TESTMODE",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	
	pComboMode->SetCurSel(b_DTCMODE);
	
	if(b_DTCMODE == 1){
		ModeName = "DTC Clear";
	}else{
		ModeName = "DTC Detect";
	}
	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->NameChange(ModeName);
	DTC_MODE_STATE(ModeName);

	UpdateData(FALSE);
}



void CDTC_Option::Load_parameter(){
	CFileFind filefind;
	CString str="";
	
	b_DTCMODE = GetPrivateProfileInt(str_model,strTitle+"TESTMODE",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(b_DTCMODE  == -1){//dtc clear 가 기본
		b_DTCMODE = 1;
		str.Empty();
		str.Format("%d",b_DTCMODE);
		WritePrivateProfileString(str_model,strTitle+"TESTMODE",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}
	
	pComboMode->SetCurSel(b_DTCMODE);
	
	if(b_DTCMODE == 1){
		ModeName = "DTC Clear";	
	}else{
		ModeName = "DTC Detect";
	}

	DTC_MODE_STATE(ModeName);
	UpdateData(FALSE);
}

tResultVal CDTC_Option::Run()
{
	tResultVal retval;
	
	m_Etc_State = 0;
	b_StopFail = FALSE;
	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
		InsertList();
	}

	if(DTC_READ() == FALSE){//초기 DTC READ를 실패한 경우 
		for(int lop = 0;lop<4;lop++){
			m_DTCList.SetItemText(InsertIndex,4+lop,str_DTC[lop]);//INIT DTC
			m_DTCList.SetItemText(InsertIndex,8+lop,str_DTC[lop]);
			m_DTCList.SetItemText(InsertIndex,12,"X");
			m_DTCList.SetItemText(InsertIndex,13,"FAIL");
			retval.ValString.Empty();
			retval.ValString.Format("DTC READ FAIL");
			retval.m_Success = FALSE;
			Str_Mes[0] = "FAIL";
			Str_Mes[1] = "0";
			m_Etc_State = 2;
			DTC_RESULT_STATE(2,"DTC READ FAIL");
			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
				StartCnt++;
			}
			return retval;
		}
	}else{
		for(int lop = 0;lop<4;lop++){
			m_DTCList.SetItemText(InsertIndex,4+lop,str_DTC[lop]);//INIT DTC
		}
	}

	if(b_DTCMODE == 0){//DTC DETECT 모드일 경우
		if(((CImageTesterDlg *)m_pMomWnd)->m_DTCRDSTATE == 1){//DTC 데이터 없을 때 
			for(int lop = 0;lop<4;lop++){
				m_DTCList.SetItemText(InsertIndex,8+lop,str_DTC[lop]);//END DTC
			}
			m_DTCList.SetItemText(InsertIndex,12,"X");
			m_DTCList.SetItemText(InsertIndex,13,"PASS");
			retval.ValString.Empty();
			retval.ValString.Format("DTC DATA 없음");
			retval.m_Success = TRUE;
			m_Etc_State = 1;
			DTC_RESULT_STATE(1,"SUCCESS");
			Str_Mes[0] = "PASS";
			Str_Mes[1] = "1";
			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
				StartCnt++;
			}
			return retval;
		}
	}

	BOOL EraseEn = FALSE;
	BOOL PASSMODE = FALSE;

	if(b_DTCMODE == 1){//CLAER 모드 일 경우 
		EraseEn = TRUE;
	}

	if(EraseEn == TRUE){
		if(DTC_ERASE() == FALSE){
			for(int lop = 0;lop<4;lop++){
				m_DTCList.SetItemText(InsertIndex,8+lop,str_DTC[lop]);//END DTC
			}
			m_DTCList.SetItemText(InsertIndex,12,"FAIL");
			m_DTCList.SetItemText(InsertIndex,13,"FAIL");
			retval.ValString.Empty();
			retval.ValString.Format("DTC ERASE FAIL");
			retval.m_Success = FALSE;
			Str_Mes[0] = "FAIL";
			Str_Mes[1] = "0";
			m_Etc_State = 2;
			DTC_RESULT_STATE(2,"DTC ERASE FAIL");
		}else{
			for(int lop = 0;lop<4;lop++){
				m_DTCList.SetItemText(InsertIndex,8+lop,str_DTC[lop]);//END DTC
			}
			m_DTCList.SetItemText(InsertIndex,12,"SUCCESS");
			m_DTCList.SetItemText(InsertIndex,13,"PASS");
			retval.ValString.Empty();
			retval.ValString.Format("DTC ERASE SUCCESS");
			retval.m_Success = TRUE;
			Str_Mes[0] = "PASS";
			Str_Mes[1] = "1";
			m_Etc_State = 1;
			DTC_RESULT_STATE(1,"SUCCESS");
		}
	}else{
		for(int lop = 0;lop<4;lop++){
			m_DTCList.SetItemText(InsertIndex,9+lop,str_DTC[lop]);//END DTC
		}
		m_DTCList.SetItemText(InsertIndex,12,"X");
		m_DTCList.SetItemText(InsertIndex,13,"FAIL");
		retval.ValString.Empty();
		retval.ValString.Format("DTC DATA DETECT");
		retval.m_Success = FALSE;
		Str_Mes[0] = "FAIL";
		Str_Mes[1] = "0";
		m_Etc_State = 2;
		DTC_RESULT_STATE(2,"DTC DATA DETECT");
	}

	
	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
		if(retval.m_Success == TRUE)
		{
			//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_DTC,"PASS");
		}
		else
		{
			//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_DTC,"FAIL");
		}

		StartCnt++;
	}
	return retval;
}
//---------------------------------------------------------------------WORKLIST
void CDTC_Option::Set_List(CListCtrl *List){
	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"INIT DTC 1",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"INIT DTC 2",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"INIT DTC 3",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"INIT DTC 4",LVCFMT_CENTER, 80);
	List->InsertColumn(8,"END DTC 1",LVCFMT_CENTER, 80);
	List->InsertColumn(9,"END DTC 2",LVCFMT_CENTER, 80);
	List->InsertColumn(10,"END DTC 3",LVCFMT_CENTER, 80);
	List->InsertColumn(11,"END DTC 4",LVCFMT_CENTER, 80);
	List->InsertColumn(12,"ERASE WORK",LVCFMT_CENTER, 80);
	List->InsertColumn(13,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(14,"비고",LVCFMT_CENTER, 80);

	ListItemNum=15;
	Copy_List(&m_DTCList,((CImageTesterDlg *)m_pMomWnd)->m_pWorkList,ListItemNum);
}

void CDTC_Option::LOT_Set_List(CListCtrl *List)
{
	while(List->DeleteColumn(0));
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"INIT DTC 1",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"INIT DTC 2",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"INIT DTC 3",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"INIT DTC 4",LVCFMT_CENTER, 80);
	List->InsertColumn(8,"END DTC 1",LVCFMT_CENTER, 80);
	List->InsertColumn(9,"END DTC 2",LVCFMT_CENTER, 80);
	List->InsertColumn(10,"END DTC 3",LVCFMT_CENTER, 80);
	List->InsertColumn(11,"END DTC 4",LVCFMT_CENTER, 80);
	List->InsertColumn(12,"ERASE WORK",LVCFMT_CENTER, 80);
	List->InsertColumn(13,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(14,"비고",LVCFMT_CENTER, 80);

	Lot_InsertNum=15;
}


void CDTC_Option::EXCEL_SAVE(){
	CString Item[9]={"INIT DTC 1","INIT DTC 2","INIT DTC 3","INIT DTC 4","END DTC 1","END DTC 2","END DTC 3","END DTC 4","ERASE WORK"};
	CString Data ="";
	CString str="";
	str = "***********************DTC진단***********************\n\n";
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("DTC TEST",Item,9,&m_DTCList,Data);
}

void CDTC_Option::LOT_EXCEL_SAVE(){
	CString Item[9]={"INIT DTC 1","INIT DTC 2","INIT DTC 3","INIT DTC 4","END DTC 1","END DTC 2","END DTC 3","END DTC 4","ERASE WORK"};
	CString Data ="";
	CString str="";
	str = "***********************DTC진단***********************\n\n";
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("DTC TEST",Item,9,&m_Lot_DTCList,Data);
}

void CDTC_Option::InsertList(){
	CString strCnt;

	InsertIndex = m_DTCList.InsertItem(StartCnt,"",0);
	
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_DTCList.SetItemText(InsertIndex,0,strCnt);
	
	m_DTCList.SetItemText(InsertIndex,1,((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_DTCList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_DTCList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");
}

void CDTC_Option::FAIL_UPLOAD(){
	if ((((CImageTesterDlg *)m_pMomWnd)->LotMod == 1) && (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)){
		InsertList();
		for(int t=0; t<9;t++){
			m_DTCList.SetItemText(InsertIndex,t+4,"X");
		}
		m_DTCList.SetItemText(InsertIndex,13,"FAIL");
		m_DTCList.SetItemText(InsertIndex,14,"STOP");
		Str_Mes[0] = "FAIL";
		Str_Mes[1] = "0";
		//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_DTC,"STOP");
		StartCnt++;
		b_StopFail = TRUE;
	}
}

bool CDTC_Option::EXCEL_UPLOAD(){
	m_DTCList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("DTC TEST",&m_DTCList,1,&StartCnt)==TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return FALSE;
}

bool CDTC_Option::LOT_EXCEL_UPLOAD(){
	m_DTCList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("DTC TEST",&m_Lot_DTCList,1,&StartCnt)==TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return FALSE;
}

CString CDTC_Option::Mes_Result()
{
	CString sz;

	m_szMesResult = _T("");
	if(b_StopFail == FALSE){
	m_szMesResult.Format(_T("%s:%s"),Str_Mes[0],Str_Mes[1]);
	}else{
		m_szMesResult.Format(_T(":1"));
	}
	return m_szMesResult;
}

void CDTC_Option::OnBnClickedBtnDtcread()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Power_On();
	DoEvents(500);
	DTC_READ();
}
//-----------------------------------------------------------------------------

//-----------------------------이벤트 처리 함수--------------------------------

BOOL CDTC_Option::DTC_READ()
{
	BOOL ret = FALSE;
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	DTC_RESULT(4,0,"");//모두 NONE표시
	DTC_STATE(0,"Stand By");

	if(((CImageTesterDlg *)m_pMomWnd)->DCC_DTC_READ() == TRUE){
		if(((CImageTesterDlg *)m_pMomWnd)->m_DTCRDSTATE == 1){//DTC ERR가 없음
			//DTC_RESULT(4,0,"NONE");//모두 NONE표시
			for(int lop = 0;lop<4;lop++){
				str.Format("0x%02X",((CImageTesterDlg *)m_pMomWnd)->DTCRDDATA[lop]);
				DTC_RESULT(lop,1,str);
			}
		}else{
			for(int lop = 0;lop<4;lop++){
				str.Format("0x%02X",((CImageTesterDlg *)m_pMomWnd)->DTCRDDATA[lop]);
				DTC_RESULT(lop,1,str);
			}
		}
		DTC_STATE(1,"DTC READ SUCCESS");
		ret =  TRUE;
	}else{//실패시 
		DTC_RESULT(4,2,"ERR");//모두 ERR표시
		DTC_STATE(2,"DTC READ FAIL");
		ret = FALSE;
	}
	return ret;
}



void CDTC_Option::OnBnClickedBtnDtcerase()
{
	((CImageTesterDlg *)m_pMomWnd)->Power_On();
	DoEvents(500);

	DTC_ERASE();
}

BOOL CDTC_Option::DTC_ERASE()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	BOOL ret = FALSE;
	DTC_RESULT(4,0,"");//모두 NONE표시
	DTC_STATE(0,"Stand By");

	if(((CImageTesterDlg *)m_pMomWnd)->DCC_DTC_ERASE() == FALSE){
		DoEvents(100);
		if(((CImageTesterDlg *)m_pMomWnd)->DCC_DTC_ERASE() == FALSE){
			DTC_STATE(2,"DTC ERASE FAIL");
			DTC_RESULT(4,2,"ERR");//모두 ERR표시
			return FALSE;
		}
	}
	DoEvents(3000);
	if(((CImageTesterDlg *)m_pMomWnd)->DCC_DTC_READ() == TRUE){
		if(((CImageTesterDlg *)m_pMomWnd)->m_DTCRDSTATE == 1){//DTC ERR가 없음
			//DTC_RESULT(4,0,"NONE");//모두 NONE표시
			for(int lop = 0;lop<4;lop++){
				str.Format("0x%02X",((CImageTesterDlg *)m_pMomWnd)->DTCRDDATA[lop]);
				DTC_RESULT(lop,1,str);
			}
			DTC_STATE(1,"DTC ERASE SUCCESS");
			ret = TRUE;
		}else{
			for(int lop = 0;lop<4;lop++){
				str.Format("0x%02X",((CImageTesterDlg *)m_pMomWnd)->DTCRDDATA[lop]);
				DTC_RESULT(lop,1,str);
			}
			DTC_STATE(2,"DTC ERASE FAIL");
			ret = FALSE;
		}
	}else{//실패시 
		DTC_RESULT(4,2,"ERR");//모두 ERR표시
		DTC_STATE(2,"DTC ERASE FAIL");
		ret = FALSE;
	}
	return TRUE;	
}

void CDTC_Option::DTC_RESULT(int num,int col,LPCSTR lpcszString, ...)
{	
	switch(num){
		case 0:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC1))->SetWindowText(lpcszString);
			break;
		case 1:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC2))->SetWindowText(lpcszString);
			break;
		case 2:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC3))->SetWindowText(lpcszString);
			break;
		case 3:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC4))->SetWindowText(lpcszString);
			break;
		default:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC1))->SetWindowText(lpcszString);
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC2))->SetWindowText(lpcszString);
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC3))->SetWindowText(lpcszString);
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_DTC4))->SetWindowText(lpcszString);
			break;
	}
	
	if(num <=3){
		if(col == 0){
			Valcol[num] = RGB(86,86,86);
		}else if(col  ==1){
			Valcol[num] = RGB(18,69,171);
		}else{
			Valcol[num] = RGB(201,0,0);	
		}
		str_DTC[num] = lpcszString;
	}else{
		for(int lop = 0;lop<4;lop++){
			if(col == 0){
				Valcol[lop] = RGB(86,86,86);
			}else if(col  ==1){
				Valcol[lop] = RGB(18,69,171);
			}else{
				Valcol[lop] = RGB(201,0,0);	
			}
			str_DTC[lop] = lpcszString;
		}
	}
	if(((CImageTesterDlg *)m_pMomWnd)->m_pResDTCOptWnd != NULL){
		((CImageTesterDlg *)m_pMomWnd)->m_pResDTCOptWnd->DTC_RESULT(num,col,lpcszString);
	}
}

void CDTC_Option::DTC_RESULT_STATE(int col,LPCSTR lpcszString, ...)
{	
	if(((CImageTesterDlg *)m_pMomWnd)->m_pResDTCOptWnd != NULL){
		((CImageTesterDlg *)m_pMomWnd)->m_pResDTCOptWnd->DTC_STATE(col,lpcszString);
	}
}


void CDTC_Option::DTC_STATE(int col,LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_DTC_DATA))->SetWindowText(lpcszString);
	if(col == 0){
		st_col = RGB(86,86,86);
	}else if(col  ==1){
		st_col = RGB(18,69,171);
	}else{
		st_col = RGB(201,0,0);		
	}
}

HBRUSH CDTC_Option::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_DTC_NAME1){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_DTC_NAME2){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_DTC_NAME3){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_DTC_NAME4){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_DTC1){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[0]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_DTC2){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[1]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_DTC3){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[2]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_DTC4){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[3]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_DTC_DATA){
		pDC->SetTextColor(st_col);
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_DTC_MODE){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86,86,86));
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CDTC_Option::initstat()
{
	((CEdit *)GetDlgItem(IDC_EDIT_DTC_NAME1))->SetWindowText("DTC 1");
	((CEdit *)GetDlgItem(IDC_EDIT_DTC_NAME2))->SetWindowText("DTC 2");
	((CEdit *)GetDlgItem(IDC_EDIT_DTC_NAME3))->SetWindowText("DTC 3");
	((CEdit *)GetDlgItem(IDC_EDIT_DTC_NAME4))->SetWindowText("DTC 4");
	DTC_RESULT(4,0,"");//모두 NONE표시
	DTC_STATE(0,"Stand By");
	DTC_RESULT_STATE(0,"STAND BY");
}

//void CDTC_Option::OnBnClickedCheckManualmode()
//{
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//}

void CDTC_Option::OnCbnSelchangeComboDtcmode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CDTC_Option::DTC_MODE_STATE(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_DTC_MODE))->SetWindowText(lpcszString);
}


void CDTC_Option::OnBnClickedBtnDtcsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	b_DTCMODE = pComboMode->GetCurSel();
	
	Save_parameter();
}






BOOL CDTC_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		
		if (pMsg->wParam == VK_RETURN){

			return TRUE;
		}

		if(pMsg->wParam == VK_F7)
		{
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CDTC_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		pComboMode->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_DTCSAVE))->EnableWindow(0);
	}
}