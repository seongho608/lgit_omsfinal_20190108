#pragma once
#include "afxcmn.h"

// CMicom_Center 대화 상자입니다.

class CMicom_Center : public CDialog
{
	DECLARE_DYNAMIC(CMicom_Center)

public:
	CMicom_Center(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMicom_Center();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MICOM_CENTER };

	void Focus_move_start();
	void Focus_move_startDefaultX();
	void Focus_move_startDefaultY();

	tSetPoint Read_Pos;

	BOOL	READ_START_XY();
	BOOL	READ_END_XY();

	//void	Micom_Popup();

	void	LoadMicomPrmSet(tSetPoint *buf_pos,CString Filename);
	void	SaveMicomPrmSet(tSetPoint *buf_pos,CString Filename);

	BOOL	WRITE_START(int SetStart_X, int SetStart_Y);
	BOOL	WRITE_START_XY(int SetStart_X, int SetStart_Y);
	BOOL	WRITE_END_XY(int SetEnd_X, int SetEnd_Y);
	BOOL	WRITE_Stand(void);
	void	Pwr_Rst();

	void	RUNSTATE_TEXT(LPCSTR lpcszString, ...);
	void	MODE_TEXT(LPCSTR lpcszString, ...);

	bool Read_Func();
	bool Write_Func();

//	tSetPoint Read_Pos;
	tSetPoint Control_Pos;

	void	Update_List();
	void	Upload_List();
	void	Set_List();
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	CFont ft,ft2;
	int InIndex;
	CString	inifilename;
	CString micom_path;
	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT);//,tINFO INFO);
	//bool	b_edit;

	int		m_def_startX;
	int		m_def_startY;
	int		m_def_endX;
	int		m_def_endY;		

	BOOL	DefaultInitwr();
	BOOL	DefaultWrite();
	BOOL	SettingPosRead();
	BOOL	SettingPosWrite();

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CString str_def_startX;
	CString str_def_startY;
	CString str_def_endX;
	CString str_def_endY;
	CString edit_offsetX;
	CString edit_offsetY;
	afx_msg void OnBnClickedSettingPosRead();
	CListCtrl Setting_Poslist;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedDefaultPosSave();
	afx_msg void OnEnChangeDefaultStartX();
	afx_msg void OnEnChangeDefaultStartY();
	afx_msg void OnEnChangeDefaultEndX();
	afx_msg void OnEnChangeDefaultEndY();
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnBnClickedSettingPosWrite();
	afx_msg void OnBnClickedCheckEditing();
	afx_msg void OnEnChangeEditOffsetX();
	afx_msg void OnEnChangeEditOffsetY();
	afx_msg void OnBnClickedDefaultWrite();
	afx_msg void OnBnClickedDefaultInitwr();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	BOOL b_edit;
	afx_msg void OnEnSetfocusDefaultStartX();
	afx_msg void OnEnSetfocusDefaultStartY();
	afx_msg void OnEnSetfocusDefaultEndX();
	afx_msg void OnEnSetfocusDefaultEndY();
	afx_msg void OnEnSetfocusEditModeName();
	afx_msg void OnEnSetfocusEditValueRunstate();
};
