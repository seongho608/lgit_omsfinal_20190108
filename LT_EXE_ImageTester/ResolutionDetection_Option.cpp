// ResolutionDetection.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "ResolutionDetection_Option.h"

#define MIN_RESOLUTION	200
#define MAX_RESOLUTION	600

#define SIDE_MIN_RESOLUTION	200
#define SIDE_MAX_RESOLUTION	400

#define RD_FRAME_NUM	10

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
extern BYTE	m_RGBScanAvgBuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
// CPatternNoise_Option 대화 상자입니다.
extern BYTE	m_OverlayArea[CAM_IMAGE_WIDTH][CAM_IMAGE_HEIGHT];

CString RD_RectName[14] = {"C_B", "C_W", "C_LEFT", "C_RIGHT", "C_UP", "C_DOWN",/*"LT_Right","LT_Down","RT_Left","RT_Down","RD_Left","RD_Up","LD_Right","LD_Up",*/ "S1", "S2", "S3",
							"S4", "S5", "S6", "S7", "S8"};

//"C_Left","C_Right","C_Up","C_Down","LT_Right","LT_Down","RT_Left","RT_Down","RD_Left","RD_Up","LD_Right","LD_Up"

// CResolutionDetection_Option 대화 상자입니다.

IMPLEMENT_DYNAMIC(CResolutionDetection_Option, CDialog)

CResolutionDetection_Option::CResolutionDetection_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CResolutionDetection_Option::IDD, pParent)		
	, m_dMTFRatio(0)	
	, m_dResolutionThreshold(0)
	, E_Total_PosX(0)
	, E_Total_PosY(0)
	, E_Dis_Width(0)
	, E_Dis_Height(0)
	, E_Total_Width(0)
	, E_Total_Height(0)
	, E_Edge_PosX(0)
	, E_Edge_PosY(0)
	, E_Dis_Width_Edge(0)
	, E_Dis_Height_Edge(0)
	, E_Edge_Width(0)
	, E_Edge_Height(0)
	, m_dEdgeMTFRatio(0)
	, m_dEdgeResolutionThreshold(0)
	, b_Auto_Mod(FALSE)
	, str_CenterOffset(_T(""))
	, str_EdgeOffset(_T(""))
	, m_dResolutionThreshold_Peak(0)
	, m_dEdgeResolutionThreshold_Peak(0)
{
	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;
//***************************************************************************
	SetStandardRDZoneParameter(0, 340, 213, 40, 40, 0, 0);
	SetStandardRDZoneParameter(1, 340, 213, 40, 40, 0, 0);
//***************************************************************************
	SetStandardRDZoneParameter(2, 186, 225, 133, 35, 1, 0);
	SetStandardRDZoneParameter(3, 404, 221, 134, 34, 1, 1);
	SetStandardRDZoneParameter(4, 343, 87, 39, 118, 0, 1);
	SetStandardRDZoneParameter(5, 344, 277, 38, 122, 0, 0);
//***************************************************************************
	SetStandardRDZoneParameter(6, 165, 44, 119, 42, 1, 1);	//-안씀
	SetStandardRDZoneParameter(7, 40, 117, 50, 119, 0, 0);
//***************************************************************************
	SetStandardRDZoneParameter(8, 453, 39, 115, 47, 1, 0);	//-안씀
	SetStandardRDZoneParameter(9, 627, 108, 60, 120, 0, 0);
//***************************************************************************
	SetStandardRDZoneParameter(10, 454, 426, 108, 31, 1, 0);	//안씀
	SetStandardRDZoneParameter(11, 627, 245, 58, 117, 0, 1);
//***************************************************************************
	SetStandardRDZoneParameter(12, 159, 421, 106, 44, 1, 1);	//안씀-
	SetStandardRDZoneParameter(13, 40, 248, 56, 120, 0, 1);	


	b_Auto_Mod = TRUE;
	MasterMod = FALSE;

	ChangeCheck=0;
	for(int t=0; t<14; t++){
		ChangeItem[t]=-1;
	}
	changecount=0;
	i_CenterOffset=0;
	i_EdgeOffset=50;

	m_dResol_Mode = 1;
	StartCnt=0;
	b_StopFail = FALSE;
}

CResolutionDetection_Option::~CResolutionDetection_Option()
{	
}

void CResolutionDetection_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_EDIT_MTFRATIO, m_dMTFRatio);
	DDV_MinMaxInt(pDX, m_dMTFRatio, 0, 99);
	DDX_Control(pDX, IDC_LIST_RD_DATA, m_ctrlRDDataList);
	DDX_Text(pDX, IDC_EDIT_RV, m_dResolutionThreshold);
	DDX_Control(pDX, IDC_LIST_RESOLUTION, m_ResolutionList);
	DDX_Control(pDX, IDC_CHECK_AUTO, m_Automation_Check);

	//------------------------------------------------------------
	DDX_Text(pDX, IDC_RE_PosX, E_Total_PosX);
	DDX_Text(pDX, IDC_RE_PosY, E_Total_PosY);
	DDX_Text(pDX, IDC_RE_Dis_W, E_Dis_Width);
	DDX_Text(pDX, IDC_RE_Dis_H, E_Dis_Height);
	DDX_Text(pDX, IDC_RE_Width, E_Total_Width);
	DDX_Text(pDX, IDC_RE_Height, E_Total_Height);


	DDX_Text(pDX, IDC_RE_PosX2, E_Edge_PosX);
	DDX_Text(pDX, IDC_RE_PosY2, E_Edge_PosY);
	DDX_Text(pDX, IDC_RE_Dis_W2, E_Dis_Width_Edge);
	DDX_Text(pDX, IDC_RE_Dis_H2, E_Dis_Height_Edge);
	DDX_Text(pDX, IDC_RE_Width2, E_Edge_Width);
	DDX_Text(pDX, IDC_RE_Height2, E_Edge_Height);
	DDX_Text(pDX, IDC_EDIT_EDGE_MTF, m_dEdgeMTFRatio);
	DDV_MinMaxInt(pDX, m_dEdgeMTFRatio, 0, 99);
	DDX_Text(pDX, IDC_EDIT_EDGE_TRHESHOLD, m_dEdgeResolutionThreshold);
	DDX_Check(pDX, IDC_CHECK_AUTO, b_Auto_Mod);
	DDX_Text(pDX, IDC_EDIT_CENTERTOFFSET, str_CenterOffset);
	DDX_Text(pDX, IDC_EDIT_EDGEOFFSET, str_EdgeOffset);

	DDX_Text(pDX, IDC_EDIT_RV_MAX, m_dResolutionThreshold_Peak);
	DDX_Text(pDX, IDC_EDIT_EDGE_TRHESHOLD_MAX, m_dEdgeResolutionThreshold_Peak);
	DDX_Control(pDX, IDC_LIST_RESOLUTION_LOT, m_Lot_ResolutionList);
}


BEGIN_MESSAGE_MAP(CResolutionDetection_Option, CDialog)
//	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_SHOWWINDOW()
	ON_NOTIFY(NM_CLICK, IDC_LIST_RD_DATA, &CResolutionDetection_Option::OnNMClickListRdData)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_RD_DATA, &CResolutionDetection_Option::OnNMDblclkListRdData)
	ON_BN_CLICKED(IDC_BTN_SETZONE, &CResolutionDetection_Option::OnBnClickedBtnSetzone)
	ON_BN_CLICKED(IDC_BTN_DEFAULT, &CResolutionDetection_Option::OnBnClickedBtnDefault)
	ON_BN_CLICKED(IDC_BTN_SET_MTF, &CResolutionDetection_Option::OnBnClickedBtnSetMtf)
	ON_BN_CLICKED(IDC_BTN_SET_ZONE_AUTO, &CResolutionDetection_Option::OnBnClickedBtnSetZoneAuto)
	ON_BN_CLICKED(IDC_CHECK_AUTO, &CResolutionDetection_Option::OnBnClickedCheckAuto)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_RD_DATA, &CResolutionDetection_Option::OnNMCustomdrawListRdData)
	ON_EN_KILLFOCUS(IDC_EDIT_RD_MOD, &CResolutionDetection_Option::OnEnKillfocusEditRdMod)
	
	ON_BN_CLICKED(IDC_BUTTON_R_Load, &CResolutionDetection_Option::OnBnClickedButtonRLoad)
	ON_WM_TIMER()
	ON_EN_CHANGE(IDC_RE_PosX, &CResolutionDetection_Option::OnEnChangeRePosx)
	ON_EN_CHANGE(IDC_RE_PosY, &CResolutionDetection_Option::OnEnChangeRePosy)
	ON_EN_CHANGE(IDC_RE_Dis_W, &CResolutionDetection_Option::OnEnChangeReDisW)
	ON_EN_CHANGE(IDC_RE_Dis_H, &CResolutionDetection_Option::OnEnChangeReDisH)
	ON_EN_CHANGE(IDC_RE_Width, &CResolutionDetection_Option::OnEnChangeReWidth)
	ON_EN_CHANGE(IDC_RE_Height, &CResolutionDetection_Option::OnEnChangeReHeight)
	ON_EN_CHANGE(IDC_EDIT_MTFRATIO, &CResolutionDetection_Option::OnEnChangeEditMtfratio)
	ON_EN_CHANGE(IDC_EDIT_RV, &CResolutionDetection_Option::OnEnChangeEditRv)
	ON_EN_CHANGE(IDC_RE_PosX2, &CResolutionDetection_Option::OnEnChangeRePosx2)
	ON_EN_CHANGE(IDC_RE_PosY2, &CResolutionDetection_Option::OnEnChangeRePosy2)
	ON_EN_CHANGE(IDC_RE_Dis_W2, &CResolutionDetection_Option::OnEnChangeReDisW2)
	ON_EN_CHANGE(IDC_RE_Dis_H2, &CResolutionDetection_Option::OnEnChangeReDisH2)
	ON_EN_CHANGE(IDC_RE_Width2, &CResolutionDetection_Option::OnEnChangeReWidth2)
	ON_EN_CHANGE(IDC_RE_Height2, &CResolutionDetection_Option::OnEnChangeReHeight2)
	ON_EN_CHANGE(IDC_EDIT_EDGE_MTF, &CResolutionDetection_Option::OnEnChangeEditEdgeMtf)
	ON_EN_CHANGE(IDC_EDIT_EDGE_TRHESHOLD, &CResolutionDetection_Option::OnEnChangeEditEdgeTrheshold)
	ON_EN_CHANGE(IDC_EDIT_RD_MOD, &CResolutionDetection_Option::OnEnChangeEditRdMod)
	ON_WM_MOUSEWHEEL()
	ON_EN_CHANGE(IDC_EDIT_CENTERTOFFSET, &CResolutionDetection_Option::OnEnChangeEditCentertoffset)
	ON_EN_CHANGE(IDC_EDIT_EDGEOFFSET, &CResolutionDetection_Option::OnEnChangeEditEdgeoffset)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_OFFSET, &CResolutionDetection_Option::OnBnClickedButtonSaveOffset)
	ON_BN_CLICKED(IDC_BUTTON1, &CResolutionDetection_Option::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_CHECK_SET_LIMITED, &CResolutionDetection_Option::OnBnClickedCheckSetLimited)
	ON_BN_CLICKED(IDC_CHECK_SET_SFR, &CResolutionDetection_Option::OnBnClickedCheckSetSfr)
	ON_BN_CLICKED(IDC_CHECK_190F, &CResolutionDetection_Option::OnBnClickedCheck190f)
END_MESSAGE_MAP()



BOOL CResolutionDetection_Option::OnInitDialog()
{
	CDialog::OnInitDialog();
	

	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;
	m_dMTFRatio = 80;
	m_dEdgeMTFRatio = 80;
	m_dResolutionThreshold = 300;
	m_dResolutionThreshold_Peak = 900;
	m_dEdgeResolutionThreshold = 300;
	m_dEdgeResolutionThreshold_Peak = 475;

	m_Automation_Check.SetCheck(TRUE);
	
	m_b190Angle_Field = FALSE;

	H_filename=((CImageTesterDlg  *)m_pMomWnd)->modelfile_Name;
	
	m_RD_dc = NULL;
	
	SETLIST();
	
	for(int i=0; i<14; i++)
	{	
		if( i == 0 && i == 1)
		{
			RD_RECT[i].MTFRatio = 0;
			RD_RECT[i].m_Thresold = 0;
			RD_RECT[i].m_Thresold_Peak = 0;
			RD_RECT[i].EdgeDelta = 0;
		}
		else
		{
			RD_RECT[i].MTFRatio = 10;
			RD_RECT[i].m_Thresold = 300;
			RD_RECT[i].m_Thresold_Peak = 900;

			RD_RECT[i].SFR_Width = 0;
			RD_RECT[i].SFR_Height = 0;
			RD_RECT[i].SFR_MTF_Value = NULL;
			RD_RECT[i].SFR_MTF_LineFitting_Value = NULL;
			RD_RECT[i].SFR_BWRatio = NULL;
			RD_RECT[i].EdgeDelta = 0;
		}

		RD_RECT[i].m_checkLine_Buffer_cnt = 0;
	}

	RD_CHECK_MODE = FALSE;	
	
	Load_parameter();
	
	if(m_dResol_Mode == 0)
	{
		((CButton*)GetDlgItem(IDC_CHECK_SET_LIMITED))->SetCheck(1);
		((CButton*)GetDlgItem(IDC_CHECK_SET_SFR))->SetCheck(0);
	}
	else
	{
		((CButton*)GetDlgItem(IDC_CHECK_SET_LIMITED))->SetCheck(0);
		((CButton*)GetDlgItem(IDC_CHECK_SET_SFR))->SetCheck(1);
	}

	UpdateData(FALSE);

	if(b_Auto_Mod == 1){
		(CEdit *)GetDlgItem(IDC_RE_PosX)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_PosY)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Dis_W)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Dis_H)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Width)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Height)->EnableWindow(0);

		(CEdit *)GetDlgItem(IDC_RE_PosX2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_PosY2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Dis_W2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Dis_H2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Width2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Height2)->EnableWindow(0);
	
	}else{
		(CEdit *)GetDlgItem(IDC_RE_PosX)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_PosY)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Dis_W)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Dis_H)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Width)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Height)->EnableWindow(1);

		(CEdit *)GetDlgItem(IDC_RE_PosX2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_PosY2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Dis_W2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Dis_H2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Width2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Height2)->EnableWindow(1);
	}

	Set_List(&m_ResolutionList);
	LOT_Set_List(&m_Lot_ResolutionList);

	InitEVMS();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CResolutionDetection_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_SAVE_OFFSET))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_SET_SFR))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_AUTO))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_190F))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK3))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_RESOLUTION_KEEPRUN))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_SET_ZONE_AUTO))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_R_Load))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_SET_MTF))->EnableWindow(0);
		
		((CEdit *)GetDlgItem(IDC_RE_PosX))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_RE_PosY))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_RE_Dis_W))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_RE_Dis_H))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_RE_Width))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_RE_Height))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MTFRATIO))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_RV))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_RV_MAX))->EnableWindow(0);

		((CEdit *)GetDlgItem(IDC_RE_PosX2))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_RE_PosY2))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_RE_Dis_W2))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_RE_Dis_H2))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_RE_Width2))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_RE_Height2))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_EDGE_MTF))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_EDGE_TRHESHOLD))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_EDGE_TRHESHOLD_MAX))->EnableWindow(0);

		((CEdit *)GetDlgItem(IDC_EDIT_EDGEOFFSET))->EnableWindow(0);

		
	}
}

void CResolutionDetection_Option::SETLIST(){
	m_ctrlRDDataList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);
	m_ctrlRDDataList.InsertColumn(0, "구분", LVCFMT_CENTER, 80);
	m_ctrlRDDataList.InsertColumn(1, "PosX", LVCFMT_CENTER, 40);
	m_ctrlRDDataList.InsertColumn(2, "PosY", LVCFMT_CENTER, 40);
	
	m_ctrlRDDataList.InsertColumn(3, "Start X", LVCFMT_CENTER, 50);
	m_ctrlRDDataList.InsertColumn(4, "Start Y", LVCFMT_CENTER, 50);
	m_ctrlRDDataList.InsertColumn(5, "Exit X", LVCFMT_CENTER, 50);
	m_ctrlRDDataList.InsertColumn(6, "Exit Y", LVCFMT_CENTER, 50);

	m_ctrlRDDataList.InsertColumn(7, "Width", LVCFMT_CENTER, 50);
	m_ctrlRDDataList.InsertColumn(8, "Height", LVCFMT_CENTER, 50);
	m_ctrlRDDataList.InsertColumn(9, "MODE", LVCFMT_CENTER, 45);
	m_ctrlRDDataList.InsertColumn(10, "Dir", LVCFMT_CENTER, 35);
	
	m_ctrlRDDataList.InsertColumn(11, "MTF", LVCFMT_CENTER, 45);
	m_ctrlRDDataList.InsertColumn(12, "本", LVCFMT_CENTER, 50);
	m_ctrlRDDataList.InsertColumn(13, "Peak 本", LVCFMT_CENTER, 50);

	m_ctrlRDDataList.InsertItem(0, "C_B");
	m_ctrlRDDataList.InsertItem(1, "C_W");
	m_ctrlRDDataList.InsertItem(2, "C_LEFT");
	m_ctrlRDDataList.InsertItem(3, "C_RIGHT");
	m_ctrlRDDataList.InsertItem(4, "C_UP");
	m_ctrlRDDataList.InsertItem(5, "C_DOWN");
	m_ctrlRDDataList.InsertItem(6, "S1");
	m_ctrlRDDataList.InsertItem(7, "S2");
	
	m_ctrlRDDataList.InsertItem(8, "S3");
	m_ctrlRDDataList.InsertItem(9, "S4");
	//m_ctrlRDDataList.InsertItem(10, "S5");
	//m_ctrlRDDataList.InsertItem(11, "S6");
	//m_ctrlRDDataList.InsertItem(12, "S7");
	//m_ctrlRDDataList.InsertItem(13, "S8");
	//m_ctrlRDDataList.InsertItem(6, "SIDE_LEFT_UP_RIGHT");
	//m_ctrlRDDataList.InsertItem(7, "SIDE_LEFT_UP_DOWN");
	//
	//m_ctrlRDDataList.InsertItem(8, "SIDE_RIGHT_UP_LEFT");
	//m_ctrlRDDataList.InsertItem(9, "SIDE_RIGHT_UP_DOWN");
	//m_ctrlRDDataList.InsertItem(10, "SIDE_RIGHT_DOWN_LEFT");
	//m_ctrlRDDataList.InsertItem(11, "SIDE_RIGHT_DOWN_UP");
	//m_ctrlRDDataList.InsertItem(12, "SIDE_LEFT_DOWN_RIGHT");
	//m_ctrlRDDataList.InsertItem(13, "SIDE_LEFT_DOWN_UP");
	
	m_ctrlRDDataList.EnableWindow(1);
}

void CResolutionDetection_Option::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",H_filename);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

void CResolutionDetection_Option::Save_parameter(){

	CString str="";
	CString strTitle="";

//	WritePrivateProfileString(str_model,NULL,"",H_filename);
	str.Empty();
	str.Format("%d",m_INFO.ID);
	WritePrivateProfileString(str_model,"ID",str,H_filename);
	str.Empty();
	str.Format("%s",m_INFO.MODE);
	WritePrivateProfileString(str_model,"MODE",str,H_filename);
	str.Empty();
	str.Format("%s",m_INFO.NAME);
	WritePrivateProfileString(str_model,"NAME",str,H_filename);

	strTitle.Empty();
	strTitle="RESOLUTION_INIT";	
	
	str.Empty();	
	str.Format("%d", b_Auto_Mod);
	WritePrivateProfileString(str_model,strTitle+"AUTOMODE",str,H_filename);
	
	//------------------------------------------------------------------------------------

	str.Empty();
	str.Format("%d",m_dMTFRatio);
	WritePrivateProfileString(str_model,strTitle+"CenterMTFRatio",str,H_filename);
	str.Empty();
	str.Format("%d",m_dResolutionThreshold);
	WritePrivateProfileString(str_model,strTitle+"CenterResolutionThreshold",str,H_filename);
	str.Empty();
	str.Format("%d",m_dResolutionThreshold_Peak);
	WritePrivateProfileString(str_model,strTitle+"CenterResolutionThreshold_Peak",str,H_filename);

	str.Empty();
	str.Format("%d",m_dEdgeMTFRatio);
	WritePrivateProfileString(str_model,strTitle+"EdgeMTFRatio",str,H_filename);
	str.Empty();
	str.Format("%d",m_dEdgeResolutionThreshold);
	WritePrivateProfileString(str_model,strTitle+"EdgeResolutionThreshold",str,H_filename);
	str.Format("%d",m_dEdgeResolutionThreshold_Peak);
	WritePrivateProfileString(str_model,strTitle+"EdgeResolutionThreshold_Peak",str,H_filename);

	str.Empty();
	str.Format("%d",E_Total_PosX);
	WritePrivateProfileString(str_model,strTitle+"PosX",str,H_filename);
	str.Empty();
	str.Format("%d",E_Total_PosY);
	WritePrivateProfileString(str_model,strTitle+"PosY",str,H_filename);
	str.Empty();
	str.Format("%d",E_Dis_Width);
	WritePrivateProfileString(str_model,strTitle+"DisW",str,H_filename);
	str.Empty();
	str.Format("%d",E_Dis_Height);
	WritePrivateProfileString(str_model,strTitle+"DisH",str,H_filename);
	str.Empty();
	str.Format("%d",E_Total_Width);
	WritePrivateProfileString(str_model,strTitle+"Width",str,H_filename);
	str.Empty();
	str.Format("%d",E_Total_Height);
	WritePrivateProfileString(str_model,strTitle+"Height",str,H_filename);
	str.Empty();
	str.Format("%d",E_Edge_PosX);
	WritePrivateProfileString(str_model,strTitle+"PosX_E",str,H_filename);
	str.Empty();
	str.Format("%d",E_Edge_PosY);
	WritePrivateProfileString(str_model,strTitle+"PosY_E",str,H_filename);
	str.Empty();
	str.Format("%d",E_Dis_Width_Edge);
	WritePrivateProfileString(str_model,strTitle+"DisW_E",str,H_filename);
	str.Empty();
	str.Format("%d",E_Dis_Height_Edge);
	WritePrivateProfileString(str_model,strTitle+"DisH_E",str,H_filename);
	str.Empty();
	str.Format("%d",E_Edge_Width);
	WritePrivateProfileString(str_model,strTitle+"Width_E",str,H_filename);
	str.Empty();
	str.Format("%d",E_Edge_Height);
	WritePrivateProfileString(str_model,strTitle+"Height_E",str,H_filename);
	
	str.Empty();
	str.Format("%d",i_CenterOffset);
	WritePrivateProfileString(str_model,strTitle+"CenterOffset",str,H_filename);
	str.Empty();
	str.Format("%d",i_EdgeOffset);
	WritePrivateProfileString(str_model,strTitle+"EdgeOffset",str,H_filename);
	
	//------------------------------------------------------------------------------------
	for(int t=0; t<14; t++){	
		
		strTitle.Empty();strTitle=RD_RectName[t];	
		
		str.Empty();
		str.Format("%d",RD_RECT[t].m_PosX);
		WritePrivateProfileString(str_model,strTitle+"PosX",str,H_filename);
		str.Empty();
		str.Format("%d",RD_RECT[t].m_PosY);
		WritePrivateProfileString(str_model,strTitle+"PosY",str,H_filename);
		str.Empty();
		str.Format("%d",RD_RECT[t].m_Left);
		WritePrivateProfileString(str_model,strTitle+"StartX",str,H_filename);
		str.Empty();
		str.Format("%d",RD_RECT[t].m_Top);
		WritePrivateProfileString(str_model,strTitle+"StartY",str,H_filename);
		str.Empty();
		str.Format("%d",RD_RECT[t].m_Right);
		WritePrivateProfileString(str_model,strTitle+"ExitX",str,H_filename);
		str.Empty();
		str.Format("%d",RD_RECT[t].m_Bottom);
		WritePrivateProfileString(str_model,strTitle+"ExitY",str,H_filename);
		str.Empty();
		str.Format("%d",RD_RECT[t].m_Width);
		WritePrivateProfileString(str_model,strTitle+"Width",str,H_filename);
		str.Empty();
		str.Format("%d",RD_RECT[t].m_Height);
		WritePrivateProfileString(str_model,strTitle+"Height",str,H_filename);
		str.Empty();
		str.Format("%d",RD_RECT[t].m_bmode);
		WritePrivateProfileString(str_model,strTitle+"Mode",str,H_filename);
		str.Empty();
		str.Format("%d",RD_RECT[t].m_dir);
		WritePrivateProfileString(str_model,strTitle+"Dir",str,H_filename);		

		
		str.Empty();
		str.Format("%0.0f", RD_RECT[t].MTFRatio);
		WritePrivateProfileString(str_model,strTitle+"MTFRatio",str,H_filename);
		str.Empty();
		str.Format("%d", RD_RECT[t].m_Thresold);
		WritePrivateProfileString(str_model,strTitle+"ResolutionThreshold",str,H_filename);
		str.Empty();
		str.Format("%d", RD_RECT[t].m_Thresold_Peak);
		WritePrivateProfileString(str_model,strTitle+"ResolutionThreshold_Peak",str,H_filename);
		str.Empty();
		str.Format("%d", RD_RECT[t].ENABLE);
		WritePrivateProfileString(str_model,strTitle+"Enable",str,H_filename);
	}

}

void CResolutionDetection_Option::Load_parameter(){
	CFileFind filefind;
	CString str="";
	CString strTitle="";

	UpdateData(TRUE);//불러들인 값을 윈도우에 써넣는다. . 

	if((GetPrivateProfileCString(str_model,"NAME",H_filename) != m_INFO.NAME)||(GetPrivateProfileCString(str_model,"MODE",H_filename) != m_INFO.MODE)){//ini파일이 없으면 파일을 생성한다.
		
		//SetDefaultRDZone();
		b_Auto_Mod = TRUE;

		E_Total_PosX=360;
		E_Total_PosY=240;
		E_Dis_Width=30;
		E_Dis_Height=30;
		E_Total_Width=140;
		E_Total_Height=50;
		
		E_Edge_PosX=190;
		E_Edge_PosY=100;
		E_Dis_Width_Edge=350;
		E_Dis_Height_Edge=50;
		E_Edge_Width=60;
		E_Edge_Height=60;

		m_dMTFRatio = 80;
		m_dResolutionThreshold = 300;
		m_dResolutionThreshold_Peak = 475;

		m_dEdgeMTFRatio = 80;		
		m_dEdgeResolutionThreshold = 250;
		m_dEdgeResolutionThreshold_Peak = 475;


		INIT_EIJAGEN();

		i_CenterOffset = 0;
		i_EdgeOffset = 50;
		
		m_b190Angle_Field = TRUE;
		((CButton*)GetDlgItem(IDC_CHECK_190F))->SetCheck(TRUE);
		
		UpdateData(FALSE);//현재 셋팅된 값을 써넣는다. 
	
	}else{

		strTitle.Empty();
		strTitle="RESOLUTION_INIT";

		b_Auto_Mod = GetPrivateProfileInt(str_model,strTitle+"AUTOMODE",-1,H_filename);
		if(b_Auto_Mod == -1){
			b_Auto_Mod = 0;
		}
		
		m_b190Angle_Field = GetPrivateProfileInt(str_model,strTitle+"190F",-1,H_filename);
		if( m_b190Angle_Field == -1 || m_b190Angle_Field == 1 )
		{
			m_b190Angle_Field = TRUE;
			((CButton*)GetDlgItem(IDC_CHECK_190F))->SetCheck(TRUE);
		}
		else if( m_b190Angle_Field == 0)
		{
			m_b190Angle_Field = FALSE;
			((CButton*)GetDlgItem(IDC_CHECK_190F))->SetCheck(FALSE);
		}		

		m_dResol_Mode = GetPrivateProfileInt(str_model,strTitle+"RESOL_MODE",1,H_filename);

		m_dMTFRatio = GetPrivateProfileInt(str_model,strTitle+"CenterMTFRatio",-1,H_filename);
		if(m_dMTFRatio ==-1){
			m_dMTFRatio = 80;
		}
		m_dResolutionThreshold = GetPrivateProfileInt(str_model,strTitle+"CenterResolutionThreshold",-1,H_filename);
		if(m_dResolutionThreshold ==-1){
			m_dResolutionThreshold = 300;
		}
		m_dResolutionThreshold_Peak = GetPrivateProfileInt(str_model,strTitle+"CenterResolutionThreshold_Peak",-1,H_filename);
		if(m_dResolutionThreshold_Peak ==-1){
			m_dResolutionThreshold_Peak = 475;
		}
		m_dEdgeMTFRatio = GetPrivateProfileInt(str_model,strTitle+"EdgeMTFRatio",-1,H_filename);
		if(m_dEdgeMTFRatio == -1){
			m_dEdgeMTFRatio = 80;
		}
		m_dEdgeResolutionThreshold = GetPrivateProfileInt(str_model,strTitle+"EdgeResolutionThreshold",-1,H_filename);
		if(m_dEdgeResolutionThreshold == -1){
			m_dEdgeResolutionThreshold = 250;
		}
		m_dEdgeResolutionThreshold_Peak = GetPrivateProfileInt(str_model,strTitle+"EdgeResolutionThreshold_Peak",-1,H_filename);
		if(m_dEdgeResolutionThreshold_Peak == -1){
			m_dEdgeResolutionThreshold_Peak = 475;
		}
		E_Total_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,H_filename);
		if(E_Total_PosX == -1){
			E_Total_PosX=360;
		}
		E_Total_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,H_filename);
		if(E_Total_PosY == -1){
			E_Total_PosY=240;
		}
		E_Dis_Width=GetPrivateProfileInt(str_model,strTitle+"DisW",-1,H_filename);
		if(E_Dis_Width== -1){
			E_Dis_Width=30;
		}
		E_Dis_Height=GetPrivateProfileInt(str_model,strTitle+"DisH",-1,H_filename);
		if(E_Dis_Height == -1){
			E_Dis_Height=30;
		}
		E_Total_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,H_filename);
		if(E_Total_Width == -1){
			E_Total_Width=140;
		}
		E_Total_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,H_filename);
		if(E_Total_Height == -1){
			E_Total_Height=50;
		}
		E_Edge_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX_E",-1,H_filename);
		if(E_Edge_PosX == -1){
			E_Edge_PosX=190;
		}
		E_Edge_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY_E",-1,H_filename);
		if(E_Edge_PosY == -1){
			E_Edge_PosY=100;
		}
		E_Dis_Width_Edge=GetPrivateProfileInt(str_model,strTitle+"DisW_E",-1,H_filename);
		if(E_Dis_Width_Edge == -1){
			E_Dis_Width_Edge=350;
		}
		E_Dis_Height_Edge=GetPrivateProfileInt(str_model,strTitle+"DisH_E",-1,H_filename);
		if(E_Dis_Height_Edge == -1){
			E_Dis_Height_Edge=50;
		}
		E_Edge_Width=GetPrivateProfileInt(str_model,strTitle+"Width_E",-1,H_filename);
		if(E_Edge_Width == -1){
			E_Edge_Width=60;
		}
		E_Edge_Height=GetPrivateProfileInt(str_model,strTitle+"Height_E",-1,H_filename);
		if(E_Edge_Height == -1){
			E_Edge_Height=60;
		}
		i_CenterOffset=GetPrivateProfileInt(str_model,strTitle+"CenterOffset",-1,H_filename);
		if(i_CenterOffset ==-1){
			i_CenterOffset =0;
			str.Empty();
			str.Format("%d",i_CenterOffset);
			WritePrivateProfileString(str_model,strTitle+"CenterOffset",str,H_filename);
		}
		str_CenterOffset.Format("%d",i_CenterOffset);
		i_EdgeOffset=GetPrivateProfileInt(str_model,strTitle+"EdgeOffset",-1,H_filename);
		if(i_EdgeOffset ==-1){
			i_EdgeOffset =50;
			str.Empty();
			str.Format("%d",i_EdgeOffset);
			WritePrivateProfileString(str_model,strTitle+"EdgeOffset",str,H_filename);
		}
		str_EdgeOffset.Format("%d",i_EdgeOffset);

		
		for(int t=0; t<10; t++){
			strTitle.Empty();strTitle=RD_RectName[t];
			
			RD_RECT[t].m_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,H_filename);
			if(RD_RECT[t].m_PosX == -1){
				E_Total_PosX=360;
				E_Total_PosY=240;
				E_Dis_Width=30;
				E_Dis_Height=30;
				E_Total_Width=140;
				E_Total_Height=50;
				
				E_Edge_PosX=190;
				E_Edge_PosY=100;
				E_Dis_Width_Edge=350;
				E_Dis_Height_Edge=50;
				E_Edge_Width=60;
				E_Edge_Height=60;

				m_dMTFRatio = 80;
				m_dResolutionThreshold = 300;
				m_dResolutionThreshold_Peak = 475;

				m_dEdgeMTFRatio = 80;		
				m_dEdgeResolutionThreshold = 250;
				m_dEdgeResolutionThreshold_Peak = 475;

				INIT_EIJAGEN();

				UpdateData(FALSE);//현재 셋팅된 값을 써넣는다. 
				Save_parameter();
			}
			RD_RECT[t].m_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,H_filename);
			RD_RECT[t].m_Left=GetPrivateProfileInt(str_model,strTitle+"StartX",-1,H_filename);
			RD_RECT[t].m_Top=GetPrivateProfileInt(str_model,strTitle+"StartY",-1,H_filename);
			RD_RECT[t].m_Right=GetPrivateProfileInt(str_model,strTitle+"ExitX",-1,H_filename);
			RD_RECT[t].m_Bottom=GetPrivateProfileInt(str_model,strTitle+"ExitY",-1,H_filename);			
			RD_RECT[t].m_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,H_filename);
			RD_RECT[t].m_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,H_filename);
			RD_RECT[t].m_bmode=GetPrivateProfileInt(str_model,strTitle+"Mode",-1,H_filename);
			RD_RECT[t].m_dir=GetPrivateProfileInt(str_model,strTitle+"Dir",-1,H_filename);					
			RD_RECT[t].MTFRatio=GetPrivateProfileInt(str_model,strTitle+"MTFRatio",-1,H_filename);
			RD_RECT[t].m_Thresold=GetPrivateProfileInt(str_model,strTitle+"ResolutionThreshold",-1,H_filename);
			RD_RECT[t].m_Thresold_Peak=GetPrivateProfileInt(str_model,strTitle+"ResolutionThreshold_Peak",-1,H_filename);
			
			BASIC_RECT[t].m_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,H_filename);
			BASIC_RECT[t].m_Left=GetPrivateProfileInt(str_model,strTitle+"StartX",-1,H_filename);
			BASIC_RECT[t].m_Top=GetPrivateProfileInt(str_model,strTitle+"StartY",-1,H_filename);
			BASIC_RECT[t].m_Right=GetPrivateProfileInt(str_model,strTitle+"ExitX",-1,H_filename);
			BASIC_RECT[t].m_Bottom=GetPrivateProfileInt(str_model,strTitle+"ExitY",-1,H_filename);			
			BASIC_RECT[t].m_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,H_filename);
			BASIC_RECT[t].m_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,H_filename);
			BASIC_RECT[t].m_bmode=GetPrivateProfileInt(str_model,strTitle+"Mode",-1,H_filename);
			BASIC_RECT[t].m_dir=GetPrivateProfileInt(str_model,strTitle+"Dir",-1,H_filename);					

			int BUFDATA = GetPrivateProfileInt(str_model,strTitle+"ResolutionThreshold_Peak",-1,H_filename);			
			if(BUFDATA == -1)
			{
				if( t  == 0 || t == 1 )
				{
					WritePrivateProfileString(str_model,strTitle+"ResolutionThreshold_Peak","0",H_filename);	
					RD_RECT[t].m_Thresold_Peak = 0;
				}
				else if( t  > 1 && t < 6 )
				{
					WritePrivateProfileString(str_model,strTitle+"ResolutionThreshold_Peak","475",H_filename);	
					RD_RECT[t].m_Thresold_Peak = 475;
				}
				else
				{
					WritePrivateProfileString(str_model,strTitle+"ResolutionThreshold_Peak","475",H_filename);	
					RD_RECT[t].m_Thresold_Peak = 475;
				}
			}

			BUFDATA = GetPrivateProfileInt(str_model,strTitle+"Enable",-1,H_filename);
			if(BUFDATA == -1){
				WritePrivateProfileString(str_model,strTitle+"Enable","1",H_filename);	
				RD_RECT[t].ENABLE = TRUE;
			}else{
				RD_RECT[t].ENABLE = BUFDATA;
			}
		}

		RD_RECT[10].ENABLE = FALSE;
		RD_RECT[11].ENABLE = FALSE;
		RD_RECT[12].ENABLE = FALSE;
		RD_RECT[14].ENABLE = FALSE;

		Uploadlist();//SetLoadParameterRDZone();
	}
	
	Save_parameter();
	str_CenterOffset.Format("%d",i_CenterOffset);
	str_EdgeOffset.Format("%d",i_EdgeOffset);
}

// CResolutionDetection_Option 메시지 처리기입니다.
tResultVal CResolutionDetection_Option::Run()
{
	int ResolutionLoopCnt = 21;
	b_StopFail = FALSE;	
	for(int i=2; i<10; i++)
	{
		RD_RECT[i].m_dPatternStartPos.x = 0;
		RD_RECT[i].m_dPatternStartPos.y = 0;
		RD_RECT[i].m_CheckLine = 0;
		RD_RECT[i].m_ResultData = 0;

		for(int j=0; j<30; j++)
			RD_RECT[i].m_checkLine_Buffer[j] = 0;
	}

	if(ChangeCheck == TRUE){
		OnBnClickedBtnSetMtf();
	}

	
	RD_CHECK_MODE = TRUE;
	
	tResultVal retval = {0,};
	
	retval.m_Success = TRUE;

	CString str="";
	CHECK_MODE =FALSE;
	if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand==TRUE){

		InsertList();	//-------------------------------------------------------워크리스트 추가
	}
	int count=0;
	
	int camcnt = 0;
	bool EdgeFailCnt = FALSE;
	
	int loop_cnt = 0;
	int avg_check_line = 0;

	OnBnClickedBtnSetzone();
	ChangeCheck =0;
	for(int t=0; t<14; t++){
		ChangeItem[t]=-1;
	}
	for(int t=0; t<10; t++){
		m_ctrlRDDataList.Update(t);
	}
		
	int old_offset_x=0, old_offset_y=0;
	int a=0, b=0;
	
	CvPoint old_centerOffset = cvPoint(0, 0);
	
	RGBScanBufCnt = 0;
	
	memset(m_RGBScanAvgBuf,0,sizeof(m_RGBScanAvgBuf));

	while(loop_cnt < ResolutionLoopCnt)
	{
		((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan =1;						
		for(int i =0;i<50;i++){
			DoEvents(10);
			if(((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan == 0){
				break;
			}
		}		
		
		if( ((CButton *)GetDlgItem(IDC_CHECK_AUTO))->GetCheck() ){
			SetZoneUsingAutoDetection(m_RGBScanbuf);		
		}

		for(int lop=0; lop<2;lop++)
		{
			Get_Ref_Threshold_Value(m_RGBScanbuf,lop);//중심점을 측정하여 적용한다. 
		}
		
		CvPoint centerPt = GetCenterPoint(m_RGBScanbuf);
				
		CvPoint new_centerOffset = cvPoint( centerPt.x-(m_CAM_SIZE_WIDTH/2), centerPt.y-(m_CAM_SIZE_HEIGHT/2));

		if((new_centerOffset.x >30)||(new_centerOffset.x <-30)){
			new_centerOffset.x = 0;
		}
		if((new_centerOffset.y >30)||(new_centerOffset.y <-30)){
			new_centerOffset.y = 0;

		}
		
		RGBScanBufCnt++;
		
		int sum_B=0;
		int sum_G=0;
		int sum_R=0;

		for(int y=0; y<m_CAM_SIZE_HEIGHT; y++)
		{
			for(int x=0; x<m_CAM_SIZE_WIDTH; x++)
			{
				sum_B=0;
				sum_G=0;
				sum_R=0;

				sum_B += (unsigned char)m_RGBScanbuf[y*(m_CAM_SIZE_WIDTH*4) + x*4	] + (unsigned char)m_RGBScanAvgBuf[y*(m_CAM_SIZE_WIDTH*4) + x*4	];
				sum_G += (unsigned char)m_RGBScanbuf[y*(m_CAM_SIZE_WIDTH*4) + x*4+1 ] + (unsigned char)m_RGBScanAvgBuf[y*(m_CAM_SIZE_WIDTH*4) + x*4+1 ];
				sum_R += (unsigned char)m_RGBScanbuf[y*(m_CAM_SIZE_WIDTH*4) + x*4+2 ] + (unsigned char)m_RGBScanAvgBuf[y*(m_CAM_SIZE_WIDTH*4) + x*4+2 ];
				
				if(RGBScanBufCnt > 1)
				{
					sum_B = sum_B / 2;
					sum_G = sum_G / 2;
					sum_R = sum_R / 2;
				}
				else
				{
					sum_B = sum_B / 1;
					sum_G = sum_G / 1;
					sum_R = sum_R / 1;
				}				

			//	m_RGBScanAvgBuf[y*(m_CAM_SIZE_WIDTH*4) + x*4	] += m_RGBScanbuf[y*(m_CAM_SIZE_WIDTH*4) + x*4	];
			//	m_RGBScanAvgBuf[y*(m_CAM_SIZE_WIDTH*4) + x*4+1  ] += m_RGBScanbuf[y*(m_CAM_SIZE_WIDTH*4) + x*4	+1];
			//	m_RGBScanAvgBuf[y*(m_CAM_SIZE_WIDTH*4) + x*4+2  ] += m_RGBScanbuf[y*(m_CAM_SIZE_WIDTH*4) + x*4	+2];

				m_RGBScanAvgBuf[y*(m_CAM_SIZE_WIDTH*4) + x*4	] = sum_B;
				m_RGBScanAvgBuf[y*(m_CAM_SIZE_WIDTH*4) + x*4+1  ] = sum_G;
				m_RGBScanAvgBuf[y*(m_CAM_SIZE_WIDTH*4) + x*4+2	] = sum_R;
			}
		}
		
		

	//	if(RGBScanBufCnt > ResolutionLoopCnt - 1)
		{
		
		/*	BYTE R,G,B;
			IplImage *tempRGBImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3);

			for (int y=0; y<m_CAM_SIZE_HEIGHT; y++)
			{
				for (int x=0; x<m_CAM_SIZE_WIDTH; x++)
				{
				
					tempRGBImage->imageData[y*tempRGBImage->widthStep+(x*3)] = m_RGBScanAvgBuf[y*(m_CAM_SIZE_WIDTH*4) + x*4	];
					tempRGBImage->imageData[y*tempRGBImage->widthStep+(x*3) + 1] = m_RGBScanAvgBuf[y*(m_CAM_SIZE_WIDTH*4) + x*4+1  ];	
					tempRGBImage->imageData[y*tempRGBImage->widthStep+(x*3) + 2] = m_RGBScanAvgBuf[y*(m_CAM_SIZE_WIDTH*4) + x*4+2	];	
				}
			}
			cvSaveImage("C:\\AvgRGBImage.bmp", tempRGBImage);*/
		

			for(int lop=2; lop<10;lop++)
			{
				avg_check_line = 0;

				if(RD_RECT[lop].ENABLE == TRUE){
								
				/*	RD_RECT[lop].m_Left += (new_centerOffset.x - old_centerOffset.x);
					RD_RECT[lop].m_Right += (new_centerOffset.x - old_centerOffset.x);
					RD_RECT[lop].m_Top += (new_centerOffset.y - old_centerOffset.y);
					RD_RECT[lop].m_Bottom += (new_centerOffset.y - old_centerOffset.y);*/
					old_centerOffset = new_centerOffset;
					
					if(RD_RECT[lop].MTFRatio > 30.0)
						m_Ref_Threshold = (double)(m_White_Ref_Value - m_Black_Ref_Value) * (double)(31.0/100.0);
					else
						m_Ref_Threshold = (double)(m_White_Ref_Value - m_Black_Ref_Value) * (double)(RD_RECT[lop].MTFRatio/100.0);

					CheckResolution(m_RGBScanAvgBuf,lop, loop_cnt);
					RD_RECT[lop].m_checkLine_Buffer_cnt++;

					if(RD_RECT[lop].m_checkLine_Buffer_cnt == 16)
					{
						for(int k=0;k <15; k++)
							RD_RECT[lop].m_checkLine_Buffer[k] = RD_RECT[lop].m_checkLine_Buffer[k+1];
				
						RD_RECT[lop].m_checkLine_Buffer_cnt = 15;
					}
											
					for(int k=0; k<RD_RECT[lop].m_checkLine_Buffer_cnt; k++)
						avg_check_line += RD_RECT[lop].m_checkLine_Buffer[k];

					avg_check_line = avg_check_line / 15;	//10 : Circular Buffer Number
					
							
					RD_RECT[lop].m_CheckLine = avg_check_line;			
					
					int data = 0;
					if((lop >1) && (lop < 6)){
						RD_RECT[lop].m_ResultData = GetResultData(lop);//; + i_CenterOffset;
					}else{
						RD_RECT[lop].m_ResultData = GetResultData(lop);//; + i_EdgeOffset;
						
					}

					

					
					if(lop > 5)  
					{
						if(RD_RECT[lop].m_ResultData >= RD_RECT[lop].m_Thresold && RD_RECT[lop].m_ResultData <= RD_RECT[lop].m_Thresold_Peak && RD_RECT[lop].EdgeDelta < i_EdgeOffset){
							RD_RECT[lop].m_Success = TRUE;  
							MES_RES_SUCC[lop] = TRUE;
						}else{
							RD_RECT[lop].m_Success = FALSE;
							MES_RES_SUCC[lop] = FALSE;
						}
					}
					else{
						if(RD_RECT[lop].m_ResultData >= RD_RECT[lop].m_Thresold && RD_RECT[lop].m_ResultData <= RD_RECT[lop].m_Thresold_Peak){
							RD_RECT[lop].m_Success = TRUE;
							MES_RES_SUCC[lop] = TRUE;
						}else{
							RD_RECT[lop].m_Success = FALSE;
							MES_RES_SUCC[lop] = FALSE;
						}
					}

					
				}
			}
		}
		loop_cnt++;		

		if( RGBScanBufCnt > 1 )
		{
			RGBScanBufCnt = 0;
			memset(m_RGBScanAvgBuf,0,sizeof(m_RGBScanAvgBuf));
		}		
	}
	
	for(int lop=2; lop<10; lop++)
	{
		//if(QL_IG == 0)
		//{
		//	if(RD_RECT[lop].m_bmode == 0)
		//		RD_RECT[lop].m_ResultData = 193+((double)RD_RECT[lop].m_ResultData*0.44);

		//	int data = RD_RECT[lop].m_ResultData;

		//	if (data >= 500 && data <= 510)
		//	{
		//		RD_RECT[lop].m_ResultData -= 10;
		//	}else if (data >= 511 && data <= 520)
		//	{
		//		RD_RECT[lop].m_ResultData -= 20;
		//	}else if (data >= 521 && data <= 530)
		//	{
		//		RD_RECT[lop].m_ResultData -= 30;
		//	}else if (data >= 531 && data <= 540)
		//	{
		//		RD_RECT[lop].m_ResultData -= 40;
		//	}else if (data >= 541 && data <= 550)
		//	{
		//		RD_RECT[lop].m_ResultData -= 50;
		//	}else if (data >= 551 && data <= 560)
		//	{
		//		RD_RECT[lop].m_ResultData -= 60;
		//	}
		//}


		if(lop > 5)
		{
			if(RD_RECT[lop].m_ResultData >= RD_RECT[lop].m_Thresold && RD_RECT[lop].m_ResultData <= RD_RECT[lop].m_Thresold_Peak && RD_RECT[lop].EdgeDelta < i_EdgeOffset){
				RD_RECT[lop].m_Success = TRUE;
				MES_RES_SUCC[lop] = 1;
			}else{
				RD_RECT[lop].m_Success = FALSE;
				MES_RES_SUCC[lop] = 0;
			}
		}
		else{
			if(RD_RECT[lop].m_ResultData >= RD_RECT[lop].m_Thresold && RD_RECT[lop].m_ResultData <= RD_RECT[lop].m_Thresold_Peak){
				RD_RECT[lop].m_Success = TRUE;
				MES_RES_SUCC[lop] = 1;
			}else{
				RD_RECT[lop].m_Success = FALSE;
				MES_RES_SUCC[lop] = 0;
			}
		}
	}

	//-------------------------------------------------------워크리스트 추가
	int enableCnt = 0;
	count = 0;

	for(int i=2; i<10; i++)
	{
		if(RD_RECT[i].ENABLE == TRUE){
			enableCnt++;
		}
	}

	CString stateDATA ="해상도_ ";

	for(int lop=2; lop<10;lop++){
	

		if(RD_RECT[lop].ENABLE ==TRUE){
			str.Empty();
			str.Format("%d",RD_RECT[lop].m_ResultData);

		}else{
			str = "X";
		}

		stateDATA += RD_RectName[lop] + ("_ ")+str+(", ");
		
		if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == TRUE){

			if(RD_RECT[lop].ENABLE ==TRUE){
				str.Empty();
				str.Format("%d",RD_RECT[lop].m_ResultData);
				m_ResolutionList.SetItemText(InsertIndex,2+lop,str);
			}
			else{
				m_ResolutionList.SetItemText(InsertIndex,2+lop,"X");
			}
		}
		if(RD_RECT[lop].m_Success== TRUE && RD_RECT[lop].ENABLE){
			count++;
		}

		if(lop<6){
			int Index = ((CImageTesterDlg  *)m_pMomWnd)->m_pResResolutionOptWnd->m_CenterList.InsertItem(lop-2,"",0);

			((CImageTesterDlg  *)m_pMomWnd)->m_pResResolutionOptWnd->m_CenterList.SetItemText(Index,0,RD_RectName[lop]);
			if(RD_RECT[lop].m_Success == TRUE){
				((CImageTesterDlg  *)m_pMomWnd)->m_pResResolutionOptWnd->m_CenterList.SetItemText(Index,2,"PASS");
				
			}else{
				((CImageTesterDlg  *)m_pMomWnd)->m_pResResolutionOptWnd->m_CenterList.SetItemText(Index,2,"FAIL");
				
			}
			if(RD_RECT[lop].ENABLE ==TRUE){
				str.Empty();
				str.Format("%d",RD_RECT[lop].m_ResultData);
				((CImageTesterDlg  *)m_pMomWnd)->m_pResResolutionOptWnd->m_CenterList.SetItemText(Index,1,str);
			}
			else{
				((CImageTesterDlg  *)m_pMomWnd)->m_pResResolutionOptWnd->m_CenterList.SetItemText(Index,1,"X");
				((CImageTesterDlg  *)m_pMomWnd)->m_pResResolutionOptWnd->m_CenterList.SetItemText(Index,2,"NONE");

			}
			
			
		}else{
			int Index = ((CImageTesterDlg  *)m_pMomWnd)->m_pResResolutionOptWnd->m_SideList.InsertItem(lop-2,"",0);

			if(RD_RECT[lop].m_Success == TRUE){
				((CImageTesterDlg  *)m_pMomWnd)->m_pResResolutionOptWnd->m_SideList.SetItemText(Index,2,"PASS");
				
			}else{
				((CImageTesterDlg  *)m_pMomWnd)->m_pResResolutionOptWnd->m_SideList.SetItemText(Index,2,"FAIL");
				
			}
			((CImageTesterDlg  *)m_pMomWnd)->m_pResResolutionOptWnd->m_SideList.SetItemText(Index,0,RD_RectName[lop]);
			if(RD_RECT[lop].ENABLE ==TRUE){
				str.Empty();
				str.Format("%d",RD_RECT[lop].m_ResultData);
				((CImageTesterDlg  *)m_pMomWnd)->m_pResResolutionOptWnd->m_SideList.SetItemText(Index,1,str);
			}
			else{
				((CImageTesterDlg  *)m_pMomWnd)->m_pResResolutionOptWnd->m_SideList.SetItemText(Index,1,"X");
				((CImageTesterDlg  *)m_pMomWnd)->m_pResResolutionOptWnd->m_SideList.SetItemText(Index,2,"NONE");
			}
		
		}
	}
	if(count == enableCnt){
			retval.m_Success = TRUE;
			stateDATA += "_ PASS";
			if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == TRUE){

			//((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex,WORKLIST_RESOLUTION,"PASS");
			m_ResolutionList.SetItemText(InsertIndex,12,"PASS");
			}
	}else{
		retval.m_Success = FALSE;
		stateDATA += "_ FAIL";
		if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == TRUE){

			//((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex,WORKLIST_RESOLUTION,"FAIL");
			m_ResolutionList.SetItemText(InsertIndex,12,"FAIL");
		}
	}
	((CImageTesterDlg  *)m_pMomWnd)->StatePrintf(stateDATA);
	DoEvents(1000);
	
	retval.m_ID = 0x09;
	retval.ValString.Empty();
	retval.ValString.Format("%d개 중 %d 성공",enableCnt, count);	

if(((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == TRUE){
		StartCnt++;
	}
	//----------------------------------------------------------------------
	//StatePrintf("해상력 측정 모드가 종료되었습니다");
//	retval.m_Success = FALSE;

	return retval;
	
}

void CResolutionDetection_Option::FAIL_UPLOAD(){
	if ((((CImageTesterDlg *)m_pMomWnd)->LotMod == 1) && (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)){

	InsertList();
	for (int t=0; t<8; t++)
	{
		m_ResolutionList.SetItemText(InsertIndex,t+4,"X");	

	}
	m_ResolutionList.SetItemText(InsertIndex,12,"FAIL");	
	m_ResolutionList.SetItemText(InsertIndex,13,"STOP");

	//((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex,WORKLIST_RESOLUTION,"STOP");
	StartCnt++;
	b_StopFail = TRUE;
	}
}
void CResolutionDetection_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);;
}

void CModel_Create(CResolutionDetection_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO){
	CRect OptionRect;
	if(pTabWnd != NULL){
		((CResolutionDetection_Option *)*pWnd) = new CResolutionDetection_Option(pMomWnd);
		((CResolutionDetection_Option *)*pWnd)->Setup(pMomWnd,pEdit,INFO);
		((CResolutionDetection_Option *)*pWnd)->Create(((CResolutionDetection_Option *)*pWnd)->IDD,pTabWnd);//&m_CtrTab);	
		((CResolutionDetection_Option *)*pWnd)->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		((CResolutionDetection_Option *)*pWnd)->MoveWindow(20,40,OptionRect.Width(),OptionRect.Height());
	}
}

void CModel_Delete(CResolutionDetection_Option **pWnd){
	if(((CResolutionDetection_Option *)*pWnd) != NULL){
//		((CResolutionDetection_Option *)*pWnd)->KillTimer(130);
		((CResolutionDetection_Option *)*pWnd)->DestroyWindow();
		delete ((CResolutionDetection_Option *)*pWnd);
		((CResolutionDetection_Option *)*pWnd) = NULL;	
	}
}


bool CResolutionDetection_Option::ResolutionDectctionPic(CDC *cdc,int NUM)
{

	//if(EIJARect.Chkdata() == FALSE){return 0;}//재대로 된 데이터가 아니면 바로 빠져나간다.

	CPen	my_Pan,*old_pan;
	CString TEXTDATA ="";
	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	
//***********************초기 원점 탐색 영역******************************//
	my_Pan.CreatePen(PS_SOLID,2,WHITE_COLOR);
	old_pan = cdc->SelectObject(&my_Pan);

	int left = (m_CAM_SIZE_WIDTH*0.375);
	int right = (m_CAM_SIZE_WIDTH*0.625); 
	int top = (m_CAM_SIZE_HEIGHT*0.333) ;
	int bottom = (m_CAM_SIZE_HEIGHT*0.666);
	cdc->MoveTo(left, top);
	cdc->LineTo(right,top);
	cdc->LineTo(right,bottom);
	cdc->LineTo(left,bottom);
	cdc->LineTo(left,top);

	cdc->SelectObject(old_pan);
	old_pan->DeleteObject();
	my_Pan.DeleteObject();
//*********************************************************************

	if(RD_RECT[NUM].ENABLE == TRUE)
	{
					
		if(NUM > 1)
		{
			cdc->SetTextColor(GREEN_COLOR);
			cdc->TextOut(RD_RECT[NUM].m_Left, RD_RECT[NUM].m_Top,RD_RectName[NUM].GetBuffer(0),RD_RectName[NUM].GetLength());
		}

		if(RD_RECT[NUM].m_Success == TRUE){
			my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
			cdc->SetTextColor(BLUE_COLOR);
		}else{
			my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
			cdc->SetTextColor(RED_COLOR);
		}
		old_pan = cdc->SelectObject(&my_Pan);

	//	Rectangle_Empty(cdc,EIJARect.m_Left,EIJARect.m_Top,EIJARect.m_Right,EIJARect.m_Bottom);//사각형 이미지를 그린다.
		cdc->MoveTo(RD_RECT[NUM].m_Left,RD_RECT[NUM].m_Top);
		cdc->LineTo(RD_RECT[NUM].m_Right,RD_RECT[NUM].m_Top);
		cdc->LineTo(RD_RECT[NUM].m_Right,RD_RECT[NUM].m_Bottom);
		cdc->LineTo(RD_RECT[NUM].m_Left,RD_RECT[NUM].m_Bottom);
		cdc->LineTo(RD_RECT[NUM].m_Left,RD_RECT[NUM].m_Top);
		

		cdc->SelectObject(old_pan);
		old_pan->DeleteObject();
		my_Pan.DeleteObject();

		if(RD_RECT[NUM].m_bmode == 0){//0: 상하 1:좌우
			
			int m_ConstValue = RD_RECT[NUM].m_Top + RD_RECT[NUM].m_CheckLine;
			int m_ResultValue = RD_RECT[NUM].m_Top + RD_RECT[NUM].m_ResultLine;
			
			my_Pan.CreatePen(PS_SOLID,2,YELLOW_COLOR);
			old_pan = cdc->SelectObject(&my_Pan);
			cdc->MoveTo(RD_RECT[NUM].m_Left,m_ConstValue);//고정좌표
			cdc->LineTo(RD_RECT[NUM].m_Right,m_ConstValue);
			cdc->SelectObject(old_pan);
			old_pan->DeleteObject();
			my_Pan.DeleteObject();

			my_Pan.CreatePen(PS_SOLID,2,GREEN_COLOR);
			old_pan = cdc->SelectObject(&my_Pan);

			int offset1, offset2;
			offset1 = RD_RECT[NUM].Height() - RD_RECT[NUM].m_dPatternStartPos.y;
			offset2 = RD_RECT[NUM].m_dPatternEndPos.y;

			cdc->MoveTo(RD_RECT[NUM].m_Left, RD_RECT[NUM].m_Bottom - offset1);
			cdc->LineTo(RD_RECT[NUM].m_Right,RD_RECT[NUM].m_Bottom - offset1);	
			cdc->MoveTo(RD_RECT[NUM].m_Left, RD_RECT[NUM].m_Top + offset2);
			cdc->LineTo(RD_RECT[NUM].m_Right,RD_RECT[NUM].m_Top + offset2);	
			
		//	RD_RECT[NUM].m_ResultData			= GetResultData(NUM);
			RD_RECT[NUM].m_ThresholdPosition	= GetStandardData(NUM);

			cdc->SelectObject(old_pan);
			old_pan->DeleteObject();
			my_Pan.DeleteObject();
							
	//*******************************************************************************//기준점 표출 부
			if(NUM > 1)
			{			

				if(RD_RECT[NUM].m_dir == 0)
				{
					int ThresholdValue = RD_RECT[NUM].m_Bottom - offset1 - RD_RECT[NUM].m_ThresholdPosition;
					int PeakThresholdValue = RD_RECT[NUM].m_Bottom - offset1 - GetStandardPeakData(NUM);
					
					my_Pan.CreatePen(PS_SOLID,2,PINK_COLOR);
					old_pan = cdc->SelectObject(&my_Pan);
					
					if(abs(RD_RECT[NUM].m_Left-RD_RECT[NUM].m_Right) != 0)
					{
						cdc->MoveTo(RD_RECT[NUM].m_Left,ThresholdValue);//기준점
						cdc->LineTo(RD_RECT[NUM].m_Right,ThresholdValue);
					}

					cdc->SelectObject(old_pan);
					old_pan->DeleteObject();
					my_Pan.DeleteObject();
					my_Pan.CreatePen(PS_SOLID,2,RGB(200, 0, 200));
					old_pan = cdc->SelectObject(&my_Pan);

					if(abs(RD_RECT[NUM].m_Left-RD_RECT[NUM].m_Right) != 0)
					{
						cdc->MoveTo(RD_RECT[NUM].m_Left,PeakThresholdValue);//기준점
						cdc->LineTo(RD_RECT[NUM].m_Right,PeakThresholdValue);
					}	

					cdc->SelectObject(old_pan);
					old_pan->DeleteObject();
					my_Pan.DeleteObject();
				}
				else
				{
					int ThresholdValue = RD_RECT[NUM].m_Top + RD_RECT[NUM].m_dPatternStartPos.y + RD_RECT[NUM].m_ThresholdPosition;
					int PeakThresholdValue = RD_RECT[NUM].m_Top + RD_RECT[NUM].m_dPatternStartPos.y + GetStandardPeakData(NUM);

					my_Pan.CreatePen(PS_SOLID,2,PINK_COLOR);
					old_pan = cdc->SelectObject(&my_Pan);

					if(abs(RD_RECT[NUM].m_Left-RD_RECT[NUM].m_Right) != 0)
					{
						cdc->MoveTo(RD_RECT[NUM].m_Left,ThresholdValue);//기준점
						cdc->LineTo(RD_RECT[NUM].m_Right,ThresholdValue);
					}
				
					cdc->SelectObject(old_pan);
					old_pan->DeleteObject();
					my_Pan.DeleteObject();
					my_Pan.CreatePen(PS_SOLID,2,RGB(200, 0, 200));
					old_pan = cdc->SelectObject(&my_Pan);

					if(abs(RD_RECT[NUM].m_Left-RD_RECT[NUM].m_Right) != 0)
					{
						cdc->MoveTo(RD_RECT[NUM].m_Left,PeakThresholdValue);//기준점
						cdc->LineTo(RD_RECT[NUM].m_Right,PeakThresholdValue);
					}
					cdc->SelectObject(old_pan);
					old_pan->DeleteObject();
					my_Pan.DeleteObject();
				}
			}
	//*******************************************************************************
		

			if(NUM > 5)
			{
				if(RD_RECT[NUM].m_ResultData >=  RD_RECT[NUM].m_Thresold && RD_RECT[NUM].EdgeDelta < i_EdgeOffset){
					my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
					cdc->SetTextColor(BLUE_COLOR);
				}else{
					my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
					cdc->SetTextColor(RED_COLOR);
				}
			}
			else{
				if(RD_RECT[NUM].m_ResultData >=  RD_RECT[NUM].m_Thresold){
					my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
					cdc->SetTextColor(BLUE_COLOR);
				}else{
					my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
					cdc->SetTextColor(RED_COLOR);
				}
			}
			
			old_pan = cdc->SelectObject(&my_Pan);
			
			
			
			if(((CButton*)GetDlgItem(IDC_CHECK_190F))->GetCheck() && NUM > 5)
			{
				RD_RECT[NUM].m_resultX = (RD_RECT[NUM].m_Left+RD_RECT[NUM].m_Right)/2;
				RD_RECT[NUM].m_resultY = (RD_RECT[NUM].m_Top+RD_RECT[NUM].m_Bottom)/2;

				double standard_dist = GetDistance(m_CAM_SIZE_WIDTH/2, m_CAM_SIZE_HEIGHT/2, m_CAM_SIZE_WIDTH, 0);
				double dist = GetDistance(m_CAM_SIZE_WIDTH/2, m_CAM_SIZE_HEIGHT/2, RD_RECT[NUM].m_resultX, RD_RECT[NUM].m_resultY);
				
			//	if( fabs((dist/standard_dist) - 0.7) < 0.01)
			//		TEXTDATA.Format("0.70F");
			//	else
					TEXTDATA.Format("(%6.2f)F",dist/standard_dist);

				cdc->TextOut(RD_RECT[NUM].m_Left -30, m_ResultValue+30,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
			}

			if(NUM > 1)
			{
				TEXTDATA.Format("%d",RD_RECT[NUM].m_ResultData);
				cdc->TextOut(RD_RECT[NUM].m_Left -30, m_ResultValue,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
			}
			

//****************************************************** SIDE 해상력 편차 *********************************************//
			if(RD_RECT[6].ENABLE && RD_RECT[10].ENABLE){
				if(NUM == 6 || NUM == 10)
				{
					TEXTDATA.Format("S1-S5 : %d", abs(RD_RECT[6].m_ResultData - RD_RECT[10].m_ResultData));
					RD_RECT[NUM].EdgeDelta = abs(RD_RECT[6].m_ResultData - RD_RECT[10].m_ResultData);
					cdc->TextOut(m_ResultValue,RD_RECT[NUM].m_Top+35,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
				}
			}
			if(RD_RECT[7].ENABLE && RD_RECT[11].ENABLE){
				if(NUM == 7 || NUM == 11)
				{
					TEXTDATA.Format("S2-S6 : %d", abs(RD_RECT[7].m_ResultData - RD_RECT[11].m_ResultData));
					RD_RECT[NUM].EdgeDelta = abs(RD_RECT[7].m_ResultData - RD_RECT[11].m_ResultData);
					cdc->TextOut(m_ResultValue,RD_RECT[NUM].m_Top+35,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
				}
			}
			if(RD_RECT[8].ENABLE && RD_RECT[12].ENABLE){
				if(NUM == 8 || NUM == 12)
				{
					TEXTDATA.Format("S3-S7 : %d", abs(RD_RECT[8].m_ResultData - RD_RECT[12].m_ResultData));
					RD_RECT[NUM].EdgeDelta = abs(RD_RECT[8].m_ResultData - RD_RECT[12].m_ResultData);
					cdc->TextOut(m_ResultValue,RD_RECT[NUM].m_Top+35,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
				}
			}
			if(RD_RECT[9].ENABLE && RD_RECT[13].ENABLE){
				if(NUM == 9 || NUM == 13)
				{
					TEXTDATA.Format("S4-S8 : %d", abs(RD_RECT[9].m_ResultData - RD_RECT[13].m_ResultData));
					RD_RECT[NUM].EdgeDelta = abs(RD_RECT[9].m_ResultData - RD_RECT[13].m_ResultData);
					cdc->TextOut(m_ResultValue,RD_RECT[NUM].m_Top+35,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
				}
			}
//*********************************************************************************************************************//		

			cdc->SelectObject(old_pan);
			old_pan->DeleteObject();
			my_Pan.DeleteObject();

			if( ((CButton*)GetDlgItem(IDC_CHECK3))->GetCheck() )
				DrawSFRGraph(cdc, NUM);
		}else{
			int m_ConstValue = RD_RECT[NUM].m_Left + RD_RECT[NUM].m_CheckLine;
			int m_ResultValue = RD_RECT[NUM].m_Left + RD_RECT[NUM].m_ResultLine;
			
			my_Pan.CreatePen(PS_SOLID,2,YELLOW_COLOR);
			old_pan = cdc->SelectObject(&my_Pan);
			cdc->MoveTo(m_ConstValue,RD_RECT[NUM].m_Top);//고정좌표
			cdc->LineTo(m_ConstValue,RD_RECT[NUM].m_Bottom);
			cdc->SelectObject(old_pan);
			old_pan->DeleteObject();
			my_Pan.DeleteObject();

			my_Pan.CreatePen(PS_SOLID,2,GREEN_COLOR);
			old_pan = cdc->SelectObject(&my_Pan);
			
			int offset1, offset2;
			offset1 = RD_RECT[NUM].Length() - RD_RECT[NUM].m_dPatternEndPos.x;
			offset2 = RD_RECT[NUM].m_dPatternStartPos.x;

			cdc->MoveTo(RD_RECT[NUM].m_Left + offset2, RD_RECT[NUM].m_Top);
			cdc->LineTo(RD_RECT[NUM].m_Left + offset2, RD_RECT[NUM].m_Bottom);	
			cdc->MoveTo(RD_RECT[NUM].m_Right - offset1, RD_RECT[NUM].m_Top);
			cdc->LineTo(RD_RECT[NUM].m_Right - offset1, RD_RECT[NUM].m_Bottom);	
			
		//	RD_RECT[NUM].m_ResultData			= GetResultData(NUM);
			RD_RECT[NUM].m_ThresholdPosition    = GetStandardData(NUM);
			int PeakThresholdValue = GetStandardPeakData(NUM);
	//*******************************************************************************//기준점 표출 부
			
			cdc->SelectObject(old_pan);
			old_pan->DeleteObject();
			my_Pan.DeleteObject();
			if(NUM > 1)
			{			
				

				if(RD_RECT[NUM].m_dir == 0)
				{
					int ThresholdValue = RD_RECT[NUM].m_Left + (RD_RECT[NUM].m_dPatternStartPos.x) + RD_RECT[NUM].m_ThresholdPosition;
					int ThresholdValue_Peak = RD_RECT[NUM].m_Left + (RD_RECT[NUM].m_dPatternStartPos.x) + PeakThresholdValue;
					
					my_Pan.CreatePen(PS_SOLID,2,PINK_COLOR);
					old_pan = cdc->SelectObject(&my_Pan);
				
					if(abs(RD_RECT[NUM].m_Top-RD_RECT[NUM].m_Bottom) != 0)
					{
						cdc->MoveTo(ThresholdValue, RD_RECT[NUM].m_Top);//기준점
						cdc->LineTo(ThresholdValue, RD_RECT[NUM].m_Bottom);
					}
					
					cdc->SelectObject(old_pan);
					old_pan->DeleteObject();
					my_Pan.DeleteObject();
					my_Pan.CreatePen(PS_SOLID,2,RGB(200, 0, 200));
					old_pan = cdc->SelectObject(&my_Pan);

					if(abs(RD_RECT[NUM].m_Top-RD_RECT[NUM].m_Bottom) != 0)
					{
						cdc->MoveTo(ThresholdValue_Peak, RD_RECT[NUM].m_Top);//기준점
						cdc->LineTo(ThresholdValue_Peak, RD_RECT[NUM].m_Bottom);
					}
					cdc->SelectObject(old_pan);
					old_pan->DeleteObject();
					my_Pan.DeleteObject();
				}
				else
				{
					int ThresholdValue = RD_RECT[NUM].m_Right - (RD_RECT[NUM].Length() - RD_RECT[NUM].m_dPatternStartPos.x) - RD_RECT[NUM].m_ThresholdPosition;
					int ThresholdValue_Peak = RD_RECT[NUM].m_Right - (RD_RECT[NUM].Length() - RD_RECT[NUM].m_dPatternStartPos.x) - PeakThresholdValue;
					
					my_Pan.CreatePen(PS_SOLID,2,PINK_COLOR);
					old_pan = cdc->SelectObject(&my_Pan);

					if(abs(RD_RECT[NUM].m_Top-RD_RECT[NUM].m_Bottom) != 0)
					{
						cdc->MoveTo(ThresholdValue, RD_RECT[NUM].m_Top);//기준점
						cdc->LineTo(ThresholdValue, RD_RECT[NUM].m_Bottom);
					}
					
					cdc->SelectObject(old_pan);
					old_pan->DeleteObject();
					my_Pan.DeleteObject();
					my_Pan.CreatePen(PS_SOLID,2,RGB(200, 0, 200));
					old_pan = cdc->SelectObject(&my_Pan);
				
					if(abs(RD_RECT[NUM].m_Top-RD_RECT[NUM].m_Bottom) != 0)
					{
						cdc->MoveTo(ThresholdValue_Peak, RD_RECT[NUM].m_Top);//기준점
						cdc->LineTo(ThresholdValue_Peak, RD_RECT[NUM].m_Bottom);
					}
					cdc->SelectObject(old_pan);
					old_pan->DeleteObject();
					my_Pan.DeleteObject();
				}
			}
	//*******************************************************************************

		
			
			if(NUM > 5)
			{
				if(RD_RECT[NUM].m_ResultData >=  RD_RECT[NUM].m_Thresold && RD_RECT[NUM].EdgeDelta < i_EdgeOffset){
					my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
					cdc->SetTextColor(BLUE_COLOR);
				}else{
					my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
					cdc->SetTextColor(RED_COLOR);
				}
			}
			else{
				if(RD_RECT[NUM].m_ResultData >=  RD_RECT[NUM].m_Thresold){
					my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
					cdc->SetTextColor(BLUE_COLOR);
				}else{
					my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
					cdc->SetTextColor(RED_COLOR);
				}
			}
			
			old_pan = cdc->SelectObject(&my_Pan);
			
			
			if(((CButton*)GetDlgItem(IDC_CHECK_190F))->GetCheck() && NUM > 5)
			{
				RD_RECT[NUM].m_resultX = (RD_RECT[NUM].m_Left+RD_RECT[NUM].m_Right)/2;
				RD_RECT[NUM].m_resultY = (RD_RECT[NUM].m_Top+RD_RECT[NUM].m_Bottom)/2;
					
				double standard_dist = GetDistance(m_CAM_SIZE_WIDTH/2, m_CAM_SIZE_HEIGHT/2, m_CAM_SIZE_WIDTH, 0);
				double dist = GetDistance(m_CAM_SIZE_WIDTH/2, m_CAM_SIZE_HEIGHT/2, RD_RECT[NUM].m_resultX, RD_RECT[NUM].m_resultY);

			//	if( fabs((dist/standard_dist) - 0.7) < 0.01)
			//		TEXTDATA.Format("0.70F");
			//	else
					TEXTDATA.Format("(%6.2f)F",dist/standard_dist);

				cdc->TextOut(m_ResultValue,RD_RECT[NUM].m_Top -50,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
			}

			if(NUM > 1)
			{				
				TEXTDATA.Format("%d",RD_RECT[NUM].m_ResultData);
				cdc->TextOut(m_ResultValue,RD_RECT[NUM].m_Top -20,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());				
			}
			
//****************************************************** SIDE 해상력 편차 *********************************************//
			if(RD_RECT[6].ENABLE && RD_RECT[10].ENABLE){
				if(NUM == 6 || NUM == 10)
				{
					TEXTDATA.Format("S1-S5 : %d", abs(RD_RECT[6].m_ResultData - RD_RECT[10].m_ResultData));
					RD_RECT[NUM].EdgeDelta = abs(RD_RECT[6].m_ResultData - RD_RECT[10].m_ResultData);
					cdc->TextOut(m_ResultValue,RD_RECT[NUM].m_Top-35,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
				}
			}
			if(RD_RECT[7].ENABLE && RD_RECT[11].ENABLE){
				if(NUM == 7 || NUM == 11)
				{
					TEXTDATA.Format("S2-S6 : %d", abs(RD_RECT[7].m_ResultData - RD_RECT[11].m_ResultData));
					RD_RECT[NUM].EdgeDelta = abs(RD_RECT[7].m_ResultData - RD_RECT[11].m_ResultData);
					cdc->TextOut(m_ResultValue,RD_RECT[NUM].m_Top-35,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
				}
			}
			if(RD_RECT[8].ENABLE && RD_RECT[12].ENABLE){
				if(NUM == 8 || NUM == 12)
				{
					TEXTDATA.Format("S3-S7 : %d", abs(RD_RECT[8].m_ResultData - RD_RECT[12].m_ResultData));
					RD_RECT[NUM].EdgeDelta = abs(RD_RECT[8].m_ResultData - RD_RECT[12].m_ResultData);
					cdc->TextOut(m_ResultValue,RD_RECT[NUM].m_Top-35,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
				}
			}
			if(RD_RECT[9].ENABLE && RD_RECT[13].ENABLE){
				if(NUM == 9 || NUM == 13)
				{
					TEXTDATA.Format("S4-S8 : %d", abs(RD_RECT[9].m_ResultData - RD_RECT[13].m_ResultData));
					RD_RECT[NUM].EdgeDelta = abs(RD_RECT[9].m_ResultData - RD_RECT[13].m_ResultData);
					cdc->TextOut(m_ResultValue,RD_RECT[NUM].m_Top-35,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
				}
			}
//*********************************************************************************************************************//		

			cdc->SelectObject(old_pan);
			old_pan->DeleteObject();
			my_Pan.DeleteObject();

			if( ((CButton*)GetDlgItem(IDC_CHECK3))->GetCheck() )
				DrawSFRGraph(cdc, NUM);
			
		}

		if(RD_RECT[NUM].m_ResultData >=  RD_RECT[NUM].m_Thresold && RD_RECT[NUM].m_ResultData <= RD_RECT[NUM].m_Thresold_Peak){
			my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
			cdc->SetTextColor(BLUE_COLOR);

		}else{
			my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
			cdc->SetTextColor(RED_COLOR);

		}
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->MoveTo(RD_RECT[NUM].m_Left,RD_RECT[NUM].m_Top);
		cdc->LineTo(RD_RECT[NUM].m_Right,RD_RECT[NUM].m_Top);
		cdc->LineTo(RD_RECT[NUM].m_Right,RD_RECT[NUM].m_Bottom);
		cdc->LineTo(RD_RECT[NUM].m_Left,RD_RECT[NUM].m_Bottom);
		cdc->LineTo(RD_RECT[NUM].m_Left,RD_RECT[NUM].m_Top);

		
		cdc->SelectObject(old_pan);
		old_pan->DeleteObject();
		my_Pan.DeleteObject();

		if(NUM == 0 || NUM == 1)
		{
			my_Pan.CreatePen(PS_SOLID,2, GRAY_COLOR);
			old_pan = cdc->SelectObject(&my_Pan);
			cdc->MoveTo(RD_RECT[NUM].m_Left,RD_RECT[NUM].m_Top);
			cdc->LineTo(RD_RECT[NUM].m_Right,RD_RECT[NUM].m_Top);
			cdc->LineTo(RD_RECT[NUM].m_Right,RD_RECT[NUM].m_Bottom);
			cdc->LineTo(RD_RECT[NUM].m_Left,RD_RECT[NUM].m_Bottom);
			cdc->LineTo(RD_RECT[NUM].m_Left,RD_RECT[NUM].m_Top);

			cdc->SelectObject(old_pan);
			old_pan->DeleteObject();
			my_Pan.DeleteObject();	
		}		

	}	

	return 1;
}
void CResolutionDetection_Option::Pic(CDC *cdc)
{
	for(int lop=0;lop<14;lop++){
		ResolutionDectctionPic(cdc,lop);
	}
}

void CResolutionDetection_Option::InitPrm()
{
	Load_parameter();

	UpdateData(FALSE);

	if(b_Auto_Mod == 1){
		(CEdit *)GetDlgItem(IDC_RE_PosX)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_PosY)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Dis_W)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Dis_H)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Width)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Height)->EnableWindow(0);

		(CEdit *)GetDlgItem(IDC_RE_PosX2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_PosY2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Dis_W2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Dis_H2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Width2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Height2)->EnableWindow(0);
	
	}else{
		(CEdit *)GetDlgItem(IDC_RE_PosX)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_PosY)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Dis_W)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Dis_H)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Width)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Height)->EnableWindow(1);

		(CEdit *)GetDlgItem(IDC_RE_PosX2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_PosY2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Dis_W2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Dis_H2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Width2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Height2)->EnableWindow(1);
	}

	for(int i=2; i<14; i++)
	{
		RD_RECT[i].m_dPatternStartPos.x = 0;
		RD_RECT[i].m_dPatternStartPos.y = 0;
		RD_RECT[i].m_CheckLine = 0;
		RD_RECT[i].m_ResultData = 0;
		RD_RECT[i].m_Success = FALSE;

		for(int j=0; j<10; j++)
			RD_RECT[i].m_checkLine_Buffer[j] = 0;
	}

}


void CResolutionDetection_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}
bool CResolutionDetection_Option::CheckResolution(LPBYTE IN_RGB,int NUM, int loop_cnt)
{
	RD_RECT[NUM].m_Success= FALSE;
	//	if(RD_RECT[NUM].Chkdata() == FALSE){return 0;}//재대로 된 데이터가 아니면 바로 빠져나간다.

	BYTE R,G,B;
	double Ave_V[CAM_IMAGE_WIDTH];
	BYTE BW[360][360];
	BYTE BW_BOOL_LAVE[CAM_IMAGE_WIDTH];
	bool BW_BOOL[360][360];

	int lop1,lopmax1;
	int	lop2,lopmax2;
	int OffSetL = 0;

	int Width = RD_RECT[NUM].Length();
	int Height = RD_RECT[NUM].Height();
	int OffSetX = RD_RECT[NUM].m_Left;
	int OffSetY = RD_RECT[NUM].m_Top;
	
	BYTE *BWImage = new BYTE [Width * Height * 4];
	BYTE *Overlay_BWImage = new BYTE [Width * Height * 4];
		
	BYTE	Sum_Y = 0; 
	int		R_Count=1;

	for (int y=0; y<Height; y++){//영역으로 지정한 범위의 RGB데이터를 추출하여 밝기 영상만 추출한다. 	
		for (int x=0; x<Width; x++){//0~100	
			B = IN_RGB[(OffSetY+y)*(m_CAM_SIZE_WIDTH*4) + (OffSetX+x)*4    ];
			G = IN_RGB[(OffSetY+y)*(m_CAM_SIZE_WIDTH*4) + (OffSetX+x)*4 + 1];
			R = IN_RGB[(OffSetY+y)*(m_CAM_SIZE_WIDTH*4) + (OffSetX+x)*4 + 2];
			
		//	Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
		//	Sum_Y = (BYTE)((0.34900*R)+(0.58700*G)+(0.06400*B));
			int Total_RGB = (R + G + B) / 3;
			Sum_Y = Total_RGB;
			
			int deltaY = (OffSetY+y);
			int deltaX = (OffSetX+x);

			double dist = sqrt( (double)(((m_CAM_SIZE_WIDTH/2)-deltaX)*((m_CAM_SIZE_WIDTH/2)-deltaX)) + (double)((240-(m_CAM_SIZE_HEIGHT/2))*((m_CAM_SIZE_HEIGHT/2)-deltaY)) );
			
			unsigned int brightness_weight = Sum_Y + (BYTE)((Sum_Y+dist) * 0.00);
						
			if(brightness_weight > 255 || brightness_weight > m_White_Ref_Value)
				Sum_Y = m_White_Ref_Value;
            else
				Sum_Y = brightness_weight;			

			BWImage[(y)*(Width*4)+(x)*4		] = Sum_Y;
			BWImage[(y)*(Width*4)+(x)*4 + 1	] = Sum_Y;
			BWImage[(y)*(Width*4)+(x)*4 + 2	] = Sum_Y;

			if(abs(R-G) < 100 && abs(G-B) < 100 && abs(R-B) < 100)
			{
				Overlay_BWImage[(y)*(Width*4)+(x)*4		] = 255;
				Overlay_BWImage[(y)*(Width*4)+(x)*4 + 1	] = 255;
				Overlay_BWImage[(y)*(Width*4)+(x)*4 + 2	] = 255;	
			}
			else
				Overlay_BWImage[(y)*(Width*4)+(x)*4] = 0;	
		}
		R_Count++;
	}	
	
//	RD_RECT[NUM].m_CheckLine = GetCheckLine(BWImage, RD_RECT[NUM].m_bmode, Width, Height);

	if(RD_RECT[NUM].m_dir == 0)
	{
		SetStartEndPos(BWImage, NUM, RD_RECT[NUM].m_bmode, RD_RECT[NUM].m_dir, m_White_Ref_Value, m_Black_Ref_Value, Overlay_BWImage);
		
		if( m_dResol_Mode == 0 )
			RD_RECT[NUM].m_checkLine_Buffer[RD_RECT[NUM].m_checkLine_Buffer_cnt] = GetCheckLine_LR_DU(BWImage, RD_RECT[NUM].m_bmode, Width, Height);
		else
			RD_RECT[NUM].m_checkLine_Buffer[RD_RECT[NUM].m_checkLine_Buffer_cnt] = GetCheckLine_LR_DU_using_Sharpness(BWImage, RD_RECT[NUM].m_bmode, Width, Height, NUM, loop_cnt);		
		
	}
	else if(RD_RECT[NUM].m_dir == 1)
	{
		SetStartEndPos(BWImage, NUM, RD_RECT[NUM].m_bmode, RD_RECT[NUM].m_dir, m_White_Ref_Value, m_Black_Ref_Value, Overlay_BWImage);
		
		if( m_dResol_Mode == 0 )
			RD_RECT[NUM].m_checkLine_Buffer[RD_RECT[NUM].m_checkLine_Buffer_cnt] = GetCheckLine_RL_UD(BWImage, RD_RECT[NUM].m_bmode, Width, Height);
		else
			RD_RECT[NUM].m_checkLine_Buffer[RD_RECT[NUM].m_checkLine_Buffer_cnt] = GetCheckLine_RL_UD_using_Sharpness(BWImage, RD_RECT[NUM].m_bmode, Width, Height, NUM, loop_cnt);
		
	}
//	RD_RECT[NUM].m_CheckLine = GetCheckLineUsingMTF(BWImage, RD_RECT[NUM].m_bmode, Width, Height);
//	RD_RECT[NUM].m_CheckLine = GetCheckLineUsingMTFAvg(BWImage, RD_RECT[NUM].m_bmode, Width, Height);
//	RD_RECT[NUM].m_ResultData = GetResultData(BWImage, RD_RECT[NUM].m_bmode, Width, Height);

	delete []BWImage;	
	delete []Overlay_BWImage;

	return FALSE;
}

void CResolutionDetection_Option::SetStartEndPos(BYTE *BWImage, int NUM, int mode, int dir, int ref_white_value, int ref_black_value, BYTE *Overlay_BWImage)
{
	int width, height;
	unsigned int curr_pixel_v;

	width	= RD_RECT[NUM].Length();
	height	= RD_RECT[NUM].Height();
	
	BYTE *temp_BWImage = new BYTE [width * height];
	
	RD_RECT[NUM].m_dPatternStartPos.x = 0;
	RD_RECT[NUM].m_dPatternStartPos.y = 0;
	RD_RECT[NUM].m_dPatternEndPos.x = 0;
	RD_RECT[NUM].m_dPatternEndPos.y = 0;
	
	IplImage *GrayImage = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	
	for (int x=0; x<width; x++)
		for (int y=0; y<height; y++)
			GrayImage->imageData[y * GrayImage->widthStep + x] = BWImage[(y)*(width*4)+(x)*4];
	
	cvThreshold(GrayImage, GrayImage, 0, 255, CV_THRESH_OTSU);

	for (int x=0; x<width; x++)
	{
		for (int y=0; y<height; y++)
		{
			curr_pixel_v = GrayImage->imageData[y * GrayImage->widthStep + x];			

			if(curr_pixel_v > (double)(ref_white_value)/1.5)
				temp_BWImage[(y)*(width)+(x)] = 255;
			else
			{
				if(Overlay_BWImage[(y)*(width*4)+(x)*4] == 255)
					temp_BWImage[(y)*(width)+(x)] = 0;
				else
					temp_BWImage[(y)*(width)+(x)] = 255;
			}
		}
	}
	
	if(mode == 1)
	{
		bool seek_OK = FALSE;

		for (int x=0; x<width; x++)
		{
			for (int y=0; y<height; y++)
			{
				curr_pixel_v = temp_BWImage[(y)*(width)+(x)];
				
				if(curr_pixel_v == 0)
				{	
					if(dir == 0){
						RD_RECT[NUM].m_dPatternStartPos.x = x;
						RD_RECT[NUM].m_dPatternStartPos.y = y;
					}else if(dir == 1){
						RD_RECT[NUM].m_dPatternEndPos.x = x;
						RD_RECT[NUM].m_dPatternEndPos.y = y;
					}
					seek_OK = TRUE;
					break;
				}
			}
			if(seek_OK == TRUE)
				break;
		}
		
		seek_OK = FALSE;

		for (int x=width-1; x>-1; x--)
		{
			for (int y=0; y<height; y++)
			{
				curr_pixel_v = temp_BWImage[(y)*(width)+(x)];
				
				if(curr_pixel_v == 0)
				{
					if(dir == 0){
						RD_RECT[NUM].m_dPatternEndPos.x = x;
						RD_RECT[NUM].m_dPatternEndPos.y = y;
					}else if(dir == 1){
						RD_RECT[NUM].m_dPatternStartPos.x = x;
						RD_RECT[NUM].m_dPatternStartPos.y = y;
					}


					seek_OK = TRUE;
					break;
				}
			}
			if(seek_OK == TRUE)
				break;
		}
	}else if(mode == 0){
		bool seek_OK = FALSE;

		for (int y=0; y<height; y++)
		{
			for (int x=0; x<width; x++)
			{
				curr_pixel_v = temp_BWImage[(y)*(width)+(x)];
				
				if(curr_pixel_v == 0)
				{
					if(dir == 0){
						RD_RECT[NUM].m_dPatternEndPos.x = x;
						RD_RECT[NUM].m_dPatternEndPos.y = y;
					}else if(dir == 1){
						RD_RECT[NUM].m_dPatternStartPos.x = x;
						RD_RECT[NUM].m_dPatternStartPos.y = y;
					}

					seek_OK = TRUE;
					break;
				}
			}
			if(seek_OK == TRUE)
				break;
		}
		
		seek_OK = FALSE;

		for (int y=height-1; y>-1; y--)
		{
			for (int x=0; x<width; x++)
			{
				curr_pixel_v = temp_BWImage[(y)*(width)+(x)];
				
				if(curr_pixel_v == 0)
				{
					if(dir == 0){
						RD_RECT[NUM].m_dPatternStartPos.x = x;
						RD_RECT[NUM].m_dPatternStartPos.y = y;	
					}else if(dir == 1){
						RD_RECT[NUM].m_dPatternEndPos.x = x;
						RD_RECT[NUM].m_dPatternEndPos.y = y;
					}

					seek_OK = TRUE;
					break;
				}
			}
			if(seek_OK == TRUE)
				break;
		}
	}
	
	cvReleaseImage(&GrayImage);

	delete []temp_BWImage;
}

int CResolutionDetection_Option::GetCheckLine_LR_DU(BYTE *BWImage, int check_mode, int Width, int Height)
{
	BYTE *Q_BWImage = new BYTE [Width * Height * 4];
	
	int max_v=0, min_v=255, loc_index=0;
	int unsigned curr_pixel_v, next_pixel_v, direction;
	bool Detected_Flag = FALSE;
	int result_Line;

	if(check_mode == 1)	// 상->하로 체크
	{
		bool weight_line_flag = FALSE;
		unsigned int scaled_curr_pixel_v;

		for (int x=0; x<Width; x++)
		{
		//	max_v=0;
		//	min_v=255;			

			for (int y=0; y<Height; y++)
			{
				curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
				
				if(curr_pixel_v > m_White_Ref_Value)
					BWImage[(y)*(Width*4)+(x)*4] = m_White_Ref_Value;
				if(curr_pixel_v < m_Black_Ref_Value)
					BWImage[(y)*(Width*4)+(x)*4] = m_Black_Ref_Value;

			//	curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
			////	scaled_curr_pixel_v = (curr_pixel_v - m_Black_Ref_Value)/(double)(m_White_Ref_Value - m_Black_Ref_Value) * (double)(m_White_Ref_Value - m_Black_Ref_Value) + m_Black_Ref_Value;
			////	scaled_curr_pixel_v = (double)(curr_pixel_v - m_Black_Ref_Value)/(double)(m_White_Ref_Value - m_Black_Ref_Value) * m_White_Ref_Value;
			//	scaled_curr_pixel_v = (double)(curr_pixel_v - m_Black_Ref_Value)/(double)(m_White_Ref_Value - m_Black_Ref_Value) * double(m_White_Ref_Value - m_Black_Ref_Value) + m_Black_Ref_Value;
			//	BWImage[(y)*(Width*4)+(x)*4] = scaled_curr_pixel_v;
			//	BWImage[(y)*(Width*4)+(x)*4 + 1] = scaled_curr_pixel_v;
			//	BWImage[(y)*(Width*4)+(x)*4 + 2] = scaled_curr_pixel_v;
			}		
			
			int black_line_cnt=0;

			for(int j=0; j<Height; j++){
				curr_pixel_v = BWImage[(j)*(Width*4)+(x)*4];								

				if(curr_pixel_v > (m_BW_Ref_Sub_Value - m_Ref_Threshold))
				{
					Q_BWImage[(j)*(Width*4)+(x)*4] = 255;
					Q_BWImage[(j)*(Width*4)+(x)*4 + 1] = 255;
					Q_BWImage[(j)*(Width*4)+(x)*4 + 2] = 255;
				}
				else
				{
					Q_BWImage[(j)*(Width*4)+(x)*4] = 0;
					Q_BWImage[(j)*(Width*4)+(x)*4 + 1] = 0;
					Q_BWImage[(j)*(Width*4)+(x)*4 + 2] = 0;
				}
			}
			
			for(int j=1; j<Height-1; j++){

				curr_pixel_v = Q_BWImage[(j)*(Width*4)+(x)*4];

				if(curr_pixel_v == 0 && j<(Height-1) && (unsigned char)Q_BWImage[(j+1)*(Width*4)+(x)*4] == 255 && (unsigned char)Q_BWImage[(j-1)*(Width*4)+(x)*4] == 0)
						black_line_cnt++;				
			}

			if(black_line_cnt == 7)
			{
			//	RD_RECT[NUM].m_CheckLine = x;
				result_Line = x;
				weight_line_flag = TRUE;
				Detected_Flag = TRUE;
			}
			else if(weight_line_flag == TRUE)
			{
		//		break;
			}
			
			black_line_cnt = 0;				
		}
		
	//	Capture_Gray_SAVE("C:\\ASDF_Hor_Org", BWImage, Width, Height);
	//	Capture_Gray_SAVE("C:\\ASDF_Hor", Q_BWImage, Width, Height);
		
		if(Detected_Flag == FALSE)
			result_Line = 1;
	}
	else if(check_mode == 0)	// 좌->우로 체크
	{		
		bool weight_line_flag = FALSE;
		unsigned int scaled_curr_pixel_v;

	//	Capture_Gray_SAVE("C:\\DU_Image", BWImage, Width, Height);

		for (int y=Height-1; y>0; y--)
		{
			max_v=0;
			min_v=255;			

			for (int x=0; x<Width; x++)
			{
				curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
				
				if(curr_pixel_v > m_White_Ref_Value)
					BWImage[(y)*(Width*4)+(x)*4] = m_White_Ref_Value;
				if(curr_pixel_v < m_Black_Ref_Value)
					BWImage[(y)*(Width*4)+(x)*4] = m_Black_Ref_Value;

				/*curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
				scaled_curr_pixel_v = (double)(curr_pixel_v - m_Black_Ref_Value)/(double)(m_White_Ref_Value - m_Black_Ref_Value) * double(m_White_Ref_Value - m_Black_Ref_Value) + m_Black_Ref_Value;
				BWImage[(y)*(Width*4)+(x)*4] = scaled_curr_pixel_v;
				BWImage[(y)*(Width*4)+(x)*4 + 1] = scaled_curr_pixel_v;
				BWImage[(y)*(Width*4)+(x)*4 + 2] = scaled_curr_pixel_v;*/
			}		
			
			int black_line_cnt=0;			
				
			for(int j=0; j<Width; j++){

				curr_pixel_v = BWImage[(y)*(Width*4)+(j)*4];

				if(curr_pixel_v > (m_BW_Ref_Sub_Value - m_Ref_Threshold))
				{
					Q_BWImage[(y)*(Width*4)+(j)*4] = 255;
					Q_BWImage[(y)*(Width*4)+(j)*4 + 1] = 255;
					Q_BWImage[(y)*(Width*4)+(j)*4 + 2] = 255;
				}
				else
				{
					Q_BWImage[(y)*(Width*4)+(j)*4] = 0;
					Q_BWImage[(y)*(Width*4)+(j)*4 + 1] = 0;
					Q_BWImage[(y)*(Width*4)+(j)*4 + 2] = 0;
				}

			}

			for(int j=1; j<Width-1; j++){

				curr_pixel_v = Q_BWImage[(y)*(Width*4)+(j)*4];

				if(curr_pixel_v == 0 && j<(Width-1) && (unsigned char)Q_BWImage[(y)*(Width*4)+(j+1)*4] == 255 && (unsigned char)Q_BWImage[(y)*(Width*4)+(j-1)*4] == 0)
						black_line_cnt++;
			}

			if(black_line_cnt == 7)
			{
			//	RD_RECT[NUM].m_CheckLine = y;
				result_Line = y;
				weight_line_flag = TRUE;
				Detected_Flag = TRUE;
			}
			else if(weight_line_flag == TRUE)
			{
		//		break;
			}
			
			black_line_cnt = 0;
		}		

	//	Capture_Gray_SAVE("C:\\ASDF_Ver_Org", BWImage, Width, Height);
	//	Capture_Gray_SAVE("C:\\ASDF_Ver", Q_BWImage, Width, Height);

		if(Detected_Flag == FALSE)
			result_Line = Height-1;
	}

	delete []Q_BWImage;

	return result_Line;
}
int CResolutionDetection_Option::GetCheckLine_RL_UD(BYTE *BWImage, int check_mode, int Width, int Height)
{
	BYTE *Q_BWImage = new BYTE [Width * Height * 4];
	
	int max_v=0, min_v=255, loc_index=0;
	unsigned int curr_pixel_v, next_pixel_v, direction;
	bool Detected_Flag = FALSE;
	int result_Line;

	if(check_mode == 1)	// 상->하로 체크
	{
		bool weight_line_flag = FALSE;
		unsigned int scaled_curr_pixel_v;

		for (int x=Width-1; x>-1; x--)
		{
			max_v=0;
			min_v=255;			

			for (int y=0; y<Height; y++)
			{
				curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
				
				if(curr_pixel_v > m_White_Ref_Value)
					BWImage[(y)*(Width*4)+(x)*4] = m_White_Ref_Value;
				if(curr_pixel_v < m_Black_Ref_Value)
					BWImage[(y)*(Width*4)+(x)*4] = m_Black_Ref_Value;

			//	curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
			////	scaled_curr_pixel_v = (curr_pixel_v - m_Black_Ref_Value)/(double)(m_White_Ref_Value - m_Black_Ref_Value) * (double)(m_White_Ref_Value - m_Black_Ref_Value) + m_Black_Ref_Value;
			////	scaled_curr_pixel_v = (double)(curr_pixel_v - m_Black_Ref_Value)/(double)(m_White_Ref_Value - m_Black_Ref_Value) * m_White_Ref_Value;
			//	scaled_curr_pixel_v = (double)(curr_pixel_v - m_Black_Ref_Value)/(double)(m_White_Ref_Value - m_Black_Ref_Value) * double(m_White_Ref_Value - m_Black_Ref_Value) + m_Black_Ref_Value;
			//	BWImage[(y)*(Width*4)+(x)*4] = scaled_curr_pixel_v;
			//	BWImage[(y)*(Width*4)+(x)*4 + 1] = scaled_curr_pixel_v;
			//	BWImage[(y)*(Width*4)+(x)*4 + 2] = scaled_curr_pixel_v;
			}				

			int black_line_cnt=0;

			for(int j=0; j<Height; j++){
				curr_pixel_v = BWImage[(j)*(Width*4)+(x)*4];								

				if(curr_pixel_v >= (m_BW_Ref_Sub_Value - m_Ref_Threshold))
				{
					Q_BWImage[(j)*(Width*4)+(x)*4] = 255;
					Q_BWImage[(j)*(Width*4)+(x)*4 + 1] = 255;
					Q_BWImage[(j)*(Width*4)+(x)*4 + 2] = 255;
				}
				else
				{
					Q_BWImage[(j)*(Width*4)+(x)*4] = 0;
					Q_BWImage[(j)*(Width*4)+(x)*4 + 1] = 0;
					Q_BWImage[(j)*(Width*4)+(x)*4 + 2] = 0;
				}
			}

			for(int j=1; j<Height-1; j++){

				curr_pixel_v = Q_BWImage[(j)*(Width*4)+(x)*4];

				if(curr_pixel_v == 0 && j<(Height-1) && (unsigned char)Q_BWImage[(j+1)*(Width*4)+(x)*4] == 255 && (unsigned char)Q_BWImage[(j-1)*(Width*4)+(x)*4] == 0)
						black_line_cnt++;				
			}

			if(black_line_cnt == 7)
			{
			//	RD_RECT[NUM].m_CheckLine = x;
				result_Line = x;
				weight_line_flag = TRUE;
				Detected_Flag = TRUE;
			}
			else if(weight_line_flag == TRUE)
			{
		//		break;
			}
			
			black_line_cnt = 0;				
		}
		
	//	Capture_Gray_SAVE("C:\\ASDF_Hor_Org", BWImage, Width, Height);
	//	Capture_Gray_SAVE("C:\\ASDF_Hor", Q_BWImage, Width, Height);
		

		if(Detected_Flag == FALSE)
			result_Line = Width-1;
	}
	else if(check_mode == 0)	// 좌->우로 체크
	{		
		bool weight_line_flag = FALSE;
		unsigned int scaled_curr_pixel_v;

		for (int y=0; y<Height; y++)
		{
			max_v=0;
			min_v=255;			

		//	Capture_Gray_SAVE("C:\\UD_Image", BWImage, Width, Height);

			for (int x=0; x<Width; x++)
			{
				curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
				
				if(curr_pixel_v > m_White_Ref_Value)
					BWImage[(y)*(Width*4)+(x)*4] = m_White_Ref_Value;
				if(curr_pixel_v < m_Black_Ref_Value)
					BWImage[(y)*(Width*4)+(x)*4] = m_Black_Ref_Value;

			//	curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
			////	scaled_curr_pixel_v = (curr_pixel_v - m_Black_Ref_Value)/(double)(m_White_Ref_Value - m_Black_Ref_Value) * (double)(m_White_Ref_Value - m_Black_Ref_Value) + m_Black_Ref_Value;
			////	scaled_curr_pixel_v = (double)(curr_pixel_v - m_Black_Ref_Value)/(double)(m_White_Ref_Value - m_Black_Ref_Value) * m_White_Ref_Value;
			//	scaled_curr_pixel_v = (double)(curr_pixel_v - m_Black_Ref_Value)/(double)(m_White_Ref_Value - m_Black_Ref_Value) * double(m_White_Ref_Value - m_Black_Ref_Value) + m_Black_Ref_Value;
			//	BWImage[(y)*(Width*4)+(x)*4] = scaled_curr_pixel_v;
			//	BWImage[(y)*(Width*4)+(x)*4 + 1] = scaled_curr_pixel_v;
			//	BWImage[(y)*(Width*4)+(x)*4 + 2] = scaled_curr_pixel_v;
			}		
			
			int black_line_cnt=0;			
				
			for(int j=0; j<Width; j++){

				curr_pixel_v = BWImage[(y)*(Width*4)+(j)*4];

				if(curr_pixel_v >= (m_BW_Ref_Sub_Value - m_Ref_Threshold))
				{
					Q_BWImage[(y)*(Width*4)+(j)*4] = 255;
					Q_BWImage[(y)*(Width*4)+(j)*4 + 1] = 255;
					Q_BWImage[(y)*(Width*4)+(j)*4 + 2] = 255;
				}
				else
				{
					Q_BWImage[(y)*(Width*4)+(j)*4] = 0;
					Q_BWImage[(y)*(Width*4)+(j)*4 + 1] = 0;
					Q_BWImage[(y)*(Width*4)+(j)*4 + 2] = 0;
				}

			}

			for(int j=1; j<Width-1; j++){

				curr_pixel_v = Q_BWImage[(y)*(Width*4)+(j)*4];

				if(curr_pixel_v == 0 && j<(Width-1) && (unsigned char)Q_BWImage[(y)*(Width*4)+(j+1)*4] == 255 && (unsigned char)Q_BWImage[(y)*(Width*4)+(j-1)*4] == 0)
						black_line_cnt++;
			}

			if(black_line_cnt == 7)
			{
			//	RD_RECT[NUM].m_CheckLine = y;
				result_Line = y;
				weight_line_flag = TRUE;
				Detected_Flag = TRUE;
			}
			else if(weight_line_flag == TRUE)
			{
		//		break;
			}
			
			black_line_cnt = 0;
		}		

	//	Capture_Gray_SAVE("C:\\ASDF_Ver_Org", BWImage, Width, Height);
	//	Capture_Gray_SAVE("C:\\ASDF_Ver", Q_BWImage, Width, Height);

		if(Detected_Flag == FALSE)
			result_Line = 1;
	}
	
//	Capture_Gray_SAVE("C:\\DAEWOO_DEBUG\\LP_BIN_IMAGE.BMP", Q_BWImage, Width, Height);

	delete []Q_BWImage;

	return result_Line;
}


void CResolutionDetection_Option::Get_Ref_Threshold_Value(LPBYTE IN_RGB,int NUM)
{
	RD_RECT[NUM].m_Success= FALSE;
	//	if(RD_RECT[NUM].Chkdata() == FALSE){return 0;}//재대로 된 데이터가 아니면 바로 빠져나간다.

	BYTE R,G,B;
	
	int Width = RD_RECT[NUM].Length();
	int Height = RD_RECT[NUM].Height();
	int OffSetX = RD_RECT[NUM].m_Left;
	int OffSetY = RD_RECT[NUM].m_Top;
	
	BYTE *BWImage = new BYTE [Width * Height * 4];
		
	double	Sum_Y = 0;

	int Max_Y_Value=0;
	int Min_Y_Value=99999;
	
	for (int y=0; y<Height; y++){//영역으로 지정한 범위의 RGB데이터를 추출하여 밝기 영상만 추출한다. 	
		for (int x=0; x<Width; x++){//0~100	
			B = IN_RGB[(OffSetY+y)*(m_CAM_SIZE_WIDTH*4) + (OffSetX+x)*4    ];
			G = IN_RGB[(OffSetY+y)*(m_CAM_SIZE_WIDTH*4) + (OffSetX+x)*4 + 1];
			R = IN_RGB[(OffSetY+y)*(m_CAM_SIZE_WIDTH*4) + (OffSetX+x)*4 + 2];
			
		//	Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));

			int Total_RGB = (R + G + B) / 3;
			Sum_Y = Total_RGB;

			BWImage[(y)*(Width*4)+(x)*4		] = Sum_Y;
			BWImage[(y)*(Width*4)+(x)*4 + 1	] = Sum_Y;
			BWImage[(y)*(Width*4)+(x)*4 + 2	] = Sum_Y;
		}
	}
	
	//for (int y=160; y<320; y++){//영역으로 지정한 범위의 RGB데이터를 추출하여 밝기 영상만 추출한다. 	
	//	for (int x=270; x<450; x++){//0~100	
	for (int y=(m_CAM_SIZE_HEIGHT*0.33); y<(m_CAM_SIZE_HEIGHT*0.66); y++){//영역으로 지정한 범위의 RGB데이터를 추출하여 밝기 영상만 추출한다. 	
		for (int x=(m_CAM_SIZE_WIDTH*0.375); x<(m_CAM_SIZE_WIDTH*0.9375); x++){//0~100	
			B = IN_RGB[(y)*(m_CAM_SIZE_WIDTH*4) + (x)*4    ];
			G = IN_RGB[(y)*(m_CAM_SIZE_WIDTH*4) + (x)*4 + 1];
			R = IN_RGB[(y)*(m_CAM_SIZE_WIDTH*4) + (x)*4 + 2];
			
		//	Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));

			int Total_RGB = (R + G + B) / 3;
			Sum_Y = Total_RGB;
			
			if(Sum_Y > Max_Y_Value)
				Max_Y_Value = Sum_Y;

			if(Sum_Y < Min_Y_Value)
				Min_Y_Value = Sum_Y;
		}
	}
	
	unsigned int Total_Y=0, Avg_Y=0;
	
	int overlayRefCnt = 0;

	if(NUM == 1)
	{
		for (int y=0; y<Height; y++)
		{
			for (int x=0; x<Width; x++)
			{
			//	if(m_OverlayArea[x][y] == 1)
				{
					Total_Y += BWImage[(y)*(Width*4)+(x)*4];
					overlayRefCnt++;
				}				
			}
		}

		if(RD_RECT[1].ENABLE)
		{
			m_White_Ref_Value = Total_Y / overlayRefCnt;
		}
	}
	else if(NUM == 0)
	{
		overlayRefCnt = 0;

		for (int y=0; y<Height; y++)
		{
			for (int x=0; x<Width; x++)
			{
			//	if(m_OverlayArea[x][y] == 1)
				{
					Total_Y += BWImage[(y)*(Width*4)+(x)*4];
					overlayRefCnt++;
				}
			}
		}

		if(RD_RECT[0].ENABLE)
		{
			m_Black_Ref_Value = Total_Y / overlayRefCnt;
		}
	}
	
	
	if(!RD_RECT[1].ENABLE)
		m_White_Ref_Value = Max_Y_Value;

	if(!RD_RECT[0].ENABLE)
		m_Black_Ref_Value = Min_Y_Value;
		
	m_BW_Ref_Sub_Value = m_White_Ref_Value - m_Black_Ref_Value;
//	m_Ref_Threshold = (double)(m_White_Ref_Value - m_Black_Ref_Value) * (double)(RD_RECT[NUM].MTFRatio/100.0);
//	m_Ref_Threshold = (double)(m_White_Ref_Value - m_Black_Ref_Value) * (double)(0.05);

	delete []BWImage;	
}

bool CResolutionDetection_Option::Capture_Gray_SAVE(CString FilePath, unsigned char *ImageBuffer, int Width, int Height)
{
	//TCHAR			lpszBuf[256];
	//CString			cstr;
	//CFileFind		folderfind;

	//GeneralFile		tGFF;
	//CString strLogFile = FilePath + (".BMP");
	//tGFF.FileNameSet(strLogFile);
	//if (tGFF.Begin(GeneralFileFormat::GF_Write) == false)
	//{	
	//	return FALSE;
	//}
	//BITMAPINFOHEADER	m_BITM;
	////--------------------
	//m_BITM.biSize		    = sizeof(BITMAPINFOHEADER);             // 비트맵 헤더크기
	//m_BITM.biWidth		    = Width;                           // 비트맵의 가로 크기
	//m_BITM.biHeight 		= Height;                          // 비트맵의 세로 크기
	//m_BITM.biPlanes 		= 1;                                    // Plane 수 (1로 설정)
	//m_BITM.biBitCount		= 32;                                   // 한 픽셀당 비트수
	//m_BITM.biCompression	= 0;                        // 압축 유무
	//m_BITM.biSizeImage		= Width * Height * 4;          // 그림 데이터 크기
	//m_BITM.biXPelsPerMeter	= 0;                                    // 한 픽셀당 가로 미터
	//m_BITM.biYPelsPerMeter	= 0;                                    // 한 픽셀당 세로 미터
	//m_BITM.biClrUsed		= 0;                                    // 그림에서 실제 사용되는 컬러수
	//m_BITM.biClrImportant	= 0;                                    // 중요하게 사용되는 컬러
	////--------------------
	//BITMAPFILEHEADER	bfh;
	//bfh.bfType		= 0x4D42;	
	//bfh.bfSize	    = m_BITM.biSizeImage + m_BITM.biSize + sizeof(BITMAPFILEHEADER);
	//bfh.bfReserved1 = 0;
	//bfh.bfReserved2 = 0;
	//bfh.bfOffBits	= m_BITM.biSize + sizeof(BITMAPFILEHEADER);

	//tGFF.Run( (LPBYTE)&bfh,	   sizeof(BITMAPFILEHEADER));
	//tGFF.Run( (LPBYTE)&m_BITM,     sizeof(BITMAPINFOHEADER));
	//tGFF.Run( (LPBYTE)ImageBuffer,m_BITM.biSizeImage);
	//
	//tGFF.End();
	return  TRUE;
}

bool CResolutionDetection_Option::Capture_SAVE(CString FilePath, unsigned char *ImageBuffer)
{
	//TCHAR			lpszBuf[256];
	//CString			cstr;
	//CFileFind		folderfind;

	//GeneralFile		tGFF;
	//CString strLogFile = FilePath + (".BMP");
	//tGFF.FileNameSet(strLogFile);
	//if (tGFF.Begin(GeneralFileFormat::GF_Write) == false)
	//{	
	//	return FALSE;
	//}
	//BITMAPINFOHEADER	m_BITM;
	////--------------------
	//m_BITM.biSize		    = sizeof(BITMAPINFOHEADER);             // 비트맵 헤더크기
	//m_BITM.biWidth		    = 720;                           // 비트맵의 가로 크기
	//m_BITM.biHeight 		= 480;                          // 비트맵의 세로 크기
	//m_BITM.biPlanes 		= 1;                                    // Plane 수 (1로 설정)
	//m_BITM.biBitCount		= 32;                                   // 한 픽셀당 비트수
	//m_BITM.biCompression	= 0;                        // 압축 유무
	//m_BITM.biSizeImage		= 720 * 480 * 4;          // 그림 데이터 크기
	//m_BITM.biXPelsPerMeter	= 0;                                    // 한 픽셀당 가로 미터
	//m_BITM.biYPelsPerMeter	= 0;                                    // 한 픽셀당 세로 미터
	//m_BITM.biClrUsed		= 0;                                    // 그림에서 실제 사용되는 컬러수
	//m_BITM.biClrImportant	= 0;                                    // 중요하게 사용되는 컬러
	////--------------------
	//BITMAPFILEHEADER	bfh;
	//bfh.bfType		= 0x4D42;	
	//bfh.bfSize	    = m_BITM.biSizeImage + m_BITM.biSize + sizeof(BITMAPFILEHEADER);
	//bfh.bfReserved1 = 0;
	//bfh.bfReserved2 = 0;
	//bfh.bfOffBits	= m_BITM.biSize + sizeof(BITMAPFILEHEADER);

	//tGFF.Run( (LPBYTE)&bfh,	   sizeof(BITMAPFILEHEADER));
	//tGFF.Run( (LPBYTE)&m_BITM,     sizeof(BITMAPINFOHEADER));
	//tGFF.Run( (LPBYTE)ImageBuffer,m_BITM.biSizeImage);
	//
	//tGFF.End();
	return  TRUE;
}
//void CResolutionDetection_Option::OnTimer(UINT_PTR nIDEvent)
//{
//	BYTE RGBArray[5][720 * 480 * 4];
//	BYTE **RGBArray;
//	
//	RGBArray = new BYTE *[RD_FRAME_NUM];
//
//	for(int i=0; i<RD_FRAME_NUM; i++)
//		RGBArray[i] = new BYTE [720 * 480 * 4];
//
//	switch(nIDEvent)
//	{
//		case 130 :
//			
//			((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan =1;						
//			
//			for(int i =0;i<10;i++){
//				if(((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan == 0){
//					break;
//				}else{
//					DoEvents(50);
//				}
//			}
//
//			int count = 0;
//			int avg_check_line=0;
//			
//			for(int lop=0; lop<2;lop++)
//			{
//				Get_Ref_Threshold_Value(m_RGBScanbuf,lop);
//			}
//			
//			m_Avg_BW_Array			= new BYTE [720 * 480 * 4];
//			m_dTotal_Sum_Y_Image	= new unsigned int [720 * 480 * 4];
//
//			for(int lop=2; lop<12;lop++)
//			{
//			//	memset(m_dTotal_Sum_Y_Image, 0, sizeof(m_dTotal_Sum_Y_Image));
//
//				for (int y=0; y<480; y++)//동적배열 초기화 부분 - 이상하게 memset이 먹히지 않는다...
//				{
//					for (int x=0; x<720; x++)
//					{//0~100	
//						m_dTotal_Sum_Y_Image[(y)*2880 + (x)*4    ] = 0;
//						m_dTotal_Sum_Y_Image[(y)*2880 + (x)*4 + 1] = 0;	
//						m_dTotal_Sum_Y_Image[(y)*2880 + (x)*4 + 2] = 0;
//					}
//				}
//			
//				CheckResolution(m_RGBScanbuf,lop);
//
//				RD_RECT[lop].m_checkLine_Buffer_cnt++;
//
//				if(RD_RECT[lop].m_checkLine_Buffer_cnt == 10)
//				{
//					for(int k=0;k <9; k++)
//						RD_RECT[lop].m_checkLine_Buffer[k] = RD_RECT[lop].m_checkLine_Buffer[k+1];
//
//					RD_RECT[lop].m_checkLine_Buffer_cnt = 9;
//				}
//								
//				
//				for(int k=0; k<RD_RECT[lop].m_checkLine_Buffer_cnt; k++)
//					avg_check_line += RD_RECT[lop].m_checkLine_Buffer[k];
//
//				avg_check_line = avg_check_line / RD_RECT[lop].m_checkLine_Buffer_cnt;
//				
//				RD_RECT[lop].m_CheckLine = avg_check_line;			
//			}
//		break;
//	}
//	
//	delete []m_dTotal_Sum_Y_Image;
//	delete []m_Avg_BW_Array;	
//	CDialog::OnTimer(nIDEvent);
//}

void CResolutionDetection_Option::OnDestroy()
{		
	for(int NUM=2; NUM<14; NUM++)
	{
		if(RD_RECT[NUM].SFR_MTF_Value != NULL)
		{
			delete []RD_RECT[NUM].SFR_MTF_Value;
			delete []RD_RECT[NUM].SFR_MTF_LineFitting_Value;
			delete []RD_RECT[NUM].SFR_BWRatio;

			RD_RECT[NUM].SFR_MTF_Value = NULL;
		}
	}

	CDialog::OnDestroy();	
}

void CResolutionDetection_Option::MakeAvgBWImageArray(LPBYTE IN_RGB, int Width, int Height, int count)
{
	BYTE	R,G,B;
	double	Sum_Y = 0;
	
	for (int y=0; y<Height; y++){//영역으로 지정한 범위의 RGB데이터를 추출하여 밝기 영상만 추출한다. 	
		for (int x=0; x<Width; x++){//0~100	
			B = IN_RGB[(y)*(m_CAM_SIZE_WIDTH*4) + (x)*4    ];
			G = IN_RGB[(y)*(m_CAM_SIZE_WIDTH*4) + (x)*4 + 1];
			R = IN_RGB[(y)*(m_CAM_SIZE_WIDTH*4) + (x)*4 + 2];
			
			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));

			m_dTotal_Sum_Y_Image[(y)*(m_CAM_SIZE_WIDTH*4) + (x)*4    ] += Sum_Y;			
			m_dTotal_Sum_Y_Image[(y)*(m_CAM_SIZE_WIDTH*4) + (x)*4 + 1] += Sum_Y;			
			m_dTotal_Sum_Y_Image[(y)*(m_CAM_SIZE_WIDTH*4) + (x)*4 + 2] += Sum_Y;			
		}
	}

	if(count == RD_FRAME_NUM - 1)
	{
		for (int y=0; y<Height; y++){//영역으로 지정한 범위의 RGB데이터를 추출하여 밝기 영상만 추출한다. 	
			for (int x=0; x<Width; x++){//0~100	
				int kkk = m_dTotal_Sum_Y_Image[(y)*(Width*4)+(x)*4    ] / RD_FRAME_NUM;
				m_Avg_BW_Array[(y)*(Width*4)+(x)*4		] = m_dTotal_Sum_Y_Image[(y)*(Width*4)+(x)*4    ] / RD_FRAME_NUM;
				m_Avg_BW_Array[(y)*(Width*4)+(x)*4 + 1	] = m_dTotal_Sum_Y_Image[(y)*(Width*4)+(x)*4 + 1] / RD_FRAME_NUM;
				m_Avg_BW_Array[(y)*(Width*4)+(x)*4 + 2	] = m_dTotal_Sum_Y_Image[(y)*(Width*4)+(x)*4 + 2] / RD_FRAME_NUM;
			}
		}
	}
}

int CResolutionDetection_Option::GetCheckLineUsingMTF(BYTE *BWImage, int check_mode, int Width, int Height)
{
	BYTE *Q_BWImage = new BYTE [Width * Height * 4];
	
	int max_v=0, min_v=255, loc_index=0;
	int unsigned curr_pixel_v, next_pixel_v, prev_pixel_v, direction;
	bool Detected_Flag = FALSE;
	int result_Line;

	if(check_mode == 1)	// 상->하로 체크
	{
		bool weight_line_flag = FALSE;

		for (int x=0; x<Width; x++)
		{
			max_v=0;
			min_v=255;			

			for (int y=0; y<Height-1; y++)
			{				
				curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
				next_pixel_v = BWImage[(y+1)*(Width*4)+(x)*4];

				if(max_v < curr_pixel_v)
					max_v = curr_pixel_v;

				if(min_v > curr_pixel_v)
					min_v = curr_pixel_v;				
			}			
			
			int black_line_cnt=0;

			for(int j=0; j<Height; j++){
				curr_pixel_v = BWImage[(j)*(Width*4)+(x)*4];				
				
				if( max_v > (min_v * 2) )
				{
					if( (max_v - curr_pixel_v) < (curr_pixel_v - min_v) )
					{
						Q_BWImage[(j)*(Width*4)+(x)*4] = 255;
						Q_BWImage[(j)*(Width*4)+(x)*4 + 1] = 255;
						Q_BWImage[(j)*(Width*4)+(x)*4 + 2] = 255;
					}
					else
					{
						Q_BWImage[(j)*(Width*4)+(x)*4] = 0;
						Q_BWImage[(j)*(Width*4)+(x)*4 + 1] = 0;
						Q_BWImage[(j)*(Width*4)+(x)*4 + 2] = 0;
					}
				}
				else
				{
					Q_BWImage[(j)*(Width*4)+(x)*4] = 255;
					Q_BWImage[(j)*(Width*4)+(x)*4 + 1] = 255;
					Q_BWImage[(j)*(Width*4)+(x)*4 + 2] = 255;
				}
			}

			for(int j=0; j<Height; j++){

				curr_pixel_v = Q_BWImage[(j)*(Width*4)+(x)*4];

				if(curr_pixel_v == 0 && j<(Height-1) && (unsigned char)Q_BWImage[(j+1)*(Width*4)+(x)*4] == 255)
						black_line_cnt++;				
			}

			if(black_line_cnt == 7)
			{
			//	RD_RECT[NUM].m_CheckLine = x;
				result_Line = x;
				weight_line_flag = TRUE;
				Detected_Flag = TRUE;
			}
			else if(weight_line_flag == TRUE)
			{
				break;
			}			
			black_line_cnt = 0;				
		}
		
		//Capture_Gray_SAVE("C:\\ASDF_Hor_Org", BWImage, Width, Height);
		Capture_Gray_SAVE("C:\\ASDF_Hor", Q_BWImage, Width, Height);
		
		if(Detected_Flag == FALSE)
			result_Line = 1;
	}
	else if(check_mode == 0)	// 좌->우로 체크
	{		
		bool weight_line_flag = FALSE;
	
		for (int y=Height-1; y>-1; y--)
		{
			max_v=0;
			min_v=255;			

			for (int x=0; x<Width; x++)
			{
				curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
			//	next_pixel_v = BWImage[(y)*(Width*4)+(x+1)*4];
				
				if(max_v < curr_pixel_v)
					max_v = curr_pixel_v;
				if(min_v > curr_pixel_v)
					min_v = curr_pixel_v;
			}					
			
			int black_line_cnt=0;

			for(int j=0; j<Width; j++){

				curr_pixel_v = BWImage[(y)*(Width*4)+(j)*4];
				
				if( max_v > (min_v * 2) )
				{
					if( (max_v - curr_pixel_v) < (curr_pixel_v - min_v) )
					{
						Q_BWImage[(y)*(Width*4)+(j)*4] = 255;
						Q_BWImage[(y)*(Width*4)+(j)*4 + 1] = 255;
						Q_BWImage[(y)*(Width*4)+(j)*4 + 2] = 255;
					}
					else
					{
						Q_BWImage[(y)*(Width*4)+(j)*4] = 0;
						Q_BWImage[(y)*(Width*4)+(j)*4 + 1] = 0;
						Q_BWImage[(y)*(Width*4)+(j)*4 + 2] = 0;				
					}
				}
				else
				{
					Q_BWImage[(y)*(Width*4)+(j)*4] = 255;
					Q_BWImage[(y)*(Width*4)+(j)*4 + 1] = 255;
					Q_BWImage[(y)*(Width*4)+(j)*4 + 2] = 255;
				}

			}

			for(int j=0; j<Width; j++){

				curr_pixel_v = Q_BWImage[(y)*(Width*4)+(j)*4];

				if(curr_pixel_v == 0 && j<(Width-1) && (unsigned char)Q_BWImage[(y)*(Width*4)+(j+1)*4] == 255)
						black_line_cnt++;
			}
			
			if(black_line_cnt == 7)
			{
				int local_period_flag = 0, period_cnt=0;
				int local_min=255, local_max=0;
				int MTF_array[7];

				for(int j=1; j<Width-1; j++){
					curr_pixel_v = Q_BWImage[(y)*(Width*4)+(j)*4];
					prev_pixel_v = Q_BWImage[(y)*(Width*4)+(j-1)*4];
					next_pixel_v = Q_BWImage[(y)*(Width*4)+(j+1)*4];

					if(curr_pixel_v == 0 && prev_pixel_v == 255 && local_period_flag == 0)
					{
						local_period_flag = 1;
					}
					else if(curr_pixel_v == 255 && next_pixel_v == 0 && local_period_flag == 1)
					{
						local_period_flag = 0;

						MTF_array[period_cnt] = local_max - local_min;
						
						local_min = 255;
						local_max = 0;

						period_cnt++;
					}

					if(local_period_flag > 0)
					{
						if(local_min > BWImage[(y)*(Width*4)+(j)*4])
							local_min = BWImage[(y)*(Width*4)+(j)*4];
						if(local_max < BWImage[(y)*(Width*4)+(j)*4])
							local_max = BWImage[(y)*(Width*4)+(j)*4];
					}
				}				

				MTF_array[6] = local_max - local_min;
				
				int MTF_cnt=0;
				
				for(int k=0; k<7; k++)
				{
					if(MTF_array[k] >= (m_BW_Ref_Sub_Value - m_Ref_Threshold))
						MTF_cnt++;
				}

				if(MTF_cnt == 7)
				{
				//	RD_RECT[NUM].m_CheckLine = y;
					result_Line = y;
					weight_line_flag = TRUE;
					Detected_Flag = TRUE;
				}
				else if(weight_line_flag == TRUE)
				{
					break;
				}			
				black_line_cnt = 0;	
			}
		}		

		Capture_Gray_SAVE("C:\\ASDF_Ver_Org", BWImage, Width, Height);
		Capture_Gray_SAVE("C:\\ASDF_Ver", Q_BWImage, Width, Height);

		if(Detected_Flag == FALSE)
			result_Line = Height;
	}
	
	Capture_Gray_SAVE("C:\\MTF_Thres_Image", Q_BWImage, Width, Height);

	delete []Q_BWImage;

	return result_Line;
}

int CResolutionDetection_Option::GetCheckLineUsingMTFAvg(BYTE *BWImage, int check_mode, int Width, int Height)
{
	BYTE *Q_BWImage = new BYTE [Width * Height * 4];
	
	int max_v=0, min_v=255, loc_index=0;
	int unsigned curr_pixel_v, next_pixel_v, prev_pixel_v;
	bool Detected_Flag = FALSE;
	int result_Line;

	if(check_mode == 1)	// 상->하로 체크
	{
		bool weight_line_flag = FALSE;

		for (int x=0; x<Width; x++)
		{
			max_v=0;
			min_v=255;			

			for (int y=0; y<Height-1; y++)
			{				
				curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
				next_pixel_v = BWImage[(y+1)*(Width*4)+(x)*4];

				if(max_v < curr_pixel_v)
					max_v = curr_pixel_v;

				if(min_v > curr_pixel_v)
					min_v = curr_pixel_v;				
			}			
			
			int black_line_cnt=0;

			for(int j=0; j<Height; j++){
				curr_pixel_v = BWImage[(j)*(Width*4)+(x)*4];				
				
				if( max_v > (min_v * 2) )
				{
					if( (max_v - curr_pixel_v) < (curr_pixel_v - min_v) )
					{
						Q_BWImage[(j)*(Width*4)+(x)*4] = 255;
						Q_BWImage[(j)*(Width*4)+(x)*4 + 1] = 255;
						Q_BWImage[(j)*(Width*4)+(x)*4 + 2] = 255;
					}
					else
					{
						Q_BWImage[(j)*(Width*4)+(x)*4] = 0;
						Q_BWImage[(j)*(Width*4)+(x)*4 + 1] = 0;
						Q_BWImage[(j)*(Width*4)+(x)*4 + 2] = 0;
					}
				}
				else
				{
					Q_BWImage[(j)*(Width*4)+(x)*4] = 255;
					Q_BWImage[(j)*(Width*4)+(x)*4 + 1] = 255;
					Q_BWImage[(j)*(Width*4)+(x)*4 + 2] = 255;
				}
			}

			for(int j=0; j<Height; j++){

				curr_pixel_v = Q_BWImage[(j)*(Width*4)+(x)*4];

				if(curr_pixel_v == 0 && j<(Height-1) && (unsigned char)Q_BWImage[(j+1)*(Width*4)+(x)*4] == 255)
						black_line_cnt++;				
			}

			if(black_line_cnt == 7)
			{
			//	RD_RECT[NUM].m_CheckLine = x;
				result_Line = x;
				weight_line_flag = TRUE;
				Detected_Flag = TRUE;
			}
			else if(weight_line_flag == TRUE)
			{
				break;
			}			
			black_line_cnt = 0;				
		}
		
		//Capture_Gray_SAVE("C:\\ASDF_Hor_Org", BWImage, Width, Height);
		Capture_Gray_SAVE("C:\\ASDF_Hor", Q_BWImage, Width, Height);
		
		if(Detected_Flag == FALSE)
			result_Line = 1;
	}
	else if(check_mode == 0)	// 좌->우로 체크
	{		
		bool weight_line_flag = FALSE;
		bool end_valley = FALSE;	

		for (int y=Height-1; y>-1; y--)
		{
			max_v=0;
			min_v=255;			

			for (int x=0; x<Width; x++)
			{
				curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
			//	next_pixel_v = BWImage[(y)*(Width*4)+(x+1)*4];
				
				if(max_v < curr_pixel_v)
					max_v = curr_pixel_v;
				if(min_v > curr_pixel_v)
					min_v = curr_pixel_v;
			}					
			
			int black_line_cnt=0;

			for(int j=0; j<Width-1; j++){

				curr_pixel_v = BWImage[(y)*(Width*4)+(j)*4];
				
				if( max_v > (min_v * 2) )
				{
					if( (max_v - curr_pixel_v) < (curr_pixel_v - min_v) )
					{
						Q_BWImage[(y)*(Width*4)+(j)*4] = 255;
						Q_BWImage[(y)*(Width*4)+(j)*4 + 1] = 255;
						Q_BWImage[(y)*(Width*4)+(j)*4 + 2] = 255;
					}
					else
					{
						Q_BWImage[(y)*(Width*4)+(j)*4] = 0;
						Q_BWImage[(y)*(Width*4)+(j)*4 + 1] = 0;
						Q_BWImage[(y)*(Width*4)+(j)*4 + 2] = 0;				
					}
				}
				else
				{
					Q_BWImage[(y)*(Width*4)+(j)*4] = 255;
					Q_BWImage[(y)*(Width*4)+(j)*4 + 1] = 255;
					Q_BWImage[(y)*(Width*4)+(j)*4 + 2] = 255;
				}

			}

			for(int j=0; j<Width; j++){

				curr_pixel_v = Q_BWImage[(y)*(Width*4)+(j)*4];

				if(curr_pixel_v == 0 && j<(Width-1) && (unsigned char)Q_BWImage[(y)*(Width*4)+(j+1)*4] == 255)
						black_line_cnt++;
			}
			
			if(black_line_cnt == 7)
			{
				int local_period_flag = 0, period_cnt=0;
				int local_min_sum=0, local_max_sum=0;
				int MTF_array[7], local_min_cnt=0, local_max_cnt=0;

				for(int j=1; j<Width; j++){
					curr_pixel_v = Q_BWImage[(y)*(Width*4)+(j)*4];
					prev_pixel_v = Q_BWImage[(y)*(Width*4)+(j-1)*4];
					next_pixel_v = Q_BWImage[(y)*(Width*4)+(j+1)*4];

					if(curr_pixel_v == 0 && prev_pixel_v == 255 && local_period_flag == 0)
					{
						local_period_flag = 1;
					}
					else if(curr_pixel_v == 255 && next_pixel_v == 0 && local_period_flag == 1)
					{
					//	local_period_flag = 0;
						end_valley = TRUE;
					}

					if(local_period_flag > 0)
					{
						if(Q_BWImage[(y)*(Width*4)+(j)*4] == 0)
						{
							if(BWImage[(y)*(Width*4)+(j)*4] < m_Black_Ref_Value)
								local_min_sum += m_Black_Ref_Value;
							else
								local_min_sum += BWImage[(y)*(Width*4)+(j)*4];

							local_min_cnt++;
						}
						if(Q_BWImage[(y)*(Width*4)+(j)*4] == 255)
						{
							if(BWImage[(y)*(Width*4)+(j)*4] > m_White_Ref_Value)
								local_max_sum += m_White_Ref_Value;
							else
								local_max_sum += BWImage[(y)*(Width*4)+(j)*4];

							local_max_cnt++;
						}

						if(end_valley == TRUE)
							local_period_flag = 0;
					}

					if(end_valley == TRUE)
					{
						MTF_array[period_cnt] = (local_max_sum/local_max_cnt) - (local_min_sum/local_min_cnt);
					
						local_max_sum = 0;
						local_max_cnt = 0;
						local_min_sum = 0;
						local_min_cnt = 0;

						period_cnt++;

						end_valley = FALSE;
					}
				}				

				MTF_array[6] = (local_max_sum/local_max_cnt) - (local_min_sum/local_min_cnt);
				
				int MTF_cnt=0;
				
				for(int k=0; k<7; k++)
				{
					if(MTF_array[k] >= (m_BW_Ref_Sub_Value - m_Ref_Threshold))
						MTF_cnt++;
				}

				if(MTF_cnt == 7)
				{
				//	RD_RECT[NUM].m_CheckLine = y;
					result_Line = y;
					weight_line_flag = TRUE;
					Detected_Flag = TRUE;
				}
				else if(weight_line_flag == TRUE)
				{
					break;
				}			
				black_line_cnt = 0;	
			}
		}		

		Capture_Gray_SAVE("C:\\ASDF_Ver_Org", BWImage, Width, Height);
		Capture_Gray_SAVE("C:\\ASDF_Ver", Q_BWImage, Width, Height);

		if(Detected_Flag == FALSE)
			result_Line = Height;
	}
	
	Capture_Gray_SAVE("C:\\MTF_Thres_Image", Q_BWImage, Width, Height);

	delete []Q_BWImage;

	return result_Line;
}

void CResolutionDetection_Option::OnNMClickListRdData(NMHDR *pNMHDR, LRESULT *pResult)
{

	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	*pResult = 0;
	
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		return;
	}

	iSavedItem = pNMITEM->iItem;
	iSavedSubitem = pNMITEM->iSubItem;

	if(iSavedSubitem == 0){
		ChangeCheck=1;
		SetTimer(150,5,NULL); //InitProgram을 불러들이는데 쓰인다. 
		List_COLOR_Change();
		UpdateData(FALSE);
	}
	

	

	/*
	CString str;
    GetDlgItemText(IDC_EDIT_RD_MOD, str);
	
	int k = atoi(str);
	if(k > -1 && k < 1281)
		m_ctrlRDDataList.SetItemText(iSavedItem, iSavedSubitem, str);	

	GetDlgItem(IDC_EDIT_RD_MOD)->SetWindowPos(NULL, 0, 0, 0, 0, SWP_SHOWWINDOW );
	
	for(int i=0; i< 14; i++)
	{
		int center_x = atoi(m_ctrlRDDataList.GetItemText(i, 1));
		int center_y = atoi(m_ctrlRDDataList.GetItemText(i, 2));	
		int large_w = atoi(m_ctrlRDDataList.GetItemText(i, 7));
		int large_h = atoi(m_ctrlRDDataList.GetItemText(i, 8));
		int mode = atoi(m_ctrlRDDataList.GetItemText(i, 9));
		int dir = atoi(m_ctrlRDDataList.GetItemText(i, 10));

		RD_RECT[i].EX_RESOLUTION_SET(center_x,center_y,large_w,large_h,mode,dir); //왼쪽 
	}
	
	Uploadlist();//SetLoadParameterRDZone();

	UpdateData(FALSE);
	*/
}
/*
CString CResolutionDetection_Option::GetItemText(HWND hWnd, int nItem, int nSubItem) const
{
	LVITEM lvi;
	memset(&lvi, 0, sizeof(LVITEM));
	lvi.iSubItem = nSubItem;
	CString str;
	int nLen = 128;
	int nRes;
	do
	{
		nLen *= 2;
		lvi.cchTextMax = nLen;
		lvi.pszText = str.GetBufferSetLength(nLen);
		nRes  = (int)::SendMessage(hWnd, LVM_GETITEMTEXT, (WPARAM)nItem,(LPARAM)&lvi);
	} while (nRes == nLen-1);
	str.ReleaseBuffer();
	return str;
}
*/
void CResolutionDetection_Option::OnNMDblclkListRdData(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		return;
	}

	ChangeCheck=1;

	iSavedItem = pNMITEM->iItem;
	iSavedSubitem = pNMITEM->iSubItem;

	if(pNMITEM->iItem != -1)
	{
		if(pNMITEM->iSubItem != 0)
		{	
			CRect rect;

			m_ctrlRDDataList.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			m_ctrlRDDataList.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(m_ctrlRDDataList.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
			((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetFocus();			
		}else{
			SetTimer(160,5,NULL); //InitProgram을 불러들이는데 쓰인다.
		}
	}
	List_COLOR_Change();
	UpdateData(FALSE);
	
	*pResult = 0;
}

void CResolutionDetection_Option::OnNMDblclkGroupListRdData(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		return;
	}

	ChangeCheck = 1;

	iSavedItem = pNMITEM->iItem;
	iSavedSubitem = pNMITEM->iSubItem;

	if (pNMITEM->iItem != -1)
	{
		if (pNMITEM->iSubItem != 0)
		{
			CRect rect;

			m_ctrlRDDataList.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			m_ctrlRDDataList.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_GROUP_SPEC))->SetWindowText(m_ctrlRDDataList.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_GROUP_SPEC))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW);
			((CEdit *)GetDlgItem(IDC_EDIT_GROUP_SPEC))->SetFocus();
		}
		else{
			SetTimer(160, 5, NULL); //InitProgram을 불러들이는데 쓰인다.
		}
	}
	List_COLOR_Change();
	UpdateData(FALSE);

	*pResult = 0;
}

void CResolutionDetection_Option::OnEnKillfocusEditRdMod()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	ChangeCheck=1;
	if((iSavedItem != -1)&&(iSavedSubitem != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_RD_MOD, str));
//		List_COLOR_Change();
		if(m_ctrlRDDataList.GetItemText(iSavedItem, iSavedSubitem) != str){
			Change_DATA();
			Uploadlist();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );
	iSavedItem = -1;
	iSavedSubitem = -1;
	((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
}
void CResolutionDetection_Option::List_COLOR_Change(){
	bool FLAG = TRUE;
		int Check=0;
		for(int t=0;t<14; t++){
			if(ChangeItem[t]==0)Check++;
			if(ChangeItem[t]==1)Check++;
			if(ChangeItem[t]==2)Check++;
			if(ChangeItem[t]==3)Check++;
			if(ChangeItem[t]==4)Check++;
			if(ChangeItem[t]==5)Check++;
			if(ChangeItem[t]==6)Check++;
			if(ChangeItem[t]==7)Check++;
			if(ChangeItem[t]==8)Check++;
			if(ChangeItem[t]==9)Check++;
			if(ChangeItem[t]==10)Check++;
			if(ChangeItem[t]==11)Check++;
			if(ChangeItem[t]==12)Check++;
			if(ChangeItem[t]==13)Check++;
		}
		if(Check !=14){
			FLAG =FALSE;
			ChangeItem[changecount]=iSavedItem;
		}

		if(!FLAG){
			for(int t=0; t<changecount+1; t++){
				for(int k=0; k<changecount+1; k++){
					
					if(ChangeItem[t] == ChangeItem[k]){
						if(t==k){break;	}			
						ChangeItem[k] =-1;
					}
				
				
				}
			}
					//----------------데이터 정렬
			int temp=0;

			for(int i=0; i<14; i++)
			{
				for(int j=i+1; j<14; j++)
				{
					if(ChangeItem[i] < ChangeItem[j])  // 내림차순
					{
						temp = ChangeItem[i];
						ChangeItem[i] = ChangeItem[j];
						ChangeItem[j] = temp;
					}
				}
			}
			//--------------------------------
			for(int t=0; t<14; t++){

				if(ChangeItem[t] ==-1){
					changecount =t;
					break;
				}
			}
		}

		for(int t=0; t<14; t++){
			m_ctrlRDDataList.Update(t);
		}
}

void CResolutionDetection_Option::Change_DATA(){

		CString str;
		
		((CEdit *)GetDlgItemText(IDC_EDIT_RD_MOD, str));
		m_ctrlRDDataList.SetItemText(iSavedItem, iSavedSubitem, str);

		if(iSavedSubitem == 11){
			double d_num = atof(str);
			if(d_num < 0){
				d_num = 0;
			}else if(d_num >99){
				d_num = 99;
			}
			RD_RECT[iSavedItem].MTFRatio = d_num;
		}else{
			int num = _ttoi(str);

			if(num < 0){
				num =0;
			}

			if(iSavedSubitem ==1){
				RD_RECT[iSavedItem].m_PosX = num;
				
			}

			if(iSavedSubitem ==2){
				RD_RECT[iSavedItem].m_PosY = num;
				
			}
			if(iSavedSubitem ==3){
				RD_RECT[iSavedItem].m_Left = num;
				RD_RECT[iSavedItem].m_PosX=(RD_RECT[iSavedItem].m_Right+1 +RD_RECT[iSavedItem].m_Left) /2;
				RD_RECT[iSavedItem].m_Width=RD_RECT[iSavedItem].m_Right+1 -RD_RECT[iSavedItem].m_Left;

			}
			if(iSavedSubitem ==4){
				RD_RECT[iSavedItem].m_Top = num;
				RD_RECT[iSavedItem].m_PosY=(RD_RECT[iSavedItem].m_Top +RD_RECT[iSavedItem].m_Bottom+1)/2;
				RD_RECT[iSavedItem].m_Height=RD_RECT[iSavedItem].m_Bottom+1 -RD_RECT[iSavedItem].m_Top;

			}
			if(iSavedSubitem ==5){
				if(num == 0){
					RD_RECT[iSavedItem].m_Right = 0;
				}
				else{
					RD_RECT[iSavedItem].m_Right = num;
					RD_RECT[iSavedItem].m_PosX=(RD_RECT[iSavedItem].m_Right +RD_RECT[iSavedItem].m_Left+1) /2;
					RD_RECT[iSavedItem].m_Width=RD_RECT[iSavedItem].m_Right+1 -RD_RECT[iSavedItem].m_Left;
				}
			}
			if(iSavedSubitem ==6){
				if(num ==0){
					RD_RECT[iSavedItem].m_Bottom =0;
				}
				else{
					RD_RECT[iSavedItem].m_Bottom = num;
					RD_RECT[iSavedItem].m_PosY=(RD_RECT[iSavedItem].m_Top +RD_RECT[iSavedItem].m_Bottom+1)/2;
					RD_RECT[iSavedItem].m_Height=RD_RECT[iSavedItem].m_Bottom+1 -RD_RECT[iSavedItem].m_Top;
				}
			}
			if(iSavedSubitem ==7){
				RD_RECT[iSavedItem].m_Width = num;
			}
			
			if(iSavedSubitem ==8){
				RD_RECT[iSavedItem].m_Height = num;
			}
			
			if(iSavedSubitem ==9){
				RD_RECT[iSavedItem].m_bmode = num;
			}

			if(iSavedSubitem ==10){
				RD_RECT[iSavedItem].m_dir = num;
			}

			if(iSavedSubitem ==12){
				RD_RECT[iSavedItem].m_Thresold = num;
				if(RD_RECT[iSavedItem].m_Thresold > MAX_RESOLUTION){
					RD_RECT[iSavedItem].m_Thresold = MAX_RESOLUTION;
				}else if(RD_RECT[iSavedItem].m_Thresold < MIN_RESOLUTION){
					RD_RECT[iSavedItem].m_Thresold =MIN_RESOLUTION;
				}
			}
			if(iSavedSubitem ==13){
				RD_RECT[iSavedItem].m_Thresold_Peak = num;
				if(RD_RECT[iSavedItem].m_Thresold_Peak > MAX_RESOLUTION){
					RD_RECT[iSavedItem].m_Thresold_Peak = MAX_RESOLUTION;
				}else if(RD_RECT[iSavedItem].m_Thresold_Peak < MIN_RESOLUTION){
					RD_RECT[iSavedItem].m_Thresold_Peak =MIN_RESOLUTION;
				}
			}
			
			RD_RECT[iSavedItem].EX_RECT_SET(RD_RECT[iSavedItem].m_PosX,RD_RECT[iSavedItem].m_PosY,RD_RECT[iSavedItem].m_Width,RD_RECT[iSavedItem].m_Height);
		}

		if(RD_RECT[iSavedItem].m_Left < 0){//////////////////////수정
			RD_RECT[iSavedItem].m_Left = 0;
			RD_RECT[iSavedItem].m_Width = RD_RECT[iSavedItem].m_Right+1 -RD_RECT[iSavedItem].m_Left;
			RD_RECT[iSavedItem].EX_RECT_SET(RD_RECT[iSavedItem].m_PosX,RD_RECT[iSavedItem].m_PosY,RD_RECT[iSavedItem].m_Width,RD_RECT[iSavedItem].m_Height);
		}
		if(RD_RECT[iSavedItem].m_Right >m_CAM_SIZE_WIDTH){
			RD_RECT[iSavedItem].m_Right = m_CAM_SIZE_WIDTH;
			RD_RECT[iSavedItem].m_Width = RD_RECT[iSavedItem].m_Right+1 -RD_RECT[iSavedItem].m_Left;
			RD_RECT[iSavedItem].EX_RECT_SET(RD_RECT[iSavedItem].m_PosX,RD_RECT[iSavedItem].m_PosY,RD_RECT[iSavedItem].m_Width,RD_RECT[iSavedItem].m_Height);
		}
		if(RD_RECT[iSavedItem].m_Top< 0){
			RD_RECT[iSavedItem].m_Top = 0;
			RD_RECT[iSavedItem].m_Height = RD_RECT[iSavedItem].m_Bottom+1 -RD_RECT[iSavedItem].m_Top;
			RD_RECT[iSavedItem].EX_RECT_SET(RD_RECT[iSavedItem].m_PosX,RD_RECT[iSavedItem].m_PosY,RD_RECT[iSavedItem].m_Width,RD_RECT[iSavedItem].m_Height);
		}
		if(RD_RECT[iSavedItem].m_Bottom >m_CAM_SIZE_HEIGHT){
			RD_RECT[iSavedItem].m_Bottom = m_CAM_SIZE_HEIGHT;
			RD_RECT[iSavedItem].m_Height = RD_RECT[iSavedItem].m_Bottom+1 -RD_RECT[iSavedItem].m_Top;
			RD_RECT[iSavedItem].EX_RECT_SET(RD_RECT[iSavedItem].m_PosX,RD_RECT[iSavedItem].m_PosY,RD_RECT[iSavedItem].m_Width,RD_RECT[iSavedItem].m_Height);
		}

		if(RD_RECT[iSavedItem].m_Height<= 0){
			RD_RECT[iSavedItem].m_Height = 1;
			RD_RECT[iSavedItem].EX_RECT_SET(RD_RECT[iSavedItem].m_PosX,RD_RECT[iSavedItem].m_PosY,RD_RECT[iSavedItem].m_Width,RD_RECT[iSavedItem].m_Height);
		}
		if(RD_RECT[iSavedItem].m_Height >=m_CAM_SIZE_HEIGHT){
			RD_RECT[iSavedItem].m_Height = m_CAM_SIZE_HEIGHT;
			RD_RECT[iSavedItem].EX_RECT_SET(RD_RECT[iSavedItem].m_PosX,RD_RECT[iSavedItem].m_PosY,RD_RECT[iSavedItem].m_Width,RD_RECT[iSavedItem].m_Height);
		}

		if(RD_RECT[iSavedItem].m_Width <= 0){
			RD_RECT[iSavedItem].m_Width = 1;
			RD_RECT[iSavedItem].EX_RECT_SET(RD_RECT[iSavedItem].m_PosX,RD_RECT[iSavedItem].m_PosY,RD_RECT[iSavedItem].m_Width,RD_RECT[iSavedItem].m_Height);
		}
		if(RD_RECT[iSavedItem].m_Width >=m_CAM_SIZE_WIDTH){
			RD_RECT[iSavedItem].m_Width = m_CAM_SIZE_WIDTH;
			RD_RECT[iSavedItem].EX_RECT_SET(RD_RECT[iSavedItem].m_PosX,RD_RECT[iSavedItem].m_PosY,RD_RECT[iSavedItem].m_Width,RD_RECT[iSavedItem].m_Height);
		}



		RD_RECT[iSavedItem].EX_RECT_SET(RD_RECT[iSavedItem].m_PosX,RD_RECT[iSavedItem].m_PosY,RD_RECT[iSavedItem].m_Width,RD_RECT[iSavedItem].m_Height);

	



		CString data = "";

		if(iSavedSubitem ==1){
			data.Format("%d",RD_RECT[iSavedItem].m_PosX);			
		}
		else if(iSavedSubitem ==2){
			data.Format("%d",RD_RECT[iSavedItem].m_PosY);
		}
		else if(iSavedSubitem ==3){
			data.Format("%d",RD_RECT[iSavedItem].m_Left);			
		}
		else if(iSavedSubitem ==4){
			data.Format("%d",RD_RECT[iSavedItem].m_Top);			
		}
		else if(iSavedSubitem ==5){
			data.Format("%d",RD_RECT[iSavedItem].m_Right);			
		}
		else if(iSavedSubitem ==6){
			data.Format("%d",RD_RECT[iSavedItem].m_Bottom);			
		}
		else if(iSavedSubitem ==7){
			data.Format("%d",RD_RECT[iSavedItem].m_Width);			
		}
		else if(iSavedSubitem ==8){
			data.Format("%d",RD_RECT[iSavedItem].m_Height);			
		}		
		else if(iSavedSubitem ==9){
			data.Format("%d",RD_RECT[iSavedItem].m_Thresold);			
		}
		else if(iSavedSubitem ==11){
			data.Format("%6.1f",RD_RECT[iSavedItem].MTFRatio);			
		}
		else if(iSavedSubitem ==12){
			data.Format("%d",RD_RECT[iSavedItem].m_Thresold);			
		}
		else if(iSavedSubitem ==13){
			data.Format("%d",RD_RECT[iSavedItem].m_Thresold_Peak);			
		}
		
		//((CEdit *)SetDlgItemText(IDC_EDIT_BRIGHT, data));
		((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(data);


}


/*
void CResolutionDetection_Option::SetDefaultRDZone()
{
	

//*********************좌측 3개 패턴*************************
	m_ctrlRDDataList.SetItemText(2, 1, "117");	
	m_ctrlRDDataList.SetItemText(2, 2, "203");
	m_ctrlRDDataList.SetItemText(2, 3, "192");
	m_ctrlRDDataList.SetItemText(2, 4, "57");
	m_ctrlRDDataList.SetItemText(2, 5, "1");
	m_ctrlRDDataList.SetItemText(2, 6, "0");

	m_ctrlRDDataList.SetItemText(3, 1, "421");
	m_ctrlRDDataList.SetItemText(3, 2, "201");
	m_ctrlRDDataList.SetItemText(3, 3, "186");
	m_ctrlRDDataList.SetItemText(3, 4, "56");
	m_ctrlRDDataList.SetItemText(3, 5, "1");
	m_ctrlRDDataList.SetItemText(3, 6, "1");

	m_ctrlRDDataList.SetItemText(4, 1, "336");
	m_ctrlRDDataList.SetItemText(4, 2, "14");
	m_ctrlRDDataList.SetItemText(4, 3, "59");
	m_ctrlRDDataList.SetItemText(4, 4, "164");	
	m_ctrlRDDataList.SetItemText(4, 5, "0");
	m_ctrlRDDataList.SetItemText(4, 6, "1");

//*********************우측 3개 패턴*************************

	m_ctrlRDDataList.SetItemText(5, 1, "341");	
	m_ctrlRDDataList.SetItemText(5, 2, "274");
	m_ctrlRDDataList.SetItemText(5, 3, "51");
	m_ctrlRDDataList.SetItemText(5, 4, "167");
	m_ctrlRDDataList.SetItemText(5, 5, "0");
	m_ctrlRDDataList.SetItemText(5, 6, "0");

	m_ctrlRDDataList.SetItemText(6, 1, "154");
	m_ctrlRDDataList.SetItemText(6, 2, "12");
	m_ctrlRDDataList.SetItemText(6, 3, "119");
	m_ctrlRDDataList.SetItemText(6, 4, "42");
	m_ctrlRDDataList.SetItemText(6, 5, "1");
	m_ctrlRDDataList.SetItemText(6, 6, "1");

	m_ctrlRDDataList.SetItemText(7, 1, "92");
	m_ctrlRDDataList.SetItemText(7, 2, "69");
	m_ctrlRDDataList.SetItemText(7, 3, "43");
	m_ctrlRDDataList.SetItemText(7, 4, "98");	
	m_ctrlRDDataList.SetItemText(7, 5, "0");
	m_ctrlRDDataList.SetItemText(7, 6, "0");

//***********************위쪽 2개 패턴************************

	m_ctrlRDDataList.SetItemText(8, 1, "453");
	m_ctrlRDDataList.SetItemText(8, 2, "4");
	m_ctrlRDDataList.SetItemText(8, 3, "115");
	m_ctrlRDDataList.SetItemText(8, 4, "47");
	m_ctrlRDDataList.SetItemText(8, 5, "1");
	m_ctrlRDDataList.SetItemText(8, 6, "0");

	m_ctrlRDDataList.SetItemText(9, 1, "585");
	m_ctrlRDDataList.SetItemText(9, 2, "65");
	m_ctrlRDDataList.SetItemText(9, 3, "48");
	m_ctrlRDDataList.SetItemText(9, 4, "103");	
	m_ctrlRDDataList.SetItemText(9, 5, "0");
	m_ctrlRDDataList.SetItemText(9, 6, "0");

//***********************아래쪽 2개 패턴************************

	m_ctrlRDDataList.SetItemText(10, 1, "454");
	m_ctrlRDDataList.SetItemText(10, 2, "407");
	m_ctrlRDDataList.SetItemText(10, 3, "111");
	m_ctrlRDDataList.SetItemText(10, 4, "47");
	m_ctrlRDDataList.SetItemText(10, 5, "1");
	m_ctrlRDDataList.SetItemText(10, 6, "0");

	m_ctrlRDDataList.SetItemText(11, 1, "591");
	m_ctrlRDDataList.SetItemText(11, 2, "296");
	m_ctrlRDDataList.SetItemText(11, 3, "40");
	m_ctrlRDDataList.SetItemText(11, 4, "95");	
	m_ctrlRDDataList.SetItemText(11, 5, "0");
	m_ctrlRDDataList.SetItemText(11, 6, "1");	

	m_ctrlRDDataList.SetItemText(12, 1, "159");
	m_ctrlRDDataList.SetItemText(12, 2, "409");
	m_ctrlRDDataList.SetItemText(12, 3, "115");
	m_ctrlRDDataList.SetItemText(12, 4, "44");	
	m_ctrlRDDataList.SetItemText(12, 5, "1");
	m_ctrlRDDataList.SetItemText(12, 6, "1");	

	m_ctrlRDDataList.SetItemText(13, 1, "92");
	m_ctrlRDDataList.SetItemText(13, 2, "293");
	m_ctrlRDDataList.SetItemText(13, 3, "48");
	m_ctrlRDDataList.SetItemText(13, 4, "100");	
	m_ctrlRDDataList.SetItemText(13, 5, "0");
	m_ctrlRDDataList.SetItemText(13, 6, "1");	

	UpdateData(FALSE);
}
*/

//void CResolutionDetection_Option::SetLoadParameterRDZone()//Uploadlist
void CResolutionDetection_Option::Uploadlist()//Uploadlist
{
	CString str;
	
	for(int i=0; i<14; i++)
	{
		str.Format("%d", RD_RECT[i].m_PosX);
		m_ctrlRDDataList.SetItemText(i, 1, str);
		str.Format("%d", RD_RECT[i].m_PosY);
		m_ctrlRDDataList.SetItemText(i, 2, str);

		str.Format("%d", RD_RECT[i].m_Left);
		m_ctrlRDDataList.SetItemText(i, 3, str);
		str.Format("%d", RD_RECT[i].m_Top);
		m_ctrlRDDataList.SetItemText(i, 4, str);
		
		str.Format("%d", RD_RECT[i].m_Right);
		m_ctrlRDDataList.SetItemText(i, 5, str);
		str.Format("%d", RD_RECT[i].m_Bottom);
		m_ctrlRDDataList.SetItemText(i, 6, str);

		str.Format("%d", RD_RECT[i].m_Width);
		m_ctrlRDDataList.SetItemText(i, 7, str);
		str.Format("%d", RD_RECT[i].m_Height);
		m_ctrlRDDataList.SetItemText(i, 8, str);	
		str.Format("%d", RD_RECT[i].m_bmode);
		m_ctrlRDDataList.SetItemText(i, 9, str);
		str.Format("%d", RD_RECT[i].m_dir);
		m_ctrlRDDataList.SetItemText(i, 10, str);

		str.Format("%0.0f", RD_RECT[i].MTFRatio);
		m_ctrlRDDataList.SetItemText(i, 11, str);
		str.Format("%d", RD_RECT[i].m_Thresold);
		m_ctrlRDDataList.SetItemText(i, 12, str);
		str.Format("%d", RD_RECT[i].m_Thresold_Peak);
		m_ctrlRDDataList.SetItemText(i, 13, str);

		if(RD_RECT[i].ENABLE == TRUE){
			m_ctrlRDDataList.SetItemState(i,0x2000,LVIS_STATEIMAGEMASK);
		}else{
			m_ctrlRDDataList.SetItemState(i,0x1000,LVIS_STATEIMAGEMASK);
		}

	}

	UpdateData(FALSE);
}

void CResolutionDetection_Option::OnBnClickedBtnSetzone()
{
	ChangeCheck =1;
	((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	
	MasterMod =TRUE;

	
	if(b_Auto_Mod == TRUE){
		((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan =1;	
			for(int i =0;i<10;i++){
				if(((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan == 0){
					break;
				}else{
					DoEvents(50);
				}
			}
				
		SetZoneUsingAutoDetection(m_RGBScanbuf);//검출이 아될 경우 기본 파라메터를 불러들이는 알고리즘 추가 
	}

	Uploadlist();//SetLoadParameterRDZone();
	MasterMod =FALSE;

	
	for(int t=0; t<14; t++){
		ChangeItem[t]=t;
	}
	for(int t=0; t<14; t++){
		m_ctrlRDDataList.Update(t);
	}		
}
void CResolutionDetection_Option::SetZoneUsingAutoDetection(LPBYTE IN_RGB)
{
	IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);	
	IplImage *temp_PatternImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH,CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);

	IplImage *tempOriginImage = cvCreateImage(cvSize(1280, 720), IPL_DEPTH_8U, 3);

	BYTE R,G,B;
	double Sum_Y;

	for (int y=0; y<CAM_IMAGE_HEIGHT; y++)
	{
		for (int x=0; x<CAM_IMAGE_WIDTH; x++)
		{
			B = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 2];
			
			tempOriginImage->imageData[y * (CAM_IMAGE_WIDTH*3) + x*3 ] = B;
			tempOriginImage->imageData[y * (CAM_IMAGE_WIDTH*3) + x*3 +1] = G;
			tempOriginImage->imageData[y * (CAM_IMAGE_WIDTH*3) + x*3 +2] = R;

			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;						
		}
	}	
	
	SetStandardRDZoneParameter(0, m_CAM_SIZE_WIDTH/2, m_CAM_SIZE_HEIGHT/2, 40, 40, 0, 0);
	SetStandardRDZoneParameter(1, m_CAM_SIZE_WIDTH/2, m_CAM_SIZE_HEIGHT/2, 40, 40, 0, 0);
//***************************************************************************
	SetStandardRDZoneParameter(2, BASIC_RECT[2].m_Left, BASIC_RECT[2].m_Top, BASIC_RECT[2].m_Width, BASIC_RECT[2].m_Height, BASIC_RECT[2].m_bmode, BASIC_RECT[2].m_dir);
	SetStandardRDZoneParameter(3, BASIC_RECT[3].m_Left, BASIC_RECT[3].m_Top, BASIC_RECT[3].m_Width, BASIC_RECT[3].m_Height, BASIC_RECT[3].m_bmode, BASIC_RECT[3].m_dir);
	SetStandardRDZoneParameter(4, BASIC_RECT[4].m_Left, BASIC_RECT[4].m_Top, BASIC_RECT[4].m_Width, BASIC_RECT[4].m_Height, BASIC_RECT[4].m_bmode, BASIC_RECT[4].m_dir);
	SetStandardRDZoneParameter(5, BASIC_RECT[5].m_Left, BASIC_RECT[5].m_Top, BASIC_RECT[5].m_Width, BASIC_RECT[5].m_Height, BASIC_RECT[5].m_bmode, BASIC_RECT[5].m_dir);
//***************************************************************************
	SetStandardRDZoneParameter(6, BASIC_RECT[6].m_Left, BASIC_RECT[6].m_Top, BASIC_RECT[6].m_Width, BASIC_RECT[6].m_Height, BASIC_RECT[6].m_bmode, BASIC_RECT[6].m_dir);
	SetStandardRDZoneParameter(7, BASIC_RECT[7].m_Left, BASIC_RECT[7].m_Top, BASIC_RECT[7].m_Width, BASIC_RECT[7].m_Height, BASIC_RECT[7].m_bmode, BASIC_RECT[7].m_dir);
//***************************************************************************
	SetStandardRDZoneParameter(8, BASIC_RECT[8].m_Left, BASIC_RECT[8].m_Top, BASIC_RECT[8].m_Width, BASIC_RECT[8].m_Height, BASIC_RECT[8].m_bmode, BASIC_RECT[8].m_dir);
	SetStandardRDZoneParameter(9, BASIC_RECT[9].m_Left, BASIC_RECT[9].m_Top, BASIC_RECT[9].m_Width, BASIC_RECT[9].m_Height, BASIC_RECT[9].m_bmode, BASIC_RECT[9].m_dir);
//***************************************************************************
	SetStandardRDZoneParameter(10, BASIC_RECT[10].m_Left, BASIC_RECT[10].m_Top, BASIC_RECT[10].m_Width, BASIC_RECT[10].m_Height, BASIC_RECT[10].m_bmode, BASIC_RECT[10].m_dir);
	SetStandardRDZoneParameter(11, BASIC_RECT[11].m_Left, BASIC_RECT[11].m_Top, BASIC_RECT[11].m_Width, BASIC_RECT[11].m_Height, BASIC_RECT[11].m_bmode, BASIC_RECT[11].m_dir);
//***************************************************************************
	SetStandardRDZoneParameter(12, BASIC_RECT[12].m_Left, BASIC_RECT[12].m_Top, BASIC_RECT[12].m_Width, BASIC_RECT[12].m_Height, BASIC_RECT[12].m_bmode, BASIC_RECT[12].m_dir);
	SetStandardRDZoneParameter(13, BASIC_RECT[13].m_Left, BASIC_RECT[13].m_Top, BASIC_RECT[13].m_Width, BASIC_RECT[13].m_Height, BASIC_RECT[13].m_bmode, BASIC_RECT[13].m_dir);

//	cvSaveImage("C:\\ORIGIANLCAMIMAGE.bmp", tempOriginImage);
	cvReleaseImage(&tempOriginImage);
//	OriginImage = cvLoadImage("C:\\asdasd.bmp", 0);

//	Capture_Gray_SAVE("C:\\RGBIMAGE", IN_RGB, 720, 480);	
//	cvSaveImage("C:\\Default_Image.bmp", OriginImage);
	
//	BOOL WideCamMode = GetWideCamCheckFunc(RGBOrgImage);

	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 3, 1, 1);
	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 3, 1, 1); //객체 외곽의 Overshooting으로 인하여 한번 더 처리 함..

	cvCanny(SmoothImage, CannyImage, 0, 200);

	cvDilate(CannyImage, DilateImage);
	cvCopyImage(DilateImage, temp_PatternImage);
	
	cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);
	
	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
		counter++;
	
	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area=0, arcCount=0;
	counter = 0;
	double old_dist = 999999;

	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;
		double circularity = (4.0*3.14*area)/(arcCount*arcCount);
		
		if(circularity > 0.8)
		{			
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;

			if(center_x > (CAM_IMAGE_WIDTH/2) - (CAM_IMAGE_WIDTH/8) && center_x < (CAM_IMAGE_WIDTH/2) + (CAM_IMAGE_WIDTH/8) && center_y > (CAM_IMAGE_HEIGHT/2) - (CAM_IMAGE_HEIGHT/6) && center_y < (CAM_IMAGE_HEIGHT/2) + (CAM_IMAGE_HEIGHT/6))
			{
				if(rect.width < (CAM_IMAGE_WIDTH/2) && rect.height < (CAM_IMAGE_HEIGHT/2))
				{
				//	cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
					
					double dist = GetDistance(center_x, center_y, (Standard_RD_RECT[0].m_Left+Standard_RD_RECT[0].m_Width/2), (Standard_RD_RECT[0].m_Top+Standard_RD_RECT[0].m_Height/2));
					
					if(dist < old_dist)
					{
						RD_RECT[0].m_Left = rect.width/4+rect.x;
						RD_RECT[0].m_Top = rect.height/4+rect.y;
						RD_RECT[0].m_Width = rect.width/2;
						RD_RECT[0].m_Height = rect.height/2;
				
						RD_RECT[0].m_Right	= RD_RECT[0].m_Left + RD_RECT[0].m_Width;
						RD_RECT[0].m_Bottom = RD_RECT[0].m_Top + RD_RECT[0].m_Height;
						RD_RECT[0].m_checkLine_Buffer_cnt = 0;
						
						RD_RECT[0].m_PosX = rect.x + rect.width/2;
						RD_RECT[0].m_PosY = rect.y + rect.height/2;

						RD_RECT[0].m_resultX = rect.x + rect.width/2;
						RD_RECT[0].m_resultY= rect.y + rect.height/2;

						RD_RECT[1].m_Left	= RD_RECT[0].m_Right + (RD_RECT[0].m_Width/2);
						RD_RECT[1].m_Top	= RD_RECT[0].m_Top + RD_RECT[0].m_Height;
						RD_RECT[1].m_Width	= RD_RECT[0].m_Width;
						RD_RECT[1].m_Height = RD_RECT[0].m_Height;
						RD_RECT[1].m_Right	= RD_RECT[1].m_Left + RD_RECT[1].m_Width;
						RD_RECT[1].m_Bottom = RD_RECT[1].m_Top + RD_RECT[1].m_Height;
						RD_RECT[1].m_checkLine_Buffer_cnt = 0;

						RD_RECT[1].m_PosX = RD_RECT[1].m_Left + RD_RECT[1].m_Width/2;
						RD_RECT[1].m_PosY = RD_RECT[1].m_Top + RD_RECT[1].m_Height/2;

						old_dist = dist;
					}
				}
			}
			else
			{
			//	cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8);  
			}
		}

		counter++;
	}
	
	CvRect center_pos_Rect = cvRect(RD_RECT[0].m_Left, RD_RECT[0].m_Top, RD_RECT[0].m_Width, RD_RECT[0].m_Height);
	int rect_cnt = 0;	
	
	for(int q = 2; q < 14; q++)
	{		
		double old_dist = 9999999;		
		double WHRatio_Thr, HWRatio_Thr;
		double areaRatio_Thr;

		if(q > 5)
		{
			WHRatio_Thr = 1.0;
			HWRatio_Thr = 1.0;
			areaRatio_Thr = 0.3;
		}
		else
		{			
			WHRatio_Thr = 2.0;
			HWRatio_Thr = 2.0;
			areaRatio_Thr = 0.3;
		}

		for(int k=0; k<counter; k++)
		{
			rect = rectArray[k];
			double WHRatio = (double)rect.width / (double)rect.height;
			double HWRatio = (double)rect.height / (double)rect.width;
			int White_area_cnt = 0;
			unsigned char pixel_v=0;			

			if(WHRatio > WHRatio_Thr || HWRatio > WHRatio_Thr){
				if(rect.width > center_pos_Rect.width || rect.height > center_pos_Rect.height){
					for(int i=rect.y; i<rect.y+rect.height; i++){
						for(int j=rect.x; j<rect.x+rect.width; j++){
							pixel_v = RGBResultImage->imageData[i*RGBResultImage->widthStep+j*3];
							
							if(pixel_v == 255)
								White_area_cnt++;
						}
					}

					if( (rect.width * rect.height) * areaRatio_Thr < areaArray[k])
					{			
						if( (RD_RECT[0].m_Width*2.0) < rect.width ||  (RD_RECT[0].m_Width*2) < rect.height) 
						{						
							int curr_rect_center_x = rect.x + rect.width/2;
							int curr_rect_center_y = rect.y + rect.height/2;
							int curr_centerRect_center_x = center_pos_Rect.x + center_pos_Rect.width/2;
							int curr_centerRect_center_y = center_pos_Rect.y + center_pos_Rect.height/2;

							double dist = GetDistance(curr_rect_center_x, curr_rect_center_y,  Standard_RD_RECT[q].m_Left+Standard_RD_RECT[q].m_Width/2, Standard_RD_RECT[q].m_Top+Standard_RD_RECT[q].m_Height/2);							

							if(old_dist > dist)
							{
								if(rect.height < rect.width && Standard_RD_RECT[q].m_bmode == 1 && rect.height < CAM_IMAGE_HEIGHT/2 && rect.width < CAM_IMAGE_WIDTH/2)
								{
									if(Standard_RD_RECT[q].m_Width > dist)
									{
										RD_RECT[q].m_Left = rect.x - 3;
										RD_RECT[q].m_Top = rect.y - 3;
										RD_RECT[q].m_Width = rect.width + 6;
										RD_RECT[q].m_Height = rect.height + 6;
										RD_RECT[q].m_Right	= RD_RECT[q].m_Left + RD_RECT[q].m_Width;
										RD_RECT[q].m_Bottom = RD_RECT[q].m_Top + RD_RECT[q].m_Height;									
										RD_RECT[q].m_bmode = 1;
										
										RD_RECT[q].m_PosX = RD_RECT[q].m_Left + RD_RECT[q].m_Width / 2;
										RD_RECT[q].m_PosY = RD_RECT[q].m_Top + RD_RECT[q].m_Height / 2;

										/*if(q < 6){
											if(curr_rect_center_x < curr_centerRect_center_x)
												RD_RECT[q].m_dir = 0;
											else
												RD_RECT[q].m_dir = 1;
										}
										else
										{
											if(curr_rect_center_x < curr_centerRect_center_x)
												RD_RECT[q].m_dir = 1;
											else
												RD_RECT[q].m_dir = 0;
										}*/

										cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 0), 1, 8);					
										old_dist = dist;
									}
								}
								else if(rect.height > rect.width && Standard_RD_RECT[q].m_bmode == 0 && rect.height < CAM_IMAGE_HEIGHT/2 && rect.width < CAM_IMAGE_WIDTH/2)
								{
									if(Standard_RD_RECT[q].m_Height > dist)
									{
										RD_RECT[q].m_Left = rect.x - 3;
										RD_RECT[q].m_Top = rect.y - 3;
										RD_RECT[q].m_Width = rect.width + 6;
										RD_RECT[q].m_Height = rect.height + 6;
										RD_RECT[q].m_Right	= RD_RECT[q].m_Left + RD_RECT[q].m_Width;
										RD_RECT[q].m_Bottom = RD_RECT[q].m_Top + RD_RECT[q].m_Height;									
										RD_RECT[q].m_bmode = 0;
										
										RD_RECT[q].m_PosX = RD_RECT[q].m_Left + RD_RECT[q].m_Width / 2;
										RD_RECT[q].m_PosY = RD_RECT[q].m_Top + RD_RECT[q].m_Height / 2;

										/*if(q < 6){
											if(curr_rect_center_y < curr_centerRect_center_y)
												RD_RECT[q].m_dir = 1;
											else
												RD_RECT[q].m_dir = 0;
										}
										else
										{
											if(curr_rect_center_y < curr_centerRect_center_y)
												RD_RECT[q].m_dir = 0;
											else
												RD_RECT[q].m_dir = 1;
										}*/

										cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 0), 1, 8);					
										old_dist = dist;
									}
								}								
							}							
						}
					}
				}
			}
		}
	}

//	cvSaveImage("C:\\RGBResultImage.bmp", RGBResultImage);
	
	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&CannyImage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
//	cvReleaseImage(&RGBOrgImage);

	/*delete contour;
	delete temp_contour;*/
	delete []rectArray;
	delete []areaArray;

}

void CResolutionDetection_Option::OnBnClickedBtnDefault()
{
	//SetDefaultRDZone();
	ChangeCheck=1;
	UpdateData(TRUE);
	if(b_Auto_Mod==0){
		INIT_EIJAGEN();
	}
//	OnBnClickedBtnSetMtf();

	int temp_value = GetDlgItemInt(IDC_EDIT_MTFRATIO);
	
	if( ((CButton*)GetDlgItem(IDC_CHECK_SET_LIMITED))->GetCheck() )
	{
	//	if( temp_value > 40 )	//MTF 비율이 40%를 넘어가면 라인 특성이 사라지는 듯...
	//		temp_value = 40;
	}
	
	m_dMTFRatio = temp_value;

	int temp_value2 = GetDlgItemInt(IDC_EDIT_EDGE_MTF);

	if( ((CButton*)GetDlgItem(IDC_CHECK_SET_LIMITED))->GetCheck() )
	{
	//	if( temp_value2 > 40 )	//MTF 비율이 40%를 넘어가면 라인 특성이 사라지는 듯...
	//		temp_value2 = 40;
	}
	
	m_dEdgeMTFRatio = temp_value2;

	m_dResolutionThreshold = GetDlgItemInt(IDC_EDIT_RV);

	if( m_dResolutionThreshold < MIN_RESOLUTION)
		m_dResolutionThreshold = MIN_RESOLUTION;
	if( m_dResolutionThreshold > MAX_RESOLUTION)
		m_dResolutionThreshold = MAX_RESOLUTION;

	m_dResolutionThreshold_Peak = GetDlgItemInt(IDC_EDIT_RV_MAX);

	if( m_dResolutionThreshold_Peak < MIN_RESOLUTION)
		m_dResolutionThreshold_Peak = MIN_RESOLUTION;
	if( m_dResolutionThreshold_Peak > MAX_RESOLUTION)
		m_dResolutionThreshold_Peak = MAX_RESOLUTION;

	m_dEdgeResolutionThreshold = GetDlgItemInt(IDC_EDIT_EDGE_TRHESHOLD);

	if( m_dEdgeResolutionThreshold < MIN_RESOLUTION)
		m_dEdgeResolutionThreshold = MIN_RESOLUTION;
	if( m_dEdgeResolutionThreshold > MAX_RESOLUTION)
		m_dEdgeResolutionThreshold = MAX_RESOLUTION;

	m_dEdgeResolutionThreshold_Peak = GetDlgItemInt(IDC_EDIT_EDGE_TRHESHOLD_MAX);

	if( m_dEdgeResolutionThreshold_Peak < SIDE_MIN_RESOLUTION)
		m_dEdgeResolutionThreshold_Peak = SIDE_MIN_RESOLUTION;
	if( m_dEdgeResolutionThreshold_Peak > SIDE_MAX_RESOLUTION)
		m_dEdgeResolutionThreshold_Peak = SIDE_MAX_RESOLUTION;
	
	CString str;
	
	for(int i=0; i<14; i++)
	{
		if(i > 1 && i < 6)	//중앙 4개 LinePair
		{
			str.Format("%d", m_dMTFRatio);
			m_ctrlRDDataList.SetItemText(i, 11, str);
			str.Format("%d", m_dResolutionThreshold);
			m_ctrlRDDataList.SetItemText(i, 12, str);
			str.Format("%d", m_dResolutionThreshold_Peak);
			m_ctrlRDDataList.SetItemText(i, 13, str);
			

			RD_RECT[i].MTFRatio		= m_dMTFRatio;
			RD_RECT[i].m_Thresold	= m_dResolutionThreshold;
			RD_RECT[i].m_Thresold_Peak	= m_dResolutionThreshold_Peak;
		}
		else if(i > 5 && i < 14)	//외곽 8개 LinePair
		{
			str.Format("%d", m_dEdgeMTFRatio);
			m_ctrlRDDataList.SetItemText(i, 11, str);
			str.Format("%d", m_dEdgeResolutionThreshold);
			m_ctrlRDDataList.SetItemText(i, 12, str);
			str.Format("%d", m_dEdgeResolutionThreshold_Peak);
			m_ctrlRDDataList.SetItemText(i, 13, str);

			RD_RECT[i].MTFRatio		= m_dEdgeMTFRatio;
			RD_RECT[i].m_Thresold	= m_dEdgeResolutionThreshold;
			RD_RECT[i].m_Thresold_Peak	= m_dEdgeResolutionThreshold_Peak;
		}
		else	//중심점 부분
		{
			str.Format("%d", 0);
			m_ctrlRDDataList.SetItemText(i, 11, str);			
			m_ctrlRDDataList.SetItemText(i, 12, str);
			m_ctrlRDDataList.SetItemText(i, 13, str);
			RD_RECT[i].MTFRatio		= 0;
			RD_RECT[i].m_Thresold	= 0;
			RD_RECT[i].m_Thresold_Peak	= 0;
		}
	}	
	
	UpdateData(FALSE);
	
	for(int t=0; t<14; t++){
		ChangeItem[t]=t;
	}
	for(int t=0; t<14; t++){
		m_ctrlRDDataList.Update(t);
	}

	//Uploadlist
}
void CResolutionDetection_Option::INIT_EIJAGEN(){

	int center_x =E_Total_PosX;
	int center_y =E_Total_PosY;
	int l_offset_x =  E_Dis_Width;
	int l_offset_y = E_Dis_Height;

	int large_w =E_Total_Width;
	int large_h = E_Total_Height;
	/*	int Bon = E_BON;
	int Mtf = E_MTF;
*/
	int center_x_offset =E_Edge_PosX;
	int center_y_offset =E_Edge_PosY;
	int small_w =E_Edge_Width;
	int small_h =E_Edge_Height;
	int s_offset_x = E_Dis_Width_Edge;
	int s_offset_y = E_Dis_Height_Edge;

	RD_RECT[0].EX_RESOLUTION_SET(center_x,center_y,large_h/3,large_h/3,0,0); //왼쪽 
	RD_RECT[0].m_bmode = 0;
	RD_RECT[0].m_dir = 0;

	RD_RECT[1].EX_RESOLUTION_SET(center_x+(l_offset_x/2),center_y+(l_offset_y/2),large_h/3,large_h/3,0,0); //왼쪽 
	RD_RECT[1].m_bmode = 0;
	RD_RECT[1].m_dir = 0;
	
	RD_RECT[2].EX_RESOLUTION_SET(center_x-(l_offset_x+(large_w/2)),center_y,large_w,large_h,1,0); //왼쪽 
	RD_RECT[2].m_bmode = 1;
	RD_RECT[2].m_dir = 0;
	RD_RECT[3].EX_RESOLUTION_SET(center_x+(l_offset_x+(large_w/2)),center_y,large_w,large_h,1,1); //오른쪽
	RD_RECT[3].m_bmode = 1;
	RD_RECT[3].m_dir = 1;
	
	RD_RECT[4].EX_RESOLUTION_SET(center_x,center_y-(l_offset_y+(large_w/2)),large_h,large_w,0,1); //위
	RD_RECT[4].m_bmode = 0;
	RD_RECT[4].m_dir = 1;
	RD_RECT[5].EX_RESOLUTION_SET(center_x,center_y+(l_offset_y+(large_w/2)),large_h,large_w,0,0); //아래
	RD_RECT[5].m_bmode = 0;
	RD_RECT[5].m_dir = 0;

	//int buf_center_x = center_x - center_x_offset;
	//int buf_center_y = center_y - center_y_offset;

	//RD_RECT[6].EX_RESOLUTION_SET(buf_center_x+(s_offset_x+(small_w/2)),buf_center_y,small_w,small_h,1,1); //오른쪽 //왼쪽 상
	//RD_RECT[6].m_bmode = 1;
	//RD_RECT[6].m_dir = 1;
	//RD_RECT[7].EX_RESOLUTION_SET(buf_center_x,buf_center_y+(s_offset_y+(small_w/2)),small_h,small_w,0,0); //아래 
	//RD_RECT[7].m_bmode = 0;
	//RD_RECT[7].m_dir = 0;
	//

	//buf_center_x = center_x + center_x_offset;
	//buf_center_y = center_y - center_y_offset;

	//RD_RECT[8].EX_RESOLUTION_SET(buf_center_x-(s_offset_x+(small_w/2)),buf_center_y,small_w,small_h,1,0); //왼쪽 //오른쪽 상
	//RD_RECT[8].m_bmode = 1;
	//RD_RECT[8].m_dir = 0;
	//RD_RECT[9].EX_RESOLUTION_SET(buf_center_x,buf_center_y+(s_offset_y+(small_w/2)),small_h,small_w,0,0); //아래 
	//RD_RECT[9].m_bmode = 0;
	//RD_RECT[9].m_dir = 0;

	//buf_center_x = center_x + center_x_offset;
	//buf_center_y = center_y + center_y_offset;

	//RD_RECT[10].EX_RESOLUTION_SET(buf_center_x-(s_offset_x+(small_w/2)),buf_center_y,small_w,small_h,1,0); //왼쪽 //오른쪽 하
	//RD_RECT[10].m_bmode = 1;
	//RD_RECT[10].m_dir = 0;
	//RD_RECT[11].EX_RESOLUTION_SET(buf_center_x,buf_center_y-(s_offset_y+(small_w/2)),small_h,small_w,0,1); //위
	//RD_RECT[11].m_bmode = 0;
	//RD_RECT[11].m_dir = 1;


	//buf_center_x = center_x - center_x_offset;
	//buf_center_y = center_y + center_y_offset;

	//RD_RECT[12].EX_RESOLUTION_SET(buf_center_x+(s_offset_x+(small_w/2)),buf_center_y,small_w,small_h,1,1); //오른쪽 //왼쪽 하
	//RD_RECT[12].m_bmode = 1;
	//RD_RECT[12].m_dir = 1;
	//RD_RECT[13].EX_RESOLUTION_SET(buf_center_x,buf_center_y-(s_offset_y+(small_w/2)),small_h,small_w,0,1); //위 	
	//RD_RECT[13].m_bmode = 0;
	//RD_RECT[13].m_dir = 1;
	
//	int buf_center_x = center_x - center_x_offset;
//	int buf_center_y = center_y - center_y_offset;
//
//	RD_RECT[6].EX_RESOLUTION_SET(buf_center_x-(s_offset_x+(small_w)),buf_center_y,small_w,small_h,1,1); //왼쪽 //상
//	RD_RECT[6].m_bmode = 1;
//	RD_RECT[6].m_dir = 0;
//	RD_RECT[7].EX_RESOLUTION_SET(buf_center_x-(s_offset_x+(small_w)),buf_center_y+(s_offset_y*4),small_w,small_h,1,1); //왼쪽 //중앙
//	RD_RECT[7].m_bmode = 1;
//	RD_RECT[7].m_dir = 0;
//	RD_RECT[8].EX_RESOLUTION_SET(buf_center_x-(s_offset_x+(small_w)),buf_center_y+(s_offset_y*8),small_w,small_h,1,1); //왼쪽 //중앙
//	RD_RECT[8].m_bmode = 1;
//	RD_RECT[8].m_dir = 0;
//	RD_RECT[9].EX_RESOLUTION_SET(buf_center_x-(s_offset_x+(small_w)),buf_center_y+(s_offset_y*12),small_w,small_h,0,0); //왼쪽 //하
//	RD_RECT[9].m_bmode = 1;
//	RD_RECT[9].m_dir = 0;
//
//	buf_center_x = center_x + center_x_offset;
//	buf_center_y = center_y - center_y_offset;
//
//
////	buf_center_x = center_x + center_x_offset;
////	buf_center_y = center_y + center_y_offset;
//
//	RD_RECT[10].EX_RESOLUTION_SET(buf_center_x+(s_offset_x+(small_w)),buf_center_y,small_w,small_h,1,1); //왼쪽 //상
//	RD_RECT[10].m_bmode = 1;
//	RD_RECT[10].m_dir = 1;
//	RD_RECT[11].EX_RESOLUTION_SET(buf_center_x+(s_offset_x+(small_w)),buf_center_y+(s_offset_y*4),small_w,small_h,1,1); //왼쪽 //중앙
//	RD_RECT[11].m_bmode = 1;
//	RD_RECT[11].m_dir = 1;
//	RD_RECT[12].EX_RESOLUTION_SET(buf_center_x+(s_offset_x+(small_w)),buf_center_y+(s_offset_y*8),small_w,small_h,1,1); //왼쪽 //중앙
//	RD_RECT[12].m_bmode = 1;
//	RD_RECT[12].m_dir = 1;
//	RD_RECT[13].EX_RESOLUTION_SET(buf_center_x+(s_offset_x+(small_w)),buf_center_y+(s_offset_y*12),small_w,small_h,0,0); //왼쪽 //하
//	RD_RECT[13].m_bmode = 1;
//	RD_RECT[13].m_dir = 1;

	int buf_center_x = center_x - center_x_offset;
	int buf_center_y = center_y - center_y_offset;

	RD_RECT[6].EX_RESOLUTION_SET(center_x_offset-((small_w)),center_y_offset,small_w,small_h,1,0); //왼쪽 //상
	RD_RECT[6].m_bmode = 1;
	RD_RECT[6].m_dir = 0;
	RD_RECT[7].EX_RESOLUTION_SET(center_x_offset-((small_w)),center_y_offset+(s_offset_y*4),small_w,small_h,1,0); //왼쪽 //중앙
	RD_RECT[7].m_bmode = 1;
	RD_RECT[7].m_dir = 0;
	RD_RECT[8].EX_RESOLUTION_SET(center_x_offset-((small_w)),center_y_offset+(s_offset_y*8),small_w,small_h,1,0); //왼쪽 //중앙
	RD_RECT[8].m_bmode = 1;
	RD_RECT[8].m_dir = 0;
	RD_RECT[9].EX_RESOLUTION_SET(center_x_offset-((small_w)),center_y_offset+(s_offset_y*12),small_w,small_h,1,0); //왼쪽 //하
	RD_RECT[9].m_bmode = 1;
	RD_RECT[9].m_dir = 0;

	buf_center_x = center_x + center_x_offset;
	buf_center_y = center_y - center_y_offset;


//	buf_center_x = center_x + center_x_offset;
//	buf_center_y = center_y + center_y_offset;

	RD_RECT[10].EX_RESOLUTION_SET(center_x_offset+((s_offset_x+small_w)),center_y_offset,small_w,small_h,1,1); //왼쪽 //상
	RD_RECT[10].m_bmode = 1;
	RD_RECT[10].m_dir = 1;
	RD_RECT[11].EX_RESOLUTION_SET(center_x_offset+((s_offset_x+small_w)),center_y_offset+(s_offset_y*4),small_w,small_h,1,1); //왼쪽 //중앙
	RD_RECT[11].m_bmode = 1;
	RD_RECT[11].m_dir = 1;
	RD_RECT[12].EX_RESOLUTION_SET(center_x_offset+((s_offset_x+small_w)),center_y_offset+(s_offset_y*8),small_w,small_h,1,1); //왼쪽 //중앙
	RD_RECT[12].m_bmode = 1;
	RD_RECT[12].m_dir = 1;
	RD_RECT[13].EX_RESOLUTION_SET(center_x_offset+((s_offset_x+small_w)),center_y_offset+(s_offset_y*12),small_w,small_h,1,1); //왼쪽 //하
	RD_RECT[13].m_bmode = 1;
	RD_RECT[13].m_dir = 1;

	for(int lop = 0;lop<14;lop++){
		RD_RECT[lop].ENABLE = TRUE;
	}

	Uploadlist();//SetLoadParameterRDZone();
}

void CResolutionDetection_Option::OnBnClickedBtnSetMtf()
{
	UpdateData(TRUE);
	E_Total_PosX = GetDlgItemInt(IDC_RE_PosX);	
	E_Total_PosY = GetDlgItemInt(IDC_RE_PosY);
	E_Dis_Width	= GetDlgItemInt(IDC_RE_Dis_W);
	E_Dis_Height = GetDlgItemInt(IDC_RE_Dis_H);
	E_Total_Width = GetDlgItemInt(IDC_RE_Width);
	E_Total_Height = GetDlgItemInt(IDC_RE_Height);
	m_dMTFRatio = GetDlgItemInt(IDC_EDIT_MTFRATIO);
	m_dResolutionThreshold = GetDlgItemInt(IDC_EDIT_RV);
	m_dResolutionThreshold_Peak = GetDlgItemInt(IDC_EDIT_RV_MAX);


	E_Edge_PosX = GetDlgItemInt(IDC_RE_PosX2);	
	E_Edge_PosY = GetDlgItemInt(IDC_RE_PosY2);
	E_Dis_Width_Edge	= GetDlgItemInt(IDC_RE_Dis_W2);
	E_Dis_Height_Edge = GetDlgItemInt(IDC_RE_Dis_H2);
	E_Edge_Width = GetDlgItemInt(IDC_RE_Width2);
	E_Edge_Height = GetDlgItemInt(IDC_RE_Height2);
	m_dEdgeMTFRatio = GetDlgItemInt(IDC_EDIT_EDGE_MTF);
	m_dEdgeResolutionThreshold = GetDlgItemInt(IDC_EDIT_EDGE_TRHESHOLD);
	m_dEdgeResolutionThreshold_Peak = GetDlgItemInt(IDC_EDIT_EDGE_TRHESHOLD_MAX);

	int temp_value = GetDlgItemInt(IDC_EDIT_MTFRATIO);
	
//	if( temp_value > 40 )	//MTF 비율이 40%를 넘어가면 라인 특성이 사라지는 듯...
//		temp_value = 40;
	
	m_dMTFRatio = temp_value;

	int temp_value2 = GetDlgItemInt(IDC_EDIT_EDGE_MTF);

//	if( temp_value2 > 40 )	//MTF 비율이 40%를 넘어가면 라인 특성이 사라지는 듯...
//		temp_value2 = 40;
	
	m_dEdgeMTFRatio = temp_value2;

	m_dResolutionThreshold = GetDlgItemInt(IDC_EDIT_RV);
	m_dResolutionThreshold_Peak = GetDlgItemInt(IDC_EDIT_RV_MAX);
	m_dEdgeResolutionThreshold = GetDlgItemInt(IDC_EDIT_EDGE_TRHESHOLD);
	m_dEdgeResolutionThreshold_Peak = GetDlgItemInt(IDC_EDIT_EDGE_TRHESHOLD_MAX);
	

	Save_parameter();

	ChangeCheck =0;
	for(int t=0; t<14; t++){
		ChangeItem[t]=-1;
	}
	for(int t=0; t<14; t++){
		m_ctrlRDDataList.Update(t);
	}

	UpdateData(FALSE);

	((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
}


void CResolutionDetection_Option::SetStandardRDZoneParameter(int NUM, int start_x, int start_y, int width, int height, int mode, int dir)
{
	if(NUM == 1)
	{
		Standard_RD_RECT[NUM].m_Left	= Standard_RD_RECT[0].m_Right + (Standard_RD_RECT[0].m_Width/2);
		Standard_RD_RECT[NUM].m_Top	= Standard_RD_RECT[0].m_Top + Standard_RD_RECT[0].m_Height;
		Standard_RD_RECT[NUM].m_Width	= Standard_RD_RECT[0].m_Width;
		Standard_RD_RECT[NUM].m_Height = Standard_RD_RECT[0].m_Height;
		Standard_RD_RECT[NUM].m_bmode = 0;
		Standard_RD_RECT[NUM].m_dir = 0;

		Standard_RD_RECT[NUM].m_Right	= Standard_RD_RECT[NUM].m_Left + Standard_RD_RECT[NUM].m_Width;
		Standard_RD_RECT[NUM].m_Bottom = Standard_RD_RECT[NUM].m_Top + Standard_RD_RECT[NUM].m_Height;
		Standard_RD_RECT[NUM].m_checkLine_Buffer_cnt = 0;
	}
	else
	{
		Standard_RD_RECT[NUM].m_Left = start_x;
		Standard_RD_RECT[NUM].m_Top = start_y;
		Standard_RD_RECT[NUM].m_Width = width;
		Standard_RD_RECT[NUM].m_Height = height;
		Standard_RD_RECT[NUM].m_bmode = mode;
		Standard_RD_RECT[NUM].m_dir = dir;

		Standard_RD_RECT[NUM].m_Right	= Standard_RD_RECT[NUM].m_Left + Standard_RD_RECT[NUM].m_Width;
		Standard_RD_RECT[NUM].m_Bottom = Standard_RD_RECT[NUM].m_Top + Standard_RD_RECT[NUM].m_Height;
		Standard_RD_RECT[NUM].m_checkLine_Buffer_cnt = 0;
	}	
}

double CResolutionDetection_Option::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt( (double)((x2-x1)*(x2-x1)) +  ((y2-y1)*(y2-y1)));

	return result;
}
int CResolutionDetection_Option::GetResultData(int NUM)
{	
	double x=0, y=0;		
	double a = -0.0009375, b = 1.0725;	//해상력 출력 이차함수 계수(중앙부분 130도 광각 렌즈)
		
	if(RD_RECT[NUM].m_CheckLine == -1)
		return 0;

	if(NUM < 6)
	{
		a = 0.0;
		b = 1;
	}
	else
	{
		a = -0.0009375;
		b = 1.0725;
	}
	
	if(NUM < 6)
	{
		if(RD_RECT[NUM].m_bmode == 0)
		{
			if(RD_RECT[NUM].m_dir == 1)
			{
				if(RD_RECT[NUM].m_CheckLine >= RD_RECT[NUM].m_dPatternEndPos.y)
					RD_RECT[NUM].m_CheckLine >= RD_RECT[NUM].m_dPatternEndPos.y;

				if(RD_RECT[NUM].m_CheckLine >= RD_RECT[NUM].m_dPatternStartPos.y && RD_RECT[NUM].m_CheckLine <= RD_RECT[NUM].m_dPatternEndPos.y )
					x = RD_RECT[NUM].m_CheckLine - RD_RECT[NUM].m_dPatternStartPos.y;
				else
					return 0;
			}
			else if(RD_RECT[NUM].m_dir == 0)
			{
				if(RD_RECT[NUM].m_CheckLine <= RD_RECT[NUM].m_dPatternEndPos.y)
					RD_RECT[NUM].m_CheckLine = RD_RECT[NUM].m_dPatternEndPos.y;

				if(RD_RECT[NUM].m_CheckLine >= RD_RECT[NUM].m_dPatternEndPos.y && RD_RECT[NUM].m_CheckLine <= RD_RECT[NUM].m_dPatternStartPos.y)
					x = RD_RECT[NUM].m_dPatternStartPos.y - RD_RECT[NUM].m_CheckLine;
				else
					return 0;
			}		
				
			y = (a * (x*x)) + (b*x);
				
			double sx1, sy1, sx2, sy2;
			sx1 = RD_RECT[NUM].m_dPatternStartPos.x;
			sy1 = RD_RECT[NUM].m_dPatternStartPos.y;
			sx2 = RD_RECT[NUM].m_dPatternEndPos.x;
			sy2 = RD_RECT[NUM].m_dPatternEndPos.y;

			double dist = sqrt( ((sx2-sx1)*(sx2-sx1)) + ((sy2-sy1)*(sy2-sy1)) );
			
			if(dist == 0)
				return 0;

			double ratio = y / dist;
			int RD_OFFSET = ratio * (MAX_RESOLUTION-MIN_RESOLUTION);
			//RD_RECT[NUM].m_ResultData = 250 + RD_OFFSET;
			
			return MIN_RESOLUTION + RD_OFFSET;
		}else{
			if(RD_RECT[NUM].m_dir == 0)
			{
				if(RD_RECT[NUM].m_CheckLine >= RD_RECT[NUM].m_dPatternEndPos.x)
					RD_RECT[NUM].m_CheckLine = RD_RECT[NUM].m_dPatternEndPos.x;

				if(RD_RECT[NUM].m_CheckLine >= RD_RECT[NUM].m_dPatternStartPos.x && RD_RECT[NUM].m_CheckLine <= RD_RECT[NUM].m_dPatternEndPos.x )
					x = RD_RECT[NUM].m_CheckLine - RD_RECT[NUM].m_dPatternStartPos.x;
				else
					return 0;
			}			
			else if(RD_RECT[NUM].m_dir == 1)
			{
				if(RD_RECT[NUM].m_CheckLine <= RD_RECT[NUM].m_dPatternEndPos.x)
					RD_RECT[NUM].m_CheckLine = RD_RECT[NUM].m_dPatternEndPos.x;

				if(RD_RECT[NUM].m_CheckLine >= RD_RECT[NUM].m_dPatternEndPos.x && RD_RECT[NUM].m_CheckLine <= RD_RECT[NUM].m_dPatternStartPos.x )
					x = RD_RECT[NUM].m_dPatternStartPos.x - RD_RECT[NUM].m_CheckLine ;
				else
					return 0;
			}
				
				
			y = (a * (x*x)) + (b*x);
				
			double sx1, sy1, sx2, sy2;
			sx1 = RD_RECT[NUM].m_dPatternStartPos.x;
			sy1 = RD_RECT[NUM].m_dPatternStartPos.y;
			sx2 = RD_RECT[NUM].m_dPatternEndPos.x;
			sy2 = RD_RECT[NUM].m_dPatternEndPos.y;

			double dist = sqrt( ((sx2-sx1)*(sx2-sx1)) + ((sy2-sy1)*(sy2-sy1)) );

			if(dist == 0)
				return 0;

			double ratio = y / dist;
			int RD_OFFSET = ratio * (MAX_RESOLUTION-MIN_RESOLUTION);
			//RD_RECT[NUM].m_ResultData = 250 + RD_OFFSET;
			
			return MIN_RESOLUTION + RD_OFFSET;
		}		
	}
	else
	{
		if(RD_RECT[NUM].m_bmode == 0)
		{
			if(RD_RECT[NUM].m_dir == 1)
			{
				if(RD_RECT[NUM].m_CheckLine >= RD_RECT[NUM].m_dPatternEndPos.y)
					RD_RECT[NUM].m_CheckLine >= RD_RECT[NUM].m_dPatternEndPos.y;

				if(RD_RECT[NUM].m_CheckLine >= RD_RECT[NUM].m_dPatternStartPos.y && RD_RECT[NUM].m_CheckLine <= RD_RECT[NUM].m_dPatternEndPos.y )
					x = RD_RECT[NUM].m_CheckLine - RD_RECT[NUM].m_dPatternStartPos.y;
				else
					return 0;
			}
			else if(RD_RECT[NUM].m_dir == 0)
			{
				if(RD_RECT[NUM].m_CheckLine <= RD_RECT[NUM].m_dPatternEndPos.y)
					RD_RECT[NUM].m_CheckLine = RD_RECT[NUM].m_dPatternEndPos.y;

				if(RD_RECT[NUM].m_CheckLine >= RD_RECT[NUM].m_dPatternEndPos.y && RD_RECT[NUM].m_CheckLine <= RD_RECT[NUM].m_dPatternStartPos.y)
					x = RD_RECT[NUM].m_dPatternStartPos.y - RD_RECT[NUM].m_CheckLine;
				else
					return 0;
			}		

			y = (a * (x*x)) + (b*x);

			double sx1, sy1, sx2, sy2;
			sx1 = RD_RECT[NUM].m_dPatternStartPos.x;
			sy1 = RD_RECT[NUM].m_dPatternStartPos.y;
			sx2 = RD_RECT[NUM].m_dPatternEndPos.x;
			sy2 = RD_RECT[NUM].m_dPatternEndPos.y;

			double dist = sqrt( ((sx2-sx1)*(sx2-sx1)) + ((sy2-sy1)*(sy2-sy1)) );

			if(dist == 0)
				return 0;

			double ratio = y / dist;
			int RD_OFFSET = ratio * (SIDE_MAX_RESOLUTION-SIDE_MIN_RESOLUTION);
			//RD_RECT[NUM].m_ResultData = 250 + RD_OFFSET;

			return SIDE_MIN_RESOLUTION + RD_OFFSET;
		}else{
			if(RD_RECT[NUM].m_dir == 0)
			{
				if(RD_RECT[NUM].m_CheckLine >= RD_RECT[NUM].m_dPatternEndPos.x)
					RD_RECT[NUM].m_CheckLine = RD_RECT[NUM].m_dPatternEndPos.x;

				if(RD_RECT[NUM].m_CheckLine >= RD_RECT[NUM].m_dPatternStartPos.x && RD_RECT[NUM].m_CheckLine <= RD_RECT[NUM].m_dPatternEndPos.x )
					x = RD_RECT[NUM].m_CheckLine - RD_RECT[NUM].m_dPatternStartPos.x;
				else
					return 0;
			}			
			else if(RD_RECT[NUM].m_dir == 1)
			{
				if(RD_RECT[NUM].m_CheckLine <= RD_RECT[NUM].m_dPatternEndPos.x)
					RD_RECT[NUM].m_CheckLine = RD_RECT[NUM].m_dPatternEndPos.x;

				if(RD_RECT[NUM].m_CheckLine >= RD_RECT[NUM].m_dPatternEndPos.x && RD_RECT[NUM].m_CheckLine <= RD_RECT[NUM].m_dPatternStartPos.x )
					x = RD_RECT[NUM].m_dPatternStartPos.x - RD_RECT[NUM].m_CheckLine ;
				else
					return 0;
			}


			y = (a * (x*x)) + (b*x);

			double sx1, sy1, sx2, sy2;
			sx1 = RD_RECT[NUM].m_dPatternStartPos.x;
			sy1 = RD_RECT[NUM].m_dPatternStartPos.y;
			sx2 = RD_RECT[NUM].m_dPatternEndPos.x;
			sy2 = RD_RECT[NUM].m_dPatternEndPos.y;

			double dist = sqrt( ((sx2-sx1)*(sx2-sx1)) + ((sy2-sy1)*(sy2-sy1)) );

			if(dist == 0)
				return 0;

			double ratio = y / dist;
			int RD_OFFSET = ratio * (SIDE_MAX_RESOLUTION-SIDE_MIN_RESOLUTION);
			//RD_RECT[NUM].m_ResultData = 250 + RD_OFFSET;

			return SIDE_MIN_RESOLUTION + RD_OFFSET;
		}		
	}
}
int CResolutionDetection_Option::GetStandardData(int NUM)
{
	double x=0, y=0;		
	
	if(NUM < 6)
	{
		if(RD_RECT[NUM].m_dir == 1)
			x = (RD_RECT[NUM].m_Thresold - MIN_RESOLUTION);			
		else if(RD_RECT[NUM].m_dir == 0)
			x = (RD_RECT[NUM].m_Thresold - MIN_RESOLUTION);
				
		y = x/(MAX_RESOLUTION - MIN_RESOLUTION);
				
		double sx1, sy1, sx2, sy2;
		sx1 = RD_RECT[NUM].m_dPatternStartPos.x;
		sy1 = RD_RECT[NUM].m_dPatternStartPos.y;
		sx2 = RD_RECT[NUM].m_dPatternEndPos.x;
		sy2 = RD_RECT[NUM].m_dPatternEndPos.y;

		double dist = sqrt( ((sx2-sx1)*(sx2-sx1)) + ((sy2-sy1)*(sy2-sy1)) );
		int RD_OFFSET = dist * y;
			
		return RD_OFFSET;
	}
	else
	{
		if(RD_RECT[NUM].m_dir == 1)
			x = (RD_RECT[NUM].m_Thresold - SIDE_MIN_RESOLUTION);			
		else if(RD_RECT[NUM].m_dir == 0)
			x = (RD_RECT[NUM].m_Thresold - SIDE_MIN_RESOLUTION);

		y = x/(SIDE_MAX_RESOLUTION - SIDE_MIN_RESOLUTION);

		double sx1, sy1, sx2, sy2;
		sx1 = RD_RECT[NUM].m_dPatternStartPos.x;
		sy1 = RD_RECT[NUM].m_dPatternStartPos.y;
		sx2 = RD_RECT[NUM].m_dPatternEndPos.x;
		sy2 = RD_RECT[NUM].m_dPatternEndPos.y;

		double dist = sqrt( ((sx2-sx1)*(sx2-sx1)) + ((sy2-sy1)*(sy2-sy1)) );
		int RD_OFFSET = dist * y;

		return RD_OFFSET;

	}
}

int CResolutionDetection_Option::GetStandardPeakData(int NUM)
{
	double x=0, y=0;		
	
	if(NUM < 6)
	{
		if(RD_RECT[NUM].m_dir == 1)
			x = (RD_RECT[NUM].m_Thresold_Peak - MIN_RESOLUTION);			
		else if(RD_RECT[NUM].m_dir == 0)
			x = (RD_RECT[NUM].m_Thresold_Peak - MIN_RESOLUTION);
				
		y = x/(MAX_RESOLUTION - MIN_RESOLUTION);
				
		double sx1, sy1, sx2, sy2;
		sx1 = RD_RECT[NUM].m_dPatternStartPos.x;
		sy1 = RD_RECT[NUM].m_dPatternStartPos.y;
		sx2 = RD_RECT[NUM].m_dPatternEndPos.x;
		sy2 = RD_RECT[NUM].m_dPatternEndPos.y;

		double dist = sqrt( ((sx2-sx1)*(sx2-sx1)) + ((sy2-sy1)*(sy2-sy1)) );
		int RD_OFFSET = dist * y;
			
		return RD_OFFSET;
	}
	else
	{
		if(RD_RECT[NUM].m_dir == 1)
			x = (RD_RECT[NUM].m_Thresold_Peak - SIDE_MIN_RESOLUTION);			
		else if(RD_RECT[NUM].m_dir == 0)
			x = (RD_RECT[NUM].m_Thresold_Peak - SIDE_MIN_RESOLUTION);

		y = x/(SIDE_MAX_RESOLUTION - SIDE_MIN_RESOLUTION);

		double sx1, sy1, sx2, sy2;
		sx1 = RD_RECT[NUM].m_dPatternStartPos.x;
		sy1 = RD_RECT[NUM].m_dPatternStartPos.y;
		sx2 = RD_RECT[NUM].m_dPatternEndPos.x;
		sy2 = RD_RECT[NUM].m_dPatternEndPos.y;

		double dist = sqrt( ((sx2-sx1)*(sx2-sx1)) + ((sy2-sy1)*(sy2-sy1)) );
		int RD_OFFSET = dist * y;

		return RD_OFFSET;

	}
}

//---------------------------------------------------------------------WORKLIST
void CResolutionDetection_Option::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	//if(LotMod ==1){
	InsertIndex = m_ResolutionList.InsertItem(StartCnt,"",0);
	//	totalint = okint + failint +1;/////////////////////////////////////////////////////////////////////////////////////////0527
	//}
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_ResolutionList.SetItemText(InsertIndex,0,strCnt);
	
	m_ResolutionList.SetItemText(InsertIndex,1,((CImageTesterDlg  *)m_pMomWnd)->m_modelName);
	m_ResolutionList.SetItemText(InsertIndex,2,((CImageTesterDlg  *)m_pMomWnd)->strDay+"");
	m_ResolutionList.SetItemText(InsertIndex,3,((CImageTesterDlg  *)m_pMomWnd)->strTime+"");

}

void CResolutionDetection_Option::Set_List(CListCtrl *List){
	CString Item[12]={"C_Left","C_Right","C_Up","C_Down","S1","S2","S3","S4"};//,"S5","S6","S7","S8"};

	while(List->DeleteColumn(0));
	((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);

	for(int t=0; t<8; t++){
		List->InsertColumn(4+t,Item[t],LVCFMT_CENTER, 80);
	}

	List->InsertColumn(12,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(13,"비고",LVCFMT_CENTER, 80);

	ListItemNum=14;
	Copy_List(&m_ResolutionList,((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList,ListItemNum);
}

CString CResolutionDetection_Option::Mes_Result()
{
	CString sz[12];
	int		nResult[12];
	CString	szResult[12];

	CString	str_tPass;
	
	// 제일 마지막 항목을 추가하자
	int nSel = m_ResolutionList.GetItemCount()-1;

	// CENTER_UP
	sz[0] = m_ResolutionList.GetItemText(nSel, 6);

	if(MES_RES_SUCC[4] == TRUE){
		szResult[0] = "1";
	}else{ 
		szResult[0] = "0";
	}
	// CENTER_DOWN
	sz[1] = m_ResolutionList.GetItemText(nSel, 7);

	if(MES_RES_SUCC[5] == TRUE){
		szResult[1] = "1";
	}else{ 
		szResult[1] = "0";
	}
	// CENTER_LEFT
	sz[2] = m_ResolutionList.GetItemText(nSel, 4);

	if(MES_RES_SUCC[2] == TRUE){
		szResult[2] = "1";
	}else{ 
		szResult[2] = "0";
	}
	// CENTER_RIGHT
	sz[3] = m_ResolutionList.GetItemText(nSel, 5);

	if(MES_RES_SUCC[3] == TRUE){
		szResult[3] = "1";
	}else{ 
		szResult[3] = "0";
	}

	//SDY 테스트
	str_tPass = m_ResolutionList.GetItemText(nSel, 12);
	if(str_tPass == "PASS"){
		szResult[0] = "1";
		szResult[1] = "1";
		szResult[2] = "1";
		szResult[3] = "1";
	}
	// S1
	sz[4] = m_ResolutionList.GetItemText(nSel, 8);

	if(MES_RES_SUCC[6] == TRUE){
		szResult[4] = "1";
	}else{ 
		szResult[4] = "0";
	}

	// S2
	sz[5] = m_ResolutionList.GetItemText(nSel, 9);

	if(MES_RES_SUCC[7] == TRUE){
		szResult[5] = "1";
	}else{ 
		szResult[5] = "0";
	}
	// S3
	sz[6] = m_ResolutionList.GetItemText(nSel, 10);

	if(MES_RES_SUCC[8] == TRUE){
		szResult[6] = "1";
	}else{ 
		szResult[6] = "0";
	}
	// S4
	sz[7] = m_ResolutionList.GetItemText(nSel, 11);

	if(MES_RES_SUCC[9] == TRUE){
		szResult[7] = "1";
	}else{ 
		szResult[7] = "0";
	}

	for(int lop = 0;lop<8;lop++){
		if(sz[lop] == 'X'){
			sz[lop] = "0";
			szResult[lop] = "1";	
		}
	}
	
	if(b_StopFail == FALSE){
		m_szMesResult.Format(_T("%s:%s,%s:%s,%s:%s,%s:%s,%s:%s,%s:%s,%s:%s,%s:%s"), sz[0], szResult[0], sz[1], szResult[1], sz[2], szResult[2], sz[3], szResult[3], sz[4], szResult[4], sz[5], szResult[5], sz[6], szResult[6], sz[7], szResult[7]);
		m_szMesResult.Replace(" ","");
	}else{
		m_szMesResult.Format(_T(":1,:1,:1,:1,:1,:1,:1,:1"));
	}

	return m_szMesResult;
}

void CResolutionDetection_Option::EXCEL_SAVE(){
	CString Item[12]={"C_Left","C_Right","C_Up","C_Down","S1","S2","S3","S4"};//,"S5","S6","S7","S8",};

	CString Data ="";
	CString str="";
	str = "***********************Resolution 검사***********************\n";
	Data += str;
	str.Empty();
	str.Format("C_Left,MTF, %6.2f ,本,%d,/,C_Right,MTF, %6.2f,本,%d,/,C_Up,MTF, %6.2f,本,%d,/,C_Down,MTF, %6.2f ,本,%d,,\n ",RD_RECT[2].MTFRatio,RD_RECT[2].m_Thresold,RD_RECT[3].MTFRatio,RD_RECT[3].m_Thresold,RD_RECT[4].MTFRatio,RD_RECT[4].m_Thresold,RD_RECT[5].MTFRatio,RD_RECT[5].m_Thresold);
	Data += str;
	str.Empty();
	str.Format("S1,MTF, %6.2f ,本,%d,/,S2,MTF, %6.2f ,本,%d,/,S3,MTF, %6.2f ,本,%d,/,S4,MTF, %6.2f,本,%d,,\n ",RD_RECT[6].MTFRatio,RD_RECT[6].m_Thresold,RD_RECT[7].MTFRatio,RD_RECT[7].m_Thresold,RD_RECT[8].MTFRatio,RD_RECT[8].m_Thresold,RD_RECT[9].MTFRatio,RD_RECT[9].m_Thresold);
	Data += str;
	str.Empty();
	str.Format("S5,MTF, %6.2f ,本,%d,/,S6,MTF,%6.2f,本,%d,/,S7,MTF, %6.2f ,本,%d,/,S8,MTF, %6.2f,本,%d,,\n ",RD_RECT[10].MTFRatio,RD_RECT[10].m_Thresold,RD_RECT[11].MTFRatio,RD_RECT[11].m_Thresold,RD_RECT[12].MTFRatio,RD_RECT[12].m_Thresold,RD_RECT[13].MTFRatio,RD_RECT[13].m_Thresold);
	Data += str;
	str.Empty();
	str = "*************************************************\n";
	Data += str;
	((CImageTesterDlg  *)m_pMomWnd)->SAVE_EXCEL("해상력검사",Item,8,&m_ResolutionList,Data);
}

bool CResolutionDetection_Option::EXCEL_UPLOAD(){
	m_ResolutionList.DeleteAllItems();
	if(((CImageTesterDlg  *)m_pMomWnd)->EXCEL_UPLOAD("해상력검사",&m_ResolutionList,1,&StartCnt)==TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return 0;
}

void CResolutionDetection_Option::OnBnClickedBtnSetZoneAuto()
{

	((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan =1;						
		
	for(int i =0;i<10;i++){
		if(((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan == 0){
			break;
		}else{
			DoEvents(50);
		}
	}

	SetZoneUsingAutoDetection(m_RGBScanbuf);

	((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan =1;
	DoEvents(200);
	((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	
}

void CResolutionDetection_Option::OnBnClickedCheckAuto()
{
	((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum =  m_INFO.Number;

	UpdateData(TRUE);
	for(int t=0; t<14; t++){
		m_ctrlRDDataList.Update(t);
	}

	CString strTitle;
	strTitle.Empty();
	strTitle="RESOLUTION_INIT";	

	//if(AUTO_MODE == 1){
	if(b_Auto_Mod == 1){
		WritePrivateProfileString(str_model,strTitle+"AUTOMODE","1",H_filename);
		(CEdit *)GetDlgItem(IDC_RE_PosX)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_PosY)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Dis_W)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Dis_H)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Width)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Height)->EnableWindow(0);

		(CEdit *)GetDlgItem(IDC_RE_PosX2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_PosY2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Dis_W2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Dis_H2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Width2)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_RE_Height2)->EnableWindow(0);
	
	}else{
		WritePrivateProfileString(str_model,strTitle+"AUTOMODE","0",H_filename);
		(CEdit *)GetDlgItem(IDC_RE_PosX)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_PosY)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Dis_W)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Dis_H)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Width)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Height)->EnableWindow(1);

		(CEdit *)GetDlgItem(IDC_RE_PosX2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_PosY2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Dis_W2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Dis_H2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Width2)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_RE_Height2)->EnableWindow(1);
	}

}

BOOL CResolutionDetection_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
			if(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->GetSafeHwnd())//GetSafeHwnd() 자신의 핸들을 가져오는 함수
			{
				int bufSubitem = iSavedSubitem + 1;
				
				if(bufSubitem ==12){
					if(iSavedItem == 13){
						iSavedItem = 0; bufSubitem =1;
					}
					else{
						iSavedItem = iSavedItem+1; bufSubitem =1;
					}
				}
				
				int bufItem = iSavedItem;
				
				m_ctrlRDDataList.EnsureVisible(iSavedItem, FALSE);
				m_ctrlRDDataList.GetSubItemRect(iSavedItem,bufSubitem , LVIR_BOUNDS, rect);
				m_ctrlRDDataList.ClientToScreen(rect);
				this->ScreenToClient(rect);
				m_ctrlRDDataList.SetSelectionMark(iSavedItem);
				m_ctrlRDDataList.SetFocus(); //저장
				iSavedItem = bufItem;
				iSavedSubitem = bufSubitem;
				((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowText(m_ctrlRDDataList.GetItemText(iSavedItem, iSavedSubitem));
				((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
				((CEdit *)GetDlgItem(IDC_EDIT_RD_MOD))->SetFocus();
			}
			
			if ((pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_RE_PosX))->GetSafeHwnd())||  
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_RE_PosY))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_RE_Dis_W))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_RE_Dis_H))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_RE_Width))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_RE_Height))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_RE_PosX2))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_RE_PosY2))->GetSafeHwnd())||  
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_RE_Dis_W2))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_RE_Dis_H2))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_RE_Width2))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_RE_Height2))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_RV))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_EDGE_TRHESHOLD))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_MTFRATIO))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_EDGE_MTF))->GetSafeHwnd()))
			{	
				OnBnClickedBtnDefault();
			}


			return TRUE;
		}

		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		/*if(pMsg->wParam == VK_ADD){
			((CImageTesterDlg  *)m_pMomWnd)->ExBtnStart();
			return TRUE;
		}
		if (pMsg->wParam == VK_SUBTRACT){
			((CImageTesterDlg  *)m_pMomWnd)->ExBtnStop();
			return TRUE;
		}
		if(pMsg->wParam == VK_MULTIPLY&& ((CImageTesterDlg  *)m_pMomWnd)->b_UserMode ==TRUE){
			((CImageTesterDlg  *)m_pMomWnd)->JIG_UP();
			
			return TRUE;
		}
		if (pMsg->wParam == VK_DIVIDE&& ((CImageTesterDlg  *)m_pMomWnd)->b_UserMode ==TRUE){
			((CImageTesterDlg  *)m_pMomWnd)->JIG_DOWN();
			return TRUE;
		}*/
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CResolutionDetection_Option::OnNMCustomdrawListRdData(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//*pResult = 0;

	 COLORREF text_color = 0;
        COLORREF bg_color = RGB(255, 255, 255);
     
        LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
		
	//	lplvcd->iPartId
        switch(lplvcd->nmcd.dwDrawStage){
            case CDDS_PREPAINT:
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
             
            // 배경 혹은 텍스트를 수정한다.
            case CDDS_ITEMPREPAINT:
                // 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
               /* if(lplvcd->nmcd.dwItemSpec == 0) text_color = RGB(255, 0, 0);
                else if(lplvcd->nmcd.dwItemSpec == 1) text_color = RGB(0, 255, 0);
                else text_color = RGB(0, 0, 255);
                lplvcd->clrText = text_color;*/
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
           
            // 서브 아이템의 배경 혹은 텍스트를 수정한다.
            case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
                if(lplvcd->iSubItem != 0){
                    // 1번째 행이라면...

					if(lplvcd->nmcd.dwItemSpec >=0 ||lplvcd->nmcd.dwItemSpec <14){
						if(b_Auto_Mod ==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 7 ){
								text_color = RGB(0, 0, 0);
								bg_color = RGB(200, 200, 200);
							}
						}
						else{
							text_color = RGB(0, 0, 0);
							bg_color = RGB(255, 255, 255);
						
						}
					
					

						if(ChangeCheck==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 13 ){
								for(int t=0; t<14; t++){
									if(lplvcd->nmcd.dwItemSpec == ChangeItem[t]){
										text_color = RGB(0, 0, 0);
										bg_color = RGB(250, 230, 200);					
									}
								}
							}
						
						
						}
					}

				    lplvcd->clrText = text_color;
                    lplvcd->clrTextBk = bg_color;
					
                }
                *pResult = CDRF_NEWFONT;
                return;
        }
}


void CResolutionDetection_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == TRUE){
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = -1;
	}
}

void CResolutionDetection_Option::OnBnClickedButtonRLoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Load_parameter();
	UpdateData(FALSE);
	Uploadlist();
}

bool CResolutionDetection_Option::GetWideCamCheckFunc(IplImage *srcImage)
{
	int org_width, org_height;

	org_width = srcImage->width;
	org_height = srcImage->height;
	
	IplImage *GrayImage = cvCreateImage(cvSize(org_width, org_height), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(org_width, org_height), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(org_width, org_height), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(org_width, org_height), IPL_DEPTH_8U, 3);
		
	cvCvtColor(srcImage, GrayImage, CV_RGB2GRAY);

	cvSmooth(GrayImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);

	cvCanny(SmoothImage, CannyImage, 0, 200);
	
	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
		
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	CvRect rect;
	double area=0, arcCount=0;
	
	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		double circularity = (4.0*3.14*area)/(arcCount*arcCount);
		
		if(circularity > 0.8)
		{			
			rect = cvContourBoundingRect(contour, 1);	
			int center_pt_x = rect.x+rect.width/2;
			int center_pt_y = rect.y+rect.height/2;
						
			cvRectangle(srcImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 255), 1, 8);					
			CString str;
			
			unsigned char R, G, B;

			B = srcImage->imageData[center_pt_y * srcImage->widthStep + 3 * center_pt_x + 0];
			G = srcImage->imageData[center_pt_y * srcImage->widthStep + 3 * center_pt_x + 1];
			R = srcImage->imageData[center_pt_y * srcImage->widthStep + 3 * center_pt_x + 2];
			
			if(B > G+30 && B > R+30)
			{
				cvReleaseMemStorage(&contour_storage);
				
				cvReleaseImage(&GrayImage);
				cvReleaseImage(&CannyImage);
				cvReleaseImage(&SmoothImage);
				cvReleaseImage(&RGBResultImage);	

				/*	delete contour;
					delete temp_contour;*/
				return TRUE;
			}			
		}

	}		
	
	cvReleaseMemStorage(&contour_storage);
	
	cvReleaseImage(&GrayImage);
	cvReleaseImage(&CannyImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);	

	//delete contour;
	//delete temp_contour;
	//
	return FALSE;
}
void CResolutionDetection_Option::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	UINT BUF = 0;
	if(nIDEvent == 150){
		KillTimer(150);
		BUF = m_ctrlRDDataList.GetItemState(iSavedItem,LVIS_STATEIMAGEMASK);
		if( BUF == 0x2000){
			RD_RECT[iSavedItem].ENABLE = TRUE;
		}else if(BUF == 0x1000){
			RD_RECT[iSavedItem].ENABLE = FALSE;
		}
	}else if(nIDEvent == 160){
		KillTimer(160);
		BUF = m_ctrlRDDataList.GetItemState(iSavedItem,LVIS_STATEIMAGEMASK);
		if( BUF == 0x2000){
			RD_RECT[iSavedItem].ENABLE = TRUE;
		}else if(BUF == 0x1000){
			RD_RECT[iSavedItem].ENABLE = FALSE;
		}
	}
	CDialog::OnTimer(nIDEvent);
}

void CResolutionDetection_Option::OnEnChangeRePosx()
{
	
	if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}

void CResolutionDetection_Option::OnEnChangeRePosy()
{
	if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}

void CResolutionDetection_Option::OnEnChangeReDisW()
{
	if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}

void CResolutionDetection_Option::OnEnChangeReDisH()
{
	
if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}
void CResolutionDetection_Option::OnEnChangeReWidth()
{
if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}

void CResolutionDetection_Option::OnEnChangeReHeight()
{
if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}

void CResolutionDetection_Option::OnEnChangeEditMtfratio()
{
if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}

void CResolutionDetection_Option::OnEnChangeEditRv()
{
if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}


void CResolutionDetection_Option::OnEnChangeRePosx2()
{
if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}

void CResolutionDetection_Option::OnEnChangeRePosy2()
{
if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}
void CResolutionDetection_Option::OnEnChangeReDisW2()
{
if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}

void CResolutionDetection_Option::OnEnChangeReDisH2()
{
	if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}
void CResolutionDetection_Option::OnEnChangeReWidth2()
{
if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}

void CResolutionDetection_Option::OnEnChangeReHeight2()
{
	if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}
void CResolutionDetection_Option::OnEnChangeEditEdgeMtf()
{
	if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}

void CResolutionDetection_Option::OnEnChangeEditEdgeTrheshold()
{
	if((E_Total_PosX >= 0)&&(E_Total_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Total_PosY >= 0)&&(E_Total_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width>=0)&&(E_Dis_Width<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height>=0)&&(E_Dis_Height<m_CAM_SIZE_HEIGHT)&&
		(E_Total_Width>=0)&&(E_Total_Width<m_CAM_SIZE_WIDTH)&&
		(E_Total_Height>=0)&&(E_Total_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dMTFRatio>=1)&&(m_dMTFRatio<=99)&&
		(m_dResolutionThreshold>=MIN_RESOLUTION)&&(m_dResolutionThreshold<=MAX_RESOLUTION)&&
		(E_Edge_PosX >= 0)&&(E_Edge_PosX < m_CAM_SIZE_WIDTH)&&
		(E_Edge_PosY >= 0)&&(E_Edge_PosY < m_CAM_SIZE_HEIGHT)&&
		(E_Dis_Width_Edge>=0)&&(E_Dis_Width_Edge<m_CAM_SIZE_WIDTH)&&
		(E_Dis_Height_Edge>=0)&&(E_Dis_Height_Edge<m_CAM_SIZE_HEIGHT)&&
		(E_Edge_Width>=0)&&(E_Edge_Width<m_CAM_SIZE_WIDTH)&&
		(E_Edge_Height>=0)&&(E_Edge_Height<m_CAM_SIZE_HEIGHT)&&
		(m_dEdgeMTFRatio>=1)&&(m_dEdgeMTFRatio<=99)&&
		(m_dEdgeResolutionThreshold>=SIDE_MIN_RESOLUTION)&&(m_dEdgeResolutionThreshold<=SIDE_MAX_RESOLUTION)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BTN_DEFAULT))->EnableWindow(0);
	}
}

void CResolutionDetection_Option::OnEnChangeEditRdMod()
{
	int num = iSavedItem;
	if((RD_RECT[num].m_PosX >= 0)&&(RD_RECT[num].m_PosX < m_CAM_SIZE_WIDTH)&&
		(RD_RECT[num].m_PosY >= 0)&&(RD_RECT[num].m_PosY < m_CAM_SIZE_HEIGHT)&&
		(RD_RECT[num].m_Left>=0)&&(RD_RECT[num].m_Left<m_CAM_SIZE_WIDTH)&&
		(RD_RECT[num].m_Top>=0)&&(RD_RECT[num].m_Top<m_CAM_SIZE_HEIGHT)&&
		(RD_RECT[num].m_Right>=0)&&(RD_RECT[num].m_Right<m_CAM_SIZE_WIDTH)&&
		(RD_RECT[num].m_Bottom>=0)&&(RD_RECT[num].m_Bottom<m_CAM_SIZE_HEIGHT)&&
		(RD_RECT[num].m_Width>=0)&&(RD_RECT[num].m_Width<m_CAM_SIZE_WIDTH)&&
		(RD_RECT[num].m_Height>=0)&&(RD_RECT[num].m_Height<m_CAM_SIZE_HEIGHT)){
					((CButton *)GetDlgItem(IDC_BTN_SET_MTF))->EnableWindow(1);
			
			if((num != 0)&&(num !=1)){
				if(num <6){
					if((RD_RECT[num].MTFRatio>=1)&&(RD_RECT[num].MTFRatio<=99)&&
						(RD_RECT[num].m_Thresold>=MIN_RESOLUTION)&&(RD_RECT[num].m_Thresold<=MAX_RESOLUTION)){
						((CButton *)GetDlgItem(IDC_BTN_SET_MTF))->EnableWindow(1);
						
					
					}else{
					((CButton *)GetDlgItem(IDC_BTN_SET_MTF))->EnableWindow(0);
						
					}
				}else{
					if((RD_RECT[num].MTFRatio>=1)&&(RD_RECT[num].MTFRatio<=99)&&
						(RD_RECT[num].m_Thresold>=SIDE_MIN_RESOLUTION)&&(RD_RECT[num].m_Thresold<=SIDE_MAX_RESOLUTION)){
						((CButton *)GetDlgItem(IDC_BTN_SET_MTF))->EnableWindow(1);
						
					
					}else{
					((CButton *)GetDlgItem(IDC_BTN_SET_MTF))->EnableWindow(0);
						
					}
				}
			}

		

	}else{
		((CButton *)GetDlgItem(IDC_BTN_SET_MTF))->EnableWindow(0);
	}
}

BOOL CResolutionDetection_Option::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
int znum = zDelta/120;
	CWnd *focusid;
	focusid = GetFocus();
	bool FLAG=0;
	if(znum > 0 ){
		FLAG = 1;
	}
	else if(znum < 0 ){
		FLAG =0;
	}
	else if(znum == 0){
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	}
	int casenum = focusid->GetDlgCtrlID();
	switch(casenum){
		case IDC_RE_PosX   :
			EditWheelIntSet(IDC_RE_PosX ,znum);
			OnEnChangeRePosx();
			break;
		case IDC_RE_PosY :
			EditWheelIntSet(IDC_RE_PosY,znum);
			OnEnChangeRePosy();
			break;
		case IDC_RE_Dis_W :
			EditWheelIntSet(IDC_RE_Dis_W ,znum);
			OnEnChangeReDisW();
			break;
		case IDC_RE_Dis_H :
			EditWheelIntSet(IDC_RE_Dis_H,znum);
			OnEnChangeReDisH();
			break;
		case  IDC_RE_Width :
			EditWheelIntSet( IDC_RE_Width,znum);
			OnEnChangeReWidth();
			break;
		case IDC_RE_Height :
			EditWheelIntSet(IDC_RE_Height,znum);
			OnEnChangeReHeight();
			break;
		case IDC_EDIT_MTFRATIO :
			EditWheelIntSet(IDC_EDIT_MTFRATIO,znum);
			OnEnChangeEditMtfratio();
			break;
		case IDC_EDIT_RV :
			EditWheelIntSet(IDC_EDIT_RV,znum);
			OnEnChangeEditRv();
			break;

		case IDC_RE_PosX2   :
			EditWheelIntSet(IDC_RE_PosX2 ,znum);
			OnEnChangeRePosx2();
			break;
		case IDC_RE_PosY2 :
			EditWheelIntSet(IDC_RE_PosY2,znum);
			OnEnChangeRePosy2();
			break;
		case IDC_RE_Dis_W2 :
			EditWheelIntSet(IDC_RE_Dis_W2 ,znum);
			OnEnChangeReDisW2();
			break;
		case IDC_RE_Dis_H2 :
			EditWheelIntSet(IDC_RE_Dis_H2,znum);
			OnEnChangeReDisH2();
			break;
		case  IDC_RE_Width2 :
			EditWheelIntSet( IDC_RE_Width2,znum);
			OnEnChangeReWidth2();
			break;
		case IDC_RE_Height2 :
			EditWheelIntSet(IDC_RE_Height2,znum);
			OnEnChangeReHeight2();
			break;
		case IDC_EDIT_EDGE_MTF :
			EditWheelIntSet(IDC_EDIT_EDGE_MTF,znum);
			OnEnChangeEditEdgeMtf();
			break;
		case IDC_EDIT_EDGE_TRHESHOLD :
			EditWheelIntSet(IDC_EDIT_EDGE_TRHESHOLD,znum);
			OnEnChangeEditEdgeTrheshold();
			break;
		
		case IDC_EDIT_RD_MOD :
			if((iSavedSubitem != 9 )&& (iSavedSubitem != 10)){
				if(FLAG == 1){
					if((Change_DATA_CHECK(1) == TRUE) /*|| (znum ==-1)*/){
						EditWheelIntSet(IDC_EDIT_RD_MOD,znum);
						Change_DATA();
						Uploadlist();
						OnEnChangeEditRdMod();
					}
				}
				if(FLAG == 0){
					if((Change_DATA_CHECK(0) == TRUE) /*|| (znum == 1)*/){
						EditWheelIntSet(IDC_EDIT_RD_MOD,znum);
						Change_DATA();
						Uploadlist();
						OnEnChangeEditRdMod();
					}
				}


				
			}

			break;
	}
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}


bool CResolutionDetection_Option::Change_DATA_CHECK(bool FLAG){

		
	BOOL STAT = TRUE;
	//상한
	if(RD_RECT[iSavedItem].m_Width > m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(RD_RECT[iSavedItem].m_Height > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(RD_RECT[iSavedItem].m_PosX > m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(RD_RECT[iSavedItem].m_PosY > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(RD_RECT[iSavedItem].m_Top > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(RD_RECT[iSavedItem].m_Bottom > m_CAM_SIZE_HEIGHT  )
		STAT = FALSE;
	if(RD_RECT[iSavedItem].m_Left> m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(RD_RECT[iSavedItem].m_Right> m_CAM_SIZE_WIDTH )
		STAT = FALSE;

	//하한

	if(RD_RECT[iSavedItem].m_Width < 1 )
		STAT = FALSE;
	if(RD_RECT[iSavedItem].m_Height < 1 )
		STAT = FALSE;
	if(RD_RECT[iSavedItem].m_PosX < 0 )
		STAT = FALSE;
	if(RD_RECT[iSavedItem].m_PosY < 0 )
		STAT = FALSE;
	if(RD_RECT[iSavedItem].m_Top < 0 )
		STAT = FALSE;
	if(RD_RECT[iSavedItem].m_Bottom < 0  )
		STAT = FALSE;
	if(RD_RECT[iSavedItem].m_Left< 0 )
		STAT = FALSE;
	if(RD_RECT[iSavedItem].m_Right< 0 )
		STAT = FALSE;

	if((iSavedItem != 0)&&(iSavedItem != 1)){

		if(iSavedItem <6){
			if(RD_RECT[iSavedItem].m_Thresold< MIN_RESOLUTION )
				STAT = FALSE;
			if(RD_RECT[iSavedItem].m_Thresold> MAX_RESOLUTION )
				STAT = FALSE;

		}else{
			if(RD_RECT[iSavedItem].m_Thresold< SIDE_MIN_RESOLUTION )
			STAT = FALSE;
			if(RD_RECT[iSavedItem].m_Thresold> SIDE_MAX_RESOLUTION )
				STAT = FALSE;
		}

		
		if(RD_RECT[iSavedItem].MTFRatio>99)
			STAT = FALSE;

		if(RD_RECT[iSavedItem].MTFRatio<0)
			STAT = FALSE;

	}
	if(FLAG  == 1){
		if(RD_RECT[iSavedItem].m_Width == 1 )
			STAT = FALSE;
		if(RD_RECT[iSavedItem].m_Height == 1 )
			STAT = FALSE;
		
	}


	return STAT;



}
void CResolutionDetection_Option::EditWheelIntSet(int nID,int Val)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = 0;
	}else{
		buf = GetDlgItemInt(nID);
		buf+= Val;
	}
	SetDlgItemInt(nID,buf,1);
	((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
}
void CResolutionDetection_Option::EditMinMaxIntSet(int nID,int* Val,int Min,int Max)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = Min;
		retval = FALSE;
	}else{
		buf = GetDlgItemInt(nID);
		if(buf<Min){
			buf = *Val;
			retval = TRUE;
		}else if(buf > Max){
			buf = *Val;
			retval = TRUE;
		}else{
			retval = FALSE;
		}
	}
	*Val = buf;
	if(retval == TRUE){
		SetDlgItemInt(nID,buf,1);
		((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
	}
}

CvPoint CResolutionDetection_Option::GetCenterPoint(LPBYTE IN_RGB)
{	
	CvPoint resultPt = cvPoint(m_CAM_SIZE_WIDTH/2, m_CAM_SIZE_HEIGHT/2);

	IplImage *OriginImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *PreprecessedImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	
	IplImage *RGBOrgImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3);

	BYTE R,G,B;
	double Sum_Y;

	for (int y=0; y<CAM_IMAGE_HEIGHT; y++)
	{
		for (int x=0; x<CAM_IMAGE_WIDTH; x++)
		{
		/*	B = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 2];
			
			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;		*/

			B = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 2];

			if( R < 100 && G < 100 && B < 100)
				OriginImage->imageData[y*OriginImage->widthStep+x] = 255;
			else
				OriginImage->imageData[y*OriginImage->widthStep+x] = 0;

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}	
	
//	cvSaveImage("C:\\CenterImage.bmp", OriginImage);
//	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
//	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);

	cvCopyImage(OriginImage, SmoothImage);
	
	cvCanny(SmoothImage, CannyImage, 0, 255);

//	cvDilate(CannyImage, DilateImage);
	
	cvCopyImage(CannyImage, temp_PatternImage);
	
	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);
	
//	cvSaveImage("C:\\CenterImage.bmp", CannyImage);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
		counter++;
	
	if(counter == 0)
	{
		cvReleaseMemStorage(&contour_storage);
		cvReleaseImage(&OriginImage);
		cvReleaseImage(&PreprecessedImage);
		cvReleaseImage(&CannyImage);	
		cvReleaseImage(&DilateImage);
		cvReleaseImage(&SmoothImage);
		cvReleaseImage(&RGBResultImage);
		cvReleaseImage(&temp_PatternImage);
		cvReleaseImage(&RGBOrgImage);

// 		delete contour;
// 		delete temp_contour;

		return resultPt;
	}

	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area=0, arcCount=0;
	counter = 0;
	double old_dist = 999999;
	
	int old_center_pos_x = 0;
	int old_center_pos_y = 0;
	int center_pt_x = 0;
	int center_pt_y = 0;
	int obj_Cnt = 0;
	int size=0, old_size = 0;	

	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;

		double circularity = (4.0*3.14*area)/(arcCount*arcCount);		

		if(circularity > 0.8)
		{
		
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;

			/*if(center_x > 160 && center_x < 560 && center_y > 40 && center_y < 440)
			{*/
			if(center_x > (m_CAM_SIZE_WIDTH*0.222) && center_x < (m_CAM_SIZE_WIDTH*0.777) && center_y > (m_CAM_SIZE_HEIGHT*0.083) && center_y < (m_CAM_SIZE_HEIGHT*0.916))
			{
				if(rect.width < m_CAM_SIZE_WIDTH/2 && rect.height < m_CAM_SIZE_HEIGHT/2)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
					
					/*center_pt_x = center_pt_x+ center_x;
					center_pt_y = center_pt_y+ center_y;
					obj_Cnt++;

					old_center_pos_x = center_pt_x/obj_Cnt;
					old_center_pos_y = center_pt_y/obj_Cnt;*/
					
					double distance = GetDistance(rect.x + rect.width/2, rect.y+rect.height/2, m_CAM_SIZE_WIDTH/2, m_CAM_SIZE_HEIGHT/2);
					
					obj_Cnt++;

					size = rect.width * rect.height;
					
					if(distance < old_dist)
					{
						old_size = size;
						resultPt.x = center_x;
						resultPt.y = center_y;
						old_dist = distance;
					}

					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 0), 1, 8);  
				
				}
			}
		}
		
		counter++;
	}
//	cvSaveImage("C:\\RGBResultImage.bmp", RGBResultImage);
	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&PreprecessedImage);
	cvReleaseImage(&CannyImage);	
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
	cvReleaseImage(&RGBOrgImage);

// 	delete contour;
// 	delete temp_contour;

	delete []rectArray;
	delete []areaArray;
	
	/*if(obj_Cnt != 0)
	{
		resultPt.x = center_pt_x / obj_Cnt;
		resultPt.y = center_pt_y / obj_Cnt;
	}*/
/*	if(obj_Cnt != 0)
	{
		resultPt.x = center_pt_x;
		resultPt.y = center_pt_y;
	}*/
		
	return resultPt;	
}
void CResolutionDetection_Option::OnEnChangeEditCentertoffset()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CResolutionDetection_Option::OnEnChangeEditEdgeoffset()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CResolutionDetection_Option::OnBnClickedButtonSaveOffset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	i_CenterOffset = atoi(str_CenterOffset);
	i_EdgeOffset = atoi(str_EdgeOffset);

	CString strTitle="";
	strTitle.Empty();
	strTitle="RESOLUTION_INIT";	

	str_CenterOffset.Format("%d",i_CenterOffset);
	str_EdgeOffset.Format("%d",i_EdgeOffset);

	UpdateData(FALSE);

	CString str ="";
	str.Empty();
	str.Format("%d",i_CenterOffset);
	WritePrivateProfileString(str_model,strTitle+"CenterOffset",str,H_filename);
	str.Empty();
	str.Format("%d",i_EdgeOffset);
	WritePrivateProfileString(str_model,strTitle+"EdgeOffset",str,H_filename);
}

void CResolutionDetection_Option::OnBnClickedButton1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CButton *)GetDlgItem(IDC_BUTTON1))->EnableWindow(0);
	((CImageTesterDlg  *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	((CButton *)GetDlgItem(IDC_BUTTON1))->EnableWindow(1);
}

void CResolutionDetection_Option::OnBnClickedCheckSetLimited()
{
	UpdateData(TRUE);
	
	if( ((CButton*)GetDlgItem(IDC_CHECK_SET_SFR))->GetCheck() )
		((CButton*)GetDlgItem(IDC_CHECK_SET_SFR))->SetCheck(0);
	else
		((CButton*)GetDlgItem(IDC_CHECK_SET_LIMITED))->SetCheck(1);
	
	CString strTitle="";
	strTitle.Empty();
	strTitle="RESOLUTION_INIT";	

	WritePrivateProfileString(str_model,strTitle+"RESOL_MODE","0",H_filename);
	m_dResol_Mode = 0;
}

void CResolutionDetection_Option::OnBnClickedCheckSetSfr()
{
	UpdateData(TRUE);

	if( ((CButton*)GetDlgItem(IDC_CHECK_SET_LIMITED))->GetCheck() )
		((CButton*)GetDlgItem(IDC_CHECK_SET_LIMITED))->SetCheck(0);
	else
		((CButton*)GetDlgItem(IDC_CHECK_SET_SFR))->SetCheck(1);
	
	CString strTitle="";
	strTitle.Empty();
	strTitle="RESOLUTION_INIT";	

	WritePrivateProfileString(str_model,strTitle+"RESOL_MODE","1",H_filename);
	m_dResol_Mode = 1;
}

void CResolutionDetection_Option::DrawSFRGraph(CDC *cdc,int NUM)
{
	if((NUM <2)&&(NUM >5)){
		return;
	}
	CString TEXTDATA;
	CPen my_pen,*old_pan, bk_pen,*old_bk_pen; 
	
	if(RD_RECT[NUM].m_ResultData >=  RD_RECT[NUM].m_Thresold){
		bk_pen.CreatePen(PS_SOLID,2,BLUE_COLOR);
		old_bk_pen = cdc->SelectObject(&bk_pen);
		cdc->SetTextColor(BLUE_COLOR);
	}else{
		bk_pen.CreatePen(PS_SOLID,2,RED_COLOR);
		old_bk_pen = cdc->SelectObject(&bk_pen);
		cdc->SetTextColor(RED_COLOR);
	}		
	
	if(NUM == 2)
	{
		cdc->Rectangle(RD_RECT[NUM].m_Left, RD_RECT[NUM].m_Top - 10, RD_RECT[NUM].m_Right, RD_RECT[NUM].m_Top - 110);
				

		for(int i=0; i<RD_RECT[NUM].SFR_Width-1; i++)
		{
			cdc->MoveTo(RD_RECT[NUM].m_Left + i,		RD_RECT[NUM].m_Top -10 - RD_RECT[NUM].SFR_MTF_Value[i]);
			cdc->LineTo(RD_RECT[NUM].m_Left + (i+1),	RD_RECT[NUM].m_Top -10 - RD_RECT[NUM].SFR_MTF_Value[i+1]);
		}			
		cdc->SelectObject(old_bk_pen);
		old_bk_pen->DeleteObject();
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for(int i=0; i<RD_RECT[NUM].SFR_Width-1; i++)
		{
			cdc->MoveTo(RD_RECT[NUM].m_Left + i,		RD_RECT[NUM].m_Top -10 - RD_RECT[NUM].SFR_MTF_LineFitting_Value[i]);
			cdc->LineTo(RD_RECT[NUM].m_Left + (i+1),	RD_RECT[NUM].m_Top -10 - RD_RECT[NUM].SFR_MTF_LineFitting_Value[i+1]);			
		}
		cdc->SelectObject(old_pan);
		old_pan->DeleteObject();
		my_pen.DeleteObject();	
	}
	else if(NUM == 3)
	{	
		cdc->Rectangle(RD_RECT[NUM].m_Right, RD_RECT[NUM].m_Bottom + 10, RD_RECT[NUM].m_Left, RD_RECT[NUM].m_Bottom + 110);


		for(int i=RD_RECT[NUM].SFR_Width-1; i>0; i--)
		{
			cdc->MoveTo(RD_RECT[NUM].m_Left + i,		RD_RECT[NUM].m_Bottom +10 + RD_RECT[NUM].SFR_MTF_Value[i]);
			cdc->LineTo(RD_RECT[NUM].m_Left + (i-1),	RD_RECT[NUM].m_Bottom +10 + RD_RECT[NUM].SFR_MTF_Value[i-1]);
		}			
		cdc->SelectObject(old_bk_pen);
		old_bk_pen->DeleteObject();
		bk_pen.DeleteObject();


		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for(int i=RD_RECT[NUM].SFR_Width-1; i>0; i--)
		{
			cdc->MoveTo(RD_RECT[NUM].m_Left + i,		RD_RECT[NUM].m_Bottom +10 + RD_RECT[NUM].SFR_MTF_LineFitting_Value[i]);
			cdc->LineTo(RD_RECT[NUM].m_Left + (i-1),	RD_RECT[NUM].m_Bottom +10 + RD_RECT[NUM].SFR_MTF_LineFitting_Value[i-1]);
		}
		cdc->SelectObject(old_pan);
		old_pan->DeleteObject();
		my_pen.DeleteObject();		
	}
	
	else if(NUM == 4)
	{	
		cdc->Rectangle(RD_RECT[NUM].m_Right+10, RD_RECT[NUM].m_Top, RD_RECT[NUM].m_Right + 110, RD_RECT[NUM].m_Bottom);


		for(int i=0; i<RD_RECT[NUM].SFR_Height-1; i++)
		{
			cdc->MoveTo(RD_RECT[NUM].m_Right +10 + RD_RECT[NUM].SFR_MTF_Value[i], RD_RECT[NUM].m_Top + i);
			cdc->LineTo(RD_RECT[NUM].m_Right +10 + RD_RECT[NUM].SFR_MTF_Value[i+1], RD_RECT[NUM].m_Top + (i+1));			
		}			
		
		cdc->SelectObject(old_bk_pen);
		old_bk_pen->DeleteObject();
		bk_pen.DeleteObject();


		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for(int i=0; i<RD_RECT[NUM].SFR_Height-1; i++)
		{
			cdc->MoveTo(RD_RECT[NUM].m_Right +10 + RD_RECT[NUM].SFR_MTF_LineFitting_Value[i],	RD_RECT[NUM].m_Top + i);
			cdc->LineTo(RD_RECT[NUM].m_Right +10 + RD_RECT[NUM].SFR_MTF_LineFitting_Value[i+1], RD_RECT[NUM].m_Top + (i+1));			
		}
		cdc->SelectObject(old_pan);
		old_pan->DeleteObject();
		my_pen.DeleteObject();		
	}

	else if(NUM == 5)
	{	
		cdc->Rectangle(RD_RECT[NUM].m_Left-10, RD_RECT[NUM].m_Top, RD_RECT[NUM].m_Left - 110, RD_RECT[NUM].m_Bottom);


		for(int i=0; i<RD_RECT[NUM].SFR_Height-1; i++)
		{
			cdc->MoveTo(RD_RECT[NUM].m_Left -10 - RD_RECT[NUM].SFR_MTF_Value[i],	RD_RECT[NUM].m_Top + i);
			cdc->LineTo(RD_RECT[NUM].m_Left -10 - RD_RECT[NUM].SFR_MTF_Value[i+1],	RD_RECT[NUM].m_Top + (i+1));
		}			
		
		cdc->SelectObject(old_bk_pen);
		old_bk_pen->DeleteObject();
		bk_pen.DeleteObject();


		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for(int i=0; i<RD_RECT[NUM].SFR_Height-1; i++)
		{
			cdc->MoveTo(RD_RECT[NUM].m_Left -10 - RD_RECT[NUM].SFR_MTF_LineFitting_Value[i],	RD_RECT[NUM].m_Top + i);
			cdc->LineTo(RD_RECT[NUM].m_Left -10 - RD_RECT[NUM].SFR_MTF_LineFitting_Value[i+1],	RD_RECT[NUM].m_Top + (i+1));			
		}
		cdc->SelectObject(old_pan);
		old_pan->DeleteObject();
		my_pen.DeleteObject();		
	}
	else if(NUM == 13)
	{	
		cdc->Rectangle(RD_RECT[NUM].m_Left-10, RD_RECT[NUM].m_Top, RD_RECT[NUM].m_Left - 110, RD_RECT[NUM].m_Bottom);


		for(int i=0; i<RD_RECT[NUM].SFR_Height-1; i++)
		{
			cdc->MoveTo(RD_RECT[NUM].m_Left -10 - RD_RECT[NUM].SFR_MTF_Value[i],	RD_RECT[NUM].m_Top + i);
			cdc->LineTo(RD_RECT[NUM].m_Left -10 - RD_RECT[NUM].SFR_MTF_Value[i+1],	RD_RECT[NUM].m_Top + (i+1));
		}			

		cdc->SelectObject(old_bk_pen);
		old_bk_pen->DeleteObject();
		bk_pen.DeleteObject();


		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for(int i=0; i<RD_RECT[NUM].SFR_Height-1; i++)
		{
			cdc->MoveTo(RD_RECT[NUM].m_Left -10 - RD_RECT[NUM].SFR_MTF_LineFitting_Value[i],	RD_RECT[NUM].m_Top + i);
			cdc->LineTo(RD_RECT[NUM].m_Left -10 - RD_RECT[NUM].SFR_MTF_LineFitting_Value[i+1],	RD_RECT[NUM].m_Top + (i+1));			
		}
		cdc->SelectObject(old_pan);
		old_pan->DeleteObject();
		my_pen.DeleteObject();		
	}
	else{
		cdc->SelectObject(old_bk_pen);
		old_bk_pen->DeleteObject();
		bk_pen.DeleteObject();

	}

	
}

int CResolutionDetection_Option::GetCheckLine_LR_DU_using_Sharpness(BYTE *BWImage, int check_mode, int Width, int Height, int NUM, int loop_cnt)
{
	BYTE *Q_BWImage = new BYTE [Width * Height * 4];
	
	int max_v=0, min_v=255, loc_index=0;
	int unsigned curr_pixel_v, next_pixel_v, direction;
	bool Detected_Flag = FALSE;
	int result_Line;
	int avg_Deviation = 0, sub_Y;
	double BW_Ratio=0.0;
	int black_line_cnt=0;
	
	IplImage *tempImage = cvCreateImage(cvSize(Width, Height), IPL_DEPTH_8U, 1);
	

	for(int x=0; x<Width; x++)
		for(int y=0; y<Height; y++)
			tempImage->imageData[y* tempImage->widthStep + x] = BWImage[(y)*(Width*4)+(x)*4];
	
//	cvSaveImage("C:\\GAUSSIAN_IMAGE.BMP", tempImage);
	
	//if(QL_IG == 0)
	//{
	//	if( RD_RECT[NUM].m_bmode == 0)
	//	{
	//		cvSmooth(tempImage, tempImage, CV_GAUSSIAN, 3, 3, 1, 1);
	//		cvSmooth(tempImage, tempImage, CV_GAUSSIAN, 3, 3, 1, 1);
	//	}
	//	else
	//	{
	//		if(loop_cnt%2 == 0)
	//			cvSmooth(tempImage, tempImage, CV_GAUSSIAN, 3, 3, 1, 1);
	//	}
	//}
	//else
	{
		cvSmooth(tempImage, tempImage, CV_GAUSSIAN, 3, 3, 1, 1);
	}
	
	for(int x=0; x<Width; x++){
		for(int y=0; y<Height; y++)
		{
			 BWImage[(y)*(Width*4)+(x)*4]		= tempImage->imageData[y*tempImage->widthStep + x];
			 BWImage[(y)*(Width*4)+(x)*4 + 1]	= tempImage->imageData[y*tempImage->widthStep + x];
			 BWImage[(y)*(Width*4)+(x)*4 + 2]	= tempImage->imageData[y*tempImage->widthStep + x];
			 BWImage[(y)*(Width*4)+(x)*4 + 3]	= tempImage->imageData[y*tempImage->widthStep + x];
		}
	}

	if(hjm_re == 1){
		cvThreshold(tempImage, tempImage, 0, 255, CV_THRESH_OTSU);
		cvCanny(tempImage, tempImage, 0, 150);	
	}else{
		//	cvThreshold(tempImage, tempImage, 0, 255, CV_THRESH_OTSU);
		cvCanny(tempImage, tempImage, 0, 150);	
	}

		
	if(check_mode == 1)	// 상->하로 체크
	{
		bool weight_line_flag = FALSE;

		for (int x=0; x<Width; x++)
		{
			for (int y=0; y<Height-1; y++)
			{
				curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
				
				if(curr_pixel_v > m_White_Ref_Value)
					curr_pixel_v = m_White_Ref_Value;
				if(curr_pixel_v < m_Black_Ref_Value)
					curr_pixel_v = m_Black_Ref_Value;

		//		curr_pixel_v = (double)((double)(curr_pixel_v - m_Black_Ref_Value)/(double)(m_White_Ref_Value-m_Black_Ref_Value)) * (double)m_White_Ref_Value;
							

				BWImage[(y)*(Width*4)+(x)*4] = curr_pixel_v;
				BWImage[(y)*(Width*4)+(x)*4 + 1] = curr_pixel_v;
				BWImage[(y)*(Width*4)+(x)*4 + 2] = curr_pixel_v;
				
				unsigned int EdgeValue = tempImage->imageData[y * tempImage->widthStep + x];

				if(curr_pixel_v > EdgeValue)
				{
					Q_BWImage[(y)*(Width*4)+(x*4)] = 255;
					Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 255;
					Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 255;
				}
				else
				{
					Q_BWImage[(y)*(Width*4)+(x*4)] = 0;
					Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 0;
					Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 0;
				}

			}		
		}		

	//	IplImage *SFRImage = cvCreateImage(cvSize(Width, 100), IPL_DEPTH_8U, 3);
	//	IplImage *SFRAreaImage = cvCreateImage(cvSize(Width, Height), IPL_DEPTH_8U, 3);

	//	cvSetZero(SFRImage);
	//	cvSetZero(SFRAreaImage);
		
		CvPoint oldSFRValPos = cvPoint( 0, 0 );
		
		if(RD_RECT[NUM].SFR_MTF_Value == NULL)
		{
			RD_RECT[NUM].SFR_Width = Width;
			RD_RECT[NUM].SFR_Height = 100;

			RD_RECT[NUM].SFR_MTF_Value = new double [Width];
			RD_RECT[NUM].SFR_MTF_LineFitting_Value = new double [Width];
			RD_RECT[NUM].SFR_BWRatio = new double [Width];
			
			for(int k=0; k<Width; k++)
			{
				RD_RECT[NUM].SFR_MTF_Value[k] = 0;
				RD_RECT[NUM].SFR_MTF_LineFitting_Value[k] = 0;
				RD_RECT[NUM].SFR_BWRatio[k] = 0;
			}
		}
		else
		{
			if(RD_RECT[NUM].SFR_Width != Width)
			{
				RD_RECT[NUM].SFR_Width = Width;
				RD_RECT[NUM].SFR_Height = 100;
				
				delete []RD_RECT[NUM].SFR_MTF_Value;
				delete []RD_RECT[NUM].SFR_MTF_LineFitting_Value;
				delete []RD_RECT[NUM].SFR_BWRatio;

				RD_RECT[NUM].SFR_MTF_Value = new double [Width];
				RD_RECT[NUM].SFR_MTF_LineFitting_Value = new double [Width];
				RD_RECT[NUM].SFR_BWRatio = new double [Width];
				

				for(int k=0; k<Width; k++)
				{
					RD_RECT[NUM].SFR_MTF_Value[k] = 0;
					RD_RECT[NUM].SFR_MTF_LineFitting_Value[k] = 0;
					RD_RECT[NUM].SFR_BWRatio[k] = 0;
				}
			}
			else
			{
				RD_RECT[NUM].SFR_Width = Width;
				RD_RECT[NUM].SFR_Height = 100;

				delete []RD_RECT[NUM].SFR_MTF_Value;
				delete []RD_RECT[NUM].SFR_MTF_LineFitting_Value;
				delete []RD_RECT[NUM].SFR_BWRatio;

				RD_RECT[NUM].SFR_MTF_Value = new double [Width];
				RD_RECT[NUM].SFR_MTF_LineFitting_Value = new double [Width];
				RD_RECT[NUM].SFR_BWRatio = new double [Width];


				for(int k=0; k<Width; k++)
				{
					RD_RECT[NUM].SFR_MTF_Value[k] = 0;
					RD_RECT[NUM].SFR_MTF_LineFitting_Value[k] = 0;
					RD_RECT[NUM].SFR_BWRatio[k] = 0;
				}
			}
		}

		for (int x=0; x<Width; x++)
		{
			avg_Deviation = 0;
			
			int subCnt = 0;			
			int StartY_pos=-1, EndY_pos=-1;

			for(int y=0; y<Height; y++)
			{
				if(Q_BWImage[(y)*(Width*4)+(x*4)] == 0)
				{
					StartY_pos = y;
					break;
				}
			}
			for(int y=Height-1; y>0; y--)
			{
				if(Q_BWImage[(y)*(Width*4)+(x*4)] == 0)
				{
					EndY_pos = y;
					break;
				}
			}
			
			if(StartY_pos != -1 && EndY_pos != -1 && (EndY_pos - StartY_pos) != 0)
			{
				int Dmax=0, Dmin=255;
				int meanVal = 0;
				
		//		IplImage *LinePairImage = cvCreateImage(cvSize((EndY_pos-StartY_pos), 255), IPL_DEPTH_8U, 3);
		//		cvSetZero(LinePairImage);

				for (int y=StartY_pos+1; y<EndY_pos-1; y++)
				{
					curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];

					if(curr_pixel_v > Dmax)
						Dmax = curr_pixel_v;
					if(curr_pixel_v < Dmin)
						Dmin = curr_pixel_v;					

		//			cvLine(LinePairImage, cvPoint(y, 0), cvPoint(y, curr_pixel_v), CV_RGB(255, 255, 255), 1, 8);					
				}
				
		//		cvFlip(LinePairImage, LinePairImage);
	//			cvSaveImage("C:\\LinePairImage.bmp", LinePairImage);
				
		//		cvReleaseImage(&LinePairImage);

				for(int y=StartY_pos+1; y<EndY_pos-1; y++)
				{
					curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];

					if(curr_pixel_v > (Dmax + Dmin)/2.0)
					{
						Q_BWImage[(y)*(Width*4)+(x*4)] = 255;
						Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 255;
						Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 255;
					}
					else
					{
						Q_BWImage[(y)*(Width*4)+(x*4)] = 0;
						Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 0;
						Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 0;
					}
				}
				
				for(int y=0; y<StartY_pos; y++)
				{					
					Q_BWImage[(y)*(Width*4)+(x*4)] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+1] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+2] = 127;
				}
				for(int y=Height-1; y>EndY_pos; y--)
				{
					Q_BWImage[(y)*(Width*4)+(x*4)] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+1] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+2] = 127;
				}				

				Dmax = 0;
				Dmin = 0;
				int DmaxCnt = 0;
				int DminCnt = 0;

				for (int y=StartY_pos; y<EndY_pos; y++)
				{					
					unsigned int curr_Q_pixel_v = Q_BWImage[(y)*(Width*4)+(x*4)];
					curr_pixel_v = BWImage[(y)*(Width*4)+(x*4)];

					if(curr_Q_pixel_v == 255)
					{
						Dmax += curr_pixel_v;
						DmaxCnt++;
					}
					else if(curr_Q_pixel_v == 0)
					{
						Dmin += curr_pixel_v;
						DminCnt++;
					}
				}				

				if(DmaxCnt > DminCnt)
					BW_Ratio = (double)DminCnt/(double)DmaxCnt;
				else
					BW_Ratio = (double)DmaxCnt/(double)DminCnt;				


				if(DmaxCnt != 0 && DminCnt != 0)
				{
					RD_RECT[NUM].SFR_BWRatio[x] = BW_Ratio;
  
					Dmax = Dmax / (DmaxCnt);
					Dmin = Dmin / (DminCnt);

					double Ri = (double)(m_White_Ref_Value - m_Black_Ref_Value) / (double)(m_White_Ref_Value + m_Black_Ref_Value);
					double Ro = (double)(Dmax - Dmin) / (double)(Dmax + Dmin);
					double MTF = Ro / Ri;
				
			//		cvLine(SFRImage, oldSFRValPos, cvPoint(x, (MTF*100.0)), CV_RGB(255, 255, 255), 1, 8);
			//		cvCircle(SFRImage, cvPoint(x, (MTF*100.0)), 1, CV_RGB(255, 0, 0), 1, 8);
					oldSFRValPos = cvPoint(x, (MTF*100.0));
					
					if(RD_RECT[NUM].SFR_BWRatio[x] > 0.5)
						RD_RECT[NUM].SFR_MTF_Value[x] = (MTF*100.0);
					else
						RD_RECT[NUM].SFR_MTF_Value[x] = (MTF*0.0);
				}
			}			
		}
		
		double sum_square_x=0, sum_x=0, sum_xy=0, sum_y=0, sum_1=0;
		double a, b;
		double inverse_x11, inverse_x12, inverse_x21, inverse_x22, inverse_param;
		
		int SFR_range = abs(RD_RECT[NUM].m_dPatternStartPos.x - RD_RECT[NUM].m_dPatternEndPos.x);

		for (int x=RD_RECT[NUM].m_dPatternStartPos.x; x<RD_RECT[NUM].m_dPatternStartPos.x+((double)SFR_range*0.8); x++)	
		{	
			if(RD_RECT[NUM].SFR_BWRatio[x] > 0.5)
			{
				sum_square_x += x*x;
				sum_x += x;
				sum_1 += 1;
				sum_y += RD_RECT[NUM].SFR_MTF_Value[x];
				sum_xy += x * RD_RECT[NUM].SFR_MTF_Value[x];
			}
		}
		
		inverse_param = 1.0 / ((sum_square_x*sum_1)-(sum_x*sum_x));
		inverse_x11 = inverse_param*sum_1;
		inverse_x12 = (inverse_param*sum_x) * -1;
		inverse_x21 = (inverse_param*sum_x) * -1;
		inverse_x22 = (inverse_param*sum_square_x);

		a = (inverse_x11*sum_xy) + (inverse_x12*sum_y);
		b = (inverse_x21*sum_xy) + (inverse_x22*sum_y);

		for (int x=RD_RECT[NUM].m_dPatternStartPos.x; x<RD_RECT[NUM].m_dPatternEndPos.x; x++)
		{
			double y = a*x + b;

			if(x > RD_RECT[NUM].m_dPatternStartPos.x)
			{
				double curr_y, before_y;
				
				curr_y = y;
				before_y = RD_RECT[NUM].SFR_MTF_LineFitting_Value[x-1];

				if(curr_y >= before_y)
				{
					y = 0.0;//before_y-0.1;
				}
			}

			RD_RECT[NUM].SFR_MTF_LineFitting_Value[x] = y;
		}
		
		for (int x=RD_RECT[NUM].m_dPatternStartPos.x; x<RD_RECT[NUM].m_dPatternEndPos.x; x++)
		{
			if(100.0 - (RD_RECT[NUM].MTFRatio) < RD_RECT[NUM].SFR_MTF_LineFitting_Value[x])
			{
				result_Line = x;
				Detected_Flag = TRUE;				
			}
		}

//		Capture_Gray_SAVE("C:\\Mean_Image", Q_BWImage, Width, Height);
//		Capture_Gray_SAVE("C:\\BW_Image", BWImage, Width, Height);
		
	//	cvFlip(SFRImage, SFRImage);
		
	//	cvSaveImage("C:\\SFRImage.bmp", SFRImage);
		

		if(Detected_Flag == FALSE)
			result_Line = 1;

	//	cvReleaseImage(&SFRImage);
	//	cvReleaseImage(&SFRAreaImage);		
	}
	else if(check_mode == 0)	// 좌->우로 체크
	{		
		bool weight_line_flag = FALSE;

		for (int y=Height-1; y>-1; y--)
		{
			int subCnt = 0;			
			int StartY_pos=-1, EndY_pos=-1;

			for (int x=0; x<Width; x++)
			{
				curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
				
				if(curr_pixel_v > m_White_Ref_Value)
					BWImage[(y)*(Width*4)+(x)*4] = m_White_Ref_Value;
				if(curr_pixel_v < m_Black_Ref_Value)
					BWImage[(y)*(Width*4)+(x)*4] = m_Black_Ref_Value;		

				BWImage[(y)*(Width*4)+(x)*4] = curr_pixel_v;
				BWImage[(y)*(Width*4)+(x)*4 + 1] = curr_pixel_v;
				BWImage[(y)*(Width*4)+(x)*4 + 2] = curr_pixel_v;

				unsigned int EdgeValue = tempImage->imageData[y * tempImage->widthStep + x];

				if(curr_pixel_v > EdgeValue)
				{
					Q_BWImage[(y)*(Width*4)+(x*4)] = 255;
					Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 255;
					Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 255;
				}
				else
				{
					Q_BWImage[(y)*(Width*4)+(x*4)] = 0;
					Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 0;
					Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 0;
				}
			}
		}
			
		CvPoint oldSFRValPos = cvPoint( 0, 0 );
	
		if(RD_RECT[NUM].SFR_MTF_Value == NULL)
		{
			RD_RECT[NUM].SFR_Width = 100;
			RD_RECT[NUM].SFR_Height = Height;

			RD_RECT[NUM].SFR_MTF_Value = new double [Height];
			RD_RECT[NUM].SFR_MTF_LineFitting_Value = new double [Height];
			RD_RECT[NUM].SFR_BWRatio = new double [Height];

			for(int k=0; k<Height; k++)
			{
				RD_RECT[NUM].SFR_MTF_Value[k] = 0;
				RD_RECT[NUM].SFR_MTF_LineFitting_Value[k] = 0;
				RD_RECT[NUM].SFR_BWRatio[k] = 0;
			}
		}
		else
		{
			if(RD_RECT[NUM].SFR_Height != Height)
			{
				RD_RECT[NUM].SFR_Width = 100;
				RD_RECT[NUM].SFR_Height = Height;
				
				delete []RD_RECT[NUM].SFR_MTF_Value;
				delete []RD_RECT[NUM].SFR_MTF_LineFitting_Value;
				delete []RD_RECT[NUM].SFR_BWRatio;

				RD_RECT[NUM].SFR_MTF_Value = new double [Height];
				RD_RECT[NUM].SFR_MTF_LineFitting_Value = new double [Height];
				RD_RECT[NUM].SFR_BWRatio = new double [Height];

				for(int k=0; k<Height; k++)
				{
					RD_RECT[NUM].SFR_MTF_Value[k] = 0;
					RD_RECT[NUM].SFR_MTF_LineFitting_Value[k] = 0;
					RD_RECT[NUM].SFR_BWRatio[k] = 0;
				}
			}
			else
			{
				RD_RECT[NUM].SFR_Width = 100;
				RD_RECT[NUM].SFR_Height = Height;

				delete []RD_RECT[NUM].SFR_MTF_Value;
				delete []RD_RECT[NUM].SFR_MTF_LineFitting_Value;
				delete []RD_RECT[NUM].SFR_BWRatio;

				RD_RECT[NUM].SFR_MTF_Value = new double [Height];
				RD_RECT[NUM].SFR_MTF_LineFitting_Value = new double [Height];
				RD_RECT[NUM].SFR_BWRatio = new double [Height];

				for(int k=0; k<Height; k++)
				{
					RD_RECT[NUM].SFR_MTF_Value[k] = 0;
					RD_RECT[NUM].SFR_MTF_LineFitting_Value[k] = 0;
					RD_RECT[NUM].SFR_BWRatio[k] = 0;
				}
			}
		}


		for(int y=Height-1; y>0; y--)
		{
			int subCnt = 0;			
			int StartY_pos=-1, EndY_pos=-1;

			for(int x=0; x<Width; x++)
			{
				if(Q_BWImage[(y)*(Width*4)+(x*4)] == 0)
				{
					StartY_pos = x;
					break;
				}
			}
			for(int x=Width-1; x>0; x--)
			{
				if(Q_BWImage[(y)*(Width*4)+(x*4)] == 0)
				{
					EndY_pos = x;
					break;
				}
			}
		

			if(StartY_pos != -1 && EndY_pos != -1 && (EndY_pos - StartY_pos) != 0)
			{
				int Dmax=0, Dmin=255;
				int meanVal = 0;
				
			//	IplImage *LinePairImage = cvCreateImage(cvSize((EndY_pos-StartY_pos), 255), IPL_DEPTH_8U, 3);
			//	cvSetZero(LinePairImage);

				for (int x=StartY_pos+1; x<EndY_pos-1; x++)
				{
					curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];

					if(curr_pixel_v > Dmax)
						Dmax = curr_pixel_v;
					if(curr_pixel_v < Dmin)
						Dmin = curr_pixel_v;					

			//		cvLine(LinePairImage, cvPoint(x, 0), cvPoint(x, curr_pixel_v), CV_RGB(255, 255, 255), 1, 8);					
				}
				
		//		Capture_Gray_SAVE("C:\\DU_BWIMAGE", BWImage, Width, Height);

		//		cvFlip(LinePairImage, LinePairImage);
		//		cvSaveImage("C:\\DU_LinePairImage.bmp", LinePairImage);
				
		//		cvReleaseImage(&LinePairImage);

				for(int x=StartY_pos+1; x<EndY_pos-1; x++)
				{
					curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];

					if(curr_pixel_v > (Dmax + Dmin)/2.0)
					{
						Q_BWImage[(y)*(Width*4)+(x*4)] = 255;
						Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 255;
						Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 255;
					}
					else
					{
						Q_BWImage[(y)*(Width*4)+(x*4)] = 0;
						Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 0;
						Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 0;
					}
				}
				
				for(int x=0; x<StartY_pos; x++)
				{					
					Q_BWImage[(y)*(Width*4)+(x*4)] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+1] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+2] = 127;
				}
				for(int x=Width-1; x>EndY_pos; x--)
				{
					Q_BWImage[(y)*(Width*4)+(x*4)] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+1] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+2] = 127;
				}				

				Dmax = 0;
				Dmin = 0;
				int DmaxCnt = 0;
				int DminCnt = 0;

				for (int x=StartY_pos; x<EndY_pos; x++)
				{					
					unsigned int curr_Q_pixel_v = Q_BWImage[(y)*(Width*4)+(x*4)];
					curr_pixel_v = BWImage[(y)*(Width*4)+(x*4)];

					if(curr_Q_pixel_v == 255)
					{
						Dmax += curr_pixel_v;
						DmaxCnt++;
					}
					else if(curr_Q_pixel_v == 0)
					{
						Dmin += curr_pixel_v;
						DminCnt++;
					}
				}
				
				if(DmaxCnt > DminCnt)
					BW_Ratio = (double)DminCnt/(double)DmaxCnt;
				else
					BW_Ratio = (double)DmaxCnt/(double)DminCnt;	

				if(DmaxCnt != 0 && DminCnt != 0)
				{
					RD_RECT[NUM].SFR_BWRatio[y] = BW_Ratio;

					Dmax = Dmax / (DmaxCnt);
					Dmin = Dmin / (DminCnt);

					double Ri = (double)(m_White_Ref_Value - m_Black_Ref_Value) / (double)(m_White_Ref_Value + m_Black_Ref_Value);
					double Ro = (double)(Dmax - Dmin) / (double)(Dmax + Dmin);
					double MTF = Ro / Ri;
				
			//		cvLine(SFRImage, oldSFRValPos, cvPoint(x, (MTF*100.0)), CV_RGB(255, 255, 255), 1, 8);
			//		cvCircle(SFRImage, cvPoint(x, (MTF*100.0)), 1, CV_RGB(255, 0, 0), 1, 8);
					oldSFRValPos = cvPoint((MTF*100.0), y);
					
					if(RD_RECT[NUM].SFR_BWRatio[y] > 0.5)
						RD_RECT[NUM].SFR_MTF_Value[y] = (MTF*100.0);
					else
						RD_RECT[NUM].SFR_MTF_Value[y] = (MTF*0.0);					
				}
			}			
		}
		
		double sum_square_x=0, sum_x=0, sum_xy=0, sum_y=0, sum_1=0;
		double a, b;
		double inverse_x11, inverse_x12, inverse_x21, inverse_x22, inverse_param;
		
		int SFR_range = abs(RD_RECT[NUM].m_dPatternStartPos.y - RD_RECT[NUM].m_dPatternEndPos.y);

		for (int y=RD_RECT[NUM].m_dPatternStartPos.y; y>RD_RECT[NUM].m_dPatternStartPos.y-((double)SFR_range*0.8); y--)	//약 75% 지점까지 감안한다..
		{	
			if(RD_RECT[NUM].SFR_BWRatio[y] > 0.5)
			{
				sum_square_x += y*y;
				sum_x += y;			
				sum_1 += 1;
				sum_y += RD_RECT[NUM].SFR_MTF_Value[y];
				sum_xy += y * RD_RECT[NUM].SFR_MTF_Value[y];
			}
		}
		
		inverse_param = 1.0 / ((sum_square_x*sum_1)-(sum_x*sum_x));
		inverse_x11 = inverse_param*sum_1;
		inverse_x12 = (inverse_param*sum_x) * -1;
		inverse_x21 = (inverse_param*sum_x) * -1;
		inverse_x22 = (inverse_param*sum_square_x);

		a = (inverse_x11*sum_xy) + (inverse_x12*sum_y);
		b = (inverse_x21*sum_xy) + (inverse_x22*sum_y);

		for (int y=RD_RECT[NUM].m_dPatternStartPos.y; y>RD_RECT[NUM].m_dPatternEndPos.y; y--)
		{			
			double x = a*y + b;

			if(y < RD_RECT[NUM].m_dPatternStartPos.y)
			{
				double curr_x, before_x;
				
				curr_x = x;
				before_x = RD_RECT[NUM].SFR_MTF_LineFitting_Value[y+1];

				if(curr_x >= before_x)
				{
					x = 0.0;//before_x-0.1;
				}
			}

			RD_RECT[NUM].SFR_MTF_LineFitting_Value[y] = x;
		}
		
		for (int y=RD_RECT[NUM].m_dPatternStartPos.y; y>RD_RECT[NUM].m_dPatternEndPos.y; y--)
		{
			if(100.0 - (RD_RECT[NUM].MTFRatio) < RD_RECT[NUM].SFR_MTF_LineFitting_Value[y])
			{
				result_Line = y;
				Detected_Flag = TRUE;
			}
		}

	//	Capture_Gray_SAVE("C:\\DU_Mean_Image", Q_BWImage, Width, Height);
	//	Capture_Gray_SAVE("C:\\DU_BW_Image", BWImage, Width, Height);
		
	//	cvFlip(SFRImage, SFRImage);
		
	//	cvSaveImage("C:\\SFRImage.bmp", SFRImage);

		if(Detected_Flag == FALSE)
			result_Line = Height-1;
	}

	delete []Q_BWImage;
	cvReleaseImage(&tempImage);	
	
	return result_Line;
}

int CResolutionDetection_Option::GetCheckLine_RL_UD_using_Sharpness(BYTE *BWImage, int check_mode, int Width, int Height, int NUM, int loop_cnt)
{
	BYTE *Q_BWImage = new BYTE [Width * Height * 4];
	
	int max_v=0, min_v=255, loc_index=0;
	int unsigned curr_pixel_v, next_pixel_v, direction;
	bool Detected_Flag = FALSE;
	int result_Line;
	int avg_Deviation = 0, sub_Y;
	double BW_Ratio=0.0;
	int black_line_cnt = 0;

	IplImage *tempImage = cvCreateImage(cvSize(Width, Height), IPL_DEPTH_8U, 1);

	for(int x=0; x<Width; x++)
		for(int y=0; y<Height; y++)
			tempImage->imageData[y* tempImage->widthStep + x] = BWImage[(y)*(Width*4)+(x)*4];
	
	//if(QL_IG == 0)
	//{
	//	if( RD_RECT[NUM].m_bmode == 0)
	//	{
	//		cvSmooth(tempImage, tempImage, CV_GAUSSIAN, 3, 3, 1, 1);
	//		cvSmooth(tempImage, tempImage, CV_GAUSSIAN, 3, 3, 1, 1);
	//	}
	//	else
	//	{
	//		if(loop_cnt%2 == 0)
	//			cvSmooth(tempImage, tempImage, CV_GAUSSIAN, 3, 3, 1, 1);
	//	}
	//}
	//else
	{
		cvSmooth(tempImage, tempImage, CV_GAUSSIAN, 3, 3, 1, 1);
	}
	
	for(int x=0; x<Width; x++){
		for(int y=0; y<Height; y++)
		{
			 BWImage[(y)*(Width*4)+(x)*4]		= tempImage->imageData[y*tempImage->widthStep + x];
			 BWImage[(y)*(Width*4)+(x)*4 + 1]	= tempImage->imageData[y*tempImage->widthStep + x];
			 BWImage[(y)*(Width*4)+(x)*4 + 2]	= tempImage->imageData[y*tempImage->widthStep + x];
			 BWImage[(y)*(Width*4)+(x)*4 + 3]	= tempImage->imageData[y*tempImage->widthStep + x];
		}
	}
	
	if(hjm_re == 1){
		cvThreshold(tempImage, tempImage, 0, 255, CV_THRESH_OTSU);
		cvCanny(tempImage, tempImage, 0, 150);	
	}else{
		//	cvThreshold(tempImage, tempImage, 0, 255, CV_THRESH_OTSU);
		cvCanny(tempImage, tempImage, 0, 150);	
	}	
	
	if(check_mode == 1)	// 상->하로 체크
	{
		bool weight_line_flag = FALSE;

		for (int x=RD_RECT[NUM].m_dPatternStartPos.x; x>RD_RECT[NUM].m_dPatternEndPos.x; x--)
		{
			for (int y=0; y<Height-1; y++)
			{
				curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
				
				if(curr_pixel_v > m_White_Ref_Value)
					curr_pixel_v = m_White_Ref_Value;
				if(curr_pixel_v < m_Black_Ref_Value)
					curr_pixel_v = m_Black_Ref_Value;

		//		curr_pixel_v = (double)((double)(curr_pixel_v - m_Black_Ref_Value)/(double)(m_White_Ref_Value-m_Black_Ref_Value)) * (double)m_White_Ref_Value;
							

				BWImage[(y)*(Width*4)+(x)*4] = curr_pixel_v;
				BWImage[(y)*(Width*4)+(x)*4 + 1] = curr_pixel_v;
				BWImage[(y)*(Width*4)+(x)*4 + 2] = curr_pixel_v;

				unsigned int EdgeValue = tempImage->imageData[y * tempImage->widthStep + x];

				if(curr_pixel_v > EdgeValue)
				{
					Q_BWImage[(y)*(Width*4)+(x*4)] = 255;
					Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 255;
					Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 255;
				}
				else
				{
					Q_BWImage[(y)*(Width*4)+(x*4)] = 0;
					Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 0;
					Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 0;
				}

			}		
		}		

	//	IplImage *SFRImage = cvCreateImage(cvSize(Width, 100), IPL_DEPTH_8U, 3);
	//	IplImage *SFRAreaImage = cvCreateImage(cvSize(Width, Height), IPL_DEPTH_8U, 3);

	//	cvSetZero(SFRImage);
	//	cvSetZero(SFRAreaImage);
		
		CvPoint oldSFRValPos = cvPoint( 0, 0 );
		
		if(RD_RECT[NUM].SFR_MTF_Value == NULL)
		{
			RD_RECT[NUM].SFR_Width = Width;
			RD_RECT[NUM].SFR_Height = 100;

			RD_RECT[NUM].SFR_MTF_Value = new double [Width];
			RD_RECT[NUM].SFR_MTF_LineFitting_Value = new double [Width];
			RD_RECT[NUM].SFR_BWRatio = new double [Width];

			for(int k=0; k<Width; k++)
			{
				RD_RECT[NUM].SFR_MTF_Value[k] = 0;
				RD_RECT[NUM].SFR_MTF_LineFitting_Value[k] = 0;
				RD_RECT[NUM].SFR_BWRatio[k] = 0;
			}
		}
		else
		{
			if(RD_RECT[NUM].SFR_Width != Width)
			{
				RD_RECT[NUM].SFR_Width = Width;
				RD_RECT[NUM].SFR_Height = 100;
				
				delete []RD_RECT[NUM].SFR_MTF_Value;
				delete []RD_RECT[NUM].SFR_MTF_LineFitting_Value;
				delete []RD_RECT[NUM].SFR_BWRatio;

				RD_RECT[NUM].SFR_MTF_Value = new double [Width];
				RD_RECT[NUM].SFR_MTF_LineFitting_Value = new double [Width];
				RD_RECT[NUM].SFR_BWRatio = new double [Width];

				for(int k=0; k<Width; k++)
				{
					RD_RECT[NUM].SFR_MTF_Value[k] = 0;
					RD_RECT[NUM].SFR_MTF_LineFitting_Value[k] = 0;
					RD_RECT[NUM].SFR_BWRatio[k] = 0;
				}
			}
			else
			{
				RD_RECT[NUM].SFR_Width = Width;
				RD_RECT[NUM].SFR_Height = 100;

				delete []RD_RECT[NUM].SFR_MTF_Value;
				delete []RD_RECT[NUM].SFR_MTF_LineFitting_Value;
				delete []RD_RECT[NUM].SFR_BWRatio;

				RD_RECT[NUM].SFR_MTF_Value = new double [Width];
				RD_RECT[NUM].SFR_MTF_LineFitting_Value = new double [Width];
				RD_RECT[NUM].SFR_BWRatio = new double [Width];

				for(int k=0; k<Width; k++)
				{
					RD_RECT[NUM].SFR_MTF_Value[k] = 0;
					RD_RECT[NUM].SFR_MTF_LineFitting_Value[k] = 0;
					RD_RECT[NUM].SFR_BWRatio[k] = 0;
				}
			}
		}

		for (int x=RD_RECT[NUM].m_dPatternStartPos.x; x>RD_RECT[NUM].m_dPatternEndPos.x; x--)
		{
			avg_Deviation = 0;
			
			int subCnt = 0;			
			int StartY_pos=-1, EndY_pos=-1;

			for(int y=0; y<Height; y++)
			{
				if(Q_BWImage[(y)*(Width*4)+(x*4)] == 0)
				{
					StartY_pos = y;
					break;
				}
			}
			for(int y=Height-1; y>0; y--)
			{
				if(Q_BWImage[(y)*(Width*4)+(x*4)] == 0)
				{
					EndY_pos = y;
					break;
				}
			}
			
			if(StartY_pos != -1 && EndY_pos != -1 && (EndY_pos - StartY_pos) != 0)
			{
				int Dmax=0, Dmin=255;
				int meanVal = 0;
				
			//	IplImage *LinePairImage = cvCreateImage(cvSize((EndY_pos-StartY_pos), 255), IPL_DEPTH_8U, 3);
			//	cvSetZero(LinePairImage);

				for (int y=StartY_pos+1; y<EndY_pos-1; y++)
				{
					curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];

					if(curr_pixel_v > Dmax)
						Dmax = curr_pixel_v;
					if(curr_pixel_v < Dmin)
						Dmin = curr_pixel_v;					

		//			cvLine(LinePairImage, cvPoint(y, 0), cvPoint(y, curr_pixel_v), CV_RGB(255, 255, 255), 1, 8);					
				}
				
			//	cvFlip(LinePairImage, LinePairImage);
			//	cvSaveImage("C:\\LinePairImage.bmp", LinePairImage);
				
		//		cvReleaseImage(&LinePairImage);

				for(int y=StartY_pos+1; y<EndY_pos-1; y++)
				{
					curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];

					if(curr_pixel_v > (Dmax + Dmin)/2.0)
					{
						Q_BWImage[(y)*(Width*4)+(x*4)] = 255;
						Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 255;
						Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 255;
					}
					else
					{
						Q_BWImage[(y)*(Width*4)+(x*4)] = 0;
						Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 0;
						Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 0;
					}
				}
				
				for(int y=0; y<StartY_pos; y++)
				{					
					Q_BWImage[(y)*(Width*4)+(x*4)] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+1] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+2] = 127;
				}
				for(int y=Height-1; y>EndY_pos; y--)
				{
					Q_BWImage[(y)*(Width*4)+(x*4)] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+1] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+2] = 127;
				}				

				Dmax = 0;
				Dmin = 0;
				int DmaxCnt = 0;
				int DminCnt = 0;

				for (int y=StartY_pos; y<EndY_pos; y++)
				{					
					unsigned int curr_Q_pixel_v = Q_BWImage[(y)*(Width*4)+(x*4)];
					curr_pixel_v = BWImage[(y)*(Width*4)+(x*4)];

					if(curr_Q_pixel_v == 255)
					{
						Dmax += curr_pixel_v;
						DmaxCnt++;
					}
					else if(curr_Q_pixel_v == 0)
					{
						Dmin += curr_pixel_v;
						DminCnt++;
					}
				}
				
				if(DmaxCnt > DminCnt)
					BW_Ratio = (double)DminCnt/(double)DmaxCnt;
				else
					BW_Ratio = (double)DmaxCnt/(double)DminCnt;

				if(DmaxCnt != 0 && DminCnt != 0)
				{
					RD_RECT[NUM].SFR_BWRatio[x] = BW_Ratio;

					Dmax = Dmax / (DmaxCnt);
					Dmin = Dmin / (DminCnt);

					double Ri = (double)(m_White_Ref_Value - m_Black_Ref_Value) / (double)(m_White_Ref_Value + m_Black_Ref_Value);
					double Ro = (double)(Dmax - Dmin) / (double)(Dmax + Dmin);
					double MTF = Ro / Ri;
				
			//		cvLine(SFRImage, oldSFRValPos, cvPoint(x, (MTF*100.0)), CV_RGB(255, 255, 255), 1, 8);
			//		cvCircle(SFRImage, cvPoint(x, (MTF*100.0)), 1, CV_RGB(255, 0, 0), 1, 8);
					oldSFRValPos = cvPoint(x, (MTF*100.0));
					
					if(RD_RECT[NUM].SFR_BWRatio[x] > 0.5)
						RD_RECT[NUM].SFR_MTF_Value[x] = (MTF*100.0);
					else
						RD_RECT[NUM].SFR_MTF_Value[x] = (MTF*0.0);				
					
				}
			}			
		}
		
		double sum_square_x=0, sum_x=0, sum_xy=0, sum_y=0, sum_1=0;
		double a, b;
		double inverse_x11, inverse_x12, inverse_x21, inverse_x22, inverse_param;
	
		int SFR_range = abs(RD_RECT[NUM].m_dPatternStartPos.x - RD_RECT[NUM].m_dPatternEndPos.x);

		for (int x=RD_RECT[NUM].m_dPatternStartPos.x; x>RD_RECT[NUM].m_dPatternStartPos.x-((double)SFR_range*0.8); x--)//약75% 지점까지감안한다..
		{
			if( RD_RECT[NUM].SFR_MTF_Value[x] > 0.5)
			{
				sum_square_x += x*x;
				sum_x += x;			
				sum_1 += 1;
				sum_y += RD_RECT[NUM].SFR_MTF_Value[x];
				sum_xy += x * RD_RECT[NUM].SFR_MTF_Value[x];
			}
		}
		
		inverse_param = 1.0 / ((sum_square_x*sum_1)-(sum_x*sum_x));
		inverse_x11 = inverse_param*sum_1;
		inverse_x12 = (inverse_param*sum_x) * -1;
		inverse_x21 = (inverse_param*sum_x) * -1;
		inverse_x22 = (inverse_param*sum_square_x);

		a = (inverse_x11*sum_xy) + (inverse_x12*sum_y);
		b = (inverse_x21*sum_xy) + (inverse_x22*sum_y);

		for (int x=RD_RECT[NUM].m_dPatternStartPos.x; x>RD_RECT[NUM].m_dPatternEndPos.x; x--)
		{
			double y = a*x + b;
			
			if(x < RD_RECT[NUM].m_dPatternStartPos.x)
			{
				double curr_y, before_y;
				
				curr_y = y;
				before_y = RD_RECT[NUM].SFR_MTF_LineFitting_Value[x+1];

				if(curr_y >= before_y)
				{
					y = 0.0;//before_y-0.1;
				}
			}

			RD_RECT[NUM].SFR_MTF_LineFitting_Value[x] = y;
		}
		
		for (int x=RD_RECT[NUM].m_dPatternStartPos.x; x>RD_RECT[NUM].m_dPatternEndPos.x; x--)
		{
			if(100.0 - (RD_RECT[NUM].MTFRatio) < RD_RECT[NUM].SFR_MTF_LineFitting_Value[x])
			{
				result_Line = x;
				Detected_Flag = TRUE;
			}
		}


	//	Capture_Gray_SAVE("C:\\Mean_Image", Q_BWImage, Width, Height);
	//	Capture_Gray_SAVE("C:\\BW_Image", BWImage, Width, Height);
		
	//	cvFlip(SFRImage, SFRImage);
		
	//	cvSaveImage("C:\\SFRImage.bmp", SFRImage);
		

		if(Detected_Flag == FALSE)
			result_Line = Width - 1;

	//	cvReleaseImage(&SFRImage);
	//	cvReleaseImage(&SFRAreaImage);		
	}
	else if(check_mode == 0)	// 좌->우로 체크
	{		
		bool weight_line_flag = FALSE;

		for (int y=0; y<Height; y++)
		{
			int subCnt = 0;			
			int StartY_pos=-1, EndY_pos=-1;

			for (int x=0; x<Width; x++)
			{
				curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];
				
				if(curr_pixel_v > m_White_Ref_Value)
					BWImage[(y)*(Width*4)+(x)*4] = m_White_Ref_Value;
				if(curr_pixel_v < m_Black_Ref_Value)
					BWImage[(y)*(Width*4)+(x)*4] = m_Black_Ref_Value;		

				BWImage[(y)*(Width*4)+(x)*4] = curr_pixel_v;
				BWImage[(y)*(Width*4)+(x)*4 + 1] = curr_pixel_v;
				BWImage[(y)*(Width*4)+(x)*4 + 2] = curr_pixel_v;

				unsigned int EdgeValue = tempImage->imageData[y * tempImage->widthStep + x];

				if(curr_pixel_v > EdgeValue)
				{
					Q_BWImage[(y)*(Width*4)+(x*4)] = 255;
					Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 255;
					Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 255;
				}
				else
				{
					Q_BWImage[(y)*(Width*4)+(x*4)] = 0;
					Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 0;
					Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 0;
				}
			}
		}
			
		CvPoint oldSFRValPos = cvPoint( 0, 0 );
	
		if(RD_RECT[NUM].SFR_MTF_Value == NULL)
		{
			RD_RECT[NUM].SFR_Width = 100;
			RD_RECT[NUM].SFR_Height = Height;

			RD_RECT[NUM].SFR_MTF_Value = new double [Height];
			RD_RECT[NUM].SFR_MTF_LineFitting_Value = new double [Height];
			RD_RECT[NUM].SFR_BWRatio = new double [Height];

			for(int k=0; k<Height; k++)
			{
				RD_RECT[NUM].SFR_MTF_Value[k] = 0;
				RD_RECT[NUM].SFR_MTF_LineFitting_Value[k] = 0;
				RD_RECT[NUM].SFR_BWRatio[k] = 0;
			}
		}
		else
		{		
			if(RD_RECT[NUM].SFR_Height != Height)
			{
				RD_RECT[NUM].SFR_Width = 100;
				RD_RECT[NUM].SFR_Height = Height;
				
				delete []RD_RECT[NUM].SFR_MTF_Value;
				delete []RD_RECT[NUM].SFR_MTF_LineFitting_Value;		
				delete []RD_RECT[NUM].SFR_BWRatio;	

				RD_RECT[NUM].SFR_MTF_Value = new double [Height];
				RD_RECT[NUM].SFR_MTF_LineFitting_Value = new double [Height];
				RD_RECT[NUM].SFR_BWRatio = new double [Height];

				for(int k=0; k<Height; k++)
				{
					RD_RECT[NUM].SFR_MTF_Value[k] = 0;
					RD_RECT[NUM].SFR_MTF_LineFitting_Value[k] = 0;
					RD_RECT[NUM].SFR_BWRatio[k] = 0;
				}
			}
			else
			{
				RD_RECT[NUM].SFR_Width = 100;
				RD_RECT[NUM].SFR_Height = Height;

				delete []RD_RECT[NUM].SFR_MTF_Value;
				delete []RD_RECT[NUM].SFR_MTF_LineFitting_Value;		
				delete []RD_RECT[NUM].SFR_BWRatio;	

				RD_RECT[NUM].SFR_MTF_Value = new double [Height];
				RD_RECT[NUM].SFR_MTF_LineFitting_Value = new double [Height];
				RD_RECT[NUM].SFR_BWRatio = new double [Height];

				for(int k=0; k<Height; k++)
				{
					RD_RECT[NUM].SFR_MTF_Value[k] = 0;
					RD_RECT[NUM].SFR_MTF_LineFitting_Value[k] = 0;
					RD_RECT[NUM].SFR_BWRatio[k] = 0;
				}
			}
		}


		for(int y=0; y<Height; y++)
		{
			int subCnt = 0;			
			int StartY_pos=-1, EndY_pos=-1;

			for(int x=0; x<Width; x++)
			{
				if(Q_BWImage[(y)*(Width*4)+(x*4)] == 0)
				{
					StartY_pos = x;
					break;
				}
			}
			for(int x=Width-1; x>0; x--)
			{
				if(Q_BWImage[(y)*(Width*4)+(x*4)] == 0)
				{
					EndY_pos = x;
					break;
				}
			}
		

			if(StartY_pos != -1 && EndY_pos != -1 && (EndY_pos - StartY_pos) != 0)
			{
				int Dmax=0, Dmin=255;
				int meanVal = 0;
				
			//	IplImage *LinePairImage = cvCreateImage(cvSize((EndY_pos-StartY_pos), 255), IPL_DEPTH_8U, 3);
			//	cvSetZero(LinePairImage);

				for (int x=StartY_pos+1; x<EndY_pos-1; x++)
				{
					curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];

					if(curr_pixel_v > Dmax)
						Dmax = curr_pixel_v;
					if(curr_pixel_v < Dmin)
						Dmin = curr_pixel_v;					

			//		cvLine(LinePairImage, cvPoint(x, 0), cvPoint(x, curr_pixel_v), CV_RGB(255, 255, 255), 1, 8);					
				}
				
			//	Capture_Gray_SAVE("C:\\DU_BWIMAGE", BWImage, Width, Height);

			//	cvFlip(LinePairImage, LinePairImage);
			//	cvSaveImage("C:\\DU_LinePairImage.bmp", LinePairImage);
				
			//	cvReleaseImage(&LinePairImage);

				for(int x=StartY_pos+1; x<EndY_pos-1; x++)
				{
					curr_pixel_v = BWImage[(y)*(Width*4)+(x)*4];

					if(curr_pixel_v > (Dmax + Dmin)/2.0)
					{
						Q_BWImage[(y)*(Width*4)+(x*4)] = 255;
						Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 255;
						Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 255;
					}
					else
					{
						Q_BWImage[(y)*(Width*4)+(x*4)] = 0;
						Q_BWImage[(y)*(Width*4)+(x*4) + 1] = 0;
						Q_BWImage[(y)*(Width*4)+(x*4) + 2] = 0;
					}
				}
				
				for(int x=0; x<StartY_pos; x++)
				{					
					Q_BWImage[(y)*(Width*4)+(x*4)] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+1] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+2] = 127;
				}
				for(int x=Width-1; x>EndY_pos; x--)
				{
					Q_BWImage[(y)*(Width*4)+(x*4)] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+1] = 127;
					Q_BWImage[(y)*(Width*4)+(x*4)+2] = 127;
				}				

				Dmax = 0;
				Dmin = 0;
				int DmaxCnt = 0;
				int DminCnt = 0;

				for (int x=StartY_pos; x<EndY_pos; x++)
				{					
					unsigned int curr_Q_pixel_v = Q_BWImage[(y)*(Width*4)+(x*4)];
					curr_pixel_v = BWImage[(y)*(Width*4)+(x*4)];

					if(curr_Q_pixel_v == 255)
					{
						Dmax += curr_pixel_v;
						DmaxCnt++;
					}
					else if(curr_Q_pixel_v == 0)
					{
						Dmin += curr_pixel_v;
						DminCnt++;
					}
				}
				
				if(DmaxCnt > DminCnt)
					BW_Ratio = (double)DminCnt/(double)DmaxCnt;
				else
					BW_Ratio = (double)DmaxCnt/(double)DminCnt;

				if(DmaxCnt != 0 && DminCnt != 0)
				{
					RD_RECT[NUM].SFR_BWRatio[y] = BW_Ratio;

					Dmax = Dmax / (DmaxCnt);
					Dmin = Dmin / (DminCnt);

					double Ri = (double)(m_White_Ref_Value - m_Black_Ref_Value) / (double)(m_White_Ref_Value + m_Black_Ref_Value);
					double Ro = (double)(Dmax - Dmin) / (double)(Dmax + Dmin);
					double MTF = Ro / Ri;
				
			//		cvLine(SFRImage, oldSFRValPos, cvPoint(x, (MTF*100.0)), CV_RGB(255, 255, 255), 1, 8);
			//		cvCircle(SFRImage, cvPoint(x, (MTF*100.0)), 1, CV_RGB(255, 0, 0), 1, 8);
					oldSFRValPos = cvPoint((MTF*100.0), y);
					
					RD_RECT[NUM].SFR_MTF_Value[y] = (MTF*100.0);

					if(RD_RECT[NUM].SFR_BWRatio[y] > 0.5)
						RD_RECT[NUM].SFR_MTF_Value[y] = (MTF*100.0);
					else
						RD_RECT[NUM].SFR_MTF_Value[y] = (MTF*0.0);
					
				}
			}			
		}
		
		double sum_square_x=0, sum_x=0, sum_xy=0, sum_y=0, sum_1=0;
		double a, b;
		double inverse_x11, inverse_x12, inverse_x21, inverse_x22, inverse_param;
		
		int SFR_range = abs(RD_RECT[NUM].m_dPatternStartPos.y - RD_RECT[NUM].m_dPatternEndPos.y);

		for (int y=RD_RECT[NUM].m_dPatternStartPos.y; y<RD_RECT[NUM].m_dPatternStartPos.y+((double)SFR_range*0.8); y++)	//약75% 지점까지감안한다..
		{	
			if(RD_RECT[NUM].SFR_BWRatio[y] > 0.5)
			{
				sum_square_x += y*y;
				sum_x += y;			
				sum_1 += 1;
				sum_y += RD_RECT[NUM].SFR_MTF_Value[y];
				sum_xy += y * RD_RECT[NUM].SFR_MTF_Value[y];
			}
		}
		
		inverse_param = 1.0 / ((sum_square_x*sum_1)-(sum_x*sum_x));
		inverse_x11 = inverse_param*sum_1;
		inverse_x12 = (inverse_param*sum_x) * -1;
		inverse_x21 = (inverse_param*sum_x) * -1;
		inverse_x22 = (inverse_param*sum_square_x);

		a = (inverse_x11*sum_xy) + (inverse_x12*sum_y);
		b = (inverse_x21*sum_xy) + (inverse_x22*sum_y);

		for (int y=RD_RECT[NUM].m_dPatternStartPos.y; y<RD_RECT[NUM].m_dPatternEndPos.y; y++)
		{
			double x = a*y + b;

			if(y > RD_RECT[NUM].m_dPatternStartPos.y)
			{
				double curr_x, before_x;
				
				curr_x = x;
				before_x = RD_RECT[NUM].SFR_MTF_LineFitting_Value[y-1];

				if(curr_x >= before_x)
				{
					x = 0.0;//before_x-0.1;
				}
			}

			RD_RECT[NUM].SFR_MTF_LineFitting_Value[y] = x;
		}
		
		for (int y=RD_RECT[NUM].m_dPatternStartPos.y; y<RD_RECT[NUM].m_dPatternEndPos.y; y++)
		{
			if(100.0 - (RD_RECT[NUM].MTFRatio) < RD_RECT[NUM].SFR_MTF_LineFitting_Value[y])
			{
				result_Line = y;
				Detected_Flag = TRUE;
			}
		}


//	Capture_Gray_SAVE("C:\\DU_Mean_Image", Q_BWImage, Width, Height);
//	Capture_Gray_SAVE("C:\\DU_BW_Image", BWImage, Width, Height);
	
//	cvFlip(SFRImage, SFRImage);
	
//	cvSaveImage("C:\\SFRImage.bmp", SFRImage);

	if(Detected_Flag == FALSE)
		result_Line = 1;
	}

	delete []Q_BWImage;
	cvReleaseImage(&tempImage);
	
	return result_Line;
}
void CResolutionDetection_Option::OnBnClickedCheck190f()
{
	CString strTitle="";

	strTitle.Empty();
	strTitle="RESOLUTION_INIT";	
	
	UpdateData(TRUE);

	if( ((CButton*)GetDlgItem(IDC_CHECK_190F))->GetCheck() )
	{
		m_b190Angle_Field = TRUE;
		WritePrivateProfileString(str_model,strTitle+"190F","1",H_filename);
	}
	else
	{
		m_b190Angle_Field = TRUE;
		WritePrivateProfileString(str_model,strTitle+"190F","0",H_filename);
	}	
}


#pragma region LOT관련 함수
void CResolutionDetection_Option::LOT_Set_List(CListCtrl *List){
	CString Item[12]={"C_Left","C_Right","C_Up","C_Down","S1","S2","S3","S4"};


	while(List->DeleteColumn(0));

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);


	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT 명",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);

	for(int t=0; t<8; t++){
		List->InsertColumn(5+t,Item[t],LVCFMT_CENTER, 80);
	}

	List->InsertColumn(13,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(14,"비고",LVCFMT_CENTER, 80);

	Lot_InsertNum =15;

}

void CResolutionDetection_Option::LOT_InsertDataList(){
	CString strCnt="";

	int Index = m_Lot_ResolutionList.InsertItem(Lot_StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index+1);
	m_Lot_ResolutionList.SetItemText(Index,0,strCnt);

	int CopyIndex = m_ResolutionList.GetItemCount()-1;

	int count =0;
	for(int t=0; t< Lot_InsertNum;t++){
		if((t != 2)&&(t!=0)){
			m_Lot_ResolutionList.SetItemText(Index,t, m_ResolutionList.GetItemText(CopyIndex,count));
			count++;

		}else if(t==0){
			count++;
		}else{
			m_Lot_ResolutionList.SetItemText(Index,t,((CImageTesterDlg  *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;

}
void CResolutionDetection_Option::LOT_EXCEL_SAVE(){
	//		CString Item[12]={"C_Left","C_Right","C_Up","C_Down","S1","S2","S3","S4","S5","S6","S7","S8",};
	CString Item[12]={"C_Left","C_Right","C_Up","C_Down","S1","S2","S3","S4",};

	CString Data ="";
	CString str="";
	str = "***********************Resolution 검사***********************\n";
	Data += str;
	str.Empty();
	str.Format("C_Left,MTF, %6.2f ,本,%d,/,C_Right,MTF, %6.2f,本,%d,/,C_Up,MTF, %6.2f,本,%d,/,C_Down,MTF, %6.2f ,本,%d,,\n ",RD_RECT[2].MTFRatio,RD_RECT[2].m_Thresold,RD_RECT[3].MTFRatio,RD_RECT[3].m_Thresold,RD_RECT[4].MTFRatio,RD_RECT[4].m_Thresold,RD_RECT[5].MTFRatio,RD_RECT[5].m_Thresold);
	Data += str;
	str.Empty();
	str.Format("S1,MTF, %6.2f ,本,%d,/,S2,MTF, %6.2f ,本,%d,/,S3,MTF, %6.2f ,本,%d,/,S4,MTF, %6.2f,本,%d,,\n ",RD_RECT[6].MTFRatio,RD_RECT[6].m_Thresold,RD_RECT[7].MTFRatio,RD_RECT[7].m_Thresold,RD_RECT[8].MTFRatio,RD_RECT[8].m_Thresold,RD_RECT[9].MTFRatio,RD_RECT[9].m_Thresold);
	Data += str;
	str.Empty();
	str = "*************************************************\n";
	Data += str;
	//	((CImageTesterDlg  *)m_pMomWnd)->LOT_SAVE_EXCEL("해상력검사",Item,12,&m_Lot_ResolutionList,Data);
	((CImageTesterDlg  *)m_pMomWnd)->LOT_SAVE_EXCEL("해상력검사",Item,8,&m_Lot_ResolutionList,Data);
}

bool CResolutionDetection_Option::LOT_EXCEL_UPLOAD(){
	m_Lot_ResolutionList.DeleteAllItems();
	if(((CImageTesterDlg  *)m_pMomWnd)->LOT_EXCEL_UPLOAD("해상력검사",&m_Lot_ResolutionList,1,&Lot_StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}

#pragma endregion