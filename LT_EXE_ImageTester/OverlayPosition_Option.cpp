// OverlayPosition_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "OverlayPosition_Option.h"

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4]; 

// COverlayPosition_Option 대화 상자입니다.
IMPLEMENT_DYNAMIC(COverlayPosition_Option, CDialog)

COverlayPosition_Option::COverlayPosition_Option(CWnd* pParent /*=NULL*/)
	: CDialog(COverlayPosition_Option::IDD, pParent)
	, m_OFFSET_LEFT(_T(""))
	, m_OFFSET_RIGHT(_T(""))
{
	m_nDistance = 0;
	m_nCutOff = 0;
	m_offset_left =0;
	m_offset_right =0;

	m_nLeftX = 0;
	m_nLeftY = 0;
	m_nRightX = 0;
	m_nRightY = 0;

	Lot_StartCnt = 0;
	b_StopFail = FALSE;

	iSavedItem=0;
	iSavedSubitem=0;
	for(int t=0; t<Rect_FullNum; t++){
		ChangeItem[t]=-1;
	}
	changecount=0;
}

COverlayPosition_Option::~COverlayPosition_Option()
{
}

void COverlayPosition_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_OVERLAY, m_OverlayList);
	DDX_Text(pDX, IDC_EDIT_SET_DISTANCE,		str_OverlayPos);
	DDX_Text(pDX, IDC_EDIT_SET_OVERLAY_CUTOFF,	str_OverCutOff);

	DDX_Text(pDX, IDC_EDIT_OVERLAY_LEFT_X,		str_OverlayLeftX);
	DDX_Text(pDX, IDC_EDIT_OVERLAY_LEFT_Y,		str_OverlayLeftY);

	DDX_Text(pDX, IDC_EDIT_OVERLAY_RIGHT_X,		str_OverlayRightX);
	DDX_Text(pDX, IDC_EDIT_OVERLAY_RIGHT_Y,		str_OverlayRightY);
	DDX_Text(pDX, IDC_EDIT_LEFT, m_OFFSET_LEFT);
	DDX_Text(pDX, IDC_EDIT_RIGHT, m_OFFSET_RIGHT);
	DDX_Control(pDX, IDC_LIST_RECT, m_RectList);
}

BEGIN_MESSAGE_MAP(COverlayPosition_Option, CDialog)
	ON_WM_CTLCOLOR()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()

	ON_BN_CLICKED(IDC_BTN_OVERLAY_POSITION_SAVE, &COverlayPosition_Option::OnBnClickedBtnOverlayPositionSave)
	ON_BN_CLICKED(IDC_BTN_OVERLAY_TEST, &COverlayPosition_Option::OnBnClickedBtnOverlayTest)
	ON_EN_CHANGE(IDC_EDIT_SET_DISTANCE, &COverlayPosition_Option::OnEnChangeEditSetDistance)
	ON_EN_CHANGE(IDC_EDIT_SET_OVERLAY_CUTOFF, &COverlayPosition_Option::OnEnChangeEditSetOverlayCutoff)
	ON_BN_CLICKED(IDC_BTN_OVERLAY_POINT_SAVE, &COverlayPosition_Option::OnBnClickedBtnOverlayPointSave)
	ON_EN_CHANGE(IDC_EDIT_OVERLAY_LEFT_X, &COverlayPosition_Option::OnEnChangeEditOverlayLeftX)
	ON_EN_CHANGE(IDC_EDIT_OVERLAY_LEFT_Y, &COverlayPosition_Option::OnEnChangeEditOverlayLeftY)
	ON_EN_CHANGE(IDC_EDIT_OVERLAY_RIGHT_X, &COverlayPosition_Option::OnEnChangeEditOverlayRightX)
	ON_EN_CHANGE(IDC_EDIT_OVERLAY_RIGHT_Y, &COverlayPosition_Option::OnEnChangeEditOverlayRightY)
	ON_BN_CLICKED(IDC_BTN_OFFSET_SAVE2, &COverlayPosition_Option::OnBnClickedBtnOffsetSave2)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &COverlayPosition_Option::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_LOAD, &COverlayPosition_Option::OnBnClickedButtonLoad)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_RECT, &COverlayPosition_Option::OnNMDblclkListRect)
	ON_EN_KILLFOCUS(IDC_EDIT_RECTLIST, &COverlayPosition_Option::OnEnKillfocusEditRectlist)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_RECT, &COverlayPosition_Option::OnNMCustomdrawListRect)
	ON_WM_MOUSEWHEEL()
	ON_EN_SETFOCUS(IDC_EDIT_RECTLIST, &COverlayPosition_Option::OnEnSetfocusEditRectlist)
END_MESSAGE_MAP()


// COverlayPosition_Option 메시지 처리기입니다.
BOOL COverlayPosition_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	H_filename =((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	
	Rect_SETLIST();

	InitPrm();
	Set_List(&m_OverlayList);
	InitEVMS();



	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void COverlayPosition_Option::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}

void COverlayPosition_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void COverlayPosition_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
	strTitle.Empty();
	strTitle="OVERLAYPOSITIONOPT_";
}

void COverlayPosition_Option::Pic(CDC *cdc)
{
	for(int _a=0; _a<2; _a++)
		OverlayPic(cdc, _a);
}

void COverlayPosition_Option::InitPrm()
{
	Load_parameter();

	for(int _a=0; _a<2; _a++)
	{
		m_param[_a].m_PosX = 0;
		m_param[_a].m_PosY = 0;
		m_param[_a].m_Success = 0;
	}
	UpdateData(FALSE);
}

void COverlayPosition_Option::InitUI()
{
	((CButton*)GetDlgItem(IDC_BTN_OVERLAY_POSITION_SAVE))->EnableWindow(FALSE);
	((CButton*)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(FALSE);
}

void COverlayPosition_Option::Save(int NUM)
{
	WritePrivateProfileString(str_model,NULL,"",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

void COverlayPosition_Option::Save_parameter()
{
	CString str="";
	WritePrivateProfileString(str_model,NULL,"",H_filename);
	str.Empty();
	str.Format("%d",m_INFO.ID);
	WritePrivateProfileString(str_model,"ID",str,H_filename);
	str.Empty();
	str.Format("%s",m_INFO.MODE);
	WritePrivateProfileString(str_model,"MODE",str,H_filename);
	str.Empty();
	str.Format("%s",m_INFO.NAME);
	WritePrivateProfileString(str_model,"NAME",str,H_filename);

	str.Empty();
	str.Format("%d",m_nDistance);
	WritePrivateProfileString(str_model,strTitle+"OVERLAY_DISTANCE",str,H_filename);
	str_OverlayPos = str;

	str.Empty();
	str.Format("%d",m_nCutOff);
	WritePrivateProfileString(str_model,strTitle+"OVERLAY_CUTOFF",str,H_filename);
	str_OverCutOff = str;

	// leftX
	str.Empty();
	str.Format("%d",m_nLeftX);
	WritePrivateProfileString(str_model,strTitle+"OVERLAY_POS_LEFT_X",str,H_filename);
	str_OverlayLeftX = str;

	// leftY
	str.Empty();
	str.Format("%d",m_nLeftY);
	WritePrivateProfileString(str_model,strTitle+"OVERLAY_POS_LEFT_Y",str,H_filename);
	str_OverlayLeftY = str;

	// rightX
	str.Empty();
	str.Format("%d",m_nRightX);
	WritePrivateProfileString(str_model,strTitle+"OVERLAY_POS_RIGHT_X",str,H_filename);
	str_OverlayRightX = str;

	// rightY
	str.Empty();
	str.Format("%d",m_nRightY);
	WritePrivateProfileString(str_model,strTitle+"OVERLAY_POS_RIGHT_Y",str,H_filename);
	str_OverlayRightY = str;



	str.Empty();
	str.Format("%d",m_offset_left);
	WritePrivateProfileString(str_model,strTitle+"OFFSET_LEFT",str,H_filename);
	m_OFFSET_LEFT = str;

	str.Empty();
	str.Format("%d",m_offset_right);
	WritePrivateProfileString(str_model,strTitle+"OFFSET_RIGHT",str,H_filename);
	m_OFFSET_RIGHT = str;

}


void COverlayPosition_Option::Load_parameter()
{
	CString str ="";

	m_nDistance = GetPrivateProfileInt(str_model, strTitle+"OVERLAY_DISTANCE", -1, H_filename);
	if(m_nDistance == -1)
	{
		m_nDistance = 200;
		str.Empty();
		str.Format("%d",m_nDistance);
		WritePrivateProfileString(str_model,strTitle+"OVERLAY_DISTANCE",str,H_filename);
	}

	m_nCutOff = GetPrivateProfileInt(str_model, strTitle+"OVERLAY_CUTOFF", -1, H_filename);
	if(m_nCutOff == -1)
	{
		m_nCutOff = 10;
		str.Empty();
		str.Format("%d",m_nCutOff);
		WritePrivateProfileString(str_model,strTitle+"OVERLAY_DISTANCE",str,H_filename);
	}

	str_OverlayPos.Format(_T("%d"), m_nDistance);
	str_OverCutOff.Format(_T("%d"), m_nCutOff);


	m_offset_left = GetPrivateProfileInt(str_model, strTitle+"OFFSET_LEFT", -1, H_filename);
	if(m_offset_left == -1)
	{
		m_offset_left = 0;
		str.Empty();
		str.Format("%d",m_offset_left);
		WritePrivateProfileString(str_model,strTitle+"OFFSET_LEFT",str,H_filename);
	}

	m_offset_right = GetPrivateProfileInt(str_model, strTitle+"OFFSET_RIGHT", -1, H_filename);
	if(m_offset_right == -1)
	{
		m_offset_right = 0;
		str.Empty();
		str.Format("%d",m_offset_right);
		WritePrivateProfileString(str_model,strTitle+"OFFSET_RIGHT",str,H_filename);
	}

	m_OFFSET_LEFT.Format(_T("%d"), m_offset_left);
	m_OFFSET_RIGHT.Format(_T("%d"), m_offset_right);

	// leftX
	m_nLeftX = GetPrivateProfileInt(str_model, strTitle+"OVERLAY_POS_LEFT_X", -1, H_filename);
	if(m_nLeftX == -1)
	{
		m_nLeftX = CAM_IMAGE_WIDTH/2;
		str.Empty();
		str.Format("%d",m_nLeftX);
		WritePrivateProfileString(str_model,strTitle+"OVERLAY_POS_LEFT_X",str,H_filename);
	}
	
	str_OverlayLeftX.Format("%d", m_nLeftX);

	// leftY
	m_nLeftY = GetPrivateProfileInt(str_model, strTitle+"OVERLAY_POS_LEFT_Y", -1, H_filename);
	if(m_nLeftY == -1)
	{
		m_nLeftY = CAM_IMAGE_HEIGHT/2;
		str.Empty();
		str.Format("%d",m_nLeftY);
		WritePrivateProfileString(str_model,strTitle+"OVERLAY_POS_LEFT_Y",str,H_filename);
	}

	str_OverlayLeftY.Format("%d", m_nLeftY);

	// rightX
	m_nRightX = GetPrivateProfileInt(str_model, strTitle+"OVERLAY_POS_RIGHT_X", -1, H_filename);
	if(m_nRightX == -1)
	{
		m_nRightX = CAM_IMAGE_WIDTH/2;
		str.Empty();
		str.Format("%d",m_nRightX);
		WritePrivateProfileString(str_model,strTitle+"OVERLAY_POS_RIGHT_X",str,H_filename);
	}
	str_OverlayRightX.Format("%d", m_nRightX);

	// rightY
	m_nRightY = GetPrivateProfileInt(str_model, strTitle+"OVERLAY_POS_RIGHT_Y", -1, H_filename);
	if(m_nRightY == -1)
	{
		m_nRightY = CAM_IMAGE_HEIGHT/2;
		str.Empty();
		str.Format("%d",m_nRightY);
		WritePrivateProfileString(str_model,strTitle+"OVERLAY_POS_RIGHT_Y",str,H_filename);
	}
	str_OverlayRightY.Format("%d", m_nRightY);


	Load_parameter_Rect();



}

tResultVal COverlayPosition_Option::Run()
{	
	tResultVal retval = {0,};
	BOOL FLAG = FALSE;
	b_StopFail = FALSE;
	CString str;

	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE)
		InsertList();

 	if(!((CImageTesterDlg *)m_pMomWnd)->PortChk_Cam()){//정상적으로포트가연결되어있지않으면
 		if(!((CImageTesterDlg *)m_pMomWnd)->Portopen_Cam()){
 			retval.m_Success = FALSE;
 
 			m_OverlayList.SetItemText(InsertIndex,4,"");
 			m_OverlayList.SetItemText(InsertIndex,5,"");
 			m_OverlayList.SetItemText(InsertIndex,6,"FAIL");
 			m_OverlayList.SetItemText(InsertIndex,7,"PORT DISCONNECT");
 			retval.ValString = "PORT DISCONNECT";
 			Str_Mes[0] = "FAIL";
 			Str_Mes[1] = "0";
 			return retval;
 		}
 	}
		 
	((CImageTesterDlg *)m_pMomWnd)->m_pResOverlayPositionOptWnd->OVERLAY_LEFT_NUM_TEXT("");
	((CImageTesterDlg *)m_pMomWnd)->m_pResOverlayPositionOptWnd->OVERLAY_RIGHT_NUM_TEXT("");

	for(int i=0;i<1;i++)
	{
		((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan =1;						

		for(int k =0;k<10;k++)
		{
			if(((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan == 0)
				break;
			else
				DoEvents(50);
		}	
	}

//	((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(1);

	// Overlay 검사
	retval = OverlayPosition(m_RGBScanbuf);

	str.Format("%d", m_param[0].m_ResultData);
	m_OverlayList.SetItemText(InsertIndex,4,str);

	str.Format("%d", m_param[1].m_ResultData);
	m_OverlayList.SetItemText(InsertIndex,5,str);

	if(retval.m_Success == TRUE)
	{
		retval.ValString.Empty();
		retval.ValString.Format("TEST SUCCESS");
		Str_Mes[0] = "PASS";
		Str_Mes[1] = "1";

		((CImageTesterDlg *)m_pMomWnd)->m_pResOverlayPositionOptWnd->RESULT_LEFT_TEXT(1,"PASS");
		((CImageTesterDlg *)m_pMomWnd)->m_pResOverlayPositionOptWnd->RESULT_RIGHT_TEXT(1,"PASS");

		m_OverlayList.SetItemText(InsertIndex,6,"PASS");
	}else
	{
		retval.ValString.Empty();
		retval.ValString.Format("TEST FAIL");
		Str_Mes[0] = "FAIL";
		Str_Mes[1] = "0";

		if(m_param[0].m_Success)
			((CImageTesterDlg *)m_pMomWnd)->m_pResOverlayPositionOptWnd->RESULT_LEFT_TEXT(1,"PASS");
		else
			((CImageTesterDlg *)m_pMomWnd)->m_pResOverlayPositionOptWnd->RESULT_LEFT_TEXT(2,"FAIL");
		
		if(m_param[1].m_Success)
			((CImageTesterDlg *)m_pMomWnd)->m_pResOverlayPositionOptWnd->RESULT_RIGHT_TEXT(1,"PASS");
		else
			((CImageTesterDlg *)m_pMomWnd)->m_pResOverlayPositionOptWnd->RESULT_RIGHT_TEXT(2,"FAIL");

		m_OverlayList.SetItemText(InsertIndex,6,"FAIL");
	}

	str.Format("%d", m_param[0].m_ResultData);
	((CImageTesterDlg *)m_pMomWnd)->m_pResOverlayPositionOptWnd->OVERLAY_LEFT_NUM_TEXT(str);

	str.Format("%d", m_param[1].m_ResultData);
	((CImageTesterDlg *)m_pMomWnd)->m_pResOverlayPositionOptWnd->OVERLAY_RIGHT_NUM_TEXT(str);

	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE)
		StartCnt++;
	
	//cvReleaseImage(&OriginImage);
	return retval;
}	

tResultVal COverlayPosition_Option::OverlayPosition(LPBYTE IN_RGB)
{
	tResultVal retVal;
	BYTE B,G,R;
	double Sum_Y = 0.0;
	int x,y;
	m_CAM_SIZE_WIDTH = 720;
	m_CAM_SIZE_HEIGHT = 480;

	/* 차트의 원중심 좌표*/
	int OverLay_Circle_YPoint_Right = 0; 
	int OverLay_Circle_XPoint_Right = 0;
	int OverLay_Circle_YPoint_Left = 0;
	int OverLay_Circle_XPoint_Left = 0;

	/* 차트 원의 탐색 위치 */
	int Left_ROI_X = Test_RECT[Rect_Left].m_Left;
	int Left_ROI_Y = Test_RECT[Rect_Left].m_Top;
	int Left_ROI_W = Test_RECT[Rect_Left].m_Width;
	int Left_ROI_H = Test_RECT[Rect_Left].m_Height;

	int Right_ROI_X = Test_RECT[Rect_Right].m_Left;
	int Right_ROI_Y = Test_RECT[Rect_Right].m_Top;
	int Right_ROI_W = Test_RECT[Rect_Right].m_Width;
	int Right_ROI_H = Test_RECT[Rect_Right].m_Height;

	/* 오버레이의 코너점 좌표 2개 : 지정 */
	int Left_Overlay_Conner_X = 0; 
	int Left_Overlay_Conner_Y = 0;
	int Right_Overlay_Conner_X = 0;
	int Right_Overlay_Conner_Y = 0;

	for (int i = 0; i <720 ; i++){
		for (int j = 0 ; j < 480 ; j++){
			Data_Static[i][j] = 0;
		}
	}

	OriginImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH,m_CAM_SIZE_HEIGHT),IPL_DEPTH_8U,3);

	for (y=0; y<m_CAM_SIZE_HEIGHT; y++){
		for (x=0; x<m_CAM_SIZE_WIDTH; x++){
			B = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 2];

			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));

			//OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;
			OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 0] = B;
			OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 1] = G;
			OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 2] = R;
		}
	}

	/* 이미지 이진화 */
	Image_Binarization();


	/* 원 중심 2포인트 추출 */
	int Temp_X = 0;
	int Temp_Y = 0;
	int _cnt = 0; 

	/* Circle_Left */
	for (x = Left_ROI_X ; x < Left_ROI_X + Left_ROI_W ; x++){
		for (y = Left_ROI_Y ; y < Left_ROI_Y + Left_ROI_H ; y++){
			if (Data_Static[x][y] == 1){
				Temp_X += x;
				Temp_Y += y;
				_cnt++;
			}
		}
	}
	if (_cnt > 0){
		OverLay_Circle_XPoint_Left = Temp_X / _cnt;
		OverLay_Circle_YPoint_Left = Temp_Y / _cnt;
	}
	/* Circle_Right */
	Temp_X = 0;
	Temp_Y = 0;
	_cnt = 0; 

	for (x = Right_ROI_X ; x < Right_ROI_X + Right_ROI_W ; x++){
		for (y = Right_ROI_Y ; y < Right_ROI_Y + Right_ROI_H ; y++){
			if (Data_Static[x][y] == 1){
				Temp_X += x;
				Temp_Y += y;
				_cnt++;
			}
		}
	}
	if (_cnt > 0){
		OverLay_Circle_XPoint_Right = Temp_X / _cnt;
		OverLay_Circle_YPoint_Right = Temp_Y / _cnt;
	}


	/* Pixel 계산 */
	int Left_Gap_X = abs(OverLay_Circle_XPoint_Left - m_nLeftX) ;
	int Left_Gap_Y = abs(OverLay_Circle_YPoint_Left - m_nLeftY) ;

	int Right_Gap_X = abs(OverLay_Circle_XPoint_Right - m_nRightX) ;
	int Right_Gap_Y = abs(OverLay_Circle_YPoint_Right - m_nRightY) ;

	double Gap_Left = (Left_Gap_X*Left_Gap_X) +(Left_Gap_Y*Left_Gap_Y);
	double Gap_Right = (Right_Gap_X*Right_Gap_X) +(Right_Gap_Y*Right_Gap_Y); 
	
	

	/* Offset_ MTM */
	int OffSet_Left = m_offset_left;
	int OffSet_Right = m_offset_right;
	Gap_Left = sqrt(Gap_Left)+ OffSet_Left;
	Gap_Right = sqrt(Gap_Right)+ OffSet_Right;
	/**/

	int str_OverlayPos_int = atoi(str_OverlayPos);
	int str_OverCutOff_int = atoi(str_OverCutOff);

	m_param[0].m_ResultData = Gap_Left; // 왼쪽 거리
	if ( ((str_OverlayPos_int + str_OverCutOff_int) > m_param[0].m_ResultData ) && ((str_OverlayPos_int - str_OverCutOff_int) < m_param[0].m_ResultData) ) // 
		m_param[0].m_Success	= TRUE;
	else
		m_param[0].m_Success	= FALSE;

	m_param[1].m_ResultData =Gap_Right; // 오른쪽 거리
	if (((str_OverlayPos_int + str_OverCutOff_int) > m_param[1].m_ResultData ) && ((str_OverlayPos_int - str_OverCutOff_int) < m_param[1].m_ResultData)  )
		m_param[1].m_Success	= TRUE;
	else
		m_param[1].m_Success	= FALSE;

	if(m_param[0].m_Success && m_param[1].m_Success)
		retVal.m_Success = TRUE;
	else
		retVal.m_Success = FALSE;

	/* D드라이브 저장*/
	cvLine(OriginImage,cvPoint(m_nLeftX,m_nLeftY),cvPoint(m_nLeftX,m_nLeftY),CV_RGB(255,0,0),2);
	cvLine(OriginImage,cvPoint(m_nRightX,m_nRightY),cvPoint(m_nRightX,m_nRightY),CV_RGB(255,0,0),2);

	cvLine(OriginImage,cvPoint(OverLay_Circle_XPoint_Left-10,OverLay_Circle_YPoint_Left),cvPoint(OverLay_Circle_XPoint_Left+10,OverLay_Circle_YPoint_Left),CV_RGB(0,255,0),1);
	cvLine(OriginImage,cvPoint(OverLay_Circle_XPoint_Left,OverLay_Circle_YPoint_Left-10),cvPoint(OverLay_Circle_XPoint_Left,OverLay_Circle_YPoint_Left+10),CV_RGB(0,255,0),1);
	cvLine(OriginImage,cvPoint(OverLay_Circle_XPoint_Right-10,OverLay_Circle_YPoint_Right),cvPoint(OverLay_Circle_XPoint_Right+10,OverLay_Circle_YPoint_Right),CV_RGB(0,255,0),1);
	cvLine(OriginImage,cvPoint(OverLay_Circle_XPoint_Right,OverLay_Circle_YPoint_Right-10),cvPoint(OverLay_Circle_XPoint_Right,OverLay_Circle_YPoint_Right+10),CV_RGB(0,255,0),1);

	cvLine(OriginImage,cvPoint(m_nLeftX,m_nLeftY),cvPoint(OverLay_Circle_XPoint_Left,OverLay_Circle_YPoint_Left),CV_RGB(0,0,255),1);
	cvLine(OriginImage,cvPoint(m_nRightX,m_nRightY),cvPoint(OverLay_Circle_XPoint_Right,OverLay_Circle_YPoint_Right),CV_RGB(0,0,255),1);

	//cvSaveImage("D:\\test_Image1.bmp",OriginImage);

	m_param[0].m_PosX = OverLay_Circle_XPoint_Left;
	m_param[0].m_PosY = OverLay_Circle_YPoint_Left;

	m_param[1].m_PosX = OverLay_Circle_XPoint_Right;
	m_param[1].m_PosY = OverLay_Circle_YPoint_Right;

	cvReleaseImage(&OriginImage);
	return retVal;
}

//---------------------------------------------------------------------WORKLIST
void COverlayPosition_Option::Set_List(CListCtrl *List)
{
	CString str ="";
	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"좌측",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"우측",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"result",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"비고",LVCFMT_CENTER, 80);
	
	ListItemNum=8;
	Copy_List(&m_OverlayList,((CImageTesterDlg *)m_pMomWnd)->m_pWorkList,ListItemNum);
}

void COverlayPosition_Option::LOT_InsertDataList()
{
	CString strCnt="";

	int Index = m_Lot_OverlayList.InsertItem(Lot_StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index+1);
	m_Lot_OverlayList.SetItemText(Index,0,strCnt);

	int CopyIndex = m_OverlayList.GetItemCount()-1;

	int count =0;
	for(int t=0; t< Lot_InsertNum;t++){
		if((t != 2)&&(t!=0)){
			m_Lot_OverlayList.SetItemText(Index,t, m_OverlayList.GetItemText(CopyIndex,count));
			count++;

		}else if(t==0){
			count++;
		}else{
			m_Lot_OverlayList.SetItemText(Index,t,((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;
}

void COverlayPosition_Option::LOT_Set_List(CListCtrl *List)
{
	CString str ="";
	while(List->DeleteColumn(0));

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"좌측",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"우측",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"result",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"비고",LVCFMT_CENTER, 80);

	Lot_InsertNum=8;
}

void COverlayPosition_Option::LOT_EXCEL_SAVE()
{
	CString Item[2]={"좌측", "우측"};
	CString Data ="";
	CString str="";
	str = "***********************Overlay Position***********************\n";
	Data += str;

	str.Empty();
	str.Format("좌측 x :,%d, 좌측 y :,%d, 우측 x:,%d, 우측 y:, %d, \n", m_nLeftX, m_nLeftY, m_nRightX, m_nRightY);
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("OverlayPosition",Item,2,&m_Lot_OverlayList,Data);
}

void COverlayPosition_Option::EXCEL_SAVE()
{
	CString Item[2]={"좌측", "우측"};
	CString Data ="";
	CString str="";
	str = "***********************Overlay Position***********************\n";
	Data += str;

	str.Empty();
	str.Format("좌측 x :,%d, 좌측 y :,%d, 우측 x:,%d, 우측 y:, %d, \n", m_nLeftX, m_nLeftY, m_nRightX, m_nRightY);
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("OverlayPosition",Item,2,&m_OverlayList,Data);
}

void COverlayPosition_Option::InsertList()
{
	CString strCnt;

	InsertIndex = m_OverlayList.InsertItem(StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_OverlayList.SetItemText(InsertIndex,0,strCnt);
	
	m_OverlayList.SetItemText(InsertIndex,1,((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_OverlayList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_OverlayList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");
}

void COverlayPosition_Option::FAIL_UPLOAD()
{
	if ((((CImageTesterDlg *)m_pMomWnd)->LotMod == 1) && (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)){
		InsertList();
		m_OverlayList.SetItemText(InsertIndex,6,"FAIL");
		m_OverlayList.SetItemText(InsertIndex,7,"STOP");
		Str_Mes[0] = "FAIL";
		Str_Mes[1] = "0";
		b_StopFail = TRUE;
//		((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,11,"STOP");
		StartCnt++;
	}
}

bool COverlayPosition_Option::EXCEL_UPLOAD()
{
	m_OverlayList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("OverlayPosition",&m_OverlayList,2,&StartCnt)==TRUE)
	{
		return TRUE;
	}
	else
	{
		StartCnt = 0;
		return FALSE;
	}
}

bool COverlayPosition_Option::LOT_EXCEL_UPLOAD(){
	m_OverlayList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("OverlayPosition",&m_Lot_OverlayList,2,&StartCnt)==TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
}

CString COverlayPosition_Option::Mes_Result()
{
	CString sz;
	m_szMesResult = _T("");
	if(b_StopFail == FALSE){
	m_szMesResult.Format(_T("%s:%s"),Str_Mes[0],Str_Mes[1]);
	}else{
		m_szMesResult.Format(_T(":1"));
	}
	return m_szMesResult;
}

void COverlayPosition_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POSITION_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_OFFSET_SAVE2))->EnableWindow(0);
		
		((CEdit *)GetDlgItem(IDC_EDIT_OVERLAY_LEFT_X))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_OVERLAY_RIGHT_X))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_OVERLAY_LEFT_Y))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_OVERLAY_RIGHT_Y))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_SET_DISTANCE))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_SET_OVERLAY_CUTOFF))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_LEFT))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_RIGHT))->EnableWindow(0);
	}
}


bool COverlayPosition_Option::OverlayPic(CDC *cdc)
{
	for(int _a=0; _a<2; _a++)
		OverlayPic(cdc, _a);
	return TRUE;
}

bool COverlayPosition_Option::OverlayPic(CDC *cdc, int NUM)
{
	// 사각형 그리기
	CPen	my_Pan,*old_pan;
	CString TEXTDATA ="";

	//if(C_RECT[NUM].Chkdata() == FALSE){return 0;}
	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	if(m_param[NUM].m_Success == TRUE){
		my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(WHITE_COLOR);
	}else{
		my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(WHITE_COLOR);
	}

// 	int Left_ROI_X = 145;
// 	int Left_ROI_Y = 25;
// 	int Left_ROI_W = 65;
// 	int Left_ROI_H = 60;
// 
// 	int Right_ROI_X = 520;
// 	int Right_ROI_Y = 25;
// 	int Right_ROI_W = 65;
// 	int Right_ROI_H = 60;

	TEXTDATA.Format("PIXEL:%d",m_param[NUM].m_ResultData);


	cdc->MoveTo(Test_RECT[NUM].m_Left,	Test_RECT[NUM].m_Top);
	cdc->LineTo(Test_RECT[NUM].m_Right,	Test_RECT[NUM].m_Top);
	cdc->LineTo(Test_RECT[NUM].m_Right,	Test_RECT[NUM].m_Bottom);
	cdc->LineTo(Test_RECT[NUM].m_Left,	Test_RECT[NUM].m_Bottom);
	cdc->LineTo(Test_RECT[NUM].m_Left,	Test_RECT[NUM].m_Top);

	if(m_param[NUM].m_PosX > 0 && m_param[NUM].m_PosY > 0)
	{
		// 십자가
		cdc->MoveTo(m_param[NUM].m_PosX-10,	m_param[NUM].m_PosY);
		cdc->LineTo(m_param[NUM].m_PosX+10,	m_param[NUM].m_PosY);
		cdc->MoveTo(m_param[NUM].m_PosX,	m_param[NUM].m_PosY-10);
		cdc->LineTo(m_param[NUM].m_PosX,	m_param[NUM].m_PosY+10);

		cdc->MoveTo(m_param[NUM].m_PosX,	m_param[NUM].m_PosY);
		if(NUM == 0){
			cdc->LineTo(m_nLeftX,				m_nLeftY);
		}else{
			cdc->LineTo(m_nRightX,				m_nRightY);
		}

	}

	//	cdc->TextOut(m_param[NUM].m_Left + (int)(m_param[NUM].Height()/2)-60,m_param[NUM].m_Top - 20,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();

	return TRUE;
}

void COverlayPosition_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN)
	{
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째라인의앞부분을선택한다.  
		pTStat -> Clear();			   //라인하나를지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

BOOL COverlayPosition_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에특수화된코드를추가및/또는기본클래스를호출합니다.
	if(pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_ESCAPE)
			return TRUE;
				
		if (pMsg->wParam == VK_RETURN)
			return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

HBRUSH COverlayPosition_Option::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void COverlayPosition_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if(bShow)
	{
		InitUI();
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else
	{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
	}
}

void COverlayPosition_Option::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.
}

void COverlayPosition_Option::OnBnClickedBtnOverlayTest()
{
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(0);
	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(1);
}

void COverlayPosition_Option::OnEnChangeEditSetDistance()
{
	UpdateData(TRUE);
	int buf1 = _ttoi(str_OverlayPos);
	int buf2 = _ttoi(str_OverCutOff);

	if((buf1 >=0)&&(buf2 >= 0))
	{
		if((buf1 != m_nDistance)||(buf2 != m_nCutOff))
			((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POSITION_SAVE))->EnableWindow(1);
		else
			((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POSITION_SAVE))->EnableWindow(0);
	}else
	{
		((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POSITION_SAVE))->EnableWindow(0);
	}
}

void COverlayPosition_Option::OnEnChangeEditSetOverlayCutoff()
{
	UpdateData(TRUE);
	int buf1 = _ttoi(str_OverlayPos);
	int buf2 = _ttoi(str_OverCutOff);

	if((buf1 >=0)&&(buf2 >= 0))
	{
		if((buf1 != m_nDistance)||(buf2 != m_nCutOff))
			((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POSITION_SAVE))->EnableWindow(1);
		else
			((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POSITION_SAVE))->EnableWindow(0);
	}else
	{
		((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POSITION_SAVE))->EnableWindow(0);
	}
}


void COverlayPosition_Option::Image_Binarization(){

	int i,j,_c,x,y;
	int Max_X = m_CAM_SIZE_WIDTH;
	int Max_Y = m_CAM_SIZE_HEIGHT;

	BYTE B,G,R;
	double Y = 0;
	int CO = 0;
	int Gap_Color = 80;

	int Left_ROI_X = Test_RECT[Rect_Left].m_Left;
	int Left_ROI_Y = Test_RECT[Rect_Left].m_Top;
	int Left_ROI_W = Test_RECT[Rect_Left].m_Width;
	int Left_ROI_H = Test_RECT[Rect_Left].m_Height;

	int Right_ROI_X = Test_RECT[Rect_Right].m_Left;
	int Right_ROI_Y = Test_RECT[Rect_Right].m_Top;
	int Right_ROI_W = Test_RECT[Rect_Right].m_Width;
	int Right_ROI_H = Test_RECT[Rect_Right].m_Height;
	//cvLine(m_ROIImage_Binary,cvPoint(0,CO),cvPoint(Max_X,CO),CV_RGB(255,255,255),1);

	for( x = 0 ; x < Max_X ; x++){
		for (y = 0 ; y < Max_Y; y++){

			B = OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 0];
			G = OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 1];
			R = OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 2];
			Y = 0.30*R + 0.59*G + 0.11*B;	
			if ( ( R < 50 && G< 50 && B<50 ) && ( ( x>Left_ROI_X && x < Left_ROI_X+Left_ROI_W && y>Left_ROI_Y && y<Left_ROI_Y+Left_ROI_H) || ( x>Right_ROI_X && x < Right_ROI_X+Right_ROI_W && y>Right_ROI_Y && y<Right_ROI_Y+Right_ROI_H ) ) )
			{
				OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 0] = 0;
				OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 1] = 255;
				OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 2] = 255;
				Data_Static[x][y]++;
			}else{
				OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 0] = 0;
				OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 1] = 0;
				OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 2] = 0;
			}
		}
	}
}

void COverlayPosition_Option::OnBnClickedBtnOverlayPositionSave()
{
	UpdateData(TRUE);
	CString str = "";
	m_nDistance = _ttoi(str_OverlayPos);
	m_nCutOff	= _ttoi(str_OverCutOff);
	Save_parameter();
	((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POSITION_SAVE))->EnableWindow(0);
}


void COverlayPosition_Option::OnBnClickedBtnOverlayPointSave()
{
	UpdateData(TRUE);
	CString str = "";
	m_nLeftX	= _ttoi(str_OverlayLeftX);
	m_nLeftY	= _ttoi(str_OverlayLeftY);

	m_nRightX	= _ttoi(str_OverlayRightX);
	m_nRightY	= _ttoi(str_OverlayRightY);

	Save_parameter();
	((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(0);
}

void COverlayPosition_Option::OnEnChangeEditOverlayLeftX()
{
	UpdateData(TRUE);
	int buf1 = _ttoi(str_OverlayLeftX);
	int buf2 = _ttoi(str_OverlayLeftY);

	int buf3 = _ttoi(str_OverlayRightX);
	int buf4 = _ttoi(str_OverlayRightY);

	if((buf1 >=0)&&(buf2 >= 0) && buf3 >=0 && buf4 >=0)
	{
		if((buf1 != m_nLeftX)||(buf2 != m_nLeftY) || (buf3 != m_nRightX) || (buf4 != m_nRightY))
			((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(1);
		else
			((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(0);
	}else
	{
		((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(0);
	}
}

void COverlayPosition_Option::OnEnChangeEditOverlayLeftY()
{
	UpdateData(TRUE);
	int buf1 = _ttoi(str_OverlayLeftX);
	int buf2 = _ttoi(str_OverlayLeftY);

	int buf3 = _ttoi(str_OverlayRightX);
	int buf4 = _ttoi(str_OverlayRightY);

	if((buf1 >=0)&&(buf2 >= 0) && buf3 >=0 && buf4 >=0)
	{
		if((buf1 != m_nLeftX)||(buf2 != m_nLeftY) || (buf3 != m_nRightX) || (buf4 != m_nRightY))
			((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(1);
		else
			((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(0);
	}else
	{
		((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(0);
	}
}

void COverlayPosition_Option::OnEnChangeEditOverlayRightX()
{
	UpdateData(TRUE);
	int buf1 = _ttoi(str_OverlayLeftX);
	int buf2 = _ttoi(str_OverlayLeftY);

	int buf3 = _ttoi(str_OverlayRightX);
	int buf4 = _ttoi(str_OverlayRightY);

	if((buf1 >=0)&&(buf2 >= 0) && buf3 >=0 && buf4 >=0)
	{
		if((buf1 != m_nLeftX)||(buf2 != m_nLeftY) || (buf3 != m_nRightX) || (buf4 != m_nRightY))
			((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(1);
		else
			((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(0);
	}else
	{
		((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(0);
	}
}

void COverlayPosition_Option::OnEnChangeEditOverlayRightY()
{
	UpdateData(TRUE);
	int buf1 = _ttoi(str_OverlayLeftX);
	int buf2 = _ttoi(str_OverlayLeftY);

	int buf3 = _ttoi(str_OverlayRightX);
	int buf4 = _ttoi(str_OverlayRightY);

	if((buf1 >=0)&&(buf2 >= 0) && buf3 >=0 && buf4 >=0)
	{
		if((buf1 != m_nLeftX)||(buf2 != m_nLeftY) || (buf3 != m_nRightX) || (buf4 != m_nRightY))
			((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(1);
		else
			((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(0);
	}else
	{
		((CButton *)GetDlgItem(IDC_BTN_OVERLAY_POINT_SAVE))->EnableWindow(0);
	}
}

void COverlayPosition_Option::OnBnClickedBtnOffsetSave2()
{
	UpdateData(TRUE);
	CString str = "";
	m_offset_left = _ttoi(m_OFFSET_LEFT);
	m_offset_right	= _ttoi(m_OFFSET_RIGHT);
	Save_parameter();
}

void COverlayPosition_Option::Rect_SETLIST(){

	m_RectList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_RectList.InsertColumn(0, "구분", LVCFMT_CENTER, 90);
	m_RectList.InsertColumn(1, "PosX", LVCFMT_CENTER, 50);
	m_RectList.InsertColumn(2, "PosY", LVCFMT_CENTER, 50);
	m_RectList.InsertColumn(3, "St X", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(4, "St Y", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(5, "Ex X", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(6, "Ex Y", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(7, "W", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(8, "H", LVCFMT_CENTER, 40);
}



void COverlayPosition_Option::Rect_InsertList()
{
	int InIndex=0;
	CString str="";

	m_RectList.DeleteAllItems();

	for(int t=0; t<Rect_FullNum; t++){
		InIndex = m_RectList.InsertItem(t,"초기",0);


		if(t ==0){
			str.Empty();str="LEFT";
		}
		if(t ==1){
			str.Empty();str="RIGHT";
		}

		m_RectList.SetItemText(InIndex,0,str);
		str.Empty();str.Format("%d", Test_RECT[t].m_PosX);
		m_RectList.SetItemText(InIndex,1,str);
		str.Empty();str.Format("%d", Test_RECT[t].m_PosY);
		m_RectList.SetItemText(InIndex,2,str);

		str.Empty();
		str.Format("%d", Test_RECT[t].m_Left);
		m_RectList.SetItemText(InIndex,3,str);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Top);
		m_RectList.SetItemText(InIndex,4,str);

		str.Empty();
		str.Format("%d", Test_RECT[t].m_Right+1);
		m_RectList.SetItemText(InIndex,5,str);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Bottom+1);
		m_RectList.SetItemText(InIndex,6,str);

		str.Empty();
		str.Format("%d", Test_RECT[t].m_Width);
		m_RectList.SetItemText(InIndex,7,str);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Height);
		m_RectList.SetItemText(InIndex,8,str);
	}
}

void COverlayPosition_Option::Load_parameter_Rect(){
	for(int t=0; t<Rect_FullNum; t++){
		CString RectName;
		RectName.Format("%s",g_szTestRect[t]);
		Test_RECT[t].m_PosX = GetPrivateProfileInt(str_model,RectName+"PosX",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		if (Test_RECT[Rect_Left].m_PosX == -1)
		{
			int ROI_X = 145;
			int ROI_Y = 25;
			int ROI_W = 65;
			int ROI_H = 60;

			Test_RECT[t].m_PosX = ROI_X + ( ROI_W/2);
			Test_RECT[t].m_PosY = ROI_Y + ( ROI_H/2);
			Test_RECT[t].m_Width = ROI_W;
			Test_RECT[t].m_Height = ROI_H;
			Test_RECT[t].EX_RECT_SET(Test_RECT[t].m_PosX,Test_RECT[t].m_PosY, ROI_W, ROI_H);
			Save_parameter_Rect(Rect_Left);
		}

		if (Test_RECT[Rect_Right].m_PosX == -1)
		{
			int ROI_X = 520;
			int ROI_Y = 25;
			int ROI_W = 65;
			int ROI_H = 60;

			Test_RECT[t].m_PosX = ROI_X + ( ROI_W/2);
			Test_RECT[t].m_PosY = ROI_Y + ( ROI_H/2);
			Test_RECT[t].m_Width = ROI_W;
			Test_RECT[t].m_Height = ROI_H;
			Test_RECT[t].EX_RECT_SET(Test_RECT[t].m_PosX,Test_RECT[t].m_PosY, ROI_W, ROI_H);
			Save_parameter_Rect(Rect_Right);
		}


		Test_RECT[t].m_PosY	= GetPrivateProfileInt(str_model,RectName+"PosY",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		Test_RECT[t].m_Left	= GetPrivateProfileInt(str_model,RectName+"StartX",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		Test_RECT[t].m_Top	= GetPrivateProfileInt(str_model,RectName+"StartY",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		Test_RECT[t].m_Right = GetPrivateProfileInt(str_model,RectName+"ExitX",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		Test_RECT[t].m_Bottom = GetPrivateProfileInt(str_model,RectName+"ExitY",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		Test_RECT[t].m_Width = GetPrivateProfileInt(str_model,RectName+"Width",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		Test_RECT[t].m_Height = GetPrivateProfileInt(str_model,RectName+"Height",-1,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);	
	}

	Rect_InsertList();

}


void COverlayPosition_Option::Save_parameter_Rect(UINT MODE){
	CString str;
	
	CString RectName;
	RectName.Format("%s",g_szTestRect[MODE]);

	str.Empty();
	str.Format("%d",Test_RECT[MODE].m_PosX);
	WritePrivateProfileString(str_model,RectName+"PosX",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%d",Test_RECT[MODE].m_PosY);
	WritePrivateProfileString(str_model,RectName+"PosY",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	str.Empty();
	str.Format("%d",Test_RECT[MODE].m_Left);
	WritePrivateProfileString(str_model,RectName+"StartX",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%d",Test_RECT[MODE].m_Top);
	WritePrivateProfileString(str_model,RectName+"StartY",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%d",Test_RECT[MODE].m_Right);
	WritePrivateProfileString(str_model,RectName+"ExitX",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%d",Test_RECT[MODE].m_Bottom);
	WritePrivateProfileString(str_model,RectName+"ExitY",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%d",Test_RECT[MODE].m_Width);
	WritePrivateProfileString(str_model,RectName+"Width",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%d",Test_RECT[MODE].m_Height);
	WritePrivateProfileString(str_model,RectName+"Height",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);


}
void COverlayPosition_Option::OnBnClickedButtonSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	Save_parameter_Rect(Rect_Left);
	Save_parameter_Rect(Rect_Right);

	ChangeCheck =0;
	for(int t=0; t<Rect_FullNum; t++){
		ChangeItem[t]=-1;
	}
	for(int t=0; t<Rect_FullNum; t++){
		m_RectList.Update(t);
	}
}

void COverlayPosition_Option::OnBnClickedButtonLoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Load_parameter_Rect();
	Rect_InsertList();
	ChangeCheck =0;
	for(int t=0; t<Rect_FullNum; t++){
		ChangeItem[t]=-1;
	}
	for(int t=0; t<Rect_FullNum; t++){
		m_RectList.Update(t);
	}

}

void COverlayPosition_Option::OnNMDblclkListRect(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	
	if(pNMITEM->iSubItem == 0 || pNMITEM->iSubItem == -1 || pNMITEM->iItem == -1 ){return ;}

	iSavedItem = pNMITEM->iItem;
	iSavedSubitem = pNMITEM->iSubItem;

	CRect rect;
	if(pNMITEM->iItem != -1)
	{
		if(pNMITEM->iSubItem != 0)
		{		
			m_RectList.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			m_RectList.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowText(m_RectList.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
			((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetFocus();			
		}
	}
	*pResult = 0;
}

void COverlayPosition_Option::OnEnKillfocusEditRectlist()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString str = "";
	ChangeCheck=1;
	if((iSavedItem != -1)&&(iSavedSubitem != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_RECTLIST, str));
		List_COLOR_Change(iSavedItem);
		if(m_RectList.GetItemText(iSavedItem, iSavedSubitem) != str){
			Change_DATA();
			Rect_InsertList();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );
	iSavedItem = -1;
	iSavedSubitem = -1;
}
void COverlayPosition_Option::List_COLOR_Change(int itemnum)
{
	bool FLAG = TRUE;
	int Check=0;

	for(int t=0;t<Rect_FullNum; t++){
		if(ChangeItem[t]==0)Check++;
		if(ChangeItem[t]==1)Check++;
	}

	if(Check !=Rect_FullNum){
		FLAG =FALSE;
		ChangeItem[changecount]=itemnum;
	}

	if(!FLAG){
		for(int t=0; t<changecount+1; t++){
			for(int k=0; k<changecount+1; k++){

				if(ChangeItem[t] == ChangeItem[k]){
					if(t==k){break;	}			
					ChangeItem[k] =-1;
				}
			}
		}
		//----------------데이터 정렬
		int temp=0;

		for(int i=0; i<Rect_FullNum; i++)
		{
			for(int j=i+1; j<Rect_FullNum; j++)
			{
				if(ChangeItem[i] < ChangeItem[j])  // 내림차순
				{
					temp = ChangeItem[i];
					ChangeItem[i] = ChangeItem[j];
					ChangeItem[j] = temp;
				}
			}
		}
		//--------------------------------
		for(int t=0; t<Rect_FullNum; t++){

			if(ChangeItem[t] ==-1){
				changecount =t;
				break;
			}
		}
	}

	for(int t=0; t<Rect_FullNum; t++){
		m_RectList.Update(t);
	}
}


void COverlayPosition_Option::Change_DATA(){

	CString str;

	((CEdit *)GetDlgItemText(IDC_EDIT_RECTLIST, str));
	m_RectList.SetItemText(iSavedItem, iSavedSubitem, str);

	double num = atof(str);
	if(num < 0){
		num =0;
	}

	if(iSavedSubitem ==1){
		Test_RECT[iSavedItem].m_PosX = (int)num;

	}
	if(iSavedSubitem ==2){
		Test_RECT[iSavedItem].m_PosY = (int)num;

	}
	if(iSavedSubitem ==3){
		Test_RECT[iSavedItem].m_Left = (int)num;
		Test_RECT[iSavedItem].m_PosX=(Test_RECT[iSavedItem].m_Right+1 + Test_RECT[iSavedItem].m_Left) /2;
		Test_RECT[iSavedItem].m_Width=Test_RECT[iSavedItem].m_Right+1 - Test_RECT[iSavedItem].m_Left;

	}
	if(iSavedSubitem ==4){
		Test_RECT[iSavedItem].m_Top = (int)num;
		Test_RECT[iSavedItem].m_PosY=(Test_RECT[iSavedItem].m_Top + Test_RECT[iSavedItem].m_Bottom+1)/2;
		Test_RECT[iSavedItem].m_Height= Test_RECT[iSavedItem].m_Bottom+1 - Test_RECT[iSavedItem].m_Top;

	}
	if(iSavedSubitem ==5){
		if(num == 0){
			Test_RECT[iSavedItem].m_Right = 0;
		}
		else{
			Test_RECT[iSavedItem].m_Right = (int)num;
			Test_RECT[iSavedItem].m_PosX = (Test_RECT[iSavedItem].m_Right + Test_RECT[iSavedItem].m_Left+1) /2;
			Test_RECT[iSavedItem].m_Width = Test_RECT[iSavedItem].m_Right+1 - Test_RECT[iSavedItem].m_Left;
		}
	}
	if(iSavedSubitem ==6){
		if(num ==0){
			Test_RECT[iSavedItem].m_Bottom =0;
		}
		else{
			Test_RECT[iSavedItem].m_Bottom = (int)num;
			Test_RECT[iSavedItem].m_PosY = (Test_RECT[iSavedItem].m_Top + Test_RECT[iSavedItem].m_Bottom+1)/2;
			Test_RECT[iSavedItem].m_Height = Test_RECT[iSavedItem].m_Bottom+1 - Test_RECT[iSavedItem].m_Top;
		}
	}
	if(iSavedSubitem ==7){
		Test_RECT[iSavedItem].m_Width = (int)num;
	}
	if(iSavedSubitem ==8){
		Test_RECT[iSavedItem].m_Height = (int)num;
	}


	Test_RECT[iSavedItem].EX_RECT_SET(
		Test_RECT[iSavedItem].m_PosX,
		Test_RECT[iSavedItem].m_PosY,
		Test_RECT[iSavedItem].m_Width,
		Test_RECT[iSavedItem].m_Height);

	if(Test_RECT[iSavedItem].m_Left < 0){//////////////////////수정
		Test_RECT[iSavedItem].m_Left = 0;
		Test_RECT[iSavedItem].m_Width = Test_RECT[iSavedItem].m_Right+1 -Test_RECT[iSavedItem].m_Left;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}
	if(Test_RECT[iSavedItem].m_Right >CAM_IMAGE_WIDTH){
		Test_RECT[iSavedItem].m_Right = CAM_IMAGE_WIDTH;
		Test_RECT[iSavedItem].m_Width = Test_RECT[iSavedItem].m_Right+1 -Test_RECT[iSavedItem].m_Left;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}
	if(Test_RECT[iSavedItem].m_Top< 0){
		Test_RECT[iSavedItem].m_Top = 0;
		Test_RECT[iSavedItem].m_Height = Test_RECT[iSavedItem].m_Bottom+1 - Test_RECT[iSavedItem].m_Top;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}
	if(Test_RECT[iSavedItem].m_Bottom >CAM_IMAGE_HEIGHT){
		Test_RECT[iSavedItem].m_Bottom = CAM_IMAGE_HEIGHT;
		Test_RECT[iSavedItem].m_Height = 
			Test_RECT[iSavedItem].m_Bottom+1 -
			Test_RECT[iSavedItem].m_Top;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}
	if(Test_RECT[iSavedItem].m_Height<= 0){
		Test_RECT[iSavedItem].m_Height = 1;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}
	if(Test_RECT[iSavedItem].m_Height >=CAM_IMAGE_HEIGHT){
		Test_RECT[iSavedItem].m_Height = CAM_IMAGE_HEIGHT;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}

	if(Test_RECT[iSavedItem].m_Width <= 0){
		Test_RECT[iSavedItem].m_Width = 1;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}
	if(Test_RECT[iSavedItem].m_Width >=CAM_IMAGE_WIDTH){
		Test_RECT[iSavedItem].m_Width = CAM_IMAGE_WIDTH;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}

	


	Test_RECT[iSavedItem].EX_RECT_SET(
		Test_RECT[iSavedItem].m_PosX,
		Test_RECT[iSavedItem].m_PosY,
		Test_RECT[iSavedItem].m_Width,
		Test_RECT[iSavedItem].m_Height);

	CString data = "";

	if(iSavedSubitem ==1){
		data.Format("%d",Test_RECT[iSavedItem].m_PosX);			
	}
	else if(iSavedSubitem ==2){
		data.Format("%d",Test_RECT[iSavedItem].m_PosY);
	}
	else if(iSavedSubitem ==3){
		data.Format("%d",Test_RECT[iSavedItem].m_Left);			
	}
	else if(iSavedSubitem ==4){
		data.Format("%d",Test_RECT[iSavedItem].m_Top);			
	}
	else if(iSavedSubitem ==5){
		data.Format("%d",Test_RECT[iSavedItem].m_Right);			
	}
	else if(iSavedSubitem ==6){
		data.Format("%d",Test_RECT[iSavedItem].m_Bottom);			
	}
	else if(iSavedSubitem ==7){
		data.Format("%d",Test_RECT[iSavedItem].m_Width);			
	}
	else if(iSavedSubitem ==8){
		data.Format("%d",Test_RECT[iSavedItem].m_Height);			
	}
	
	((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowText(data);
}

void COverlayPosition_Option::OnNMCustomdrawListRect(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//*pResult = 0;
	COLORREF text_color = 0;
	COLORREF bg_color = RGB(255, 255, 255);

	LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
	
	switch(lplvcd->nmcd.dwDrawStage){
        case CDDS_PREPAINT:
            *pResult = CDRF_NOTIFYITEMDRAW;
            return;
         
        // 배경 혹은 텍스트를 수정한다.
        case CDDS_ITEMPREPAINT:
            // 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
            *pResult = CDRF_NOTIFYITEMDRAW;
            return;
       
        // 서브 아이템의 배경 혹은 텍스트를 수정한다.
        case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
            if(lplvcd->iSubItem != 0){
                // 1번째 행이라면...

				if(lplvcd->nmcd.dwItemSpec >=0 ||lplvcd->nmcd.dwItemSpec <Rect_FullNum){
					

					if(ChangeCheck==1){
						if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 9 ){
							for(int t=0; t<Rect_FullNum; t++){
								if(lplvcd->nmcd.dwItemSpec == ChangeItem[t]){
									text_color = RGB(0, 0, 0);
									bg_color = RGB(250, 230, 200);					
								}
							}
						}
					
					
					}
				
				}
				

			    lplvcd->clrText = text_color;
                lplvcd->clrTextBk = bg_color;
				
            }
            *pResult = CDRF_NEWFONT;
            return;
    }
}

BOOL COverlayPosition_Option::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// 이 기능을 사용하려면 Windows Vista 이상이 있어야 합니다.
	// _WIN32_WINNT 기호는 0x0600보다 크거나 같아야 합니다.
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	int znum = zDelta/120;
	CWnd *focusid;
	focusid = GetFocus();
	bool FLAG=0;
	if(znum > 0 ){
		FLAG = 1;
	}
	else if(znum < 0 ){
		FLAG =0;
	}
	else if(znum == 0){
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	}
	int casenum = focusid->GetDlgCtrlID();
	switch(casenum){
		case IDC_EDIT_RECTLIST :
			if(FLAG == 1){
				if((Change_DATA_CHECK(1) == TRUE) /*|| (znum ==-1)*/){
					EditWheelIntSet(IDC_EDIT_RECTLIST,znum);
					Change_DATA();
					Rect_InsertList();
					OnEnSetfocusEditRectlist();
				}
			}
			if(FLAG == 0){
				if((Change_DATA_CHECK(0) == TRUE) /*|| (znum == 1)*/){
					EditWheelIntSet(IDC_EDIT_RECTLIST,znum);
					Change_DATA();
					Rect_InsertList();
					OnEnSetfocusEditRectlist();
				}
			}
			break;
	}
}


bool COverlayPosition_Option::Change_DATA_CHECK(bool FLAG){


	BOOL STAT = TRUE;
	//상한
	if(Test_RECT[iSavedItem].m_Width > 720 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Height > 480 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_PosX > 720 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_PosY > 480 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Top > 480 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Bottom > 480  )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Left> 720 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Right> 720 )
		STAT = FALSE;


	//하한

	if(Test_RECT[iSavedItem].m_Width < 1 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Height < 1 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_PosX < 0 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_PosY < 0 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Top < 0 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Bottom < 0  )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Left< 0 )
		STAT = FALSE;
	if(Test_RECT[iSavedItem].m_Right< 0 )
		STAT = FALSE;

	if(FLAG  == 1){
		if(Test_RECT[iSavedItem].m_Width == 1 )
			STAT = FALSE;
		if(Test_RECT[iSavedItem].m_Height == 1 )
			STAT = FALSE;

	}


	return STAT;
}

void COverlayPosition_Option::EditWheelIntSet(int nID,int Val)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = 0;
	}else{
		buf = GetDlgItemInt(nID);
		buf+= Val;
	}
	SetDlgItemInt(nID,buf,1);
	((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
}


void COverlayPosition_Option::OnEnSetfocusEditRectlist()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.


	int num = iSavedItem;
	if( (Test_RECT[num].m_PosX >= 0)&&(Test_RECT[num].m_PosX < 720)&&
		(Test_RECT[num].m_PosY >= 0)&&(Test_RECT[num].m_PosY < 480)&&
		(Test_RECT[num].m_Left >= 0)&&(Test_RECT[num].m_Left < 720)&&
		(Test_RECT[num].m_Top  >= 0)&&(Test_RECT[num].m_Top  < 480)&&
		(Test_RECT[num].m_Right>= 0)&&(Test_RECT[num].m_Right< 720)&&
		(Test_RECT[num].m_Bottom>=0)&&(Test_RECT[num].m_Bottom<480)&&
		(Test_RECT[num].m_Width>=0)&&(Test_RECT[num].m_Width<720)&&
		(Test_RECT[num].m_Height>=0)&&(Test_RECT[num].m_Height<480)){
			((CButton *)GetDlgItem(IDC_BUTTON_SAVE))->EnableWindow(1);
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_SAVE))->EnableWindow(0);
	}
	
}
