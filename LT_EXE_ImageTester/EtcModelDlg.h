#pragma once

#include "ETCUSER.H"

#include "cv.h"
#include "highgui.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "./Motor/MotionControl.h"
#include "afxcmn.h"

#define CAM_IMAGE_WIDTH		640
#define CAM_IMAGE_HEIGHT	480

#define BLOCK_SIZE 16
#define BLOCK_WIDTH CAM_IMAGE_WIDTH/BLOCK_SIZE
#define BLOCK_HEIGHT CAM_IMAGE_HEIGHT/BLOCK_SIZE
//#define b_luritechDebugMODE 1
#define b_luritechDebugMODE 0

// CEtcModelDlg 대화 상자입니다.
extern BYTE m_pFailRaw[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 2];
extern BYTE m_pImage[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 2];

class CEtcModelDlg : public CDialog
{
	DECLARE_DYNAMIC(CEtcModelDlg)

public:
	CEtcModelDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CEtcModelDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ETC_MODEL_DIALOG };
	
	void	EnableOption(bool benable);

	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);

	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,tINFO INFO);

	void	ShowDlg(int stat);
	void	DelModel();

	void	NameChange(CString str);

	void	GenModel(int ID);
	void	DelModel(int ID);

	void	ModeSetup(int ID);
	
	void	InitStat();
	void	ProgressEn(BOOL stat);

	void	Result_Initstat();

	void	run();
	void	run(int ID);

	void	stop();
	void	ETC_Run();
	void	ETC_Run(int ID);

	void	JIG_MOVE_CHK();
	BOOL	JIG_MOVE_CHK(int ID);
	tResultVal Result;
	void	TextView(LPCSTR lpcszString, ...);

	void	Pic(CDC *cdc);

	void	InitPrm(int ID);
	void	InitPrm();

	void	TestState(unsigned char col,LPCTSTR lpcszString, ...);

	CWnd*	IN_pWnd;
	byte	m_ID;
	tINFO	m_INFO;
	CString NAME;
	CString ModeNAME;

	COLORREF txcol;
	COLORREF bkcol;

	CFont	font1;

	void	Upload_List(CListCtrl *List);
	void	Upload_Lot_List(CListCtrl *List);
	void	Excel_Save();
	void	FAIL_UPLOAD();
	bool	Excel_Upload();

	// MES 저장
	CString		m_szMesResult;
	CString		Mes_Result();

	//LOT
	void	Lot_InsertDataList();
	void	LOT_Excel_Save();
	bool	LOT_Excel_Upload();
	void	Lot_initCnt();
	void	DeleteITEM();

private:
	CWnd		*m_pMomWnd;
	CEdit		*m_pTStat;
	CMotionControl*		m_pMotion;

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnBnClickedBtnRun();
	afx_msg void OnDestroy();
	afx_msg void OnEnSetfocusEditState();
	afx_msg void OnEnSetfocusStringState();
	void VideoModeCheck();
	bool PowerModeCheck();
	CProgressCtrl m_CtrProgress;
};
