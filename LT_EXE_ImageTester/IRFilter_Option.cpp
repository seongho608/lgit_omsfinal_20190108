// IRFilter_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "IRFilter_Option.h"

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
extern BYTE	m_OverlayArea[CAM_IMAGE_WIDTH][CAM_IMAGE_HEIGHT];
// CIRFilter_Option 대화 상자입니다.

IMPLEMENT_DYNAMIC(CIRFilter_Option, CDialog)

CIRFilter_Option::CIRFilter_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CIRFilter_Option::IDD, pParent)
	, str_R_Threshold(_T(""))
	, str_Fontsize(_T(""))
	, str_PosX(_T(""))
	, str_PosY(_T(""))
	, str_R_MinThreshold(_T(""))
{
	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;

	StartCnt=0;
	b_StopFail = FALSE;
}

CIRFilter_Option::~CIRFilter_Option()
{
}

void CIRFilter_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_IR, m_IRFilterList);
	DDX_Text(pDX, IDC_EDIT_R_Thre, str_R_Threshold);
	DDX_Text(pDX, IDC_EDIT_FontI, str_Fontsize);
	DDX_Text(pDX, IDC_EDIT_PosxI, str_PosX);
	DDX_Text(pDX, IDC_EDIT_PosYI, str_PosY);
	DDX_Control(pDX, IDC_LIST_IR_LOT, m_Lot_IRlist);
	DDX_Text(pDX, IDC_EDIT_R_Thrmin, str_R_MinThreshold);
}

void CIRFilter_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}


void CIRFilter_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
	strTitle.Empty();
	strTitle="IR_INIT";
}


BEGIN_MESSAGE_MAP(CIRFilter_Option, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_RThreSAVE, &CIRFilter_Option::OnBnClickedButtonRthresave)
	ON_EN_CHANGE(IDC_EDIT_R_Thre, &CIRFilter_Option::OnEnChangeEditRThre)
	ON_WM_SHOWWINDOW()
	ON_EN_CHANGE(IDC_EDIT_FontI, &CIRFilter_Option::OnEnChangeEditFonti)
	ON_EN_CHANGE(IDC_EDIT_PosxI, &CIRFilter_Option::OnEnChangeEditPosxi)
	ON_EN_CHANGE(IDC_EDIT_PosYI, &CIRFilter_Option::OnEnChangeEditPosyi)
	ON_BN_CLICKED(IDC_BUTTON_SAVEI, &CIRFilter_Option::OnBnClickedButtonSavei)
	ON_BN_CLICKED(IDC_BUTTON_test, &CIRFilter_Option::OnBnClickedButtontest)
	ON_EN_CHANGE(IDC_EDIT_R_Thrmin, &CIRFilter_Option::OnEnChangeEditRThrmin)
	ON_BN_CLICKED(IDC_BUTTON_test2, &CIRFilter_Option::OnBnClickedButtontest2)
	ON_BN_CLICKED(IDC_BTN_LIGHT_RAY_ON, &CIRFilter_Option::OnBnClickedBtnLightRayOn)
	ON_BN_CLICKED(IDC_BTN_LIGHT_ALL_OFF, &CIRFilter_Option::OnBnClickedBtnLightAllOff)
END_MESSAGE_MAP()

BOOL CIRFilter_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	//((CEdit *) GetDlgItem(IDC_EDIT_BRIGHT))->ShowWindow(FALSE);
	//SETLIST();
	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;
	C_filename=((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	Load_parameter();
	UpdateData(FALSE);
	//UploadList();
	


	Set_List(&m_IRFilterList);
	LOT_Set_List(&m_Lot_IRlist);
	((CButton *)GetDlgItem(IDC_BUTTON_SAVEI))->EnableWindow(0);
	((CButton *)GetDlgItem(IDC_BUTTON_RThreSAVE))->EnableWindow(0);

	InitEVMS();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CIRFilter_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CButton *)GetDlgItem(IDC_BUTTON_RThreSAVE))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_R_Thre))->EnableWindow(0);		
	}
}


void CIRFilter_Option::InitPrm()  
{
	Load_parameter();
	UpdateData(FALSE);
	//UploadList();

	for(int t=0; t<1;t++){
		C_RECT[t].BrightData_Ave =0;
		C_RECT[t].m_Success = FALSE;

	}
}

void CIRFilter_Option::Load_parameter(){
	CFileFind filefind;
	CString str="";
	CString strTitle="";
	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	if((GetPrivateProfileCString(str_model,"NAME",C_filename) != m_INFO.NAME)||(GetPrivateProfileCString(str_model,"MODE",C_filename) != m_INFO.MODE)){//ini파일이 없으면 파일을 생성한다.
		d_R_Threshold = 40;
		m_R_MaxThreshold =40;
		m_R_MinThreshold = 10;
		m_PosX = Cam_PosX;
		m_PosY = Cam_PosY;
		m_FontSize = 200;
		Save_parameter();
	}else{

		strTitle.Empty();
		strTitle="IR_INIT";
		d_R_Threshold=GetPrivateProfileDouble(str_model,strTitle+"R_Thre",-1,C_filename);
		if(d_R_Threshold == -1){
			d_R_Threshold = 40;
			str.Empty();
			str.Format("%3.1f",d_R_Threshold);
			WritePrivateProfileString(str_model,strTitle+"R_Thre",str,C_filename);
		}

		m_R_MaxThreshold = GetPrivateProfileInt(str_model,strTitle+"R_ThreMax",-1,C_filename);
		if(m_R_MaxThreshold == -1){
			m_R_MaxThreshold = 40;
			str.Empty();
			str.Format("%d",m_R_MaxThreshold);
			WritePrivateProfileString(str_model,strTitle+"R_ThreMax",str,C_filename);
		}

		m_R_MinThreshold = GetPrivateProfileInt(str_model,strTitle+"R_ThreMin",-1,C_filename);
		if(m_R_MinThreshold == -1){
			m_R_MinThreshold = 10;
			str.Empty();
			str.Format("%d",m_R_MinThreshold);
			WritePrivateProfileString(str_model,strTitle+"R_ThreMin",str,C_filename);
		}
	

		m_PosX=GetPrivateProfileInt(str_model,strTitle+"Pic_PosX",-1,C_filename);
		if(m_PosX  == -1){
			m_PosX = Cam_PosX;
		}
		m_PosY=GetPrivateProfileInt(str_model,strTitle+"Pic_PosY",-1,C_filename);
		if(m_PosY  == -1){
			m_PosY = Cam_PosY;
		}
		m_FontSize=GetPrivateProfileInt(str_model,strTitle+"Font_Size",-1,C_filename);
		if(m_FontSize  == -1){
			m_FontSize = 200;
		}

		Save_parameter();
	}

	str_R_Threshold.Format("%d",m_R_MaxThreshold);
	str_R_MinThreshold.Format("%d",m_R_MinThreshold);
//	str_R_Threshold.Format("%3.1f",d_R_Threshold);
	str_Fontsize.Format("%d",m_FontSize);
	str_PosX.Format("%d",m_PosX);
	str_PosY.Format("%d",m_PosY);

	C_RECT[0].font = m_FontSize;
	C_RECT[0].m_PosY = m_PosY;
	C_RECT[0].m_PosX = m_PosX;
}
void CIRFilter_Option::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",C_filename);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

void CIRFilter_Option::Save_parameter(){

	CString str="";
	CString strTitle="";

	WritePrivateProfileString(str_model,NULL,"",C_filename);
	str.Empty();
	str.Format("%d",m_INFO.ID);
	WritePrivateProfileString(str_model,"ID",str,C_filename);
	str.Empty();
	str.Format("%s",m_INFO.MODE);
	WritePrivateProfileString(str_model,"MODE",str,C_filename);
	str.Empty();
	str.Format("%s",m_INFO.NAME);
	WritePrivateProfileString(str_model,"NAME",str,C_filename);

	strTitle.Empty();
	strTitle="IR_INIT";

	str.Empty();
	str.Format("%3.1f",d_R_Threshold);
	WritePrivateProfileString(str_model,strTitle+"R_Thre",str,C_filename);

	str.Empty();
	str.Format("%d",m_R_MaxThreshold);
	WritePrivateProfileString(str_model,strTitle+"R_ThreMax",str,C_filename);
	
	str.Empty();
	str.Format("%d",m_R_MinThreshold);
	WritePrivateProfileString(str_model,strTitle+"R_ThreMin",str,C_filename);

	str.Empty();
	str.Format("%d",m_PosX);
	WritePrivateProfileString(str_model,strTitle+"Pic_PosX",str,C_filename);
	
	str.Empty();
	str.Format("%d",m_PosY);
	WritePrivateProfileString(str_model,strTitle+"Pic_PosY",str,C_filename);
	
	str.Empty();
	str.Format("%d",m_FontSize);
	WritePrivateProfileString(str_model,strTitle+"Font_Size",str,C_filename);
}

bool CIRFilter_Option::AvePic(CDC *cdc,int NUM){//AVE값 및 평균 RGB값 추출 및 그리기.
	
	CPen	my_Pan, *old_pan;
	CString TEXTDATA ="";
	CFont m_font, *old_font;
	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);
	
	//if(C_RECT[NUM].Chkdata() == FALSE){return 0;}
	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	if(C_RECT[NUM].m_Success == TRUE){
		my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(BLUE_COLOR);
	}else{
		my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(RED_COLOR);
	}

	
	m_font.CreatePointFont(220,"Arial");
	old_font = cdc->SelectObject(&m_font);
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);


	TEXTDATA.Format("Brightness %6.1f",C_RECT[NUM].BrightData_Ave);
	cdc->TextOut(Cam_PosX/* + (int)(C_RECT[NUM].Height()/2)-10*/,(Cam_PosY+200),TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
	
	//TEXTDATA.Format("R -> %6.1f",C_RECT[NUM].BrightData_Ave_R);
	//cdc->TextOut(C_RECT[NUM].m_PosX /* + (int)(C_RECT[NUM].Height()/2)-10*/,C_RECT[NUM].m_PosY,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
	//
	//TEXTDATA.Format("G -> %6.1f",C_RECT[NUM].BrightData_Ave_G);
	//cdc->TextOut(C_RECT[NUM].m_PosX /* + (int)(C_RECT[NUM].Height()/2)-10*/,C_RECT[NUM].m_PosY + 20,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
	//
	//TEXTDATA.Format("B -> %6.1f",C_RECT[NUM].BrightData_Ave_B);
	//cdc->TextOut(C_RECT[NUM].m_PosX /* + (int)(C_RECT[NUM].Height()/2)-10*/,C_RECT[NUM].m_PosY + 40,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
	//
	//cdc->TextOut(360,240,checkView.GetBuffer(0),checkView.GetLength());
	cdc->SelectObject(old_pan);
	old_pan->DeleteObject();
	my_Pan.DeleteObject();

	cdc->SelectObject(old_font);
	old_font->DeleteObject();
	m_font.DeleteObject();
	return TRUE;
}

void CIRFilter_Option::Pic(CDC *cdc)
{
		AvePic(cdc,0);
}

// CIRFilter_Option 메시지 처리기입니다.
tResultVal CIRFilter_Option::Run()
{	
	bool checkResult=0;
	b_StopFail = FALSE;

	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
		InsertList();
	}	
	int camcnt = 0;
	tResultVal retval = {0,};

	Str_Mes[0] = "0.0";
	Str_Mes[1] = "0";

	//StatePrintf("IRFilter 측정 모드를 시작합니다.");

	int count = 0;
	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan =1;						
			
	for(int i =0;i<10;i++){
		if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0){
			break;
		}else{
		DoEvents(50);
		}
	}	
	AveGen(m_RGBScanbuf,0);
	
	
	CString stateDATA ="IRFilter_ ";

	retval.m_ID = 0x09;
	retval.ValString.Empty();


	if(IR_TEST == FALSE){
		C_RECT[0].BrightData_Ave =0;
		C_RECT[0].m_Success = FALSE;
		retval.m_Success = FALSE;
		Str_Mes[1] = "0";
		stateDATA+= "0 _ FAIL";
		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
		m_IRFilterList.SetItemText(InsertIndex,4,"X");
		m_IRFilterList.SetItemText(InsertIndex,5,"FAIL");		
		//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_IR,"FAIL");
		}
		((CImageTesterDlg *)m_pMomWnd)->m_pResIRFilterOptWnd->RESULT_TEXT(2,"TEST 환경X");
		retval.ValString.Format("TEST 환경이 아닙니다.");
		//StatePrintf("IRFilter TEST 환경이 아닙니다.");
	}
	else{
		/*
			int m_R_MaxThreshold;
	int m_R_MinThreshold;
		*/
		CString str="";
		str.Format("%3.1f",C_RECT[0].BrightData_Ave);
		stateDATA+= str;

		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			m_IRFilterList.SetItemText(InsertIndex,4,str);
		}
		Str_Mes[0] = str;
		if(m_R_MaxThreshold < C_RECT[0].BrightData_Ave){
			stateDATA+= " _ FAIL";
			C_RECT[0].m_Success = FALSE;
			retval.m_Success = FALSE;
			Str_Mes[1] = "0";
			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			m_IRFilterList.SetItemText(InsertIndex,5,"FAIL");
			//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_IR,"FAIL");
			}
			retval.ValString.Format("IR Filter FAIL");
		//	StatePrintf("IRFilter FAIL");
			
			((CImageTesterDlg *)m_pMomWnd)->m_pResIRFilterOptWnd->RESULT_TEXT(2,"FAIL");
		}else if(m_R_MinThreshold > C_RECT[0].BrightData_Ave){
			stateDATA+= " _ FAIL";
			C_RECT[0].m_Success = FALSE;
			retval.m_Success = FALSE;
			Str_Mes[1] = "0";
			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			m_IRFilterList.SetItemText(InsertIndex,5,"FAIL");
			//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_IR,"FAIL");
			}

			StatePrintf("IR LAMP가 켜져있는지 확인해주세요");
			retval.ValString.Format("IR Filter FAIL");
		}else{
			stateDATA+= " _ PASS";
			C_RECT[0].m_Success = TRUE;
			Str_Mes[1] = "1";
			retval.m_Success = TRUE;
			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_IR,"PASS");
			m_IRFilterList.SetItemText(InsertIndex,5,"PASS");
			}
			retval.ValString.Format("IR Filter OK");
		//	StatePrintf("IRFilter OK");
			((CImageTesterDlg *)m_pMomWnd)->m_pResIRFilterOptWnd->RESULT_TEXT(1,"PASS");
			

		}
	}

	CString stat1 ="";
	stat1.Format("%3.1f",C_RECT[0].BrightData_Ave);
	((CImageTesterDlg *)m_pMomWnd)->m_pResIRFilterOptWnd->IRFILTERNUM_TEXT(stat1);
	/*
	retval.m_Val[0] = S_Val.m_ResutX;
	retval.m_Val[1] = S_Val.m_ResutY;
	*/
	//	StatePrintf("IRFilter 측정 모드가 종료되었습니다.");
	
	((CImageTesterDlg *)m_pMomWnd)->StatePrintf(stateDATA);
	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
		StartCnt++;
	}
	return retval;
	
}	

bool CIRFilter_Option::AveGen(LPBYTE IN_RGB,int NUM){//AVE값 및 평균 RGB값 추출 및 그리기.
	
	//m_Success = FALSE;
	C_RECT[NUM].m_Success = FALSE;
	BYTE *BW;
	DWORD RGBLINE=0;
	//BYTE *a_R,*a_G,*a_B;
	BYTE R = 0,G = 0,B = 0;
	DWORD	RGBPIX = 0;
	DWORD	Total_R = 0,Total_G = 0,Total_B = 0;
	int index=0;
//	if(RGBRect.Chkdata() == FALSE){return 0;}
	double Sum =0;
	double Sum_R =0;
	double Sum_G =0;
	double Sum_B =0;
	DWORD Total =m_CAM_SIZE_WIDTH*m_CAM_SIZE_HEIGHT;
	BW = new BYTE[Total];
	memset(BW,0,sizeof(BW));
	int startx = 0;
	int starty = 0;
	int endx = m_CAM_SIZE_WIDTH;
	int endy = m_CAM_SIZE_HEIGHT;
	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	/*a_R = new BYTE[Total];
	memset(a_R,0,sizeof(a_R));

	a_G = new BYTE[Total];
	memset(a_G,0,sizeof(a_G));
	
	a_B = new BYTE[Total];
	memset(a_B,0,sizeof(a_B));*/

	DWORD BW2=0;
	RGBPIX =0;
	RGBLINE =0;
	int count =0;
	for(int y=0; y<m_CAM_SIZE_HEIGHT; y++){
		RGBPIX = 0;
		for(int x=0; x<m_CAM_SIZE_WIDTH; x++){
			if((x >=(Cam_PosX*0.75)) &&(x <=(Cam_PosX*1.25))){
				if((y>=(Cam_PosY*0.625))&&(y<=(Cam_PosY*1.375))){
					if(m_OverlayArea[x][y] == 1){
						B = IN_RGB[RGBLINE + RGBPIX]; 
						G = IN_RGB[RGBLINE + RGBPIX + 1];
						R = IN_RGB[RGBLINE + RGBPIX + 2];
					
						BW2 += (0.29900*R)+(0.58700*G)+(0.11400*B);

						count++;
					}
				}
			}
			RGBPIX+=4;
		}
		RGBLINE+=(m_CAM_SIZE_WIDTH*4);
	}

	double Value = BW2 / (double)count;

	
	double SUM_V=0;
	RGBPIX =0;
	RGBLINE =0;
	count =0;
	BW2=0;
	for(int y=0; y<m_CAM_SIZE_HEIGHT; y++){
		RGBPIX = 0;
		for(int x=0; x<m_CAM_SIZE_WIDTH; x++){
			if((x >=(Cam_PosX*0.75)) &&(x <=(Cam_PosX*1.25))){
				if((y>=(Cam_PosY*0.625))&&(y<=(Cam_PosY*1.375))){
					if(m_OverlayArea[x][y] == 1){
						B = IN_RGB[RGBLINE + RGBPIX]; 
						G = IN_RGB[RGBLINE + RGBPIX + 1];
						R = IN_RGB[RGBLINE + RGBPIX + 2];
					
						BW2 = (0.29900*R)+(0.58700*G)+(0.11400*B);

						SUM_V += ((BW2 -Value)*(BW2-Value));
						count++;
					}
				}
			}
		RGBPIX+=4;
		}
		RGBLINE+=(m_CAM_SIZE_WIDTH*4);
	}

	SUM_V /= count;
	/*C_RECT[NUM].BrightData_Ave_R = Sum_R / (double)index;
	C_RECT[NUM].BrightData_Ave_G = Sum_G / (double)index;
	C_RECT[NUM].BrightData_Ave_B = Sum_B / (double)index;*/

	if(SUM_V > 200){
		IR_TEST = FALSE;
		C_RECT[NUM].m_Success = FALSE;
	}
	else{
		IR_TEST =TRUE;

	}

	if(IR_TEST == TRUE){
		
		
		/*int startx = C_RECT[NUM].m_Left;
		int starty = C_RECT[NUM].m_Top;
		int endx = startx + C_RECT[NUM].Length();
		int endy = starty + C_RECT[NUM].Height();*/
		RGBLINE = starty * (m_CAM_SIZE_WIDTH*4);
			
		for(int lopy = starty;lopy < endy;lopy++){
			RGBPIX = startx * 4;
			for(int lopx = startx;lopx < endx;lopx++){

				if(m_OverlayArea[lopx][lopy] == 1){
					B = IN_RGB[RGBLINE + RGBPIX]; 
					
					G = IN_RGB[RGBLINE + RGBPIX + 1];
					R = IN_RGB[RGBLINE + RGBPIX + 2];
					
					/*a_B[index] = B;
					a_G[index] = G;
					a_R[index] = R;*/
					BW[index] = (0.29900*R)+(0.58700*G)+(0.11400*B);
					index++;
				}
				RGBPIX+=4;
			}
			RGBLINE+=(m_CAM_SIZE_WIDTH*4);
		}

		Sum=0;
		Sum_G=0;
		Sum_G=0;
		Sum_B=0;
		for(int t=0; t<(index-1); t++){
			Sum += BW[t];	
		/*	Sum_R += a_R[t];
			Sum_G += a_G[t];
			Sum_B += a_B[t];*/

		}

		C_RECT[NUM].BrightData_Ave = Sum / (double)index;

	
	}

	delete BW;
	//delete a_R;
	//delete a_G;
	//delete a_B;
	return C_RECT[NUM].m_Success;
}

//---------------------------------------------------------------------WORKLIST
void CIRFilter_Option::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	InsertIndex = m_IRFilterList.InsertItem(StartCnt,"",0);
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_IRFilterList.SetItemText(InsertIndex,0,strCnt);
	
	m_IRFilterList.SetItemText(InsertIndex,1,((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_IRFilterList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_IRFilterList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");



}
void CIRFilter_Option::FAIL_UPLOAD(){
	if ((((CImageTesterDlg *)m_pMomWnd)->LotMod == 1) && (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)){
		InsertList();
		m_IRFilterList.SetItemText(InsertIndex,4,"X");

		m_IRFilterList.SetItemText(InsertIndex,5,"FAIL");
		m_IRFilterList.SetItemText(InsertIndex,6,"STOP");
		//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_IR,"STOP");
		Str_Mes[0] = "0.0";
		Str_Mes[1] = "0";
		StartCnt++;
		b_StopFail = TRUE;

	}
}


void CModel_Create(CIRFilter_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO){
	CRect OptionRect;
	if(pTabWnd != NULL){
		((CIRFilter_Option *)*pWnd) = new CIRFilter_Option(pMomWnd);
		((CIRFilter_Option *)*pWnd)->Setup(pMomWnd,pEdit,INFO);
		((CIRFilter_Option *)*pWnd)->Create(((CIRFilter_Option *)*pWnd)->IDD,pTabWnd);//&m_CtrTab);
		((CIRFilter_Option *)*pWnd)->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		((CIRFilter_Option *)*pWnd)->MoveWindow(20,40,OptionRect.Width(),OptionRect.Height());
	}
}

void CModel_Delete(CIRFilter_Option **pWnd){
	if(((CIRFilter_Option *)*pWnd) != NULL){
		((CIRFilter_Option *)*pWnd)->KillTimer(110);
		((CIRFilter_Option *)*pWnd)->DestroyWindow();
		delete ((CIRFilter_Option *)*pWnd);
		((CIRFilter_Option *)*pWnd) = NULL;	
	}
}

void CIRFilter_Option::Set_List(CListCtrl *List){
	

	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);

	List->InsertColumn(4,"한계점",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"비고",LVCFMT_CENTER, 80);
	
	//List->InsertColumn(13,"Serial",LVCFMT_CENTER, 80);

	ListItemNum=7;
	Copy_List(&m_IRFilterList,((CImageTesterDlg *)m_pMomWnd)->m_pWorkList,ListItemNum);
}

CString CIRFilter_Option::Mes_Result()
{
	CString sz;
	//int		nResult = 0;

	//int nSel = m_IRFilterList.GetItemCount()-1;

	//// 한계점
	//sz = m_IRFilterList.GetItemText(nSel, 4);

	//if(C_RECT[0].m_Success)
	//	nResult = 1;
	//else
	//	nResult = 0;

	//m_szMesResult.Format(_T("%s:%d"), sz, nResult);
	m_szMesResult = _T("");
	if(b_StopFail == FALSE){
	m_szMesResult.Format(_T("%s:%s"),Str_Mes[0],Str_Mes[1]);
	m_szMesResult.Replace(" ","");
	}else{
		m_szMesResult.Format(_T(":1"));
	}
	return m_szMesResult;
}

void CIRFilter_Option::EXCEL_SAVE(){
	CString Item[1]={"한계점"};//수정해야할 곳 0203
		CString Data ="";
	CString str="";
	str = "***********************IR Filter 검사***********************\n\n";
	Data += str;
	
	str.Empty();
	str.Format("한계점 ,Threshold, %3.1f,\n ",d_R_Threshold);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("IRFilter검사",Item,1,&m_IRFilterList,Data);
}

bool CIRFilter_Option::EXCEL_UPLOAD(){
	m_IRFilterList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("IRFilter검사",&m_IRFilterList,1, &StartCnt) == TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return TRUE;
	
}
void CIRFilter_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째라인의앞부분을선택한다.  
		pTStat -> Clear();			   //라인하나를지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}



void CIRFilter_Option::OnEnChangeEditRThre()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	/*UpdateData(TRUE);
	int buf =atoi(str_R_Threshold);
	if((buf >= 0)&&(buf < 255)){
		if((d_R_Threshold == buf)){
			((CButton *)GetDlgItem(IDC_BUTTON_RThreSAVE))->EnableWindow(0);
		}else{
			((CButton *)GetDlgItem(IDC_BUTTON_RThreSAVE))->EnableWindow(1);
		}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_RThreSAVE))->EnableWindow(0);
	}*/
	EditRThrmChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CIRFilter_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
	}
}

void CIRFilter_Option::OnEnChangeEditFonti()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	int X = atoi(str_PosX);
	int Y =atoi(str_PosY);
	int FontSize =atoi(str_Fontsize);
	if((X >= 0)&&(X < m_CAM_SIZE_WIDTH)&&(Y >= 0)&&(Y < m_CAM_SIZE_HEIGHT)&&(FontSize >= 0)){
		if((C_RECT[0].m_PosX == X)&&(C_RECT[0].m_PosY == Y)&&(C_RECT[0].font == FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_SAVEI))->EnableWindow(0);
		}else{
			((CButton *)GetDlgItem(IDC_BUTTON_SAVEI))->EnableWindow(1);
		}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_SAVEI))->EnableWindow(0);
	}
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CIRFilter_Option::OnEnChangeEditPosxi()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	int X = atoi(str_PosX);
	int Y =atoi(str_PosY);
	int FontSize =atoi(str_Fontsize);
	if((X >= 0)&&(X < m_CAM_SIZE_WIDTH)&&(Y >= 0)&&(Y < m_CAM_SIZE_HEIGHT)&&(FontSize >= 0)){
		if((C_RECT[0].m_PosX == X)&&(C_RECT[0].m_PosY == Y)&&(C_RECT[0].font == FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_SAVEI))->EnableWindow(0);
		}else{
			((CButton *)GetDlgItem(IDC_BUTTON_SAVEI))->EnableWindow(1);
		}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_SAVEI))->EnableWindow(0);
	}

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CIRFilter_Option::OnEnChangeEditPosyi()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	int X = atoi(str_PosX);
	int Y =atoi(str_PosY);
	int FontSize =atoi(str_Fontsize);
	if((X >= 0)&&(X < m_CAM_SIZE_WIDTH)&&(Y >= 0)&&(Y < m_CAM_SIZE_HEIGHT)&&(FontSize >= 0)){
		if((C_RECT[0].m_PosX == X)&&(C_RECT[0].m_PosY == Y)&&(C_RECT[0].font == FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_SAVEI))->EnableWindow(0);
		}else{
			((CButton *)GetDlgItem(IDC_BUTTON_SAVEI))->EnableWindow(1);
		}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_SAVEI))->EnableWindow(0);
	}

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CIRFilter_Option::OnBnClickedButtonSavei()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strTitle="";
	strTitle.Empty();
	strTitle="IR_INIT";

	m_FontSize = atoi(str_Fontsize);
	m_PosX = atoi(str_PosX);
	m_PosY = atoi(str_PosY);

	if(m_PosX <0){
		m_PosX =0;
	}
	if(m_PosX >m_CAM_SIZE_WIDTH){
		m_PosX =m_CAM_SIZE_WIDTH;
	}

	if(m_PosY <0){
		m_PosY =0;
	}
	if(m_PosY >m_CAM_SIZE_HEIGHT){
		m_PosY =m_CAM_SIZE_HEIGHT;
	}
	if(m_FontSize <0){
		m_FontSize =0;
	}

	str_Fontsize.Format("%d",m_FontSize);
	str_PosX.Format("%d",m_PosX);
	str_PosY.Format("%d",m_PosY);

	C_RECT[0].font = m_FontSize;
	C_RECT[0].m_PosX = m_PosX;
	C_RECT[0].m_PosY = m_PosY;

	UpdateData(FALSE);

	CString str = "";
	str.Empty();
	str.Format("%d",m_PosX);
	WritePrivateProfileString(str_model,strTitle+"Pic_PosX",str,C_filename);
	str.Empty();
	str.Format("%d",m_PosY);
	WritePrivateProfileString(str_model,strTitle+"Pic_PosY",str,C_filename);
	str.Empty();
	str.Format("%d",m_FontSize);
	WritePrivateProfileString(str_model,strTitle+"Font_Size",str,C_filename);
	((CButton *)GetDlgItem(IDC_BUTTON_SAVEI))->EnableWindow(0);
}

BOOL CIRFilter_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CIRFilter_Option::OnBnClickedButtontest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(0);
	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(1);

}


#pragma region LOT관련 함수
void CIRFilter_Option::LOT_Set_List(CListCtrl *List){

	while(List->DeleteColumn(0));
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT 명",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"한계점",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"비고",LVCFMT_CENTER, 80);
	Lot_InsertNum =8;
	
}

void CIRFilter_Option::LOT_InsertDataList(){
	CString strCnt="";

	int Index = m_Lot_IRlist.InsertItem(Lot_StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index+1);
	m_Lot_IRlist.SetItemText(Index,0,strCnt);

	int CopyIndex = m_IRFilterList.GetItemCount()-1;

	int count =0;
	for(int t=0; t< Lot_InsertNum;t++){
		if((t != 2)&&(t!=0)){
			m_Lot_IRlist.SetItemText(Index,t, m_IRFilterList.GetItemText(CopyIndex,count));
			count++;

		}else if(t==0){
			count++;
		}else{
			m_Lot_IRlist.SetItemText(Index,t,((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;

}
void CIRFilter_Option::LOT_EXCEL_SAVE(){
		CString Item[1]={"한계점"};//수정해야할 곳 0203
		CString Data ="";
	CString str="";
	str = "***********************IR Filter 검사***********************\n\n";
	Data += str;
	
	str.Empty();
	str.Format("한계점 ,Threshold, %3.1f,\n ",d_R_Threshold);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("IRFilter검사",Item,1,&m_Lot_IRlist,Data);
}

bool CIRFilter_Option::LOT_EXCEL_UPLOAD(){
	m_Lot_IRlist.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("IRFilter검사",&m_Lot_IRlist,1,&Lot_StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}

#pragma endregion 
void CIRFilter_Option::OnEnChangeEditRThrmin()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	/*UpdateData(TRUE);
	int buf =atoi(str_R_Threshold);
	if((buf >= 0)&&(buf < 255)){
		if((d_R_Threshold == buf)){
			((CButton *)GetDlgItem(IDC_BUTTON_RThreSAVE))->EnableWindow(0);
		}else{
			((CButton *)GetDlgItem(IDC_BUTTON_RThreSAVE))->EnableWindow(1);
		}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_RThreSAVE))->EnableWindow(0);
	}*/
	EditRThrmChk();
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CIRFilter_Option::EditRThrmChk()
{
	UpdateData(TRUE);
	BOOL b_FLAG = TRUE;

	int buf_max =atoi(str_R_Threshold);
	int buf_min =atoi(str_R_MinThreshold);
	if((buf_max < buf_min)||(buf_max < 0)||(buf_max > 255)||(buf_min < 0)||(buf_min > 255)){
		((CButton *)GetDlgItem(IDC_BUTTON_RThreSAVE))->EnableWindow(0);
	}else if((buf_max == m_R_MaxThreshold)&&(buf_min == m_R_MinThreshold)){
		((CButton *)GetDlgItem(IDC_BUTTON_RThreSAVE))->EnableWindow(0);
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_RThreSAVE))->EnableWindow(1);
	}

}

void CIRFilter_Option::OnBnClickedButtonRthresave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	
	CString strTitle="";
	strTitle.Empty();
	strTitle="IR_INIT";
	m_R_MaxThreshold =atoi(str_R_Threshold);
	m_R_MinThreshold =atoi(str_R_MinThreshold);

	if(m_R_MaxThreshold >255){
		m_R_MaxThreshold = 255;
	}
	if(m_R_MaxThreshold <0){
		m_R_MaxThreshold = 0;
	}

	if(m_R_MinThreshold >255){
		m_R_MinThreshold = 255;
	}
	if(m_R_MinThreshold <0){
		m_R_MinThreshold = 0;
	}

	/*if(d_R_Threshold >255){
		d_R_Threshold = 255;
	}
	if(d_R_Threshold <0){
		d_R_Threshold = 0;
	}*/
	str_R_Threshold.Format("%d",m_R_MaxThreshold);
	str_R_MinThreshold.Format("%d",m_R_MinThreshold);
	UpdateData(FALSE);

	CString str="";
	str.Empty();
	str.Format("%d",m_R_MaxThreshold);
	WritePrivateProfileString(str_model,strTitle+"R_ThreMax",str,C_filename);
	str.Empty();
	str.Format("%d",m_R_MinThreshold);
	WritePrivateProfileString(str_model,strTitle+"R_ThreMin",str,C_filename);
	/*str.Empty();
	str.Format("%3.1f",d_R_Threshold);
	WritePrivateProfileString(str_model,strTitle+"R_Thre",str,C_filename);*/
	((CButton *)GetDlgItem(IDC_BUTTON_RThreSAVE))->EnableWindow(0);

}
void CIRFilter_Option::OnBnClickedButtontest2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CButton *)GetDlgItem(IDC_BUTTON_test2))->EnableWindow(0);
	Run();
	((CButton *)GetDlgItem(IDC_BUTTON_test2))->EnableWindow(1);

}

void CIRFilter_Option::OnBnClickedBtnLightRayOn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->RAY_ONOFF(1);
}

void CIRFilter_Option::OnBnClickedBtnLightAllOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Light_ONOFF(0);
}
