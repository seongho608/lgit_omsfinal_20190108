#pragma once
#include "afxcmn.h"
#include "ETCUSER.H"
#include <vector>
// C_3D_Depth_Option 대화 상자입니다.

class C_3D_Depth_Option : public CDialog
{
	DECLARE_DYNAMIC(C_3D_Depth_Option)

public:
	C_3D_Depth_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~C_3D_Depth_Option();
    

	int m_CAM_SIZE_WIDTH;
	int m_CAM_SIZE_HEIGHT;
	int StartCnt;
	bool    m_Success;
	tResultVal Run();
	void Pic(CDC *cdc);
	void InitPrm();
	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
	void	StatePrintf(LPCSTR lpcszString, ...);
	bool	DepthPic(CDC *cdc,int NUM);


	CString Depth_filename;
	void	Save_parameter();
	void Save(int NUM);
	void	Load_parameter();
	void	UploadList();

	//CNoiseEnvironment *subdlg;
// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_3DDEPTH };


	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void InsertList();
	int InsertIndex;
	int Lot_InsertIndex;
	int ListItemNum;
	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();
	void FAIL_UPLOAD();
	BOOL	b_StopFail;

	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();

	#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	int Lot_StartCnt;
	int Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

	void	InitEVMS();
#pragma endregion

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:

	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtontest();
	bool SNRGen_16bit(WORD *GRAYScanBuf);
	afx_msg void OnBnClickedButtonSave();

	bool m_bAveData;
	bool m_bStdevData;

	double m_dAveData;
	double m_dStdevData;

	double m_dAveMin;
	double m_dAveMax;

	double m_dStdevMin;
	double m_dStdevMax;
	CListCtrl m_WorkList;
	CListCtrl m_Lot_WorkList;
};
void CModel_Create(C_3D_Depth_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO);
void CModel_Delete(C_3D_Depth_Option **pWnd);