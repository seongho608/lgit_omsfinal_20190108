#pragma once
#include "ETCUSER.H"
#include "afxwin.h"
#include "afxcmn.h"


// CResolutionDetection 대화 상자입니다.

struct B_R{
	int m_PosY;
	int m_Left;
	int m_Top;
	int m_Right;
	int m_Bottom;			
	int m_Width;
	int m_Height;
	int m_bmode;
	int m_dir;
};

class CResolutionDetection_Option : public CDialog
{
	DECLARE_DYNAMIC(CResolutionDetection_Option)

public:
	int m_CAM_SIZE_WIDTH;
	int m_CAM_SIZE_HEIGHT;
	int m_dResol_Mode;
	void DrawSFRGraph(CDC *cdc,int NUM); //SFR 그래프 그려주는 함수..
	int GetCheckLine_LR_DU_using_Sharpness(BYTE *BWImage, int check_mode, int Width, int Height, int NUM, int loop_cnt);
	int GetCheckLine_RL_UD_using_Sharpness(BYTE *BWImage, int check_mode, int Width, int Height, int NUM, int loop_cnt);
	
	bool m_b190Angle_Field;

	int RGBScanBufCnt;

	tResultVal Run();

	CResolutionDetection_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	void Setup(CWnd* IN_pMomWnd,CEdit *pTEDIT, tINFO INFO);

	void FAIL_UPLOAD();
	BOOL	b_StopFail;
	virtual ~CResolutionDetection_Option();

	enum { IDD = IDD_OPTION_RESOLUTION };	

// 대화 상자 데이터입니다.

	//---------------------------
	void Set_List(CListCtrl *List);
	void InsertList();
	int InsertIndex;
	int ListItemNum;

	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();
	int StartCnt;
	//---------------------------
		
	bool CHECK_MODE;
	
	int state;
	int EnterState;
	int iSavedItem, iSavedSubitem;
	int NewItem,NewSubitem;
	CRect rect;
	CvPoint GetCenterPoint(LPBYTE IN_RGB);

	bool ChangeCheck;
	int ChangeItem[14];
	int changecount;
//	CString GetItemText(HWND hWnd, int nItem, int nSubItem) const;
	bool GetWideCamCheckFunc(IplImage *srcImage);

	void StatePrintf(LPCSTR lpcszString, ...);
	void Get_Ref_Threshold_Value(LPBYTE IN_RGB,int NUM);
	void SetZoneUsingAutoDetection(LPBYTE IN_RGB);
	bool CheckResolution(LPBYTE IN_RGB,int NUM, int loop_cnt);
	CRectData E_RECT[14];
	CRectData RD_RECT[14];
	B_R BASIC_RECT[14];
	BOOL MES_RES_SUCC[14];

	CRectData Standard_RD_RECT[14];
	int GetResultData(int NUM);
	int GetStandardData(int NUM);
	int GetStandardPeakData(int NUM);
	
	void SetStandardRDZoneParameter(int NUM, int start_x, int start_y, int width, int height, int mode, int dir);
//	void SetDefaultRDZone();
//	void SetLoadParameterRDZone();

	void	SETLIST();
	void	Uploadlist();
	void	Change_DATA();
	void OnClickSaveBtn();
	double GetDistance(int x1, int y1, int x2, int y2);

	void Save(int NUM);
	void Save_parameter();
	void Load_parameter();
	void Pic(CDC *cdc);
	void InitPrm();
	bool ResolutionDectctionPic(CDC *cdc,int lop);
	CString H_filename;
	bool	RD_CHECK_MODE;
	bool	MasterMod;

	BYTE *m_Avg_BW_Array;
	unsigned int *m_dTotal_Sum_Y_Image;
	int GetCheckLine_LR_DU(BYTE *BWImage, int check_mode, int Width, int Height);
	int GetCheckLine_RL_UD(BYTE *BWImage, int check_mode, int Width, int Height);
	int GetCheckLineUsingMTF(BYTE *BWImage, int check_mode, int Width, int Height);
	int GetCheckLineUsingMTFAvg(BYTE *BWImage, int check_mode, int Width, int Height);
	void MakeAvgBWImageArray(LPBYTE IN_RGB, int Width, int Height, int count);

	
	void SetStartEndPos(BYTE *BWImage, int NUM, int mode, int dir, int ref_white_value, int ref_black_value, BYTE *Overlay_BWImage);

	unsigned int m_Black_Ref_Value, m_White_Ref_Value, m_Ref_Threshold, m_BW_Ref_Sub_Value;
	bool Capture_Gray_SAVE(CString FilePath, unsigned char *ImageBuffer, int Width, int Height);
	bool Capture_SAVE(CString FilePath, unsigned char *ImageBuffer);

	void INIT_EIJAGEN();
	void List_COLOR_Change();
	bool Change_DATA_CHECK(bool FLAG);
	void	EditWheelIntSet(int nID,int Val);
	void	EditMinMaxIntSet(int nID,int* Val,int Min,int Max);

	// MES 정보저장
	CString m_szMesResult;
	CString Mes_Result();

	int i_CenterOffset;
	int i_EdgeOffset;

	void	InitEVMS();

#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	int Lot_StartCnt;
	int Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

#pragma endregion
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

private :
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
	CDC			*m_RD_dc;
public:
	int E_Total_PosX;
	int E_Total_PosY;
	int E_Dis_Width;
	int E_Dis_Height;
	int E_Total_Width;
	int E_Total_Height;
	int E_Thresold;

	int E_Edge_PosX;
	int E_Edge_PosY;
	int E_Dis_Width_Edge;
	int E_Dis_Height_Edge;
	int E_Edge_Width;
	int E_Edge_Height;
//	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();	
	int m_dResolutionMTF;		
	virtual BOOL OnInitDialog();
	int m_dMTFRatio;
	CListCtrl m_ctrlRDDataList;
	afx_msg void OnNMClickListRdData(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListRdData(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg	void OnNMDblclkGroupListRdData(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBtnSetzone();
	afx_msg void OnBnClickedBtnDefault();
	afx_msg void OnBnClickedBtnSetMtf();
int m_dResolutionThreshold;
	CListCtrl m_ResolutionList;
	afx_msg void OnBnClickedBtnSetZoneAuto();
	afx_msg void OnBnClickedCheckAuto();
	CButton m_Automation_Check;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	int m_dEdgeMTFRatio;
	int m_dEdgeResolutionThreshold;
	afx_msg void OnNMCustomdrawListRdData(NMHDR *pNMHDR, LRESULT *pResult);
	BOOL b_Auto_Mod;
	afx_msg void OnEnKillfocusEditRdMod();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedButtonRLoad();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnEnChangeRePosx();
	afx_msg void OnEnChangeRePosy();
	afx_msg void OnEnChangeReDisW();
	afx_msg void OnEnChangeReDisH();
	afx_msg void OnEnChangeReWidth();
	afx_msg void OnEnChangeReHeight();
	afx_msg void OnEnChangeEditMtfratio();
	afx_msg void OnEnChangeEditRv();
	afx_msg void OnEnChangeRePosx2();
	afx_msg void OnEnChangeRePosy2();
	afx_msg void OnEnChangeReDisW2();
	afx_msg void OnEnChangeReDisH2();
	afx_msg void OnEnChangeReWidth2();
	afx_msg void OnEnChangeReHeight2();
	afx_msg void OnEnChangeEditEdgeMtf();
	afx_msg void OnEnChangeEditEdgeTrheshold();
	afx_msg void OnEnChangeEditRdMod();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnEnChangeEditCentertoffset();
	afx_msg void OnEnChangeEditEdgeoffset();
	afx_msg void OnBnClickedButtonSaveOffset();
	CString str_CenterOffset;
	CString str_EdgeOffset;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedCheckSetLimited();
	afx_msg void OnBnClickedCheckSetSfr();
	afx_msg void OnBnClickedCheck190f();
	int m_dResolutionThreshold_Peak;
	int m_dEdgeResolutionThreshold_Peak;
	CListCtrl m_Lot_ResolutionList;
};

void CModel_Create(CResolutionDetection_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO);
void CModel_Delete(CResolutionDetection_Option **pWnd);