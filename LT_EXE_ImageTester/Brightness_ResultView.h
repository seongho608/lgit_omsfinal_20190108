#pragma once

// CBrightness_ResultView 대화 상자입니다.

class CBrightness_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CBrightness_ResultView)

public:
	CBrightness_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CBrightness_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_BRIGHTNESS };
	void	Setup(CWnd* IN_pMomWnd);
	CFont ft1;

	COLORREF txcol_TVal,bkcol_TVal;
	COLORREF txcol_TVal2,bkcol_TVal2;
	COLORREF txcol_TVal3,bkcol_TVal3;
	COLORREF txcol_TVal4,bkcol_TVal4;
	COLORREF txcol_TVal5,bkcol_TVal5;
	void	LOCATION_TEXT(LPCSTR lpcszString, ...);
	void	LOCATION_TEXT2(LPCSTR lpcszString, ...);
	void	LOCATION_TEXT3(LPCSTR lpcszString, ...);
	void	LOCATION_TEXT4(LPCSTR lpcszString, ...);
	void	LOCATION_TEXT5(LPCSTR lpcszString, ...);

	void	VALUE_TEXT(LPCSTR lpcszString, ...);
	void	VALUE_TEXT2(LPCSTR lpcszString, ...);
	void	VALUE_TEXT3(LPCSTR lpcszString, ...);
	void	VALUE_TEXT4(LPCSTR lpcszString, ...);
	void	VALUE_TEXT5(LPCSTR lpcszString, ...);

	void	RESULT_TEXT(unsigned char col,LPCSTR lpcszString, ...);
	void	RESULT_TEXT2(unsigned char col,LPCSTR lpcszString, ...);
	void	RESULT_TEXT3(unsigned char col,LPCSTR lpcszString, ...);
	void	RESULT_TEXT4(unsigned char col,LPCSTR lpcszString, ...);
	void	RESULT_TEXT5(unsigned char col,LPCSTR lpcszString, ...);

	void	Initstat();

private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnSetfocusEditLocation1();
	afx_msg void OnEnSetfocusEditResult();
	afx_msg void OnEnSetfocusEditValue();
	afx_msg void OnEnSetfocusEditLocation2();
	afx_msg void OnEnSetfocusEditValue2();
	afx_msg void OnEnSetfocusEditResult2();
	afx_msg void OnEnSetfocusEditLocation3();
	afx_msg void OnEnSetfocusEditValue3();
	afx_msg void OnEnSetfocusEditResult3();
	afx_msg void OnEnSetfocusEditLocation4();
	afx_msg void OnEnSetfocusEditValue4();
	afx_msg void OnEnSetfocusEditResult4();
	afx_msg void OnEnSetfocusEditLocation5();
	afx_msg void OnEnSetfocusEditValue5();
	afx_msg void OnEnSetfocusEditResult5();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
