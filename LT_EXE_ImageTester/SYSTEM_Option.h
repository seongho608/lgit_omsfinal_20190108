#pragma once
#include "afxcmn.h"

// CSYSTEM_Option 대화 상자입니다.

class CSYSTEM_Option : public CDialog
{
	DECLARE_DYNAMIC(CSYSTEM_Option)

public:
	CSYSTEM_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSYSTEM_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_SYSTEM };

	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);

	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	CFont	ft,ft2;

	COLORREF st_col,Valcol[5];
	CString str_SYS[5];

	void	LOT_InsertDataList();
	void	SYS_RESULT(int num,int col,LPCSTR lpcszString, ...);
	void	SYS_STATE(int col,LPCSTR lpcszString, ...);
	void	SYS_RESULT_STATE(int col,LPCSTR lpcszString, ...);
	BOOL	DTC_ERASE();

	CComboBox	*pComboMode;
	BOOL	b_SYSMODE;
	
	BOOL	SYSTEM_READ_ALL();
	BOOL	SYSTEM_WRITE_ALL();
	BOOL	SYSTEM_READ_ALL_TEST();
	int		m_Etc_State[5];
	//---------------------------------------------
	tResultVal Run(void);
	void	Pic(CDC *cdc);
	bool	SystemPic(CDC *cdc);
	void	InitPrm();
	void	Save(int NUM);
	void	initstat();

	void	Set_List(CListCtrl *List);
	void	LOT_Set_List(CListCtrl *List);
	int		Lot_InsertNum;

	void	EXCEL_SAVE();
	void	LOT_EXCEL_SAVE();
	bool	EXCEL_UPLOAD();
	bool	LOT_EXCEL_UPLOAD();
	void	InsertList();
	int		InsertIndex;
	int		ListItemNum;

	void	Load_parameter();
	void	Save_parameter();

	void	SYS_MODE_STATE(LPCSTR lpcszString, ...);
	
	BOOL	b_StopFail;
	void	FAIL_UPLOAD();
	int		StartCnt;
	int		Lot_StartCnt;
	
	void	InitEVMS();
	//-------------------------------------------------
	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();

//	CString str_FWrdata[5];//0: HW_READ 1: SW_READ 2: CAN READ 3: 제조일자 4: 품번

	void	SYS_ST_VALUE(int num,LPCSTR lpcszString, ...);
	void	SYS_ST_VALUE_SET();
	void	SYS_ST_WR(int num,LPCSTR lpcszString, ...);
	void	SYS_ST_WR_SET();

	CString Old_str_Wrdata1;
	CString Old_str_Wrdata2;
	CString Old_str_Wrdata3;
	CString Old_str_Wrdata4;
	CString Old_str_Wrdata5;

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
	CString		strTitle;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnSysread();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CString str_Wrdata1;
	CString str_Wrdata2;
	CString str_Wrdata3;
	CString str_Wrdata4;
	CString str_Wrdata5;

	afx_msg void OnEnChangeEditValueWr1();
	afx_msg void OnEnChangeEditValueWr2();
	afx_msg void OnEnChangeEditValueWr3();
	afx_msg void OnEnChangeEditValueWr4();
	afx_msg void OnEnChangeEditValueWr5();
	afx_msg void OnBnClickedBtnWrsave();
	afx_msg void OnBnClickedBtnSysdefault();

	BOOL b_CurTimeSet;

	afx_msg void OnBnClickedCheckCurtimeset();
	afx_msg void OnBnClickedBtnSyswrie();

	CListCtrl m_SYSList;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	
//	afx_msg void OnCbnSelchangeComboSystemmode();
	CListCtrl m_Lot_SYSList;
};
