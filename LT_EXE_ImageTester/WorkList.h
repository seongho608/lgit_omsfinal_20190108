#pragma once
#include "afxcmn.h"
#include "resource.h"

// CWorkList 대화 상자입니다.

class CWorkList : public CDialog
{
	DECLARE_DYNAMIC(CWorkList)

public:
	CWorkList(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CWorkList();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_WORKLIST };
	void	Setup(CWnd* IN_pMomWnd);
	void	UpdateRect(int parm_edit_id);
	void	UpdateRect();
	void	SearchList(CString TestNAME);
private:
	CWnd		*m_pMomWnd;	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CListCtrl m_TEST_Item;
	CListCtrl m_WorkList;
	afx_msg void OnDestroy();
	afx_msg void OnLvnItemchangedListSelTest(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickListSelTest(NMHDR *pNMHDR, LRESULT *pResult);
};
