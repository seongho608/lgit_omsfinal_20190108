#pragma once
#include "afxwin.h"


// CParticle_View_LG 대화 상자입니다.

class CParticle_View_LG : public CDialog
{
	DECLARE_DYNAMIC(CParticle_View_LG)

public:
	CParticle_View_LG(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CParticle_View_LG();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_PARTICLEVIEWLG };
	void	Setup(CWnd* IN_pMomWnd);
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	CString str_OutName;
	CString str_Path;
	CString str_YPath;
	CString str_YuvPath;
	CString str_BmpPath;
	CString str_CsvPath;
	void	NameUpdate(LPCSTR Name);
	void	NameUpdate(LPCSTR Name,LPCSTR Path);
	
private:
	CWnd		*m_pMomWnd;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CStatic m_PaticleViewFrame;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnYsave();
//	afx_msg void OnBnClickedBtnYsave2();
//	afx_msg void OnBnClickedBtnYsave3();
	afx_msg void OnBnClickedBtnYuvsave();
	afx_msg void OnBnClickedBtnBmpsave();
	afx_msg void OnBnClickedBtnCsvsave();
	CString str_SaveName;
	afx_msg void OnEnChangeEditSavename();
	afx_msg void OnBnClickedBtnNamesave();
	CString str_SavePathedit;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
