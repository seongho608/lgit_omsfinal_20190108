#pragma once
#include "afxcmn.h"
#include "ETCUSER.H"
#include "resource.h"
#include <vector>
// Temperature_Option 대화 상자입니다.

class Temperature_Option : public CDialog
{
	DECLARE_DYNAMIC(Temperature_Option)

public:
	Temperature_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~Temperature_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_Temperature };


	DWORD	m_dwSlaveIDTemper;
	DWORD	m_dwAddressTemper;
	UINT	m_nTemperature;
	UINT	m_nMinSpec;
	UINT	m_nMaxSpec;
	BOOL	b_StopFail;
	bool    m_Success;
	CListCtrl m_WorkList;
	CListCtrl m_Lot_WorkList;
	int InsertIndex;
	int		StartCnt;
	CString Str_Mes[2];
	double m_dbTemper;
	int ListItemNum;
	int Lot_InsertNum;
	int		Lot_StartCnt;
	int Lot_InsertIndex;

	CString m_szMesResult;

	void	InsertList();
	void Set_List(CListCtrl *List);
	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();
	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();
	void	OnSetChange_Data(int iVal);
	//void	OnSetList_Header();
	//void	OnSetList_Data();
	void	Pic(CDC *cdc);
	void	InitPrm();
	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd, CEdit	*pTEDIT, tINFO INFO);
	void	StatePrintf(LPCSTR lpcszString, ...);
	bool	Temperature_Option_Pic(CDC *cdc, int NUM);
	void	Save(int NUM);

	void	Save_parameter();
	void	Load_parameter();
	void	UploadList();

	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();
	void FAIL_UPLOAD();
	CString Mes_Result();

	tResultVal Run();
	UINT Edit_GetValue(UINT nID, BOOL bHex = TRUE);
	void Edit_SetValue(UINT nID, DWORD wData, BOOL bHex = TRUE);

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	BOOL OnInitDialog();

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
	CString		Temperature_Sensor_filename;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedButtonTsTest();
	afx_msg void OnBnClickedButtonTsSave();
};

void CModel_Create(Temperature_Option **pWnd, CTabCtrl *pTabWnd, CWnd* pMomWnd, CEdit	*pEdit, tINFO INFO);
void CModel_Delete(Temperature_Option **pWnd);