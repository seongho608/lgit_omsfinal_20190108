
// LT_EXE_ImageTesterDlg.cpp : 구현 파일


#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "CommonFunction.h"
#include "CRC16.h"
#include <math.h>
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

const int SOCK_TCP	= 0;
const int SOCK_UDP  = 1;
BYTE	m_pImage[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 2];
BYTE	m_pFailRaw[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 2];
BYTE	m_RGBIn[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
WORD	m_GRAYIn[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];
BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
WORD	m_GRAYScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT*2];
BYTE	m_pFailScan[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
BYTE	m_YUVIn[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 2];
BYTE	m_YUVOut[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 2];
BYTE	m_pYOut[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];

BYTE	m_RGBScanAvgBuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
BYTE	m_ChartArea[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];
BYTE	m_OverlayArea[CAM_IMAGE_WIDTH][CAM_IMAGE_HEIGHT];

//#define b_luritechDebugMODE 1 // "EtcModelDlg.h"로...
#define Def_Evmsmode 1//1:작업자 모드 0:전문가 모드 
#define Def_IMGMode 1//1:IMG 모드 0:영상 모드 

#define Def_Current 1 // 1 소숫점 0 정수
// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CImageTesterDlg 대화 상자
CImageTesterDlg::CImageTesterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CImageTesterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
	m_bUseSensor = TRUE;
	m_bDoorSensor = FALSE;
	m_bSafetySensor = FALSE;
	m_EmergencyStatus = FALSE;

	b_ParticleCaptureMode = FALSE;
	b_Particle_Result = FALSE;
	m_nVideoRegisterMode = VR_POWEROFF;

	m_nRoffset = 0;
	count_V=0;
	btndelaycnt = 0;
	m_exbtn = 0;
	Overlay_State = 1;
	b_ETCRunMODE = FALSE;
	b_Chk_MESEn =FALSE;
	b_UserMode =FALSE;
	B_CAMRUN = FALSE;
	STATID=0;
	//ComArtWnd = NULL;
	m_pCommWnd = NULL;
	m_pTStat = NULL;
	m_pMainCombo = NULL;
	m_pControlWnd = NULL;
	b_CaptureMode = FALSE;
	b_CaptureWindowMode = FALSE;
	bParticleView = FALSE;
	bParticleCnt = 0;
	b_StartCommand = FALSE;
	b_StopCommand = FALSE;
	b_Particle_fail=FALSE;
	m_pModOptTabWnd = NULL;
	Get_Overcurrent = FALSE;
	b_Cylinder_In = FALSE;
	b_Cylinder_Out = FALSE;
	b_PWRON = FALSE;
	m_pBKStateDlg= NULL;
	m_pBKDialog= NULL;
	m_pModResTabWnd= NULL;
	m_pModResTabWnd2= NULL;
	m_pStandOptWnd = NULL;
	m_pStandOptMesWnd = NULL;
	m_pModSetDlgWnd = NULL;
	m_pModOptDlgWnd = NULL;
	m_pModSelDlgWnd = NULL;
	m_pResWorkerOptWnd = NULL;
	m_pResLotWnd = NULL;
	m_pStandWnd = NULL;
	m_pWorkListWnd = NULL;
	m_pWorkList = NULL;
	m_pOptPogoDlgWnd = NULL;
	m_pCommWnd = NULL;
	m_pMasterMonitor = NULL;
	m_pSubStandOptWnd= NULL;
	m_pResStandOptWnd= NULL;

	m_pAngleOptWnd = NULL;
	m_pResAngleOptWnd = NULL;

    m_pBrightnessOptWnd = NULL;
	m_pResBrightnessOptWnd = NULL;

	m_pSubCPointOptWnd = NULL;
	m_pResCPointOptWnd = NULL;
	m_pMicomCenterWnd = NULL;
	m_ParticleView = NULL;

	str_pCsvdata = "";

	OpticEnable=0;
	m_CurrentVal = 0;
	m_CurrentVal2 = 0;
	m_CurrentVal3 = 0;
	m_CurrentVal4 = 0;
	m_CurrentVal5 = 0;

	m_pColorOptWnd = NULL;
	m_pResColorOptWnd = NULL;

	m_pColorRvsOptWnd = NULL;
	m_pResColorRvsOptWnd = NULL;

	m_pDistortionOptWnd = NULL;
	m_pResDistortionOptWnd = NULL;

	m_pIRFilterOptWnd = NULL;
	m_pResIRFilterOptWnd = NULL;
	m_pPatternNoiseOptWnd = NULL;
	m_pResPatternNoiseOptWnd = NULL;

	m_pResolutionOptWnd = NULL;
 	m_pResResolutionOptWnd = NULL;

	m_pSFROptWnd = NULL;
	m_pResSFROptWnd = NULL;

	m_pRotateOptWnd = NULL;
	m_pResRotateOptWnd = NULL;

	m_pOverlayPositionOptWnd = NULL;
	m_pResOverlayPositionOptWnd = NULL;

	m_pVEROptWnd = NULL;
	m_pResVEROptWnd = NULL;

	m_pDTCOptWnd = NULL;
	m_pResDTCOptWnd = NULL;

	m_pSYSTEMOptWnd = NULL;
	m_pResSYSTEMOptWnd = NULL;

	m_pResVerifyOptWnd = NULL;
	m_pVerifyOptWnd = NULL;

	m_p3D_DepthOptWnd = NULL;
	m_pRes3D_DepthOptWnd = NULL;

	m_pHotPixel_OptWnd = NULL;
	m_pResHotPixelOptWnd = NULL;
	
	m_pDepthNoise_OptWnd = NULL;
	m_pResDepthNoiseOptWnd = NULL;

	m_pTemperature_OptWnd = NULL;
	m_pResTemperatureOptWnd = NULL;


	m_pNewLotWnd= NULL;
	m_ExistLotWnd= NULL;
	m_pCommWnd= NULL;
	m_pLGE_CamSetWnd = NULL;
	b_CamSet = FALSE;
	for(int t=0; t<MAX_TEST_NUM; t++){
		m_psubmodel[t] = NULL;
		
	}
	m_CamSerial.Port = 5;
	m_CamSerial.DataBit = 3;
	m_CamSerial.BaudRate =7;
	m_CamSerial.Parity =0;
	m_CamSerial.StopBit =0;

	m_ConSerial_Center.Port = 4;
	m_ConSerial_Center.DataBit = 3;
	m_ConSerial_Center.BaudRate = 7;
	m_ConSerial_Center.Parity = 0;
	m_ConSerial_Center.StopBit = 0;

	m_ConSerial_Side.Port = 3;
	m_ConSerial_Side.DataBit = 3;
	m_ConSerial_Side.BaudRate = 7;
	m_ConSerial_Side.Parity = 0;
	m_ConSerial_Side.StopBit = 0;

	b_SecretLgOption = FALSE;
	b_SecretOption = FALSE;
	b_masterBtn = FALSE;

	m_pDlgOrigin = NULL;
	b_Chk_Align = FALSE;
	m_Align = 0;

	m_bAreaSensor	= FALSE;
	m_bDoorOpen		= FALSE;
}

void CImageTesterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CAM_FRAME, m_CamFrame);
	DDX_Control(pDX, IDC_LIST_MAINWORK, MainWork_List);
	DDX_Control(pDX, IDC_LIST_CURRENT_LOT, m_Lot_CurrentList);
	DDX_Control(pDX, IDC_LIST_MAINWORK_LOT, m_Lot_MainWorkList);
	DDX_Control(pDX, IDC_LIST_CURRENT, m_CurrList);
	DDX_Control(pDX, IDC_EDIT_MES_RECV, m_pMesRecv);
	DDX_Control(pDX, IDC_LIST_EEPROMLIST_LOT, m_Lot_EepromList);
	DDX_Control(pDX, IDC_LIST_EEPROMLIST, m_Work_EepromList);
}

BEGIN_MESSAGE_MAP(CImageTesterDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(WM_COMM_EMERGENCY,		OnEmergencyPause)
	ON_MESSAGE(WM_COMM_START,			OnStartBtn)
	ON_MESSAGE(WM_COMM_STOP,			OnStopBtn)
	ON_MESSAGE(WM_COMM_READ3,			OnCamCommunication) // 실질적인 통신이 이루어지는 곳(RECEIVE) 
	ON_MESSAGE(WM_COMM_READ_CONCenter,	OnConCommunication_Center) // 실질적인 통신이 이루어지는 곳(RECEIVE) 
	ON_MESSAGE(WM_COMM_READ_CONSide,	OnConCommunication_Side) // 실질적인 통신이 이루어지는 곳(RECEIVE) 
	ON_MESSAGE(WM_Receive_Date,			OnTCPReceive)
	ON_MESSAGE(WM_UPDATE_CONNECTION,	OnUpdateConnection)
	ON_WM_DRAWITEM()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON1,			&CImageTesterDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BTN_USER_CHANGE,	&CImageTesterDlg::OnBnClickedBtnUserChange)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_WORK_BUTTON,	&CImageTesterDlg::OnBnClickedBtnWorkButton)
	ON_CBN_SELCHANGE(IDC_MODEL_NAME,	&CImageTesterDlg::OnCbnSelchangeModelName)
	ON_BN_CLICKED(IDC_BTN_TEST_SETTING, &CImageTesterDlg::OnBnClickedBtnTestSetting)
	ON_BN_CLICKED(IDC_BTN_ETC_SETTING,	&CImageTesterDlg::OnBnClickedBtnEtcSetting)
	ON_BN_CLICKED(IDC_BTN_MASTER_SET,	&CImageTesterDlg::OnBnClickedBtnMasterSet)
	ON_BN_CLICKED(IDC_BTN_MAIN,			&CImageTesterDlg::OnBnClickedBtnMain)
	ON_EN_SETFOCUS(IDC_EDIT_PORTNAME,	&CImageTesterDlg::OnEnSetfocusEditPortname)
	ON_EN_SETFOCUS(IDC_EDIT_PORTSTATE,	&CImageTesterDlg::OnEnSetfocusEditPortstate)
	ON_EN_SETFOCUS(IDC_EDIT_CAMNAME,	&CImageTesterDlg::OnEnSetfocusEditCamname)
	ON_EN_SETFOCUS(IDC_EDIT_CAMSTATE,	&CImageTesterDlg::OnEnSetfocusEditCamstate)
	ON_EN_SETFOCUS(IDC_TEST_STATUS,		&CImageTesterDlg::OnEnSetfocusTestStatus)
	ON_BN_CLICKED(IDCANCEL,				&CImageTesterDlg::OnBnClickedCancel)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_RESULT,	&CImageTesterDlg::OnTcnSelchangeTabResult)
	ON_NOTIFY(TCN_SELCHANGING, IDC_TAB_RESULT,	&CImageTesterDlg::OnTcnSelchangingTabResult)
	ON_BN_CLICKED(IDC_BTN_START,		&CImageTesterDlg::OnBnClickedBtnStart)
	ON_BN_CLICKED(IDC_BTN_STOP,			&CImageTesterDlg::OnBnClickedBtnStop)
	ON_EN_SETFOCUS(IDC_EDIT_POGONAME,	&CImageTesterDlg::OnEnSetfocusEditPogoname)
	ON_EN_SETFOCUS(IDC_EDIT_COUNT_TEST, &CImageTesterDlg::OnEnSetfocusEditCountTest)
	ON_EN_SETFOCUS(IDC_EDIT_ProTime,	&CImageTesterDlg::OnEnSetfocusEditProtime)
	ON_EN_SETFOCUS(IDC_EDIT_ProTime2,	&CImageTesterDlg::OnEnSetfocusEditProtime2)
//	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_INDEX, &CImageTesterDlg::OnTcnSelchangeTabIndex)
	ON_BN_CLICKED(IDC_BTN_COMMU,		&CImageTesterDlg::OnBnClickedBtnCommu)
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_CAPTURE,				&CImageTesterDlg::OnCapture)
	ON_BN_CLICKED(IDC_BUTTON6,			&CImageTesterDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON8,			&CImageTesterDlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON7,			&CImageTesterDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON9,			&CImageTesterDlg::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON10,			&CImageTesterDlg::OnBnClickedButton10)
	ON_BN_CLICKED(IDC_BUTTON11,			&CImageTesterDlg::OnBnClickedButton11)
	ON_BN_CLICKED(IDC_BTN_CAPTURE,		&CImageTesterDlg::OnBnClickedBtnCapture)
	ON_MESSAGE(WM_CAMERA_CHG_STATUS,	OnCameraChgStatus)
	ON_MESSAGE(WM_CAMERA_RECV_VIDEO,	OnCameraRecvVideo)
	ON_BN_CLICKED(IDC_BUTTON4,			&CImageTesterDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON3,			&CImageTesterDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON12,			&CImageTesterDlg::OnBnClickedButton12)
	ON_BN_CLICKED(IDC_BUTTON13,			&CImageTesterDlg::OnBnClickedButton13)
	ON_BN_CLICKED(IDC_BUTTON14,			&CImageTesterDlg::OnBnClickedButton14)
	ON_BN_CLICKED(IDC_BUTTON2, &CImageTesterDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_INIT, &CImageTesterDlg::OnBnClickedInit)
END_MESSAGE_MAP()


// CImageTesterDlg 메시지 처리기
//=============================================================================
// Method		: OnCameraChgStatus
// Access		: public  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/6/13 - 11:21
// Desc.		:
//=============================================================================
LRESULT CImageTesterDlg::OnCameraChgStatus(WPARAM wParam, LPARAM lParam)
{
	if (b_PWRON == TRUE && !(m_LVDSVideo.GetSignalStatus(0))){

		Power_Off();
	}

	return TRUE;
}

//=============================================================================
// Method		: OnCameraRecvVideo
// Access		: public  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/6/13 - 11:21
// Desc.		:
//=============================================================================
LRESULT CImageTesterDlg::OnCameraRecvVideo(WPARAM wParam, LPARAM lParam)
{
	ST_VideoRGB* pRGB = m_LVDSVideo.GetRecvVideoRGB(0);
	LPBYTE pRGBDATA = m_LVDSVideo.GetRecvRGBData(0);
	DisplayVideo(0, pRGBDATA, pRGB->m_dwSize, pRGB->m_dwWidth, pRGB->m_dwHeight);

	return TRUE;
}
//=============================================================================
// Method		: InitDevicez
// Access		: virtual public  
// Returns		: void
// Parameter	: __in HWND hWndOwner
// Qualifier	:
// Last Update	: 2017/6/13 - 11:13
// Desc.		:
//=============================================================================
void CImageTesterDlg::InitDevicez(__in HWND hWndOwner /*= NULL*/)
{
	// 프레임 그래버 보드
	m_LVDSVideo.SetOwner(hWndOwner);	
	m_LVDSVideo.SetWM_RecvVideo(WM_CAMERA_RECV_VIDEO);
	m_LVDSVideo.SetWM_ChgStatus(WM_CAMERA_CHG_STATUS);
	m_LVDSVideo.SetColorModel_All(enColorModel::CM_Bayer_GRBG, enColorModelOut::CMOUT_RGB);
	m_LVDSVideo.SetFrameRate_All(30.0f);
	if (m_pTStat != NULL)
		m_LVDSVideo.m_pTStat = m_pTStat;

	// OMS 테스트 세팅 (1280 x 960 -> 640 x 480) 우/하
	//ST_DAQOption stDaqOpt;
	stDaqOpt.dWidthMultiple		= 1.0;
	stDaqOpt.dHeightMultiple	= 0.5;
	//stDaqOpt.dwClock			= 0;
	stDaqOpt.nDataMode			= enDAQDataMode::DAQ_Data_16bit_Mode;
	stDaqOpt.nHsyncPolarity		= enDAQHsyncPolarity::DAQ_HsyncPol_Inverse;
	stDaqOpt.nPClockPolarity	= enDAQPclkPolarity::DAQ_PclkPol_RisingEdge;
	stDaqOpt.nDvalUse			= enDAQDvalUse::DAQ_DeUse_DVAL_Use;
	stDaqOpt.nVideoMode			= enDAQVideoMode::DAQ_Video_MIPI_Mode;
	stDaqOpt.nMIPILane			= enDAQMIPILane::DAQMIPILane_2;
	stDaqOpt.nSensorType		= enImgSensorType::enImgSensorType_RGBbayer12;
	stDaqOpt.nConvFormat		= enConvFormat::Conv_YCbYCr_BGGR;
	stDaqOpt.byBitPerPixels		= 12;
	//stDaqOpt.dShiftExp			= 0.0f;
	stDaqOpt.SetExposure(0.0f);
	stDaqOpt.nCropFrame = Crop_OddFrame;//Crop_OddFrame,Crop_EvenFrame;
	m_LVDSVideo.SetDAQOption_All(stDaqOpt);
	//m_LVDSVideo.SetI2CFilePath_All(_T("C:/_Dev/Source/Repos/_Dev_Module/_Binary_x64/oms_RESET_APPLY.set"));
}

//=============================================================================
// Method		: ConnectGrabberBrd
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2017/6/13 - 11:13
// Desc.		:
//=============================================================================
BOOL CImageTesterDlg::ConnectGrabberBrd(__in BOOL bConnect)
{
	return TRUE;
}

//=============================================================================
// Method		: LGEVideo_Start
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/13 - 11:14
// Desc.		:
//=============================================================================
BOOL CImageTesterDlg::LGEVideo_Start(UINT nMODE)
{
	m_LVDSVideo.m_bUseManaualMode = FALSE;
	CString szFilePath;
	CString szSetFile;

	if (nMODE == VR_PO_REGISTER)
	{
		szFilePath = modelFolder_SetFile_path + ("\\") + SAVE_SETFILE;
		m_LVDSVideo.SetI2CFilePath(0, szFilePath, SAVE_SETFILE);
		szSetFile.Format(_T("Register File_[ %s ] Setting"), SAVE_SETFILE);
		StatePrintf(szSetFile);
	}
	else if (nMODE == VR_PO_REGISTER_2) //광원 시 Register
	{
		szFilePath = modelFolder_SetFile_path + ("\\") + SAVE_SETFILE_Second;
		m_LVDSVideo.SetI2CFilePath(0, szFilePath, SAVE_SETFILE_Second);
		szSetFile.Format(_T("광원 Register File_[ %s ] Setting"), SAVE_SETFILE_Second);
		StatePrintf(szSetFile);
	}
		

	if (m_LVDSVideo.Cam_Restart(0)){
		//if (m_LVDSVideo.Capture_Start(0)){
			StatePrintf("I2C Write Pass");
			SetProperty_ALL(CAMSET_SAVE);
			return TRUE;
	//	}
	//	else{
	//		StatePrintf("I2C Write Fail !!!");
	//		return FALSE;
	//	}
	}
	else{
		StatePrintf("I2C Write Fail !!!");
		return FALSE;
	}
}

//=============================================================================
// Method		: LGEVideo_Stop
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/13 - 11:14
// Desc.		:
//=============================================================================
void CImageTesterDlg::LGEVideo_Stop()
{
	m_LVDSVideo.Capture_Stop(0);
	//m_LVDSVideo.Close();
}
//=============================================================================
// Method		: DisplayVideo
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Parameter	: __in LPBYTE lpbyRGB
// Parameter	: __in DWORD dwRGBSize
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Qualifier	:
// Last Update	: 2017/6/13 - 11:13
// Desc.		:
//=============================================================================
void CImageTesterDlg::DisplayVideo(__in UINT nChIdx, __in LPBYTE lpbyRGB, __in DWORD dwRGBSize, __in UINT nWidth, __in UINT nHeight)
{
	BITMAPINFO		m_biInfo;
	ZeroMemory(&m_biInfo, sizeof(m_biInfo));
	m_biInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	m_biInfo.bmiHeader.biWidth = nWidth;//CAM_IMAGE_WIDTH;
	m_biInfo.bmiHeader.biHeight = nHeight;//CAM_IMAGE_HEIGHT;
	m_biInfo.bmiHeader.biPlanes = 1;
	m_biInfo.bmiHeader.biBitCount = 24;//32
	m_biInfo.bmiHeader.biCompression = 0;
	m_biInfo.bmiHeader.biSizeImage = dwRGBSize;//CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4;

	BOOL bView = TRUE;
	CDC *pcDC;

	//-----------가상메모리를 만든다.------------------
	CDC	*pcMemDC = new CDC;
	CBitmap	*pcDIB = new CBitmap;

	pcDC = m_CamFrame.GetDC();

#if Def_IMGMode 
	//strLoadIMGPath,"D:\\26.PNG"

	if (TRUE == strLoadIMGPath_16.IsEmpty())
		return;

	IplImage* t_image = cvLoadImage(strLoadIMGPath_16, CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
	//IplImage *t_image = cvLoadImage(strLoadIMGPath_16, -1);

	//cv::Mat Bit16Mat(nHeight, nWidth, CV_16UC1, t_image->imageData);
	//Bit16Mat.convertTo(Bit16Mat, CV_8UC1, 0.00390625);// *pow(2, 100));///(-1) * 100));//CAMSET_SAVE.m_ViwerGain));

	//cv::Mat rgb8BitMat(nHeight, nWidth, CV_8UC3, m_RGBIn);
	//cv::cvtColor(Bit16Mat, rgb8BitMat, CV_GRAY2BGR);

	// Copy the data into an OpenCV Mat structure
	cv::Mat mat16uc1_bayer(nHeight, nWidth, CV_16UC1, t_image->imageData);

	// Decode the Bayer data to RGB but keep using 16 bits per channel
	cv::Mat mat16uc3_rgb(nWidth, nHeight, CV_16UC3);
	cv::cvtColor(mat16uc1_bayer, mat16uc3_rgb, cv::COLOR_BayerGR2RGB);

	// Convert the 16-bit per channel RGB image to 8-bit per channel
	cv::Mat mat8uc3_rgb(nWidth, nHeight, CV_8UC3);
	mat16uc3_rgb.convertTo(mat8uc3_rgb, CV_8UC3); //this could be perhaps done more

	//// Copy the data into an OpenCV Mat structure
	//cv::Mat bayer16BitMat(nHeight, nWidth, CV_16UC1, t_image->imageData);

	//// Convert the Bayer data from 16-bit to to 8-bit
	//cv::Mat bayer8BitMat = bayer16BitMat.clone();
	//// The 3rd parameter here scales the data by 1/16 so that it fits in 8 bits.
	//// Without it, convertTo() just seems to chop off the high order bits.
	//bayer8BitMat.convertTo(bayer8BitMat, CV_8UC1, 0.0625);

	//// Convert the Bayer data to 8-bit RGB
	//cv::Mat rgb8BitMat(nHeight, nWidth, CV_8UC3);
	//cv::cvtColor(bayer8BitMat, rgb8BitMat, CV_BayerGR2RGB);


	memcpy(m_RGBIn, mat8uc3_rgb.data, CAM_IMAGE_HEIGHT * CAM_IMAGE_WIDTH * 3);

// 	for(int y=0; y<CAM_IMAGE_HEIGHT; y++)
// 	{
// 		for(int x=0; x<CAM_IMAGE_WIDTH; x++)
// 		{
// 			m_RGBIn[y * CAM_IMAGE_WIDTH*3 + x*3 + 0] = t_image->imageData[y * t_image->widthStep + x*3 + 0];
// 			m_RGBIn[y * CAM_IMAGE_WIDTH*3 + x*3 + 1] = t_image->imageData[y * t_image->widthStep + x*3 + 1];
// 			m_RGBIn[y * CAM_IMAGE_WIDTH*3 + x*3 + 2] = t_image->imageData[y * t_image->widthStep + x*3 + 2];
// 		}
// 	}

	cvReleaseImage(&t_image);
#endif

	//  12bit Gray 원본 영상 데이터
	if (m_ImgScan_Gray == TRUE){
#if Def_IMGMode

		if (TRUE == strLoadIMGPath_16.IsEmpty())
			return;

	//	strLoadIMGPath_16 = "C:\\Users\\Seongho\\Desktop\\Image (2)\\18319D120360\\Capatue_Rotation_1118_002517_Pic.png";
	//	IplImage *t_image = cvLoadImage(strLoadIMGPath_16, -1);

		IplImage* t_image = cvLoadImage(strLoadIMGPath_16, CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
		memcpy(m_GRAYScanbuf, t_image->imageData, CAM_IMAGE_HEIGHT*CAM_IMAGE_WIDTH * 2);

		cvReleaseImage(&t_image);
#else
		ST_FrameData* pGray16 = m_LVDSVideo.GetSourceFrameData_ST(0);
		LPWORD	pGrayDATA = (LPWORD)m_LVDSVideo.GetSourceFrameData(0);
		IMG_COPY_GRAY(pGrayDATA, pGray16->dwSize);
// 		ST_VideoGRAY16* pGray16 = m_LVDSVideo.GetRecvVideoGray16();
// 		LPWORD	pGrayDATA = m_LVDSVideo.GetRecvGray16Data();
// 		IMG_COPY_GRAY(pGrayDATA, pGray16->m_dwSize);
#endif
		m_ImgScan_Gray = FALSE;
	}

	if (m_ImgScan == TRUE){	
#if Def_IMGMode 
		memcpy(m_RGBScanbuf, m_RGBIn, CAM_IMAGE_HEIGHT*CAM_IMAGE_WIDTH * 3);
#else
		IMG_COPY(lpbyRGB);
#endif
		m_ImgScan = FALSE;
	}

	pcMemDC->CreateCompatibleDC(pcDC);
	pcDIB->CreateCompatibleBitmap(pcDC, CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT);
	CBitmap	*pcOldDIB = pcMemDC->SelectObject(pcDIB);
	
	//---------------------------------------------
	::SetStretchBltMode(pcMemDC->m_hDC, COLORONCOLOR);//MemDC.m_hDC//pcDC->m_hDC

#if Def_IMGMode
	::StretchDIBits(pcMemDC->m_hDC,
		0, CAM_IMAGE_HEIGHT - 1,
		CAM_IMAGE_WIDTH, -CAM_IMAGE_HEIGHT,
		0, 0,
		m_biInfo.bmiHeader.biWidth,
		m_biInfo.bmiHeader.biHeight,
		m_RGBIn, &m_biInfo, DIB_RGB_COLORS, SRCCOPY);
#else
	::StretchDIBits(pcMemDC->m_hDC,
		0, CAM_IMAGE_HEIGHT - 1,
		CAM_IMAGE_WIDTH, -CAM_IMAGE_HEIGHT,
		0, 0,
		m_biInfo.bmiHeader.biWidth,
		m_biInfo.bmiHeader.biHeight,
		lpbyRGB, &m_biInfo, DIB_RGB_COLORS, SRCCOPY);
#endif
		//--------------Master Set 창 ------------------/
		if (m_pMasterMonitor != NULL){
			if ((m_pMasterMonitor->IsWindowVisible())){
				bView = TRUE;
				CDC	*pcMemDC2 = new CDC;
				CBitmap	*pcDIB2 = new CBitmap;
				pcMemDC2->CreateCompatibleDC(pcDC);
				pcDIB2->CreateCompatibleBitmap(pcDC, CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT);
				CBitmap	*pcOldDIB2 = pcMemDC2->SelectObject(pcDIB2);
				::SetStretchBltMode(pcMemDC2->m_hDC, COLORONCOLOR);//MemDC.m_hDC//pcDC->m_hDC

#if Def_IMGMode
				::StretchDIBits(pcMemDC2->m_hDC,
					0, CAM_IMAGE_HEIGHT - 1,
					CAM_IMAGE_WIDTH, -CAM_IMAGE_HEIGHT,
					0, 0,
					m_biInfo.bmiHeader.biWidth,
					m_biInfo.bmiHeader.biHeight,
					m_RGBIn, &m_biInfo, DIB_RGB_COLORS, SRCCOPY);
#else
				::StretchDIBits(pcMemDC2->m_hDC,
					0, CAM_IMAGE_HEIGHT - 1,
					CAM_IMAGE_WIDTH, -CAM_IMAGE_HEIGHT,
					0, 0,
					m_biInfo.bmiHeader.biWidth,
					m_biInfo.bmiHeader.biHeight,
					lpbyRGB, &m_biInfo, DIB_RGB_COLORS, SRCCOPY);
#endif
				m_pMasterMonitor->OnCamDisplay(pcMemDC2);
				delete pcDIB2;
				delete pcMemDC2;
			}
		}

	//--------------ParticleView 창 ------------------
		if (b_Particle == TRUE){
			if (m_ParticleView != NULL){
				CDC *pcDC3 = m_ParticleView->m_PaticleViewFrame.GetDC();
				//-----------가상메모리를 만든다.------------------

				::SetStretchBltMode(pcDC3->m_hDC, COLORONCOLOR);
				::StretchBlt(pcDC3->m_hDC,
					0, 0,
					CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT,
					pcMemDC->m_hDC,
					0, 0,
					CAM_IMAGE_WIDTH,
					CAM_IMAGE_HEIGHT, SRCCOPY);
				m_ParticleView->m_PaticleViewFrame.ReleaseDC(pcDC3);

			}
		}
	//-------------- Image 저장 -------------------------
	if (b_CaptureMode == TRUE)
	{
		b_CaptureMode = FALSE;
		SaveBitmapToDirectFile(pcMemDC, 24, imagepic_path_copy);
	}
	//-------------- PIC 그리기 ------------------
	if (bParticleView == FALSE){ //??
		if ((m_SubRunNum != -1) && (m_SubRunNum < 20)){
			if (m_psubmodel[m_SubRunNum] != NULL){
				m_psubmodel[m_SubRunNum]->Pic(pcMemDC);
			}
		}
		
	}
	if (b_Chk_Align == TRUE){
		Pic(pcMemDC);
	}
	//-------------- Modle Set 창 ------------------
	if (m_pModSetDlgWnd != NULL){
		if (m_pModSetDlgWnd->IsWindowVisible()){
			CDC *pcDC2;
			pcDC2 = m_pModSetDlgWnd->m_CamFrame.GetDC();
			::SetStretchBltMode(pcDC2->m_hDC, COLORONCOLOR);
			::StretchBlt(pcDC2->m_hDC,
				0, 0,
				CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT,
				pcMemDC->m_hDC,
				0, 0,
				CAM_IMAGE_WIDTH,
				CAM_IMAGE_HEIGHT, SRCCOPY);
			m_pModSetDlgWnd->m_CamFrame.ReleaseDC(pcDC2);
		}
	}

	//-------------- Main 창-------------------------
	if (bView)
	{
		::SetStretchBltMode(pcDC->m_hDC, COLORONCOLOR);
		::StretchBlt(pcDC->m_hDC,
			0, 0,
			CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT,
			pcMemDC->m_hDC,
			0, 0,
			CAM_IMAGE_WIDTH,
			CAM_IMAGE_HEIGHT, SRCCOPY);
	}

	m_CamFrame.ReleaseDC(pcDC);

	//-------------- Image 저장 -------------------------
	if (b_CaptureWindowMode == TRUE)
	{
		b_CaptureWindowMode = FALSE;

		SaveBitmapToDirectFile(pcMemDC, 24, winimage_path_copy);
	}

	if (b_ParticleCaptureMode == TRUE)
	{

		b_ParticleCaptureMode = FALSE;
		if (i_CapturePaticleMode != P_Image_NoSave)
		{
			if (b_Particle_Result == FALSE)
			{
				SaveBitmapToDirectFile(pcMemDC, 24, imagepic_Failcopy);
			}

			if (i_CapturePaticleMode == P_Image_ALLSave){
				if (b_Particle_Result == TRUE)
				{
					SaveBitmapToDirectFile(pcMemDC, 24, imagepic_Passcopy);
				}
			}
		}
	}

	delete pcDIB;
	delete pcMemDC;
}
void CImageTesterDlg::Pic(CDC *cdc)
{
	DWORD dwWidth = CAM_IMAGE_WIDTH;
	DWORD dwHeight = CAM_IMAGE_HEIGHT;

	CPen my_Pan, *old_pan;

	::SetBkMode(cdc->m_hDC, TRANSPARENT);


	/// Y축
	my_Pan.CreatePen(PS_SOLID, 2, RGB(0, 0, 255));
	old_pan = cdc->SelectObject(&my_Pan);

	cdc->MoveTo(dwWidth / 2, 0);
	cdc->LineTo(dwWidth / 2, dwHeight);

	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();

	/// X축 

	my_Pan.CreatePen(PS_SOLID, 2, RGB(0, 0, 255));
	old_pan = cdc->SelectObject(&my_Pan);

	cdc->MoveTo(0, dwHeight / 2);
	cdc->LineTo(dwWidth, dwHeight / 2);

	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();

	/// X축 LEFT
	my_Pan.CreatePen(PS_SOLID, 2, RGB(0, 0, 255));
	old_pan = cdc->SelectObject(&my_Pan);

	cdc->MoveTo(dwWidth / 2 - m_Align, 0);
	cdc->LineTo(dwWidth / 2 - m_Align, dwHeight);

	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();

	/// X축 RIGHT
	my_Pan.CreatePen(PS_SOLID, 2, RGB(0, 0, 255));
	old_pan = cdc->SelectObject(&my_Pan);

	cdc->MoveTo(dwWidth / 2 + m_Align, 0);
	cdc->LineTo(dwWidth / 2 + m_Align, dwHeight);

	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();
// 	CPen	my_Pan,*old_pan;
// 	CPen	my_Pan2, *old_pan2;
// 
// 	::SetBkMode(cdc->m_hDC, TRANSPARENT);
// 
// 	my_Pan.CreatePen(PS_SOLID, 2, RED_COLOR);
// 	old_pan = cdc->SelectObject(&my_Pan);
// 	cdc->SetTextColor(RED_COLOR);
// 
// 
// 	cdc->MoveTo(CAM_IMAGE_WIDTH/2, 0);
// 	cdc->LineTo(CAM_IMAGE_WIDTH / 2, CAM_IMAGE_HEIGHT);
// 
// 	cdc->MoveTo(0, CAM_IMAGE_HEIGHT/2);
// 	cdc->LineTo(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT/2);
// 
// 	my_Pan2.CreatePen(PS_SOLID,1, WHITE_COLOR);
// 	old_pan2 = cdc->SelectObject(&my_Pan2);
// 	for (int a = 1; a < 40; a++){
// 		cdc->MoveTo(16 * a , 0);
// 		cdc->LineTo(16 * a, CAM_IMAGE_HEIGHT);
// 	}
// 	for (int b = 1; b < 30; b++){
// 		cdc->MoveTo(0, 16 * b );
// 		cdc->LineTo(CAM_IMAGE_WIDTH, 16 * b);
// 	}
// 	cdc->SelectObject(old_pan2);
// 	my_Pan2.DeleteObject();
// 	cdc->SelectObject(old_pan);
// 	my_Pan.DeleteObject();
}
void	CImageTesterDlg::IMG_COPY(BYTE *RGBScanBuf)
{
	memcpy(m_RGBScanbuf, RGBScanBuf, CAM_IMAGE_WIDTH*CAM_IMAGE_HEIGHT*3);
//	IplImage *t_image = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);
// 	for (int y = 0; y < CAM_IMAGE_HEIGHT; y++)
// 	{
// 		for (int x = 0; x < CAM_IMAGE_WIDTH; x++)
// 		{
// 			t_image->imageData[y * t_image->widthStep * 3 + x * 3 + 0] = m_RGBScanbuf[y * CAM_IMAGE_WIDTH + x * 3 + 0];
// 			t_image->imageData[y * t_image->widthStep * 3 + x * 3 + 1] = m_RGBScanbuf[y * CAM_IMAGE_WIDTH + x * 3 + 1];
// 			t_image->imageData[y * t_image->widthStep * 3 + x * 3 + 2] = m_RGBScanbuf[y * CAM_IMAGE_WIDTH + x * 3 + 2];
// 		}
// 	}
// 	memcpy(t_image->imageData, m_RGBScanbuf, CAM_IMAGE_WIDTH*CAM_IMAGE_HEIGHT * 3);
// 	cvSaveImage("D:\\RGBIMAGE.bmp", t_image);
// 	cvReleaseImage(&t_image);
}
void	CImageTesterDlg::IMG_COPY_GRAY(WORD *lpbyRGB, __in DWORD dwRGBSize)
{
	memcpy(m_GRAYScanbuf, lpbyRGB, dwRGBSize);
}
//=============================================================================
// Method		: OnInitDialog
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/6/13 - 11:21
// Desc.		:
//=============================================================================
BOOL CImageTesterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.



	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	LogWriteToFile(_T("프로그램 시작"));

	m_ScanState = FALSE;
	FrameWnd = m_CamFrame.GetSafeHwnd();

	if(b_luritechDebugMODE == 1){
		InitDlgProgram();
		
		SetTimer(TM_MES_RETRYCONNECT, 5000, NULL);

	}else{
		if(!InitMotor())
			CDialog::OnCancel();
		else
		{
			InitDlgProgram();
			
			SetTimer(TM_MES_RETRYCONNECT, 5000, NULL);
		}
	}

#if Def_IMGMode
	GetDlgItem(IDC_BUTTON2)->SetWindowText(_T("Load PNG Image"));
#endif

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CImageTesterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CImageTesterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CImageTesterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

#pragma region 초기화관련

void CImageTesterDlg::InitDlgProgram(void){
	
	CString pArgs;
           
    CWinApp *App = AfxGetApp();
	pArgs = App->m_lpCmdLine;      

	b_PROTECT = ((CImageTesterApp *)AfxGetApp())->b_PROTECT;
	if((strstr(pArgs,"-D") != 0)||(strstr(pArgs,"-d") != 0)){
		EVMS_Mode = 0;//1이면 작업자모드 0이면 전문가 모드
	}else if((strstr(pArgs,"-F") != 0)||(strstr(pArgs,"-F") != 0)){
		EVMS_Mode = 2;
	}else{
		EVMS_Mode = Def_Evmsmode;//1이면 작업자모드 0이면 전문가 모드
	}	

	TCHAR			lpszWndName[128];
/*	if(QL_IG == 0){
		wsprintf(lpszWndName, "CAN TEST QL전용");
	}else{
		wsprintf(lpszWndName, "CAN TEST");
	}*/	
	sprintf(lpszWndName, "IMAGE TEST");
	SetWindowText(lpszWndName);

	// 2016.09.02 수정 : 심재원------------------------------------------------
	CString strTitle;
	//GetWindowText(strTitle);
	strTitle = lpszWndName;

	CString strVersion = GetVersionInfo(_T("ProductVersion"));    
	CString strFileVer = GetVersionInfo(_T("FileVersion"));

	CString strCaption = _T("");
	strCaption.Format (_T("%s ( Ver %s : %s)"), strTitle, strVersion, strFileVer);
	SetWindowText (strCaption);	
	InitDevicez(GetSafeHwnd());

	// end 2016.09.02 ---------------------------------------------------------

	
	m_pBKStateDlg		= GetDlgItem(IDC_MAINTAP);
	m_pBKDialog			= GetDlgItem(IDC_TAB_INDEX);
	m_pModResTabWnd		= (CTabCtrl*)GetDlgItem(IDC_TAB_RESULT);
	m_pModResTabWnd2	= (CTabCtrl*)GetDlgItem(IDC_TAB_RESULT2);
	m_pMainCombo		= (CComboBox *)GetDlgItem(IDC_MODEL_NAME);

	m_CameraPort.Msgbuf = WM_COMM_READ3;
	m_ControlPort_Center.Msgbuf = WM_COMM_READ_CONCenter;
	m_ControlPort_Side.Msgbuf = WM_COMM_READ_CONSide;

	hMainWnd = this->m_hWnd;
	m_CameraPort.hCommWnd = this->m_hWnd;
	m_ControlPort_Center.hCommWnd = this->m_hWnd;
	m_ControlPort_Side.hCommWnd = this->m_hWnd;


	TESTNAME[0] = "MODEL START";
	TESTNAME[1] = "동작전류";
	TESTNAME[2] = "EEPROM";
	TESTNAME[3] = "광축검사";
	TESTNAME[4] = "Rotate 검사";
	TESTNAME[5] = "SFR";
	TESTNAME[6] = "화각검사";
	TESTNAME[7] = "Distortion";
	TESTNAME[8] = "3DDepth";
	TESTNAME[9] = "이물검사";
	TESTNAME[10] = "광량비 검사";
	TESTNAME[11] = "이물광원 SNR";
	TESTNAME[12] = "Dynamic Range";
	TESTNAME[13] = "Fixed Pattern";
	TESTNAME[14] = "MODEL END";
	TESTNAME[15] = "RESULT";
	TESTNAME[16] = "비고";

// 	TESTNAME[0] = "MODEL START";
// 	TESTNAME[1] = "동작전류";
// 	TESTNAME[2] = "SW광축보정";
// 	TESTNAME[3] = "Color Reproduction";
// 	TESTNAME[4] = "De-Focus";
// 	TESTNAME[5] = "화각검사";
// 	TESTNAME[6] = "Overlay Position";
// 	TESTNAME[7] = "Rotate 검사";
// 	TESTNAME[8] = "이물검사";
// 	TESTNAME[9] = "조향연동";
// 	TESTNAME[10] = "경고문구";
// 	TESTNAME[11] = "Pixel 검사";
// 	TESTNAME[12] = "저조도검사";
// 	TESTNAME[13] = "Peak 검사";
// 	TESTNAME[14] = "IR Filter 검사";
// 	TESTNAME[15] = "버전 검사";
// 	TESTNAME[16] = "Mfgdata W/R";
// 	TESTNAME[17] = "MODEL END";

	Set_List(&MainWork_List);
	Set_List_C(&m_CurrList);
	


	Set_List_Eeprom(&m_Work_EepromList);
	//-통신 관련
	//
	//------ini파일에서 데이터를 읽어들인다.-------
	m_exepath = ExePathSearch();//현재 실행파일이 있는 경로를 저장한다.
	folder_gen(m_model_path);
//	INITMODELFILE = m_model_path + ("\\initmodel.ini");
	INITMODELFILE = m_exepath + ("\\initmodel.ini");
	CAMSETFILE = m_exepath + ("\\Camset.ini");

	if(EVMS_Mode == 0){//관리자 모드
			envpath = m_exepath /*+ "\\ENV"*/;
			folder_gen(envpath);
			m_inifilename = envpath +("\\ruri_fts.ini");//초기화 파일이 저장되는 
			Pogo_path = envpath +("\\FTS_POGO");//초기화 파일이 저장되는
			m_model_path = envpath +_T("\\MODEL");//초기화 파일이 저장되는 
 	}else{
 		CString BackString,bufstring;
 		BOOL pathstat = FALSE;
 		BackString = m_exepath;
 		for(int lop = 0;lop<100;lop++){
 			BackString = BackPathSearch(BackString);
 			bufstring = FinalPathSearch(BackString);
 			if((bufstring == "op")||(bufstring == "tp")||(bufstring == "OP")||(bufstring == "TP")||(bufstring == "LG_EVMS")){
 				pathstat = TRUE;	
 				break;
 			}
 		}
 		if(pathstat == TRUE){
 			envpath = BackString + "\\ENV";
 			folder_gen(envpath);
 			m_inifilename = envpath +("\\ruri_fts.ini");
 			Pogo_path = envpath +("\\FTS_POGO");//초기화 파일이 저장되는
 		}else{
 			envpath = m_exepath + "\\ENV";
 			folder_gen(envpath);
 			m_inifilename = envpath +("\\ruri_fts.ini");
 			Pogo_path = envpath +("\\FTS_POGO");//초기화 파일이 저장되는
 		}
 		m_model_path = envpath +_T("\\MODEL");//초기화 파일이 저장되는 
 	}
	EVMS_Mode = 0;
	folder_gen(Pogo_path);

	Create_CommDlg();

	////------ini파일에서 데이터를 읽어들인다.-------
	//m_exepath = ExePathSearch();//현재 실행파일이 있는 경로를 저장한다.
	//m_inifilename = m_exepath +_T("\\init.ini");//초기화 파일이 저장되는 
	//m_model_path = m_exepath +_T("\\MODEL");//초기화 파일이 저장되는 
	//Micom_Path = m_exepath +("\\MICOM.ini");//초기화 파일이 저장되는 
	//
	//Pogo_path = m_exepath +("\\POGO");//초기화 파일이 저장되는 

	//folder_gen(m_model_path);
	//folder_gen(Pogo_path);

	//m_passwordname = m_exepath +_T("\\SECURITY.ini");//초기화 파일이 저장되는 

	LoadSerialSet(&m_CamSerial,_T("CAMSERIAL"),m_inifilename);
	LoadSerialSet(&m_ConSerial_Center, _T("CENTER_CONSERIAL"), m_inifilename);
	LoadSerialSet(&m_ConSerial_Side, _T("SIDE_CONSERIAL"), m_inifilename);
	LoadParameterSet();//패스워드 및 기본 경로 등을 불러들인다. 

	m_Motion.LoadParameter();

	LoadMotorParameter();
	
	//---------------------------------------------------------VIN
	CTime time = CTime::GetTickCount();  
	YEAR = time.GetYear();   
	MONTH = time.GetMonth();   
	DAY = time.GetDay(); 
	strDay.Format("%d_%02d_%02d", YEAR, MONTH, DAY);

	CREATE_Folder(strDay, 1);
	CREATE_Folder(strDay, 4);
	LOT_CREATE_Folder(strDay, 1);

	//----------------------------------------------------------
	SetTimer(110, 100, NULL); //InitProgram을 불러들이는데 쓰인다. 
	InitModelSetup();//각 서브 다이알로그를 생성한다. 

	//MOVE_oldmodelname ="";


// 	ComArtWnd = new CComArt;
// 	ComArtWnd->Camstat[15].Enable = 1;
// 	ComArtWnd->Camstat[15].Resolution = VCAPT_TRON_720X480;
// 	bool initcam = ComArtWnd ->Setup(this,m_pTStat);
// 	ComArtWnd ->m_nTimerID = SetTimer(111, 67, NULL); //영상입력 타이머 파라메터 
// 
// 	
// 	if(ComArtWnd != NULL){
// 		ComArtWnd ->ViewChange(BYTE(110),BYTE(110));
// 	}
	

	m_Motion.SetIOOutPort(DIO_OUTPORT_BRDPOWER, TRUE);

	FontSetup();//폰트 셋팅을 한다. 
//	Set_List(&MainWork_List);
//	Set_List_C(&m_CurrList);
	SetTimer(120,50,NULL);
	SetTimer(111, 100, NULL);
	
	SetTimer(TM_CAM_FRAME, 50, NULL);// 영상
//	SetTimer(TM_MACHINE_CHECK, 100, NULL);
	//소켓 초기화 시작
	m_SocketManager.GetLocalAddress( m_szIp.GetBuffer(256), 256);
	m_szIp.ReleaseBuffer();
	// Initialize socket manager
	m_SocketManager.SetMessageWindow(&m_pMesRecv,this);
	m_SocketManager.SetServerState( false );	// run as client
	m_SocketManager.SetSmartAddressing( false );	// always send to server

	if(b_Chk_MESEn == TRUE){
		SocketConnect();
	}
	m_nSockRetryConnect = 0;
	//소켓 초기화 끝
	InitEVMS();

	SETTING_USER_Switching();
}

void CImageTesterDlg::LoadMotorParameter()
{
	CString str;
	CString strPath = _T("");
	strPath.Format(_T("%s"), FILE_AXIS);
	
	m_nRoffset = GetPrivateProfileInt(_T("MOTOR_R_OFFSET"),_T("OFFSET"),-1,strPath);
	if(m_nRoffset == -1)
	{
		m_nRoffset = 545;
		str.Format(_T("%d"),m_nRoffset);
		WritePrivateProfileString(_T("MOTOR_R_OFFSET"),_T("OFFSET"),str,strPath);
	}
}

void CImageTesterDlg::SaveMotorParameter()
{
	CString str;
	CString strPath = _T("");
	strPath.Format(_T("%s"), FILE_AXIS);
	str.Format(_T("%d"),m_nRoffset);
	WritePrivateProfileString(_T("MOTOR_R_OFFSET"),_T("OFFSET"),str,strPath);
}

void CImageTesterDlg::LoadParameterSet(void)
{
	CFileFind filefind;	
	CString str = _T("");
	CString fnstr = _T("");
	char cload[100] ={0,};
	int frnum;//폴더이름갯수

	int save=0;

	SAVE_IMAGEPATH = GetPrivateProfileCString(_T("IMAGE_PATH"), _T("SAVE_FOL"), m_inifilename);
	if (filefind.FindFile(SAVE_IMAGEPATH) == FALSE)
	{
		TCHAR szDesktopPath[MAX_PATH] = { 0, };

		SAVE_IMAGEPATH = _T("D:\\IMAGE");

		WritePrivateProfileString(_T("IMAGE_PATH"), _T("SAVE_FOL"), SAVE_IMAGEPATH, m_inifilename);
		// 소켓통신관련
	}
	else
	{
		folder_gen(SAVE_IMAGEPATH);	// 폴더 생성
	}


	SAVEFILE_RPATH = GetPrivateProfileCString(_T("SAVE_FILE_PATH"),_T("SAVE_FOL"),m_inifilename);
 	if(filefind.FindFile(SAVEFILE_RPATH)==FALSE)
 	{
 		TCHAR szDesktopPath[MAX_PATH] = {0,};
 
 		SHGetSpecialFolderPath(NULL, szDesktopPath, CSIDL_DESKTOP, FALSE);
 		str.Format(_T("%s"), szDesktopPath);
 		SAVEFILE_RPATH= str + _T("\\READ_DATA_SAVE");
 
 		WritePrivateProfileString(_T("SAVE_FILE_PATH"),_T("SAVE_FOL"),SAVEFILE_RPATH,m_inifilename);
 
 		CreateDirectory(SAVEFILE_RPATH, NULL);	// 폴더 생성
 
 		// 소켓통신관련
 
 	}else
 	{
 		LOT_CREATE_Folder(SAVEFILE_RPATH,0);	
 	}
	
	SAVE_FOLDERPATH = GetPrivateProfileCString(_T("SAVE_FOLDER_PATH"), _T("SAVE_FOL"), m_inifilename);

	if (filefind.FindFile(SAVE_FOLDERPATH) == FALSE)
	{
		TCHAR szDesktopPath[MAX_PATH] = { 0, };

		SHGetSpecialFolderPath(NULL, szDesktopPath, CSIDL_DESKTOP, FALSE);
		str.Format(_T("%s"), szDesktopPath);
		SAVE_FOLDERPATH = str + _T("\\DATA_SAVE");

		WritePrivateProfileString(_T("SAVE_FOLDER_PATH"), _T("SAVE_FOL"), SAVE_FOLDERPATH, m_inifilename);

		CREATE_Folder(SAVE_FOLDERPATH, 0);
		// 소켓통신관련

	}
	else
	{
		CREATE_Folder(SAVE_FOLDERPATH, 0);
	}

	SAVE_FOLDERPATH_P = GetPrivateProfileCString(_T("SAVE_FOLDER_PATH_P"),_T("SAVE_FOL"),m_inifilename);

 	if(filefind.FindFile(SAVE_FOLDERPATH_P)==FALSE)
 	{
 		TCHAR szDesktopPath[MAX_PATH] = {0,};
 
 		SHGetSpecialFolderPath(NULL, szDesktopPath, CSIDL_DESKTOP, FALSE);
 		str.Format(_T("%s"), szDesktopPath);
 		SAVE_FOLDERPATH_P= str + _T("\\DATA_SAVE");
 
 		WritePrivateProfileString(_T("SAVE_FOLDER_PATH_P"),_T("SAVE_FOL"),SAVE_FOLDERPATH_P,m_inifilename);
 
 		CREATE_Folder(SAVE_FOLDERPATH_P,3);
 		// 소켓통신관련
 
 	}else
 	{
 		CREATE_Folder(SAVE_FOLDERPATH_P,3);	
 	}
	//--------------LOT 저장 경로---------------
	LOT_SAVE_FOLDERPATH = GetPrivateProfileCString(_T("LOT_SAVE_FOLDER_PATH"),_T("LOT_SAVE_FOL"),m_inifilename);
	if(filefind.FindFile(LOT_SAVE_FOLDERPATH)==FALSE)
	{
		TCHAR szDesktopPath[MAX_PATH] = {0,};

		SHGetSpecialFolderPath(NULL, szDesktopPath, CSIDL_DESKTOP, FALSE);
		str.Format(_T("%s"), szDesktopPath);
		LOT_SAVE_FOLDERPATH= str + _T("\\REPORT");

		WritePrivateProfileString(_T("LOT_SAVE_FOLDER_PATH"),_T("LOT_SAVE_FOL"),LOT_SAVE_FOLDERPATH,m_inifilename);


		LOT_CREATE_Folder(LOT_SAVE_FOLDERPATH,0);
		// 소켓통신관련
		
	}else
	{
		LOT_CREATE_Folder(LOT_SAVE_FOLDERPATH,0);	
	}

	//-----------------------------------------------------------------------------------------------------


	i_MST_H = GetPrivateProfileInt(_T("MSTime"),_T("MSTime_H"),-1,m_inifilename);
	if(i_MST_H == -1)
	{
		i_MST_H = 0;
		str.Format(_T("%d"),i_MST_H);
		WritePrivateProfileString(_T("MSTime"),_T("MSTime_H"),str,m_inifilename);
	}

	i_MST_M = GetPrivateProfileInt(_T("MSTime"),_T("MSTime_M"),-1,m_inifilename);
	if(i_MST_M == -1)
	{
		i_MST_M = 0;
		str.Format(_T("%d"),i_MST_M);
		WritePrivateProfileString(_T("MSTime"),_T("MSTime_M"),str,m_inifilename);
	}

	// 소켓통신관련
	int save2=0;
	char saveFolder2[2046];
	save2 = GetPrivateProfileString(_T("MES_BMS_SAVE_FOLDER_PATH"),_T("MES_BMS_SAVE_FOL"),_T(""),saveFolder2,2046,m_inifilename);
	for(int k=0; k<save2; k++)
		MESPath+=saveFolder2[k];

	if(MESPath == _T(""))
	{
		MESPath= _T("C:\\BMS_MES");
		folder_gen(MESPath);
		WritePrivateProfileString(_T("MES_BMS_SAVE_FOLDER_PATH"),NULL,_T(""),m_inifilename);	
		WritePrivateProfileString(_T("MES_BMS_SAVE_FOLDER_PATH"),_T("MES_BMS_SAVE_FOL"),MESPath,m_inifilename);
	}else{
		folder_gen(MESPath);
	}

	b_Chk_MESEn = GetPrivateProfileInt(_T("MESOPTION"),_T("MESEN"),-1,m_inifilename);
	if(b_Chk_MESEn == -1)
	{
		b_Chk_MESEn = 0;//초기에 오토 포트 검색은 활성화한다.. 
		str.Empty();
		str.Format(_T("%d"),b_Chk_MESEn);
		WritePrivateProfileString(_T("MESOPTION"),_T("MESEN"),str,m_inifilename);
	}

	frnum = GetPrivateProfileString(_T("MESOPTION"),_T("IP_Addr"),_T(""),cload,100,m_inifilename);
	if(frnum == 0)
	{
		m_szIp.Format(_T("192.168.0.1"));
		WritePrivateProfileString(_T("MESOPTION"),_T("IP_Addr"),m_szIp,m_inifilename);
	}else
	{
		m_szIp.Empty();
		for(int i=0; i<frnum; i++)
			m_szIp += cload[i];
	}

	frnum = GetPrivateProfileInt(_T("MESOPTION"),_T("IP_Port"),-1,m_inifilename);
	if(frnum == -1)
	{
		m_socket = 0;
		m_szPort=_T("");
		m_szPort.Format(_T("%d"),m_socket);
		WritePrivateProfileString(_T("MESOPTION"),_T("IP_Port"),m_szPort,m_inifilename);
	}else
	{
		m_socket = (unsigned int)frnum;
		m_szPort=_T("");
		m_szPort.Format(_T("%d"),m_socket);
	}

	frnum = GetPrivateProfileString(_T("MESOPTION"),_T("MCODE"),_T(""),cload,100,m_inifilename);
	if(frnum == 0)
	{
		m_szCode = _T("MCODE");
		WritePrivateProfileString(_T("MESOPTION"),_T("MCODE"),m_szCode,m_inifilename);
	}else
	{
		m_szCode.Empty();
		for(int i=0; i<frnum; i++)
			m_szCode += cload[i];
	}

	frnum = GetPrivateProfileString(_T("PassWord"),_T("PW"),_T(""),cload,2046,m_inifilename);
	if(frnum == 0)
	{
		MatchPassWord = _T("");
		WritePrivateProfileString(_T("PassWord"),_T("PW"),MatchPassWord,m_inifilename);
	}else
	{
		MatchPassWord.Empty();
		for(int i=0; i<frnum; i++){
			MatchPassWord += cload[i];
		}
	}


// 	CString szUseSensor;
// 	frnum = GetPrivateProfileInt("SAFETY_DOOR", "USE", -1, m_inifilename);
// 
// 	if (frnum == -1)
// 	{
// 		m_bUseSensor = 0;
// 		szUseSensor.Format("%d", m_bUseSensor);
// 		WritePrivateProfileString("SAFETY_DOOR", "USE", szUseSensor, m_inifilename);
// 	}
// 	else
// 	{
// 		m_bUseSensor = (BOOL)frnum;
// 	}


	UpdateData(FALSE);//광축값등을 외부에 표현한다.
	UpdateData(TRUE);
}
void CImageTesterDlg::InitProgram(void){
	
	KillTimer(110);

	for(int lop = 0;lop < 20;lop++){
		m_ModelInfo[lop].ID = 0;
		m_ModelInfo[lop].NAME = "";
		m_ModelInfo[lop].MODE = "";
	}

	int nBoard;
	m_LVDSVideo.Open_Device(nBoard);
	if (nBoard > 0)
	{
		StatePrintf("Device Open Success!");
		DoEvents(100);
		m_LVDSVideo.LVDS_Init(0);
		DoEvents(100);

		m_LVDSVideo.Capture_Start(0);
		DoEvents(100);
	}
	else
	{
		StatePrintf("Device Open Fail!");
	}

	AutoPortChk_Cam();
	DoEvents(100);
	AutoPortChk_ConCenter();
	DoEvents(100);
	AutoPortChk_ConSide();
	DoEvents(100);

	if (!PortTst_Cam()){
		PORT_STAT_CAM(0, "DISCONNECT");
	}
	else{
		PORT_STAT_CAM(1, "CONNECTING");
	}

	if (Power_Total_Control() == true){
		StatePrintf("Back Light On Success!");
	}
	else{
		StatePrintf("Back Light On Fail!");
	}
	Power_Off();
	
	ALARM_ONOFF(4);
}


void CImageTesterDlg::FontSetup(void){
	Font_Size_Change(IDC_EDIT_ProTime,&ft1,40,23);
	GetDlgItem(IDC_EDIT_ProTime2)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_TIME)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_TIME2)->SetFont(&ft1);
	

//	Font_Size_Change(IDC_MPORTSTAT,&ft5,30,18);

	Font_Size_Change(IDC_BTN_START,&ft2,50,35);
	GetDlgItem(IDC_BTN_STOP)->SetFont(&ft2);


	Font_Size_Change(IDC_BTN_USER_CHANGE,&ft3,100,18);
	GetDlgItem(IDC_BTN_WORK_BUTTON)->SetFont(&ft3);
	GetDlgItem(IDC_BTN_TEST_SETTING)->SetFont(&ft3);
	GetDlgItem(IDC_BTN_ETC_SETTING)->SetFont(&ft3);
	GetDlgItem(IDC_BTN_MAIN)->SetFont(&ft3);
	GetDlgItem(IDC_BTN_MASTER_SET)->SetFont(&ft3);
		GetDlgItem(IDC_EDIT_POGONAME)->SetFont(&ft3);
	GetDlgItem(IDC_EDIT_COUNT_TEST)->SetFont(&ft3);

	Font_Size_Change(IDC_EDIT_PORTNAME,&ft4,45,30);
	GetDlgItem(IDC_EDIT_PORTSTATE)->SetFont(&ft4);
	GetDlgItem(IDC_EDIT_CAMNAME)->SetFont(&ft4);
	GetDlgItem(IDC_EDIT_CAMSTATE)->SetFont(&ft4);

	Font_Size_Change(IDC_TEST_STATUS,&ft5,90,60);
	

	Time_TEXT("Current Time");
	Time_NUM("00:00:00");
	ProTime_TEXT("Test Time");
	ProTime_NUM("00:00:00");

	PORT_NAME("CAM PORT");
	CAM_NAME("CAMERA");  

	TEST_STAT(0,"STAND BY");
//	Font_Size_Change(IDC_STATIC_ITEM,&ft4,700,18);

}

void CImageTesterDlg::Focus_move_start()
{
	if(((CButton *)GetDlgItem(IDC_BTN_START))->IsWindowEnabled() == TRUE){
		GotoDlgCtrl(((CButton *)GetDlgItem(IDC_BTN_START)));
	}else{
		GotoDlgCtrl(((CStatic *)GetDlgItem(IDC_STATIC)));
	}
}

void CImageTesterDlg::InitModelSetup(void){
	m_SubRunNum = -1;

	for(int lop = 0;lop < 20;lop++){
		m_ModelInfo[lop].ID = 0;
		m_ModelInfo[lop].NAME = "";
		m_ModelInfo[lop].MODE = "";
	}

	CStand_Create();
	CModel_Worker_Create();
	WorkListCreate();
	//CParticleViewLG_Create();

	CLGE_CamSet_Create();
	CLGE_CamSet_Parti_Create();
	CModOpt_Create();
	CModSet_Create();
	CModSel_Create();
	CStandOpt_Create();
	CControl_Create();
	SETTING_USER_Switching(); 

	MainModelSel();//경로상에 폴더가 있는지 확인한다.
	ModelSet(); //luri 파일을 읽어 각 모델이 무엇인지 파악한다.
	ModelGen();
}
void CImageTesterDlg::EndModelSetup(void)
{
	CLGE_CamSet_Delete();
	CLGE_CamSet_Parti_Delete();
	CModOpt_Delete();
	CModSet_Delete();
	CControl_Delete();
	CModSel_Delete();
	CStandOpt_Delete();
	WorkListDelete();
	CModel_Worker_Delete();
	//CParticleViewLG_Delete();
	CStand_Delete(); 
}

void CImageTesterDlg::CStand_Create(){
	CRect OptionRect;
	m_pStandWnd = new CStandDlg(this);
	m_pStandWnd->Setup(this);
	m_pStandWnd->Create(CStandDlg::IDD,m_pBKStateDlg);
	m_pStandWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
	m_pStandWnd->MoveWindow(0,0,OptionRect.Width(),OptionRect.Height());
	m_pTStat = m_pStandWnd->m_pStandEdit;	
	m_pStandWnd->ShowWindow(SW_SHOW);
}

void CImageTesterDlg::CStand_Delete(){
	if(m_pStandWnd != NULL){
		m_pStandWnd->DestroyWindow();
		delete m_pStandWnd;
		m_pStandWnd = NULL;	
	}

//	delete m_pChart;
}

void CImageTesterDlg::CModel_Worker_Create(){  
	CRect OptionRect;
	if(m_pModResTabWnd2 != NULL){

		//////////////--MES 모드
		m_pResWorkerOptWnd = new COption_ResultWStand(this);
		m_pResWorkerOptWnd->Setup(this);
		m_pResWorkerOptWnd->Create(COption_ResultWStand::IDD,m_pModResTabWnd2);
		m_pModResTabWnd2->GetWindowRect(OptionRect);
		m_pResWorkerOptWnd->MoveWindow(0,0,OptionRect.Width(),OptionRect.Height());
		m_pResWorkerOptWnd->ShowWindow(SW_SHOW);

		//////////////--LOT 모드
		m_pResLotWnd = new CLot_StandDlg(this);
		m_pResLotWnd->Setup(this);
		m_pResLotWnd->Create(CLot_StandDlg::IDD,m_pModResTabWnd2);
		m_pModResTabWnd2->GetWindowRect(OptionRect);
		m_pResLotWnd->MoveWindow(0,0,OptionRect.Width(),OptionRect.Height());
		m_pResLotWnd->ShowWindow(SW_HIDE);
	}
}

void CImageTesterDlg::CModel_Worker_Delete(){
	//////////////--MES 모드
	if(m_pResWorkerOptWnd != NULL){
		m_pResWorkerOptWnd->DestroyWindow();
		delete m_pResWorkerOptWnd;
		m_pResWorkerOptWnd = NULL;	
	}

	//////////////--LOT 모드
	if(m_pResLotWnd != NULL){
		m_pResLotWnd->DestroyWindow();
		delete m_pResLotWnd;
		m_pResLotWnd = NULL;	
	}
}
void CImageTesterDlg::CLGE_CamSet_Create(){

	m_pLGE_CamSetWnd = new CLGE_CamSetDlg(this);
	m_pLGE_CamSetWnd->Setup(this);
	m_pLGE_CamSetWnd->Create(CLGE_CamSetDlg::IDD, CWnd::GetDesktopWindow());
	m_pLGE_CamSetWnd->ShowWindow(SW_HIDE);
}
void CImageTesterDlg::CLGE_CamSet_Delete(){
	if (m_pLGE_CamSetWnd != NULL){
		m_pLGE_CamSetWnd->DestroyWindow();
		delete m_pLGE_CamSetWnd;
		m_pLGE_CamSetWnd = NULL;
	}
}

void CImageTesterDlg::CLGE_CamSet_ShowWindow(){
	if (m_pLGE_CamSetWnd != NULL){
		m_pLGE_CamSetWnd->ShowWindow(FALSE);
		m_pLGE_CamSetWnd->ShowWindow(TRUE);
	}
}
void CImageTesterDlg::CControl_Create(){

	m_pControlWnd = new CControlDlg(this);
	m_pControlWnd->Setup(this, m_pTStat);
	m_pControlWnd->Create(CControlDlg::IDD, CWnd::GetDesktopWindow());
	m_pControlWnd->ShowWindow(SW_HIDE);
}
void CImageTesterDlg::CControl_Delete(){
	if (m_pControlWnd != NULL){
		m_pControlWnd->DestroyWindow();
		delete m_pControlWnd;
		m_pControlWnd = NULL;
	}
}

void CImageTesterDlg::CControl_ShowWindow(){
	if (m_pControlWnd != NULL){
		m_pControlWnd->ShowWindow(FALSE);
		m_pControlWnd->ShowWindow(TRUE);
	}
}
void CImageTesterDlg::CLGE_CamSet_Parti_Create(){

	m_pLGE_CamSet_PartiWnd = new CLGE_CamSet_ParticleDlg(this);
	m_pLGE_CamSet_PartiWnd->Setup(this);
	m_pLGE_CamSet_PartiWnd->Create(CLGE_CamSet_ParticleDlg::IDD, CWnd::GetDesktopWindow());
	m_pLGE_CamSet_PartiWnd->ShowWindow(SW_HIDE);
}
void CImageTesterDlg::CLGE_CamSet_Parti_Delete(){
	if (m_pLGE_CamSet_PartiWnd != NULL){
		m_pLGE_CamSet_PartiWnd->DestroyWindow();
		delete m_pLGE_CamSet_PartiWnd;
		m_pLGE_CamSet_PartiWnd = NULL;
	}
}

void CImageTesterDlg::CLGE_CamSet_Parti_ShowWindow(){
	if (m_pLGE_CamSet_PartiWnd != NULL){
		m_pLGE_CamSet_PartiWnd->ShowWindow(FALSE);
		m_pLGE_CamSet_PartiWnd->ShowWindow(TRUE);
	}
}
void CImageTesterDlg::WorkListCreate(){
	CRect OptionRect;
	m_pWorkListWnd = new CWorkList(this);
	m_pWorkListWnd->Setup(this);
	m_pWorkListWnd->Create(CWorkList::IDD,CWnd::GetDesktopWindow());

	m_pWorkListWnd->ShowWindow(SW_HIDE);
	m_pWorkList = &(m_pWorkListWnd->m_WorkList);
		
}
void CImageTesterDlg::WorkListDelete(){
	if(m_pWorkListWnd != NULL){
		m_pWorkListWnd->DestroyWindow();
		delete m_pWorkListWnd;
		m_pWorkListWnd = NULL;	
		m_pWorkList = NULL;
	}
}
void CImageTesterDlg::CModOpt_Create()
{
	m_pModOptDlgWnd = new CModOptDlg(this);
	m_pModOptDlgWnd->Setup(this);
	m_pModOptDlgWnd->Create(CModOptDlg::IDD,CWnd::GetDesktopWindow());
	m_pModOptDlgWnd->GetWindowRect(InitOptionRect);
	m_pModOptDlgWnd->ShowWindow(SW_HIDE);
}

void CImageTesterDlg::CModOpt_Delete()
{
	if(m_pModOptDlgWnd != NULL)
	{
		m_pModOptDlgWnd->DestroyWindow();
		delete m_pModOptDlgWnd;
		m_pModOptDlgWnd = NULL;
	}
}

void CImageTesterDlg::CModSet_Create()
{
	m_pModSetDlgWnd = new CModSetDlg(this);
	m_pModSetDlgWnd->Setup(this);
	m_pModSetDlgWnd->Create(CModSetDlg::IDD,CWnd::GetDesktopWindow());//&m_CtrTab);
	m_pModSetDlgWnd->GetWindowRect(InitOptionRect2);
	m_pModOptTabWnd = &m_pModSetDlgWnd->m_ModCtrTab;
	m_pModSetDlgWnd->ShowWindow(SW_HIDE);
}

void CImageTesterDlg::CModSet_Delete()
{
	if(m_pModSetDlgWnd != NULL)
	{
		m_pModSetDlgWnd->DestroyWindow();
		delete m_pModSetDlgWnd;
		m_pModSetDlgWnd = NULL;
	}
}

void CImageTesterDlg::CModSel_Create()
{
	CRect OptionRect;
	m_pModSelDlgWnd = new CModSelDlg(this);
	m_pModSelDlgWnd->Setup(this);
	m_pModSelDlgWnd->Create(CModSelDlg::IDD,&m_pModOptDlgWnd->m_Tab_Manager);
	m_pModSelDlgWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
	m_pModSelDlgWnd->MoveWindow(5,5,OptionRect.Width(),OptionRect.Height());
	m_pModSelDlgWnd->ShowWindow(SW_HIDE);
}

void CImageTesterDlg::CModSel_Delete()
{
	if(m_pModSelDlgWnd != NULL)
	{
		m_pModSelDlgWnd->DestroyWindow();
		delete m_pModSelDlgWnd;
		m_pModSelDlgWnd = NULL;
	}
}


void CImageTesterDlg::CStandOpt_Create()
{
	CRect OptionRect;

	// 시리얼 옵션 탭
	m_pStandOptWnd = new CStandOptDlg(this);
	m_pStandOptWnd->Setup(this);
	m_pStandOptWnd->Create(CStandOptDlg::IDD,&m_pModOptDlgWnd->m_Tab_Setting/*pBKResultDlg*/);
	m_pStandOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
	m_pStandOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
	m_pStandOptWnd->ShowWindow(SW_HIDE);

 	// MES 옵션 탭
 	m_pStandOptMesWnd = new CStandOptMesDlg(this);
 	m_pStandOptMesWnd->Setup(this);
 	m_pStandOptMesWnd->Create(CStandOptMesDlg::IDD,&m_pModOptDlgWnd->m_Tab_Setting);
 	m_pStandOptMesWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
 	m_pStandOptMesWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
 	m_pStandOptMesWnd->ShowWindow(SW_HIDE);

	// POGO 옵션 탭
	m_pOptPogoDlgWnd = new CPogo_Option(this);
	m_pOptPogoDlgWnd->Setup(this);
	m_pOptPogoDlgWnd->Create(CPogo_Option::IDD,&m_pModOptDlgWnd->m_Tab_Setting);//&m_CtrTab);
	m_pOptPogoDlgWnd->GetWindowRect(OptionRect);
	m_pOptPogoDlgWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
	m_pOptPogoDlgWnd->ShowWindow(SW_HIDE);

}

void CImageTesterDlg::CStandOpt_Delete()
{
	if(m_pStandOptWnd != NULL)
	{
		m_pStandOptWnd->DestroyWindow();
		delete m_pStandOptWnd;
		m_pStandOptWnd = NULL;	
	}
 
 	if(m_pStandOptMesWnd != NULL)
 	{
 		m_pStandOptMesWnd->DestroyWindow();
 		delete m_pStandOptMesWnd;
 		m_pStandOptMesWnd = NULL;	
 	}

	if(m_pOptPogoDlgWnd != NULL){
		m_pOptPogoDlgWnd->DestroyWindow();
		delete m_pOptPogoDlgWnd;
		m_pOptPogoDlgWnd = NULL;
	}
}


void CImageTesterDlg::CEtcModel_Create(tINFO INFO)
{
	CRect OptionRect;
	if(m_pBKDialog != NULL)
	{
		m_psubmodel[INFO.Number] = new CEtcModelDlg(this);
		m_psubmodel[INFO.Number]->Setup(this,INFO);
		m_psubmodel[INFO.Number]->Create(CEtcModelDlg::IDD,m_pBKDialog);
		m_psubmodel[INFO.Number]->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.

		m_psubmodel[INFO.Number]->MoveWindow(10,((OptionRect.Height()+1)*INFO.Number),OptionRect.Width(),OptionRect.Height());

		m_psubmodel[INFO.Number]->ShowDlg(SW_HIDE);
		m_psubmodel[INFO.Number]->ShowWindow(SW_SHOW);
	}	
}

void CImageTesterDlg::CEtcModel_Delete(int NUM)
{
	if(m_psubmodel[NUM] != NULL){
		m_psubmodel[NUM]->DestroyWindow();
		delete m_psubmodel[NUM];
		m_psubmodel[NUM] = NULL;	
	}
}

void CImageTesterDlg::CEtcModel_Delete(tINFO INFO)
{
	if(m_psubmodel[INFO.Number] != NULL){
		m_psubmodel[INFO.Number]->DestroyWindow();
		delete m_psubmodel[INFO.Number];
		m_psubmodel[INFO.Number] = NULL;
	}
}


void CImageTesterDlg::OnSelchangeModSubCtrtab()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int itab = m_pModOptTabWnd->GetCurSel();
	m_pModResTabWnd->SetCurSel(itab);

	if(m_psubmodel[itab]->IN_pWnd != NULL){
		m_psubmodel[itab]->ShowDlg(SW_SHOW);
	}

	if(m_pModSetDlgWnd !=NULL)
	m_pModSetDlgWnd->STATE_VIEW(0);
}

void CImageTesterDlg::OnSelchangingModSubCtrtab()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int itab = m_pModOptTabWnd->GetCurSel();
	
	if(m_psubmodel[itab]->IN_pWnd != NULL){
		m_psubmodel[itab]->InitPrm();
		m_psubmodel[itab]->ShowDlg(SW_HIDE);
	}
	if(m_pModSetDlgWnd !=NULL)

	m_pModSetDlgWnd->STATE_VIEW(0);



}




#pragma endregion
void CImageTesterDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if((nIDCtl==IDC_BUTTON1)||(nIDCtl==IDC_BTN_COMMU))
	{
		CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
		return;
	}

	if(nIDCtl==IDC_BTN_MAIN ||nIDCtl==IDC_BTN_WORK_BUTTON||nIDCtl==IDC_BTN_TEST_SETTING||nIDCtl==IDC_BTN_USER_CHANGE||nIDCtl==IDC_BTN_ETC_SETTING||nIDCtl==IDC_BTN_MASTER_SET)
	{
		// DRAWITEMSTRUCT 구조체의 정보중에서 HDC 형식의 핸들값을 CDC 객체로 변환한다.
		CDC *p_dc = CDC::FromHandle(lpDrawItemStruct->hDC);
		int t=0;
		// RECT 형식의 구조체값을 이용하여 CRect 객체를 생성한다.
		CRect r(lpDrawItemStruct->rcItem); 
		CString str;

		if(nIDCtl==IDC_BTN_MAIN)
		{
			str.Empty();
			str.Format(_T("MAIN"));
			t=1;	
		}
	
		if(nIDCtl==IDC_BTN_TEST_SETTING)
		{
			str.Empty();
			str.Format(_T("SETTING"));
			t=4;
		}
	
		if(nIDCtl==IDC_BTN_WORK_BUTTON)
		{
			str.Empty();
			str.Format(_T("WORKLIST"));
			t=5;
		}

		if(nIDCtl==IDC_BTN_USER_CHANGE)
		{
			if(b_UserMode == FALSE)
			{
				str.Empty();

				if(b_Chk_MESEn)
					str.Format(_T("MES 모드"));	
				else
					str.Format(_T("작업자 모드"));	

			}else if(b_UserMode == TRUE)
			{
				str.Empty();
				str.Format(_T("관리자 모드"));	
			}
			t=6;
		}

		if(nIDCtl==IDC_BTN_ETC_SETTING)
		{
			str.Empty();
			str.Format(_T("MODEL SET"));
			t=4;
		}

		if(nIDCtl==IDC_BTN_MASTER_SET)
		{
			str.Empty();
			str.Format(_T("MASTER SET"));
		}

        // 글자를 출력할 때 적용할 배경을 투명으로 설정한다.
        int old_mode = p_dc->SetBkMode(TRANSPARENT);
 
        // 버튼을 사용할 수 없는 상태일 경우
        if(lpDrawItemStruct->itemState & ODS_DISABLED)
		{
            // 버튼 영역을 회색으로 채운다.
            p_dc->FillSolidRect(r, RGB(200, 200, 200));
			
			p_dc->Draw3dRect(r, RGB(150, 150, 150),  RGB(150, 150, 150));
            // 진한 회색으로 글자색을 설정한다.
            p_dc->SetTextColor(RGB(128, 128, 128));
            // 버튼의 캡션을 출력한다.
            p_dc->DrawText(str, r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

        }else 
		{	
			//버튼을 사용할수 있을 경우 
            // 버튼 영역을 배경색으로 채운다.
            p_dc->FillSolidRect(r, RGB(240, 240, 240));
                
            // 버튼이 포커스를 얻은 경우
            if(lpDrawItemStruct->itemState & ODS_FOCUS)
			{
				p_dc->FillSolidRect(r,RGB(50, 82, 152));
                // 분홍색으로 테두리를 그리고, 글자색도 분홍색으로 설정한다.
                p_dc->Draw3dRect(r, RGB(50, 82, 152),  RGB(50, 82, 152));
				p_dc->SetTextColor(RGB(255, 255, 255));
            } else 
			{
                // 회색으로 테두리를 그리고, 글자색을 흰색으로 설정한다.
				if(nIDCtl==STATID)
				{
					COLORREF bkcol = RGB(122, 122, 192);
					COLORREF txcol = RGB(255, 255, 255);
					p_dc->FillSolidRect(r,bkcol);
					p_dc->Draw3dRect(r, bkcol,  bkcol);
					p_dc->SetTextColor(txcol);
				}else
				{
					p_dc->Draw3dRect(r, RGB(240, 240, 240),RGB(240, 240, 240));
					p_dc->SetTextColor(RGB(0, 0, 0));
				}
			}
            // 영역을 한픽셀씩 내부 방향으로 줄인다.
            r.left++;
            r.top++;
            r.right--;
            r.bottom--;
 
            // 버튼을 선택한 경우
            if(lpDrawItemStruct->itemState & ODS_SELECTED)
			{
                // 테두리의 색상을 지정한다.
                p_dc->Draw3dRect(r,  RGB(50, 82, 152),  RGB(50, 82, 152));
				
               // 버튼의 캡션을 오른쪽 아래 방향으로 한픽셀씩 이동시켜 출력한다.
                p_dc->DrawText(str, r + CPoint(1, 1), DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			}else 
			{
                // 테두리의 색상을 지정한다.
            	p_dc->Draw3dRect(r, RGB(110, 130, 142), RGB(110, 130, 142));
				// 버튼의 캡션을 출력한다.
                p_dc->DrawText(str, r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
            }
        }
	
        // 배경을 이전 모드로 설정한다.
		p_dc->SetBkMode(old_mode);
	}
	else
	{	
		CDC *p_dc = CDC::FromHandle(lpDrawItemStruct->hDC);
		
        // RECT 형식의 구조체값을 이용하여 CRect 객체를 생성한다.
        CRect r(lpDrawItemStruct->rcItem); 
 
        CString str;
		if(nIDCtl==IDCANCEL)
		{
			str.Empty();
			str.Format(_T("EXIT"));
		}
			
		if(nIDCtl==IDC_BTN_STOP)
		{
			str.Empty();
			str.Format(_T("STOP"));
		}
		
		if(nIDCtl==IDC_BTN_START)
		{
			str.Empty();
			str.Format(_T("START"));
		}

		int old_mode = p_dc->SetBkMode(TRANSPARENT);
 
        // 버튼을 사용할 수 없는 상태일 경우
        if(lpDrawItemStruct->itemState & ODS_DISABLED)
		{
            // 버튼 영역을 회색으로 채운다.
            p_dc->FillSolidRect(r, RGB(200, 200, 200));
			p_dc->Draw3dRect(r, RGB(150, 150, 150),  RGB(150, 150, 150));
            // 진한 회색으로 글자색을 설정한다.
            p_dc->SetTextColor(RGB(128, 128, 128));
            // 버튼의 캡션을 출력한다.
            p_dc->DrawText(str, r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
		} else 
		{
            // 버튼 영역을 하늘색으로 채운다.
        	p_dc->FillSolidRect(r, RGB(240, 240, 240));
                
            // 버튼이 포커스를 얻은 경우
            if(lpDrawItemStruct->itemState & ODS_FOCUS)
			{
				p_dc->FillSolidRect(r,RGB(50, 82, 152));
                // 분홍색으로 테두리를 그리고, 글자색도 분홍색으로 설정한다.
				//p_dc->Draw3dRect(r, RGB(236, 233, 216),RGB(236, 233, 216));
                p_dc->Draw3dRect(r, RGB(50, 82, 152),  RGB(50, 82, 152));
				p_dc->SetTextColor(RGB(255, 255, 255));
			} else 
			{
                // 회색으로 테두리를 그리고, 글자색을 흰색으로 설정한다.
				p_dc->Draw3dRect(r, RGB(240, 240, 240),RGB(240, 240, 240));
				p_dc->SetTextColor(RGB(0, 0, 0));
			}

            // 영역을 한픽셀씩 내부 방향으로 줄인다.
            r.left++;
            r.top++;
            r.right--;
            r.bottom--;
 
            // 버튼을 선택한 경우
            if(lpDrawItemStruct->itemState & ODS_SELECTED)
			{
                // 테두리의 색상을 지정한다.
                p_dc->Draw3dRect(r,  RGB(50, 82, 152),  RGB(50, 82, 152));

                // 버튼의 캡션을 오른쪽 아래 방향으로 한픽셀씩 이동시켜 출력한다.
                p_dc->DrawText(str, r + CPoint(1, 1), DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			} else 
			{
                // 테두리의 색상을 지정한다.
            	p_dc->Draw3dRect(r, RGB(110, 130, 142), RGB(110, 130, 142));
				// 버튼의 캡션을 출력한다.
                p_dc->DrawText(str, r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
            }
        }
        // 배경을 이전 모드로 설정한다.
		p_dc->SetBkMode(old_mode);
	}


	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}
#pragma region 영상


LRESULT CImageTesterDlg::OnCamDisplay(WPARAM wParam, LPARAM lParam)		// 실재 들어온 영상을 변환하여 활용하는 파트 
{	
	return TRUE;
}
void CImageTesterDlg::SaveBitmapToDirectFile(CDC* pDC, int BitCount, CString Path)
{

	CBitmap bmp, *pOldBmp;
	CDC dcMem;  
	BITMAP                  bm;
	BITMAPINFOHEADER        bi;
	LPBITMAPINFOHEADER      lpbi;
	DWORD                   dwLen;
	HANDLE                  handle;
	HANDLE                  hDIB;   
	HPALETTE                hPal=NULL;

	/*----- CDC의 내용을 Bitmap으로 전송 ----*/
	dcMem.CreateCompatibleDC(pDC);
	bmp.CreateCompatibleBitmap(pDC, CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT);
	pOldBmp = (CBitmap*) dcMem.SelectObject(&bmp);
	dcMem.BitBlt(0, 0, CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT, pDC, 0, 0, SRCCOPY);
	dcMem.SelectObject(pOldBmp);

	if (Path == "")          return;

	/*------------------------- 비트멥 헤더를 기록함 -------------------------*/

	if (hPal==NULL)
		hPal = (HPALETTE) GetStockObject(DEFAULT_PALETTE);
	GetObject(HBITMAP(bmp), sizeof(BITMAP), &bm);

	bi.biSize               = sizeof(BITMAPINFOHEADER);
	bi.biWidth              = bm.bmWidth;
	bi.biHeight             = bm.bmHeight;
	bi.biPlanes             = 1;
	bi.biBitCount           = 32;       
	bi.biCompression        = BI_RGB;
	bi.biSizeImage          = bm.bmWidth * bm.bmHeight * 3;
	bi.biXPelsPerMeter      = 0;
	bi.biYPelsPerMeter      = 0;
	bi.biClrUsed            = 0;
	bi.biClrImportant       = 0;

	int nColors = (1 << bi.biBitCount);
	if( nColors > 256 ) 
		nColors = 0;
	dwLen  = bi.biSize + nColors * sizeof(RGBQUAD);
	hPal = SelectPalette(pDC->GetSafeHdc(),hPal,FALSE);
	RealizePalette(pDC->GetSafeHdc());
	hDIB = GlobalAlloc(GMEM_FIXED,dwLen);
	lpbi = (LPBITMAPINFOHEADER)hDIB;
	*lpbi = bi;
	GetDIBits(pDC->GetSafeHdc(), 
		HBITMAP(bmp), 
		0, 
		(DWORD)bi.biHeight,
		(LPBYTE)NULL, 
		(LPBITMAPINFO)lpbi, 
		(DWORD)DIB_RGB_COLORS);
	bi = *lpbi;
	if (bi.biSizeImage == 0)
	{
		bi.biSizeImage = ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8) 
			* bi.biHeight;
	}
	dwLen += bi.biSizeImage;
	if (handle = GlobalReAlloc(hDIB, dwLen, GMEM_MOVEABLE))
		hDIB = handle;

	lpbi = (LPBITMAPINFOHEADER)hDIB;
	GetDIBits(pDC->GetSafeHdc(), 
		HBITMAP(bmp),
		0,                    
		(DWORD)bi.biHeight,      
		(LPBYTE)lpbi        
		+ (bi.biSize + nColors * sizeof(RGBQUAD)),
		(LPBITMAPINFO)lpbi,   
		(DWORD)DIB_RGB_COLORS);

	BITMAPFILEHEADER      hdr;
	hdr.bfType        = ((WORD) ('M' << 8) | 'B');        
	hdr.bfSize        = GlobalSize (hDIB) + sizeof(hdr);   
	hdr.bfReserved1   = 0;                                 
	hdr.bfReserved2   = 0;                                 
	hdr.bfOffBits=(DWORD)(sizeof(hdr)+lpbi->biSize + nColors * sizeof(RGBQUAD));
	char* pBmpBuf; 
	DWORD FileSize; 
	FileSize=sizeof(hdr)+GlobalSize(hDIB);
	pBmpBuf = new char[FileSize];
	memcpy(pBmpBuf,&hdr,sizeof(hdr));
	memcpy(pBmpBuf+sizeof(hdr),lpbi,GlobalSize(hDIB));
	/*--------------------- 실제 파일에 기록함 --------------------------*/



	CFile file;
	file.Open(Path, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite);
	file.Write(pBmpBuf,FileSize);
	file.Close();

	/*------------------------ 임시로 할당한 메모리를 해제.. -------------*/
	delete[] pBmpBuf;

	if(hDIB) 
	{       
		GlobalFree(hDIB);
	}
	SelectPalette(pDC->GetSafeHdc(),hPal,FALSE);  

	GotoDlgCtrl(((CStatic *)GetDlgItem(IDC_CAM_FRAME)));
}

BOOL CImageTesterDlg::CAMERA_STAT_CHK()
{

#if Def_IMGMode 

	return TRUE;
#endif

	return m_LVDSVideo.GetSignalStatus(0) &&b_PWRON;
	//return ComArtWnd->m_dwaVLossTBL[15];
}

void CImageTesterDlg::OverlayRegionINIT(int data){
	for(int x=0; x< CAM_IMAGE_WIDTH; x++){
		for(int y=0; y< CAM_IMAGE_HEIGHT; y++){
			m_OverlayArea[x][y] =data;
		}
	}
}

	
void CImageTesterDlg::Overlay_InputBuffer(bool mod){

	//IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);
	//BYTE R,G,B,BW;
	//DWORD	RGBPIX = 0,RGBLINE=0;
	//int index=0;
	//int startx = 0, starty = 0, endx = CAM_IMAGE_WIDTH, endy = CAM_IMAGE_HEIGHT;
	//int HEIGHT = CAM_IMAGE_HEIGHT, WIDTH = CAM_IMAGE_WIDTH, Xc=0, Yc=0, Radius=0;

	//OriginImage = cvLoadImage(OverlayImagePath);
	//	
	//if(!OriginImage)
	//	return;

	//for(int y = 0; y<CAM_IMAGE_HEIGHT; y++)
	//{
	//	for(int x=0; x<CAM_IMAGE_WIDTH; x++)
	//	{
	//		B =OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 0];
	//		G =OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 1];
	//		R =OriginImage->imageData[y * OriginImage->widthStep + x * 3 + 2];
	//		BW = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));

	//		if(BW > d_OVThrValue){
	//			m_OverlayArea[x][y] = 0;
	//			OriginImage->imageData[y*OriginImage->widthStep+x* 3 + 0] = 0;
	//			OriginImage->imageData[y*OriginImage->widthStep+x* 3 + 1] = 0;
	//			OriginImage->imageData[y*OriginImage->widthStep+x* 3 + 2] = 0;
	//		}
	//		else{
	//			m_OverlayArea[x][y] = 1;
	//			OriginImage->imageData[y*OriginImage->widthStep+x* 3 + 0] = 255;
	//			OriginImage->imageData[y*OriginImage->widthStep+x* 3 + 1] = 255;
	//			OriginImage->imageData[y*OriginImage->widthStep+x* 3 + 2] = 255;
	//		}
	//	}
	//}


	//if(mod == 1){
	//	cvNamedWindow("Region",1);      // result 라는 이름의 윈도우를 만들고
	//	cvShowImage("Region",OriginImage);  // 이 윈도우에 내가 만든 ipl_Img 이미지를 보이고
	//	cvWaitKey(0);                           // 키입력을 받은후에
	//	cvDestroyWindow("Region");        // 윈도우를 종료
	//}

	//cvReleaseImage(&OriginImage);
}

void CImageTesterDlg::OverlayImageSeach()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	CFileFind  finder;
//	int mcnt=0;
//	int bmpcnt[5]={0,};
//	int loopmum = 0,loopmum2=0;
//
////------------------------
//	CString fname;
//	int charnum=0;//폴더이름갯수
////-----------------------------
//
//	BOOL bWorking = finder.FindFile(OverlayImagePath);
//	BOOL FLAG = FALSE;
//	while(bWorking)
//	{
//		bWorking = finder.FindNextFile();
//		fname = finder.GetFileTitle();	
//		FLAG = TRUE;
//	}
//
//	if(FLAG == TRUE){
//
//		OverlayImagePath = Overlay_path+"\\"+fname+".bmp";
//		if(m_pSubStandOptWnd != NULL)
//			m_pSubStandOptWnd->OVERLAY_NAMEVIEW(fname);


//
//	}
}


#pragma endregion

#pragma region 통신관련


BOOL CImageTesterDlg::Portopen_Cam(){
	if(m_CameraPort.m_bConnected == FALSE)  //포트가 닫혀 있을 경우에만 포트를 열기 위해
	{
		if(m_CameraPort.OpenPort(m_CamSerial) ==TRUE)
		{	
			PORT_STAT_CAM(1,"CONNECTING");
			m_OldCamSerial = m_CamSerial;
			return TRUE;
		}else{
			PORT_STAT_CAM(0,"DISCONNECT");
			return FALSE;
		}
	}else if(m_OldCamSerial.Port == m_CamSerial.Port){
		PORT_STAT_CAM(1,"CONNECTING");
		return TRUE;
	}else{
		m_CameraPort.ClosePort();
		DoEvents(300);
		if(m_CameraPort.OpenPort(m_CamSerial) ==TRUE)
		{	
			PORT_STAT_CAM(1,"CONNECTING");
			m_OldCamSerial = m_CamSerial;
			return TRUE;
		}else{
			PORT_STAT_CAM(0,"DISCONNECT");
			return FALSE;
		}
	}
}

void CImageTesterDlg::PortClose_Cam(){
	if(m_CameraPort.m_bConnected == TRUE)  //포트가 닫혀 있을 경우에만 포트를 열기 위해
	{
		m_CameraPort.ClosePort();
	}
	PORT_STAT_CAM(0,"DISCONNECT");
}

BOOL CImageTesterDlg::PortChk_Cam()
{
	return m_CameraPort.m_bConnected;
}

BOOL CImageTesterDlg::AutoPortChk_Cam(){
	CString str="";
	if(Portopen_Cam() == TRUE){
		if(PortTst_Cam()){
			StatePrintf("CAMERA HW PortChk Success..Port COM %d Select",m_CamSerial.Port+1);
			return TRUE; //우선 기본적으로 지정되어 있는 포트를 우선 체크한다.
		}else{
			PortClose_Cam();
		}
	}

	int BackPort = m_CamSerial.Port;
	for(int lop = 0;lop<14;lop++){
		//if(BackPort != lop)
		{
			m_CamSerial.Port = lop;
			if(m_CameraPort.OpenPort(m_CamSerial) ==TRUE){
				if(PortTst_Cam()){
					//if(BackPort != lop)
					{
						m_CamSerial.Port = lop; //
						SaveSerialSet(&m_CamSerial,"CAMSERIAL",m_inifilename);
						if(m_pStandOptWnd != NULL){
							m_pStandOptWnd->PortUpdate();
						}
					}
					StatePrintf("CAMERA HW PortChk Success..Port COM %d Select",m_CamSerial.Port+1);
					m_OldCamSerial = m_CamSerial;
					return TRUE;
				}else{
					m_CameraPort.ClosePort();
				}
			}
		}
	}
	m_CamSerial.Port = BackPort;
	StatePrintf("CAMERA HW Chk Fails.....Manual Port COM %d Select",m_CamSerial.Port+1);
	DoEvents(100);	
	return FALSE;
}


char CImageTesterDlg::SendData8Byte_Cam(unsigned char * TxData, int Num)
{
	char ret = FALSE;
	if(m_CameraPort.m_bConnected)
	{
		if(m_CameraPort.WriteComm(TxData, Num) == Num)
		{
			ret = TRUE;
		}
		if(ret){
			if(m_pCommWnd != NULL){
				m_pCommWnd->DisplayDebugData(TxData,Num,TX_MODE,2);
			}
		}
	}
	return ret;
}

BOOL CImageTesterDlg::PortTst_Cam(){
// 	BYTE DATA[20] ={'!','0','0','0','0',
// 					'0','0','0','0','0','@'};
	BYTE DATA[30] = { '!', '0', '0', '0', '0',
						'0', '0', '0', '0', '0',
						'0', '0', '0', '0', '0',
						'0', '0', '0', '0', '0',
						'0', '0', '0', '0', '0',
						'0', '@' };
	Buf_Wait_Cam = TRUE;
	SendData8Byte_Cam(DATA, 27);
	BYTE ASCNUM = '1';
	for(int lop = 0;lop<50;lop++){//0.1초동안 ACK를 기다린다. 
		if(Buf_Wait_Cam == FALSE){
			if((m_SerialInput_Cam[1] == '0')&&(m_SerialInput_Cam[2] == ASCNUM)){
				return TRUE;
			}
		}
		DoEvents(10);
	}
	return FALSE;	
}

LRESULT CImageTesterDlg::OnCamCommunication(WPARAM wParam, LPARAM lParam)		// 실제 통신이 가능한 부분. 받는 데이터를 관장 //khk UART Recieve
{
	if(Read_Comport_Cam(&m_CameraPort)){//합당한 데이터가 들어왔을 경우 
	
	}
	return TRUE;
}

BOOL CImageTesterDlg::Read_Comport_Cam(CCommThread* serialport) //m_SerialInput
{
	BOOL retVal = FALSE;
	int iSize = (serialport->m_QueueRead).GetSize();
	if(iSize)
	{
		BYTE aByte;
		for(int i  = 0 ; i < iSize; i++)
		{
			(serialport->m_QueueRead).GetByte(&aByte);  //글자 하나씩 읽어옴
			if(aByte == '!'){
				memset(m_RXData_Cam.data,0,MAX_BUF_LEN);
				memset(m_SerialInput_Cam,0,sizeof(m_SerialInput_Cam));
				m_RXData_Cam.len = 0;
			}
			m_RXData_Cam.data[m_RXData_Cam.len] = aByte;
			m_RXData_Cam.len++;
			if(aByte == '@'){
				if(m_RXData_Cam.len >1024){
					m_RXData_Cam.len = 1024;
				}	
				memcpy(m_SerialInput_Cam,m_RXData_Cam.data,m_RXData_Cam.len);
				if(m_pCommWnd != NULL){//데이터 포멧이 정상적으로 완성이 되었을때 제어된다.
					m_pCommWnd->DisplayDebugData(m_RXData_Cam.data, m_RXData_Cam.len, RX_MODE,2);
				}
				Chk_Trans_Cam();
				memset(m_RXData_Cam.data,0,MAX_BUF_LEN);
				m_RXData_Cam.len = 0;
				retVal =TRUE;
			}
		}
	}
	
	return retVal;
}

void CImageTesterDlg::Chk_Trans_Cam()
{
	bool DataChk = FALSE;
	BYTE chkdata = m_SerialInput_Cam[1];
	switch(chkdata){
        case '1':
			break;
		case '2':
			break;
		case '3':
			break;
		case 'P':
		//	CurrentCheck();
			break;
		case '5':
            break;
		case '6':
            break;
		case '7':
			break;
		case '8':
			break;
		case '9':
			//VideoSyncCheck();
			break;
		case 'O':
			//Enable_Wait = FALSE;
			break;
		case 'A':
			//DCC_SYSTEM_WRITE_ACK();
			break;
		case 'B':
			BUTTON_STATE_CONTROL();
			//DCC_CHECKSUM_ACK();
			break;
		case 'C':
			Get_Wait = FALSE;
			break;
		case 'D':
			Get_Overcurrent = TRUE;
			//DCC_DTC_ERASE_ACK();
			break;
		default :
			break;
	}
	Buf_Wait_Cam = FALSE; //ACK가 정상적으로 왔다는 신호
}
void CImageTesterDlg::BUTTON_STATE_CONTROL(){

	int StartBtn = 0;
	int StopBtn = 0;
	int CylInBtn = 0;
	int CylOutBtn = 0;

	if (m_exbtn == 0){//0.5초안에들어오는신호는무시한다. 
		m_exbtn = 1;
		StartBtn = Ascii2int(m_SerialInput_Cam[2]);
		StopBtn = Ascii2int(m_SerialInput_Cam[3]);
		CylInBtn = Ascii2int(m_SerialInput_Cam[4]);
		CylOutBtn = Ascii2int(m_SerialInput_Cam[5]);
		DoEvents(500);

		if (StartBtn){
			if (((CButton *)GetDlgItem(IDC_BTN_START))->IsWindowEnabled() == TRUE){
				::PostMessage(hMainWnd, WM_COMM_START, 0, 0);
			}
		}
		if (StopBtn){
			if (((CButton *)GetDlgItem(IDC_BTN_STOP))->IsWindowEnabled() == TRUE){
				::PostMessage(hMainWnd, WM_COMM_STOP, 0, 0);
			}
		}
		if (CylInBtn){
			Cylinder_MOVE(0);
		}
		if (CylOutBtn){
			Cylinder_MOVE(1);
		}
	}
}
BOOL CImageTesterDlg::SIGNAL_LAMP(int MODE){ // 0:ALL OFF 1:RED 2:YELLOW 3:GREEN

	memset(m_SerialInput_Cam, 0, sizeof(m_SerialInput_Cam));
	BYTE DATA[20] = { '!', 'T', '0', '0', '0',
		'0', '0', '0', '0', '0', '@' };
	int lop = 0;

	if (MODE != 0)
		DATA[MODE + 1] = '1';

	if (!SendData8Byte_Cam(DATA, 11)){
		return FALSE;
	}

	for (lop = 0; lop < 150; lop++){
		DoEvents(10);
		if ((m_SerialInput_Cam[1] == 'T')){
			lop = 1000;
			break;
		}
	}

	if (lop == 1000){
		return TRUE;
	}
	return FALSE;
}
BOOL CImageTesterDlg::CHK_SENSER(){

	BOOL JigOpen = 0;
	memset(m_SerialInput_Cam, 0, sizeof(m_SerialInput_Cam));
	BYTE DATA[20] = {'!', 'S', '0', '0', '0',
					 '0', '0', '0', '0', '0', '@' };
	int lop = 0;

	if (!SendData8Byte_Cam(DATA, 11)){
		return FALSE;
	}

	for (lop = 0; lop < 150; lop++){
		DoEvents(10);
		if ((m_SerialInput_Cam[1] == 'S')){
			lop = 1000;
			break;
		}
	}

	if (lop == 1000){
		b_Cylinder_In = m_SerialInput_Cam[2] - '0';
		b_Cylinder_Out = m_SerialInput_Cam[3] - '0';
		JigOpen = m_SerialInput_Cam[4] - '0';
		return JigOpen;
	}
	return	FALSE;

}

BOOL CImageTesterDlg::Cylinder_MOVE(int InOut){

	BOOL JigClose = 0;
	memset(m_SerialInput_Cam, 0, sizeof(m_SerialInput_Cam));
	BYTE DATA[20] = {'!', 'C', '0', '0', '0',
					 '0', '0', '0', '0', '0', '@' };
	int lop = 0;
	
	
	CHK_SENSER();
	if (InOut == 0 && b_Cylinder_In == TRUE)
		return TRUE;
	if (InOut == 1 && b_Cylinder_Out == TRUE)
		return TRUE;
	DoEvents(100);
	if (!CHK_SENSER()){
		DoEvents(100);
		DATA[2 + InOut] = '1';
		if (!SendData8Byte_Cam(DATA, 11)){
			return FALSE;
		}

		for (lop = 0; lop < 150; lop++){
			DoEvents(10);
			if ((m_SerialInput_Cam[1] == 'C')){
				lop = 1000;
				break;
			}
		}
	}
	else{
		AfxMessageBox("안착 Jig를 확인해 주세요. ");
		return FALSE;
	}

	if (lop == 1000)
		DoEvents(i_Lightoffdelay);

	return TRUE;

}
BOOL CImageTesterDlg::MCU_READ_MODE(BOOL MODE)
{	
	BYTE DATA[20] ={'!','T','0','0','0',
					'0','0','0','0','0',
					'0','0','0','0','0',
					'0','0','0','0','@'};
	int lop = 0;
	BYTE Buf[3];
	Buf[0] = (BYTE)(MODE);

	DATA[2] = Hex2Ascii(Buf[0]);

	if(!SendData8Byte_Cam(DATA, 20)){
		return FALSE;
	}

	for(lop = 0;lop<150;lop++){
		DoEvents(10);
		if((m_SerialInput_Cam[1] == 'T') &&( m_SerialInput_Cam[2] == '1')){
			lop = 1000;
			break;
		}
	}

	if(lop == 1000){
		return TRUE;
	}
	return	FALSE;
}
BOOL CImageTesterDlg::MCU_READ_ADDR(BYTE* ADDR)
{	
	BYTE DATA[20] ={'!','Q','0','0','0',
					'0','0','0','0','0',
					'0','0','0','0','0',
					'0','0','0','0','@'};
	int lop = 0;
	DATA[9] = (BYTE)ADDR[7];
	DATA[8] = (BYTE)ADDR[6];
	DATA[7] = (BYTE)ADDR[5];
	DATA[6] = (BYTE)ADDR[4];
	DATA[5] = (BYTE)ADDR[3];
	DATA[4] = (BYTE)ADDR[2];
	DATA[3] = (BYTE)ADDR[1];
	DATA[2] = (BYTE)ADDR[0];

	if(!SendData8Byte_Cam(DATA, 20)){
		return FALSE;
	}

	for(lop = 0;lop<150;lop++){
		DoEvents(10);
		if((m_SerialInput_Cam[1] == 'Q') &&( m_SerialInput_Cam[2] == '1')){
			lop = 1000;
			break;
		}
	}

	if(lop == 1000){
		return TRUE;
	}
	return	FALSE;
}
BOOL CImageTesterDlg::MCU_READ_START()
{	
	BYTE DATA[20] ={'!','N','0','0','0',
					'0','0','0','0','0',
					'0','0','0','0','0',
					'0','0','0','0','@'};
	int lop = 0;

	if(!SendData8Byte_Cam(DATA, 20)){
		return FALSE;
	}

	for(lop = 0;lop<150;lop++){
		DoEvents(10);
		if((m_SerialInput_Cam[1] == 'N') &&( m_SerialInput_Cam[2] == '1')){
			lop = 1000;
			break;
		}
	}

	if(lop == 1000){
		return TRUE;
	}
	return	FALSE;
}
BOOL CImageTesterDlg::MCU_READ_START_SEND()
{	
	BYTE DATA[20] ={'!','M','0','0','0',
					'0','0','0','0','0',
					'0','0','0','0','0',
					'0','0','0','0','@'};
	int lop = 0;

	if(!SendData8Byte_Cam(DATA, 20)){
		return FALSE;
	}

	for(lop = 0;lop<150;lop++){
		DoEvents(10);
		if((m_SerialInput_Cam[1] == 'M') ){
			lop = 1000;
			break;
		}
	}

	if(lop == 1000){
		return TRUE;
	}
	return	FALSE;
}
void CImageTesterDlg::READ_DATA()
{
	int NUM,a,b = 0;
	CString fileName,str;
	CStdioFile sFile;
	char BUFDATA[2] = {0,};
	unsigned long BUFREAD1,BUFREAD2;
	BYTE BUFREAD[2] = {0,};
	char buf[1];
	int buf_read;
	a=0;

	for(int lop=0;lop<32;lop++)
	{
		BUFDATA[1] = m_SerialInput_Cam[3+(lop*18)];
		BUFDATA[0] = m_SerialInput_Cam[2+(lop*18)];
		NUM = atoi(BUFDATA);

		//NUM = ((int)m_SerialInput_Cam[2]*10)+(int)m_SerialInput_Cam[3];

		if(NUM>=0&&NUM<32){
			//for(int i=0;i<17;i++){
			for(int i=0;i<9;i++){
				//CAMREADBUFF[(NUM*16)+i] = m_SerialInput_Cam[4+i+(lop*18)];
				//BUFREAD[1] = m_SerialInput_Cam[4+i+(lop*18)];
				//BUFREAD[0] = m_SerialInput_Cam[4+i+(lop*18) +1];

				//CAMREADBUFF[(NUM*8)+i] = buf_read;

				BUFREAD1 = Ascii2Hex( m_SerialInput_Cam[4+(i*2)+(lop*18)]);
				BUFREAD2 = Ascii2Hex( m_SerialInput_Cam[4+(i*2)+(lop*18) +1]);
				CAMREADBUFF[(NUM*8)+i] = (BUFREAD1 << 4) | BUFREAD2;

			}
		}

		//if(NUM==31){
		//	fileName.Format("D:\\READ_DATA_%d.txt",count_V);
		//	count_V++;
		//	sFile.Open(fileName, CFile::modeCreate | CFile::modeWrite | CFile::typeText);

		//		CString data;
		//		for(int j=0; j<=NUM; j++)
		//		{
		//			//for(int i=0;i<16;i++)
		//			for(int i=0;i<8;i++)
		//			{
		//				//buf[0] = CAMREADBUFF[(j*16)+i];
		//				//buf1 = atoi(buf);
		//				//data.Format("%c ",CAMREADBUFF[(j*16)+i]);
		//				data.Format("%0.2X ",CAMREADBUFF[(j*8)+i]);
		//				sFile.WriteString(data);
		//				if(i==7)
		//				{
		//					data.Format("\t");
		//					sFile.WriteString(data);
		//					a++;
		//				}
		//				if(a==2)
		//				{
		//					data.Format("\n");
		//					sFile.WriteString(data);
		//					a=0;
		//				}
		//			}
		//			
		//		}
		//		
		//	sFile.Close();
		//}
	}	

}
BOOL CImageTesterDlg::Power_Rst()
{	
	if(Power_Off() == FALSE){
		return FALSE;
	}
	DoEvents(500);
	if(Power_On() == FALSE){
		return FALSE;
	}
	DoEvents(500);
	return TRUE;
}


BOOL CImageTesterDlg::Power_On(UINT nMODE /*= VR_PO_REGISTER*/)
{	
#if Def_IMGMode 
	return TRUE;
#endif
// 	if (b_PWRON == TRUE)
// 		return TRUE;

// 	if (b_luritechDebugMODE == 1){
// 		LGEVideo_Start();
// 		DoEvents(500);
// 		b_PWRON = TRUE;
// 		return TRUE;
// 	}

	if (m_nVideoRegisterMode == nMODE &&b_PWRON == TRUE)
	{
		return TRUE;
	}

	if (mB_Check_RegisterChange && nMODE != VR_POWEROFF)
	{
		Power_Off();
	}

	memset(m_SerialInput_Cam, 0, sizeof(m_SerialInput_Cam));
// 	BYTE DATA[20] ={'!','V','0','0','0',
// 					'0','0','0','0','0','@'};
	BYTE DATA[30] = { '!', 'V', '1', '0', '0',
					  '0', '0', '0', '0', '0',
					  '0', '0', '0', '0', '0',
					  '0', '0', '0', '0', '0',
					  '0', '0', '0', '0', '0',
					  '0', '@' };
	int lop = 0;
// 	double Volt = dVoltSave *10;
// 	BYTE Buf[3];
// 	Buf[0] = (BYTE)((int)Volt/100);
// 	BYTE INF = (BYTE)((int)Volt%100);
// 	Buf[1]= INF/10;
// 	Buf[2]= INF%10;
// 
// 	DATA[2] = Hex2Ascii(Buf[0]);
// 	DATA[3] = Hex2Ascii(Buf[1]);
// 	DATA[4] = Hex2Ascii(Buf[2]);


	Buf_Wait_Cam = TRUE;
	if(!SendData8Byte_Cam(DATA, 27)){
		return FALSE;
	}

	for(lop = 0;lop<150;lop++){
		DoEvents(10);
		if ((m_SerialInput_Cam[1] == 'V') && (m_SerialInput_Cam[2] == DATA[2])){
			lop = 1000;
			break;
		}
	}

	if (lop == 1000){
		DoEvents(i_VoltageDelay);

		if (LGEVideo_Start(nMODE)){
			DoEvents(1000);
			b_PWRON = TRUE;
			m_nVideoRegisterMode = nMODE; //RegisterSetting을 위한 작업
			return TRUE;
		}
	}
	return	FALSE;
}

BOOL CImageTesterDlg::Power_Off()
{	
#if Def_IMGMode 
	return TRUE;
#endif
// 	if (b_luritechDebugMODE == 1){
// 		LGEVideo_Stop();
// 		b_PWRON = FALSE;
// 		return TRUE;
// 	}
	memset(m_SerialInput_Cam, 0, sizeof(m_SerialInput_Cam));
	int lop = 0;
	//BYTE DATA[20] ={'!','V','0','0','0',
	//				'0','0','0','0','0','@'};
	BYTE DATA[30] = { '!', 'V', '0', '0', '0',
					'0', '0', '0', '0', '0',
					'0', '0', '0', '0', '0',
					'0', '0', '0', '0', '0',
					'0', '0', '0', '0', '0',
					'0', '@' };


	Buf_Wait_Cam = TRUE;
	if(!SendData8Byte_Cam(DATA, 27)){
		return FALSE;
	}

	for(lop = 0;lop<150;lop++){
		DoEvents(10);
		if((m_SerialInput_Cam[1] == 'V') &&( m_SerialInput_Cam[2] == '0')){
			lop = 1000;
			break;
		}
	}
	
	if(lop == 1000){
		//DoEvents(i_VoltageDelay);
		LGEVideo_Stop();
		DoEvents(1000);
		b_PWRON = FALSE;
		m_nVideoRegisterMode = VR_POWEROFF;
		return TRUE;
	}
	return FALSE;
	
}

void CImageTesterDlg::Video_Sync_Detect()
{
	BYTE DATA[20] ={'!','9','0','0','0','0','0','0','0','0','0','0','0','0','@'};

	m_Voltpp = 0;
	m_VoltSync = 0;
	Buf_Wait_Cam = TRUE;
	VSync_Wait = TRUE;
	SendData8Byte_Cam(DATA, 15);
}

void CImageTesterDlg::VideoSyncCheck()
{
	char BUFDATA[4] = {0,};
	char BUFDATA2[4] = {0,};
	
	BUFDATA[0] = m_SerialInput_Cam[2];
	BUFDATA[1] = m_SerialInput_Cam[3];
	BUFDATA[2] = m_SerialInput_Cam[4];
	BUFDATA[3] = m_SerialInput_Cam[5];
	m_Voltpp = atoi(BUFDATA);

	/*BUFDATA2[3] = m_SerialInput_Cam[9];
	BUFDATA2[2] = m_SerialInput_Cam[8];
	BUFDATA2[1] = m_SerialInput_Cam[7];
	BUFDATA2[0] = m_SerialInput_Cam[6];
	m_VoltSync = atoi(BUFDATA);*/
	VSync_Wait = FALSE;
}
#pragma endregion



void CImageTesterDlg::StatePrintf(LPCSTR lpcszString, ...)
{

	if(m_pTStat == NULL) return;
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = m_pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = m_pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		m_pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		m_pTStat -> Clear();			   //라인하나를 지운다
		bufLen = m_pTStat ->GetWindowTextLength();
		del_line++;
	}

	m_pTStat -> SetSel(-1,0);
	m_pTStat -> ReplaceSel(cstr);
	m_pTStat -> SetSel(-1,-1);
}


void CImageTesterDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CTime thetime;
	CString str= _T("");
	int hour=0,minute=0,second=0;
	int CamState = 0;
	switch(nIDEvent)	
	{
	case 110:	
		InitProgram();
		break;
	case 120:
#if Def_IMGMode 

		OnCameraRecvVideo(0, 0);
#endif
		if(m_exbtn == 1){
            if(btndelaycnt >= 8){
                btndelaycnt = 0;
			    m_exbtn = 0;
		    }else{
		        btndelaycnt++;
	        }
		}else{
			m_exbtn = 0;
		}

		if (Get_Overcurrent == TRUE && b_PWRON == TRUE)
		{
			Get_Overcurrent = FALSE;
			OnBnClickedBtnStop();
			AfxMessageBox("과전류가 감지되었습니다. TEST를 중지합니다.");
		}

		if(m_Motion.IsOpen())
		{
			if(m_exbtn == 0){
				m_exbtn =1;

				if (m_Motion.GetIOInStatus(DIO_INPORT_SOCKETDOOR)){
					b_DoorOpen = FALSE;
				}
				else{
					b_DoorOpen = TRUE;// 도어가 열려있음
				}

				if (m_Motion.GetIOInStatus(DIO_INPORT_SOCKETAREA)){
					b_SocketReady = FALSE;// 소켓이 누워있음
				}
				else{
					b_SocketReady = TRUE;
				}

				if(m_Motion.GetIOInStatus(DIO_INPORT_START))
					if(b_Chk_MESEn == TRUE && b_UserMode == FALSE && m_strLotIdFull.GetLength() == 0){}
					else
						::PostMessage(hMainWnd, WM_COMM_START, 0, 0 );
					
				else if(m_Motion.GetIOInStatus(DIO_INPORT_STOP))
					::PostMessage(hMainWnd, WM_COMM_STOP, 0, 0 );
			}							   

			if ((m_bUseSensor == TRUE) && (b_StartCommand == TRUE))
			{
				if (!m_Motion.GetIOInStatus(DIO_INPORT_DOOR))
				{
					if (m_bDoorSensor == FALSE)
						m_bDoorSensor = TRUE;
				}
				else if (!m_Motion.GetIOInStatus(DIO_INPORT_AREA) && b_Chk_AreaSensor)
				{
					if (b_StartCommand == TRUE)
					{
						if (m_bSafetySensor == FALSE)
						{
							m_bSafetySensor = TRUE;
						}
					}
				}
			}


			if (m_bDoorSensor || m_bSafetySensor)
			{
				if (m_EmergencyStatus == FALSE)
				{
					m_EmergencyStatus = TRUE;
					::SendMessage(hMainWnd, WM_COMM_EMERGENCY, 0, 0);
				}
			}


		}

		if(Buzzer_MODE == TRUE){
			if(Buzzer_Cnt > 0){
				Buzzer_Cnt--;	
			}else{
				Buzzer_MODE = FALSE;
				Buzzer_Cnt =0;
				BUZZER_OPERATING(0);
				
			}
		}

		if(Alarm_MODE == TRUE){
			if(Alarm_Cnt > 0){
				Alarm_Cnt--;	
			}else{
				Alarm_MODE = FALSE;
				Alarm_Cnt =0;
				ALARM_OPERATING(0);

			}
		}
	
		break;
	case 111:
		thetime = CTime::GetCurrentTime();
		hour=thetime.GetHour(); minute=thetime.GetMinute(); second=thetime.GetSecond();
		str.Empty();
		str.Format("%02d:%02d:%02d",hour,minute,second);

		Time_NUM(str);

		if(b_StartCommand == TRUE){
			DWORD end = GetTickCount();
			Mid_Time(TESTstartTime,end);
		}

		CAM_STAT(1, "CONNECT");
		//
		/*if (m_LVDSVideo.GetSignalStatus(0) == TRUE){
			CAM_STAT(1, "CONNECT");
			}
			else{
			CAM_STAT(0, "DISCONNECT");
			SubBmpPic(IDB_BITMAP_CAMOFF);
			SubBmpPic_master(IDB_BITMAP_CAMOFF);
			if (m_pMasterMonitor != NULL)
			{
			if ((m_pMasterMonitor->IsWindowVisible()))
			{
			m_pMasterMonitor->SubBmpPic(IDB_BITMAP_CAMOFF);
			}
			}
			}*/

		break;

	case TM_CAM_FRAME:

// 		CamState = ViewFrame();
// 
// 		//디버깅
// 		//CamState = TRUE;
// 
// 		if (CamState != 1)
// 		{
// 			if (LostFrameCnt > curre)
// 			{
// 				if (CamOnCheck == TRUE)
// 				{
// 					Device_Stop();
// 				}
// 
// 				CAM_STAT(0, "DISCONNECT");
// 
// 
// 				SubBmpPic(IDB_BITMAP_CAMOFF);
// 				SubBmpPic_master(IDB_BITMAP_CAMOFF);
// 
// 
// 				if (m_pMasterMonitor != NULL)
// 				{
// 					if ((m_pMasterMonitor->IsWindowVisible()))
// 					{
// 						m_pMasterMonitor->SubBmpPic(IDB_BITMAP_CAMOFF);
// 					}
// 				}
// 			}
// 			LostFrameCnt++;
// 		}
// 		else
// 		{
// 			/*DoEvents(10);*/
// 			OnCamDisplay();
// 			LostFrameCnt = 0;
// 			CAM_STAT(1, "CONNECT");
// 		}

		break;
	case TM_MES_RETRYCONNECT:	//연결이 되지 않았을 경우 5초에 한번씩 연결을 시도한다. 
			if((b_AutoMesConnect == TRUE)&&(b_Chk_MESEn == TRUE)&&(b_StartCommand == FALSE) && b_UserMode == FALSE)	
			{
				if(!m_SocketManager.IsStart())
					SocketConnect();
			}else
			{
				m_nSockRetryConnect = 0;
			}
			break;

	case TM_MACHINE_CHECK:
		WaitCheckMachineSafety();
		break;

	}	
	CDialog::OnTimer(nIDEvent);
}
// int CImageTesterDlg::ViewFrame(void)
// {
// 	int     nRet;
// 	DWORD	dwCount, nPacKet;
// 	dwCount = (LONG)CAM_IMAGE_WIDTH * (LONG)CAM_IMAGE_HEIGHT * 2;
// 
// 	//if(i_SignalBit > 1)	// 24,32bit mode
// 	//	dwCount *= 2;
// 
// 	nPacKet = dwCount / 16384;
// 
// 	if ((dwCount % 16384) != 0)
// 	{
// 		nPacKet++;
// 	}
// 
// 	dwCount = nPacKet * 16384;
// 
// 	memset(m_YUVIn, 0, sizeof(m_YUVIn));
// 
// 	if (LVDS_GetFrame(m_nBoard, &dwCount, m_YUVIn))
// 	{
// 		ConvertImageData();
// 		nRet = 1;
// 	}
// 	else
// 	{
// 		nRet = 0;
// 	}
// 
// 	return nRet;
// }
// void CImageTesterDlg::ConvertImageData()
// {
// // 	if (i_SignalType == 2)
// // 	{
// // 		Mul = 3;
// 		Convert_Bayer_Image(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT, m_RGBIn, m_YUVIn);
// 
// // 	}
// // 	else{
// // 		Mul = 4;
// // 		Convert_YUV_Image(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT, m_RGBIn, m_YUVIn);
// // 	}
// 
// }
// void CImageTesterDlg::Convert_Bayer_Image(int nXres, int nYres, LPBYTE pBmpBuffer, LPBYTE pImageData)
// {
// 
// 	cv::Mat bayer16BitMat(nYres, nXres, CV_16U, pImageData);
// 	cv::bitwise_and(bayer16BitMat, 0x0FFF, bayer16BitMat);
// 	cv::Mat bayer8BitMat = bayer16BitMat.clone();
// 	bayer8BitMat.convertTo(bayer8BitMat, CV_8UC1, 0.0625);
// 	cv::Mat rgb8BitMat(1080, 1920, CV_8UC3, pBmpBuffer);
// 
// // 	if (i_FormatType == 0)
// // 	{
// // 		cv::cvtColor(bayer8BitMat, rgb8BitMat, CV_BayerBG2BGR);
// // 	}
// // 	else if (i_FormatType == 1)
// // 	{
// // 		cv::cvtColor(bayer8BitMat, rgb8BitMat, CV_BayerRG2BGR);
// // 	}
// // 	else if (i_FormatType == 2)
// // 	{
// // 		cv::cvtColor(bayer8BitMat, rgb8BitMat, CV_BayerGB2BGR);
// // 	}
// // 	else if (i_FormatType == 3)
// // 	{
// // 		cv::cvtColor(bayer8BitMat, rgb8BitMat, CV_BayerGR2BGR);
// // 	}
// 	for (int Y = 0; Y < nYres; Y++)
// 	{
// 		for (int X = 0; X<nXres; X++)
// 		{
// 			if (pBmpBuffer[Y * (nXres * 3) + X * 3 + 0] >255)
// 			{
// 				pBmpBuffer[Y * (nXres * 3) + X * 3 + 0] = 255;
// 			}
// 			if (pBmpBuffer[Y * (nXres * 3) + X * 3 + 0] < 0)
// 			{
// 				pBmpBuffer[Y * (nXres * 3) + X * 3 + 0] = 0;
// 			}
// 
// 			if (pBmpBuffer[Y * (nXres * 3) + X * 3 + 1] > 255)
// 			{
// 				pBmpBuffer[Y * (nXres * 3) + X * 3 + 1] = 255;
// 			}
// 			if (pBmpBuffer[Y * (nXres * 3) + X * 3 + 1] < 0)
// 			{
// 				pBmpBuffer[Y * (nXres * 3) + X * 3 + 1] = 0;
// 			}
// 
// 			if (pBmpBuffer[Y * (nXres * 3) + X * 3 + 2] > 255)
// 			{
// 				pBmpBuffer[Y * (nXres * 3) + X * 3 + 2] = 255;
// 			}
// 			if (pBmpBuffer[Y * (nXres * 3) + X * 3 + 2] < 0)
// 			{
// 				pBmpBuffer[Y * (nXres * 3) + X * 3 + 2] = 0;
// 			}
// 		}
// 	}
// 	//memcpy(m_pBuf, pBmpBuffer, nXres * nYres * 3);//3360
// }
// void CImageTesterDlg::Device_Open()
// {
// 	int nBoard;
// 	nBoard = OpenDAQDevice();
// 
// 	if (nBoard == 0)
// 	{
// 		StatePrintf("Board Open Error!");
// 		return;
// 	}
// 	else
// 	{
// 		StatePrintf("Board Open Success!");
// 		m_nBoard = nBoard - 1;
// 	}
// 
// 	DoEvents(100);
// 	BOOL bRet = LVDS_Init(m_nBoard);
// 
// 	if (!bRet)
// 	{
// 		StatePrintf("Board Init Error!");
// 		return;
// 	}
// 	StatePrintf("Board Init Success!");
// }
// 

// BOOL CImageTesterDlg::Device_Play()
// {
// 
// 	LVDS_Stop(m_nBoard);
// 	DoEvents(100);
// 
// 	LVDS_Close(m_nBoard);
// 	DoEvents(100);
// 
// 	LVDS_Init(m_nBoard);
// 	DoEvents(100);
// 
// 	//	CLK_Set(m_nBoard, 1000000);
// 	DoEvents(100);
// 
// 	CString strI2cPath = I2C_FolderPath + _T("\\") + I2C_FileName + _T(".set");
// 	I2C_File_Init(strI2cPath);
// 
// 	// option
// 	// TVI : 16bit, RVC : 8bit
// // 	if (i_SignalType == 0){
// // 		LVDS_SetDataMode(m_nBoard, 0);
// // 	}
// // 	else{
// 		LVDS_SetDataMode(m_nBoard, 1);
// //	}
// // 	if (i_SignalType == 2){
// // 		LVDS_VideoMode(m_nBoard, 0);
// // 	}
// // 	else{
// 		LVDS_VideoMode(m_nBoard, 1);
// //	}
// 	LVDS_SetDeUse(m_nBoard, FALSE);
// 	LVDS_HsyncPol(m_nBoard, TRUE);
// 	LVDS_PclkPol(m_nBoard, FALSE);
// 
// 	if (LVDS_Start(m_nBoard) == FALSE)
// 	{
// 		StatePrintf("Device Play Error!");
// 		return FALSE;
// 	}
// 
// 	DoEvents(100);
// 	CamOnCheck = TRUE;
// 	return TRUE;
// }
// 
// void CImageTesterDlg::Device_Stop()
// {
// 	LVDS_Stop(m_nBoard);
// 	CamOnCheck = FALSE;
// }
// BOOL CImageTesterDlg::I2C_File_Init(CString FilePath)
// {
// 	CStdioFile file;
// 
// 	if (!file.Open(FilePath, CFile::modeRead))
// 	{
// 		return FALSE;
// 	}
// 
// 	BOOL	bRet = TRUE;
// 	int nPos, subPos, lastPos;
// 	CString cs, csData, trans;
// 	int nID, nAddr, /*nData,*/ nSleep, nAddrlength, nDatalength;
// 	int DataType = 0;
// 	BYTE buf_Data[1024];
// 
// 	BOOL Success = TRUE;
// 
// 	while (Success)
// 	{
// 		Success = file.ReadString(cs);
// 
// 		cs.MakeLower();
// 		cs.Remove(' ');
// 		cs.Remove('\t');
// 
// 		nPos = cs.Find(_T("sleep"));
// 		if (nPos != -1)
// 		{
// 			nSleep = _ttoi(cs.Mid(nPos + 5, cs.GetLength() - (nPos + 5)));
// 			DoEvents(nSleep);
// 		}
// 
// 		nPos = cs.Find(_T("slave"));
// 		if (nPos != -1)
// 		{
// 			nID = _tcstoul(cs.Mid(nPos + 5, cs.GetLength() - (nPos + 5)), 0, 16);
// 		}
// 		else
// 		{
// 			nPos = cs.Find(_T("0x"));
// 			if (nPos != -1)
// 			{
// 				subPos = cs.Find(_T("0x"), nPos + 2);
// 				lastPos = cs.GetLength();
// 
// 				nAddrlength = (subPos - nPos - 2) / 2;
// 				nDatalength = (lastPos - subPos - 2) / 2;
// 
// 				for (int i = 0; i < nDatalength; i++)
// 				{
// 					//csData = cs.Mid(lastPos-((2*i)+2), 2); //little endian
// 					csData = cs.Mid(subPos + ((2 * i) + 2), 2); //big endian
// 					//trans = trans+csData;
// 					buf_Data[i] = (BYTE)_tcstoul(csData, 0, 16);
// 				}
// 
// 				nAddr = _tcstoul(cs.Mid(nPos, subPos), 0, 16);
// 
// 				bRet = I2C_SYS_Write(m_nBoard, nID << 1, nAddrlength, nAddr, nDatalength, buf_Data);
// 
// 				if (bRet == FALSE)
// 				{
// 					return FALSE;
// 				}
// 			}
// 		}
// 	}
// 
// 	return TRUE;
//}
void CImageTesterDlg::Create_CommDlg(){
	m_pCommWnd = new CCommDlg(this);
	m_pCommWnd->Setup(this);
	m_pCommWnd->Create(CCommDlg::IDD,CWnd::GetDesktopWindow());
	m_pCommWnd->ShowWindow(SW_HIDE);
}
void CImageTesterDlg::Delete_CommDlg(){
	if(m_pCommWnd != NULL){
		m_pCommWnd->DestroyWindow();
		delete m_pCommWnd;
		m_pCommWnd = NULL;	
	}
}

void CImageTesterDlg::PortUpdate()
{
	PortClose_Cam();
	PortClose_ConCenter();
	PortClose_ConSide();
	Portopen_Cam();
	Portopen_ConCenter();
	Portopen_ConSide();
}
void CImageTesterDlg::OnBnClickedBtnUserChange()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(b_UserMode == FALSE){
		if(MatchPassWord == _T(""))
		{
			b_UserMode = TRUE; //전문가 모드
			m_pModResTabWnd->ShowWindow(1);
			m_pModResTabWnd2->ShowWindow(0);
			m_pMainCombo->ShowWindow(1);
			FullOptionEnable(1);

		}else
		{
			CUserSwitching subdlg;
			subdlg.Setup(this);
			if(subdlg.DoModal() == IDOK)
			{
				b_UserMode = TRUE; //전문가 모드
				m_pModResTabWnd->ShowWindow(1);
				m_pModResTabWnd2->ShowWindow(0);
				m_pMainCombo->ShowWindow(1);
				FullOptionEnable(1);
			}else{
				FullOptionEnable(0);

			}
			delete subdlg;

		}
		

	}else
	{
		b_UserMode = FALSE;//작업자 모드
		m_pModResTabWnd->ShowWindow(0);
		m_pModResTabWnd2->ShowWindow(1);

		if(b_Chk_MESEn == 1){
			if(m_pResLotWnd != NULL)
				m_pResLotWnd->ShowWindow(SW_HIDE);
			if(m_pResWorkerOptWnd != NULL)
				m_pResWorkerOptWnd->ShowWindow(SW_SHOW);
		}else{
			if(m_pResLotWnd != NULL)
				m_pResLotWnd->ShowWindow(SW_SHOW);
			if(m_pResWorkerOptWnd != NULL)
				m_pResWorkerOptWnd->ShowWindow(SW_HIDE);
		}
		m_pMainCombo->ShowWindow(0);
		if(m_pModOptDlgWnd->IsWindowVisible())
			m_pModOptDlgWnd->ShowWindow(SW_HIDE);
		
		b_SecretOption=0;
		FullOptionEnable(0);

	}
	SETTING_USER_Switching();
	OnBnClickedBtnMain();
	Focus_move_start();
}

void CImageTesterDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	Power_Off();
	if (b_luritechDebugMODE == 0)
		Power_Total_Control_off();
	SaveMotorParameter();

	ClickedBtnLotExit();
//	m_SocketManager.StopComm();

	KillTimer(TM_MES_RETRYCONNECT);
	KillTimer(110);
	KillTimer(120);
	KillTimer(111);
	KillTimer(TM_MACHINE_CHECK);
	Sleep(100);
	SocketDisconnect();

// 	ComArtWnd->VCAPT_STOP(15);
// 	delete ComArtWnd;
	
	Delete_CommDlg();

	ModelClear();
	EndModelSetup();
	Delete_CommDlg();
	if(m_pDlgOrigin)
	{
		delete m_pDlgOrigin;
		m_pDlgOrigin = NULL;
	}

	m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_RED, FALSE);
	m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_GREEN, FALSE);
	m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_YELLOW, FALSE);

	m_Motion.SetIOOutPort(DIO_OUTPORT_BRDPOWER, FALSE);
	DoEvents(100);
	m_Motion.SetIOOutPort(DIO_OUTPORT_BRDPOWER, TRUE);
	CDialog::OnCancel();
}

HBRUSH CImageTesterDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_ProTime){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_ProTime2){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_TIME){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_TIME2){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_PORTNAME){
	//	pDC->SetTextColor(txcol_PortName);
	//	pDC->SetBkColor(bkcol_PortName);
		pDC->SetTextColor(RGB(255, 255, 255));
		// 텍스트의 배경색상을 설정한다.
		//pDC->SetBkColor(RGB(138, 75, 36));
		pDC->SetBkColor(RGB(86,86,86));
		// 바탕색상을 설정한다.
	}

	

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_CAMNAME){
	//	pDC->SetTextColor(txcol_CamNmae);
	//	pDC->SetBkColor(bkcol_CamNmae);
		pDC->SetTextColor(RGB(255, 255, 255));
		// 텍스트의 배경색상을 설정한다.
		//pDC->SetBkColor(RGB(138, 75, 36));
		pDC->SetBkColor(RGB(86,86,86));
		// 바탕색상을 설정한다.
	}

	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_PORTSTATE){
		pDC->SetTextColor(txcol_PortState);
		pDC->SetBkColor(bkcol_PortState);
	}


	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_CAMSTATE){
		pDC->SetTextColor(txcol_CamState);
		pDC->SetBkColor(bkcol_CamState);
	}

	if(pWnd->GetDlgCtrlID() ==IDC_TEST_STATUS){
		pDC->SetTextColor(txcol_TVal);
		pDC->SetBkColor(bkcol_TVal);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_POGONAME){
		pDC->SetTextColor(RGB( 86,86,86));
		// 텍스트의 배경색상을 설정한다.
		pDC->SetBkColor(RGB(255, 255, 255));
		// 바탕색상을 설정한다.
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_COUNT_TEST){
//		pDC->SetTextColor(RGB( 86,86,86));
//		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(txcol_pg);
		pDC->SetBkColor(bkcol_pg);

	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CImageTesterDlg::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}


void CImageTesterDlg::PORT_NAME(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_EDIT_PORTNAME))->SetWindowText(lpcszString);
}

void CImageTesterDlg::CAM_NAME(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_EDIT_CAMNAME))->SetWindowText(lpcszString);
}


void CImageTesterDlg::PORT_STAT_CAM(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){
		txcol_PortState = RGB(0, 0, 0);
		bkcol_PortState = RGB(255, 255, 0);
	}else if(col == 1){
		txcol_PortState = RGB(255, 255, 255);
		bkcol_PortState = RGB(47, 157, 39);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_PORTSTATE))->SetWindowText(lpcszString);
}



void CImageTesterDlg::CAM_STAT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){
		txcol_CamState = RGB(0, 0, 0);
		bkcol_CamState = RGB(255, 255, 0);
	}else if(col == 1){
		txcol_CamState = RGB(255, 255, 255);
		bkcol_CamState = RGB(47, 157, 39);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_CAMSTATE))->SetWindowText(lpcszString);
}

void CImageTesterDlg::TEST_STAT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}else if(col == 1){
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18, 69, 171);
	}else if(col == 2){
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}

	((CEdit *)GetDlgItem(IDC_TEST_STATUS))->SetWindowText(lpcszString);
}


void CImageTesterDlg::Time_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_TIME))->SetWindowText(lpcszString);
}
void CImageTesterDlg::Time_NUM(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_TIME2))->SetWindowText(lpcszString);
}
void CImageTesterDlg::ProTime_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_ProTime))->SetWindowText(lpcszString);
}
void CImageTesterDlg::ProTime_NUM(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_ProTime2))->SetWindowText(lpcszString);
}
void CImageTesterDlg::SubBmpPic(UINT nIDResource)//나중에 전역변수로 간략화 할것임
{	
	CRect rect;
	CDC *pcDC = m_CamFrame.GetDC();
	m_CamFrame.GetClientRect(&rect);
			
	CDC MemDC;
	CBitmap image;
	BITMAP bmpinfo;

	MemDC.CreateCompatibleDC(pcDC);
	image.LoadBitmap(nIDResource);
	image.GetBitmap(&bmpinfo);
	CBitmap*pOldBitmap = (CBitmap*)MemDC.SelectObject(&image);
	pcDC->SetStretchBltMode(COLORONCOLOR);
	pcDC->StretchBlt(0, 0, rect.Width(), rect.Height(), &MemDC, 0, 0, bmpinfo.bmWidth, bmpinfo.bmHeight, SRCCOPY);
	MemDC.SelectObject(pOldBitmap);
	image.DeleteObject();
	MemDC.DeleteDC();
	ReleaseDC(pcDC);
}

void CImageTesterDlg::SubBmpPic_master(UINT nIDResource)//나중에 전역변수로 간략화 할것임
{	
	CRect rect;
	CDC *pcDC = m_pModSetDlgWnd->m_CamFrame.GetDC();
	m_pModSetDlgWnd->m_CamFrame.GetClientRect(&rect);
			
	CDC MemDC;
	CBitmap image;
	BITMAP bmpinfo;

	MemDC.CreateCompatibleDC(pcDC);
	image.LoadBitmap(nIDResource);
	image.GetBitmap(&bmpinfo);
	CBitmap*pOldBitmap = (CBitmap*)MemDC.SelectObject(&image);
	pcDC->SetStretchBltMode(COLORONCOLOR);
	pcDC->StretchBlt(0, 0, rect.Width(), rect.Height(), &MemDC, 0, 0, bmpinfo.bmWidth, bmpinfo.bmHeight, SRCCOPY);
	MemDC.SelectObject(pOldBitmap);
	image.DeleteObject();
	MemDC.DeleteDC();
	ReleaseDC(pcDC);
}


void CImageTesterDlg::ClickedBtnLotStart(void)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	NewLotCreate();
}

void CImageTesterDlg::ClickedBtnLotExit(void)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	if(LotMod ==1){
		for(int lop=0; lop<20; lop++){
			if(m_psubmodel[lop] != NULL)
			{
				//m_psubmodel[lop]->LOT_Excel_Save();
				m_psubmodel[lop]->Excel_Save();
			}
		}
//		LOT_EXCEL_SAVE();

		LOT_NUMBER ="";
		if(m_pResLotWnd != NULL){
			m_pResLotWnd->LotBtn_Enable(1);
		}
		LotMod=0;Lot_Passint=0;Lot_Failint=0;Lot_Totalint=0;Lot_Percentint=0;Lot_StartCnt=0;
	
		m_pResLotWnd->LOTID_VAL("");
		SETTING_LotInfo();
		((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_STOP))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_USER_CHANGE))->EnableWindow(1);
		m_pMainCombo->EnableWindow(1);


		Focus_move_start();
	}
	ALL_WORKLIST_UPLOAD();
	FullOptionEnable(0);
	
}

void CImageTesterDlg::OnBnClickedBtnWorkButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nOldItem = m_pWorkListWnd->m_TEST_Item.GetSelectionMark();

	if(nOldItem > 0){
		m_pWorkListWnd->m_TEST_Item.SetItemState(nOldItem, LVIS_FOCUSED, ~LVIS_FOCUSED); 
		m_pWorkListWnd->m_TEST_Item.SetItemState(nOldItem, LVIS_SELECTED, ~LVIS_SELECTED); 
	}

	STATID = IDC_BTN_WORK_BUTTON;
	int m_BackCursel=0;

	m_pWorkListWnd->m_TEST_Item.SetSelectionMark(0);
	m_pWorkListWnd->m_TEST_Item.SetItemState(0, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED);
	m_pWorkListWnd->m_TEST_Item.SetFocus();
	Set_List(&(m_pWorkListWnd->m_WorkList));

	if (LotMod == TRUE)
		Copy_List(&m_Lot_MainWorkList, &(m_pWorkListWnd->m_WorkList), ListItemNum);
	else
		Copy_List(&MainWork_List, &(m_pWorkListWnd->m_WorkList), ListItemNum);
	m_pWorkListWnd->ShowWindow(SW_SHOW);
	ButtonInit();
}


void CImageTesterDlg::MainModelSel()//초기 모델명에 따른 폴더가 있는지 확인한다. 
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//--------------------초기화파일을 읽어 과거에 저장된 folder이름을 불러온다.-------------
	CString	   loadmodel = _T("");
	TCHAR cload[100] ={0,};
	int frnum;//폴더이름갯수
	frnum = GetPrivateProfileString(_T("INITPLACE"),_T("MODEL"),_T(""),cload,100,INITMODELFILE);
	loadmodel.Format(_T("%s"), cload);
	//---------------------------------------------------------------------------------------
	ModelSearch(loadmodel);
}

void CImageTesterDlg::ModelSet()
{
	CString str_buf = _T("");
	for(int lop = 0;lop<MAX_TEST_NUM;lop++)
	{
		str_buf.Empty();
		str_buf.Format(_T("Model_%d"),lop+1);
		m_ModelInfo[lop].ID = GetPrivateProfileInt(str_buf,_T("ID"),0,modelfile_Name);
		m_ModelInfo[lop].MODE = GetPrivateProfileCString(str_buf,_T("MODE"),modelfile_Name);
		m_ModelInfo[lop].NAME = GetPrivateProfileCString(str_buf,_T("NAME"),modelfile_Name);
		m_ModelInfo[lop].nMesIdx = _ttoi(GetPrivateProfileCString(str_buf,_T("MESID"),modelfile_Name));
		m_ModelInfo[lop].nMesNum= _ttoi(GetPrivateProfileCString(str_buf,_T("MESNUM"),modelfile_Name));
	}


	if(m_pModSelDlgWnd != NULL)
		m_pModSelDlgWnd->InitDstSet();
}

void CImageTesterDlg::ModelGen()
{
	ModelClear();

	int m_number = 0;
	m_ModelStart.Number = m_number;
	m_ModelStart.ID = 0xfe;
	m_ModelStart.NAME = _T("Model Start");
	m_ModelStart.nMesIdx = MES_TEST1;

	m_number++;
	CEtcModel_Create(m_ModelStart);
	m_pModOptTabWnd->InsertItem(m_ModelStart.Number,_T("STANDARD"));
	m_pModResTabWnd->InsertItem(m_ModelStart.Number,m_ModelStart.NAME);
	//m_psubmodel[0]->NameChange("Model Start");

	if(b_Chk_CurrentChkEn == TRUE)
	{
		m_psubmodel[0]->NameChange(_T("Start(동작전류)"));
	}else
	{
		m_psubmodel[0]->NameChange(_T("Start"));
	}

	for(int lop = 0;lop < MAX_TEST_NUM;lop++)
	{
		if(m_ModelInfo[lop].ID != 0)
		{
			m_ModelInfo[lop].Number = m_number;
			m_number++;
			CEtcModel_Create(m_ModelInfo[lop]);
			m_pModOptTabWnd->InsertItem(m_ModelInfo[lop].Number,m_ModelInfo[lop].NAME);
			m_pModResTabWnd->InsertItem(m_ModelInfo[lop].Number,m_ModelInfo[lop].NAME);


		}else{
			if(m_number != 0)
			{
				m_pModOptTabWnd->SetCurSel(0);
				m_pModOptTabWnd->ShowWindow(1);
				m_pModResTabWnd->SetCurSel(0);
				if(b_UserMode == FALSE)
				{
					m_pModResTabWnd->ShowWindow(0);
					m_pModResTabWnd2->ShowWindow(1);
					m_pMainCombo->ShowWindow(0);
				}else
				{
					m_pModResTabWnd->ShowWindow(1);
					m_pModResTabWnd2->ShowWindow(0);
					m_pMainCombo->ShowWindow(1);
				}
				if(m_psubmodel[0]->IN_pWnd != NULL)
					m_psubmodel[0]->ShowDlg(SW_SHOW);
				
			}else
			{
				m_pModOptTabWnd->ShowWindow(0);
				m_pModResTabWnd->ShowWindow(0);
			}
			break;
		}
	}

	if(b_Chk_CurrentChkEn == TRUE){
		worklist_uploadList(1);
	}else{
		worklist_uploadList(0);
	}

	m_ModelEnd.Number = m_number;
	m_ModelEnd.ID = 0xff;
	m_ModelEnd.NAME = _T("Model End");
	CEtcModel_Create(m_ModelEnd);

	m_psubmodel[m_number]->NameChange(_T("End"));

	LOT_Set_List(&m_Lot_MainWorkList);
	LOT_Set_List_C(&m_Lot_CurrentList);
	LOT_Set_List_Eeprom(&m_Lot_EepromList);
	
	CREATE_Folder(m_modelName, 2);
	ALL_WORKLIST_UPLOAD();


	SETTING_USER_Switching();
}

void CImageTesterDlg::worklist_uploadList(bool stat){

	m_pWorkListWnd->m_TEST_Item.DeleteAllItems();

	m_pWorkListWnd->m_TEST_Item.InsertItem(0,"");
	m_pWorkListWnd->m_TEST_Item.SetItemText(0,0,"TOTAL");

	if(stat == TRUE){
		if(m_pWorkListWnd != NULL){
			m_pWorkListWnd->m_TEST_Item.InsertItem(1,"");
			m_pWorkListWnd->m_TEST_Item.SetItemText(1,0,"전류");
		}
	}
	for(int lop = 0;lop < 20;lop++){
		if(m_ModelInfo[lop].ID != 0){
			if(m_pWorkListWnd != NULL){
				if(stat == TRUE){
					m_pWorkListWnd->m_TEST_Item.InsertItem(lop+2,"");
					m_pWorkListWnd->m_TEST_Item.SetItemText(lop+2,0,m_ModelInfo[lop].NAME);
				}else{
					m_pWorkListWnd->m_TEST_Item.InsertItem(lop+1,"");
					m_pWorkListWnd->m_TEST_Item.SetItemText(lop+1,0,m_ModelInfo[lop].NAME);
				}
			}
		}

	}
}

void CImageTesterDlg::ModelSave()
{
	CString str_buf = "";
	CString str = "";
	for(int lop = 0;lop<20;lop++){
		str_buf.Empty();
		str_buf.Format("Model_%d",lop+1);
		WritePrivateProfileString(str_buf,NULL,"",modelfile_Name);
		if(m_ModelInfo[lop].ID != 0){
			str.Empty();
			str.Format("%d",m_ModelInfo[lop].ID);
			WritePrivateProfileString(str_buf,"ID",str,modelfile_Name);
			str.Empty();
			str.Format("%s",m_ModelInfo[lop].MODE);
			WritePrivateProfileString(str_buf,"MODE",str,modelfile_Name);
			str.Empty();
			str.Format("%s",m_ModelInfo[lop].NAME);
			WritePrivateProfileString(str_buf,"NAME",str,modelfile_Name);
			str.Empty();
			str.Format("%d",m_ModelInfo[lop].nMesIdx);
			WritePrivateProfileString(str_buf,"MESID",str,modelfile_Name);
			str.Empty();
			str.Format("%d",m_ModelInfo[lop].nMesNum);
			WritePrivateProfileString(str_buf,"MESNUM",str,modelfile_Name);
		}
	}
}


void CImageTesterDlg::ModelClear()
{
	if(m_pModOptTabWnd != NULL)
	m_pModOptTabWnd->DeleteAllItems();

	if(m_pModResTabWnd != NULL)
	m_pModResTabWnd->DeleteAllItems();
	m_pWorkListWnd->m_TEST_Item.DeleteAllItems();

	for(int lop = 0;lop < MAX_TEST_NUM;lop++)
		CEtcModel_Delete(lop);
	
	m_pWorkListWnd->m_TEST_Item.InsertItem(0,_T(""));
	m_pWorkListWnd->m_TEST_Item.SetItemText(0,0,_T("TOTAL"));
}

void CImageTesterDlg::ModelSearch(CString compdata)
{
	CFileFind  finder;
	CFileFind  finder2;
	int exnum = -1;
	int conum = -1;
	int ret = -1;
	CString example= _T("");

	CString	fname = _T("");
//	CString	strFile = _T("\\*.lurc"); 
	CString	strFile = _T("\\*.*"); 
	m_pMainCombo->ResetContent();//콤보를 초기화한다.
	if(m_pStandOptWnd != NULL)
		m_pStandOptWnd->pModelCombo->ResetContent();
	
	if(m_pModOptDlgWnd != NULL)
		m_pModOptDlgWnd->pModelCombo->ResetContent();
	
	if(m_pModSetDlgWnd != NULL)
		m_pModSetDlgWnd->pModelCombo->ResetContent();

	if(m_pNewLotWnd != NULL)
		m_pNewLotWnd->pModelCombo->ResetContent();
		
	BOOL bWorking = finder.FindFile(m_model_path +strFile);

	while(bWorking)
	{
		bWorking = finder.FindNextFile();
	//	if(!finder.IsDots() && !finder.IsDirectory())
		if(finder.IsDirectory())
		{
			fname.Empty();
		//	fname = finder.GetFileTitle();
			fname = finder.GetFileTitle();
			if(finder2.FindFile(m_model_path + _T("\\") + fname + _T("\\") + fname + _T(".luri")) == TRUE){
				m_pMainCombo->AddString(fname);
				if(m_pStandOptWnd != NULL)
					m_pStandOptWnd->pModelCombo->AddString(fname);
				
				if(m_pModOptDlgWnd != NULL)
					m_pModOptDlgWnd->pModelCombo->AddString(fname);
				
				if(m_pModSetDlgWnd != NULL)
					m_pModSetDlgWnd->pModelCombo->AddString(fname);

				if(m_pNewLotWnd != NULL)
					m_pNewLotWnd->pModelCombo->AddString(fname);
				
				exnum++;
				if(fname == compdata){
					conum = exnum;
				}
			}
		}
	}
	if((conum >= 0)&&(exnum>=conum))
		m_combo_sel = conum;
	else
		m_combo_sel = exnum;

	if(m_combo_sel != -1)
	{
		m_pMainCombo->SetCurSel(m_combo_sel);
		if(m_pStandOptWnd != NULL)
			m_pStandOptWnd->pModelCombo->SetCurSel(m_combo_sel);
		
		if(m_pModOptDlgWnd != NULL)
			m_pModOptDlgWnd->pModelCombo->SetCurSel(m_combo_sel);
		
		if(m_pModSetDlgWnd != NULL)
			m_pModSetDlgWnd->pModelCombo->SetCurSel(m_combo_sel);
		if(m_pNewLotWnd != NULL)
				m_pNewLotWnd->pModelCombo->SetCurSel(m_combo_sel);
		SetModelSet();
	}else
	{
		//모델이 하나도 없을때 기본 모델을 생성한다. 
		folder_gen(m_model_path);
		Overlay_path = m_model_path + _T("\\DEFAULT");
		folder_gen(Overlay_path);
		modelFolder_SetFile_path = Overlay_path + ("\\SET");
		folder_gen(modelFolder_SetFile_path);
		modelfile_Name = Overlay_path + _T("\\DEFAULT.luri");
		m_newmodelname = _T("DEFAULT");
		m_oldmodelname = _T("DEFAULT");

		m_pMainCombo->AddString(_T("DEFAULT"));
		example.Empty();
		example=_T("TOTAL");
		WritePrivateProfileString(example,NULL,_T(""),modelfile_Name);
		m_combo_sel = 0;
		m_pMainCombo->SetCurSel(m_combo_sel);

		if(m_pStandOptWnd != NULL)
		{
			m_pStandOptWnd->pModelCombo->AddString(_T("DEFAULT"));
			m_pStandOptWnd->pModelCombo->SetCurSel(m_combo_sel);
		}

		if(m_pModOptDlgWnd != NULL)
		{
			m_pModOptDlgWnd->pModelCombo->AddString(_T("DEFAULT"));
			m_pModOptDlgWnd->pModelCombo->SetCurSel(m_combo_sel);
		}

		if(m_pModSetDlgWnd != NULL)
		{
			m_pModSetDlgWnd->pModelCombo->AddString(_T("DEFAULT"));
			m_pModSetDlgWnd->pModelCombo->SetCurSel(m_combo_sel);
		}

		if(m_pNewLotWnd != NULL){
			m_pNewLotWnd->pModelCombo->AddString(_T("DEFAULT"));
			m_pNewLotWnd->pModelCombo->SetCurSel(m_combo_sel);
		}
	}
}

void CImageTesterDlg::SetModelSet()
{
	CString str = _T("");
	Start_oldmodelname ="";

	m_combo_sel = m_pMainCombo->GetCurSel();
	if(m_combo_sel < 0){
		return;
	}
	m_pMainCombo->GetLBText(m_pMainCombo->GetCurSel(),str);

	m_modelName = str;

	LOT_CREATE_Folder(str,2);
	//------
	if(str == (""))     
	{         
		return;     
	}
	WritePrivateProfileString(_T("INITPLACE"),_T("MODEL"),str,INITMODELFILE);//변경된 폴더를 ini에 저장한다. 

 	if(m_pResWorkerOptWnd != NULL){
 		m_pResWorkerOptWnd->Modelname_text(str);
 	}
	if(m_pResLotWnd != NULL){
 		m_pResLotWnd->Modelname_text(str);
 	}
	m_newmodelname = str;

	Overlay_path = m_model_path + _T("\\") + str;
	folder_gen(Overlay_path);
	modelfile_Name = Overlay_path +_T("\\")+str+_T(".luri");
	modelFolder_SetFile_path = Overlay_path + ("\\SET");
	folder_gen(modelFolder_SetFile_path);

	// MES 
	TCHAR szBuf[MAX_PATH] = {0, };
	GetPrivateProfileString(_T("MESINIT"),_T("ModelId"),_T(""),szBuf,MAX_PATH,modelfile_Name);
	m_str_ModelID.Format(_T("%s"), szBuf);
	//포고
	if(m_pOptPogoDlgWnd != NULL){
		m_pOptPogoDlgWnd->MainGroupSel();
		m_pOptPogoDlgWnd->InitUI();
	}

	if (m_pControlWnd != NULL)//광원
	{
		m_pControlWnd->Load_Parameter();
		if (b_luritechDebugMODE == 0){
			if (Power_Total_Control() == true){
				StatePrintf("Back Light On Success!");
			}
			else{
				StatePrintf("Back Light On Fail!");
			}
		}
	}

}
void CImageTesterDlg::OnCbnSelchangeModelName()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_combo_sel = m_pMainCombo->GetCurSel();
	if(m_pStandOptWnd != NULL){
		m_pStandOptWnd->pModelCombo->SetCurSel(m_combo_sel);
	}	
	if(m_pModOptDlgWnd != NULL){
		m_pModOptDlgWnd->pModelCombo->SetCurSel(m_combo_sel);
	}
	if(m_pModSetDlgWnd != NULL){
		m_pModSetDlgWnd->pModelCombo->SetCurSel(m_combo_sel);
	}

	SetModelSet();
	ModelSet(); //luri 파일을 읽어 각 모델이 무엇인지 파악한다.
	ModelGen(); //파악된 모델을 외부에 표시한다.
}


void CImageTesterDlg::Pogo_Load_Prm(){
	CString str ="";
	i_PogoPinSET = GetPrivateProfileInt("COUNT","PogoSetting",-1,PogoSelect_path);
	if(i_PogoPinSET == -1){
		i_PogoPinSET =5000;
		WritePrivateProfileString("COUNT","PogoSetting","5000",PogoSelect_path);
	}

	i_PogoCnt = GetPrivateProfileInt("COUNT","PogoCnt",-1,PogoSelect_path);
	if(i_PogoCnt == -1){
		i_PogoCnt = 0;
		WritePrivateProfileString("COUNT","PogoCnt","0",PogoSelect_path);
	}
	m_pOptPogoDlgWnd->InitUI();
	SetCountUpdate();
}

void CImageTesterDlg::SetCountUpdate(void){
	CString str = "";
	str.Format("%d",i_PogoCnt);
	if(i_PogoPinSET <= i_PogoCnt){
		b_Pogostate = FALSE;
	}else{
		b_Pogostate = TRUE;
	}
	TestCountText(str);
	m_pOptPogoDlgWnd->SetCountUpdate();
}

void CImageTesterDlg::POGOGROUPNAME(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_POGONAME))->SetWindowText(lpcszString);
}

void CImageTesterDlg::TestCountText(LPCSTR lpcszString, ...)
{	
	if(b_Pogostate == TRUE){
		txcol_pg = RGB(86, 86, 86);
		bkcol_pg= RGB(255, 255, 255);
	}else if(b_Pogostate == FALSE){
		txcol_pg = RGB(255, 255, 255);
		bkcol_pg = RGB(201,0,0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_COUNT_TEST))->SetWindowText(lpcszString);
}


void CImageTesterDlg::SETTING_USER_Switching()
{
	if(b_UserMode == FALSE)
	{
		if(b_Chk_MESEn)
		{
			SetDlgItemText(IDC_BTN_USER_CHANGE,_T("MES 모드"));	
			((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(FALSE);
		}
		else
		{
			SetDlgItemText(IDC_BTN_USER_CHANGE,_T("작업자 모드"));	
			((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(FALSE);
		}

		if(b_Chk_MESEn == 1){
			if(m_pResLotWnd != NULL)
				m_pResLotWnd->ShowWindow(SW_HIDE);
			if(m_pResWorkerOptWnd != NULL)
				m_pResWorkerOptWnd->ShowWindow(SW_SHOW);
		}else{
			if(m_pResLotWnd != NULL){
				m_pResLotWnd->ShowWindow(SW_SHOW);
				m_pResLotWnd->LotBtn_Enable(1);
			}
			if(m_pResWorkerOptWnd != NULL)
				m_pResWorkerOptWnd->ShowWindow(SW_HIDE);
		}

		m_pMainCombo->ShowWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_ETC_SETTING))->EnableWindow(FALSE);
		((CButton *)GetDlgItem(IDC_BTN_TEST_SETTING))->EnableWindow(FALSE);
		((CButton *)GetDlgItem(IDC_BTN_STOP))->EnableWindow(FALSE);
		FullOptionEnable(0);
	}

	if(b_UserMode ==TRUE)
	{
		m_pMainCombo->ShowWindow(1);

		SetDlgItemText(IDC_BTN_USER_CHANGE,_T("관리자 모드"));
		((CButton *)GetDlgItem(IDC_BTN_ETC_SETTING))->EnableWindow(TRUE);
		((CButton *)GetDlgItem(IDC_BTN_TEST_SETTING))->EnableWindow(TRUE);
		((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(TRUE);
		((CButton *)GetDlgItem(IDC_BTN_STOP))->EnableWindow(FALSE);
		FullOptionEnable(1);
	}

	if(b_SecretOption == 1){	
		((CButton *)GetDlgItem(IDC_BUTTON1))->ShowWindow(TRUE);
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON1))->ShowWindow(FALSE);
	}

//	((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(1);

}


void CImageTesterDlg::FullOptionEnable(bool benable)
{
	for(int lop = 0;lop < MAX_TEST_NUM;lop++)
	{
		if(m_psubmodel[lop] != NULL)	
			m_psubmodel[lop]->EnableOption(benable);
	}

	if(m_pModSetDlgWnd != NULL)
	m_pModSetDlgWnd->TEST_Enable(benable);
}

void CImageTesterDlg::OnBnClickedBtnTestSetting()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
	if(!m_pModOptDlgWnd->IsWindowVisible()){
		ModelSet(); //luri 파일을 읽어 각 모델이 무엇인지 파악한다.
		ModelGen(); //파악된 모델을 외부에 표시한다.
		m_pModSetDlgWnd->ShowWindow(SW_HIDE);
		m_pModOptDlgWnd->ShowWindow(SW_SHOW);
		m_pModSelDlgWnd->ShowWindow(SW_SHOW);
		m_pStandOptWnd->ShowWindow(SW_SHOW);
		m_pModOptDlgWnd->MoveWindow(InitOptionRect);
	}else{
		m_pModOptDlgWnd->ShowWindow(SW_HIDE);
	}
	InitEVMS();
}

void CImageTesterDlg::OnBnClickedBtnEtcSetting()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
	if(!m_pModSetDlgWnd->IsWindowVisible())
	{
		ModelSet(); //luri 파일을 읽어 각 모델이 무엇인지 파악한다.
		ModelGen(); //파악된 모델을 외부에 표시한다.
		m_pModSetDlgWnd->ShowWindow(SW_SHOW);
		m_pModOptDlgWnd->ShowWindow(SW_HIDE);
		m_pModSetDlgWnd->MoveWindow(InitOptionRect2);
	}else
	{
		m_pModSetDlgWnd->ShowWindow(SW_HIDE);
		
	}
	InitEVMS();
}

void CImageTesterDlg::OnBnClickedBtnMasterSet()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_pMasterMonitor != NULL){
		if(!m_pMasterMonitor->IsWindowVisible()){
			b_masterBtn =1;
			m_pMasterMonitor->ShowWindow(SW_SHOW);
		}else{
			m_pMasterMonitor->ShowWindow(SW_HIDE);
			DoEvents(100);
			b_masterBtn =1;
			m_pMasterMonitor->ShowWindow(SW_SHOW);
		}
	}
	InitEVMS();
}

void CImageTesterDlg::OnBnClickedBtnMain()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	STATID = IDC_BTN_MAIN;
	m_pStandWnd->ShowWindow(SW_SHOW);
	m_pWorkListWnd->ShowWindow(SW_HIDE);
	ButtonInit();
	Focus_move_start();
}

void CImageTesterDlg::ButtonInit()
{
	((CButton *)GetDlgItem(IDC_BTN_MAIN))->EnableWindow(FALSE);
	((CButton *)GetDlgItem(IDC_BTN_WORK_BUTTON))->EnableWindow(FALSE);
	((CButton *)GetDlgItem(IDC_BTN_MAIN))->EnableWindow(TRUE);
	((CButton *)GetDlgItem(IDC_BTN_WORK_BUTTON))->EnableWindow(TRUE);

	if(b_UserMode ==TRUE)
	{
		((CButton *)GetDlgItem(IDC_BTN_TEST_SETTING))->EnableWindow(FALSE);
		((CButton *)GetDlgItem(IDC_BTN_TEST_SETTING))->EnableWindow(TRUE);
		((CButton *)GetDlgItem(IDC_BTN_ETC_SETTING))->EnableWindow(FALSE);
		((CButton *)GetDlgItem(IDC_BTN_ETC_SETTING))->EnableWindow(TRUE);
	}else
	{
		((CButton *)GetDlgItem(IDC_BTN_TEST_SETTING))->EnableWindow(FALSE);
		((CButton *)GetDlgItem(IDC_BTN_ETC_SETTING))->EnableWindow(FALSE);
	}

	InitEVMS();
}
void CImageTesterDlg::OnEnSetfocusEditPortname()
{
	AutoPortChk_Cam();

	Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CImageTesterDlg::OnEnSetfocusEditPortstate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(PortChk_Cam() == FALSE){
		Portopen_Cam();
	}else{
		PortClose_Cam();
	}
	Focus_move_start();
}

void CImageTesterDlg::OnEnSetfocusEditCamname()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
}

void CImageTesterDlg::OnEnSetfocusEditCamstate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
}

void CImageTesterDlg::OnEnSetfocusTestStatus()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
}

void CImageTesterDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}


void CImageTesterDlg::CREATE_Folder(LPCTSTR lpcszString, int saveNum, ...)
{
	TCHAR lpszBuf[256];
	CString str;
	CFileFind		folderfind;

	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);

	switch(saveNum)
	{
		case 0:
			if(!folderfind.FindFile(SAVE_FOLDERPATH))
				CreateDirectory(SAVE_FOLDERPATH, NULL);	// 폴더 생성
			break;

		case 1:
			DATE_FOLDERPATH.Format(_T("%s\\%s"), SAVE_FOLDERPATH, lpszBuf);

			if(!folderfind.FindFile(DATE_FOLDERPATH))
				CreateDirectory(DATE_FOLDERPATH, NULL);	// 폴더 생성
			break;

		case 2:
			MODEL_FOLDERPATH.Format(_T("%s\\%s"), DATE_FOLDERPATH, lpszBuf);

			if(!folderfind.FindFile(MODEL_FOLDERPATH))
				CreateDirectory(MODEL_FOLDERPATH, NULL);	// 폴더 생성
			break;
		case 6:
			LOT_FOLDERPATH.Format(_T("%s\\%s"), MODEL_FOLDERPATH, lpszBuf);

			if (!folderfind.FindFile(LOT_FOLDERPATH))
				CreateDirectory(LOT_FOLDERPATH, NULL);	// 폴더 생성
			break;
		case 3:
			if(!folderfind.FindFile(SAVE_FOLDERPATH_P))
				CreateDirectory(SAVE_FOLDERPATH_P, NULL);	// 폴더 생성
			break;
		case 4:		// LOT 디렉토리
			DATE_FOLDERPATH_P.Format(_T("%s\\%s"), SAVE_FOLDERPATH_P, lpszBuf);

			if(!folderfind.FindFile(DATE_FOLDERPATH_P))
				CreateDirectory(DATE_FOLDERPATH_P, NULL);	// 폴더 생성
			break;
		case 5:
			MODEL_FOLDERPATH_P.Format(_T("%s\\%s"), DATE_FOLDERPATH_P, lpszBuf);

			if(!folderfind.FindFile(MODEL_FOLDERPATH_P))
				CreateDirectory(MODEL_FOLDERPATH_P, NULL);	// 폴더 생성
			break;
	}
}

void CImageTesterDlg::ALL_WORKLIST_UPLOAD(){

	MainWork_List.DeleteAllItems();
	if(EXCEL_UPLOAD("TOTAL_TEST",&MainWork_List,0,&StartCnt)== TRUE){
		
	}
	else{
		StartCnt =0;
	}

	for(int lop=0; lop<20; lop++){
		if(m_psubmodel[lop] != NULL){
			if(m_psubmodel[lop]->Excel_Upload() ==TRUE){
				
			}
			
		}else{
			break;
		}
	}
}

void CImageTesterDlg::SETTING_LotInfo(){
		CString str="";
		
		str.Empty();
		str.Format("%d",Lot_Totalint);
	//	m_pStandWnd->TOTAL_NUM(str);///////////////////////
		if(m_pResLotWnd != NULL)
			m_pResLotWnd->TESTNUM_VAL(str);
		if(Lot_Totalint !=0){
			Lot_Percentint = ((double)Lot_Passint / (double)Lot_Totalint)*100.0;
		}
		else{Lot_Percentint=0;}
		//str.Empty();
		//str.Format("%6.2f%%",Lot_Percentint);
		//m_pStandWnd->PERCENT_NUM(str);//////////////////
		//str.Empty();
		//str.Format("%d",Lot_Passint);
		//m_pStandWnd->PASS_NUM(str);/////////////////////////
		//str.Empty();
		//str.Format("%d",Lot_Failint);
		//m_pStandWnd->FAIL_NUM(str);//////////////////////
		
		

}

void CImageTesterDlg::LOT_RESET_LIST(int state){
	
	CString str="";
	if(state ==0){/////////////////LOT 시작할 때 List 초기화
		Lot_Totalint=0;
		Lot_Passint=0;
		Lot_Failint=0;
		Lot_Percentint=0;/////////////////////////////////////////130410
		Lot_StartCnt=0;

		m_Lot_MainWorkList.DeleteAllItems();
		for(int lop=0; lop<20; lop++){/////////////////////////////////////////////0723
			if(m_psubmodel[lop] != NULL){
				m_psubmodel[lop]->Lot_initCnt();
				m_psubmodel[lop]->DeleteITEM();
			}else{
				break;
			}
		}

		SETTING_LotInfo();
		
	}
	
	if(state == 1){/////////////////LOT 시작할 때기존 LOT가 있을 때  List 초기화
		m_Lot_MainWorkList.DeleteAllItems();
		
		Lot_Totalint=GetPrivateProfileInt("LOT_RESULT","TOTAL",-1,modelLotPath);
		Lot_Passint=GetPrivateProfileInt("LOT_RESULT","OK",-1,modelLotPath);
		Lot_Failint=GetPrivateProfileInt("LOT_RESULT","FAIL",-1,modelLotPath);
		Lot_Percentint=GetPrivateProfileDouble("LOT_RESULT","PERCENT",-1,modelLotPath);		

		if(Lot_Totalint != 0){
			if(LOT_EXCEL_UPLOAD("TOTAL_TEST",&m_Lot_MainWorkList,0,&Lot_StartCnt)== TRUE){
				for(int lop=0; lop<20; lop++){
					if(m_psubmodel[lop] != NULL){
						if(m_psubmodel[lop]->LOT_Excel_Upload() ==TRUE){
							
						}
						else{
							Lot_Totalint=0;
							Lot_Passint=0;
							Lot_Failint=0;
							Lot_Percentint=0;
							Lot_StartCnt=0;
							SETTING_LotInfo();
							LOT_RESET_LIST(0);
							break;
						}
					}else{
						break;
					}
				}
			}
			else{
				Lot_Totalint=0;
				Lot_Passint=0;
				Lot_Failint=0;
				Lot_Percentint=0;
				Lot_StartCnt=0;
				LOT_RESET_LIST(0);
				SETTING_LotInfo();
			}
		}
		else{
			Lot_Totalint=0;
			Lot_Passint=0;
			Lot_Failint=0;
			Lot_Percentint=0;
			Lot_StartCnt=0;
			LOT_RESET_LIST(0);
			SETTING_LotInfo();
		}

		str.Empty();
		str.Format("%d",Lot_Passint);
		WritePrivateProfileString("LOT_RESULT","OK",str,modelLotPath);
		str.Empty();
		str.Format("%d",Lot_Failint);
		WritePrivateProfileString("LOT_RESULT","FAIL",str,modelLotPath);
		str.Empty();
		str.Format("%d",Lot_Totalint);
		WritePrivateProfileString("LOT_RESULT","TOTAL",str,modelLotPath);
		str.Empty();
		str.Format("%6.2f",Lot_Percentint);
		WritePrivateProfileString("LOT_RESULT","PERCENT",str,modelLotPath);
		SETTING_LotInfo();
	}
}
//---------------------------------------------------------------------WORKLIST
void CImageTesterDlg::FileCopy_AnotherPath_Func(){

	CFileFind		folderfind;
	CString path = "D:\\BackUpReport";
	bool b_SearchFolder = TRUE;
	CString SearchPath = "";
	int count =0;


	folder_gen(path);//path+"\\"+SaveData_Date


	if(old_DATE_FOLDERPATH == ""){ 	/*처음 프로그램을 틀었는데도 폴더가 존재하는경우 폴더 서치 후 저장해줘야한다.*/
		if(!folderfind.FindFile(path+"\\"+SaveData_Date)){
			FileFolder_Copy(DATE_FOLDERPATH,path+"\\"+SaveData_Date);
			SAME_DATE_COPYPATH = path+"\\"+SaveData_Date;
		}else{
			while(b_SearchFolder){
				SearchPath.Format(path+"\\"+SaveData_Date+"_%d",count+1);
				if(!folderfind.FindFile(SearchPath)){
					SAME_DATE_COPYPATH = SearchPath;
					FileFolder_Copy(DATE_FOLDERPATH,SearchPath);
					b_SearchFolder = FALSE;
					break;
				}else{				
					count++;
				}
			}
		}	
	}
	else if(DATE_FOLDERPATH == old_DATE_FOLDERPATH){                /////////////////////이전 저장경로와 지금 경로가 같을 때 같은 폴더에 저장해야한다.
		
		FileFolder_Copy(DATE_FOLDERPATH,SAME_DATE_COPYPATH);
	}
	else{                                                       //경로가 다를 때는 폴더를 다시 생성하여 저장해야한다.
		if(!folderfind.FindFile(path+"\\"+SaveData_Date)){
			FileFolder_Copy(DATE_FOLDERPATH,path+"\\"+SaveData_Date);
			SAME_DATE_COPYPATH = path+"\\"+SaveData_Date;
		}else{
			while(b_SearchFolder){
				SearchPath.Format(path+"\\"+SaveData_Date+"_%d",count+1);
				if(!folderfind.FindFile(SearchPath)){
					SAME_DATE_COPYPATH = SearchPath;
					FileFolder_Copy(DATE_FOLDERPATH,SearchPath);
					b_SearchFolder = FALSE;
					break;
				}else{				
					count++;
				}
			}
		}	
	}

	old_DATE_FOLDERPATH = DATE_FOLDERPATH;

	
}
void CImageTesterDlg::SAVE_FILE(){
	
	CFileFind filefind;

	CString str="";
	CString Data="";
	

	Data.Empty();
	str.Empty();
	str= "********************TOTAL TEST***************************\n";
	Data += str;
	str.Empty();
	str.Format("\n\n\n");
	Data += str;
	str.Empty();
	str= "*****************************************************\n";
	Data += str;
	
	SAVE_EXCEL_TOTAL("TOTAL_TEST", TESTNAME,( TEST_ITEM_NUM - WORKLIST_MODELSTART)+1, &MainWork_List, Data);
	Data.Empty();
}
void CImageTesterDlg ::InsertList_C(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	
	InsertIndex_C = m_CurrList.InsertItem(StartCnt_C,"",0);
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex_C+1);
	m_CurrList.SetItemText(InsertIndex_C,0,strCnt);
	
	m_CurrList.SetItemText(InsertIndex_C,1,m_modelName);
	m_CurrList.SetItemText(InsertIndex_C,2,strDay);
	m_CurrList.SetItemText(InsertIndex_C,3,strTime);
}
void CImageTesterDlg ::Set_List_C(CListCtrl *List){
	

	while(List->DeleteColumn(0));
	if(m_pWorkList != NULL)
	m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"1.8V",LVCFMT_CENTER, 80);
	List->InsertColumn(5, "3.3V", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "9.0V", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "14.7V", LVCFMT_CENTER, 80);
	List->InsertColumn(8, "-5.7V", LVCFMT_CENTER, 80);
	List->InsertColumn(9,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(10,"비고",LVCFMT_CENTER, 80);
	//List->InsertColumn(12,"Serial",LVCFMT_CENTER, 80);
	ListItemNum_C=11;
	Copy_List(&m_CurrList,m_pWorkList,ListItemNum_C);
}

CString CImageTesterDlg ::Mes_Result_C()
{
	CString sz;

	m_szMesResult = _T("");

	m_szMesResult.Format(_T("%s:%d, %s:%d, %s:%d, %s:%d, %s:%d"), m_stMes[0].szData, m_stMes[0].nResult, 
																	m_stMes[1].szData, m_stMes[1].nResult,
																	m_stMes[2].szData, m_stMes[2].nResult,
																	m_stMes[3].szData, m_stMes[3].nResult,
																	m_stMes[4].szData, m_stMes[4].nResult);


	m_stNewMesData[NewMES_Current] = m_szMesResult;
	return m_szMesResult;
}

void CImageTesterDlg ::FAIL_UPLOAD(){
	if (b_StartCommand == 1){
		InsertList_C();
		m_CurrList.SetItemText(InsertIndex_C, 4, "X");
		m_CurrList.SetItemText(InsertIndex_C, 5, "X");
		m_CurrList.SetItemText(InsertIndex_C, 6, "X");
		m_CurrList.SetItemText(InsertIndex_C, 7, "X");
		m_CurrList.SetItemText(InsertIndex_C, 8, "X");
		m_CurrList.SetItemText(InsertIndex_C, 9, "FAIL");
		m_CurrList.SetItemText(InsertIndex_C, 10, "STOP");
		MainWork_List.SetItemText(InsertIndex,6, "STOP");
		StartCnt_C++;
	}

}

void CImageTesterDlg ::EXCEL_SAVE_C(){
	CString Item[5] = { "1.8V", "3.3V", "9.0V", "14.7", "M5V7V" };
	CString Data = "";
	CString str = "";
	str = "***********************전류 검사***********************\n";
	Data += str;
	str.Empty();
	str.Format("CH 1 : 최소 mA , %d ,최대 mA,  %d, \n", m_MinAmp, m_MaxAmp);
	Data += str;
	str.Empty();
	str.Format("CH 2 :최소 mA , %d ,최대 mA,  %d, CH 3 :최소 mA , %d ,최대 mA,  %d, \n", m_MinAmp2, m_MaxAmp2, m_MinAmp3, m_MaxAmp3);
	Data += str;
	str.Empty();
	str.Format("CH 4 :최소 mA , %d ,최대 mA,  %d, CH 5 :최소 mA , %d ,최대 mA,  %d, \n", m_MinAmp4, m_MaxAmp4, m_MinAmp5, m_MaxAmp5);
	Data += str;
	str = "*************************************************\n";
	Data += str;
	SAVE_EXCEL("동작전류",Item,5,&m_CurrList,Data);
}

bool CImageTesterDlg ::EXCEL_UPLOAD_C(){
	m_CurrList.DeleteAllItems();
	if(EXCEL_UPLOAD("동작전류",&m_CurrList,1,&StartCnt_C)==TRUE){
		return TRUE;	
	}
	else{
		StartCnt_C = 0;
		return FALSE;	
	}
}
void CImageTesterDlg::EmptyList_Input_X_Data(CListCtrl *List){

	CString str="";
	for(int i =0; i<ListItemNum; i++){
		str= List->GetItemText(List->GetItemCount()-1,i);
		if(str != ""){
			
		}else{
			List->SetItemText(List->GetItemCount()-1,i,"X");
		}
	
	}
}

void CImageTesterDlg ::Set_List(CListCtrl *List){
	int count=0;
	
	while(List->DeleteColumn(0));

	if(m_pWorkList != NULL)
	m_pWorkList->DeleteAllItems();
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4, "End TIME", LVCFMT_CENTER, 80);

	List->InsertColumn(WORKLIST_MODELSTART,		"MODEL START",		LVCFMT_CENTER, 80);
	List->InsertColumn(WORKLIST_CURRENT,		"동작전류",			LVCFMT_CENTER, 80);
	List->InsertColumn(WORKLIST_EEPROMCHK,		"EEPROM",			LVCFMT_CENTER, 80);
 	List->InsertColumn(WORKLIST_CENTER,			"광축측정",			LVCFMT_CENTER, 80);
	List->InsertColumn(WORKLIST_ROTATE,			"Rotate 검사",		LVCFMT_CENTER, 80);
	List->InsertColumn(WORKLIST_SFR,			"SFR",				LVCFMT_CENTER, 80);
	List->InsertColumn(WORKLIST_ANGLE,			"화각검사",			LVCFMT_CENTER, 80);
	List->InsertColumn(WORKLIST_DISTORTION,		"Distortion",		LVCFMT_CENTER, 80);
	List->InsertColumn(WORKLIST_3D_DEPTH,		"3D_DEPTH",			LVCFMT_CENTER, 80);
	List->InsertColumn(WORKLIST_DUST,			"이물 검사",		LVCFMT_CENTER, 80);
	List->InsertColumn(WORKLIST_BRIGHTNESS,		"광량비 검사",		LVCFMT_CENTER, 80);
	List->InsertColumn(WORKLIST_DYNAMICRANGE,	"Dynamic Range",	LVCFMT_CENTER, 80);
	List->InsertColumn(WORKLIST_FIXED_PATTERN,	"Fixed Pattern",	LVCFMT_CENTER, 80);
	List->InsertColumn(WORKLIST_HOTPIXEL,		"Hot Pixel",		LVCFMT_CENTER, 80);
	List->InsertColumn(WORKLIST_DEPTHNOISE,		"Depth Noise",		LVCFMT_CENTER, 80);
	List->InsertColumn(TEST_ITEM_NUM,			"MODEL END",		LVCFMT_CENTER, 80);
	List->InsertColumn(TEST_ITEM_NUM + 1,		"RESULT",			LVCFMT_CENTER, 80);
	List->InsertColumn(TEST_ITEM_NUM + 2,		"비고",				LVCFMT_CENTER, 80);

	ListItemNum = TEST_ITEM_NUM + 3;
}

void CImageTesterDlg ::SAVE_EXCEL(LPCSTR lpcszString,CString Item[],int ItemNum,CListCtrl *List, CString Data, ...){///////////////////////////////130321_bin
	
	CStdioFile File;
	CString strFilename ="";
	CString strOut ="";
	CFileFind filefind;

	CString str="";int length=0;
	TCHAR			lpszBuf[256];

	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);

//	strFilename = LOT_FOLDERPATH + ("\\") + ("%s", lpszBuf) + (".csv");
	strFilename = MODEL_FOLDERPATH + ("\\") + ("%s", lpszBuf) + (".csv");
	CFileException e;

	
	bool FLAG = TRUE;

	if(!File.Open((LPCTSTR)strFilename,CFile::modeCreate|CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
		CString strFilename2 = MODEL_FOLDERPATH + ("\\") + ("%s", lpszBuf) + ("_bak.csv");
		if(!File.Open((LPCTSTR)strFilename2,CFile::modeCreate|CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
			return;
			
		}
	}
	
	strOut ="";

	strOut+=Data;// 상세 옵션 설정 값들 5줄
	
	str.Empty();
	str= L"NO ,";
	strOut+= str;
	str.Empty();
	str=L"모델명 ,";
	strOut+= str;
	str.Empty();
	str=L"Start DATE ,";
	strOut+= str;
	str.Empty();
	str=L"Start TIME ,";
	strOut+= str;
	
	for(int t=0; t<ItemNum;t++){
		if(t != ItemNum){
			str.Empty();
			str= Item[t]+"," ;
			strOut+= str;
		}
	}
	
	str.Empty();
	str=L"Result,";
	strOut+= str;
	str.Empty();
	str=L"비고 \n";
	strOut+= str;
	/*str.Empty();
	str=L"Serial \n";
	strOut+= str;*/
	
	for (int t = 0; t < List->GetItemCount(); t++){
		for (int i = 0; i < (ItemNum + 6); i++){
			str.Empty();
			str.Format(List->GetItemText(t, i) + ",");
			strOut += str;
			if (i == (ItemNum + 5)){
				strOut += "\n";
			}
		}

	}


	File.WriteString(LPCTSTR(strOut));

	File.Close();
	

}
void CImageTesterDlg::SAVE_EXCEL_TOTAL(LPCSTR lpcszString, CString Item[], int ItemNum, CListCtrl *List, CString Data, ...){///////////////////////////////130321_bin

	CStdioFile File;
	CString strFilename = "";
	CString strOut = "";
	CFileFind filefind;

	CString str = ""; int length = 0;
	TCHAR			lpszBuf[256];

	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;
	va_start(args, lpcszString);
	wvsprintf(lpszBuf, lpcszString, args);

	//strFilename = LOT_FOLDERPATH + ("\\") + ("%s", lpszBuf) + (".csv");
	strFilename = MODEL_FOLDERPATH + ("\\") + ("%s", lpszBuf) + (".csv");
	CFileException e;


	bool FLAG = TRUE;

	if (!File.Open((LPCTSTR)strFilename, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
		CString strFilename2 = MODEL_FOLDERPATH + ("\\") + ("%s", lpszBuf) + ("_bak.csv");
		if (!File.Open((LPCTSTR)strFilename2, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
			return;

		}
	}

	strOut = "";

	strOut += Data;// 상세 옵션 설정 값들 5줄

	str.Empty();
	str = L"NO ,";
	strOut += str;
	str.Empty();
	str = L"모델명 ,";
	strOut += str;
	str.Empty();
	str = L"Start DATE ,";
	strOut += str;
	str.Empty();
	str = L"Start TIME ,";
	strOut += str;
	str.Empty();
	str = L"End TIME ,";
	strOut += str;

	for (int t = 0; t<ItemNum; t++){
		if (t != ItemNum){
			str.Empty();
			str = Item[t] + ",";
			strOut += str;
		}
	}

	str.Empty();
	str = L"Result,";
	strOut += str;
	str.Empty();
	str = L"비고 \n";
	strOut += str;

	for (int t = 0; t < List->GetItemCount(); t++){
		for (int i = 0; i < (ItemNum + 7); i++){
			str.Empty();
			str.Format(List->GetItemText(t, i) + ",");
			strOut += str;
			if (i == (ItemNum + 6)){
				strOut += "\n";
			}
		}

	}


	File.WriteString(LPCTSTR(strOut));

	File.Close();


}

bool CImageTesterDlg::EXCEL_UPLOAD(LPCSTR lpcszString, CListCtrl *List,int state, int *NUM, ...)
{///////////////////////////////130408
	CStdioFile File;
	
	CString filePath;  //파일 경로 저장
	CString tmp="";        // 한줄씩 읽은 문자열 저장
	CString pb;         // 문자열에서 잘라낸 로그값 저장
	CString str;       // 문자열에서 잘라낸 시간값 저장
	CString strFilename ="";
	TCHAR			lpszBuf[256];

	int count=0;
	

	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);

	strFilename = MODEL_FOLDERPATH + ("\\") + ("%s", lpszBuf) + (".csv");
//	strFilename = LOT_FOLDERPATH + ("\\") + ("%s", lpszBuf) + (".csv");

	CFileException e;
	if(!File.Open((LPCTSTR)strFilename,CFile::modeRead |CFile::shareDenyNone,&e)){
		//AfxMessageBox("해당 csv파일을 찾을 수 없습니다. 처음부터 작업시작합니다.");
		return FALSE;
	}
	File.SeekToBegin();
	while(File.ReadString(tmp))
	{		
		AfxExtractSubString(str,tmp,count-1,',');
		if(count >5){	/////////////////////////////////워크리스트에 보여질 부분 Update
		 
			  List->InsertItem(count-6, str); 
			  for(int t=0; t<200; t++){
				AfxExtractSubString(pb, tmp,t,',');
				List->SetItemText(count-6, t, pb);
			  }
		}
		count++;/////////////////////////////////저장되었던 csv에 토탈 라인 수..

	}
	
	*NUM = count-6;////////////////////////////////저장되었던 csv에 토탈 라인 수 - 6(옵션들)

	File.Close();

	return TRUE;
}


void CImageTesterDlg::LOT_CREATE_Folder(LPCTSTR lpcszString, int saveNum, ...)
{
	TCHAR lpszBuf[256];
	CString str;
	CFileFind		folderfind;

	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);

	switch(saveNum)
	{
	case 0:
		if(!folderfind.FindFile(LOT_SAVE_FOLDERPATH))
			CreateDirectory(LOT_SAVE_FOLDERPATH, NULL);	// 폴더 생성
		break;

	case 1:
		LOT_DATE_FOLDERPATH.Format(_T("%s\\%s"), LOT_SAVE_FOLDERPATH, lpszBuf);

		if(!folderfind.FindFile(LOT_DATE_FOLDERPATH))
			CreateDirectory(LOT_DATE_FOLDERPATH, NULL);	// 폴더 생성
		break;

	case 2:
		LOT_MODEL_FOLDERPATH.Format(_T("%s\\%s"), LOT_DATE_FOLDERPATH, lpszBuf);

		if(!folderfind.FindFile(LOT_MODEL_FOLDERPATH))
			CreateDirectory(LOT_MODEL_FOLDERPATH, NULL);	// 폴더 생성
		break;

	case 3:		// LOT 디렉토리

		LOT_FOLDERPATH = LOT_MODEL_FOLDERPATH + _T(("\\") +(_T("%s"),lpszBuf));
		modelLotPath = LOT_FOLDERPATH+ _T(("\\") +(_T("%s"),lpszBuf) +_T("_LOG.ini"));
		if(!folderfind.FindFile(LOT_FOLDERPATH))
		{
			CreateDirectory(LOT_FOLDERPATH, NULL);	// 폴더 생성
			Lot_StartCnt=0;
			Lot_Totalint=0;///////////////////////////////////////VIN
			Lot_Passint=0;
			Lot_Failint=0;
			Lot_Percentint=0;///////////////////////////////////////
			LOT_RESET_LIST(0);
			SETTING_LotInfo();
		}
		else{
			CExistLOTDlg_Create();
		}

		break;
	}
}


void CImageTesterDlg::NewLotCreate(){
	
	

	m_pNewLotWnd = new CNewLot(this);
	m_pNewLotWnd->Setup(this);
	int skip =0;
	if(m_pNewLotWnd->DoModal() == IDOK){
		if(m_pResLotWnd != NULL){
			m_pResLotWnd->LotBtn_Enable(0);
		}
		((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BTN_STOP))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_USER_CHANGE))->EnableWindow(0);
		m_pMainCombo->EnableWindow(0);
		Focus_move_start();	

		if(Start_oldmodelname == m_modelName){
			skip =1;
		}
	
		if(skip == 0){
			if(m_pMasterMonitor != NULL){
				CPopUP2 subDlg;
				subDlg.Warning_TEXT ="모델이 변경되었습니다\r\n\r\nMaster 카메라 SETTING 해주세요\r\n";
				if(subDlg.DoModal() == IDOK){
					OnBnClickedBtnMasterSet();
					b_MasterDlg = TRUE;
					while(b_MasterDlg){
						DoEvents(100);
					}
					Start_oldmodelname =m_modelName;

				}else{
					Start_oldmodelname ="";
				}
			}
		}
		

		
	}
	delete m_pNewLotWnd;
	m_pNewLotWnd = NULL;
	
}

void CImageTesterDlg::CExistLOTDlg_Create(){

	m_ExistLotWnd = new CExistLot(this);

	m_ExistLotWnd->Setup(this);
	if(m_ExistLotWnd->DoModal() == IDOK){
	}
	else{
		DeleteDir(LOT_FOLDERPATH);
		CREATE_Folder(m_pNewLotWnd->m_LotInfo,6);
	}
	delete m_ExistLotWnd;
	m_ExistLotWnd = NULL;
}



void CImageTesterDlg ::LOT_SAVE_EXCEL(LPCSTR lpcszString,CString Item[],int ItemNum,CListCtrl *List, CString Data, ...){///////////////////////////////130321_bin
	
	CStdioFile File;
	CString strFilename ="";
	CString strOut ="";
	CFileFind filefind;

	CString str="";int length=0;
	TCHAR			lpszBuf[256];

	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);

	strFilename = LOT_FOLDERPATH +("\\") + ("%s",lpszBuf)+(".csv");

	CFileException e;

	
	bool FLAG = TRUE;

	if(!File.Open((LPCTSTR)strFilename,CFile::modeCreate|CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
		CString strFilename2 = LOT_FOLDERPATH +("\\") + ("%s",lpszBuf)+("_bak.csv");
		if(!File.Open((LPCTSTR)strFilename2,CFile::modeCreate|CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
			return;
			
		}
	}
	
	strOut ="";

	strOut+=Data;// 상세 옵션 설정 값들 5줄
	
	str.Empty();
	str= L"NO ,";
	strOut+= str;
	str.Empty();
	str=L"모델명 ,";
	strOut+= str;
	str.Empty();
	str=L"LOT명 ,";
	strOut+= str;
	str.Empty();
	str=L"Start DATE ,";
	strOut+= str;
	str.Empty();
	str=L"Start TIME ,";
	strOut+= str;
	
	for(int t=0; t<ItemNum;t++){
		if(t != ItemNum){
			str.Empty();
			str= Item[t]+"," ;
			strOut+= str;
		}
	}
	
	str.Empty();
	str=L"Result,";
	strOut+= str;
	str.Empty();
	str=L"비고 \n";
	strOut+= str;
	/*str.Empty();
	str=L"Serial \n";
	strOut+= str;*/
	
	for(int t =0; t<List->GetItemCount(); t++){
		for(int i =0; i<(ItemNum+7); i++){
			str.Empty();
			str.Format(List->GetItemText(t,i)+",");
			strOut+= str; 
			if(i==(ItemNum+6)){
				strOut+= "\n";
			}
		}
	
	}
	File.WriteString(LPCTSTR(strOut));

	File.Close();
	

}
void CImageTesterDlg::LOT_SAVE_EXCEL_TOTAL(LPCSTR lpcszString, CString Item[], int ItemNum, CListCtrl *List, CString Data, ...){///////////////////////////////130321_bin

	CStdioFile File;
	CString strFilename = "";
	CString strOut = "";
	CFileFind filefind;

	CString str = ""; int length = 0;
	TCHAR			lpszBuf[256];

	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;
	va_start(args, lpcszString);
	wvsprintf(lpszBuf, lpcszString, args);

	strFilename = LOT_FOLDERPATH + ("\\") + ("%s", lpszBuf) + (".csv");

	CFileException e;


	bool FLAG = TRUE;

	if (!File.Open((LPCTSTR)strFilename, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
		CString strFilename2 = LOT_FOLDERPATH + ("\\") + ("%s", lpszBuf) + ("_bak.csv");
		if (!File.Open((LPCTSTR)strFilename2, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
			return;

		}
	}

	strOut = "";

	strOut += Data;// 상세 옵션 설정 값들 5줄

	str.Empty();
	str = L"NO ,";
	strOut += str;
	str.Empty();
	str = L"모델명 ,";
	strOut += str;
	str.Empty();
	str = L"LOT명 ,";
	strOut += str;
	str.Empty();
	str = L"Start DATE ,";
	strOut += str;
	str.Empty();
	str = L"Start TIME ,";
	strOut += str;
	str.Empty();
	str = L"End TIME ,";
	strOut += str;

	for (int t = 0; t<ItemNum; t++){
		if (t != ItemNum){
			str.Empty();
			str = Item[t] + ",";
			strOut += str;
		}
	}

	str.Empty();
	str = L"Result,";
	strOut += str;
	str.Empty();
	str = L"비고 \n";
	strOut += str;
	/*str.Empty();
	str=L"Serial \n";
	strOut+= str;*/

	for (int t = 0; t<List->GetItemCount(); t++){
		for (int i = 0; i<(ItemNum + 8); i++){
			str.Empty();
			str.Format(List->GetItemText(t, i) + ",");
			strOut += str;
			if (i == (ItemNum + 7)){
				strOut += "\n";
			}
		}

	}
	File.WriteString(LPCTSTR(strOut));

	File.Close();


}
bool CImageTesterDlg ::LOT_EXCEL_UPLOAD(LPCSTR lpcszString, CListCtrl *List,int state, int *NUM, ...){///////////////////////////////130408
	CStdioFile File;
	
	CString filePath;  //파일 경로 저장
	CString tmp="";        // 한줄씩 읽은 문자열 저장
	CString pb;         // 문자열에서 잘라낸 로그값 저장
	CString str;       // 문자열에서 잘라낸 시간값 저장
	CString strFilename ="";
	TCHAR			lpszBuf[256];

	int count=0;
	

	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);


	strFilename = LOT_FOLDERPATH +("\\") + ("%s",lpszBuf)+(".csv");

	CFileException e;
	if(!File.Open((LPCTSTR)strFilename,CFile::modeRead |CFile::shareDenyNone,&e)){
		//AfxMessageBox("해당 csv파일을 찾을 수 없습니다. 처음부터 작업시작합니다.");
		return FALSE;
	}
	File.SeekToBegin();
	while(File.ReadString(tmp))
	{		
		AfxExtractSubString(str,tmp,count-1,',');
		if(count >5){	/////////////////////////////////워크리스트에 보여질 부분 Update
		 
			  List->InsertItem(count-6, str); 
			  for(int t=0; t<50; t++){
				AfxExtractSubString(pb, tmp,t,',');
				List->SetItemText(count-6, t, pb);
			  }
		}
		count++;/////////////////////////////////저장되었던 csv에 토탈 라인 수..

	}
	*NUM = count-6;////////////////////////////////저장되었던 csv에 토탈 라인 수 - 6(옵션들)
	File.Close();
	return TRUE;
}

void CImageTesterDlg ::LOT_Set_List(CListCtrl *List){
	int count=0;

	while(List->DeleteColumn(0));
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT CARD",LVCFMT_CENTER, 100);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(5, "End TIME", LVCFMT_CENTER, 80);
	TestItemNum =0;
	for(int lop=0; lop<20; lop++){
		
		if(m_psubmodel[lop] != NULL){
			List->InsertColumn(6+lop,m_psubmodel[lop]->m_INFO.NAME,LVCFMT_CENTER, 80);
			TestModelName[lop]=m_psubmodel[lop]->m_INFO.NAME;
			count=6+lop;
			TestItemNum++;
		}
		else{break;}
		
	}
	List->InsertColumn(count+1,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(count+2,"작업자",LVCFMT_CENTER, 80);
	List->InsertColumn(count+3,"비고",LVCFMT_CENTER, 80);
//	List->InsertColumn(count+4,"CH",LVCFMT_CENTER, 80);
	List->DeleteAllItems();
	Lot_InsertNum=count+4;
}



void CImageTesterDlg ::LOT_EXCEL_SAVE(){
	CString Data,str;
	Data.Empty();
	str.Empty();
	str= "********************TOTAL TEST***************************\n";
	Data += str;
	str.Empty();
	str.Format("TEST 항목 수 , %d \n총 TEST 수, %d, 합격, %d, 실패, %d, 수율, %6.2f%%\n",TestItemNum-2,Lot_Totalint,Lot_Passint,Lot_Failint,Lot_Percentint);
	Data += str;
	str.Empty();
	str= "*****************************************************\n\n";
	Data += str;
		
	LOT_SAVE_EXCEL_TOTAL("TOTAL_TEST",TestModelName,TestItemNum,&m_Lot_MainWorkList,Data);

	str.Empty();
	str.Format("%d",Lot_Passint);
	WritePrivateProfileString("LOT_RESULT","OK",str,modelLotPath);
	str.Empty();
	str.Format("%d",Lot_Failint);
	WritePrivateProfileString("LOT_RESULT","FAIL",str,modelLotPath);
	str.Empty();
	str.Format("%d",Lot_Totalint);
	WritePrivateProfileString("LOT_RESULT","TOTAL",str,modelLotPath);
	str.Empty();
	str.Format("%6.2f",Lot_Percentint);
	WritePrivateProfileString("LOT_RESULT","PERCENT",str,modelLotPath);
}

bool CImageTesterDlg ::LOT_EXCEL_UPLOAD(){
	m_Lot_MainWorkList.DeleteAllItems();
	if(LOT_EXCEL_UPLOAD("TOTAL_TEST",&m_Lot_MainWorkList,1,&Lot_StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}


void CImageTesterDlg ::LOT_Set_List_C(CListCtrl *List){

	while(List->DeleteColumn(0));
	m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT 명",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(5, "1.8V", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "3.3V", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "9.0V", LVCFMT_CENTER, 80);
	List->InsertColumn(8, "14.7V", LVCFMT_CENTER, 80);
	List->InsertColumn(9, "M5V7V", LVCFMT_CENTER, 80);
	List->InsertColumn(10,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(11,"비고",LVCFMT_CENTER, 80);
	Lot_InsertNum_C =12;
	Copy_List(&m_Lot_CurrentList, m_pWorkList, Lot_InsertNum_C);
	
}

void CImageTesterDlg ::LOT_InsertDataList_C(){
	CString strCnt = "";

	int Index = m_Lot_CurrentList.InsertItem(Lot_StartCnt_C, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index + 1);
	m_Lot_CurrentList.SetItemText(Index, 0, strCnt);

	int CopyIndex = m_CurrList.GetItemCount() - 1;

	int count = 0;
	for (int t = 0; t < Lot_InsertNum_C; t++){
		if ((t != 2) && (t != 0)){
			m_Lot_CurrentList.SetItemText(Index, t, m_CurrList.GetItemText(CopyIndex, count));
			count++;

		}
		else if (t == 0){
			count++;
		}
		else{
			m_Lot_CurrentList.SetItemText(Index, t, LOT_NUMBER);
		}
	}

	Lot_StartCnt++;

}
void CImageTesterDlg ::LOT_EXCEL_SAVE_C(){
	CString Item[5]={"1.8V","3.3V","9.0V","14.7","M5V7V"};
	CString Data ="";
	CString str="";
	str = "***********************전류 검사***********************\n";
	Data += str;
	str.Empty();
	str.Format("CH 1 : 최소 mA , %d ,최대 mA,  %d, \n", m_MinAmp, m_MaxAmp);
	Data += str;
	str.Empty();
	str.Format("CH 2 :최소 mA , %d ,최대 mA,  %d, CH 3 :최소 mA , %d ,최대 mA,  %d, \n", m_MinAmp2, m_MaxAmp2, m_MinAmp3, m_MaxAmp3);
	Data += str;
	str.Empty();
	str.Format("CH 4 :최소 mA , %d ,최대 mA,  %d, CH 5 :최소 mA , %d ,최대 mA,  %d, \n", m_MinAmp4, m_MaxAmp4, m_MinAmp5, m_MaxAmp5);
	Data += str;
	str = "*************************************************\n";
	Data += str;
	LOT_SAVE_EXCEL("동작전류",Item,5,&m_Lot_CurrentList,Data);
}

bool CImageTesterDlg ::LOT_EXCEL_UPLOAD_C(){
	m_Lot_CurrentList.DeleteAllItems();
	if(LOT_EXCEL_UPLOAD("동작전류",&m_Lot_CurrentList,1,&Lot_StartCnt_C)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt_C = 0;
		return FALSE;	
	}
	return TRUE;
}

#pragma region TEST항목 생성

void CImageTesterDlg::CMasterMo_Create(){
	if(m_pMasterMonitor == NULL){
		m_pMasterMonitor = new CMasterMonitorDlg(this);
		m_pMasterMonitor->Setup(this,m_pTStat);
		m_pMasterMonitor->Create(CMasterMonitorDlg::IDD, CWnd::GetDesktopWindow());//&m_CtrTab);
//		m_pMasterMonitor->GetWindowRect(InitMasterMoRect);
		m_pMasterMonitor->ShowWindow(SW_HIDE);
		((CButton *)GetDlgItem(IDC_BTN_MASTER_SET))->EnableWindow(1);
	}
}

void CImageTesterDlg::CMasterMo_Delete(){
	if(m_pMasterMonitor != NULL){
		m_pMasterMonitor->DestroyWindow();
		delete m_pMasterMonitor;
		m_pMasterMonitor = NULL;
		((CButton *)GetDlgItem(IDC_BTN_MASTER_SET))->EnableWindow(0);
	}
}


void CImageTesterDlg::CModelOpt_Stand_Create(){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResStandOptWnd = new COption_ResultStand(this);
		m_pResStandOptWnd->Setup(this);
		m_pResStandOptWnd->Create(COption_ResultStand::IDD,m_pModResTabWnd);
		m_pResStandOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResStandOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResStandOptWnd->ShowWindow(SW_HIDE);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pSubStandOptWnd = new COption_SubStand(this);
		m_pSubStandOptWnd->Setup(this,m_pTStat);
		m_pSubStandOptWnd->Create(COption_SubStand::IDD,m_pModOptTabWnd);
		m_pSubStandOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pSubStandOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pSubStandOptWnd->ShowWindow(SW_HIDE);
	}
	
}

void CImageTesterDlg::CModelOpt_Stand_Delete(){
	
	if(m_pSubStandOptWnd != NULL){
		m_pSubStandOptWnd->DestroyWindow();
		delete m_pSubStandOptWnd;
		m_pSubStandOptWnd = NULL;	
	}
	
	if(m_pResStandOptWnd != NULL){
		m_pResStandOptWnd->DestroyWindow();
		delete m_pResStandOptWnd;
		m_pResStandOptWnd = NULL;	
	}
	
}

void CImageTesterDlg::Model_ShowWindow(int inshow){
	m_pResStandOptWnd->ShowWindow(inshow);
	m_pSubStandOptWnd->ShowWindow(inshow);
}


void CImageTesterDlg::CModelOpt_Angle_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResAngleOptWnd = new CAngle_ResultView(this);
		m_pResAngleOptWnd->Setup(this);
		m_pResAngleOptWnd->Create(CAngle_ResultView::IDD,m_pModResTabWnd);
		m_pResAngleOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResAngleOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResAngleOptWnd->ShowWindow(SW_SHOW);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pAngleOptWnd = new CAngle_Option(this);
		m_pAngleOptWnd->Setup(this,m_pTStat,INFO);
		m_pAngleOptWnd->Create(CAngle_Option::IDD,m_pModOptTabWnd);
		m_pAngleOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pAngleOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pAngleOptWnd->ShowWindow(SW_HIDE);
	}
	
}

void CImageTesterDlg::CModelOpt_Angle_Delete(){
	
	if(m_pAngleOptWnd != NULL){
		m_pAngleOptWnd->DestroyWindow();
		delete m_pAngleOptWnd;
		m_pAngleOptWnd = NULL;	
	}
	
	if(m_pResAngleOptWnd != NULL){
		m_pResAngleOptWnd->DestroyWindow();
		delete m_pResAngleOptWnd;
		m_pResAngleOptWnd = NULL;	
	}
	
}

void CImageTesterDlg::CModelOpt_Angle_ShowWindow(int inshow){
	m_pAngleOptWnd->ShowWindow(inshow);
	m_pResAngleOptWnd->ShowWindow(inshow);
}

void CImageTesterDlg::CModelOpt_Brightness_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResBrightnessOptWnd = new CBrightness_ResultView(this);
		m_pResBrightnessOptWnd->Setup(this);
		m_pResBrightnessOptWnd->Create(CBrightness_ResultView::IDD,m_pModResTabWnd);
		m_pResBrightnessOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResBrightnessOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResBrightnessOptWnd->ShowWindow(SW_HIDE);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pBrightnessOptWnd = new CBrightness_Option(this);
		m_pBrightnessOptWnd->Setup(this,m_pTStat,INFO);
		m_pBrightnessOptWnd->Create(CBrightness_Option::IDD,m_pModOptTabWnd);
		m_pBrightnessOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pBrightnessOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pBrightnessOptWnd->ShowWindow(SW_HIDE);
	}
	
}

void CImageTesterDlg::CModelOpt_Brightness_Delete(){
	
	if(m_pBrightnessOptWnd != NULL){
		m_pBrightnessOptWnd->DestroyWindow();
		delete m_pBrightnessOptWnd;
		m_pBrightnessOptWnd = NULL;	
	}
	
	if(m_pResBrightnessOptWnd != NULL){
		m_pResBrightnessOptWnd->DestroyWindow();
		delete m_pResBrightnessOptWnd;
		m_pResBrightnessOptWnd = NULL;	
	}
	
}

void CImageTesterDlg::CModelOpt_Brightness_ShowWindow(int inshow){
	m_pBrightnessOptWnd->ShowWindow(inshow);

	m_pResBrightnessOptWnd->ShowWindow(inshow);
}

void CImageTesterDlg::CModelOpt_CPoint_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResCPointOptWnd = new CCENTER_DETECT_ResultView(this);
		m_pResCPointOptWnd->Setup(this);
		m_pResCPointOptWnd->Create(CCENTER_DETECT_ResultView::IDD,m_pModResTabWnd);
		m_pResCPointOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResCPointOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResCPointOptWnd->ShowWindow(SW_SHOW);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pSubCPointOptWnd = new CCENTER_DETECT_Option(this);
		m_pSubCPointOptWnd->Setup(this,m_pTStat,INFO);
		m_pSubCPointOptWnd->Create(CCENTER_DETECT_Option::IDD,m_pModOptTabWnd);
		m_pSubCPointOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pSubCPointOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pSubCPointOptWnd->ShowWindow(SW_SHOW);
	}
	CMasterMo_Create();

	CMicomCenter_Create();
	
}

void CImageTesterDlg::CModelOpt_CPoint_Delete(){
	if(m_pSubCPointOptWnd != NULL){
		m_pSubCPointOptWnd->DestroyWindow();
		delete m_pSubCPointOptWnd;
		m_pSubCPointOptWnd = NULL;	
	}
	
	if(m_pResCPointOptWnd != NULL){
		m_pResCPointOptWnd->DestroyWindow();
		delete m_pResCPointOptWnd;
		m_pResCPointOptWnd = NULL;	
	}

	CMasterMo_Delete();
	CMicomCenter_Delete();
}

void CImageTesterDlg::CModelOpt_CPoint_ShowWindow(int inshow){
	m_pSubCPointOptWnd->ShowWindow(inshow);
	m_pResCPointOptWnd->ShowWindow(inshow);
}

//-------------------------------------------------
void CImageTesterDlg::CModelOpt_NTSC_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResCNTSCOptWnd = new CNTSC_ResultView(this);
		m_pResCNTSCOptWnd->Setup(this);
		m_pResCNTSCOptWnd->Create(CNTSC_ResultView::IDD,m_pModResTabWnd);
		m_pResCNTSCOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResCNTSCOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResCNTSCOptWnd->ShowWindow(SW_HIDE);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pSubCNTSCOptWnd = new CNTSC_Option(this);
		m_pSubCNTSCOptWnd->Setup(this,m_pTStat,INFO);
		m_pSubCNTSCOptWnd->Create(CNTSC_Option::IDD,m_pModOptTabWnd);
		m_pSubCNTSCOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pSubCNTSCOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pSubCNTSCOptWnd->ShowWindow(SW_HIDE);
	}
}

void CImageTesterDlg::CModelOpt_NTSC_Delete(){
	if(m_pSubCNTSCOptWnd != NULL){
		m_pSubCNTSCOptWnd->DestroyWindow();
		delete m_pSubCNTSCOptWnd;
		m_pSubCNTSCOptWnd = NULL;	
	}
	
	if(m_pResCNTSCOptWnd != NULL){
		m_pResCNTSCOptWnd->DestroyWindow();
		delete m_pResCNTSCOptWnd;
		m_pResCNTSCOptWnd = NULL;	
	}
}

void CImageTesterDlg::CModelOpt_NTSC_ShowWindow(int inshow){
	m_pSubCNTSCOptWnd->ShowWindow(inshow);
	m_pResCNTSCOptWnd->ShowWindow(inshow);
}
//-------------------------------------------------

void CImageTesterDlg::CMicomCenter_Create(){
	m_pMicomCenterWnd = new CMicom_Center(this);
	m_pMicomCenterWnd->Setup(this,m_pTStat);
	m_pMicomCenterWnd->Create(CMicom_Center::IDD,CWnd::GetDesktopWindow());
	m_pMicomCenterWnd->ShowWindow(SW_HIDE);
	
}

void CImageTesterDlg::CMicomCenter_Delete(){
	if(m_pMicomCenterWnd != NULL){
		m_pMicomCenterWnd->DestroyWindow();
		delete m_pMicomCenterWnd;
		m_pMicomCenterWnd = NULL;	
	}
}


void CImageTesterDlg::MicomCenter_Popup()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_pMicomCenterWnd != NULL){
		if(!m_pMicomCenterWnd->IsWindowVisible()){

			m_pMicomCenterWnd->ShowWindow(SW_SHOW);
			//m_pMasterMonitor->Chartset();
		}else{
			
			m_pMicomCenterWnd->ShowWindow(SW_HIDE);
		
			m_pMicomCenterWnd->ShowWindow(SW_SHOW);
		}
	}
}

void CImageTesterDlg::CModelOpt_Color_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResColorOptWnd = new CColor_ResultView(this);
		m_pResColorOptWnd->Setup(this);
		m_pResColorOptWnd->Create(CColor_ResultView::IDD,m_pModResTabWnd);
		m_pResColorOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResColorOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResColorOptWnd->ShowWindow(SW_SHOW);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pColorOptWnd = new CColor_Option(this);
		m_pColorOptWnd->Setup(this,m_pTStat,INFO);
		m_pColorOptWnd->Create(CColor_Option::IDD,m_pModOptTabWnd);
		m_pColorOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pColorOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pColorOptWnd->ShowWindow(SW_HIDE);
	}
	
}

void CImageTesterDlg::CModelOpt_Color_Delete(){
	
	if(m_pColorOptWnd != NULL){
		m_pColorOptWnd->DestroyWindow();
		delete m_pColorOptWnd;
		m_pColorOptWnd = NULL;	
	}
	
	if(m_pResColorOptWnd != NULL){
		m_pResColorOptWnd->DestroyWindow();
		delete m_pResColorOptWnd;
		m_pResColorOptWnd = NULL;	
	}
	
}

void CImageTesterDlg::CModelOpt_Color_ShowWindow(int inshow){
	m_pColorOptWnd->ShowWindow(inshow);

	m_pResColorOptWnd->ShowWindow(inshow);
}

void CImageTesterDlg::CModelOpt_ColorReversal_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResColorRvsOptWnd = new CColorReversal_ResultView(this);
		m_pResColorRvsOptWnd->Setup(this);
		m_pResColorRvsOptWnd->Create(CColorReversal_ResultView::IDD,m_pModResTabWnd);
		m_pResColorRvsOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResColorRvsOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResColorRvsOptWnd->ShowWindow(SW_SHOW);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pColorRvsOptWnd = new CColorReversal_Option(this);
		m_pColorRvsOptWnd->Setup(this,m_pTStat,INFO);
		m_pColorRvsOptWnd->Create(CColorReversal_Option::IDD,m_pModOptTabWnd);
		m_pColorRvsOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pColorRvsOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pColorRvsOptWnd->ShowWindow(SW_HIDE);
	}
	
}

void CImageTesterDlg::CModelOpt_ColorReversal_Delete(){
	
	if(m_pColorRvsOptWnd != NULL){
		m_pColorRvsOptWnd->DestroyWindow();
		delete m_pColorRvsOptWnd;
		m_pColorRvsOptWnd = NULL;	
	}
	
	if(m_pResColorRvsOptWnd != NULL){
		m_pResColorRvsOptWnd->DestroyWindow();
		delete m_pResColorRvsOptWnd;
		m_pResColorRvsOptWnd = NULL;	
	}
	
}

void CImageTesterDlg::CModelOpt_ColorReversal_ShowWindow(int inshow){
	m_pColorRvsOptWnd->ShowWindow(inshow);

	m_pResColorRvsOptWnd->ShowWindow(inshow);
}

void CImageTesterDlg::CModelOpt_Distortion_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResDistortionOptWnd = new CDistortion_ResultView(this);
		m_pResDistortionOptWnd->Setup(this);
		m_pResDistortionOptWnd->Create(CDistortion_ResultView::IDD,m_pModResTabWnd);
		m_pResDistortionOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResDistortionOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResDistortionOptWnd->ShowWindow(SW_HIDE);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pDistortionOptWnd = new CDistortion_Option(this);
		m_pDistortionOptWnd->Setup(this,m_pTStat,INFO);
		m_pDistortionOptWnd->Create(CDistortion_Option::IDD,m_pModOptTabWnd);
		m_pDistortionOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pDistortionOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pDistortionOptWnd->ShowWindow(SW_HIDE);
	}
	
}

void CImageTesterDlg::CModelOpt_Distortion_Delete(){
	
	if(m_pDistortionOptWnd != NULL){
		m_pDistortionOptWnd->DestroyWindow();
		delete m_pDistortionOptWnd;
		m_pDistortionOptWnd = NULL;	
	}
	
	if(m_pResDistortionOptWnd != NULL){
		m_pResDistortionOptWnd->DestroyWindow();
		delete m_pResDistortionOptWnd;
		m_pResDistortionOptWnd = NULL;	
	}
	
}

void CImageTesterDlg::CModelOpt_Distortion_ShowWindow(int inshow){
	m_pDistortionOptWnd->ShowWindow(inshow);

	m_pResDistortionOptWnd->ShowWindow(inshow);
}

void CImageTesterDlg::CModelOpt_IRFilter_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResIRFilterOptWnd = new CIRFilter_ResultView(this);
		m_pResIRFilterOptWnd->Setup(this);
		m_pResIRFilterOptWnd->Create(CIRFilter_ResultView::IDD,m_pModResTabWnd);
		m_pResIRFilterOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResIRFilterOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResIRFilterOptWnd->ShowWindow(SW_HIDE);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pIRFilterOptWnd = new CIRFilter_Option(this);
		m_pIRFilterOptWnd->Setup(this,m_pTStat,INFO);
		m_pIRFilterOptWnd->Create(CIRFilter_Option::IDD,m_pModOptTabWnd);
		m_pIRFilterOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pIRFilterOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pIRFilterOptWnd->ShowWindow(SW_HIDE);
	}
	
}

void CImageTesterDlg::CModelOpt_IRFilter_Delete(){
	
	if(m_pIRFilterOptWnd != NULL){
		m_pIRFilterOptWnd->DestroyWindow();
		delete m_pIRFilterOptWnd;
		m_pIRFilterOptWnd = NULL;	
	}
	
	if(m_pResIRFilterOptWnd != NULL){
		m_pResIRFilterOptWnd->DestroyWindow();
		delete m_pResIRFilterOptWnd;
		m_pResIRFilterOptWnd = NULL;	
	}
	
}

void CImageTesterDlg::CModelOpt_IRFilter_ShowWindow(int inshow){
	m_pIRFilterOptWnd->ShowWindow(inshow);

	m_pResIRFilterOptWnd->ShowWindow(inshow);
}


/////////////////////////////////////////////////////////왜곡
void CImageTesterDlg::CModelOpt_COPoint_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResCOpticPointOptWnd = new COPTICAL_DETECT_ResultView(this);
		m_pResCOpticPointOptWnd->Setup(this);
		m_pResCOpticPointOptWnd->Create(COPTICAL_DETECT_ResultView::IDD,m_pModResTabWnd);
		m_pResCOpticPointOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResCOpticPointOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResCOpticPointOptWnd->ShowWindow(SW_SHOW);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pSubCOpticPointOptWnd = new COPTIC_DETECT_Option(this);
		m_pSubCOpticPointOptWnd->Setup(this,m_pTStat,INFO);
		m_pSubCOpticPointOptWnd->Create(COPTIC_DETECT_Option::IDD,m_pModOptTabWnd);
		m_pSubCOpticPointOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pSubCOpticPointOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pSubCOpticPointOptWnd->ShowWindow(SW_SHOW);
	}

	CMicomOptic_Create();

}

void CImageTesterDlg::CModelOpt_COPoint_Delete(){
	
	if(m_pSubCOpticPointOptWnd != NULL){
		m_pSubCOpticPointOptWnd->DestroyWindow();
		delete m_pSubCOpticPointOptWnd;
		m_pSubCOpticPointOptWnd = NULL;	
	}
	
	if(m_pResCOpticPointOptWnd != NULL){
		m_pResCOpticPointOptWnd->DestroyWindow();
		delete m_pResCOpticPointOptWnd;
		m_pResCOpticPointOptWnd = NULL;	
	}
	
		CMicomOptic_Delete();
}




void CImageTesterDlg::CModelOpt_COPoint_ShowWindow(int inshow){
	m_pSubCOpticPointOptWnd->ShowWindow(inshow);

	m_pResCOpticPointOptWnd->ShowWindow(inshow);
}


void CImageTesterDlg::CMicomOptic_Create(){
	m_pMicomOpticWnd = new CMicom_Optic(this);
	m_pMicomOpticWnd->Setup(this,m_pTStat);
	m_pMicomOpticWnd->Create(CMicom_Optic::IDD,CWnd::GetDesktopWindow());
	m_pMicomOpticWnd->ShowWindow(SW_HIDE);
	
}

void CImageTesterDlg::CMicomOptic_Delete(){
	if(m_pMicomOpticWnd != NULL){
		m_pMicomOpticWnd->DestroyWindow();
		delete m_pMicomOpticWnd;
		m_pMicomOpticWnd = NULL;	
	}
}


void CImageTesterDlg::MicomOptic_Popup()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_pMicomOpticWnd != NULL){
		if(!m_pMicomOpticWnd->IsWindowVisible()){

			m_pMicomOpticWnd->ShowWindow(SW_SHOW);
			//m_pMasterMonitor->Chartset();
		}else{
			
			m_pMicomOpticWnd->ShowWindow(SW_HIDE);
		
			m_pMicomOpticWnd->ShowWindow(SW_SHOW);
		}
	}
}
/////////////////////////////////////////////////////////이물검사
void CImageTesterDlg::CModelOpt_Particle_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResParticleOptWnd = new CParticle_ResultView(this);
		m_pResParticleOptWnd->Setup(this);
		m_pResParticleOptWnd->Create(CParticle_ResultView::IDD,m_pModResTabWnd);
		m_pResParticleOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResParticleOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResParticleOptWnd->ShowWindow(SW_HIDE);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pParticleOptWnd = new CParticle_Option(this);
		m_pParticleOptWnd->Setup(this,m_pTStat,INFO);
		m_pParticleOptWnd->Create(CParticle_Option::IDD,m_pModOptTabWnd);
		m_pParticleOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pParticleOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pParticleOptWnd->ShowWindow(SW_HIDE);
	}
	
}

void CImageTesterDlg::CModelOpt_Particle_Delete(){
	
	if(m_pParticleOptWnd != NULL){
		m_pParticleOptWnd->DestroyWindow();
		delete m_pParticleOptWnd;
		m_pParticleOptWnd = NULL;	
	}
	
	if(m_pResParticleOptWnd != NULL){
		m_pResParticleOptWnd->DestroyWindow();
		delete m_pResParticleOptWnd;
		m_pResParticleOptWnd = NULL;	
	}
	
}

void CImageTesterDlg::CModelOpt_Particle_ShowWindow(int inshow){
	m_pParticleOptWnd->ShowWindow(inshow);
	m_pResParticleOptWnd->ShowWindow(inshow);
}
/////////////////////////////////////////////////////////오버레이
void CImageTesterDlg::CModelOpt_Overlay_CAN_Create(tINFO INFO){  
	CRect OptionRect;

	if(m_pModResTabWnd != NULL){
		m_pResOverlay_CAN_OptWnd = new COption_ResultOverlay(this);
		m_pResOverlay_CAN_OptWnd->Setup(this);
		m_pResOverlay_CAN_OptWnd->Create(COption_ResultOverlay::IDD,m_pModResTabWnd);
		m_pResOverlay_CAN_OptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResOverlay_CAN_OptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResOverlay_CAN_OptWnd->ShowWindow(SW_SHOW);
	}

	if(m_pModOptTabWnd != NULL){
		m_pOverlay_CAN_OptWnd = new COverlayDetect_Option(this);
		m_pOverlay_CAN_OptWnd->Setup(this,m_pTStat,INFO);
		m_pOverlay_CAN_OptWnd->Create(COverlayDetect_Option::IDD,m_pModOptTabWnd);
		m_pOverlay_CAN_OptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pOverlay_CAN_OptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pOverlay_CAN_OptWnd->ShowWindow(SW_HIDE);
	}

}

void CImageTesterDlg::CModelOpt_Overlay_CAN_Delete(){

	if(m_pOverlay_CAN_OptWnd != NULL){
		m_pOverlay_CAN_OptWnd->DestroyWindow();
		delete m_pOverlay_CAN_OptWnd;
		m_pOverlay_CAN_OptWnd = NULL;	
	}

	if(m_pResOverlay_CAN_OptWnd != NULL){
		m_pResOverlay_CAN_OptWnd->DestroyWindow();
		delete m_pResOverlay_CAN_OptWnd;
		m_pResOverlay_CAN_OptWnd = NULL;	
	}

}

void CImageTesterDlg::CModelOpt_Overlay_CAN_ShowWindow(int inshow){
	m_pOverlay_CAN_OptWnd->ShowWindow(inshow);

	m_pResOverlay_CAN_OptWnd->ShowWindow(inshow);
}

/////////////////////////////////////////////////////////경고문구
void CImageTesterDlg::CModelOpt_Warning_CAN_Create(tINFO INFO){  
	CRect OptionRect;

	if(m_pModResTabWnd != NULL){
		m_pResWarning_CAN_OptWnd = new COption_ResultWarning(this);
		m_pResWarning_CAN_OptWnd->Setup(this);
		m_pResWarning_CAN_OptWnd->Create(COption_ResultWarning::IDD,m_pModResTabWnd);
		m_pResWarning_CAN_OptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResWarning_CAN_OptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResWarning_CAN_OptWnd->ShowWindow(SW_SHOW);
	}

	if(m_pModOptTabWnd != NULL){
		m_pWarning_CAN_OptWnd = new CWarningDetect_Option(this);
		m_pWarning_CAN_OptWnd->Setup(this,m_pTStat,INFO);
		m_pWarning_CAN_OptWnd->Create(CWarningDetect_Option::IDD,m_pModOptTabWnd);
		m_pWarning_CAN_OptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pWarning_CAN_OptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pWarning_CAN_OptWnd->ShowWindow(SW_HIDE);
	}

}

void CImageTesterDlg::CModelOpt_Warning_CAN_Delete(){

	if(m_pWarning_CAN_OptWnd != NULL){
		m_pWarning_CAN_OptWnd->DestroyWindow();
		delete m_pWarning_CAN_OptWnd;
		m_pWarning_CAN_OptWnd = NULL;	
	}

	if(m_pResWarning_CAN_OptWnd != NULL){
		m_pResWarning_CAN_OptWnd->DestroyWindow();
		delete m_pResWarning_CAN_OptWnd;
		m_pResWarning_CAN_OptWnd = NULL;	
	}

}

void CImageTesterDlg::CModelOpt_Warning_CAN_ShowWindow(int inshow){
	m_pWarning_CAN_OptWnd->ShowWindow(inshow);

	m_pResWarning_CAN_OptWnd->ShowWindow(inshow);
}



/////////////////////////////////////////////////////////이물검사_LG
void CImageTesterDlg::CModelOpt_ParticleLG_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResParticleOptLGWnd = new CParticle_ResultView_LG(this);
		m_pResParticleOptLGWnd->Setup(this);
		m_pResParticleOptLGWnd->Create(CParticle_ResultView_LG::IDD,m_pModResTabWnd);
		m_pResParticleOptLGWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResParticleOptLGWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResParticleOptLGWnd->ShowWindow(SW_HIDE);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pParticleOptLGWnd = new CParticle_Option_LG(this);
		m_pParticleOptLGWnd->Setup(this,m_pTStat,INFO);
		m_pParticleOptLGWnd->Create(CParticle_Option_LG::IDD,m_pModOptTabWnd);
		m_pParticleOptLGWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pParticleOptLGWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pParticleOptLGWnd->ShowWindow(SW_HIDE);
	}
	
}

void CImageTesterDlg::CModelOpt_ParticleLG_Delete(){
	
	if(m_pParticleOptLGWnd != NULL){
		m_pParticleOptLGWnd->DestroyWindow();
		delete m_pParticleOptLGWnd;
		m_pParticleOptLGWnd = NULL;	
	}
	
	if(m_pResParticleOptLGWnd != NULL){
		m_pResParticleOptLGWnd->DestroyWindow();
		delete m_pResParticleOptLGWnd;
		m_pResParticleOptLGWnd = NULL;	
	}
	
}

void CImageTesterDlg::CModelOpt_ParticleLG_ShowWindow(int inshow){
	m_pParticleOptLGWnd->ShowWindow(inshow);
	m_pResParticleOptLGWnd->ShowWindow(inshow);
}

/////////////////////////////////////////////////////////Fixed Pattern
void CImageTesterDlg::CModelOpt_FixedPattern_Create(tINFO INFO){
	CRect OptionRect;

	if (m_pModResTabWnd != NULL){
		m_pResFixedPatternWnd = new CFixedPattern_ResultView(this);
		m_pResFixedPatternWnd->Setup(this);
		m_pResFixedPatternWnd->Create(CFixedPattern_ResultView::IDD, m_pModResTabWnd);
		m_pResFixedPatternWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResFixedPatternWnd->MoveWindow(5, 25, OptionRect.Width(), OptionRect.Height());
		m_pResFixedPatternWnd->ShowWindow(SW_SHOW);
	}

	if (m_pModOptTabWnd != NULL){
		m_pFixedPatternOptWnd = new COption_FixedPattern(this);
		m_pFixedPatternOptWnd->Setup(this, m_pTStat, INFO);
		m_pFixedPatternOptWnd->Create(COption_FixedPattern::IDD, m_pModOptTabWnd);
		m_pFixedPatternOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pFixedPatternOptWnd->MoveWindow(10, 45, OptionRect.Width(), OptionRect.Height());
		m_pFixedPatternOptWnd->ShowWindow(SW_HIDE);
	}

}

void CImageTesterDlg::CModelOpt_FixedPattern_Delete(){

	if (m_pFixedPatternOptWnd != NULL){
		m_pFixedPatternOptWnd->DestroyWindow();
		delete m_pFixedPatternOptWnd;
		m_pFixedPatternOptWnd = NULL;
	}

	if (m_pResFixedPatternWnd != NULL){
		m_pResFixedPatternWnd->DestroyWindow();
		delete m_pResFixedPatternWnd;
		m_pResFixedPatternWnd = NULL;
	}

}

void CImageTesterDlg::CModelOpt_FixedPattern_ShowWindow(int inshow){
	m_pFixedPatternOptWnd->ShowWindow(inshow);

	m_pResFixedPatternWnd->ShowWindow(inshow);
}
/////////////////////////////////////////////////////////저조도검사
void CImageTesterDlg::CModelOpt_PatternNoise_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResPatternNoiseOptWnd = new CPatternNoise_ResultView(this);
		m_pResPatternNoiseOptWnd->Setup(this);
		m_pResPatternNoiseOptWnd->Create(CPatternNoise_ResultView::IDD,m_pModResTabWnd);
		m_pResPatternNoiseOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResPatternNoiseOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResPatternNoiseOptWnd->ShowWindow(SW_SHOW);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pPatternNoiseOptWnd = new CPatternNoise_Option(this);
		m_pPatternNoiseOptWnd->Setup(this,m_pTStat,INFO);
		m_pPatternNoiseOptWnd->Create(CPatternNoise_Option::IDD,m_pModOptTabWnd);
		m_pPatternNoiseOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pPatternNoiseOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pPatternNoiseOptWnd->ShowWindow(SW_HIDE);
	}
	
}

void CImageTesterDlg::CModelOpt_PatternNoise_Delete(){
	
	if(m_pPatternNoiseOptWnd != NULL){
		m_pPatternNoiseOptWnd->DestroyWindow();
		delete m_pPatternNoiseOptWnd;
		m_pPatternNoiseOptWnd = NULL;	
	}
	
	if(m_pResPatternNoiseOptWnd != NULL){
		m_pResPatternNoiseOptWnd->DestroyWindow();
		delete m_pResPatternNoiseOptWnd;
		m_pResPatternNoiseOptWnd = NULL;	
	}
	
}

void CImageTesterDlg::CModelOpt_PatternNoise_ShowWindow(int inshow){
	m_pPatternNoiseOptWnd->ShowWindow(inshow);

	m_pResPatternNoiseOptWnd->ShowWindow(inshow);
}

/////////////////////////////////////////////////////////DynamicRange
void CImageTesterDlg::CModelOpt_DynamicRange_Create(tINFO INFO){
	CRect OptionRect;

	if (m_pModResTabWnd != NULL){
		m_pResDynamicRangeOptWnd = new CDynamicRange_ResultView(this);
		m_pResDynamicRangeOptWnd->Setup(this);
		m_pResDynamicRangeOptWnd->Create(CDynamicRange_ResultView::IDD, m_pModResTabWnd);
		m_pResDynamicRangeOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResDynamicRangeOptWnd->MoveWindow(5, 25, OptionRect.Width(), OptionRect.Height());
		m_pResDynamicRangeOptWnd->ShowWindow(SW_SHOW);
	}

	if (m_pModOptTabWnd != NULL){
		m_pDynamicRangeOptWnd = new CDynamicRange_Option(this);
		m_pDynamicRangeOptWnd->Setup(this, m_pTStat, INFO);
		m_pDynamicRangeOptWnd->Create(CDynamicRange_Option::IDD, m_pModOptTabWnd);
		m_pDynamicRangeOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pDynamicRangeOptWnd->MoveWindow(10, 45, OptionRect.Width(), OptionRect.Height());
		m_pDynamicRangeOptWnd->ShowWindow(SW_HIDE);
	}

}

void CImageTesterDlg::CModelOpt_DynamicRange_Delete(){

	if (m_pDynamicRangeOptWnd != NULL){
		m_pDynamicRangeOptWnd->DestroyWindow();
		delete m_pDynamicRangeOptWnd;
		m_pDynamicRangeOptWnd = NULL;
	}

	if (m_pResDynamicRangeOptWnd != NULL){
		m_pResDynamicRangeOptWnd->DestroyWindow();
		delete m_pResDynamicRangeOptWnd;
		m_pResDynamicRangeOptWnd = NULL;
	}

}

void CImageTesterDlg::CModelOpt_DynamicRange_ShowWindow(int inshow){
	m_pDynamicRangeOptWnd->ShowWindow(inshow);

	m_pResDynamicRangeOptWnd->ShowWindow(inshow);
}

/////////////////////////////////////////////////////////해상력
void CImageTesterDlg::CModelOpt_Resolution_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResResolutionOptWnd = new CResolutionDetection_ResultView(this);
		m_pResResolutionOptWnd->Setup(this);
		m_pResResolutionOptWnd->Create(CResolutionDetection_ResultView::IDD,m_pModResTabWnd);
		m_pResResolutionOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResResolutionOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResResolutionOptWnd->ShowWindow(SW_HIDE);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pResolutionOptWnd = new CResolutionDetection_Option(this);
		m_pResolutionOptWnd->Setup(this,m_pTStat,INFO);
		m_pResolutionOptWnd->Create(CResolutionDetection_Option::IDD,m_pModOptTabWnd);
		m_pResolutionOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResolutionOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pResolutionOptWnd->ShowWindow(SW_HIDE);
	}
	
}

void CImageTesterDlg::CModelOpt_Resolution_Delete(){
	
	if(m_pResolutionOptWnd != NULL){
		m_pResolutionOptWnd->DestroyWindow();
		delete m_pResolutionOptWnd;
		m_pResolutionOptWnd = NULL;	
	}
	
	if(m_pResResolutionOptWnd != NULL){
		m_pResResolutionOptWnd->DestroyWindow();
		delete m_pResResolutionOptWnd;
		m_pResResolutionOptWnd = NULL;	
	}
	
}



void CImageTesterDlg::CModelOpt_Resolution_ShowWindow(int inshow){
	m_pResolutionOptWnd->ShowWindow(inshow);

	m_pResResolutionOptWnd->ShowWindow(inshow);
}
/////////////////////////////////////////////////////////해상력(SFR)
void CImageTesterDlg::CModelOpt_SFR_Create(tINFO INFO){
	CRect OptionRect;

	if (m_pModResTabWnd != NULL){
		m_pResSFROptWnd = new CSFR_ResultView(this);
		m_pResSFROptWnd->Setup(this);
		m_pResSFROptWnd->Create(CSFR_ResultView::IDD, m_pModResTabWnd);
		m_pResSFROptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResSFROptWnd->MoveWindow(5, 25, OptionRect.Width(), OptionRect.Height());
		m_pResSFROptWnd->ShowWindow(SW_HIDE);
	}

	if (m_pModOptTabWnd != NULL){
		m_pSFROptWnd = new CSFRTILTOptionDlg(this);
		m_pSFROptWnd->Setup(this, m_pTStat, INFO);
		m_pSFROptWnd->Create(CSFRTILTOptionDlg::IDD, m_pModOptTabWnd);
		m_pSFROptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pSFROptWnd->MoveWindow(10, 45, OptionRect.Width(), OptionRect.Height());
		m_pSFROptWnd->ShowWindow(SW_HIDE);
	}

}

void CImageTesterDlg::CModelOpt_SFR_Delete(){

	if (m_pSFROptWnd != NULL){
		m_pSFROptWnd->DestroyWindow();
		delete m_pSFROptWnd;
		m_pSFROptWnd = NULL;
	}

	if (m_pResSFROptWnd != NULL){
		m_pResSFROptWnd->DestroyWindow();
		delete m_pResSFROptWnd;
		m_pResSFROptWnd = NULL;
	}

}



void CImageTesterDlg::CModelOpt_SFR_ShowWindow(int inshow){
	m_pSFROptWnd->ShowWindow(inshow);

	m_pResSFROptWnd->ShowWindow(inshow);
}
/////////////////////////////////////////////////////////로테이트
void CImageTesterDlg::CModelOpt_Rotate_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResRotateOptWnd = new CRotate_ResultView(this);
		m_pResRotateOptWnd->Setup(this);
		m_pResRotateOptWnd->Create(CRotate_ResultView::IDD,m_pModResTabWnd);
		m_pResRotateOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResRotateOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResRotateOptWnd->ShowWindow(SW_HIDE);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pRotateOptWnd = new CRotate_Option(this);
		m_pRotateOptWnd->Setup(this,m_pTStat,INFO);
		m_pRotateOptWnd->Create(CRotate_Option::IDD,m_pModOptTabWnd);
		m_pRotateOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pRotateOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pRotateOptWnd->ShowWindow(SW_HIDE);
	}
	
	//CMasterMo_Create();
}

void CImageTesterDlg::CModelOpt_Rotate_Delete(){
	
	if(m_pRotateOptWnd != NULL){
		m_pRotateOptWnd->DestroyWindow();
		delete m_pRotateOptWnd;
		m_pRotateOptWnd = NULL;	
	}
	
	if(m_pResRotateOptWnd != NULL){
		m_pResRotateOptWnd->DestroyWindow();
		delete m_pResRotateOptWnd;
		m_pResRotateOptWnd = NULL;	
	}
	
	//CMasterMo_Delete();
}



void CImageTesterDlg::CModelOpt_Rotate_ShowWindow(int inshow){
	m_pRotateOptWnd->ShowWindow(inshow);

	m_pResRotateOptWnd->ShowWindow(inshow);
}


void CImageTesterDlg::CModelOpt_SYSTEM_Create(tINFO INFO){  ////0x1b
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResSYSTEMOptWnd = new CSYSTEM_ResultView(this);
		m_pResSYSTEMOptWnd->Setup(this);
		m_pResSYSTEMOptWnd->Create(CSYSTEM_ResultView::IDD,m_pModResTabWnd);
		m_pResSYSTEMOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResSYSTEMOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResSYSTEMOptWnd->ShowWindow(SW_HIDE);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pSYSTEMOptWnd = new CSYSTEM_Option(this);
		m_pSYSTEMOptWnd->Setup(this,m_pTStat,INFO);
		m_pSYSTEMOptWnd->Create(CSYSTEM_Option::IDD,m_pModOptTabWnd);
		m_pSYSTEMOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pSYSTEMOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pSYSTEMOptWnd->ShowWindow(SW_HIDE);
	}
}

void CImageTesterDlg::CModelOpt_SYSTEM_Delete(){
	if(m_pSYSTEMOptWnd != NULL){
		m_pSYSTEMOptWnd->DestroyWindow();
		delete m_pSYSTEMOptWnd;
		m_pSYSTEMOptWnd = NULL;	
	}
	
	if(m_pResSYSTEMOptWnd != NULL){
		m_pResSYSTEMOptWnd->DestroyWindow();
		delete m_pResSYSTEMOptWnd;
		m_pResSYSTEMOptWnd = NULL;	
	}
}

void CImageTesterDlg::CModelOpt_SYSTEM_ShowWindow(int inshow)
{
	m_pSYSTEMOptWnd->ShowWindow(inshow);
	m_pResSYSTEMOptWnd->ShowWindow(inshow);
}

void CImageTesterDlg::CModelOpt_DTC_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResDTCOptWnd = new CDTC_ResultView(this);
		m_pResDTCOptWnd->Setup(this);
		m_pResDTCOptWnd->Create(CDTC_ResultView::IDD,m_pModResTabWnd);
		m_pResDTCOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResDTCOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResDTCOptWnd->ShowWindow(SW_SHOW);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pDTCOptWnd = new CDTC_Option(this);
		m_pDTCOptWnd->Setup(this,m_pTStat,INFO);
		m_pDTCOptWnd->Create(CDTC_Option::IDD,m_pModOptTabWnd);
		m_pDTCOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pDTCOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pDTCOptWnd->ShowWindow(SW_HIDE);
	}
}


void CImageTesterDlg::CModelOpt_DTC_Delete(){
	
	if(m_pDTCOptWnd != NULL){
		m_pDTCOptWnd->DestroyWindow();
		delete m_pDTCOptWnd;
		m_pDTCOptWnd = NULL;	
	}
	
	if(m_pResDTCOptWnd != NULL){
		m_pResDTCOptWnd->DestroyWindow();
		delete m_pResDTCOptWnd;
		m_pResDTCOptWnd = NULL;	
	}
}


void CImageTesterDlg::CModelOpt_DTC_ShowWindow(int inshow)
{
	m_pDTCOptWnd->ShowWindow(inshow);
	m_pResDTCOptWnd->ShowWindow(inshow);
}


void CImageTesterDlg::CModelOpt_VER_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResVEROptWnd = new CVER_ResultView(this);
		m_pResVEROptWnd->Setup(this);
		m_pResVEROptWnd->Create(CVER_ResultView::IDD,m_pModResTabWnd);
		m_pResVEROptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResVEROptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResVEROptWnd->ShowWindow(SW_SHOW);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pVEROptWnd = new CVER_Option(this);
		m_pVEROptWnd->Setup(this,m_pTStat,INFO);
		m_pVEROptWnd->Create(CVER_Option::IDD,m_pModOptTabWnd);
		m_pVEROptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pVEROptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pVEROptWnd->ShowWindow(SW_HIDE);
	}
}


void CImageTesterDlg::CModelOpt_VER_Delete(){
	
	if(m_pVEROptWnd != NULL){
		m_pVEROptWnd->DestroyWindow();
		delete m_pVEROptWnd;
		m_pVEROptWnd = NULL;	
	}
	
	if(m_pResVEROptWnd != NULL){
		m_pResVEROptWnd->DestroyWindow();
		delete m_pResVEROptWnd;
		m_pResVEROptWnd = NULL;	
	}
}


void CImageTesterDlg::CModelOpt_VER_ShowWindow(int inshow)
{
	m_pVEROptWnd->ShowWindow(inshow);
	m_pResVEROptWnd->ShowWindow(inshow);
}

//
void CImageTesterDlg::CModelOpt_OVERLAYPOSITION_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResOverlayPositionOptWnd = new COverlayPosition_ResultView(this);
		m_pResOverlayPositionOptWnd->Setup(this);
		m_pResOverlayPositionOptWnd->Create(COverlayPosition_ResultView::IDD,m_pModResTabWnd);
		m_pResOverlayPositionOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResOverlayPositionOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResOverlayPositionOptWnd->ShowWindow(SW_SHOW);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pOverlayPositionOptWnd = new COverlayPosition_Option(this);
		m_pOverlayPositionOptWnd->Setup(this,m_pTStat,INFO);
		m_pOverlayPositionOptWnd->Create(COverlayPosition_Option::IDD,m_pModOptTabWnd);
		m_pOverlayPositionOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pOverlayPositionOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pOverlayPositionOptWnd->ShowWindow(SW_HIDE);
	}
}


void CImageTesterDlg::CModelOpt_OVERLAYPOSITION_Delete(){
	
	if(m_pOverlayPositionOptWnd != NULL){
		m_pOverlayPositionOptWnd->DestroyWindow();
		delete m_pOverlayPositionOptWnd;
		m_pOverlayPositionOptWnd = NULL;	
	}
	
	if(m_pResOverlayPositionOptWnd != NULL){
		m_pResOverlayPositionOptWnd->DestroyWindow();
		delete m_pResOverlayPositionOptWnd;
		m_pResOverlayPositionOptWnd = NULL;	
	}
}


void CImageTesterDlg::CModelOpt_OVERLAYPOSITION_ShowWindow(int inshow)
{
	m_pOverlayPositionOptWnd->ShowWindow(inshow);
	m_pResOverlayPositionOptWnd->ShowWindow(inshow);
}

/////////////////////////////////////////////////////////저조도검사(백점검사)
void CImageTesterDlg::CModelOpt_LowLight_Create(tINFO INFO){  
	CRect OptionRect;
	
	if(m_pModResTabWnd != NULL){
		m_pResLowLightOptWnd = new CLowLight_ResultView(this);
		m_pResLowLightOptWnd->Setup(this);
		m_pResLowLightOptWnd->Create(CLowLight_ResultView::IDD,m_pModResTabWnd);
		m_pResLowLightOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResLowLightOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
		m_pResLowLightOptWnd->ShowWindow(SW_HIDE);
	}
	
	if(m_pModOptTabWnd != NULL){
		m_pLowLightOptWnd = new CLowLight_Option(this);
		m_pLowLightOptWnd->Setup(this,m_pTStat,INFO);
		m_pLowLightOptWnd->Create(CLowLight_Option::IDD,m_pModOptTabWnd);
		m_pLowLightOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pLowLightOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pLowLightOptWnd->ShowWindow(SW_HIDE);
	}
	
}

void CImageTesterDlg::CModelOpt_LowLight_Delete(){
	
	if(m_pLowLightOptWnd != NULL){
		m_pLowLightOptWnd->DestroyWindow();
		delete m_pLowLightOptWnd;
		m_pLowLightOptWnd = NULL;	
	}
	
	if(m_pResLowLightOptWnd != NULL){
		m_pResLowLightOptWnd->DestroyWindow();
		delete m_pResLowLightOptWnd;
		m_pResLowLightOptWnd = NULL;	
	}
	
}

void CImageTesterDlg::CModelOpt_LowLight_ShowWindow(int inshow){
	m_pLowLightOptWnd->ShowWindow(inshow);
	m_pResLowLightOptWnd->ShowWindow(inshow);
}
/////////////////////////////////////////////////////////////////////////////////// Verify
void CImageTesterDlg::CModelOpt_Verify_Create(tINFO INFO){  
	CRect OptionRect;

	if(m_pModOptTabWnd != NULL){
		m_pVerifyOptWnd = new COption_Verify(this);
		m_pVerifyOptWnd->Setup(this,m_pTStat,INFO);
		m_pVerifyOptWnd->Create(COption_Verify::IDD,m_pModOptTabWnd);
		m_pVerifyOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pVerifyOptWnd->MoveWindow(10,45,OptionRect.Width(),OptionRect.Height());
		m_pVerifyOptWnd->ShowWindow(SW_HIDE);
	}

 	if(m_pModResTabWnd != NULL){
 		m_pResVerifyOptWnd = new CVerify_ResultView(this);
 		m_pResVerifyOptWnd->Setup(this);
 		m_pResVerifyOptWnd->Create(CVerify_ResultView::IDD,m_pModResTabWnd);
 		m_pResVerifyOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
 		m_pResVerifyOptWnd->MoveWindow(5,25,OptionRect.Width(),OptionRect.Height());
 		m_pResVerifyOptWnd->ShowWindow(SW_HIDE);
 	}
}

void CImageTesterDlg::CModelOpt_Verify_Delete(){

	if(m_pVerifyOptWnd != NULL){
		m_pVerifyOptWnd->DestroyWindow();
		delete m_pVerifyOptWnd;
		m_pVerifyOptWnd = NULL;	
	}
 
 	if(m_pResVerifyOptWnd != NULL){
 		m_pResVerifyOptWnd->DestroyWindow();
 		delete m_pResVerifyOptWnd;
 		m_pResVerifyOptWnd = NULL;	
 	}

}

void CImageTesterDlg::CModelOpt_Verify_ShowWindow(int inshow){
	m_pVerifyOptWnd->ShowWindow(inshow);
	m_pResVerifyOptWnd->ShowWindow(inshow);
}
//////////////////////////////////////////////////////////////


void CImageTesterDlg::CModelOpt_3D_Depth_Create(tINFO INFO){
	CRect OptionRect;
// 
 	if (m_pModResTabWnd != NULL){
		m_pRes3D_DepthOptWnd = new C_3D_Depth_ResultView(this);
		m_pRes3D_DepthOptWnd->Setup(this);
		m_pRes3D_DepthOptWnd->Create(C_3D_Depth_ResultView::IDD, m_pModResTabWnd);
		m_pRes3D_DepthOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pRes3D_DepthOptWnd->MoveWindow(5, 25, OptionRect.Width(), OptionRect.Height());
		m_pRes3D_DepthOptWnd->ShowWindow(SW_SHOW);
 	}

	if (m_pModOptTabWnd != NULL){
		m_p3D_DepthOptWnd = new C_3D_Depth_Option(this);
		m_p3D_DepthOptWnd->Setup(this, m_pTStat, INFO);
		m_p3D_DepthOptWnd->Create(C_3D_Depth_Option::IDD, m_pModOptTabWnd);
		m_p3D_DepthOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_p3D_DepthOptWnd->MoveWindow(10, 45, OptionRect.Width(), OptionRect.Height());
		m_p3D_DepthOptWnd->ShowWindow(SW_HIDE);
	}

}

void CImageTesterDlg::CModelOpt_3D_Depth_Delete(){

	if (m_p3D_DepthOptWnd != NULL){
		m_p3D_DepthOptWnd->DestroyWindow();
		delete m_p3D_DepthOptWnd;
		m_p3D_DepthOptWnd = NULL;
	}

	if (m_pRes3D_DepthOptWnd != NULL){
		m_pRes3D_DepthOptWnd->DestroyWindow();
		delete m_pRes3D_DepthOptWnd;
		m_pRes3D_DepthOptWnd = NULL;
	}

}

void CImageTesterDlg::CModelOpt_3D_Depth_ShowWindow(int inshow){
	m_p3D_DepthOptWnd->ShowWindow(inshow);

	m_pRes3D_DepthOptWnd->ShowWindow(inshow);
}


void CImageTesterDlg::CModelOpt_HotPixel_Create(tINFO INFO)
{
	CRect OptionRect;

	if (m_pModResTabWnd != NULL)
	{
		m_pResHotPixelOptWnd = new CHotPixel_ResultView(this);
		m_pResHotPixelOptWnd->Setup(this);
		m_pResHotPixelOptWnd->Create(CHotPixel_ResultView::IDD, m_pModResTabWnd);
		m_pResHotPixelOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResHotPixelOptWnd->MoveWindow(5, 25, OptionRect.Width(), OptionRect.Height());
		m_pResHotPixelOptWnd->ShowWindow(SW_SHOW);
	}

	if (m_pModOptTabWnd != NULL)
	{
		m_pHotPixel_OptWnd = new CHotPixel_Option(this);
		m_pHotPixel_OptWnd->Setup(this, m_pTStat, INFO);
		m_pHotPixel_OptWnd->Create(CHotPixel_Option::IDD, m_pModOptTabWnd);
		m_pHotPixel_OptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pHotPixel_OptWnd->MoveWindow(10, 45, OptionRect.Width(), OptionRect.Height());
		m_pHotPixel_OptWnd->ShowWindow(SW_HIDE);
	}
}

void CImageTesterDlg::CModelOpt_HotPixel_Delete()
{
	if (m_pResHotPixelOptWnd != NULL)
	{
		m_pResHotPixelOptWnd->DestroyWindow();
		delete m_pResHotPixelOptWnd;
		m_pResHotPixelOptWnd = NULL;
	}

	if (m_pHotPixel_OptWnd != NULL)
	{
		m_pHotPixel_OptWnd->DestroyWindow();
		delete m_pHotPixel_OptWnd;
		m_pHotPixel_OptWnd = NULL;
	}
}

void CImageTesterDlg::CModelOpt_HotPixel_ShowWindow(int inshow)
{
	m_pHotPixel_OptWnd->ShowWindow(inshow);
	m_pResHotPixelOptWnd->ShowWindow(inshow);
}

void CImageTesterDlg::CModelOpt_DepthNoise_Create(tINFO INFO)
{
	CRect OptionRect;

	if (m_pModResTabWnd != NULL)
	{
		m_pResDepthNoiseOptWnd = new CDepthNoise_ResultView(this);
		m_pResDepthNoiseOptWnd->Setup(this);
		m_pResDepthNoiseOptWnd->Create(CDepthNoise_ResultView::IDD, m_pModResTabWnd);
		m_pResDepthNoiseOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResDepthNoiseOptWnd->MoveWindow(5, 25, OptionRect.Width(), OptionRect.Height());
		m_pResDepthNoiseOptWnd->ShowWindow(SW_SHOW);
	}

	if (m_pModOptTabWnd != NULL)
	{
		m_pDepthNoise_OptWnd = new CDepthNoise_Option(this);
		m_pDepthNoise_OptWnd->Setup(this, m_pTStat, INFO);
		m_pDepthNoise_OptWnd->Create(CDepthNoise_Option::IDD, m_pModOptTabWnd);
		m_pDepthNoise_OptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pDepthNoise_OptWnd->MoveWindow(10, 45, OptionRect.Width(), OptionRect.Height());
		m_pDepthNoise_OptWnd->ShowWindow(SW_HIDE);
	}

}

void CImageTesterDlg::CModelOpt_DepthNoise_Delete()
{
	if (m_pResDepthNoiseOptWnd != NULL)
	{
		m_pResDepthNoiseOptWnd->DestroyWindow();
		delete m_pResDepthNoiseOptWnd;
		m_pResDepthNoiseOptWnd = NULL;
	}

	if (m_pDepthNoise_OptWnd != NULL)
	{
		m_pDepthNoise_OptWnd->DestroyWindow();
		delete m_pDepthNoise_OptWnd;
		m_pDepthNoise_OptWnd = NULL;
	}
}

void CImageTesterDlg::CModelOpt_DepthNoise_ShowWindow(int inshow)
{
	m_pDepthNoise_OptWnd->ShowWindow(inshow);
	m_pResDepthNoiseOptWnd->ShowWindow(inshow);
}


void CImageTesterDlg::CModelOpt_Temperature_Create(tINFO INFO)
{
	CRect OptionRect;

	if (m_pModResTabWnd != NULL)
	{
		m_pResTemperatureOptWnd = new CTemperature_ResultView(this);
		m_pResTemperatureOptWnd->Setup(this);
		m_pResTemperatureOptWnd->Create(CTemperature_ResultView::IDD, m_pModResTabWnd);
		m_pResTemperatureOptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pResTemperatureOptWnd->MoveWindow(5, 25, OptionRect.Width(), OptionRect.Height());
		m_pResTemperatureOptWnd->ShowWindow(SW_SHOW);
	}

	if (m_pModOptTabWnd != NULL)
	{
		m_pTemperature_OptWnd = new Temperature_Option(this);
		m_pTemperature_OptWnd->Setup(this, m_pTStat, INFO);
		m_pTemperature_OptWnd->Create(Temperature_Option::IDD, m_pModOptTabWnd);
		m_pTemperature_OptWnd->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		m_pTemperature_OptWnd->MoveWindow(10, 45, OptionRect.Width(), OptionRect.Height());
		m_pTemperature_OptWnd->ShowWindow(SW_HIDE);
	}

}

void CImageTesterDlg::CModelOpt_Temperature_Delete()
{
	if (m_pResTemperatureOptWnd != NULL)
	{
		m_pResTemperatureOptWnd->DestroyWindow();
		delete m_pResTemperatureOptWnd;
		m_pResTemperatureOptWnd = NULL;
	}

	if (m_pTemperature_OptWnd != NULL)
	{
		m_pTemperature_OptWnd->DestroyWindow();
		delete m_pTemperature_OptWnd;
		m_pTemperature_OptWnd = NULL;
	}
}

void CImageTesterDlg::CModelOpt_Temperature_ShowWindow(int inshow)
{
	m_pTemperature_OptWnd->ShowWindow(inshow);
	m_pResTemperatureOptWnd->ShowWindow(inshow);
}
#pragma endregion

#pragma region TEST

tResultVal	CImageTesterDlg:: Model_Start()
{  
	tResultVal retval = {0,};
	retval.m_ID = 0xfe;
	m_TOTAL_SUCCESS = FALSE;

	m_pTStat ->SetWindowText("");//EDIT창을 클리어한다. 
	AutoPortChk_Cam();
	if (b_luritechDebugMODE == 0){
		Power_Total_Control();
	}

	if(b_StartCommand == TRUE){
		RUN_LED_CONTROL(0);

		ALARM_OPERATING(1);
	}

	for (int t = 0; t < 5; t++)
	{
		m_stMes[t].Reset();
	}
	//-------------------------------------
	//셋팅된 위치로 지그를 움직인다.
	//-------------------------------------	
 	if(!JIG_MOVE())//모델 스타트시 지그를 앞으로 움직여야 하는 이유는?
 	{
 		retval.m_Success = FALSE;
 		retval.ValString.Format("JIG MOVE Fail");
 		StatePrintf("JIG Move Fail!");
 		if(b_StartCommand == TRUE){
 			MainWork_List.SetItemText(InsertIndex,4,"FAIL");
 			if((b_Chk_CurrentChkEn == TRUE)&&(b_Chk_Volton == TRUE)){//전류측정 
 				FAIL_UPLOAD();
 			}
			if ((m_stEEPROM.bCheckSumCheck == TRUE) || (m_stEEPROM.bCheckSumCheck == TRUE)){//전류측정 
				FAIL_UPLOAD_Eeprom();
			}
 		}
 		return retval;
 	}

	//Light_ONOFF(0);
	//LogWriteToFile("MODEL_Start_1 Light_ONOFF(0);");
	//m_Motion.OriginStandbyPosMove();
	//LogWriteToFile("MODEL_Start_2 OriginStandbyPosMove");
	//m_Motion.CylinderOut();

	if(b_Chk_Volton == TRUE){
		if(!Power_On()){//파워on을 실패할 경우
			retval.m_Success = FALSE;
			retval.ValString.Format("Power On Fail");
			StatePrintf("Power On Fail!");
			if(b_StartCommand == TRUE){
				MainWork_List.SetItemText(InsertIndex,5,"FAIL");
				if((b_Chk_CurrentChkEn == TRUE)&&(b_Chk_Volton == TRUE)){//전류측정 
					FAIL_UPLOAD();
				}

				if ((m_stEEPROM.bCheckSumCheck == TRUE) || (m_stEEPROM.bCheckSumCheck == TRUE)){//전류측정 
					FAIL_UPLOAD_Eeprom();
				}
			}
			return retval;
		}
	}
	
	
	TestData[0].m_Enable = 1;

	if(b_StartCommand == TRUE)
	{
		RUN_LED_CONTROL(1);
	}
	//-2D에서 적혀진 eeprom Read
	if (m_stEEPROM.bCheckSumCheck || m_stEEPROM.bReadCheck)
	{
		if (!PreEEPROM_CHECK()){
			retval.m_Success = FALSE;
			retval.ValString.Format("Eeprom Check Fail");
			StatePrintf("Eeprom Check Fail!");
			if (b_StartCommand == TRUE){
				MainWork_List.SetItemText(InsertIndex, 5, "FAIL");
				if ((b_Chk_CurrentChkEn == TRUE) && (b_Chk_Volton == TRUE)){//전류측정 
					FAIL_UPLOAD();
				}
			}
			return retval;
		}
		if (LotMod == 1){
			Lot_StartCnt_Eeprom++;
		}
		else{
			StartCnt_Eeprom++;
		}
	}







	CString str_buf = "";
	CString stateDATA ="전류_ ";
	if((b_Chk_CurrentChkEn == TRUE)&&(b_Chk_Volton == TRUE)) //전류측정
	{ 
		if (b_StartCommand == TRUE)
			InsertList_C();
				
		i_PogoCnt++;
		SetCountUpdate();

		if(Current_Detect() == TRUE)
		{
		//	for (int _a = 0; _a < 4; _a++)
			{
				stateDATA += "동작전류_";
				// 2016.08.29 -------------------------------------------
				// 보임량 광축 검사를 하는 경우
				// 영상 신호 없으면 Sector Write
				for (int lop = 0; lop <10; lop++)
				{
					if (m_ModelInfo[lop].ID == 0x03)
					{
						if (Current_Detect() == FALSE)
						{
							if (b_StartCommand == TRUE)
							{
								m_CurrList.SetItemText(InsertIndex_C, 4, "X");
								m_CurrList.SetItemText(InsertIndex_C, 5, "X");
								m_CurrList.SetItemText(InsertIndex_C, 6, "X");
								m_CurrList.SetItemText(InsertIndex_C, 7, "X");
								m_CurrList.SetItemText(InsertIndex_C, 8, "X");
								m_CurrList.SetItemText(InsertIndex_C, 9, "FAIL");
								MainWork_List.SetItemText(InsertIndex, WORKLIST_CURRENT, "FAIL");
							}

							StatePrintf("Current Detect Fail!");
							m_pResStandOptWnd->CurrState_Val(1, "FAIL");//FAIL 빨강색
							retval.m_Success = FALSE;
							retval.ValString.Format("Current Detect Fail!");
							if (LotMod == 1) {
								MainWork_List.SetItemText(InsertIndex, WORKLIST_CURRENT, "FAIL");
							}
							stateDATA += "X _ FAIL";
							StatePrintf(stateDATA);

							return retval;
						}

						if (m_CurrentVal5 < 1.0 && m_CurrentVal4 < 1.0&& m_CurrentVal3 < 1.0&& m_CurrentVal2 < 1.0&& m_CurrentVal < 1.0)
						{
							break;
						}


						if (m_CurrentVal5  > 0.9)
						{
							break;
						}
						DoEvents(1);

					//	break;
					}
				//	DoEvents(50);
				}
				// End 2016.08.29 ---------------------------------------
			/*	if (m_CurrentVal5 < 1.0 && m_CurrentVal4 < 1.0&& m_CurrentVal3 < 1.0&& m_CurrentVal2 < 1.0&& m_CurrentVal < 1.0)
				{

				}
				else{
					if (m_CurrentVal5 < 0.9)
					{
						double dData = (rand() % 10)* 0.1;
						m_CurrentVal5 = 1.0 + dData;
					}
				}*/


				int PassCount = 0;
				if ((m_CurrentVal >= m_MinAmp) && (m_CurrentVal <= m_MaxAmp)){
					PassCount++;
				}
				if ((m_CurrentVal2 >= m_MinAmp2) && (m_CurrentVal2 <= m_MaxAmp2)){
					PassCount++;
				}
				if ((m_CurrentVal3 >= m_MinAmp3) && (m_CurrentVal3 <= m_MaxAmp3)){
					PassCount++;
				}
				if ((m_CurrentVal4 >= m_MinAmp4) && (m_CurrentVal4 <= m_MaxAmp4)){
					PassCount++;
				}
				if ((m_CurrentVal5 >= m_MinAmp5) && (m_CurrentVal5 <= m_MaxAmp5)){
					PassCount++;
				}
				str_buf.Format("1.8V :%3.1f mA, 3.3V :%3.1f mA, 9.0V :%3.1f mA, 14.7V :%3.1f mA, -5.7V :%3.1f mA", m_CurrentVal, m_CurrentVal2, m_CurrentVal3, m_CurrentVal4, m_CurrentVal5);
				stateDATA = str_buf;

				str_buf.Format("%d / 5_PASS", PassCount);
				retval.ValString = str_buf;

				if (PassCount == 5)
				{
					m_pResStandOptWnd->CurrState_Val(0, str_buf);//PASS 판란색
					retval.m_Success = TRUE;
					stateDATA += /*str_buf +*/"_ PASS";

					if (b_StartCommand == TRUE)
					{
						m_CurrList.SetItemText(InsertIndex_C, 9, "PASS");
						MainWork_List.SetItemText(InsertIndex, WORKLIST_CURRENT, "PASS");

						str_buf.Format("%3.1f", m_CurrentVal);
						m_CurrList.SetItemText(InsertIndex_C, 4, str_buf);
						str_buf.Format("%3.1f", m_CurrentVal2);
						m_CurrList.SetItemText(InsertIndex_C, 5, str_buf);
						str_buf.Format("%3.1f", m_CurrentVal3);
						m_CurrList.SetItemText(InsertIndex_C, 6, str_buf);
						str_buf.Format("%3.1f", m_CurrentVal4);
						m_CurrList.SetItemText(InsertIndex_C, 7, str_buf);
						str_buf.Format("%3.1f", m_CurrentVal5);
						m_CurrList.SetItemText(InsertIndex_C, 8, str_buf);
					}
					//break;
				}
				else
				{
					m_pResStandOptWnd->CurrState_Val(1, str_buf);//FAIL 빨강색
					retval.m_Success = FALSE;
					stateDATA += /*str_buf +*/"_ FAIL";

					if (b_StartCommand == TRUE)
					{
						m_CurrList.SetItemText(InsertIndex_C, 9, "FAIL");
						MainWork_List.SetItemText(InsertIndex, WORKLIST_CURRENT, "FAIL");
					}
				}

				if (b_StartCommand == TRUE)
				{
					str_buf.Format("%3.1f", m_CurrentVal);
					m_CurrList.SetItemText(InsertIndex_C, 4, str_buf);
					str_buf.Format("%3.1f", m_CurrentVal2);
					m_CurrList.SetItemText(InsertIndex_C, 5, str_buf);
					str_buf.Format("%3.1f", m_CurrentVal3);
					m_CurrList.SetItemText(InsertIndex_C, 6, str_buf);
					str_buf.Format("%3.1f", m_CurrentVal4);
					m_CurrList.SetItemText(InsertIndex_C, 7, str_buf);
					str_buf.Format("%3.1f", m_CurrentVal5);
					m_CurrList.SetItemText(InsertIndex_C, 8, str_buf);

				}
			}
		}else
		{

			if (b_StartCommand == TRUE){
				m_CurrList.SetItemText(InsertIndex_C, 4, "X");
				m_CurrList.SetItemText(InsertIndex_C, 5, "X");
				m_CurrList.SetItemText(InsertIndex_C, 6, "X");
				m_CurrList.SetItemText(InsertIndex_C, 7, "X");
				m_CurrList.SetItemText(InsertIndex_C, 8, "X");
				m_CurrList.SetItemText(InsertIndex_C, 9, "FAIL");
				MainWork_List.SetItemText(InsertIndex, WORKLIST_CURRENT, "FAIL");
			}
			
			StatePrintf("Current Detect Fail!");
			m_pResStandOptWnd->CurrState_Val(1,"FAIL");//FAIL 빨강색
			retval.m_Success = FALSE;
			retval.ValString.Format("Current Detect Fail!");
			MainWork_List.SetItemText(InsertIndex, WORKLIST_MODELSTART, "FAIL");
			stateDATA += "X _ FAIL";
			//StatePrintf(stateDATA);
			return retval;
		}
		if (LotMod == 1){
			Lot_StartCnt_C++;
		}
		else{
			StartCnt_C++;
		}
		

		// mes 데이터
		if (TRUE == retval.m_Success)
		{
			m_stMes[0].szData.Format(_T("%3.1f"), m_CurrentVal);
			m_stMes[1].szData.Format(_T("%3.1f"), m_CurrentVal2);
			m_stMes[2].szData.Format(_T("%3.1f"), m_CurrentVal3);
			m_stMes[3].szData.Format(_T("%3.1f"), m_CurrentVal4);
			m_stMes[4].szData.Format(_T("%3.1f"), m_CurrentVal5);

			if ((m_CurrentVal >= m_MinAmp) && (m_CurrentVal <= m_MaxAmp))
				m_stMes[0].nResult = TEST_OK;
			else
				m_stMes[0].nResult = TEST_NG;

			if ((m_CurrentVal2 >= m_MinAmp2) && (m_CurrentVal2 <= m_MaxAmp2))
				m_stMes[1].nResult = TEST_OK;
			else
				m_stMes[1].nResult = TEST_NG;

			if ((m_CurrentVal3 >= m_MinAmp3) && (m_CurrentVal3 <= m_MaxAmp3))
				m_stMes[2].nResult = TEST_OK;
			else
				m_stMes[2].nResult = TEST_NG;

			if ((m_CurrentVal4 >= m_MinAmp4) && (m_CurrentVal4 <= m_MaxAmp4))
				m_stMes[3].nResult = TEST_OK;
			else
				m_stMes[3].nResult = TEST_NG;

			if ((m_CurrentVal5 >= m_MinAmp5) && (m_CurrentVal5 <= m_MaxAmp5))
				m_stMes[4].nResult = TEST_OK;
			else
				m_stMes[4].nResult = TEST_NG;
		}
		else
		{

		}
	}else{
		i_PogoCnt++;
		SetCountUpdate();
		retval.m_Success = TRUE;
		retval.ValString.Format("Model Start Success!");
	}

	DoEvents(500);
	StatePrintf(stateDATA);
	MainWork_List.SetItemText(InsertIndex, WORKLIST_MODELSTART, "PASS");
	str_FailName = "";
	m_AddFinalMesResult = "";
	m_ScanState = FALSE;
	return retval;
}

void CImageTesterDlg::Model_Start_Pic(CDC *cdc,tResultVal *result)
{
	
	CPen	my_Pan,*old_pan;
	CString TEXTDATA ="";
	CFont m_font;
	
	if((b_Chk_CurrentChkEn == FALSE)||(b_Chk_Volton == FALSE)){//전류측정 
		return;
	}

	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	
	if(result->m_Success  == TRUE){
		my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(BLUE_COLOR);
	}else{
		my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(RED_COLOR);
	}
    
	m_font.CreatePointFont(260,"Arial");
	cdc->SelectObject(&m_font);
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);

	TEXTDATA = result->ValString;

	cdc->TextOut(CAM_IMAGE_WIDTH/2,CAM_IMAGE_HEIGHT-100,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
	//	cdc->TextOut(400,400,strBuf.GetBuffer(0),strBuf.GetLength());

	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();
	m_font.DeleteObject();
}

tResultVal	CImageTesterDlg:: Model_End(int Number)
{
	tResultVal retval = {0,};

	int count=0;
	int OKcount=0;
	int FAILcount=0;
	
	LogWriteToFile("MODEL_END 1");

	Power_Off();
	DoEvents(200);
	LogWriteToFile("MODEL_END 1 Power_Off");

	//Light_ONOFF(0);
	Control_Current_SLOT(3, 0);

	LogWriteToFile("MODEL_END 2 Light_ONOFF(0);");

	if (b_luritechDebugMODE == 0){
		m_Motion.OriginStandbyPosMove();
		m_Motion.CylinderOut();
	}
	LogWriteToFile("MODEL_END 3 OriginStandbyPosMove");
	
	for(int i= 0;i<Number;i++){
		if(m_psubmodel[i] != NULL){
			if((TestData[i].m_Success != 3) && (TestData[i].m_Success != 0)){
				if(TestData[i].m_Success == 1){
					OKcount++;
					if(LotMod ==1){
						m_Lot_MainWorkList.SetItemText(Lot_InsertIndex,i+6,"PASS");
					}
				}
				else{
					FAILcount++;
					if(LotMod ==1){
						m_Lot_MainWorkList.SetItemText(Lot_InsertIndex,i+6,"FAIL");
					}
				}
				count++;				
			}
		}
	}
	LogWriteToFile("MODEL_END 5 결과 산출");

	if(count == OKcount)
	{	
		retval.m_Success = TRUE;
		m_psubmodel[Number]->TestState(0,"PASS");
		m_szMesPacket[TOTAL_RESULT] = _T("1");

		TEST_STAT(1,"PASS");
		retval.ValString="MODEL SUCCESS";
		if(b_StartCommand == TRUE)
		{
			MainWork_List.SetItemText(InsertIndex, TEST_ITEM_NUM, "PASS");
			MainWork_List.SetItemText(InsertIndex, TEST_ITEM_NUM+1, "PASS");

			RUN_LED_CONTROL(2);
			LogWriteToFile("MODEL_END 6 RUN_LED_CONTROL");

			ALARM_OPERATING(2);
			LogWriteToFile("MODEL_END 6 ALARM_OPERATING");
			BUZZER_OPERATING(1);
			LogWriteToFile("MODEL_END 6 BUZZER_OPERATING");

		}
		if(LotMod ==1){
			m_Lot_MainWorkList.SetItemText(Lot_InsertIndex,count+6,"PASS");
			m_Lot_MainWorkList.SetItemText(Lot_InsertIndex,count+7,"PASS");
			Lot_Passint++;
		}
	}else
	{
		retval.m_Success = FALSE;
		m_psubmodel[Number]->TestState(1,"FAIL");
		m_szMesPacket[TOTAL_RESULT] = _T("0");

		TEST_STAT(2,"FAIL");
		retval.ValString="MODEL FAIL";
		if(b_StartCommand == TRUE){
			MainWork_List.SetItemText(InsertIndex, TEST_ITEM_NUM, "FAIL");
			MainWork_List.SetItemText(InsertIndex, TEST_ITEM_NUM+1, "FAIL");
			
			RUN_LED_CONTROL(3);
			LogWriteToFile("MODEL_END 6 RUN_LED_CONTROL");
			ALARM_OPERATING(3);
			LogWriteToFile("MODEL_END 6 ALARM_OPERATING");
			BUZZER_OPERATING(2);
			LogWriteToFile("MODEL_END 6 BUZZER_OPERATING");
		}
		if(LotMod ==1){
			m_Lot_MainWorkList.SetItemText(Lot_InsertIndex,count+6,"FAIL");
			m_Lot_MainWorkList.SetItemText(Lot_InsertIndex,count+7,"FAIL");
			Lot_Failint++;
		}
	}


	//비어있는 리스트 X자 넣기.
	if(b_StartCommand == TRUE){
		EmptyList_Input_X_Data(&MainWork_List);
	}

	LogWriteToFile("MODEL_END 7 LOT,MES DATA 넣기");

	m_psubmodel[Number]->TextView(retval.ValString);
	LogWriteToFile("MODEL_END 8 TextView");


	if((LotMod ==1) && (b_UserMode == FALSE)){
		
		LogWriteToFile("MODEL_END 9 LOT관련");

		for(int lop=0; lop<20; lop++){
			if(m_psubmodel[lop] != NULL)
			{
				m_psubmodel[lop]->Lot_InsertDataList();
				m_psubmodel[lop]->LOT_Excel_Save();
			}
		}
		LogWriteToFile("MODEL_END 9 LOT_Excel_Save");

			
		Lot_StartCnt++;
		Lot_Totalint = Lot_Passint+Lot_Failint;

		LOT_EXCEL_SAVE();
		LogWriteToFile("MODEL_END 10 LOT_EXCEL_SAVE");

		SETTING_LotInfo();
		LogWriteToFile("MODEL_END 11 SETTING_LotInfo");
	}

	
 	if(b_StartCommand == TRUE)
 	{
 		LogWriteToFile("MODEL_END 9 LOT관련");
 		SAVE_FILE();
 
 		int nNum = 0;
 		for(int lop=0; lop<17; lop++){
 			m_szMesPacket[3+lop] = "";
 		}
 
 		for(int lop=0; lop<17; lop++)
		{
 			if(m_psubmodel[lop] != NULL)
 			{
 				m_psubmodel[lop]->Excel_Save();
 				
 				if(lop == 0)
				{
					m_szMesPacket[MES_TEST1] = m_psubmodel[lop]->Mes_Result();
					
					if (m_szMesPacket[MES_TEST1] == "")
					{
						m_szMesPacket[MES_TEST1] = " ";
 					}
 				}else
				{
					m_szMesPacket[MES_TEST1 + nNum] = m_psubmodel[lop]->Mes_Result();
 				}
 				nNum++;
 			}else
 			{
 				break;
 			}
 		}
  		FileCopy_AnotherPath_Func();
  	
  		StartCnt++;


		// MES 파일로 저장
		if (b_Chk_MESEn == TRUE && b_UserMode == FALSE)
		{
			MesSavFileGen(14);
			MESRunStop();
		}
  	}

	DoEvents(100);
	

	return retval;
}

void CImageTesterDlg::MesSavFileGen(int nNum)
{
	CTime timedata = CTime::GetCurrentTime();
	CString strtime = _T("");
	strtime.Format(_T("%0.4d%0.2d%0.2d%0.2d%0.2d%0.2d"),timedata.GetYear(),timedata.GetMonth(),timedata.GetDay(),timedata.GetHour(),timedata.GetMinute(),timedata.GetSecond());
	CString filename = m_szCode+_T("_")+m_strdegre+_T("_")+strtime+_T(".txt");
	CString str_Mes = MESPath +_T("\\")+filename;
	CString str_Mes_P=MODEL_FOLDERPATH_P +_T("\\")+filename;

	CFile MESFILE_P;	
	CFile MESFILE;

	if(b_Particle_fail==TRUE)
	{
		folder_gen(MODEL_FOLDERPATH_P);
		if(!MESFILE_P.Open(str_Mes_P,CFile::modeCreate|CFile::modeWrite))
			return;
	}

	folder_gen(MESPath);
	if(!MESFILE.Open(str_Mes,CFile::modeCreate|CFile::modeWrite))
		return;
	
	if(m_szMesPacket[LOT_ID] == _T(""))
		m_szMesPacket[LOT_ID] = _T("0");
	
	CString szTemp = _T("");
	
	CString str_value = _T("");


#if 0
	for (int _a = 0; _a < 2; _a++)//0과 1은 그냥 쓴다.
	{
		if (_a != 0)
		{
			str_value += _T(",");
		}

		szTemp = m_szMesPacket[_a];
		str_value += szTemp;
	}

	for(int _a=0; _a<nNum; _a++)
	{
		if(m_szMesPacket[_a+2] != "")
		{
			str_value += _T(",");
			szTemp = m_szMesPacket[_a+2];
			str_value += szTemp;
		}
	}
#else
	for (int _a = 0; _a < 3; _a++)//0, Lot ID 1, 차수, 3 , 최종 결과 
	{
		if (_a != 0)
		{
			str_value += _T(",");
		}

		szTemp = m_szMesPacket[_a];
		str_value += szTemp;
	}


	int eepromNUM = NewMES_PREEEPROM;
	for (; eepromNUM < MAX_NewMES_NUM; eepromNUM++)
	{
		//m_stNewMesData[_a] 이 데이터는 각 Test에 MesResult 함수에서 생성 된다.

		if (m_stNewMesData[eepromNUM].IsEmpty())//데이터가 비어 있으면 Test를 하지 않는 항목으로 데이터 값이 없는 :1, 갯수 만큼 추가 한다.
		{
			for (int m = 0; m < m_nNewMesDataNum[eepromNUM]; m++)//마지막 데이터에는 콤마를 붙이지 않는다.
			{
				m_stNewMesData[eepromNUM] += _T(":1");

				if (m != (m_nNewMesDataNum[eepromNUM] - 1))
				{
					m_stNewMesData[eepromNUM] += _T(",");
				}
			}
		}

		str_value += _T(",");
		szTemp = m_stNewMesData[eepromNUM];
		str_value += szTemp;
	}
#endif
// 	for (int _a = 0; _a<nNum; _a++)
// 	{
// 		if (m_szMesPacket[_a + 2] != "")
// 		{
// 			str_value += _T(",");
// 			szTemp = m_szMesPacket[_a + 2];
// 			str_value += szTemp;
// 		}
// 	}



	str_value +=_T("\r\n");
	MESFILE.Write(str_value,str_value.GetLength());
	MESFILE.Close();

	if(b_Particle_fail==TRUE){
		MESFILE_P.Write(str_value,str_value.GetLength());
		MESFILE_P.Close();
	}
}

void CImageTesterDlg::SaveScan(HWND hWnd, LPCTSTR lpszFileName)
{
	HDC hDC = ::GetDC(hWnd);

	// 윈도우의 크기 알아내기
	RECT rc;
	::GetClientRect(hWnd, &rc);

	// 비트맵(DDB) 생성하기
	HDC hMemDC = CreateCompatibleDC(hDC);
	HBITMAP hBitmap = CreateCompatibleBitmap(hDC, rc.right, rc.bottom);
	HBITMAP hBmpOld = (HBITMAP)SelectObject(hMemDC, hBitmap);
	BitBlt(hMemDC, 0, 0, rc.right, rc.bottom, hDC, 0, 0, SRCCOPY);
	SelectObject(hMemDC, hBmpOld);
	DeleteDC(hMemDC);

	// 비트맵(DIB) 사양 설정
	BITMAPINFOHEADER bmih;
	ZeroMemory(&bmih, sizeof(BITMAPINFOHEADER));
	bmih.biSize = sizeof(BITMAPINFOHEADER);
	bmih.biWidth = rc.right;
	bmih.biHeight = rc.bottom;
	bmih.biPlanes = 1;
	bmih.biBitCount = 24;
	bmih.biCompression = BI_RGB;

	// 비트맵(DIB) 데이터 추출
	GetDIBits(hDC, hBitmap, 0, rc.bottom, NULL, (LPBITMAPINFO)&bmih, DIB_RGB_COLORS);
//	LPBYTE lpBits = new BYTE[bmih.biSizeImage];
	GetDIBits(hDC, hBitmap, 0, rc.bottom, m_pFailScan, (LPBITMAPINFO)&bmih, DIB_RGB_COLORS);
	::ReleaseDC(hWnd, hDC);

	DeleteObject(hBitmap);

	///////

	// 비트맵 파일 헤더 설정
	BITMAPFILEHEADER bmfh;
	bmfh.bfType = 'MB';
	bmfh.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + bmih.biSizeImage;
	bmfh.bfReserved1 = 0;
	bmfh.bfReserved2 = 0;
	bmfh.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	// 비트맵 파일 생성 및 데이터 저장
	DWORD dwWritten;
	HANDLE hFile = CreateFile(lpszFileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,

		FILE_ATTRIBUTE_NORMAL, NULL);
	WriteFile(hFile, &bmfh, sizeof(BITMAPFILEHEADER), &dwWritten, NULL);
	WriteFile(hFile, &bmih, sizeof(BITMAPINFOHEADER), &dwWritten, NULL);
	WriteFile(hFile, m_pFailScan, bmih.biSizeImage, &dwWritten, NULL);
	CloseHandle(hFile);
		
//	delete [] lpBits;
}

void CImageTesterDlg::Input_Current_Detect()
{
	memset(m_SerialInput_Cam, 0, sizeof(m_SerialInput_Cam));
	BYTE DATA[30] ={'!','P','0','0','0',
					'0','0','0','0','0',
					'0', '0', '0', '0', '0',
					'0', '0', '0', '0', '0',
					'0', '0', '0', '0', '0',
					'0', '@' };

	m_CurrentVal = 0;
	Buf_Wait_Cam = TRUE;
	Current_Wait = TRUE;
	SendData8Byte_Cam(DATA, 27);
}

void CImageTesterDlg::CurrentCheck()
{	
	char BUFDATA[4] = {0,};
	char BUFDATA2[4] = { 0, };
	char BUFDATA3[4] = { 0, };
	char BUFDATA4[4] = { 0, };
	char BUFDATA5[4] = { 0, };
	
	BUFDATA[3] = m_SerialInput_Cam[5];
	BUFDATA[2] = m_SerialInput_Cam[4];
	BUFDATA[1] = m_SerialInput_Cam[3];
	BUFDATA[0] = m_SerialInput_Cam[2];

	double dData = atof(BUFDATA);
	if (Def_Current == 1)
	{
		dData *= 0.1;
	}
	m_CurrentVal = dData*m_Offset;

	BUFDATA2[3] = m_SerialInput_Cam[9];
	BUFDATA2[2] = m_SerialInput_Cam[8];
	BUFDATA2[1] = m_SerialInput_Cam[7];
	BUFDATA2[0] = m_SerialInput_Cam[6];
	dData = atof(BUFDATA2);
	if (Def_Current == 1)
	{
		dData *= 0.1;
	}
	m_CurrentVal2 = dData*m_Offset2;

	BUFDATA3[3] = m_SerialInput_Cam[13];
	BUFDATA3[2] = m_SerialInput_Cam[12];
	BUFDATA3[1] = m_SerialInput_Cam[11];
	BUFDATA3[0] = m_SerialInput_Cam[10];
	dData = atof(BUFDATA3);
	if (Def_Current == 1)
	{
		dData *= 0.1;
	}
	m_CurrentVal3 = dData*m_Offset3;

	BUFDATA4[3] = m_SerialInput_Cam[17];
	BUFDATA4[2] = m_SerialInput_Cam[16];
	BUFDATA4[1] = m_SerialInput_Cam[15];
	BUFDATA4[0] = m_SerialInput_Cam[14];
	dData = atof(BUFDATA4);
	if (Def_Current == 1)
	{
		dData *= 0.1;
	}
	m_CurrentVal4 = dData*m_Offset4;

	BUFDATA5[3] = m_SerialInput_Cam[21];
	BUFDATA5[2] = m_SerialInput_Cam[20];
	BUFDATA5[1] = m_SerialInput_Cam[19];
	BUFDATA5[0] = m_SerialInput_Cam[18];
	dData = atof(BUFDATA5);
	if (Def_Current == 1)
	{
		dData *= 0.1;

	}
	dData += 0.5;
	m_CurrentVal5 = dData*m_Offset5;

	Current_Wait = FALSE;
}

BOOL CImageTesterDlg::Current_Detect()
{	
	Buf_Wait_Cam = TRUE;
	Input_Current_Detect();
	for(int lop = 0;lop<150;lop++){
		DoEvents(10);
		if (m_SerialInput_Cam[1] == 'P' && Buf_Wait_Cam ==FALSE){

			CurrentCheck();
			return TRUE;
		}
	}
	return FALSE;
}


#pragma endregion


#pragma region TEST 시 활성/비활성


void CImageTesterDlg::RUN_MODE_CHK(bool Mode){//
	if(Mode == TRUE){
		((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(0);
	
		((CButton *)GetDlgItem(IDC_BTN_ETC_SETTING))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_USER_CHANGE))->EnableWindow(0);
	
	
		m_pTStat ->SetWindowText(""); 
		
		((CButton *)GetDlgItem(IDC_BTN_MASTER_SET))->EnableWindow(0);
		if(m_pMasterMonitor != NULL){
			if(m_pMasterMonitor->IsWindowVisible()){
				m_pMasterMonitor->ShowWindow(SW_HIDE);
			}
		}

		if(b_UserMode == TRUE){//전문가 모드일때
		//	m_pMainCombo->EnableWindow(0);
			((CButton *)GetDlgItem(IDC_BTN_TEST_SETTING))->EnableWindow(0);
			
			if(m_pModOptDlgWnd->IsWindowVisible()){
				m_pModOptDlgWnd->ShowWindow(SW_HIDE);
			}

		}else{
		//	m_pMainCombo->EnableWindow(1);
			((CButton *)GetDlgItem(IDC_BTN_TEST_SETTING))->EnableWindow(0);			
		}
	}else{
		((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BTN_STOP))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_USER_CHANGE))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BTN_MASTER_SET))->EnableWindow(1);
	
		if(b_UserMode == TRUE){
		//	m_pMainCombo->EnableWindow(1);
			((CButton *)GetDlgItem(IDC_BTN_TEST_SETTING))->EnableWindow(1);
			((CButton *)GetDlgItem(IDC_BTN_ETC_SETTING))->EnableWindow(1);

		}else{
		//	m_pMainCombo->EnableWindow(0);

			((CButton *)GetDlgItem(IDC_BTN_TEST_SETTING))->EnableWindow(0);
			((CButton *)GetDlgItem(IDC_BTN_ETC_SETTING))->EnableWindow(0);
		}
	}

	//((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(1);

}

#pragma endregion


#pragma region 소켓 통신 함수


void CImageTesterDlg::SocketConnect()
{
	bool bSuccess;
	CString strMsg = _T("");
	m_nSockType = 0;
	if(m_SocketManager.IsStart()){//스레드가 이미 존재한다면 빠져나온다.
		if(m_SocketManager.IsOpen())
		{
			strMsg = _T("Already Connected to the server: ") + m_szIp;
			strMsg += _T(" on port ") + m_szPort;
			StatePrintf(strMsg);
		}
		return;
	}

	if (m_nSockType == SOCK_TCP)
		bSuccess = m_SocketManager.ConnectTo(m_szIp, m_szPort, AF_INET, SOCK_STREAM); // TCP
	else
		bSuccess = m_SocketManager.ConnectTo(m_szIp, m_szPort, AF_INET, SOCK_DGRAM); // UDP

	if (bSuccess && m_SocketManager.WatchComm())
	{
		if (m_nSockType == SOCK_TCP)
		{
			strMsg = _T("Connection established with server: ") + m_szIp;
			strMsg += _T(" on port ") + m_szPort;
			m_SocketManager.GetPeerName( m_SockPeer );
		}
		else
		{
			SockAddrIn addrin;
			m_SocketManager.GetSockName( addrin ); // just to get our current address
			LONG  uAddr = addrin.GetIPAddr();
			BYTE* sAddr = (BYTE*) &uAddr;
			int nPort = ntohs( addrin.GetPort() );
			CString strAddr;
			// Address is stored in network format...
			strAddr.Format(_T("IP: %u.%u.%u.%u, Port: %d"),
				(UINT)(sAddr[0]), (UINT)(sAddr[1]),
				(UINT)(sAddr[2]), (UINT)(sAddr[3]), nPort);
			strMsg = _T("Socket created: ") + strAddr;
			m_SockPeer.CreateFrom( m_szIp, m_szPort, AF_INET);
		}
		StatePrintf(strMsg);
		//		MESCONNECT_STATE(1,"CONNECTING");
		//		if(b_MesRunEn == FALSE){
		//			MESMONITOR_VIEW(3,"Stand By Receive Data");
		//		}else{
		//			MESMONITOR_VIEW(0,"Receive Data Success");
		//		}
		b_AutoMesConnect = FALSE;
	}else{
		StatePrintf("Failed to Connect server");
		//		MESCONNECT_STATE(0,"DISCONNECT");
		//		MESMONITOR_VIEW(1,"Failed to Connect server");
		b_AutoMesConnect = TRUE;
	}
}

void CImageTesterDlg::SocketDisconnect()
{
	m_SocketManager.StopComm();
	if (!m_SocketManager.IsOpen())
	{
//		MESCONNECT_STATE(0,"DISCONNECT");
	}
}

LRESULT CImageTesterDlg::OnTCPReceive(WPARAM wParam, LPARAM lParam)
{
	if(b_StartCommand == TRUE) return 0;

	if (b_DoorOpen == FALSE)
	{
		AfxMessageBox("Open the socket. Change the camera");
		return 0;
	}

	CString str_Redata = _T("");
	int nIndex = 0;
	CString szTemp = _T("");
	CSocketManager* pManager = reinterpret_cast<CSocketManager*>( lParam );

	if (m_nSockType != SOCK_TCP)
		return 0L;

	if ( pManager != NULL)
	{
		str_Redata = pManager->str_Redata;
		szTemp.Format(_T("MES Recv : %s"), str_Redata);
		StatePrintf(szTemp);

		CString strdata[10] = {"","","","","","","","","",""};

		int tnum = 0;
		CString str_buf = str_Redata;
		LPTSTR pszdata = str_buf.GetBuffer(0);
		TCHAR * token = _tcstok(pszdata, "*");
		while(token)
		{
			if(tnum < 10){
				strdata[tnum] = token;
			}else{
				break;
			}
			token = _tcstok(NULL, "*");
			tnum++;
		}

		if((strdata[1] != "")&&(strdata[2] != ""))
		{
			szTemp.Format(_T("MES Message Recv : %s"), str_Redata);
			StatePrintf(szTemp);

			m_strLotIdFull = strdata[1];			// LOT ID
			m_strdegre = strdata[2];				// 차수
			if(m_str_ModelID == _T(""))
			{
				nIndex = 0;
				m_strLotId = m_strLotIdFull;
			}
			else
			{
				// 2016.09.21 심재원 --------------------------------
				nIndex = m_strLotIdFull.Find(m_str_ModelID);
				if (nIndex < 0)
				{
					AfxMessageBox(_T("Warining! \r\n모델명이 일치하지 않습니다."), MB_SYSTEMMODAL);
				}
				else
				{
					nIndex = 1;
					m_strLotId = m_strLotIdFull;
				}
				// End 2016.09.21 -----------------------------------

				/*//nIndex = m_strLotIdFull.Find(m_str_ModelID);
				nIndex = 1;
				m_strLotId = m_strLotIdFull;*/
			}

			m_szMesPacket[LOT_ID] = m_strLotId;	
			m_szMesPacket[NUM] = m_strdegre;	

			if(nIndex != -1)
			{
				MESRunStart();
			}
			else
			{
				AfxMessageBox("Receive Data Match Fail");
				StatePrintf("Receive Data Match Fail");
				b_MesRunEn = FALSE;
				((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(0);
				m_strLotId = _T("");
				m_strdegre = _T("");
				((COption_ResultWStand*)m_pResWorkerOptWnd)->LOTID_VAL(m_strLotId);
				((COption_ResultWStand*)m_pResWorkerOptWnd)->TESTNUM_VAL(m_strdegre);
			}
		}else
		{
			AfxMessageBox("Receive Data Match Fail");
			StatePrintf("Receive Data Match Fail");
			b_MesRunEn = FALSE;
			((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(0);
			m_strLotId = _T("");
			m_strdegre = _T("");
			((COption_ResultWStand*)m_pResWorkerOptWnd)->LOTID_VAL(m_strLotId);
			((COption_ResultWStand*)m_pResWorkerOptWnd)->TESTNUM_VAL(m_strdegre);
		}
	}

	return 1L;
}

LRESULT CImageTesterDlg::OnUpdateConnection(WPARAM wParam, LPARAM lParam)
{
	UINT uEvent = (UINT) wParam;
	CSocketManager* pManager = reinterpret_cast<CSocketManager*>( lParam );

	if (m_nSockType != SOCK_TCP){
		return 0L;
	}

	if ( pManager != NULL)
	{
		// Server socket is now connected, we need to pick a new one
		if (uEvent == EVT_CONSUCCESS)
		{

		}
		else if (uEvent == EVT_CONFAILURE || uEvent == EVT_CONDROP)
		{
			SocketDisconnect();
			b_AutoMesConnect = TRUE;
		}
	}

	return 1L;
}

void CImageTesterDlg::MESRunStart()	//서버로부터 정상적인 데이터를 입력받으면 테스트가 가능하도록 활성화
{
	b_MesRunEn = TRUE;
	((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(TRUE);

	((COption_ResultWStand*)m_pResWorkerOptWnd)->LOTID_VAL(m_strLotId);
	((COption_ResultWStand*)m_pResWorkerOptWnd)->MESBARCODE_VIEW(m_strLotIdFull);
	((COption_ResultWStand*)m_pResWorkerOptWnd)->TESTNUM_VAL(m_strdegre);
	StatePrintf("Receive Data Success");
}

void CImageTesterDlg::MESRunStop()
{
	b_MesRunEn = FALSE;
	((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(FALSE);
	StatePrintf("Stand By Receive Data");
	m_strLotId = _T("");
	m_strdegre = _T("");
	m_strLotIdFull = _T("");
	((COption_ResultWStand*)m_pResWorkerOptWnd)->LOTID_VAL(m_strLotId);
	((COption_ResultWStand*)m_pResWorkerOptWnd)->MESBARCODE_VIEW(m_strLotIdFull);
	((COption_ResultWStand*)m_pResWorkerOptWnd)->TESTNUM_VAL(m_strdegre);
}


#pragma endregion

#pragma region 모터관련

BOOL CImageTesterDlg::InitMotor()
{
	// 모터 접속
	LogWriteToFile(_T("InitMotor()시작"));

	if(m_Motion.Open())
	{
		m_Motion.StartIOMonitoring();

		DoEvents(100);

		if (m_Motion.GetIOInStatus(DIO_INPORT_SOCKETDOOR) == FALSE) // 소켓 도어 , 소켓 위치 확인
		{
			if (m_Motion.GetIOInStatus(DIO_INPORT_SOCKETDOOR) == FALSE)// 도어가 열려있음
			{
				AfxMessageBox("Please check the socket door and try again.");
			}
// 			else if (m_Motion.GetIOInStatus(DIO_INPORT_SOCKETAREA) == FALSE)// 소켓이 누워 있음
// 			{
// 				AfxMessageBox("Please check the socket status and try again.");
// 			}
			return FALSE;
		}
		
		// 원점잡기 여부 물어보기
		if(AfxMessageBox(_T("원점잡기를 수행하시겠습니까?"), MB_OKCANCEL)	== IDOK)
		{
			LogWriteToFile(_T("원점잡기 수행"));
			if(m_pDlgOrigin)
			{
				delete m_pDlgOrigin;
				m_pDlgOrigin = NULL;
			}

			if(m_pDlgOrigin == NULL)
			{
				m_pDlgOrigin = new CDlgOrigin();
				m_pDlgOrigin->Create(CDlgOrigin::IDD);
				m_pDlgOrigin->StartTimer();
				m_pDlgOrigin->SetMotion(&m_Motion);
				m_pDlgOrigin->ShowWindow(SW_SHOW);
			}
			
			CButton* p = (CButton*)m_pDlgOrigin->GetDlgItem(IDB_DIALOG_CLOSE);

			if(p)
				p->EnableWindow(FALSE);

			BOOL bRet = m_Motion.AllOrigin();

			if(p)
				p->EnableWindow(TRUE);

			if(bRet)
				m_pDlgOrigin->ShowWindow(SW_HIDE);

			LogWriteToFile(_T("원점잡기 수행 후 LED ON"));

			RUN_LED_CONTROL(0);
			m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_START,  1);
			m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_STOP, 1);
			//m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_INIT, 1);
		}
		else
		{
			LogWriteToFile(_T("InitMotor() 끝 FAIL"));

			return FALSE;
		}
	}else{
		LogWriteToFile(_T("InitMotor() 끝 FAIL"));

		return TRUE;//모터가 없는 환경에서도 동작은 하도록 수정한다. 
		//return FALSE;
	}
	LogWriteToFile(_T("InitMotor() 끝 PASS"));

	return TRUE;
}
void CImageTesterDlg::RUN_LED_CONTROL(int stat)//0:소명 1:RUN킨다. 2:합격 3: 불합격
{
	
	BOOL    bState=0;
	BOOL    bState2=0;
	BOOL    bState3=0;
		

	if(stat == 0){
		bState = 0;
		bState2 = 0;
		bState3 = 0;
		LogWriteToFile(_T("LED ON,PASS,FAIL,RUN OFF"));

	}else if(stat == 1){
	
		bState = 1;
		bState2 = 0;
		bState3 = 0;
		LogWriteToFile(_T("LED ON,PASS ON,FAIL,RUN OFF"));


	}
	else if(stat == 2){
	
		bState = 0;
		bState2 = 1;
		bState3 = 0;
		LogWriteToFile(_T("LED ON,PASS OFF,FAIL ON,RUN OFF"));


	}else if(stat == 3){
	
		bState = 0;
		bState2 = 0;
		bState3 = 1;
		LogWriteToFile(_T("LED ON,PASS OFF,FAIL OFF,RUN ON"));


	}

// 	m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_OP_RUN,  bState);
// 	m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_OP_PASS, bState2);
// 	m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_OP_FAIL, bState3);



}
BOOL CImageTesterDlg::DISTORTION_JIG_MOVE()
{
#if Def_IMGMode 
	return TRUE;
#endif
	BOOL bRet(FALSE);

	if (m_Motion.IsOpen())
	{
		m_Motion.SetMotorModeStatus();

		int nMotorStatus = m_Motion.GetMotorModeStatus();

		if (m_Motion.GetIOInStatus(DIO_INPORT_CYL_IN))
		{
			double dbPos = m_Motion.GetCurrentPos(AXIS_R);

			BOOL bRet = TRUE;

			// 이물광원 들어가있는 상태에 카메라 있을경우 모터를 뒤로 옮기고 실린더 OFF하자
// 			if (((m_Motion.m_dbPos[3] - 5000) > dbPos && (m_Motion.m_dbPos[3] - 7000) < dbPos) ||
// 				nMotorStatus == ACTION_TESTPOS)
// 				bRet = m_Motion.MoveAxis(AXIS_R, dbPos + 10000, m_Motion.param[AXIS_R].Acc, m_Motion.param[AXIS_R].Acc, m_Motion.param[AXIS_R].MaxVel, TRUE, FALSE);
// 
// 			if (!bRet)
// 				return FALSE;

			if (!m_Motion.CylinderOut())
			{
				AfxMessageBox(_T("이물광원이 있습니다. 확인하시기 바랍니다."));
				return FALSE;
			}
		}
	}

	if (m_Motion.GetIOInStatus(DIO_INPORT_CYL_OUT))
	{
		Move_Chart(m_dDistortDistance, FALSE);
		Move_ModuleX(m_dXModuleDistance, FALSE);
		Move_ModuleY(m_dYModuleDistance, FALSE);

		if (!m_Motion.MultiMoveAxis(m_AxisParam, 3))
			return FALSE;
	}

	return bRet;
}
BOOL CImageTesterDlg ::JIG_MOVE()
{
#if Def_IMGMode 
	return TRUE;
#endif
	LogWriteToFile(_T("JIG_MOVE() 시작"));

	if(m_Motion.IsOpen())
	{
		m_Motion.SetMotorModeStatus();
		
		int nMotorStatus = m_Motion.GetMotorModeStatus();

		if(m_Motion.GetIOInStatus(DIO_INPORT_CYL_IN))
		{
// 			double dbPos = m_Motion.GetCurrentPos(AXIS_R);
// 			
// 			BOOL bRet = TRUE;
// 
// 			// 이물광원 들어가있는 상태에 카메라 있을경우 모터를 뒤로 옮기고 실린더 OFF하자
// 			if(((m_Motion.m_dbPos[3] - 5000) > dbPos && (m_Motion.m_dbPos[3] - 7000) < dbPos) ||
// 				nMotorStatus == ACTION_TESTPOS)
// 				bRet = m_Motion.MoveAxis(AXIS_R, dbPos - 10000, m_Motion.param[AXIS_R].Acc, m_Motion.param[AXIS_R].Acc, m_Motion.param[AXIS_R].MaxVel, TRUE, FALSE);
// 
// 			if(!bRet){
// 				LogWriteToFile(_T("1 JIG_MOVE 실패"));
// 			
// 				return FALSE;
// 			}
			if(!m_Motion.CylinderOut())
			{
				AfxMessageBox(_T("이물광원이 있습니다. 확인하시기 바랍니다."));
				LogWriteToFile(_T("이물광원이 있음."));
				LogWriteToFile(_T("2 JIG_MOVE 실패"));
				
				return FALSE;
			}
		}
	}
	
	if(m_Motion.GetIOInStatus(DIO_INPORT_CYL_OUT))
	{
		Move_ModuleS(m_dSModuleDistance, TRUE);
		Move_Chart(m_dChartDistance, FALSE);
		Move_ModuleX(m_dXModuleDistance, FALSE);
		Move_ModuleY(m_dYModuleDistance, FALSE);

		if(!m_Motion.MultiMoveAxis(m_AxisParam, 3)){
			LogWriteToFile(_T("3 JIG_MOVE 실패"));
					
			return FALSE;
		}
	}
	LogWriteToFile(_T("JIG_MOVE 성공"));

		
	return TRUE;
}


# if 0
BOOL CImageTesterDlg ::Move_Chart(int distance, BOOL bMove)//차트 움직임에 걸리는 시간 최대 5초
{
	LogWriteToFile(_T("Move_Chart(int distance, BOOL bMove) 시작"));
	CString str ="";
	str.Format("1. %d",distance);
	LogWriteToFile(_T(str));
/*
	distance =  abs(555 -distance);
	if(distance >545){
		distance = 545;
	}
*/

	distance =  abs(m_nRoffset -distance);
	if(distance >m_nRoffset){
		distance = m_nRoffset;
	}

	if(55> distance){
		if(distance <0 ){
			distance =0;
		}else{
			distance = distance-1;
			if(distance <0 ){
				distance =0;
			}
		}
	}
	str.Format("2. %d",distance);
	LogWriteToFile(_T(str));

	distance = distance * m_Motion.param[AXIS_R].nMMperPulse;

	str.Format("3. %d",distance);
	LogWriteToFile(_T(str));
	BOOL FLAG = TRUE;

	m_AxisParam[AXIS_R].nAxisNum = AXIS_R;
	m_AxisParam[AXIS_R].dbPos = distance * -1;
	m_AxisParam[AXIS_R].dbVel = m_Motion.param[AXIS_R].MaxVel;
	m_AxisParam[AXIS_R].dbAcc = m_Motion.param[AXIS_R].Acc;

	if(bMove)
	{
		if(m_Motion.CylinderOut()){
			LogWriteToFile(_T("MoveAxis 시작"));

			FLAG = m_Motion.MoveAxis(AXIS_R, m_AxisParam[AXIS_R].dbPos, m_AxisParam[AXIS_R].dbAcc, m_AxisParam[AXIS_R].dbAcc, m_AxisParam[AXIS_R].dbVel);
			LogWriteToFile(_T("MoveAxis 끝"));
			
		}
	}

	if(FLAG == TRUE){
		LogWriteToFile(_T("Move_Chart(int distance, BOOL bMove) 성공"));
		
	}else{
		LogWriteToFile(_T("Move_Chart(int distance, BOOL bMove) 실패"));
		
	}


	return FLAG;
}

#else
BOOL CImageTesterDlg::Move_Chart(int distance, BOOL bMove)//차트 움직임에 걸리는 시간 최대 5초
{
#if Def_IMGMode 
	return TRUE;
#endif
	StatePrintf("Move Chart 시작");

	LogWriteToFile(_T("Move_Chart(int distance, BOOL bMove) 시작"));
	CString str = "";
	str.Format("1. %d", distance);
	LogWriteToFile(_T(str));

	/*
	distance =  abs(555 -distance);
	if(distance >545){
	distance = 545;
	}
	*/
	CString szLog;

	szLog.Format(_T(" 실제 입력된 차트 입력 거리 : offset %d, %d "), m_nRoffset, distance);
	StatePrintf(szLog);

	distance = abs(m_nRoffset - distance);
	if (distance >m_nRoffset){
		distance = m_nRoffset;
	}

	if (55> distance){
		if (distance <0){
			distance = 0;
		}
		else{
			distance = distance - 1;
			if (distance <0){
				distance = 0;
			}
		}
	}

	str.Format("2. %d", distance);
	LogWriteToFile(_T(str));

	szLog.Format(_T("입력된 차트 입력 거리 : %s, "), str);
	StatePrintf(szLog);

	distance = distance * m_Motion.param[AXIS_R].nMMperPulse;

	str.Format("3. %d", distance);
	LogWriteToFile(_T(str));

	szLog.Format(_T("실제 이동 펄스 값 : %s, "), str);
	StatePrintf(szLog);

	BOOL FLAG = TRUE;

	m_AxisParam[AXIS_R].nAxisNum = AXIS_R;
	m_AxisParam[AXIS_R].dbPos = distance * -1;
	m_AxisParam[AXIS_R].dbVel = m_Motion.param[AXIS_R].MaxVel;
	m_AxisParam[AXIS_R].dbAcc = m_Motion.param[AXIS_R].Acc;

	szLog.Format(_T("모터이동 bMove : %d, "), bMove);
	StatePrintf(szLog);

	if (bMove)
	{
		szLog.Format(_T("이물광원 Out 시작"));
		StatePrintf(szLog);

		if (m_Motion.CylinderOut())
		{
			szLog.Format(_T("이물광원 Out 완료"));
			StatePrintf(szLog);

			LogWriteToFile(_T("MoveAxis 시작"));

			FLAG = m_Motion.MoveAxis(AXIS_R, m_AxisParam[AXIS_R].dbPos, m_AxisParam[AXIS_R].dbAcc, m_AxisParam[AXIS_R].dbAcc, m_AxisParam[AXIS_R].dbVel);

			szLog.Format(_T("모터 이동 완료 : %d"), FLAG);
			StatePrintf(szLog);

			LogWriteToFile(_T("MoveAxis 끝"));
		}
	}

	if (FLAG == TRUE)
	{
		LogWriteToFile(_T("Move_Chart(int distance, BOOL bMove) 성공"));
	}
	else
	{
		LogWriteToFile(_T("Move_Chart(int distance, BOOL bMove) 실패"));

	}

	return FLAG;
}
#endif

BOOL CImageTesterDlg ::Move_ModuleX(int distance, BOOL bMove)
{
#if Def_IMGMode 
	return TRUE;
#endif
	LogWriteToFile(_T("Move_ModuleX(int distance, BOOL bMove) 시작"));
	distance = distance * m_Motion.param[AXIS_X].nMMperPulse;
	distance /= 10;

	m_AxisParam[AXIS_X].nAxisNum = AXIS_X;
	m_AxisParam[AXIS_X].dbPos = distance * -1;
	m_AxisParam[AXIS_X].dbVel = INIT_VEL;
	m_AxisParam[AXIS_X].dbAcc = INIT_ACC;

	BOOL FLAG =TRUE;

	if(bMove)
		FLAG = m_Motion.MoveAxis(AXIS_X, m_AxisParam[AXIS_X].dbPos, m_AxisParam[AXIS_X].dbAcc, m_AxisParam[AXIS_X].dbAcc, m_AxisParam[AXIS_X].dbVel);


	
	if(FLAG == TRUE){
		LogWriteToFile(_T("Move_ModuleX(int distance, BOOL bMove) 성공"));
		
	}else{
		LogWriteToFile(_T("Move_ModuleX(int distance, BOOL bMove) 실패"));
		
	}

	return FLAG;
}



BOOL CImageTesterDlg ::Move_ModuleY(int distance, BOOL bMove)
{
#if Def_IMGMode 
	return TRUE;
#endif
	LogWriteToFile(_T("Move_ModuleY(int distance, BOOL bMove) 시작"));
	distance = distance * m_Motion.param[AXIS_Z].nMMperPulse;
	distance /= 10;

	m_AxisParam[AXIS_Z].nAxisNum = AXIS_Z;
	m_AxisParam[AXIS_Z].dbPos = distance;
	m_AxisParam[AXIS_Z].dbVel = INIT_VEL;
	m_AxisParam[AXIS_Z].dbAcc = INIT_ACC;

	BOOL FLAG =FALSE;

	if(bMove)
		FLAG = m_Motion.MoveAxis(AXIS_Z, m_AxisParam[AXIS_Z].dbPos, m_AxisParam[AXIS_Z].dbAcc, m_AxisParam[AXIS_Z].dbAcc, m_AxisParam[AXIS_Z].dbVel);

	if(FLAG == TRUE){
		LogWriteToFile(_T("Move_ModuleY(int distance, BOOL bMove) 성공"));
		
	}else{
		LogWriteToFile(_T("Move_ModuleY(int distance, BOOL bMove) 실패"));
		
	}
	return TRUE;
}



/////////////////부저 알람

BOOL CImageTesterDlg ::ALARM_ONOFF(int MODE)
{
#if Def_IMGMode 
	return TRUE;
#endif
	if(MODE == 1)
	{///RUN
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_RED, 0);
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_GREEN, 0);
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_YELLOW, 1);
	}else if(MODE == 2)
	{///PASS
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_RED, 0);
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_YELLOW, 0);
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_GREEN, 1);
	}else if(MODE == 3)
	{//FAIL
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_GREEN, 0);
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_YELLOW, 0);
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_RED, 1);
	}else
	{
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_RED, 0);
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_GREEN, 1);
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_YELLOW, 0);
	}
	DoEvents(1000);	
	return TRUE;
}

BOOL CImageTesterDlg ::ALARM_OPERATING(int MODE){

#if Def_IMGMode 
	return TRUE;
#endif
	if((MODE == 2)||(MODE ==3)){
		Alarm_MODE =1;
	}

	ALARM_ONOFF(MODE);

	int temp = (int)(i_Alarmdelay/100);
	if(MODE !=0){
		Alarm_Cnt = temp;
	}else{
		Alarm_Cnt =0;	
	}

	return TRUE;
}

BOOL CImageTesterDlg ::BUZZER_ONOFF(int MODE)
{
#if Def_IMGMode 
	return TRUE;
#endif
	if (MODE == 1){
		LogWriteToFile(_T("BUZZER_ONOFF 1"));
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_MELODY1, 0);
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_MELODY2, 1);
	}
	else if (MODE == 2){
		LogWriteToFile(_T("BUZZER_ONOFF 2"));
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_MELODY1, 1);
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_MELODY2, 0);
	}
	else{
		LogWriteToFile(_T("BUZZER_ONOFF OFF"));
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_MELODY1, 0);
		m_Motion.SetIOOutPort(DIO_OUTPORT_LAMP_MELODY2, 0);
	}
	DoEvents(1000);
	return TRUE;
}

BOOL CImageTesterDlg ::BUZZER_OPERATING(int MODE){
#if Def_IMGMode 
	return TRUE;
#endif
	
	if(MODE != 0){
		Buzzer_MODE =1;
	}else{
		Buzzer_MODE =0;

	}
	
	int temp =0;
	if(i_Buzzerdelay < 100){
		temp =0;

	}
	else{
		temp = (int)(i_Buzzerdelay/100);
	}

	
	if(MODE !=0){
		Buzzer_Cnt = temp;
	}else{
		Buzzer_Cnt =0;	
		
	}
	if(Buzzer_Cnt == 0){

		MODE =0;
	}
	
//	BUZZER_ONOFF(MODE);

	
	return TRUE;
}

BOOL CImageTesterDlg::Move_ModuleS(int distance, BOOL bMove /*= TRUE*/)
{
#if Def_IMGMode 
	return TRUE;
#endif
	LogWriteToFile(_T("Move_ModuleS(int distance, BOOL bMove) 시작"));
	distance = distance * m_Motion.param[AXIS_S].nMMperPulse;

	m_AxisParam[AXIS_S].nAxisNum = AXIS_S;
	m_AxisParam[AXIS_S].dbPos = distance;
	m_AxisParam[AXIS_S].dbVel = m_Motion.param[AXIS_S].MaxVel;
	m_AxisParam[AXIS_S].dbAcc = m_Motion.param[AXIS_S].Acc;

	BOOL FLAG = FALSE;

	if (bMove)
		FLAG = m_Motion.MoveAxis(AXIS_S, m_AxisParam[AXIS_S].dbPos, m_AxisParam[AXIS_S].dbAcc, m_AxisParam[AXIS_S].dbAcc, m_AxisParam[AXIS_S].dbVel);

	if (FLAG == TRUE){
		LogWriteToFile(_T("Move_ModuleS(int distance, BOOL bMove) 성공"));
	}
	else{
		LogWriteToFile(_T("Move_ModuleS(int distance, BOOL bMove) 실패"));

	}
	return TRUE;
}

BOOL CImageTesterDlg::IRDust_ONOFF(BOOL IR, BOOL Dust){
#if Def_IMGMode 
	return TRUE;
#endif
	memset(m_SerialInput_Cam, 0, sizeof(m_SerialInput_Cam));
	BYTE DATA[20] = { '!', 'R', '0', '0', '0',
		'0', '0', '0', '0', '0', '@' };

	Buf_Wait_Cam = TRUE;
	BYTE Buf[2];

	Buf[0] = (BYTE)IR;
	Buf[1] = (BYTE)Dust;

	DATA[2] = Hex2Ascii(Buf[0]);
	DATA[3] = Hex2Ascii(Buf[1]);

	SendData8Byte_Cam(DATA, 11);
	BYTE ASCNUM = '1';
	for (int lop = 0; lop < 10; lop++){//0.1초동안 ACK를 기다린다. 
		if (Buf_Wait_Cam == FALSE){
			if ((m_SerialInput_Cam[1] == 'R') && (m_SerialInput_Cam[2] == DATA[2]) && (m_SerialInput_Cam[3] == DATA[3])){
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		DoEvents(10);
	}
	return FALSE;

}

BOOL CImageTesterDlg::Motion_All_Stop()
{
#if Def_IMGMode 
	return TRUE;
#endif
	if (!m_Motion.IsOpen())
		return FALSE;

	BOOL bRet = FALSE;

	bRet = m_Motion.StopAllAxis();

	return bRet;
}

BOOL CImageTesterDlg ::LightRAY_ONOFF(BOOL Light, BOOL Ray)
{
#if Def_IMGMode 
	return TRUE;
#endif
	CString str ="";
	str.Format("LightRAY_ONOFF Light %d, Ray %d",Light,Ray);
	LogWriteToFile(_T(str));
// 	m_Motion.SetIOOutPort(DIO_OUTPORT_LIGHT, Light);
// 	m_Motion.SetIOOutPort(DIO_OUTPORT_IRLIGHT, Ray);

	//DoEvents(1000);	
	return TRUE;
}

BOOL CImageTesterDlg ::Light_ONOFF(bool CHK){

#if Def_IMGMode 
	return TRUE;
#endif
// 	if (IRDust_ONOFF(CHK, CHK)){
// 		DoEvents(i_Lightoffdelay);
// 		return TRUE;
// 	}
	if (CHK == 0){
		if (!Power_Each_Control_Off(3)){
			return FALSE;
		}
		else{
			DoEvents(i_Lightoffdelay);

			return TRUE;
		}
		
	}
	else{
		if (!Power_Each_Control(3)){
			return FALSE;
		}
		else{
			DoEvents(i_Lightondelay);

			return TRUE;
		}
		
	}

// 	if(CHK == 0){
// 		if(!IO_CHECK_LIGHT_RAY(0,0)){
// 			if(!LightRAY_ONOFF(0,0)){
// 				return FALSE;
// 			}else{
// 				DoEvents(i_Lightoffdelay);
// 
// 				return TRUE;
// 			}
// 		}else{
// 			return TRUE;
// 		}
// 	}else if(CHK == 1){//빛을 활용하기 위해 켰을 경우 1초동안 기다린다. 
// 		if(!IO_CHECK_LIGHT_RAY(1,0)){
// 
// 			if(!LightRAY_ONOFF(1,0)){
// 				return FALSE;
// 			}else{
// 			
// 				DoEvents(i_Lightondelay);
// 				return TRUE;
// 			}
// 		}else{
// 			return TRUE;
// 		}
// 	}
	return FALSE;
}

BOOL CImageTesterDlg ::IO_CHECK_LIGHT_RAY(BOOL Light,BOOL Ray){
#if Def_IMGMode 
	return TRUE;
#endif
	CString str ="";
	str.Format("IO_CHECK_LIGHT_RAY Light %d, Ray %d",Light,Ray);
	LogWriteToFile(_T(str));

// 	if(m_Motion.GetIOOutStatus(DIO_OUTPORT_LIGHT) != Light){
// 		LogWriteToFile(_T("m_Motion.GetIOOutStatus(DIO_OUTPORT_LIGHT) FAIL"));
// 		return FALSE;
// 	}
// 	if(m_Motion.GetIOOutStatus(DIO_OUTPORT_IRLIGHT )!=Ray){
// 		LogWriteToFile(_T("m_Motion.GetIOOutStatus(DIO_OUTPORT_IRLIGHT) FAIL"));
// 		return FALSE;
// 	}

	LogWriteToFile(_T("IO_CHECK_LIGHT_RAY PASS"));
	
	return TRUE;
}

BOOL CImageTesterDlg ::RAY_ONOFF(bool CHK){//빛을 활용하기 위해 켰을 경우 1초동안 기다린다. 
	#if Def_IMGMode 
	return TRUE;
#endif
	if(CHK == 0){
		if(!IO_CHECK_LIGHT_RAY(0,0)){
			if(!LightRAY_ONOFF(0,0)){
				return FALSE;
			}else{
				DoEvents(i_Lightoffdelay);

				return TRUE;
			}
		}else{
			return TRUE;
		}
	}else if(CHK == 1){
		if(!IO_CHECK_LIGHT_RAY(0,1)){
			if(!LightRAY_ONOFF(0,1)){
				return FALSE;
			}else{
				DoEvents(i_Rayondelay);
				return TRUE;
			}
		}else{
			return TRUE;
		}
	}
	return FALSE;
}
#pragma endregion
void CImageTesterDlg::OnTcnSelchangeTabResult(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int itab = m_pModResTabWnd->GetCurSel();
	m_pModOptTabWnd->SetCurSel(itab);

	if(m_psubmodel[itab]->IN_pWnd != NULL){
		m_psubmodel[itab]->ShowDlg(SW_SHOW);
	}
	
	*pResult = 0;
}

void CImageTesterDlg::OnTcnSelchangingTabResult(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int itab = m_pModResTabWnd->GetCurSel();
	
	if(m_psubmodel[itab]->IN_pWnd != NULL){
		m_psubmodel[itab]->InitPrm();
		m_psubmodel[itab]->ShowDlg(SW_HIDE);
	}
	*pResult = 0;
}

void CImageTesterDlg::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	int hour=0,minute=0,second=0,hour2=0;
	//int year2 = YEAR-2000; 
	b_StopCommand = FALSE;
	hour=thetime.GetHour(); minute=thetime.GetMinute(); second=thetime.GetSecond();

	if(hour>12){/////////////////////////////////////////////////////시간
	hour2 = hour-12;
	strTime.Empty();
	strTime.Format("PM%02d:%02d:%02d",hour2,minute,second);
	}
	else{strTime.Empty();
	strTime.Format("AM%02d:%02d:%02d",hour,minute,second);///////////////////시간
	}
	strDay.Empty();
	strDay.Format("%04d_%02d_%02d", thetime.GetYear(), thetime.GetMonth(), thetime.GetDay());


	


	//if(LotMod ==1){
	InsertIndex = MainWork_List.InsertItem(StartCnt,"",0);
	//	totalint = okint + failint +1;/////////////////////////////////////////////////////////////////////////////////////////0527
	//}
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	MainWork_List.SetItemText(InsertIndex,0,strCnt);
	
	MainWork_List.SetItemText(InsertIndex,1,m_modelName);
	MainWork_List.SetItemText(InsertIndex,2,strDay+"");
	MainWork_List.SetItemText(InsertIndex,3,strTime+"");

	if(LotMod == 1){
		Lot_InsertIndex = m_Lot_MainWorkList.InsertItem(Lot_StartCnt,"",0);

		strCnt.Empty();
		strCnt.Format(_T("%d"), Lot_InsertIndex+1);
		m_Lot_MainWorkList.SetItemText(Lot_InsertIndex,0,strCnt);
	
		m_Lot_MainWorkList.SetItemText(Lot_InsertIndex,1,m_modelName);
		m_Lot_MainWorkList.SetItemText(Lot_InsertIndex,2,LOT_NUMBER);
		m_Lot_MainWorkList.SetItemText(Lot_InsertIndex,3,strDay+"");
		m_Lot_MainWorkList.SetItemText(Lot_InsertIndex,4,strTime+"");
		
	}

}

void CImageTesterDlg::OnBnClickedBtnStart()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//while (1)
	//for (int a = 0; a < 300;a++)
	if (b_Pogostate == FALSE)
	{
		CPopUP2 subDlg;
		subDlg.Warning_TEXT = "Pogo pin connect Count Limit Over.";
		if (subDlg.DoModal() == IDOK){

			//m_pOptPogoDlgWnd->OnBnClickedButtonCntinit();
			if (MatchPassWord == ""){
				AfxMessageBox("Pogo pin Count Reset.");
				i_PogoCnt = 0;
				SetCountUpdate();
			}
			else{

				CUserSwitching subdlg;
				subdlg.Setup(this);

				if (subdlg.DoModal() == IDOK){
					AfxMessageBox("Pogo pin Count Reset.");
					i_PogoCnt = 0;
					SetCountUpdate();
				}

				delete subdlg;
			}

		}

		return;
	}

	{
		RUN_MODE_CHK(1);
		onInitFuncMesData();
		int start=0,end =0;
		((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_STOP))->EnableWindow(1);

		if(m_pResLotWnd !=NULL)
			m_pResLotWnd->LotexitBtn_Enable(0);
		CString str="";
		int count =0;
		int Check =0;
		int	Fin=0;

		b_StopCommand = FALSE;

		if (b_DoorOpen == TRUE ||b_SocketReady == FALSE) // 소켓 도어 , 소켓 위치 확인
		{
			if (b_DoorOpen == TRUE)// 도어가 열려있음
				AfxMessageBox("Please check the socket door and try again.");
			else if (b_SocketReady == FALSE)// 소켓이 누워 있음
				AfxMessageBox("Please check the socket status and try again.");

			if (b_UserMode == 1)
				FullOptionEnable(1);

			if (b_UserMode == 1)
				FullOptionEnable(1);

			b_StartCommand = FALSE;
			b_StopCommand = FALSE;
			TESTstartTime = 0;

			OnBnClickedBtnMain();
			RUN_MODE_CHK(0);

			if (m_pResLotWnd != NULL)
				m_pResLotWnd->LotexitBtn_Enable(1);

			if (b_Chk_MESEn == TRUE && b_UserMode == FALSE)
				((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(FALSE);
			else
				((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(TRUE);

			for (int _a = 0; _a < MAX_MES_NUM; _a++)
				m_szMesPacket[_a] = _T("");

			m_strLotId = _T("");
			m_strdegre = _T("");
			return;
		}

		if(b_StartCommand == FALSE){
			TESTstartTime = GetTickCount(); //시작시간
			b_StartCommand = TRUE;
		}
		else{
			return;
		}

		LogWriteToFile(_T("OnBnClickedBtnStart() 진입"));


		//--------------------------------Lot
		if(b_StartCommand == TRUE){
			InsertList();
			LogWriteToFile(_T("InsertList();"));

		}
		//--------------------------------
	
		FullOptionEnable(0);	
		LogWriteToFile(_T("FullOptionEnable(0);"));

		for(int i= 0;i<20;i++){
			if(m_psubmodel[i] != NULL){
				m_psubmodel[i]->Result.m_Success =FALSE;
				m_psubmodel[i]->InitStat();
			}
		}

		Initprm();
		LogWriteToFile(_T("Initprm();"));

		TEST_STAT(0,"PROCESSING...");
		LogWriteToFile(_T("TEST_STAT(0,PROCESSING...);"));

		int currentfail=0;
		LogWriteToFile(_T("------------------------------------------------TEST 시작"));

		for(int lop=0; lop<20; lop++)
		{
			// Area Sensor 감지 되었거나, Door 열렸을 경우
			if(m_psubmodel[lop] != NULL)
			{
				if((b_StopCommand == FALSE) /*&& (TestData[0].m_Success != 2)*/)		 //강제 중지 명령 
				{
					m_psubmodel[lop]->run();
				}
				else
				{
					m_psubmodel[lop]->stop();
				}
					count++;
					///TestItemNum = count;
			}
			else
			{
				break;
			}

			DoEvents(500);
		}
		//---------------------------------------유저가 정한 이름.아이디..TEST모드
	
		//Process_Time(start, end);

		LogWriteToFile(_T("------------------------------------------------TEST 종료"));

		end = GetTickCount();
		EndTime= end;

		CTime thetime = CTime::GetCurrentTime();

		int hour = 0, minute = 0, second = 0, hour2 = 0;
		b_StopCommand = FALSE;
		hour = thetime.GetHour(); minute = thetime.GetMinute(); second = thetime.GetSecond();

		if (hour > 12){/////////////////////////////////////////////////////시간
			hour2 = hour - 12;
			strEndTime.Empty();
			strEndTime.Format("PM%02d:%02d:%02d", hour2, minute, second);
		}
		else{
			strEndTime.Empty();
			strEndTime.Format("AM%02d:%02d:%02d", hour, minute, second);///////////////////시간
		}

 		if((LotMod ==1) && (b_UserMode == FALSE)){
 	 	
			m_Lot_MainWorkList.SetItemText(Lot_InsertIndex, 5, strEndTime + "");
 	 		LogWriteToFile("MODEL_END 9 LOT관련");
 	 
 	 		for(int lop=0; lop<20; lop++){
 	 			if(m_psubmodel[lop] != NULL)
 	 			{
 	 				//m_psubmodel[lop]->Lot_InsertDataList();
 	 				m_psubmodel[lop]->LOT_Excel_Save();
 	 			}
 	 		}
 	 		LogWriteToFile("MODEL_END 9 LOT_Excel_Save");
 	 
 	 			
 	 		Lot_StartCnt++;
 	 		Lot_Totalint = Lot_Passint+Lot_Failint;
 	 
 	 		LOT_EXCEL_SAVE();
 	 		LogWriteToFile("MODEL_END 10 LOT_EXCEL_SAVE");
 	 
 	 		SETTING_LotInfo();
 	 		LogWriteToFile("MODEL_END 11 SETTING_LotInfo");
 		}
 		else{

			MainWork_List.SetItemText(InsertIndex, 4, strEndTime + "");
			SAVE_FILE();
			FileCopy_AnotherPath_Func();
			StartCnt++;
		}

		if(b_UserMode == 1)
			FullOptionEnable(1);
	
		b_StartCommand = FALSE;
		b_StopCommand = FALSE;
		TESTstartTime  =0;

		OnBnClickedBtnMain();
		RUN_MODE_CHK(0);

		if(m_pResLotWnd !=NULL)
		m_pResLotWnd->LotexitBtn_Enable(1);

		if(b_Chk_MESEn == TRUE && b_UserMode == FALSE)
			((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(FALSE);
		else
			((CButton *)GetDlgItem(IDC_BTN_START))->EnableWindow(TRUE);
		
		for(int _a=0; _a<MAX_MES_NUM;_a++)
			m_szMesPacket[_a] = _T("");

		m_strLotId = _T("");
		m_strdegre = _T("");

		Focus_move_start();
	}
}

void CImageTesterDlg::OnBnClickedBtnStop()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CButton *)GetDlgItem(IDC_BTN_STOP))->EnableWindow(0);
	

	if(b_StartCommand == TRUE){//현재 스타트중이면

			MainWork_List.SetItemText(InsertIndex,18,"STOP");
			b_StopCommand = TRUE;
			b_StartCommand = FALSE;

	}else if(b_StopCommand == FALSE){
			Power_Off();

	}

	Focus_move_start();
	((CButton *)GetDlgItem(IDC_BTN_STOP))->EnableWindow(1);


}


void CImageTesterDlg::Mid_Time(DWORD start, DWORD end){
	CString str="";
	int time_data = end - start;

	int hours = (time_data/(1000*60*60)) % 24;
	int minutes = (time_data/(1000*60)) % 60;
	int seconds = (time_data/1000) % 60;///////////////////////////////////////////////BIN


	str.Empty();
	//strTime.Format("%d:%d:%d  %dms",hours,minutes,seconds,time_data);
	//str.Format("%d,%dms",time_data/1000,time_data%1000);
	str.Format("%02d:%02d:%02d",hours,minutes,seconds);///////////////////////////////////////////////BIN
	ProTime_NUM(str);
}

void CImageTesterDlg::Initprm(){
	for(int t=0; t<20; t++){

		TestData[t].m_Success =0;
		TestData[t].m_Enable =0;
		TestData[t].m_ID=0;

		for(int n=0; n<10; n++){
			TestData[t].m_Val[n] =0;
		}
	}

	for(int t=0; t<20; t++){

		if(m_psubmodel[t] != NULL){
			m_psubmodel[t]->InitPrm();

		}
			
	}
	
}

bool CImageTesterDlg::STAY_TIME(){
	//GetTickCount로 측정.
	int STARTTime = GetTickCount();


	int time_data = STARTTime- EndTime ;

	int hours = (time_data/(1000*60*60)) % 24;
	int minutes = (time_data/(1000*60)) % 60;

	if(EndTime != 0){
		if((hours >= i_MST_H)&& (minutes >= i_MST_M)){
			return TRUE;
		}
	}
	return FALSE;

}


LRESULT CImageTesterDlg::OnStartBtn(WPARAM wParam, LPARAM lParam)		//스타트 버튼이 눌렸을 때
{
	RunStart();
	return TRUE;
}


LRESULT CImageTesterDlg::OnStopBtn(WPARAM wParam, LPARAM lParam)		// 스톱버튼이 눌렸을 때
{
	RunStop();
	return TRUE;
}

LRESULT CImageTesterDlg::OnEmergencyPause(WPARAM wParam, LPARAM lParam)
{
	BUZZER_OPERATING(3);
	LogWriteToFile(_T("BUZZER_OPERATING(0);"));

	ALARM_OPERATING(3);
	LogWriteToFile(_T("ALARM_OPERATING(0);"));

	// 모션 부 정지 신호.
	Motion_All_Stop();

	// 경고 팝업
	// 
	CDlg_Emergency DlgEmergency;

	DlgEmergency.DoModal();
	//DlgEmergency

	// 부저 종료
	BUZZER_OPERATING(0);

	// 진행
	ALARM_OPERATING(0);

	// 검사 중이면,  검사 종료
	if (b_StartCommand == TRUE)
	{
		OnBnClickedBtnStop();
		InitMotor();
	}

	m_bDoorSensor = FALSE;
	m_bSafetySensor = FALSE;
	m_EmergencyStatus = FALSE;

	return TRUE;
}

void CImageTesterDlg::RunStart()
{
	if((((CButton *)GetDlgItem(IDC_BTN_START))->IsWindowEnabled() == TRUE)&&(b_ETCRunMODE == FALSE)){
		OnBnClickedBtnStart();
	}
	if(((((CButton *)GetDlgItem(IDC_BTN_START))->IsWindowEnabled() == FALSE)&&(b_Particle == TRUE))||(b_ETCRunMODE == TRUE)){
		if(m_ParticleView != NULL){
			m_ParticleView->OnBnClickedOk();
		}

		if (b_ETCRunMODE == TRUE)
		{
			DoEvents(500);
		}
	}
		
}

void CImageTesterDlg::RunStop()
{	
	if((((CButton *)GetDlgItem(IDC_BTN_STOP))->IsWindowEnabled() == TRUE)&&(b_ETCRunMODE == FALSE)){
		OnBnClickedBtnStop();
	}
	if(((((CButton *)GetDlgItem(IDC_BTN_STOP))->IsWindowEnabled() == FALSE)&&(b_Particle == TRUE))||(b_ETCRunMODE == TRUE)){
		if(m_ParticleView != NULL){
			m_ParticleView->OnBnClickedCancel();
		}
		if (b_ETCRunMODE == TRUE)
		{
			DoEvents(500);
		}
	}
}


void CImageTesterDlg::CParticleViewLG_Create(){
	m_ParticleViewLG = new CParticle_View_LG(this);
	m_ParticleViewLG->Setup(this);
	m_ParticleViewLG->Create(CParticle_View_LG::IDD,CWnd::GetDesktopWindow());//&m_CtrTab);
	m_ParticleViewLG->GetWindowRect(InitMasterMoRect);
	m_ParticleViewLG->ShowWindow(SW_HIDE);
}

void CImageTesterDlg::CParticleViewLG_Delete(){
	if(m_ParticleViewLG != NULL){
		m_ParticleViewLG->DestroyWindow();
		delete m_ParticleViewLG;
		m_ParticleViewLG = NULL;
	}
}

void CImageTesterDlg::CParticleViewLG_Show(BOOL MODE)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(MODE == TRUE){//창을 킨다. 
		if(!m_ParticleViewLG->IsWindowVisible()){
			m_ParticleViewLG->ShowWindow(SW_SHOW);
			m_ParticleViewLG->MoveWindow(InitMasterMoRect);
		}else{
			m_ParticleViewLG->ShowWindow(SW_HIDE);
			DoEvents(100);
			m_ParticleViewLG->ShowWindow(SW_SHOW);
		}
	}else{//창을 끈다. 
		if(m_ParticleViewLG->IsWindowVisible()){
			m_ParticleViewLG->MoveWindow(InitMasterMoRect);
			m_ParticleViewLG->ShowWindow(SW_HIDE);
		}
	}
}

void CImageTesterDlg::YUV_SAVE(LPCTSTR PATH)
{
	DWORD	dwk = 0,dwi = 0,dwnx = 0;
	DWORD	dwd=691200;

	if(!CAMERA_STAT_CHK()){
		return;
	}
	m_YUVSCAN = TRUE;

	for(int i =0;i<10;i++){
		if(m_YUVSCAN == FALSE){
			break;
		}else{
			DoEvents(50);
		}
	}
	if(m_YUVSCAN == FALSE){
		dwd=691200;
		for (dwk=0; dwk<691200; dwk+=4)//khk
			{	
			m_YUVOut[dwk+1]= m_YUVIn[dwk];//cb
			m_YUVOut[dwk]= m_YUVIn[dwk+1];//y0
			m_YUVOut[dwk+3]= m_YUVIn[dwk+2];//cr
			m_YUVOut[dwk+2]= m_YUVIn[dwk+3];//y1
		}

		//int R1,G1,B1,R2,G2,B2;
		//int CB0,Y0,CR0,CB1,Y1,CR1;

		//DWORD	YUVCNT = 0;
		//DWORD	YCNT = 0;

		//DWORD	RGBPIX,RGBLINE;

		//for(int lopy = 0;lopy < 480;lopy++){
		//	RGBLINE = 720*4*lopy;
		//	for(int lopx = 0;lopx < 360;lopx++){
		//		RGBPIX = lopx*8;
		//		B1 = m_RGBScanbuf[RGBLINE + RGBPIX];  
		//		G1 = m_RGBScanbuf[RGBLINE + RGBPIX + 1];
		//		R1 = m_RGBScanbuf[RGBLINE + RGBPIX + 2];
		//		B2 = m_RGBScanbuf[RGBLINE + RGBPIX + 4];  
		//		G2 = m_RGBScanbuf[RGBLINE + RGBPIX + 5];
		//		R2 = m_RGBScanbuf[RGBLINE + RGBPIX + 6];
		//	
		//		Y0 = (int)((0.299 * R1) + (0.587 * G1) + (0.114 * B1));
		//		CB0 = 128 + (int)((-0.1687 * R1) - (0.3313* G1) + (0.5 * B1)); 
		//		CR0 = 128 + (int)((0.5 * R1) - (0.4187 * G1) - (0.813 * B1));
		//		Y1 = (int)((0.299 * R2) + (0.587 * G2) + (0.114 * B2));
		//		CB1 = 128 + (int)((-0.1687 * R2) - (0.3313* G2) + (0.5 * B2)); 
		//		CR1 = 128 + (int)((0.5 * R2) - (0.4187 * G2) - (0.813 * B2));

		//		m_YUVOut[YUVCNT+1] = 0x80;//(BYTE)(CB0);//(BYTE)((CB0+CB1)/2);
		//		m_YUVOut[YUVCNT+0] = (BYTE)Y0;
		//		m_YUVOut[YUVCNT+3] = 0x80;//(BYTE)(CR0);//(BYTE)((CR0+CR1)/2);
		//		m_YUVOut[YUVCNT+2] = (BYTE)Y1;

		//		m_pYOut[YCNT] = (BYTE)(Y0);
		//		m_pYOut[YCNT+1] = (BYTE)(Y1);
		//			
		//		YUVCNT += 4;
		//		YCNT += 2;
		//	}
		//}

		FILE *fp=fopen(PATH, "wb");

		for(int i=0; i < CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 2; i++)
		{
			fwrite(&m_YUVOut[i], sizeof(BYTE), (1), fp);

		}
		fclose(fp);
	}
}

void CImageTesterDlg::Y_SAVE(LPCTSTR PATH)
{
	DWORD	dwk = 0,dwi = 0,dwnx = 0;
	DWORD	dwd=691200;

	if(!CAMERA_STAT_CHK()){
		return;
	}
	m_YUVSCAN = TRUE;

	for(int i =0;i<10;i++){
		if(m_YUVSCAN == FALSE){
			break;
		}else{
			DoEvents(50);
		}
	}

	for(int i =0;i<10;i++){
		if(m_YUVSCAN == FALSE){
			break;
		}else{
			DoEvents(50);
		}
	}

	if(m_YUVSCAN == FALSE){
		
		dwd=0;
		for (dwk=0; dwk<691200; dwk+=4)//khk
			{	
			dwd = dwk/2;
			m_pYOut[dwd]= m_YUVIn[dwk+1];//y0
			m_pYOut[dwd+1]= m_YUVIn[dwk+3];//y1
			}

		/*int R1,G1,B1,R2,G2,B2;
		int CB0,Y0,CR0,CB1,Y1,CR1;

		DWORD YUVCNT = 0;
		DWORD YCNT = 0;

		DWORD	RGBPIX,RGBLINE;

		for(int lopy = 0;lopy < 480;lopy++){
			RGBLINE = 720*4*lopy;
			for(int lopx = 0;lopx < 360;lopx++){
				RGBPIX = lopx*8;
				B1 = m_RGBScanbuf[RGBLINE + RGBPIX];  
				G1 = m_RGBScanbuf[RGBLINE + RGBPIX + 1];
				R1 = m_RGBScanbuf[RGBLINE + RGBPIX + 2];
				B2 = m_RGBScanbuf[RGBLINE + RGBPIX + 4];  
				G2 = m_RGBScanbuf[RGBLINE + RGBPIX + 5];
				R2 = m_RGBScanbuf[RGBLINE + RGBPIX + 6];
			
				Y0 = (int)((0.299 * R1) + (0.587 * G1) + (0.114 * B1));
				CB0 = 128 + (int)((-0.1687 * R1) - (0.3313* G1) + (0.5 * B1)); 
				CR0 = 128 + (int)((0.5 * R1) - (0.4187 * G1) - (0.813 * B1));
				Y1 = (int)((0.299 * R2) + (0.587 * G2) + (0.114 * B2));
				CB1 = 128 + (int)((-0.1687 * R2) - (0.3313* G2) + (0.5 * B2)); 
				CR1 = 128 + (int)((0.5 * R2) - (0.4187 * G2) - (0.813 * B2));

				m_YUVOut[YUVCNT] = (BYTE)((CB0+CB1)/2);
				m_YUVOut[YUVCNT+1] = (BYTE)Y0;
				m_YUVOut[YUVCNT+2] = (BYTE)((CR0+CR1)/2);
				m_YUVOut[YUVCNT+3] = (BYTE)Y1;

				m_pYOut[YCNT] = (BYTE)(Y0);
				m_pYOut[YCNT+1] = (BYTE)(Y1);
					
				YUVCNT += 4;
				YCNT += 2;
		}
		}*/


		FILE *fp=fopen(PATH, "wb");

		for(int i=0; i < CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT; i++)
		{
			fwrite(&m_pYOut[i], sizeof(BYTE), (1), fp);

		}
		fclose(fp);
	}
}


void CImageTesterDlg::SAVE_CSV(LPCSTR PATH){///////////////////////////////130321_bin
	
	CStdioFile File;
	CString strFilename ="";

	strFilename = PATH;

	CFileException e;

	
	bool FLAG = TRUE;

	if(!File.Open((LPCTSTR)strFilename,CFile::modeCreate|CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
		return;
	}
	
	File.WriteString(LPCTSTR(str_pCsvdata));

	File.Close();
	

}
void CImageTesterDlg::OnEnSetfocusEditPogoname()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
}

void CImageTesterDlg::OnEnSetfocusEditCountTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
}

void CImageTesterDlg::OnEnSetfocusEditProtime()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
}

void CImageTesterDlg::OnEnSetfocusEditProtime2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
}

BOOL CImageTesterDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CImageTesterDlg::InitEVMS()
{
	if(EVMS_Mode == TRUE)
	{
		m_pMainCombo->EnableWindow(0);
	//	((CButton *)GetDlgItem(IDC_BTN_MASTER_SET))->EnableWindow(0);
	}
}


//=======================================================================================
BOOL CImageTesterDlg::Overlay_Init()
{
	BYTE DATA[20] ={'!','W', '0','0','0','0','0','0','0','0','0','0','0','0','@'};
	Buf_Wait_Cam = TRUE;
	Init_Wait = TRUE;
	SendData8Byte_Cam(DATA, 15);

	for(int lop=0; lop<200; lop++)//ACK가 오기까지 2s를 기다린다. 
	{
		DoEvents(10);
		
		if(Init_Wait == FALSE)
		{
			if(m_SerialInput_Cam[2] == '1'){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
	return FALSE;
}

BOOL CImageTesterDlg::Overlay_Enable(BOOL stat)//0:disable,1:enable
{
	BYTE DATA[20] ={'!','O', '0','0','0','0','0','0','0','0','0','0','0','0','@'};
	if(stat == 1){
		DATA[2] = '1';
	}else{
		DATA[2] = '0';
	}

	Buf_Wait_Cam = TRUE;
	Enable_Wait = TRUE;
	SendData8Byte_Cam(DATA, 15);

	for(int lop=0; lop<100; lop++)//ACK가 오기까지 2s를 기다린다. 
	{
		DoEvents(10);
		if(Enable_Wait == FALSE)
		{
			if(m_SerialInput_Cam[2] == '1'){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
	return FALSE;
}

BOOL CImageTesterDlg::Overlay_Move(int Angle,BYTE LAN,BYTE PAL)  
{
	BYTE DATA[20] ={'!','C', '0','0','0','0','0','0','0','0','0','0','0','0','@'};
	
	BYTE BBUF = 0;

	if(Angle < 0){
		DATA[4] = '1';
	}else{
		DATA[4] = '0';
	}
	unsigned int absangle = abs(Angle);
	if(absangle > 180){
		absangle = 0;
	}
	BBUF = (BYTE)absangle;
	BYTE buf10 = absangle/10;
	BYTE buf1 = absangle%10;

	DATA[2] = Hex2Ascii(buf10);
	DATA[3] = Hex2Ascii(buf1);

	BBUF = LAN;
	DATA[5] = Hex2Ascii((BBUF&0xF0)>>4);
	DATA[6] = Hex2Ascii(BBUF&0x0F);

	BBUF = PAL;
	DATA[7] = Hex2Ascii(BBUF&0x0F);

	BBUF = m_HuType;
	DATA[8] = Hex2Ascii((BBUF&0xF0)>>4);
	DATA[9] = Hex2Ascii(BBUF&0x0F);


	Get_Wait = TRUE;
	Buf_Wait_Cam = TRUE;
	Buf_IMG_SUC = FALSE;
	SendData8Byte_Cam(DATA, 15);
	for(int lop=0; lop<100; lop++)//ACK가 오기까지 2s를 기다린다. 
	{
		DoEvents(10);
		if(Get_Wait == FALSE)
		{
			break;
		}
	}

	m_ImgScan = TRUE;
	Get_Wait = TRUE;
	Buf_Wait_Cam = TRUE;
	Buf_IMG_SUC = TRUE;
	SendData8Byte_Cam(DATA, 15);
	for(int lop=0; lop<300; lop++)//ACK가 오기까지 2s를 기다린다. 
	{
		DoEvents(10);
		if(Get_Wait == FALSE)
		{
			if(m_ImgScan == FALSE){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
	
	return FALSE;
}

void CImageTesterDlg::Overlay_Move_Stand(int Angle,BYTE LAN,BYTE PAL)  
{
	BYTE DATA[20] ={'!','C', '0','0','0','0','0','0','0','0','0','0','0','0','@'};

	BYTE BBUF = 0;

	if(Angle < 0){
		DATA[4] = '1';
	}else{
		DATA[4] = '0';
	}
	unsigned int absangle = abs(Angle);
	if(absangle > 180){
		absangle = 0;
	}
	BBUF = (BYTE)absangle;
	BYTE buf10 = absangle/10;
	BYTE buf1 = absangle%10;

	DATA[2] = Hex2Ascii(buf10);
	DATA[3] = Hex2Ascii(buf1);

	BBUF = LAN;
	DATA[5] = Hex2Ascii((BBUF&0xF0)>>4);
	DATA[6] = Hex2Ascii(BBUF&0x0F);

	BBUF = PAL;
	DATA[7] = Hex2Ascii(BBUF&0x0F);

	BBUF = m_HuType;
	DATA[8] = Hex2Ascii((BBUF&0xF0)>>4);
	DATA[9] = Hex2Ascii(BBUF&0x0F);


	Get_Wait = TRUE;
	Buf_Wait_Cam = TRUE;
	SendData8Byte_Cam(DATA, 15);
}

BOOL CImageTesterDlg::Overlay_Move(int Angle)  
{
	return Overlay_Move(Angle,0x00,0x00);
}

BOOL CImageTesterDlg::Overlay_Move(BYTE LAN,BYTE PAL)  
{
	return Overlay_Move(0,LAN,PAL);
}

void CImageTesterDlg::Overlay_Start()
{
	for(int lop = 0;lop<4;lop++){
		Overlay_Move(0,0,0);
		DoEvents(10);
	}
}

BOOL CImageTesterDlg::DCC_DTC_READ()
{
	int		lop = 0;
	BOOL	ret = FALSE;

	BYTE DATA[20] ={'!','Y','0','0','0',
					'0','0','0','0','0',
					'0','0','0','0','@'};
	
	Buf_Wait_Cam = TRUE;
	Buf_Wait_DTCRD = TRUE;
	m_DTCRDSTATE = FALSE;
	for(int t=0;t<4;t++){
		DTCRDDATA[t] = 0;
	}
	for(int t=0;t<8;t++){
		DTC_BUFDATA[t] = 0;
	}
	SendData8Byte_Cam(DATA, 15);

	for(int lop=0; lop<150; lop++)//1.5초동안 대기
	{
		DoEvents(10);
		if(Buf_Wait_DTCRD == FALSE){
			if(m_DTCRDSTATE != 0){
				ret = TRUE;
			}
			break;
		}
	}
	return ret;

}

void CImageTesterDlg::DCC_DTC_READ_ACK()
{
	BYTE HBUF,LBUF;
	BOOL ZState = FALSE;
	if(m_SerialInput_Cam[2] == '1'){
		for(int lop = 0;lop<8;lop++){
			DTC_BUFDATA[lop] = m_SerialInput_Cam[3+lop];
		}

		for(int lop = 0;lop<4;lop++){
			HBUF = Ascii2Hex(DTC_BUFDATA[lop*2]);
			LBUF = Ascii2Hex(DTC_BUFDATA[(lop*2) + 1]);
			DTCRDDATA[lop] = ((HBUF<<4)&0xf0)+(LBUF&0x0f);
			if(DTCRDDATA[lop] != 0xAA){
				ZState = TRUE;
			}
		}
		if(ZState == TRUE){
			m_DTCRDSTATE = 2;
		}else{
			m_DTCRDSTATE = 1;	
		}
	}else{
		m_DTCRDSTATE = 0;
	}
	Buf_Wait_DTCRD = FALSE;
}

BOOL CImageTesterDlg::DCC_DTC_ERASE()
{
	int		lop = 0;
	BOOL	ret = FALSE;

	BYTE DATA[20] ={'!','D', '0','0','0','0','0','0','0','0','0','0','0','0','@'};

	Buf_Wait_Cam = TRUE;
	Buf_Wait_DTCER = TRUE;
	SendData8Byte_Cam(DATA, 15);

	for(lop=0; lop<150; lop++)//1.5초동안 대기
	{
		DoEvents(10);
		if(Buf_Wait_DTCER == FALSE){
			if(m_SerialInput_Cam[2] == '1'){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
	return ret;
}

void CImageTesterDlg::DCC_DTC_ERASE_ACK()
{
	Buf_Wait_DTCER = FALSE;
}

BOOL CImageTesterDlg::DCC_VER_READ()
{
	int		lop = 0;
	BOOL	ret = FALSE;

	BYTE DATA[20] ={'!','R','0','0','0',
					'0','0','0','0','0',	
					'0','0','0','0','0',
					'0','0','0','0','@'};
	
	Buf_Wait_Cam = TRUE;
	Buf_Wait_VERRD = TRUE;
	
	for(int t=0;t<2;t++){
		str_VER_RD[t] = "";
		m_VER_RD[t] = 0;
	}

	SendData8Byte_Cam(DATA, 20);

	for(lop=0; lop<150; lop++)//1.5초동안 대기
	{
		DoEvents(10);
		if(Buf_Wait_VERRD == FALSE){
			if(m_SerialInput_Cam[2] == '1'){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
	return ret;
}

void CImageTesterDlg::DCC_VER_READ_ACK()
{	
	CString str = "";
	TCHAR HEBUF[16] = {0,};	
	TCHAR LEBUF[16] = {0,};	

	if(m_SerialInput_Cam[2] == '1'){
		for(int lop = 0;lop<8;lop++){
			HEBUF[lop] = m_SerialInput_Cam[lop+3];
			LEBUF[lop] = m_SerialInput_Cam[lop+11];
		}
		str_VER_RD[0].Format(_T("0x%s"), HEBUF);
		str_VER_RD[1].Format(_T("0x%s"), LEBUF);
		for(int t=0;t<2;t++){
			m_VER_RD[t] = StringHEX2DWORD(str_VER_RD[t]);
		}
	}else{
		for(int t=0;t<2;t++){
			str_VER_RD[t] = "";
			m_VER_RD[t] = 0;
		}
	}
	Buf_Wait_VERRD = FALSE;
}

BOOL CImageTesterDlg::DCC_SYSTEM_READ(int Number)//0: HW_READ 1: SW_READ 2: CAN READ 3: 제조일자 4: 품번
{
	int		lop = 0;
	BOOL	ret = FALSE;

	BYTE DATA[20] ={'!','S','0','0','0',
					'0','0','0','0','0',	
					'0','0','0','0','0',
					'0','0','0','0','@'};
	DATA[2] = '0' + Number;
	m_RdNum = Number;

	Buf_Wait_Cam = TRUE;
	Buf_Wait_SYSRD = TRUE;
	str_SYS_RD[m_RdNum] = "";
	SendData8Byte_Cam(DATA, 20);

	for(lop=0; lop<150; lop++)//1.5초동안 대기
	{
		DoEvents(10);
		if(Buf_Wait_SYSRD == FALSE){
			if(m_SYSRDSTATE == TRUE){
				ret = TRUE;
			}
			break;
		}
	}
	return ret;
}

void CImageTesterDlg::DCC_SYSTEM_READ_ACK()//0: HW_READ 1: SW_READ 2: CAN READ 3: 제조일자4: 품번
{
        CString str = "";
        TCHAR HEBUF[255] = {0,};

        if(m_SerialInput_Cam[2] == '1'){
               for(int lop = 0;lop<35;lop++){
                       SYS_BUFDATA[lop] = m_SerialInput_Cam[3+lop];
               }                      
               switch(m_RdNum){
                       case 0://HW
                              /*if((SYS_BUFDATA[1] == 0x62)&&(SYS_BUFDATA[2] == 0xF1)&&(SYS_BUFDATA[3] == 0x93)){
                                      for(int lop = 0;lop<3;lop++){
                                             HEBUF[lop] = (char)SYS_BUFDATA[lop + 4];
                                      }
                                      str.Format("%s", HEBUF);
                                      str_SYS_RD[0] = str; 
                                      m_SYSRDSTATE = TRUE;
                              }else{
                                      m_SYSRDSTATE = FALSE;
                              }*/
							  for(int lop = 0;lop<3;lop++){
                                      HEBUF[lop] = (char)SYS_BUFDATA[lop];
                              }
                              str.Format(_T("%s"), HEBUF);
                              str_SYS_RD[0] = str;
                              m_SYSRDSTATE = TRUE;
                              break;
                       case 1://SW
                              /*if((SYS_BUFDATA[1] == 0x62)&&(SYS_BUFDATA[2] == 0xF1)&&(SYS_BUFDATA[3] == 0x95)){
                                      for(int lop = 0;lop<3;lop++){
                                             HEBUF[lop] = (char)SYS_BUFDATA[lop + 4];
                                      }
                                      str.Format(_T("%s"), HEBUF);
                                      str_SYS_RD[1] = str;
                                      m_SYSRDSTATE = TRUE;
                              }else{
                                      m_SYSRDSTATE = FALSE;
                              }*/
							  for(int lop = 0;lop<3;lop++){
                                      HEBUF[lop] = (char)SYS_BUFDATA[lop];
                              }
                              str.Format(_T("%s"), HEBUF);
                              str_SYS_RD[1] = str;
                              m_SYSRDSTATE = TRUE;
                              break;
                       case 2://CAN버전 12개
                              for(int lop = 0;lop<12;lop++){
                                      HEBUF[lop] = (char)SYS_BUFDATA[lop];
                              }
                              str.Format(_T("%s"), HEBUF);
                              str_SYS_RD[2] = str;
                              m_SYSRDSTATE = TRUE;
                               break;
                       case 3://생산날짜
                              for(int lop = 0;lop<8;lop++){
                                      HEBUF[lop] = (char)SYS_BUFDATA[lop];
                              }
                              str.Format(_T("%s"), HEBUF);
                              str_SYS_RD[3] = str;
                              m_SYSRDSTATE = TRUE;
                              break;
                       case 4://품번
                              for(int lop = 0;lop<10;lop++){
                                      HEBUF[lop] = (char)SYS_BUFDATA[lop];
                              }
                              str.Format(_T("%s"), HEBUF);
                              str_SYS_RD[4] = str;
                              m_SYSRDSTATE = TRUE;
                              break;
                       default :
                              m_SYSRDSTATE = FALSE;
                              break;
               }
        }else{
               m_SYSRDSTATE = FALSE;
        }
        Buf_Wait_SYSRD = FALSE;
}

BOOL CImageTesterDlg::DCC_SYSTEM_READ_ALL()//0: HW_READ 1: SW_READ 2: CAN READ 3: 제조일자 4: 품번
{
	BOOL	ret = TRUE;
	for(int lop = 0;lop<5;lop++){
		if(DCC_SYSTEM_READ(lop) == FALSE){
			DoEvents(100);
			if(DCC_SYSTEM_READ(lop) == FALSE){
				ret = FALSE;
				break;
			}
		}
		DoEvents(100);
	}
	return ret;
}

BOOL CImageTesterDlg::DCC_SYSTEM_WRITE(...)//0: HW_READ 1: SW_READ 2: CAN READ 3: 제조일자 4: 품번
{
	int		lop = 0;
	BOOL	ret = FALSE;
	TCHAR	lpszBuf[256];
	va_list		args;

	BYTE DATA[40] = {'!','A', '0','0','0','0', '0','0','0','0', '0','0','0','0', '0','0','0','0', '0','0','0','0', '0','0','0','0', '0','0','0','0', '0','0','0','0', '0','0','0','0', '@'};
	
	for(int lop = 0;lop<5;lop++){
		ASSERT(AfxIsValidString(str_SYS_WR[lop], FALSE));
		va_start(args, str_SYS_WR[lop]);	
		wvsprintf(lpszBuf, str_SYS_WR[lop], args);
		switch(lop){
			case 0://HW 버전 3개 스타트 20
				for(int t=0;t<3;t++){
					DATA[t+20] = lpszBuf[t];
				}
				break;
			case 1://SW 버전 3개 스타트 23
				for(int t=0;t<3;t++){
					DATA[t+23] = lpszBuf[t];
				}
				break;
			case 2://CAN 버전 12개 스타트 26
				for(int t=0;t<12;t++){
					DATA[t+26] = lpszBuf[t];
				}
				break;
			case 3://날짜 8개 스타트 12
				for(int t=0;t<8;t++){
					DATA[t+12] = lpszBuf[t];
				}
				break;
			case 4://품번 10개 스타트 2
				for(int t=0;t<10;t++){
					DATA[t+2] = lpszBuf[t];
				}
				break;
			default:
				break;
		}
	}
	
	Buf_Wait_Cam = TRUE;
	Buf_Wait_SYSWR = TRUE;
	SendData8Byte_Cam(DATA, 39);

	for(lop=0; lop<500; lop++)//1.5초동안 대기
	{
		DoEvents(10);
		if(Buf_Wait_SYSWR == FALSE){
			if(m_SerialInput_Cam[2] == '1'){
				ret = TRUE;
			}
			break;
		}
	}
	return ret;
}

void CImageTesterDlg::DCC_SYSTEM_WRITE_ACK()//0: HW_READ 1: SW_READ 2: CAN READ 3: 제조일자 4: 품번
{
	Buf_Wait_SYSWR = FALSE;
}

BOOL CImageTesterDlg::DL_UPDATEMODE(BOOL state)
{
	int		lop = 0;
	BOOL	ret = FALSE;

	BYTE DATA[20] ={'!','U','0','0','0',
					'0','0','0','0','0',	
					'0','0','0','0','0',
					'0','0','0','0','@'};
	
	if(state == 1){//진입
		DATA[2] = '1';
	}else{
		DATA[2] = '0';
	}
	Buf_Wait_Cam = TRUE;
	Buf_Wait_DLUP = TRUE;
	
	SendData8Byte_Cam(DATA, 20);

	for(lop=0; lop<500; lop++)//2초동안 대기
	{
		DoEvents(10);
		if(Buf_Wait_DLUP == FALSE){
			if(m_SerialInput_Cam[2] == '1'){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
	return ret;
}

BOOL CImageTesterDlg::DL_ADDRSET(DWORD ADDR,DWORD SIZE)
{
	int		lop = 0;
	BOOL	ret = FALSE;
	DWORD	BUF_1 = ADDR;
	DWORD	BUF_2 = SIZE;

	BYTE DATA[20] ={'!','Z','0','0','0',
					'0','0','0','0','0',	
					'0','0','0','0','0',
					'0','0','0','0','@'};
	
	

	for(lop = 0;lop<6;lop++){
		DATA[7-lop] = Hex2Ascii((BYTE)(BUF_1&0x00000F));//ADDR
		DATA[13-lop] = Hex2Ascii((BYTE)(BUF_2&0x00000F));//HEX
		BUF_1 = BUF_1>>4;
		BUF_2 = BUF_2>>4;
	}

	Buf_Wait_Cam = TRUE;
	Buf_Wait_DLADDR = TRUE;
	
	SendData8Byte_Cam(DATA, 20);

	for(lop=0; lop<200; lop++)//2초동안 대기
	{
		DoEvents(10);
		if(Buf_Wait_DLADDR == FALSE){
			if(m_SerialInput_Cam[2] == '1'){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
	return ret;
}

BOOL CImageTesterDlg::DL_DATASET(_EtcWrdata * INDATA,int NUM)
{
	int		lop = 0;
	BOOL	ret = FALSE;

	BYTE DATA[20] ={'!','G','0','0','0',
					'0','0','0','0','0',	
					'0','0','0','0','0',
					'0','0','0','0','@'};
	
	
	DATA[2] = 0x41 + NUM;
	int offdata = NUM*8;

	for(lop = 0;lop<8;lop++){
		DATA[3+(lop*2)] = Hex2Ascii((INDATA->buffer[offdata+lop]&0xf0)>>4);//ADDR
		DATA[4+(lop*2)] = Hex2Ascii(INDATA->buffer[offdata+lop]&0x0f);//HEX
	}

	Buf_Wait_Cam = TRUE;
	Buf_Wait_DLDATASET = TRUE;
	
	SendData8Byte_Cam(DATA, 20);

	//return TRUE;
	BOOL b_CHK = TRUE;
	for(lop=0; lop<200; lop++)//200ms동안 대기
	{
		if(Buf_Wait_DLDATASET == FALSE){
			/*b_CHK = TRUE;
			for(int t =0;t<17;t++){
				if(m_SerialInput_Cam[2+lop] != DATA[2+lop]){
					b_CHK = FALSE;
					break;
				}
			}*/
			if(m_SerialInput_Cam[2] == '1'){
				return TRUE;
			}else{
				return FALSE;
			}
		}
		DoEvents(1);
	}
	return ret;
}

BOOL CImageTesterDlg::DL_DATAWR(BYTE BLS,BYTE SESIZE)
{
	int		lop = 0;
	BOOL	ret = FALSE;

	BYTE DATA[20] ={'!','X','0','0','0',
					'0','0','0','0','0',	
					'0','0','0','0','0',
					'0','0','0','0','@'};
	
	
	DATA[2] = Hex2Ascii((BLS&0xf0)>>4);//ADDR
	DATA[3] = Hex2Ascii(BLS&0x0f);//HEX
	
	DATA[4] = Hex2Ascii((SESIZE&0xf0)>>4);//ADDR
	DATA[5] = Hex2Ascii(SESIZE&0x0f);//HEX
	
	Buf_Wait_Cam = TRUE;
	Buf_Wait_DLDATAWR = TRUE;
	
	SendData8Byte_Cam(DATA, 20);

	for(lop=0; lop<100; lop++)//2s동안 대기
	{
		DoEvents(10);
		if(Buf_Wait_DLDATAWR == FALSE){
			if(m_SerialInput_Cam[2] == '1'){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
	return ret;
}

BOOL CImageTesterDlg::DL_OFFSETRD()
{
	int		lop = 0;
	BOOL	ret = FALSE;

	BYTE DATA[20] ={'!','H','0','0','0',
					'0','0','0','0','0',
					'0','0','0','0','@'};
	
	
	for(int lop = 0;lop<2;lop++){
		m_RDOffset[lop] = 0X00;
	}

	Buf_Wait_Cam = TRUE;
	Buf_Wait_OFFRD = TRUE;
	
	SendData8Byte_Cam(DATA, 15);

	for(lop=0; lop<100; lop++)//2s동안 대기
	{
		DoEvents(10);
		if(Buf_Wait_OFFRD == FALSE){
			if(m_SerialInput_Cam[2] == '1'){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
	return ret;
}


void CImageTesterDlg::DL_OFFSETRD_ACK()
{
	BYTE HBUF,LBUF;
	if(m_SerialInput_Cam[2] == '1'){
		for(int lop = 0;lop<2;lop++){
			HBUF = Ascii2Hex(m_SerialInput_Cam[(lop*2)+3]);
			LBUF = Ascii2Hex(m_SerialInput_Cam[(lop*2)+4]);
			m_RDOffset[lop] = ((HBUF<<4)&0xf0)+(LBUF&0x0f);
		}
	}
	Buf_Wait_OFFRD = FALSE;
}

BOOL CImageTesterDlg::DL_OFFSETWR(BYTE OFFSETX,BYTE OFFSETY)
{
	int		lop = 0;
	BOOL	ret = FALSE;

	BYTE DATA[20] ={'!','P','0','0','0',
					'0','0','0','0','0',
					'0','0','0','0','@'};
	
	BYTE BUFX = OFFSETX;
	BYTE BUFY = OFFSETY;	


	for(lop = 0;lop<2;lop++){
		DATA[4-lop] = Hex2Ascii((BYTE)(BUFX&0x0F));
		DATA[6-lop] = Hex2Ascii((BYTE)(BUFY&0x0F));
		BUFX = BUFX>>4;
		BUFY = BUFY>>4;
	}

	Buf_Wait_Cam = TRUE;
	Buf_Wait_OFFWR = TRUE;
	
	SendData8Byte_Cam(DATA, 15);

	for(lop=0; lop<100; lop++)//2s동안 대기
	{
		DoEvents(10);
		if(Buf_Wait_OFFWR == FALSE){
			if(m_SerialInput_Cam[2] == '1'){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
	return ret;
}

void CImageTesterDlg::DL_OFFSETWR_ACK()
{
	Buf_Wait_OFFWR = FALSE;
}

BOOL CImageTesterDlg::DCC_CHECKSUM()
{
	int		lop = 0;
	BOOL	ret = FALSE;

	BYTE DATA[20] ={'!','B','0','0','0',
					'0','0','0','0','0',
					'0','0','0','0','@'};
	
	
	for(int lop = 0;lop<2;lop++){
		m_DCCChksum[lop] = 0X00;
	}
	m_DCCTotalChk = 0;

	Buf_Wait_Cam = TRUE;
	Buf_Wait_CHKSUM = TRUE;
	
	SendData8Byte_Cam(DATA, 15);

	for(lop=0; lop<100; lop++)//2s동안 대기
	{
		DoEvents(10);
		if(Buf_Wait_CHKSUM == FALSE){
			if(m_SerialInput_Cam[2] == '1'){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
	return ret;
}


void CImageTesterDlg::DCC_CHECKSUM_ACK()
{
	BYTE HBUF,LBUF;
	if(m_SerialInput_Cam[2] == '1'){
		for(int lop = 0;lop<2;lop++){
			HBUF = Ascii2Hex(m_SerialInput_Cam[(lop*2)+3]);
			LBUF = Ascii2Hex(m_SerialInput_Cam[(lop*2)+4]);
			m_DCCChksum[lop] = ((HBUF<<4)&0xf0)+(LBUF&0x0f);
		}
		m_DCCTotalChk = (((int)(m_DCCChksum[0])<<8)&0xff00)+((int)(m_DCCChksum[1])&0x00ff);
	}
	Buf_Wait_CHKSUM = FALSE;
}

void CImageTesterDlg::OnBnClickedBtnCommu()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_pCommWnd->IsWindowVisible() == FALSE){
		m_pCommWnd->ShowWindow(SW_SHOW);
	}else{
		m_pCommWnd->ShowWindow(SW_HIDE);
	}
}

void CImageTesterDlg::OnBnClickedButton1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_pCommWnd->IsWindowVisible() == FALSE){
		m_pCommWnd->ShowWindow(SW_SHOW);
	}else{
		m_pCommWnd->ShowWindow(SW_HIDE);
	}
}

void CImageTesterDlg::SavePositionSet(int Xdistance,int Ydistance)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str;
	if(Xdistance != m_dXModuleDistance){
		m_dXModuleDistance = Xdistance;
		str.Empty();
		str.Format("%d",m_dXModuleDistance);
		WritePrivateProfileString("STANDARD OPTION","MODULEX_DISTANCE_A",str,modelfile_Name);
		
	}	
	if(Ydistance != m_dYModuleDistance){
		m_dYModuleDistance = Ydistance;
		str.Empty();
		str.Format("%d",m_dYModuleDistance);
		WritePrivateProfileString("STANDARD OPTION","MODULEY_DISTANCE_A",str,modelfile_Name);
	}

	if(m_pSubStandOptWnd != NULL){
		m_pSubStandOptWnd->m_dXModuleDistance = m_dXModuleDistance;
		m_pSubStandOptWnd->m_dYModuleDistance = m_dYModuleDistance;
		m_pSubStandOptWnd->Position_Set();
	}

// 	if(m_pMasterMonitor != NULL){
// 		m_pMasterMonitor->UpdatePositionVal();
// 	}
	
}

void CImageTesterDlg::OnContextMenu(CWnd* pWnd, CPoint point)
{
	CMenu popup;
	CMenu* pMenu;
	popup.LoadMenu(IDR_MENU1);
	pMenu = popup.GetSubMenu(0);
	pMenu->TrackPopupMenu(TPM_LEFTALIGN || TPM_RIGHTBUTTON, point.x, point.y, this);
}

void CImageTesterDlg::OnCapture()
{
	TCHAR fileFilter[] = "IMG FILE(*.bmp)";
	CString str_Path = "";
	CString FileName = "";

	CFileDialog filedlg(FALSE, "BMP", "IMAGE", OFN_HIDEREADONLY, fileFilter);
	if (filedlg.DoModal() == IDOK)
	{
		FileName = filedlg.GetFileName();
		str_Path = filedlg.GetFolderPath();

		if(FileName == "")
			return;

		CString ImgFilePath = str_Path +("\\")+ FileName;
		OnImgCapture(ImgFilePath);
	}
}

void CImageTesterDlg::OnImgCapture(CString Path)
{
	m_ImgScan =1;						

	for(int k =0;k<10;k++)
	{
		if(m_ImgScan == 0)
			break;
		else
			DoEvents(50);
	}

	IplImage *Copyimage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 3);

	for(int y=0; y<480; y++)
	{
		for(int x=0; x<720; x++)
		{
			Copyimage->imageData[y*(Copyimage->widthStep) + x*3 + 0] = m_RGBScanbuf[y*(720*4) + (5+(710*x/720))*4 + 0];
			Copyimage->imageData[y*(Copyimage->widthStep) + x*3 + 1] = m_RGBScanbuf[y*(720*4) + (5+(710*x/720))*4 + 1];
			Copyimage->imageData[y*(Copyimage->widthStep) + x*3 + 2] = m_RGBScanbuf[y*(720*4) + (5+(710*x/720))*4 + 2];
		}
	}

	cvSaveImage(Path, Copyimage);
	cvReleaseImage(&Copyimage);
	AfxMessageBox(("경로: ")+Path +("\r\n저장되었습니다."));
}
void CImageTesterDlg::OnBnClickedButton6()
{
/*	MCU_READ_MODE(1);*/
}

void CImageTesterDlg::OnBnClickedButton8()
{
// 	unsigned long DATA = 510;
// 	BYTE Address_DATA[8];
// 	BYTE ADDR[8];
// 	for(int t=0; t<8; t++){
// 		Address_DATA[7-t] = (DATA>>(t*4))&0x0000000F;
// 		ADDR[7-t] =Hex2Ascii(Address_DATA[7-t]);
// 	}
// 	
// 	MCU_READ_ADDR(ADDR);
}

void CImageTesterDlg::OnBnClickedButton7()
{
	//MCU_READ_START();
}

void CImageTesterDlg::OnBnClickedButton9()
{
	//MCU_READ_MODE(0);
}

void CImageTesterDlg::OnBnClickedButton10()
{
	//MCU_READ_START_SEND();

}

void CImageTesterDlg::OnBnClickedButton11()
{
	//MCU_READ_MODE(1);

	//unsigned long DATA = 0;
	//BYTE Address_DATA[8];
	//BYTE ADDR[8];
	//int count = 0;
	//CString Scount;
	
	////426
	//	for(int lop=0; lop<=426;lop++)
	//	{	for(int t=0; t<8; t++){
	//			Address_DATA[7-t] = (DATA+(lop*255)>>(t*4))&0x0000000F;
	//			ADDR[7-t] =Hex2Ascii(Address_DATA[7-t]);
	//		}

	//		MCU_READ_ADDR(ADDR);
	//		MCU_READ_START();
	//		if(MCU_READ_START_SEND()){
	//			for(int i=0;i<255;i++){
	//				if(CAMREADBUFF[i]!=m_pVerifyOptWnd->BINBUF[(255*lop)+i]){
	//					count++;
	//				}
	//			}
	//		}
	//		Wait(100);
	//	}
	//
	//Scount.Format("%d BYTE의 데이터가 다릅니다.",count);
	//StatePrintf(Scount);

}

void CImageTesterDlg::OnBnClickedBtnCapture()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	/*if(CAMERA_STAT_CHK() == FALSE)
	{
		return;
	}*/

	TCHAR fileFilter[] = _T("이미지 파일(*.bmp)");
	CFileDialog dlg(FALSE, NULL, NULL, OFN_HIDEREADONLY, fileFilter);

	if (dlg.DoModal() == IDOK)
	{
		CString FolderPath = dlg.GetFolderPath();
		CString FileName = dlg.GetFileName();

		if(FileName == ""){
			return;
		}
		CString ImgFilePath_Org = FolderPath +("\\")+ FileName+("_Original.bmp");
		CString ImgFilePath_Tst = FolderPath +("\\")+ FileName+("_Test.bmp");
		
		IMG_Capture(ImgFilePath_Org);
		Wait(100);

		SaveScan(m_CamFrame.GetSafeHwnd(), ImgFilePath_Tst);
		Wait(100);
	}
}

void CImageTesterDlg::IMG_Capture(CString SavePath)
{

	IplImage *Copyimage = cvCreateImage(cvSize(720, 480), IPL_DEPTH_8U, 3);

	m_ImgScan = TRUE; // 영상 추출
	for(int t=0; t<50;t++)
	{
		if(m_ImgScan == FALSE){
			break;
		}
		Wait(10);
	}  

	if(m_ImgScan == TRUE){
		m_ImgScan = FALSE;
		StatePrintf("CAM OFF ->IMG CAPTURE FAIL !");
	}

	for(int y=0; y<480; y++)
	{
		for(int x=0; x<720; x++)
		{
			Copyimage->imageData[y*(Copyimage->widthStep) + x*3 + 0] = m_RGBScanbuf[(y)*(720*4) + x*4 + 0];
			Copyimage->imageData[y*(Copyimage->widthStep) + x*3 + 1] = m_RGBScanbuf[(y)*(720*4) + x*4 + 1];
			Copyimage->imageData[y*(Copyimage->widthStep) + x*3 + 2] = m_RGBScanbuf[(y)*(720*4) + x*4 + 2];	
		}
	}
	cvSaveImage(SavePath, Copyimage);
	cvReleaseImage(&Copyimage);
}


//void CImageTesterDlg::SaveScan(HWND hWnd, LPCTSTR lpszFileName)
//{
//	HDC hDC = ::GetDC(hWnd);
//
//	// 윈도우의 크기 알아내기
//	RECT rc;
//	::GetClientRect(hWnd, &rc);
//
//	// 비트맵(DDB) 생성하기
//	HDC hMemDC = CreateCompatibleDC(hDC);
//	HBITMAP hBitmap = CreateCompatibleBitmap(hDC, rc.right, rc.bottom);
//	HBITMAP hBmpOld = (HBITMAP)SelectObject(hMemDC, hBitmap);
//	BitBlt(hMemDC, 0, 0, rc.right, rc.bottom, hDC, 0, 0, SRCCOPY);
//	SelectObject(hMemDC, hBmpOld);
//	DeleteDC(hMemDC);
//
//	// 비트맵(DIB) 사양 설정
//	BITMAPINFOHEADER bmih;
//	ZeroMemory(&bmih, sizeof(BITMAPINFOHEADER));
//	bmih.biSize = sizeof(BITMAPINFOHEADER);
//	bmih.biWidth = rc.right;
//	bmih.biHeight = rc.bottom;
//	bmih.biPlanes = 1;
//	bmih.biBitCount = 24;
//	bmih.biCompression = BI_RGB;
//
//	// 비트맵(DIB) 데이터 추출
//	GetDIBits(hDC, hBitmap, 0, rc.bottom, NULL, (LPBITMAPINFO)&bmih, DIB_RGB_COLORS);
////	LPBYTE lpBits = new BYTE[bmih.biSizeImage];
//	GetDIBits(hDC, hBitmap, 0, rc.bottom, m_pFailScan, (LPBITMAPINFO)&bmih, DIB_RGB_COLORS);
//	::ReleaseDC(hWnd, hDC);
//
//	DeleteObject(hBitmap);
//
//	///////
//
//	// 비트맵 파일 헤더 설정
//	BITMAPFILEHEADER bmfh;
//	bmfh.bfType = 'MB';
//	bmfh.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + bmih.biSizeImage;
//	bmfh.bfReserved1 = 0;
//	bmfh.bfReserved2 = 0;
//	bmfh.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
//
//	// 비트맵 파일 생성 및 데이터 저장
//	DWORD dwWritten;
//	HANDLE hFile = CreateFile(lpszFileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//
//		FILE_ATTRIBUTE_NORMAL, NULL);
//	WriteFile(hFile, &bmfh, sizeof(BITMAPFILEHEADER), &dwWritten, NULL);
//	WriteFile(hFile, &bmih, sizeof(BITMAPINFOHEADER), &dwWritten, NULL);
//	WriteFile(hFile, m_pFailScan, bmih.biSizeImage, &dwWritten, NULL);
//	CloseHandle(hFile);
//}
void CImageTesterDlg::Original_ImageCapture(){

	CFileFind modelfind;

// 	if (CAMERA_STAT_CHK() == FALSE){
// 		AfxMessageBox("카메라를 켜고 다시 시도해 주세요. ");
// 		return;;
// 	}

	HWND hWnd = m_CamFrame.GetSafeHwnd();

	m_ImgScan_Gray =1;						

	for(int i =0;i<10;i++){
		if (m_ImgScan_Gray == 0){
			break;
		}else{
			DoEvents(50);
		}
	}

	IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_16U, 1);
	BYTE R,G,B;
	double Sum_Y;

	memcpy(OriginImage->imageData, m_GRAYScanbuf, CAM_IMAGE_WIDTH*CAM_IMAGE_HEIGHT*2);


	CTime time = CTime::GetTickCount();  
	int y = time.GetYear();   
	int m = time.GetMonth();   
	int d = time.GetDay(); 

	CTime thetime = CTime::GetCurrentTime();
	CString str;
	CREATE_Folder(m_modelName, 5);

	int hour=0,minute=0,second=0,hour2=0;
	hour=thetime.GetHour(); minute=thetime.GetMinute(); second=thetime.GetSecond();
	str.Format("%02dh_%02dm_%02ds",hour,minute,second);

	CString oripath = MODEL_FOLDERPATH_P + "\\" + str + "_origin.png";
	imagepic_path_copy = MODEL_FOLDERPATH_P +*"\\" + str + ".png";
	winimage_path_copy = MODEL_FOLDERPATH_P + *"\\" + str + "_PIC.png";
 //MODEL_FOLDERPATH_P + "\\" + m_szCode + _T("_") + m_strdegre + "_UI.png";


	if(!modelfind.FindFile(imagepic_path_copy))		//같은모델이없을경우시행한다. 
	{ 
		//Capture(winimage_path_copy);
		b_CaptureMode = TRUE;
		b_CaptureWindowMode = TRUE;
		cvSaveImage(oripath, OriginImage);
// 		CString temp;
// 		temp.Format(_T("%s 에 저장 되었습니다."),MODEL_FOLDERPATH_P);
// 		AfxMessageBox(temp);

	}else{
		Original_ImageCapture();
	}

	cvReleaseImage(&OriginImage);
}
void CImageTesterDlg::PNG_ImageCapture(CString TIME){

	CFileFind modelfind;

	IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_16U, 1);
	BYTE R, G, B;
	double Sum_Y;

	memcpy(OriginImage->imageData, m_GRAYScanbuf, CAM_IMAGE_WIDTH*CAM_IMAGE_HEIGHT * 2);

	CTime time = CTime::GetTickCount();
	int y = time.GetYear();
	int m = time.GetMonth();
	int d = time.GetDay();

	CTime thetime = CTime::GetCurrentTime();
	CString str;
	CREATE_Folder(m_modelName, 5);

	CString oripath = MODEL_FOLDERPATH_P + "\\" + TIME + "_origin.png";
	imagepic_path_copy = MODEL_FOLDERPATH_P + *"\\" + TIME + ".png";
	winimage_path_copy = MODEL_FOLDERPATH_P + *"\\" + TIME + "_PIC.png";
	//MODEL_FOLDERPATH_P + "\\" + m_szCode + _T("_") + m_strdegre + "_UI.png";


	if (!modelfind.FindFile(imagepic_path_copy))		//같은모델이없을경우시행한다. 
	{
		b_CaptureMode = TRUE;
		b_CaptureWindowMode = TRUE;
		cvSaveImage(oripath, OriginImage);

	}

	cvReleaseImage(&OriginImage);
}
void CImageTesterDlg::Capture(CString Path) 
{
	CClientDC  dc(this); //dc생성
	CRect rc(0,0,1270,930); 
	//CRect rc(0,0,GetSystemMetrics(SM_CXMAXTRACK),GetSystemMetrics(SM_CYMAXTRACK)); 
	//전체화면 구역설정

	CBitmap  captureBmp; 
	captureBmp.CreateCompatibleBitmap(&dc,  rc.Width(),  rc.Height()); 

	CDC  dcMem; 
	dcMem.CreateCompatibleDC(&dc); 
	CBitmap  *pOldBitmap  =  dcMem.SelectObject(&captureBmp); 

	//  복사한다. 
	dcMem.BitBlt(0,0,rc.Width(),  rc.Height(),  &dc,  0,0,SRCCOPY); 
	dcMem.SelectObject(pOldBitmap); 

	HANDLE hDIB = DDBToDIB(captureBmp, BI_RGB, NULL); // 이 함수들은 여기! bu.cpp
	if(hDIB != NULL)
	{
		WriteDIB(Path, hDIB);
		//CloseHandle(hDIB);
	}
	captureBmp.DeleteObject();



}
HANDLE CImageTesterDlg::DDBToDIB(CBitmap &bitmap, DWORD dwCompression, CPalette *pPal)
{
	BITMAP   bm;
	BITMAPINFOHEADER bi;
	LPBITMAPINFOHEADER  lpbi;
	DWORD   dwLen;
	HANDLE   hDIB;
	HANDLE   handle;
	HDC    hDC;
	HPALETTE  hPal;


	ASSERT( bitmap.GetSafeHandle() );

	// The function has no arg for bitfields
	if( dwCompression == BI_BITFIELDS )
		return NULL;

	// If a palette has not been supplied use defaul palette
	hPal = (HPALETTE) pPal->GetSafeHandle();
	if (hPal==NULL)
		hPal = (HPALETTE) GetStockObject(DEFAULT_PALETTE);

	// Get bitmap information
	bitmap.GetObject(sizeof(bm),(LPSTR)&bm);

	// Initialize the bitmapinfoheader
	bi.biSize  = sizeof(BITMAPINFOHEADER);
	bi.biWidth  = bm.bmWidth;
	bi.biHeight   = bm.bmHeight;
	bi.biPlanes   = 1;
	bi.biBitCount  = bm.bmPlanes * bm.bmBitsPixel;
	bi.biCompression = dwCompression;
	bi.biSizeImage  = 0;
	bi.biXPelsPerMeter = 0;
	bi.biYPelsPerMeter = 0;
	bi.biClrUsed  = 0;
	bi.biClrImportant = 0;

	// Compute the size of the  infoheader and the color table
	int nColors = (1 << bi.biBitCount);
	if( nColors > 256 )
		nColors = 0;
	dwLen  = bi.biSize + nColors * sizeof(RGBQUAD);

	// We need a device context to get the DIB from
	hDC = ::GetDC(NULL);
	hPal = SelectPalette(hDC,hPal,FALSE);
	RealizePalette(hDC);

	// Allocate enough memory to hold bitmapinfoheader and color table
	hDIB = GlobalAlloc(GMEM_FIXED,dwLen);

	if (!hDIB){
		SelectPalette(hDC,hPal,FALSE);
		::ReleaseDC(NULL,hDC);
		return NULL;
	}

	lpbi = (LPBITMAPINFOHEADER)hDIB;

	*lpbi = bi;

	// Call GetDIBits with a NULL lpBits param, so the device driver 
	// will calculate the biSizeImage field 
	GetDIBits(hDC, (HBITMAP)bitmap.GetSafeHandle(), 0L, (DWORD)bi.biHeight,
		(LPBYTE)NULL, (LPBITMAPINFO)lpbi, (DWORD)DIB_RGB_COLORS);

	bi = *lpbi;

	// If the driver did not fill in the biSizeImage field, then compute it
	// Each scan line of the image is aligned on a DWORD (32bit) boundary
	if (bi.biSizeImage == 0){
		bi.biSizeImage = ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8)
			* bi.biHeight;

		// If a compression scheme is used the result may infact be larger
		// Increase the size to account for this.
		if (dwCompression != BI_RGB)
			bi.biSizeImage = (bi.biSizeImage * 3) / 2;
	}

	// Realloc the buffer so that it can hold all the bits
	dwLen += bi.biSizeImage;
	if (handle = GlobalReAlloc(hDIB, dwLen, GMEM_MOVEABLE))
		hDIB = handle;
	else{
		GlobalFree(hDIB);

		// Reselect the original palette
		SelectPalette(hDC,hPal,FALSE);
		::ReleaseDC(NULL,hDC);
		return NULL;
	}

	// Get the bitmap bits
	lpbi = (LPBITMAPINFOHEADER)hDIB;

	// FINALLY get the DIB
	BOOL bGotBits = GetDIBits( hDC, (HBITMAP)bitmap.GetSafeHandle(),
		0L,    // Start scan line
		(DWORD)bi.biHeight,  // # of scan lines
		(LPBYTE)lpbi    // address for bitmap bits
		+ (bi.biSize + nColors * sizeof(RGBQUAD)),
		(LPBITMAPINFO)lpbi,  // address of bitmapinfo
		(DWORD)DIB_RGB_COLORS);  // Use RGB for color table//////////

	if( !bGotBits )
	{
		GlobalFree(hDIB);

		SelectPalette(hDC,hPal,FALSE);
		::ReleaseDC(NULL,hDC);
		return NULL;
	}

	SelectPalette(hDC,hPal,FALSE);
	::ReleaseDC(NULL,hDC);
	return hDIB;

}

bool CImageTesterDlg::WriteDIB(CString szFile, HANDLE hDIB)
{
	BITMAPFILEHEADER    hdr;
	LPBITMAPINFOHEADER  lpbi;

	if(!hDIB)
		return FALSE;

	CFile file;

	if(!file.Open( szFile, CFile::modeWrite|CFile::modeCreate) )
		return FALSE;
	lpbi = (LPBITMAPINFOHEADER)hDIB;
	int nColors = 1 << lpbi->biBitCount;

	// Fill in the fields of the file header 
	hdr.bfType      = ((WORD) ('M' << 8) | 'B');    // is always "BM"
	hdr.bfSize      = GlobalSize (hDIB) + sizeof(hdr);
	hdr.bfReserved1 = 0;
	hdr.bfReserved2 = 0;
	hdr.bfOffBits   = (DWORD) (sizeof( hdr ) + lpbi->biSize + nColors * sizeof(RGBQUAD));

	// Write the file header 
	file.Write(&hdr, sizeof(hdr));

	// Write the DIB header and the bits 
	file.Write(lpbi, GlobalSize(hDIB));

	return TRUE;
}

//160922 KDY 추가 //////////////////////

BOOL CImageTesterDlg::Manual_Exp_Mode()
{
	int		lop = 0;
	BOOL	ret = FALSE;

	BYTE DATA[20] ={'!','2','0','0','0',
		'0','0','0','0','0',
		'0','0','0','0','0',
		'0','0','0','0','@'};

	Buf_Wait_Cam = TRUE;


	SendData8Byte_Cam(DATA, 20);

	for(lop=0; lop<100; lop++)//2s동안 대기
	{
		DoEvents(10);
		if(Buf_Wait_Cam == FALSE){
			if(m_SerialInput_Cam[2] == '1'){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
	return ret;
}

BOOL CImageTesterDlg::Manual_Exp_Gain_Ctrl(CString Target_Value)
{
	int		lop = 0;
	BOOL	ret = FALSE;

	BYTE DATA[20] ={'!','3','0','0','0',
		'0','0','0','0','0',
		'0','0','0','0','0',
		'0','0','0','0','@'};

	char* buf = LPSTR(LPCTSTR(Target_Value));

	DATA[3] = buf[0];
	DATA[4] = buf[1];
	DATA[5] = buf[2];
	DATA[6] = buf[3];


	int buf2 = 0;
	int buf3 = 0;
	int buf4 = 0;

	DATA[3] = '0';

	buf2 = atoi(Target_Value);
	switch(buf2 / 256)
	{
		case 0:
			DATA[4] = '0';
			break;
		case 1:
			DATA[4] = '1';
			break;
		default:
			break;
	}


	buf3 = buf2 % 256;
	switch(buf3 / 16)
	{
		case 0:
			DATA[5] = '0';
			break;
		case 1:
			DATA[5] = '1';
			break;
		case 2:
			DATA[5] = '2';
			break;
		case 3:
			DATA[5] = '3';
			break;
		case 4:
			DATA[5] = '4';
			break;
		case 5:
			DATA[5] = '5';
			break;
		case 6:
			DATA[5] = '6';
			break;
		case 7:
			DATA[5] = '7';
			break;
		case 8:
			DATA[5] = '8';
			break;
		case 9:
			DATA[5] = '9';
			break;
		case 10:
			DATA[5] = 'A';
			break;
		case 11:
			DATA[5] = 'B';
			break;
		case 12:
			DATA[5] = 'C';
			break;
		case 13:
			DATA[5] = 'D';
			break;
		case 14:
			DATA[5] = 'E';
			break;
		case 15:
			DATA[5] = 'F';
			break;
		default:
			break;
	}


	buf4 = buf3 % 16;
	switch(buf4)
	{
		case 0:
			DATA[6] = '0';
			break;
		case 1:
			DATA[6] = '1';
			break;
		case 2:
			DATA[6] = '2';
			break;
		case 3:
			DATA[6] = '3';
			break;
		case 4:
			DATA[6] = '4';
			break;
		case 5:
			DATA[6] = '5';
			break;
		case 6:
			DATA[6] = '6';
			break;
		case 7:
			DATA[6] = '7';
			break;
		case 8:
			DATA[6] = '8';
			break;
		case 9:
			DATA[6] = '9';
			break;
		case 10:
			DATA[6] = 'A';
			break;
		case 11:
			DATA[6] = 'B';
			break;
		case 12:
			DATA[6] = 'C';
			break;
		case 13:
			DATA[6] = 'D';
			break;
		case 14:
			DATA[6] = 'E';
			break;
		case 15:
			DATA[6] = 'F';
			break;
		default:
			break;
	}

	
	

	/*DATA[3] = Target_Value.Mid(0,1);
	DATA[4] = Target_Value.Mid(1,1);
	DATA[5] = Target_Value.Mid(2,1);
	DATA[6] = Target_Value.Mid(3,1);*/

	Buf_Wait_Cam = TRUE;
	
	SendData8Byte_Cam(DATA, 20);

	for(lop=0; lop<100; lop++)//2s동안 대기
	{
		DoEvents(10);
		if(Buf_Wait_Cam == FALSE){
			if(m_SerialInput_Cam[2] == '1'){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
	return ret;
}


//////// 추가끝 //////////////////////





void CImageTesterDlg::OnBnClickedButton4()
{
	LGEVideo_Start(1);
}


void CImageTesterDlg::OnBnClickedButton3()
{
	LGEVideo_Stop();
}


void CImageTesterDlg::OnBnClickedButton12()
{

}


void CImageTesterDlg::OnBnClickedButton13()
{

}


void CImageTesterDlg::OnBnClickedButton14()
{
// 	HWND hWnd = m_CamFrame.GetSafeHwnd();
// 	SaveScan(hWnd, _T("D:\\aa.bmp"));
	Original_ImageCapture();
}

void CImageTesterDlg::SetProperty_ALL(CLGE_LIB_Data Property){

	SetProperty_TOF1_Emission(Property.b_Emission);
	SetProperty_TOF1_CaliSlope(Property.m_Slope);
	SetProperty_TOF1_Alpha(Property.m_Alpha);
	SetProperty_TOF1_IRMode(Property.b_IRMode);
	SetProperty_TOF1_IRGain(Property.m_IRGain);
	SetProperty_TOF1_SmallSignalRemoval(Property.m_Removal);
	SetUse_Gain(Property.b_ViwerGain, Property.m_ViwerGain);

}
void CImageTesterDlg::SetProperty_TOF1_Alpha(UINT nValue)
{
	// ( 0 ~ 1000 )
	int iValue = (int)nValue;
	//m_LGEVideo.SetProperty_TOF1_Alpha(iValue);
}

void CImageTesterDlg::SetProperty_TOF1_CaliSlope(UINT wValue)
{
	// ( 0 ~ 65535 )
	int iValue = (int)wValue;
	//m_LGEVideo.SetProperty_TOF1_CaliSlope(iValue);
}


void CImageTesterDlg::SetProperty_TOF1_SmallSignalRemoval(UINT wValue)
{
	// ( 0 ~ 65535 )
	int iValue = (int)wValue;
	//m_LGEVideo.SetProperty_TOF1_SmallSignalRemoval(iValue);
}

void CImageTesterDlg::SetProperty_TOF1_IRMode(BOOL bOn)
{
	// ( 1: on , 0 : off )
	int iValue = (int)bOn;
	//m_LGEVideo.SetProperty_TOF1_IRMode(iValue);
}

void CImageTesterDlg::SetProperty_TOF1_IRGain(UINT byValue)
{
	// (1, 2, 4, 8, 16, 32)
	int iValue = (int)byValue;
	//m_LGEVideo.SetProperty_TOF1_IRGain(iValue);
}

void CImageTesterDlg::SetProperty_TOF1_Emission(UINT nValue)
{
	// ( 1: on , 0 : off )
	int iValue = (int)nValue;
	//m_LGEVideo.SetProperty_TOF1_Emission(iValue);
}
void CImageTesterDlg::SetUse_Gain(__in BOOL bUse, __in int nExp)
{
	// ( 1: on , 0 : off )
	int iValue = (int)nExp;
	//m_LGEVideo.SetUse_Gain(bUse, iValue);
	if (bUse == TRUE){		
		//stDaqOpt.dShiftExp = nExp;
		stDaqOpt.SetExposure(nExp);
		m_LVDSVideo.SetDAQOption(0, stDaqOpt);
	}
	else{
		//stDaqOpt.dShiftExp = 0;
		stDaqOpt.SetExposure(0);
		m_LVDSVideo.SetDAQOption(0, stDaqOpt);
	}
}


void CImageTesterDlg::OnBnClickedButton2()
{
#if Def_IMGMode
	SetUse_Gain(CAMSET_SAVE.b_ViwerGain, CAMSET_SAVE.m_ViwerGain);
	LoadPNGImage();
// 	LGEVideo_Start(1);
// 	Power_On();
#else

	if (CAMERA_STAT_CHK() == FALSE){
		AfxMessageBox("카메라를 켜고 다시 시도해 주세요. ");
		return;;
	}

	m_ImgScan_Gray = 1;
	for (int i = 0; i < 50; i++)
	{
		DoEvents(10);
		if (m_ImgScan_Gray == 0)
		{
			break;
		}
	}
	CTime time = CTime::GetTickCount();
	int y = time.GetYear();
	int m = time.GetMonth();
	int d = time.GetDay();

	CTime thetime = CTime::GetCurrentTime();
	CREATE_Folder(m_modelName, 5);

	int hour = 0, minute = 0, second = 0, hour2 = 0;
	hour = thetime.GetHour(); minute = thetime.GetMinute(); second = thetime.GetSecond();

	CString strFilename;
	strFilename.Format("%s\\RAW_%02dh_%02dm_%02ds.csv", MODEL_FOLDERPATH_P, hour, minute, second);

	CStdioFile File;
	CFileException e;
	if (!File.Open((LPCTSTR)strFilename, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
		return;

	}

	CString strOut = "";
	CString str = "";


	for (int t = 0; t < CAM_IMAGE_HEIGHT; t++){
		for (int i = 0; i < CAM_IMAGE_WIDTH; i++){
			str.Empty();
			str.Format("%d,", m_GRAYScanbuf[CAM_IMAGE_WIDTH*t + i]);
			strOut += str;
			if (i == CAM_IMAGE_WIDTH - 1){
				strOut += "\n";
			}
		}

	}
	File.WriteString(LPCTSTR(strOut));
	File.Close();

	strFilename.Format("%s\\RAW_%02dh_%02dm_%02ds.raw", MODEL_FOLDERPATH_P, hour, minute, second);
	m_LVDSVideo.SaveRawImage(0, strFilename);

#endif
}
void CImageTesterDlg::RAW_ImageCapture(CString TIME)
{
	CREATE_Folder(m_modelName, 5);

	CString strFilename;
	strFilename.Format("%s\\RAW_%s.csv", MODEL_FOLDERPATH_P, TIME);

	CStdioFile File;
	CFileException e;
	if (!File.Open((LPCTSTR)strFilename, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone)){
		return;

	}

	CString strOut = "";
	CString str = "";


	for (int t = 0; t < CAM_IMAGE_HEIGHT; t++){
		for (int i = 0; i < CAM_IMAGE_WIDTH; i++){
			str.Empty();
			str.Format("%d,", m_GRAYScanbuf[CAM_IMAGE_WIDTH*t + i]);
			strOut += str;
			if (i == CAM_IMAGE_WIDTH - 1){
				strOut += "\n";
			}
		}

	}
	File.WriteString(LPCTSTR(strOut));
	File.Close();

}
//-------------------------------------------------------Center광원 통신
LRESULT CImageTesterDlg::OnConCommunication_Center(WPARAM wParam, LPARAM lParam)		// 실제 통신이 가능한 부분. 받는 데이터를 관장 //khk UART Recieve
{
	if (Read_Comport_ConCenter(&m_ControlPort_Center)){//합당한 데이터가 들어왔을 경우 

	}
	return TRUE;
}
char CImageTesterDlg::SendData8Byte_ConCenter(unsigned char * TxData, int Num)
{
	char ret = FALSE;
	if (m_ControlPort_Center.m_bConnected)
	{
		if (m_ControlPort_Center.WriteComm(TxData, Num) == Num)
		{
			ret = TRUE;
		}
		if (ret){
			if (m_pCommWnd != NULL){
				m_pCommWnd->DisplayDebugData(TxData, Num, TX_MODE, 0);
			}
		}
	}
	return ret;
}
BOOL CImageTesterDlg::Read_Comport_ConCenter(CCommThread* serialport) //m_SerialInput
{
	BOOL retVal = FALSE;
	int iSize = (serialport->m_QueueRead).GetSize();
	if (iSize)
	{
		BYTE aByte;
		for (int i = 0; i < iSize; i++)
		{
			(serialport->m_QueueRead).GetByte(&aByte);  //글자 하나씩 읽어옴
			if (aByte == '!'){
				memset(m_RXData_ConCenter.data, 0, MAX_BUF_LEN);
				memset(m_SerialInput_ConCenter, 0, sizeof(m_SerialInput_ConCenter));
				m_RXData_ConCenter.len = 0;
			}
			m_RXData_ConCenter.data[m_RXData_ConCenter.len] = aByte;
			m_RXData_ConCenter.len++;
			if (aByte == '@'){
				if (m_RXData_ConCenter.len > 1024){
					m_RXData_ConCenter.len = 1024;
				}
				memcpy(m_SerialInput_ConCenter, m_RXData_ConCenter.data, m_RXData_ConCenter.len);
				if (m_pCommWnd != NULL){//데이터 포멧이 정상적으로 완성이 되었을때 제어된다.
					m_pCommWnd->DisplayDebugData(m_RXData_ConCenter.data, m_RXData_ConCenter.len, RX_MODE, 0);
				}
				Chk_Trans_ConCenter();
				memset(m_RXData_ConCenter.data, 0, MAX_BUF_LEN);
				m_RXData_ConCenter.len = 0;
				retVal = TRUE;
			}
		}
	}

	return retVal;
}
void CImageTesterDlg::Chk_Trans_ConCenter()
{
	bool DataChk = FALSE;
	BYTE chkdata = m_SerialInput_ConCenter[1];
	switch (chkdata){
	case '1':
		break;
	case '2':
		break;
	case '3':
		break;
	default:
		break;
	}
	Buf_Wait_ConCenter = FALSE; //ACK가 정상적으로 왔다는 신호
}
BOOL CImageTesterDlg::PortChk_ConCenter()
{
	return m_ControlPort_Center.m_bConnected;
}
BOOL CImageTesterDlg::Portopen_ConCenter(){
	if (m_ControlPort_Center.m_bConnected == FALSE)  //포트가 닫혀 있을 경우에만 포트를 열기 위해
	{
		if (m_ControlPort_Center.OpenPort(m_ConSerial_Center) == TRUE)
		{
			//PORT_STAT_CAM(1, "CONNECTING");
			m_OldConSerial_Center = m_ConSerial_Center;
			return TRUE;
		}
		else{
			//PORT_STAT_CAM(0, "DISCONNECT");
			return FALSE;
		}
	}
	else if (m_OldConSerial_Center.Port == m_ConSerial_Center.Port){
		//PORT_STAT_CAM(1, "CONNECTING");
		return TRUE;
	}
	else{
		m_ControlPort_Center.ClosePort();
		DoEvents(300);
		if (m_ControlPort_Center.OpenPort(m_ConSerial_Center) == TRUE)
		{
			//PORT_STAT_CAM(1, "CONNECTING");
			m_OldConSerial_Center = m_ConSerial_Center;
			return TRUE;
		}
		else{
			//PORT_STAT_CAM(0, "DISCONNECT");
			return FALSE;
		}
	}
}
void CImageTesterDlg::PortClose_ConCenter(){
	if (m_ControlPort_Center.m_bConnected == TRUE)  //포트가 닫혀 있을 경우에만 포트를 열기 위해
	{
		m_ControlPort_Center.ClosePort();
	}
	//PORT_STAT_CAM(0, "DISCONNECT");
}
BOOL CImageTesterDlg::PortTst_ConCenter(){

	BYTE DATA[30] = { '!', '0', '0', '0', '0',
		'0', '0', '0', '0', '0',
		'0', '0', '0', '0', '0',
		'0', '0', '0', '0', '0',
		'0', '0', '@' };
	memset(m_SerialInput_ConCenter, 0, sizeof(m_SerialInput_ConCenter));
	Buf_Wait_ConCenter= TRUE;
	SendData8Byte_ConCenter(DATA, 23);
	BYTE ASCNUM = '1';
	for (int lop = 0; lop < 50; lop++){//0.1초동안 ACK를 기다린다. 
		if (Buf_Wait_ConCenter == FALSE){
			if ((m_SerialInput_ConCenter[1] == '0') && (m_SerialInput_ConCenter[2] == ASCNUM)){
				return TRUE;
			}
		}
		DoEvents(10);
	}
	return FALSE;
}
BOOL CImageTesterDlg::AutoPortChk_ConCenter(){
	CString str = "";
	if (Portopen_ConCenter() == TRUE){
		if (PortTst_ConCenter()){
			StatePrintf("Center BackLight Control PortChk Success..Port COM %d Select", m_ConSerial_Center.Port + 1);
			return TRUE; //우선 기본적으로 지정되어 있는 포트를 우선 체크한다.
		}
		else{
			PortClose_ConCenter();
		}
	}

	int BackPort = m_ConSerial_Center.Port;
// 	for (int lop = 0; lop < 14; lop++){
// 		//if(BackPort != lop)
// 		{
// 			m_ConSerial_Center.Port = lop;
// 			if (m_ControlPort_Center.OpenPort(m_ConSerial_Center) == TRUE){
// 				if (PortTst_ConCenter()){
// 					//if(BackPort != lop)
// 					{
// 						m_ConSerial_Center.Port = lop; //
// 						SaveSerialSet(&m_ConSerial_Center, "CENTER_CONSERIAL", m_inifilename);
// 						if (m_pStandOptWnd != NULL){
// 							m_pStandOptWnd->PortUpdate();
// 						}
// 					}
// 					StatePrintf("Center BackLight Control PortChk Success..Port COM %d Select", m_ConSerial_Center.Port + 1);
// 					m_OldConSerial_Center = m_ConSerial_Center;
// 					return TRUE;
// 				}
// 				else{
// 					m_ControlPort_Center.ClosePort();
// 				}
// 			}
// 		}
// 	}
	if (m_ControlPort_Center.OpenPort(m_ConSerial_Center) == TRUE){
		if (PortTst_ConCenter()){
			StatePrintf("Center BackLight Control PortChk Success..Port COM %d Select", m_ConSerial_Center.Port + 1);
			return TRUE;
		}
	}
	m_ConSerial_Center.Port = BackPort;
	StatePrintf("Center BackLight Control Chk Fails.....Manual Port COM %d Select", m_ConSerial_Center.Port + 1);
	DoEvents(100);
	return FALSE;
}
//-------------------------------------------------------Side광원 통신
LRESULT CImageTesterDlg::OnConCommunication_Side(WPARAM wParam, LPARAM lParam)		// 실제 통신이 가능한 부분. 받는 데이터를 관장 //khk UART Recieve
{
	if (Read_Comport_ConSide(&m_ControlPort_Side)){//합당한 데이터가 들어왔을 경우 

	}
	return TRUE;
}
char CImageTesterDlg::SendData8Byte_ConSide(unsigned char * TxData, int Num)
{
	char ret = FALSE;
	if (m_ControlPort_Side.m_bConnected)
	{
		if (m_ControlPort_Side.WriteComm(TxData, Num) == Num)
		{
			ret = TRUE;
		}
		if (ret){
			if (m_pCommWnd != NULL){
				m_pCommWnd->DisplayDebugData(TxData, Num, TX_MODE, 1);
			}
		}
	}
	return ret;
}
BOOL CImageTesterDlg::Read_Comport_ConSide(CCommThread* serialport) //m_SerialInput
{
	BOOL retVal = FALSE;
	int iSize = (serialport->m_QueueRead).GetSize();
	if (iSize)
	{
		BYTE aByte;
		for (int i = 0; i < iSize; i++)
		{
			(serialport->m_QueueRead).GetByte(&aByte);  //글자 하나씩 읽어옴
			if (aByte == '!'){
				memset(m_RXData_ConSide.data, 0, MAX_BUF_LEN);
				memset(m_SerialInput_ConSide, 0, sizeof(m_SerialInput_ConSide));
				m_RXData_ConSide.len = 0;
			}
			m_RXData_ConSide.data[m_RXData_ConSide.len] = aByte;
			m_RXData_ConSide.len++;
			if (aByte == '@'){
				if (m_RXData_ConSide.len > 1024){
					m_RXData_ConSide.len = 1024;
				}
				memcpy(m_SerialInput_ConSide, m_RXData_ConSide.data, m_RXData_ConSide.len);
				if (m_pCommWnd != NULL){//데이터 포멧이 정상적으로 완성이 되었을때 제어된다.
					m_pCommWnd->DisplayDebugData(m_RXData_ConSide.data, m_RXData_ConSide.len, RX_MODE, 1);
				}
				Chk_Trans_ConSide();
				memset(m_RXData_ConSide.data, 0, MAX_BUF_LEN);
				m_RXData_ConSide.len = 0;
				retVal = TRUE;
			}
		}
	}

	return retVal;
}
void CImageTesterDlg::Chk_Trans_ConSide()
{
	bool DataChk = FALSE;
	BYTE chkdata = m_SerialInput_ConSide[1];
	switch (chkdata){
	case '1':
		break;
	case '2':
		break;
	case '3':
		break;
	default:
		break;
	}
	Buf_Wait_ConSide = FALSE; //ACK가 정상적으로 왔다는 신호
}
BOOL CImageTesterDlg::PortChk_ConSide()
{
	return m_ControlPort_Side.m_bConnected;
}
BOOL CImageTesterDlg::Portopen_ConSide(){
	if (m_ControlPort_Side.m_bConnected == FALSE)  //포트가 닫혀 있을 경우에만 포트를 열기 위해
	{
		if (m_ControlPort_Side.OpenPort(m_ConSerial_Side) == TRUE)
		{
			//PORT_STAT_CAM(1, "CONNECTING");
			m_OldConSerial_Side = m_ConSerial_Side;
			return TRUE;
		}
		else{
			//PORT_STAT_CAM(0, "DISCONNECT");
			return FALSE;
		}
	}
	else if (m_OldConSerial_Side.Port == m_ConSerial_Side.Port){
		//PORT_STAT_CAM(1, "CONNECTING");
		return TRUE;
	}
	else{
		m_ControlPort_Side.ClosePort();
		DoEvents(300);
		if (m_ControlPort_Side.OpenPort(m_ConSerial_Side) == TRUE)
		{
			//PORT_STAT_CAM(1, "CONNECTING");
			m_OldConSerial_Side = m_ConSerial_Side;
			return TRUE;
		}
		else{
			//PORT_STAT_CAM(0, "DISCONNECT");
			return FALSE;
		}
	}
}
void CImageTesterDlg::PortClose_ConSide(){
	if (m_ControlPort_Side.m_bConnected == TRUE)  //포트가 닫혀 있을 경우에만 포트를 열기 위해
	{
		m_ControlPort_Side.ClosePort();
	}
	//PORT_STAT_CAM(0, "DISCONNECT");
}
BOOL CImageTesterDlg::PortTst_ConSide(){

	BYTE DATA[30] = { '!', '0', '0', '0', '0',
		'0', '0', '0', '0', '0',
		'0', '0', '0', '0', '0',
		'0', '0', '0', '0', '0',
		'0', '0', '@' };
	memset(m_SerialInput_ConSide, 0, sizeof(m_SerialInput_ConSide));
	Buf_Wait_ConSide = TRUE;
	SendData8Byte_ConSide(DATA, 23);
	BYTE ASCNUM = '2';
	for (int lop = 0; lop < 50; lop++){//0.1초동안 ACK를 기다린다. 
		if (Buf_Wait_ConSide == FALSE){
			if ((m_SerialInput_ConSide[1] == '0') && (m_SerialInput_ConSide[2] == ASCNUM)){
				return TRUE;
			}
		}
		DoEvents(10);
	}
	return FALSE;
}
BOOL CImageTesterDlg::AutoPortChk_ConSide(){
	CString str = "";
	if (Portopen_ConSide() == TRUE){
		if (PortTst_ConSide()){
			StatePrintf("Side BackLight Control PortChk Success..Port COM %d Select", m_ConSerial_Side.Port + 1);
			return TRUE; //우선 기본적으로 지정되어 있는 포트를 우선 체크한다.
		}
		else{
			PortClose_ConSide();
		}
	}

	int BackPort = m_ConSerial_Side.Port;
// 	for (int lop = 0; lop < 14; lop++){
// 		//if(BackPort != lop)
// 		{
// 			m_ConSerial_Side.Port = lop;
// 			if (m_ControlPort_Side.OpenPort(m_ConSerial_Side) == TRUE){
// 				if (PortTst_ConSide()){
// 					//if(BackPort != lop)
// 					{
// 						m_ConSerial_Side.Port = lop; //
// 						SaveSerialSet(&m_ConSerial_Side, "SIDE_CONSERIAL", m_inifilename);
// 						if (m_pStandOptWnd != NULL){
// 							m_pStandOptWnd->PortUpdate();
// 						}
// 					}
// 					StatePrintf("Side BackLight Control PortChk Success..Port COM %d Select", m_ConSerial_Side.Port + 1);
// 					m_OldConSerial_Side = m_ConSerial_Side;
// 					return TRUE;
// 				}
// 				else{
// 					m_ControlPort_Side.ClosePort();
// 				}
// 			}
// 		}
// 	}
	if (m_ControlPort_Side.OpenPort(m_ConSerial_Side) == TRUE){
		if (PortTst_ConSide()){
			StatePrintf("Side BackLight Control PortChk Success..Port COM %d Select", m_ConSerial_Side.Port + 1);
			return TRUE;
		}
	}
	m_ConSerial_Side.Port = BackPort;
	StatePrintf("Side BackLight Control Chk Fails.....Manual Port COM %d Select", m_ConSerial_Side.Port + 1);
	DoEvents(100);
	return FALSE;
}

//-------------------------------------------------------광원 제어통신

bool CImageTesterDlg::Power_Total_Control(){
	bool Result = true;

	for (int a = 0; a < 4; a++){// 전원On
		Result &= Control_Voltage_SLOT(a, Power_Ctl_Data[a]);
	}

	DoEvents(100);

	for (int a = 0; a < 4; a++){// 전류On
		Result &= Control_Current_SLOT(a, Current_Ctl_Data[a]);
	}

	return Result;
}
bool CImageTesterDlg::Power_Total_Control_off(){
	bool Result = true;

	for (int a = 0; a < 4; a++){// 전원Off
		Result &= Control_Current_SLOT(a, 0);
	}

	for (int a = 0; a < 4; a++){// 전류Off
		Result &= Control_Voltage_SLOT(a, 0);
	}

	return Result;
}
bool CImageTesterDlg::Power_Each_Control(int SLOT){
	bool Result = true;

	Result &= Control_Voltage_SLOT(SLOT, Power_Ctl_Data[SLOT]);

	DoEvents(100);

	Result &= Control_Current_SLOT(SLOT, Current_Ctl_Data[SLOT]);

	return Result;
}

bool CImageTesterDlg::Power_Each_Control_Off(int SLOT){
	bool Result = true;

	Result &= Control_Current_SLOT(SLOT,0);

	Result &= Control_Voltage_SLOT(SLOT, 0);

	return Result;
}

bool CImageTesterDlg::Control_Voltage_SLOT(int SLOT,int Volt){

	bool Result = true;
	if (SLOT == 1 || SLOT == 3){//Left,Right
		if (SLOT == 1){//Center
			for (int a = 1; a < 5; a++){
				Result &= Control_Voltage(0, a, Volt);
			}
		}
		if (SLOT == 3){//이물
			Result &= Control_Voltage(0, 5, Volt);
		}
	}
	if (SLOT == 0 || SLOT == 2){//Left,Right
		if (SLOT == 0){//Left
			for (int a = 1; a < 3; a++){
				Result &= Control_Voltage(1, a, Volt);
			}
		}
		if (SLOT == 2){//Right
			for (int a = 3; a < 5; a++){
				Result &= Control_Voltage(1, a, Volt);
			}
		}
	}

	return Result;
}

bool CImageTesterDlg::Control_Voltage(int Board, int ch, int Volt){

	BYTE DATA[30] = { '!', 'U', '0', '0', '0',
						'0', '0', '0', '0', '0',
						'0', '0', '0', '0', '0',
						'0', '0', '0', '0', '0',
						'0', '0', '@' };
	BYTE Buf[4];
	BYTE INF;
	BYTE INF2;

	if (Volt >= 1000)
	{
		Buf[0] = (BYTE)((int)Volt / 1000);
		INF = (BYTE)((int)Volt % 1000);

		Buf[1] = INF / 100;
		INF2 = INF % 100;
	}
	else if ((Volt < 1000) && (Volt>= 100))
	{
		Buf[0] = 0;
		Buf[1] = (BYTE)((int)Volt / 100);
		INF2 = (BYTE)((int)Volt % 100);
	}
	else
	{
		Buf[0] = 0;
		Buf[1] = 0;
		INF2 = (BYTE)((int)Volt);
	}

	Buf[2] = INF2 / 10;
	Buf[3] = INF2 % 10;


	DATA[2] = Hex2Ascii((BYTE)ch);
	DATA[3] = Hex2Ascii(Buf[0]);
	DATA[4] = Hex2Ascii(Buf[1]);
	DATA[5] = Hex2Ascii(Buf[2]);
	DATA[6] = Hex2Ascii(Buf[3]);


	if (Board == 0 ){
		memset(m_SerialInput_ConCenter, 0, sizeof(m_SerialInput_ConCenter));
		Buf_Wait_ConCenter = TRUE;
		SendData8Byte_ConCenter(DATA, 23);
		for (int lop = 0; lop < 50; lop++){//0.1초동안 ACK를 기다린다. 
			if (Buf_Wait_ConCenter == FALSE){
				if ((m_SerialInput_ConCenter[1] == 'U')){
					return true;
				}
			}
			DoEvents(10);
		}
	}
	else{
		memset(m_SerialInput_ConSide, 0, sizeof(m_SerialInput_ConSide));
		Buf_Wait_ConSide = TRUE;
		SendData8Byte_ConSide(DATA, 23);
		for (int lop = 0; lop < 50; lop++){//0.1초동안 ACK를 기다린다. 
			if (Buf_Wait_ConSide == FALSE){
				if ((m_SerialInput_ConSide[1] == 'U')){
					return true;
				}
			}
			DoEvents(10);
		}
	}
	return false;
}

bool CImageTesterDlg::Control_Current_SLOT(int SLOT, int Current){

	bool Result = true;
	if (SLOT == 1 || SLOT == 3){//Left,Right
		if (SLOT == 1){//Center
			for (int a = 1; a < 5; a++){
				Result &= Control_Current(0, a, Current);
			}
		}
		if (SLOT == 3){//이물
			Result &= Control_Current(0, 5, Current);
		}
	}
	if (SLOT == 0 || SLOT == 2){//Left,Right
		if (SLOT == 0){//Left
			for (int a = 1; a < 3; a++){
				Result &= Control_Current(1, a, Current);
			}
		}
		if (SLOT == 2){//Right
			for (int a = 3; a < 5; a++){
				Result &= Control_Current(1, a, Current);
			}
		}
	}

	return Result;
}

bool CImageTesterDlg::Control_Current(int Board, int ch, int Current){
	BYTE DATA[30] = { '!', '5', '0', '0', '0',
		'0', '0', '0', '0', '0',
		'0', '0', '0', '0', '0',
		'0', '0', '0', '0', '0',
		'0', '0', '@' };
	BYTE Buf[4];
	int INF;
	BYTE INF2;

	if (Current >= 1000)
	{
		Buf[0] = (BYTE)((int)Current / 1000);
		INF = /*(BYTE)*/((int)Current % 1000);

		Buf[1] = INF / 100;
		INF2 = INF % 100;
	}
	else if ((Current < 1000) && (Current >= 100))
	{
		Buf[0] = 0;
		Buf[1] = (BYTE)((int)Current / 100);
		INF2 = (BYTE)((int)Current % 100);
	}
	else
	{
		Buf[0] = 0;
		Buf[1] = 0;
		INF2 = (BYTE)((int)Current);
	}

	Buf[2] = INF2 / 10;
	Buf[3] = INF2 % 10;


	DATA[2] = Hex2Ascii((BYTE)ch);
	DATA[3] = Hex2Ascii(Buf[0]);
	DATA[4] = Hex2Ascii(Buf[1]);
	DATA[5] = Hex2Ascii(Buf[2]);
	DATA[6] = Hex2Ascii(Buf[3]);


	if (Board == 0){
		memset(m_SerialInput_ConCenter, 0, sizeof(m_SerialInput_ConCenter));
		Buf_Wait_ConCenter = TRUE;
		SendData8Byte_ConCenter(DATA, 23);
		for (int lop = 0; lop < 50; lop++){//0.1초동안 ACK를 기다린다. 
			if (Buf_Wait_ConCenter == FALSE){
				if ((m_SerialInput_ConCenter[1] == '5')){
					return true;
				}
			}
			DoEvents(10);
		}
	}
	else{
		memset(m_SerialInput_ConSide, 0, sizeof(m_SerialInput_ConSide));
		Buf_Wait_ConSide = TRUE;
		SendData8Byte_ConSide(DATA, 23);
		for (int lop = 0; lop < 50; lop++){//0.1초동안 ACK를 기다린다. 
			if (Buf_Wait_ConSide == FALSE){
				if ((m_SerialInput_ConSide[1] == '5')){
					return true;
				}
			}
			DoEvents(10);
		}
	}
	return false;
}

void CImageTesterDlg::WaitCheckMachineSafety()
{
// 	if (m_Motion.IsOpen())
// 	{
// 		BOOL bAreaSensorOn(FALSE);
// 		BOOL bDoorLockOff(FALSE);
// 
// 		m_bAreaSensor	= m_Motion.GetIOInStatus(DIO_INPORT_SOCKETAREA);
// 		m_bDoorOpen		= m_Motion.GetIOInStatus(DIO_INPORT_SOCKETDOOR);
// 
// 		// AREA SENSOR 감지되었을 경우 팝업창..
// 		if (m_bAreaSensor)
// 		{
// 			while (TRUE)
// 			{
// 				if (m_bAreaSensor != bAreaSensorOn)
// 					StatePrintf("Area Sensor Detect...");
// 
// 				bAreaSensorOn = m_Motion.GetIOInStatus(DIO_INPORT_SOCKETAREA);
// 
// 				if (FALSE == bAreaSensorOn)
// 					break;
// 
// 				DoEvents(5);
// 			}
// 		}
// 		
// 		// DOOR 열렸을 경우 팝업창..
// 		if (m_bDoorOpen)
// 		{
// 			while (TRUE)
// 			{
// 				if (m_bDoorOpen != bDoorLockOff)
// 					StatePrintf("Door Open...!");
// 
// 				bDoorLockOff = m_Motion.GetIOInStatus(DIO_INPORT_SOCKETDOOR);
// 
// 				if (FALSE == bDoorLockOff)
// 					break;
// 
// 				DoEvents(5);
// 			}
// 		}
// 	}
}

//=============================================================================
// Method		: LoadPNGImage
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/30 - 20:28
// Desc.		:
//=============================================================================
void CImageTesterDlg::LoadPNGImage()
{
	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), _T("PNG"), _T("PNG"));
	CString strFileSel;
	strFileSel.Format(_T("*.%s"), _T("PNG"));


	TCHAR szExePath[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, szExePath, MAX_PATH);

	TCHAR drive[_MAX_DRIVE];
	TCHAR dir[_MAX_DIR];
	TCHAR file[_MAX_FNAME];
	TCHAR ext[_MAX_EXT];
	_tsplitpath_s(szExePath, drive, _MAX_DRIVE, dir, _MAX_DIR, file, _MAX_FNAME, ext, _MAX_EXT);

	CString szProgramPath;
	szProgramPath.Format(_T("%s%s"), drive, dir);


	// 파일 불러오기
	CFileDialog fileDlg(TRUE, _T("PNG"), strFileSel, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = szProgramPath;

	if (fileDlg.DoModal() == IDOK)
	{
		strFullPath = fileDlg.GetPathName();
		strFileTitle = fileDlg.GetFileTitle();

		StatePrintf("Load Image : %s", strFullPath.GetBuffer(0));

		m_LVDSVideo.SetUseManualMode(strFullPath.GetBuffer(0));
		strLoadIMGPath_16 = strFullPath;
	}
}


void CImageTesterDlg::TestImageSaveMode(int nMode, LPWORD pGrayBuf, CString szTestName)
{

	switch (nMode)
	{
	case Image_NoSave:
		return;
	default:
		break;
	}

	IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_16U, 1);
	BYTE R, G, B;
	double Sum_Y;

	memcpy(OriginImage->imageData, pGrayBuf, CAM_IMAGE_WIDTH*CAM_IMAGE_HEIGHT * 2);

	//이미지 경로-> 날짜 -> 바코드_test이름.png

	CTime time = CTime::GetTickCount();
	int y = time.GetYear();
	int m = time.GetMonth();
	int d = time.GetDay();

	CString strDay;
	strDay.Format("%04d_%02d_%02d", y, m, d);

	CString ImagePath;

	ImagePath.Format(_T("%s\\%s"), SAVE_IMAGEPATH, strDay);

	MakeDirectory(ImagePath);

	CString strFile;

	strFile = m_strLotId;
	if (m_strLotId == "")
	{
		CTime thetime = CTime::GetCurrentTime();
		CString str;

		int hour = 0, minute = 0, second = 0, hour2 = 0;
		hour = thetime.GetHour(); minute = thetime.GetMinute(); second = thetime.GetSecond();
		str.Format("%02dh_%02dm_%02ds", hour, minute, second);

		strFile = str;		
	}

	CString oripath;
	oripath.Format(_T("%s\\%s_%s.png"), ImagePath, strFile, szTestName);
	imagepic_path_copy.Format(_T("%s\\%s_%s_View.png"), ImagePath, strFile, szTestName);
	winimage_path_copy.Format(_T("%s\\%s_%s_PIC.png"), ImagePath, strFile, szTestName);
	//MODEL_FOLDERPATH_P + "\\" + m_szCode + _T("_") + m_strdegre + "_UI.png";

	CFileFind modelfind;

	if (!modelfind.FindFile(oripath))		//같은모델이없을경우시행한다. 
	{
		//Capture(winimage_path_copy);
		if (nMode == Image_ALLSave)
		{
			b_CaptureMode = TRUE;
			b_CaptureWindowMode = TRUE;
		}		
		cvSaveImage(oripath, OriginImage);


	}
	else{
	}

	cvReleaseImage(&OriginImage);

}

//-New Mes


void CImageTesterDlg::onInitFuncMesData()
{
	for (int t = 0; t < MAX_NewMES_NUM; t++)
	{
		m_stNewMesData[t].Empty();
	}
	m_nNewMesDataNum[NewMES_PREEEPROM] = NewMESDataNum_PREEEPROM;
	m_nNewMesDataNum[NewMES_Current] = NewMESDataNum_Current;
	m_nNewMesDataNum[NewMES_Temperature] = NewMESDataNum_Temperature;
	m_nNewMesDataNum[NewMES_SFR] = NewMESDataNum_SFR;
	m_nNewMesDataNum[NewMES_FOV] = NewMESDataNum_FOV;
	m_nNewMesDataNum[NewMES_Distortion] = NewMESDataNum_Distortion;
	m_nNewMesDataNum[NewMES_3D_Depth] = NewMESDataNum_3D_Depth;
	m_nNewMesDataNum[NewMES_Particle] = NewMESDataNum_Particle;
	m_nNewMesDataNum[NewMES_Brightness] = NewMESDataNum_Brightness;
	m_nNewMesDataNum[NewMES_SNR] = NewMESDataNum_SNR;
	m_nNewMesDataNum[NewMES_DynamicRange] = NewMESDataNum_DynamicRange;
	m_nNewMesDataNum[NewMES_CenterPoint] = NewMESDataNum_CenterPoint;
	m_nNewMesDataNum[NewMES_Rotate] = NewMESDataNum_Rotate;
	m_nNewMesDataNum[NewMES_PixedPattern] = NewMESDataNum_PixedPattern;
	
}


BOOL CImageTesterDlg::PreEEPROM_CHECK(){

	m_pResStandOptWnd->Test2_Val(2, "");
	m_stEEPROMTEST_DATA.reset();
	BYTE arbyBufRead[256] = { 0, };
	CString strData;

	CString szDataX;
	CString szDataY;
	CString szData_Chksum;
	CString szData_Zero;


	if (b_StartCommand == TRUE){
		InsertList_Eeprom();
	}

	BOOL bReulst = TRUE;
	// * Read :  Max 256 Byte
	if (FALSE == m_LVDSVideo.I2C_Read_Repeat(0, (BYTE)m_stEEPROM.wSlave, 1, 0x00, 0xFF, arbyBufRead))
	{
		TRACE(_T("EEPROM Read Failed : First Read Failed"));
		strData.Format(_T("EEPROM Read Failed : First Read Failed"));
		StatePrintf(strData);
		m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 4, _T("Read Fail"));
		m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 5, _T("Read Fail"));
		m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 6, _T("Read Fail"));
		m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 7, _T("Read Fail"));

		m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 8, "FAIL");
		MainWork_List.SetItemText(InsertIndex, WORKLIST_EEPROMCHK, "FAIL");

		return FALSE;
	}


// 	CFile eepromfile;
// 
// //	eepromfile
// 
 	CTime time = CTime::GetTickCount();
 	int nYear = time.GetYear();
 	int nMONTH = time.GetMonth();
 	int nDAY = time.GetDay();
 	int nHour = time.GetHour();
 	int nMin = time.GetMinute();
 	int nSec = time.GetSecond();
 	CString strPath;

	folder_gen("D:\\EEPROM");
 	
 	strPath.Format(_T("D:\\EEPROM\\%04d%02d%02d_%02d%02d%02d.bin"), nYear, nMONTH, nDAY, nHour, nMin, nSec);
 
// 	CFile file;
// 	if (file.Open(strPath, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
// 	{
// 		file.Write(arbyBufRead, 256);
// 		file.Close();
// 
// 	}

	CString str;
// 	if (m_stEEPROM.bZeroCheck == TRUE)
// 	{
// 
// 		if (arbyBufRead[0xFC] == 0 && arbyBufRead[0xFD] == 0)
// 		{
// 			m_stEEPROMTEST_DATA.bResult_CheckZero = TRUE;
// 			str.Format(_T("0xFC - 0x%02x, 0xFD - 0x%02x  _Pass"), arbyBufRead[0xFC], arbyBufRead[0xFD]);
// 			szData_Zero.Format(_T("PASS"));
// 		}
// 		else{
// 			m_stEEPROMTEST_DATA.bResult_CheckZero = FALSE;
// 			str.Format(_T("0xFC - 0x%02x, 0xFD - 0x%02x  _Fail"), arbyBufRead[0xFC], arbyBufRead[0xFD]);
// 			bReulst = FALSE;
// 			szData_Zero.Format(_T("FAIL"));
// 		}
// 		StatePrintf(str);
// 
//	}
// 	else{
// 		szData_Zero.Format(_T("X"));
// 
// 	}

	//광축 값만 읽어서 양불 check
	if (m_stEEPROM.bReadCheck == TRUE)
	{
		float fDataX, fDataY;
		for (int t = 0; t < 8; t++)
		{
			str.Format(_T("0x%02x "), arbyBufRead[m_stEEPROM.wOC_X_Addr+t]);
			strData += str;
		}

		StatePrintf(strData);

		int nCntX = 0;
		int nCntY = 0;
		for (int t = 0; t < 4; t++)
		{
			if (arbyBufRead[m_stEEPROM.wOC_X_Addr+ t] ==0xff)
			{
				nCntX++;
			}
			if (arbyBufRead[m_stEEPROM.wOC_Y_Addr + t] == 0xff)
			{
				nCntY++;
			}
		}
		if (nCntX == 4)
		{
			fDataX = -1.0;
		}
		else{

			memcpy(&fDataX, &arbyBufRead[m_stEEPROM.wOC_X_Addr], 4);
		}
		if (nCntY == 4)
		{
			fDataY = -1.0;
		}
		else{

			//memcpy(&fDataX, &arbyBufRead[m_stEEPROM.wOC_X_Addr], 4);
			memcpy(&fDataY, &arbyBufRead[m_stEEPROM.wOC_Y_Addr], 4);
		}

		
		m_stEEPROMTEST_DATA.fOC_X = fDataX;
		m_stEEPROMTEST_DATA.fOC_Y = fDataY;
		if (m_stEEPROM.nOC_X_Min <= fDataX&&
			m_stEEPROM.nOC_X_Max >= fDataX)
		{
			m_stEEPROMTEST_DATA.bResult_OC_X = TRUE;
		}
		else{
			m_stEEPROMTEST_DATA.bResult_OC_X = FALSE;
			bReulst = FALSE;
		}
		if (m_stEEPROM.nOC_Y_Min <= fDataY&&
			m_stEEPROM.nOC_Y_Max >= fDataY)
		{
			m_stEEPROMTEST_DATA.bResult_OC_Y = TRUE;
		}
		else{
			m_stEEPROMTEST_DATA.bResult_OC_Y = FALSE;
			bReulst = FALSE;
		}
		if (m_stEEPROMTEST_DATA.bResult_OC_X&& m_stEEPROMTEST_DATA.bResult_OC_Y)
		{
			strData.Format(_T("OC X_ %f, OC_Y %f _Pass"), m_stEEPROMTEST_DATA.fOC_X, m_stEEPROMTEST_DATA.fOC_Y);
		}
		else{
			strData.Format(_T("OC X_ %f, OC_Y %f _Fail"), m_stEEPROMTEST_DATA.fOC_X, m_stEEPROMTEST_DATA.fOC_Y);

		}
		StatePrintf(strData);

		szDataX.Format(_T("%f"), m_stEEPROMTEST_DATA.fOC_X);
		szDataY.Format(_T("%f"), m_stEEPROMTEST_DATA.fOC_Y);

	}
	else{
		szDataX.Format(_T("X"));
		szDataY.Format(_T("X"));
	}
	if (m_stEEPROM.bCheckSumCheck == TRUE)
	{
		BYTE Buff[4] = { 0, };
		//-1차 checksum
		UINT16 crc_uint16 = 0;
		UINT8 crc = 0;
		crc_uint16 = crc16(crc_uint16, arbyBufRead, m_stEEPROM.wCheckSum_Addr);
		Buff[0] = crc_uint16 & 0xff;
		Buff[1] = crc_uint16 / 0x100;

		crc_uint16 = 0;
		crc = 0;
		crc_uint16 = crc16(crc_uint16, arbyBufRead, 0xfc);

		Buff[2] = crc_uint16 & 0xff;
		Buff[3] = crc_uint16 / 0x100;

		m_stEEPROMTEST_DATA.szCheckSum.Format(_T("%02x%02x%02x%02x"), Buff[0], Buff[1], Buff[2], Buff[3]);

		if ((arbyBufRead[m_stEEPROM.wCheckSum_Addr] == Buff[0]) && (arbyBufRead[m_stEEPROM.wCheckSum_Addr+1] == Buff[1])
			&& (arbyBufRead[m_stEEPROM.wCheckSum_Addr+2] == Buff[2]) && (arbyBufRead[m_stEEPROM.wCheckSum_Addr + 3] == Buff[3]))
		{
			m_stEEPROMTEST_DATA.bResult_CheckSum = TRUE;
			strData.Format(_T("CheckSum %02x %02x %02x %02x _ Pass"), Buff[0], Buff[1], Buff[2], Buff[3]);
		}
		else{
			m_stEEPROMTEST_DATA.bResult_CheckSum = FALSE;
			bReulst = FALSE;
			strData.Format(_T("CheckSum %02x %02x %02x %02x _ Fail"), Buff[0], Buff[1], Buff[2], Buff[3]);
		}
		StatePrintf(strData);
		szData_Chksum.Format(_T("0x%02x%02x"), Buff[0], Buff[1]);
		szData_Zero.Format(_T("0x%02x%02x"), Buff[2], Buff[3]);


	}else{
		szData_Chksum.Format(_T("X"));
		szData_Zero.Format(_T("X"));

	}


	m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 4, szDataX);
	m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 5, szDataY);
	m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 6, szData_Chksum);
	m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 7, szData_Zero);


	if (bReulst)
	{
		m_pResStandOptWnd->Test2_Val(0, "PASS");//FAIL 빨강색
		m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 8, "PASS");
		MainWork_List.SetItemText(InsertIndex, WORKLIST_EEPROMCHK, "PASS");
	}
	else{
		m_pResStandOptWnd->Test2_Val(1, "FAIL");//FAIL 빨강색
		m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 8, "FAIL");
		MainWork_List.SetItemText(InsertIndex, WORKLIST_EEPROMCHK, "FAIL");

	}

	return bReulst;
}


CString CImageTesterDlg::Mes_Result_EEPROM()
{
	CString sz;
	m_szMesResult = _T("");
	if (m_stEEPROM.bReadCheck &&m_stEEPROM.bCheckSumCheck)
	{
		m_szMesResult.Format(_T("%f:%d,%f:%d,%s:%d"), m_stEEPROMTEST_DATA.fOC_X, m_stEEPROMTEST_DATA.bResult_OC_X,
			m_stEEPROMTEST_DATA.fOC_Y, m_stEEPROMTEST_DATA.bResult_OC_Y,
			m_stEEPROMTEST_DATA.szCheckSum, m_stEEPROMTEST_DATA.bResult_CheckSum);
		m_stNewMesData[NewMES_PREEEPROM] = m_szMesResult;
	


	}
	else if (m_stEEPROM.bReadCheck &&!m_stEEPROM.bCheckSumCheck)
	{
		m_szMesResult.Format(_T("%f:%d,%f:%d,:1"), m_stEEPROMTEST_DATA.fOC_X, m_stEEPROMTEST_DATA.bResult_OC_X,
			m_stEEPROMTEST_DATA.fOC_Y, m_stEEPROMTEST_DATA.bResult_OC_Y);
		m_stNewMesData[NewMES_PREEEPROM] = m_szMesResult;
		
	}
	else if (!m_stEEPROM.bReadCheck &&m_stEEPROM.bCheckSumCheck)
	{
		m_szMesResult.Format(_T(":1,:1,%s:%d"), m_stEEPROMTEST_DATA.szCheckSum, m_stEEPROMTEST_DATA.bResult_CheckSum);
		m_stNewMesData[NewMES_PREEEPROM] = m_szMesResult;

	}

	return m_szMesResult;
}

//  [3/12/2018 CAROLINE]
void CImageTesterDlg::Set_List_Eeprom(CListCtrl *List){


	while (List->DeleteColumn(0));
	if (m_pWorkList != NULL)
		m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0, "NO", LVCFMT_CENTER, 30);
	List->InsertColumn(1, "모델명", LVCFMT_CENTER, 100);
	List->InsertColumn(2, "Start DATE", LVCFMT_CENTER, 80);
	List->InsertColumn(3, "Start TIME", LVCFMT_CENTER, 80);
	List->InsertColumn(4, "CenterX", LVCFMT_CENTER, 80);
	List->InsertColumn(5, "CenterY", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "CheckSum", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "CheckSum2", LVCFMT_CENTER, 80);
	List->InsertColumn(8, "RESULT", LVCFMT_CENTER, 80);
	List->InsertColumn(9, "비고", LVCFMT_CENTER, 80);
	//List->InsertColumn(12,"Serial",LVCFMT_CENTER, 80);
	ListItemNum_Eeprom = 10;
	Copy_List(&m_Work_EepromList, m_pWorkList, ListItemNum_Eeprom);
}


void CImageTesterDlg::InsertList_Eeprom()
{
// 	if (b_InsertEepromList == TRUE)
// 	{
// 		return;
// 	}
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt, strStartMode;


	InsertIndex_Eeprom = m_Work_EepromList.InsertItem(StartCnt_Eeprom, "", 0);
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex_Eeprom + 1);
	m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 0, strCnt);

	m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 1, m_modelName);
	m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 2, strDay);
	m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 3, strTime);
}



void CImageTesterDlg::FAIL_UPLOAD_Eeprom(){
	if (b_StartCommand == 1){
		InsertList_Eeprom();
		m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 4, "X");
		m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 5, "X");
		m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 6, "X");
		m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 7, "X");
		m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 8, "FAIL");
		m_Work_EepromList.SetItemText(InsertIndex_Eeprom, 9, "STOP");
		MainWork_List.SetItemText(InsertIndex, WORKLIST_EEPROMCHK, "STOP");
		StartCnt_Eeprom++;
	}

}

void CImageTesterDlg::EXCEL_SAVE_Eeprom(){
	CString Item[4] = { "CenterX", "CenterY", "CheckSum", "CheckSum2" };
	CString Data = "";
	CString str = "";
	str = "***********************EEProm Check 검사***********************\n";
	Data += str;
	str.Empty();
	str.Format("OC Min X , %d ,OC Max X,  %d, \n", m_stEEPROM.nOC_X_Min, m_stEEPROM.nOC_X_Max);
	Data += str;
	str.Empty();
	str.Format("OC Min Y , %d ,OC Max Y,  %d, \n", m_stEEPROM.nOC_Y_Min, m_stEEPROM.nOC_Y_Max);
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str = "*************************************************\n";
	Data += str;
	SAVE_EXCEL("EEPROM_CHECK", Item, 4, &m_Work_EepromList, Data);
}

bool CImageTesterDlg::EXCEL_UPLOAD_Eeprom(){
	m_Work_EepromList.DeleteAllItems();
	if (EXCEL_UPLOAD("EEPROM_CHECK", &m_Work_EepromList, 1, &StartCnt_Eeprom) == TRUE){
		return TRUE;
	}
	else{
		StartCnt_Eeprom = 0;
		return FALSE;
	}
}


void CImageTesterDlg::LOT_Set_List_Eeprom(CListCtrl *List){

	while (List->DeleteColumn(0));
	m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0, "NO", LVCFMT_CENTER, 30);
	List->InsertColumn(1, "모델명", LVCFMT_CENTER, 100);
	List->InsertColumn(2, "LOT 명", LVCFMT_CENTER, 80);
	List->InsertColumn(3, "Start DATE", LVCFMT_CENTER, 80);
	List->InsertColumn(4, "Start TIME", LVCFMT_CENTER, 80);
	List->InsertColumn(5, "CenterX", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "CenterY", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "CheckSum", LVCFMT_CENTER, 80);
	List->InsertColumn(8, "CheckSum2", LVCFMT_CENTER, 80);
	List->InsertColumn(9, "RESULT", LVCFMT_CENTER, 80);
	List->InsertColumn(10, "비고", LVCFMT_CENTER, 80);

	Lot_InsertNum_Eeprom = 11;
	Copy_List(&m_Lot_EepromList, m_pWorkList, Lot_InsertNum_Eeprom);

}

void CImageTesterDlg::LOT_InsertDataList_Eeprom(){
	CString strCnt = "";

	int Index = m_Lot_EepromList.InsertItem(Lot_StartCnt_Eeprom, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index + 1);
	m_Lot_EepromList.SetItemText(Index, 0, strCnt);

	int CopyIndex = m_Work_EepromList.GetItemCount() - 1;

	int count = 0;
	for (int t = 0; t < Lot_InsertNum_Eeprom; t++){
		if ((t != 2) && (t != 0)){
			m_Lot_EepromList.SetItemText(Index, t, m_Work_EepromList.GetItemText(CopyIndex, count));
			count++;

		}
		else if (t == 0){
			count++;
		}
		else{
			m_Lot_EepromList.SetItemText(Index, t, LOT_NUMBER);
		}
	}

	Lot_StartCnt_Eeprom++;

}
void CImageTesterDlg::LOT_EXCEL_SAVE_Eeprom(){
	CString Item[4] = { "CenterX", "CenterY", "CheckSum", "CheckSum2" };
	CString Data = "";
	CString str = "";
	str = "***********************EEProm Check 검사***********************\n";
	Data += str;
	str.Empty();
	str.Format("OC Min X , %d ,OC Max X,  %d, \n", m_stEEPROM.nOC_X_Min, m_stEEPROM.nOC_X_Max);
	Data += str;
	str.Empty();
	str.Format("OC Min Y , %d ,OC Max Y,  %d, \n", m_stEEPROM.nOC_Y_Min, m_stEEPROM.nOC_Y_Max);
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str = "*************************************************\n";
	Data += str;
	LOT_SAVE_EXCEL("EEPROM_CHECK", Item, 4, &m_Lot_EepromList, Data);
}

bool CImageTesterDlg::LOT_EXCEL_UPLOAD_Eeprom(){
	m_Lot_EepromList.DeleteAllItems();
	if (LOT_EXCEL_UPLOAD("EEPROM_CHECK", &m_Lot_EepromList, 1, &Lot_StartCnt_Eeprom) == TRUE){
		return TRUE;
	}
	else{
		Lot_StartCnt_Eeprom = 0;
		return FALSE;
	}
	return TRUE;
}


void CImageTesterDlg::OnBnClickedInit()
{
	if (IDYES == AfxMessageBox(_T("Are you sure you want to Initialize?"), MB_YESNO))
	{
		if (TRUE == InitMotor())
		{
			ALARM_OPERATING(2);
			LogWriteToFile(_T("ALARM_OPERATING(0);"));

			m_bDoorSensor		= FALSE;
			m_bSafetySensor		= FALSE;
		}
	}
}
