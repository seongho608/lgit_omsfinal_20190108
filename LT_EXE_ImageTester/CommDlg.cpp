// CommDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "CommDlg.h"


// CCommDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCommDlg, CDialog)

CCommDlg::CCommDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCommDlg::IDD, pParent)
	, m_CheckCommAscii(FALSE)
{

}

CCommDlg::~CCommDlg()
{
}

void CCommDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_UART_RICHEDIT, m_CommuEdit);
	DDX_Check(pDX, IDC_COMM_ASCII_CHECK2, m_CheckCommAscii);
}


BEGIN_MESSAGE_MAP(CCommDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_Clear, &CCommDlg::OnBnClickedButtonClear)
//	ON_BN_CLICKED(IDCANCEL, &CCommDlg::OnBnClickedCancel)
ON_BN_CLICKED(IDC_COMM_ASCII_CHECK2, &CCommDlg::OnBnClickedCommAsciiCheck2)
END_MESSAGE_MAP()

void CCommDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}
// CCommDlg 메시지 처리기입니다.

void CCommDlg::OnBnClickedButtonClear()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_CommuEdit.SetWindowText("");
}


void CCommDlg::DisplayDebugData(BYTE* m_data, int len, char mode,int NUM)
{
	UpdateData();
//	if(m_Pause) return;
	
	int spos = m_CommuEdit.GetScrollPos(SB_VERT);
	m_CommuEdit.SetSel(-1,-1);
	m_CommuEdit.SetRedraw(FALSE);
	CHARFORMAT cf;
	cf.cbSize = sizeof(cf);
	cf.dwMask = CFM_COLOR;
	
	m_CommuEdit.GetSelectionCharFormat(cf);
	CString add_str;
	if(mode == TX_MODE)
	{
		cf.crTextColor = TX_COLOR;
	}
	else
	{
		cf.crTextColor = RX_COLOR;
	}
	add_str.Empty();

	
	if(NUM ==0){
		add_str += "[CON_CEN] ";
	}
	else if(NUM ==1){
		add_str += "[CON_SIDE] ";
	}else if(NUM == 2){
		add_str += "[CAM] ";
	}else if(NUM ==3){
		add_str += "[4] ";
	}else{
		add_str += "[CON] ";
	}

	if(mode == TX_MODE)
	{
		add_str += "[TX] ";
	}
	else
	{
		add_str += "[RX] ";
	}

	
	unsigned long bufLen = m_CommuEdit.GetTextLength();

	if (bufLen > MAX_LOG_LEN)//문자열 정보를 50Kb 수준으로 유지한다.
	{ 
		m_CommuEdit.SetWindowText("");
	}


	m_CommuEdit.SetSel(-1,-1);
	
	cf.dwEffects &= ~CFE_AUTOCOLOR;
	m_CommuEdit.SetSelectionCharFormat(cf);
	// Add the event to the edit control
	CString out_str;

	CString temp;
	BOOL sec_change = TRUE;
	int section_idx = 0;
	int num = 0;

	if(m_CheckCommAscii)//m_CheckCommAscii)
	{
		//temp.Format("%s", m_data);

		for (int index=0; index < len ; index++)
		{
			//temp.Format("%s", (BYTE)m_data[index]);
			out_str += (BYTE)m_data[index];
		}
	}
	else
	{
		for (int index=0; index < len ; index++)
		{
			temp.Format("%02X ", (BYTE)m_data[index]);
			out_str += temp;
		}
		
	}

	out_str += "\r\n";

//	if(b_FILEWR == FALSE){
		m_CommuEdit.ReplaceSel(add_str+out_str);
//	}
	int nMax = m_CommuEdit.GetScrollLimit(SB_VERT);
	m_CommuEdit.SendMessage(WM_VSCROLL, MAKEWPARAM(SB_THUMBPOSITION, nMax), NULL);
	m_CommuEdit.SendMessage(WM_VSCROLL, MAKEWPARAM(SB_ENDSCROLL, nMax), NULL);
	
	m_CommuEdit.SetRedraw(TRUE);
	m_CommuEdit.Invalidate();
}

void CCommDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	
//		delete m_pCommWnd;
//		m_pCommWnd = NULL;
	CDialog::OnCancel();
//	((CImageTesterDlg *)m_pMomWnd)->Delete_CommDlg();
}

void CCommDlg::OnBnClickedCommAsciiCheck2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData();
}

BOOL CCommDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_CheckCommAscii = TRUE;
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
