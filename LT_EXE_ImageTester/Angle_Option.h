#pragma once
#include "afxcmn.h"
#include "ETCUSER.H"
#include "afxwin.h"
// CAngle_Option 대화 상자입니다.

class CAngle_Option : public CDialog
{
	DECLARE_DYNAMIC(CAngle_Option)

public:
	CAngle_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAngle_Option();
	enum { IDD = IDD_OPTION_ANGLE };
	
	double m_dOffset;
	double m_dPixelSize;
	double m_dFocalLength;
	double m_dDistortion;
	double m_dMasterH;
	double m_dOffsetH;
	double m_dMasterV;
	double m_dOffsetV;

	double ResultDegree_H;
	double ResultDegree_V;

	CRectData A_RECT[5];
	CRectData A_Original[5];
	void Load_Original_pra();
	CRectData C_CHECKING[5];
	tResultVal Run(void);
	void Pic(CDC *cdc);
	void InitPrm();
	void FAIL_UPLOAD();
	void	FINAL_UPLOAD(LPCSTR lpcszString);

	CvPoint GetCenterPoint(LPBYTE IN_RGB);
	CRectData SC_RECT[5];	//Side Circle
	CRectData Standard_SC_RECT[5];
	
	void GetSideCircleCoordinate(LPBYTE m_RGBIn);
	double GetDistance(int x1, int y1, int x2, int y2);

// 대화 상자 데이터입니다.
	void	SETLIST();

	void	Setup(CWnd* IN_pMomWnd);
	void    Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
	void	StatePrintf(LPCSTR lpcszString, ...);
	void	UploadList();
	void	Save_parameter();
	void	Save(int NUM);
	CString A_filename;
	int PosX,PosY;
	int InIndex;
	int iSavedItem, iSavedSubitem;
	int iSavedSubitem2;
	int state;
	int EnterState;
	int NewItem,NewSubitem;
	CRect rect;
	void	AngleSum();
	double	Degree_Sum(int NUM);
	void Change_DATA();

	void Automation_DATA_INPUT();
	double disL,disR,disT,disB;
	double dgrL, dgrR, dgrT, dgrB;
	int TestMod;
	bool AutomationMod;
	BYTE	m_InR;
	BYTE	m_InG;
	BYTE	m_InB;
	bool MasterMod;
	int	m_Comp_L_MAX,m_Comp_R_MAX,m_Comp_T_MAX,m_Comp_B_MAX;
	int	m_Comp_L_MIN,m_Comp_R_MIN,m_Comp_T_MIN,m_Comp_B_MIN;
	void Load_parameter();
	bool AngleGen(LPBYTE IN_RGB,int NUM);
	bool AnglePic(CDC *cdc,int NUM);
	CBitmap bmp;
	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void InsertList();
	int InsertIndex;
	int ListItemNum;
	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();
	int StartCnt;
	//--------
	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();
	int		Lot_StartCnt;
	int		Lot_InsertIndex;
	int		Lot_InsertNum;
	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

	void List_COLOR_Change();
	bool ChangeCheck;
	int ChangeItem[5];
	int changecount;

	bool Change_DATA_CHECK(bool FLAG);

	
	void	EditWheelIntSet(int nID,int Val);
	void	EditMinMaxIntSet(int nID,int* Val,int Min,int Max);
	bool WheelCheck;

	// MES 정보저장
	CString Str_Mes[2];
	int		m_nResult[2];

	CString m_szMesResult;
	CString Mes_Result();
	void	CopyMesData();
	BOOL	b_StopFail;

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	CString		str_model;
//	int m_Number;
	tINFO	m_INFO;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListCtrl A_DATALIST;
	int A_Total_PosX;
	int A_Total_PosY;
	int A_Dis_Width;
	int A_Dis_Height;
	int A_Total_Width;
	int A_Total_Height;
	double A_Thresold;
	afx_msg void OnBnClickedButtonsetanglezone();
	afx_msg void OnBnClickedButtonARectSave();
	afx_msg void OnNMClickListAngle(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
//	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonMasterA();
	CListCtrl m_AngleList;
	afx_msg void OnBnClickedCheckVer();
	afx_msg void OnBnClickedCheckHor();
	afx_msg void OnBnClickedCheckAuto();
	afx_msg void OnNMCustomdrawListAngle(NMHDR *pNMHDR, LRESULT *pResult);
//	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnNMDblclkListAngle(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusEditAMod();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedButtonALoad();
	BOOL TestModALL;
	BOOL TestModVer;
	BOOL TestModHor;
	afx_msg void OnBnClickedCheck1();
	//afx_msg void OnBnClickedCheckAngleKeeprun();
	BOOL AngleKeepRun;
	CButton btn_AngleKeepRun;
	afx_msg void OnDestroy();
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnEnChangeAnglePosx();
	afx_msg void OnEnChangeAnglePosy();
	afx_msg void OnEnChangeAngleDisW();
	afx_msg void OnEnChangeAngleDisH();
	afx_msg void OnEnChangeAngleWidth();
	afx_msg void OnEnChangeAngleHeight();
	afx_msg void OnEnChangeAngleThresold();
	afx_msg void OnEnChangeEditAMod();
	afx_msg void OnLvnItemchangedListAngle(NMHDR *pNMHDR, LRESULT *pResult);
	CListCtrl m_Lot_AngleList;
	afx_msg void OnBnClickedBtnEtcSave();
	afx_msg void OnBnClickedButtonSaveH();
	afx_msg void OnBnClickedButtonSaveV();
};

void CModel_Create(CAngle_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO);
void CModel_Delete(CAngle_Option **pWnd);
