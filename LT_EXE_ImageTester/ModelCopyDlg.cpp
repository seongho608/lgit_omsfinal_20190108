// ModelCopyDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "ModelCopyDlg.h"


// CModelCopyDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CModelCopyDlg, CDialog)

CModelCopyDlg::CModelCopyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModelCopyDlg::IDD, pParent)
	, m_str_cpyname(_T(""))
{

}

CModelCopyDlg::~CModelCopyDlg()
{
}

void CModelCopyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_Model, m_str_cpyname);
}


BEGIN_MESSAGE_MAP(CModelCopyDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CModelCopyDlg::OnBnClickedOk)
	ON_EN_SETFOCUS(IDC_EDIT_Model2, &CModelCopyDlg::OnEnSetfocusEditModel2)
END_MESSAGE_MAP()


// CModelCopyDlg 메시지 처리기입니다.
void CModelCopyDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CModelCopyDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString ModelPath = _T("");
	CString FoldPath = _T("");
	CString DelfilePath = _T("");
	CFileFind modelfind;
	UpdateData(TRUE);

	CString Overlay_path	= ((CImageTesterDlg *)m_pMomWnd)->m_model_path + _T("\\") + str_copyname;
	CString modelfile_path	= ((CImageTesterDlg *)m_pMomWnd)->m_model_path + _T("\\") + str_copyname + _T("\\") + str_copyname+ _T(".luri");

	if (m_str_cpyname != _T(""))
	{
		ModelPath	= ((CImageTesterDlg *)m_pMomWnd)->m_model_path + _T("\\") + m_str_cpyname + _T("\\") + m_str_cpyname + _T(".luri");
		FoldPath	= ((CImageTesterDlg *)m_pMomWnd)->m_model_path + _T("\\") + m_str_cpyname;

		if(!modelfind.FindFile(ModelPath))		//같은 모델이 없을 경우 시행한다. 
		{ 
			DelfilePath = FoldPath +_T("\\") + str_copyname  + _T(".luri"); 
			CopyFolder(Overlay_path,FoldPath);
			DeleteDir(DelfilePath);
			CopyFile(modelfile_path,ModelPath,FALSE);
			Wait2(100);
			((CImageTesterDlg *)m_pMomWnd)->modelfile_Name = ModelPath;
			((CImageTesterDlg *)m_pMomWnd)->Overlay_path = FoldPath;
			((CImageTesterDlg *)m_pMomWnd)->m_newmodelname = m_str_cpyname;
			if(((CImageTesterDlg *)m_pMomWnd)->m_newmodelname != ((CImageTesterDlg *)m_pMomWnd)->m_oldmodelname)
					((CImageTesterDlg *)m_pMomWnd)->m_oldmodelname = m_str_cpyname;
			
			WritePrivateProfileString(_T("INITPLACE"),_T("MODEL"),m_str_cpyname,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);//변경된 폴더를 ini에 저장한다. 
		}else		/////////////////동일한 모델이 있을 경우
		{
			AfxMessageBox(_T("동일한 카메라 모델이 있습니다. 다시 입력해주세요."));
			m_str_cpyname = _T("");
			UpdateData(FALSE);
			//((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_HIDE);
			//((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_SHOW);
			return;
		}
		((CImageTesterDlg *)m_pMomWnd)->MainModelSel();
		((CImageTesterDlg *)m_pMomWnd)->ModelSet(); //luri 파일을 읽어 각 모델이 무엇인지 파악한다.
		((CImageTesterDlg *)m_pMomWnd)->ModelGen(); //파악된 모델을 외부에 표시한다.

		m_str_cpyname = _T("");
		UpdateData(FALSE);

		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_HIDE);
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptDlgWnd->ShowWindow(SW_SHOW);
	}

	OnOK();
}


void CModelCopyDlg::OnBnClickedCancel()
{
	OnCancel();
}

BOOL CModelCopyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	((CEdit *)GetDlgItem(IDC_EDIT_Model2))->SetWindowText(str_copyname);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CModelCopyDlg::OnEnSetfocusEditModel2()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
