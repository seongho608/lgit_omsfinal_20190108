

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "_3D_Depth_Option.h"

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
extern WORD	m_GRAYScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];
// C_3D_Depth_Option 대화 상자입니다.
extern BYTE	m_OverlayArea[CAM_IMAGE_WIDTH][CAM_IMAGE_HEIGHT];
IMPLEMENT_DYNAMIC(C_3D_Depth_Option, CDialog)

C_3D_Depth_Option::C_3D_Depth_Option(CWnd* pParent /*=NULL*/)
	: CDialog(C_3D_Depth_Option::IDD, pParent)
{
	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;
//	b_ChangeMod=0;
	StartCnt=0;
	m_dAveData = 0;
	m_dStdevData = 0;

	m_bAveData = 0;
	m_bStdevData = 0;
	b_StopFail = FALSE;
}

C_3D_Depth_Option::~C_3D_Depth_Option()
{
}

void C_3D_Depth_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_WORK, m_WorkList);
	DDX_Control(pDX, IDC_LIST_LOT_WORKLIST, m_Lot_WorkList);
}


BEGIN_MESSAGE_MAP(C_3D_Depth_Option, CDialog)
	ON_WM_SHOWWINDOW()	
	ON_WM_MOUSEWHEEL()	
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_test, &C_3D_Depth_Option::OnBnClickedButtontest)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &C_3D_Depth_Option::OnBnClickedButtonSave)
END_MESSAGE_MAP()

// C_3D_Depth_Option 메시지 처리기입니다.

void C_3D_Depth_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void C_3D_Depth_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO    = INFO; 
	str_model.Empty();
	str_model.Format(INFO.NAME);;
}

BOOL C_3D_Depth_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;

	Depth_filename =((CImageTesterDlg  *)m_pMomWnd)->modelfile_Name;
	UpdateData(TRUE);
	Load_parameter();
	UpdateData(FALSE);
	//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->EnableWindow(0);
	Set_List(&m_WorkList);
	LOT_Set_List(&m_Lot_WorkList);



	InitEVMS();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void C_3D_Depth_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
// 		//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->EnableWindow(0);
// 		((CButton *)GetDlgItem(IDC_BUTTON_PN_LOAD))->EnableWindow(0);
// 		((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVEZONE))->EnableWindow(0);
// 		((CButton *)GetDlgItem(IDC_BUTTON_setPNZone))->EnableWindow(0);
// 		((CButton *)GetDlgItem(IDC_BUTTON_CAM))->EnableWindow(0);
// 
// 		
// 		((CEdit *)GetDlgItem(IDC_Thresold_Y))->EnableWindow(0);
// 		((CEdit *)GetDlgItem(IDC_Thresold_V))->EnableWindow(0);
// 		((CEdit *)GetDlgItem(IDC_PN_PosX))->EnableWindow(0);
// 		((CEdit *)GetDlgItem(IDC_PN_PosY))->EnableWindow(0);
// 		((CEdit *)GetDlgItem(IDC_PN_Dis_W))->EnableWindow(0);
// 		((CEdit *)GetDlgItem(IDC_PN_Dis_H))->EnableWindow(0);
// 		((CEdit *)GetDlgItem(IDC_PN_Width))->EnableWindow(0);
// 		((CEdit *)GetDlgItem(IDC_PN_Height))->EnableWindow(0);
// 
// 		((CEdit *)GetDlgItem(IDC_EDIT_CAM_Bright))->EnableWindow(0);
// 		((CEdit *)GetDlgItem(IDC_EDIT_CAM_Contrast))->EnableWindow(0);
	}
}


bool C_3D_Depth_Option::DepthPic(CDC *cdc,int NUM){

	//if(EIJARect.Chkdata() == FALSE){return 0;}//재대로 된 데이터가 아니면 바로 빠져나간다.

	CPen	my_Pan,*old_pan;
	CString TEXTDATA ="";
	::SetBkMode(cdc->m_hDC,TRANSPARENT);

	if (m_bAveData)
	{
		cdc->SetTextColor(BLUE_COLOR);
		cdc->SetTextAlign(TA_CENTER | TA_BASELINE);
	}
	else{
		cdc->SetTextColor(RED_COLOR);
		cdc->SetTextAlign(TA_CENTER | TA_BASELINE);
	}

	TEXTDATA.Format("평균 : %6.2f", m_dAveData);
	cdc->TextOut(CAM_IMAGE_WIDTH/2, CAM_IMAGE_HEIGHT-100, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());

	if (m_bStdevData)
	{
		cdc->SetTextColor(BLUE_COLOR);
		cdc->SetTextAlign(TA_CENTER | TA_BASELINE);
	}
	else{
		cdc->SetTextColor(RED_COLOR);
		cdc->SetTextAlign(TA_CENTER | TA_BASELINE);
	}

	TEXTDATA.Format("표준편차 : %6.2f", m_dStdevData);
	cdc->TextOut(CAM_IMAGE_WIDTH / 2, CAM_IMAGE_HEIGHT - 70, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());


//	if (PN_RECT[NUM].ENABLE == TRUE)
	//{


// 		if(PN_RECT[NUM].m_Success == TRUE){
// 			my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
// 			cdc->SetTextColor(BLUE_COLOR);
// 		}else{
// 			my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
// 			cdc->SetTextColor(RED_COLOR);
// 		}
// 
// 
// 
// 		old_pan = cdc->SelectObject(&my_Pan);
// 
// 		cdc->SetTextAlign(TA_CENTER|TA_BASELINE);
// 
// 		cdc->SelectObject(old_pan);
	// 



	// 		cdc->MoveTo(PN_RECT[NUM].m_Left,PN_RECT[NUM].m_Top);
	// 		cdc->LineTo(PN_RECT[NUM].m_Right,PN_RECT[NUM].m_Top);
	// 		cdc->LineTo(PN_RECT[NUM].m_Right,PN_RECT[NUM].m_Bottom);
	// 		cdc->LineTo(PN_RECT[NUM].m_Left,PN_RECT[NUM].m_Bottom);
	// 		cdc->LineTo(PN_RECT[NUM].m_Left,PN_RECT[NUM].m_Top);
	// 
	// 		TEXTDATA.Format("STD: %6.2f",PN_RECT[NUM].m_result);	
	// 		//	}
	// 		cdc->TextOut(PN_RECT[NUM].m_PosX,PN_RECT[NUM].m_Top - 3,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());



// 		old_pan->DeleteObject();
// 		my_Pan.DeleteObject();


		my_Pan.CreatePen(PS_SOLID, 2, WHITE_COLOR);
			cdc->SetTextColor(RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		int posX = 0;
		

		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();


		my_Pan.CreatePen(PS_SOLID, 2, YELLOW_COLOR);
		cdc->SetTextColor(YELLOW_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);

		cdc->SetTextAlign(TA_CENTER|TA_BASELINE);



	

		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();

		

	return 1;
}

void C_3D_Depth_Option::Pic(CDC *cdc)
{
	//for(int lop=0;lop<9;lop++){
		DepthPic(cdc,0);
	//}
	//PatternNoisePic(cdc,0);
}

void C_3D_Depth_Option::InitPrm()
{

	Load_parameter();
	UpdateData(FALSE);

	//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE))->EnableWindow(0);
}


tResultVal C_3D_Depth_Option::Run()
{	
	CString str="";
	b_StopFail = FALSE;

	m_bAveData = FALSE;
	m_bStdevData = FALSE;
	m_dAveData = 0;
	m_dStdevData = 0;

	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

		InsertList();
	}
		((CImageTesterDlg  *)m_pMomWnd)->m_pRes3D_DepthOptWnd->initstat();
		int camcnt = 0;
		tResultVal retval = {0,};
	
		//StatePrintf("저조도 측정 모드를 시작합니다");
		

		DoEvents(500);


		/////////////////////////////////////

		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = 1;
		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;

		for (int i = 0; i < 10; i++){
			if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0 && ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0){
				break;
			}
			else{
				DoEvents(50);
			}
		}
				
		bool bResult= SNRGen_16bit(m_GRAYScanbuf);





		if (bResult == TRUE){
		 	m_Success = TRUE;
		 
		 	if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
		 		m_Lot_WorkList.SetItemText(Lot_InsertIndex, Lot_InsertNum - 2, "PASS");////////
		 	}
		 	else{
		 		m_WorkList.SetItemText(InsertIndex, ListItemNum - 2, "PASS");////////
		 	}
		 
		 
		 
		}
		else{
		 	m_Success = FALSE;
		 	if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
				m_Lot_WorkList.SetItemText(Lot_InsertIndex, Lot_InsertNum - 2, "FAIL");////////
		 	}
		 	else{
				m_WorkList.SetItemText(InsertIndex, ListItemNum - 2, "FAIL");////////
		 	}
		 
		}

		CString stateDATA ="3D Depth 검사_ ";

		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
			int LISTNUM = 5;
			str.Empty();
			str.Format("%6.2f", m_dAveData);
			stateDATA += "평균_" + str + ",";

			m_Lot_WorkList.SetItemText(Lot_InsertIndex, LISTNUM++, str);
			str.Format("%6.2f", m_dStdevData);
			stateDATA += "표준편차_" + str;

			m_Lot_WorkList.SetItemText(Lot_InsertIndex, LISTNUM++, str);
			
		}
		else{

			int LISTNUM = 4;
			str.Empty();
			str.Format("%6.2f", m_dAveData);

			stateDATA += "평균_"+str + ",";

			m_WorkList.SetItemText(InsertIndex, LISTNUM++, str);
			str.Format("%6.2f", m_dStdevData);
			stateDATA += "표준편차_" + str;
			m_WorkList.SetItemText(InsertIndex, LISTNUM++, str);
		
		

		}



		//---------------------------

		retval.m_ID = 0x16;
		retval.ValString.Empty();
		if(m_Success == TRUE){
			retval.m_Success = TRUE;
			Str_Mes[0] = "PASS";
			Str_Mes[1] = "1";
			if (((CImageTesterDlg *)m_pMomWnd)->LotMod != 1){

				((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_3D_DEPTH, "PASS");
			}
			stateDATA+= "_ PASS";
			retval.ValString.Format("OK");

		}else{
			retval.m_Success = FALSE;
			Str_Mes[0] = "FAIL";
			Str_Mes[1] = "0";
			if (((CImageTesterDlg *)m_pMomWnd)->LotMod != 1){

				((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_3D_DEPTH, "FAIL");
			}
			stateDATA+= "_ FAIL";
			retval.ValString.Format("FAIL");


		}
		//StatePrintf("저조도 측정 모드가 종료되었습니다");
		((CImageTesterDlg  *)m_pMomWnd)->StatePrintf(stateDATA);
// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 			if (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == TRUE){
// 				Lot_StartCnt++;
// 			}
// 		}
// 		else{
// 			StartCnt++;
// 		}
		if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			StartCnt++;
		}
		return retval;
	
}	

void C_3D_Depth_Option::FAIL_UPLOAD(){

 	if (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1){
// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 			LOT_InsertDataList();
// // 			for (int t = 0; t < 9; t++)
// // 			{
// 				m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, 5, "X");//result
// 
// /*			}*/
// 			m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, Lot_InsertNum - 2, "FAIL");//result
// 			m_Lot_PatternNoiseList.SetItemText(Lot_InsertIndex, Lot_InsertNum - 1, "STOP");//비고
// 			b_StopFail = TRUE;
// 			Lot_StartCnt++;
// 		}
// 		else{
			InsertList();
 			for (int t = 0; t < 2; t++)
 			{
				m_WorkList.SetItemText(InsertIndex, 4+t, "X");//result

			}
			m_WorkList.SetItemText(InsertIndex, ListItemNum - 2, "FAIL");//result
			m_WorkList.SetItemText(InsertIndex, ListItemNum - 1, "STOP");//비고
			Str_Mes[0] = "FAIL";
			Str_Mes[1] = "0";
//			((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_LOW_LIGHT, "STOP");
			StartCnt++;
			b_StopFail = TRUE;
// 		}
 	}
}
void CModel_Create(C_3D_Depth_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO){
	CRect OptionRect;
	if(pTabWnd != NULL){
		((C_3D_Depth_Option *)*pWnd) = new C_3D_Depth_Option(pMomWnd);
		((C_3D_Depth_Option *)*pWnd)->Setup(pMomWnd,pEdit,INFO);
		((C_3D_Depth_Option *)*pWnd)->Create(((C_3D_Depth_Option *)*pWnd)->IDD,pTabWnd);//&m_CtrTab);
		((C_3D_Depth_Option *)*pWnd)->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		((C_3D_Depth_Option *)*pWnd)->MoveWindow(20,40,OptionRect.Width(),OptionRect.Height());
	}
}
void CModel_Delete(C_3D_Depth_Option **pWnd){
	if(((C_3D_Depth_Option *)*pWnd) != NULL){
		delete ((C_3D_Depth_Option *)*pWnd);
		((C_3D_Depth_Option *)*pWnd) = NULL;	
	}
}

void C_3D_Depth_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

void C_3D_Depth_Option::Load_parameter(){

	CFileFind filefind;
	CString str = "";
	CString strTitle = "";
	strTitle.Empty();
	strTitle = "3D_INIT";

	m_dAveMin = GetPrivateProfileDouble(str_model, strTitle + "Ave_Min", 2, Depth_filename);
	m_dAveMax = GetPrivateProfileDouble(str_model, strTitle + "Ave_Max", 100, Depth_filename);

	m_dStdevMin = GetPrivateProfileDouble(str_model, strTitle + "Stdev_Min", 2, Depth_filename);
	m_dStdevMax = GetPrivateProfileDouble(str_model, strTitle + "Stdev_Max", 100, Depth_filename);


	CString strData;
	strData.Format(_T("%6.2f"), m_dAveMin);
	((CEdit *)GetDlgItem(IDC_EDIT_BR_MIN))->SetWindowText(strData);

	strData.Format(_T("%6.2f"), m_dAveMax);
	((CEdit *)GetDlgItem(IDC_EDIT_BR_MAX))->SetWindowText(strData);

	strData.Format(_T("%6.2f"), m_dStdevMin);
	((CEdit *)GetDlgItem(IDC_EDIT_BRSTDEV_MIN))->SetWindowText(strData);

	strData.Format(_T("%6.2f"), m_dStdevMax);
	((CEdit *)GetDlgItem(IDC_EDIT_BRSTDEV_MAX))->SetWindowText(strData);

	Save_parameter();
}

void C_3D_Depth_Option::Save_parameter(){

		CString str="";
		CString strTitle="";
		str.Empty();
		str.Format("%d", m_INFO.ID);
		WritePrivateProfileString(str_model, "ID", str, Depth_filename);
		str.Empty();
		str.Format("%s", m_INFO.MODE);
		WritePrivateProfileString(str_model, "MODE", str, Depth_filename);
		str.Empty();
		str.Format("%s", m_INFO.NAME);
		WritePrivateProfileString(str_model, "NAME", str, Depth_filename);

		strTitle.Empty();
		strTitle = "3D_INIT";

		str.Empty();
		str.Format("%6.2f", m_dAveMin);
		WritePrivateProfileString(str_model, strTitle + "Ave_Min", str, Depth_filename);
		str.Empty();
		str.Format("%6.2f", m_dAveMax);
		WritePrivateProfileString(str_model, strTitle + "Ave_Max", str, Depth_filename);

		str.Empty();
		str.Format("%6.2f", m_dStdevMin);
		WritePrivateProfileString(str_model, strTitle + "Stdev_Min", str, Depth_filename);
		str.Empty();
		str.Format("%6.2f", m_dStdevMax);
		WritePrivateProfileString(str_model, strTitle + "Stdev_Max", str, Depth_filename);

}

void C_3D_Depth_Option::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",Depth_filename);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

//---------------------------------------------------------------------WORKLIST

void C_3D_Depth_Option::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	
	InsertIndex = m_WorkList.InsertItem(StartCnt,"",0);
	
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_WorkList.SetItemText(InsertIndex,0,strCnt);
	
	m_WorkList.SetItemText(InsertIndex,1,((CImageTesterDlg  *)m_pMomWnd)->m_modelName);
	m_WorkList.SetItemText(InsertIndex,2,((CImageTesterDlg  *)m_pMomWnd)->strDay+"");
	m_WorkList.SetItemText(InsertIndex,3,((CImageTesterDlg  *)m_pMomWnd)->strTime+"");



}
void C_3D_Depth_Option::Set_List(CListCtrl *List){/////

	while(List->DeleteColumn(0));
	((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	//List->InsertColumn(2, "LOT CARD", LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);

	int LISTNUM = 4;

	List->InsertColumn(LISTNUM++, "밝기 평균", LVCFMT_CENTER, 80);
	List->InsertColumn(LISTNUM++, "표준편차", LVCFMT_CENTER, 80);



	List->InsertColumn(LISTNUM++, "RESULT", LVCFMT_CENTER, 80);
	List->InsertColumn(LISTNUM++, "비고", LVCFMT_CENTER, 80);

	ListItemNum = LISTNUM;
	Copy_List(&m_WorkList, ((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList, ListItemNum);
}

CString C_3D_Depth_Option::Mes_Result()
{
	CString sz[9];
	int		nResult[9];

	// 제일 마지막 항목을 추가하자
	int nSel = m_WorkList.GetItemCount()-1;

	// C-DB
	CString str;
	CString strData;

	
	strData.Empty();
	m_szMesResult = _T("");
	if(b_StopFail == FALSE){
		int LISTNUM = 4;

		sz[0].Format(_T("%6.2f"), m_dAveData);

		strData += sz[0] + ":";

		if (m_bAveData)
		{
			strData += "1,";
		}
		else{
			strData += "0,";
		}

		sz[1].Format(_T("%6.2f"), m_dStdevData);

		strData += sz[1] + ":";

		if (m_bStdevData)
		{
			strData += "1";
		}
		else{
			strData += "0";
		}

		m_szMesResult = strData;
		m_szMesResult.Replace(" ", "");
	}
	else{

		strData += ":1,";
		strData += ":1";
		
		m_szMesResult = strData;
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_stNewMesData[NewMES_3D_Depth] = m_szMesResult;

	return m_szMesResult;
}

void C_3D_Depth_Option::EXCEL_SAVE()
{
	CString Item[2] = { "밝기 평균", "표준편차" };


	CString ListName;
	int ListNUM = 0;


	CString str;


	CString Data = "";
	str = "***********************3D Depth ***********************\n\n";
	Data += str;
	str.Empty();
	str.Format("평균 Threshold,MIN, %6.2f,MAX, %6.2f,\n표준편차 Threshold, MIN, %6.2f,MAX, %6.2f,\n", m_dAveMin, m_dAveMax, m_dStdevMin,m_dStdevMax );
	Data += str;
	str.Empty();
	str = "*************************************************\n";
	Data += str;

	((CImageTesterDlg  *)m_pMomWnd)->SAVE_EXCEL("3D_Depth", Item, 2, &m_WorkList, Data);
}

bool C_3D_Depth_Option::EXCEL_UPLOAD(){
	m_WorkList.DeleteAllItems();
	if(((CImageTesterDlg  *)m_pMomWnd)->EXCEL_UPLOAD("3D_Depth",&m_WorkList,1,&StartCnt)== TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}

	return TRUE;
}


BOOL C_3D_Depth_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

		if (pMsg->wParam == VK_RETURN){
		
			return TRUE;
		}
		
	}

	return CDialog::PreTranslateMessage(pMsg);
}

	
void C_3D_Depth_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == TRUE){
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;

		if(((CImageTesterDlg  *)m_pMomWnd)->b_SecretOption == 1){
		//	((CEdit *)GetDlgItem(IDC_Thresold_D))->ShowWindow(1);
			//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE2))->ShowWindow(1);

		}else{
		//	((CEdit *)GetDlgItem(IDC_Thresold_D))->ShowWindow(0);
			//((CButton *)GetDlgItem(IDC_BUTTON_PN_SAVE2))->ShowWindow(0);

		}



		
	}else{
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = -1;

	}
}

// void C_3D_Depth_Option::OnBnClickedButtonPnSave2()
// {
// 	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
// 
// 	CString str;
// 	str.Empty();
// 	str.Format("%d",ThrDiff);
// 	WritePrivateProfileString(str_model,"Diff",str,PN_filename);
// 	UpdateData(FALSE);
// }

void C_3D_Depth_Option::UploadList(){
	
}

void C_3D_Depth_Option::OnTimer(UINT_PTR nIDEvent)
{

	CDialog::OnTimer(nIDEvent);
}


void C_3D_Depth_Option::OnBnClickedButtontest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(0);
	((CImageTesterDlg  *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(1);
}


#pragma region LOT관련 함수
void C_3D_Depth_Option::LOT_Set_List(CListCtrl *List){

	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT 명",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(5, "밝기 평균", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "표준편차", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "RESULT", LVCFMT_CENTER, 80);
	List->InsertColumn(8, "비고", LVCFMT_CENTER, 80);

	Lot_InsertNum = 9;
	Copy_List(&m_Lot_WorkList, ((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList, Lot_InsertNum);
}

void C_3D_Depth_Option::LOT_InsertDataList(){
	CString strCnt="";

	int Index = m_Lot_WorkList.InsertItem(Lot_StartCnt, "", 0);
	Lot_InsertIndex = m_Lot_WorkList.InsertItem(Lot_StartCnt, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Lot_InsertIndex + 1);
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 0, strCnt);

	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 1, ((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 2, ((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);////////LOT 이름
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 3, ((CImageTesterDlg *)m_pMomWnd)->strDay + "");
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 4, ((CImageTesterDlg *)m_pMomWnd)->strTime + "");

	int CopyIndex = m_WorkList.GetItemCount() - 1;

	int count = 0;
	for (int t = 0; t < Lot_InsertNum; t++){
		if ((t != 2) && (t != 0)){
			m_Lot_WorkList.SetItemText(Index, t, m_WorkList.GetItemText(CopyIndex, count));
			count++;

		}
		else if (t == 0){
			count++;
		}
		else{
			m_Lot_WorkList.SetItemText(Index, t, ((CImageTesterDlg  *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;

}
void C_3D_Depth_Option::LOT_EXCEL_SAVE(){
	CString Item[2] = { "밝기 평균", "표준편차"};

	CString ListName;
	
	CString str = "";
	CString Data = "";
	str = "***********************3D Depth ***********************\n\n";
	Data += str;
	str.Empty();
	str.Format("평균 Threshold,MIN, %6.2f,MAX, %6.2f,\n표준편차 Threshold, MIN, %6.2f,MAX, %6.2f,\n", m_dAveMin, m_dAveMax, m_dStdevMin, m_dStdevMax);
	Data += str;
	str.Empty();
	str = "*************************************************\n";
	Data += str;

	((CImageTesterDlg  *)m_pMomWnd)->LOT_SAVE_EXCEL("3D_Depth", Item, 2, &m_Lot_WorkList, Data);
}

bool C_3D_Depth_Option::LOT_EXCEL_UPLOAD(){
	m_Lot_WorkList.DeleteAllItems();
	if(((CImageTesterDlg  *)m_pMomWnd)->LOT_EXCEL_UPLOAD("3D_Depth",&m_Lot_WorkList,1,&Lot_StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}
 
#pragma endregion 

bool C_3D_Depth_Option::SNRGen_16bit(WORD *GRAYScanBuf){

	// 
	bool m_Success = false;
	unsigned long count = 0;

	double Sum1 = 0, Sum2 = 0;
	double data = 0;
	for (int y = 0; y < CAM_IMAGE_HEIGHT; y++)
	{
		for (int x = 0; x < CAM_IMAGE_WIDTH; x++)
		{
			data = (double)GRAYScanBuf[y*CAM_IMAGE_WIDTH + x];
			Sum1 += data;
			Sum2 += data*data;
			count++;
		}
	}
	double Mean = 0;
	double mean2 = 0;
	double StdDev = 0;
	if (count > 0)
	{
		Mean = Sum1 / (double)count;

		mean2 = Sum2 / (double)count;
		
		StdDev = sqrt(mean2 - Mean*Mean);
	}

	m_dAveData = Mean;
	m_dStdevData = StdDev;

	CString strData;
	strData.Format(_T("%6.2f"), m_dAveData);
	((CImageTesterDlg  *)m_pMomWnd)->m_pRes3D_DepthOptWnd->AVE_VALUE(strData);
	strData.Format(_T("%6.2f"), m_dStdevData);
	((CImageTesterDlg  *)m_pMomWnd)->m_pRes3D_DepthOptWnd->STDEV_VALUE(strData);

	if (m_dAveMin <= m_dAveData && m_dAveMax >= m_dAveData)
	{
		m_bAveData = TRUE;
		((CImageTesterDlg  *)m_pMomWnd)->m_pRes3D_DepthOptWnd->AVE_RESULT(1, "PASS");
	}
	else{
		m_bAveData = FALSE;
		((CImageTesterDlg  *)m_pMomWnd)->m_pRes3D_DepthOptWnd->AVE_RESULT(2, "FAIL");
	}

	if (m_dStdevMin <= m_dStdevData && m_dStdevMax >= m_dStdevData)
	{
		m_bStdevData = TRUE;
		((CImageTesterDlg  *)m_pMomWnd)->m_pRes3D_DepthOptWnd->STDEV_RESULT(1, "PASS");
	}
	else{
		m_bStdevData = FALSE;
		((CImageTesterDlg  *)m_pMomWnd)->m_pRes3D_DepthOptWnd->STDEV_RESULT(2, "FAIL");
	}

	if (m_bAveData&&m_bStdevData){
		m_Success = TRUE;
	}
	else{
		m_Success = FALSE;
	}
	return m_Success;

}


void C_3D_Depth_Option::OnBnClickedButtonSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strData;
	((CEdit *)GetDlgItem(IDC_EDIT_BR_MIN))->GetWindowText(strData);
	m_dAveMin = _ttof(strData);

	((CEdit *)GetDlgItem(IDC_EDIT_BR_MAX))->GetWindowText(strData);
	m_dAveMax = _ttof(strData);

	((CEdit *)GetDlgItem(IDC_EDIT_BRSTDEV_MIN))->GetWindowText(strData);
	m_dStdevMin = _ttof(strData);

	((CEdit *)GetDlgItem(IDC_EDIT_BRSTDEV_MAX))->GetWindowText(strData);
	m_dStdevMax = _ttof(strData);


	if (m_dAveMin > m_dAveMax)
	{
		m_dAveMax = m_dAveMin + 1;
	}
	if (m_dStdevMin > m_dStdevMax)
	{
		m_dStdevMax = m_dStdevMin + 1;
	}

	strData.Format(_T("%6.2f"), m_dAveMin);
	((CEdit *)GetDlgItem(IDC_EDIT_BR_MIN))->SetWindowText(strData);

	strData.Format(_T("%6.2f"), m_dAveMax);
	((CEdit *)GetDlgItem(IDC_EDIT_BR_MAX))->SetWindowText(strData);

	strData.Format(_T("%6.2f"), m_dStdevMin);
	((CEdit *)GetDlgItem(IDC_EDIT_BRSTDEV_MIN))->SetWindowText(strData);

	strData.Format(_T("%6.2f"), m_dStdevMax);
	((CEdit *)GetDlgItem(IDC_EDIT_BRSTDEV_MAX))->SetWindowText(strData);


	Save_parameter();
}
