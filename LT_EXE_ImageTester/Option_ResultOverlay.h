#pragma once


// COption_ResultOverlay 대화 상자입니다.

class COption_ResultOverlay : public CDialog
{
	DECLARE_DYNAMIC(COption_ResultOverlay)

public:
	COption_ResultOverlay(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COption_ResultOverlay();

	void	Setup(CWnd* IN_pMomWnd);
	// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_OVERLAY_CAN };
	void InitStat();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	CWnd	*m_pMomWnd;	

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_listCh1OverlayResult;
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnLvnItemchangedListCh1OverlayResult(NMHDR *pNMHDR, LRESULT *pResult);
};
