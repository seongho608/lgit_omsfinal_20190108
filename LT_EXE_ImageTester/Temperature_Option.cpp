// Temperature_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Temperature_Option.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "afxdialogex.h"


// Temperature_Option 대화 상자입니다.

IMPLEMENT_DYNAMIC(Temperature_Option, CDialog)

Temperature_Option::Temperature_Option(CWnd* pParent /*=NULL*/)
	: CDialog(Temperature_Option::IDD, pParent)
{
	m_dbTemper = 0;
}

Temperature_Option::~Temperature_Option()
{
}

void Temperature_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_LIST_DN, m_ROI_List);
	DDX_Control(pDX, IDC_LIST_WORK, m_WorkList);
	DDX_Control(pDX, IDC_LIST_LOT_WORKLIST, m_Lot_WorkList);
}


BEGIN_MESSAGE_MAP(Temperature_Option, CDialog)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_TS_TEST, &Temperature_Option::OnBnClickedButtonTsTest)
	ON_BN_CLICKED(IDC_BUTTON_TS_SAVE, &Temperature_Option::OnBnClickedButtonTsSave)
END_MESSAGE_MAP()


// Temperature_Option 메시지 처리기입니다.

//=============================================================================
// Method		: OnInitDialog
// Access		: virtual public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/6/2 - 17:45
// Desc.		:
//=============================================================================
BOOL Temperature_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	//m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	//m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;

	//OnSetList_Header();

	Temperature_Sensor_filename = ((CImageTesterDlg  *)m_pMomWnd)->modelfile_Name;

	Load_parameter();
	UpdateData(FALSE);

	//OnSetList_Data();

	Set_List(&m_WorkList);
	LOT_Set_List(&m_Lot_WorkList);

	//InitEVMS();

	return TRUE;  // return TRUE unless you set the focus to a control
}
// CDepthNoise_Option 메시지 처리기입니다.
void Temperature_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd = IN_pMomWnd;
}

//=============================================================================
// Method		: Setup
// Access		: public  
// Returns		: void
// Parameter	: CWnd * IN_pMomWnd
// Parameter	: CEdit * pTEDIT
// Parameter	: tINFO INFO
// Qualifier	:
// Last Update	: 2018/6/2 - 17:45
// Desc.		:
//=============================================================================
void Temperature_Option::Setup(CWnd* IN_pMomWnd, CEdit	*pTEDIT, tINFO INFO)
{
	m_pMomWnd = IN_pMomWnd;
	pTStat = pTEDIT;
	m_INFO = INFO;

	str_model.Empty();
	str_model.Format(INFO.NAME);
}

//=============================================================================
// Method		: DepthNoisePic
// Access		: public  
// Returns		: bool
// Parameter	: CDC * cdc
// Parameter	: int NUM
// Qualifier	:
// Last Update	: 2018/6/2 - 18:13
// Desc.		:
//=============================================================================
bool Temperature_Option::Temperature_Option_Pic(CDC *cdc, int NUM)
{
	CPen	my_Pan;
	CPen	my_Pan2;
	CPen	*old_pan;
	CPen	*old_pan2;
	CFont	font;
	CString szText;

	::SetBkMode(cdc->m_hDC, TRANSPARENT);

	my_Pan.CreatePen(PS_SOLID, 2, RED_COLOR);
	old_pan = cdc->SelectObject(&my_Pan);

	if (m_Success == TRUE)
	{
		cdc->SetTextColor(BLUE_COLOR);

		my_Pan2.CreatePen(PS_SOLID, 2, RGB(0, 0, 255));
		old_pan2 = cdc->SelectObject(&my_Pan2);
	}
	else
	{
		cdc->SetTextColor(RED_COLOR);

		my_Pan2.CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
		old_pan2 = cdc->SelectObject(&my_Pan2);
	}

	font.CreatePointFont(120, "Arial");
	cdc->SetTextAlign(TA_CENTER | TA_BASELINE);
	szText.Format("Temperature : %.3f", m_dbTemper);

	cdc->TextOut(CAM_IMAGE_WIDTH / 2, CAM_IMAGE_HEIGHT - 100, szText.GetBuffer(0), szText.GetLength());

	//cdc->MoveTo(m_rtROI.left, m_rtROI.top);
	//cdc->LineTo(m_rtROI.right, m_rtROI.top);
	//cdc->LineTo(m_rtROI.right, m_rtROI.bottom);
	//cdc->LineTo(m_rtROI.left, m_rtROI.bottom);
	//cdc->LineTo(m_rtROI.left, m_rtROI.top);

	//cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();
	my_Pan2.DeleteObject();
	old_pan->DeleteObject();
	old_pan2->DeleteObject();

	font.DeleteObject();

	return TRUE;
}

//=============================================================================
// Method		: Pic
// Access		: public  
// Returns		: void
// Parameter	: CDC * cdc
// Qualifier	:
// Last Update	: 2018/6/2 - 18:25
// Desc.		:
//=============================================================================
void Temperature_Option::Pic(CDC *cdc)
{
	Temperature_Option_Pic(cdc, 0);
}

//=============================================================================
// Method		: InitPrm
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:25
// Desc.		:
//=============================================================================
void Temperature_Option::InitPrm()
{
	Load_parameter();
	UpdateData(FALSE);
}


void Temperature_Option::OnBnClickedButtonTsSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString szData;

	m_dwSlaveIDTemper = Edit_GetValue(IDC_TS_SLAVEID);
	m_dwAddressTemper = Edit_GetValue(IDC_TS_ADDRESS);
		
	((CEdit *)GetDlgItem(IDC_TS_MINSPEC))->GetWindowText(szData);
	m_nMinSpec = _ttoi(szData);

	((CEdit *)GetDlgItem(IDC_TS_MAXSPEC))->GetWindowText(szData);
	m_nMaxSpec = _ttoi(szData);

	Save_parameter();
}


void Temperature_Option::OnBnClickedButtonTsTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CButton *)GetDlgItem(IDC_BUTTON_TS_TEST))->EnableWindow(0);

	((CImageTesterDlg  *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();

	((CButton *)GetDlgItem(IDC_BUTTON_TS_TEST))->EnableWindow(1);
}


//=============================================================================
// Method		: FAIL_UPLOAD
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:28
// Desc.		:
//=============================================================================
void Temperature_Option::FAIL_UPLOAD()
{
	if (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)
	{
		InsertList();

		m_WorkList.SetItemText(InsertIndex, 4, "X");//result
		m_WorkList.SetItemText(InsertIndex, 5, "FAIL");//result
		m_WorkList.SetItemText(InsertIndex, 6, "STOP");//비고

		Str_Mes[0] = "FAIL";
		Str_Mes[1] = "0";

		((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_DEPTHNOISE, "STOP");
		StartCnt++;
		b_StopFail = TRUE;
	}
}

//=============================================================================
// Method		: CModel_Create
// Access		: public  
// Returns		: void
// Parameter	: CDepthNoise_Option * * pWnd
// Parameter	: CTabCtrl * pTabWnd
// Parameter	: CWnd * pMomWnd
// Parameter	: CEdit * pEdit
// Parameter	: tINFO INFO
// Qualifier	:
// Last Update	: 2018/6/2 - 18:29
// Desc.		:
//=============================================================================
void CModel_Create(Temperature_Option **pWnd, CTabCtrl *pTabWnd, CWnd* pMomWnd, CEdit *pEdit, tINFO INFO)
{
	CRect OptionRect;

	if (pTabWnd != NULL)
	{
		((Temperature_Option *)*pWnd) = new Temperature_Option(pMomWnd);
		((Temperature_Option *)*pWnd)->Setup(pMomWnd, pEdit, INFO);
		((Temperature_Option *)*pWnd)->Create(((Temperature_Option *)*pWnd)->IDD, pTabWnd);//&m_CtrTab);
		((Temperature_Option *)*pWnd)->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		((Temperature_Option *)*pWnd)->MoveWindow(20, 40, OptionRect.Width(), OptionRect.Height());
	}
}


//=============================================================================
// Method		: CModel_Delete
// Access		: public  
// Returns		: void
// Parameter	: CDepthNoise_Option * * pWnd
// Qualifier	:
// Last Update	: 2018/6/2 - 18:29
// Desc.		:
//=============================================================================
void CModel_Delete(Temperature_Option **pWnd)
{
	if (((Temperature_Option *)*pWnd) != NULL)
	{
		delete ((Temperature_Option *)*pWnd);
		((Temperature_Option *)*pWnd) = NULL;
	}
}



//=============================================================================
// Method		: StatePrintf
// Access		: public  
// Returns		: void
// Parameter	: LPCSTR lpcszString
// Parameter	: ...
// Qualifier	:
// Last Update	: 2018/6/2 - 18:29
// Desc.		:
//=============================================================================
void Temperature_Option::StatePrintf(LPCSTR lpcszString, ...)
{
	if (pTStat == NULL){ return; }
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;

	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;
	va_start(args, lpcszString);
	wvsprintf(lpszBuf, lpcszString, args);
	cstr.Empty();
	cstr.Format("%s\r\n", lpszBuf);
	va_end(args);

	TRACE(cstr);
	bufLen = pTStat->GetWindowTextLength();

	int del_line = 0;
	while (bufLen > MAX_LOG_LEN)
	{
		int nLineIndex = pTStat->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat->SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat->Clear();			   //라인하나를 지운다
		bufLen = pTStat->GetWindowTextLength();
		del_line++;
	}

	pTStat->SetSel(-1, 0);
	pTStat->ReplaceSel(cstr);
	pTStat->SetSel(-1, -1);
}

//=============================================================================
// Method		: Edit_SetValue
// Access		: public  
// Returns		: void
// Parameter	: UINT nID
// Parameter	: DWORD wData
// Parameter	: BOOL bHex
// Qualifier	:
// Last Update	: 2018/6/3 - 14:28
// Desc.		:
//=============================================================================
void Temperature_Option::Edit_SetValue(UINT nID, DWORD wData, BOOL bHex /*= TRUE*/)
{
	CString text;
	if (bHex)
	{
		text.Format(_T("0x%04x"), wData);
	}
	else{
		text.Format(_T("%d"), wData);

	}
	((CEdit *)GetDlgItem(nID))->SetWindowText(text);
}


//=============================================================================
// Method		: Edit_GetValue
// Access		: public  
// Returns		: UINT
// Parameter	: UINT nID
// Parameter	: BOOL bHex
// Qualifier	:
// Last Update	: 2018/6/3 - 14:28
// Desc.		:
//=============================================================================
UINT Temperature_Option::Edit_GetValue(UINT nID, BOOL bHex /*= TRUE*/)
{
	CString text;
	((CEdit *)GetDlgItem(nID))->GetWindowText(text);

	//hex
	unsigned int n = 0;
	if (bHex)
	{
		if (text.Find(_T("0x")) == 0)
			_stscanf_s(text.GetBuffer(0) + 2, _T("%x"), &n);
		else
			_stscanf_s(text.GetBuffer(0), _T("%x"), &n);
	}
	else{
		_stscanf_s(text.GetBuffer(0), _T("%u"), &n);
	}

	return n;
}


//=============================================================================
// Method		: Save_parameter
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 17:59
// Desc.		:
//=============================================================================
void Temperature_Option::Save_parameter()
{
	CString szValue;
	CString szTitle = "Temperature_Sensor_INIT";

	szValue.Format("%d", m_INFO.ID);
	WritePrivateProfileString(str_model, "ID", szValue, Temperature_Sensor_filename);

	szValue.Format("%s", m_INFO.MODE);
	WritePrivateProfileString(str_model, "MODE", szValue, Temperature_Sensor_filename);

	szValue.Format("%s", m_INFO.NAME);
	WritePrivateProfileString(str_model, "NAME", szValue, Temperature_Sensor_filename);

	szValue.Format("%d", m_dwSlaveIDTemper);
	WritePrivateProfileString(str_model, szTitle + "SlaveID", szValue, Temperature_Sensor_filename);

	szValue.Format("%d", m_dwAddressTemper);
	WritePrivateProfileString(str_model, szTitle + "Address", szValue, Temperature_Sensor_filename);

	//szValue.Format("%d", m_nTemperature);
	//WritePrivateProfileString(str_model, szTitle + "Temperature", szValue, Temperature_Sensor_filename);


	szValue.Format("%d", m_nMinSpec);
	WritePrivateProfileString(str_model, szTitle + "MinSpec", szValue, Temperature_Sensor_filename);

	szValue.Format("%d", m_nMaxSpec);
	WritePrivateProfileString(str_model, szTitle + "MaxSpec", szValue, Temperature_Sensor_filename);
	
	UpdateData(FALSE);
}

//=============================================================================
// Method		: Load_parameter
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 17:56
// Desc.		:
//=============================================================================
void Temperature_Option::Load_parameter()
{
	CFileFind filefind;
	CString szTitle = "Temperature_Sensor_INIT";

	m_dwSlaveIDTemper = GetPrivateProfileDouble(str_model, szTitle + "SlaveID", 0, Temperature_Sensor_filename);
	m_dwAddressTemper = GetPrivateProfileDouble(str_model, szTitle + "Address", 0, Temperature_Sensor_filename);
	m_nMinSpec = GetPrivateProfileDouble(str_model, szTitle + "MinSpec", 0, Temperature_Sensor_filename);
	m_nMaxSpec = GetPrivateProfileDouble(str_model, szTitle + "MaxSpec", 0, Temperature_Sensor_filename);

	CString szData;
	Edit_SetValue(IDC_TS_SLAVEID, m_dwSlaveIDTemper);
	Edit_SetValue(IDC_TS_ADDRESS, m_dwAddressTemper);

	szData.Format(_T("%d"), m_nMinSpec);
	((CEdit *)GetDlgItem(IDC_TS_MINSPEC))->SetWindowText(szData);

	szData.Format(_T("%d"), m_nMaxSpec);
	((CEdit *)GetDlgItem(IDC_TS_MAXSPEC))->SetWindowText(szData);

	//// ROI
	//m_rtROI.left = GetPrivateProfileInt(str_model, szTitle + "left", -1, DepthNoise_filename);
	//m_rtROI.top = GetPrivateProfileInt(str_model, szTitle + "top", -1, DepthNoise_filename);
	//m_rtROI.right = GetPrivateProfileInt(str_model, szTitle + "right", -1, DepthNoise_filename);
	//m_rtROI.bottom = GetPrivateProfileInt(str_model, szTitle + "bottom", -1, DepthNoise_filename);

	Save_parameter();
}
//=============================================================================
// Method		: Save
// Access		: public  
// Returns		: void
// Parameter	: int NUM
// Qualifier	:
// Last Update	: 2018/6/2 - 18:29
// Desc.		:
//=============================================================================
void Temperature_Option::Save(int NUM)
{
	WritePrivateProfileString(str_model, NULL, "", Temperature_Sensor_filename);

	if (NUM != -1)
	{
		str_model.Empty();
		str_model.Format("Model_%d", NUM);
		Save_parameter();
	}
}

//=============================================================================
// Method		: Run
// Access		: public  
// Returns		: tResultVal
// Qualifier	:
// Last Update	: 2018/6/2 - 18:25
// Desc.		:
//=============================================================================
tResultVal Temperature_Option::Run()
{
	CString szValue = "";
	b_StopFail = FALSE;
	
	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE)
	{
		InsertList();
	} 

	//	((CImageTesterDlg  *)m_pMomWnd)->m_pResDepthNoiseOptWnd->Initstat();
	
	int camcnt = 0;
	tResultVal retval = { 0, };
	BYTE arbyBufRead[256] = { 0, };
	BOOL bResult = FALSE;
	DWORD dwTemp = NULL;
	double dbTemper = 0.0;

	// * Read :  Max 256 Byte
	if (TRUE == ((CImageTesterDlg *)m_pMomWnd)->m_LVDSVideo.I2C_Read_Repeat(0, (BYTE)(m_dwSlaveIDTemper ), 1, m_dwAddressTemper, 2, arbyBufRead))
	{
		CString strData;

		strData.Format(_T("%02x%02x"), arbyBufRead[0], arbyBufRead[1]);
		if (_T("0000") == strData)		{			bResult = FALSE;			((CImageTesterDlg  *)m_pMomWnd)->StatePrintf("Temperature Sensor Read FAIL");
		}
		else
		{
			bResult = TRUE;
			dwTemp = arbyBufRead[0] << 4;
			dwTemp |= arbyBufRead[1] >> 4;

			dbTemper = 0.0625 * dwTemp;
		}

		m_dbTemper = dbTemper;
	}	else	{		bResult = FALSE;		((CImageTesterDlg  *)m_pMomWnd)->StatePrintf("Temperature Sensor Read FAIL");
	}
	//---------------------------
	retval.m_ID = 0x19;
	retval.ValString.Empty();

	CString stateDATA = "Temperature Sensor 검사_";


	if (bResult == TRUE && m_dbTemper > m_nMinSpec && m_dbTemper < m_nMaxSpec)
	{
		m_Success = TRUE;

		szValue.Format("%.3f", m_dbTemper);
		m_WorkList.SetItemText(InsertIndex, 4, szValue);
		m_WorkList.SetItemText(InsertIndex, 5, "PASS");

		retval.m_Success = TRUE;
		Str_Mes[0] = szValue;
		Str_Mes[1] = "1";

		if (((CImageTesterDlg *)m_pMomWnd)->LotMod != 1)
		{
			((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_DEPTHNOISE, "PASS");//고쳐야함
		}

		stateDATA += "PASS";
		retval.ValString.Format("%.3f / OK", m_dbTemper);

		((CImageTesterDlg  *)m_pMomWnd)->m_pResTemperatureOptWnd->RESULT_TEXT(1, "PASS");
		((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->TestState(0, "PASS");
	}
	else
	{
		m_Success = FALSE;
		m_WorkList.SetItemText(InsertIndex, 4, szValue);
		m_WorkList.SetItemText(InsertIndex, 5, "FAIL");

		retval.m_Success = FALSE;
		Str_Mes[0] = szValue;
		Str_Mes[1] = "0";

		if (((CImageTesterDlg *)m_pMomWnd)->LotMod != 1)
		{
			((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_DEPTHNOISE, "FAIL");
		}

		stateDATA += "FAIL";
		retval.ValString.Format("%.3f / FAIL", m_dbTemper);

		((CImageTesterDlg  *)m_pMomWnd)->m_pResTemperatureOptWnd->RESULT_TEXT(2, "FAIL");
		((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->TestState(1, "FAIL");
	}

	((CImageTesterDlg  *)m_pMomWnd)->StatePrintf(stateDATA);
	((CImageTesterDlg  *)m_pMomWnd)->m_pResTemperatureOptWnd->TemperatureNUM_TEXT(szValue);

	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE)
	{
		StartCnt++;
	}

	return retval;
}


//---------------------------------------------------------------------WORKLIST
//=============================================================================
// Method		: InsertList
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:29
// Desc.		:
//=============================================================================
void Temperature_Option::InsertList()
{
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;
	
	InsertIndex = m_WorkList.InsertItem(StartCnt,"",0);
	
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_WorkList.SetItemText(InsertIndex,0,strCnt);
	
	m_WorkList.SetItemText(InsertIndex,1,((CImageTesterDlg  *)m_pMomWnd)->m_modelName);
	m_WorkList.SetItemText(InsertIndex,2,((CImageTesterDlg  *)m_pMomWnd)->strDay+"");
	m_WorkList.SetItemText(InsertIndex,3,((CImageTesterDlg  *)m_pMomWnd)->strTime+"");
}

//=============================================================================
// Method		: Set_List
// Access		: public  
// Returns		: void
// Parameter	: CListCtrl * List
// Qualifier	:
// Last Update	: 2018/6/2 - 18:29
// Desc.		:
//=============================================================================
void Temperature_Option::Set_List(CListCtrl *List)
{

	while (List->DeleteColumn(0));
	((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0, "NO", LVCFMT_CENTER, 30);
	List->InsertColumn(1, "모델명", LVCFMT_CENTER, 100);
	List->InsertColumn(2, "Start DATE", LVCFMT_CENTER, 80);
	List->InsertColumn(3, "Start TIME", LVCFMT_CENTER, 80);

	int LISTNUM = 4;

	List->InsertColumn(LISTNUM++, "Temperature", LVCFMT_CENTER, 80);
	List->InsertColumn(LISTNUM++, "RESULT", LVCFMT_CENTER, 80);
	List->InsertColumn(LISTNUM++, "비고", LVCFMT_CENTER, 80);

	ListItemNum = LISTNUM;
	Copy_List(&m_WorkList, ((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList, ListItemNum);
}


//=============================================================================
// Method		: Mes_Result
// Access		: public  
// Returns		: CString
// Qualifier	:
// Last Update	: 2018/6/2 - 18:30
// Desc.		:
//=============================================================================
CString Temperature_Option::Mes_Result()
{
	CString sz[9];
	int	nResult[9];

	// 제일 마지막 항목을 추가하자
	int nSel = m_WorkList.GetItemCount() - 1;

	// C-DB
	CString szValue;
	CString strData;

	strData.Empty();
	m_szMesResult = _T("");

	if (b_StopFail == FALSE){
		m_szMesResult.Format(_T("%s:%s"), Str_Mes[0], Str_Mes[1]);
		m_szMesResult.Replace(" ", "");
	}
	else{
		m_szMesResult.Format(_T(":1"));
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_stNewMesData[NewMES_Temperature] = m_szMesResult;

	return m_szMesResult;
}

//=============================================================================
// Method		: EXCEL_SAVE
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:30
// Desc.		:
//=============================================================================
void Temperature_Option::EXCEL_SAVE()
{
	CString Item[1] = { "VAL" };
	CString Data = "";
	CString szValue = "";
	szValue = "***********************Temperature***********************\n";
	Data += szValue;
	szValue.Empty();
	szValue.Format("\n");
	Data += szValue;
	szValue.Empty();
	szValue.Format("\n");
	Data += szValue;
	szValue.Empty();
	szValue = "*************************************************\n\n";
	Data += szValue;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("TemperatureSensor", Item, 1, &m_WorkList, Data);
}

//=============================================================================
// Method		: EXCEL_UPLOAD
// Access		: public  
// Returns		: bool
// Qualifier	:
// Last Update	: 2018/6/2 - 18:30
// Desc.		:
//=============================================================================
bool Temperature_Option::EXCEL_UPLOAD()
{
	m_WorkList.DeleteAllItems();

	if (((CImageTesterDlg  *)m_pMomWnd)->EXCEL_UPLOAD("TemperatureSensor", &m_WorkList, 1, &StartCnt) == TRUE)
	{
		return TRUE;
	}
	else
	{
		StartCnt = 0;
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2018/6/2 - 18:30
// Desc.		:
//=============================================================================
BOOL Temperature_Option::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}

		if (pMsg->wParam == VK_RETURN)
		{
			return TRUE;
		}

	}

	return CDialog::PreTranslateMessage(pMsg);
}

#pragma region LOT관련 함수
//=============================================================================
// Method		: LOT_Set_List
// Access		: public  
// Returns		: void
// Parameter	: CListCtrl * List
// Qualifier	:
// Last Update	: 2018/6/2 - 18:31
// Desc.		:
//=============================================================================
void Temperature_Option::LOT_Set_List(CListCtrl *List)
{
	while (List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0, "NO", LVCFMT_CENTER, 30);
	List->InsertColumn(1, "모델명", LVCFMT_CENTER, 100);
	List->InsertColumn(2, "LOT 명", LVCFMT_CENTER, 80);
	List->InsertColumn(3, "Start DATE", LVCFMT_CENTER, 80);
	List->InsertColumn(4, "Start TIME", LVCFMT_CENTER, 80);
	List->InsertColumn(5, "Temperature", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "result", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "비고", LVCFMT_CENTER, 80);

	Lot_InsertNum = 8;
	Copy_List(&m_Lot_WorkList, ((CImageTesterDlg  *)m_pMomWnd)->m_pWorkList, Lot_InsertNum);
}

//=============================================================================
// Method		: LOT_InsertDataList
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:31
// Desc.		:
//=============================================================================
void Temperature_Option::LOT_InsertDataList()
{
	CString strCnt = "";

	int Index = m_Lot_WorkList.InsertItem(Lot_StartCnt, "", 0);
	Lot_InsertIndex = m_Lot_WorkList.InsertItem(Lot_StartCnt, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Lot_InsertIndex + 1);
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 0, strCnt);

	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 1, ((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 2, ((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);////////LOT 이름
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 3, ((CImageTesterDlg *)m_pMomWnd)->strDay + "");
	m_Lot_WorkList.SetItemText(Lot_InsertIndex, 4, ((CImageTesterDlg *)m_pMomWnd)->strTime + "");

	int CopyIndex = m_WorkList.GetItemCount() - 1;

	int count = 0;
	for (int t = 0; t < Lot_InsertNum; t++){
		if ((t != 2) && (t != 0)){
			m_Lot_WorkList.SetItemText(Index, t, m_WorkList.GetItemText(CopyIndex, count));
			count++;

		}
		else if (t == 0){
			count++;
		}
		else{
			m_Lot_WorkList.SetItemText(Index, t, ((CImageTesterDlg  *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;
}

//=============================================================================
// Method		: LOT_EXCEL_SAVE
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/2 - 18:32
// Desc.		:
//=============================================================================
void Temperature_Option::LOT_EXCEL_SAVE()
{
	CString Item[1] = { "Temperature" };
	CString Data = "";
	CString szValue = "";
	szValue = "***********************Temperature_Sensor***********************\n";
	Data += szValue;
	szValue.Empty();
	szValue.Format("\n");
	Data += szValue;
	szValue.Empty();
	szValue.Format("\n");
	Data += szValue;
	szValue.Empty();
	szValue = "*************************************************\n\n";
	Data += szValue;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("Temperature_Sensor", Item, 1, &m_Lot_WorkList, Data);
}

//=============================================================================
// Method		: LOT_EXCEL_UPLOAD
// Access		: public  
// Returns		: bool
// Qualifier	:
// Last Update	: 2018/6/2 - 18:32
// Desc.		:
//=============================================================================
bool Temperature_Option::LOT_EXCEL_UPLOAD()
{
	m_Lot_WorkList.DeleteAllItems();

	if (((CImageTesterDlg  *)m_pMomWnd)->LOT_EXCEL_UPLOAD("Temperature_Sensor", &m_Lot_WorkList, 1, &Lot_StartCnt) == TRUE)
	{
		return TRUE;
	}
	else
	{
		Lot_StartCnt = 0;
		return FALSE;
	}
	return TRUE;
}

void Temperature_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if (bShow == TRUE)
	{
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;
	}
	else
	{
		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = -1;
	}
}
#pragma endregion 

