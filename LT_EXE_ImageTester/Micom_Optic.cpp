// Micom_Optic.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Micom_Optic.h"


// CMicom_Optic 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMicom_Optic, CDialog)

CMicom_Optic::CMicom_Optic(CWnd* pParent /*=NULL*/)
	: CDialog(CMicom_Optic::IDD, pParent)
	, b_edit(FALSE)
{

}

CMicom_Optic::~CMicom_Optic()
{
}

void CMicom_Optic::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
	//str_ModelPath = ((CImageTesterDlg  *)m_pMomWnd)->modelfile_Name;
	//Load_parameter();
}

void CMicom_Optic::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT)//,tINFO INFO)
{
	pTStat		= pTEDIT;
	Setup(IN_pMomWnd);
}

void CMicom_Optic::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_DEFAULT_START_X, str_def_startX);
	DDX_Text(pDX, IDC_DEFAULT_START_Y, str_def_startY);
	DDX_Text(pDX, IDC_DEFAULT_END_X, str_def_endX);
	DDX_Text(pDX, IDC_DEFAULT_END_Y, str_def_endY);
	DDX_Text(pDX, IDC_EDIT_OFFSET_X, edit_offsetX);
	DDX_Text(pDX, IDC_EDIT_OFFSET_Y, edit_offsetY);
	DDX_Control(pDX, IDC_LIST_SETTING_POS, Setting_Poslist);
	DDX_Check(pDX, IDC_CHECK_EDITING, b_edit);
}


BEGIN_MESSAGE_MAP(CMicom_Optic, CDialog)
	ON_BN_CLICKED(IDC_SETTING_POS_READ, &CMicom_Optic::OnBnClickedSettingPosRead)
	ON_BN_CLICKED(IDC_DEFAULT_POS_SAVE, &CMicom_Optic::OnBnClickedDefaultPosSave)
	ON_EN_CHANGE(IDC_DEFAULT_START_X, &CMicom_Optic::OnEnChangeDefaultStartX)
	ON_EN_CHANGE(IDC_DEFAULT_START_Y, &CMicom_Optic::OnEnChangeDefaultStartY)
	ON_EN_CHANGE(IDC_DEFAULT_END_X, &CMicom_Optic::OnEnChangeDefaultEndX)
	ON_EN_CHANGE(IDC_DEFAULT_END_Y, &CMicom_Optic::OnEnChangeDefaultEndY)
	ON_WM_MEASUREITEM()
	ON_BN_CLICKED(IDC_SETTING_POS_WRITE, &CMicom_Optic::OnBnClickedSettingPosWrite)
	ON_BN_CLICKED(IDC_CHECK_EDITING, &CMicom_Optic::OnBnClickedCheckEditing)
	ON_EN_CHANGE(IDC_EDIT_OFFSET_X, &CMicom_Optic::OnEnChangeEditOffsetX)
	ON_EN_CHANGE(IDC_EDIT_OFFSET_Y, &CMicom_Optic::OnEnChangeEditOffsetY)
	ON_BN_CLICKED(IDC_DEFAULT_WRITE, &CMicom_Optic::OnBnClickedDefaultWrite)
	ON_BN_CLICKED(IDC_DEFAULT_INITWR, &CMicom_Optic::OnBnClickedDefaultInitwr)
	ON_WM_CTLCOLOR()
	ON_EN_SETFOCUS(IDC_EDIT_MODE_NAME, &CMicom_Optic::OnEnSetfocusEditModeName)
	ON_EN_SETFOCUS(IDC_DEFAULT_START_X, &CMicom_Optic::OnEnSetfocusDefaultStartX)
	ON_EN_SETFOCUS(IDC_DEFAULT_START_Y, &CMicom_Optic::OnEnSetfocusDefaultStartY)
	ON_EN_SETFOCUS(IDC_DEFAULT_END_X, &CMicom_Optic::OnEnSetfocusDefaultEndX)
	ON_EN_SETFOCUS(IDC_DEFAULT_END_Y, &CMicom_Optic::OnEnSetfocusDefaultEndY)
	ON_EN_SETFOCUS(IDC_EDIT_VALUE_RUNSTATE, &CMicom_Optic::OnEnSetfocusEditValueRunstate)
END_MESSAGE_MAP()


// CMicom_Optic 메시지 처리기입니다.


BOOL CMicom_Optic::OnInitDialog()
{
	CDialog::OnInitDialog();

	LoadMicomPrmSet(&Read_Pos, ((CImageTesterDlg  *)m_pMomWnd)->Micom_Path);


	((CEdit *)GetDlgItem(IDC_DEFAULT_START_X))->SetReadOnly(TRUE);
	((CEdit *)GetDlgItem(IDC_DEFAULT_START_Y))->SetReadOnly(TRUE);
	((CEdit *)GetDlgItem(IDC_DEFAULT_END_X))->SetReadOnly(TRUE);
	((CEdit *)GetDlgItem(IDC_DEFAULT_END_Y))->SetReadOnly(TRUE);
	((CButton *)GetDlgItem(IDC_DEFAULT_POS_SAVE))->EnableWindow(0);

	((CEdit *)GetDlgItem(IDC_EDIT_OFFSET_X))->SetWindowText("0");
	((CEdit *)GetDlgItem(IDC_EDIT_OFFSET_Y))->SetWindowText("0");

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_LIST_SETTING_POS,&ft,500,15);
	Font_Size_Change(IDC_EDIT_VALUE_RUNSTATE,&ft2,500,25);
	GetDlgItem(IDC_EDIT_MODE_NAME)->SetFont(&ft2);

	MODE_TEXT("왜곡 광축 DOWNLOAD");
	
	Set_List();
	Update_List();

	//LoadMicomPrmSet(tSetPoint *buf_pos,inifilename);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CMicom_Optic::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;
	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);
	LogFont.lfWeight =Weight;
	LogFont.lfHeight = Height;
	font->CreateFontIndirect(&LogFont);
	GetDlgItem(nID)->SetFont(font);
}



void CMicom_Optic::OnBnClickedSettingPosRead()
{
	RUNSTATE_TEXT("READING");
	SettingPosRead();
}

BOOL CMicom_Optic::SettingPosRead()
{
	int m_OffsetX = atoi(edit_offsetX);
	int m_OffsetY = atoi(edit_offsetY);

	edit_offsetX.Format("%d",m_OffsetX);
	edit_offsetY.Format("%d",m_OffsetY);
	UpdateData(FALSE);

	//((CImageTesterDlg  *)m_pMomWnd)->str_MainOffsetX.Format("%d",m_OffsetX);//str_MainOffsetX 변수는 메인 ui에 있는 에디트 컨트롤 offsetX를 선언한 변수
	//((CImageTesterDlg  *)m_pMomWnd)->str_MainOffsetY.Format("%d",m_OffsetY);//str_MainOffsetY 변수는 메인 ui에 있는 에디트 컨트롤 offsetY를 선언한 변수
	//((CImageTesterDlg  *)m_pMomWnd)->UpdateData(FALSE);/////////////////////이렇게 하면 메인 ui에 변수가 바뀐다.

	if(!Read_Func()){
		return FALSE;
	}

	///READ 에서 받아온 값.
	int StartX = Control_Pos.Old_Pos_Start.x + m_OffsetX;
	if(StartX < 0){
		StartX = 0;
	}
	StartX = (StartX/2)*2;
	
	int StartY = Control_Pos.Old_Pos_Start.y + m_OffsetY;
	if(StartY < 0){
		StartY = 0;
	}
	StartY = (StartY/2)*2;

	int EndX = StartX + 1287;
	int EndY = StartY + 727;

	Read_Pos.Set_Pos_Start.x = StartX;
	Read_Pos.Set_Pos_Start.y = StartY;
	Read_Pos.Set_Pos_End.x = EndX;
	Read_Pos.Set_Pos_End.y = EndY;
	Update_List();

	RUNSTATE_TEXT("READING OK");
	return TRUE;
}

BOOL CMicom_Optic::READ_START_XY()
{
	int start_x = 0;
	int start_y = 0;
	int buf = 0;

	// 1 r
	memset(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam,0,sizeof(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam));
	BYTE DATA[20] ={'!','1','R','0','0','0','0','0','0','0','0','@'};
	((CImageTesterDlg  *)m_pMomWnd)->SendData8Byte_Cam(DATA, 12);

	int lop = 0;
	for(lop = 0;lop<100;lop++){//500ms체크 
		DoEvents(10);
		if((((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[1] == '1')&&(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[2] == 'R')){
			lop = 300;
			break;
		}
	}

	if (lop != 300){
		return FALSE;
	}else {
		
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[3]);
		start_x += buf * 4096;
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[4]);
		start_x += buf * 256;
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[5]);
		start_x += buf * 16;
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[6]);
		start_x += buf;
		
		Read_Pos.Old_Pos_Start.x = start_x;
		
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[7]);
		start_y += buf * 4096;
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[8]);
		start_y += buf * 256;
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[9]);
		start_y += buf * 16;
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[10]);
		start_y += buf;
		
		Read_Pos.Old_Pos_Start.y = start_y;

		return TRUE;
	}
}

BOOL CMicom_Optic::READ_END_XY()
{
	int end_x = 0;
	int end_y = 0;
	int buf = 0;

	memset(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam,0,sizeof(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam));
	BYTE DATA[20] ={'!','2','R','0','0','0','0','0','0','0','0','@'};
	((CImageTesterDlg  *)m_pMomWnd)->SendData8Byte_Cam(DATA, 12);

	int lop = 0;
	for(lop = 0;lop<100;lop++){//500ms체크 
		DoEvents(10);
		if((((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[1] == '2')&&(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[2] == 'R')){
			lop = 300;
			break;
		}
	}

	if (lop != 300){
		return FALSE;
	}else {
		// end-x
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[3]);
		end_x += buf * 4096;
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[4]);
		end_x += buf * 256;
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[5]);
		end_x += buf * 16;
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[6]);
		end_x += buf;
		
		Read_Pos.Old_Pos_End.x = end_x;


		// end-y
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[7]);
		end_y += buf * 4096;
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[8]);
		end_y += buf * 256;
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[9]);
		end_y += buf * 16;
		buf = Ascii2Hex(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[10]);
		end_y += buf;
		
		Read_Pos.Old_Pos_End.y = end_y;

		return TRUE;
	}
}



BOOL CMicom_Optic::WRITE_START(int SetStart_X, int SetStart_Y)
{
	int lop = 0;
	BYTE Buf[8];

	int xbuf_Set = (SetStart_X/2)*2;
	int ybuf_Set = (SetStart_Y/2)*2;

	int xend_Set = xbuf_Set + 1287;
	int yend_Set = ybuf_Set + 727;

	memset(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam,0,sizeof(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam));

	BYTE DATA[20] ={'!','1','W','0','0','0','0','0','0','0','0','@'};

	Buf[0] = ((xbuf_Set>>12)&0x000f);
	Buf[1] = ((xbuf_Set>>8)&0x000f);
	Buf[2] = ((xbuf_Set>>4)&0x000f);
	Buf[3] = ((xbuf_Set)&0x000f);
	Buf[4] = ((ybuf_Set>>12)&0x000f);
	Buf[5] = ((ybuf_Set>>8)&0x000f);
	Buf[6] = ((ybuf_Set>>4)&0x000f);
	Buf[7] = ((ybuf_Set)&0x000f);

	DATA[3] = Hex2Ascii(Buf[0]);
	DATA[4] = Hex2Ascii(Buf[1]);
	DATA[5] = Hex2Ascii(Buf[2]);
	DATA[6] = Hex2Ascii(Buf[3]);
	DATA[7] = Hex2Ascii(Buf[4]);
	DATA[8] = Hex2Ascii(Buf[5]);
	DATA[9] = Hex2Ascii(Buf[6]);
	DATA[10] = Hex2Ascii(Buf[7]);
	((CImageTesterDlg  *)m_pMomWnd)->SendData8Byte_Cam(DATA, 12);

	for(lop = 0;lop<100;lop++){//500ms체크 
		DoEvents(10);
		if((((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[1] == '1')&&(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[2] == 'W')){
			lop = 300;
			break;
		}
	}
	
	if(lop != 300){
		return FALSE;	
	}

	DATA[1] = '2';

	Buf[0] = ((xend_Set>>12)&0x000f);
	Buf[1] = ((xend_Set>>8)&0x000f);
	Buf[2] = ((xend_Set>>4)&0x000f);
	Buf[3] = ((xend_Set)&0x000f);
	Buf[4] = ((yend_Set>>12)&0x000f);
	Buf[5] = ((yend_Set>>8)&0x000f);
	Buf[6] = ((yend_Set>>4)&0x000f);
	Buf[7] = ((yend_Set)&0x000f);

	DATA[3] = Hex2Ascii(Buf[0]);
	DATA[4] = Hex2Ascii(Buf[1]);
	DATA[5] = Hex2Ascii(Buf[2]);
	DATA[6] = Hex2Ascii(Buf[3]);
	DATA[7] = Hex2Ascii(Buf[4]);
	DATA[8] = Hex2Ascii(Buf[5]);
	DATA[9] = Hex2Ascii(Buf[6]);
	DATA[10] = Hex2Ascii(Buf[7]);

	((CImageTesterDlg  *)m_pMomWnd)->SendData8Byte_Cam(DATA, 12);

	for(lop = 0;lop<100;lop++){//500ms체크 
		DoEvents(10);
		if((((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[1] == '2')&&(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[2] == 'W')){
			lop = 300;
			break;
		}
	}

	if(lop != 300){
		return FALSE;	
	}else{
		return TRUE;
	}



}


BOOL CMicom_Optic::WRITE_START_XY(int SetStart_X, int SetStart_Y)
{
	int lop = 0;
	BYTE Buf[8];

	int xbuf_Set = (SetStart_X/2)*2;
	int ybuf_Set = (SetStart_Y/2)*2;

	memset(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam,0,sizeof(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam));

	// 1 r
	BYTE DATA[20] ={'!','1','W','0','0','0','0','0','0','0','0','@'};

	Buf[0] = ((xbuf_Set>>12)&0x000f);
	Buf[1] = ((xbuf_Set>>8)&0x000f);
	Buf[2] = ((xbuf_Set>>4)&0x000f);
	Buf[3] = ((xbuf_Set)&0x000f);
	Buf[4] = ((ybuf_Set>>12)&0x000f);
	Buf[5] = ((ybuf_Set>>8)&0x000f);
	Buf[6] = ((ybuf_Set>>4)&0x000f);
	Buf[7] = ((ybuf_Set)&0x000f);

	DATA[3] = Hex2Ascii(Buf[0]);
	DATA[4] = Hex2Ascii(Buf[1]);
	DATA[5] = Hex2Ascii(Buf[2]);
	DATA[6] = Hex2Ascii(Buf[3]);
	DATA[7] = Hex2Ascii(Buf[4]);
	DATA[8] = Hex2Ascii(Buf[5]);
	DATA[9] = Hex2Ascii(Buf[6]);
	DATA[10] = Hex2Ascii(Buf[7]);
	((CImageTesterDlg  *)m_pMomWnd)->SendData8Byte_Cam(DATA, 12);

	for(lop = 0;lop<100;lop++){//500ms체크 
		DoEvents(10);
		if((((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[1] == '1')&&(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[2] == 'W')){
			lop = 300;
			break;
		}
	}
	
	if(lop != 300){
		return FALSE;	
	}else{
		return TRUE;
	}
}


BOOL CMicom_Optic::WRITE_END_XY(int SetEnd_X, int SetEnd_Y)
{
	int lop = 0;
	BYTE Buf[8];

	int xbuf_Set = ((SetEnd_X/2)*2)+1;
	int ybuf_Set = ((SetEnd_Y/2)*2)+1;

	memset(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam,0,sizeof(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam));

	BYTE DATA[20] ={'!','2','W','0','0','0','0','0','0','0','0','@'};

	Buf[0] = ((xbuf_Set>>12)&0x000f);
	Buf[1] = ((xbuf_Set>>8)&0x000f);
	Buf[2] = ((xbuf_Set>>4)&0x000f);
	Buf[3] = ((xbuf_Set)&0x000f);
	Buf[4] = ((ybuf_Set>>12)&0x000f);
	Buf[5] = ((ybuf_Set>>8)&0x000f);
	Buf[6] = ((ybuf_Set>>4)&0x000f);
	Buf[7] = ((ybuf_Set)&0x000f);

	DATA[3] = Hex2Ascii(Buf[0]);
	DATA[4] = Hex2Ascii(Buf[1]);
	DATA[5] = Hex2Ascii(Buf[2]);
	DATA[6] = Hex2Ascii(Buf[3]);
	DATA[7] = Hex2Ascii(Buf[4]);
	DATA[8] = Hex2Ascii(Buf[5]);
	DATA[9] = Hex2Ascii(Buf[6]);
	DATA[10] = Hex2Ascii(Buf[7]);
	((CImageTesterDlg  *)m_pMomWnd)->SendData8Byte_Cam(DATA, 12);

	for(lop = 0;lop<100;lop++){//500ms체크 
		DoEvents(10);
		if((((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[1] == '2')&&(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[2] == 'W')){
			lop = 300;
			break;
		}
	}

	if(lop != 300){
		return FALSE;	
	}else{
		return TRUE;
	}
}

BOOL CMicom_Optic::WRITE_Stand(void)
{
	int lop = 0;
	memset(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam,0,sizeof(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam));

	BYTE DATA[20] ={'!','S','0','0','0','0','0','0','0','0','0','@'};

	((CImageTesterDlg  *)m_pMomWnd)->SendData8Byte_Cam(DATA, 12);

	for(lop = 0;lop<100;lop++){//500ms체크 
		DoEvents(10);
		if(((CImageTesterDlg  *)m_pMomWnd)->m_SerialInput_Cam[1] == 'S'){
			lop = 300;
			break;
		}
	}

	if(lop != 300){
		return FALSE;	
	}else{
		DoEvents(1000);
		return TRUE;
	}
}



void CMicom_Optic::Pwr_Rst()
{
	
	((CImageTesterDlg  *)m_pMomWnd)->Power_Off();
//	DoEvents(500);
	((CImageTesterDlg  *)m_pMomWnd)->Power_On();
//	DoEvents(500);
}


void CMicom_Optic::LoadMicomPrmSet(tSetPoint *buf_pos,CString Filename)
{
	CFileFind filefind;	
	CString str = "";
	CString fnstr = "";
	int bufdata = 0;
	
	/////////////////////////////////////////////////////////////////////////
	bufdata = GetPrivateProfileInt("OPTIC_DEFAULT","Start_X",-1,Filename);
	if(bufdata != -1){
		buf_pos->Default_Pos_Start.x = bufdata;
	}else{
		buf_pos->Default_Pos_Start.x =18;
		str.Empty();
		str.Format("%d",buf_pos->Default_Pos_Start.x);
		WritePrivateProfileString("OPTIC_DEFAULT","Start_X",str,Filename);
	}
	buf_pos->Old_Pos_Start.x = buf_pos->Default_Pos_Start.x;
	buf_pos->Set_Pos_Start.x = buf_pos->Default_Pos_Start.x;


	bufdata = GetPrivateProfileInt("OPTIC_DEFAULT","Start_Y",-1,Filename);
	if(bufdata != -1){
		buf_pos->Default_Pos_Start.y = bufdata;
	}else{
		buf_pos->Default_Pos_Start.y = 64;

		str.Empty();
		str.Format("%d",buf_pos->Default_Pos_Start.y);
		WritePrivateProfileString("OPTIC_DEFAULT","Start_Y",str,Filename);
	}

	buf_pos->Old_Pos_Start.y = buf_pos->Default_Pos_Start.y;
	buf_pos->Set_Pos_Start.y = buf_pos->Default_Pos_Start.y;


	bufdata = GetPrivateProfileInt("OPTIC_DEFAULT","End_X",-1,Filename);
	if(bufdata != -1){
		buf_pos->Default_Pos_End.x = bufdata;
	}else{
		buf_pos->Default_Pos_End.x = buf_pos->Default_Pos_Start.x + 1287;
		str.Empty();
		str.Format("%d",buf_pos->Default_Pos_End.x);
		WritePrivateProfileString("OPTIC_DEFAULT","End_X",str,Filename);
	}

	buf_pos->Old_Pos_End.x = buf_pos->Default_Pos_End.x;
	buf_pos->Set_Pos_End.x = buf_pos->Default_Pos_End.x;

	bufdata = GetPrivateProfileInt("OPTIC_DEFAULT","End_Y",-1,Filename);
	if(bufdata != -1){
		buf_pos->Default_Pos_End.y = bufdata;
	}else{
		buf_pos->Default_Pos_End.y= buf_pos->Default_Pos_Start.y + 727;
		str.Empty();
		str.Format("%d",buf_pos->Default_Pos_End.y);
		WritePrivateProfileString("OPTIC_DEFAULT","End_Y",str,Filename);
	}

	buf_pos->Old_Pos_End.y = buf_pos->Default_Pos_End.y;
	buf_pos->Set_Pos_End.y = buf_pos->Default_Pos_End.y;

	/////////////////////////////////////////////////////////////////////////
	
	Update_List();

}


void CMicom_Optic::SaveMicomPrmSet(tSetPoint *buf_pos, CString Filename){
	
	CString str = "";

	str.Empty();
	str.Format("%d",buf_pos->Default_Pos_Start.x);
	WritePrivateProfileString("OPTIC_DEFAULT","Start_X",str,Filename);
	
	str.Empty();
	str.Format("%d",buf_pos->Default_Pos_Start.y);
	WritePrivateProfileString("OPTIC_DEFAULT","Start_Y",str,Filename);

	str.Empty();
	str.Format("%d",buf_pos->Default_Pos_End.x);
	WritePrivateProfileString("OPTIC_DEFAULT","End_X",str,Filename);
	
	str.Empty();
	str.Format("%d",buf_pos->Default_Pos_End.y);
	WritePrivateProfileString("OPTIC_DEFAULT","End_Y",str,Filename);

	
	Update_List();
	
}

bool CMicom_Optic::Read_Func(){
	bool FLAG = FALSE;

	int count =0;
	FLAG = TRUE;
	if(!READ_START_XY()){
		FLAG = FALSE;
	}
	
	if(FLAG == FALSE){
		RUNSTATE_TEXT("OFFSET READ FAIL");
		//Pwr_Rst();
		DoEvents(1000);
		return FALSE;
	}

	if(!READ_END_XY()){
		FLAG = FALSE;
	}

	if(FLAG == FALSE){
		RUNSTATE_TEXT("OFFSET READ FAIL");
		//Pwr_Rst();
		//AfxMessageBox("Read 실패");
		
		DoEvents(1000);
		return FALSE;
	}
	
	
	return TRUE;
}

void CMicom_Optic::Set_List()
{
	Setting_Poslist.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	Setting_Poslist.ModifyStyle(LVS_OWNERDRAWFIXED, 0, 0);
	Setting_Poslist.InsertColumn(0,"Position",LVCFMT_CENTER, 80);
	Setting_Poslist.InsertColumn(1,"Start X",LVCFMT_CENTER, 65);
	Setting_Poslist.InsertColumn(2,"Start Y",LVCFMT_CENTER, 65);
	Setting_Poslist.InsertColumn(3,"End X",LVCFMT_CENTER, 65);
	Setting_Poslist.InsertColumn(4,"End Y",LVCFMT_CENTER, 65);
	
}

void CMicom_Optic::Upload_List()
{
	CString str = "";
	Setting_Poslist.DeleteAllItems();

	for(int i=0; i<3; i++){
		InIndex = Setting_Poslist.InsertItem(i,"",0);

		if(i==0){
			str.Empty();
			str = "Set";

			Setting_Poslist.SetItemText(InIndex,0,str);
			
			str.Empty();
			str.Format("%d",Read_Pos.Set_Pos_Start.x);
			Setting_Poslist.SetItemText(InIndex, 1, str);

			str.Empty();
			str.Format("%d",Read_Pos.Set_Pos_Start.y);
			Setting_Poslist.SetItemText(InIndex, 2, str);

			str.Empty();
			str.Format("%d",Read_Pos.Set_Pos_End.x);
			Setting_Poslist.SetItemText(InIndex, 3, str);

			str.Empty();
			str.Format("%d",Read_Pos.Set_Pos_End.y);
			Setting_Poslist.SetItemText(InIndex, 4, str);
		}
		if(i==1){
			str.Empty();
			str = "Old";

			Setting_Poslist.SetItemText(InIndex,0,str);
			
			str.Empty();
			str.Format("%d",Read_Pos.Old_Pos_Start.x);
			Setting_Poslist.SetItemText(InIndex, 1, str);

			str.Empty();
			str.Format("%d",Read_Pos.Old_Pos_Start.y);
			Setting_Poslist.SetItemText(InIndex, 2, str);

			str.Empty();
			str.Format("%d",Read_Pos.Old_Pos_End.x);
			Setting_Poslist.SetItemText(InIndex, 3, str);

			str.Empty();
			str.Format("%d",Read_Pos.Old_Pos_End.y);
			Setting_Poslist.SetItemText(InIndex, 4, str);
		}
		
		if(i==2){
			str.Empty();
			str = "Default";
			Setting_Poslist.SetItemText(InIndex,0,str);
			
			str.Empty();
			str.Format("%d",Read_Pos.Default_Pos_Start.x);
			Setting_Poslist.SetItemText(InIndex, 1, str);

			str.Empty();
			str.Format("%d",Read_Pos.Default_Pos_Start.y);
			Setting_Poslist.SetItemText(InIndex, 2, str);

			str.Empty();
			str.Format("%d",Read_Pos.Default_Pos_End.x);
			Setting_Poslist.SetItemText(InIndex, 3, str);

			str.Empty();
			str.Format("%d",Read_Pos.Default_Pos_End.y);
			Setting_Poslist.SetItemText(InIndex, 4, str);
		}
	}
}

void CMicom_Optic::Update_List()
{
	str_def_startX.Format("%d",Read_Pos.Default_Pos_Start.x);
	str_def_startY.Format("%d",Read_Pos.Default_Pos_Start.y);
	str_def_endX.Format("%d",Read_Pos.Default_Pos_End.x);
	str_def_endY.Format("%d",Read_Pos.Default_Pos_End.y);

	UpdateData(FALSE);

	Upload_List();
}

void CMicom_Optic::OnBnClickedDefaultPosSave()
{
	int		startX = atoi(str_def_startX);
	int		startY = atoi(str_def_startY);
	int		endX = atoi(str_def_endX);
	int		endY = atoi(str_def_endY);

	m_def_startX = (startX/2)*2;
	m_def_startY = (startY/2)*2;
	m_def_endX = m_def_startX + 1287;
	m_def_endY = m_def_startY + 727;

	Read_Pos.Default_Pos_Start.x = m_def_startX;
	Read_Pos.Default_Pos_Start.y = m_def_startY;
	Read_Pos.Default_Pos_End.x= m_def_endX;
	Read_Pos.Default_Pos_End.y = m_def_endY;

	str_def_startX.Format("%d",Read_Pos.Default_Pos_Start.x);
	str_def_startY.Format("%d",Read_Pos.Default_Pos_Start.y);
	str_def_endX.Format("%d",Read_Pos.Default_Pos_End.x);
	str_def_endY.Format("%d",Read_Pos.Default_Pos_End.y);

	UpdateData(FALSE);

	SaveMicomPrmSet(&Read_Pos,((CImageTesterDlg  *)m_pMomWnd)->Micom_Path);

	Update_List();



}

void CMicom_Optic::OnEnChangeDefaultStartX()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	if(atoi(str_def_startX) < 0){
		str_def_startX.Format("0");
	}else if(atoi(str_def_startX) > 56){
		str_def_startX.Format("56");
	}

	if(atoi(str_def_startY) < 0){
		str_def_startY.Format("0");
	}else if(atoi(str_def_startY) > 120){
		str_def_startY.Format("120");;
	}

	int endx = atoi(str_def_startX) + 1287;
	int endy = atoi(str_def_startY) + 727;
	str_def_endX.Format("%d",endx); 
	str_def_endY.Format("%d",endy);
	UpdateData(FALSE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMicom_Optic::OnEnChangeDefaultStartY()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	if(atoi(str_def_startX) < 0){
		str_def_startX.Format("0");
	}else if(atoi(str_def_startX) > 56){
		str_def_startX.Format("56");
	}

	if(atoi(str_def_startY) < 0){
		str_def_startY.Format("0");
	}else if(atoi(str_def_startY) > 120){
		str_def_startY.Format("120");;
	}

	int endx = atoi(str_def_startX) + 1287;
	int endy = atoi(str_def_startY) + 727;
	str_def_endX.Format("%d",endx); 
	str_def_endY.Format("%d",endy);
	UpdateData(FALSE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMicom_Optic::OnEnChangeDefaultEndX()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	if(atoi(str_def_startX) < 0){
		str_def_startX.Format("0");
	}else if(atoi(str_def_startX) > 56){
		str_def_startX.Format("56");
	}

	if(atoi(str_def_startY) < 0){
		str_def_startY.Format("0");
	}else if(atoi(str_def_startY) > 120){
		str_def_startY.Format("120");;
	}

	int endx = atoi(str_def_startX) + 1287;
	int endy = atoi(str_def_startY) + 727;
	str_def_endX.Format("%d",endx); 
	str_def_endY.Format("%d",endy);
	UpdateData(FALSE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMicom_Optic::OnEnChangeDefaultEndY()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	if(atoi(str_def_startX) < 0){
		str_def_startX.Format("0");
	}else if(atoi(str_def_startX) > 56){
		str_def_startX.Format("56");
	}

	if(atoi(str_def_startY) < 0){
		str_def_startY.Format("0");
	}else if(atoi(str_def_startY) > 120){
		str_def_startY.Format("120");;
	}

	int endx = atoi(str_def_startX) + 1287;
	int endy = atoi(str_def_startY) + 727;
	str_def_endX.Format("%d",endx); 
	str_def_endY.Format("%d",endy);
	UpdateData(FALSE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMicom_Optic::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

		if(nIDCtl == IDC_LIST_SETTING_POS)
     {
          lpMeasureItemStruct->itemHeight += 16;      //  - 연산 설정하면 높이가 줄어듭니다.
     }
	CDialog::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CMicom_Optic::OnBnClickedSettingPosWrite()
{
	RUNSTATE_TEXT("WRITING");
	SettingPosWrite();
}

BOOL CMicom_Optic::SettingPosWrite()
{
	int m_OffsetX = atoi(edit_offsetX);
	int m_OffsetY = atoi(edit_offsetY);

	edit_offsetX.Format("%d",m_OffsetX);
	edit_offsetY.Format("%d",m_OffsetY);
	UpdateData(FALSE);
	
	//((CImageTesterDlg  *)m_pMomWnd)->str_MainOffsetX.Format("%d",m_OffsetX);//str_MainOffsetX 변수는 메인 ui에 있는 에디트 컨트롤 offsetX를 선언한 변수
	//((CImageTesterDlg  *)m_pMomWnd)->str_MainOffsetY.Format("%d",m_OffsetY);//str_MainOffsetY 변수는 메인 ui에 있는 에디트 컨트롤 offsetY를 선언한 변수
	//((CImageTesterDlg  *)m_pMomWnd)->UpdateData(FALSE);/////////////////////이렇게 하면 메인 ui에 변수가 바뀐다.

	if(!Read_Func()){
		return FALSE;
	}

	Control_Pos = Read_Pos;
	///READ 에서 받아온 값.
	int StartX = Control_Pos.Old_Pos_Start.x + m_OffsetX;
	if(StartX < 0){
		StartX = 0;
	}else if(StartX > 56){
		StartX = 56;
	}
	
	int StartY = Control_Pos.Old_Pos_Start.y + m_OffsetY;
	if(StartY < 0){
		StartY = 0;
	}else if(StartY > 120){
		StartY = 120;
	}

	StartX = (StartX/2)*2;
	StartY = (StartY/2)*2;

	int EndX = StartX + 1287;
	int EndY = StartY + 727;

	Control_Pos.Set_Pos_Start.x = StartX;
	Control_Pos.Set_Pos_Start.y = StartY;
	Control_Pos.Set_Pos_End.x = EndX;
	Control_Pos.Set_Pos_End.y = EndY;

	if(!Write_Func()){
		return FALSE;
	}

	Read_Pos = Control_Pos;
	Update_List();

	RUNSTATE_TEXT("WRITING OK");

	return TRUE;
}

bool CMicom_Optic::Write_Func(){
	bool FLAG = FALSE;
	
	FLAG = TRUE;
	if(((CImageTesterDlg  *)m_pMomWnd)->Power_Rst() == FALSE){
		RUNSTATE_TEXT("POWER RST FAIL");
		return FALSE;
	}

	if(!WRITE_START(Control_Pos.Set_Pos_Start.x,Control_Pos.Set_Pos_Start.y)){
		FLAG = FALSE;
	}
	
	if(FLAG == FALSE){
		RUNSTATE_TEXT("OFFSET WRITE FAIL");
		//Pwr_Rst();
		//AfxMessageBox("Write 실패");
		//DoEvents(1000);
		return FALSE;
	}

	if(!WRITE_Stand()){
		FLAG = FALSE;
	}

	if(FLAG == FALSE){
		RUNSTATE_TEXT("OFFSET WRITE FAIL");
		//Pwr_Rst();
		//DoEvents(1000);
		return FALSE;
	}
	
	return TRUE;
}


void CMicom_Optic::OnBnClickedCheckEditing()
{
	if(b_edit == 1){
		
		((CEdit *)GetDlgItem(IDC_DEFAULT_START_X))->SetReadOnly(TRUE);
		((CEdit *)GetDlgItem(IDC_DEFAULT_START_Y))->SetReadOnly(TRUE);
		((CEdit *)GetDlgItem(IDC_DEFAULT_END_X))->SetReadOnly(TRUE);
		((CEdit *)GetDlgItem(IDC_DEFAULT_END_Y))->SetReadOnly(TRUE);
		((CButton *)GetDlgItem(IDC_DEFAULT_POS_SAVE))->EnableWindow(0);
		b_edit = 0;
	}else if(b_edit == 0){
		
		((CEdit *)GetDlgItem(IDC_DEFAULT_START_X))->SetReadOnly(FALSE);
		((CEdit *)GetDlgItem(IDC_DEFAULT_START_Y))->SetReadOnly(FALSE);
		((CEdit *)GetDlgItem(IDC_DEFAULT_END_X))->SetReadOnly(TRUE);
		((CEdit *)GetDlgItem(IDC_DEFAULT_END_Y))->SetReadOnly(TRUE);
		((CButton *)GetDlgItem(IDC_DEFAULT_POS_SAVE))->EnableWindow(1);
		b_edit = 1;
	}
	UpdateData(FALSE);
}

void CMicom_Optic::OnEnChangeEditOffsetX()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMicom_Optic::OnEnChangeEditOffsetY()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CMicom_Optic::OnBnClickedDefaultWrite()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
		// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	RUNSTATE_TEXT("DEFAULT WRITING");
	DefaultWrite();
}

BOOL CMicom_Optic::DefaultWrite()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
		// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	if(atoi(str_def_startX) < 0){
		str_def_startX.Format("0");
	}else if(atoi(str_def_startX) > 56){
		str_def_startX.Format("56");
	}

	if(atoi(str_def_startY) < 0){
		str_def_startY.Format("0");
	}else if(atoi(str_def_startY) > 120){
		str_def_startY.Format("120");;
	}

	int		startX = atoi(str_def_startX);
	int		startY = atoi(str_def_startY);
	int		endX = atoi(str_def_endX);
	int		endY = atoi(str_def_endY);

	m_def_startX = (startX/2)*2;
	m_def_startY = (startY/2)*2;
	m_def_endX = m_def_startX + 1287;
	m_def_endY = m_def_startY + 727;

	str_def_startX.Format("%d",m_def_startX);
	str_def_startY.Format("%d",m_def_startY);
	str_def_endX.Format("%d",m_def_endX); 
	str_def_endY.Format("%d",m_def_endY);
	
	Read_Pos.Default_Pos_Start.x = atoi(str_def_startX);
	Read_Pos.Default_Pos_Start.y = atoi(str_def_startY);
	Read_Pos.Default_Pos_End.x= atoi(str_def_endX);
	Read_Pos.Default_Pos_End.y = atoi(str_def_endY);

	Control_Pos.Set_Pos_Start.x = atoi(str_def_startX);
	Control_Pos.Set_Pos_Start.y = atoi(str_def_startY);
	Control_Pos.Set_Pos_End.x = atoi(str_def_endX);
	Control_Pos.Set_Pos_End.y = atoi(str_def_endY);

	UpdateData(FALSE);

	if(!Write_Func()){
		return FALSE;
	}

	if(!Read_Func()){
		return FALSE;
	}

	RUNSTATE_TEXT("DEFAULT WRITING OK");

	SaveMicomPrmSet(&Read_Pos,((CImageTesterDlg  *)m_pMomWnd)->Micom_Path);
	return TRUE;
}



void CMicom_Optic::OnBnClickedDefaultInitwr()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	RUNSTATE_TEXT("INIT WRITING");
	DefaultInitwr();
}

BOOL CMicom_Optic::DefaultInitwr()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	str_def_startX.Format("28");
	str_def_startY.Format("60");
	int endx = atoi(str_def_startX) + 1287;
	int endy = atoi(str_def_startY) + 727;
	str_def_endX.Format("%d",endx); 
	str_def_endY.Format("%d",endy);
	
	Read_Pos.Default_Pos_Start.x = atoi(str_def_startX);
	Read_Pos.Default_Pos_Start.y = atoi(str_def_startY);
	Read_Pos.Default_Pos_End.x= atoi(str_def_endX);
	Read_Pos.Default_Pos_End.y = atoi(str_def_endY);

	Control_Pos.Set_Pos_Start.x = atoi(str_def_startX);
	Control_Pos.Set_Pos_Start.y = atoi(str_def_startY);
	Control_Pos.Set_Pos_End.x = atoi(str_def_endX);
	Control_Pos.Set_Pos_End.y = atoi(str_def_endY);

	UpdateData(FALSE);

	if(!Write_Func()){
		return FALSE;
	}

	if(!Read_Func()){
		return FALSE;
	}

	SaveMicomPrmSet(&Read_Pos,((CImageTesterDlg  *)m_pMomWnd)->Micom_Path);

	return TRUE;
}

void CMicom_Optic::RUNSTATE_TEXT(LPCSTR lpcszString, ...)       
{	
	((CEdit *)GetDlgItem(IDC_EDIT_VALUE_RUNSTATE))->SetWindowText(lpcszString);
}

void CMicom_Optic::MODE_TEXT(LPCSTR lpcszString, ...)       
{	
	((CEdit *)GetDlgItem(IDC_EDIT_MODE_NAME))->SetWindowText(lpcszString);
}




HBRUSH CMicom_Optic::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_MODE_NAME){
		pDC->SetTextColor(RGB(255, 255, 255));
		// 텍스트의 배경색상을 설정한다.
		pDC->SetBkColor(RGB(86,86,86));
		// 바탕색상을 설정한다.
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_RUNSTATE){
		pDC->SetTextColor(RGB(86,86,86));
		// 텍스트의 배경색상을 설정한다.
		pDC->SetBkColor(RGB(255, 255, 255));
		// 바탕색상을 설정한다.
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CMicom_Optic::Focus_move_start()
{
	if(((CButton *)GetDlgItem(IDC_SETTING_POS_READ))->IsWindowEnabled() == TRUE){
		GotoDlgCtrl(((CButton *)GetDlgItem(IDC_SETTING_POS_READ)));
	}else{
		GotoDlgCtrl(((CStatic *)GetDlgItem(IDC_STATIC)));
	}
}

void CMicom_Optic::Focus_move_startDefaultX()
{
	if(b_edit == 0){
		GotoDlgCtrl(((CButton *)GetDlgItem(IDC_SETTING_POS_READ)));
	}else{
		GotoDlgCtrl(((CStatic *)GetDlgItem(IDC_DEFAULT_START_X)));
	}
}

void CMicom_Optic::Focus_move_startDefaultY()
{
	if(b_edit == 0){
		GotoDlgCtrl(((CButton *)GetDlgItem(IDC_SETTING_POS_READ)));
	}else{
		GotoDlgCtrl(((CStatic *)GetDlgItem(IDC_DEFAULT_START_Y)));
	}
}


void CMicom_Optic::OnEnSetfocusEditModeName()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
}

void CMicom_Optic::OnEnSetfocusDefaultStartX()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_startDefaultX();
}

void CMicom_Optic::OnEnSetfocusDefaultStartY()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_startDefaultY();
}

void CMicom_Optic::OnEnSetfocusDefaultEndX()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
}

void CMicom_Optic::OnEnSetfocusDefaultEndY()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
}

void CMicom_Optic::OnEnSetfocusEditValueRunstate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
}
