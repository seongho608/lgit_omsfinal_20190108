#pragma once
#include "resource.h"

// CModSelDlg 대화 상자입니다.

class CModSelDlg : public CDialog
{
	DECLARE_DYNAMIC(CModSelDlg)

public:
	CModSelDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CModSelDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MODELSEL_DIALOG };

	void	Setup(CWnd* IN_pMomWnd);
	tINFO	m_ModeSrcInfo[20];
	tINFO	m_ModeDstInfo[20];
	void	InitDstSet(void);
	void	DstSet(void);
	bool	ModelSettingChk();
	bool	b_fullselct;
	void	InitEVMS();
private:
	CWnd	*m_pMomWnd;
	int		SRCItemIndex;
	int		DSTItemIndex;
	int		m_SListCnt;
	int		m_DListCnt;
	void	InitSrcSet(void);
	void	InitSrcData(void);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButtonSelsave();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnNMClickListSrc(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListSrc(NMHDR *pNMHDR, LRESULT *pResult);
};
