#pragma once
#include "ETCUSER.H"
#include "afxcmn.h"

// CDTC_Option 대화 상자입니다.

class CDTC_Option : public CDialog
{
	DECLARE_DYNAMIC(CDTC_Option)

public:
	CDTC_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDTC_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_DTC };

	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);

	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	CFont	ft,ft2;

	tResultVal Run(void);
	void	Pic(CDC *cdc);
	bool	DTCPic(CDC *cdc);
	void	InitPrm();
	void	Save(int NUM);
	void	initstat();

	int Lot_InsertNum;

	BOOL	m_Etc_State;
	
	CString ModeName;
	BOOL	b_DTCMODE;
	BOOL	b_AutoErase;
	BOOL	b_ManualMode;
	CComboBox	*pComboMode;

	void	LOT_Set_List(CListCtrl *List);
	void	Set_List(CListCtrl *List);
	void	EXCEL_SAVE();
	void	LOT_EXCEL_SAVE();
	bool	EXCEL_UPLOAD();
	bool	LOT_EXCEL_UPLOAD();
	void	InsertList();
	int		InsertIndex;
	int		ListItemNum;

	void	LOT_InsertDataList();
	void	Load_parameter();
	void	Save_parameter();	

	BOOL	b_StopFail;
	void	FAIL_UPLOAD();
	int		StartCnt;
	int		Lot_StartCnt;

	void	InitEVMS();
	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();

	CString str_DTC[4];	
	BOOL	DTC_READ();
	BOOL	DTC_ERASE();

	COLORREF st_col,Valcol[4];

	void	DTC_RESULT(int num,int col,LPCSTR lpcszString, ...);
	void	DTC_STATE(int col,LPCSTR lpcszString, ...);
	void	DTC_RESULT_STATE(int col,LPCSTR lpcszString, ...);
	void	DTC_MODE_STATE(LPCSTR lpcszString, ...);
private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
	CString		strTitle;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnDtcread();
	afx_msg void OnBnClickedBtnDtcerase();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CListCtrl m_DTCList;
//	afx_msg void OnBnClickedCheckManualmode();
	afx_msg void OnCbnSelchangeComboDtcmode();
	afx_msg void OnBnClickedBtnDtcsave();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CListCtrl m_Lot_DTCList;
};
