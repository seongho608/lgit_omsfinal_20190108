#pragma once


// COverlayDetect_Option 대화 상자입니다.
#include "ETCUSER.H"
#include "afxcmn.h"

struct OverlayData{
	BOOL	enable;

	CString OverlayMasterImagePath;
	int OverlayCheckCnt;
	int checkAnlge[5];
	int Overlay_start_x, Overlay_start_y;
	int Overlay_end_x, Overlay_end_y;
	bool OverlayCheckResult[50];
	CString testName[50];

	CString WarningMasterImagePath;
	int WarningCheckCnt;
	int CheckWarning[5];
	int Warning_start_x, Warning_start_y;
	int Warning_end_x, Warning_end_y;
	bool WarningCheckResult[50];

	double templateRatio[50];
};

class COverlayDetect_Option : public CDialog
{
	DECLARE_DYNAMIC(COverlayDetect_Option)

public:
	COverlayDetect_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COverlayDetect_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_OVERLAY_CAN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:

	int Lot_StartCnt;
	tINFO		m_INFO;
	CWnd		*m_pMomWnd;	
	CEdit		*pTStat;
	CString		str_model;
	CString		str_ModelPath;
	CString		strSelectedModelPath;

	void	InitPrm();
	int		Old_ModelCode;
	void	WModeSetup();
	int		m_WModeNum;
	int		TEST_LIST_COUNT;
	CString TESTNAME[40];

	CEdit	*pSubDlgEdit[5];

	void Setup_List();
	int	m_number;
	int m_dOverlayStart_x, m_dOverlayStart_y;
	int m_dOverlayEnd_x, m_dOverlayEnd_y;
	
	CComboBox	*pWModeCombo;
	void Save_parameter();
	void Load_parameter();
	
	int m_ShowMode;
	int	m_dMatchingRatio;
	void GetOverlayImage(int angle);
	void GetOverlayImage(CString str_angle);
	
	void EtcCamInit(int Model);

	OverlayData m_sOverlayData;

	void	Pic(CDC *cdc);
	tResultVal	Run();

	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();
	
	//EVMS 적용
	void	InitEVMS();

	bool	CompareOverlay(LPBYTE IN_RGBIn_Target, IplImage *master_image, int start_x, int start_y, int end_x, int end_y, double threshold, OverlayData *sOverlayData, int Cnt);
	bool	CompareOverlay(LPBYTE IN_RGBIn_Target, IplImage *master_image,OverlayData *sOverlayData, int Cnt);


	bool Run(int ch, LPBYTE IN_RGBIn, OverlayData *sOverlayData);

	void Setup(CWnd* IN_pMomWnd);
	void Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);

	BOOL	Overlay_Etc_Compare(int num);
	CStatic m_stOverlayFrame;

	void UpateMasterFileList();

	CListBox m_lsOverlayList;
	afx_msg void OnLbnSelchangeListOverlay();
	CListBox m_lsOverlayCheckList;

	void ShowOverlayImage(IplImage *srcImage);
	
	afx_msg void OnBnClickedBtnDeleteoverlaymasterlist();
	afx_msg void OnBnClickedBtnMasterImgAdd();
	afx_msg void OnBnClickedBtnOverlaySave();
	afx_msg void OnBnClickedBtnSetArea();
	virtual BOOL OnInitDialog();
	
	int m_dTemplateRatio[4][50];
	bool m_dResult[4][50];
	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void LOT_Set_List(CListCtrl *List);

	int	Lot_InsertNum;
	void InsertList();
	int InsertIndex;
	int ListItemNum;
	void SETTING_DATA();

	CString TESTLOT[100];
	void EXCEL_SAVE();
	void LOT_EXCEL_SAVE();
	bool EXCEL_UPLOAD();
	bool LOT_EXCEL_UPLOAD();
	BOOL	b_StopFail;
	void FAIL_UPLOAD();
	int StartCnt;
	void	LOT_InsertDataList();
	
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CListCtrl m_OverlayWorkList;
	afx_msg void OnBnClickedBtnMasterImgAutoExt();
	afx_msg void OnEnChangeEditMasteroverlayAngle();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	CListCtrl m_Lot_OverlayWorkList;
};
