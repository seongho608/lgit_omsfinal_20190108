// Angle_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Angle_Option.h"
//
//#include
//#include <afxdb.h>


// CAngle_Option 대화 상자입니다.
extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
extern WORD	m_GRAYScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];

IMPLEMENT_DYNAMIC(CAngle_Option, CDialog)

CAngle_Option::CAngle_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CAngle_Option::IDD, pParent)
	, A_Total_PosX(0)
	, A_Total_PosY(0)
	, A_Dis_Width(0)
	, A_Dis_Height(0)
	, A_Total_Width(0)
	, A_Total_Height(0)
	, A_Thresold(0)
	, TestModALL(FALSE)
	, TestModVer(FALSE)
	, TestModHor(FALSE)
	, AngleKeepRun(FALSE)
{
	iSavedItem=0;
	iSavedSubitem=0;
	iSavedSubitem2=0;
	state=0;
	EnterState=0;
	MasterMod =FALSE;
	NewItem =0;NewSubitem=0;
	disL=0;
	disR=0;
	disT=0;
	disB=0;
	dgrL = 0; dgrR = 0; dgrT = 0; dgrB = 0;
	TestMod=0;
	AutomationMod=0;
	ChangeCheck=0;
	WheelCheck=0;
	b_StopFail = FALSE; 
	ResultDegree_H = 0;
	ResultDegree_V = 0;
	for(int t=0; t<5; t++){
		ChangeItem[t]=-1;
	}
	changecount=0;
	StartCnt = 0;
	//****************Side Circle 기준 탐색 점 설정*******************//
	// 170205 HTH -> 고정 좌표에서 Rect값 좌표 받아서 검출로 수정
// 	Standard_SC_RECT[0].m_resultX = A_RECT[0].m_PosX;	//중앙 Side circle
// 	Standard_SC_RECT[0].m_resultY = A_RECT[0].m_PosY;
// 	Standard_SC_RECT[1].m_resultX = A_RECT[1].m_PosX;	//왼쪽 Side circle
// 	Standard_SC_RECT[1].m_resultY = A_RECT[1].m_PosY;
// 	Standard_SC_RECT[2].m_resultX = A_RECT[2].m_PosX;	//오른쪽 Side circle
// 	Standard_SC_RECT[2].m_resultY = A_RECT[2].m_PosY;
// 	Standard_SC_RECT[3].m_resultX = A_RECT[3].m_PosX;	//위쪽 Side circle
// 	Standard_SC_RECT[3].m_resultY = A_RECT[3].m_PosY;
// 	Standard_SC_RECT[4].m_resultX = A_RECT[4].m_PosX;	//아래쪽 Side circle
// 	Standard_SC_RECT[4].m_resultY = A_RECT[4].m_PosY;
//****************************************************************//

}

CAngle_Option::~CAngle_Option()
{
//	KillTimer(110);
}

void CAngle_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_Angle, A_DATALIST);
	DDX_Text(pDX, IDC_Angle_PosX, A_Total_PosX);
	DDX_Text(pDX, IDC_Angle_PosY, A_Total_PosY);
	DDX_Text(pDX, IDC_Angle_Dis_W, A_Dis_Width);
	DDX_Text(pDX, IDC_Angle_Dis_H, A_Dis_Height);
	DDX_Text(pDX, IDC_Angle_Width, A_Total_Width);
	DDX_Text(pDX, IDC_Angle_Height, A_Total_Height);
	DDX_Text(pDX, IDC_Angle_Thresold, A_Thresold);
	DDX_Control(pDX, IDC_LIST_ANGLELIST, m_AngleList);
	DDX_Check(pDX, IDC_CHECK1, TestModALL);
	DDX_Check(pDX, IDC_CHECK_Ver, TestModVer);
	DDX_Check(pDX, IDC_CHECK_Hor, TestModHor);
	DDX_Check(pDX, IDC_CHECK_Angle_Keeprun, AngleKeepRun);
	DDX_Control(pDX, IDC_CHECK_Angle_Keeprun, btn_AngleKeepRun);
	DDX_Control(pDX, IDC_LIST_ANGLELIST_LOT, m_Lot_AngleList);
}


BEGIN_MESSAGE_MAP(CAngle_Option, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_setAngleZone, &CAngle_Option::OnBnClickedButtonsetanglezone)
	ON_BN_CLICKED(IDC_BUTTON_A_RECT_SAVE, &CAngle_Option::OnBnClickedButtonARectSave)
	ON_NOTIFY(NM_CLICK, IDC_LIST_Angle, &CAngle_Option::OnNMClickListAngle)
//	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_MASTER_A, &CAngle_Option::OnBnClickedButtonMasterA)
	ON_BN_CLICKED(IDC_CHECK_Ver, &CAngle_Option::OnBnClickedCheckVer)
	ON_BN_CLICKED(IDC_CHECK_Hor, &CAngle_Option::OnBnClickedCheckHor)
	ON_BN_CLICKED(IDC_CHECK_AUTO, &CAngle_Option::OnBnClickedCheckAuto)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_Angle, &CAngle_Option::OnNMCustomdrawListAngle)
//	ON_WM_SETFOCUS()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_Angle, &CAngle_Option::OnNMDblclkListAngle)
	ON_EN_KILLFOCUS(IDC_EDIT_A_MOD, &CAngle_Option::OnEnKillfocusEditAMod)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_A_Load, &CAngle_Option::OnBnClickedButtonALoad)
	ON_BN_CLICKED(IDC_CHECK1, &CAngle_Option::OnBnClickedCheck1)
//	ON_BN_CLICKED(IDC_CHECK_Angle_Keeprun, &CAngle_Option::OnBnClickedCheckAngleKeeprun)
	ON_WM_DESTROY()
	ON_WM_KILLFOCUS()
	ON_WM_MOUSEWHEEL()
	ON_EN_CHANGE(IDC_Angle_PosX, &CAngle_Option::OnEnChangeAnglePosx)
	ON_EN_CHANGE(IDC_Angle_PosY, &CAngle_Option::OnEnChangeAnglePosy)
	ON_EN_CHANGE(IDC_Angle_Dis_W, &CAngle_Option::OnEnChangeAngleDisW)
	ON_EN_CHANGE(IDC_Angle_Dis_H, &CAngle_Option::OnEnChangeAngleDisH)
	ON_EN_CHANGE(IDC_Angle_Width, &CAngle_Option::OnEnChangeAngleWidth)
	ON_EN_CHANGE(IDC_Angle_Height, &CAngle_Option::OnEnChangeAngleHeight)
	ON_EN_CHANGE(IDC_Angle_Thresold, &CAngle_Option::OnEnChangeAngleThresold)
	ON_EN_CHANGE(IDC_EDIT_A_MOD, &CAngle_Option::OnEnChangeEditAMod)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_Angle, &CAngle_Option::OnLvnItemchangedListAngle)
	ON_BN_CLICKED(IDC_BTN_ETC_SAVE, &CAngle_Option::OnBnClickedBtnEtcSave)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_H, &CAngle_Option::OnBnClickedButtonSaveH)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_V, &CAngle_Option::OnBnClickedButtonSaveV)
END_MESSAGE_MAP()


// CAngle_Option 메시지 처리기입니다.

void CAngle_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CAngle_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO      = INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
}
// CAngle_Option 메시지 처리기입니다.

BOOL CAngle_Option::OnInitDialog()
{
	

	CDialog::OnInitDialog();
	((CEdit *) GetDlgItem(IDC_EDIT_A_MOD))->ShowWindow(FALSE);
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	SETLIST();

	A_filename=((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	
	Load_parameter();
	UpdateData(FALSE);

	UploadList();

//	SetTimer(110,100,NULL);
	Set_List(&m_AngleList);
	LOT_Set_List(&m_Lot_AngleList);
//	UploadList();
	if(TestMod ==0){
		TestModALL=1;
		TestModHor=1;
		TestModVer=1;
	}
	else if(TestMod ==1){
		TestModALL=0;
		TestModHor=1;
		TestModVer=0;
	}
	else if(TestMod ==2){
		TestModALL=0;
		TestModHor=0;
		TestModVer=1;
	}

	UpdateData(FALSE);
	if(AutomationMod == 1){
		AutomationMod =0;	
		OnBnClickedCheckAuto();
		CheckDlgButton(IDC_CHECK_AUTO,TRUE);
	}

//	((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	((CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE))->EnableWindow(0);
	

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CAngle_Option::SETLIST(){
	A_DATALIST.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	A_DATALIST.InsertColumn(0,"Zone",LVCFMT_CENTER, 90);
	A_DATALIST.InsertColumn(1,"PosX",LVCFMT_CENTER, 60);
	A_DATALIST.InsertColumn(2,"PosY",LVCFMT_CENTER, 60);
	A_DATALIST.InsertColumn(3,"START X",LVCFMT_CENTER, 60);
	A_DATALIST.InsertColumn(4,"START Y",LVCFMT_CENTER, 60);
	A_DATALIST.InsertColumn(5,"EXIT X",LVCFMT_CENTER, 60);
	A_DATALIST.InsertColumn(6,"EXIT Y",LVCFMT_CENTER, 60);
	A_DATALIST.InsertColumn(7,"Width",LVCFMT_CENTER, 50);
	A_DATALIST.InsertColumn(8,"Height",LVCFMT_CENTER, 50);
// 	A_DATALIST.InsertColumn(9,"±º합격편차",LVCFMT_CENTER, 60);
// 	A_DATALIST.InsertColumn(10, "º", LVCFMT_CENTER, 60);
	//A_DATALIST.InsertColumn(9, "±Pixel합격편차", LVCFMT_CENTER, 60);
	//A_DATALIST.InsertColumn(10,"Distance",LVCFMT_CENTER, 60);

	

}
void CAngle_Option::OnBnClickedButtonsetanglezone()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeCheck=1;
	UpdateData(TRUE);

	if(AutomationMod ==0){
		PosX=A_Total_PosX;
		PosY=A_Total_PosY;
		
		A_RECT[0].m_PosX = PosX;
		A_RECT[0].m_PosY = PosY;
		A_RECT[1].m_PosX = PosX-A_Dis_Width;
		A_RECT[1].m_PosY = PosY;
		A_RECT[2].m_PosX = PosX+A_Dis_Width;
		A_RECT[2].m_PosY = PosY;

		A_RECT[3].m_PosX = PosX;
		A_RECT[3].m_PosY = PosY-A_Dis_Height;
		A_RECT[4].m_PosX = PosX;
		A_RECT[4].m_PosY = PosY+A_Dis_Height;
	}

	for(int t=0; t<5; t++){
		A_RECT[t].m_Width= A_Total_Width;
		A_RECT[t].m_Height=A_Total_Height;
		A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);//////C

		if(A_RECT[t].m_Left < 0){//////////////////////수정
			A_RECT[t].m_Left = 0;
			A_RECT[t].m_Width = A_RECT[t].m_Right+1 -A_RECT[t].m_Left;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		if (A_RECT[t].m_Right >CAM_IMAGE_WIDTH){
			A_RECT[t].m_Right = CAM_IMAGE_WIDTH;
			A_RECT[t].m_Width = A_RECT[t].m_Right+1 -A_RECT[t].m_Left;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		if(A_RECT[t].m_Top< 0){
			A_RECT[t].m_Top = 0;
			A_RECT[t].m_Height = A_RECT[t].m_Bottom+1 -A_RECT[t].m_Top;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		if (A_RECT[t].m_Bottom >CAM_IMAGE_HEIGHT){
			int gap = A_RECT[t].m_Bottom - CAM_IMAGE_HEIGHT;
			A_RECT[t].m_Bottom = CAM_IMAGE_HEIGHT;
			A_RECT[t].m_Height = A_RECT[t].m_Bottom -A_RECT[t].m_Top -gap;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);

		//A_RECT[t].m_Thresold = A_Thresold;
		A_RECT[t].d_Thresold = A_Thresold;//º
	}
	UpdateData(FALSE);

	UploadList();//리스트컨트롤의 입력


	for(int t=0; t<5; t++){
		ChangeItem[t]=t;
	}

	for(int t=0; t<5; t++){
		A_DATALIST.Update(t);
	}

	((CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE))->EnableWindow(1);
	//Save_parameter();

}

void CAngle_Option::UploadList(){
	CString str="";
	
	A_DATALIST.DeleteAllItems();

	for(int t=0; t<5; t++){
		InIndex = A_DATALIST.InsertItem(t,"",0);
		
 		
		if(t ==0){
			str.Empty();str="CENTER";
		}
		if(t ==1){
			str.Empty();str="LEFT";
		}
		if(t ==2){
			str.Empty();str="RIGHT";
		}
		if(t ==3){
			str.Empty();str="TOP";
		}
		if(t ==4){
			str.Empty();str="BOTTOM";
		}	
		A_DATALIST.SetItemText(InIndex,0,str);
		str.Empty();str.Format("%d", A_RECT[t].m_PosX);
		A_DATALIST.SetItemText(InIndex,1,str);
		str.Empty();str.Format("%d", A_RECT[t].m_PosY);
		A_DATALIST.SetItemText(InIndex,2,str);
		
		str.Empty();
		str.Format("%d", A_RECT[t].m_Left);
		A_DATALIST.SetItemText(InIndex,3,str);
		str.Empty();
		str.Format("%d", A_RECT[t].m_Top);
		A_DATALIST.SetItemText(InIndex,4,str);
	
		str.Empty();
		str.Format("%d", A_RECT[t].m_Right+1);
		A_DATALIST.SetItemText(InIndex,5,str);
		str.Empty();
		str.Format("%d", A_RECT[t].m_Bottom+1);
		A_DATALIST.SetItemText(InIndex,6,str);

		str.Empty();
		str.Format("%d", A_RECT[t].m_Width);
		A_DATALIST.SetItemText(InIndex,7,str);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Height);
// 		A_DATALIST.SetItemText(InIndex,8,str);
// 		str.Empty();
// 		str.Format("%d",A_RECT[t].m_Thresold);
		A_DATALIST.SetItemText(InIndex, 8, str);//º
		str.Empty();
		str.Format("%0.2f", A_RECT[t].d_Thresold);
		A_DATALIST.SetItemText(InIndex,9,str);
		if(t ==0){
// 			str.Empty();
// 			str.Format("%d",(int)A_RECT[t].m_resultDis);
// 			A_DATALIST.SetItemText(InIndex,10,str);
			str.Empty();//º
			str.Format("%0.2f", A_RECT[t].m_resultDegree);
			A_DATALIST.SetItemText(InIndex, 10, str);
		}
		if(TestMod ==0 || TestMod ==1){
			if(t>0&& t<3){
	// 			str.Empty();
	// 			str.Format("%d",(int)A_RECT[t].m_resultDis);
	// 			A_DATALIST.SetItemText(InIndex,10,str);
				str.Empty();//º
				str.Format("%0.2f", A_RECT[t].m_resultDegree);
				A_DATALIST.SetItemText(InIndex, 10, str);
			}
		}
		if(TestMod ==0 || TestMod ==2){
			if(t>2&& t<5){
	// 			str.Empty();
	// 			str.Format("%d",(int)A_RECT[t].m_resultDis);
	// 			A_DATALIST.SetItemText(InIndex,10,str);
				str.Empty();//º
				str.Format("%0.2f", A_RECT[t].m_resultDegree);
				A_DATALIST.SetItemText(InIndex, 10, str);
			}
		}
	}
}

void CAngle_Option::Save_parameter(){


	CString str="";
	CString strTitle="";

	WritePrivateProfileString(str_model,NULL,"",A_filename);
	str.Empty();
	str.Format("%d",m_INFO.ID);
	WritePrivateProfileString(str_model,"ID",str,A_filename);
	str.Empty();
	str.Format("%s",m_INFO.MODE);
	WritePrivateProfileString(str_model,"MODE",str,A_filename);
	str.Empty();
	str.Format("%s",m_INFO.NAME);
	WritePrivateProfileString(str_model,"NAME",str,A_filename);

	strTitle.Empty();
	strTitle="ANGLE_INIT";

	str.Empty();
	str.Format("%d",A_Total_PosX);
	WritePrivateProfileString(str_model,strTitle+"PosX",str,A_filename);
	str.Empty();
	str.Format("%d",A_Total_PosY);
	WritePrivateProfileString(str_model,strTitle+"PosY",str,A_filename);
	str.Empty();
	str.Format("%d",A_Dis_Width);
	WritePrivateProfileString(str_model,strTitle+"DisW",str,A_filename);
	str.Empty();
	str.Format("%d",A_Dis_Height);
	WritePrivateProfileString(str_model,strTitle+"DisH",str,A_filename);
	str.Empty();
	str.Format("%d",A_Total_Width);
	WritePrivateProfileString(str_model,strTitle+"Width",str,A_filename);
	str.Empty();
	str.Format("%d",A_Total_Height);
	WritePrivateProfileString(str_model,strTitle+"Height",str,A_filename);
	str.Empty();
	str.Format("%0.2f",A_Thresold);
	WritePrivateProfileString(str_model,strTitle+"Thresold",str,A_filename);

	str.Empty();
	str.Format("%d",TestMod);
	WritePrivateProfileString(str_model,strTitle+"TestMod",str,A_filename);

	str.Empty();
	str.Format("%6.2f", m_dOffset);
	WritePrivateProfileString(str_model, strTitle + "OFFSET", str, A_filename);
	str.Empty();
	str.Format("%6.2f", m_dPixelSize);
	WritePrivateProfileString(str_model, strTitle + "PIXEL_SIZE", str, A_filename);
	str.Empty();
	str.Format("%6.2f", m_dFocalLength);
	WritePrivateProfileString(str_model, strTitle + "FOCAL_LENGTH", str, A_filename);
	str.Empty();
	str.Format("%6.2f", m_dDistortion);
	WritePrivateProfileString(str_model, strTitle + "DISTORTION", str, A_filename);


	if(AutomationMod ==1){
		WritePrivateProfileString(str_model,strTitle+"Auto","1",A_filename);}
	if(AutomationMod ==0){
		WritePrivateProfileString(str_model,strTitle+"Auto","0",A_filename);}
	for(int t=0; t<5; t++){
	
		if(t ==0){
			strTitle.Empty();
			strTitle="CENTER_";
		}
		if(t ==1){
			strTitle.Empty();
			strTitle="LEFT_";		
		}
		if(t ==2){
			strTitle.Empty();
			strTitle="RIGHT_";
		}
		if(t ==3){
			strTitle.Empty();
			strTitle="TOP_";		
		}
		if(t ==4){
			strTitle.Empty();
			strTitle="BOTTOM_";
		}
	
		str.Empty();
		str.Format("%d",A_RECT[t].m_PosX);
		A_Original[t].m_PosX=A_RECT[t].m_PosX;
		WritePrivateProfileString(str_model,strTitle+"PosX",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_PosY);
		A_Original[t].m_PosY=A_RECT[t].m_PosY;
		WritePrivateProfileString(str_model,strTitle+"PosY",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Left);
		WritePrivateProfileString(str_model,strTitle+"StartX",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Top);
		WritePrivateProfileString(str_model,strTitle+"StartY",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Right +1);
		WritePrivateProfileString(str_model,strTitle+"ExitX",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Bottom +1);
		WritePrivateProfileString(str_model,strTitle+"ExitY",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Width);
		WritePrivateProfileString(str_model,strTitle+"Width",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Height);
		WritePrivateProfileString(str_model,strTitle+"Height",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Thresold);
		WritePrivateProfileString(str_model,strTitle+"Thresold",str,A_filename);
		str.Empty();
		str.Format("%0.2f", A_RECT[t].d_Thresold);
		WritePrivateProfileString(str_model, strTitle + "d_Thresold", str, A_filename);//º
		str.Empty();
		str.Format("%d", (int)A_RECT[t].m_resultDis);
		WritePrivateProfileString(str_model,strTitle+"Distance",str,A_filename);
		str.Empty();//º
		str.Format("%0.2f", A_RECT[t].m_resultDegree);
		WritePrivateProfileString(str_model, strTitle + "Degree", str, A_filename);
	}

}

void CAngle_Option::OnBnClickedButtonARectSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Save_parameter();
	ChangeCheck =0;
	for(int t=0; t<5; t++){
		ChangeItem[t]=-1;
	}
	for(int t=0; t<5; t++){
		A_DATALIST.Update(t);
	}
}

void CAngle_Option::Load_parameter(){
	CFileFind filefind;
	CString str="";
	CString strTitle="";
	CString EmptyStr="";

	if((GetPrivateProfileCString(str_model,"NAME",A_filename) != m_INFO.NAME)||(GetPrivateProfileCString(str_model,"MODE",A_filename) != m_INFO.MODE)){//ini파일이 없으면 파일을 생성한다.
		A_Total_PosX = CAM_IMAGE_WIDTH/2;
		A_Total_PosY = CAM_IMAGE_HEIGHT/2;
		A_Dis_Width = 200;
		A_Dis_Height = 100;
		A_Total_Width = 80;
		A_Total_Height = 80;
		A_Thresold = 0.5;
		m_dPixelSize = 5.7;
		m_dFocalLength = 0.75;
		m_dDistortion = 1.0;
		m_dOffset = 1.0;
		m_dMasterH = 110.0;
		m_dOffsetH = 1.0;
		m_dMasterV = 80.0;
		m_dOffsetV = 1.0;

		A_RECT[0].m_PosX = CAM_IMAGE_WIDTH / 2;
		A_RECT[0].m_PosY = CAM_IMAGE_HEIGHT / 2;
		A_RECT[0].m_Width=80;
		A_RECT[0].m_Height=80;

		A_RECT[1].m_PosX = CAM_IMAGE_WIDTH / 2 -200;
		A_RECT[1].m_PosY = CAM_IMAGE_HEIGHT / 2;
		A_RECT[1].m_Width=80;
		A_RECT[1].m_Height=80;

		A_RECT[2].m_PosX = CAM_IMAGE_WIDTH / 2 +200;
		A_RECT[2].m_PosY = CAM_IMAGE_HEIGHT / 2;
		A_RECT[2].m_Width=80;
		A_RECT[2].m_Height=80;

		A_RECT[3].m_PosX = CAM_IMAGE_WIDTH / 2;
		A_RECT[3].m_PosY = CAM_IMAGE_HEIGHT / 2 -100;
		A_RECT[3].m_Width=80;
		A_RECT[3].m_Height=80;

		A_RECT[4].m_PosX = CAM_IMAGE_WIDTH / 2;
		A_RECT[4].m_PosY = CAM_IMAGE_HEIGHT / 2 +100;
		A_RECT[4].m_Width=80;
		A_RECT[4].m_Height=80;

		//----
		A_Original[0].m_PosX = CAM_IMAGE_WIDTH / 2;
		A_Original[0].m_PosY = CAM_IMAGE_HEIGHT / 2;
		A_Original[1].m_PosX = CAM_IMAGE_WIDTH / 2 -200;
		A_Original[1].m_PosY = CAM_IMAGE_HEIGHT / 2;
		A_Original[2].m_PosX = CAM_IMAGE_WIDTH / 2 +200;
		A_Original[2].m_PosY = CAM_IMAGE_HEIGHT / 2;
		A_Original[3].m_PosX = CAM_IMAGE_WIDTH / 2;
		A_Original[3].m_PosY = CAM_IMAGE_HEIGHT / 2 -100;
		A_Original[4].m_PosX = CAM_IMAGE_WIDTH / 2;
		A_Original[4].m_PosY = CAM_IMAGE_HEIGHT / 2 +100;
		AutomationMod =0;
		for(int t=0; t<5; t++){
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);//////C
			A_RECT[t].m_Thresold = 10; // Pixel Thresold
			A_RECT[t].d_Thresold = 0.5;//º Thresold
		
		}
		TestMod =0;

		Save_parameter();
	}else{
		
		strTitle="ANGLE_INIT";

		m_dOffset = GetPrivateProfileDouble(str_model, strTitle + "OFFSET", -999, A_filename);
		if (m_dOffset == -999)
		{
			m_dOffset = 1.0;
			str.Format("%0.2f", m_dOffset);
			WritePrivateProfileString(str_model, strTitle + "OFFSET", str, A_filename);
		}
		str.Format("%0.2f", m_dOffset);

		((CEdit *)GetDlgItem(IDC_EDIT_ANGLE_OFFSET))->SetWindowText(str);

		m_dPixelSize = GetPrivateProfileDouble(str_model, strTitle + "PIXEL_SIZE", -1, A_filename);
		if (m_dPixelSize == -1)
		{
			m_dPixelSize = 5.7;
			str.Format("%0.2f", m_dPixelSize);
			WritePrivateProfileString(str_model, strTitle + "PIXEL_SIZE", str, A_filename);
		}
		str.Format("%0.2f", m_dPixelSize);
		((CEdit *)GetDlgItem(IDC_EDIT_PIXEL_SIZE))->SetWindowText(str);
		m_dFocalLength = GetPrivateProfileDouble(str_model, strTitle + "FOCAL_LENGTH", -1, A_filename);
		if (m_dFocalLength == -1)
		{
			m_dFocalLength = 0.75;
			str.Format("%0.2f", m_dFocalLength);
			WritePrivateProfileString(str_model, strTitle + "FOCAL_LENGTH", str, A_filename);
		}
		str.Format("%0.2f", m_dFocalLength);
		((CEdit *)GetDlgItem(IDC_EDIT_FOCAL_LENGTH))->SetWindowText(str);
		m_dDistortion = GetPrivateProfileDouble(str_model, strTitle + "DISTORTION", -1, A_filename);
		if (m_dDistortion == -1)
		{
			m_dDistortion = 1.0;
			str.Format("%6.2f", m_dDistortion);
			WritePrivateProfileString(str_model, strTitle + "DISTORTION", str, A_filename);
		}
		str.Format("%6.2f", m_dDistortion);
		((CEdit *)GetDlgItem(IDC_EDIT_DISTORTION))->SetWindowText(str);

		m_dMasterH = GetPrivateProfileDouble(str_model, strTitle + "MASTER_H", -1, A_filename);
		if (m_dMasterH == -1)
		{
			m_dMasterH = 110.0;
			str.Format("%6.2f", m_dMasterH);
			WritePrivateProfileString(str_model, strTitle + "MASTER_H", str, A_filename);
		}
		str.Format("%6.2f", m_dMasterH);
		((CEdit *)GetDlgItem(IDC_EDIT_MASTER_H))->SetWindowText(str);

		m_dOffsetH = GetPrivateProfileDouble(str_model, strTitle + "OFFSET_H", -1, A_filename);
		if (m_dOffsetH == -1)
		{
			m_dOffsetH = 1.0;
			str.Format("%6.2f", m_dOffsetH);
			WritePrivateProfileString(str_model, strTitle + "OFFSET_H", str, A_filename);
		}
		str.Format("%6.2f", m_dOffsetH);
		((CEdit *)GetDlgItem(IDC_EDIT_OFFSET_H))->SetWindowText(str);

		m_dMasterV = GetPrivateProfileDouble(str_model, strTitle + "MASTER_V", -1, A_filename);
		if (m_dMasterV == -1)
		{
			m_dMasterV = 80.0;
			str.Format("%6.2f", m_dMasterV);
			WritePrivateProfileString(str_model, strTitle + "MASTER_V", str, A_filename);
		}
		str.Format("%6.2f", m_dMasterV);
		((CEdit *)GetDlgItem(IDC_EDIT_MASTER_V))->SetWindowText(str);

		m_dOffsetV = GetPrivateProfileDouble(str_model, strTitle + "OFFSET_V", -1, A_filename);
		if (m_dOffsetV == -1)
		{
			m_dOffsetV = 1.0;
			str.Format("%6.2f", m_dOffsetV);
			WritePrivateProfileString(str_model, strTitle + "OFFSET_V", str, A_filename);
		}
		str.Format("%6.2f", m_dOffsetV);
		((CEdit *)GetDlgItem(IDC_EDIT_OFFSET_V))->SetWindowText(str);


		A_Total_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,A_filename);
		if(A_Total_PosX == -1){
			A_Total_PosX = CAM_IMAGE_WIDTH / 2;
			str.Empty();
			str.Format("%d",A_Total_PosX);
			WritePrivateProfileString(str_model,strTitle+"PosX",str,A_filename);
		}
		A_Total_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,A_filename);
		A_Dis_Width=GetPrivateProfileInt(str_model,strTitle+"DisW",-1,A_filename);
		A_Dis_Height=GetPrivateProfileInt(str_model,strTitle+"DisH",-1,A_filename);
		A_Total_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,A_filename);
		A_Total_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,A_filename);
		A_Thresold=GetPrivateProfileDouble(str_model,strTitle+"Thresold",-1,A_filename);
		TestMod = GetPrivateProfileInt(str_model,strTitle+"TestMod",0,A_filename);

		CString Mod = GetPrivateProfileCString(str_model,strTitle+"Auto",A_filename);
		if("1" == Mod){
			AutomationMod = 1;
		}
		if("0" == Mod){
			AutomationMod = 0;
		}
		AutomationMod = 0;
		for(int t=0; t<5; t++){
			if(t ==0){
				strTitle.Empty();
				strTitle="CENTER_";
			}
			if(t ==1){
				strTitle.Empty();
				strTitle="LEFT_";		
			}
			if(t ==2){
				strTitle.Empty();
				strTitle="RIGHT_";
			}
			if(t ==3){
				strTitle.Empty();
				strTitle="TOP_";		
			}
			if(t ==4){
				strTitle.Empty();
				strTitle="BOTTOM_";
			}	
			A_RECT[t].m_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,A_filename);
			A_Original[t].m_PosX=A_RECT[t].m_PosX;
			A_RECT[t].m_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,A_filename);
			A_Original[t].m_PosY=A_RECT[t].m_PosY;
			A_RECT[t].m_Left=GetPrivateProfileInt(str_model,strTitle+"StartX",-1,A_filename);
			A_RECT[t].m_Top=GetPrivateProfileInt(str_model,strTitle+"StartY",-1,A_filename);
			A_RECT[t].m_Right=GetPrivateProfileInt(str_model,strTitle+"ExitX",-1,A_filename) -1;
			A_RECT[t].m_Bottom=GetPrivateProfileInt(str_model,strTitle+"ExitY",-1,A_filename) -1;
			A_RECT[t].m_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,A_filename);
			A_RECT[t].m_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,A_filename);	
			A_RECT[t].m_Thresold=GetPrivateProfileInt(str_model,strTitle+"Thresold",-1,A_filename);
			A_RECT[t].d_Thresold = GetPrivateProfileDouble(str_model, strTitle + "d_Thresold", 0, A_filename);//º
			A_RECT[t].m_resultDis=GetPrivateProfileInt(str_model,strTitle+"Distance",0,A_filename);
			A_RECT[t].m_resultDegree = GetPrivateProfileDouble(str_model, strTitle + "Degree", 0, A_filename);//º
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
	}
	ChangeCheck =0;
	for(int t=0; t<5; t++){
		ChangeItem[t]=-1;
	}
	for(int t=0; t<5; t++){
		A_DATALIST.Update(t);
	}
}
void CAngle_Option::OnNMDblclkListAngle(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
		//ChangeCheck=1;
	if(pNMITEM->iSubItem == 0 || pNMITEM->iSubItem == -1 || pNMITEM->iItem == -1 )
		return ;
		iSavedItem = pNMITEM->iItem;
		iSavedSubitem = pNMITEM->iSubItem;
	

	if(pNMITEM->iItem != -1)
	{
		if(pNMITEM->iSubItem != 0)
		{		
			A_DATALIST.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			A_DATALIST.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText(A_DATALIST.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetFocus();
			
		}
	}
	//List_COLOR_Change();
	
	*pResult = 0;
}
void CAngle_Option::List_COLOR_Change(){
	bool FLAG = TRUE;
	int Check=0;


	for(int t=0;t<5; t++){
		if(ChangeItem[t]==0)Check++;
		if(ChangeItem[t]==1)Check++;
		if(ChangeItem[t]==2)Check++;
		if(ChangeItem[t]==3)Check++;
		if(ChangeItem[t]==4)Check++;
	}
 	if(Check !=5){
		FLAG =FALSE;
		ChangeItem[changecount]=iSavedItem;
	}

	if(!FLAG){
		for(int t=0; t<changecount+1; t++){
			for(int k=0; k<changecount+1; k++){
				
				if(ChangeItem[t] == ChangeItem[k]){
					if(t==k){break;	}			
					ChangeItem[k] =-1;
				}
			
			
			}
		}
		//----------------데이터 정렬
		int temp=0;

		for(int i=0; i<5; i++)
		{
			for(int j=i+1; j<5; j++)
			{
				if(ChangeItem[i] < ChangeItem[j])  // 내림차순
				{
					temp = ChangeItem[i];
					ChangeItem[i] = ChangeItem[j];
					ChangeItem[j] = temp;
				}
			}
		}
		//--------------------------------
		for(int t=0; t<5; t++){

			if(ChangeItem[t] ==-1){
				changecount =t;
				break;
			}
		}
	}
	for(int t=0; t<5; t++){
		A_DATALIST.Update(t);
	}

}

void CAngle_Option::OnNMClickListAngle(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.


	*pResult = 0;
}

void CAngle_Option::Change_DATA(){

		CString str;
		
		((CEdit *)GetDlgItemText(IDC_EDIT_A_MOD, str));
		A_DATALIST.SetItemText(iSavedItem, iSavedSubitem, str);

		int num = _ttoi(str);
		double d_num = _ttof(str);
		if(num < 0){
			num =0;
			
		}

		if(iSavedSubitem ==1){
			A_RECT[iSavedItem].m_PosX = num;
			
		}
		if(iSavedSubitem ==2){
			A_RECT[iSavedItem].m_PosY = num;
			
		}
		/*if(iSavedSubitem ==3){
			A_RECT[iSavedItem].m_Left = num;
			A_RECT[iSavedItem].m_PosX=(A_RECT[iSavedItem].m_Right+1 +A_RECT[iSavedItem].m_Left) /2;
			A_RECT[iSavedItem].m_Width=A_RECT[iSavedItem].m_Right+1 -A_RECT[iSavedItem].m_Left;

		}
		if(iSavedSubitem ==4){
			A_RECT[iSavedItem].m_Top = num;
			A_RECT[iSavedItem].m_PosY=(A_RECT[iSavedItem].m_Top +A_RECT[iSavedItem].m_Bottom+1)/2;
			A_RECT[iSavedItem].m_Height=A_RECT[iSavedItem].m_Bottom+1 -A_RECT[iSavedItem].m_Top;

		}
		if(iSavedSubitem ==5){
			if(num ==0){
				A_RECT[iSavedItem].m_Right =0;
			}
			else{
				A_RECT[iSavedItem].m_Right = num-1;
			}
			A_RECT[iSavedItem].m_PosX=(A_RECT[iSavedItem].m_Right +A_RECT[iSavedItem].m_Left+1) /2;
			A_RECT[iSavedItem].m_Width=A_RECT[iSavedItem].m_Right+1 -A_RECT[iSavedItem].m_Left;
		}
		if(iSavedSubitem ==6){
			if(num ==0){
				A_RECT[iSavedItem].m_Bottom =0;
			}
			else{
				A_RECT[iSavedItem].m_Bottom = num-1;
			}
			A_RECT[iSavedItem].m_PosY=(A_RECT[iSavedItem].m_Top +A_RECT[iSavedItem].m_Bottom+1)/2;
			A_RECT[iSavedItem].m_Height=A_RECT[iSavedItem].m_Bottom+1 -A_RECT[iSavedItem].m_Top;
		}*/
		if(iSavedSubitem ==3){
			if(num < A_RECT[iSavedItem].m_Right+1){
				A_RECT[iSavedItem].m_Left = num;
				A_RECT[iSavedItem].m_PosX=(A_RECT[iSavedItem].m_Right+1 +A_RECT[iSavedItem].m_Left) /2;
				A_RECT[iSavedItem].m_Width=A_RECT[iSavedItem].m_Right+1 -A_RECT[iSavedItem].m_Left;
			}else{
				A_RECT[iSavedItem].m_Left = A_RECT[iSavedItem].m_Right-A_RECT[iSavedItem].m_Width+1;
			}
		}
		if(iSavedSubitem ==4){
			if(num < A_RECT[iSavedItem].m_Bottom+1){
				A_RECT[iSavedItem].m_Top = num;
				A_RECT[iSavedItem].m_PosY=(A_RECT[iSavedItem].m_Top +A_RECT[iSavedItem].m_Bottom+1)/2;
				A_RECT[iSavedItem].m_Height=A_RECT[iSavedItem].m_Bottom+1 -A_RECT[iSavedItem].m_Top;
			}else{
				A_RECT[iSavedItem].m_Top = A_RECT[iSavedItem].m_Bottom-A_RECT[iSavedItem].m_Height+1;
			}
		}
		if(iSavedSubitem ==5){
			if(num == 0){
				A_RECT[iSavedItem].m_Right = 0;
			}
			else{
				if(num >A_RECT[iSavedItem].m_Left){
					A_RECT[iSavedItem].m_Right = num-1;
					A_RECT[iSavedItem].m_PosX=(A_RECT[iSavedItem].m_Right +A_RECT[iSavedItem].m_Left+1) /2;
					A_RECT[iSavedItem].m_Width=A_RECT[iSavedItem].m_Right+1 -A_RECT[iSavedItem].m_Left;
				}else{
					A_RECT[iSavedItem].m_Right = A_RECT[iSavedItem].m_Left+A_RECT[iSavedItem].m_Width-1;
				}
			}
		}
		if(iSavedSubitem ==6){
			if(num ==0){
				A_RECT[iSavedItem].m_Bottom =0;
			}
			else{
				if(num >A_RECT[iSavedItem].m_Top){
					A_RECT[iSavedItem].m_Bottom = num-1;
					A_RECT[iSavedItem].m_PosY=(A_RECT[iSavedItem].m_Top +A_RECT[iSavedItem].m_Bottom+1)/2;
					A_RECT[iSavedItem].m_Height=A_RECT[iSavedItem].m_Bottom+1 -A_RECT[iSavedItem].m_Top;
				}else{
					A_RECT[iSavedItem].m_Bottom = A_RECT[iSavedItem].m_Top + A_RECT[iSavedItem].m_Height - 1;
				}
			}
		}
		if(iSavedSubitem ==7){
			A_RECT[iSavedItem].m_Width = num;
		}
		if(iSavedSubitem ==8){
			A_RECT[iSavedItem].m_Height = num;
		}
// 		if(iSavedSubitem ==9){
// 			A_RECT[iSavedItem].m_Thresold = num;
// 		}
		if (iSavedSubitem == 9){
			A_RECT[iSavedItem].d_Thresold = d_num;//º
		}
		A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
			

		if(A_RECT[iSavedItem].m_Left < 0){//////////////////////수정
			A_RECT[iSavedItem].m_Left = 0;
			A_RECT[iSavedItem].m_Width = A_RECT[iSavedItem].m_Right+1 -A_RECT[iSavedItem].m_Left;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if (A_RECT[iSavedItem].m_Right >= CAM_IMAGE_WIDTH){
			A_RECT[iSavedItem].m_Right = CAM_IMAGE_WIDTH-1;
			A_RECT[iSavedItem].m_Width = A_RECT[iSavedItem].m_Right+1 -A_RECT[iSavedItem].m_Left;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if(A_RECT[iSavedItem].m_Top< 0){
			A_RECT[iSavedItem].m_Top = 0;
			A_RECT[iSavedItem].m_Height = A_RECT[iSavedItem].m_Bottom+1 -A_RECT[iSavedItem].m_Top;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if (A_RECT[iSavedItem].m_Height >= CAM_IMAGE_HEIGHT){
			A_RECT[iSavedItem].m_Height = CAM_IMAGE_HEIGHT-1;
			A_RECT[iSavedItem].m_Width = A_RECT[iSavedItem].m_Bottom+1 -A_RECT[iSavedItem].m_Top;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}

		if(A_RECT[iSavedItem].m_Height<= 0){
			A_RECT[iSavedItem].m_Height = 1;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if (A_RECT[iSavedItem].m_Height >= CAM_IMAGE_HEIGHT){
			A_RECT[iSavedItem].m_Height = CAM_IMAGE_HEIGHT;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}

		if(A_RECT[iSavedItem].m_Width <= 0){
			A_RECT[iSavedItem].m_Width = 1;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if (A_RECT[iSavedItem].m_Width >= CAM_IMAGE_WIDTH){
			A_RECT[iSavedItem].m_Width = CAM_IMAGE_WIDTH;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}



		A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		

		CString data = "";

		if(iSavedSubitem ==1){
			data.Format("%d",A_RECT[iSavedItem].m_PosX);			
		}
		else if(iSavedSubitem ==2){
			data.Format("%d",A_RECT[iSavedItem].m_PosY);
		}
		else if(iSavedSubitem ==3){
			data.Format("%d",A_RECT[iSavedItem].m_Left);			
		}
		else if(iSavedSubitem ==4){
			data.Format("%d",A_RECT[iSavedItem].m_Top);			
		}
		else if(iSavedSubitem ==5){
			data.Format("%d",A_RECT[iSavedItem].m_Right+1);			
		}
		else if(iSavedSubitem ==6){
			data.Format("%d",A_RECT[iSavedItem].m_Bottom+1);			
		}
		else if(iSavedSubitem ==7){
			data.Format("%d",A_RECT[iSavedItem].m_Width);			
		}
		else if(iSavedSubitem ==8){
			data.Format("%d",A_RECT[iSavedItem].m_Height);			
		}		
// 		else if(iSavedSubitem ==9){
// 			data.Format("%d",A_RECT[iSavedItem].m_Thresold);			
// 		}
		else if (iSavedSubitem == 9){
			data.Format("%0.2f", A_RECT[iSavedItem].d_Thresold);//º
		}
		
		((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText(data);

}

bool CAngle_Option::Change_DATA_CHECK(bool FLAG){
 
	BOOL STAT = TRUE;
	//상한
	if (A_RECT[iSavedItem].m_Width > CAM_IMAGE_WIDTH)
		STAT = FALSE;
	if (A_RECT[iSavedItem].m_Height > CAM_IMAGE_HEIGHT)
		STAT = FALSE;
	if (A_RECT[iSavedItem].m_PosX > CAM_IMAGE_WIDTH)
		STAT = FALSE;
	if (A_RECT[iSavedItem].m_PosY > CAM_IMAGE_HEIGHT)
		STAT = FALSE;
	if (A_RECT[iSavedItem].m_Top > CAM_IMAGE_HEIGHT)
		STAT = FALSE;
	if (A_RECT[iSavedItem].m_Bottom > CAM_IMAGE_HEIGHT)
		STAT = FALSE;
	if (A_RECT[iSavedItem].m_Left> CAM_IMAGE_WIDTH)
		STAT = FALSE;
	if (A_RECT[iSavedItem].m_Right> CAM_IMAGE_WIDTH)
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Thresold> 200 )
		STAT = FALSE;

	//하한

	if(A_RECT[iSavedItem].m_Width < 1 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Height < 1 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_PosX < 0 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_PosY < 0 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Top < 0 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Bottom < 0  )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Left< 0 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Right< 0 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Thresold< 0 )
		STAT = FALSE;


	if(FLAG  == 1){
		/*if(A_RECT[iSavedItem].m_Width == 1 )
			STAT = FALSE;
		if(A_RECT[iSavedItem].m_Height == 1 )
			STAT = FALSE;*/
		
	}

	return STAT;
	
	

}

BOOL CAngle_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
			if(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->GetSafeHwnd())//GetSafeHwnd() 자신의 핸들을 가져오는 함수
			{
				//ChangeCheck =1;
				int bufSubitem = iSavedSubitem + 1;
				
				if(bufSubitem ==11){
					if(iSavedItem == 0){
						iSavedItem = 1; bufSubitem =1;
					}
					else if(iSavedItem == 1){
						iSavedItem = 2; bufSubitem =1;
					}
					else if(iSavedItem == 2){
						iSavedItem = 3; bufSubitem =1;
					}
					else if(iSavedItem == 3){
						iSavedItem = 4; bufSubitem =1;
					}
					else if(iSavedItem == 4){
						iSavedItem = 0; bufSubitem =1;
					}
				}
				int bufItem = iSavedItem;
				A_DATALIST.EnsureVisible(iSavedItem, FALSE);
				A_DATALIST.GetSubItemRect(iSavedItem,bufSubitem , LVIR_BOUNDS, rect);
				A_DATALIST.ClientToScreen(rect);
				this->ScreenToClient(rect);
				A_DATALIST.SetSelectionMark(iSavedItem);
				A_DATALIST.SetFocus(); //저장
				iSavedItem = bufItem;
				iSavedSubitem = bufSubitem;
				((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText(A_DATALIST.GetItemText(iSavedItem, iSavedSubitem));
				((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
				((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetFocus();
			}
			
			if ((pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_PosX))->GetSafeHwnd())||  
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_PosY))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_Dis_W))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_Dis_H))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_Width))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_Height))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_Thresold))->GetSafeHwnd()))
			{	
				OnBnClickedButtonsetanglezone();
			}

			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}

	return CDialog::PreTranslateMessage(pMsg);
}

//void CAngle_Option::OnTimer(UINT_PTR nIDEvent)
//{
//	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
//
//	CDialog::OnTimer(nIDEvent);
//	switch(nIDEvent)	
//	{
//	case 110:
//		if(EnterState==1)
//		{
//	        //int nCount = C_DATALIST.GetItemCount();
//			//C_DATALIST.EnsureVisible(nCount, TRUE); 
//			if(iSavedSubitem2 ==11){
//				if(iSavedItem == 0){
//					iSavedItem = 1; iSavedSubitem2 =1;
//				}
//				else if(iSavedItem == 1){
//					iSavedItem = 2; iSavedSubitem2 =1;
//				}
//				else if(iSavedItem == 2){
//					iSavedItem = 3; iSavedSubitem2 =1;
//				}
//				else if(iSavedItem == 3){
//					iSavedItem = 4; iSavedSubitem2 =1;
//				}
//				else if(iSavedItem == 4){
//					iSavedItem = 0; iSavedSubitem2 =1;
//				}
//				
//				
//			}
//			
//			A_DATALIST.GetSubItemRect(iSavedItem,iSavedSubitem2 , LVIR_BOUNDS, rect);
//			A_DATALIST.ClientToScreen(rect);
//			this->ScreenToClient(rect);
//
//			A_DATALIST.SetSelectionMark(iSavedItem);
//			A_DATALIST.SetFocus();
//			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText(A_DATALIST.GetItemText(iSavedItem, iSavedSubitem2));
//			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
//			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetFocus();
//
//			iSavedSubitem=iSavedSubitem2;
//
//			EnterState=0;
//			
//		}
//
//	break;
//
//	}
//}


// bool CAngle_Option::AngleGen(LPBYTE IN_RGB,int NUM){
// 	int	dwk = 0,dwi = 0;
// 		DWORD	RGBPIX = 0,RGBLINE = 0;
// 		DWORD 	SumX = 0,SumY = 0;
// 		DWORD	N1 = 0;
// 		BYTE *BW;
// 		DWORD Total = A_RECT[NUM].Height()*A_RECT[NUM].Length();
// 		BW = new BYTE[Total];
// 		memset(BW,0,sizeof(BW));
// 			int index=0;
// 		A_RECT[NUM].m_Success =FALSE;
// 
// 		int offsetX1 = A_RECT[NUM].m_Left;
// 		int offsetX2 = A_RECT[NUM].m_Right+1;
// 		int offsetY1 = A_RECT[NUM].m_Top;
// 		int offsetY2 = A_RECT[NUM].m_Bottom+1;
// 
// 		unsigned int PostionX[15000]={0,};
// 		unsigned int PostionY[15000]={0,};
// 
// 		//if(m_EN_RUN != 1){
// 		//	m_Success = FALSE;
// 		//	return	FALSE;
// 		//}
// 		
// 		IplImage *t_image = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
// 
// 		for (dwk = 0; dwk<CAM_IMAGE_HEIGHT; dwk++)
// 		{	
// 			for (dwi = 0; dwi<CAM_IMAGE_WIDTH; dwi++)
// 			{
// 				m_InB = IN_RGB[RGBLINE + RGBPIX];
// 				m_InG = IN_RGB[RGBLINE + RGBPIX + 1];
// 				m_InR = IN_RGB[RGBLINE + RGBPIX + 2];
// 				
// 				int R, G, B;
// 				R = IN_RGB[RGBLINE + RGBPIX + 2];
// 				G = IN_RGB[RGBLINE + RGBPIX + 1];
// 				B = IN_RGB[RGBLINE + RGBPIX];
// 
// 				if(dwi>offsetX1 && dwi<offsetX2){
// 					if(dwk >offsetY1 &&dwk<offsetY2){
// 					//	BW[index] = (0.29900*m_InB)+(0.58700*m_InG)+(0.11400*m_InR);
// 						double Y_Temp = (0.29900*B) + (0.58700*G) + (0.11400*R);
// 						
// 						if(NUM !=0){
// 						//	if(R > (B + G)*2)
// 							if (Y_Temp < 150)
// 							{
// 								BW[index] = 0;
// 								t_image->imageData[dwk * t_image->widthStep + dwi] = 0;
// 							}
// 							else
// 							{
// 								BW[index] = 255;
// 								t_image->imageData[dwk * t_image->widthStep + dwi] = 255;
// 							}
// 
// 							if(BW[index]<50 ){
// 							//if(m_InR<50 && m_InG<50 && m_InB<50 ){ //&& RED1
// 								PostionX[N1]= dwi; PostionY[N1]=dwk;
// 								SumX += (double)PostionX[N1]; SumY += (double)PostionY[N1];
// 								N1 ++;
// 							}
// 							index++;
// 						}else
// 						{
// 							if(m_InR <100 && m_InB<100 && m_InG <100){
// 								BW[index] = 0;
// 							}
// 							else{
// 								BW[index] = 255;
// 							}
// 							if(BW[index]<50 ){
// 							//if(m_InR<50 && m_InG<50 && m_InB<50 ){ //&& RED1
// 								PostionX[N1]= dwi; PostionY[N1]=dwk;
// 								SumX += (double)PostionX[N1]; SumY += (double)PostionY[N1];
// 								N1 ++;
// 							}
// 							index++;
// 
// 						}
// 					}
// 				}
// 				RGBPIX += 3;
// 			}
// 			RGBLINE += CAM_IMAGE_WIDTH*3;
// 			RGBPIX = 0;
// 		}
// 		if(N1 > 5){
// 
// 		A_RECT[NUM].m_resultX = (int)((SumX/N1)+0.5);//반올림
// 		A_RECT[NUM].m_resultY = (int)((SumY/N1)+0.5);//반올림
// 		}
// 
// 		delete []BW;
// 
// 	cvSaveImage("D:\\AngleImage.bmp", t_image);
// 	cvReleaseImage(&t_image);
// 	return A_RECT[NUM].m_Success;
// 
// }

bool CAngle_Option::AngleGen(LPBYTE IN_RGB, int NUM){


	IplImage *OriginImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 3);

	int Curr_Center_X = -99999;
	int Curr_Center_Y = -99999;

	A_RECT[NUM].m_resultX = -1;
	A_RECT[NUM].m_resultY = -1;

	BYTE R, G, B;
	double Sum_Y;

	BOOL detect = FALSE;

	cvSetZero(OriginImage);

	for (int y = A_RECT[NUM].m_Top; y < A_RECT[NUM].m_Bottom + 1; y++)
	{
		for (int x = A_RECT[NUM].m_Left; x < A_RECT[NUM].m_Right + 1; x++)
		{
			B = IN_RGB[y*CAM_IMAGE_WIDTH * 3 + x * 3];
			G = IN_RGB[y*CAM_IMAGE_WIDTH * 3 + x * 3 + 1];
			R = IN_RGB[y*CAM_IMAGE_WIDTH * 3 + x * 3 + 2];

			Sum_Y = (BYTE)((0.29900*R) + (0.58700*G) + (0.11400*B));

			OriginImage->imageData[(y - (A_RECT[NUM].m_Top))*OriginImage->widthStep + (x - (A_RECT[NUM].m_Left))] = (char)Sum_Y;
		}
	}

	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 3, 1.0, 1.0);
	//	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);

	//cvSaveImage("D:\\OriginImage_A.jpg", OriginImage);

	//	cvSaveImage("D:\\SmoothImage.jpg", SmoothImage);

	//cvThreshold(SmoothImage, SmoothImage, 100, 255, CV_THRESH_BINARY);
	cvThreshold(SmoothImage, SmoothImage, 100, 255, CV_THRESH_OTSU);

	if (NUM == 0)// ((NUM != 1) && (NUM != 2))
		cvNot(SmoothImage, SmoothImage);


	//	cvCanny(SmoothImage, CannyImage, 0, 50);

	cvDilate(SmoothImage, DilateImage);

	cvSaveImage("D:\\DilateImage_A.jpg", DilateImage);

	cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;

	int counter = 0;

	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
		counter++;

	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area = 0, arcCount = 0;
	counter = 0;
	double old_dist = 999999;

	for (; contour != 0; contour = contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;
		double circularity = (4.0*3.14*area) / (arcCount*arcCount);

		if (circularity > 0.6)
		{
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width / 2;
			center_y = rect.y + rect.height / 2;

			//if(center_x > (CAM_IMAGE_WIDTH/2) - (CAM_IMAGE_WIDTH/8) && center_x < (CAM_IMAGE_WIDTH/2) + (CAM_IMAGE_WIDTH/8) && center_y > (CAM_IMAGE_HEIGHT/2) - (CAM_IMAGE_HEIGHT/6) && center_y < (CAM_IMAGE_HEIGHT/2) + (CAM_IMAGE_HEIGHT/6))
			//{
			//	if(rect.width > 50 && rect.height > 50)
			{
				if (rect.width < A_RECT[NUM].m_Width && rect.height < A_RECT[NUM].m_Height)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);
					//		
					//	//	double dist = GetDistance(center_x, center_y, (Standard_RD_RECT[0].m_Left+Standard_RD_RECT[0].m_Width/2), (Standard_RD_RECT[0].m_Top+Standard_RD_RECT[0].m_Height/2));
					//double dist = GetDistance(center_x, center_y, (OriginImage->width / 2), (OriginImage->height / 2));
					double dist = GetDistance(center_x, center_y, ((A_RECT[NUM].m_Left + A_RECT[NUM].m_Right) / 2), ((A_RECT[NUM].m_Top + A_RECT[NUM].m_Bottom) / 2));
					//if (dist != 0)
					{
						if (dist < old_dist)
						{
							Curr_Center_X = rect.x + rect.width / 2;
							Curr_Center_Y = rect.y + rect.height / 2;
							old_dist = dist;

							detect = TRUE;
						}
					}
				}
			}
			cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 0, 255), 1, 8);
		}

		counter++;
	}

	if ((Curr_Center_X == -99999) || (Curr_Center_Y == -99999)){
		Curr_Center_X = -1;
		Curr_Center_Y = -1;

	}
	else{
		Curr_Center_X = Curr_Center_X + A_RECT[NUM].m_Left;
		Curr_Center_Y = Curr_Center_Y + A_RECT[NUM].m_Top;
	}


	SC_RECT[NUM].m_resultX = Curr_Center_X;
	SC_RECT[NUM].m_resultY = Curr_Center_Y;

	cvSaveImage("D:\\RGBResultImage_A.jpg", RGBResultImage);

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&CannyImage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);

	delete[]rectArray;
	delete[]areaArray;

	return detect;
}

void CAngle_Option::AngleSum(){//AVE값 및 평균 RGB값 추출 및 그리기.
	A_RECT[0].m_resultDis=0;
	
	if(MasterMod == TRUE){
		A_RECT[1].m_resultDis=int(sqrt(pow(A_RECT[0].m_resultX-A_RECT[1].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[1].m_resultY,2)));
		A_RECT[2].m_resultDis=int(sqrt(pow(A_RECT[0].m_resultX-A_RECT[2].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[2].m_resultY,2)));

		A_RECT[3].m_resultDis=int(sqrt(pow(A_RECT[0].m_resultX-A_RECT[3].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[3].m_resultY,2)));
		A_RECT[4].m_resultDis=int(sqrt(pow(A_RECT[0].m_resultX-A_RECT[4].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[4].m_resultY,2)));
	}
	else{
		disL=sqrt(pow(A_RECT[0].m_resultX-A_RECT[1].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[1].m_resultY,2));
		disR=sqrt(pow(A_RECT[0].m_resultX-A_RECT[2].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[2].m_resultY,2));	
		disT=sqrt(pow(A_RECT[0].m_resultX-A_RECT[3].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[3].m_resultY,2));
		disB=sqrt(pow(A_RECT[0].m_resultX-A_RECT[4].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[4].m_resultY,2));	


		m_Comp_L_MAX = A_RECT[1].m_resultDis +A_RECT[1].m_Thresold;
		m_Comp_R_MAX= A_RECT[2].m_resultDis +A_RECT[2].m_Thresold;
		m_Comp_L_MIN= A_RECT[1].m_resultDis -A_RECT[1].m_Thresold;
		m_Comp_R_MIN= A_RECT[2].m_resultDis -A_RECT[2].m_Thresold;

		m_Comp_T_MAX = A_RECT[3].m_resultDis +A_RECT[3].m_Thresold;
		m_Comp_B_MAX= A_RECT[4].m_resultDis +A_RECT[4].m_Thresold;
		m_Comp_T_MIN= A_RECT[3].m_resultDis -A_RECT[3].m_Thresold;
		m_Comp_B_MIN= A_RECT[4].m_resultDis -A_RECT[4].m_Thresold;

// 		if(TestMod == 0 || TestMod == 1){
// 			CString str="";
// 			str.Empty();
// 			str.Format("%6.2f",disL);
// 
// 			if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1)
// 				m_Lot_AngleList.SetItemText(Lot_InsertIndex, 5, str);
// 			else
// 				m_AngleList.SetItemText(InsertIndex,4,str);
// 			
// 			
// 
// 			str.Empty();
// 			str.Format("%6.2f",disR);
// 
// 			if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1)
// 				m_Lot_AngleList.SetItemText(Lot_InsertIndex, 6, str);
// 			else
// 				m_AngleList.SetItemText(InsertIndex,5,str);
// 			
// 		}
// 	
// 		if(TestMod == 0 || TestMod == 2){
// 			CString str="";
// 			str.Empty();
// 			str.Format("%6.2f", disT);
// 
// 			if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1)
// 				m_Lot_AngleList.SetItemText(Lot_InsertIndex, 7, str);
// 			else
// 				m_AngleList.SetItemText(InsertIndex,6,str);
// 			
// 			str.Empty();
// 			str.Format("%6.2f",disB);
// 
// 			if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1)
// 				m_Lot_AngleList.SetItemText(Lot_InsertIndex, 8, str);
// 			else
// 				m_AngleList.SetItemText(InsertIndex,7,str);
// 			
// 
// 		}
// 		for(int NUM =0; NUM<5; NUM++){
// 			
// 			if(TestMod == 0 || TestMod == 1){
// 				if(NUM<3){
// 					if(disL>= m_Comp_L_MIN&&disL <=m_Comp_L_MAX){
// 						if(disR >= m_Comp_R_MIN&&disR <=m_Comp_R_MAX){
// 							A_RECT[NUM].m_Success = TRUE;
// 						
// 						}
// 					}
// 					A_RECT[0].m_Success = TRUE;
// 				}
// 			}
// 			if(TestMod == 0 || TestMod == 2){
// 				if(NUM>=3){
// 					if(disT>= m_Comp_T_MIN&&disT <=m_Comp_T_MAX){
// 						if(disB >= m_Comp_B_MIN&&disB <=m_Comp_B_MAX){
// 							A_RECT[NUM].m_Success = TRUE;
// 						
// 						}
// 					}
// 					A_RECT[0].m_Success = TRUE;
// 				}
// 			}
// 		}

	}
	
}/*

int CAngle_Option::Distance_Sum(int x1, int y1, int x2, int y2){

	int Sum=0;
	int X_result=0;
	int Y_result=0;
	X_result =int(sqrt(pow(x2-x,2)+pow(A_RECT[0].m_resultX-A_RECT[1].m_resultX,2)));

	return Sum;
}
*/
bool CAngle_Option::AnglePic(CDC *cdc,int NUM){//AVE값 및 평균 RGB값 추출 및 그리기.
	
	CPen	my_Pan,my_Pan2,*old_pan,*old_pan2;
	CString RESULTDATA ="";
	CString TEXTDATA ="";


// 	A_RECT[NUM].m_resultX = SC_RECT[NUM].m_resultX;
// 	A_RECT[NUM].m_resultY = SC_RECT[NUM].m_resultY;
	//if(A_RECT[NUM].Chkdata() == FALSE){return 0;}
	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	if(A_RECT[NUM].m_Success == TRUE){
		my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(BLUE_COLOR);
	}else{
		my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(RED_COLOR);
	}

	cdc->MoveTo(A_RECT[NUM].m_Left,A_RECT[NUM].m_Top);
	cdc->LineTo(A_RECT[NUM].m_Right,A_RECT[NUM].m_Top);
	cdc->LineTo(A_RECT[NUM].m_Right,A_RECT[NUM].m_Bottom);
	cdc->LineTo(A_RECT[NUM].m_Left,A_RECT[NUM].m_Bottom);
	cdc->LineTo(A_RECT[NUM].m_Left,A_RECT[NUM].m_Top);


	my_Pan2.CreatePen(PS_SOLID,2,WHITE_COLOR);
	old_pan2 = cdc->SelectObject(&my_Pan2);
	cdc->MoveTo(int(A_RECT[NUM].m_resultX)-5,int(A_RECT[NUM].m_resultY));
	cdc->LineTo(int(A_RECT[NUM].m_resultX)+5,int(A_RECT[NUM].m_resultY));
	cdc->MoveTo(int(A_RECT[NUM].m_resultX),int(A_RECT[NUM].m_resultY)-5);
	cdc->LineTo(int(A_RECT[NUM].m_resultX),int(A_RECT[NUM].m_resultY)+5);
	

	///if(MasterMod == TRUE){
	//	TEXTDATA.Format("R:%03d G:%03d B:%03d",A_RECT[NUM].m_R,A_RECT[NUM].m_G,A_RECT[NUM].m_B);
//	}
//	else{
	TEXTDATA.Format("X:%6.2f Y:%6.2f",A_RECT[NUM].m_resultX,A_RECT[NUM].m_resultY);	
//	}
	cdc->TextOut(A_RECT[NUM].m_Left + (int)(A_RECT[NUM].Height()/2)-60,A_RECT[NUM].m_Top - 20,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	
	::SetTextColor(cdc->m_hDC,RGB(255, 255, 0)); 

	if (TestMod == 0 || TestMod == 1){
		if (dgrL != 0){
			RESULTDATA.Empty();
			RESULTDATA.Format("%6.2f", dgrL);
		}
		else{
			RESULTDATA.Empty();
			RESULTDATA.Format("");
		}
		//cdc->TextOut(((A_RECT[0].m_resultX+A_RECT[1].m_resultX)/2),A_RECT[1].m_resultY,RESULTDATA.GetBuffer(0),RESULTDATA.GetLength());
		cdc->TextOut(A_RECT[1].m_Left + (int)(A_RECT[1].Height() / 2) - 30, A_RECT[1].m_Top - 40, RESULTDATA.GetBuffer(0), RESULTDATA.GetLength());
		if (dgrR != 0){
			RESULTDATA.Empty();
			RESULTDATA.Format("%6.2f", dgrR);
		}
		else{
			RESULTDATA.Empty();
			RESULTDATA.Format("");
		}
		//cdc->TextOut(((A_RECT[0].m_resultX+A_RECT[2].m_resultX)/2),A_RECT[2].m_resultY,RESULTDATA.GetBuffer(0),RESULTDATA.GetLength());
		cdc->TextOut(A_RECT[2].m_Left + (int)(A_RECT[2].Height() / 2) - 30, A_RECT[2].m_Top - 40, RESULTDATA.GetBuffer(0), RESULTDATA.GetLength());
	}

	if (TestMod == 0 || TestMod == 2){
		if (dgrT != 0){
			RESULTDATA.Empty();
			RESULTDATA.Format("%6.2f", dgrT);
		}
		else{
			RESULTDATA.Empty();
			RESULTDATA.Format("");
		}
		//cdc->TextOut(A_RECT[3].m_resultX-10,((A_RECT[3].m_resultY +A_RECT[0].m_resultY) /2)+3,RESULTDATA.GetBuffer(0),RESULTDATA.GetLength());
		cdc->TextOut(A_RECT[3].m_Left + (int)(A_RECT[3].Height() / 2) - 30, A_RECT[3].m_Top - 40, RESULTDATA.GetBuffer(0), RESULTDATA.GetLength());


		if (dgrB != 0){
			RESULTDATA.Empty();
			RESULTDATA.Format("%6.2f", dgrB);
		}
		else{
			RESULTDATA.Empty();
			RESULTDATA.Format("");
		}
		//cdc->TextOut(A_RECT[4].m_resultX-10,((A_RECT[4].m_resultY +A_RECT[0].m_resultY) /2)-3,RESULTDATA.GetBuffer(0),RESULTDATA.GetLength());
		cdc->TextOut(A_RECT[4].m_Left + (int)(A_RECT[4].Height() / 2) - 30, A_RECT[4].m_Top - 40, RESULTDATA.GetBuffer(0), RESULTDATA.GetLength());
	}
// 	if(TestMod == 0 || TestMod ==1){
// 		if(disL != 0 ){
// 			RESULTDATA.Empty();
// 			RESULTDATA.Format("%6.2f",disL);
// 		}else{RESULTDATA.Empty();
// 		RESULTDATA.Format("");}
// 		//cdc->TextOut(((A_RECT[0].m_resultX+A_RECT[1].m_resultX)/2),A_RECT[1].m_resultY,RESULTDATA.GetBuffer(0),RESULTDATA.GetLength());
// 		cdc->TextOut(A_RECT[1].m_Left + (int)(A_RECT[1].Height()/2)-30,A_RECT[1].m_Top - 40,RESULTDATA.GetBuffer(0),RESULTDATA.GetLength());
// 		if(disR != 0 ){
// 			RESULTDATA.Empty();
// 			RESULTDATA.Format("%6.2f",disR);
// 		}else{RESULTDATA.Empty();
// 		RESULTDATA.Format("");}
// 		//cdc->TextOut(((A_RECT[0].m_resultX+A_RECT[2].m_resultX)/2),A_RECT[2].m_resultY,RESULTDATA.GetBuffer(0),RESULTDATA.GetLength());
// 		cdc->TextOut(A_RECT[2].m_Left + (int)(A_RECT[2].Height()/2)-30,A_RECT[2].m_Top - 40,RESULTDATA.GetBuffer(0),RESULTDATA.GetLength());
// 	}
// 
// 	if(TestMod == 0 || TestMod ==2){
// 		if(disT != 0 ){
// 			RESULTDATA.Empty();
// 			RESULTDATA.Format("%6.2f",disT);
// 		}else{RESULTDATA.Empty();
// 		RESULTDATA.Format("");}
// 		//cdc->TextOut(A_RECT[3].m_resultX-10,((A_RECT[3].m_resultY +A_RECT[0].m_resultY) /2)+3,RESULTDATA.GetBuffer(0),RESULTDATA.GetLength());
// 		cdc->TextOut(A_RECT[3].m_Left + (int)(A_RECT[3].Height()/2)-30,A_RECT[3].m_Top - 40,RESULTDATA.GetBuffer(0),RESULTDATA.GetLength());
// 
// 
// 		if(disB != 0 ){
// 			RESULTDATA.Empty();
// 			RESULTDATA.Format("%6.2f",disB);
// 		}else{RESULTDATA.Empty();
// 		RESULTDATA.Format("");}
// 		//cdc->TextOut(A_RECT[4].m_resultX-10,((A_RECT[4].m_resultY +A_RECT[0].m_resultY) /2)-3,RESULTDATA.GetBuffer(0),RESULTDATA.GetLength());
// 		cdc->TextOut(A_RECT[4].m_Left + (int)(A_RECT[4].Height()/2)-30,A_RECT[4].m_Top - 40,RESULTDATA.GetBuffer(0),RESULTDATA.GetLength());
// 	}

	cdc->SelectObject(old_pan);
	cdc->SelectObject(old_pan2);


	my_Pan.DeleteObject();
	my_Pan2.DeleteObject();

	return TRUE;
}
void CAngle_Option::OnBnClickedButtonMasterA()
{
	ChangeCheck=1;
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	((CImageTesterDlg *)m_pMomWnd)->OverLay_Off();
//	((CImageTesterDlg *)m_pMomWnd)->m_pChartWnd->ChartImgClr(((CImageTesterDlg *)m_pMomWnd)->m_pChartWnd->Imgcx,((CImageTesterDlg *)m_pMomWnd)->m_pChartWnd->Imgcy);
//	((CImageTesterDlg *)m_pMomWnd)->m_pChartWnd->ANGLEIMGPic(((CImageTesterDlg *)m_pMomWnd)->m_pChartWnd->Imgcx,((CImageTesterDlg *)m_pMomWnd)->m_pChartWnd->Imgcy,40);
	
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	

	MasterMod =TRUE;
	BOOL FLAG = FALSE;
			
	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan =1;						
			
	for(int i =0;i<10;i++){
		if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0){
			break;
		}else{
			DoEvents(50);
		}
	}

	if (AutomationMod == 1){
	// 오토메이션 모드
	//C_CHECKING[] PosX,PosY 받은 거 체킹	

		//Automation_DATA_INPUT();// 성공하면 데이터 C_RECT에 넣기.
		//실패하면 그냥 진행...
		Load_parameter();// 실패한 경우
		GetSideCircleCoordinate(m_RGBScanbuf);

		FLAG = TRUE;
		for(int lop=0; lop<5; lop++)
		{
			C_CHECKING[lop].m_PosX = A_RECT[lop].m_resultX;
			C_CHECKING[lop].m_PosY = A_RECT[lop].m_resultY;					
			C_CHECKING[lop].m_Success = A_RECT[lop].m_Success;					

			/*if(!C_CHECKING[lop].m_Success)
			{
				FLAG = FALSE;
				break;
			}*/
		}

// 		if(TestMod == 0)
// 		{			
// 			if(C_CHECKING[0].m_Success == FALSE || C_CHECKING[1].m_Success == FALSE || C_CHECKING[2].m_Success == FALSE || C_CHECKING[3].m_Success == FALSE || C_CHECKING[4].m_Success == FALSE)
// 				FLAG = FALSE;
// 		}
// 		else if(TestMod == 1)
// 		{
// 			if(C_CHECKING[0].m_Success == FALSE || C_CHECKING[1].m_Success == FALSE || C_CHECKING[2].m_Success == FALSE)
// 				FLAG = FALSE;
// 		}
// 		else if(TestMod == 2)
// 		{
// 			if(C_CHECKING[0].m_Success == FALSE || C_CHECKING[3].m_Success == FALSE || C_CHECKING[4].m_Success == FALSE)
// 				FLAG = FALSE;
// 		}	
// 		if(FLAG){
// 			Automation_DATA_INPUT();
// 		}else{Load_parameter();}

				
		
	}/*else{
		Load_Original_pra();
	}*/

	
	//if(!FLAG){
		for(int lop=0; lop<5; lop++){
				AngleGen(m_RGBScanbuf, lop);
		}
	//}
// 	else{
// 		for(int t=0; t<5; t++){
// 			A_RECT[t].m_resultX=C_CHECKING[t].m_PosX;
// 			A_RECT[t].m_resultY=C_CHECKING[t].m_PosY;
// 		}
// 	}
	for(int NUM=0; NUM<5; NUM++){
		A_RECT[NUM].m_resultX = SC_RECT[NUM].m_resultX;
		A_RECT[NUM].m_resultY = SC_RECT[NUM].m_resultY;
	}
	AngleSum();
	for (int NUM = 0; NUM < 5; NUM++){
		A_RECT[NUM].m_resultDegree = Degree_Sum(NUM);
	}
	UploadList();

	CString str_Buf = "";

	m_dMasterH = A_RECT[1].m_resultDegree + A_RECT[2].m_resultDegree;
	m_dMasterV = A_RECT[3].m_resultDegree + A_RECT[4].m_resultDegree;

	str_Buf.Format("%6.2f", m_dMasterH);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_H))->SetWindowText(str_Buf);

	str_Buf.Format("%6.2f", m_dMasterV);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_V))->SetWindowText(str_Buf);

	MasterMod =FALSE;

	for(int t=0; t<5; t++){
		ChangeItem[t]=t;
	}
	for(int t=0; t<5; t++){
		A_DATALIST.Update(t);
	}
	((CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE))->EnableWindow(1);
}
void CAngle_Option::Load_Original_pra(){

	for(int t=0; t<5; t++){
		A_RECT[t].m_PosX=A_Original[t].m_PosX;
		A_RECT[t].m_PosY=A_Original[t].m_PosY;
		A_RECT[t].EX_RECT_SET(A_Original[t].m_PosX,A_Original[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);//////C
	}

}

tResultVal CAngle_Option::Run()
{	

	CString str="";
	if(ChangeCheck == TRUE){
		OnBnClickedButtonARectSave();
	}	
	b_StopFail = FALSE;
// 	if(((CImageTesterDlg *)m_pMomWnd)->LotMod ==1){
// 		LOT_InsertDataList();
// 	}
// 	else{
// 		InsertList();
// 	}
	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

		InsertList();
	}
	int camcnt = 0;
	int count=0;
	int Resultcount=0;
	tResultVal retval = {0,};


	BOOL FLAG = FALSE;
	int loop_cnt=0;

	ResultDegree_H = 0;
	ResultDegree_V = 0;

	//AngleKeepRun = btn_AngleKeepRun.GetCheck();

	//while(loop_cnt < 1 || AngleKeepRun)
	//{
	//	AngleKeepRun = btn_AngleKeepRun.GetCheck();
		count = 0;
		Resultcount=0;
		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan =1;						
		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;
		for (int i = 0; i < 50; i++)
		{
			DoEvents(10);
			if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0 && ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0)
			{
				break;
			}
		}


		if(AutomationMod ==1){
			// 오토메이션 모드
			//C_CHECKING[] PosX,PosY 받은 거 체킹	
			//Automation_DATA_INPUT();// 성공하면 데이터 C_RECT에 넣기.
			//실패하면 그냥 진행...
			//Load_parameter();// 실패한 경우

			GetSideCircleCoordinate(m_RGBScanbuf);
			
			

			for(int lop=0; lop<5; lop++)
			{
				C_CHECKING[lop].m_PosX = SC_RECT[lop].m_resultX;
				C_CHECKING[lop].m_PosY = SC_RECT[lop].m_resultY;					
				C_CHECKING[lop].m_Success = SC_RECT[lop].m_Success;					

				/*if(!C_CHECKING[lop].m_Success)
				{
					FLAG = FALSE;
					break;
				}*/
				FLAG = TRUE;
				
			}

// 			if(TestMod == 0)
// 			{			
// 				if(C_CHECKING[0].m_Success == FALSE || C_CHECKING[1].m_Success == FALSE || C_CHECKING[2].m_Success == FALSE || C_CHECKING[3].m_Success == FALSE || C_CHECKING[4].m_Success == FALSE)
// 					FLAG = FALSE;
// 			}
// 			else if(TestMod == 1)
// 			{
// 				if(C_CHECKING[0].m_Success == FALSE || C_CHECKING[1].m_Success == FALSE || C_CHECKING[2].m_Success == FALSE)
// 					FLAG = FALSE;
// 			}
// 			else if(TestMod == 2)
// 			{
// 				if(C_CHECKING[0].m_Success == FALSE || C_CHECKING[3].m_Success == FALSE || C_CHECKING[4].m_Success == FALSE)
// 					FLAG = FALSE;
// 			}
// 
// 			if(FLAG){
// 				Automation_DATA_INPUT();
// 			}
// 			else{Load_parameter();}

			
		
		}

		for (int lop = 0; lop < 5; lop++){
			AngleGen(m_RGBScanbuf, lop);
		}

		for(int NUM=0; NUM<5; NUM++){
			A_RECT[NUM].m_resultX = SC_RECT[NUM].m_resultX;
			A_RECT[NUM].m_resultY = SC_RECT[NUM].m_resultY;
		}
		AngleSum();
		A_RECT[0].m_Success = TRUE; // 센터는 무조건 PASS
		CString stateDATA = "화각_  ";
		for (int NUM = 0; NUM < 5; NUM++){

			SC_RECT[NUM].m_resultDegree = Degree_Sum(NUM);

			if (NUM == 2){
				ResultDegree_H = SC_RECT[1].m_resultDegree + SC_RECT[2].m_resultDegree;
				if ((ResultDegree_H <= m_dMasterH + m_dOffsetH) && (ResultDegree_H >= m_dMasterH - m_dOffsetH)){
					A_RECT[1].m_Success = true;
					A_RECT[2].m_Success = true;
				}
				if (TestMod == 0 || TestMod == 1){

					CString str = "";
					stateDATA += "수평_ ";
					str.Empty();
					str.Format("%6.2f", ResultDegree_H);
					if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE)
						m_AngleList.SetItemText(InsertIndex, 4, str);
					stateDATA += str;
					str.Empty();
					str.Format("%d", A_RECT[2].m_Success);
					if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE)
						m_AngleList.SetItemText(InsertIndex, 5, str);
				}
			}
			else if (NUM == 4){
				ResultDegree_V = SC_RECT[3].m_resultDegree + SC_RECT[4].m_resultDegree;
				if ((ResultDegree_V <= m_dMasterV + m_dOffsetV) && (ResultDegree_V >= m_dMasterV - m_dOffsetV))
				{
					A_RECT[3].m_Success = true;
					A_RECT[4].m_Success = true;
				}
				if (TestMod == 0 || TestMod == 2){
					CString str = "";
					stateDATA += "수직_ ";
					str.Empty();
					str.Format("%6.2f", ResultDegree_V);
					if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE)
						m_AngleList.SetItemText(InsertIndex, 6, str);
					stateDATA += str;
					str.Empty();
					str.Format("%d", A_RECT[4].m_Success);
					if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE)
						m_AngleList.SetItemText(InsertIndex, 7, str);
				}
			}
		}
	
		
		for(int lop=0; lop<5; lop++){
			

			if(lop ==0){
				if(A_RECT[lop].m_Success== TRUE){
						count++;
				}
				Resultcount++;
			}
			if(TestMod ==0 || TestMod ==1){
				if(lop >0 && lop < 3){
					if(A_RECT[lop].m_Success== TRUE){
							count++;
					}
					Resultcount++;
				}
				
			}
			if(TestMod ==0 || TestMod ==2){
				if(lop >2 && lop < 5){
					if(A_RECT[lop].m_Success== TRUE){
							count++;
					}
					Resultcount++;
				}
				
			}
		}
	/*	AngleKeepRun =  btn_AngleKeepRun.GetCheck();
		loop_cnt++;
	}*/

	//////////////
	if(TestMod ==0 || TestMod ==1){
		str.Empty();
		//str.Format("%3.1f", disL);
		str.Format("%3.2f", dgrL);//º
		((CImageTesterDlg *)m_pMomWnd)->m_pResAngleOptWnd->LC_VALUE(str);

		str.Empty();
		//str.Format("%3.1f", disR);
		str.Format("%3.2f", dgrR);//º
		((CImageTesterDlg *)m_pMomWnd)->m_pResAngleOptWnd->RC_VALUE(str);
		if(A_RECT[1].m_Success== TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_pResAngleOptWnd->LC_RESULT(1,"PASS");
		}else if(A_RECT[1].m_Success== FALSE){
			((CImageTesterDlg *)m_pMomWnd)->m_pResAngleOptWnd->LC_RESULT(2,"FAIL");	
		}
		
		if(A_RECT[2].m_Success== TRUE){
			((CImageTesterDlg *)m_pMomWnd)->m_pResAngleOptWnd->RC_RESULT(1,"PASS");
		}else if(A_RECT[2].m_Success== FALSE){
			((CImageTesterDlg *)m_pMomWnd)->m_pResAngleOptWnd->RC_RESULT(2,"FAIL");	
		}
	}
	if(TestMod ==0 || TestMod ==2){
		str.Empty();
		//str.Format("%3.1f", disT);
		str.Format("%3.2f", dgrT);//º
		((CImageTesterDlg *)m_pMomWnd)->m_pResAngleOptWnd->TC_VALUE(str);

		str.Empty();
		//str.Format("%3.1f", disB);
		str.Format("%3.2f", dgrB);//º
		((CImageTesterDlg *)m_pMomWnd)->m_pResAngleOptWnd->BC_VALUE(str);

		if (A_RECT[3].m_Success== TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_pResAngleOptWnd->TC_RESULT(1,"PASS");
		}else if(A_RECT[3].m_Success== FALSE){
			((CImageTesterDlg *)m_pMomWnd)->m_pResAngleOptWnd->TC_RESULT(2,"FAIL");	
		}
		
		if(A_RECT[4].m_Success== TRUE){
			((CImageTesterDlg *)m_pMomWnd)->m_pResAngleOptWnd->BC_RESULT(1,"PASS");
		}else if(A_RECT[4].m_Success== FALSE){
			((CImageTesterDlg *)m_pMomWnd)->m_pResAngleOptWnd->BC_RESULT(2,"FAIL");	
		}
	}

	

	//////////////

	retval.m_ID = 0x06;
	retval.ValString.Empty();
	retval.ValString.Format("%d개 중 %d 성공",Resultcount, count );

	if (count == Resultcount){
		retval.m_Success = TRUE;
		m_AngleList.SetItemText(InsertIndex, 8, "PASS");
		stateDATA += "PASS";
		((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_ANGLE, "PASS");
		Str_Mes[0] = "1";
	}
	else{
		retval.m_Success = FALSE;
		m_AngleList.SetItemText(InsertIndex, 8, "FAIL");
		stateDATA += "FAIL";
		((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_ANGLE, "FAIL");
		Str_Mes[0] = "0";
	}

	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
		StartCnt++;
	}

	// mes 정보 갱신
	CopyMesData();

	((CImageTesterDlg  *)m_pMomWnd)->StatePrintf(stateDATA);
	return retval;
}	

double CAngle_Option::Degree_Sum(int NUM){

	double Degree = 0;
	double Dis = 0;
	CString str = "";

	if (MasterMod == TRUE){
		Dis = A_RECT[NUM].m_resultDis;
	}
	else{
		if (NUM == 1)
			Dis = disL;
		if (NUM == 2)
			Dis = disR;
		if (NUM == 3)
			Dis = disT;
		if (NUM == 4)
			Dis = disB;
	}


	double m_Distance = (m_dPixelSize *Dis) / 1.0;
	Degree = atan((m_Distance / 1000.0) / m_dFocalLength) * 180.0 / 3.141592;

	//-20170904 추가
	if (NUM == 1 || NUM == 2)
	{
		Degree *= m_dOffset;
	}
	else{

		Degree *= m_dDistortion;
	}

// 	if ((MasterMod != TRUE) && NUM != 0){
// 		str.Empty();
// 		str.Format("%6.2f", Degree);
// 		Degree = atof(str);
// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1)
// 			m_Lot_AngleList.SetItemText(Lot_InsertIndex, 4 + NUM, str);
// 		else
// 			m_AngleList.SetItemText(InsertIndex, 3 + NUM, str);
// 	}
// 
// 	//if ((Degree <= A_RECT[NUM].m_resultDegree + A_RECT[NUM].d_Thresold) && (Degree >= A_RECT[NUM].m_resultDegree - A_RECT[NUM].d_Thresold)){
// 	if (((NUM == 1 || NUM == 2) &&(Degree <= m_dMasterH + m_dOffsetH) && (Degree >= m_dMasterH - m_dOffsetH))
// 		|| ((NUM == 3 || NUM == 4) && (Degree <= m_dMasterV + m_dOffsetV) && (Degree >= m_dMasterV - m_dOffsetV))){
// 
// 		A_RECT[NUM].m_Success = true;
// 	}
// 	else{
// 		A_RECT[NUM].m_Success = false;
// 	}
// 
	if (NUM ==0){
		Degree = 0;
	}
	if (NUM == 1)
		dgrL = Degree;
	if (NUM == 2)
		dgrR = Degree;
	if (NUM == 3)
		dgrT = Degree;
	if (NUM == 4)
		dgrB = Degree;

	return Degree;
}

void CAngle_Option::FAIL_UPLOAD()
{
	if (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)
	{
		InsertList();
		for (int t = 0; t < 4; t++)
		{
			m_AngleList.SetItemText(InsertIndex, t + 4, "X");
		}
		m_AngleList.SetItemText(InsertIndex, 8, "FAIL");
		m_AngleList.SetItemText(InsertIndex, 9, "STOP");
		((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_ANGLE, "STOP");
		b_StopFail = TRUE;
		Str_Mes[0] = "0";
		Str_Mes[1] = "0";

		m_nResult[0] = 0;
		m_nResult[1] = 0;
		StartCnt++;
	}
}

CString CAngle_Option::Mes_Result()
{
	m_szMesResult = _T("");
	if(b_StopFail == FALSE){
		m_szMesResult.Format(_T("%s:%d,%s:%d"),Str_Mes[0], m_nResult[0], Str_Mes[1], m_nResult[1]);
	}else{
		m_szMesResult.Format(_T(":1,:1"));
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_stNewMesData[NewMES_FOV] = m_szMesResult;

	return m_szMesResult;
}

void CAngle_Option::CopyMesData()
{
	if (TRUE == A_RECT[1].m_Success && TRUE == A_RECT[2].m_Success)
		m_nResult[0] = TEST_OK;
	else
		m_nResult[0] = TEST_NG;

	if (TRUE == A_RECT[3].m_Success && TRUE == A_RECT[4].m_Success)
		m_nResult[1] = TEST_OK;
	else
		m_nResult[1] = TEST_NG;

	Str_Mes[0].Format(_T("%6.2f"), ResultDegree_H);
	Str_Mes[1].Format(_T("%6.2f"), ResultDegree_V);
}

void CAngle_Option::FINAL_UPLOAD(LPCSTR lpcszString){
	if(((CImageTesterDlg *)m_pMomWnd)->LotMod ==1){
		m_AngleList.SetItemText(InsertIndex,9,lpcszString);
	}
}

void CAngle_Option::Pic(CDC *cdc)
{
	for(int lop=0;lop<5;lop++){
		if(lop ==0){
			AnglePic(cdc,lop);
		}
		if(TestMod ==0  || TestMod ==1){
			if(lop >0&& lop<3){
				AnglePic(cdc,lop);
			}
		}
		if(TestMod ==0  || TestMod ==2){
			if(lop >2&& lop<5){
				AnglePic(cdc,lop);
			}
		}
	}
}  

void CAngle_Option::InitPrm()
{
	GotoDlgCtrl(((CStatic *)GetDlgItem(IDC_STATIC)));

	Load_parameter();
	UpdateData(FALSE);

	UploadList();

}

void CAngle_Option::Automation_DATA_INPUT(){
	for(int t=0; t<5; t++){
		A_RECT[t].m_PosX=C_CHECKING[t].m_PosX;
		A_RECT[t].m_PosY=C_CHECKING[t].m_PosY;
		A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);

		if(A_RECT[t].m_Left < 0){//////////////////////수정
			A_RECT[t].m_Left = 0;
			A_RECT[t].m_Width = A_RECT[t].m_Right+1 -A_RECT[t].m_Left;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		if (A_RECT[t].m_Right >= CAM_IMAGE_WIDTH){
			A_RECT[t].m_Right = CAM_IMAGE_WIDTH-1;
			A_RECT[t].m_Width = A_RECT[t].m_Right+1 -A_RECT[t].m_Left;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		if(A_RECT[t].m_Top< 0){
			A_RECT[t].m_Top = 0;
			A_RECT[t].m_Height = A_RECT[t].m_Bottom+1 -A_RECT[t].m_Top;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		if (A_RECT[t].m_Bottom >= CAM_IMAGE_HEIGHT){
			int gap = A_RECT[t].m_Bottom - CAM_IMAGE_HEIGHT;
			A_RECT[t].m_Bottom = CAM_IMAGE_HEIGHT-1;
			A_RECT[t].m_Height = A_RECT[t].m_Bottom -A_RECT[t].m_Top -gap;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
	}

}

void CModel_Create(CAngle_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO){
	CRect OptionRect;
	if(pTabWnd != NULL){
		((CAngle_Option *)*pWnd) = new CAngle_Option(pMomWnd);
		((CAngle_Option *)*pWnd)->Setup(pMomWnd,pEdit,INFO);
		((CAngle_Option *)*pWnd)->Create(((CAngle_Option *)*pWnd)->IDD,pTabWnd);//&m_CtrTab);
		((CAngle_Option *)*pWnd)->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		((CAngle_Option *)*pWnd)->MoveWindow(20,40,OptionRect.Width(),OptionRect.Height());
	}
}

void CModel_Delete(CAngle_Option **pWnd){
	if(((CAngle_Option *)*pWnd) != NULL){
//		((CAngle_Option *)*pWnd)->KillTimer(110);
		((CAngle_Option *)*pWnd)->DestroyWindow();
		delete ((CAngle_Option *)*pWnd);
		((CAngle_Option *)*pWnd) = NULL;	
	}
}

void CAngle_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

void CAngle_Option::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",A_filename);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}
//---------------------------------------------------------------------WORKLIST
void CAngle_Option::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	InsertIndex = m_AngleList.InsertItem(StartCnt, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_AngleList.SetItemText(InsertIndex,0,strCnt);
	
	m_AngleList.SetItemText(InsertIndex, 1, ((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_AngleList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_AngleList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");

}
void CAngle_Option::LOT_InsertDataList()
{
	CString strCnt = "";

	int Index = m_Lot_AngleList.InsertItem(Lot_StartCnt, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index + 1);
	m_Lot_AngleList.SetItemText(Index, 0, strCnt);

	int CopyIndex = m_AngleList.GetItemCount() - 1;

	int count = 0;
	for (int t = 0; t < Lot_InsertNum; t++){
		if ((t != 2) && (t != 0)){
			m_Lot_AngleList.SetItemText(Index, t, m_AngleList.GetItemText(CopyIndex, count));
			count++;

		}
		else if (t == 0){
			count++;
		}
		else{
			m_Lot_AngleList.SetItemText(Index, t, ((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;

}
void CAngle_Option::Set_List(CListCtrl *List){
	

	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	//List->InsertColumn(2,"LOT CARD",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4, "HOR DEGREE", LVCFMT_CENTER, 80);
	List->InsertColumn(5, "HOR _PASS", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "VER DEGREE", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "VER_PASS", LVCFMT_CENTER, 80);
	List->InsertColumn(8,"result",LVCFMT_CENTER, 80);
	List->InsertColumn(9,"Remark",LVCFMT_CENTER, 80);

	ListItemNum=10;
	Copy_List(&m_AngleList, ((CImageTesterDlg *)m_pMomWnd)->m_pWorkList, ListItemNum);
	
	

}
void CAngle_Option::LOT_Set_List(CListCtrl *List){

	while (List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0, "NO", LVCFMT_CENTER, 30);
	List->InsertColumn(1, "모델명", LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT CARD",LVCFMT_CENTER, 100);
	List->InsertColumn(3, "Start DATE", LVCFMT_CENTER, 80);
	List->InsertColumn(4, "Start TIME", LVCFMT_CENTER, 80);
	List->InsertColumn(5, "HOR DEGREE", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "HOR_PASS", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "VER DEGREE", LVCFMT_CENTER, 80);
	List->InsertColumn(8, "VER_PASS", LVCFMT_CENTER, 80);
	List->InsertColumn(9, "result", LVCFMT_CENTER, 80);
	List->InsertColumn(10, "Remark", LVCFMT_CENTER, 80);
	Lot_InsertNum = 11;
}
void CAngle_Option::EXCEL_SAVE(){
	CString Item[4] = { "HOR DEGREE", "HOR_PASS", "VER DEGREE", "VER_PASS" };
	CString Data = "";
	CString str = "";
	str = "***********************화각 검사 [ 단위 : º ] ***********************\n";//º
	Data += str;
	str.Empty();
	str.Format("Horizon, %0.2f ,Threshold, ± %0.2f º,\n", m_dMasterH, m_dOffsetH);
	Data += str;
	str.Empty();
	str.Format("Vertical , %0.2f ,Threshold, ± %0.2f º, \n", m_dMasterV, m_dOffsetV);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("화각검사",Item,4,&m_AngleList,Data);
}
void CAngle_Option::LOT_EXCEL_SAVE()
{
	CString Item[4] = { "HOR DEGREE", "HOR_PASS", "VER DEGREE", "VER_PASS" };
	CString Data = "";
	CString str = "";
	str = "***********************화각 검사 [ 단위 : º ] ***********************\n";//º
	Data += str;
	str.Empty();
	str.Format("Horizon, %0.2f ,Threshold, ± %0.2f º,\n", m_dMasterH, m_dOffsetH);
	Data += str;
	str.Empty();
	str.Format("Vertical , %0.2f ,Threshold, ± %0.2f º, \n", m_dMasterV, m_dOffsetV);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("화각검사", Item, 4, &m_Lot_AngleList, Data);
}
bool CAngle_Option::EXCEL_UPLOAD(){

	m_AngleList.DeleteAllItems();
	if (((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("화각검사", &m_AngleList, 1, &StartCnt)){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return TRUE;
}
bool CAngle_Option::LOT_EXCEL_UPLOAD()
{
	m_Lot_AngleList.DeleteAllItems();
	if (((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("화각검사", &m_Lot_AngleList, 1, &Lot_StartCnt) == TRUE){
		return TRUE;
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;
	}
	return TRUE;
}
void CAngle_Option::OnBnClickedCheckVer()
{
	
	UpdateData(TRUE);

	if (TestModVer == 1 && TestModHor == 1){
		TestMod = 0;
	}
	else if (TestModVer == 0 && TestModHor == 1){
		TestMod = 1;
	}
	else if (TestModVer == 1 && TestModHor == 0){
		TestMod = 2;
	}

	CString strTitle="";
	CString str="";
	strTitle.Empty();
	strTitle="ANGLE_INIT";
	str.Empty();
	str.Format("%d",TestMod);
	WritePrivateProfileString(str_model,strTitle+"TestMod",str,A_filename);

	UpdateData(FALSE);
	
}

void CAngle_Option::OnBnClickedCheckHor()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	if (TestModVer == 1 && TestModHor == 1){
		TestMod = 0;
	}
	else if (TestModVer == 0 && TestModHor == 1){
		TestMod = 1;
	}
	else if (TestModVer == 1 && TestModHor == 0){
		TestMod = 2;
	}

	CString strTitle="";
	CString str="";
	strTitle.Empty();
	strTitle="ANGLE_INIT";
	str.Empty();
	str.Format("%d",TestMod);
	WritePrivateProfileString(str_model,strTitle+"TestMod",str,A_filename);


	UpdateData(FALSE);
	
}

void CAngle_Option::OnBnClickedCheckAuto()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
		if(AutomationMod ==0){
		
		AutomationMod =1;
		//A_DATALIST.EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_PosX)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_PosY)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_Dis_W)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_Dis_H)->EnableWindow(0);
		/*(CEdit *)GetDlgItem(IDC_Angle_Width)->EnableWindow(0);
		
		(CEdit *)GetDlgItem(IDC_Angle_Height)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_Thresold)->EnableWindow(0);
		*/
//		(CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE)->EnableWindow(0);
		//(CButton *)GetDlgItem(IDC_BUTTON_setAngleZone)->EnableWindow(0);
		
		//////////////////////////////////////////////////////////////////////오토메이션 중심점으로 영역 잡는 소스 추가해야함.
	}
	else{
		AutomationMod =0;
		
		//A_DATALIST.EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_PosX)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_PosY)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_Dis_W)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_Dis_H)->EnableWindow(1);
		/*(CEdit *)GetDlgItem(IDC_Angle_Width)->EnableWindow(1);
		
		(CEdit *)GetDlgItem(IDC_Angle_Height)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_Thresold)->EnableWindow(1);
		*/
//		(CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE)->EnableWindow(1);
		//(CButton *)GetDlgItem(IDC_BUTTON_setAngleZone)->EnableWindow(1);
	
	}

		for(int t=0; t<3; t++){
			A_DATALIST.Update(t);
		}
	CString strTitle;
	strTitle.Empty();
	strTitle="ANGLE_INIT";
	if(AutomationMod ==1){
		WritePrivateProfileString(str_model,strTitle+"Auto","1",A_filename);}
	if(AutomationMod ==0){
		WritePrivateProfileString(str_model,strTitle+"Auto","0",A_filename);}

}

void CAngle_Option::GetSideCircleCoordinate(LPBYTE IN_RGB)
{
	IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);

	BYTE R,G,B;
	double Sum_Y;

	for (int y = 0; y<CAM_IMAGE_HEIGHT; y++)
	{
		for (int x = 0; x<CAM_IMAGE_WIDTH; x++)
		{
			B = IN_RGB[y*(CAM_IMAGE_WIDTH * 3) + x * 3];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH * 3) + x * 3 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH * 3) + x * 3 + 2];
			
			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;			
		}
	}
	
//	Capture_Gray_SAVE("C:\\RGBIMAGE", IN_RGB, CAM_IMAGE_WIDTH, 480);	
//	cvSaveImage("D:\\Default_Image.bmp", OriginImage);

	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	//cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0); //객체 외곽의 Overshooting으로 인하여 한번 더 처리 함..

	cvCanny(SmoothImage, CannyImage, 0, 255);
//	cvDilate(CannyImage,CannyImage);
	
	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);
	//cvSaveImage("D:\\Default_Image.bmp", RGBResultImage);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;
	CvRect rect;
	double area=0, arcCount=0;
	double old_dist = 999999;
	
	SC_RECT[0].m_Success = FALSE;

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
	{
		area = cvContourArea(temp_contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(temp_contour, CV_WHOLE_SEQ, -1);
		
		double circularity = (4.0*3.14*area)/(arcCount*arcCount);
		
		if(circularity > 0.5)
		{			
			rect = cvContourBoundingRect(temp_contour, 1); 

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;
			
			if (center_x > CAM_IMAGE_WIDTH/2 -90 && center_x < CAM_IMAGE_WIDTH/2 +90 && center_y > CAM_IMAGE_HEIGHT/2 -80 && center_y < CAM_IMAGE_HEIGHT/2 +80)
			{
				if (rect.width < CAM_IMAGE_WIDTH / 2 && rect.height < CAM_IMAGE_HEIGHT / 2)
				{
					double st_circle_center_x = Standard_SC_RECT[0].m_resultX;
					double st_circle_center_y = Standard_SC_RECT[0].m_resultY;
					double curr_circle_center_x = rect.x + rect.width/2;
					double curr_circle_center_y = rect.y + rect.height/2;

					double dist = GetDistance(st_circle_center_x, st_circle_center_y, curr_circle_center_x, curr_circle_center_y);
					
					if(dist < old_dist && dist < 120)
					{
						SC_RECT[0].m_resultX = curr_circle_center_x;
						SC_RECT[0].m_resultY = curr_circle_center_y;

						old_dist = dist;

						SC_RECT[0].m_Success = TRUE;
						cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 0), 1, 8);  

					}
				}
			}
			else
			{
				counter++;
			}
		}		
	}
	
	int total_circle_count = counter;
	CvRect *rectArray = new CvRect[counter];
	counter = 0;
	
	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);		
	
		double circularity = (4.0*3.14*area)/(arcCount*arcCount);		

		if(circularity > 0.8)
		{			
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;		

			if (center_x > CAM_IMAGE_WIDTH / 2 - 90 && center_x < CAM_IMAGE_WIDTH / 2 + 90 && center_y > CAM_IMAGE_HEIGHT / 2 - 80 && center_y < CAM_IMAGE_HEIGHT / 2 + 80)
			{

			}
			else
			{
				cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 3, 8);  

				rectArray[counter] = rect;
				counter++;
			}
		}
	}
	
	old_dist = 999999;

	for (int i = 1; i<5; i++)
	{
		SC_RECT[i].m_Success = FALSE;

		for(int j=0; j<total_circle_count; j++)
		{
			rect = rectArray[j];
			
			double st_circle_center_x = A_RECT[i].m_PosX;
			double st_circle_center_y = A_RECT[i].m_PosY;
			double curr_circle_center_x = rect.x + rect.width/2;
			double curr_circle_center_y = rect.y + rect.height/2;

			double dist = GetDistance(st_circle_center_x, st_circle_center_y, curr_circle_center_x, curr_circle_center_y);

			if(dist < old_dist && dist < 150)
			{	
				int total_R_Value = 0;
				int total_G_Value = 0;
				int total_B_Value = 0;
				int R, G, B, R_Avg, G_Avg, B_Avg;

				for(int k=rect.y; k<rect.y+rect.height; k++)
				{
					for(int q=rect.x; q<rect.x+rect.width; q++)
					{
						B = (unsigned char)IN_RGB[k*(CAM_IMAGE_WIDTH * 3) + q * 3 + 0];
						G = (unsigned char)IN_RGB[k*(CAM_IMAGE_WIDTH * 3) + q * 3 + 1];
						R = (unsigned char)IN_RGB[k*(CAM_IMAGE_WIDTH * 3) + q * 3 + 2];
// 						total_R_Value += R;
// 						total_G_Value += G;
// 						total_B_Value += B;
					}
				}
				
// 				R_Avg = total_R_Value/(rect.width*rect.height);
// 				G_Avg = total_G_Value/(rect.width*rect.height);
// 				B_Avg = total_B_Value/(rect.width*rect.height);

// 				if(R_Avg > G_Avg+B_Avg)
// 				{
					SC_RECT[i].m_resultX = curr_circle_center_x;
					SC_RECT[i].m_resultY = curr_circle_center_y;

					old_dist = dist;

					SC_RECT[i].m_Success = TRUE;
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
//				}
				
			}
			
		}

		A_RECT[i].m_PosX = SC_RECT[i].m_resultX;
		A_RECT[i].m_PosY = SC_RECT[i].m_resultY;
		A_RECT[i].EX_RECT_SET(A_RECT[i].m_PosX, A_RECT[i].m_PosY, A_RECT[i].m_Width, A_RECT[i].m_Height);

		old_dist = 999999;
	}

	//cvSaveImage("D:\\test\\test_bbb.bmp", RGBResultImage);
	CvPoint pt1 = GetCenterPoint(IN_RGB);

// 	SC_RECT[0].m_resultX = pt1.x;
// 	SC_RECT[0].m_resultY = pt1.y;
	A_RECT[0].m_PosX = pt1.x;
	A_RECT[0].m_PosY = pt1.y;
	A_RECT[0].EX_RECT_SET(A_RECT[0].m_PosX, A_RECT[0].m_PosY, A_RECT[0].m_Width, A_RECT[0].m_Height);

// 	for(int i=1; i<5; i++)
// 	{
// 		if(SC_RECT[i].m_Success == FALSE)
// 		{
// 
// // 			SC_RECT[i].m_resultX = Standard_SC_RECT[i].m_resultX;
// // 			SC_RECT[i].m_resultY = Standard_SC_RECT[i].m_resultY;
// 
// 
// 			
// 			//cvRectangle(RGBResultImage, cvPoint(SC_RECT[i].m_resultX-2, SC_RECT[i].m_resultY-2), cvPoint(SC_RECT[i].m_resultX+2, SC_RECT[i].m_resultY+2), CV_RGB(255, 0, 255), 4, 8);  
// 
// 		}
// 	}





	//cvSaveImage("D:\\SideCircleResultImage.bmp", RGBResultImage);
	
	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&CannyImage);	
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);	
	
	UploadList();
	delete []rectArray;
}

double CAngle_Option::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt( (double)((x2-x1)*(x2-x1)) +  ((y2-y1)*(y2-y1)));

	return result;
}
void CAngle_Option::OnNMCustomdrawListAngle(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//*pResult = 0;

	 COLORREF text_color = 0;
	 COLORREF bg_color = RGB(255, 255, 255);
     /////////////////////////////////////////////////////////////default 컬러

     LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
		
	//	lplvcd->iPartId
        switch(lplvcd->nmcd.dwDrawStage){
            case CDDS_PREPAINT:
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
             
            // 배경 혹은 텍스트를 수정한다.
            case CDDS_ITEMPREPAINT:
                // 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
               /* if(lplvcd->nmcd.dwItemSpec == 0) text_color = RGB(255, 0, 0);
                else if(lplvcd->nmcd.dwItemSpec == 1) text_color = RGB(0, 255, 0);
                else text_color = RGB(0, 0, 255);
                lplvcd->clrText = text_color;*/
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
           
            // 서브 아이템의 배경 혹은 텍스트를 수정한다.
            case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
                if(lplvcd->iSubItem != 0){
                    // 1번째 행이라면...

					if(lplvcd->nmcd.dwItemSpec >=0 ||lplvcd->nmcd.dwItemSpec <5){
						if(AutomationMod ==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 7 ){
								text_color = RGB(0, 0, 0);
								bg_color = RGB(200, 200, 200);
							}
						}
						else{
							text_color = RGB(0, 0, 0);
							bg_color = RGB(255, 255, 255);
						
						}

						if(ChangeCheck==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 11 ){
								for(int t=0; t<5; t++){
									if(lplvcd->nmcd.dwItemSpec == ChangeItem[t]){
										text_color = RGB(0, 0, 0);
										bg_color = RGB(250, 230, 200);					
									}
								}
							}
						
						
						}
					
					}
					

				    lplvcd->clrText = text_color;
                    lplvcd->clrTextBk = bg_color;
					
                }
                *pResult = CDRF_NEWFONT;
                return;
        }
}


void CAngle_Option::OnEnKillfocusEditAMod()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	ChangeCheck=1;
	if((iSavedItem != -1)&&(iSavedSubitem != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_A_MOD, str));
		List_COLOR_Change();
		if(A_DATALIST.GetItemText(iSavedItem, iSavedSubitem) != str){
			Change_DATA();
			UploadList();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );
	iSavedItem = -1;
	iSavedSubitem = -1;
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	
}

void CAngle_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
		AngleKeepRun = FALSE;
//		((CImageTesterDlg *)m_pMomWnd)->keepRun = FALSE;
		UpdateData(FALSE);
	}
}

void CAngle_Option::OnBnClickedButtonALoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Load_parameter();
	UpdateData(FALSE);
	UploadList();
}

void CAngle_Option::OnBnClickedCheck1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	TestModALL=1;
	TestMod =0;
	TestModHor=0;
	TestModVer=0;

	CString strTitle="";
	CString str="";
	strTitle.Empty();
	strTitle="ANGLE_INIT";
	str.Empty();
	str.Format("%d",TestMod);
	WritePrivateProfileString(str_model,strTitle+"TestMod",str,A_filename);

	UpdateData(FALSE);
}

//void CAngle_Option::OnBnClickedCheckAngleKeeprun()
//{
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	AngleKeepRun = btn_AngleKeepRun.GetCheck();
//	UpdateData(FALSE);
//	if(AngleKeepRun == TRUE){
//		AutomationMod =1;
//		OnBnClickedCheckAuto();
//		(CButton *)GetDlgItem(IDC_CHECK_AUTO)->EnableWindow(0);
//		CheckDlgButton(IDC_CHECK_AUTO,FALSE);
////		((CImageTesterDlg *)m_pMomWnd)->keepRun = TRUE; 
////		((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->KeepRun();
//
//	}
//	else{
////		((CImageTesterDlg *)m_pMomWnd)->keepRun = FALSE;
//		(CButton *)GetDlgItem(IDC_CHECK_AUTO)->EnableWindow(1);
//	}
//	
//}

void CAngle_Option::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CAngle_Option::OnKillFocus(CWnd* pNewWnd)
{
	CDialog::OnKillFocus(pNewWnd);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	
}



BOOL CAngle_Option::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	int znum = zDelta/120;
	CWnd *focusid;
	focusid = GetFocus();
	bool FLAG = FALSE;
	if(znum > 0 ){
		FLAG = 1;
	}
	else if(znum < 0 ){
		FLAG =0;
	}
	else if(znum == 0){
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	}
	int casenum = focusid->GetDlgCtrlID();
	switch(casenum){
		case IDC_Angle_PosX   :
			EditWheelIntSet(IDC_Angle_PosX ,znum);
			OnEnChangeAnglePosx();
			break;
		case IDC_Angle_PosY :
			EditWheelIntSet(IDC_Angle_PosY,znum);
			OnEnChangeAnglePosy();
			break;
		case IDC_Angle_Dis_W :
			EditWheelIntSet(IDC_Angle_Dis_W ,znum);
			OnEnChangeAngleDisW();
			break;
		case IDC_Angle_Dis_H :
			EditWheelIntSet(IDC_Angle_Dis_H,znum);
			OnEnChangeAngleDisH();
			break;
		case  IDC_Angle_Width :
			EditWheelIntSet( IDC_Angle_Width,znum);
			OnEnChangeAngleWidth();
			break;
		case IDC_Angle_Height :
			EditWheelIntSet(IDC_Angle_Height,znum);
			OnEnChangeAngleHeight();
			break;
		case IDC_Angle_Thresold :
			EditWheelIntSet(IDC_Angle_Thresold,znum);
			OnEnChangeAngleThresold();
			break;
		
		case IDC_EDIT_A_MOD :
		if (iSavedSubitem !=9 && iSavedSubitem !=10 ){
			if(FLAG == 1){
				if((Change_DATA_CHECK(1) == TRUE) /*|| (znum ==-1)(WheelCheck ==0)*/){
					EditWheelIntSet(IDC_EDIT_A_MOD,znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditAMod();
					//WheelCheck =1;
				}
			}
			else if(FLAG == 0){
				if((Change_DATA_CHECK(0) == TRUE)/* || (znum == 1)(WheelCheck ==1)*/){
					EditWheelIntSet(IDC_EDIT_A_MOD,znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditAMod();
					//WheelCheck = 0;
				}
			}
		}
		else{
			CString str_buf = "";
			double buf = 0;
			int retval = 0;
			GetDlgItemText(IDC_EDIT_A_MOD, str_buf);
			str_buf.Remove(' ');
			if (str_buf == ""){
				buf = 0;
			}
			else{
				GetDlgItemText(IDC_EDIT_A_MOD, str_buf);
				buf = atof(str_buf);
				buf += (double)znum / 10;
			}
			str_buf.Format("%0.2f", buf);
			SetDlgItemText(IDC_EDIT_A_MOD, str_buf);
			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetSel(-2, -1);
			Change_DATA();
			UploadList();
			OnEnChangeEditAMod();
		}

			break;
	}
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

void CAngle_Option::EditWheelIntSet(int nID,int Val)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = 0;
	}else{
		buf = GetDlgItemInt(nID);
		buf+= Val;
	}
	SetDlgItemInt(nID,buf,1);
	((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
}
void CAngle_Option::EditMinMaxIntSet(int nID,int* Val,int Min,int Max)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = Min;
		retval = FALSE;
	}else{
		buf = GetDlgItemInt(nID);
		if(buf<Min){
			buf = *Val;
			retval = TRUE;
		}else if(buf > Max){
			buf = *Val;
			retval = TRUE;
		}else{
			retval = FALSE;
		}
	}
	*Val = buf;
	if(retval == TRUE){
		SetDlgItemInt(nID,buf,1);
		((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
	}
}

void CAngle_Option::OnEnChangeAnglePosx()
{
	EditMinMaxIntSet(IDC_Angle_PosX, &A_Total_PosX, 0, CAM_IMAGE_WIDTH);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);

	if ((A_Total_PosX >= 0) && (A_Total_PosX <= CAM_IMAGE_WIDTH) &&
		(A_Total_PosY >= 0) && (A_Total_PosY <= CAM_IMAGE_HEIGHT) &&
		(A_Dis_Width >= 0) && (A_Dis_Width <= CAM_IMAGE_WIDTH) &&
		(A_Dis_Height >= 0) && (A_Dis_Height <= CAM_IMAGE_HEIGHT) &&
		(A_Total_Width >= 0) && (A_Total_Width <= CAM_IMAGE_WIDTH) &&
		(A_Total_Height >= 0) && (A_Total_Height <= CAM_IMAGE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CAngle_Option::OnEnChangeAnglePosy()
{
	EditMinMaxIntSet(IDC_Angle_PosY, &A_Total_PosY, 0, CAM_IMAGE_HEIGHT);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);

	if ((A_Total_PosX >= 0) && (A_Total_PosX <= CAM_IMAGE_WIDTH) &&
		(A_Total_PosY >= 0) && (A_Total_PosY <= CAM_IMAGE_HEIGHT) &&
		(A_Dis_Width >= 0) && (A_Dis_Width <= CAM_IMAGE_WIDTH) &&
		(A_Dis_Height >= 0) && (A_Dis_Height <= CAM_IMAGE_HEIGHT) &&
		(A_Total_Width >= 0) && (A_Total_Width <= CAM_IMAGE_WIDTH) &&
		(A_Total_Height >= 0) && (A_Total_Height <= CAM_IMAGE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CAngle_Option::OnEnChangeAngleDisW()
{
	EditMinMaxIntSet(IDC_Angle_Dis_W, &A_Dis_Width, 0, CAM_IMAGE_WIDTH);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);



	if ((A_Total_PosX >= 0) && (A_Total_PosX <= CAM_IMAGE_WIDTH) &&
		(A_Total_PosY >= 0) && (A_Total_PosY <= CAM_IMAGE_HEIGHT) &&
		(A_Dis_Width >= 0) && (A_Dis_Width <= CAM_IMAGE_WIDTH) &&
		(A_Dis_Height >= 0) && (A_Dis_Height <= CAM_IMAGE_HEIGHT) &&
		(A_Total_Width >= 0) && (A_Total_Width <= CAM_IMAGE_WIDTH) &&
		(A_Total_Height >= 0) && (A_Total_Height <= CAM_IMAGE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CAngle_Option::OnEnChangeAngleDisH()
{
	EditMinMaxIntSet(IDC_Angle_Dis_H, &A_Dis_Height, 0, CAM_IMAGE_HEIGHT);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);

	if ((A_Total_PosX >= 0) && (A_Total_PosX <= CAM_IMAGE_WIDTH) &&
		(A_Total_PosY >= 0) && (A_Total_PosY <= CAM_IMAGE_HEIGHT) &&
		(A_Dis_Width >= 0) && (A_Dis_Width <= CAM_IMAGE_WIDTH) &&
		(A_Dis_Height >= 0) && (A_Dis_Height <= CAM_IMAGE_HEIGHT) &&
		(A_Total_Width >= 0) && (A_Total_Width <= CAM_IMAGE_WIDTH) &&
		(A_Total_Height >= 0) && (A_Total_Height <= CAM_IMAGE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CAngle_Option::OnEnChangeAngleWidth()
{
	EditMinMaxIntSet(IDC_Angle_Width, &A_Total_Width, 0, CAM_IMAGE_WIDTH);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);

	if ((A_Total_PosX >= 0) && (A_Total_PosX <= CAM_IMAGE_WIDTH) &&
		(A_Total_PosY >= 0) && (A_Total_PosY <= CAM_IMAGE_HEIGHT) &&
		(A_Dis_Width >= 0) && (A_Dis_Width <= CAM_IMAGE_WIDTH) &&
		(A_Dis_Height >= 0) && (A_Dis_Height <= CAM_IMAGE_HEIGHT) &&
		(A_Total_Width >= 0) && (A_Total_Width <= CAM_IMAGE_WIDTH) &&
		(A_Total_Height >= 0) && (A_Total_Height <= CAM_IMAGE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CAngle_Option::OnEnChangeAngleHeight()
{
	EditMinMaxIntSet(IDC_Angle_Height, &A_Total_Height, 0, CAM_IMAGE_HEIGHT);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);

	if ((A_Total_PosX >= 0) && (A_Total_PosX <= CAM_IMAGE_WIDTH) &&
		(A_Total_PosY >= 0) && (A_Total_PosY <= CAM_IMAGE_HEIGHT) &&
		(A_Dis_Width >= 0) && (A_Dis_Width <= CAM_IMAGE_WIDTH) &&
		(A_Dis_Height >= 0) && (A_Dis_Height <= CAM_IMAGE_HEIGHT) &&
		(A_Total_Width >= 0) && (A_Total_Width <= CAM_IMAGE_WIDTH) &&
		(A_Total_Height >= 0) && (A_Total_Height <= CAM_IMAGE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CAngle_Option::OnEnChangeAngleThresold()
{
// 	EditMinMaxIntSet(IDC_Angle_Thresold, &A_Thresold,0,200);
// 	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
// 	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
// 	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
// 	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
// 	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
// 	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);
// 
// 	if ((A_Total_PosX >= 0) && (A_Total_PosX <= CAM_IMAGE_WIDTH) &&
// 		(A_Total_PosY >= 0) && (A_Total_PosY <= CAM_IMAGE_HEIGHT) &&
// 		(A_Dis_Width >= 0) && (A_Dis_Width <= CAM_IMAGE_WIDTH) &&
// 		(A_Dis_Height >= 0) && (A_Dis_Height <= CAM_IMAGE_HEIGHT) &&
// 		(A_Total_Width >= 0) && (A_Total_Width <= CAM_IMAGE_WIDTH) &&
// 		(A_Total_Height >= 0) && (A_Total_Height <= CAM_IMAGE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

// 	}else{
// 		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
// 	}
}

void CAngle_Option::OnEnChangeEditAMod()
{
	int num = iSavedItem;
	if ((A_RECT[num].m_PosX >= 0) && (A_RECT[num].m_PosX <= CAM_IMAGE_WIDTH) &&
		(A_RECT[num].m_PosY >= 0) && (A_RECT[num].m_PosY <= CAM_IMAGE_HEIGHT) &&
		(A_RECT[num].m_Left >= 0) && (A_RECT[num].m_Left <= CAM_IMAGE_WIDTH) &&
		(A_RECT[num].m_Top >= 0) && (A_RECT[num].m_Top <= CAM_IMAGE_HEIGHT) &&
		(A_RECT[num].m_Right >= 0) && (A_RECT[num].m_Right <= CAM_IMAGE_WIDTH) &&
		(A_RECT[num].m_Bottom >= 0) && (A_RECT[num].m_Bottom <= CAM_IMAGE_HEIGHT) &&
		(A_RECT[num].m_Width >= 0) && (A_RECT[num].m_Width <= CAM_IMAGE_WIDTH) &&
		(A_RECT[num].m_Height >= 0) && (A_RECT[num].m_Height <= CAM_IMAGE_HEIGHT) &&
		(A_RECT[num].m_Thresold>=0)&&(A_RECT[num].m_Thresold<=200)){

			((CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE))->EnableWindow(1);
		

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE))->EnableWindow(0);
	}
}

void CAngle_Option::OnLvnItemchangedListAngle(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}

CvPoint CAngle_Option::GetCenterPoint(LPBYTE IN_RGB)
{	
	CvPoint resultPt = cvPoint(-99999, -99999);

	IplImage *OriginImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *PreprecessedImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
	
	IplImage *RGBOrgImage = cvCreateImage(cvSize(CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT), IPL_DEPTH_8U, 3);

	BYTE R,G,B;
	double Sum_Y;

	for (int y=0; y<CAM_IMAGE_HEIGHT; y++)
	{
		for (int x=0; x<CAM_IMAGE_WIDTH; x++)
		{

			B = IN_RGB[y*(CAM_IMAGE_WIDTH * 3) + x * 3];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH * 3) + x * 3 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH * 3) + x * 3 + 2];

			if( R < 150 && G < 150 && B < 150)
				OriginImage->imageData[y*OriginImage->widthStep+x] = 255;
			else
				OriginImage->imageData[y*OriginImage->widthStep+x] = 0;

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}	
	
//	cvSaveImage("C:\\CenterImage.bmp", OriginImage);
//	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
//	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);

	cvCopyImage(OriginImage, SmoothImage);
	
	cvCanny(SmoothImage, CannyImage, 0, 255);

	cvDilate(CannyImage, CannyImage);
	
	cvCopyImage(CannyImage, temp_PatternImage);
	
	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);
	
	//cvSaveImage("D:\\test\\CenterImage.bmp", CannyImage);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
		counter++;
	
	if(counter == 0)
		return resultPt;

	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area=0, arcCount=0;
	counter = 0;
	double old_dist = 999999;
	
	int old_center_pos_x = 0;
	int old_center_pos_y = 0;
	int center_pt_x = 0;
	int center_pt_y = 0;
	int obj_Cnt = 0;
	int size=0, old_size = 0;
	
	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;

		double circularity = (4.0*3.14*area)/(arcCount*arcCount);
		
		if(circularity > 0.7)
		{		
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;

			if (center_x > CAM_IMAGE_WIDTH / 2 - 90 && center_x < CAM_IMAGE_WIDTH / 2 + 90 && center_y > CAM_IMAGE_HEIGHT / 2 - 80 && center_y < CAM_IMAGE_HEIGHT / 2 +80)
			{
				if(rect.width < CAM_IMAGE_WIDTH/2 && rect.height < CAM_IMAGE_HEIGHT/2 && rect.width > 20 && rect.height > 20)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
					
					/*center_pt_x = center_pt_x+ center_x;
					center_pt_y = center_pt_y+ center_y;
					obj_Cnt++;

					old_center_pos_x = center_pt_x/obj_Cnt;
					old_center_pos_y = center_pt_y/obj_Cnt;*/
					
					double distance = GetDistance(rect.x + rect.width / 2, rect.y + rect.height / 2, CAM_IMAGE_WIDTH / 2, CAM_IMAGE_HEIGHT / 2);

					obj_Cnt++;
					
					size = rect.width * rect.height;
					if(distance < old_dist)
					{
						old_size = size;
						resultPt.x = center_x;
						resultPt.y = center_y;

						old_dist = distance;
					}

				//	cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 0), 1, 8);  
				
				}
			}
		}
		
		counter++;
	}
	//cvSaveImage("D:\\RGBResultImage_C.bmp", RGBResultImage);
	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
	cvReleaseImage(&RGBOrgImage);
	cvReleaseImage(&PreprecessedImage);
	cvReleaseImage(&CannyImage);

	delete []rectArray;
	delete []areaArray;
	
	/*if(obj_Cnt != 0)
	{
		resultPt.x = center_pt_x / obj_Cnt;
		resultPt.y = center_pt_y / obj_Cnt;
	}*/
/*	if(obj_Cnt != 0)
	{
		resultPt.x = center_pt_x;
		resultPt.y = center_pt_y;
	}*/
		
	return resultPt;	
}

void CAngle_Option::OnBnClickedBtnEtcSave()
{
	CString str_Buf = "";
	CString strTitle = "ANGLE_INIT";

	((CEdit *)GetDlgItem(IDC_EDIT_ANGLE_OFFSET))->GetWindowText(str_Buf);
	WritePrivateProfileString(str_model, strTitle + "OFFSET", str_Buf, A_filename);
	m_dOffset = atof(str_Buf);
	((CEdit *)GetDlgItem(IDC_EDIT_PIXEL_SIZE))->GetWindowText(str_Buf);
	WritePrivateProfileString(str_model, strTitle + "PIXEL_SIZE", str_Buf, A_filename);
	m_dPixelSize = atof(str_Buf);
	((CEdit *)GetDlgItem(IDC_EDIT_FOCAL_LENGTH))->GetWindowText(str_Buf);
	WritePrivateProfileString(str_model, strTitle + "FOCAL_LENGTH", str_Buf, A_filename);
	m_dFocalLength = atof(str_Buf);
	((CEdit *)GetDlgItem(IDC_EDIT_DISTORTION))->GetWindowText(str_Buf);
	WritePrivateProfileString(str_model, strTitle + "DISTORTION", str_Buf, A_filename);
	m_dDistortion = atof(str_Buf);

}


void CAngle_Option::OnBnClickedButtonSaveH()
{
	CString str_Buf = "";
	CString strTitle = "ANGLE_INIT";

	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_H))->GetWindowText(str_Buf);
	WritePrivateProfileString(str_model, strTitle + "MASTER_H", str_Buf, A_filename);
	m_dMasterH = atof(str_Buf);
	((CEdit *)GetDlgItem(IDC_EDIT_OFFSET_H))->GetWindowText(str_Buf);
	WritePrivateProfileString(str_model, strTitle + "OFFSET_H", str_Buf, A_filename);
	m_dOffsetH = atof(str_Buf);

	str_Buf.Format("%6.2f", m_dMasterH);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_H))->SetWindowText(str_Buf);

	str_Buf.Format("%6.2f", m_dOffsetH);
	((CEdit *)GetDlgItem(IDC_EDIT_OFFSET_H))->SetWindowText(str_Buf);

}


void CAngle_Option::OnBnClickedButtonSaveV()
{
	CString str_Buf = "";
	CString strTitle = "ANGLE_INIT";

	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_V))->GetWindowText(str_Buf);
	WritePrivateProfileString(str_model, strTitle + "MASTER_V", str_Buf, A_filename);
	m_dMasterV = atof(str_Buf);
	((CEdit *)GetDlgItem(IDC_EDIT_OFFSET_V))->GetWindowText(str_Buf);
	WritePrivateProfileString(str_model, strTitle + "OFFSET_V", str_Buf, A_filename);
	m_dOffsetV = atof(str_Buf);


	str_Buf.Format("%6.2f", m_dMasterV);
	((CEdit *)GetDlgItem(IDC_EDIT_MASTER_V))->SetWindowText(str_Buf);

	str_Buf.Format("%6.2f", m_dOffsetV);
	((CEdit *)GetDlgItem(IDC_EDIT_OFFSET_V))->SetWindowText(str_Buf);

}
