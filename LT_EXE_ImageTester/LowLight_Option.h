#pragma once
#include "ETCUSER.H"
#include "afxcmn.h"


// CLowLight_Option 대화 상자입니다.

class CLowLight_Option : public CDialog
{
	DECLARE_DYNAMIC(CLowLight_Option)

public:
	CLowLight_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CLowLight_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_LOWLIGHT };

	
	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);

	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();

	void	InitEVMS();
	void	FAIL_UPLOAD();
	BOOL	b_StopFail;
	void	Set_List(CListCtrl *List);
	void	InsertList();

	int		InsertIndex;
	int		ListItemNum;

	void	EXCEL_SAVE();
	bool	EXCEL_UPLOAD();
	int		StartCnt;

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

	int		Lot_StartCnt;
	int		Lot_InsertNum;

	tResultVal Run(void);
	void	InitPrm();

	void	StatePrintf(LPCSTR lpcszString, ...);
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);


	////////////////////////////////

	int		m_Count_Dust[255];
	float	m_Concentration[255];
	int		m_i_DustNumOfSector[3];
	double	m_d_Thr[3];
	double	m_d_OvershootingThr[3];
	int		m_CaptureNum;
	int		m_Black_STEP;
	int		m_PASSNUM;
	int		m_Separation_V;
	int		m_Brightness;
	int		m_Contrast;
	BOOL	m_b_Ellipse[3];
	int		m_RemovePic;

	CRectData Test_RECT[3];
	CFont	font1,font2;

	BOOL	particle_start;
	int		ParticleCnt;

	void	Rect_SETLIST();
	void	Change_DATA();
	void	List_COLOR_Change(int itemnum);
	void	EditWheelIntSet(int nID,int Val);
	bool	Change_DATA_CHECK(bool FLAG);
	void	Rect_InsertList();
	void	Load_parameter();
	void	Save_parameter();
	void	Init_Stand();

	bool	ChangeCheck;
	int		changecount;
	int		ChangeItem[4];
	int		iSavedItem_click;
	int		iSavedItem, iSavedSubitem;
	
	CRectData P_RECT[256];
	CRectData R_RECT[1];
	CRectData FULL_RECT[1];
	CRectData MIDDLE_RECT[1];

	BYTE	m_Expand_OverlayArea[720][480];
	void	FULL_RegionSUM();
	void	DustFilterSum();
//	BOOL	Run();

	BYTE	m_CaptureBuf[720 * 480 * 4];
	char	m_Area[720][480];
	BYTE	m_DustFilter[720][480];

	void	CaptureImage();
	bool	ParticleGen(LPBYTE IN_RGB,int NUM);
	bool	ParticlePic(CDC *cdc,int NUM);
	void	Pic(CDC *cdc);
	bool	ParticlePic_New(CDC *cdc,int NUM);

	double m_Lens_Shading_W;
	int m_DustNumOfSector;
	double P_Black_Threshold;

	double m_Lens_Shading_W_B;
	int m_DustNumOfSector_B;
	double P_Black_Threshold_B;

	double m_Lens_Shading_W_C;
	int m_DustNumOfSector_C;
	double P_Black_Threshold_C;

	BOOL m_Ellipse;
	BOOL m_Ellipse_MIDDLE;

	int i_DustDis[3];
	int DustRoiX;
	int DustRoiY;

	double  Distance(double  x1, double y1, double x2, double y2);
	double	EllipseDistanceSum(int XC,int YC,double A,double B,int X1,int Y1);


private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
	CString		strTitle;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_ParticleList;
	CListCtrl m_Lot_ParticleList;
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CListCtrl m_RectList;
	afx_msg void OnNMClickListRect(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonDefault();
	afx_msg void OnBnClickedButtonStepdefault();
	afx_msg void OnEnSetfocusEditRectlist();
	afx_msg void OnEnKillfocusEditRectlist();
	afx_msg void OnBnClickedButtonPSave();
	BOOL b_FailCheck;
	CString str_CaptureNum;
	CString str_Black_STEP;
	CString str_PassNum;
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnBnClickedButtonLoad();
	afx_msg void OnBnClickedButtonListSave();
	afx_msg void OnBnClickedButtonCam();
	CString str_CamBrightness;
	CString str_CamContrast;
	CString str_Separation_V;
	afx_msg void OnNMDblclkListRect(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawListRect(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedCheckFail();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};