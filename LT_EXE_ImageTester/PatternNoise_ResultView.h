#pragma once
#include "afxcmn.h"


// CPatternNoise_ResultView 대화 상자입니다.

class CPatternNoise_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CPatternNoise_ResultView)

public:
	CPatternNoise_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPatternNoise_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_PatternNoise };
	CFont ft;
	COLORREF txcol_TVal,bkcol_TVal;
	COLORREF txcol_TVal2,bkcol_TVal2;
	COLORREF txcol_TVal3,bkcol_TVal3;
	void	initstat();
	void	Setup(CWnd* IN_pMomWnd);
	void	Y_TEXT(LPCSTR lpcszString, ...);
	void	Y_VALUE(LPCSTR lpcszString, ...);
	void	Y_RESULT(unsigned char col,LPCSTR lpcszString, ...);
	void	V_TEXT(LPCSTR lpcszString, ...);
	void	V_VALUE(LPCSTR lpcszString, ...);
	void	V_RESULT(unsigned char col,LPCSTR lpcszString, ...);
	void	D_TEXT(LPCSTR lpcszString, ...);
	void	D_VALUE(LPCSTR lpcszString, ...);
	void	D_RESULT(unsigned char col,LPCSTR lpcszString, ...);

private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CListCtrl m_ResultList;
	afx_msg void OnEnSetfocusEditYStat();
};
