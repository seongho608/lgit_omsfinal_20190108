#pragma once


// C_3D_Depth_ResultView 대화 상자입니다.

class C_3D_Depth_ResultView : public CDialog
{
	DECLARE_DYNAMIC(C_3D_Depth_ResultView)

public:
	C_3D_Depth_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~C_3D_Depth_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_3D_DEPTH };
	CFont ft;
	COLORREF txcol_LTVal,bkcol_LTVal;
	COLORREF txcol_RTVal,bkcol_RTVal;
	COLORREF txcol_LBVal,bkcol_LBVal;
	COLORREF txcol_RBVal,bkcol_RBVal;

	COLORREF txcol_TVal2,bkcol_TVal2;
	COLORREF txcol_TVal3,bkcol_TVal3;
	
	void	initstat();

	void	AVE_TEXT(LPCSTR lpcszString, ...);
	void	AVE_VALUE(LPCSTR lpcszString, ...);
	void	AVE_RESULT(unsigned char col,LPCSTR lpcszString, ...);

	void	STDEV_TEXT(LPCSTR lpcszString, ...);
	void	STDEV_VALUE(LPCSTR lpcszString, ...);
	void	STDEV_RESULT(unsigned char col,LPCSTR lpcszString, ...);


	void	Setup(CWnd* IN_pMomWnd);
private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
