// ResolutionDetection_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "ResolutionDetection_ResultView.h"


// CResolutionDetection_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CResolutionDetection_ResultView, CDialog)

CResolutionDetection_ResultView::CResolutionDetection_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CResolutionDetection_ResultView::IDD, pParent)
{

}

CResolutionDetection_ResultView::~CResolutionDetection_ResultView()
{
}

void CResolutionDetection_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CENTER, m_CenterList);
	DDX_Control(pDX, IDC_LIST_SIDE, m_SideList);
}


BEGIN_MESSAGE_MAP(CResolutionDetection_ResultView, CDialog)
END_MESSAGE_MAP()


// CResolutionDetection_ResultView 메시지 처리기입니다.

void CResolutionDetection_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CResolutionDetection_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
BOOL CResolutionDetection_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_CenterList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_CenterList.InsertColumn(0,"LOCAL",LVCFMT_CENTER, 70);
	m_CenterList.InsertColumn(1,"本",LVCFMT_CENTER, 60);
	m_CenterList.InsertColumn(2,"RESULT",LVCFMT_CENTER, 70);

	m_SideList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_SideList.InsertColumn(0,"LOCAL",LVCFMT_CENTER, 70);
	m_SideList.InsertColumn(1,"本",LVCFMT_CENTER, 60);
	m_SideList.InsertColumn(2,"RESULT",LVCFMT_CENTER, 70);

	Initstat();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void CResolutionDetection_ResultView::Initstat(){
	m_CenterList.DeleteAllItems();
	m_SideList.DeleteAllItems();
}
BOOL CResolutionDetection_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}
