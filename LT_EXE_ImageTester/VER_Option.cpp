// VER_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "VER_Option.h"


// CVER_Option 대화 상자입니다.

IMPLEMENT_DYNAMIC(CVER_Option, CDialog)

CVER_Option::CVER_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CVER_Option::IDD, pParent)
	, str_Wrdata1(_T(""))
	, str_Wrdata2(_T(""))
{
	InsertIndex = 0;
	ListItemNum = 0;
	StartCnt = 0;
	m_Etc_State = 0;
	str_Wrdata1 = "";
	str_Wrdata2 = "";
	Old_str_Wrdata1 = "";
	Old_str_Wrdata2 = "";
	b_StopFail = FALSE;
	Lot_StartCnt=0;
}

CVER_Option::~CVER_Option()
{
}

void CVER_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_VERLIST, m_VERList);
	DDX_Text(pDX, IDC_EDIT_SETVER_WR1, str_Wrdata1);
	DDX_Text(pDX, IDC_EDIT_SETVER_WR2, str_Wrdata2);
	DDX_Control(pDX, IDC_LIST_VERLIST_LOT, m_Lot_VERList);
}


BEGIN_MESSAGE_MAP(CVER_Option, CDialog)
	ON_WM_CTLCOLOR()
	ON_EN_CHANGE(IDC_EDIT_SETVER_WR1, &CVER_Option::OnEnChangeEditSetverWr1)
	ON_EN_CHANGE(IDC_EDIT_SETVER_WR2, &CVER_Option::OnEnChangeEditSetverWr2)
	ON_BN_CLICKED(IDC_BTN_VERREAD, &CVER_Option::OnBnClickedBtnVerread)
	ON_BN_CLICKED(IDC_BTN_VERSAVE, &CVER_Option::OnBnClickedBtnVersave)
END_MESSAGE_MAP()


// CVER_Option 메시지 처리기입니다.
BOOL CVER_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_EDIT_VER_NAME1,&ft,100,20);
	GetDlgItem(IDC_EDIT_VER_NAME2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_VER1)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VALUE_VER2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_SETVER_NAME1)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_SETVER_RD1)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_SETVER_WR1)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_SETVER_NAME2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_SETVER_RD2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_SETVER_WR2)->SetFont(&ft);
	GetDlgItem(IDC_EDIT_VER_DATA)->SetFont(&ft);

	initstat();

	Set_List(&m_VERList);
	LOT_Set_List(&m_Lot_VERList);
	Load_parameter();

	InitEVMS();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CVER_Option::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}

void CVER_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CVER_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
	strTitle.Empty();
	strTitle="VEROPT_";
}

void CVER_Option::Pic(CDC *cdc)
{
	VerPic(cdc);
}

bool CVER_Option::VerPic(CDC *cdc){
	
	CPen	my_Pan,*old_pan;
	CString TEXTNAME[2] = {"  App Ver : ","   BL Ver : "};
	CString TEXTDATA = "";
	CFont m_font;

	if(m_Etc_State == 0){
		return TRUE;
	}
	
	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	m_font.CreatePointFont(200,"Arial");
	cdc->SelectObject(&m_font);
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);

	if(m_Etc_State == 1){
		my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(BLUE_COLOR);
	}else{
		my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(RED_COLOR);
	}

	for(int lop = 0;lop<2;lop++){
		TEXTDATA = TEXTNAME[lop] + ((CImageTesterDlg *)m_pMomWnd)->str_VER_RD[lop];
		cdc->TextOut(200,60+(lop*30),TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
	}

	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();
	m_font.DeleteObject();
	return TRUE;
}

void CVER_Option::InitPrm()
{
	Load_parameter();
	UpdateData(FALSE);
}

void CVER_Option::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

void CVER_Option::Save_parameter(){
	CString str = "";
	
	WritePrivateProfileString(str_model,NULL,"",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%d",m_INFO.ID);
	WritePrivateProfileString(str_model,"ID",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%s",m_INFO.MODE);
	WritePrivateProfileString(str_model,"MODE",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%s",m_INFO.NAME);
	WritePrivateProfileString(str_model,"NAME",str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	
	str_SetVerDat[0].Empty();
	str_SetVerDat[0].Format("0x%08X",m_SetVerDat[0]);
	WritePrivateProfileString(str_model,strTitle+"APPVER",str_SetVerDat[0],((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	
	str_SetVerDat[1].Empty();
	str_SetVerDat[1].Format("0x%08X",m_SetVerDat[1]);
	WritePrivateProfileString(str_model,strTitle+"BLVER",str_SetVerDat[1],((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	
	VER_ST_VALUE_SET();

	UpdateData(FALSE);
}

void CVER_Option::Load_parameter(){
	
	CString strbuf = "";
	
	str_SetVerDat[0] = GetPrivateProfileCString(str_model,strTitle+"APPVER",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(str_SetVerDat[0] == ""){
		str_SetVerDat[0] = "0x00004C0A";
		WritePrivateProfileString(str_model,strTitle+"APPVER",str_SetVerDat[0],((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}
	m_SetVerDat[0] = StringHEX2DWORD(str_SetVerDat[0]);

	str_SetVerDat[1] = GetPrivateProfileCString(str_model,strTitle+"BLVER",((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if(str_SetVerDat[1] == ""){
		str_SetVerDat[1] = "0x00002DBB";
		WritePrivateProfileString(str_model,strTitle+"BLVER",str_SetVerDat[1],((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	}
	m_SetVerDat[1] = StringHEX2DWORD(str_SetVerDat[1]);

	VER_ST_VALUE_SET();

	UpdateData(FALSE);
}

tResultVal CVER_Option::Run()
{
	tResultVal retval = {0,};
	m_Etc_State = 0;
	b_StopFail = FALSE;

	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
		InsertList();
	}

	if(VER_READ() == FALSE){//VER_READ를 실패할 경우
		m_VERList.SetItemText(InsertIndex,6,"");
		m_VERList.SetItemText(InsertIndex,7,"");
		m_VERList.SetItemText(InsertIndex,8,"FAIL");
		retval.ValString.Empty();
		retval.ValString.Format("VERSION READ FAIL");
		retval.m_Success = FALSE;
		Str_Mes[0] = "FAIL";
		Str_Mes[1] = "0";
		m_Etc_State = 2;
		VER_RESULT_STATE(2,"VERSION READ FAIL");
	}else{
		for(int lop =0;lop<2;lop++){
			m_VERList.SetItemText(InsertIndex,6+lop,((CImageTesterDlg *)m_pMomWnd)->str_VER_RD[lop]);
		}
		if(b_tMatch == TRUE){//합격이면
			m_VERList.SetItemText(InsertIndex,8,"PASS");
			retval.ValString.Empty();
			retval.ValString.Format("VERSION MATCH SUCCESS");
			retval.m_Success = TRUE;
			Str_Mes[0] = "PASS";
			Str_Mes[1] = "1";
			m_Etc_State = 1;
			VER_RESULT_STATE(1,"SUCCESS");
		}else{//실패면
			m_VERList.SetItemText(InsertIndex,8,"FAIL");
			retval.ValString.Empty();
			retval.ValString.Format("VERSION MATCH FAIL");
			retval.m_Success = FALSE;
			Str_Mes[0] = "FAIL";
			Str_Mes[1] = "0";
			m_Etc_State = 2;
			VER_RESULT_STATE(2,"VERSION MATCH FAIL");
		}
	}
	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
		if(retval.m_Success == TRUE)
		{
		//	((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_VERSION,"PASS");
		}
		else
		{
		//	((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_VERSION,"PASS");
		}
		StartCnt++;
	}



	return retval;
}

//---------------------------------------------------------------------WORKLIST
void CVER_Option::Set_List(CListCtrl *List){
	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"SET App Ver",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"SET BL Ver",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"RD App Ver",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"RD BL Ver",LVCFMT_CENTER, 80);
	List->InsertColumn(8,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(9,"비고",LVCFMT_CENTER, 80);

	ListItemNum=10;
	Copy_List(&m_VERList,((CImageTesterDlg *)m_pMomWnd)->m_pWorkList,ListItemNum);
}

void CVER_Option::LOT_Set_List(CListCtrl *List){
	while(List->DeleteColumn(0));
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"SET App Ver",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"SET BL Ver",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"RD App Ver",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"RD BL Ver",LVCFMT_CENTER, 80);
	List->InsertColumn(8,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(9,"비고",LVCFMT_CENTER, 80);

	Lot_InsertNum=10;
}

void CVER_Option::EXCEL_SAVE(){
	CString Item[9]={"SET App Ver","SET BL Ver","RD App Ver","RD BL Ver"};
	CString Data ="";
	CString str="";
	str = "***********************VERSION TEST***********************\n\n";
	Data += str;
	str.Empty();
	str = "App Version,"+str_SetVerDat[0]+",BL VERSION,"+str_SetVerDat[1]+ ",\n";
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("버전확인",Item,4,&m_VERList,Data);
}

void CVER_Option::LOT_EXCEL_SAVE(){
	CString Item[9]={"SET App Ver","SET BL Ver","RD App Ver","RD BL Ver"};
	CString Data ="";
	CString str="";
	str = "***********************VERSION TEST***********************\n\n";
	Data += str;
	str.Empty();
	str = "App Version,"+str_SetVerDat[0]+",BL VERSION,"+str_SetVerDat[1]+ ",\n";
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("버전확인",Item,4,&m_Lot_VERList,Data);
}

void CVER_Option::InsertList(){
	CString strCnt;

	InsertIndex = m_VERList.InsertItem(StartCnt,"",0);
	
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_VERList.SetItemText(InsertIndex,0,strCnt);
	
	m_VERList.SetItemText(InsertIndex,1,((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_VERList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_VERList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");
	m_VERList.SetItemText(InsertIndex,4,str_SetVerDat[0]);
	m_VERList.SetItemText(InsertIndex,5,str_SetVerDat[1]);
}

void CVER_Option::FAIL_UPLOAD(){
	if ((((CImageTesterDlg *)m_pMomWnd)->LotMod == 1) && (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)){
		InsertList();
		for(int t=0; t<2;t++){
			m_VERList.SetItemText(InsertIndex,t+6,"X");
		}
		m_VERList.SetItemText(InsertIndex,8,"FAIL");
		m_VERList.SetItemText(InsertIndex,9,"STOP");
		//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_VERSION,"STOP");
		StartCnt++;
		b_StopFail = TRUE;
	}
}

bool CVER_Option::EXCEL_UPLOAD(){
	m_VERList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("버전확인",&m_VERList,1,&StartCnt)==TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return FALSE;
}

bool CVER_Option::LOT_EXCEL_UPLOAD(){
	m_VERList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("버전확인",&m_Lot_VERList,1,&StartCnt)==TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return FALSE;
}


void CVER_Option::LOT_InsertDataList()
{
	CString strCnt="";

	int Index = m_Lot_VERList.InsertItem(Lot_StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index+1);
	m_Lot_VERList.SetItemText(Index,0,strCnt);

	int CopyIndex = m_VERList.GetItemCount()-1;

	int count =0;
	for(int t=0; t< Lot_InsertNum;t++){
		if((t != 2)&&(t!=0)){
			m_Lot_VERList.SetItemText(Index,t, m_VERList.GetItemText(CopyIndex,count));
			count++;

		}else if(t==0){
			count++;
		}else{
			m_Lot_VERList.SetItemText(Index,t,((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;
}

CString CVER_Option::Mes_Result()
{
	CString sz;

	m_szMesResult = _T("");
	if(b_StopFail == FALSE){
	m_szMesResult.Format(_T("%s:%s"),Str_Mes[0],Str_Mes[1]);
	}else{
		m_szMesResult.Format(":1");//강제스톱 발생시
	}
	return m_szMesResult;
}

//----------------------------------------------------------------------------------------------------------
HBRUSH CVER_Option::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VER_NAME1){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VER_NAME2){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_SETVER_NAME1){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_SETVER_NAME2){
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86, 86, 86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_VER1){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[0]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VALUE_VER2){
		pDC->SetBkColor(RGB(255, 255, 255));
		pDC->SetTextColor(Valcol[1]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_VER_DATA){
		pDC->SetTextColor(st_col);
		pDC->SetBkColor(RGB(255, 255, 255));
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

BOOL CVER_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		
		if (pMsg->wParam == VK_RETURN){

			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}


void CVER_Option::initstat()
{
	((CEdit *)GetDlgItem(IDC_EDIT_VER_NAME1))->SetWindowText("App Version");
	((CEdit *)GetDlgItem(IDC_EDIT_VER_NAME2))->SetWindowText("BL Version");
	((CEdit *)GetDlgItem(IDC_EDIT_SETVER_NAME1))->SetWindowText("App Version");
	((CEdit *)GetDlgItem(IDC_EDIT_SETVER_NAME2))->SetWindowText("BL Version");
	VER_RESULT(2,0,"");//모두 NONE표시
	VER_STATE(0,"Stand By");
	VER_RESULT_STATE(0,"STAND BY");
}

void CVER_Option::VER_RESULT(int num,int col,LPCSTR lpcszString, ...)
{	
	switch(num){
		case 0:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_VER1))->SetWindowText(lpcszString);
			break;
		case 1:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_VER2))->SetWindowText(lpcszString);
			break;
		default:
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_VER1))->SetWindowText(lpcszString);
			((CEdit *)GetDlgItem(IDC_EDIT_VALUE_VER2))->SetWindowText(lpcszString);
	}
	
	if(num <=3){
		if(col == 0){
			Valcol[num] = RGB(86,86,86);
		}else if(col  ==1){
			Valcol[num] = RGB(18,69,171);
		}else{
			Valcol[num] = RGB(201,0,0);	
		}
		//str_DTC[num] = lpcszString;
	}else{
		for(int lop = 0;lop<4;lop++){
			if(col == 0){
				Valcol[lop] = RGB(86,86,86);
			}else if(col  ==1){
				Valcol[lop] = RGB(18,69,171);
			}else{
				Valcol[lop] = RGB(201,0,0);	
			}
		//	str_DTC[lop] = lpcszString;
		}
	}
	if(((CImageTesterDlg *)m_pMomWnd)->m_pResVEROptWnd != NULL){
		((CImageTesterDlg *)m_pMomWnd)->m_pResVEROptWnd->VER_RESULT(num,col,lpcszString);
	}
}
void CVER_Option::VER_STATE(int col,LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_VER_DATA))->SetWindowText(lpcszString);
	if(col == 0){
		st_col = RGB(86,86,86);
	}else if(col  ==1){
		st_col = RGB(18,69,171);
	}else{
		st_col = RGB(201,0,0);		
	}
}

void CVER_Option::VER_RESULT_STATE(int col,LPCSTR lpcszString, ...)
{	
	if(((CImageTesterDlg *)m_pMomWnd)->m_pResVEROptWnd != NULL){
		((CImageTesterDlg *)m_pMomWnd)->m_pResVEROptWnd->VER_STATE(col,lpcszString);
	}
}

void CVER_Option::VER_ST_VALUE(int num,LPCSTR lpcszString, ...)
{	
	switch(num){
		case 0:
			((CEdit *)GetDlgItem(IDC_EDIT_SETVER_RD1))->SetWindowText(lpcszString);
			break;
		case 1:
			((CEdit *)GetDlgItem(IDC_EDIT_SETVER_RD2))->SetWindowText(lpcszString);
			break;
		default:
			((CEdit *)GetDlgItem(IDC_EDIT_SETVER_RD1))->SetWindowText(lpcszString);
			((CEdit *)GetDlgItem(IDC_EDIT_SETVER_RD2))->SetWindowText(lpcszString);
	}
}

void CVER_Option::VER_ST_VALUE_SET()
{	
	for(int lop = 0;lop<2;lop++){
		VER_ST_VALUE(lop,str_SetVerDat[lop]);
	}
}

void CVER_Option::OnEnChangeEditSetverWr1()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	CString str_buf;
	CString str;
	BOOL b_add = FALSE;;
	UpdateData(TRUE);
	str_buf = str_Wrdata1;
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_SETVER_WR1))->GetSel();
	if(str_buf.Find("0x") != 0){
		if(Old_str_Wrdata1.Find("0x") == 0)
		{
			str_buf = Old_str_Wrdata1;
		}else{
			str_buf = "0x" + str_buf;
			b_add = TRUE;
		}
	}else{
		if(str_buf == "0x"){
			str_buf = "";
			Old_str_Wrdata1 = str_buf;
			str_Wrdata1 = "";
			UpdateData(FALSE);
			((CEdit *)GetDlgItem(IDC_EDIT_SETVER_WR1))->SetSel(0,0);
			return;
		}
	}

	str = StringMoc(str_buf);
	if(str_buf != str){
		str_buf = str;
	}
	if(str_buf == "0x"){
		str_buf = "";
		Old_str_Wrdata1 = str_buf;
		str_Wrdata1 = "";
		UpdateData(FALSE);
		((CEdit *)GetDlgItem(IDC_EDIT_SETVER_WR1))->SetSel(0,0);
		return;
	}
	DWORD DATA = StringHEX2DWORD(str_buf);
	int len = str_buf.GetLength();
	
	if(len > 10){
		str_buf = Old_str_Wrdata1; 
	}else{
		Old_str_Wrdata1 = str_buf;
	}
	str_Wrdata1 = str_buf;
	UpdateData(FALSE);
	if(b_add == FALSE){
		((CEdit *)GetDlgItem(IDC_EDIT_SETVER_WR1))->SetSel(SELCURSER,SELCURSER);
	}else{
		((CEdit *)GetDlgItem(IDC_EDIT_SETVER_WR1))->SetSel(-1);
	}
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CVER_Option::OnEnChangeEditSetverWr2()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	CString str_buf;
	CString str;
	BOOL b_add = FALSE;;
	UpdateData(TRUE);
	str_buf = str_Wrdata2;
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_SETVER_WR2))->GetSel();
	if(str_buf.Find("0x") != 0){
		if(Old_str_Wrdata1.Find("0x") == 0)
		{
			str_buf = Old_str_Wrdata2;
		}else{
			str_buf = "0x" + str_buf;
			b_add = TRUE;
		}
	}else{
		if(str_buf == "0x"){
			str_buf = "";
			Old_str_Wrdata2 = str_buf;
			str_Wrdata2 = "";
			UpdateData(FALSE);
			((CEdit *)GetDlgItem(IDC_EDIT_SETVER_WR2))->SetSel(0,0);
			return;
		}
	}

	str = StringMoc(str_buf);
	if(str_buf != str){
		str_buf = str;
	}
	if(str_buf == "0x"){
		str_buf = "";
		Old_str_Wrdata2 = str_buf;
		str_Wrdata2 = "";
		UpdateData(FALSE);
		((CEdit *)GetDlgItem(IDC_EDIT_SETVER_WR2))->SetSel(0,0);
		return;
	}
	DWORD DATA = StringHEX2DWORD(str_buf);
	int len = str_buf.GetLength();
	
	if(len > 10){
		str_buf = Old_str_Wrdata2; 
	}else{
		Old_str_Wrdata2 = str_buf;
	}
	str_Wrdata2 = str_buf;
	UpdateData(FALSE);
	if(b_add == FALSE){
		((CEdit *)GetDlgItem(IDC_EDIT_SETVER_WR2))->SetSel(SELCURSER,SELCURSER);
	}else{
		((CEdit *)GetDlgItem(IDC_EDIT_SETVER_WR2))->SetSel(-1);
	}
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CVER_Option::OnBnClickedBtnVerread()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Power_On();
	DoEvents(500);
	VER_READ();
}

void CVER_Option::OnBnClickedBtnVersave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	DWORD DATA = 0;
	CString str_buf = str_Wrdata1;
	str_buf.Replace(" ","");
	if(str_buf != ""){
		m_SetVerDat[0] = StringHEX2DWORD(str_buf);
	}

	str_buf = str_Wrdata2;
	str_buf.Replace(" ","");
	if(str_buf != ""){
		m_SetVerDat[1] = StringHEX2DWORD(str_buf);
	}

	for(int lop = 0;lop<2;lop++)
	{
		str_SetVerDat[lop].Format("0x%08X",m_SetVerDat[lop]);
	}

	Save_parameter();
	str_Wrdata1 = "";
	str_Wrdata2 = "";
	Old_str_Wrdata1 = "";
	Old_str_Wrdata2 = "";

	UpdateData(FALSE);
}

BOOL CVER_Option::VER_READ()
{
	BOOL ret = FALSE;
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	BOOL colst = 0;
	
	VER_RESULT(2,0,"");//모두 NONE표시
	VER_STATE(0,"Stand By");

	for(int lop = 0;lop<2;lop++){
		b_Match[lop] = FALSE;
		((CImageTesterDlg *)m_pMomWnd)->m_VER_RD[lop] = 0;
		((CImageTesterDlg *)m_pMomWnd)->str_VER_RD[lop] = "";
	}
	if(((CImageTesterDlg *)m_pMomWnd)->DCC_VER_READ() == TRUE){
		b_tMatch = TRUE;
		for(int lop = 0;lop<2;lop++){
			if(((CImageTesterDlg *)m_pMomWnd)->m_VER_RD[lop] == m_SetVerDat[lop]){
				b_Match[lop] = TRUE;
				colst = 1;
			}else{
				b_Match[lop] = FALSE;
				b_tMatch = FALSE;
				colst = 2;
			}
			
			VER_RESULT(lop,colst,((CImageTesterDlg *)m_pMomWnd)->str_VER_RD[lop]);//모두 ERR표시
		}
		VER_STATE(1,"VER READ SUCCESS");
		ret =  TRUE;
	}else{//실패시 
		b_tMatch = FALSE;
		VER_RESULT(2,2,"ERR");//모두 ERR표시
		VER_STATE(2,"VER READ FAIL");
		ret = FALSE;
	}
	return ret;
}


void CVER_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CButton *)GetDlgItem(IDC_BTN_VERSAVE))->EnableWindow(0);
		
		((CEdit *)GetDlgItem(IDC_EDIT_SETVER_WR1))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_SETVER_WR2))->EnableWindow(0);
	}
}