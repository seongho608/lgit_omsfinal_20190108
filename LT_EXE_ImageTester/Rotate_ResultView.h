#pragma once


// CRotate_ResultView 대화 상자입니다.

class CRotate_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CRotate_ResultView)

public:
	CRotate_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CRotate_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_ROTATE };
	void	Initstat();
	COLORREF txcol_TVal,bkcol_TVal;
	COLORREF txcol_TVal2,bkcol_TVal2;
	COLORREF txcol_Val, bkcol_Val;
	COLORREF txcol_Val2, bkcol_Val2;
	COLORREF txcol_Val3, bkcol_Val3;
	COLORREF txcol_Val4, bkcol_Val4;
	CFont ft1;
	void	LOCATION_TEXT(LPCSTR lpcszString, ...);
	void	LOCATION_TEXT2(LPCSTR lpcszString, ...);
	void	VALUE_TEXT(LPCSTR lpcszString, ...);
	void	VALUE_TEXT2(LPCSTR lpcszString, ...);

	void	RESULT_TEXT(unsigned char col,LPCSTR lpcszString, ...);
	void	RESULT_TEXT2(unsigned char col,LPCSTR lpcszString, ...);

	void	TILT_TEXT(LPCSTR lpcszString, ...);
	void	TILT_TEXT2(LPCSTR lpcszString, ...);
	void	TILT_TEXT3(LPCSTR lpcszString, ...);
	void	TILT_TEXT4(LPCSTR lpcszString, ...);
	void	TILT_VALUE_TEXT(unsigned char col, LPCSTR lpcszString, ...);
	void	TILT_VALUE_TEXT2(unsigned char col, LPCSTR lpcszString, ...);
	void	TILT_VALUE_TEXT3(unsigned char col, LPCSTR lpcszString, ...);
	void	TILT_VALUE_TEXT4(unsigned char col, LPCSTR lpcszString, ...);

	void	Setup(CWnd* IN_pMomWnd);
private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnEnSetfocusEditRotateTxt();
	afx_msg void OnEnSetfocusEditRotateValue();
	afx_msg void OnEnSetfocusEditRotateResult();
	afx_msg void OnEnSetfocusEditTiltTxt();
	afx_msg void OnEnSetfocusEditTiltTxt2();
	afx_msg void OnEnSetfocusEditTiltTxt3();
	afx_msg void OnEnSetfocusEditTiltTx4();
	afx_msg void OnEnSetfocusEditTiltValue();
	afx_msg void OnEnSetfocusEditTiltValue2();
	afx_msg void OnEnSetfocusEditTiltValue3();
	afx_msg void OnEnSetfocusEditTiltValue4();
};
