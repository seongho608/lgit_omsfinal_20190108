// EtcModelDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "EtcModelDlg.h"


// CEtcModelDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CEtcModelDlg, CDialog)

CEtcModelDlg::CEtcModelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEtcModelDlg::IDD, pParent)
{

}

CEtcModelDlg::~CEtcModelDlg()
{
}

void CEtcModelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS_TRANS, m_CtrProgress);
}


BEGIN_MESSAGE_MAP(CEtcModelDlg, CDialog)
	ON_WM_CTLCOLOR()
	ON_WM_DRAWITEM()
	ON_BN_CLICKED(IDC_Btn_Run, &CEtcModelDlg::OnBnClickedBtnRun)
	ON_WM_DESTROY()
	ON_EN_SETFOCUS(IDC_EDIT_STATE, &CEtcModelDlg::OnEnSetfocusEditState)
	ON_EN_SETFOCUS(IDC_STRING_STATE, &CEtcModelDlg::OnEnSetfocusStringState)
END_MESSAGE_MAP()


// CEtcModelDlg 메시지 처리기입니다.
void CEtcModelDlg::NameChange(CString str)
{
	NAME = str;
	SetDlgItemText(IDC_Btn_Run,NAME);
}

void CEtcModelDlg::EnableOption(bool benable)
{
	((CButton *)GetDlgItem(IDC_Btn_Run))->EnableWindow(benable);
}

void CEtcModelDlg::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);
	LogFont.lfWeight =Weight;

	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);

	GetDlgItem(nID)->SetFont(font);
}

void CEtcModelDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
	IN_pWnd		= m_pMomWnd;
	m_pTStat	= ((CImageTesterDlg *)m_pMomWnd)->m_pTStat;
}

void CEtcModelDlg::Setup(CWnd* IN_pMomWnd,tINFO INFO)
{
	Setup(IN_pMomWnd);
	m_INFO = INFO;
	NAME = INFO.NAME;
	ModeNAME = INFO.NAME;
	ModeSetup(INFO.ID);
}

void CEtcModelDlg::ModeSetup(int ID)
{
	if((m_ID != 0)&&(m_ID != ID)){
		DelModel(m_ID); //->기존에 세팅된 프로젝트가 생성되어 있다면 삭제한다.
	}
	if((ID != 0)&&(m_ID != ID)){
		m_ID = ID;
		GenModel(m_ID);
	}
}

void CEtcModelDlg::ShowDlg(int stat)
{
 	if((m_ID != 0)&&(IN_pWnd != NULL)){
 		switch(m_ID){
 			case 0x01://
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_NTSC_ShowWindow(stat);
 				break;
 			case 0x02:
 				break;
 			case 0x03:
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_CPoint_ShowWindow(stat);
 
 				break;
 			case 0x04:
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Rotate_ShowWindow(stat);
 
 				break;
 			case 0x05:
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Resolution_ShowWindow(stat);
 
 				break;
			case 0x15:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_SFR_ShowWindow(stat);

				break;
 			case 0x06://화각
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Angle_ShowWindow(stat);
 				break;

 			case 0x07://반전
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_ColorReversal_ShowWindow(stat);
 				break;
 			case 0x08://저조도
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_PatternNoise_ShowWindow(stat);
 				break;	
 			case 0x09:
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_IRFilter_ShowWindow(stat);
 
 				break;	
 			case 0x0a://이물
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Particle_ShowWindow(stat);
 				break;	
			
			case 0x0b://광량비
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Brightness_ShowWindow(stat);
 				break;	
 			case 0x0c:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_FixedPattern_ShowWindow(stat);
				//((CImageTesterDlg *)m_pMomWnd)->CModelOpt_COPoint_ShowWindow(stat);
 
 				break;
 			case 0x0d://Distortion
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Distortion_ShowWindow(stat);
 
 				break;
 			case 0x0e:
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Color_ShowWindow(stat);
 				break;
			
			case 0x0f:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Overlay_CAN_ShowWindow(stat);
				break;

			case 0x10://이물_LG
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_ParticleLG_ShowWindow(stat);
 				break;

			case 0x11:
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_OVERLAYPOSITION_ShowWindow(stat);
 				break;

			case 0x12://저조도(백점검사)
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_LowLight_ShowWindow(stat);
 				break;

			case 0x13://Dynamic Range
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_DynamicRange_ShowWindow(stat);
				break;

			case 0x16://3D Depth
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_3D_Depth_ShowWindow(stat);
				break;

			case 0x17://HotPixel
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_HotPixel_ShowWindow(stat);
				break;

			case 0x18://Depth Noise
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_DepthNoise_ShowWindow(stat);
				break;

			case 0x19://온도 센서
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Temperature_ShowWindow(stat);
				break;

			case 0x1f:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Warning_CAN_ShowWindow(stat);
				break;

			case 0x1a:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_DTC_ShowWindow(stat);
				break;

			case 0x1b:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_SYSTEM_ShowWindow(stat);
				break;
			
			case 0x1c:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_VER_ShowWindow(stat);
				break;

 			case 0xfe: //standard option
 				((CImageTesterDlg *)m_pMomWnd)->Model_ShowWindow(stat);
 				break;
 			case 0xff:
				break;
			case 0x2f:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Verify_ShowWindow(stat);
 				break;

 			default:
 				break;
 		}
 	}
}

BOOL CEtcModelDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_EDIT_STATE,&font1,45,20);
	TestState(2,_T(""));

	SetDlgItemText(IDC_Btn_Run,NAME);
	m_CtrProgress.SetRange(0,1000);
	InitStat();
	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CEtcModelDlg::DelModel(int ID)
{	
 	if((ID != 0)&&(IN_pWnd != NULL)){
 		switch(ID){
 			case 0x01://NTSC TEST
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_NTSC_Delete();
 				break;
 			case 0x02:
 				break;
 			case 0x03:
 				((CImageTesterDlg *)m_pMomWnd)->OpticEnable = 0;
 
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_CPoint_Delete();
 				break;
 			case 0x04:
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Rotate_Delete();
 				break;
 			case 0x05://해상력
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Resolution_Delete();
 				break;
			case 0x15://해상력
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_SFR_Delete();
				break;	
 			case 0x06://화각
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Angle_Delete();
 				break;
 			case 0x07://반전
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_ColorReversal_Delete();
 				break;
 			case 0x08://저조도
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_PatternNoise_Delete();
 				break;
 			case 0x09:
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_IRFilter_Delete();
 
 				break;
 			case 0x0a://이물
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Particle_Delete();
 				break;
			case 0x10://이물_LG
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_ParticleLG_Delete();
 				break;
			case 0x11://오버레이 포지션
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_OVERLAYPOSITION_Delete();
 				break;
			case 0x12://저조도(백점검사)
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_LowLight_Delete();
 				break;
			case 0x13://Dynamic Range
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_DynamicRange_Delete();
				break;
			case 0x16://3D Depth
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_3D_Depth_Delete();
				break;
			case 0x17://
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_HotPixel_Delete();
				break;
			case 0x18://
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_DepthNoise_Delete();
				break;

			case 0x19://온도 센서
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Temperature_Delete();
				break;

 			case 0x0b://광량비
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Brightness_Delete();
 				break;
 			case 0x0c:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_FixedPattern_Delete();
 				//((CImageTesterDlg *)m_pMomWnd)->CModelOpt_COPoint_Delete();
 
 				break;
 			case 0x0d://Distortion
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Distortion_Delete();
 
 				break;
 			case 0x0e:
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Color_Delete();
 				break;

			case 0x0f:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Overlay_CAN_Delete();
				break;

			case 0x1f:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Warning_CAN_Delete();
				break;
			
			case 0x1a:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_DTC_Delete();
				break;

			case 0x1b:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_SYSTEM_Delete();
				break;

			case 0x1c:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_VER_Delete();
				break;

 			case 0xfe: //standard option
 				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Stand_Delete();
 				break;
 			case 0x2f:
				((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Verify_Delete();
 				break;
			case 0xff:
				break;
 			default:
 				IN_pWnd = NULL;
 		}
 	}

	NAME = _T("");
	m_ID = 0;
	IN_pWnd = NULL;
}

void CEtcModelDlg::GenModel(int ID)
{
	if(ID != 0)
		NAME = m_INFO.NAME;
	
 	switch(ID)
 	{
 		case 0x01:
			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_NTSC_Create(m_INFO);
 			break;
 		case 0x02:				
 			break;
 		case 0x03:
 			((CImageTesterDlg *)m_pMomWnd)->OpticEnable = 1;
 			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_CPoint_Create(m_INFO);
 			NAME = ((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->ModeName;
			IN_pWnd = m_pMomWnd;
			break;
 		case 0x04:
 			IN_pWnd = m_pMomWnd;
 			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Rotate_Create(m_INFO);
 			break;
 		case 0x05://해상력
 			IN_pWnd = m_pMomWnd;
 			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Resolution_Create(m_INFO);
 			break;
		case 0x15://SFR
			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_SFR_Create(m_INFO);
			break;
 		case 0x06://화각
 			IN_pWnd = m_pMomWnd;
 			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Angle_Create(m_INFO);
 			break;
 		case 0x07://반전
 			IN_pWnd = m_pMomWnd;
 			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_ColorReversal_Create(m_INFO);
 			break;
 		case 0x08://저조도
 			IN_pWnd = m_pMomWnd;
 			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_PatternNoise_Create(m_INFO);
 			break;
 		case 0x09://IR
 			IN_pWnd = m_pMomWnd;
 			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_IRFilter_Create(m_INFO);
 			break;
 		case 0x0a://이물
 			IN_pWnd = m_pMomWnd;
 			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Particle_Create(m_INFO);
 			break;
		case 0x10://이물_LG
 			IN_pWnd = m_pMomWnd;
 			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_ParticleLG_Create(m_INFO);
 			break;
		case 0x11://오버레이포지션
 			IN_pWnd = m_pMomWnd;
 			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_OVERLAYPOSITION_Create(m_INFO);
 			break;
		case 0x12://저조도(백점검사)
 			IN_pWnd = m_pMomWnd;
 			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_LowLight_Create(m_INFO);
 			break;
		case 0x13://Dynamic Range
			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_DynamicRange_Create(m_INFO);
			break;
		case 0x16://3D Depth
			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_3D_Depth_Create(m_INFO);
			break;
		case 0x17://HotPixel
			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_HotPixel_Create(m_INFO);
			break;
		case 0x18://HotPixel
			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_DepthNoise_Create(m_INFO);
			break;

		case 0x19://온도 센서
			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Temperature_Create(m_INFO);
			break;

 		case 0x0b://광량비
 			IN_pWnd = m_pMomWnd;
 			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Brightness_Create(m_INFO);
 			break;
 		case 0x0c:
 			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_FixedPattern_Create(m_INFO);
 			//((CImageTesterDlg *)m_pMomWnd)->CModelOpt_COPoint_Create(m_INFO);
 			break;
 		case 0x0d:
 			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Distortion_Create(m_INFO);
 			break;
 		case 0x0e:
 			IN_pWnd = m_pMomWnd;
 			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Color_Create(m_INFO);
 			break;
		case 0x0f:
			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Overlay_CAN_Create(m_INFO);
			break;
		case 0x1f:
			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Warning_CAN_Create(m_INFO);
			break;
		
		case 0x1a:
			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_DTC_Create(m_INFO);
			NAME = ((CImageTesterDlg *)m_pMomWnd)->m_pDTCOptWnd->ModeName;
			break;

		case 0x1b:
			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_SYSTEM_Create(m_INFO);
			break;
		
		case 0x1c:
			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_VER_Create(m_INFO);
			break;	

 		case 0xfe: //standard option
 			IN_pWnd = m_pMomWnd;
 			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Stand_Create();
			NAME = ((CImageTesterDlg *)m_pMomWnd)->m_pSubStandOptWnd->ModeName;
			break;
 		case 0xff:
			IN_pWnd = m_pMomWnd;
			break;
		case 0x2f:
 			IN_pWnd = m_pMomWnd;
			((CImageTesterDlg *)m_pMomWnd)->CModelOpt_Verify_Create(m_INFO);
 			break;
 		default:
 			IN_pWnd = NULL;
 	}
}

void CEtcModelDlg::TestState(unsigned char col,LPCTSTR lpcszString, ...)
{	
	if(col == 0){
		txcol = RGB(255, 255, 255); 
		bkcol = RGB(18,  69,171);
	}else if(col == 1){
		txcol = RGB(255, 255, 255);
		bkcol = RGB(201, 0, 0);
	}else if(col == 2){
		txcol = RGB(255, 255, 255);
		bkcol = RGB(47, 157, 39);
	}else if(col == 3){
		txcol = RGB(0, 0, 0);
		bkcol = RGB(255, 240, 0);
	}
	if(lpcszString == "X"){
		((CEdit *)GetDlgItem(IDC_EDIT_STATE))->SetWindowText("X");

	}else{
		((CEdit *)GetDlgItem(IDC_EDIT_STATE))->SetWindowText("");
	}
}

HBRUSH CEtcModelDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	CBrush m_edit_bk_brush;
	m_edit_bk_brush.CreateSolidBrush(RGB(64, 64, 64)); //중요 끝날때 해제해야 한다.
	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_STATE)
	{
		pDC->SetTextColor(txcol);
		pDC->SetBkColor(bkcol);
		hbr = m_edit_bk_brush;
	}
	m_edit_bk_brush.DeleteObject();
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CEtcModelDlg::InitStat()
{
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
	m_CtrProgress.SetPos(0);
	TestState(2,_T(""));
	((CEdit*)GetDlgItem(IDC_STRING_STATE))->SetWindowText(_T(""));
	ProgressEn(0);

}

void CEtcModelDlg::ProgressEn(BOOL stat)
{
	if(stat == 0){
		m_CtrProgress.ShowWindow(SW_HIDE);
		((CEdit*)GetDlgItem(IDC_STRING_STATE))->ShowWindow(SW_SHOW);
	}else{
		m_CtrProgress.ShowWindow(SW_SHOW);
		((CEdit*)GetDlgItem(IDC_STRING_STATE))->ShowWindow(SW_HIDE);
	}
	m_CtrProgress.SetPos(0);

}

void CEtcModelDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDCtl == IDC_Btn_Run){

		CDC *p_dc = CDC::FromHandle(lpDrawItemStruct->hDC);

		// RECT 형식의 구조체값을 이용하여 CRect 객체를 생성한다.
		CRect r(lpDrawItemStruct->rcItem); 

		CString str;
		str.Empty();
		str.Format(NAME);

		// 글자를 출력할 때 적용할 배경을 투명으로 설정한다.
		int old_mode = p_dc->SetBkMode(TRANSPARENT);

		// 버튼을 사용할 수 없는 상태일 경우
		if(lpDrawItemStruct->itemState & ODS_DISABLED){
			// 버튼 영역을 회색으로 채운다.
			//          p_dc->FillSolidRect(r, RGB(200, 200, 200));
			//p_dc->FillSolidRect(r, RGB(236, 233, 216));
				p_dc->FillSolidRect(r,RGB(230, 230, 230));

			// 진한 회색으로 글자색을 설정한다.
			//            p_dc->SetTextColor(RGB(128, 128, 128));
			p_dc->SetTextColor(RGB(0, 0, 0));
			// 버튼의 캡션을 출력한다.
			p_dc->DrawText(str, r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

		} else {
			// 버튼 영역을 하늘색으로 채운다.

			p_dc->FillSolidRect(r, RGB(240, 240, 240));

			// 버튼이 포커스를 얻은 경우
			if(lpDrawItemStruct->itemState & ODS_FOCUS){
				p_dc->FillSolidRect(r, RGB(50, 82, 152));
				// 분홍색으로 테두리를 그리고, 글자색도 분홍색으로 설정한다.
				//p_dc->Draw3dRect(r, RGB(236, 233, 216),RGB(236, 233, 216));
				p_dc->Draw3dRect(r, RGB(50, 82, 152),  RGB(50, 82, 152));
				p_dc->SetTextColor(RGB(255, 255, 255));
				// p_dc->DrawText(str, r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

			} else {
				// 회색으로 테두리를 그리고, 글자색을 흰색으로 설정한다.
				p_dc->Draw3dRect(r, RGB(240, 240, 240),RGB(240, 240, 240));
				p_dc->SetTextColor(RGB(0, 0, 0));
				//	 p_dc->DrawText(str, r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			}
			// 영역을 한픽셀씩 내부 방향으로 줄인다.
			r.left++;
			r.top++;
			r.right--;
			r.bottom--;

			// 버튼을 선택한 경우
			if(lpDrawItemStruct->itemState & ODS_SELECTED){
				// 테두리의 색상을 지정한다.
				p_dc->Draw3dRect(r,  RGB(50, 82, 152),  RGB(50, 82, 152));

				// 버튼의 캡션을 오른쪽 아래 방향으로 한픽셀씩 이동시켜 출력한다.
				p_dc->DrawText(str, r + CPoint(1, 1), DT_CENTER | DT_VCENTER | DT_SINGLELINE);

			} else {
				// 테두리의 색상을 지정한다.
				// p_dc->Draw3dRect(r, RGB(60, 92, 162), RGB(60, 92, 162));
				p_dc->Draw3dRect(r, RGB(110, 130, 142), RGB(110, 130, 142));
				// 버튼의 캡션을 출력한다.
				p_dc->DrawText(str, r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			}
		}
		// 배경을 이전 모드로 설정한다.
		p_dc->SetBkMode(old_mode);
	}

	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CEtcModelDlg::OnBnClickedBtnRun()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->b_ETCRunMODE = TRUE;
	((CImageTesterDlg *)m_pMomWnd)->FullOptionEnable(0);
	ETC_Run();
	((CImageTesterDlg *)m_pMomWnd)->FullOptionEnable(1);
	((CImageTesterDlg *)m_pMomWnd)->b_ETCRunMODE = FALSE;

}

void CEtcModelDlg::DelModel()
{	
	DelModel(m_ID);
}
void CEtcModelDlg::OnDestroy()
{
	CDialog::OnDestroy();
	DelModel();
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CEtcModelDlg::InitPrm(){
	InitPrm(m_ID);
}

void CEtcModelDlg::InitPrm(int ID)
{
	if((ID != 0)&&(IN_pWnd != NULL)){
		switch(ID){
			case 0x01:	
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCNTSCOptWnd->InitPrm();
				break;
			case 0x02:	
				break;
			case 0x03:
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->Initgen(&((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->CenterVal);
				break;
			case 0x04:
				((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->InitPrm();
				break;
			case 0x05:
				((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->InitPrm();
				break;
			case 0x15:
				((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->InitPrm();
				break;
			case 0x06://화각
				((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->InitPrm();
				break;
			case 0x07://반전
				((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->InitPrm();
				break;
			case 0x08://저조도
				((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->InitPrm();
				break;
			case 0x09:
				((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->InitPrm();

				break;
			case 0x0a://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->InitPrm();
				break;

			case 0x10://이물_LG
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->InitPrm();
				break;

			case 0x11://오버레이포지션
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlayPositionOptWnd->InitPrm();
				break;

			case 0x12://저조도(백점검사)
				((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->InitPrm();
				break;

			case 0x13://Dynamic Range
				((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->InitPrm();
				break;
			case 0x16://3D depth
				((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->InitPrm();
				break;
			case 0x17://HotPixel
				((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->InitPrm();
				break;
			case 0x18:
				((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->InitPrm();
				break;

			case 0x19://온도 센서
				((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->InitPrm();
				break;

			case 0x0b://광량비
				((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->InitPrm();
				break;
			case 0x0c:
				((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->InitPrm();
				break;
			case 0x0d:
				((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->InitPrm();

				break;
			case 0x0e:
				((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->InitPrm();

				break;
			case 0x0f:
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->InitPrm();
				break;
			case 0x1f:
				((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->InitPrm();
				break;
			case 0x1a:
				((CImageTesterDlg *)m_pMomWnd)->m_pDTCOptWnd->InitPrm();
				break;
			case 0x1b:
				((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->InitPrm();
				break;
			case 0x1c:
				((CImageTesterDlg *)m_pMomWnd)->m_pVEROptWnd->InitPrm();
				break;
			case 0xfe: //standard option
				break;
			case 0x2f:
				((CImageTesterDlg *)m_pMomWnd)->m_pVerifyOptWnd->InitPrm();
				break;
			case 0xff:

				break;
			default:
				break;
		}
	}
}

void CEtcModelDlg::Result_Initstat()
{
	Result.m_Success = FALSE;
	Result.ValString = "";
	if((m_ID != 0)&&(IN_pWnd != NULL)){
		switch(m_ID){
			case 0x01:	
				((CImageTesterDlg *)m_pMomWnd)->m_pResCNTSCOptWnd->Initstat();
				break;
			case 0x02:	
				break;
			case 0x03:
				((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->InitStat();
				break;
			case 0x04:
				((CImageTesterDlg *)m_pMomWnd)->m_pResRotateOptWnd->Initstat();
				break;
			case 0x05:
				((CImageTesterDlg *)m_pMomWnd)->m_pResResolutionOptWnd->Initstat();
				break;
			case 0x15:
				((CImageTesterDlg *)m_pMomWnd)->m_pResSFROptWnd->Initstat();
				break;
			case 0x06://화각
				((CImageTesterDlg *)m_pMomWnd)->m_pResAngleOptWnd->initstat();
				break;
			case 0x07://반전
				((CImageTesterDlg *)m_pMomWnd)->m_pResColorRvsOptWnd->initstat();
				break;
			case 0x08://저조도
				((CImageTesterDlg *)m_pMomWnd)->m_pResPatternNoiseOptWnd->initstat();
				break;
			case 0x09:
				((CImageTesterDlg *)m_pMomWnd)->m_pResIRFilterOptWnd->Initstat();

				break;
			case 0x0a://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptWnd->Initstat();
				break;
			case 0x10://이물_LG
				((CImageTesterDlg *)m_pMomWnd)->m_pResParticleOptLGWnd->Initstat();
				break;

			case 0x11://오버레이포지션
				((CImageTesterDlg *)m_pMomWnd)->m_pResOverlayPositionOptWnd->Initstat();
				break;

			case 0x12://저조도(백점검사)
				((CImageTesterDlg *)m_pMomWnd)->m_pResLowLightOptWnd->Initstat();
				break;

			case 0x13://Dynamic Range
				((CImageTesterDlg *)m_pMomWnd)->m_pResDynamicRangeOptWnd->Initstat();
				break;

			case 0x0b://광량비
				((CImageTesterDlg *)m_pMomWnd)->m_pResBrightnessOptWnd->Initstat();
				break;
			case 0x0c:
				((CImageTesterDlg *)m_pMomWnd)->m_pResFixedPatternWnd->Initstat();
				//((CImageTesterDlg *)m_pMomWnd)->m_pResCOpticPointOptWnd->InitStat();
				break;
			case 0x0d:
				((CImageTesterDlg *)m_pMomWnd)->m_pResDistortionOptWnd->initstat();

				break;
			case 0x0e:
				((CImageTesterDlg *)m_pMomWnd)->m_pResColorOptWnd->initstat();

				break;
			case 0x0f:
				((CImageTesterDlg *)m_pMomWnd)->m_pResOverlay_CAN_OptWnd->InitStat();
				break;
			case 0x1f:
				((CImageTesterDlg *)m_pMomWnd)->m_pResWarning_CAN_OptWnd->InitStat();
				break;
			
			case 0x1a:
				((CImageTesterDlg *)m_pMomWnd)->m_pResDTCOptWnd->InitStat();
				break;

			case 0x1b:
				((CImageTesterDlg *)m_pMomWnd)->m_pResSYSTEMOptWnd->InitStat();
				break;

			case 0x1c:
				((CImageTesterDlg *)m_pMomWnd)->m_pResVEROptWnd->InitStat();
				break;
			case 0x2f: //standard option
				((CImageTesterDlg *)m_pMomWnd)->m_pResVerifyOptWnd->InitStat();
				break;

			case 0xfe: //standard option
				break;
			case 0xff:

				break;
			default:
				break;
		}
	}
}

void CEtcModelDlg::run(int ID)
{
	((CEdit*)GetDlgItem(IDC_STRING_STATE))->SetWindowText("");
	Result_Initstat();
	
	CString num="";
	num.Format("NUM %d_START ",ID);
	LogWriteToFile(	m_INFO.NAME + num);

	m_szMesResult = _T("");
	TestState(2,"PROCESSING.");
	
	
	if((ID != 0xfe) && (ID !=0xff)){
		if (((CImageTesterDlg  *)m_pMomWnd)->b_Chk_TestSkip){
			if (((CImageTesterDlg *)m_pMomWnd)->TestData[m_INFO.Number - 1].m_Success == 2){
				if (((CImageTesterDlg *)m_pMomWnd)->TestData[0].m_Success == 2){
					((CEdit*)GetDlgItem(IDC_STRING_STATE))->SetWindowText("X");
					((CImageTesterDlg *)m_pMomWnd)->TestData[m_INFO.Number].m_Success = 2;
					if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
						FAIL_UPLOAD();
					}
					Result.m_Success = FALSE;
					Result.ValString = "X";
					TestState(3, "X");
					LogWriteToFile(m_INFO.NAME + "FAIL");
					return;
				}
				else{
					((CEdit*)GetDlgItem(IDC_STRING_STATE))->SetWindowText("X");
					((CImageTesterDlg *)m_pMomWnd)->TestData[m_INFO.Number].m_Success = 2;
					if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
						FAIL_UPLOAD();
					}
					Result.m_Success = FALSE;
					Result.ValString = "X";
					TestState(3, "X");
					LogWriteToFile(m_INFO.NAME + "FAIL");
					return;
				}
			}
		}
		JIG_MOVE_CHK(ID);//테스트전 지그의 위치 셋팅이 필요하다면 지그를 움직인다. 

		if(!((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK()){
			((CEdit*)GetDlgItem(IDC_STRING_STATE))->SetWindowText("CAMERA DISCONNECT");
			((CImageTesterDlg *)m_pMomWnd)->TestData[m_INFO.Number].m_Success =2;
			if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
				FAIL_UPLOAD();
			}
			//----------------------------------------------------------개별TEST LOG
			if (((CImageTesterDlg *)m_pMomWnd)->LotMod != 1)
				Excel_Save();
			//----------------------------------------------------------
			Result.m_Success = FALSE;
			Result.ValString = "CAMERA DISCONNECT";
			TestState(1,"FAIL");
			LogWriteToFile(	m_INFO.NAME + "CAM DISCONNECT");

			return;
		}
		VideoModeCheck();

		if (((CImageTesterDlg *)m_pMomWnd)->mB_Check_RegisterChange)
		{
			if (!PowerModeCheck()){
				((CEdit*)GetDlgItem(IDC_STRING_STATE))->SetWindowText("Register Change Fail");
				((CImageTesterDlg *)m_pMomWnd)->TestData[m_INFO.Number].m_Success = 2;
				if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
					FAIL_UPLOAD();
				}
				//----------------------------------------------------------개별TEST LOG
				if (((CImageTesterDlg *)m_pMomWnd)->LotMod != 1)
					Excel_Save();
				//----------------------------------------------------------
				Result.m_Success = FALSE;
				Result.ValString = "Register Change Fail";
				TestState(1, "FAIL");
				LogWriteToFile(m_INFO.NAME + "Register Change Fail");

				return;
			}
		}
		

	}

// 	if (!JIG_MOVE_CHK(ID)){//테스트전 지그의 위치 셋팅이 필요하다면 지그를 움직인다. 
// 		Result.m_Success = FALSE;
// 		Result.ValString = "CYL MOVE FAIL";
// 		((CImageTesterDlg *)m_pMomWnd)->TestData[m_INFO.Number].m_Success = 2;
// 		TestState(1, "FAIL");
// 		((CImageTesterDlg *)m_pMomWnd)->TEST_STAT(2, Result.ValString);
// 		((CEdit*)GetDlgItem(IDC_STRING_STATE))->SetWindowText(Result.ValString);
// 		return;
// 
// 	}

	((CImageTesterDlg *)m_pMomWnd)->OnSelchangingModSubCtrtab();
	if(ID != 0xff){
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptTabWnd->SetCurSel(m_INFO.Number);
		((CImageTesterDlg *)m_pMomWnd)->m_pModResTabWnd->SetCurSel(m_INFO.Number);
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptTabWnd->SetCurSel(0);
		((CImageTesterDlg *)m_pMomWnd)->m_pModResTabWnd->SetCurSel(0);
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
	}
	((CImageTesterDlg *)m_pMomWnd)->OnSelchangeModSubCtrtab();

	
	BOOL REGSTATE = TRUE;
	
	if((ID != 0)&&(IN_pWnd != NULL)){
		
		
		switch(ID){
			case 0x01://왜곡광축 세팅
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pSubCNTSCOptWnd->Run();
				LogWriteToFile(_T("NTSC 검사종료"));
				break;
			case 0x02:
				break;
			case 0x03:
			//	REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->Run();
				LogWriteToFile(_T("보임량 검사종료"));
				break;
			case 0x04:
			//	REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->Run();

				LogWriteToFile(_T("로테이트 검사종료"));
				break;
			case 0x05:
			//	REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->Run();
				LogWriteToFile(_T("해상력 검사종료"));
				break;
			case 0x15:
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->Run();
				LogWriteToFile(_T("SFR 검사종료"));
				break;
			case 0x06://화각
			//	REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->Run();
				LogWriteToFile(_T("화각 검사종료"));
				break;
			case 0x07://반전
			//	REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->Run();
				LogWriteToFile(_T("역상 검사종료"));
				break;
			case 0x08://저조도
				//((CImageTesterDlg *)m_pMomWnd)->LightRAY_ONOFF(0,0);//
			//	REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_LOWLIGHT_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->Run();
				LogWriteToFile(_T("저조도 검사종료"));
				break;	
			case 0x09:
				//((CImageTesterDlg *)m_pMomWnd)->RAY_ONOFF(1);
			//	REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->Run();

				LogWriteToFile(_T("IR FILTER 검사종료"));
				break;
			case 0x0a://이물
			//	REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->Run();
				LogWriteToFile(_T("이물 검사종료"));
				break;
			case 0x10://이물_LG
//				REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->Run();
				break;
			case 0x11://오버레이포지션
//				REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pOverlayPositionOptWnd->Run();
				((CImageTesterDlg *)m_pMomWnd)->Overlay_State = 1;
				break;
			case 0x12://저조도 백점검사
//				REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->Run();
				break;

			case 0x13://Dynamic Range
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->Run();
				break;
			case 0x16://3D depth
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->Run();
				break;
			case 0x17://3D depth
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->Run();
				break;
			case 0x18://
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->Run();
				break;

			case 0x19://온도 센서
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->Run();
				break;

			case 0x0b://광량비
			//	REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->Run();
				LogWriteToFile(_T("광량비 검사종료"));
				break;
			case 0x0c:
			//	REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->Run();
				LogWriteToFile(_T("FixedPattern 검사종료"));
				//Result = ((CImageTesterDlg *)m_pMomWnd)->m_pSubCOpticPointOptWnd->Run();
				//LogWriteToFile(_T("왜곡 검사종료"));
				break;
			case 0x0d:
			//	REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->Run();
				LogWriteToFile(_T("DISTORTION 검사종료"));
				break;
			case 0x0e://컬러
			//	REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->Run();
				LogWriteToFile(_T("컬러 검사종료"));
				break;

			case 0x0f://오버레이 CAN
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->Run();
				((CImageTesterDlg *)m_pMomWnd)->Overlay_State = 1;
				LogWriteToFile(_T("오버레이 CAN 검사종료"));
				break;

			case 0x1f://경고문구 CAN
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->Run();
				((CImageTesterDlg *)m_pMomWnd)->Overlay_State = 1;
				LogWriteToFile(_T("경고문구 CAN 검사종료"));
				break;

			case 0x1a://DTC ERASE
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pDTCOptWnd->Run();
				LogWriteToFile(_T("DTC 검사종료"));
				break;

			case 0x1b://Mfgdata W/R
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->Run();
				LogWriteToFile(_T("Mfgdata W/R 검사종료"));
				break;
			
			case 0x1c://버전검사
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pVEROptWnd->Run();
				LogWriteToFile(_T("버전 검사종료"));
				break;

			case 0xfe: 
				Result = ((CImageTesterDlg *)m_pMomWnd)->Model_Start();
				LogWriteToFile(_T("모델시작 검사종료"));
				break;
			case 0x2f:
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pVerifyOptWnd->Run();
				break;
			case 0xff:
				Result = ((CImageTesterDlg *)m_pMomWnd)->Model_End(m_INFO.Number);
				break;
			default:
				IN_pWnd = NULL;
		}
	}
	//----------------------------------------------------------개별TEST LOG
// 	if ((ID != 0xfe) && (ID != 0xff)){
// 		CTime thetime = CTime::GetCurrentTime();
// 		CString strTime, strDay;
// 
// 		int hour = 0, minute = 0, second = 0, hour2 = 0;
// 		//int year2 = YEAR-2000; 
// 
// 		hour = thetime.GetHour(); minute = thetime.GetMinute(); second = thetime.GetSecond();
// 
// 		if (hour > 12){/////////////////////////////////////////////////////시간
// 			hour2 = hour - 12;
// 			strTime.Empty();
// 			strTime.Format("PM%02d_%02d_%02d", hour2, minute, second);
// 		}
// 		else{
// 			strTime.Empty();
// 			strTime.Format("AM%02d_%02d_%02d", hour, minute, second);///////////////////시간
// 		}
// 		((CImageTesterDlg *)m_pMomWnd)->PNG_ImageCapture(strTime);
// 		((CImageTesterDlg *)m_pMomWnd)->RAW_ImageCapture(strTime);
// 	}
// 	if (((CImageTesterDlg *)m_pMomWnd)->LotMod != 1)
// 		Excel_Save();
	//----------------------------------------------------------
	LogWriteToFile(	m_INFO.NAME + "TEST END");
	
	((CImageTesterDlg *)m_pMomWnd)->TestData[m_INFO.Number].ValString = Result.ValString;

	if(Result.m_Success == TRUE){
	//	DoEvents(1000);
		((CImageTesterDlg *)m_pMomWnd)->TestData[m_INFO.Number].m_Success =1;
		TestState(0,"PASS");

		if(((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
			if(RESULT_AUTO_CAPTURE)
			{
		//==================================성공일때 저장==================================================//
				if((ID != 0xfe)&&(ID != 0xff)){
					CString str ="";
					CString str_folder ="";
					CString str_file ="";
					if(m_INFO.NAME != ""){
						str.Format("PASS_%04d_"+m_INFO.NAME+".BMP",((CImageTesterDlg *)m_pMomWnd)->Lot_Totalint+1);
						str_folder = ((CImageTesterDlg *)m_pMomWnd)->LOT_FOLDERPATH +_T("\\PASS_IMG");
						folder_gen(str_folder);
						str_file = str_folder +_T("\\")+str;
						((CImageTesterDlg *)m_pMomWnd)->SaveScan(((CImageTesterDlg *)m_pMomWnd)->FrameWnd, str_file);					
					}		
				}
		//=================================================================================================//
			}
		}
	}else{
		
		((CImageTesterDlg *)m_pMomWnd)->TestData[m_INFO.Number].m_Success =2;

		if((ID != 0xfe)&&(ID != 0xff)){
			if(((CImageTesterDlg *)m_pMomWnd)->m_ScanState == FALSE){
				memcpy(m_pFailRaw,m_pImage,sizeof(m_pFailRaw));
				((CImageTesterDlg *)m_pMomWnd)->m_ScanState = TRUE;
				((CImageTesterDlg *)m_pMomWnd)->str_FailName = m_INFO.NAME;
			}
		}
		TestState(1,"FAIL");
	}
	((CEdit*)GetDlgItem(IDC_STRING_STATE))->SetWindowText(Result.ValString);

	LogWriteToFile(	m_INFO.NAME + "TEST 결과산출");

}

void CEtcModelDlg::TextView(LPCSTR lpcszString, ...){
	((CEdit*)GetDlgItem(IDC_STRING_STATE))->SetWindowText(lpcszString);
}

BOOL CEtcModelDlg::JIG_MOVE_CHK(int ID)
{
	if(b_luritechDebugMODE == 1){
		return TRUE;
	}

	CImageTesterDlg*	pMain = (CImageTesterDlg*)m_pMomWnd;
	if(pMain)
		m_pMotion	=	&pMain->m_Motion;

	if((ID != 0)&&(IN_pWnd != NULL)){
		switch(ID){
			case 0x01://NTSC 세팅
// 				LogWriteToFile(_T("NTSC 테스트 동작 시작"));
// 				if(m_pMotion){
// 					m_pMotion->TestPosMove();
// 				}
// 				((CImageTesterDlg *)m_pMomWnd)->Light_ONOFF(1);//
// 				LogWriteToFile(_T("NTSC 테스트 동작 종료"));
				break;
			case 0x02:
			case 0x03://광축 
				if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
					((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE);

				LogWriteToFile(_T("화각 모터 동작 시작"));
				((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE();
				LogWriteToFile(_T("화각 모터 동작 종료"));
				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(1);
				break;
			case 0x04:
				if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
					((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE);

				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(1);
				break;
			case 0x15://SFR
				if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
					((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE);

				LogWriteToFile(_T("SFR 모터 동작 시작"));
				((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE();
				LogWriteToFile(_T("SFR 모터 동작 종료"));
				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(1);
				break;
			case 0x06://화각
				if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
					((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE);

				LogWriteToFile(_T("화각 모터 동작 시작"));
				((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE();
				LogWriteToFile(_T("화각 모터 동작 종료"));
				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(1);
				break;
			case 0x07://반전
// 				LogWriteToFile(_T("모터 동작 시작"));
// 				((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE();
// 				LogWriteToFile(_T("모터 동작 종료"));
				break;

			case 0x08://저조도
// 				if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
// 					((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel);

				((CImageTesterDlg *)m_pMomWnd)->Light_ONOFF(1);
				
				LogWriteToFile(_T("저조도모터 동작 시작"));
				if(m_pMotion)
					m_pMotion->TestPosMove();

				
				LogWriteToFile(_T("저조도모터 동작 종료"));

//				return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(0);
				break;	
			case 0x0f://오버레이
// 				LogWriteToFile(_T("오버레이 동작 시작"));
// 				if(m_pMotion)
// 					m_pMotion->TestPosMove();
// 				((CImageTesterDlg *)m_pMomWnd)->Light_ONOFF(0);//
// 				LogWriteToFile(_T("오버레이 동작 종료"));
				break;
			case 0x1f://경고문구
// 				LogWriteToFile(_T("경고문구 동작 시작"));
// 				if(m_pMotion)
// 					m_pMotion->TestPosMove();
// 				((CImageTesterDlg *)m_pMomWnd)->Light_ONOFF(0);//
// 				LogWriteToFile(_T("경고문구 동작 종료"));
				break;	
			case 0x09://IR

// 				LogWriteToFile(_T("IR 모터 동작 시작"));
// 				if(m_pMotion)
// 					m_pMotion->TestPosMove();
// 				((CImageTesterDlg *)m_pMomWnd)->RAY_ONOFF(1);
// 
// 				LogWriteToFile(_T("IR 모터 동작 종료"));
				break;
			case 0x10://이물_LG
// 				LogWriteToFile(_T("이물 검사 LG 동작 시작"));
// 				if(m_pMotion)
// 					m_pMotion->TestPosMove();
// 				((CImageTesterDlg *)m_pMomWnd)->Light_ONOFF(1);
// 				LogWriteToFile(_T("이물 검사 LG 동작 종료"));
				break;
			case 0x11://오버레이포지션
				// 				LogWriteToFile(_T("모터 동작 시작"));
				// 				((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE();
				// 				LogWriteToFile(_T("모터 동작 종료"));
				break;
			case 0x12://저조도(백점검사)
// 				LogWriteToFile(_T("저조도(백점) 동작 시작"));
// 				if(m_pMotion)
// 					m_pMotion->TestPosMove();
// 				((CImageTesterDlg *)m_pMomWnd)->Light_ONOFF(0);
// 				LogWriteToFile(_T("저조도(백점) 동작 종료"));
				break;
			case 0x13://Dynamic Range
// 				if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
// 					((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel);

				LogWriteToFile(_T("Dynamic Range 모터 동작 시작"));

				((CImageTesterDlg *)m_pMomWnd)->Light_ONOFF(1);
				if (m_pMotion)
					m_pMotion->TestPosMove();
				LogWriteToFile(_T("Dynamic Range 모터 동작 종료"));
				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(1);
				break;

			case 0x17://이물
				if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
					// 					((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel);

					((CImageTesterDlg *)m_pMomWnd)->Light_ONOFF(1);

				LogWriteToFile(_T("HotPixel 모터 동작 시작"));
				if (m_pMotion)
					m_pMotion->TestPosMove();

				LogWriteToFile(_T("HotPixel 모터 동작 종료"));
				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(0);
				break;
			case 0x18:
				if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
					// 					((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel);

					((CImageTesterDlg *)m_pMomWnd)->Light_ONOFF(0);
					//((CImageTesterDlg *)m_pMomWnd)->Control_Current_SLOT(3, 0);

				LogWriteToFile(_T("Depth Noise 모터 동작 시작"));
				if (m_pMotion)
					m_pMotion->TestPosMove();

				LogWriteToFile(_T("Depth Noise 모터 동작 종료"));
				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(0);
				break;

			case 0x19://온도 센서
				//if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
				//	// 					((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel);

				//	((CImageTesterDlg *)m_pMomWnd)->Light_ONOFF(0);
				////((CImageTesterDlg *)m_pMomWnd)->Control_Current_SLOT(3, 0);

				//LogWriteToFile(_T("Depth Noise 모터 동작 시작"));
				//if (m_pMotion)
				//	m_pMotion->TestPosMove();

				//LogWriteToFile(_T("Depth Noise 모터 동작 종료"));
				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(0);
				break;


			case 0x0a://이물
// 				if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
// 					((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel);

				((CImageTesterDlg *)m_pMomWnd)->Light_ONOFF(1);

				LogWriteToFile(_T("이물 모터 동작 시작"));
				if (m_pMotion)
					m_pMotion->TestPosMove();

				LogWriteToFile(_T("이물 모터 동작 종료"));
				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(0);
				break;
			case 0x0b://광량비
// 				if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
// 					((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel);

				((CImageTesterDlg *)m_pMomWnd)->Light_ONOFF(1);
				
				LogWriteToFile(_T("광량비 모터 동작 시작"));
				if(m_pMotion)
					m_pMotion->TestPosMove();

				LogWriteToFile(_T("광량비 모터 동작 종료"));
				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(0);
				break;
			case 0x0c://Fixed Pattern
// 				if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
// 					((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE_Particel);

				((CImageTesterDlg *)m_pMomWnd)->Light_ONOFF(0);
				//((CImageTesterDlg *)m_pMomWnd)->Control_Current_SLOT(3, 0);

				
				LogWriteToFile(_T("Fixed Pattern 모터 동작 시작"));
				if (m_pMotion)
					m_pMotion->TestPosMove();

				LogWriteToFile(_T("Fixed Pattern 모터 동작 종료"));
				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(0);
				break;

			case 0x0d://Distortion
				if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
					((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE);

				LogWriteToFile(_T("Distortion 모터 동작 시작"));
				((CImageTesterDlg *)m_pMomWnd)->DISTORTION_JIG_MOVE();
				LogWriteToFile(_T("Distortion 모터 동작 종료"));
				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(1);
				break;

			case 0x16://3D
				if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
					((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE);

				LogWriteToFile(_T("3D Depth 모터 동작 시작"));
				((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE();
				LogWriteToFile(_T("3D Depth 모터 동작 종료"));
				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(1);
				break;


			case 0x0e:
// 				LogWriteToFile(_T("컬러 모터 동작 시작"));
// 				((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE();
// 				LogWriteToFile(_T("컬러 모터 동작 종료"));
				break;

				case 0xfe:
					if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
						((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE);

				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(1);
				break;
				case 0xff:
					if (((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
						((CImageTesterDlg *)m_pMomWnd)->SetProperty_ALL(((CImageTesterDlg *)m_pMomWnd)->CAMSET_SAVE);
				//return ((CImageTesterDlg *)m_pMomWnd)->Cylinder_MOVE(1);
				break;
			case 0x2f:
			break;	
			default:
				break;
			//	IN_pWnd = NULL;
		}
	}
	return TRUE;
}

void CEtcModelDlg::JIG_MOVE_CHK()
{
	JIG_MOVE_CHK(m_ID);
}

void CEtcModelDlg::ETC_Run(int ID)
{	
	//테스트전 다른 런이 동작하는지 확인후 동작하고 있다면 그냥 빠져나온다. 
	
	Result_Initstat();
	((CImageTesterDlg *)m_pMomWnd)->RUN_MODE_CHK(1);
	InitPrm();
	CImageTesterDlg*	pMain = (CImageTesterDlg*)m_pMomWnd;
	if(pMain)
		m_pMotion	=	&pMain->m_Motion;
	((CImageTesterDlg *)m_pMomWnd)->OnSelchangingModSubCtrtab();
	if(ID != 0xff){
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptTabWnd->SetCurSel(m_INFO.Number);
		((CImageTesterDlg *)m_pMomWnd)->m_pModResTabWnd->SetCurSel(m_INFO.Number);
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_pModOptTabWnd->SetCurSel(0);
		((CImageTesterDlg *)m_pMomWnd)->m_pModResTabWnd->SetCurSel(0);
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
	}
	((CImageTesterDlg *)m_pMomWnd)->OnSelchangeModSubCtrtab();

	((CEdit*)GetDlgItem(IDC_STRING_STATE))->SetWindowText("");
	TestState(2,"PROCESSING.");

	//----------------------------------------------------------개별TEST LOG
// 	CTime thetime = CTime::GetCurrentTime();
// 	CString strTime, strDay;
// 
// 	int hour = 0, minute = 0, second = 0, hour2 = 0;
// 	//int year2 = YEAR-2000; 
// 
// 	hour = thetime.GetHour(); minute = thetime.GetMinute(); second = thetime.GetSecond();
// 
// 	if (hour > 12){/////////////////////////////////////////////////////시간
// 		hour2 = hour - 12;
// 		strTime.Empty();
// 		strTime.Format("PM%02d:%02d:%02d", hour2, minute, second);
// 	}
// 	else{
// 		strTime.Empty();
// 		strTime.Format("AM%02d:%02d:%02d", hour, minute, second);///////////////////시간
// 	}
// 	((CImageTesterDlg *)m_pMomWnd)->strTime = strTime;
	//-------------------------------------------------------------------

	((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->STATE_VIEW(1);
	if((ID >= 0x08) && (ID <= 0x0b)){

		//if(((CImageTesterDlg *)m_pMomWnd)->b_OverlayRegionCheck == 1){
		//	((CImageTesterDlg *)m_pMomWnd)->Overlay_InputBuffer(0);// 오버레이 영역은 0 아닌 영역 1.
		//}else{
			((CImageTesterDlg *)m_pMomWnd)->OverlayRegionINIT(1);///오버레이 영역 제거 안할 시 모든 버퍼 1로 세팅
		//}
	}

	if (((CImageTesterDlg *)m_pMomWnd)->b_DoorOpen == TRUE || ((CImageTesterDlg *)m_pMomWnd)->b_SocketReady == FALSE) // 소켓 도어 , 소켓 위치 확인
	{
		if (((CImageTesterDlg *)m_pMomWnd)->b_DoorOpen == TRUE)// 도어가 열려있음
		{
			TestState(1, "FAIL");
			((CEdit*)GetDlgItem(IDC_STRING_STATE))->SetWindowText("SOCKET OPEN");
			AfxMessageBox("Please check the socket door and try again.");
		}
		else if (((CImageTesterDlg *)m_pMomWnd)->b_SocketReady == FALSE)// 소켓이 누워 있음
		{
			TestState(1, "FAIL");
			((CEdit*)GetDlgItem(IDC_STRING_STATE))->SetWindowText("SOCKET ERROR");
			AfxMessageBox("Please check the socket status and try again.");
		}
		return;
	}

	JIG_MOVE_CHK();


	if ((ID != 0xfe) && (ID != 0xff)){//테스트에 카메라가 입력되지 않는다면 에러를 표시한다. 
		//테스트전 전원을 넣어야 한다면 전원을 넣는다.

		if (!((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK()){

			((CImageTesterDlg *)m_pMomWnd)->Power_On();//전원을 넣었다면 안정화되기까지 대기한다.
			DoEvents(1000);//전원을 넣었다면 안정화되기까지 대기한다.
		}
		VideoModeCheck();

		if (((CImageTesterDlg *)m_pMomWnd)->mB_Check_RegisterChange){
			PowerModeCheck();
		}

		if (!((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK()){
			((CEdit*)GetDlgItem(IDC_STRING_STATE))->SetWindowText("CAMERA DISCONNECT");
			TestState(1, "FAIL");
			((CImageTesterDlg *)m_pMomWnd)->RUN_MODE_CHK(0);
			return;
		}
	}

	BOOL REGSTATE = TRUE;
	//((CImageTesterDlg *)m_pMomWnd)->m_AWBSTATE = FALSE;
	if((ID != 0)&&(IN_pWnd != NULL)){
		switch(ID){
			case 0x01://왜곡광축 세팅
				Result  = ((CImageTesterDlg *)m_pMomWnd)->m_pSubCNTSCOptWnd->Run();
				break;
			case 0x02:
				break;
			case 0x03:
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->Run();
				break;
			case 0x04:
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->Run();
				break;
			case 0x05:
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->Run();
				break;
			case 0x15:
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->Run();
				break;
			case 0x06://화각
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->Run();
				break;
			case 0x07://반전
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->Run();
				break;
			case 0x08://저조도
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_LOWLIGHT_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->Run();
				break;	
			case 0x09:
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->Run();
				break;
			case 0x0a://이물
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->Run();
				break;
			case 0x10://이물_LG
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->Run();
				break;
			case 0x11://오버레이포지션
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pOverlayPositionOptWnd->Run();
				break;
			case 0x12://저조도(백점검사)
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->Run();
				break;
			case 0x13://Dynamic Range
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->Run();
				break;
			case 0x16://3D depth
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->Run();
				break;
			case 0x17:// HotPixel
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->Run();
				break;
			case 0x18:// 
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->Run();
				break;

			case 0x19://온도 센서
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->Run();
				break;

			case 0x0b://광량비
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->Run();
				break;
			case 0x0c:
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->Run();
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				//Result = ((CImageTesterDlg *)m_pMomWnd)->m_pSubCOpticPointOptWnd->Run();
				break;
			case 0x0d:
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->Run();
				break;
			case 0x0e://컬러
				//REGSTATE = ((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->Run();
				break;
			case 0x0f://오버레이 CAN
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->Run();
				break;

			case 0x1f://경고문구 CAN
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->Run();
				break;
			
			case 0x1a://DTC
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pDTCOptWnd->Run();
				break;

			case 0x1b://시스템 정보
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->Run();
				break;

			case 0x1c://시스템 정보
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pVEROptWnd->Run();
				break;

			case 0xfe: //standard option
				Result = ((CImageTesterDlg *)m_pMomWnd)->Model_Start();
				break;
			case 0x2f:
				Result = ((CImageTesterDlg *)m_pMomWnd)->m_pVerifyOptWnd->Run();
				break;
			case 0xff:
				Result = ((CImageTesterDlg *)m_pMomWnd)->Model_End(m_INFO.Number);
				break;
			
		}
	}
	//----------------------------------------------------------개별TEST LOG
// 	if ((ID != 0xfe) && (ID != 0xff)){
// 		if (hour > 12){/////////////////////////////////////////////////////시간
// 			hour2 = hour - 12;
// 			strTime.Empty();
// 			strTime.Format("PM%02d_%02d_%02d", hour2, minute, second);
// 		}
// 		else{
// 			strTime.Empty();
// 			strTime.Format("AM%02d_%02d_%02d", hour, minute, second);///////////////////시간
// 		}
// 		((CImageTesterDlg *)m_pMomWnd)->PNG_ImageCapture(strTime);
// 		((CImageTesterDlg *)m_pMomWnd)->RAW_ImageCapture(strTime);
// 	}
// 	Excel_Save();
	//----------------------------------------------------------

	if(ID == 0xff){
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;
	}
	if(Result.m_Success == TRUE){
		TestState(0,"PASS");
		((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->STATE_VIEW(2);

	}else{
		TestState(1,"FAIL");
		((CImageTesterDlg *)m_pMomWnd)->m_pModSetDlgWnd->STATE_VIEW(3);

	}
	((CEdit*)GetDlgItem(IDC_STRING_STATE))->SetWindowText(Result.ValString);
	((CImageTesterDlg *)m_pMomWnd)->RUN_MODE_CHK(0);
}

void CEtcModelDlg::stop()
{
	TestState(1,"FAIL");
	if(((CImageTesterDlg *)m_pMomWnd)->LotMod == TRUE){
		FAIL_UPLOAD();
	}
	
	((CImageTesterDlg *)m_pMomWnd)->TestData[m_INFO.Number].m_Success =2;
	if(m_ID == 0xff){
		run(m_ID);
	}
}
void CEtcModelDlg::run()
{
	run(m_ID);
}

void CEtcModelDlg::ETC_Run()
{
	ETC_Run(m_ID);
}


void CEtcModelDlg::Pic(CDC *cdc)
{
	if(IN_pWnd != NULL){
		switch(m_ID){
			case 0x01:
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCNTSCOptWnd->Pic(cdc);
			break;
			case 0x02:				
				break;
			case 0x03:	
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->Center_Pic(cdc);
				break;
			case 0x04:
				((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->Pic(cdc);

				break;
			case 0x05://해상력
				((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->Pic(cdc);
				break;
			case 0x15:
				((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->Pic(cdc);
				break;
			case 0x06://화각	
				((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->Pic(cdc);
				break;
			case 0x07://반전
				((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->Pic(cdc);
				break;
			case 0x08://저조도
				((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->Pic(cdc);
				break;
			case 0x09:
				((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->Pic(cdc);
				break;
			case 0x0a://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->Pic(cdc);
				break;
			case 0x10://이물_LG
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->Pic(cdc);
				break;

			case 0x11://오버레이포지션
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlayPositionOptWnd->Pic(cdc);
				break;

			case 0x12://저조도(백점검사)
				((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->Pic(cdc);
				break;

			case 0x13://Dynamic Range
				((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->Pic(cdc);
				break;
			case 0x16://Dynamic Range
				((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->Pic(cdc);
				break;
			case 0x17://HotPixel
				((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->Pic(cdc);
				break;
			case 0x18://
				((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->Pic(cdc);
				break;

			case 0x19://온도 센서
				((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->Pic(cdc);
				break;

			case 0x0b://광량비
				((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->Pic(cdc);
				break;

			case 0x0c:
				((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->Pic(cdc);

				//((CImageTesterDlg *)m_pMomWnd)->m_pSubCOpticPointOptWnd->Center_Pic(cdc);
				break;
			case 0x0d:
				((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->Pic(cdc);

				break;
			case 0x0e:
				((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->Pic(cdc);
				break;
			case 0x0f:
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->Pic(cdc);
				break;
			case 0x1f:
				((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->Pic(cdc);
				break;
			case 0x1a:
				((CImageTesterDlg *)m_pMomWnd)->m_pDTCOptWnd->Pic(cdc);
				break;
			case 0x1b:
				((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->Pic(cdc);
				break;
			case 0x1c:
				((CImageTesterDlg *)m_pMomWnd)->m_pVEROptWnd->Pic(cdc);
				break;
			case 0xfe: //standard option
				((CImageTesterDlg *)m_pMomWnd)->Model_Start_Pic(cdc,&Result);
				break;
			case 0xf2:
				break;
			case 0xff:
				break;
			default:
				break;
		}
	}
}
void CEtcModelDlg::OnEnSetfocusEditState()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CEtcModelDlg::Upload_List(CListCtrl *List)
{
	if((m_ID != 0)&&(IN_pWnd != NULL)){
		switch(m_ID){
			case 0x01://왜곡광축 세팅
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCNTSCOptWnd->Set_List(List);
				break;
			case 0x02:
				break;
			case 0x03:
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->Set_List(List);
				break;
			case 0x04:
				((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->Set_List(List);
				break;
			case 0x05:
				((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->Set_List(List);
				break;
			case 0x15:
				((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->Set_List(List);
				break;
			case 0x06://화각
				((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->Set_List(List);
				break;
			case 0x07://반전
				((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->Set_List(List);
				break;
			case 0x08://저조도
				((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->Set_List(List);
				break;
			case 0x09:
				((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->Set_List(List);

				break;
			case 0x0a://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->Set_List(List);
				break;
			case 0x10://이물_LG
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->Set_List(List);
				break;
			case 0x11://오버레이포지션
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlayPositionOptWnd->Set_List(List);
				break;

			case 0x12://저조도(백점검사)
				((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->Set_List(List);
				break;

			case 0x13://Dynamic Range
				((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->Set_List(List);
				break;
			case 0x16://3D Depth
				((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->Set_List(List);
				break;
			case 0x17://3D Depth
				((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->Set_List(List);
				break;
			case 0x18://
				((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->Set_List(List);
				break;

			case 0x19://온도 센서
				((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->Set_List(List);
				break;


			case 0x0b://광량비
				((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->Set_List(List);
				break;
			case 0x0c:
				((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->Set_List(List);
				//((CImageTesterDlg *)m_pMomWnd)->m_pSubCOpticPointOptWnd->Set_List(List);
				break;
			case 0x0d:
				((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->Set_List(List);
				break;
			case 0x0e:
				((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->Set_List(List);
				break;
			case 0x0f:
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->Set_List(List);
				break;
			case 0x1f:
				((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->Set_List(List);
				break;
			case 0x1a:
				((CImageTesterDlg *)m_pMomWnd)->m_pDTCOptWnd->Set_List(List);
				break;
			case 0x1b:
				((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->Set_List(List);
				break;
			case 0x1c:
				((CImageTesterDlg *)m_pMomWnd)->m_pVEROptWnd->Set_List(List);
				break;
			case 0xfe:
				if(((CImageTesterDlg *)m_pMomWnd)->b_Chk_CurrentChkEn == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->Set_List_C(List);
				}
// 				if (((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bReadCheck || ((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bCheckSumCheck)
// 				{
// 					((CImageTesterDlg *)m_pMomWnd)->Set_List_Eeprom(List);
// 				}
				break;
			case 0xff:
				break;
			case 0x2f:
				break;
			default:
				break;
		}
	}
}
void CEtcModelDlg::Upload_Lot_List(CListCtrl *List)
{
	if ((m_ID != 0) && (IN_pWnd != NULL)){
		switch (m_ID){
		case 0x01://왜곡광축 세팅
			((CImageTesterDlg *)m_pMomWnd)->m_pSubCNTSCOptWnd->LOT_Set_List(List);
			break;
		case 0x02:
			break;
		case 0x03:
			((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->LOT_Set_List(List);
			break;
		case 0x04:
			((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->LOT_Set_List(List);
			break;
		case 0x05:
			((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->LOT_Set_List(List);
			break;
		case 0x15:
			((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->LOT_Set_List(List);
			break;
		case 0x06://화각
			((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->LOT_Set_List(List);
			break;
		case 0x07://반전
			((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->LOT_Set_List(List);
			break;
		case 0x08://저조도
			((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->LOT_Set_List(List);
			break;
		case 0x09:
			((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->LOT_Set_List(List);

			break;
		case 0x0a://이물
			((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->LOT_Set_List(List);
			break;
		case 0x10://이물_LG
			((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->LOT_Set_List(List);
			break;
		case 0x11://오버레이포지션
			((CImageTesterDlg *)m_pMomWnd)->m_pOverlayPositionOptWnd->LOT_Set_List(List);
			break;

		case 0x12://저조도(백점검사)
			((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->LOT_Set_List(List);
			break;

		case 0x13://DynamicRange
			((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->LOT_Set_List(List);
			break;
		case 0x16://3D depth
			((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->LOT_Set_List(List);
			break;
		case 0x17://HotPixel
			((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->LOT_Set_List(List);
			break;
		case 0x18://
			((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->LOT_Set_List(List);
			break;

		case 0x19://온도 센서
			((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->LOT_Set_List(List);
			break;

		case 0x0b://광량비
			((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->LOT_Set_List(List);
			break;
		case 0x0c:
			((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->LOT_Set_List(List);
			//((CImageTesterDlg *)m_pMomWnd)->m_pSubCOpticPointOptWnd->Set_List(List);
			break;
		case 0x0d:
			((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->LOT_Set_List(List);
			break;
		case 0x0e:
			((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->LOT_Set_List(List);
			break;
		case 0x0f:
			((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->LOT_Set_List(List);
			break;
		case 0x1f:
			((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->LOT_Set_List(List);
			break;
		case 0x1a:
			((CImageTesterDlg *)m_pMomWnd)->m_pDTCOptWnd->LOT_Set_List(List);
			break;
		case 0x1b:
			((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->LOT_Set_List(List);
			break;
		case 0x1c:
			((CImageTesterDlg *)m_pMomWnd)->m_pVEROptWnd->LOT_Set_List(List);
			break;
		case 0xfe:
			if (((CImageTesterDlg *)m_pMomWnd)->b_Chk_CurrentChkEn == TRUE){
				((CImageTesterDlg *)m_pMomWnd)->LOT_Set_List_C(List);
			}
// 			if (((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bReadCheck || ((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bCheckSumCheck)
// 			{
// 				((CImageTesterDlg *)m_pMomWnd)->LOT_Set_List_Eeprom(List);
// 			}
			break;
		case 0xff:
			break;
		case 0x2f:
			break;
		default:
			break;
		}
	}
}
CString CEtcModelDlg::Mes_Result()
{
	m_szMesResult = "";

	if((m_ID != 0)&&(IN_pWnd != NULL)){
		switch(m_ID){
			case 0x01:
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pSubCNTSCOptWnd->Mes_Result(); //1
				break;
			case 0x02:
				break;
			case 0x03:	// 보임량 광축
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->Mes_Result(); //4,5
				break;
			case 0x04:
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->Mes_Result(); //21
				break;
			case 0x05:
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->Mes_Result(); //10~17
				break;
			case 0x06://화각
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->Mes_Result(); //18,19 
				break;
			case 0x07://반전
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->Mes_Result();
				break;
			case 0x08://저조도
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->Mes_Result();
				break;
			case 0x09:
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->Mes_Result();
				break;
			case 0x0a://이물
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->Mes_Result();//6
				break;
			case 0x10://이물_LG
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->Mes_Result();
				break;
			case 0x11://오버레이포지션
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pOverlayPositionOptWnd->Mes_Result();
				break;
			case 0x12://저조도(백점검사)
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->Mes_Result();//2
				break;
			case 0x13://DynamicRange
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->Mes_Result();
				break;
			case 0x16://3D Depth
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->Mes_Result();
				break;
			case 0x17://HotPixel
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->Mes_Result();
				break;
			case 0x18://
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->Mes_Result();
				break;

			case 0x19://온도 센서
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->Mes_Result();
				break;

			case 0x15:
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->Mes_Result();
				break;
			case 0x0b://광량비
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->Mes_Result();
				break;
			case 0x0c:
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->Mes_Result();
				break;
			case 0x0d:
				m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->Mes_Result();
				break;
			case 0x0e:
				m_szMesResult =  ((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->Mes_Result();
				break;
			case 0x0f:
				m_szMesResult =  ((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->Mes_Result();
				break;
			case 0x1f:
				m_szMesResult =  ((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->Mes_Result();
				break;
			case 0x1a:
				m_szMesResult =  ((CImageTesterDlg *)m_pMomWnd)->m_pDTCOptWnd->Mes_Result();
				break;
			case 0x1b:
				m_szMesResult =  ((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->Mes_Result();
				break;
			case 0x1c:
				m_szMesResult =  ((CImageTesterDlg *)m_pMomWnd)->m_pVEROptWnd->Mes_Result();
				break;
			case 0xfe:
				if(((CImageTesterDlg *)m_pMomWnd)->b_Chk_CurrentChkEn == TRUE){
					m_szMesResult = ((CImageTesterDlg *)m_pMomWnd)->Mes_Result_C();
				}

				((CImageTesterDlg *)m_pMomWnd)->Mes_Result_EEPROM();
			
				break;
			default:
				break;
		}
	}

	return m_szMesResult;
}

void CEtcModelDlg::Excel_Save()
{
	if((m_ID != 0)&&(IN_pWnd != NULL)){
		switch(m_ID){
			case 0x01://왜곡광축 세팅
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCNTSCOptWnd->EXCEL_SAVE();
				break;
			case 0x02:
				break;
			case 0x03:
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->EXCEL_SAVE();

				break;
			case 0x04:
				((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->EXCEL_SAVE();
				break;
			case 0x05:
				((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->EXCEL_SAVE();
				break;
			case 0x15:
				((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->EXCEL_SAVE();
				break;
			case 0x06://화각
				((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->EXCEL_SAVE();
				break;
			case 0x07://반전
				((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->EXCEL_SAVE();
				break;
			case 0x08://저조도
				((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->EXCEL_SAVE();
				break;
			case 0x09:
				((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->EXCEL_SAVE();

				break;
			case 0x0a://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->EXCEL_SAVE();
				break;
			case 0x10://이물_LG
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->EXCEL_SAVE();
				break;
			case 0x11://오버레이포지션
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlayPositionOptWnd->EXCEL_SAVE();
				break;
			case 0x12://저조도(백점검사)
				((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->EXCEL_SAVE();
				break;
			case 0x13://DynamicRange
				((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->EXCEL_SAVE();
				break;
			case 0x16://3D Depth
				((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->EXCEL_SAVE();
				break;
			case 0x17://HotPixel
				((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->EXCEL_SAVE();
				break;
			case 0x18://
				((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->EXCEL_SAVE();
				break;

			case 0x19://온도 센서
				((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->EXCEL_SAVE();
				break;

			case 0x0b://광량비
				((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->EXCEL_SAVE();
				break;
			case 0x0c:
				((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->EXCEL_SAVE();
				//((CImageTesterDlg *)m_pMomWnd)->m_pSubCOpticPointOptWnd->EXCEL_SAVE();
				break;
			case 0x0d:
				((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->EXCEL_SAVE();
				break;
			case 0x0e:
				((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->EXCEL_SAVE();
				break;
			case 0x0f:
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->EXCEL_SAVE();
				break;
			case 0x1f:
				((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->EXCEL_SAVE();
				break;
			case 0x1a:
				((CImageTesterDlg *)m_pMomWnd)->m_pDTCOptWnd->EXCEL_SAVE();
				break;
			case 0x1b:
				((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->EXCEL_SAVE();
				break;
			case 0x1c:
				((CImageTesterDlg *)m_pMomWnd)->m_pVEROptWnd->EXCEL_SAVE();
				break;
			case 0xfe:
				if(((CImageTesterDlg *)m_pMomWnd)->b_Chk_CurrentChkEn == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->EXCEL_SAVE_C();

				}
				if (((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bCheckSumCheck == TRUE
					|| ((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bReadCheck == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->EXCEL_SAVE_Eeprom();

				}


//				((CCenterPointModify_4CHDlg *)m_pMomWnd)->m_pSubStandOptWnd->EXCEL_SAVE();
				break;
			default:
				break;
		}
	}
}

void CEtcModelDlg::FAIL_UPLOAD()
{
	if((m_ID != 0)&&(IN_pWnd != NULL)){
		switch(m_ID){
			case 0x01://왜곡광축 세팅
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCNTSCOptWnd->FAIL_UPLOAD();
				break;
			case 0x02:
				break;
			case 0x03:
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->FAIL_UPLOAD();

				break;
			case 0x04:
				((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->FAIL_UPLOAD();
				break;
			case 0x05:
				((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->FAIL_UPLOAD();
				break;
			case 0x15:
				((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->FAIL_UPLOAD();
				break;
			case 0x06://화각
				((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->FAIL_UPLOAD();
				break;
			case 0x07://반전
				((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->FAIL_UPLOAD();
				break;
			case 0x08://저조도
				((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->FAIL_UPLOAD();
				break;
			case 0x09:
				((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->FAIL_UPLOAD();

				break;
			case 0x0a://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->FAIL_UPLOAD();
				break;
			case 0x10://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->FAIL_UPLOAD();
				break;
			case 0x11://오버레이포지션
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlayPositionOptWnd->FAIL_UPLOAD();
				break;
			case 0x12://저조도 백점검사
				((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->FAIL_UPLOAD();//10
				break;
			case 0x13://DynamicRange
				((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->FAIL_UPLOAD();
				break;
			case 0x16://3D Depth
				((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->FAIL_UPLOAD();
				break;
			case 0x17://HotPixel
				((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->FAIL_UPLOAD();
				break;
			case 0x18://
				((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->FAIL_UPLOAD();
				break;

			case 0x19://온도 센서
				((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->FAIL_UPLOAD();
				break;

			case 0x0b://광량비
				((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->FAIL_UPLOAD();//9
				break;
			case 0x0c:
				((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->FAIL_UPLOAD();
				//((CImageTesterDlg *)m_pMomWnd)->m_pSubCOpticPointOptWnd->FAIL_UPLOAD();//8
				break;
			case 0x0d:
				((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->FAIL_UPLOAD();//7
				break;
			case 0x0e:
				((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->FAIL_UPLOAD();//6
				break;
			case 0x0f:
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->FAIL_UPLOAD();//5
				break;
			case 0x1f:
				((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->FAIL_UPLOAD();//4
				break;
			case 0x1a:
				((CImageTesterDlg *)m_pMomWnd)->m_pDTCOptWnd->FAIL_UPLOAD();//3
				break;
			case 0x1b:
				((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->FAIL_UPLOAD();//2
				break;
			case 0x1c:
				((CImageTesterDlg *)m_pMomWnd)->m_pVEROptWnd->FAIL_UPLOAD();//1
				break;
			case 0xfe:
				if(((CImageTesterDlg *)m_pMomWnd)->b_Chk_CurrentChkEn == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->FAIL_UPLOAD();

				}
// 				if (((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bCheckSumCheck == TRUE
// 					|| ((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bReadCheck == TRUE){
// 					((CImageTesterDlg *)m_pMomWnd)->FAIL_UPLOAD_Eeprom();
// 
// 				}
//				((CCenterPointModify_4CHDlg *)m_pMomWnd)->m_pSubStandOptWnd->EXCEL_SAVE();
				break;
			default:
				break;
		}
	}
}
bool CEtcModelDlg::Excel_Upload()
{
	bool ret = TRUE;
	if((m_ID != 0)&&(IN_pWnd != NULL)){
		switch(m_ID){
			case 0x01://왜곡광축 세팅
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCNTSCOptWnd->EXCEL_UPLOAD();
				break;
			case 0x02:
				break;
			case 0x03:
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->EXCEL_UPLOAD();

				break;
			case 0x04:
				((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->EXCEL_UPLOAD();
				break;
			case 0x05:
				((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->EXCEL_UPLOAD();
				break;
			case 0x15:
				((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->EXCEL_UPLOAD();
				break;
			case 0x06://화각
				((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->EXCEL_UPLOAD();
				break;
			case 0x07://반전
				((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->EXCEL_UPLOAD();
				break;
			case 0x08:
				((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->EXCEL_UPLOAD();
				break;
			case 0x09:
				((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->EXCEL_UPLOAD();

				break;
			case 0x0a://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->EXCEL_UPLOAD();
				break;
			case 0x10://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->EXCEL_UPLOAD();
				break;
			case 0x11://오버레이포지션
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlayPositionOptWnd->EXCEL_UPLOAD();
				break;
			case 0x12://오버레이포지션
				((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->EXCEL_UPLOAD();
				break;
			case 0x13://DynamicRange
				((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->EXCEL_UPLOAD();
				break;
			case 0x16://3D Depth 
				((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->EXCEL_UPLOAD();
				break;
			case 0x17://HotPixel
				((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->EXCEL_UPLOAD();
				break;
			case 0x18://
				((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->EXCEL_UPLOAD();
				break;

			case 0x19://온도 센서
				((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->EXCEL_UPLOAD();
				break;

			case 0x0b://광량비
				((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->EXCEL_UPLOAD();
				break;
			case 0x0c:
				((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->EXCEL_UPLOAD();
				//((CImageTesterDlg *)m_pMomWnd)->m_pSubCOpticPointOptWnd->EXCEL_UPLOAD();
				break;
			case 0x0d:
				((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->EXCEL_UPLOAD();
				break;
			case 0x0e:
				((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->EXCEL_UPLOAD();
				break;
			case 0x0f:
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->EXCEL_UPLOAD();
				break;
			case 0x1f:
				((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->EXCEL_UPLOAD();
				break;
			case 0x1a:
				((CImageTesterDlg *)m_pMomWnd)->m_pDTCOptWnd->EXCEL_UPLOAD();
				break;
			case 0x1b:
				((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->EXCEL_UPLOAD();
				break;
			case 0x1c:
				((CImageTesterDlg *)m_pMomWnd)->m_pVEROptWnd->EXCEL_UPLOAD();
				break;
			case 0xfe:
				if(((CImageTesterDlg *)m_pMomWnd)->b_Chk_CurrentChkEn == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD_C();
				}
				if (((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bCheckSumCheck == TRUE
					|| ((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bReadCheck == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD_Eeprom();

				}
//				((CCenterPointModify_4CHDlg *)m_pMomWnd)->m_pSubStandOptWnd->EXCEL_UPLOAD();
				break;
			default:
				break;
		}
	}
	return ret;
}

#pragma region LOT관련
void CEtcModelDlg::Lot_initCnt()
{
	if((m_ID != 0)&&(IN_pWnd != NULL)){
		switch(m_ID){
			case 0x01:
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCNTSCOptWnd->Lot_StartCnt=0;
				break;	
			case 0x02:
				break;
			case 0x03://중심점
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->Lot_StartCnt=0;
				break;
			case 0x04://로테이트
//				((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->Lot_StartCnt=0;
				break;
			case 0x05://해상도
				((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->Lot_StartCnt=0;
				break;
			case 0x15:
				((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->Lot_StartCnt = 0;
				break;
			case 0x06://화각
				((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->Lot_StartCnt = 0;
				break;
			case 0x07://반전
				((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->Lot_StartCnt=0;
				break;
			case 0x08://저조도
				((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->Lot_StartCnt=0;
				break;
			case 0x09://IR
				((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->Lot_StartCnt=0;
				break;
			case 0x0a://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->Lot_StartCnt=0;
				break;
			case 0x10://이물_LG
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->Lot_StartCnt=0;
				break;
			case 0x11:// overlay position
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlayPositionOptWnd->Lot_StartCnt=0;
				break;
			case 0x12://저조도 백점
				((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->Lot_StartCnt=0;
				break;
			case 0x13://DynamicRange
				((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->Lot_StartCnt = 0;
				break;
			case 0x16://3D Depth
				((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->Lot_StartCnt = 0;
				break;
			case 0x17://HotPixel
				((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->Lot_StartCnt = 0;
				break;
			case 0x18://
				((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->Lot_StartCnt = 0;
				break;

			case 0x19://온도 센서
				((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->Lot_StartCnt = 0;
				break;


			case 0x0b://광량비
				((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->Lot_StartCnt=0;
				break;
			case 0x0c://왜곡
				((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->Lot_StartCnt = 0;
				//((CImageTesterDlg *)m_pMomWnd)->m_pSubCOpticPointOptWnd->Lot_StartCnt=0;
				break;
			case 0x0d://distortion
				((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->Lot_StartCnt=0;
				break;
			case 0x0e://컬러
				((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->Lot_StartCnt=0;
				break;
			case 0x0f://조향연동
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->Lot_StartCnt=0;
				break;
			case 0x1a://DTC 진단
				((CImageTesterDlg *)m_pMomWnd)->m_pDTCOptWnd->Lot_StartCnt=0;
				break;
			case 0x1b://시스템 진단
				((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->Lot_StartCnt=0;
				break;
			case 0x1c://버전검사
				((CImageTesterDlg *)m_pMomWnd)->m_pVEROptWnd->Lot_StartCnt=0;
				break;
			case 0x1f://경고문구
				((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->Lot_StartCnt=0;
				break;
			case 0xfe:
				if(((CImageTesterDlg *)m_pMomWnd)->b_Chk_CurrentChkEn == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->Lot_StartCnt_C=0;
				}
				if (((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bCheckSumCheck == TRUE
					|| ((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bReadCheck == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->Lot_StartCnt_Eeprom = 0;

				}
				break;
			default:
				break;
		}
	}
}
void CEtcModelDlg::Lot_InsertDataList()
{
	if((m_ID != 0)&&(IN_pWnd != NULL)){
		switch(m_ID){
			case 0x01:
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCNTSCOptWnd->LOT_InsertDataList();
				break;
			case 0x02:
				break;
			case 0x03://중심점
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->LOT_InsertDataList();
				break;
			case 0x04://로테이트
				((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->LOT_InsertDataList();
				break;
			case 0x05://해상도
				((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->LOT_InsertDataList();
				break;
			case 0x15:
				((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->LOT_InsertDataList();
				break;
			case 0x06://화각
				((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->LOT_InsertDataList();
				break;
			case 0x07://반전
				((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->LOT_InsertDataList();
				break;
			case 0x08://저조도
				((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->LOT_InsertDataList();
				break;
			case 0x09://IR
				((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->LOT_InsertDataList();
				break;
			case 0x0a://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->LOT_InsertDataList();
				break;
			case 0x10://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->LOT_InsertDataList();
				break;
			case 0x11://overlay position
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlayPositionOptWnd->LOT_InsertDataList();
				break;
			case 0x12://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->LOT_InsertDataList();
				break;
			case 0x13://DynamicRange
				((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->LOT_InsertDataList();
				break;
			case 0x16://3D
				((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->LOT_InsertDataList();
				break;
			case 0x17://HotPixel
				((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->LOT_InsertDataList();
				break;
			case 0x18://
				((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->LOT_InsertDataList();
				break;

			case 0x19://온도 센서
				((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->LOT_InsertDataList();
				break;

			case 0x0b://광량비
				((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->LOT_InsertDataList();
				break;
			case 0x0c://왜곡
				((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->LOT_InsertDataList();
				break;
			case 0x0d://distortion
				((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->LOT_InsertDataList();
				break;
			case 0x0e://컬러
				((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->LOT_InsertDataList();
				break;
			case 0x0f://조향연동
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->LOT_InsertDataList();
				break;
			case 0x1f://경고문구
				((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->LOT_InsertDataList();
				break;
			case 0x1c://버전검사
				((CImageTesterDlg *)m_pMomWnd)->m_pVEROptWnd->LOT_InsertDataList();
				break;
			case 0x1b://시스템진단
				((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->LOT_InsertDataList();
				break;
			case 0x1a://dtc 진단
				((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->LOT_InsertDataList();
				break;
			case 0xfe:
				if(((CImageTesterDlg *)m_pMomWnd)->b_Chk_CurrentChkEn == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->LOT_InsertDataList_C();
				}
				if (((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bCheckSumCheck == TRUE
					|| ((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bReadCheck == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->LOT_InsertDataList_Eeprom();

				}
				break;
			default:
				break;
		}
	}
}
void CEtcModelDlg::LOT_Excel_Save()
{
	if((m_ID != 0)&&(IN_pWnd != NULL)){
		switch(m_ID){
			case 0x01:
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCNTSCOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x02:
				break;
			case 0x03://보임량
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x04://로테이트
				((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x05://해상도
				((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x15:
				((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x06://화각
				((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x07://반전
				((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x08://저조도
				((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x09://IR
				((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x0a://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x10://이물_LG
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->LOT_EXCEL_SAVE();
				break;
			case 0x11://오버레이 포지션
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlayPositionOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x12://저조도(백점검사)
				((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x13://DynamicRange
				((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x16://3D
				((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x17://HotPixel
				((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x18://
				((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->LOT_EXCEL_SAVE();
				break;

			case 0x19://온도 센서
				((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->LOT_EXCEL_SAVE();
				break;


			case 0x0b://광량비
				((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x0c://왜곡
				((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x0d://Distortion
				((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x0e://컬러
				((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x0f://조향연동
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x1a://dtc 진단
				((CImageTesterDlg *)m_pMomWnd)->m_pDTCOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x1b://시스템 진단
				((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x1c://버전검사
				((CImageTesterDlg *)m_pMomWnd)->m_pVEROptWnd->LOT_EXCEL_SAVE();
				break;
			case 0x1f://경고문구
				((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->LOT_EXCEL_SAVE();
				break;
			case 0xfe://전류
				if(((CImageTesterDlg *)m_pMomWnd)->b_Chk_CurrentChkEn == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_SAVE_C();
				}
				if (((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bCheckSumCheck == TRUE
					|| ((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bReadCheck == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_SAVE_Eeprom();

				}
				break;
			default:
				break;
		}
	}
}

bool CEtcModelDlg::LOT_Excel_Upload()
{
	bool ret = TRUE;
	if((m_ID != 0)&&(IN_pWnd != NULL)){
		switch(m_ID){
			case 0x01:
				ret =((CImageTesterDlg *)m_pMomWnd)->m_pSubCNTSCOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x02:
				break;
			case 0x03://보임량
				ret =((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x04://로테이트
				ret =((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x05://해상력
				ret =((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x15:
				((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x06://화각
				ret = ((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x07://반전
				((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x08://저조도
				ret =((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x09://IR
				ret =((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x0a://이물
				ret =((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x10://이물
				ret =((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x11://오버레이 포지션
				ret =((CImageTesterDlg *)m_pMomWnd)->m_pOverlayPositionOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x12://이물
		//		ret =((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x13://DynamicRange
				((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x16://DynamicRange
				((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x17://HotPixle
				((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x18://
				((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->LOT_EXCEL_UPLOAD();
				break;

			case 0x19://온도 센서
				((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->LOT_EXCEL_UPLOAD();
				break;

			case 0x0b://광량비
				ret =((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x0c://왜곡
				ret = ((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x0d://Distortion
				ret =((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x0e://컬러
				((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x0f://조향연동
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x1a://dtc 진단
				((CImageTesterDlg *)m_pMomWnd)->m_pDTCOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x1b://시스템 진단
				((CImageTesterDlg *)m_pMomWnd)->m_pSYSTEMOptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x1c://버전검사
				((CImageTesterDlg *)m_pMomWnd)->m_pVEROptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0x1f://경고문구
				((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->LOT_EXCEL_UPLOAD();
				break;
			case 0xfe://전류
				if(((CImageTesterDlg *)m_pMomWnd)->b_Chk_CurrentChkEn == TRUE){
					ret =((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD_C();
				}
				if (((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bCheckSumCheck == TRUE
					|| ((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bReadCheck == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD_Eeprom();

				}
				break;
			default:
				break;
		}
	}
	return ret;
}

void CEtcModelDlg::DeleteITEM()/////////////////////////////////////////////0723
{
	bool ret = TRUE;
	if((m_ID != 0)&&(IN_pWnd != NULL)){
		switch(m_ID){
			case 0x01:
				break;
			case 0x02:
				break;
			case 0x03://보임량
				((CImageTesterDlg *)m_pMomWnd)->m_pSubCPointOptWnd->m_Lot_CenterList.DeleteAllItems();
				break;
			case 0x04://로테이트
//				((CImageTesterDlg *)m_pMomWnd)->m_pRotateOptWnd->m_Lot_RotateList.DeleteAllItems();
				break;
			case 0x05://해상력
				((CImageTesterDlg *)m_pMomWnd)->m_pResolutionOptWnd->m_Lot_ResolutionList.DeleteAllItems();
				break;
			case 0x15:
				((CImageTesterDlg *)m_pMomWnd)->m_pSFROptWnd->m_Lot_SFRList.DeleteAllItems();
				break;
			case 0x06://화각
				((CImageTesterDlg *)m_pMomWnd)->m_pAngleOptWnd->m_Lot_AngleList.DeleteAllItems();
				break;
			case 0x07://반전
				((CImageTesterDlg *)m_pMomWnd)->m_pColorRvsOptWnd->m_Lot_ColorReversalList.DeleteAllItems();
				break;
			case 0x08://저조도
				((CImageTesterDlg *)m_pMomWnd)->m_pPatternNoiseOptWnd->m_Lot_PatternNoiseList.DeleteAllItems();
				break;
			case 0x09://IR
				((CImageTesterDlg *)m_pMomWnd)->m_pIRFilterOptWnd->m_Lot_IRlist.DeleteAllItems();
				break;
			case 0x0a://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptWnd->m_Lot_ParticleList.DeleteAllItems();
				break;
			case 0x10://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pParticleOptLGWnd->m_Lot_ParticleList.DeleteAllItems();
				break;
			case 0x12://이물
				((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->m_Lot_ParticleList.DeleteAllItems();
				break;
			case 0x13://DynamicRange
				((CImageTesterDlg *)m_pMomWnd)->m_pDynamicRangeOptWnd->m_Lot_DynamicRangeList.DeleteAllItems();
				break;
			case 0x16://DynamicRange
				((CImageTesterDlg *)m_pMomWnd)->m_p3D_DepthOptWnd->m_Lot_WorkList.DeleteAllItems();
				break;
			case 0x17://HotPixel
				((CImageTesterDlg *)m_pMomWnd)->m_pHotPixel_OptWnd->m_Lot_WorkList.DeleteAllItems();
				break;
			case 0x18://
				((CImageTesterDlg *)m_pMomWnd)->m_pDepthNoise_OptWnd->m_Lot_WorkList.DeleteAllItems();
				break;

			case 0x19://온도 센서
				((CImageTesterDlg *)m_pMomWnd)->m_pTemperature_OptWnd->m_Lot_WorkList.DeleteAllItems();
				break;

			case 0x0b://광량비
				((CImageTesterDlg *)m_pMomWnd)->m_pBrightnessOptWnd->m_Lot_BrightnessList.DeleteAllItems();
				break;
			case 0x0c://왜곡
				((CImageTesterDlg *)m_pMomWnd)->m_pFixedPatternOptWnd->m_Lot_FixedPatternList.DeleteAllItems();
				break;
			case 0x0d://Distortion
				((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->m_Lot_DistortionList.DeleteAllItems();
				break;
			case 0x0e://컬러
				((CImageTesterDlg *)m_pMomWnd)->m_pColorOptWnd->m_Lot_ColorList.DeleteAllItems();
				break;
			case 0x0f:
				((CImageTesterDlg *)m_pMomWnd)->m_pOverlay_CAN_OptWnd->m_OverlayWorkList.DeleteAllItems();
				break;
			case 0x1f:
				((CImageTesterDlg *)m_pMomWnd)->m_pWarning_CAN_OptWnd->m_WarningWorkList.DeleteAllItems();
				break;
			case 0xfe://전류
				if(((CImageTesterDlg *)m_pMomWnd)->b_Chk_CurrentChkEn == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->m_Lot_CurrentList.DeleteAllItems();
				}
				if (((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bCheckSumCheck == TRUE
					|| ((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bReadCheck == TRUE){
					((CImageTesterDlg *)m_pMomWnd)->m_Lot_EepromList.DeleteAllItems();

				}
				break;
			default:
				break;
		}
	}
}

#pragma endregion
void CEtcModelDlg::OnEnSetfocusStringState()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CEtcModelDlg::VideoModeCheck()
{
	bool ret = TRUE;

	int nVideoMode = -1;

	if ((m_ID != 0) && (IN_pWnd != NULL)){
		switch (m_ID){
		case 0x16://3D
			nVideoMode = Crop_EvenFrame;
			break;
		default:
			nVideoMode = Crop_OddFrame;
			break;
		}
	}

	if (((CImageTesterDlg  *)m_pMomWnd)->stDaqOpt.nCropFrame != nVideoMode)
	{

		((CImageTesterDlg  *)m_pMomWnd)->m_LVDSVideo.SetVideoChangeMode(0,TRUE);
		DoEvents(300);
		((CImageTesterDlg  *)m_pMomWnd)->stDaqOpt.nCropFrame = (enCropFrame)nVideoMode;
		((CImageTesterDlg  *)m_pMomWnd)->m_LVDSVideo.SetDAQOption_All(((CImageTesterDlg  *)m_pMomWnd)->stDaqOpt);
		((CImageTesterDlg  *)m_pMomWnd)->m_LVDSVideo.SetVideoChangeMode(0, FALSE);
		DoEvents(100);

	}

}


bool CEtcModelDlg::PowerModeCheck()
{


	bool ret = TRUE;

	int nPowerRegisterMode = -1;

	if ((m_ID != 0) && (IN_pWnd != NULL)){
		switch (m_ID){
		case 0x01:
			nPowerRegisterMode = VR_PO_REGISTER;
			break;
		case 0x02:
			break;
		case 0x03://보임량
			nPowerRegisterMode = VR_PO_REGISTER;
			break;
		case 0x04://로테이트
			nPowerRegisterMode = VR_PO_REGISTER;
			break;
		case 0x05://해상력
			nPowerRegisterMode = VR_PO_REGISTER;
			break;
		case 0x15:
			nPowerRegisterMode = VR_PO_REGISTER;
			break;
		case 0x06://화각
			nPowerRegisterMode = VR_PO_REGISTER;
			break;
		case 0x07://반전
			nPowerRegisterMode = VR_PO_REGISTER;
			break;
		case 0x08://저조도
			nPowerRegisterMode = VR_PO_REGISTER_2;
			break;
		case 0x09://IR
			nPowerRegisterMode = VR_PO_REGISTER_2;
			break;
		case 0x0a://이물
			nPowerRegisterMode = VR_PO_REGISTER_2;
			break;
		case 0x10://이물
			nPowerRegisterMode = VR_PO_REGISTER_2;
			break;
		case 0x17://이물
			nPowerRegisterMode = VR_PO_REGISTER_2;
			break;
		case 0x18://이물
			nPowerRegisterMode = VR_PO_REGISTER_2;
			break;

		case 0x19://온도 센서
			nPowerRegisterMode = VR_PO_REGISTER_2;
			break;

		case 0x11://오버레이 포지션
			nPowerRegisterMode = VR_PO_REGISTER_2;
			break;
		case 0x12://이물
			//		ret =((CImageTesterDlg *)m_pMomWnd)->m_pLowLightOptWnd->LOT_EXCEL_UPLOAD();
			nPowerRegisterMode = VR_PO_REGISTER_2;
			break;
		case 0x13://DynamicRange
			nPowerRegisterMode = VR_PO_REGISTER_2;
			break;
		case 0x16://3D
			nPowerRegisterMode = VR_PO_REGISTER;
			break;
		case 0x0b://광량비
			nPowerRegisterMode = VR_PO_REGISTER_2;
			break;
		case 0x0c://왜곡
			nPowerRegisterMode = VR_PO_REGISTER_2;
			break;
		case 0x0d://Distortion
			nPowerRegisterMode = VR_PO_REGISTER;
			break;
		case 0x0e://컬러
			nPowerRegisterMode = VR_PO_REGISTER;
			break;
		case 0x0f://조향연동
			nPowerRegisterMode = VR_PO_REGISTER_2;
			break;
		default:
			break;
		}
	}

	if (nPowerRegisterMode != -1)
	{
		ret = ((CImageTesterDlg *)m_pMomWnd)->Power_On(nPowerRegisterMode);
		//DoEvents(500);//전원을 넣었다면 안정화되기까지 대기한다.
	}

	return ret;
}
