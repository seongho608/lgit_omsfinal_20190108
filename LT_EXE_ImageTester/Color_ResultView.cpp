// ColorReversal_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Color_ResultView.h"


// CColor_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CColor_ResultView, CDialog)

CColor_ResultView::CColor_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CColor_ResultView::IDD, pParent)
{

}

CColor_ResultView::~CColor_ResultView()
{
}

void CColor_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CColor_ResultView, CDialog)
	ON_WM_CTLCOLOR()
	ON_EN_CHANGE(IDC_STATIC_MASTER, &CColor_ResultView::OnEnChangeStaticMaster)
	ON_EN_SETFOCUS(IDC_VALUE_REVERSAL, &CColor_ResultView::OnEnSetfocusValueReversal)
	ON_EN_SETFOCUS(IDC_STATIC_LEFT_T, &CColor_ResultView::OnEnSetfocusStaticLeftT)
	ON_EN_SETFOCUS(IDC_VALUE_LEFT_T, &CColor_ResultView::OnEnSetfocusValueLeftT)
	ON_EN_SETFOCUS(IDC_STATIC_RIGHT_T, &CColor_ResultView::OnEnSetfocusStaticRightT)
	ON_EN_SETFOCUS(IDC_VALUE_RIGHT_T, &CColor_ResultView::OnEnSetfocusValueRightT)
	ON_EN_SETFOCUS(IDC_STATIC_LEFT_B, &CColor_ResultView::OnEnSetfocusStaticLeftB)
	ON_EN_SETFOCUS(IDC_VALUE_LEFT_B, &CColor_ResultView::OnEnSetfocusValueLeftB)
	ON_EN_SETFOCUS(IDC_STATIC_RIGHT_B, &CColor_ResultView::OnEnSetfocusStaticRightB)
	ON_EN_SETFOCUS(IDC_VALUE_RIGHT_B, &CColor_ResultView::OnEnSetfocusValueRightB)
	ON_EN_SETFOCUS(IDC_VALUE_LEFT_T2, &CColor_ResultView::OnEnSetfocusValueLeftT2)
	ON_EN_SETFOCUS(IDC_VALUE_RIGHT_T2, &CColor_ResultView::OnEnSetfocusValueRightT2)
	ON_EN_SETFOCUS(IDC_VALUE_LEFT_B2, &CColor_ResultView::OnEnSetfocusValueLeftB2)
	ON_EN_SETFOCUS(IDC_VALUE_RIGHT_B2, &CColor_ResultView::OnEnSetfocusValueRightB2)
END_MESSAGE_MAP()


// CColor_ResultView 메시지 처리기입니다.
void CColor_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CColor_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
BOOL CColor_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();
	Font_Size_Change(IDC_STATIC_MASTER,&ft,100,20);
	GetDlgItem(IDC_VALUE_MASTER)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_TEST)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_TEST)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_LEFT_T)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_LEFT_T)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_RIGHT_T)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_RIGHT_T)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_LEFT_B)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_LEFT_B)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_RIGHT_B)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_RIGHT_B)->SetFont(&ft);

	GetDlgItem(IDC_VALUE_LEFT_T2)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_RIGHT_T2)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_LEFT_B2)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_RIGHT_B2)->SetFont(&ft);

	Font_Size_Change(IDC_VALUE_REVERSAL,&ft2,100,38);

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	initstat();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CColor_ResultView::initstat()
{
	MASTER_TEXT("MASTER");
	TEST_TEXT("TEST");
	LT_TEXT("L-T");
	RT_TEXT("R-T");
	LB_TEXT("L-B");
	RB_TEXT("R-B");

	MASTER_VALUE("");
	TEST_VALUE("");
	LT_VALUE("");
	RT_VALUE("");
	LB_VALUE("");
	RB_VALUE("");
	LT_VALUE2("");
	RT_VALUE2("");
	LB_VALUE2("");
	RB_VALUE2("");
	RV_VALUE(0,"STAND BY");
}

HBRUSH CColor_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_MASTER){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_TEST){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_LEFT_T){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_RIGHT_T){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_LEFT_B){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_RIGHT_B){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_MASTER){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_TEST){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_LEFT_T){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_RIGHT_T){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_LEFT_B){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_RIGHT_B){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}

	if(pWnd->GetDlgCtrlID() == IDC_VALUE_LEFT_T2){
		pDC->SetTextColor(RGB(86,86,86));
		// 텍스트의 배경색상을 설정한다.
		pDC->SetBkColor(RGB(255, 255, 255));
		// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_RIGHT_T2){
		pDC->SetTextColor(RGB(86,86,86));
		// 텍스트의 배경색상을 설정한다.
		pDC->SetBkColor(RGB(255, 255, 255));
		// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_LEFT_B2){
		pDC->SetTextColor(RGB(86,86,86));
		// 텍스트의 배경색상을 설정한다.
		pDC->SetBkColor(RGB(255, 255, 255));
		// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_RIGHT_B2){
		pDC->SetTextColor(RGB(86,86,86));
		// 텍스트의 배경색상을 설정한다.
		pDC->SetBkColor(RGB(255, 255, 255));
		// 바탕색상을 설정한다.
	}

	if(pWnd->GetDlgCtrlID() == IDC_VALUE_REVERSAL){
			pDC->SetTextColor(txcol_TVal);
			pDC->SetBkColor(bkcol_TVal);
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CColor_ResultView::MASTER_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_MASTER))->SetWindowText(lpcszString);
}

void CColor_ResultView::TEST_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_TEST))->SetWindowText(lpcszString);
}

void CColor_ResultView::LT_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_LEFT_T))->SetWindowText(lpcszString);
}

void CColor_ResultView::RT_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_RIGHT_T))->SetWindowText(lpcszString);
}

void CColor_ResultView::LB_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_LEFT_B))->SetWindowText(lpcszString);
}

void CColor_ResultView::RB_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_RIGHT_B))->SetWindowText(lpcszString);
}
///
void CColor_ResultView::MASTER_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_MASTER))->SetWindowText(lpcszString);
}

void CColor_ResultView::TEST_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_TEST))->SetWindowText(lpcszString);
}

void CColor_ResultView::LT_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_LEFT_T))->SetWindowText(lpcszString);
}

void CColor_ResultView::RT_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_RIGHT_T))->SetWindowText(lpcszString);
}

void CColor_ResultView::LB_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_LEFT_B))->SetWindowText(lpcszString);
}

void CColor_ResultView::RB_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_RIGHT_B))->SetWindowText(lpcszString);
}


void CColor_ResultView::LT_VALUE2(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_LEFT_T2))->SetWindowText(lpcszString);
}

void CColor_ResultView::RT_VALUE2(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_RIGHT_T2))->SetWindowText(lpcszString);
}

void CColor_ResultView::LB_VALUE2(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_LEFT_B2))->SetWindowText(lpcszString);
}

void CColor_ResultView::RB_VALUE2(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_RIGHT_B2))->SetWindowText(lpcszString);
}


void CColor_ResultView::RV_VALUE(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_VALUE_REVERSAL))->SetWindowText(lpcszString);
}
BOOL CColor_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CColor_ResultView::OnEnChangeStaticMaster()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CColor_ResultView::OnEnSetfocusValueReversal()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColor_ResultView::OnEnSetfocusStaticLeftT()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColor_ResultView::OnEnSetfocusValueLeftT()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColor_ResultView::OnEnSetfocusStaticRightT()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColor_ResultView::OnEnSetfocusValueRightT()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColor_ResultView::OnEnSetfocusStaticLeftB()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColor_ResultView::OnEnSetfocusValueLeftB()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColor_ResultView::OnEnSetfocusStaticRightB()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColor_ResultView::OnEnSetfocusValueRightB()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColor_ResultView::OnEnSetfocusValueLeftT2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColor_ResultView::OnEnSetfocusValueRightT2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColor_ResultView::OnEnSetfocusValueLeftB2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}

void CColor_ResultView::OnEnSetfocusValueRightB2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}
