#pragma once
// CModOptDlg 대화 상자입니다.
#include "resource.h"

class CModOptDlg : public CDialog
{
	DECLARE_DYNAMIC(CModOptDlg)

public:
	CModOptDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CModOptDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MODELOPT_DIALOG };

	void	Setup(CWnd* IN_pMomWnd);

	CComboBox	*pModelCombo;
	CComboBox	*pPogoCombo;

	CTabCtrl	m_Tab_Setting;
	CTabCtrl	m_Tab_Manager;
	void		OnShowSel(int Num);
	void		SetPogoSet();
	void		InitEVMS();
private:
	CWnd		*m_pMomWnd;

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangeSubModel();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnTcnSelchangeTabSetting(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangeSubPogo();
};
