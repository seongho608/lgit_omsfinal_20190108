#pragma once
#include "resource.h"

// CPogo_Option 대화 상자입니다.

class CPogo_Option : public CDialog
{
	DECLARE_DYNAMIC(CPogo_Option)

public:
	CPogo_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPogo_Option();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_STAND_OPT_POGO_DIALOG };
	void	Setup(CWnd* IN_pMomWnd);
	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	CFont	ft1;

	CComboBox *pModelCombo;
	CString pogoName;
	int		m_combo_sel;

	int		i_PogoPinSET;
	int		i_PogoCnt;
	
	void	InitUI();
	void	CNT_VIEW(LPCSTR lpcszString, ...);
	void	POGONAME_VIEW(LPCSTR lpcszString, ...);
	void	MainGroupSel();
	void	SetPogoSet();
	void	SetCountUpdate();
	void	ModelFileSearch(CString compdata);
	
	void	Enable_Show(bool MODE);
	void	InitEVMS();
private:
	CWnd		*m_pMomWnd;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CString str_PogoGroupNewName;
	afx_msg void OnEnChangeEditPogogroupname();
	afx_msg void OnBnClickedButtonGroupsave();
	afx_msg void OnCbnSelchangeComboGroup();
	afx_msg void OnBnClickedButtonGroupdel();
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedButtonCntsave();
	CString str_PogoCount;
	afx_msg void OnEnChangeEditPogogroupname2();
	afx_msg void OnBnClickedBtnPogoup();
	afx_msg void OnBnClickedButtonCntinit();
	afx_msg void OnBnClickedBtnPogodown();
	afx_msg void OnEnSetfocusEditGroupview();
};
