// Option_SubStand.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Option_SubStand.h"
ST_EEPROM g_stEEPROM;

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];

// COption_SubStand 대화 상자입니다.

IMPLEMENT_DYNAMIC(COption_SubStand, CDialog)

COption_SubStand::COption_SubStand(CWnd* pParent /*=NULL*/)
	: CDialog(COption_SubStand::IDD, pParent)
	, m_VOLT_Change(_T(""))
	, m_VOLT_VALUE(_T(""))
	, str_MinAmp(_T(""))
	, str_MaxAmp(_T(""))
	, b_Chk_Volton(FALSE)
	, b_Chk_CurrentChkEn(FALSE)
	, str_LightonDelay(_T(""))
	, str_RayonDelay(_T(""))
	, str_LightoffDelay(_T(""))
	, str_VoltageDelay(_T(""))
	, str_AlarmDelay(_T(""))
	, str_Buzzerdelay(_T(""))
	, b_SAEStat(FALSE)
	, b_LAEStat(FALSE)
	, str_sshutter(_T(""))
	, str_ssensor(_T(""))
	, str_sISP(_T(""))
	, str_sRED(_T(""))
	, str_sBLUE(_T(""))
	, str_lshutter(_T(""))
	, str_lsensor(_T(""))
	, str_lISP(_T(""))
	, str_lRED(_T(""))
	, str_lBLUE(_T(""))
	, b_Chk_TestSkip(FALSE)
	, FILE_PATH(_T(""))
	, str_MinAmp2(_T(""))
	, str_MinAmp3(_T(""))
	, str_MinAmp4(_T(""))
	, str_MinAmp5(_T(""))
	, str_MaxAmp2(_T(""))
	, str_MaxAmp3(_T(""))
	, str_MaxAmp4(_T(""))
	, str_MaxAmp5(_T(""))
	, str_Offset(_T(""))
	, str_Offset2(_T(""))
	, str_Offset3(_T(""))
	, str_Offset4(_T(""))
	, str_Offset5(_T(""))
	, str_Align(_T(""))
	, b_Chk_Align(FALSE)
	, FILE_PATH_SECOND(_T(""))
	, B_Check_RegisterChange(FALSE)
	, B_Check_Read(FALSE)
	, B_Check_Checksum(FALSE)
	, B_CHECK_ZERO(FALSE)
	, b_Chk_AreaSensor(FALSE)
{

}

COption_SubStand::~COption_SubStand()
{
}

void COption_SubStand::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_INPUT_VVALUE, m_VOLT_Change);
	DDX_Text(pDX, IDC_DATA_VVALUE, m_VOLT_VALUE);
	DDX_Text(pDX, IDC_EDIT_MinAmp, str_MinAmp);
	DDX_Text(pDX, IDC_EDIT_MaxAmp, str_MaxAmp);
	DDX_Check(pDX, IDC_CHECK_VOLTAGEON, b_Chk_Volton);
	DDX_Check(pDX, IDC_CHECK_CHKCURRENT, b_Chk_CurrentChkEn);

	DDX_Text(pDX, IDC_EDIT_LIGHTON, str_LightonDelay);
	DDX_Text(pDX, IDC_EDIT_LIGHTON2, str_RayonDelay);
	DDX_Text(pDX, IDC_EDIT_LIGHTON3, str_LightoffDelay);
	DDX_Text(pDX, IDC_EDIT_VOLTAGEDELAY, str_VoltageDelay);
	DDX_Text(pDX, IDC_EDIT_ALARM_, str_AlarmDelay);
	DDX_Text(pDX, IDC_EDIT_ALARM_2, str_Buzzerdelay);
	DDX_Check(pDX, IDC_CHECK_SAEOFF, b_SAEStat);
	DDX_Check(pDX, IDC_CHECK_LAEOFF, b_LAEStat);
	DDX_Text(pDX, IDC_EDIT_SSHUTTER, str_sshutter);
	DDX_Text(pDX, IDC_EDIT_SSENSOR, str_ssensor);
	DDX_Text(pDX, IDC_EDIT_SISP, str_sISP);
	DDX_Text(pDX, IDC_EDIT_SRED, str_sRED);
	DDX_Text(pDX, IDC_EDIT_SBLUE, str_sBLUE);
	DDX_Text(pDX, IDC_EDIT_LSHUTTER, str_lshutter);
	DDX_Text(pDX, IDC_EDIT_LSENSOR, str_lsensor);
	DDX_Text(pDX, IDC_EDIT_LISP, str_lISP);
	DDX_Text(pDX, IDC_EDIT_LRED, str_lRED);
	DDX_Text(pDX, IDC_EDIT_LBLUE, str_lBLUE);
	DDX_Check(pDX, IDC_CHECK_TESTSKIP, b_Chk_TestSkip);
	DDX_Text(pDX, IDC_EDIT_REGISTER, FILE_PATH);
	DDX_Text(pDX, IDC_EDIT_MinAmp2, str_MinAmp2);
	DDX_Text(pDX, IDC_EDIT_MinAmp3, str_MinAmp3);
	DDX_Text(pDX, IDC_EDIT_MinAmp4, str_MinAmp4);
	DDX_Text(pDX, IDC_EDIT_MinAmp5, str_MinAmp5);
	DDX_Text(pDX, IDC_EDIT_MaxAmp2, str_MaxAmp2);
	DDX_Text(pDX, IDC_EDIT_MaxAmp3, str_MaxAmp3);
	DDX_Text(pDX, IDC_EDIT_MaxAmp4, str_MaxAmp4);
	DDX_Text(pDX, IDC_EDIT_MaxAmp5, str_MaxAmp5);
	DDX_Text(pDX, IDC_EDIT_OFFSET, str_Offset);
	DDX_Text(pDX, IDC_EDIT_OFFSET2, str_Offset2);
	DDX_Text(pDX, IDC_EDIT_OFFSET3, str_Offset3);
	DDX_Text(pDX, IDC_EDIT_OFFSET4, str_Offset4);
	DDX_Text(pDX, IDC_EDIT_OFFSET5, str_Offset5);
	DDX_Text(pDX, IDC_EDIT_ALIGN, str_Align);
	DDX_Check(pDX, IDC_CHECK_ALIGN, b_Chk_Align);
	DDX_Text(pDX, IDC_EDIT_REGISTER2, FILE_PATH_SECOND);
	DDX_Check(pDX, IDC_CHECK_REGISTERCHANGE, B_Check_RegisterChange);
	DDX_Check(pDX, IDC_CHECK_READ, B_Check_Read);
	DDX_Check(pDX, IDC_CHECK_CHECKSUM, B_Check_Checksum);
	DDX_Check(pDX, IDC_CHECK_ZERO, B_CHECK_ZERO);
	DDX_Check(pDX, IDC_CHECK_AREA_SENSOR, b_Chk_AreaSensor);
}


BEGIN_MESSAGE_MAP(COption_SubStand, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_AmpSave, &COption_SubStand::OnBnClickedButtonAmpsave)
//	ON_BN_CLICKED(IDC_BUTTON_CONTRLSAVE, &COption_SubStand::OnBnClickedButtonContrlsave)
ON_EN_CHANGE(IDC_INPUT_VVALUE, &COption_SubStand::OnEnChangeInputVvalue)
ON_BN_CLICKED(IDC_BTN_VOLTSAVE, &COption_SubStand::OnBnClickedBtnVoltsave)
ON_EN_CHANGE(IDC_EDIT_MinAmp, &COption_SubStand::OnEnChangeEditMinamp)
ON_EN_CHANGE(IDC_EDIT_MaxAmp, &COption_SubStand::OnEnChangeEditMaxamp)
ON_BN_CLICKED(IDC_CHECK_VOLTAGEON, &COption_SubStand::OnBnClickedCheckVoltageon)
ON_BN_CLICKED(IDC_CHECK_CHKCURRENT, &COption_SubStand::OnBnClickedCheckChkcurrent)
ON_EN_CHANGE(IDC_EDIT_CHART_DISTANCE, &COption_SubStand::OnEnChangeEditChartDistance)
ON_EN_CHANGE(IDC_EDIT_MODULE_XDISTANCE, &COption_SubStand::OnEnChangeEditModuleXdistance)
ON_EN_CHANGE(IDC_EDIT_MODULE_YDISTANCE, &COption_SubStand::OnEnChangeEditModuleYdistance)
ON_EN_CHANGE(IDC_EDIT_MODULE_SHUTTER, &COption_SubStand::OnEnChangeEditModuleSdistance)
ON_BN_CLICKED(IDC_BTN_CHART_MOVE_SAVE, &COption_SubStand::OnBnClickedBtnChartMoveSave)
ON_BN_CLICKED(IDC_BTN_MODULE_X_SAVE, &COption_SubStand::OnBnClickedBtnModuleXSave)
ON_BN_CLICKED(IDC_BTN_MODULE_Y_SAVE, &COption_SubStand::OnBnClickedBtnModuleYSave)
ON_BN_CLICKED(IDC_BTN_CHART_TESTMOVE, &COption_SubStand::OnBnClickedBtnChartTestmove)
ON_BN_CLICKED(IDC_BTN_MODULEX_TESTMOVE, &COption_SubStand::OnBnClickedBtnModulexTestmove)
ON_BN_CLICKED(IDC_BTN_MODULEY_TESTMOVE, &COption_SubStand::OnBnClickedBtnModuleyTestmove)
ON_BN_CLICKED(IDC_BTN_LIGHT_ALL_OFF, &COption_SubStand::OnBnClickedBtnLightAllOff)
ON_BN_CLICKED(IDC_BTN_LIGHT_RAY_ON, &COption_SubStand::OnBnClickedBtnLightRayOn)
ON_BN_CLICKED(IDC_BTN_LIGHT_LI_ON, &COption_SubStand::OnBnClickedBtnLightLiOn)
ON_WM_SHOWWINDOW()
ON_WM_CTLCOLOR()
ON_BN_CLICKED(IDC_BUTTON_ORIGIN, &COption_SubStand::OnBnClickedButtonOrigin)
ON_BN_CLICKED(IDC_BUTTON_ORIGIN_STOP, &COption_SubStand::OnBnClickedButtonOriginStop)
ON_BN_CLICKED(IDC_BUTTON_CYL_IN, &COption_SubStand::OnBnClickedButtonCylIn)
ON_BN_CLICKED(IDC_BUTTON_CYL_OUT, &COption_SubStand::OnBnClickedButtonCylOut)
ON_EN_SETFOCUS(IDC_DATA_VVALUE, &COption_SubStand::OnEnSetfocusDataVvalue)
ON_BN_CLICKED(IDC_BUTTON_TEST_POS, &COption_SubStand::OnBnClickedButtonTestPos)
ON_BN_CLICKED(IDC_BUTTON_STANDBY, &COption_SubStand::OnBnClickedButtonStandby)
ON_EN_CHANGE(IDC_EDIT_LIGHTON, &COption_SubStand::OnEnChangeEditLighton)
ON_EN_CHANGE(IDC_EDIT_LIGHTON2, &COption_SubStand::OnEnChangeEditLighton2)
ON_EN_CHANGE(IDC_EDIT_LIGHTON3, &COption_SubStand::OnEnChangeEditLighton3)
ON_BN_CLICKED(IDC_BUTTON_LIGHTONDELAYSAVE, &COption_SubStand::OnBnClickedButtonLightondelaysave)
ON_BN_CLICKED(IDC_BUTTON_LIGHTONDELAYSAVE2, &COption_SubStand::OnBnClickedButtonLightondelaysave2)
ON_BN_CLICKED(IDC_BUTTON_LIGHTONDELAYSAVE3, &COption_SubStand::OnBnClickedButtonLightondelaysave3)
ON_EN_CHANGE(IDC_EDIT_VOLTAGEDELAY, &COption_SubStand::OnEnChangeEditVoltagedelay)
ON_BN_CLICKED(IDC_BUTTON_ALARM_RUN, &COption_SubStand::OnBnClickedButtonAlarmRun)
ON_BN_CLICKED(IDC_BUTTON_BUZZER_PASS, &COption_SubStand::OnBnClickedButtonBuzzerPass)
ON_BN_CLICKED(IDC_BUTTON_BUZZER_FAIL, &COption_SubStand::OnBnClickedButtonBuzzerFail)
ON_BN_CLICKED(IDC_BUTTON_BUZZER_OFF, &COption_SubStand::OnBnClickedButtonBuzzerOff)
ON_BN_CLICKED(IDC_BUTTON_ALARM_PASS, &COption_SubStand::OnBnClickedButtonAlarmPass)
ON_BN_CLICKED(IDC_BUTTON_ALARM_FAIL, &COption_SubStand::OnBnClickedButtonAlarmFail)
ON_BN_CLICKED(IDC_BUTTON_ALARM_OFF, &COption_SubStand::OnBnClickedButtonAlarmOff)
ON_BN_CLICKED(IDC_BUTTON_ALARNSAVE, &COption_SubStand::OnBnClickedButtonAlarnsave)
ON_EN_CHANGE(IDC_EDIT_ALARM_, &COption_SubStand::OnEnChangeEditAlarm)
ON_EN_CHANGE(IDC_EDIT_ALARM_2, &COption_SubStand::OnEnChangeEditAlarm2)
ON_BN_CLICKED(IDC_BUTTON_ALARNSAVE2, &COption_SubStand::OnBnClickedButtonAlarnsave2)
ON_BN_CLICKED(IDC_BTN_VOLTSAVE2, &COption_SubStand::OnBnClickedBtnVoltsave2)
ON_BN_CLICKED(IDC_CHECK_SAEOFF, &COption_SubStand::OnBnClickedCheckSaeoff)
ON_BN_CLICKED(IDC_CHECK_LAEOFF, &COption_SubStand::OnBnClickedCheckLaeoff)
ON_EN_CHANGE(IDC_EDIT_SSHUTTER, &COption_SubStand::OnEnChangeEditSshutter)
ON_EN_CHANGE(IDC_EDIT_SSENSOR, &COption_SubStand::OnEnChangeEditSsensor)
ON_EN_CHANGE(IDC_EDIT_SISP, &COption_SubStand::OnEnChangeEditSisp)
ON_EN_CHANGE(IDC_EDIT_SRED, &COption_SubStand::OnEnChangeEditSred)
ON_EN_CHANGE(IDC_EDIT_SBLUE, &COption_SubStand::OnEnChangeEditSblue)
ON_EN_CHANGE(IDC_EDIT_LSHUTTER, &COption_SubStand::OnEnChangeEditLshutter)
ON_EN_CHANGE(IDC_EDIT_LSENSOR, &COption_SubStand::OnEnChangeEditLsensor)
ON_EN_CHANGE(IDC_EDIT_LISP, &COption_SubStand::OnEnChangeEditLisp)
ON_EN_CHANGE(IDC_EDIT_LRED, &COption_SubStand::OnEnChangeEditLred)
ON_EN_CHANGE(IDC_EDIT_LBLUE, &COption_SubStand::OnEnChangeEditLblue)
ON_EN_SETFOCUS(IDC_EDIT_SSHUTTER, &COption_SubStand::OnEnSetfocusEditSshutter)
ON_EN_SETFOCUS(IDC_EDIT_SSENSOR, &COption_SubStand::OnEnSetfocusEditSsensor)
ON_EN_SETFOCUS(IDC_EDIT_SISP, &COption_SubStand::OnEnSetfocusEditSisp)
ON_EN_SETFOCUS(IDC_EDIT_SRED, &COption_SubStand::OnEnSetfocusEditSred)
ON_EN_SETFOCUS(IDC_EDIT_SBLUE, &COption_SubStand::OnEnSetfocusEditSblue)
ON_EN_SETFOCUS(IDC_EDIT_LSHUTTER, &COption_SubStand::OnEnSetfocusEditLshutter)
ON_EN_SETFOCUS(IDC_EDIT_LSENSOR, &COption_SubStand::OnEnSetfocusEditLsensor)
ON_EN_SETFOCUS(IDC_EDIT_LISP, &COption_SubStand::OnEnSetfocusEditLisp)
ON_EN_SETFOCUS(IDC_EDIT_LRED, &COption_SubStand::OnEnSetfocusEditLred)
ON_EN_SETFOCUS(IDC_EDIT_LBLUE, &COption_SubStand::OnEnSetfocusEditLblue)
ON_BN_CLICKED(IDC_BUTTON_SAETEST, &COption_SubStand::OnBnClickedButtonSaetest)
ON_BN_CLICKED(IDC_BUTTON_LAETEST, &COption_SubStand::OnBnClickedButtonLaetest)
ON_BN_CLICKED(IDC_BUTTON_SAESAVE, &COption_SubStand::OnBnClickedButtonSaesave)
ON_BN_CLICKED(IDC_BUTTON_LAESAVE, &COption_SubStand::OnBnClickedButtonLaesave)
ON_BN_CLICKED(IDC_BUTTON_RESET, &COption_SubStand::OnBnClickedButtonReset)
ON_CBN_SELCHANGE(IDC_COMBO_HUTYPE, &COption_SubStand::OnCbnSelchangeComboHutype)
ON_BN_CLICKED(IDC_BUTTON_HUTYPESAVE, &COption_SubStand::OnBnClickedButtonHutypesave)
ON_BN_CLICKED(IDC_BUTTON_PROPERTY, &COption_SubStand::OnBnClickedButtonProperty)
ON_EN_CHANGE(IDC_EDIT_LIGHTON4, &COption_SubStand::OnEnChangeEditLighton4)
ON_BN_CLICKED(IDC_CHECK_TESTSKIP, &COption_SubStand::OnBnClickedCheckTestskip)
ON_BN_CLICKED(IDC_BUTTON_REGISTEROPEN, &COption_SubStand::OnBnClickedButtonRegisteropen)
ON_BN_CLICKED(IDC_BUTTON_CONTROL, &COption_SubStand::OnBnClickedButtonControl)
ON_EN_CHANGE(IDC_EDIT_OFFSET, &COption_SubStand::OnEnChangeEditOffset)
ON_EN_CHANGE(IDC_EDIT_OFFSET2, &COption_SubStand::OnEnChangeEditOffset2)
ON_EN_CHANGE(IDC_EDIT_OFFSET3, &COption_SubStand::OnEnChangeEditOffset3)
ON_EN_CHANGE(IDC_EDIT_OFFSET4, &COption_SubStand::OnEnChangeEditOffset4)
ON_EN_CHANGE(IDC_EDIT_OFFSET5, &COption_SubStand::OnEnChangeEditOffset5)
ON_BN_CLICKED(IDC_CHECK_ALIGN, &COption_SubStand::OnBnClickedCheckAlign)
ON_BN_CLICKED(IDC_BTN_ALIGN, &COption_SubStand::OnBnClickedBtnAlign)
ON_BN_CLICKED(IDC_BUTTON_REGISTEROPEN2, &COption_SubStand::OnBnClickedButtonRegisteropen2)
ON_BN_CLICKED(IDC_CHECK_REGISTERCHANGE, &COption_SubStand::OnBnClickedCheckRegisterchange)
ON_BN_CLICKED(IDC_BUTTON_READ_SAVE, &COption_SubStand::OnBnClickedButtonReadSave)
ON_BN_CLICKED(IDC_CHECK_READ, &COption_SubStand::OnBnClickedCheckRead)
ON_BN_CLICKED(IDC_CHECK_CHECKSUM, &COption_SubStand::OnBnClickedCheckChecksum)
ON_EN_CHANGE(IDC_EDIT_MinAmp2, &COption_SubStand::OnEnChangeEditMinamp2)
ON_EN_CHANGE(IDC_EDIT_MaxAmp2, &COption_SubStand::OnEnChangeEditMaxamp2)
ON_EN_CHANGE(IDC_EDIT_MinAmp3, &COption_SubStand::OnEnChangeEditMinamp3)
ON_EN_CHANGE(IDC_EDIT_MaxAmp3, &COption_SubStand::OnEnChangeEditMaxamp3)
ON_EN_CHANGE(IDC_EDIT_MinAmp4, &COption_SubStand::OnEnChangeEditMinamp4)
ON_EN_CHANGE(IDC_EDIT_MaxAmp4, &COption_SubStand::OnEnChangeEditMaxamp4)
ON_EN_CHANGE(IDC_EDIT_MinAmp5, &COption_SubStand::OnEnChangeEditMinamp5)
ON_EN_CHANGE(IDC_EDIT_MaxAmp5, &COption_SubStand::OnEnChangeEditMaxamp5)
ON_BN_CLICKED(IDC_CHECK_ZERO, &COption_SubStand::OnBnClickedCheckZero)
ON_BN_CLICKED(IDC_BTN_MODULE_S_SAVE, &COption_SubStand::OnBnClickedBtnModuleSSave)
ON_BN_CLICKED(IDC_BTN_MODULES_TESTMOVE, &COption_SubStand::OnBnClickedBtnModulesTestmove)
ON_BN_CLICKED(IDC_BTN_CHART_MOVE_SAVE2, &COption_SubStand::OnBnClickedBtnChartMoveSave2)
ON_BN_CLICKED(IDC_CHECK_AREA_SENSOR, &COption_SubStand::OnBnClickedCheckAreaSensor)
END_MESSAGE_MAP()


// COption_SubStand 메시지 처리기입니다.
void COption_SubStand::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void COption_SubStand::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT)
{
	pTStat		= pTEDIT;
	str_model.Empty();
	str_model.Format("STANDARD OPTION");
	Setup(IN_pMomWnd);
}
BOOL COption_SubStand::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	
	pHuTypeCombo = (CComboBox *)GetDlgItem(IDC_COMBO_HUTYPE);
	
	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;
	
	str_ModelPath=((CImageTesterDlg  *)m_pMomWnd)->modelfile_Name;

	//Font_Size_Change(IDC_EDIT_OVERLAYNAME,&ft,100,20);

	Load_parameter();
	hModeSetup();
	//((CButton *)GetDlgItem(IDC_BTN_VOLTSAVE))->EnableWindow(0);
	////((CButton *)GetDlgItem(IDC_BUTTON_AmpSave))->EnableWindow(0);
	
	if(b_Chk_Volton == FALSE){
		((CEdit *)GetDlgItem(IDC_INPUT_VVALUE))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MinAmp))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MaxAmp))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_CHKCURRENT))->EnableWindow(0);
		ModeName = "Start";
	}else if(b_Chk_CurrentChkEn == FALSE){
		((CEdit *)GetDlgItem(IDC_EDIT_MinAmp))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MaxAmp))->EnableWindow(0);
		ModeName = "Start(동작전류)";
	}
	
	InitEVMS();
	UpdateData(FALSE);


	CString str = "";
	double buf_data;
	int Offset = ((CImageTesterDlg  *)m_pMomWnd)->m_nRoffset;

	str.Format(_T("%d"), Offset);
	SetDlgItemText(IDC_EDIT_CHART_OFFSET, str);
//	Enable_Overlay(b_OverlayRegionCheck);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void COption_SubStand::hModeSetup()
{
	pHuTypeCombo->ResetContent();//콤보를 초기화한다.
	CString str = "";
	
	for(int lop = 0;lop<256;lop++){
		str.Format("0x%2X",lop);
		pHuTypeCombo->AddString(str);
	}
	pHuTypeCombo->SetCurSel(((CImageTesterDlg  *)m_pMomWnd)->m_HuType);
	((CButton *)GetDlgItem(IDC_BUTTON_HUTYPESAVE))->EnableWindow(0);
}

BOOL COption_SubStand::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		
		if (pMsg->wParam == VK_RETURN){
			/*
			if(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_INPUT_VVALUE))->GetSafeHwnd()) //자신의 핸들을 가져오는 함수
			{	
				if(((CButton *)GetDlgItem(IDC_BTN_VOLTSAVE))->IsWindowEnabled() == TRUE){
					OnBnClickedBtnVoltsave();
				}
			}
			*/
			return TRUE;
		}
		if(pMsg->wParam == VK_ADD){
//			::PostMessage(((CCenterPointModify_4CHDlg *)m_pMomWnd)->hMainWnd, WM_COMM_START, 0, 0 );
			return TRUE;
		}
		if (pMsg->wParam == VK_SUBTRACT){
//			::PostMessage(((CCenterPointModify_4CHDlg *)m_pMomWnd)->hMainWnd, WM_COMM_STOP, 0, 0 );
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void COption_SubStand::UpdateEditRect(int parm_edit_id)
{
 CRect r;
 GetDlgItem(parm_edit_id)->GetWindowRect(r);
 // 에디트 좌표를 대화상자 기준으로 변환한다.
 ScreenToClient(r);
 // 다이얼로그의 에디트 컨트롤 영역을 갱신한다.
 InvalidateRect(r);
}

void COption_SubStand::OnEnChangeInputVvalue()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	double DValue = atof(m_VOLT_Change);
	int IntBuf = (int)(DValue *10);

	if((double)IntBuf != DValue *10){
		DValue = IntBuf/10; 
		m_VOLT_Change.Format("%3.1f",DValue) ;
		UpdateData(FALSE);		
	}
	
	//if(DValue == d_Volt_Val){
	//	((CButton *)GetDlgItem(IDC_BTN_VOLTSAVE))->EnableWindow(0);//허용되지 않는 값이 나오면 저장을 불허한다.
	//}else if((DValue <= 0)||(DValue > 12)){
	//	((CButton *)GetDlgItem(IDC_BTN_VOLTSAVE))->EnableWindow(0);//허용되지 않는 값이 나오면 저장을 불허한다.
	//}else{
	//	((CButton *)GetDlgItem(IDC_BTN_VOLTSAVE))->EnableWindow(1);
	//}

	UpdateEditRect(IDC_INPUT_VVALUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
//double d_Volt_Val;
void COption_SubStand::OnBnClickedBtnVoltsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.




	CString str_buf = m_VOLT_Change;

	if(m_VOLT_Change == ""){
		return;
	}
	WritePrivateProfileString(str_model,"VOLTVALUE",str_buf,str_ModelPath);

		


	//((CButton *)GetDlgItem(IDC_BTN_VOLTSAVE))->EnableWindow(0);
	d_Volt_Val = GetPrivateProfileDouble(str_model,"VOLTVALUE",-1,str_ModelPath);
	((CImageTesterDlg  *)m_pMomWnd)->dVoltSave =d_Volt_Val;

	m_VOLT_Change.Format("");
	m_VOLT_VALUE.Format("%3.1f V",d_Volt_Val);
	((CImageTesterDlg  *)m_pMomWnd)->m_pResStandOptWnd->Voltage_Value(m_VOLT_VALUE);
	((CImageTesterDlg  *)m_pMomWnd)->m_pResWorkerOptWnd->Voltage_Value(m_VOLT_VALUE);
	((CImageTesterDlg  *)m_pMomWnd)->m_pResLotWnd->Voltage_Value(m_VOLT_VALUE);
	UpdateData(FALSE);
}

void COption_SubStand::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CButton *)GetDlgItem(IDC_BTN_VOLTSAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_VOLTSAVE2))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_CHKCURRENT))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_VOLTAGEON))->EnableWindow(0);

		////((CButton *)GetDlgItem(IDC_BUTTON_AmpSave))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_ALARNSAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_ALARNSAVE2))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_LIGHTONDELAYSAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_LIGHTONDELAYSAVE2))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_LIGHTONDELAYSAVE3))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_CHART_MOVE_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_MODULE_X_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_MODULE_Y_SAVE))->EnableWindow(0);

		((CEdit *)GetDlgItem(IDC_INPUT_VVALUE))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_VOLTAGEDELAY))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MinAmp))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MaxAmp))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_ALARM_))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_ALARM_2))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_LIGHTON))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_LIGHTON2))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_LIGHTON3))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_CHART_DISTANCE))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MODULE_XDISTANCE))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MODULE_YDISTANCE))->EnableWindow(0);

		((CButton *)GetDlgItem(IDC_BUTTON_HUTYPESAVE))->EnableWindow(0);
		pHuTypeCombo->EnableWindow(0);
	}
}


void COption_SubStand::Load_parameter()
{  
	CString str="";
//	UpdateData(TRUE);
	b_Chk_TestSkip = GetPrivateProfileInt(str_model, "TEST_SKIP", -1, str_ModelPath);
	if (b_Chk_TestSkip == -1){
		b_Chk_TestSkip = 0;  
		str.Empty();
		str.Format("%d", b_Chk_TestSkip);
		WritePrivateProfileString(str_model, "TEST_SKIP", str, str_ModelPath);
	}

	b_Chk_AreaSensor = GetPrivateProfileInt(str_model, "AREA_SENSOR", -1, str_ModelPath);
	if (b_Chk_AreaSensor == -1){
		b_Chk_AreaSensor =1;
		str.Empty();
		str.Format("%d", b_Chk_AreaSensor);
		WritePrivateProfileString(str_model, "AREA_SENSOR", str, str_ModelPath);
	}



	b_Chk_Volton = GetPrivateProfileInt(str_model,"START_CHKVOLTON",-1,str_ModelPath);
	if(b_Chk_Volton == -1){
		b_Chk_Volton = 1;//초기에 전압 출력은 활성화한다.  
		str.Empty();
		str.Format("%d",b_Chk_Volton);
		WritePrivateProfileString(str_model,"START_CHKVOLTON",str,str_ModelPath);
	}

	b_Chk_CurrentChkEn = GetPrivateProfileInt(str_model,"START_CHKCURRENTEN",-1,str_ModelPath);
	if(b_Chk_CurrentChkEn == -1){
		b_Chk_CurrentChkEn = 1;//초기에 전류 체크를 활성화한다. 
		str.Empty();
		str.Format("%d",b_Chk_CurrentChkEn);
		WritePrivateProfileString(str_model,"START_CHKCURRENTEN",str,str_ModelPath);
	}
	
	b_Chk_Align = GetPrivateProfileInt(str_model, "USE_ALIGN", -1, str_ModelPath);
	if (b_Chk_Align == -1){
		b_Chk_Align = 0;//초기에 전류 체크를 활성화한다. 
		str.Empty();
		str.Format("%d", b_Chk_Align);
		WritePrivateProfileString(str_model, "USE_ALIGN", str, str_ModelPath);
	}

	m_MinAmp = GetPrivateProfileDouble(str_model,"MinAmp",-1.0,str_ModelPath);
	if ((m_MinAmp == -1.0)){
		m_MinAmp = 120.0;
		str.Empty();
		str.Format("%.1f",m_MinAmp);
		WritePrivateProfileString(str_model,"MinAmp",str,str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_MinAmp = m_MinAmp;
	str_MinAmp.Format("%.1f",m_MinAmp);

	m_MaxAmp = GetPrivateProfileDouble(str_model, "MaxAmp", -1.0, str_ModelPath);
	if((m_MaxAmp == -1.0)){
		m_MaxAmp = 180.0;
		str.Empty();
		str.Format("%.1f",m_MaxAmp);
		WritePrivateProfileString(str_model,"MaxAmp",str,str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_MaxAmp = m_MaxAmp;
	str_MaxAmp.Format("%.1f",m_MaxAmp);

	m_Offset = GetPrivateProfileDouble(str_model, "OFFSET", -1, str_ModelPath);
	if ((m_Offset == -1)){
		m_Offset = 1.0;
		str.Empty();
		str.Format("%0.2f", m_Offset);
		WritePrivateProfileString(str_model, "OFFSET", str, str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_Offset = m_Offset;
	str_Offset.Format("%0.2f", m_Offset);

	m_MinAmp2 = GetPrivateProfileDouble(str_model, "MinAmp2", -1.0, str_ModelPath);
	if ((m_MinAmp2 == -1.0)){
		m_MinAmp2 = 20.0;
		str.Empty();
		str.Format("%.1f", m_MinAmp2);
		WritePrivateProfileString(str_model, "MinAmp2", str, str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_MinAmp2 = m_MinAmp2;
	str_MinAmp2.Format("%.1f", m_MinAmp2);

	m_MaxAmp2 = GetPrivateProfileDouble(str_model, "MaxAmp2", -1.0, str_ModelPath);
	if ((m_MaxAmp2 == -1.0)){
		m_MaxAmp2 = 40.0;
		str.Empty();
		str.Format("%.1f", m_MaxAmp2);
		WritePrivateProfileString(str_model, "MaxAmp2", str, str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_MaxAmp2 = m_MaxAmp2;
	str_MaxAmp2.Format("%.1f", m_MaxAmp2);

	m_Offset2 = GetPrivateProfileDouble(str_model, "OFFSET2", -1, str_ModelPath);
	if ((m_Offset2 == -1)){
		m_Offset2 = 1.0;
		str.Empty();
		str.Format("%0.2f", m_Offset2);
		WritePrivateProfileString(str_model, "OFFSET2", str, str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_Offset2 = m_Offset2;
	str_Offset2.Format("%0.2f", m_Offset2);

	m_MinAmp3 = GetPrivateProfileDouble(str_model, "MinAmp3", -1.0, str_ModelPath);
	if ((m_MinAmp3 == -1.0)){
		m_MinAmp3 =5;
		str.Empty();
		str.Format("%.1f", m_MinAmp3);
		WritePrivateProfileString(str_model, "MinAmp3", str, str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_MinAmp3 = m_MinAmp3;
	str_MinAmp3.Format("%.1f", m_MinAmp3);

	m_MaxAmp3 = GetPrivateProfileDouble(str_model, "MaxAmp3", -1.0, str_ModelPath);
	if ((m_MaxAmp3 == -1.0)){
		m_MaxAmp3 = 15;
		str.Empty();
		str.Format("%.1f", m_MaxAmp3);
		WritePrivateProfileString(str_model, "MaxAmp3", str, str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_MaxAmp3 = m_MaxAmp3;
	str_MaxAmp3.Format("%.1f", m_MaxAmp3);

	m_Offset3 = GetPrivateProfileDouble(str_model, "OFFSET3", -1, str_ModelPath);
	if ((m_Offset3 == -1)){
		m_Offset3 = 1.0;
		str.Empty();
		str.Format("%0.2f", m_Offset3);
		WritePrivateProfileString(str_model, "OFFSET3", str, str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_Offset3 = m_Offset3;
	str_Offset3.Format("%0.2f", m_Offset3);

	m_MinAmp4 = GetPrivateProfileDouble(str_model, "MinAmp4", -1.0, str_ModelPath);
	if ((m_MinAmp4 == -1.0)){
		m_MinAmp4 = 0;
		str.Empty();
		str.Format("%.1f", m_MinAmp4);
		WritePrivateProfileString(str_model, "MinAmp4", str, str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_MinAmp4 = m_MinAmp4;
	str_MinAmp4.Format("%.1f", m_MinAmp4);

	m_MaxAmp4 = GetPrivateProfileInt(str_model, "MaxAmp4", -1.0, str_ModelPath);
	if ((m_MaxAmp4 == -1.0)){
		m_MaxAmp4 = 10;
		str.Empty();
		str.Format("%.1f", m_MaxAmp4);
		WritePrivateProfileString(str_model, "MaxAmp4", str, str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_MaxAmp4 = m_MaxAmp4;
	str_MaxAmp4.Format("%.1f", m_MaxAmp4);

	m_Align = GetPrivateProfileInt(str_model, "Align", -1, str_ModelPath);
	if ((m_Align == -1)){
		m_Align = 10;
		str.Empty();
		str.Format("%d", m_Align);
		WritePrivateProfileString(str_model, "Align", str, str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_Align = m_Align;
	str_Align.Format("%d", m_Align);

	m_Offset4 = GetPrivateProfileDouble(str_model, "OFFSET4", -1, str_ModelPath);
	if ((m_Offset4 == -1)){
		m_Offset4 = 1.0;
		str.Empty();
		str.Format("%0.2f", m_Offset4);
		WritePrivateProfileString(str_model, "OFFSET4", str, str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_Offset4 = m_Offset4;
	str_Offset4.Format("%0.2f", m_Offset4);

	m_MinAmp5 = GetPrivateProfileDouble(str_model, "MinAmp5", -1.0, str_ModelPath);
	if ((m_MinAmp5 == -1.0)){
		m_MinAmp5 = 0;
		str.Empty();
		str.Format("%.1f", m_MinAmp5);
		WritePrivateProfileString(str_model, "MinAmp5", str, str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_MinAmp5 = m_MinAmp5;
	str_MinAmp5.Format("%.1f", m_MinAmp5);

	m_MaxAmp5 = GetPrivateProfileDouble(str_model, "MaxAmp5", -1.0, str_ModelPath);
	if ((m_MaxAmp5 == -1.0)){
		m_MaxAmp5 = 5;
		str.Empty();
		str.Format("%.1f", m_MaxAmp5);
		WritePrivateProfileString(str_model, "MaxAmp5", str, str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_MaxAmp5 = m_MaxAmp5;
	str_MaxAmp5.Format("%.1f", m_MaxAmp5);

	m_Offset5 = GetPrivateProfileDouble(str_model, "OFFSET5", -1, str_ModelPath);
	if ((m_Offset5 == -1)){
		m_Offset5 = 1.0;
		str.Empty();
		str.Format("%0.2f", m_Offset5);
		WritePrivateProfileString(str_model, "OFFSET5", str, str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_Offset5 = m_Offset5;
	str_Offset5.Format("%0.2f", m_Offset5);
	//-----------------
	d_Volt_Val = GetPrivateProfileDouble(str_model,"VOLTVALUE",-1,str_ModelPath);
	if((d_Volt_Val == -1)||(d_Volt_Val > 12)){
		d_Volt_Val = 6.5;
		str.Empty();
		str.Format("%3.1f",d_Volt_Val);
		WritePrivateProfileString(str_model,"VOLTVALUE",str,str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->dVoltSave =d_Volt_Val;
	m_VOLT_VALUE.Format("%3.1f V",d_Volt_Val);
	((CImageTesterDlg  *)m_pMomWnd)->m_pResStandOptWnd->Voltage_Value(m_VOLT_VALUE);
	((CImageTesterDlg  *)m_pMomWnd)->m_pResWorkerOptWnd->Voltage_Value(m_VOLT_VALUE);
	((CImageTesterDlg  *)m_pMomWnd)->m_pResLotWnd->Voltage_Value(m_VOLT_VALUE);

	((CImageTesterDlg  *)m_pMomWnd)->b_Chk_TestSkip = b_Chk_TestSkip;
	((CImageTesterDlg  *)m_pMomWnd)->b_Chk_Volton = b_Chk_Volton;
	((CImageTesterDlg  *)m_pMomWnd)->b_Chk_CurrentChkEn = b_Chk_CurrentChkEn;//3
	((CImageTesterDlg  *)m_pMomWnd)->b_Chk_Align = b_Chk_Align;

	m_dChartDistance = GetPrivateProfileInt(str_model,"CHART_DISTANCE",300,str_ModelPath);
	if((m_dChartDistance > 615)){
		m_dChartDistance = 615;
		str.Empty();
		str.Format("%d",m_dChartDistance);
		WritePrivateProfileString(str_model,"CHART_DISTANCE",str,str_ModelPath);
	}
	if((m_dChartDistance < 135)){
		m_dChartDistance = 135;
		str.Empty();
		str.Format("%d",m_dChartDistance);
		WritePrivateProfileString(str_model,"CHART_DISTANCE",str,str_ModelPath);
	}
	SetDlgItemInt(IDC_EDIT_CHART_DISTANCE, m_dChartDistance);

	m_dXModuleDistance = GetPrivateProfileInt(str_model,"MODULEX_DISTANCE_A",150,str_ModelPath);
	if((m_dXModuleDistance > 280)){
		m_dXModuleDistance = 280;
		str.Empty();
		str.Format("%d",m_dXModuleDistance);
		WritePrivateProfileString(str_model,"MODULEX_DISTANCE_A",str,str_ModelPath);
	}

	str.Format("%3.1f",(double)m_dXModuleDistance/10);
	((CEdit *)GetDlgItem(IDC_EDIT_MODULE_XDISTANCE))->SetWindowText(str);
//	SetDlgItemInt(IDC_EDIT_MODULE_XDISTANCE, m_dXModuleDistance);

	m_dYModuleDistance = GetPrivateProfileInt(str_model,"MODULEY_DISTANCE_A",150,str_ModelPath);
	if((m_dYModuleDistance > 200)){
		m_dYModuleDistance = 200;
		str.Empty();
		str.Format("%d",m_dYModuleDistance);
		WritePrivateProfileString(str_model,"MODULEY_DISTANCE_A",str,str_ModelPath);
	}
	str.Format("%3.1f",(double)m_dYModuleDistance/10);
	((CEdit *)GetDlgItem(IDC_EDIT_MODULE_YDISTANCE))->SetWindowText(str);
//	SetDlgItemInt(IDC_EDIT_MODULE_YDISTANCE, m_dYModuleDistance);

	m_dSModuleDistance = GetPrivateProfileInt(str_model, "MODULES_DISTANCE_A", 32539, str_ModelPath);
	if ((m_dSModuleDistance > 32539)){
		m_dSModuleDistance = 32539;
		str.Empty();
		str.Format("%d", m_dSModuleDistance);
		WritePrivateProfileString(str_model, "MODULES_DISTANCE_A", str, str_ModelPath);
	}
	str.Format("%3.1f", (double)m_dSModuleDistance);
	((CEdit *)GetDlgItem(IDC_EDIT_MODULE_SHUTTER))->SetWindowText(str);

	i_Lightondelay = GetPrivateProfileInt(str_model,"Lightondelay",-1,str_ModelPath);
	if(i_Lightondelay == -1){
		i_Lightondelay =1000;
		str.Empty();
		str.Format("%d",i_Lightondelay);
		WritePrivateProfileString(str_model,"Lightondelay",str,str_ModelPath);
	}
	str_LightonDelay.Format("%d",i_Lightondelay);

	i_Rayondelay = GetPrivateProfileInt(str_model,"Rayondelay",-1,str_ModelPath);
	if(i_Rayondelay == -1){
		i_Rayondelay =1000;
		str.Empty();
		str.Format("%d",i_Rayondelay);
		WritePrivateProfileString(str_model,"Rayondelay",str,str_ModelPath);
	}
	str_RayonDelay.Format("%d",i_Rayondelay);

	i_Lightoffdelay = GetPrivateProfileInt(str_model,"Lightoffdelay",-1,str_ModelPath);
	if(i_Lightoffdelay == -1){
		i_Lightoffdelay =1000;
		str.Empty();
		str.Format("%d",i_Lightoffdelay);
		WritePrivateProfileString(str_model,"Lightoffdelay",str,str_ModelPath);
	}
	str_LightoffDelay.Format("%d",i_Lightoffdelay);

	i_VoltageDelay  = GetPrivateProfileInt(str_model,"VoltageDelay",-1,str_ModelPath);
	if(i_VoltageDelay  == -1){
		i_VoltageDelay  =300;
		str.Empty();
		str.Format("%d",i_VoltageDelay );
		WritePrivateProfileString(str_model,"VoltageDelay",str,str_ModelPath);
	}
	str_VoltageDelay.Format("%d",i_VoltageDelay);


	i_Alarmdelay  = GetPrivateProfileInt(str_model,"AlarmDelay",-1,str_ModelPath);
	if(i_Alarmdelay  == -1){
		i_Alarmdelay  =3000;
		str.Empty();
		str.Format("%d",i_Alarmdelay );
		WritePrivateProfileString(str_model,"AlarmDelay",str,str_ModelPath);
	}
	str_AlarmDelay.Format("%d",i_Alarmdelay);

	i_Buzzerdelay  = GetPrivateProfileInt(str_model,"BuzzerDelay",-1,str_ModelPath);
	if(i_Buzzerdelay  == -1){
		i_Buzzerdelay  =1600;
		str.Empty();
		str.Format("%d",i_Buzzerdelay );
		WritePrivateProfileString(str_model,"BuzzerDelay",str,str_ModelPath);
	}
	str_Buzzerdelay.Format("%d",i_Buzzerdelay);

	int buf_HuType;
	buf_HuType = GetPrivateProfileInt(str_model,"Hutype",-1,str_ModelPath);
	if(buf_HuType  == -1){
		buf_HuType  =0x30;
		str.Empty();
		str.Format("%d",buf_HuType );
		WritePrivateProfileString(str_model,"Hutype",str,str_ModelPath);
	}
	((CImageTesterDlg  *)m_pMomWnd)->m_HuType = (BYTE)buf_HuType;

	buf_HuType = GetPrivateProfileInt(str_model, "CHECK_RegisterChange", -1, str_ModelPath);
	if (buf_HuType == -1){
		buf_HuType = 0;
		str.Empty();
		str.Format("%d", buf_HuType);
		WritePrivateProfileString(str_model, "CHECK_RegisterChange", str, str_ModelPath);
	}
	B_Check_RegisterChange = buf_HuType;
	((CImageTesterDlg *)m_pMomWnd)->mB_Check_RegisterChange = B_Check_RegisterChange;



	Disable_RegisterChange(B_Check_RegisterChange);
 
	//-EEPROM LOAD
	g_stEEPROM.wSlave = GetPrivateProfileInt(str_model, "EEPROM_SLAVEID", -1, str_ModelPath);
	if (g_stEEPROM.wSlave == -1){
		g_stEEPROM.wSlave = 0xAC;
		str.Empty();
		str.Format("%d", g_stEEPROM.wSlave);
		WritePrivateProfileString(str_model, "EEPROM_SLAVEID", str, str_ModelPath);
	}

	g_stEEPROM.wOC_X_Addr = GetPrivateProfileInt(str_model, "EEPROM_OC_X_Addr", -1, str_ModelPath);
	if (g_stEEPROM.wOC_X_Addr == -1){
		g_stEEPROM.wOC_X_Addr = 0x40;
		str.Empty();
		str.Format("%d", g_stEEPROM.wOC_X_Addr);
		WritePrivateProfileString(str_model, "EEPROM_OC_X_Addr", str, str_ModelPath);
	}
	g_stEEPROM.wOC_Y_Addr = GetPrivateProfileInt(str_model, "EEPROM_OC_Y_Addr", -1, str_ModelPath);
	if (g_stEEPROM.wOC_Y_Addr == -1){
		g_stEEPROM.wOC_Y_Addr = 0x44;
		str.Empty();
		str.Format("%d", g_stEEPROM.wOC_Y_Addr);
		WritePrivateProfileString(str_model, "EEPROM_OC_Y_Addr", str, str_ModelPath);
	}
	g_stEEPROM.wCheckSum_Addr = GetPrivateProfileInt(str_model, "EEPROM_CheckSum_Addr", -1, str_ModelPath);
	if (g_stEEPROM.wCheckSum_Addr == -1){
		g_stEEPROM.wCheckSum_Addr = 0xFA;
		str.Empty();
		str.Format("%d", g_stEEPROM.wCheckSum_Addr);
		WritePrivateProfileString(str_model, "EEPROM_CheckSum_Addr", str, str_ModelPath);
	}
	g_stEEPROM.wOC_X_Len = GetPrivateProfileInt(str_model, "EEPROM_OC_X_Len", -1, str_ModelPath);
	if (g_stEEPROM.wOC_X_Len == -1){
		g_stEEPROM.wOC_X_Len = 4;
		str.Empty();
		str.Format("%d", g_stEEPROM.wOC_X_Len);
		WritePrivateProfileString(str_model, "EEPROM_OC_X_Len", str, str_ModelPath);
	}

	g_stEEPROM.wOC_Y_Len = GetPrivateProfileInt(str_model, "EEPROM_OC_Y_Len", -1, str_ModelPath);
	if (g_stEEPROM.wOC_Y_Len == -1){
		g_stEEPROM.wOC_Y_Len = 4;
		str.Empty();
		str.Format("%d", g_stEEPROM.wOC_Y_Len);
		WritePrivateProfileString(str_model, "EEPROM_OC_Y_Len", str, str_ModelPath);
	}

	g_stEEPROM.wCheckSum_Len = GetPrivateProfileInt(str_model, "EEPROM_CheckSum_Len", -1, str_ModelPath);
	if (g_stEEPROM.wCheckSum_Len == -1){
		g_stEEPROM.wCheckSum_Len = 1;
		str.Empty();
		str.Format("%d", g_stEEPROM.wCheckSum_Len);
		WritePrivateProfileString(str_model, "EEPROM_CheckSum_Len", str, str_ModelPath);
	}


	buf_HuType = GetPrivateProfileInt(str_model, "EEPROM_ReadCheck", -1, str_ModelPath);
	if (buf_HuType == -1){
		buf_HuType = 0;
		str.Empty();
		str.Format("%d", buf_HuType);
		WritePrivateProfileString(str_model, "EEPROM_ReadCheck", str, str_ModelPath);
	}
	g_stEEPROM.bReadCheck = buf_HuType;
	buf_HuType = GetPrivateProfileInt(str_model, "EEPROM_CheckSumCheck", -1, str_ModelPath);
	if (buf_HuType == -1){
		buf_HuType = 0;
		str.Empty();
		str.Format("%d", buf_HuType);
		WritePrivateProfileString(str_model, "EEPROM_CheckSumCheck", str, str_ModelPath);
	}
	g_stEEPROM.bCheckSumCheck = buf_HuType;

	buf_HuType = GetPrivateProfileInt(str_model, "EEPROM_ZeroCheck", -1, str_ModelPath);
	if (buf_HuType == -1){
		buf_HuType = 0;
		str.Empty();
		str.Format("%d", buf_HuType);
		WritePrivateProfileString(str_model, "EEPROM_ZeroCheck", str, str_ModelPath);
	}
	g_stEEPROM.bZeroCheck = buf_HuType;



	
	buf_HuType = GetPrivateProfileInt(str_model, "EEPROM_OC_X_Min", -1, str_ModelPath);
	if (buf_HuType == -1){
		buf_HuType = 313;
		str.Empty();
		str.Format("%d", buf_HuType);
		WritePrivateProfileString(str_model, "EEPROM_OC_X_Min", str, str_ModelPath);
	}
	g_stEEPROM.nOC_X_Min = buf_HuType;

	buf_HuType = GetPrivateProfileInt(str_model, "EEPROM_OC_X_Max", -1, str_ModelPath);
	if (buf_HuType == -1){
		buf_HuType = 327;
		str.Empty();
		str.Format("%d", buf_HuType);
		WritePrivateProfileString(str_model, "EEPROM_OC_X_Max", str, str_ModelPath);
	}
	g_stEEPROM.nOC_X_Max = buf_HuType;
	buf_HuType = GetPrivateProfileInt(str_model, "EEPROM_OC_Y_Min", -1, str_ModelPath);
	if (buf_HuType == -1){
		buf_HuType = 233;
		str.Empty();
		str.Format("%d", buf_HuType);
		WritePrivateProfileString(str_model, "EEPROM_OC_Y_Min", str, str_ModelPath);
	}
	g_stEEPROM.nOC_Y_Min = buf_HuType;
	buf_HuType = GetPrivateProfileInt(str_model, "EEPROM_OC_Y_Max", -1, str_ModelPath);
	if (buf_HuType == -1){
		buf_HuType = 247;
		str.Empty();
		str.Format("%d", buf_HuType);
		WritePrivateProfileString(str_model, "EEPROM_OC_Y_Max", str, str_ModelPath);
	}
	g_stEEPROM.nOC_Y_Max = buf_HuType;



	Edit_SetValue(IDC_EDIT_SLAVEID, g_stEEPROM.wSlave);
	Edit_SetValue(IDC_EDIT_OCX_ADD, g_stEEPROM.wOC_X_Addr);
	Edit_SetValue(IDC_EDIT_OCY_ADD, g_stEEPROM.wOC_Y_Addr);
	Edit_SetValue(IDC_EDIT_CHECKSUM_ADD, g_stEEPROM.wCheckSum_Addr);
	Edit_SetValue(IDC_EDIT_OCX_LEN, g_stEEPROM.wOC_X_Len,0);
	Edit_SetValue(IDC_EDIT_OCY_LEN, g_stEEPROM.wOC_Y_Len, 0);
	Edit_SetValue(IDC_EDIT_CHECKSUM_LEN, g_stEEPROM.wCheckSum_Len, 0);
	Edit_SetValue(IDC_EDIT_OCX_MIN, g_stEEPROM.nOC_X_Min, 0);
	Edit_SetValue(IDC_EDIT_OCX_MAX, g_stEEPROM.nOC_X_Max, 0);
	Edit_SetValue(IDC_EDIT_OCY_MIN, g_stEEPROM.nOC_Y_Min, 0);
	Edit_SetValue(IDC_EDIT_OCY_MAX, g_stEEPROM.nOC_Y_Max, 0);

	B_Check_Read = g_stEEPROM.bReadCheck;
	B_Check_Checksum = g_stEEPROM.bCheckSumCheck;
	B_CHECK_ZERO = g_stEEPROM.bZeroCheck;


	FILE_PATH = GetPrivateProfileCString(str_model, "SAVE_FILE", str_ModelPath);
	((CImageTesterDlg *)m_pMomWnd)->SAVE_SETFILE = FILE_PATH;

	FILE_PATH_SECOND = GetPrivateProfileCString(str_model, "SAVE_FILE_SECOND", str_ModelPath);
	((CImageTesterDlg *)m_pMomWnd)->SAVE_SETFILE_Second = FILE_PATH_SECOND;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM = g_stEEPROM;

	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bCheckSumCheck = g_stEEPROM.bCheckSumCheck;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bReadCheck = g_stEEPROM.bReadCheck;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bZeroCheck = g_stEEPROM.bZeroCheck;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.nOC_X_Max = g_stEEPROM.nOC_X_Max;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.nOC_X_Min = g_stEEPROM.nOC_X_Min;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.nOC_Y_Max = g_stEEPROM.nOC_Y_Max;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.nOC_Y_Min = g_stEEPROM.nOC_Y_Min;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.wCheckSum_Addr = g_stEEPROM.wCheckSum_Addr;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.wCheckSum_Len = g_stEEPROM.wCheckSum_Len;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.wOC_X_Addr = g_stEEPROM.wOC_X_Addr;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.wOC_Y_Addr = g_stEEPROM.wOC_Y_Addr;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.wOC_X_Len = g_stEEPROM.wOC_X_Len;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.wOC_Y_Len = g_stEEPROM.wOC_Y_Len;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.wSlave = g_stEEPROM.wSlave;

	((CImageTesterDlg  *)m_pMomWnd)->i_Buzzerdelay   = i_Buzzerdelay;
	((CImageTesterDlg  *)m_pMomWnd)->i_Alarmdelay   = i_Alarmdelay;
	((CImageTesterDlg  *)m_pMomWnd)->i_VoltageDelay   = i_VoltageDelay;
	((CImageTesterDlg  *)m_pMomWnd)->i_Lightondelay   = i_Lightondelay;
	((CImageTesterDlg  *)m_pMomWnd)->i_Lightoffdelay   = i_Lightoffdelay;
	((CImageTesterDlg  *)m_pMomWnd)->i_Rayondelay = i_Rayondelay;
	((CImageTesterDlg  *)m_pMomWnd)->b_Chk_AreaSensor = b_Chk_AreaSensor;

	

	((CImageTesterDlg  *)m_pMomWnd)->m_dChartDistance   = m_dChartDistance;
	((CImageTesterDlg  *)m_pMomWnd)->m_dXModuleDistance = m_dXModuleDistance;
	((CImageTesterDlg  *)m_pMomWnd)->m_dYModuleDistance = m_dYModuleDistance;
	((CImageTesterDlg  *)m_pMomWnd)->m_dSModuleDistance = m_dSModuleDistance;

	UpdateData(FALSE);
}

void COption_SubStand::Position_Set(){
	CString str = "";
	str.Format("%3.1f",(double)m_dXModuleDistance/10);
	((CEdit *)GetDlgItem(IDC_EDIT_MODULE_XDISTANCE))->SetWindowText(str);
	str.Format("%3.1f",(double)m_dYModuleDistance/10);
	((CEdit *)GetDlgItem(IDC_EDIT_MODULE_YDISTANCE))->SetWindowText(str);
	str.Format("%3.1f", (double)m_dSModuleDistance / 10);
	((CEdit *)GetDlgItem(IDC_EDIT_MODULE_SHUTTER))->SetWindowText(str);
}

void COption_SubStand::Save_parameter(){
	
}

void COption_SubStand::OnEnChangeEditMinamp()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
// 	int buf1 = GetDlgItemInt(IDC_EDIT_MinAmp);
// 	int buf2 = GetDlgItemInt(IDC_EDIT_MaxAmp);
// 
// 	if((buf1 >=0)&&(buf2 >= 0)&&(buf1<=buf2)){
// 		if((buf1 != m_MinAmp)||(buf2 != m_MaxAmp)){
// 			((CButton *)GetDlgItem(IDC_BUTTON_AmpSave))->EnableWindow(1);
// 		}else{
// 			////((CButton *)GetDlgItem(IDC_BUTTON_AmpSave))->EnableWindow(0);
// 		}
// 	}else{
// 		////((CButton *)GetDlgItem(IDC_BUTTON_AmpSave))->EnableWindow(0);
// 	}
	UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnEnChangeEditMaxamp()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	UpdateData(TRUE);
	// 이 알림 메시지를 보내지 않습니다.
// 	int buf1 = GetDlgItemInt(IDC_EDIT_MinAmp);
// 	int buf2 = GetDlgItemInt(IDC_EDIT_MaxAmp);
// 
// 	if((buf1 >=0)&&(buf2 >= 0)&&(buf1<=buf2)){
// 		if((buf1 != m_MinAmp)||(buf2 != m_MaxAmp)){
// 			((CButton *)GetDlgItem(IDC_BUTTON_AmpSave))->EnableWindow(1);
// 		}else{
// 			//((CButton *)GetDlgItem(IDC_BUTTON_AmpSave))->EnableWindow(0);
// 		}
// 	}else{
// 		//((CButton *)GetDlgItem(IDC_BUTTON_AmpSave))->EnableWindow(0);
// 	}
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnBnClickedButtonAmpsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";

	m_MinAmp = atof(str_MinAmp);
	m_MaxAmp = atof(str_MaxAmp);
	m_Offset = atof(str_Offset);
	m_MinAmp2 = atof(str_MinAmp2);
	m_MaxAmp2 = atof(str_MaxAmp2);
	m_Offset2 = atof(str_Offset2);
	m_MinAmp3 = atof(str_MinAmp3);
	m_MaxAmp3 = atof(str_MaxAmp3);
	m_Offset3 = atof(str_Offset3);
	m_MinAmp4 = atof(str_MinAmp4);
	m_MaxAmp4 = atof(str_MaxAmp4);
	m_Offset4 = atof(str_Offset4);
	m_MinAmp5 = atof(str_MinAmp5);
	m_MaxAmp5 = atof(str_MaxAmp5);
	m_Offset5 = atof(str_Offset5);


	//m_MinAmp = GetDlgItemInt(IDC_EDIT_MinAmp);
	//m_MaxAmp = GetDlgItemInt(IDC_EDIT_MaxAmp);
	//m_Offset = atof(str_Offset);
	//m_MinAmp2 = GetDlgItemInt(IDC_EDIT_MinAmp2);
	//m_MaxAmp2 = GetDlgItemInt(IDC_EDIT_MaxAmp2);
	//m_Offset2 = atof(str_Offset2);
	//m_MinAmp3 = GetDlgItemInt(IDC_EDIT_MinAmp3);
	//m_MaxAmp3 = GetDlgItemInt(IDC_EDIT_MaxAmp3);
	//m_Offset3 = atof(str_Offset3);
	//m_MinAmp4 = GetDlgItemInt(IDC_EDIT_MinAmp4);
	//m_MaxAmp4 = GetDlgItemInt(IDC_EDIT_MaxAmp4);
	//m_Offset4 = atof(str_Offset4);
	//m_MinAmp5 = GetDlgItemInt(IDC_EDIT_MinAmp5);
	//m_MaxAmp5 = GetDlgItemInt(IDC_EDIT_MaxAmp5);
	//m_Offset5 = atof(str_Offset5);

	((CImageTesterDlg  *)m_pMomWnd)->m_MinAmp = m_MinAmp;
	((CImageTesterDlg  *)m_pMomWnd)->m_MaxAmp = m_MaxAmp;
	((CImageTesterDlg  *)m_pMomWnd)->m_Offset = m_Offset;
	((CImageTesterDlg  *)m_pMomWnd)->m_MinAmp2 = m_MinAmp2;
	((CImageTesterDlg  *)m_pMomWnd)->m_MaxAmp2 = m_MaxAmp2;
	((CImageTesterDlg  *)m_pMomWnd)->m_Offset2 = m_Offset2;
	((CImageTesterDlg  *)m_pMomWnd)->m_MinAmp3 = m_MinAmp3;
	((CImageTesterDlg  *)m_pMomWnd)->m_MaxAmp3 = m_MaxAmp3;
	((CImageTesterDlg  *)m_pMomWnd)->m_Offset3 = m_Offset3;
	((CImageTesterDlg  *)m_pMomWnd)->m_MinAmp4 = m_MinAmp4;
	((CImageTesterDlg  *)m_pMomWnd)->m_MaxAmp4 = m_MaxAmp4;
	((CImageTesterDlg  *)m_pMomWnd)->m_Offset4 = m_Offset4;
	((CImageTesterDlg  *)m_pMomWnd)->m_MinAmp5 = m_MinAmp5;
	((CImageTesterDlg  *)m_pMomWnd)->m_MaxAmp5 = m_MaxAmp5;
	((CImageTesterDlg  *)m_pMomWnd)->m_Offset5 = m_Offset5;

	str_MinAmp.Empty();
	str_MinAmp.Format("%.1f",m_MinAmp);
	WritePrivateProfileString(str_model,"MinAmp",str_MinAmp,str_ModelPath);
	str_MaxAmp.Empty();
	str_MaxAmp.Format("%.1f",m_MaxAmp);
	WritePrivateProfileString(str_model,"MaxAmp",str_MaxAmp,str_ModelPath);
	str_Offset.Empty();
	str_Offset.Format("%0.2f", m_Offset);
	WritePrivateProfileString(str_model, "Offset", str_Offset, str_ModelPath);
	str_MinAmp2.Empty();
	str_MinAmp2.Format("%.1f", m_MinAmp2);
	WritePrivateProfileString(str_model, "MinAmp2", str_MinAmp2, str_ModelPath);
	str_MaxAmp2.Empty();
	str_MaxAmp2.Format("%.1f", m_MaxAmp2);
	WritePrivateProfileString(str_model, "MaxAmp2", str_MaxAmp2, str_ModelPath);
	str_Offset2.Empty();
	str_Offset2.Format("%0.2f", m_Offset2);
	WritePrivateProfileString(str_model, "Offset2", str_Offset2, str_ModelPath);
	str_MinAmp3.Empty();
	str_MinAmp3.Format("%.1f", m_MinAmp3);
	WritePrivateProfileString(str_model, "MinAmp3", str_MinAmp3, str_ModelPath);
	str_MaxAmp3.Empty();
	str_MaxAmp3.Format("%.1f", m_MaxAmp3);
	WritePrivateProfileString(str_model, "MaxAmp3", str_MaxAmp3, str_ModelPath);
	str_Offset3.Empty();
	str_Offset3.Format("%0.2f", m_Offset3);
	WritePrivateProfileString(str_model, "Offset3", str_Offset3, str_ModelPath);
	str_MinAmp4.Empty();
	str_MinAmp4.Format("%.1f", m_MinAmp4);
	WritePrivateProfileString(str_model, "MinAmp4", str_MinAmp4, str_ModelPath);
	str_MaxAmp4.Empty();
	str_MaxAmp4.Format("%.1f", m_MaxAmp4);
	WritePrivateProfileString(str_model, "MaxAmp4", str_MaxAmp4, str_ModelPath);
	str_Offset4.Empty();
	str_Offset4.Format("%0.2f", m_Offset4);
	WritePrivateProfileString(str_model, "Offset4", str_Offset4, str_ModelPath);
	str_MinAmp5.Empty();
	str_MinAmp5.Format("%.1f", m_MinAmp5);
	WritePrivateProfileString(str_model, "MinAmp5", str_MinAmp5, str_ModelPath);
	str_MaxAmp5.Empty();
	str_MaxAmp5.Format("%.1f", m_MaxAmp5);
	WritePrivateProfileString(str_model, "MaxAmp5", str_MaxAmp5, str_ModelPath);
	str_Offset5.Empty();
	str_Offset5.Format("%0.2f", m_Offset5);
	WritePrivateProfileString(str_model, "Offset5", str_Offset5, str_ModelPath);


	SetDlgItemText(IDC_EDIT_MinAmp, str_MinAmp);
	SetDlgItemText(IDC_EDIT_MaxAmp, str_MaxAmp);
	SetDlgItemText(IDC_EDIT_OFFSET, str_Offset);
	SetDlgItemText(IDC_EDIT_MinAmp2, str_MinAmp2);
	SetDlgItemText(IDC_EDIT_MaxAmp2, str_MaxAmp2);
	SetDlgItemText(IDC_EDIT_OFFSET2, str_Offset2);
	SetDlgItemText(IDC_EDIT_MinAmp3, str_MinAmp3);
	SetDlgItemText(IDC_EDIT_MaxAmp3, str_MaxAmp3);
	SetDlgItemText(IDC_EDIT_OFFSET3, str_Offset3);
	SetDlgItemText(IDC_EDIT_MinAmp4, str_MinAmp4);
	SetDlgItemText(IDC_EDIT_MaxAmp4, str_MaxAmp4);
	SetDlgItemText(IDC_EDIT_OFFSET4, str_Offset4);
	SetDlgItemText(IDC_EDIT_MinAmp5, str_MinAmp5);
	SetDlgItemText(IDC_EDIT_MaxAmp5, str_MaxAmp5);
	SetDlgItemText(IDC_EDIT_OFFSET5, str_Offset5);

	UpdateData(FALSE);
	//SetDlgItemInt(IDC_EDIT_MinAmp,m_MinAmp,0);
	//SetDlgItemInt(IDC_EDIT_MaxAmp,m_MaxAmp,0);
	//SetDlgItemText(IDC_EDIT_OFFSET, str_Offset);
	//SetDlgItemInt(IDC_EDIT_MinAmp2, m_MinAmp2, 0);
	//SetDlgItemInt(IDC_EDIT_MaxAmp2, m_MaxAmp2, 0);
	//SetDlgItemText(IDC_EDIT_OFFSET2, str_Offset2);
	//SetDlgItemInt(IDC_EDIT_MinAmp3, m_MinAmp3, 0);
	//SetDlgItemInt(IDC_EDIT_MaxAmp3, m_MaxAmp3, 0);
	//SetDlgItemText(IDC_EDIT_OFFSET3, str_Offset3);
	//SetDlgItemInt(IDC_EDIT_MinAmp4, m_MinAmp4, 0);
	//SetDlgItemInt(IDC_EDIT_MaxAmp4, m_MaxAmp4, 0);
	//SetDlgItemText(IDC_EDIT_OFFSET4, str_Offset4);
	//SetDlgItemInt(IDC_EDIT_MinAmp5, m_MinAmp5, 0);
	//SetDlgItemInt(IDC_EDIT_MaxAmp5, m_MaxAmp5, 0);
	//SetDlgItemText(IDC_EDIT_OFFSET5, str_Offset5);
	
	//((CButton *)GetDlgItem(IDC_BUTTON_AmpSave))->EnableWindow(0);
}

void COption_SubStand::OnBnClickedCheckVoltageon()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	CString str = "";
	((CImageTesterDlg  *)m_pMomWnd)->b_Chk_Volton = b_Chk_Volton;
	str.Empty();
	str.Format("%d",b_Chk_Volton);
	WritePrivateProfileString(str_model,"START_CHKVOLTON",str,str_ModelPath);

	m_VOLT_Change.Format("");
	m_MinAmp = ((CImageTesterDlg  *)m_pMomWnd)->m_MinAmp;
	m_MaxAmp = ((CImageTesterDlg  *)m_pMomWnd)->m_MaxAmp;
	if(b_Chk_Volton == TRUE){
		((CEdit *)GetDlgItem(IDC_INPUT_VVALUE))->EnableWindow(1);
		((CEdit *)GetDlgItem(IDC_EDIT_MinAmp))->EnableWindow(1);
		((CEdit *)GetDlgItem(IDC_EDIT_MaxAmp))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_CHECK_CHKCURRENT))->EnableWindow(1);
		//((CButton *)GetDlgItem(IDC_BUTTON_AmpSave))->EnableWindow(0);
	}else{
		((CEdit *)GetDlgItem(IDC_INPUT_VVALUE))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MinAmp))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MaxAmp))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_CHKCURRENT))->EnableWindow(0);
		//((CButton *)GetDlgItem(IDC_BUTTON_AmpSave))->EnableWindow(0);
	}
	UpdateData(FALSE);
}

void COption_SubStand::OnBnClickedCheckChkcurrent()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	CString str = "";
	((CImageTesterDlg  *)m_pMomWnd)->b_Chk_CurrentChkEn = b_Chk_CurrentChkEn;
	str.Empty();
	str.Format("%d",b_Chk_CurrentChkEn);
	WritePrivateProfileString(str_model,"START_CHKCURRENTEN",str,str_ModelPath);
	
	m_MinAmp = ((CImageTesterDlg  *)m_pMomWnd)->m_MinAmp;
	m_MaxAmp = ((CImageTesterDlg  *)m_pMomWnd)->m_MaxAmp;
	if(b_Chk_CurrentChkEn == TRUE){
		((CEdit *)GetDlgItem(IDC_EDIT_MinAmp))->EnableWindow(1);
		((CEdit *)GetDlgItem(IDC_EDIT_MaxAmp))->EnableWindow(1);
		//((CButton *)GetDlgItem(IDC_BUTTON_AmpSave))->EnableWindow(0);
		ModeName = "Start(동작전류)";
	}else{
		((CEdit *)GetDlgItem(IDC_EDIT_MinAmp))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MaxAmp))->EnableWindow(0);
		//((CButton *)GetDlgItem(IDC_BUTTON_AmpSave))->EnableWindow(0);
		ModeName = "Start";
	}
	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[0]->NameChange(ModeName);
	/*if(b_Chk_CurrentChkEn == TRUE){
		((CImageTesterDlg  *)m_pMomWnd)->worklist_uploadList(1);
	}else{
		((CImageTesterDlg  *)m_pMomWnd)->worklist_uploadList(0);
	}*/

	
	UpdateData(FALSE);
}

void COption_SubStand::OnEnChangeEditChartDistance()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	int buf = GetDlgItemInt(IDC_EDIT_CHART_DISTANCE);
	if((buf >= 135)&&(buf <= 615)){
		if((m_dChartDistance == buf)){
			((CButton *)GetDlgItem(IDC_BTN_CHART_MOVE_SAVE))->EnableWindow(0);
		}else{
			((CButton *)GetDlgItem(IDC_BTN_CHART_MOVE_SAVE))->EnableWindow(1);
		}
		((CButton *)GetDlgItem(IDC_BTN_CHART_TESTMOVE))->EnableWindow(1);
	}else{
		((CButton *)GetDlgItem(IDC_BTN_CHART_MOVE_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_CHART_TESTMOVE))->EnableWindow(0);
	}
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnBnClickedBtnChartMoveSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_dChartDistance = GetDlgItemInt(IDC_EDIT_CHART_DISTANCE);
	
	if(m_dChartDistance <= 135){
		m_dChartDistance = 135;
	}else if(m_dChartDistance >= 615){
		m_dChartDistance = 615;
	}
	CString str="";
	str.Empty();
	str.Format("%d",m_dChartDistance);
	((CImageTesterDlg  *)m_pMomWnd)->m_dChartDistance = m_dChartDistance;

	/*if(((CImageTesterDlg  *)m_pMomWnd)->m_pDistortionOptWnd != NULL){
		((CImageTesterDlg  *)m_pMomWnd)->m_pDistortionOptWnd->i_MotorDis = m_dChartDistance;
		((CImageTesterDlg  *)m_pMomWnd)->m_pDistortionOptWnd->str_MotorDis.Format("%d",m_dChartDistance);
		((CImageTesterDlg  *)m_pMomWnd)->m_pDistortionOptWnd->UpdateData(FALSE);
	}*/
	WritePrivateProfileString(str_model,"CHART_DISTANCE",str,str_ModelPath);
	((CButton *)GetDlgItem(IDC_BTN_CHART_MOVE_SAVE))->EnableWindow(0);
}


void COption_SubStand::OnEnChangeEditModuleXdistance()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	CString str = "";
	double buf_data;
	GetDlgItemText(IDC_EDIT_MODULE_XDISTANCE,str);
	str.Replace(" ","");
	buf_data = atof(str) + 0.05;
	int buf  = (int)(buf_data*10);
	
	if((buf >= 0)&&(buf <= 280)&&(str != "")){
		if((m_dXModuleDistance == buf)){
			((CButton *)GetDlgItem(IDC_BTN_MODULE_X_SAVE))->EnableWindow(0);
		}else{
			((CButton *)GetDlgItem(IDC_BTN_MODULE_X_SAVE))->EnableWindow(1);
		}
		((CButton *)GetDlgItem(IDC_BTN_MODULEX_TESTMOVE))->EnableWindow(1);
	}else{
		((CButton *)GetDlgItem(IDC_BTN_MODULE_X_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_MODULEX_TESTMOVE))->EnableWindow(0);
	}
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnBnClickedBtnModuleXSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//m_dXModuleDistance = GetDlgItemInt(IDC_EDIT_MODULE_XDISTANCE);
	CString str = "";
	double buf_data;
	GetDlgItemText(IDC_EDIT_MODULE_XDISTANCE,str);
	buf_data = atof(str) + 0.05;
	m_dXModuleDistance = (int)(buf_data*10);

	if(m_dXModuleDistance <= 0){
		m_dXModuleDistance = 0;
	}else if(m_dXModuleDistance >= 280){
		m_dXModuleDistance = 280;
	}
//	buf_data = (double)m_dXModuleDistance/10;
	/*str.Empty();
	str.Format("%d",m_dXModuleDistance);
	((CImageTesterDlg  *)m_pMomWnd)->m_dXModuleDistance = m_dXModuleDistance;
	WritePrivateProfileString(str_model,"MODULEX_DISTANCE_A",str,str_ModelPath);*/
	((CImageTesterDlg  *)m_pMomWnd)->SavePositionSet(m_dXModuleDistance,((CImageTesterDlg  *)m_pMomWnd)->m_dYModuleDistance);
	((CButton *)GetDlgItem(IDC_BTN_MODULE_X_SAVE))->EnableWindow(0);
}

void COption_SubStand::OnEnChangeEditModuleYdistance()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	//int buf = GetDlgItemInt(IDC_EDIT_MODULE_YDISTANCE);
	CString str = "";
	double buf_data;
	GetDlgItemText(IDC_EDIT_MODULE_YDISTANCE,str);
	str.Replace(" ","");
	buf_data = atof(str) + 0.05;
	int buf  = (int)(buf_data*10);


	if((buf >=0)&&(buf <= 200)&&(str != "")){
		if((m_dYModuleDistance == buf)){
			((CButton *)GetDlgItem(IDC_BTN_MODULE_Y_SAVE))->EnableWindow(0);
		}else{
			((CButton *)GetDlgItem(IDC_BTN_MODULE_Y_SAVE))->EnableWindow(1);
		}
		((CButton *)GetDlgItem(IDC_BTN_MODULEY_TESTMOVE))->EnableWindow(1);
	}else{
		((CButton *)GetDlgItem(IDC_BTN_MODULE_Y_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_MODULEY_TESTMOVE))->EnableWindow(0);
	}
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnEnChangeEditModuleSdistance()
{
	CString str = "";
	double buf_data;
	GetDlgItemText(IDC_EDIT_MODULE_SHUTTER, str);
	str.Replace(" ", "");
	buf_data = atof(str) + 0.05;
	int buf = (int)(buf_data);


	if ((buf >= 0) && (buf <= 32539) && (str != "")){
		if ((m_dSModuleDistance == buf)){
			((CButton *)GetDlgItem(IDC_BTN_MODULE_S_SAVE))->EnableWindow(0);
		}
		else{
			((CButton *)GetDlgItem(IDC_BTN_MODULE_S_SAVE))->EnableWindow(1);
		}
		((CButton *)GetDlgItem(IDC_BTN_MODULES_TESTMOVE))->EnableWindow(1);
	}
	else{
		((CButton *)GetDlgItem(IDC_BTN_MODULE_S_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_MODULES_TESTMOVE))->EnableWindow(0);
	}
}

void COption_SubStand::OnBnClickedBtnModuleYSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	double buf_data;
	GetDlgItemText(IDC_EDIT_MODULE_YDISTANCE,str);
	buf_data = atof(str) + 0.05;
	m_dYModuleDistance = (int)(buf_data*10);

	if(m_dYModuleDistance <= 0){
		m_dYModuleDistance = 0;
	}else if(m_dYModuleDistance >= 200){
		m_dYModuleDistance = 200;
	}
	/*str.Empty();
	str.Format("%d",m_dYModuleDistance);
	((CImageTesterDlg  *)m_pMomWnd)->m_dYModuleDistance = m_dYModuleDistance;
	WritePrivateProfileString(str_model,"MODULEY_DISTANCE_A",str,str_ModelPath);*/
	((CImageTesterDlg  *)m_pMomWnd)->SavePositionSet(((CImageTesterDlg  *)m_pMomWnd)->m_dXModuleDistance,m_dYModuleDistance);
	((CButton *)GetDlgItem(IDC_BTN_MODULE_Y_SAVE))->EnableWindow(0);
}
void COption_SubStand::TESTMOVEBTN_Enable(bool MODE){
	
// 	(CButton *)GetDlgItem(IDC_BTN_CHART_TESTMOVE)->EnableWindow(MODE);
// 	(CButton *)GetDlgItem(IDC_BTN_MODULEX_TESTMOVE)->EnableWindow(MODE);
// 	(CButton *)GetDlgItem(IDC_BTN_MODULEY_TESTMOVE)->EnableWindow(MODE);
	(CButton *)GetDlgItem(IDC_BUTTON_CYL_IN)->EnableWindow(MODE);
	(CButton *)GetDlgItem(IDC_BUTTON_CYL_OUT)->EnableWindow(MODE);
// 	(CButton *)GetDlgItem(IDC_BUTTON_TEST_POS)->EnableWindow(MODE);
// 	(CButton *)GetDlgItem(IDC_BUTTON_STANDBY)->EnableWindow(MODE);

}
void COption_SubStand::OnBnClickedBtnChartTestmove()//0~350
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	TESTMOVEBTN_Enable(0);
	int m_tstdata = GetDlgItemInt(IDC_EDIT_CHART_DISTANCE);

	if(m_tstdata <= 135){
		m_tstdata = 135;
	}
	
	if(m_tstdata >= 615){
		m_tstdata = 615;
	}

	((CImageTesterDlg  *)m_pMomWnd)->Move_Chart(m_tstdata);
	TESTMOVEBTN_Enable(1);

}

void COption_SubStand::OnBnClickedBtnModulexTestmove()//0~200
{
 	 //TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
  	TESTMOVEBTN_Enable(0);
  
  	CString str = "";
  	double buf_data;
  	GetDlgItemText(IDC_EDIT_MODULE_XDISTANCE,str);
  	buf_data = atof(str) + 0.05;
  	int m_tstdata = (int)(buf_data*10);
  
  	if(m_tstdata <= 0){
  		m_tstdata = 0;
  	}
  	
  	if(m_tstdata >= 280){
  		m_tstdata = 280;
  	}
  
  	((CImageTesterDlg  *)m_pMomWnd)->Move_ModuleX(m_tstdata);
  	TESTMOVEBTN_Enable(1);

}

void COption_SubStand::OnBnClickedBtnModuleyTestmove()//35~155
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	TESTMOVEBTN_Enable(0);

	CString str = "";
	double buf_data;
	GetDlgItemText(IDC_EDIT_MODULE_YDISTANCE,str);
	buf_data = atof(str) + 0.05;
	int m_tstdata = (int)(buf_data*10);

	if(m_tstdata <= 0){
		m_tstdata = 0;
	}
	
	if(m_tstdata >= 200){
		m_tstdata = 200;
	}

	((CImageTesterDlg  *)m_pMomWnd)->Move_ModuleY(m_tstdata);
	TESTMOVEBTN_Enable(1);

}



void COption_SubStand::OnBnClickedBtnLightAllOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Light_ONOFF(0);//실린더를 올린다.
}

void COption_SubStand::OnBnClickedBtnLightRayOn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->RAY_ONOFF(1);
}

void COption_SubStand::OnBnClickedBtnLightLiOn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->Light_ONOFF(1);
}

void COption_SubStand::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == TRUE){
//		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = 0;	//SDY
	}else{
//		((CImageTesterDlg  *)m_pMomWnd)->m_SubRunNum = -1;

	}
}


void COption_SubStand::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}

void COption_SubStand::GetOverlayImage(CString MODELNAME, bool STATE)
{
		DWORD	RGBPIX = 0,RGBLINE=0;

	IplImage *testImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3);
	BYTE	*Aver,*m_NUM1,*m_NUM2,*m_NUM3;


	m_NUM1 = (BYTE *)malloc(sizeof(BYTE *)*(m_CAM_SIZE_WIDTH*m_CAM_SIZE_HEIGHT*4));
	m_NUM2 = (BYTE *)malloc(sizeof(BYTE *)*(m_CAM_SIZE_WIDTH*m_CAM_SIZE_HEIGHT*4));
	m_NUM3 = (BYTE *)malloc(sizeof(BYTE *)*(m_CAM_SIZE_WIDTH*m_CAM_SIZE_HEIGHT*4));

	Aver = (BYTE *)malloc(sizeof(BYTE *)*(m_CAM_SIZE_WIDTH*m_CAM_SIZE_HEIGHT*4));

	for(int t=0; t<3; t++){       // 3개 찍어서 배열에 넣고.
		((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan =1;						

		for(int i =0;i<10;i++){
			if(((CImageTesterDlg  *)m_pMomWnd)->m_ImgScan == 0){
				break;
			}else{
				DoEvents(50);
			}
		}	

		RGBLINE=0;
		RGBPIX=0;
		for(int lopy = 0;lopy < m_CAM_SIZE_HEIGHT;lopy++){
			RGBPIX=0;
			for(int lopx = 0;lopx < m_CAM_SIZE_HEIGHT;lopx++){
				if(t ==0){
					m_NUM1[RGBLINE + RGBPIX] =	m_RGBScanbuf[RGBLINE + RGBPIX];  
					m_NUM1[RGBLINE + RGBPIX + 1]=	m_RGBScanbuf[RGBLINE + RGBPIX+1]; 
					m_NUM1[RGBLINE + RGBPIX + 2]=	m_RGBScanbuf[RGBLINE + RGBPIX+2]; 
				}
				if(t==1){
					m_NUM2[RGBLINE + RGBPIX] =	m_RGBScanbuf[RGBLINE + RGBPIX];  
					m_NUM2[RGBLINE + RGBPIX + 1]=	m_RGBScanbuf[RGBLINE + RGBPIX+1]; 
					m_NUM2[RGBLINE + RGBPIX + 2]=	m_RGBScanbuf[RGBLINE + RGBPIX+2]; 
				}
				if(t==2){
					m_NUM3[RGBLINE + RGBPIX] =	m_RGBScanbuf[RGBLINE + RGBPIX];  
					m_NUM3[RGBLINE + RGBPIX + 1]=	m_RGBScanbuf[RGBLINE + RGBPIX+1]; 
					m_NUM3[RGBLINE + RGBPIX + 2]=	m_RGBScanbuf[RGBLINE + RGBPIX+2]; 
				}
				RGBPIX+=4;
			}
			RGBLINE+=(m_CAM_SIZE_WIDTH*4);
		}
	}

	RGBLINE=0;
	RGBPIX=0;
	for(int lopy = 0;lopy < m_CAM_SIZE_HEIGHT;lopy++){
		RGBPIX=0;
		for(int lopx = 0;lopx < m_CAM_SIZE_WIDTH;lopx++){//////한번이라도 크면 크게 나오면 빼기
		/*	
			Aver[RGBLINE + RGBPIX] = highlevel_Search(m_NUM1[RGBLINE + RGBPIX],m_NUM2[RGBLINE + RGBPIX],m_NUM3[RGBLINE + RGBPIX]);
			Aver[RGBLINE + RGBPIX+1] = highlevel_Search(m_NUM1[RGBLINE + RGBPIX+1],m_NUM2[RGBLINE + RGBPIX+1],m_NUM3[RGBLINE + RGBPIX+1]);
			Aver[RGBLINE + RGBPIX+2] = highlevel_Search(m_NUM1[RGBLINE + RGBPIX+2],m_NUM2[RGBLINE + RGBPIX+2],m_NUM3[RGBLINE + RGBPIX+2]);*/

			Aver[RGBLINE + RGBPIX] = (m_NUM1[RGBLINE + RGBPIX]+m_NUM2[RGBLINE + RGBPIX]+m_NUM3[RGBLINE + RGBPIX])/3;
			Aver[RGBLINE + RGBPIX+ 1] = (m_NUM1[RGBLINE + RGBPIX+1]+m_NUM2[RGBLINE + RGBPIX+1]+m_NUM3[RGBLINE + RGBPIX+1])/3;
			Aver[RGBLINE + RGBPIX+ 2] = (m_NUM1[RGBLINE + RGBPIX+2]+m_NUM2[RGBLINE + RGBPIX+2]+m_NUM3[RGBLINE + RGBPIX+2])/3;

			RGBPIX+=4;
		}
		RGBLINE+=(m_CAM_SIZE_WIDTH*4);
	}




	for(int y = 0; y<m_CAM_SIZE_HEIGHT; y++)
	{
		for(int x=0; x<m_CAM_SIZE_WIDTH; x++)
		{
			testImage->imageData[y * testImage->widthStep + x * 3 + 0] = Aver[y * (m_CAM_SIZE_WIDTH*4) + x * 4 + 0];
			testImage->imageData[y * testImage->widthStep + x * 3 + 1] = Aver[y * (m_CAM_SIZE_WIDTH*4) + x * 4 + 1];
			testImage->imageData[y * testImage->widthStep + x * 3 + 2] = Aver[y * (m_CAM_SIZE_WIDTH*4) + x * 4 + 2];
		}
	}
		

	CString savePath;
	savePath.Empty();
	savePath = MODELNAME +".bmp";

		cvShowImage(MODELNAME,testImage);
		cvWaitKey(0);
		cvDestroyWindow(MODELNAME);

	//	((CImageTesterDlg  *)m_pMomWnd)->OverlayImagePath = ((CImageTesterDlg  *)m_pMomWnd)->Overlay_path + "\\" + savePath;
	//	cvSaveImage(((CImageTesterDlg  *)m_pMomWnd)->OverlayImagePath, testImage);

	//((CImageTesterDlg  *)m_pMomWnd)->OverlayName=MODELNAME;



	cvReleaseImage(&testImage);


	free(Aver);
	free(m_NUM1);
	free(m_NUM2);
	free(m_NUM3);

}
BYTE COption_SubStand::highlevel_Search(BYTE N1,BYTE N2,BYTE N3){

	if((N1 >= N2) && (N1 >= N3)){
		return N1;
	}else if((N2 >= N1) && (N2 >= N3)){
		return N2;
		
	}else if((N3 >= N1) && (N3 >= N2)){
		return N3;
	
	}


}




HBRUSH COption_SubStand::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_OVERLAYNAME){
	//	pDC->SetTextColor(txcol_PortName);
	//	pDC->SetBkColor(bkcol_PortName);
		pDC->SetTextColor(RGB(255, 255, 255));
		// 텍스트의 배경색상을 설정한다.
		//pDC->SetBkColor(RGB(138, 75, 36));
		pDC->SetBkColor(RGB(86,86,86));
		// 바탕색상을 설정한다.
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void COption_SubStand::OnBnClickedButtonOrigin()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	((CImageTesterDlg  *)m_pMomWnd)->InitMotor();

	
	((CImageTesterDlg  *)m_pMomWnd)->m_pModSetDlgWnd->ShowWindow(SW_HIDE);
	((CImageTesterDlg  *)m_pMomWnd)->m_pModSetDlgWnd->ShowWindow(SW_SHOW);
}

void COption_SubStand::OnBnClickedButtonOriginStop()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	
	((CImageTesterDlg  *)m_pMomWnd)->m_Motion.StopAllAxis();
	

}


void COption_SubStand::OnBnClickedButtonCylIn()
{
	TESTMOVEBTN_Enable(0);
	((CImageTesterDlg  *)m_pMomWnd)->m_Motion.CylinderIn();
	TESTMOVEBTN_Enable(1);
}

void COption_SubStand::OnBnClickedButtonCylOut()
{
	TESTMOVEBTN_Enable(0);
	((CImageTesterDlg  *)m_pMomWnd)->m_Motion.CylinderOut();
	TESTMOVEBTN_Enable(1);
}

void COption_SubStand::OnEnSetfocusDataVvalue()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Focus_move_start();
}

void COption_SubStand::Focus_move_start()
{
	if(((CButton *)GetDlgItem(IDC_BTN_VOLTSAVE))->IsWindowEnabled() == TRUE){
		GotoDlgCtrl(((CButton *)GetDlgItem(IDC_BTN_VOLTSAVE)));
	}else{
		GotoDlgCtrl(((CStatic *)GetDlgItem(IDC_STATIC)));
	}
}

void COption_SubStand::OnBnClickedButtonTestPos()
{
	TESTMOVEBTN_Enable(0);
	((CImageTesterDlg *)m_pMomWnd)->m_Motion.CylinderOut();
	((CImageTesterDlg *)m_pMomWnd)->m_Motion.TestPosMove();
	TESTMOVEBTN_Enable(1);
}

void COption_SubStand::OnBnClickedButtonStandby()
{
	TESTMOVEBTN_Enable(0);

	((CImageTesterDlg *)m_pMomWnd)->m_Motion.OriginStandbyPosMove();
	TESTMOVEBTN_Enable(1);

}

void COption_SubStand::OnEnChangeEditLighton()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnEnChangeEditLighton2()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnEnChangeEditLighton3()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnBnClickedButtonLightondelaysave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	i_Lightondelay = atoi(str_LightonDelay);

	if(i_Lightondelay < 1){
		i_Lightondelay = 1;
	} 
	if(i_Lightondelay > 300000){
		i_Lightondelay = 300000;
	}

	str_LightonDelay.Format("%d",i_Lightondelay);

	UpdateData(FALSE);
	((CImageTesterDlg *)m_pMomWnd)->i_Lightondelay = i_Lightondelay;
	 WritePrivateProfileString(str_model,"Lightondelay",str_LightonDelay,str_ModelPath);
}

void COption_SubStand::OnBnClickedButtonLightondelaysave2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	i_Rayondelay = atoi(str_RayonDelay);

	if(i_Rayondelay < 1){
		i_Rayondelay = 1;
	} 
	if(i_Rayondelay > 300000){
		i_Rayondelay = 300000;
	}

	str_RayonDelay.Format("%d",i_Rayondelay);

	UpdateData(FALSE);
	((CImageTesterDlg *)m_pMomWnd)->i_Rayondelay = i_Rayondelay;
	 WritePrivateProfileString(str_model,"Rayondelay",str_RayonDelay,str_ModelPath);
	
}

void COption_SubStand::OnBnClickedButtonLightondelaysave3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.


	i_Lightoffdelay = atoi(str_LightoffDelay);

	if(i_Lightoffdelay < 1){
		i_Lightoffdelay = 1;
	} 
	if(i_Lightoffdelay > 300000){
		i_Lightoffdelay = 300000;
	}

	str_LightoffDelay.Format("%d",i_Lightoffdelay);

	UpdateData(FALSE);
	((CImageTesterDlg *)m_pMomWnd)->i_Lightoffdelay = i_Lightoffdelay;
	 WritePrivateProfileString(str_model,"Lightoffdelay",str_LightoffDelay,str_ModelPath);
}

void COption_SubStand::OnEnChangeEditVoltagedelay()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnBnClickedButtonBuzzerPass()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->BUZZER_OPERATING(1);

}

void COption_SubStand::OnBnClickedButtonBuzzerFail()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->BUZZER_OPERATING(2);

}

void COption_SubStand::OnBnClickedButtonBuzzerOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->BUZZER_OPERATING(0);

}

void COption_SubStand::OnBnClickedButtonAlarmRun()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->ALARM_OPERATING(1);

}

void COption_SubStand::OnBnClickedButtonAlarmPass()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->ALARM_OPERATING(2);

}

void COption_SubStand::OnBnClickedButtonAlarmFail()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->ALARM_OPERATING(3);

}

void COption_SubStand::OnBnClickedButtonAlarmOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg  *)m_pMomWnd)->ALARM_OPERATING(0);

}

void COption_SubStand::OnBnClickedButtonAlarnsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	i_Alarmdelay = atoi(str_AlarmDelay);

	if(i_Alarmdelay < 1){
		i_Alarmdelay = 1;
	} 
	if(i_Alarmdelay > 300000){
		i_Alarmdelay = 300000;
	}

	str_AlarmDelay.Format("%d",i_Alarmdelay);

	UpdateData(FALSE);
	((CImageTesterDlg *)m_pMomWnd)->i_Alarmdelay = i_Alarmdelay;
	WritePrivateProfileString(str_model,"AlarmDelay",str_AlarmDelay,str_ModelPath);

}

void COption_SubStand::OnEnChangeEditAlarm()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnEnChangeEditAlarm2()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnBnClickedButtonAlarnsave2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	i_Buzzerdelay = atoi(str_Buzzerdelay);

	if(i_Buzzerdelay < 1){
		i_Buzzerdelay = 1;
	} 
	if(i_Buzzerdelay > 300000){
		i_Buzzerdelay = 300000;
	}

	str_Buzzerdelay.Format("%d",i_Buzzerdelay);

	UpdateData(FALSE);
	((CImageTesterDlg *)m_pMomWnd)->i_Buzzerdelay = i_Buzzerdelay;
	WritePrivateProfileString(str_model,"BuzzerDelay",str_Buzzerdelay,str_ModelPath);
}

void COption_SubStand::OnBnClickedBtnVoltsave2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	i_VoltageDelay = atoi(str_VoltageDelay);


	if(i_VoltageDelay < 1){
		i_VoltageDelay = 1;
	} 
	if(i_VoltageDelay > 300000){
		i_VoltageDelay = 300000;
	}

	str_VoltageDelay.Format("%d",i_VoltageDelay);

	UpdateData(FALSE);
	((CImageTesterDlg *)m_pMomWnd)->i_VoltageDelay = i_VoltageDelay;
	 WritePrivateProfileString(str_model,"VoltageDelay",str_VoltageDelay,str_ModelPath);
}

void COption_SubStand::OnBnClickedCheckSaeoff()
{
	//// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//UpdateData(TRUE);
	//CString str = "";
	//((CImageTesterDlg  *)m_pMomWnd)->m_SAEStat = b_SAEStat;
	//str.Empty();
	//str.Format("%d",b_SAEStat);
	//WritePrivateProfileString(str_model,"STANDAE",str,str_ModelPath);

	//if(b_SAEStat == TRUE){
	//	((CEdit *)GetDlgItem(IDC_EDIT_SSHUTTER))->EnableWindow(1);
	//	((CEdit *)GetDlgItem(IDC_EDIT_SSENSOR))->EnableWindow(1);
	//	((CEdit *)GetDlgItem(IDC_EDIT_SISP))->EnableWindow(1);
	//	((CEdit *)GetDlgItem(IDC_EDIT_SRED))->EnableWindow(1);
	//	((CEdit *)GetDlgItem(IDC_EDIT_SBLUE))->EnableWindow(1);

	//	((CButton *)GetDlgItem(IDC_BUTTON_SAESAVE))->EnableWindow(1);
	//	((CButton *)GetDlgItem(IDC_BUTTON_SAETEST))->EnableWindow(1);
	//}else{
	//	((CEdit *)GetDlgItem(IDC_EDIT_SSHUTTER))->EnableWindow(0);
	//	((CEdit *)GetDlgItem(IDC_EDIT_SSENSOR))->EnableWindow(0);
	//	((CEdit *)GetDlgItem(IDC_EDIT_SISP))->EnableWindow(0);
	//	((CEdit *)GetDlgItem(IDC_EDIT_SRED))->EnableWindow(0);
	//	((CEdit *)GetDlgItem(IDC_EDIT_SBLUE))->EnableWindow(0);

	//	((CButton *)GetDlgItem(IDC_BUTTON_SAESAVE))->EnableWindow(0);
	//	((CButton *)GetDlgItem(IDC_BUTTON_SAETEST))->EnableWindow(0);
	//}
	//UpdateData(FALSE);

}

void COption_SubStand::OnBnClickedCheckLaeoff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	/*UpdateData(TRUE);
	CString str = "";
	((CImageTesterDlg  *)m_pMomWnd)->m_LAEStat = b_LAEStat;
	str.Empty();
	str.Format("%d",b_LAEStat);
	WritePrivateProfileString(str_model,"LOWLIGHTAE",str,str_ModelPath);

	if(b_LAEStat == TRUE){
		((CEdit *)GetDlgItem(IDC_EDIT_LSHUTTER))->EnableWindow(1);
		((CEdit *)GetDlgItem(IDC_EDIT_LSENSOR))->EnableWindow(1);
		((CEdit *)GetDlgItem(IDC_EDIT_LISP))->EnableWindow(1);
		((CEdit *)GetDlgItem(IDC_EDIT_LRED))->EnableWindow(1);
		((CEdit *)GetDlgItem(IDC_EDIT_LBLUE))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BUTTON_LAESAVE))->EnableWindow(1);
		((CButton *)GetDlgItem(IDC_BUTTON_LAETEST))->EnableWindow(1);
	}else{
		((CEdit *)GetDlgItem(IDC_EDIT_LSHUTTER))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_LSENSOR))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_LISP))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_LRED))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_LBLUE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_LAESAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_LAETEST))->EnableWindow(0);
	}
	UpdateData(FALSE);*/
}

void COption_SubStand::OnEnChangeEditSshutter()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	CString str;
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_SSHUTTER))->GetSel();
	str = StringMoc(str_sshutter);
	if(str_sshutter != str){
		str_sshutter = str;
	}
	DWORD DATA = StringHEX2DWORD(str_sshutter);
	if(DATA > 0xffff){
		str_sshutter = str_Oldsshutter; 
	}else{
		str_Oldsshutter = str_sshutter;
	}
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_SSHUTTER))->SetSel(SELCURSER,SELCURSER);
}

void COption_SubStand::OnEnChangeEditSsensor()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	CString str;
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_SSENSOR))->GetSel();
	str = StringMoc(str_ssensor);
	if(str_ssensor != str){
		str_ssensor = str;
	}
	DWORD DATA = StringHEX2DWORD(str_ssensor);
	if(DATA > 0xffff){
		str_ssensor = str_Oldssensor; 
	}else{
		str_Oldssensor = str_ssensor;
	}
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_SSENSOR))->SetSel(SELCURSER,SELCURSER);
}

void COption_SubStand::OnEnChangeEditSisp()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	CString str;
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_SISP))->GetSel();
	str = StringMoc(str_sISP);
	if(str_sISP != str){
		str_sISP = str;
	}
	DWORD DATA = StringHEX2DWORD(str_sISP);
	if(DATA > 0xffff){
		str_sISP = str_OldsISP; 
	}else{
		str_OldsISP = str_sISP;
	}
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_SISP))->SetSel(SELCURSER,SELCURSER);
}

void COption_SubStand::OnEnChangeEditSred()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	CString str;
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_SRED))->GetSel();
	str = StringMoc(str_sRED);
	if(str_sRED != str){
		str_sRED = str;
	}
	DWORD DATA = StringHEX2DWORD(str_sRED);
	if(DATA > 0xff){
		str_sRED = str_OldsRED; 
	}else{
		str_OldsRED = str_sRED;
	}
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_SRED))->SetSel(SELCURSER,SELCURSER);
}

void COption_SubStand::OnEnChangeEditSblue()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	CString str;
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_SBLUE))->GetSel();
	str = StringMoc(str_sBLUE);
	if(str_sBLUE != str){
		str_sBLUE = str;
	}
	DWORD DATA = StringHEX2DWORD(str_sBLUE);
	if(DATA > 0xff){
		str_sBLUE = str_OldsBLUE; 
	}else{
		str_OldsBLUE = str_sBLUE;
	}
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_SBLUE))->SetSel(SELCURSER,SELCURSER);
}

void COption_SubStand::OnEnChangeEditLshutter()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	CString str;
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_LSHUTTER))->GetSel();
	str = StringMoc(str_lshutter);
	if(str_lshutter != str){
		str_lshutter = str;
	}
	DWORD DATA = StringHEX2DWORD(str_lshutter);
	if(DATA >= 0xffff){
		str_lshutter = str_Oldlshutter; 
	}else{
		str_Oldlshutter = str_lshutter;
	}
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_LSHUTTER))->SetSel(SELCURSER,SELCURSER);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnEnChangeEditLsensor()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	CString str;
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_LSENSOR))->GetSel();
	str = StringMoc(str_lsensor);
	if(str_lsensor != str){
		str_lsensor = str;
	}
	DWORD DATA = StringHEX2DWORD(str_lsensor);
	if(DATA >= 0xffff){
		str_lsensor = str_Oldlsensor; 
	}else{
		str_Oldlsensor = str_lsensor;
	}
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_LSENSOR))->SetSel(SELCURSER,SELCURSER);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnEnChangeEditLisp()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	CString str;
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_LISP))->GetSel();
	str = StringMoc(str_lISP);
	if(str_lISP != str){
		str_lISP = str;
	}
	DWORD DATA = StringHEX2DWORD(str_lISP);
	if(DATA >= 0xffff){
		str_lISP = str_OldlISP; 
	}else{
		str_OldlISP = str_lISP;
	}
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_LISP))->SetSel(SELCURSER,SELCURSER);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnEnChangeEditLred()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	CString str;
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_LRED))->GetSel();
	str = StringMoc(str_lRED);
	if(str_lRED != str){
		str_lRED = str;
	}
	DWORD DATA = StringHEX2DWORD(str_lRED);
	if(DATA >= 0xff){
		str_lRED = str_OldlRED; 
	}else{
		str_OldlRED = str_lRED;
	}
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_LRED))->SetSel(SELCURSER,SELCURSER);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnEnChangeEditLblue()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	CString str;
	DWORD SELCURSER = ((CEdit *)GetDlgItem(IDC_EDIT_LBLUE))->GetSel();
	str = StringMoc(str_lBLUE);
	if(str_lBLUE != str){
		str_lBLUE = str;
	}
	DWORD DATA = StringHEX2DWORD(str_lBLUE);
	if(DATA >= 0xff){
		str_lBLUE = str_OldlBLUE; 
	}else{
		str_OldlBLUE = str_lBLUE;
	}
	UpdateData(FALSE);
	((CEdit *)GetDlgItem(IDC_EDIT_LBLUE))->SetSel(SELCURSER,SELCURSER);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnEnSetfocusEditSshutter()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	str_Oldsshutter = str_sshutter;
}

void COption_SubStand::OnEnSetfocusEditSsensor()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	str_Oldssensor = str_ssensor;
}

void COption_SubStand::OnEnSetfocusEditSisp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	str_OldsISP = str_sISP;
}

void COption_SubStand::OnEnSetfocusEditSred()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	str_OldsRED = str_sRED;
}

void COption_SubStand::OnEnSetfocusEditSblue()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	str_OldsBLUE = str_sBLUE;
}

void COption_SubStand::OnEnSetfocusEditLshutter()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	str_Oldlshutter = str_lshutter;
}

void COption_SubStand::OnEnSetfocusEditLsensor()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	str_Oldlsensor = str_lsensor;
}

void COption_SubStand::OnEnSetfocusEditLisp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	str_OldlISP = str_lISP;
}

void COption_SubStand::OnEnSetfocusEditLred()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	str_OldlRED = str_lRED;
}

void COption_SubStand::OnEnSetfocusEditLblue()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	str_OldlBLUE = str_lBLUE;
}

void COption_SubStand::OnBnClickedButtonSaetest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	/*str_sshutter.Format("0x%04X",m_SShut);
	str_ssensor.Format("0x%04X",m_SSensor);
	str_sISP.Format("0x%04X",m_SISP);
	str_sRED.Format("0x%02X",m_SRed);
	str_sBLUE.Format("0x%02X",m_SBlue);
	UpdateData(FALSE);
	((CImageTesterDlg *)m_pMomWnd)->m_AWBSTATE = FALSE;
	((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();*/
}

void COption_SubStand::OnBnClickedButtonLaetest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	/*str_lshutter.Format("0x%04X",m_LShut);
	str_lsensor.Format("0x%04X",m_LSensor);
	str_lISP.Format("0x%04X",m_LISP);
	str_lRED.Format("0x%02X",m_LRed);
	str_lBLUE.Format("0x%02X",m_LBlue);
	UpdateData(FALSE);
	((CImageTesterDlg *)m_pMomWnd)->m_AWBSTATE = FALSE;
	((CImageTesterDlg *)m_pMomWnd)->REG_LOWLIGHT_SETUP();*/
}

void COption_SubStand::OnBnClickedButtonSaesave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	/*UpdateData(TRUE);
	m_SShut = (unsigned int)(StringHEX2DWORD(str_sshutter));
	((CImageTesterDlg  *)m_pMomWnd)->m_SShut = m_SShut;
	m_SSensor = (unsigned int)(StringHEX2DWORD(str_ssensor));
	((CImageTesterDlg  *)m_pMomWnd)->m_SSensor = m_SSensor;
	m_SISP = (unsigned int)(StringHEX2DWORD(str_sISP));
	((CImageTesterDlg  *)m_pMomWnd)->m_SISP = m_SISP;
	m_SRed = (BYTE)(StringHEX2DWORD(str_sRED));
	((CImageTesterDlg  *)m_pMomWnd)->m_SRed = m_SRed;
	m_SBlue = (BYTE)(StringHEX2DWORD(str_sBLUE));
	((CImageTesterDlg  *)m_pMomWnd)->m_SBlue = m_SBlue;

	CString str = "";
	str.Empty();
	str.Format("%d",m_SShut);
	WritePrivateProfileString(str_model,"STANDSHUT",str,str_ModelPath);
	str.Empty();
	str.Format("%d",m_SSensor);
	WritePrivateProfileString(str_model,"STANDSENSOR",str,str_ModelPath);
	str.Empty();
	str.Format("%d",m_SISP);
	WritePrivateProfileString(str_model,"STANDISP",str,str_ModelPath);
	str.Empty();
	str.Format("%d",m_SRed);
	WritePrivateProfileString(str_model,"STANDRED",str,str_ModelPath);
	str.Empty();
	str.Format("%d",m_SBlue);
	WritePrivateProfileString(str_model,"STANDBLUE",str,str_ModelPath);


	str_sshutter.Format("0x%04X",m_SShut);
	str_ssensor.Format("0x%04X",m_SSensor);
	str_sISP.Format("0x%04X",m_SISP);
	str_sRED.Format("0x%02X",m_SRed);
	str_sBLUE.Format("0x%02X",m_SBlue);
	UpdateData(FALSE);*/
}

void COption_SubStand::OnBnClickedButtonLaesave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	/*UpdateData(TRUE);
	m_LShut = (unsigned int)(StringHEX2DWORD(str_lshutter));
	((CImageTesterDlg  *)m_pMomWnd)->m_LShut = m_LShut;
	m_LSensor = (unsigned int)(StringHEX2DWORD(str_lsensor));
	((CImageTesterDlg  *)m_pMomWnd)->m_LSensor = m_LSensor;
	m_LISP = (unsigned int)(StringHEX2DWORD(str_lISP));
	((CImageTesterDlg  *)m_pMomWnd)->m_LISP = m_LISP;
	m_LRed = (BYTE)(StringHEX2DWORD(str_lRED));
	((CImageTesterDlg  *)m_pMomWnd)->m_LRed = m_LRed;
	m_LBlue = (BYTE)(StringHEX2DWORD(str_lBLUE));
	((CImageTesterDlg  *)m_pMomWnd)->m_LBlue = m_LBlue;

	CString str = "";
	str.Empty();
	str.Format("%d",m_LShut);
	WritePrivateProfileString(str_model,"LOWLIGHTSHUT",str,str_ModelPath);
	str.Empty();
	str.Format("%d",m_LSensor);
	WritePrivateProfileString(str_model,"LOWLIGHTSENSOR",str,str_ModelPath);
	str.Empty();
	str.Format("%d",m_LISP);
	WritePrivateProfileString(str_model,"LOWLIGHTISP",str,str_ModelPath);
	str.Empty();
	str.Format("%d",m_LRed);
	WritePrivateProfileString(str_model,"LOWLIGHTRED",str,str_ModelPath);
	str.Empty();
	str.Format("%d",m_LBlue);
	WritePrivateProfileString(str_model,"LOWLIGHTBLUE",str,str_ModelPath);

	str_lshutter.Format("0x%04X",m_LShut);
	str_lsensor.Format("0x%04X",m_LSensor);
	str_lISP.Format("0x%04X",m_LISP);
	str_lRED.Format("0x%02X",m_LRed);
	str_lBLUE.Format("0x%02X",m_LBlue);
	UpdateData(FALSE);*/




}

void COption_SubStand::OnBnClickedButtonReset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void COption_SubStand::OnCbnSelchangeComboHutype()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int bufhutype = pHuTypeCombo->GetCurSel();
	if((BYTE)bufhutype != ((CImageTesterDlg  *)m_pMomWnd)->m_HuType)
	{
		((CButton *)GetDlgItem(IDC_BUTTON_HUTYPESAVE))->EnableWindow(1);
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_HUTYPESAVE))->EnableWindow(0);
	}
}

void COption_SubStand::OnBnClickedButtonHutypesave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
// 	int buf_HuType = pHuTypeCombo->GetCurSel();
// 	CString str;
// 	if((buf_HuType  >= 0)&&(buf_HuType  <= 0xff)){
// 		str.Empty();
// 		str.Format("%d",buf_HuType );
// 		WritePrivateProfileString(str_model,"Hutype",str,str_ModelPath);
// 	}
// 	((CImageTesterDlg  *)m_pMomWnd)->m_HuType = (BYTE)buf_HuType;
}


void COption_SubStand::OnBnClickedButtonProperty()
{
	((CImageTesterDlg *)m_pMomWnd)->CLGE_CamSet_ShowWindow();
}


void COption_SubStand::OnEnChangeEditLighton4()
{
	UpdateData(TRUE);
}


void COption_SubStand::OnBnClickedCheckTestskip()
{
	UpdateData(TRUE);
	CString str = "";
	((CImageTesterDlg  *)m_pMomWnd)->b_Chk_TestSkip = b_Chk_TestSkip;
	str.Empty();
	str.Format("%d", b_Chk_TestSkip);
	WritePrivateProfileString(str_model, "TEST_SKIP", str, str_ModelPath);
}


void COption_SubStand::OnBnClickedButtonRegisteropen()
{
	CString GetFilepath, GetFolderPath, GetListCount, GetFileName;
	CString DstFile = "";
	CString str = "";

	CFile file;
	CFileDialog dlg(TRUE, "set", "*.set", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_ALLOWMULTISELECT,
		"set 파일 (*.set)|*.set|모든 파일 (*.*)|*.*|");

	if (dlg.DoModal() == IDOK)
	{
		GetFileName = dlg.GetFileTitle(); //    load 할 파일 이름
		GetFilepath = dlg.GetPathName();  //	load 할 파일 이름이 있는 경로	


		DstFile = ((CImageTesterDlg *)m_pMomWnd)->modelFolder_SetFile_path + ("\\") + GetFileName + ".set";
		if (GetFilepath != DstFile){
			CopyFile(GetFilepath, DstFile, false);
		}
		str = dlg.GetFileTitle();
		FILE_PATH = str + ".set";
		((CImageTesterDlg *)m_pMomWnd)->SAVE_SETFILE = FILE_PATH;
		WritePrivateProfileString(str_model, "SAVE_FILE", ((CImageTesterDlg *)m_pMomWnd)->SAVE_SETFILE, str_ModelPath);


	}
	else
	{
		FILE_PATH = ((CImageTesterDlg *)m_pMomWnd)->SAVE_SETFILE;
	}

	UpdateData(FALSE);
}


void COption_SubStand::OnBnClickedButtonControl()
{
	((CImageTesterDlg *)m_pMomWnd)->CControl_ShowWindow();
}


void COption_SubStand::OnEnChangeEditOffset()
{
	UpdateData(TRUE);
}


void COption_SubStand::OnEnChangeEditOffset2()
{
	UpdateData(TRUE);
}


void COption_SubStand::OnEnChangeEditOffset3()
{
	UpdateData(TRUE);
}


void COption_SubStand::OnEnChangeEditOffset4()
{
	UpdateData(TRUE);
}


void COption_SubStand::OnEnChangeEditOffset5()
{
	UpdateData(TRUE);
}


void COption_SubStand::OnBnClickedCheckAlign()
{
	UpdateData(TRUE);
	CString str = "";
	((CImageTesterDlg  *)m_pMomWnd)->b_Chk_Align = b_Chk_Align;
	str.Empty();
	str.Format("%d", b_Chk_Align);
	WritePrivateProfileString(str_model, "USE_ALIGN", str, str_ModelPath);
	UpdateData(FALSE);
}


void COption_SubStand::OnBnClickedBtnAlign()
{
	UpdateData(TRUE);
	CString str;
	m_Align = atoi(str_Align);

	str.Empty();
	str.Format("%d", m_Align);
	WritePrivateProfileString(str_model, "Align", str, str_ModelPath);

	((CImageTesterDlg  *)m_pMomWnd)->m_Align = m_Align;
	
	str_Align.Format("%d", m_Align);
	UpdateData(FALSE);
}


void COption_SubStand::OnBnClickedButtonRegisteropen2()
{
	CString GetFilepath, GetFolderPath, GetListCount, GetFileName;
	CString DstFile = "";
	CString str = "";

	CFile file;
	CFileDialog dlg(TRUE, "set", "*.set", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_ALLOWMULTISELECT,
		"set 파일 (*.set)|*.set|모든 파일 (*.*)|*.*|");

	if (dlg.DoModal() == IDOK)
	{
		GetFileName = dlg.GetFileTitle(); //    load 할 파일 이름
		GetFilepath = dlg.GetPathName();  //	load 할 파일 이름이 있는 경로	


		DstFile = ((CImageTesterDlg *)m_pMomWnd)->modelFolder_SetFile_path + ("\\") + GetFileName + ".set";
		if (GetFilepath != DstFile){
			CopyFile(GetFilepath, DstFile, false);
		}
		str = dlg.GetFileTitle();
		FILE_PATH_SECOND = str + ".set";
		((CImageTesterDlg *)m_pMomWnd)->SAVE_SETFILE_Second = FILE_PATH_SECOND;
		WritePrivateProfileString(str_model, "SAVE_FILE_SECOND", ((CImageTesterDlg *)m_pMomWnd)->SAVE_SETFILE_Second, str_ModelPath);


	}
	else
	{
		FILE_PATH_SECOND = ((CImageTesterDlg *)m_pMomWnd)->SAVE_SETFILE_Second;
	}
	UpdateData(FALSE);
}


void COption_SubStand::OnBnClickedCheckRegisterchange()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (B_Check_RegisterChange)
	{
		B_Check_RegisterChange = FALSE;
	}
	else{
		B_Check_RegisterChange = TRUE;
	}
	Disable_RegisterChange(B_Check_RegisterChange);
	CString strData;

	strData.Format(_T("%d"), B_Check_RegisterChange);

	WritePrivateProfileString(str_model, "CHECK_RegisterChange", strData, str_ModelPath);
	

	((CImageTesterDlg *)m_pMomWnd)->mB_Check_RegisterChange = B_Check_RegisterChange;
	UpdateData(FALSE);
}

void COption_SubStand::Disable_RegisterChange(BOOL bMode){
	

	((CButton *)GetDlgItem(IDC_BUTTON_REGISTEROPEN2))->EnableWindow(bMode);
}

void COption_SubStand::OnBnClickedButtonReadSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	CString strData;

	DWORD wData = 0;
	wData = Edit_GetValue(IDC_EDIT_SLAVEID);
	g_stEEPROM.wSlave = wData;
	wData = Edit_GetValue(IDC_EDIT_OCX_ADD);
	g_stEEPROM.wOC_X_Addr = wData;
	wData = Edit_GetValue(IDC_EDIT_OCY_ADD);
	g_stEEPROM.wOC_Y_Addr = wData;
	wData = Edit_GetValue(IDC_EDIT_CHECKSUM_ADD);
	g_stEEPROM.wCheckSum_Addr = wData;
	wData = Edit_GetValue(IDC_EDIT_OCX_LEN,0);
	g_stEEPROM.wOC_X_Len = wData;
	wData = Edit_GetValue(IDC_EDIT_OCY_LEN, 0);
	g_stEEPROM.wOC_Y_Len = wData;
	wData = Edit_GetValue(IDC_EDIT_CHECKSUM_LEN, 0);
	g_stEEPROM.wCheckSum_Len = wData;
	UINT nData = 0;

	nData = Edit_GetValue(IDC_EDIT_OCX_MIN, 0);
	g_stEEPROM.nOC_X_Min = nData;
	nData = Edit_GetValue(IDC_EDIT_OCX_MAX, 0);
	g_stEEPROM.nOC_X_Max = nData;

	nData = Edit_GetValue(IDC_EDIT_OCY_MIN, 0);
	g_stEEPROM.nOC_Y_Min = nData;
	nData = Edit_GetValue(IDC_EDIT_OCY_MAX, 0);
	g_stEEPROM.nOC_Y_Max = nData;

	g_stEEPROM.bReadCheck = B_Check_Read;
	g_stEEPROM.bCheckSumCheck = B_Check_Checksum;
	g_stEEPROM.bZeroCheck = B_CHECK_ZERO;

	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bCheckSumCheck = g_stEEPROM.bCheckSumCheck;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bReadCheck = g_stEEPROM.bReadCheck;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.bZeroCheck = g_stEEPROM.bZeroCheck;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.nOC_X_Max = g_stEEPROM.nOC_X_Max;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.nOC_X_Min = g_stEEPROM.nOC_X_Min;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.nOC_Y_Max = g_stEEPROM.nOC_Y_Max;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.nOC_Y_Min = g_stEEPROM.nOC_Y_Min;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.wCheckSum_Addr = g_stEEPROM.wCheckSum_Addr;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.wCheckSum_Len = g_stEEPROM.wCheckSum_Len;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.wOC_X_Addr = g_stEEPROM.wOC_X_Addr;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.wOC_Y_Addr = g_stEEPROM.wOC_Y_Addr;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.wOC_X_Len = g_stEEPROM.wOC_X_Len;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.wOC_Y_Len = g_stEEPROM.wOC_Y_Len;
	((CImageTesterDlg *)m_pMomWnd)->m_stEEPROM.wSlave = g_stEEPROM.wSlave;

	Save_parameter_EEPROM();
}

UINT COption_SubStand::Edit_GetValue(UINT nID, BOOL bHex /*= TRUE*/){
	CString text;
	((CEdit *)GetDlgItem(nID))->GetWindowText(text);

	//hex
	unsigned int n = 0;
	if (bHex)
	{
		if (text.Find(_T("0x")) == 0)
			_stscanf_s(text.GetBuffer(0) + 2, _T("%x"), &n);
		else
			_stscanf_s(text.GetBuffer(0), _T("%x"), &n);
	}
	else{
		_stscanf_s(text.GetBuffer(0), _T("%u"), &n);
	}
	
	return n;
	
}

void COption_SubStand::Edit_SetValue(UINT nID, DWORD wData, BOOL bHex /*= TRUE*/){
	CString text;
	if (bHex)
	{
		text.Format(_T("0x%04x"), wData);
	}
	else{
		text.Format(_T("%d"), wData);

	}
	((CEdit *)GetDlgItem(nID))->SetWindowText(text);

}

void COption_SubStand::Save_parameter_EEPROM(){

	CString strData;

	strData.Format(_T("%d"), g_stEEPROM.wSlave);
	WritePrivateProfileString(str_model, "EEPROM_SLAVEID", strData, str_ModelPath);
	strData.Format(_T("%d"), g_stEEPROM.wOC_X_Addr);
	WritePrivateProfileString(str_model, "EEPROM_OC_X_Addr", strData, str_ModelPath);
	strData.Format(_T("%d"), g_stEEPROM.wOC_Y_Addr);
	WritePrivateProfileString(str_model, "EEPROM_OC_Y_Addr", strData, str_ModelPath);
	strData.Format(_T("%d"), g_stEEPROM.wCheckSum_Addr);
	WritePrivateProfileString(str_model, "EEPROM_CheckSum_Addr", strData, str_ModelPath);
	strData.Format(_T("%d"), g_stEEPROM.wOC_X_Len);
	WritePrivateProfileString(str_model, "EEPROM_OC_X_Len", strData, str_ModelPath);
	strData.Format(_T("%d"), g_stEEPROM.wOC_Y_Len);
	WritePrivateProfileString(str_model, "EEPROM_OC_Y_Len", strData, str_ModelPath);
	strData.Format(_T("%d"), g_stEEPROM.wCheckSum_Len);
	WritePrivateProfileString(str_model, "EEPROM_CheckSum_Len", strData, str_ModelPath);
	strData.Format(_T("%d"), g_stEEPROM.bReadCheck);
	WritePrivateProfileString(str_model, "EEPROM_ReadCheck", strData, str_ModelPath);
	strData.Format(_T("%d"), g_stEEPROM.bCheckSumCheck);
	WritePrivateProfileString(str_model, "EEPROM_CheckSumCheck", strData, str_ModelPath);

	strData.Format(_T("%d"), g_stEEPROM.bZeroCheck);
	WritePrivateProfileString(str_model, "EEPROM_ZeroCheck", strData, str_ModelPath);


	strData.Format(_T("%d"), g_stEEPROM.nOC_X_Min);
	WritePrivateProfileString(str_model, "EEPROM_OC_X_Min", strData, str_ModelPath);

	strData.Format(_T("%d"), g_stEEPROM.nOC_X_Max);
	WritePrivateProfileString(str_model, "EEPROM_OC_X_Max", strData, str_ModelPath);

	strData.Format(_T("%d"), g_stEEPROM.nOC_Y_Min);
	WritePrivateProfileString(str_model, "EEPROM_OC_Y_Min", strData, str_ModelPath);

	strData.Format(_T("%d"), g_stEEPROM.nOC_Y_Max);
	WritePrivateProfileString(str_model, "EEPROM_OC_Y_Max", strData, str_ModelPath);



}

void COption_SubStand::OnBnClickedCheckRead()
{
	if (B_Check_Read)
	{
		B_Check_Read = FALSE;
	}
	else{
		B_Check_Read = TRUE;

	}
	UpdateData(FALSE);
}


void COption_SubStand::OnBnClickedCheckChecksum()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (B_Check_Checksum)
	{
		B_Check_Checksum = FALSE;
	}
	else{
		B_Check_Checksum = TRUE;

	}
	UpdateData(FALSE);
}


void COption_SubStand::OnEnChangeEditMinamp2()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void COption_SubStand::OnEnChangeEditMaxamp2()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void COption_SubStand::OnEnChangeEditMinamp3()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void COption_SubStand::OnEnChangeEditMaxamp3()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void COption_SubStand::OnEnChangeEditMinamp4()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void COption_SubStand::OnEnChangeEditMaxamp4()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void COption_SubStand::OnEnChangeEditMinamp5()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void COption_SubStand::OnEnChangeEditMaxamp5()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void COption_SubStand::OnBnClickedCheckZero()
{
	if (B_CHECK_ZERO)
	{
		B_CHECK_ZERO = FALSE;
	}
	else{
		B_CHECK_ZERO = TRUE;

	}
	UpdateData(FALSE);
}


void COption_SubStand::OnBnClickedBtnModuleSSave()
{
	m_dSModuleDistance = GetDlgItemInt(IDC_EDIT_MODULE_SHUTTER);

	CString str = "";
	str.Empty();
	str.Format("%d", m_dSModuleDistance);
	((CImageTesterDlg  *)m_pMomWnd)->m_dSModuleDistance = m_dSModuleDistance;

	/*if(((CImageTesterDlg  *)m_pMomWnd)->m_pDistortionOptWnd != NULL){
	((CImageTesterDlg  *)m_pMomWnd)->m_pDistortionOptWnd->i_MotorDis = m_dChartDistance;
	((CImageTesterDlg  *)m_pMomWnd)->m_pDistortionOptWnd->str_MotorDis.Format("%d",m_dChartDistance);
	((CImageTesterDlg  *)m_pMomWnd)->m_pDistortionOptWnd->UpdateData(FALSE);
	}*/

	WritePrivateProfileString(str_model, "MODULES_DISTANCE_A", str, str_ModelPath);
	((CButton *)GetDlgItem(IDC_BTN_MODULE_S_SAVE))->EnableWindow(0);
}


void COption_SubStand::OnBnClickedBtnModulesTestmove()
{
	TESTMOVEBTN_Enable(0);

	CString str = "";
	double buf_data;
	GetDlgItemText(IDC_EDIT_MODULE_SHUTTER, str);
	buf_data = atof(str) + 0.05;
	int m_tstdata = (int)(buf_data * 10);

	if (m_tstdata <= 0){
		m_tstdata = 0;
	}

	if (m_tstdata >= 32539){
		m_tstdata = 32539;
	}

	((CImageTesterDlg  *)m_pMomWnd)->Move_ModuleS(m_tstdata);
	TESTMOVEBTN_Enable(1);
}


void COption_SubStand::OnBnClickedBtnChartMoveSave2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	double buf_data;
	GetDlgItemText(IDC_EDIT_CHART_OFFSET, str);
	int Offset = _ttoi(str);

	if (Offset < 0)
	{
		Offset = 0;
	}

	if (Offset > 800)
	{
		Offset = 800;
	}
		
	((CImageTesterDlg  *)m_pMomWnd)->m_nRoffset = Offset;
	str.Format(_T("%d"), Offset);
	SetDlgItemText(IDC_EDIT_CHART_OFFSET, str);
	((CImageTesterDlg  *)m_pMomWnd)->SaveMotorParameter();
}


void COption_SubStand::OnBnClickedCheckAreaSensor()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	UpdateData(TRUE);
	CString str = "";
	((CImageTesterDlg  *)m_pMomWnd)->b_Chk_TestSkip = b_Chk_AreaSensor;
	str.Empty();
	str.Format("%d", b_Chk_AreaSensor);
	WritePrivateProfileString(str_model, "AREA_SENSOR", str, str_ModelPath);
}
