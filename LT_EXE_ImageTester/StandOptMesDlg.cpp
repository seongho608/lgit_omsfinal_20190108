// StandOptMesDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "StandOptMesDlg.h"

#include "Option_ResultWStand.h"


// StandOptMesDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CStandOptMesDlg, CDialog)

CStandOptMesDlg::CStandOptMesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CStandOptMesDlg::IDD, pParent)
	, REPORT_PATH(_T(""))
{
	m_pMomWnd		= NULL;
	m_bUseSocket	= FALSE;
	m_szMachineCode = _T("");
	m_szIp			= _T("");
	m_szPort		= _T("");
	m_szMesLocation	= _T("");
}

CStandOptMesDlg::~CStandOptMesDlg()
{
}

void CStandOptMesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_UDP_IPADDRESS, m_ctrllp_IP);
	DDX_Text(pDX, IDC_LotSaveReport, REPORT_PATH);
	
	
	DDX_Check(pDX, IDC_CHECK_SOCKET, m_bUseSocket);

}


BEGIN_MESSAGE_MAP(CStandOptMesDlg, CDialog)
	ON_BN_CLICKED(IDC_CHECK_SOCKET, &CStandOptMesDlg::OnBnClickedCheckSocket)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_CODE, &CStandOptMesDlg::OnBnClickedButtonSaveCode)
	ON_BN_CLICKED(IDC_BUTTON_UDP_SAVE, &CStandOptMesDlg::OnBnClickedButtonUdpSave)
	ON_BN_CLICKED(IDC_BUTTON_MES_LOCATION_SAVE, &CStandOptMesDlg::OnBnClickedButtonMesLocationSave)
	ON_EN_CHANGE(IDC_UDP_RX_PORT_EDIT, &CStandOptMesDlg::OnEnChangeUdpRxPortEdit)
	ON_WM_SHOWWINDOW()
	ON_EN_CHANGE(IDC_EDIT_CODE, &CStandOptMesDlg::OnEnChangeEditCode)
	ON_BN_CLICKED(IDC_BUTTON_LOTID_SAVE, &CStandOptMesDlg::OnBnClickedButtonLotidSave)
	ON_EN_CHANGE(IDC_EDIT_LOTID, &CStandOptMesDlg::OnEnChangeEditLotid)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_LotReportPathSave, &CStandOptMesDlg::OnBnClickedLotreportpathsave)
	ON_EN_SETFOCUS(IDC_EDIT_MODE, &CStandOptMesDlg::OnEnSetfocusEditMode)
END_MESSAGE_MAP()

// StandOptMesDlg 메시지 처리기입니다.
void CStandOptMesDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CStandOptMesDlg::OnBnClickedCheckSocket()
{
	UpdateData(TRUE);
	CString str_buf = _T("");

	m_bUseSocket = ((CButton*)GetDlgItem(IDC_CHECK_SOCKET))->GetCheck();

	((CImageTesterDlg *)m_pMomWnd)->b_Chk_MESEn = m_bUseSocket;
	str_buf.Format(_T("%d"),m_bUseSocket);

	WritePrivateProfileString(_T("MESOPTION"),_T("MESEN"),str_buf,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
	if(m_bUseSocket == TRUE)
		((CImageTesterDlg *)m_pMomWnd)->SocketConnect();
	else
		((CImageTesterDlg *)m_pMomWnd)->SocketDisconnect();

	EnableWindow(m_bUseSocket);

	UpdateData(FALSE);
}

void CStandOptMesDlg::EnableWindow(BOOL bEnable)
{
	((CEdit*)GetDlgItem(IDC_EDIT_CODE))->EnableWindow(bEnable);
	((CIPAddressCtrl*)GetDlgItem(IDC_UDP_IPADDRESS))->EnableWindow(bEnable);
	((CEdit*)GetDlgItem(IDC_UDP_RX_PORT_EDIT))->EnableWindow(bEnable);
	((CEdit*)GetDlgItem(IDC_EDIT_MES_LOCATION))->EnableWindow(bEnable);
	((CEdit*)GetDlgItem(IDC_EDIT_LOTID))->EnableWindow(bEnable);
	((CButton*)GetDlgItem(IDC_BUTTON_SAVE_CODE))->EnableWindow(bEnable);
	((CButton*)GetDlgItem(IDC_BUTTON_UDP_SAVE))->EnableWindow(bEnable);
	((CButton*)GetDlgItem(IDC_BUTTON_MES_LOCATION_SAVE))->EnableWindow(bEnable);
	((CButton*)GetDlgItem(IDC_BUTTON_LOTID_SAVE))->EnableWindow(bEnable);
	
	
	
	if(bEnable == 1){
		MODE_EditView("MES MODE");
	}else{
		MODE_EditView("LOT MODE");		
	}
	
	((CStatic*)GetDlgItem(IDC_STATIC_LOT))->EnableWindow(1-bEnable);
	((CStatic*)GetDlgItem(IDC_STATIC_REPORT))->EnableWindow(1-bEnable);

	((CEdit*)GetDlgItem(IDC_LotSaveReport))->EnableWindow(1-bEnable);
	((CButton*)GetDlgItem(IDC_LotReportPathSave))->EnableWindow(1-bEnable);
	
	InitEVMS();

}

void CStandOptMesDlg::OnBnClickedButtonSaveCode()
{
	UpdateData(TRUE);

	((CEdit*)GetDlgItem(IDC_EDIT_CODE))->GetWindowText(m_szMachineCode);
	((CImageTesterDlg *)m_pMomWnd)->m_szCode = m_szMachineCode;
	WritePrivateProfileString(_T("MESOPTION"),_T("MCODE"),m_szMachineCode,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
	((CButton *)GetDlgItem(IDC_BUTTON_SAVE_CODE))->EnableWindow(0);
	UpdateData(FALSE);
}

void CStandOptMesDlg::OnBnClickedButtonUdpSave()
{
	//// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str_buf = _T("");
	((CImageTesterDlg *)m_pMomWnd)->SocketDisconnect();
	BYTE szField0,szField1,szField2,szField3;
	m_ctrllp_IP.GetAddress(szField0,szField1,szField2,szField3);
	m_szIp.Format(_T("%d.%d.%d.%d"),szField0,szField1,szField2,szField3);
	m_ctrllp_IP.SetAddress(htonl(inet_addr(m_szIp)));
	WritePrivateProfileString(_T("MESOPTION"),_T("IP_Addr"),m_szIp,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
	m_socket = GetDlgItemInt(IDC_UDP_RX_PORT_EDIT);	
	if(m_socket < 0)
		m_socket = 0;	
	
	if(m_socket > 9999)
		m_socket = 9999;	
	
	((CImageTesterDlg *)m_pMomWnd)->m_szPort.Format(_T("%d"),m_socket);
	WritePrivateProfileString(_T("MESOPTION"),_T("IP_Port"),((CImageTesterDlg *)m_pMomWnd)->m_szPort,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
	((CImageTesterDlg *)m_pMomWnd)->m_szIp = m_szIp;
	((CImageTesterDlg *)m_pMomWnd)->m_socket = m_socket;
	((CImageTesterDlg *)m_pMomWnd)->m_pResWorkerOptWnd->MESIP_VIEW(m_szIp);
	((CImageTesterDlg *)m_pMomWnd)->m_pResWorkerOptWnd->MESPORT_VIEW(((CImageTesterDlg *)m_pMomWnd)->m_szPort);
	DoEvents(500);
	((CImageTesterDlg *)m_pMomWnd)->SocketConnect();
}

void CStandOptMesDlg::OnBnClickedButtonMesLocationSave()
{
	ITEMIDLIST        *pidlBrowse;
	char    SaveFolderPath[2046] = {0,}; 
	BROWSEINFO BrInfo;
	BrInfo.hwndOwner = NULL; 
	BrInfo.pidlRoot = NULL;
	memset( &BrInfo, 0, sizeof(BrInfo) );
	BrInfo.pszDisplayName = SaveFolderPath;
	BrInfo.lpszTitle = _T("저장할 디렉토리를 선택하세요");
	BrInfo.ulFlags = BIF_RETURNONLYFSDIRS;

	// Show Dialog 
	pidlBrowse = ::SHBrowseForFolder(&BrInfo);    

	if( pidlBrowse != NULL)
	{
		// Get Path
		::SHGetPathFromIDList(pidlBrowse, SaveFolderPath);   
		m_szMesLocation = SaveFolderPath;
		((CImageTesterDlg *)m_pMomWnd)->MESPath =SaveFolderPath;

		((CEdit*)GetDlgItem(IDC_EDIT_MES_LOCATION))->SetWindowText(m_szMesLocation);

		WritePrivateProfileString(_T("MES_BMS_SAVE_FOLDER_PATH"),NULL,_T(""),((CImageTesterDlg *)m_pMomWnd)->m_inifilename);	
		WritePrivateProfileString(_T("MES_BMS_SAVE_FOLDER_PATH"),_T("MES_BMS_SAVE_FOL"),SaveFolderPath,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
		UpdateData(FALSE);
		UpdateData(TRUE);
	}
}

BOOL CStandOptMesDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	Font_Size_Change(IDC_EDIT_MODE,&ft,40,23);
	m_bUseSocket = ((CImageTesterDlg *)m_pMomWnd)->b_Chk_MESEn;

	if(m_bUseSocket == 1){
		MODE_EditView("MES MODE");
	}else{
		MODE_EditView("LOT MODE");		
	}

	m_szIp = ((CImageTesterDlg *)m_pMomWnd)->m_szIp;
	m_socket = ((CImageTesterDlg *)m_pMomWnd)->m_socket;
	m_szMachineCode = ((CImageTesterDlg *)m_pMomWnd)->m_szCode;
	m_ctrllp_IP.SetAddress(htonl(inet_addr(m_szIp)));

	REPORT_PATH =  ((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_FOLDERPATH;

	InitEVMS();

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
}

void CStandOptMesDlg::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		
		((CButton*)GetDlgItem(IDC_CHECK_SOCKET))->EnableWindow(0);

		((CEdit*)GetDlgItem(IDC_EDIT_CODE))->EnableWindow(0);
		((CIPAddressCtrl*)GetDlgItem(IDC_UDP_IPADDRESS))->EnableWindow(0);
		((CEdit*)GetDlgItem(IDC_UDP_RX_PORT_EDIT))->EnableWindow(0);
		((CEdit*)GetDlgItem(IDC_EDIT_MES_LOCATION))->EnableWindow(0);
		((CEdit*)GetDlgItem(IDC_EDIT_LOTID))->EnableWindow(0);
		((CButton*)GetDlgItem(IDC_BUTTON_SAVE_CODE))->EnableWindow(0);
		((CButton*)GetDlgItem(IDC_BUTTON_UDP_SAVE))->EnableWindow(0);
		((CButton*)GetDlgItem(IDC_BUTTON_MES_LOCATION_SAVE))->EnableWindow(0);
		((CButton*)GetDlgItem(IDC_BUTTON_LOTID_SAVE))->EnableWindow(0);

		((CEdit*)GetDlgItem(IDC_LotSaveReport))->EnableWindow(0);
		((CButton*)GetDlgItem(IDC_LotReportPathSave))->EnableWindow(0);

//		pModelCombo->EnableWindow(0);
//		((CButton *)GetDlgItem(IDC_BTN_MODELSAVE))->EnableWindow(0);
//		((CEdit *)GetDlgItem(IDC_MODELNAME))->EnableWindow(0);
	}
}

void CStandOptMesDlg::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
void CStandOptMesDlg::OnEnChangeUdpRxPortEdit()
{
	m_socket = GetDlgItemInt(IDC_UDP_RX_PORT_EDIT);	
	if(m_socket > 9999)
	{
		m_socket = 9999;	
		SetDlgItemInt(IDC_UDP_RX_PORT_EDIT,m_socket,0);
	}
}

void CStandOptMesDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if(bShow)
	{
		m_bUseSocket = ((CImageTesterDlg *)m_pMomWnd)->b_Chk_MESEn;

		m_szIp = ((CImageTesterDlg *)m_pMomWnd)->m_szIp;
		m_socket = ((CImageTesterDlg *)m_pMomWnd)->m_socket;
		m_szPort.Format(_T("%d"), m_socket);
		m_szMachineCode = ((CImageTesterDlg *)m_pMomWnd)->m_szCode;
		m_szMesLocation = ((CImageTesterDlg *)m_pMomWnd)->MESPath;
		m_szModelID     = ((CImageTesterDlg *)m_pMomWnd)->m_str_ModelID;

		m_ctrllp_IP.SetAddress(htonl(inet_addr(m_szIp)));

		((CEdit*)GetDlgItem(IDC_BUTTON_LOTID_SAVE))->EnableWindow(FALSE);

		InitUI();
	}
}

void CStandOptMesDlg::InitUI()
{
	((CButton*)GetDlgItem(IDC_CHECK_SOCKET))->SetCheck(m_bUseSocket);
	EnableWindow(m_bUseSocket);

	((CEdit*)GetDlgItem(IDC_EDIT_CODE))->SetWindowText(m_szMachineCode);
	((CIPAddressCtrl*)GetDlgItem(IDC_UDP_IPADDRESS))->SetAddress(htonl(inet_addr(m_szIp)));
	((CEdit*)GetDlgItem(IDC_UDP_RX_PORT_EDIT))->SetWindowText(m_szPort);
	((CEdit*)GetDlgItem(IDC_EDIT_MES_LOCATION))->SetWindowText(m_szMesLocation);
	((CEdit*)GetDlgItem(IDC_EDIT_LOTID))->SetWindowText(m_szModelID);
	UpdateData(FALSE);
}

void CStandOptMesDlg::OnEnChangeEditCode()
{
	UpdateData(TRUE);
	
	((CEdit*)GetDlgItem(IDC_EDIT_CODE))->GetWindowText(m_szMachineCode);
	if(((CImageTesterDlg *)m_pMomWnd)->m_szCode != m_szMachineCode)
		((CButton *)GetDlgItem(IDC_BUTTON_SAVE_CODE))->EnableWindow(TRUE);
	else
		((CButton *)GetDlgItem(IDC_BUTTON_SAVE_CODE))->EnableWindow(FALSE);
}

void CStandOptMesDlg::OnBnClickedButtonLotidSave()
{
	UpdateData(TRUE);
	
	CString str;
	CEdit*		p = (CEdit*)GetDlgItem(IDC_EDIT_LOTID);
	p->GetWindowText(str);

	CString modelfile = ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	((CImageTesterDlg *)m_pMomWnd)->m_str_ModelID = str;
	WritePrivateProfileString(_T("MESINIT"),_T("ModelId"),str,modelfile);

	CButton*	pBtn = (CButton*)GetDlgItem(IDC_BUTTON_LOTID_SAVE);
	pBtn->EnableWindow(FALSE);
}

void CStandOptMesDlg::OnEnChangeEditLotid()
{
	UpdateData(TRUE);

	CEdit*		p = (CEdit*)GetDlgItem(IDC_EDIT_LOTID);
	CButton*	pBtn = (CButton*)GetDlgItem(IDC_BUTTON_LOTID_SAVE);

	CString str;
	p->GetWindowText(str);
	
	if(((CImageTesterDlg *)m_pMomWnd)->m_str_ModelID.Compare(str))
		pBtn->EnableWindow(TRUE);
	else
		pBtn->EnableWindow(FALSE);
	
}

void CStandOptMesDlg::MODE_EditView(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_MODE))->SetWindowText(lpcszString);
	
}
HBRUSH CStandOptMesDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_MODE){
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(86,86,86));
	}
	return hbr;
}

void CStandOptMesDlg::OnBnClickedLotreportpathsave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	ITEMIDLIST        *pidlBrowse;
	char    SaveFolderPath[2046] = {0,}; 
	BROWSEINFO BrInfo;
	BrInfo.hwndOwner = NULL; 
	BrInfo.pidlRoot = NULL;
	memset( &BrInfo, 0, sizeof(BrInfo) );
	BrInfo.pszDisplayName = SaveFolderPath;
	BrInfo.lpszTitle = "저장할 디렉토리를 선택하세요";
	BrInfo.ulFlags = BIF_RETURNONLYFSDIRS;
	
	// Show Dialog
	 pidlBrowse = ::SHBrowseForFolder(&BrInfo);    

	 if( pidlBrowse != NULL)
	 {
	//   Get Path
		::SHGetPathFromIDList(pidlBrowse, SaveFolderPath);   
		REPORT_PATH = SaveFolderPath;
		((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_FOLDERPATH =SaveFolderPath;

		//----LOT 폴더 안 폴더 생성 해줘야함. 날짜
		CString strDate="";
		strDate.Format("%d_%02d_%02d",((CImageTesterDlg *)m_pMomWnd)->YEAR,((CImageTesterDlg *)m_pMomWnd)->MONTH,((CImageTesterDlg *)m_pMomWnd)->DAY);
		((CImageTesterDlg *)m_pMomWnd)->LOT_CREATE_Folder(strDate,1);
		((CImageTesterDlg *)m_pMomWnd)->LOT_CREATE_Folder(((CImageTesterDlg *)m_pMomWnd)->m_modelName,2);



		WritePrivateProfileString("LOT_SAVE_FOLDER_PATH","LOT_SAVE_FOL",SaveFolderPath,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
		
		UpdateData(FALSE);
		UpdateData(TRUE);
	 }
}

void CStandOptMesDlg::OnEnSetfocusEditMode()
{
	GotoDlgCtrl(((CButton *)GetDlgItem(IDC_CHECK_SOCKET)));

}
