// Option_FixedPattern.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Option_FixedPattern.h"
#include "afxdialogex.h"


// COption_FixedPattern 대화 상자입니다.

extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];
extern WORD	m_GRAYScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];
IMPLEMENT_DYNAMIC(COption_FixedPattern, CDialog)

COption_FixedPattern::COption_FixedPattern(CWnd* pParent /*=NULL*/)
	: CDialog(COption_FixedPattern::IDD, pParent)
	, ThrNoise(0)
	, ThrDark(0)
{
	iSavedItem = 0;
	iSavedSubitem = 0;
	changecount = 0;
	iSavedItem_click = 0;
	for (int t = 0; t<3; t++){
		ChangeItem[t] = -1;
	}

	m_b_StopFail = FALSE;
}

COption_FixedPattern::~COption_FixedPattern()
{
}

void COption_FixedPattern::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_FIXEDPATTERN, m_FixedPatternList);
	DDX_Control(pDX, IDC_LIST_FIXEDPATTERN_LOT, m_Lot_FixedPatternList);
	DDX_Control(pDX, IDC_LIST_RECT, m_RectList);
	DDX_Text(pDX, IDC_Thresold_V, ThrNoise);
	DDX_Text(pDX, IDC_Thresold_Y, ThrDark);
}


BEGIN_MESSAGE_MAP(COption_FixedPattern, CDialog)
	ON_WM_CTLCOLOR()
	ON_WM_MOUSEWHEEL()
	ON_WM_SHOWWINDOW()
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_LIST_RECT, &COption_FixedPattern::OnNMClickListRect)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_RECT, &COption_FixedPattern::OnNMCustomdrawListRect)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_RECT, &COption_FixedPattern::OnNMDblclkListRect)
	ON_EN_KILLFOCUS(IDC_EDIT_RECTLIST, &COption_FixedPattern::OnEnKillfocusEditRectlist)
	ON_EN_SETFOCUS(IDC_EDIT_RECTLIST, &COption_FixedPattern::OnEnSetfocusEditRectlist)
	ON_BN_CLICKED(IDC_BUTTON_DEFAULT, &COption_FixedPattern::OnBnClickedButtonDefault)
	ON_BN_CLICKED(IDC_BUTTON_LOAD, &COption_FixedPattern::OnBnClickedButtonLoad)
	ON_BN_CLICKED(IDC_BUTTON_LIST_SAVE, &COption_FixedPattern::OnBnClickedButtonListSave)
	ON_BN_CLICKED(IDC_BUTTON_FPN_SAVE, &COption_FixedPattern::OnBnClickedButtonFpnSave)
	ON_EN_CHANGE(IDC_Thresold_V, &COption_FixedPattern::OnEnChangeThresoldV)
	ON_EN_CHANGE(IDC_Thresold_Y, &COption_FixedPattern::OnEnChangeThresoldY)
END_MESSAGE_MAP()


// COption_FixedPattern 메시지 처리기입니다.
void COption_FixedPattern::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd = IN_pMomWnd;

}

void COption_FixedPattern::Setup(CWnd* IN_pMomWnd, CEdit	*pTEDIT, tINFO INFO)
{
	m_pMomWnd = IN_pMomWnd;
	pTStat = pTEDIT;
	m_INFO = INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
	strTitle.Empty();
	strTitle = "FIXED PATTERN_";
}

BOOL COption_FixedPattern::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	InitPrm();
	Set_List(&m_FixedPatternList);
	LOT_Set_List(&m_Lot_FixedPatternList);

	((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->ShowWindow(FALSE);


	((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->ShowWindow(FALSE);

	Rect_SETLIST();
	Load_parameter();
	Rect_InsertList();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void COption_FixedPattern::InitPrm()
{
	Load_parameter();
	UpdateData(FALSE);
}
void COption_FixedPattern::Load_parameter(){
	CString str = "";
	CString strTitle2 = "";

	ThrDark = GetPrivateProfileInt("FIXEDPATTERN_OPT", "Dark", -1, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if (ThrDark == -1){
		ThrDark = 25;
		str.Empty();
		str.Format("%d", ThrDark);
		WritePrivateProfileString("FIXEDPATTERN_OPT", "Dark", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	}
	ThrNoise = GetPrivateProfileInt("FIXEDPATTERN_OPT", "Noise", -1, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	if (ThrNoise == -1){
		ThrNoise = 255;
		str.Empty();
		str.Format("%d", ThrNoise);
		WritePrivateProfileString("FIXEDPATTERN_OPT", "Noise", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	}

	for (int t = 0; t < 1; t++){
		if (t == 0){
			strTitle2.Empty();
			strTitle2 = "CAM_";
		}
		if (t == 1){
			strTitle2.Empty();
			strTitle2 = "SIDE_";
		}
		if (t == 2){
			strTitle2.Empty();
			strTitle2 = "CENTER_";
		}
		Test_RECT[t].m_PosX = GetPrivateProfileInt("FIXEDPATTERN_OPT", strTitle2 + "PosX", -1, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		if (Test_RECT[t].m_PosX == -1){
			for (int k = 0; k < 1; k++){
				Test_RECT[k].m_PosX = (CAM_IMAGE_WIDTH / 2);
				Test_RECT[k].m_PosY = (CAM_IMAGE_HEIGHT / 2);
				Test_RECT[k].m_Width = CAM_IMAGE_WIDTH -10;
				Test_RECT[k].m_Height = CAM_IMAGE_HEIGHT -10;
				Test_RECT[k].EX_RECT_SET(
					Test_RECT[k].m_PosX,
					Test_RECT[k].m_PosY,
					Test_RECT[k].m_Width,
					Test_RECT[k].m_Height);
				m_b_Ellipse[k] = 0;
			}
			Save_parameter();
		}
		Test_RECT[t].m_PosY = GetPrivateProfileInt("FIXEDPATTERN_OPT", strTitle2 + "PosY", -1, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		Test_RECT[t].m_Left = GetPrivateProfileInt("FIXEDPATTERN_OPT", strTitle2 + "StartX", -1, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		Test_RECT[t].m_Top = GetPrivateProfileInt("FIXEDPATTERN_OPT", strTitle2 + "StartY", -1, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		Test_RECT[t].m_Right = GetPrivateProfileInt("FIXEDPATTERN_OPT", strTitle2 + "ExitX", -1, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		Test_RECT[t].m_Bottom = GetPrivateProfileInt("FIXEDPATTERN_OPT", strTitle2 + "ExitY", -1, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		Test_RECT[t].m_Width = GetPrivateProfileInt("FIXEDPATTERN_OPT", strTitle2 + "Width", -1, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		Test_RECT[t].m_Height = GetPrivateProfileInt("FIXEDPATTERN_OPT", strTitle2 + "Height", -1, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

		m_b_Ellipse[t] = GetPrivateProfileDouble("FIXEDPATTERN_OPT", strTitle2 + "m_Ellipse", 0, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	}
	UpdateData(FALSE);
}
void COption_FixedPattern::Save_parameter(){
	CString str = "";
	CString strTitle2 = "";

	for (int t = 0; t<1; t++){

		if (t == 0){
			strTitle2.Empty();
			strTitle2 = "CAM_";
		}
		if (t == 1){
			strTitle2.Empty();
			strTitle2 = "SIDE_";
		}
		if (t == 2){
			strTitle2.Empty();
			strTitle2 = "CENTER_";
		}

		str.Empty();
		str.Format("%d", Test_RECT[t].m_PosX);
		WritePrivateProfileString("FIXEDPATTERN_OPT", strTitle2 + "PosX", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_PosY);
		WritePrivateProfileString("FIXEDPATTERN_OPT", strTitle2 + "PosY", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

		str.Empty();
		str.Format("%d", Test_RECT[t].m_Left);
		WritePrivateProfileString("FIXEDPATTERN_OPT", strTitle2 + "StartX", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Top);
		WritePrivateProfileString("FIXEDPATTERN_OPT", strTitle2 + "StartY", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Right);
		WritePrivateProfileString("FIXEDPATTERN_OPT", strTitle2 + "ExitX", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Bottom);
		WritePrivateProfileString("FIXEDPATTERN_OPT", strTitle2 + "ExitY", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Width);
		WritePrivateProfileString("FIXEDPATTERN_OPT", strTitle2 + "Width", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Height);
		WritePrivateProfileString("FIXEDPATTERN_OPT", strTitle2 + "Height", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

		str.Empty();
		str.Format("%d", m_b_Ellipse[t]);
		WritePrivateProfileString("FIXEDPATTERN_OPT", strTitle2 + "m_Ellipse", str, ((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	}
}
BOOL COption_FixedPattern::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}


HBRUSH COption_FixedPattern::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


BOOL COption_FixedPattern::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	int znum = zDelta / 120;
	CWnd *focusid;
	focusid = GetFocus();
	bool FLAG = 0;
	if (znum > 0){
		FLAG = 1;
	}
	else if (znum < 0){
		FLAG = 0;
	}
	else if (znum == 0){
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	}
	int casenum = focusid->GetDlgCtrlID();
	switch (casenum){
	case IDC_EDIT_RECTLIST:
		if (FLAG == 1){
			if ((Change_DATA_CHECK(1) == TRUE) /*|| (znum ==-1)*/){
				EditWheelIntSet(IDC_EDIT_RECTLIST, znum);
				Change_DATA();
				Rect_InsertList();
				OnEnSetfocusEditRectlist();
			}
		}
		if (FLAG == 0){
			if ((Change_DATA_CHECK(0) == TRUE) /*|| (znum == 1)*/){
				EditWheelIntSet(IDC_EDIT_RECTLIST, znum);
				Change_DATA();
				Rect_InsertList();
				OnEnSetfocusEditRectlist();
			}
		}
		break;
	}
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

void COption_FixedPattern::EditWheelIntSet(int nID, int Val)
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID, str_buf);
	str_buf.Remove(' ');
	if (str_buf == ""){
		buf = 0;
	}
	else{
		buf = GetDlgItemInt(nID);
		buf += Val;
	}
	SetDlgItemInt(nID, buf, 1);
	((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
}
void COption_FixedPattern::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if (bShow == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}
	else{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
		UpdateData(FALSE);
	}
}


void COption_FixedPattern::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	UINT BUF = 0;
	if (nIDEvent == 150){
		KillTimer(150);
		BUF = m_RectList.GetItemState(iSavedItem_click, LVIS_STATEIMAGEMASK);
		if (BUF == 0x2000){
			m_b_Ellipse[iSavedItem_click] = TRUE;
		}
		else if (BUF == 0x1000){
			m_b_Ellipse[iSavedItem_click] = FALSE;
		}
	}
	else if (nIDEvent == 160){
		KillTimer(160);
		BUF = m_RectList.GetItemState(iSavedItem_click, LVIS_STATEIMAGEMASK);
		if (BUF == 0x2000){
			m_b_Ellipse[iSavedItem_click] = TRUE;
		}
		else if (BUF == 0x1000){
			m_b_Ellipse[iSavedItem_click] = FALSE;
		}
	}
	CDialog::OnTimer(nIDEvent);
}
void COption_FixedPattern::Set_List(CListCtrl *List){

	CString str = "";
	while (List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0, "NO", LVCFMT_CENTER, 30);
	List->InsertColumn(1, "모델명", LVCFMT_CENTER, 100);
	//List->InsertColumn(2, "LOT CARD", LVCFMT_CENTER, 100);
	List->InsertColumn(2, "Start DATE", LVCFMT_CENTER, 80);
	List->InsertColumn(3, "Start TIME", LVCFMT_CENTER, 80);
	List->InsertColumn(4, "VAL", LVCFMT_CENTER, 80);
	List->InsertColumn(5, "result", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "비고", LVCFMT_CENTER, 80);

	ListItemNum = 7;
	Copy_List(&m_FixedPatternList, ((CImageTesterDlg *)m_pMomWnd)->m_pWorkList, ListItemNum);
}
void COption_FixedPattern::FAIL_UPLOAD(){
// 	if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 		if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
// 			LOT_InsertDataList();
// 			m_Lot_FixedPatternList.SetItemText(Lot_InsertIndex, 5, "FAIL");
// 			m_Lot_FixedPatternList.SetItemText(Lot_InsertIndex, 6, "STOP");
// 			Lot_StartCnt++;
// 		}
// 	}
// 	else{
//	if (((CImageTesterDlg *)m_pMomWnd)->LotMod == TRUE){
		InsertList();
		m_FixedPatternList.SetItemText(InsertIndex, 4, "X");
		m_FixedPatternList.SetItemText(InsertIndex, 5, "FAIL");
		m_FixedPatternList.SetItemText(InsertIndex, 6, "STOP");
		((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_FIXED_PATTERN, "STOP");
		StartCnt++;

		m_b_StopFail = TRUE;
//	}
}
bool COption_FixedPattern::EXCEL_UPLOAD(){
	m_FixedPatternList.DeleteAllItems();
	if (((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("Fixed_Pattern", &m_FixedPatternList, 1, &StartCnt) == TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
}
void COption_FixedPattern::EXCEL_SAVE()
{
	CString Item[1] = { "VAL" };
	CString Data = "";
	CString str = "";
	str = "*********************** Fixed_Pattern  [ 단위 : dB ] ***********************\n";
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("Fixed_Pattern", Item, 1, &m_FixedPatternList, Data);
}
void COption_FixedPattern::InsertList()
{
	CString strCnt;

	InsertIndex = m_FixedPatternList.InsertItem(StartCnt, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex + 1);
	m_FixedPatternList.SetItemText(InsertIndex, 0, strCnt);

	m_FixedPatternList.SetItemText(InsertIndex, 1, ((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_FixedPatternList.SetItemText(InsertIndex, 2, ((CImageTesterDlg *)m_pMomWnd)->strDay + "");
	m_FixedPatternList.SetItemText(InsertIndex, 3, ((CImageTesterDlg *)m_pMomWnd)->strTime + "");
}
void COption_FixedPattern::LOT_InsertDataList()
{
	CString strCnt = "";

	int Index = m_Lot_FixedPatternList.InsertItem(Lot_StartCnt, "", 0);
	//Lot_InsertIndex = m_Lot_FixedPatternList.InsertItem(Lot_StartCnt, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Lot_InsertIndex + 1);
	m_Lot_FixedPatternList.SetItemText(Lot_InsertIndex, 0, strCnt);

// 	m_Lot_FixedPatternList.SetItemText(Lot_InsertIndex, 1, ((CImageTesterDlg *)m_pMomWnd)->m_modelName);
// 	m_Lot_FixedPatternList.SetItemText(Lot_InsertIndex, 2, ((CImageTesterDlg  *)m_pMomWnd)->LOT_NUMBER);
// 	m_Lot_FixedPatternList.SetItemText(Lot_InsertIndex, 3, ((CImageTesterDlg *)m_pMomWnd)->strDay + "");
// 	m_Lot_FixedPatternList.SetItemText(Lot_InsertIndex, 4, ((CImageTesterDlg *)m_pMomWnd)->strTime + "");

	int CopyIndex = m_FixedPatternList.GetItemCount() - 1;

	int count = 0;
	for (int t = 0; t< Lot_InsertNum; t++){
		if ((t != 2) && (t != 0)){
			m_Lot_FixedPatternList.SetItemText(Index, t, m_FixedPatternList.GetItemText(CopyIndex, count));
			count++;

		}
		else if (t == 0){
			count++;
		}
		else{
			m_Lot_FixedPatternList.SetItemText(Index, t, ((CImageTesterDlg  *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;

}
bool COption_FixedPattern::LOT_EXCEL_UPLOAD()
{
	m_Lot_FixedPatternList.DeleteAllItems();
	if (((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("Fixed_Pattern", &m_Lot_FixedPatternList, 1, &Lot_StartCnt) == TRUE){
		return TRUE;
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;
	}
	return TRUE;
}
void COption_FixedPattern::LOT_Set_List(CListCtrl *List){

	while (List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0, "NO", LVCFMT_CENTER, 30);
	List->InsertColumn(1, "모델명", LVCFMT_CENTER, 100);
	List->InsertColumn(2, "LOT 명", LVCFMT_CENTER, 80);
	List->InsertColumn(3, "Start DATE", LVCFMT_CENTER, 80);
	List->InsertColumn(4, "Start TIME", LVCFMT_CENTER, 80);
	List->InsertColumn(5, "VAL", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "result", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "비고", LVCFMT_CENTER, 80);

	Lot_InsertNum = 8;
	//Copy_List(&m_Lot_FixedPatternList, ((CImageTesterDlg *)m_pMomWnd)->m_pWorkList, ListItemNum);
}

void COption_FixedPattern::LOT_EXCEL_SAVE()
{
	CString Item[1] = { "VAL" };
	CString Data = "";
	CString str = "";
	str = "*********************** Fixed_Pattern [ 단위 : dB ] ***********************\n";
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str.Format("\n");
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("Fixed_Pattern", Item, 1, &m_Lot_FixedPatternList, Data);
}

void COption_FixedPattern::OnNMClickListRect(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	if (pNMItemActivate->iSubItem == 0){
		iSavedItem_click = pNMItemActivate->iItem;
		ChangeCheck = 1;
		SetTimer(150, 5, NULL);
		List_COLOR_Change(iSavedItem_click);
		UpdateData(FALSE);
	}
	*pResult = 0;
}
void COption_FixedPattern::List_COLOR_Change(int itemnum)
{
	bool FLAG = TRUE;
	int Check = 0;

	for (int t = 0; t<3; t++){
		if (ChangeItem[t] == 0)Check++;
		if (ChangeItem[t] == 1)Check++;
		if (ChangeItem[t] == 2)Check++;
	}

	if (Check != 3){
		FLAG = FALSE;
		ChangeItem[changecount] = itemnum;
	}

	if (!FLAG){
		for (int t = 0; t<changecount + 1; t++){
			for (int k = 0; k<changecount + 1; k++){

				if (ChangeItem[t] == ChangeItem[k]){
					if (t == k){ break; }
					ChangeItem[k] = -1;
				}
			}
		}
		//----------------데이터 정렬
		int temp = 0;

		for (int i = 0; i<3; i++)
		{
			for (int j = i + 1; j<3; j++)
			{
				if (ChangeItem[i] < ChangeItem[j])  // 내림차순
				{
					temp = ChangeItem[i];
					ChangeItem[i] = ChangeItem[j];
					ChangeItem[j] = temp;
				}
			}
		}
		//--------------------------------
		for (int t = 0; t<3; t++){

			if (ChangeItem[t] == -1){
				changecount = t;
				break;
			}
		}
	}

	for (int t = 0; t<3; t++){
		m_RectList.Update(t);
	}
}

void COption_FixedPattern::OnNMCustomdrawListRect(NMHDR *pNMHDR, LRESULT *pResult)
{
	COLORREF text_color = 0;
	COLORREF bg_color = RGB(255, 255, 255);

	LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	switch (lplvcd->nmcd.dwDrawStage){
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;
		return;

		// 배경 혹은 텍스트를 수정한다.
	case CDDS_ITEMPREPAINT:
		// 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
		*pResult = CDRF_NOTIFYITEMDRAW;
		return;

		// 서브 아이템의 배경 혹은 텍스트를 수정한다.
	case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
		if (lplvcd->iSubItem != 0){
			// 1번째 행이라면...

			if (lplvcd->nmcd.dwItemSpec >= 0 || lplvcd->nmcd.dwItemSpec <3){


				if (ChangeCheck == 1){
					if (lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 13){
						for (int t = 0; t<3; t++){
							if (lplvcd->nmcd.dwItemSpec == ChangeItem[t]){
								text_color = RGB(0, 0, 0);
								bg_color = RGB(250, 230, 200);
							}
						}
					}


				}

			}


			lplvcd->clrText = text_color;
			lplvcd->clrTextBk = bg_color;

		}
		*pResult = CDRF_NEWFONT;
		return;
	}
}


void COption_FixedPattern::OnNMDblclkListRect(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (pNMITEM->iSubItem == 0 || pNMITEM->iSubItem == -1 || pNMITEM->iItem == -1){ return; }

	iSavedItem = pNMITEM->iItem;
	iSavedSubitem = pNMITEM->iSubItem;

	CRect rect;
	if (pNMITEM->iItem != -1)
	{
		if (pNMITEM->iSubItem != 0)
		{
			m_RectList.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			m_RectList.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowText(m_RectList.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW);
			((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetFocus();
		}
	}
	*pResult = 0;
}


void COption_FixedPattern::OnEnKillfocusEditRectlist()
{
	CString str = "";
	ChangeCheck = 1;
	if ((iSavedItem != -1) && (iSavedSubitem != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_RECTLIST, str));
		List_COLOR_Change(iSavedItem);
		if (m_RectList.GetItemText(iSavedItem, iSavedSubitem) != str){
			Change_DATA();
			Rect_InsertList();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
	iSavedItem = -1;
	iSavedSubitem = -1;
}
bool COption_FixedPattern::Change_DATA_CHECK(bool FLAG){


	BOOL STAT = TRUE;
	//상한
	if (Test_RECT[iSavedItem].m_Width > CAM_IMAGE_WIDTH)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Height > CAM_IMAGE_HEIGHT)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_PosX > CAM_IMAGE_WIDTH)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_PosY > CAM_IMAGE_HEIGHT)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Top > CAM_IMAGE_HEIGHT)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Bottom > CAM_IMAGE_HEIGHT)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Left> CAM_IMAGE_WIDTH)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Right> CAM_IMAGE_WIDTH)
		STAT = FALSE;

	//하한

	if (Test_RECT[iSavedItem].m_Width < 1)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Height < 1)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_PosX < 0)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_PosY < 0)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Top < 0)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Bottom < 0)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Left< 0)
		STAT = FALSE;
	if (Test_RECT[iSavedItem].m_Right< 0)
		STAT = FALSE;

	if (FLAG == 1){
		if (Test_RECT[iSavedItem].m_Width == 1)
			STAT = FALSE;
		if (Test_RECT[iSavedItem].m_Height == 1)
			STAT = FALSE;

	}


	return STAT;
}
void COption_FixedPattern::Change_DATA(){

	CString str;

	((CEdit *)GetDlgItemText(IDC_EDIT_RECTLIST, str));
	m_RectList.SetItemText(iSavedItem, iSavedSubitem, str);

	double num = atof(str);
	if (num < 0){
		num = 0;
	}

	if (iSavedSubitem == 1){
		Test_RECT[iSavedItem].m_PosX = (int)num;

	}
	if (iSavedSubitem == 2){
		Test_RECT[iSavedItem].m_PosY = (int)num;

	}
	if (iSavedSubitem == 3){
		Test_RECT[iSavedItem].m_Left = (int)num;
		Test_RECT[iSavedItem].m_PosX = (Test_RECT[iSavedItem].m_Right + 1 + Test_RECT[iSavedItem].m_Left) / 2;
		Test_RECT[iSavedItem].m_Width = Test_RECT[iSavedItem].m_Right + 1 - Test_RECT[iSavedItem].m_Left;

	}
	if (iSavedSubitem == 4){
		Test_RECT[iSavedItem].m_Top = (int)num;
		Test_RECT[iSavedItem].m_PosY = (Test_RECT[iSavedItem].m_Top + Test_RECT[iSavedItem].m_Bottom + 1) / 2;
		Test_RECT[iSavedItem].m_Height = Test_RECT[iSavedItem].m_Bottom + 1 - Test_RECT[iSavedItem].m_Top;

	}
	if (iSavedSubitem == 5){
		if (num == 0){
			Test_RECT[iSavedItem].m_Right = 0;
		}
		else{
			Test_RECT[iSavedItem].m_Right = (int)num;
			Test_RECT[iSavedItem].m_PosX = (Test_RECT[iSavedItem].m_Right + Test_RECT[iSavedItem].m_Left + 1) / 2;
			Test_RECT[iSavedItem].m_Width = Test_RECT[iSavedItem].m_Right + 1 - Test_RECT[iSavedItem].m_Left;
		}
	}
	if (iSavedSubitem == 6){
		if (num == 0){
			Test_RECT[iSavedItem].m_Bottom = 0;
		}
		else{
			Test_RECT[iSavedItem].m_Bottom = (int)num;
			Test_RECT[iSavedItem].m_PosY = (Test_RECT[iSavedItem].m_Top + Test_RECT[iSavedItem].m_Bottom + 1) / 2;
			Test_RECT[iSavedItem].m_Height = Test_RECT[iSavedItem].m_Bottom + 1 - Test_RECT[iSavedItem].m_Top;
		}
	}
	if (iSavedSubitem == 7){
		Test_RECT[iSavedItem].m_Width = (int)num;
	}
	if (iSavedSubitem == 8){
		Test_RECT[iSavedItem].m_Height = (int)num;
	}


	Test_RECT[iSavedItem].EX_RECT_SET(
		Test_RECT[iSavedItem].m_PosX,
		Test_RECT[iSavedItem].m_PosY,
		Test_RECT[iSavedItem].m_Width,
		Test_RECT[iSavedItem].m_Height);

	if (Test_RECT[iSavedItem].m_Left < 0){//////////////////////수정
		Test_RECT[iSavedItem].m_Left = 0;
		Test_RECT[iSavedItem].m_Width = Test_RECT[iSavedItem].m_Right + 1 - Test_RECT[iSavedItem].m_Left;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}
	if (Test_RECT[iSavedItem].m_Right >CAM_IMAGE_WIDTH){
		Test_RECT[iSavedItem].m_Right = CAM_IMAGE_WIDTH;
		Test_RECT[iSavedItem].m_Width = Test_RECT[iSavedItem].m_Right + 1 - Test_RECT[iSavedItem].m_Left;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}
	if (Test_RECT[iSavedItem].m_Top< 0){
		Test_RECT[iSavedItem].m_Top = 0;
		Test_RECT[iSavedItem].m_Height = Test_RECT[iSavedItem].m_Bottom + 1 - Test_RECT[iSavedItem].m_Top;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}
	if (Test_RECT[iSavedItem].m_Bottom >CAM_IMAGE_HEIGHT){
		Test_RECT[iSavedItem].m_Bottom = CAM_IMAGE_HEIGHT;
		Test_RECT[iSavedItem].m_Height =
			Test_RECT[iSavedItem].m_Bottom + 1 -
			Test_RECT[iSavedItem].m_Top;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}
	if (Test_RECT[iSavedItem].m_Height <= 0){
		Test_RECT[iSavedItem].m_Height = 1;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}
	if (Test_RECT[iSavedItem].m_Height >= CAM_IMAGE_HEIGHT){
		Test_RECT[iSavedItem].m_Height = CAM_IMAGE_HEIGHT;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}

	if (Test_RECT[iSavedItem].m_Width <= 0){
		Test_RECT[iSavedItem].m_Width = 1;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}
	if (Test_RECT[iSavedItem].m_Width >= CAM_IMAGE_WIDTH){
		Test_RECT[iSavedItem].m_Width = CAM_IMAGE_WIDTH;
		Test_RECT[iSavedItem].EX_RECT_SET(
			Test_RECT[iSavedItem].m_PosX,
			Test_RECT[iSavedItem].m_PosY,
			Test_RECT[iSavedItem].m_Width,
			Test_RECT[iSavedItem].m_Height);
	}

	Test_RECT[iSavedItem].EX_RECT_SET(
		Test_RECT[iSavedItem].m_PosX,
		Test_RECT[iSavedItem].m_PosY,
		Test_RECT[iSavedItem].m_Width,
		Test_RECT[iSavedItem].m_Height);

	CString data = "";

	if (iSavedSubitem == 1){
		data.Format("%d", Test_RECT[iSavedItem].m_PosX);
	}
	else if (iSavedSubitem == 2){
		data.Format("%d", Test_RECT[iSavedItem].m_PosY);
	}
	else if (iSavedSubitem == 3){
		data.Format("%d", Test_RECT[iSavedItem].m_Left);
	}
	else if (iSavedSubitem == 4){
		data.Format("%d", Test_RECT[iSavedItem].m_Top);
	}
	else if (iSavedSubitem == 5){
		data.Format("%d", Test_RECT[iSavedItem].m_Right);
	}
	else if (iSavedSubitem == 6){
		data.Format("%d", Test_RECT[iSavedItem].m_Bottom);
	}
	else if (iSavedSubitem == 7){
		data.Format("%d", Test_RECT[iSavedItem].m_Width);
	}
	else if (iSavedSubitem == 8){
		data.Format("%d", Test_RECT[iSavedItem].m_Height);
	}

	((CEdit *)GetDlgItem(IDC_EDIT_RECTLIST))->SetWindowText(data);
}
void COption_FixedPattern::Rect_SETLIST(){

	m_RectList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_CHECKBOXES);
	m_RectList.InsertColumn(0, "구분", LVCFMT_CENTER, 90);
	m_RectList.InsertColumn(1, "PosX", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(2, "PosY", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(3, "St X", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(4, "St Y", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(5, "Ex X", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(6, "Ex Y", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(7, "W", LVCFMT_CENTER, 40);
	m_RectList.InsertColumn(8, "H", LVCFMT_CENTER, 40);

}
void COption_FixedPattern::Rect_InsertList()
{
	int InIndex = 0;
	CString str = "";

	m_RectList.DeleteAllItems();

	for (int t = 0; t<1; t++){
		InIndex = m_RectList.InsertItem(t, "초기", 0);


		if (t == 0){
			str.Empty(); str = "카메라영역";
		}

		m_RectList.SetItemText(InIndex, 0, str);
		str.Empty(); str.Format("%d", Test_RECT[t].m_PosX);
		m_RectList.SetItemText(InIndex, 1, str);
		str.Empty(); str.Format("%d", Test_RECT[t].m_PosY);
		m_RectList.SetItemText(InIndex, 2, str);

		str.Empty();
		str.Format("%d", Test_RECT[t].m_Left);
		m_RectList.SetItemText(InIndex, 3, str);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Top);
		m_RectList.SetItemText(InIndex, 4, str);

		str.Empty();
		str.Format("%d", Test_RECT[t].m_Right + 1);
		m_RectList.SetItemText(InIndex, 5, str);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Bottom + 1);
		m_RectList.SetItemText(InIndex, 6, str);

		str.Empty();
		str.Format("%d", Test_RECT[t].m_Width);
		m_RectList.SetItemText(InIndex, 7, str);
		str.Empty();
		str.Format("%d", Test_RECT[t].m_Height);
		m_RectList.SetItemText(InIndex, 8, str);

		if (m_b_Ellipse[t] == TRUE){
			m_RectList.SetItemState(t, 0x2000, LVIS_STATEIMAGEMASK);
		}
		else{
			m_RectList.SetItemState(t, 0x1000, LVIS_STATEIMAGEMASK);
		}

	}
}
void COption_FixedPattern::OnEnSetfocusEditRectlist()
{
	int num = iSavedItem;
	if ((Test_RECT[num].m_PosX >= 0) && (Test_RECT[num].m_PosX < CAM_IMAGE_WIDTH) &&
		(Test_RECT[num].m_PosY >= 0) && (Test_RECT[num].m_PosY < CAM_IMAGE_HEIGHT) &&
		(Test_RECT[num].m_Left >= 0) && (Test_RECT[num].m_Left < CAM_IMAGE_WIDTH) &&
		(Test_RECT[num].m_Top >= 0) && (Test_RECT[num].m_Top  < CAM_IMAGE_HEIGHT) &&
		(Test_RECT[num].m_Right >= 0) && (Test_RECT[num].m_Right< CAM_IMAGE_WIDTH) &&
		(Test_RECT[num].m_Bottom >= 0) && (Test_RECT[num].m_Bottom<CAM_IMAGE_HEIGHT) &&
		(Test_RECT[num].m_Width >= 0) && (Test_RECT[num].m_Width<CAM_IMAGE_WIDTH) &&
		(Test_RECT[num].m_Height >= 0) && (Test_RECT[num].m_Height<CAM_IMAGE_HEIGHT)){
		((CButton *)GetDlgItem(IDC_BUTTON_LIST_SAVE))->EnableWindow(1);
	}
	else{
		((CButton *)GetDlgItem(IDC_BUTTON_LIST_SAVE))->EnableWindow(0);
	}
}
void COption_FixedPattern::Pic(CDC *cdc)
{
	for (int t = 0; t<1; t++){
		FIXEDPATTERNPic_New(cdc, t);
	}
}
bool COption_FixedPattern::FIXEDPATTERNPic_New(CDC *cdc, int NUM){


	CPen	my_Pan, *old_pan;
	CString TEXTDATA = "";
	::SetBkMode(cdc->m_hDC, TRANSPARENT);

	if (NUM == 0){
		my_Pan.CreatePen(PS_SOLID, 2, WHITE_COLOR);
	}
	else if (NUM == 1){
		my_Pan.CreatePen(PS_SOLID, 2, GREEN_COLOR);
	}
	else{
		my_Pan.CreatePen(PS_SOLID, 2, YELLOW_COLOR);
	}

// 	if (m_b_Ellipse[NUM] == TRUE){
// 		old_pan = cdc->SelectObject(&my_Pan);
// 
// 		CBrush oldBrush;
// 		oldBrush.CreateStockObject(NULL_BRUSH);
// 		CBrush *poldBrush = cdc->SelectObject(&oldBrush);
// 		cdc->Ellipse(Test_RECT[NUM].m_Left,
// 			Test_RECT[NUM].m_Top,
// 			Test_RECT[NUM].m_Right,
// 			Test_RECT[NUM].m_Bottom);
// 
// 		cdc->SelectObject(old_pan);
// 		cdc->SelectObject(oldBrush);
// 
// 		my_Pan.DeleteObject();
// 		oldBrush.DeleteObject();
// 	}
// 	else{
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->MoveTo(Test_RECT[NUM].m_Left, Test_RECT[NUM].m_Top);
		cdc->LineTo(Test_RECT[NUM].m_Right, Test_RECT[NUM].m_Top);
		cdc->LineTo(Test_RECT[NUM].m_Right, Test_RECT[NUM].m_Bottom);
		cdc->LineTo(Test_RECT[NUM].m_Left, Test_RECT[NUM].m_Bottom);
		cdc->LineTo(Test_RECT[NUM].m_Left, Test_RECT[NUM].m_Top);

		my_Pan.DeleteObject();
//	}

	return 1;
}

void COption_FixedPattern::OnBnClickedButtonDefault()
{
	for (int k = 0; k<1; k++){
		Test_RECT[k].m_PosX = (CAM_IMAGE_WIDTH / 2);
		Test_RECT[k].m_PosY = (CAM_IMAGE_HEIGHT / 2);
		Test_RECT[k].m_Width = CAM_IMAGE_WIDTH-10;
		Test_RECT[k].m_Height = CAM_IMAGE_HEIGHT-10;
		Test_RECT[k].EX_RECT_SET(
			Test_RECT[k].m_PosX,
			Test_RECT[k].m_PosY,
			Test_RECT[k].m_Width,
			Test_RECT[k].m_Height
			);
	}
	Rect_InsertList();
}


void COption_FixedPattern::OnBnClickedButtonLoad()
{
	Load_parameter();
	UpdateData(FALSE);
	Rect_InsertList();
	ChangeCheck = 0;
	for (int t = 0; t<1; t++){
		ChangeItem[t] = -1;
	}
	for (int t = 0; t<1; t++){
		m_RectList.Update(t);
	}
}


void COption_FixedPattern::OnBnClickedButtonListSave()
{
	Save_parameter();
	Rect_InsertList();

	ChangeCheck = 0;
	for (int t = 0; t<1; t++){
		ChangeItem[t] = -1;
	}
	for (int t = 0; t<1; t++){
		m_RectList.Update(t);
	}
}
tResultVal COption_FixedPattern::Run()
{
	CString str_Data = "";
	CString str_Pos = "";
	CString str_OutData = "";
	CString str = "";

	tResultVal retval = { 0, };
	BOOL FLAG = FALSE;
	BOOL b_FailMode = TRUE;

	m_b_StopFail = FALSE;
	m_retval.Reset();

	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

		InsertList();
	}

	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = 1;
	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;

	for (int i = 0; i < 10; i++){
		if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0 && ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0){
			break;
		}
		else{
			DoEvents(50);
		}
	}



	//TEST 알고리즘
	FLAG = PatternNoiseGen(m_RGBScanbuf, 0);
	CString stateDATA = "FixedPattern_ ";

	if (FLAG == TRUE){
		retval.m_Success = TRUE;
		retval.ValString.Empty();
		retval.ValString.Format("TEST SUCCESS");
		((CImageTesterDlg  *)m_pMomWnd)->m_pResFixedPatternWnd->RESULT_TEXT(1, "PASS");
		str.Format("%d", (int)Test_RECT[0].m_result);
		stateDATA += str +"_PASS";
		((CImageTesterDlg  *)m_pMomWnd)->m_pResFixedPatternWnd->FIXEDPATTERNNUM_TEXT(str);


// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 			if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
// 
// 				m_Lot_FixedPatternList.SetItemText(Lot_InsertIndex, 5, str);
// 				m_Lot_FixedPatternList.SetItemText(Lot_InsertIndex, 6, "PASS");
// 			}
// 		}
// 		else{
		if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			m_FixedPatternList.SetItemText(InsertIndex, 4, str);
			m_FixedPatternList.SetItemText(InsertIndex, 5, "PASS");
			((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_FIXED_PATTERN, "PASS");
		}


	}
	else{
		retval.m_Success = FALSE;
		retval.ValString.Empty();
		retval.ValString.Format("TEST FAIL");
		((CImageTesterDlg  *)m_pMomWnd)->m_pResFixedPatternWnd->RESULT_TEXT(2, "FAIL");
		str.Format("%d", (int)Test_RECT[0].m_result);
		stateDATA += str + "_FAIL";
		((CImageTesterDlg  *)m_pMomWnd)->m_pResFixedPatternWnd->FIXEDPATTERNNUM_TEXT(str);

// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 			if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
// 
// 				m_Lot_FixedPatternList.SetItemText(Lot_InsertIndex, 5, str);
// 				m_Lot_FixedPatternList.SetItemText(Lot_InsertIndex, 6, "FAIL");
// 			}
// 		}
// 		else{
		if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			m_FixedPatternList.SetItemText(InsertIndex, 4, str);
			m_FixedPatternList.SetItemText(InsertIndex, 5, "FAIL");
			((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_FIXED_PATTERN, "FAIL");
		}

	}

// 	if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 		if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
// 			Lot_StartCnt++;
// 		}
// 	}
// 	else{
	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
		StartCnt++;
	}
	((CImageTesterDlg  *)m_pMomWnd)->StatePrintf(stateDATA);

	m_retval = retval;
	return retval;
}

CString COption_FixedPattern::Mes_Result()
{
	CString sz;
	int		nResult = 0;

	// 제일 마지막 항목을 추가하자
	int nSel = m_FixedPatternList.GetItemCount() - 1;

	// 결과값
	sz = m_FixedPatternList.GetItemText(nSel, 4);

	if (m_retval.m_Success)
		nResult = 1;
	else
		nResult = 0;

	if (m_b_StopFail == FALSE)
	{
		m_szMesResult.Format(_T("%s:%d"), sz, nResult);
	}
	else{
		m_szMesResult.Format(_T(":1"));
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_stNewMesData[NewMES_PixedPattern] = m_szMesResult;

	return m_szMesResult;
}

BOOL COption_FixedPattern::PatternNoiseGen(LPBYTE IN_RGB, int NUM){
	BOOL m_Success;
	BYTE R, G, B;
	double total_abs = 0;
	double avg_abs = 0;

	double env_Value = (double)ThrNoise;///ThrDiff 루리텍 비밀 옵션
	double dBThreshold = (double)ThrDark;

	int i = 0;
	BYTE **BW;
	BW = (BYTE **)malloc(sizeof(BYTE *)*CAM_IMAGE_HEIGHT);                     //   
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)BW[i] = (BYTE *)malloc(sizeof(BYTE)* CAM_IMAGE_WIDTH);


	for (int t = 0; t<CAM_IMAGE_HEIGHT; t++){
		for (int k = 0; k<CAM_IMAGE_WIDTH; k++){
			BW[t][k] = 0;
		}
	}

	int startx = Test_RECT[NUM].m_Left;
	int starty = Test_RECT[NUM].m_Top;
	int endx = startx + Test_RECT[NUM].Length() - 2;
	int endy = starty + Test_RECT[NUM].Height() - 2;


	double **Cal_X;
	Cal_X = (double **)malloc(sizeof(double *)*CAM_IMAGE_HEIGHT);                     //   
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)Cal_X[i] = (double *)malloc(sizeof(double)* 2);

	double **Cal_Y;
	Cal_Y = (double **)malloc(sizeof(double *)*CAM_IMAGE_WIDTH);                     //   
	for (i = 0; i<CAM_IMAGE_WIDTH; i++)Cal_Y[i] = (double *)malloc(sizeof(double)* 2);

	double **testX;
	testX = (double **)malloc(sizeof(double *)* 2);                     //   
	for (i = 0; i<2; i++)testX[i] = (double *)malloc(sizeof(double)* CAM_IMAGE_HEIGHT);

	double **testY;
	testY = (double **)malloc(sizeof(double *)* 2);                     //   
	for (i = 0; i<2; i++)testY[i] = (double *)malloc(sizeof(double)* CAM_IMAGE_WIDTH);

	for (int t = 0; t<CAM_IMAGE_HEIGHT; t++){
		Cal_X[t][0] = 0;
		Cal_X[t][1] = 0;
		testX[0][t] = 0;
		testX[1][t] = 0;
		for (int k = 0; k<CAM_IMAGE_WIDTH; k++){
			BW[t][k] = 0;
			Cal_Y[k][0] = 0;
			Cal_Y[k][1] = 0;
			testY[0][k] = 0;
			testY[1][k] = 0;

		}
	}



	/*double Cal_X[CAM_IMAGE_HEIGHT][2]={0,},testX[2][CAM_IMAGE_HEIGHT];
	double Cal_Y[CAM_IMAGE_WIDTH][2]={0,},testY[2][CAM_IMAGE_WIDTH]; */

	int count = 0, CountLine[2] = { 0, };
	total_abs = 0;
	double originData = 0;
	for (int y = starty; y<endy; y++)
	{
		Cal_X[y - starty][0] = 0;	Cal_X[y - starty][1] = 0; CountLine[0] = 0;
		for (int x = startx; x<endx; x++)
		{
			if ((Test_RECT[NUM].m_Left <= x) && (Test_RECT[NUM].m_Right >= x)){
				if ((Test_RECT[NUM].m_Top <= y) && (Test_RECT[NUM].m_Bottom >= y)){

					B = (IN_RGB[y *(CAM_IMAGE_WIDTH * 3) + (x * 3) + 0]);
					G = (IN_RGB[y *(CAM_IMAGE_WIDTH * 3) + (x * 3) + 1]);
					R = (IN_RGB[y *(CAM_IMAGE_WIDTH * 3) + (x * 3) + 2]);

					//originData += (BYTE)((0.29900*(R))+(0.58700*(G))+(0.11400*(B)));


					BW[y][x] = (255 - (BYTE)((0.29900*(R)) + (0.58700*(G)) + (0.11400*(B))));


					total_abs += BW[y][x];
					count++;
					CountLine[0]++;
					Cal_X[y - starty][0] += (double)BW[y][x];			Cal_X[y - starty][1] += (double)BW[y][x] * (double)BW[y][x];
				}
			}
		}
		if (CountLine[0]>0){
			Cal_X[y - starty][0] /= (double)CountLine[0]; Cal_X[y - starty][1] /= (double)CountLine[0];
			Cal_X[y - starty][1] = Cal_X[y - starty][1] - Cal_X[y - starty][0] * Cal_X[y - starty][0];
		}
	}

	double avg_origin = 0;
	avg_origin = total_abs / (double)count;
	//avg_abs = total_abs / (double)count; // 추출된 rgb 값에 대한 평균(전체 평균)
	CountLine[1] = 0;
	for (int x = startx; x < endx; x++){
		Cal_Y[x - startx][0] = 0;		Cal_Y[x - startx][1] = 0;  CountLine[1] = 0;

		for (int y = starty; y < endy; y++){

			Cal_Y[x - startx][0] += (double)BW[y][x];			Cal_Y[x - startx][1] += (double)BW[y][x] * (double)BW[y][x];
			CountLine[1]++;
		}
		if (CountLine[1]>0){
			Cal_Y[x - startx][0] /= (double)CountLine[1]; Cal_Y[x - startx][1] /= (double)CountLine[1];
			Cal_Y[x - startx][1] = Cal_Y[x - startx][1] - Cal_Y[x - startx][0] * Cal_Y[x - startx][0];
		}
	}


	double Dark = 0, Noise[2];
	Noise[0] = Noise[1] = 0;
	for (long i = 0; i<Test_RECT[NUM].Height() - 2; i++)	Dark += Cal_X[i][0];    Dark /= (double)Test_RECT[NUM].Height() - 2;
	for (long i = 0; i<Test_RECT[NUM].Height() - 2; i++)	Noise[0] += Cal_X[i][1];    Noise[0] /= (double)Test_RECT[NUM].Height() - 2;
	for (long i = 0; i<Test_RECT[NUM].Length() - 2; i++)	Noise[1] += Cal_Y[i][1];    Noise[1] /= (double)Test_RECT[NUM].Length() - 2;

	double SNR[4] = { 0, }, Sum[4] = { 0, };
	int Margin;
	int cnt[2] = { 0, };

	//	Margin=(int)(endx*0.2);
	//	for(int i=0+Margin;i<endx-Margin;i++){
	for (int i = 0; i<Test_RECT[NUM].Length() - 2; i++){

		Sum[0] += Cal_Y[i][0];		SNR[0] += Cal_Y[i][0] * Cal_Y[i][0];
		Sum[1] += Cal_Y[i][1];		SNR[1] += Cal_Y[i][1] * Cal_Y[i][1];
		cnt[0]++;
	}

	Sum[0] /= (double)cnt[0];		SNR[0] /= (double)cnt[0];
	Sum[1] /= (double)cnt[0];		SNR[1] /= (double)cnt[0];

	SNR[0] = SNR[0] - Sum[0] * Sum[0];
	SNR[1] = SNR[1] - Sum[1] * Sum[1];

	//	Margin=(int)(endy*0.2);
	//	for(int i=0+Margin;i<endy-Margin;i++){
	for (int i = 0; i<Test_RECT[NUM].Height() - 2; i++){
		Sum[2] += Cal_Y[i][2];		SNR[2] += Cal_Y[i][2] * Cal_Y[i][2];
		Sum[3] += Cal_Y[i][3];		SNR[3] += Cal_Y[i][3] * Cal_Y[i][3];
		cnt[1]++;
	}

	Sum[2] /= (double)cnt[1];		SNR[2] /= (double)cnt[1];
	Sum[3] /= (double)cnt[1];		SNR[3] /= (double)cnt[1];

	SNR[2] = SNR[2] - Sum[2] * Sum[2];
	SNR[3] = SNR[3] - Sum[3] * Sum[3];


	//double TotalSNR=(Sum[0]*Sum[1]*Sum[2]*Sum[3])/sqrt(SNR[0]*SNR[1]*SNR[2]*SNR[3]);

	double TotalSNR = ( Sum[0]) / sqrt(SNR[0]);
	TotalSNR = 10 * log10l(TotalSNR)*1.2;

	if (SNR[0] == 0)
	{
		TotalSNR = 100;
	}



	double Difference_Data = (Noise[0] / Noise[1]);


	for (int i = 0; i<CAM_IMAGE_HEIGHT; i++){
		testX[0][i] = Cal_X[i][0]; testX[1][i] = Cal_X[i][1];

	}

	for (int i = 0; i<CAM_IMAGE_WIDTH; i++){
		testY[0][i] = Cal_Y[i][0]; testY[1][i] = Cal_Y[i][1];
	}

	if (Dark < 0){
		Dark = 0;
	}
	if (Noise[0] <0){
		Noise[0] = 0;
	}
	if (Difference_Data <0){
		Difference_Data = 0;
	}



	// 만약 기준 신호레벨은 0이라면, SNR은 1이다.(전체평균 = 평균 noise 편차)
	if (avg_origin >env_Value){
		m_Success = FALSE;
// 		Result.Empty();
// 		Result.Format("TEST환경이 아닙니다.");
		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
			m_Lot_FixedPatternList.SetItemText(Lot_InsertIndex, 7, "TEST환경X");////////
		}
		else{
			m_FixedPatternList.SetItemText(InsertIndex, 6, "TEST환경X");////비고 넘버
		}
		Test_RECT[NUM].m_result = 0;
		Test_RECT[NUM].m_Success = FALSE;
	}
	else{
		if (TotalSNR >= dBThreshold){
			m_Success = TRUE;
			Test_RECT[NUM].m_Success = TRUE;

		}
		else{
			m_Success = FALSE;
			Test_RECT[NUM].m_Success = FALSE;

		}

		Test_RECT[NUM].m_result = TotalSNR;
		CString str = "";
		str.Empty();
		str.Format("%3.1f", TotalSNR);
	}


	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)free(BW[i]);   free(BW);
	for (i = 0; i<CAM_IMAGE_HEIGHT; i++)free(Cal_X[i]);   free(Cal_X);
	for (i = 0; i<CAM_IMAGE_WIDTH; i++)free(Cal_Y[i]);   free(Cal_Y);
	for (i = 0; i<2; i++)free(testX[i]);   free(testX);
	for (i = 0; i<2; i++)free(testY[i]);   free(testY);

	return m_Success;


}

void COption_FixedPattern::OnBnClickedButtonFpnSave()
{
	UpdateData(TRUE);

	CString str = "";
	str.Empty();
	str.Format("%d", ThrDark);
	WritePrivateProfileString("FIXEDPATTERN_OPT", "Dark", str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);
	str.Empty();
	str.Format("%d", ThrNoise);
	WritePrivateProfileString("FIXEDPATTERN_OPT", "Noise", str,((CImageTesterDlg *)m_pMomWnd)->modelfile_Name);

	UpdateData(FALSE);
	((CButton *)GetDlgItem(IDC_BUTTON_FPN_SAVE))->EnableWindow(0);
}


void COption_FixedPattern::OnEnChangeThresoldV()
{
	UpdateData(TRUE);
}


void COption_FixedPattern::OnEnChangeThresoldY()
{
	UpdateData(TRUE);
}
