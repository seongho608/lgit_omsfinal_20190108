// Angle_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Angle_ResultView.h"


// CAngle_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAngle_ResultView, CDialog)

CAngle_ResultView::CAngle_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CAngle_ResultView::IDD, pParent)
{

}

CAngle_ResultView::~CAngle_ResultView()
{
}

void CAngle_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAngle_ResultView, CDialog)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CAngle_ResultView 메시지 처리기입니다.
void CAngle_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CAngle_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
BOOL CAngle_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();
	Font_Size_Change(IDC_STATIC_LEFT_C,&ft,100,20);
	GetDlgItem(IDC_VALUE_LEFT_C)->SetFont(&ft);
	GetDlgItem(IDC_RESULT_LEFT_C)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_RIGHT_C)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_RIGHT_C)->SetFont(&ft);
	GetDlgItem(IDC_RESULT_RIGHT_C)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_TOP_C)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_TOP_C)->SetFont(&ft);
	GetDlgItem(IDC_RESULT_TOP_C)->SetFont(&ft);
	GetDlgItem(IDC_STATIC_BOTTOM_C)->SetFont(&ft);
	GetDlgItem(IDC_VALUE_BOTTOM_C)->SetFont(&ft);
	GetDlgItem(IDC_RESULT_BOTTOM_C)->SetFont(&ft);

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	initstat();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CAngle_ResultView::initstat()
{
	LC_TEXT("LEFT-CENTER");
	RC_TEXT("RIGHT-CENTER");
	TC_TEXT("TOP-CENTER");
	BC_TEXT("BOTTOM-CENTER");

	LC_VALUE("");
	RC_VALUE("");
	TC_VALUE("");
	BC_VALUE("");

	LC_RESULT(0,"STAND BY");
	RC_RESULT(0,"STAND BY");
	TC_RESULT(0,"STAND BY");
	BC_RESULT(0,"STAND BY");
}

HBRUSH CAngle_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_LEFT_C){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_LEFT_C){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() ==IDC_RESULT_LEFT_C){
		pDC->SetTextColor(txcol_LTVal);
		pDC->SetBkColor(bkcol_LTVal);
	}

	if(pWnd->GetDlgCtrlID() == IDC_STATIC_RIGHT_C){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_RIGHT_C){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() ==IDC_RESULT_RIGHT_C){
		pDC->SetTextColor(txcol_RTVal);
		pDC->SetBkColor(bkcol_RTVal);
	}

	if(pWnd->GetDlgCtrlID() == IDC_STATIC_TOP_C){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_TOP_C){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() ==IDC_RESULT_TOP_C){
		pDC->SetTextColor(txcol_LBVal);
		pDC->SetBkColor(bkcol_LBVal);
	}
	if(pWnd->GetDlgCtrlID() == IDC_STATIC_BOTTOM_C){
			pDC->SetTextColor(RGB(255, 255, 255));
			// 텍스트의 배경색상을 설정한다.
			//pDC->SetBkColor(RGB(138, 75, 36));
			pDC->SetBkColor(RGB(86,86,86));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() == IDC_VALUE_BOTTOM_C){
			pDC->SetTextColor(RGB(86,86,86));
			// 텍스트의 배경색상을 설정한다.
			pDC->SetBkColor(RGB(255, 255, 255));
			// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() ==IDC_RESULT_BOTTOM_C){
		pDC->SetTextColor(txcol_RBVal);
		pDC->SetBkColor(bkcol_RBVal);
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CAngle_ResultView::LC_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_LEFT_C))->SetWindowText(lpcszString);
}
void CAngle_ResultView::RC_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_RIGHT_C))->SetWindowText(lpcszString);
}
void CAngle_ResultView::TC_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_TOP_C))->SetWindowText(lpcszString);
}
void CAngle_ResultView::BC_TEXT(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_STATIC_BOTTOM_C))->SetWindowText(lpcszString);
}

void CAngle_ResultView::LC_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_LEFT_C))->SetWindowText(lpcszString);
}
void CAngle_ResultView::RC_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_RIGHT_C))->SetWindowText(lpcszString);
}
void CAngle_ResultView::TC_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_TOP_C))->SetWindowText(lpcszString);
}
void CAngle_ResultView::BC_VALUE(LPCSTR lpcszString, ...){
	((CEdit *)GetDlgItem(IDC_VALUE_BOTTOM_C))->SetWindowText(lpcszString);
}

void CAngle_ResultView::LC_RESULT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_LTVal = RGB(255, 255, 255);
		bkcol_LTVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_LTVal = RGB(255, 255, 255);
		bkcol_LTVal = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_LTVal = RGB(255, 255, 255);
		bkcol_LTVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_LTVal = RGB(0, 0, 0);
		bkcol_LTVal = RGB(255, 255, 0);
	}

	((CEdit *)GetDlgItem(IDC_RESULT_LEFT_C))->SetWindowText(lpcszString);
}
void CAngle_ResultView::RC_RESULT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_RTVal = RGB(255, 255, 255);
		bkcol_RTVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_RTVal = RGB(255, 255, 255);
		bkcol_RTVal = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_RTVal = RGB(255, 255, 255);
		bkcol_RTVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_RTVal = RGB(0, 0, 0);
		bkcol_RTVal = RGB(255, 255, 0);
	}

	((CEdit *)GetDlgItem(IDC_RESULT_RIGHT_C))->SetWindowText(lpcszString);
}
void CAngle_ResultView::TC_RESULT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_LBVal = RGB(255, 255, 255);
		bkcol_LBVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_LBVal = RGB(255, 255, 255);
		bkcol_LBVal = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_LBVal = RGB(255, 255, 255);
		bkcol_LBVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_LBVal = RGB(0, 0, 0);
		bkcol_LBVal = RGB(255, 255, 0);
	}

	((CEdit *)GetDlgItem(IDC_RESULT_TOP_C))->SetWindowText(lpcszString);
}
void CAngle_ResultView::BC_RESULT(unsigned char col,LPCSTR lpcszString, ...){
	
	if(col == 0){//스탠드 바이
		txcol_RBVal = RGB(255, 255, 255);
		bkcol_RBVal = RGB(47, 157, 39);
	}else if(col == 1){//성공
		txcol_RBVal = RGB(255, 255, 255);
		bkcol_RBVal = RGB(18, 69, 171);
	}else if(col == 2){//실패
		txcol_RBVal = RGB(255, 255, 255);
		bkcol_RBVal = RGB(201, 0, 0);
	}else if(col == 3){
		txcol_RBVal = RGB(0, 0, 0);
		bkcol_RBVal = RGB(255, 255, 0);
	}

	((CEdit *)GetDlgItem(IDC_RESULT_BOTTOM_C))->SetWindowText(lpcszString);
}
BOOL CAngle_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}
