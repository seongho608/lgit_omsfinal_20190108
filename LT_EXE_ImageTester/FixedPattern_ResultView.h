#pragma once


// CFixedPattern_ResultView 대화 상자입니다.

class CFixedPattern_ResultView : public CDialog
{
	DECLARE_DYNAMIC(CFixedPattern_ResultView)

public:
	CFixedPattern_ResultView(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CFixedPattern_ResultView();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_FIXEDPATTERN };
	void	Setup(CWnd* IN_pMomWnd);
	void	Initstat();
	void	FIXEDPATTERN_TEXT(LPCSTR lpcszString, ...);
	void	FIXEDPATTERNNUM_TEXT(LPCSTR lpcszString, ...);
	void	RESULT_TEXT(unsigned char col, LPCSTR lpcszString, ...);

	CFont ft1;
	COLORREF txcol_TVal, bkcol_TVal;

private:
	CWnd	*m_pMomWnd;
	void	Font_Size_Change(int nID, CFont *font, LONG Weight, LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnEnSetfocusEditFixedpatterntxt();
	afx_msg void OnEnSetfocusEditFixedpatternnum();
	afx_msg void OnEnSetfocusEditFixedpatternresult();
};
