// NTSC_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "NTSC_Option.h"


// CNTSC_Option 대화 상자입니다.

IMPLEMENT_DYNAMIC(CNTSC_Option, CDialog)

CNTSC_Option::CNTSC_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CNTSC_Option::IDD, pParent)
	, str_VppMin(_T(""))
	, str_VppMax(_T(""))
	, str_VSyncMin(_T(""))
	, str_VSyncMax(_T(""))
	, m_checkVpp(FALSE)
	, m_checkVsync(FALSE)
{
	StartCnt = 0;
	m_Etc_State = 0;
	Lot_StartCnt=0;
	b_StopFail = FALSE;
}

CNTSC_Option::~CNTSC_Option()
{
}

void CNTSC_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_NTSCList);
	DDX_Text(pDX, IDC_EDIT_MIN_VPP, str_VppMin);
	DDX_Text(pDX, IDC_EDIT_MAX_VPP, str_VppMax);
	DDX_Text(pDX, IDC_EDIT_MIN_VSYNC, str_VSyncMin);
	DDX_Text(pDX, IDC_EDIT_MAX_VSYNC, str_VSyncMax);
	DDX_Check(pDX, IDC_CHECK_VPP, m_checkVpp);
	DDX_Check(pDX, IDC_CHECK_VSYNC, m_checkVsync);
	DDX_Control(pDX, IDC_LIST_LOT, m_Ntsc_Worklist);
}


BEGIN_MESSAGE_MAP(CNTSC_Option, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_VPP_SAVE, &CNTSC_Option::OnBnClickedButtonVppSave)
	ON_BN_CLICKED(IDC_BUTTON_VSYNC_SAVE, &CNTSC_Option::OnBnClickedButtonVsyncSave)
	ON_EN_CHANGE(IDC_EDIT_MIN_VPP, &CNTSC_Option::OnEnChangeEditMinVpp)
	ON_EN_CHANGE(IDC_EDIT_MAX_VPP, &CNTSC_Option::OnEnChangeEditMaxVpp)
	ON_EN_CHANGE(IDC_EDIT_MIN_VSYNC, &CNTSC_Option::OnEnChangeEditMinVsync)
	ON_EN_CHANGE(IDC_EDIT_MAX_VSYNC, &CNTSC_Option::OnEnChangeEditMaxVsync)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_CHECK_VPP, &CNTSC_Option::OnBnClickedCheckVpp)
	ON_BN_CLICKED(IDC_CHECK_VSYNC, &CNTSC_Option::OnBnClickedCheckVsync)
END_MESSAGE_MAP()


// CNTSC_Option 메시지 처리기입니다.
BOOL CNTSC_Option::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	d_Vpp= -1;
	d_VSync= -1;
	H_filename=((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;

	Load_parameter();
	UpdateData(FALSE);

	Set_List(&m_NTSCList);
	LOT_Set_List(&m_Ntsc_Worklist);
	((CButton *)GetDlgItem(IDC_BUTTON_VPP_SAVE))->EnableWindow(0);
	((CButton *)GetDlgItem(IDC_BUTTON_VSYNC_SAVE))->EnableWindow(0);

	InitEVMS();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성페이지는FALSE를반환해야합니다.
}

void CNTSC_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CNTSC_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
	strTitle.Empty();
	strTitle="NTSC_INIT";
}

void CNTSC_Option::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",H_filename);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}

void CNTSC_Option::LOT_InsertDataList()
{
	CString strCnt="";

	int Index = m_Ntsc_Worklist.InsertItem(Lot_StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index+1);
	m_Ntsc_Worklist.SetItemText(Index,0,strCnt);

	int CopyIndex = m_NTSCList.GetItemCount()-1;

	int count =0;
	for(int t=0; t< Lot_InsertNum;t++){
		if((t != 2)&&(t!=0)){
			m_Ntsc_Worklist.SetItemText(Index,t, m_NTSCList.GetItemText(CopyIndex,count));
			count++;

		}else if(t==0){
			count++;
		}else{
			m_Ntsc_Worklist.SetItemText(Index,t,((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;
}

void CNTSC_Option::Save_parameter(){

	CString str="";
	CString strTitle="";
	strTitle.Empty();
	strTitle="NTSC_INIT";

	WritePrivateProfileString(str_model,NULL,"",H_filename);
	str.Empty();
	str.Format("%d",m_INFO.ID);
	WritePrivateProfileString(str_model,"ID",str,H_filename);
	str.Empty();
	str.Format("%s",m_INFO.MODE);
	WritePrivateProfileString(str_model,"MODE",str,H_filename);
	str.Empty();
	str.Format("%s",m_INFO.NAME);
	WritePrivateProfileString(str_model,"NAME",str,H_filename);
	
	str.Empty();
	str.Format("%6.2f",d_Vppmin);
	WritePrivateProfileString(str_model,strTitle+"VppMin",str,H_filename);
	str.Empty();
	str.Format("%6.2f",d_Vppmax);
	WritePrivateProfileString(str_model,strTitle+"VppMax",str,H_filename);

	/*str.Empty();
	str.Format("%6.2f",d_Vsyncmin);
	WritePrivateProfileString(str_model,strTitle+"VSyncMin",str,H_filename);
	str.Empty();
	str.Format("%6.2f",d_Vsyncmax);
	WritePrivateProfileString(str_model,strTitle+"VSyncMax",str,H_filename);

	str.Empty();
	str.Format("%d",m_checkVpp);
	WritePrivateProfileString(str_model,strTitle+"VPP_ENABLE",str,H_filename);
	
	str.Empty();
	str.Format("%d",m_checkVsync);
	WritePrivateProfileString(str_model,strTitle+"VSYNC_ENABLE",str,H_filename);*/
	
}


void CNTSC_Option::Load_parameter(){
	CString str ="";

	d_Vppmin=GetPrivateProfileDouble(str_model,strTitle+"VppMin",-1,H_filename);
	if(d_Vppmin == -1){
		d_Vppmin =0.5;
		str.Empty();
		str.Format("%6.2f",d_Vppmin);
		WritePrivateProfileString(str_model,strTitle+"VppMin",str,H_filename);
	}
	d_Vppmax=GetPrivateProfileDouble(str_model,strTitle+"VppMax",-1,H_filename);
	if(d_Vppmax == -1){
		d_Vppmax =1.5;
		str.Empty();
		str.Format("%6.2f",d_Vppmax);
		WritePrivateProfileString(str_model,strTitle+"VppMax",str,H_filename);
	}
	/*d_Vsyncmin=GetPrivateProfileDouble(str_model,strTitle+"VSyncMin",-1,H_filename);
	if(d_Vsyncmin == -1){
		d_Vsyncmin =0.2;
		str.Empty();
		str.Format("%6.2f",d_Vsyncmin);
		WritePrivateProfileString(str_model,strTitle+"VSyncMin",str,H_filename);
	
	}
	d_Vsyncmax=GetPrivateProfileDouble(str_model,strTitle+"VSyncMax",-1,H_filename);

	if(d_Vsyncmax == -1){
		d_Vsyncmax =1.2;
		str.Empty();
		str.Format("%6.2f",d_Vsyncmax);
		WritePrivateProfileString(str_model,strTitle+"VSyncMax",str,H_filename);
	}

	m_checkVpp =GetPrivateProfileInt(str_model,strTitle+"VPP_ENABLE",-1,H_filename);
	if(m_checkVpp ==-1){
		m_checkVpp = 1;
		str.Empty();
		str.Format("%d",m_checkVpp);
		WritePrivateProfileString(str_model,strTitle+"VPP_ENABLE",str,H_filename);
	}

	m_checkVsync  =GetPrivateProfileInt(str_model,strTitle+"VSYNC_ENABLE",-1,H_filename);
	if(m_checkVsync  ==-1){
		m_checkVsync  = 1;
		str.Empty();
		str.Format("%d",m_checkVsync);
		WritePrivateProfileString(str_model,strTitle+"VSYNC_ENABLE",str,H_filename);
	}*/
	
	str_VppMin.Format("%3.1f",d_Vppmin);
	str_VppMax.Format("%3.1f",d_Vppmax);
	/*str_VSyncMin.Format("%3.1f",d_Vsyncmin);
	str_VSyncMax.Format("%3.1f",d_Vsyncmax);*/
	
}
void CNTSC_Option::NTSCGen(){//마이컴으로부터 PEAK데이터를 입력 받는다. 
	

	((CImageTesterDlg *)m_pMomWnd)->Video_Sync_Detect();

	for(int lop = 0;lop<20;lop++){
		DoEvents(100);
		if(((CImageTesterDlg *)m_pMomWnd)->VSync_Wait == FALSE){
			break;
		}
	}
	
	if(((CImageTesterDlg *)m_pMomWnd)->VSync_Wait == FALSE){
		d_Vpp= ((CImageTesterDlg *)m_pMomWnd)->m_Voltpp/1000.0;
	}else{
		((CImageTesterDlg *)m_pMomWnd)->VSync_Wait = FALSE;
		d_Vpp= -1;
	}
}

char CNTSC_Option::SendData8Byte(unsigned char * TxData, int Num)
{
	return ((CImageTesterDlg *)m_pMomWnd)->SendData8Byte_Cam(TxData,Num);
}

tResultVal CNTSC_Option::Run()
{	
	tResultVal retval = {0,};
	m_Etc_State = 0;
	b_StopFail = FALSE;
	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE){
		InsertList();
	}
	
	if(!((CImageTesterDlg *)m_pMomWnd)->PortChk_Cam()){//정상적으로포트가연결되어있지않으면
		if(!((CImageTesterDlg *)m_pMomWnd)->Portopen_Cam()){
			retval.m_Success = FALSE;
			m_NTSCList.SetItemText(InsertIndex,4,"");
			m_NTSCList.SetItemText(InsertIndex,5,"FAIL");
			m_NTSCList.SetItemText(InsertIndex,6,"PORT DISCONNECT");
			Str_Mes[0] = "0.0";
			Str_Mes[1] = "FAIL";
			retval.ValString = "PORT DISCONNECT";
			return retval;
		}
	}
	
	NTSCGen();
	if(d_Vpp==-1){
		m_Etc_State = 2;
		C_RECT[0].m_Success = FALSE;
		d_Vpp=0;
	}else{
		m_Etc_State = 1;
	}

	CString str="";
	BOOL FLAG = FALSE;
//	if(m_checkVpp  ==1){ 
	str.Empty();
	str.Format("%6.2f",d_Vpp);
	Str_Mes[0] = str;
	m_NTSCList.SetItemText(InsertIndex,4,str);
	((CImageTesterDlg *)m_pMomWnd)->m_pResCNTSCOptWnd->VPPNUM_TEXT(str);
	if((d_Vpp > d_Vppmin) && (d_Vpp < d_Vppmax)){
		((CImageTesterDlg *)m_pMomWnd)->m_pResCNTSCOptWnd->VPP_RESULT_TEXT(1,"PASS");
		Str_Mes[1] = "1";
	//	m_NTSCList.SetItemText(InsertIndex,5,"PASS");
		FLAG = TRUE;
	}else{
	//	m_NTSCList.SetItemText(InsertIndex,5,"FAIL");
		((CImageTesterDlg *)m_pMomWnd)->m_pResCNTSCOptWnd->VPP_RESULT_TEXT(2,"FAIL");
		Str_Mes[1] = "0";
		FLAG = FALSE;
	}
	

	/*}else{
			m_NTSCList.SetItemText(InsertIndex,4,"X");
			m_NTSCList.SetItemText(InsertIndex,5,"X");
	}*/
	/*BOOL FLAG2 = FALSE;
	if(m_checkVsync  ==1){
		str.Empty();
		str.Format("%6.2f",d_VSync);
		m_NTSCList.SetItemText(InsertIndex,6,str);
		((CImageTesterDlg *)m_pMomWnd)->m_pResCNTSCOptWnd->SYNCNUM_TEXT(str);
		if((d_VSync > d_Vsyncmin) && (d_VSync < d_Vsyncmax)){
			((CImageTesterDlg *)m_pMomWnd)->m_pResCNTSCOptWnd->SYNC_RESULT_TEXT(1,"PASS");
			m_NTSCList.SetItemText(InsertIndex,7,"PASS");
			FLAG2 = TRUE;
		}
		else{
			((CImageTesterDlg *)m_pMomWnd)->m_pResCNTSCOptWnd->SYNC_RESULT_TEXT(2,"FAIL");
			m_NTSCList.SetItemText(InsertIndex,7,"FAIL");
			FLAG2 = FALSE;
		}
	}else{
			m_NTSCList.SetItemText(InsertIndex,6,"X");
			m_NTSCList.SetItemText(InsertIndex,7,"X");
	}*/

	/*if((m_checkVpp  ==1) &&(m_checkVsync  ==1)){
		if((FLAG== TRUE) &&(FLAG2== TRUE)){
			retval.ValString.Format("Vpp OK , VSync OK");
			C_RECT[0].m_Success = TRUE;
		}
		else if((FLAG == TRUE) && (FLAG2 ==FALSE)){
			retval.ValString.Format("Vpp OK , VSync FAIL");
			C_RECT[0].m_Success = FALSE;
		}
		else if((FLAG == FALSE) && (FLAG2 ==TRUE)){
			retval.ValString.Format("Vpp FAIL , VSync OK");
			C_RECT[0].m_Success = FALSE;
		}else{
			retval.ValString.Format("Vpp FAIL , VSync FAIL");
			C_RECT[0].m_Success = FALSE;
		}

		if(((CImageTesterDlg *)m_pMomWnd)->b_UserMode == 1){
			retval.ValString.Empty();
			retval.ValString.Format("Vpp -> %6.2f , VSync -> %6.2f",d_Vpp,d_VSync);
		}
	}
	else if(m_checkVpp  ==1){*/
	if((FLAG== TRUE)){
		retval.ValString.Format("Vpp OK ");
		C_RECT[0].m_Success = TRUE;
		retval.m_Success = TRUE;
	}else{
		retval.ValString.Format("Vpp FAIL ");
		C_RECT[0].m_Success = FALSE;
		retval.m_Success = FALSE;
	}

	if(((CImageTesterDlg *)m_pMomWnd)->b_UserMode == 1){
		retval.ValString.Empty();
		retval.ValString.Format("Vpp -> %6.2f ",d_Vpp);
	}
	/*}
	else if(m_checkVsync == 1){
		if((FLAG2== TRUE)){
			retval.ValString.Format("VSync OK ");
			C_RECT[0].m_Success = TRUE;
		}else{
			retval.ValString.Format("VSync FAIL ");
			C_RECT[0].m_Success = FALSE;
		}
		if(((CImageTesterDlg *)m_pMomWnd)->b_UserMode == 1){
			retval.ValString.Empty();
			retval.ValString.Format("VSync -> %6.2f",d_VSync);
			}
			}*/
	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

		if(C_RECT[0].m_Success  == TRUE/* && SHORT_CHECK ==TRUE*/){
			retval.m_Success = TRUE;
			m_NTSCList.SetItemText(InsertIndex,5,"PASS");
			//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_PEAK,"PASS");
		}else{
			retval.m_Success = FALSE;
			m_NTSCList.SetItemText(InsertIndex,5,"FAIL");
			//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_PEAK,"FAIL");
		}


		StartCnt++;
	}
	return retval;

}	

void CNTSC_Option::InitPrm()
{
	Load_parameter();
	UpdateData(FALSE);
}


bool CNTSC_Option::NTSCPic(CDC *cdc){
	
	if(m_Etc_State == 0){
		return TRUE;
	}

	CPen	my_Pan,*old_pan;
	CString TEXTDATA ="";
	CFont m_font;
	
	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	
	if(C_RECT[0].m_Success  == TRUE){
		my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(BLUE_COLOR);

	}else{
		my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(RED_COLOR);
	}

    
	m_font.CreatePointFont(220,"Arial");
	cdc->SelectObject(&m_font);
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);

//	if(d_Vpp == -1 || d_VSync == -1){
	if(m_Etc_State == 2){	
		TEXTDATA.Format("측정불가");
	}else{
		TEXTDATA.Format("Vpp -> %6.2f",d_Vpp);
		/*if((m_checkVpp  ==1) &&(m_checkVsync  ==1)){
			TEXTDATA.Format("Vpp -> %6.2f ,  VSync -> %6.2f",d_Vpp,d_VSync);
		}
		else if(m_checkVpp  ==1){
			TEXTDATA.Format("Vpp -> %6.2f",d_Vpp);
		}
		else if(m_checkVsync == 1){
			TEXTDATA.Format("VSync -> %6.2f",d_VSync);
		}*/
		
	}
	
	cdc->TextOut(360,440,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
	
	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();
	m_font.DeleteObject();
	
	return TRUE;
}
//--------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------WORKLIST
void CNTSC_Option::Set_List(CListCtrl *List){
	
	CString str ="";
	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Vpp",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"result",LVCFMT_CENTER, 80);
//	List->InsertColumn(6,"VSync",LVCFMT_CENTER, 80);
//	List->InsertColumn(7,"result",LVCFMT_CENTER, 80);
//	List->InsertColumn(8,"TOTAL_RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"비고",LVCFMT_CENTER, 80);
	
	ListItemNum=7;
	
	Copy_List(&m_NTSCList,((CImageTesterDlg *)m_pMomWnd)->m_pWorkList,ListItemNum);

}

void CNTSC_Option::LOT_Set_List(CListCtrl *List){
	
	CString str ="";
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Vpp",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"result",LVCFMT_CENTER, 80);
//	List->InsertColumn(6,"VSync",LVCFMT_CENTER, 80);
//	List->InsertColumn(7,"result",LVCFMT_CENTER, 80);
//	List->InsertColumn(8,"TOTAL_RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"비고",LVCFMT_CENTER, 80);
	
	Lot_InsertNum=7;
	

}

void CNTSC_Option::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	InsertIndex = m_NTSCList.InsertItem(StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_NTSCList.SetItemText(InsertIndex,0,strCnt);
	
	m_NTSCList.SetItemText(InsertIndex,1,((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_NTSCList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_NTSCList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");
}

void CNTSC_Option::FAIL_UPLOAD(){
	if ((((CImageTesterDlg *)m_pMomWnd)->LotMod == 1) && (((CImageTesterDlg  *)m_pMomWnd)->b_StartCommand == 1)){
		InsertList();
		m_NTSCList.SetItemText(InsertIndex,5,"FAIL");
		m_NTSCList.SetItemText(InsertIndex,6,"STOP");
		//((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_PEAK,"STOP");
		StartCnt++;
		b_StopFail = TRUE;
	}
}

CString CNTSC_Option::Mes_Result()
{
	CString sz;

	m_szMesResult = _T("");
	if(b_StopFail == FALSE){
		m_szMesResult.Format(_T("%s:%s"),Str_Mes[0],Str_Mes[1]);
	}else{
		m_szMesResult.Format(_T(":1"));	
	}
	return m_szMesResult;
}

void CNTSC_Option::EXCEL_SAVE(){
	CString Item[4]={"Vpp","result"};//,"VSync","result"};
	CString Data ="";
	CString str="";
	str = "***********************NTSC(Vpp) 검사***********************\n";
	Data += str;
	str.Empty();
	str.Format("Vpp,최소 ,%6.2f ,,최대,%6.2f,,\n",d_Vppmin,d_Vppmax);
	Data += str;
	/*str.Empty();
	str.Format("VSync,최소 ,%6.2f ,,최대, %6.2f,,\n",d_Vsyncmin,d_Vsyncmax);
	Data += str;*/
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("NTSC검사",Item,2,&m_NTSCList,Data);
}

void CNTSC_Option::LOT_EXCEL_SAVE()
{
	CString Item[4]={"Vpp","result"};//,"VSync","result"};
	CString Data ="";
	CString str="";
	str = "***********************NTSC(Vpp) 검사***********************\n";
	Data += str;
	str.Empty();
	str.Format("Vpp,최소 ,%6.2f ,,최대,%6.2f,,\n",d_Vppmin,d_Vppmax);
	Data += str;
	/*str.Empty();
	str.Format("VSync,최소 ,%6.2f ,,최대, %6.2f,,\n",d_Vsyncmin,d_Vsyncmax);
	Data += str;*/
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("NTSC검사",Item,2,&m_Ntsc_Worklist,Data);
}

bool CNTSC_Option::EXCEL_UPLOAD(){
	m_NTSCList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("NTSC검사",&m_NTSCList,1,&StartCnt)==TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
}

bool CNTSC_Option::LOT_EXCEL_UPLOAD(){
	m_NTSCList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("NTSC검사",&m_Ntsc_Worklist,1,&StartCnt)==TRUE){
		return TRUE;
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
}


void CNTSC_Option::Pic(CDC *cdc)
{
	NTSCPic(cdc);
}

BOOL CNTSC_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에특수화된코드를추가및/또는기본클래스를호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CNTSC_Option::OnBnClickedButtonVppSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString str = "";
	d_Vppmin = atof(str_VppMin);
	d_Vppmax = atof(str_VppMax);

	if(d_Vppmin >d_Vppmax){
		d_Vppmax = d_Vppmin;
		str_VppMax.Format("%3.1f",d_Vppmax);
	}
	UpdateData(FALSE);
	str.Empty();
	str.Format("%6.2f",d_Vppmin);
	WritePrivateProfileString(str_model,strTitle+"VppMin",str,H_filename);
	str.Empty();
	str.Format("%6.2f",d_Vppmax);
	WritePrivateProfileString(str_model,strTitle+"VppMax",str,H_filename);
	((CButton *)GetDlgItem(IDC_BUTTON_VPP_SAVE))->EnableWindow(0);
}

void CNTSC_Option::OnBnClickedButtonVsyncSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	CString str="";
	
	d_Vsyncmin= atof(str_VSyncMin); 
	d_Vsyncmax= atof(str_VSyncMax); 

	if(d_Vsyncmin >d_Vsyncmax){
		d_Vsyncmax = d_Vsyncmin;
		str_VSyncMax.Format("%3.1f",d_Vsyncmax);
	}
	UpdateData(FALSE);
	str.Empty();
	str.Format("%6.2f",d_Vsyncmin);
	WritePrivateProfileString(str_model,strTitle+"VSyncMin",str,H_filename);
	str.Empty();
	str.Format("%6.2f",d_Vsyncmax);
	WritePrivateProfileString(str_model,strTitle+"VSyncMax",str,H_filename);
	((CButton *)GetDlgItem(IDC_BUTTON_VSYNC_SAVE))->EnableWindow(0);
	
	
}

void CNTSC_Option::OnEnChangeEditMinVpp()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	double buf1 = atof(str_VppMin);
	double buf2 = atof(str_VppMax);

	if((buf1 >=0)&&(buf2 >= 0)&&(buf1<=buf2)&&(buf2 <= 4)){
		if((buf1 != d_Vppmin)||(buf2 != d_Vppmax)){
			((CButton *)GetDlgItem(IDC_BUTTON_VPP_SAVE))->EnableWindow(1);
		}else{
			((CButton *)GetDlgItem(IDC_BUTTON_VPP_SAVE))->EnableWindow(0);
		}
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_VPP_SAVE))->EnableWindow(0);
	}
	
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CNTSC_Option::OnEnChangeEditMaxVpp()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	double buf1 = atof(str_VppMin);
	double buf2 = atof(str_VppMax);

	if((buf1 >=0)&&(buf2 >= 0)&&(buf1<=buf2)&&(buf2 <= 4)){
		if((buf1 != d_Vppmin)||(buf2 != d_Vppmax)){
			((CButton *)GetDlgItem(IDC_BUTTON_VPP_SAVE))->EnableWindow(1);
		}else{
			((CButton *)GetDlgItem(IDC_BUTTON_VPP_SAVE))->EnableWindow(0);
		}
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_VPP_SAVE))->EnableWindow(0);
	}
	
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CNTSC_Option::OnEnChangeEditMinVsync()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	double buf1 = atof(str_VSyncMin);
	double buf2 = atof(str_VSyncMax);

	if((buf1 >=0)&&(buf2 >= 0)&&(buf1<=buf2)&&(buf2 <= 4)){
		if((buf1 != d_Vsyncmin)||(buf2 !=  d_Vsyncmax)){
			((CButton *)GetDlgItem(IDC_BUTTON_VSYNC_SAVE))->EnableWindow(1);
		}else{
			((CButton *)GetDlgItem(IDC_BUTTON_VSYNC_SAVE))->EnableWindow(0);
		}
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_VSYNC_SAVE))->EnableWindow(0);
	}
	
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CNTSC_Option::OnEnChangeEditMaxVsync()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	double buf1 = atof(str_VSyncMin);
	double buf2 = atof(str_VSyncMax);

	if((buf1 >=0)&&(buf2 >= 0)&&(buf1<=buf2)&&(buf2 <= 4)){
		if((buf1 != d_Vsyncmin)||(buf2 !=  d_Vsyncmax)){
			((CButton *)GetDlgItem(IDC_BUTTON_VSYNC_SAVE))->EnableWindow(1);
		}else{
			((CButton *)GetDlgItem(IDC_BUTTON_VSYNC_SAVE))->EnableWindow(0);
		}
	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_VSYNC_SAVE))->EnableWindow(0);
	}
	
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CNTSC_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if(bShow == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
	}
}

void CNTSC_Option::OnBnClickedCheckVpp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_checkVpp == 1){
		m_checkVpp =0;
	}
	else{
		m_checkVpp =1;
	}
	UpdateData(FALSE);
	CString str="";

	str.Empty();
	str.Format("%d",m_checkVpp);
	WritePrivateProfileString(str_model,strTitle+"VPP_ENABLE",str,H_filename);
}

void CNTSC_Option::OnBnClickedCheckVsync()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_checkVsync == 1){
		m_checkVsync =0;
	}
	else{
		m_checkVsync =1;
	}
	UpdateData(FALSE);
	CString str="";

	str.Empty();
	str.Format("%d",m_checkVsync);
	WritePrivateProfileString(str_model,strTitle+"VSYNC_ENABLE",str,H_filename);
}


void CNTSC_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
	//	((CButton *)GetDlgItem(IDC_CHECK_VPP))->EnableWindow(0);
	//	((CButton *)GetDlgItem(IDC_CHECK_VSYNC))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_VPP_SAVE))->EnableWindow(0);
	//	((CButton *)GetDlgItem(IDC_BUTTON_VSYNC_SAVE))->EnableWindow(0);
		
		((CEdit *)GetDlgItem(IDC_EDIT_MIN_VPP))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_MAX_VPP))->EnableWindow(0);
	//	((CEdit *)GetDlgItem(IDC_EDIT_MIN_VSYNC))->EnableWindow(0);
	//	((CEdit *)GetDlgItem(IDC_EDIT_MAX_VSYNC))->EnableWindow(0);
	}
}