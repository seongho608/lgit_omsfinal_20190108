// OverlayPosition_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "OverlayPosition_ResultView.h"


// COverlayPosition_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(COverlayPosition_ResultView, CDialog)

COverlayPosition_ResultView::COverlayPosition_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(COverlayPosition_ResultView::IDD, pParent)
{

}

COverlayPosition_ResultView::~COverlayPosition_ResultView()
{
}

void COverlayPosition_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(COverlayPosition_ResultView, CDialog)
	ON_WM_CTLCOLOR()
//	ON_EN_CHANGE(IDC_EDIT_OVERLAYRESULT, &COverlayPosition_ResultView::OnEnChangeEditOverlayresult)
END_MESSAGE_MAP()

// COverlayPosition_ResultView 메시지 처리기입니다.
void COverlayPosition_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void COverlayPosition_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;
	
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}
HBRUSH COverlayPosition_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd->GetDlgCtrlID() == IDC_EDIT_OVERLAY_LEFTTXT || pWnd->GetDlgCtrlID() == IDC_EDIT_OVERLAY_RIGHTTXT)
	{
		pDC->SetTextColor(RGB(255, 255, 255));
		pDC->SetBkColor(RGB(86,86,86));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_OVERLAY_LEFTNUM || pWnd->GetDlgCtrlID() == IDC_EDIT_OVERLAY_RIGHTNUM)
	{
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255,255,255));
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_OVERLAY_LEFTRESULT)
	{
		pDC->SetTextColor(txcol_TVal[0]);
		pDC->SetBkColor(bkcol_TVal[0]);
	}

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_OVERLAY_RIGHTRESULT)
	{
		pDC->SetTextColor(txcol_TVal[1]);
		pDC->SetBkColor(bkcol_TVal[1]);
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

BOOL COverlayPosition_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_EDIT_OVERLAY_LEFTTXT,&ft1,40,20);
	GetDlgItem(IDC_EDIT_OVERLAY_LEFTRESULT)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_OVERLAY_LEFTNUM)->SetFont(&ft1);

	GetDlgItem(IDC_EDIT_OVERLAY_RIGHTTXT)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_OVERLAY_RIGHTRESULT)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_OVERLAY_RIGHTNUM)->SetFont(&ft1);

	Initstat();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void COverlayPosition_ResultView::OVERLAY_LEFT_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_OVERLAY_LEFTTXT))->SetWindowText(lpcszString);
}

void COverlayPosition_ResultView::OVERLAY_LEFT_NUM_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_OVERLAY_LEFTNUM))->SetWindowText(lpcszString);
}

void COverlayPosition_ResultView::RESULT_LEFT_TEXT(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0)		//대기
	{
		txcol_TVal[0] = RGB(255, 255, 255);
		bkcol_TVal[0] = RGB(47, 157, 39);
	}else if(col == 1)	//성공
	{
		txcol_TVal[0] = RGB(255, 255, 255);
		bkcol_TVal[0] = RGB(18,  69,171);
	}else if(col == 2)	//실패
	{
		txcol_TVal[0] = RGB(255, 255, 255);
		bkcol_TVal[0] = RGB(201, 0, 0);
	}else if(col == 3)
	{
		txcol_TVal[0] = RGB(0, 0, 0);
		bkcol_TVal[0] = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_OVERLAY_LEFTRESULT))->SetWindowText(lpcszString);
}

void COverlayPosition_ResultView::OVERLAY_RIGHT_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_OVERLAY_RIGHTTXT))->SetWindowText(lpcszString);
}

void COverlayPosition_ResultView::OVERLAY_RIGHT_NUM_TEXT(LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_OVERLAY_RIGHTNUM))->SetWindowText(lpcszString);
}

void COverlayPosition_ResultView::RESULT_RIGHT_TEXT(unsigned char col,LPCSTR lpcszString, ...)
{	
	if(col == 0)		//대기
	{
		txcol_TVal[1] = RGB(255, 255, 255);
		bkcol_TVal[1] = RGB(47, 157, 39);
	}else if(col == 1)	//성공
	{
		txcol_TVal[1] = RGB(255, 255, 255);
		bkcol_TVal[1] = RGB(18,  69,171);
	}else if(col == 2)	//실패
	{
		txcol_TVal[1] = RGB(255, 255, 255);
		bkcol_TVal[1] = RGB(201, 0, 0);
	}else if(col == 3)
	{
		txcol_TVal[1] = RGB(0, 0, 0);
		bkcol_TVal[1] = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_OVERLAY_RIGHTRESULT))->SetWindowText(lpcszString);
}

void	COverlayPosition_ResultView::Initstat()
{
	RESULT_LEFT_TEXT(0,"STAND BY");
	RESULT_RIGHT_TEXT(0,"STAND BY");

	OVERLAY_LEFT_NUM_TEXT("");
	OVERLAY_RIGHT_NUM_TEXT("");

	OVERLAY_LEFT_TEXT("좌측");
	OVERLAY_RIGHT_TEXT("우측");
}

BOOL COverlayPosition_ResultView::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN){
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
