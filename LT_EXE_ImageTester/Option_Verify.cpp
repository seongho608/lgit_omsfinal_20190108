// Option_Verify.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Option_Verify.h"


// COption_Verify 대화 상자입니다.

IMPLEMENT_DYNAMIC(COption_Verify, CDialog)

COption_Verify::COption_Verify(CWnd* pParent /*=NULL*/)
	: CDialog(COption_Verify::IDD, pParent)
	, str_binFile_path(_T(""))
	, b_Check_ViewList(FALSE)
	, m_MaxByte(_T(""))
	, SAVEFILE_PATH(_T(""))
{

}

COption_Verify::~COption_Verify()
{
}

void COption_Verify::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_BINFILE, str_binFile_path);
	DDX_Check(pDX, IDC_CHECK_View_List, b_Check_ViewList);
	DDX_Control(pDX, IDC_LIST_BIN, m_ctrlBINDataList);
	DDX_Control(pDX, IDC_PROGRESS_ER, m_ProgressFileRead);
	DDX_Text(pDX, IDC_EDIT_MAXBYTE, m_MaxByte);
	DDX_Control(pDX, IDC_PROGRESS_TEST, m_ProgressTest);
	DDX_Text(pDX, IDC_SAVEFILEPATH, SAVEFILE_PATH);
}


BEGIN_MESSAGE_MAP(COption_Verify, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_BIN_OPEN, &COption_Verify::OnBnClickedButtonBinOpen)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_CHECK_View_List, &COption_Verify::OnBnClickedCheckViewList)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &COption_Verify::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_test, &COption_Verify::OnBnClickedButtontest)
	ON_BN_CLICKED(IDC_BUTTON_BIN_OPEN2, &COption_Verify::OnBnClickedButtonBinOpen2)
END_MESSAGE_MAP()


// COption_Verify 메시지 처리기입니다.

void COption_Verify::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
	strTitle.Empty();
	strTitle="VERIFY_";
}


BOOL COption_Verify::OnInitDialog()
{
	CDialog::OnInitDialog();
	CFileFind		folderfind;
	str_ModelPath=((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;

	str_BinPath = ((CImageTesterDlg *)m_pMomWnd)->Overlay_path + "\\BIN";
	if(!folderfind.FindFile(str_BinPath)){ //원하는 폴더가 없으면 만든다.
		CreateDirectory(str_BinPath, NULL); 
	}
	SAVEFILE_PATH = ((CImageTesterDlg *)m_pMomWnd)->SAVEFILE_RPATH;
 	Font_Size_Change(IDC_EDIT_TEST_STAT,&ft2,100,38);

	Oldstr_binFile_path = "";
	m_ProgressFileRead.SetRange(0,1000);
	m_ProgressTest.SetRange(0,1000);
	TEST_STATE(0,"Stand By");

 	Load_parameter();

	SET_LIST();
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void COption_Verify::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;

	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);

	GetDlgItem(nID)->SetFont(font);
}
void COption_Verify::TEST_STATE(int col,LPCSTR lpcszString, ...)
{	
	((CEdit *)GetDlgItem(IDC_EDIT_TEST_STAT))->SetWindowText(lpcszString);
	if(col == 0){//스탠드 바이
		tx_st_col = RGB(255, 255, 255);
		bk_st_col = RGB(47, 157, 39);
	}else if(col == 1){//성공
		tx_st_col = RGB(255, 255, 255);
		bk_st_col = RGB(18, 69, 171);
	}else if(col == 2){//실패
		tx_st_col = RGB(255, 255, 255);
		bk_st_col = RGB(201, 0, 0);
	}else if(col == 3){
		tx_st_col = RGB(0, 0, 0);
		bk_st_col = RGB(255, 255, 0);
	}
}
void COption_Verify::OnBnClickedButtonBinOpen()
{
	CString GetFileName,GetFilepath,GetFolderPath;
	CString DstFile = "";

	CFileDialog dlg(TRUE,"bin","*.bin",OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT|OFN_ALLOWMULTISELECT, 
		"bin 파일 (*.bin)|*.bin|모든 파일 (*.*)|*.*|"); //bin파일 읽기

	if(dlg.DoModal() == IDOK)
	{
		GetFileName = dlg.GetFileName();//파일이름을 가져온다.
		GetFilepath = dlg.GetPathName();//폴더 및 파일경로를 가져온다.
		GetFolderPath = GetFilepath.Mid(0,GetFilepath.ReverseFind('\\'));//폴더를 가져온다.
		DstFile =	str_BinPath+ "\\"+ GetFileName /*+ ".bin"*/;

		WritePrivateProfileString("BINFILE","BIN",GetFileName,str_ModelPath);//변경된 폴더를 ini에 저장한다. 
		
		if(GetFilepath != DstFile){
			CopyFile(GetFilepath,DstFile,false);
		}

		str_binFile_path = GetFilepath;
		UpdateData(FALSE);


		BinFileLoad(b_Check_ViewList);
		Oldstr_binFile_path = DstFile;

	}
}
void COption_Verify::BinFileLoad(bool Mode)
{


	CString GetFileName,filedata,str;
	CFile BINFILE;   
	CFileStatus BINFILESTATUS;
	CFileException BINEX;
	unsigned int	lop = 0;


	m_ctrlBINDataList.DeleteAllItems();
	if (Mode ==0)
	{
		m_ctrlBINDataList.EnableWindow(FALSE);
	}else{
		m_ctrlBINDataList.EnableWindow(TRUE);

	}
	unsigned char	buffer[512];
	unsigned long	MAXSIZE,MAXADDR,CNTSIZE, BuffSize=0,nRet = 0,addresscnt = 0,listcnt = 0;
	unsigned long	CHKNUM = 0;
	BYTE			CHKXOR = 0X00;
	m_ProgressFileRead.SetPos(0);

	if(!BINFILE.Open(str_binFile_path, CFile::modeRead, &BINEX)){ //오류가 발생하면
		SetDlgItemText(IDC_BIN_SIZE,"");
		return ;
	}else{
		TEST_STATE(3,"Bin File Loading");
		memset(BINBUF,0xff,sizeof(BINBUF));
/*		((CEdit *)GetDlgItem(IDC_EDIT_FILE_READ_STATE))->SetWindowText("File Read");*/

		//if ( Mode ==0)
		//{
		//	BINFILE.GetStatus(BINFILESTATUS);
		//	MAXSIZE = BINFILESTATUS.m_size;//현재 bin파일의 크기를 잰다.
		//	//m_BINSIZE = MAXSIZE;
		//	MAXADDR = (unsigned long)(MAXSIZE/256);
		//	//	m_ctrlBINDataList.DeleteAllItems();
		//	for(CNTSIZE = 0;CNTSIZE < MAXSIZE;CNTSIZE+=512){
		//		m_ProgressFileRead.SetPos((unsigned int)((CNTSIZE*1000)/MAXSIZE));

		//		str.Format("0x%0.8X",addresscnt);
		//		//	m_ctrlBINDataList.InsertItem(listcnt,str);
		//		BuffSize = sizeof(buffer);	
		//		nRet = BINFILE.Read(buffer,BuffSize);//원하는 버퍼사이즈만큼 읽는다.
		//		for(lop = 0;lop < nRet;lop++){
		//			str.Format("%0.2X",buffer[lop]);
		//			BINBUF[CNTSIZE+lop] = buffer[lop];
		//			//		m_ctrlBINDataList.SetItemText(listcnt,lop+1,str);
		//			// 				CHKNUM += buffer[lop];
		//			// 				CHKNUM &= 0xffffffff;
		//			// 				CHKXOR ^= buffer[lop];
		//		}
		//		listcnt += 0x01;
		//		addresscnt += 0x10;
		//		if(CNTSIZE%65536 == 0){
		//			Sleep(1);
		//		}
		//	}
		//	//m_BinResultCode = (CHKNUM%10000)*100 + CHKXOR%100; //센서 바이너리 확인코드	
		//	//	m_BinChksumCode = CHKNUM;
		//	m_BinSIZE = MAXSIZE;
		//}else{
			BINFILE.GetStatus(BINFILESTATUS);
			MAXSIZE = BINFILESTATUS.m_size;//현재 bin파일의 크기를 잰다.
			//m_BINSIZE = MAXSIZE;
			MAXADDR = (unsigned long)(MAXSIZE/256);
			for(CNTSIZE = 0;CNTSIZE < MAXSIZE;CNTSIZE+=16){
				m_ProgressFileRead.SetPos((unsigned int)((CNTSIZE*1000)/MAXSIZE));
				str.Format("0x%0.8X",addresscnt);
				m_ctrlBINDataList.InsertItem(listcnt,str);
				BuffSize = 16;
				nRet = BINFILE.Read(buffer,BuffSize);//원하는 버퍼사이즈만큼 읽는다.
				for(lop = 0;lop < nRet;lop++){
					str.Format("%0.2X",buffer[lop]);
					BINBUF[CNTSIZE+lop] = buffer[lop];
					m_ctrlBINDataList.SetItemText(listcnt,lop+1,str);
					CHKNUM += buffer[lop];
					CHKNUM &= 0xffffffff;
					CHKXOR ^= buffer[lop];
				}
				listcnt += 0x01;
				addresscnt += 0x10;
				if(CNTSIZE%65536 == 0){
					Sleep(1);
				}
			}
// 			m_BinResultCode = (CHKNUM%10000)*100 + CHKXOR%100; //센서 바이너리 확인코드	
// 			m_BinChksumCode = CHKNUM;
 			m_BinSIZE = MAXSIZE;
			//TEST_STATE(1,"Read OK");//text_stat(2,"Bin File Load Fin");
		//}

		//text_stat(2,"Bin File Load Fin");


	}
	m_ProgressFileRead.SetPos(0);
	//((CEdit *)GetDlgItem(IDC_EDIT_FILE_READ_STATE))->SetWindowText("Read OK");
	TEST_STATE(1,"Read OK");
	str.Format("%d BYTE",m_BinSIZE);
	SetDlgItemText(IDC_BIN_SIZE,str);
// 	str.Format("%0.8X",m_BinChksumCode);
// 	SetDlgItemText(IDC_BIN_CHKSUM,str);
// 	str.Format("%0.6d",m_BinResultCode);
// 	SetDlgItemText(IDC_BIN_RESULTCODE,str);
	BINFILE.Close();

// 	CFile file;
// 	file.Open("D:\\Data.bin", CFile::modeWrite | CFile::modeCreate | CFile::shareDenyNone);
// 	file.Write(BINBUF, MAXSIZE);
// 	file.Close();

}

HBRUSH COption_Verify::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_TEST_STAT){
		pDC->SetBkColor(bk_st_col);
		pDC->SetTextColor(tx_st_col);
	}
	return hbr;
}

void COption_Verify::OnBnClickedCheckViewList()
{
	CString str = "";

	if (b_Check_ViewList == TRUE)
	{
		b_Check_ViewList  =FALSE;
	}else{
		b_Check_ViewList = TRUE;
	}

	str.Format("%d",b_Check_ViewList);
	WritePrivateProfileString("BINFILE","VIEWLIST",str,str_ModelPath);//변경된 폴더를 ini에 저장한다. 
	UpdateData(FALSE);
}
void COption_Verify::SET_LIST(void){

	//	m_ctrlBINDataList.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);
	m_ctrlBINDataList.InsertColumn(0, "Addr", LVCFMT_CENTER, 70);
	m_ctrlBINDataList.InsertColumn(1, "00", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(2, "01", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(3, "02", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(4, "03", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(5, "04", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(6, "05", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(7, "06", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(8, "07", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(9, "08", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(10, "09", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(11, "0A", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(12, "0B", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(13, "0C", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(14, "0D", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(15, "0E", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.InsertColumn(16, "0F", LVCFMT_CENTER, 37);
	m_ctrlBINDataList.EnableWindow(1);
}

void COption_Verify::Load_parameter(){
	CString	   str = "";
	CString strNUM;
	CFileFind filefind;	
	//	CString str = _T("");
	CString fnstr = _T("");

	int save=0;

	b_Check_ViewList = GetPrivateProfileInt("BINFILE","VIEWLIST",0,str_ModelPath);

	str.Format("%d",b_Check_ViewList);
	WritePrivateProfileString("BINFILE","VIEWLIST",str,str_ModelPath);//변경된 폴더를 ini에 저장한다. 

	str =GetPrivateProfileCString("BINFILE","BIN",str_ModelPath);//변경된 폴더를 ini에 저장한다. 
	if (str == "")
	{

	}else{
		str_binFile_path= str_BinPath+ "\\"+ str;

	}
	if(Oldstr_binFile_path != str_binFile_path)
	{
		BinFileLoad(0);
		Oldstr_binFile_path = str_binFile_path;
	}
	

	MaxByte = GetPrivateProfileInt("BINFILE","MAXBYTE",0,str_ModelPath);
	if(MaxByte==0){
		MaxByte = 255;
	}
	m_MaxByte.Format("");
	m_MaxByte.Format("%d",MaxByte);

	UpdateData(FALSE);
}

void COption_Verify::OnBnClickedButtonSave()
{
	UpdateData(TRUE);
	CString	   str = m_MaxByte;
	WritePrivateProfileString("BINFILE","MAXBYTE",str,str_ModelPath);//변경된 폴더를 ini에 저장한다.
	MaxByte = GetPrivateProfileInt("BINFILE","MAXBYTE",0,str_ModelPath);
	m_MaxByte.Format("");
	m_MaxByte.Format("%d",MaxByte);
	UpdateData(FALSE);
}
void COption_Verify::InitPrm()
{
	Load_parameter();
	UpdateData(FALSE);
}

tResultVal COption_Verify::Run()
{
	tResultVal retval = {0,};
	unsigned long DATA = 0;
	BYTE Address_DATA[8];
	BYTE ADDR[8];
	int count = 0;
	CString Scount,fileName;
	int a, num =0;
	CFileFind filefind;

	CTime time = CTime::GetTickCount(); 
	int YEAR=0,MONTH=0,DAY=0;
	YEAR = time.GetYear();
	YEAR = YEAR;
	MONTH = time.GetMonth();   
	DAY = time.GetDay(); 
	CTime thetime = CTime::GetCurrentTime();
	int hour=0,minute=0,second=0,hour2=0;
	//int year2 = YEAR-2000; 
	hour=thetime.GetHour(); minute=thetime.GetMinute(); second=thetime.GetSecond();

	memset(CAMBUF,0xff,sizeof(CAMBUF));

	m_ProgressTest.SetPos(0);

	((CImageTesterDlg *)m_pMomWnd)->m_pResVerifyOptWnd->m_ProgressTestRes.SetPos(0);
	((CImageTesterDlg *)m_pMomWnd)->m_pResVerifyOptWnd->VER_STATE(3,"TESTING...");
	TEST_STATE(3,"TESTING...");
	UpdateData(TRUE);
	if(str_binFile_path==""){
		retval.ValString.Empty();
		retval.ValString.Format("BIN FILE X");
		retval.m_Success = FALSE;
		((CImageTesterDlg *)m_pMomWnd)->m_pResVerifyOptWnd->VER_STATE(2,"Fail");
		TEST_STATE(2,"Fail");
		return retval;
	}
	if(MaxByte % 255){
		num = MaxByte / 255 + 1;
	}else{
		num = (MaxByte / 255);
	}

	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand==TRUE)
	{
		//InsertList();
	}else{
		((CImageTesterDlg *)m_pMomWnd)->Power_On();
		DoEvents(500);
	}

	if(((CImageTesterDlg *)m_pMomWnd)->MCU_READ_MODE(1) == FALSE)
	{

		retval.ValString.Empty();
		retval.ValString.Format("READ_OPEN FAIL");
		retval.m_Success = FALSE;
		((CImageTesterDlg *)m_pMomWnd)->MCU_READ_MODE(0);
		((CImageTesterDlg *)m_pMomWnd)->m_pResVerifyOptWnd->VER_STATE(2,"Fail");
		TEST_STATE(2,"Fail");
		return retval;

	}else
	{

		for(int lop=0; lop<=num;lop++)
		{
			for(int t=0; t<8; t++)
			{
			Address_DATA[7-t] = (DATA+(lop*255)>>(t*4))&0x0000000F;
			ADDR[7-t] =Hex2Ascii(Address_DATA[7-t]);
			}

			if(((CImageTesterDlg *)m_pMomWnd)->MCU_READ_ADDR(ADDR) == FALSE)
			{

				retval.ValString.Empty();
				retval.ValString.Format("READ_ADDR FAIL");
				retval.m_Success = FALSE;
				((CImageTesterDlg *)m_pMomWnd)->MCU_READ_MODE(0);
				((CImageTesterDlg *)m_pMomWnd)->m_pResVerifyOptWnd->VER_STATE(2,"Fail");
				TEST_STATE(2,"Fail");
				return retval;

			}else
			{			
				if(((CImageTesterDlg *)m_pMomWnd)->MCU_READ_START() == FALSE)
				{
					retval.ValString.Empty();
					retval.ValString.Format("READ_SET FAIL");
					retval.m_Success = FALSE;
					((CImageTesterDlg *)m_pMomWnd)->MCU_READ_MODE(0);
					((CImageTesterDlg *)m_pMomWnd)->m_pResVerifyOptWnd->VER_STATE(2,"Fail");
					TEST_STATE(2,"Fail");
					return retval;

				}else
				{	
					if(((CImageTesterDlg *)m_pMomWnd)->MCU_READ_START_SEND()== FALSE)
					{

						retval.ValString.Empty();
						retval.ValString.Format("READ_START FAIL");
						retval.m_Success = FALSE;
						((CImageTesterDlg *)m_pMomWnd)->MCU_READ_MODE(0);
						((CImageTesterDlg *)m_pMomWnd)->m_pResVerifyOptWnd->VER_STATE(2,"Fail");
						TEST_STATE(2,"Fail");
						return retval;

					}else
					{
						for(int i=0;i<255;i++)
						{
							if(((CImageTesterDlg *)m_pMomWnd)->CAMREADBUFF[i]!=BINBUF[(255*lop)+i]){
								count++;
							}
							if(i==254){
								retval.ValString.Empty();
								if(count==0){
									retval.ValString.Format("데이터가 모두 일치합니다.");
									retval.m_Success = TRUE;
								}else{
									retval.ValString.Format("%d BYTE의데이터가다릅니다.",count);
									retval.m_Success = FALSE;
								}

							}
							CAMBUF[(255*lop)+i] = ((CImageTesterDlg *)m_pMomWnd)->CAMREADBUFF[i];
						}

					}
					Wait(1);
				}
			}
			m_ProgressTest.SetPos((unsigned int)((lop*1000)/num));
			((CImageTesterDlg *)m_pMomWnd)->m_pResVerifyOptWnd->m_ProgressTestRes.SetPos((unsigned int)((lop*1000)/num));
		}
	}

	CString str = "";
	str.Format("\\%d_%02d_%02d",YEAR,MONTH,DAY);
	SAVEFILE_PATH=((CImageTesterDlg *)m_pMomWnd)->SAVEFILE_RPATH + str;
	if(filefind.FindFile(SAVEFILE_PATH)==FALSE)
	{
		CreateDirectory(SAVEFILE_PATH, NULL);
	}
	if(retval.m_Success == FALSE){
		str.Format("\\%02d_%02d_%02d_FAIL.bin",hour,minute,second);
	}else{
		str.Format("\\%02d_%02d_%02d.bin",hour,minute,second);
	}

	fileName = SAVEFILE_PATH + str;

	FILE *fp=fopen(fileName, "wb");

	for(int i=0; i < MaxByte; i++)
	{
		fwrite(&CAMBUF[i],sizeof(BYTE),(1),fp);

	}
	fclose(fp);
	
	if(retval.m_Success == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_pResVerifyOptWnd->VER_STATE(1,"Pass");
		TEST_STATE(1,"Pass");
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_pResVerifyOptWnd->VER_STATE(2,"Fail");
		TEST_STATE(2,"Fail");
	}

	((CImageTesterDlg *)m_pMomWnd)->MCU_READ_MODE(0);
	return retval;
}
void COption_Verify::OnBnClickedButtontest()
{
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(0);
	((CImageTesterDlg  *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();

	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(1);
}

BOOL COption_Verify::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void COption_Verify::OnBnClickedButtonBinOpen2()
{
	ITEMIDLIST        *pidlBrowse;
	char    SaveFolderPath[2046] = {0,}; 
	BROWSEINFO BrInfo;
	BrInfo.hwndOwner = NULL; 
	BrInfo.pidlRoot = NULL;
	memset( &BrInfo, 0, sizeof(BrInfo) );
	BrInfo.pszDisplayName = SaveFolderPath;
	BrInfo.lpszTitle = "저장할 디렉토리를 선택하세요";
	BrInfo.ulFlags = BIF_RETURNONLYFSDIRS;

	// Show Dialog
	pidlBrowse = ::SHBrowseForFolder(&BrInfo);    

	if( pidlBrowse != NULL)
	{
		//   Get Path
		::SHGetPathFromIDList(pidlBrowse, SaveFolderPath);   
		SAVEFILE_PATH = SaveFolderPath;
		((CImageTesterDlg *)m_pMomWnd)->SAVEFILE_RPATH =SaveFolderPath;

		WritePrivateProfileString("SAVE_FILE_PATH",NULL,"",((CImageTesterDlg *)m_pMomWnd)->m_inifilename);	
		WritePrivateProfileString("SAVE_FILE_PATH","SAVE_FOL",SaveFolderPath,((CImageTesterDlg *)m_pMomWnd)->m_inifilename);
		UpdateData(FALSE);
		UpdateData(TRUE);
	}
}
