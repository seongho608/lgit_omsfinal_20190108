// Temperature_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Temperature_ResultView.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "afxdialogex.h"


// CTemperature_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTemperature_ResultView, CDialog)

CTemperature_ResultView::CTemperature_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CTemperature_ResultView::IDD, pParent)
{

}

CTemperature_ResultView::~CTemperature_ResultView()
{
}

void CTemperature_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd = IN_pMomWnd;
}

void CTemperature_ResultView::Font_Size_Change(int nID, CFont *font, LONG Weight, LONG Height)//나중에 전역변수로 간략화 할것임
{
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = Weight;

	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);

	GetDlgItem(nID)->SetFont(font);
}
void CTemperature_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTemperature_ResultView, CDialog)
	ON_WM_CTLCOLOR()
	ON_EN_SETFOCUS(IDC_EDIT_DepthNoiseTXT, &CTemperature_ResultView::OnEnSetfocusEditTemperaturetxt)
	ON_EN_SETFOCUS(IDC_EDIT_DepthNoiseNUM2, &CTemperature_ResultView::OnEnSetfocusEditTemperaturenum)
	ON_EN_SETFOCUS(IDC_EDIT_DepthNoiseRESULT, &CTemperature_ResultView::OnEnSetfocusEditTemperatureresult)
END_MESSAGE_MAP()


// CTemperature_ResultView 메시지 처리기입니다.

BOOL CTemperature_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_EDIT_TemperatureTXT, &ft1, 40, 20);
	GetDlgItem(IDC_EDIT_TemperatureRESULT)->SetFont(&ft1);
	GetDlgItem(IDC_EDIT_TemperatureNUM2)->SetFont(&ft1);

	Initstat();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

HBRUSH CTemperature_ResultView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	if (pWnd->GetDlgCtrlID() == IDC_EDIT_TemperatureTXT){
		pDC->SetTextColor(RGB(255, 255, 255));

		pDC->SetBkColor(RGB(86, 86, 86));
	}


	if (pWnd->GetDlgCtrlID() == IDC_EDIT_TemperatureNUM2){
		pDC->SetTextColor(RGB(86, 86, 86));
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	if (pWnd->GetDlgCtrlID() == IDC_EDIT_TemperatureRESULT){
		pDC->SetTextColor(txcol_TVal);
		pDC->SetBkColor(bkcol_TVal);
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CTemperature_ResultView::Temperature_TEXT(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_TemperatureTXT))->SetWindowText(lpcszString);
}
void CTemperature_ResultView::TemperatureNUM_TEXT(LPCSTR lpcszString, ...)
{
	((CEdit *)GetDlgItem(IDC_EDIT_TemperatureNUM2))->SetWindowText(lpcszString);
}

void CTemperature_ResultView::RESULT_TEXT(unsigned char col, LPCSTR lpcszString, ...)
{
	if (col == 0){//대기
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(47, 157, 39);
	}
	else if (col == 1){//성공
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(18, 69, 171);
	}
	else if (col == 2){//실패
		txcol_TVal = RGB(255, 255, 255);
		bkcol_TVal = RGB(201, 0, 0);
	}
	else if (col == 3){
		txcol_TVal = RGB(0, 0, 0);
		bkcol_TVal = RGB(255, 255, 0);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_TemperatureRESULT))->SetWindowText(lpcszString);
}
void	CTemperature_ResultView::Initstat()
{
	RESULT_TEXT(0, "STAND BY");


	TemperatureNUM_TEXT("");
	Temperature_TEXT("Temperature");
}

void CTemperature_ResultView::OnEnSetfocusEditTemperaturetxt()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CTemperature_ResultView::OnEnSetfocusEditTemperaturenum()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CTemperature_ResultView::OnEnSetfocusEditTemperatureresult()
{
	((CImageTesterDlg  *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

BOOL CTemperature_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}
