// StandDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "StandDlg.h"


// CStandDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CStandDlg, CDialog)

CStandDlg::CStandDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CStandDlg::IDD, pParent)
{

}

CStandDlg::~CStandDlg()
{
}

void CStandDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CStandDlg, CDialog)
	ON_WM_CTLCOLOR()
	//ON_EN_SETFOCUS(IDC_TOTAL, &CStandDlg::OnEnSetfocusTotal)
	//ON_EN_SETFOCUS(IDC_TOTAL2, &CStandDlg::OnEnSetfocusTotal2)
	//ON_EN_SETFOCUS(IDC_PASS, &CStandDlg::OnEnSetfocusPass)
	//ON_EN_SETFOCUS(IDC_PASS2, &CStandDlg::OnEnSetfocusPass2)
	//ON_EN_SETFOCUS(IDC_FAIL, &CStandDlg::OnEnSetfocusFail)
	//ON_EN_SETFOCUS(IDC_FAIL2, &CStandDlg::OnEnSetfocusFail2)
	//ON_EN_SETFOCUS(IDC_PERCENT, &CStandDlg::OnEnSetfocusPercent)
	//ON_EN_SETFOCUS(IDC_PERCENT2, &CStandDlg::OnEnSetfocusPercent2)
	//ON_EN_SETFOCUS(IDC_EDIT_CPK, &CStandDlg::OnEnSetfocusEditCpk)
	//ON_EN_SETFOCUS(IDC_EDIT_CPK_X, &CStandDlg::OnEnSetfocusEditCpkX)
	//ON_EN_SETFOCUS(IDC_EDIT_CPK_Y, &CStandDlg::OnEnSetfocusEditCpkY)
	ON_EN_SETFOCUS(IDC_STAT_EDIT, &CStandDlg::OnEnSetfocusStatEdit)
END_MESSAGE_MAP()


// CStandDlg 메시지 처리기입니다.

void CStandDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

BOOL CStandDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_pStandEdit = (CEdit*)GetDlgItem(IDC_STAT_EDIT);



	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CStandDlg::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight;
	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);
	
	GetDlgItem(nID)->SetFont(font);
}

HBRUSH CStandDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	return hbr;
}

void CStandDlg::CPK_TEXT(LPCSTR lpcszString, ...)       
{	
	//((CEdit *)GetDlgItem(IDC_EDIT_CPK))->SetWindowText(lpcszString);
}
void CStandDlg::CPK_X(LPCSTR lpcszString, ...)       
{	///
	//((CEdit *)GetDlgItem(IDC_EDIT_CPK_X))->SetWindowText(lpcszString);
}
void CStandDlg::CPK_Y(LPCSTR lpcszString, ...)       
{	
	//((CEdit *)GetDlgItem(IDC_EDIT_CPK_Y))->SetWindowText(lpcszString);
}

void CStandDlg::TOTAL_TEXT(LPCSTR lpcszString, ...)       
{	
	//((CEdit *)GetDlgItem(IDC_TOTAL))->SetWindowText(lpcszString);
}

void CStandDlg::PASS_TEXT(LPCSTR lpcszString, ...)       
{	
	//((CEdit *)GetDlgItem(IDC_PASS))->SetWindowText(lpcszString);
}

void CStandDlg::FAIL_TEXT(LPCSTR lpcszString, ...)       
{	
	//((CEdit *)GetDlgItem(IDC_FAIL))->SetWindowText(lpcszString);
}

void CStandDlg::PERCENT_TEXT(LPCSTR lpcszString, ...)       
{	
	//((CEdit *)GetDlgItem(IDC_PERCENT))->SetWindowText(lpcszString);
}


void CStandDlg::TOTAL_NUM(LPCSTR lpcszString, ...)       
{	
	//((CEdit *)GetDlgItem(IDC_TOTAL2))->SetWindowText(lpcszString);
}

void CStandDlg::PASS_NUM(LPCSTR lpcszString, ...)       
{	
	//((CEdit *)GetDlgItem(IDC_PASS2))->SetWindowText(lpcszString);
}

void CStandDlg::FAIL_NUM(LPCSTR lpcszString, ...)       
{	
	//((CEdit *)GetDlgItem(IDC_FAIL2))->SetWindowText(lpcszString);
}

void CStandDlg::PERCENT_NUM(LPCSTR lpcszString, ...)       
{	
	//((CEdit *)GetDlgItem(IDC_PERCENT2))->SetWindowText(lpcszString);
}

BOOL CStandDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		if(pMsg->wParam == VK_ADD){
//			::PostMessage(((CCenterPointModify_4CHDlg *)m_pMomWnd)->hMainWnd, WM_COMM_START, 0, 0 );
			return TRUE;
		}
		if (pMsg->wParam == VK_SUBTRACT){
//			::PostMessage(((CCenterPointModify_4CHDlg *)m_pMomWnd)->hMainWnd, WM_COMM_STOP, 0, 0 );
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CStandDlg::OnEnSetfocusTotal()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandDlg::OnEnSetfocusTotal2()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandDlg::OnEnSetfocusPass()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandDlg::OnEnSetfocusPass2()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandDlg::OnEnSetfocusFail()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandDlg::OnEnSetfocusFail2()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandDlg::OnEnSetfocusPercent()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandDlg::OnEnSetfocusPercent2()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandDlg::OnEnSetfocusEditCpk()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandDlg::OnEnSetfocusEditCpkX()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandDlg::OnEnSetfocusEditCpkY()
{
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CStandDlg::OnEnSetfocusStatEdit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Focus_move_start();
}
