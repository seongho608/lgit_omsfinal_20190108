// CENTER_DETECT.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "CENTER_DETECT_Option.h"


extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4]; 

// CCENTER_DETECT_Option 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCENTER_DETECT_Option, CDialog)

CCENTER_DETECT_Option::CCENTER_DETECT_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CCENTER_DETECT_Option::IDD, pParent)
//	, m_channel_Number(0)
	, m_iStandX(0)
	, m_iStandY(0)
	, m_dPassDeviatX(0)
	, m_dPassDeviatY(0)
	, m_dRateX(0)
	, m_dRateY(0)
	, m_iEnPixX(0)
	, m_iEnPixY(0)
	, m_iMaxCount(0)
	, m_iWRMODE(0)
	, m_dPassMaxDevX(0)
	, m_dPassMaxDevY(0)
    , b_CanOverlayCheck(FALSE)
{
	BinfileName = "";
	oldBinfileName = "";
	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;
	pTStat = NULL;
	m_filemode = 0;
	m_EtcNumber = 0;
	m_Running = FALSE;
	for(int lop = 0;lop<4;lop++){
		EtcPt[lop].x = -1;
		EtcPt[lop].y = -1;
	}
	CPKcount =0;

	for(int t=0; t<200; t++){
		CPK_DATA[0][t]=0;
		CPK_DATA[1][t]=0;
	}
		b_TESTResult = FALSE;

		StartCnt=0;
		b_StopFail = FALSE;

	m_BINSIZE = 0;
}

CCENTER_DETECT_Option::~CCENTER_DETECT_Option()
{
}

void CCENTER_DETECT_Option::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //	DDX_CBIndex(pDX, IDC_COMBO_SelChNum, m_channel_Number);
    //	DDX_Control(pDX, IDC_CAM_FRAME, m_stCenterDisplay);
    DDX_Text(pDX, IDC_STAND_OFFSETX, m_iStandX);
    DDV_MinMaxUInt(pDX, m_iStandX, 0, CAM_IMAGE_WIDTH);
    DDX_Text(pDX, IDC_STAND_OFFSETY, m_iStandY);
    DDV_MinMaxUInt(pDX, m_iStandY, 0, CAM_IMAGE_HEIGHT);
    DDX_Text(pDX, IDC_PASS_DEVIATX, m_dPassDeviatX);
    double devX = (CAM_IMAGE_WIDTH/2);
    DDV_MinMaxDouble(pDX, m_dPassDeviatX, 0.0, devX);
    DDX_Text(pDX, IDC_PASS_DEVIATY, m_dPassDeviatY);
    double devY = (CAM_IMAGE_HEIGHT/2);
    DDV_MinMaxDouble(pDX, m_dPassDeviatY, 0.0, devY);
    DDX_Text(pDX, IDC_RATE_X, m_dRateX);
    DDV_MinMaxDouble(pDX, m_dRateX, 0, 10.0);
    DDX_Text(pDX, IDC_RATE_Y, m_dRateY);
    DDV_MinMaxDouble(pDX, m_dRateY, 0, 10.0);
    DDX_Text(pDX, IDC_XENPIXEL, m_iEnPixX);
    DDX_Text(pDX, IDC_YENPIXEL, m_iEnPixY);
    DDX_Text(pDX, IDC_MAXCONTROL_NUMBER, m_iMaxCount);
    DDV_MinMaxUInt(pDX, m_iMaxCount, 0, 10);
    DDX_CBIndex(pDX, IDC_OPT_WRMODE, m_iWRMODE);
    //	DDX_Control(pDX, IDC_COMBO_SelChNum, m_cbSelMasterChannel);
    DDX_Control(pDX, IDC_LIST_CENTERWORK, m_CenterWorklist);
    DDX_Text(pDX, IDC_PASS_MAXDEVX, m_dPassMaxDevX);
    DDV_MinMaxDouble(pDX, m_dPassMaxDevX, 0.0, devX);
    DDX_Text(pDX, IDC_PASS_MAXDEVY, m_dPassMaxDevY);
    DDV_MinMaxDouble(pDX, m_dPassMaxDevY, 0.0, devY);
    DDX_Control(pDX, IDC_LIST_CENTERWORK_LOT, m_Lot_CenterList);
    DDX_Control(pDX, IDC_CB_OPT_CEN_RETRY, m_cb_RetryCnt);
    DDX_Check(pDX, IDC_CHECK_CAN, b_CanOverlayCheck);
}


BEGIN_MESSAGE_MAP(CCENTER_DETECT_Option, CDialog)
//	ON_CBN_SELCHANGE(IDC_COMBO2, &CCENTER_DETECT_Option::OnCbnSelchangeCombo2)
ON_BN_CLICKED(IDC_BTN_BINADD, &CCENTER_DETECT_Option::OnBnClickedBtnBinadd)
ON_BN_CLICKED(IDC_OPTICALSTAT_SAVE, &CCENTER_DETECT_Option::OnBnClickedOpticalstatSave)
ON_CBN_SELCHANGE(IDC_COMBO_MODE, &CCENTER_DETECT_Option::OnCbnSelchangeComboMode)
//ON_BN_CLICKED(IDC_BTN_AUTOSET, &CCENTER_DETECT_Option::OnBnClickedBtnAutoset)
//ON_BN_CLICKED(IDC_BTN_CHARTSET, &CCENTER_DETECT_Option::OnBnClickedBtnChartset)
//ON_CBN_SELCHANGE(IDC_COMOB_BIN, &CCENTER_DETECT_Option::OnCbnSelchangeComobBin)
//ON_CBN_SELCHANGE(IDC_OPT_WRMODE, &CCENTER_DETECT_Option::OnCbnSelchangeOptWrmode)
ON_CBN_SELCHANGE(IDC_COMBO_INTWROPT, &CCENTER_DETECT_Option::OnCbnSelchangeComboIntwropt)
ON_CBN_SELCHANGE(IDC_COMBO_WROPT2, &CCENTER_DETECT_Option::OnCbnSelchangeComboWropt2)
//ON_CBN_SELCHANGE(IDC_OPT_WRMODE, &CCENTER_DETECT_Option::OnCbnSelchangeOptWrmode)
ON_BN_CLICKED(IDC_OPTICALSTAT_SAVE2, &CCENTER_DETECT_Option::OnBnClickedOpticalstatSave2)
//ON_BN_CLICKED(IDC_OPTICALSTAT_SAVE3, &CCENTER_DETECT_Option::OnBnClickedOpticalstatSave3)
ON_WM_SHOWWINDOW()
ON_BN_CLICKED(IDC_BUTTON2, &CCENTER_DETECT_Option::OnBnClickedButton2)
//ON_BN_CLICKED(IDC_BUTTON_WRITE, &CCENTER_DETECT_Option::OnBnClickedButtonWrite)
ON_BN_CLICKED(IDC_BUTTON_MICOM, &CCENTER_DETECT_Option::OnBnClickedButtonMicom)
ON_BN_CLICKED(IDC_BUTTON_test, &CCENTER_DETECT_Option::OnBnClickedButtontest)
ON_CBN_SELCHANGE(IDC_COMOB_WRMODE, &CCENTER_DETECT_Option::OnCbnSelchangeComobWrmode)
ON_CBN_SELCHANGE(IDC_OPT_WRMODE, &CCENTER_DETECT_Option::OnCbnSelchangeOptWrmode)
ON_CBN_SELCHANGE(IDC_CB_OPT_CEN_RETRY, &CCENTER_DETECT_Option::OnCbnSelchangeCbOptCenRetry)
ON_BN_CLICKED(IDC_CHECK_CAN, &CCENTER_DETECT_Option::OnBnClickedCheckCan)
ON_BN_CLICKED(IDC_BUTTON_OVERLAYTEST, &CCENTER_DETECT_Option::OnBnClickedButtonOverlaytest)
END_MESSAGE_MAP()


// CCENTER_DETECT_Option 메시지 처리기입니다.
void CCENTER_DETECT_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
	strTitle.Empty();
	strTitle.Format("CenterDetect");
}

void CCENTER_DETECT_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO		= INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
	strTitle.Empty();
	strTitle.Format("CenterDetect");
	str_ModelPath=((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
}


BOOL CCENTER_DETECT_Option::OnInitDialog()
{
	CDialog::OnInitDialog();
	CFileFind		folderfind;

	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;

	((CImageTesterDlg *)m_pMomWnd)->OpticEnable = 1;
	str_BinPath = ((CImageTesterDlg *)m_pMomWnd)->Overlay_path + "\\BIN";
	if(!folderfind.FindFile(str_BinPath)){ //원하는 폴더가 없으면 만든다.
		CreateDirectory(str_BinPath, NULL); 
	}
	
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	pWrModeCombo = (CComboBox *)GetDlgItem(IDC_COMOB_WRMODE);//다이알로그
	pRunCombo = (CComboBox *)GetDlgItem(IDC_COMBO_MODE);
	pINTWROPTCombo = (CComboBox *)GetDlgItem(IDC_COMBO_INTWROPT);
	pWROPTCombo = (CComboBox *)GetDlgItem(IDC_COMBO_WROPT2);
	Load_parameter();
	pINTWROPTCombo->SetCurSel(CenterPrm.INITWRSEL);
	pWROPTCombo->SetCurSel(CenterPrm.MIDWRSEL);
	Set_List(&m_CenterWorklist);
	LOT_Set_List(&m_Lot_CenterList);

	InitEVMS();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


tResultVal CCENTER_DETECT_Option::Run()
{

	tResultVal retval = {0,};
	b_StopFail = FALSE;
	((CImageTesterDlg *)m_pMomWnd)->m_AddFinalMesResult = "";
	m_dCurrCenterCoord = cvPoint(0, 0);
	Initgen(&CenterVal);

	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
	
		InsertList();
	}
	CString stateDATA ="보임량 광축_ ";
	CString str = "";
	BOOL bCanOverlayCheckTest = TRUE;
	
	if (b_CanOverlayCheck == TRUE)
	{
		if(!OverlayCheck_TEST()){
			CenterVal.m_success = FALSE;
			CenterVal.m_Init_X = -999;
			CenterVal.m_Init_Y = -999;
			CenterVal.m_F_X_ASIX = -999;
			CenterVal.m_F_Y_ASIX = -999;

			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
				str.Empty();
// 				str.Format("%d",CenterVal.m_Init_X);
// 				m_CenterWorklist.SetItemText(InsertIndex,5,str);
// 				str.Format("%d",CenterVal.m_Init_Y);
// 				m_CenterWorklist.SetItemText(InsertIndex,6,str);
				str.Format("%3.1f",CenterVal.m_F_X_ASIX);
				m_CenterWorklist.SetItemText(InsertIndex,5,str);
				str.Format("%3.1f",CenterVal.m_F_Y_ASIX);
				m_CenterWorklist.SetItemText(InsertIndex,6,str);
// 				m_CenterWorklist.SetItemText(InsertIndex,9,"");
// 				m_CenterWorklist.SetItemText(InsertIndex,10,"");
				m_CenterWorklist.SetItemText(InsertIndex,7,"");
				m_CenterWorklist.SetItemText(InsertIndex,8,"");
			}

			bCanOverlayCheckTest = FALSE;
			Str_Mes[0][0] = "-999";
			Str_Mes[1][0] = "-999";
			Str_Mes[0][1] = "0";
			Str_Mes[1][1] = "0";
			StatePrintf("Can Overlay Check Fail");
		}else{
			StatePrintf("Can Overlay Check Pass");

		}
	}



	if(bCanOverlayCheckTest == TRUE){
		if(m_RunMode == 0){//광축 조정 모드
			OpticalAsixModify(&CenterVal);
		}else if(m_RunMode == 1){//광축 측정 모드
			Find_offset(&CenterVal);
		}
		
		retval.m_Success = CenterVal.m_success;



		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			//초기 옵셋
// 			if((abs(CenterVal.m_Init_X) < 0) ||(abs(CenterVal.m_Init_X) > m_CAM_SIZE_WIDTH))
// 			{
// 				str="0";
// 			}else{
// 				str.Empty();
// 				str.Format("%d",CenterVal.m_Init_X);
// 			}
// 			m_CenterWorklist.SetItemText(InsertIndex,5,str);
// 			if((abs(CenterVal.m_Init_Y) < 0) ||(abs(CenterVal.m_Init_Y) > m_CAM_SIZE_HEIGHT))
// 			{
// 				str="0";
// 			}else{
// 				str.Empty();
// 				str.Format("%d",CenterVal.m_Init_Y);
// 			}
// 			m_CenterWorklist.SetItemText(InsertIndex,6,str);
				//초기 옵셋
			((CImageTesterDlg *)m_pMomWnd)->m_AddFinalMesResult.Format(",%d:1,%d:1",CenterVal.m_Init_X,CenterVal.m_Init_Y);

			if((fabs(CenterVal.m_X_ASIX) < 0) ||(fabs(CenterVal.m_X_ASIX) > m_CAM_SIZE_WIDTH))
			{
				str="0";
			}else{
				str.Empty();
				str.Format("%3.1f",CenterVal.m_F_X_ASIX);
			}
			m_CenterWorklist.SetItemText(InsertIndex,5,str);
			

			if((fabs(CenterVal.m_Y_ASIX) < 0) ||(fabs(CenterVal.m_Y_ASIX) > m_CAM_SIZE_HEIGHT))
			{
				str="0";
			}else{
				str.Empty();
				str.Format("%3.1f",CenterVal.m_F_Y_ASIX);
			}
			m_CenterWorklist.SetItemText(InsertIndex,6,str);
			
// 			if(m_RunMode == 0){//광축 조정 모드
// 				str.Empty();
// 				str.Format("%d",-CenterVal.m_offsetX);
// 				m_CenterWorklist.SetItemText(InsertIndex,9,str);
// 				
// 				str.Empty();
// 				str.Format("%d",-CenterVal.m_offsetY);
// 				m_CenterWorklist.SetItemText(InsertIndex,10,str);
// 			}else{
// 				m_CenterWorklist.SetItemText(InsertIndex,9,"");
// 				m_CenterWorklist.SetItemText(InsertIndex,10,"");				
//			}
			str.Empty();
			str.Format("%d",CenterVal.m_F_X_OFFSET);//-
			m_CenterWorklist.SetItemText(InsertIndex,7,str);
			
			str.Empty();
			str.Format("%d",CenterVal.m_F_Y_OFFSET);//-
			m_CenterWorklist.SetItemText(InsertIndex,8,str);

		}

		str.Empty();
		//str.Format("%3.1f",(int)CenterVal.m_F_X_ASIX);
		str.Format("%3d",(int)CenterVal.m_F_X_ASIX);
		Str_Mes[0][0] = str;
		stateDATA += "X_ "+str;
		str.Empty();
		//str.Format("%3.1f",(int)CenterVal.m_F_Y_ASIX);
		str.Format("%3d",(int)CenterVal.m_F_Y_ASIX);
		stateDATA += " Y_ "+str;
		Str_Mes[1][0] = str;
		str.Empty();
		str.Format("%d",CenterVal.m_F_X_OFFSET);//-
		stateDATA += "  offsetX_ "+str;
		str.Empty();
		str.Format("%d",CenterVal.m_F_Y_OFFSET);//-
		stateDATA += " Y_ "+str;

		if(retval.m_Success == TRUE){
			b_TESTResult = TRUE;
			Str_Mes[0][1] = "1";
			Str_Mes[1][1] = "1";
			stateDATA += " _ PASS";
			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

				m_CenterWorklist.SetItemText(InsertIndex,9,"PASS");
				((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_CENTER,"PASS");

			}
			retval.ValString.Format("X : %d,Y: %d OK",CenterVal.m_F_X_OFFSET,CenterVal.m_F_Y_OFFSET);//-
			
		}else{
			stateDATA += " _ FAIL";
			b_TESTResult = FALSE;
			Str_Mes[0][1] = "0";
			Str_Mes[1][1] = "0";
			if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

				m_CenterWorklist.SetItemText(InsertIndex,9,"FAIL");
				((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_CENTER,"FAIL");

			}
			retval.ValString.Format("X : %d,Y: %d FAIL",CenterVal.m_F_X_OFFSET,CenterVal.m_F_Y_OFFSET);//-
		}
		
	}else{
		retval.m_Success  = FALSE;
		stateDATA += " _CAN OVERLAY CHECK FAIL";
		b_TESTResult = FALSE;
		Str_Mes[0][1] = "0";
		Str_Mes[1][1] = "0";
		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

			m_CenterWorklist.SetItemText(InsertIndex,9,"FAIL");
			((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_CENTER,"FAIL");

		}
		retval.ValString.Format("Can Overlay Check Fail");//-

	}
	((CImageTesterDlg *)m_pMomWnd)->StatePrintf(stateDATA);
	if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
		StartCnt++;
	}

	return retval;
}


void CCENTER_DETECT_Option::CPK_DATA_SUM(){
	
	double AveX =0;
	double AveY =0;
	double VarX =0;
	double VarY =0;

	double PassX = CenterPrm.PassDeviatX;
	double PassY = CenterPrm.PassDeviatY;

	double USL_X = PassX;
	double LSL_X = (PassX * -1.0);

	double USL_Y = PassY;
	double LSL_Y = (PassY * -1.0);


	for(int t=0; t<CPKcount;t++){
		//--------------------------------X
		AveX += CPK_DATA[0][t];
		//--------------------------------Y
		AveY += CPK_DATA[1][t];
	}

	AveX /= (double)CPKcount;
	AveY /= (double)CPKcount;

	for(int t=0; t<CPKcount;t++){
		//--------------------------------X
		VarX += pow(((double)CPK_DATA[0][t] - AveX),2);
		//--------------------------------Y
		VarY += pow(((double)CPK_DATA[1][t] - AveY),2);
	}
	VarX /= (double)CPKcount;
	VarY /= (double)CPKcount;

	double SqrtX = sqrt(VarX);
	double SqrtY = sqrt(VarY);

	//--------------------------------X
	double K_X=0;
	if(fabs(USL_X - AveX) >fabs(LSL_X - AveX)){
		K_X = fabs(0 - AveX) / PassX;
	}
	else{
		K_X = fabs(0 - AveX) / PassX;
	}

	double CP_X = 0;
	double sum_X = (SqrtX*6.0);


	CP_X = (PassX*2.0)/(SqrtX*6.0);
	
	double CPK_X=(1.0-K_X)*CP_X;
	if(sum_X == 0){
		CPK_X =100;		
	}
	if(sum_X < 0){
		CPK_X =0;		
	}
	//--------------------------------Y
	double K_Y=0;
	if(fabs(USL_Y - AveY) >fabs(LSL_Y - AveY)){
		K_Y = fabs(0 - AveY) / PassY;
	}
	else{
		K_Y = fabs(0 - AveY) / PassY;
	}

	double CP_Y = 0;
	

	double sum_Y = (SqrtY*6.0);

	CP_Y = (PassY*2.0)/(SqrtY*6.0);

	
	double CPK_Y=(1.0-K_Y)*CP_Y;


	if(sum_Y == 0){
		CPK_Y =100;
	}
	if(sum_Y < 0){
		CPK_Y =0;		
	}
	CString str_X="";
	str_X.Format("%6.2f",CPK_X);
	((CImageTesterDlg *)m_pMomWnd)->m_pStandWnd->CPK_X(str_X);

	CString str_Y="";
	str_Y.Format("%6.2f",CPK_Y);
	((CImageTesterDlg *)m_pMomWnd)->m_pStandWnd->CPK_Y(str_Y);

	SaveCPK_X = CPK_X;
	SaveCPK_Y = CPK_Y;
	
}

CvPoint CCENTER_DETECT_Option::GetCenterPoint(LPBYTE IN_RGB)
{	
	CvPoint resultPt = cvPoint(-99999, -99999);

	IplImage *OriginImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *PreprecessedImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	
	IplImage *RGBOrgImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3);

	BYTE R,G,B;
	double Sum_Y;
	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	for (int y=0; y<m_CAM_SIZE_HEIGHT; y++)
	{
		for (int x=0; x<m_CAM_SIZE_WIDTH; x++)
		{
		/*	B = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 2];
			
			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;		*/

			B = IN_RGB[y*(m_CAM_SIZE_WIDTH * 3) + x * 3];
			G = IN_RGB[y*(m_CAM_SIZE_WIDTH * 3) + x * 3 + 1];
			R = IN_RGB[y*(m_CAM_SIZE_WIDTH * 3) + x * 3 + 2];

			if( R < 50 && G < 50 && B < 50)
				OriginImage->imageData[y*OriginImage->widthStep+x] = 255;
			else
				OriginImage->imageData[y*OriginImage->widthStep+x] = 0;

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}	
	
	cvSaveImage("D:\\CenterImage.bmp", OriginImage);
//	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
//	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);

	cvCopyImage(OriginImage, SmoothImage);
	
	cvCanny(SmoothImage, CannyImage, 0, 255);

//	cvDilate(CannyImage, DilateImage);
	
	cvCopyImage(CannyImage, temp_PatternImage);
	
	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);
	
//	cvSaveImage("C:\\CenterImage.bmp", CannyImage);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
		counter++;
	
	if(counter == 0){
		cvReleaseMemStorage(&contour_storage);
		cvReleaseImage(&OriginImage);
		cvReleaseImage(&CannyImage);
		cvReleaseImage(&PreprecessedImage);
		
		cvReleaseImage(&DilateImage);
		cvReleaseImage(&SmoothImage);
		cvReleaseImage(&RGBResultImage);
		cvReleaseImage(&temp_PatternImage);
		cvReleaseImage(&RGBOrgImage);

		return resultPt;
	}

	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area=0, arcCount=0;
	counter = 0;
	double old_dist = 999999;
	
	int old_center_pos_x = 0;
	int old_center_pos_y = 0;
	int center_pt_x = 0;
	int center_pt_y = 0;
	int obj_Cnt = 0;
	int size=0, old_size = 0;	

	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;

		double circularity = (4.0*3.14*area)/(arcCount*arcCount);		

		if(circularity > 0.8)
		{
		
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;

			//if(center_x > 270 && center_x < 450 && center_y > 160 && center_y < 320)
			if(center_x > (int)(Cam_PosX*0.60) && center_x < (int)(Cam_PosX*1.40) && center_y > (int)(Cam_PosY*0.600) && center_y < (int)(Cam_PosY*1.400))
			{
				if(rect.width < Cam_PosX && rect.height < Cam_PosY && rect.width > 20 && rect.height > 20)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
					
					/*center_pt_x = center_pt_x+ center_x;
					center_pt_y = center_pt_y+ center_y;
					obj_Cnt++;

					old_center_pos_x = center_pt_x/obj_Cnt;
					old_center_pos_y = center_pt_y/obj_Cnt;*/
					
					double distance = GetDistance(rect.x + rect.width/2, rect.y+rect.height/2, Cam_PosX, Cam_PosY);
					
					obj_Cnt++;

					size = rect.width * rect.height;
					
					if(distance < old_dist)
					{
						old_size = size;
						resultPt.x = center_x;
						resultPt.y = center_y;
						old_dist = distance;
					}

					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 0), 1, 8);  
				
				}
			}
		}
		
		counter++;
	}
//	cvSaveImage("D:\\RGBResultImage.bmp", RGBResultImage);
	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&CannyImage);
	cvReleaseImage(&PreprecessedImage);
	
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
	cvReleaseImage(&RGBOrgImage);

// 	delete contour;
// 	delete temp_contour;
	delete []rectArray;
	delete []areaArray;
	
	/*if(obj_Cnt != 0)
	{
		resultPt.x = center_pt_x / obj_Cnt;
		resultPt.y = center_pt_y / obj_Cnt;
	}*/
/*	if(obj_Cnt != 0)
	{
		resultPt.x = center_pt_x;
		resultPt.y = center_pt_y;
	}*/
		
	return resultPt;	
}

//void CCENTER_DETECT_Option::OnCbnSelchangeComboSelchnum()
//{
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	int sel = m_cbSelMasterChannel.GetCurSel();
//	CString str;
//	m_cbSelMasterChannel.GetLBText(sel, str);
//	int currCh=0;
//	
//	if(str == "채널 1")
//		currCh = 0;
//	else if(str == "채널 2")
//		currCh = 1;
//	else if(str == "채널 3")
//		currCh = 2;
//	else if(str == "채널 4")
//		currCh = 3;
//	else
//		currCh = 0;
//		
//	((CImageTesterDlg *)m_pMomWnd)->m_pChartWnd->ChartReset();
//	((CImageTesterDlg *)m_pMomWnd)->m_pChartWnd->DrawCircle(m_ptMasterCoord[currCh].x, m_ptMasterCoord[currCh].y, 75, 75);
//	
//	SetDlgItemInt(IDC_Chart_X, m_ptMasterCoord[currCh].x);
//	SetDlgItemInt(IDC_CHART_Y, m_ptMasterCoord[currCh].y);
//	
//	DoEvents(300);
//
//	if(((CImageTesterDlg *)m_pMomWnd)->m_SelCh == 0)
//		((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = 1;
//
//	for(int i =0;i<10;i++){
//		if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0){
//			break;
//		}else{
//			DoEvents(100);
//		}
//	}	
//
//	ShowCenterMasterImage(currCh);
//	
//}
/*
void CCENTER_DETECT_Option::ShowCenterMasterImage(int ch)
{
	unsigned char *RGBBuf = new unsigned char [CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];

	BITMAPINFO		m_biInfo;
	ZeroMemory(&m_biInfo, sizeof(m_biInfo));
	m_biInfo.bmiHeader.biSize		= sizeof(BITMAPINFOHEADER);
	m_biInfo.bmiHeader.biWidth		= CAM_IMAGE_WIDTH;
	m_biInfo.bmiHeader.biHeight		= CAM_IMAGE_HEIGHT;
	m_biInfo.bmiHeader.biPlanes		= 1;
	m_biInfo.bmiHeader.biBitCount	= 32;
	m_biInfo.bmiHeader.biCompression	= 0;
	m_biInfo.bmiHeader.biSizeImage	= CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4;	

	CDC *pcDC;

	//-----------가상메모리를 만든다.------------------
	CDC	*pcMemDC = new CDC;
	CBitmap	*pcDIB	= new CBitmap;

//	CSPACE_UYVY_TO_RGB32(ComArtWnd->m_YUVIn[m_Channel], CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT,m_RGBScanbuf[m_Channel], 0);
	pcDC = m_stCenterDisplay.GetDC();
	memcpy(RGBBuf,m_RGBScanbuf[ch],sizeof(m_RGBScanbuf[ch]));

	pcMemDC->CreateCompatibleDC(pcDC);
	pcDIB->CreateCompatibleBitmap(pcDC,CAM_IMAGE_WIDTH, CAM_IMAGE_HEIGHT);
	CBitmap	*pcOldDIB = pcMemDC->SelectObject(pcDIB);
	
	

	//---------------------------------------------
	::SetStretchBltMode(pcMemDC->m_hDC, COLORONCOLOR);//MemDC.m_hDC//pcDC->m_hDC
	::StretchDIBits(pcMemDC->m_hDC, 
					0,  CAM_IMAGE_HEIGHT-1,
					CAM_IMAGE_WIDTH, -CAM_IMAGE_HEIGHT,
					0, 0,
					m_biInfo.bmiHeader.biWidth,
					m_biInfo.bmiHeader.biHeight,
					RGBBuf, &m_biInfo, DIB_RGB_COLORS, SRCCOPY);

	CPen my_Pan, my_Pan2, *old_pan;
	
	my_Pan.CreatePen(PS_SOLID,3,RGB(255, 255, 255));
	old_pan = pcMemDC->SelectObject(&my_Pan);
	pcMemDC->MoveTo(270, 160);
	pcMemDC->LineTo(450,160);
	pcMemDC->LineTo(450,320);
	pcMemDC->LineTo(270,320);
	pcMemDC->LineTo(270,160);
	my_Pan.DeleteObject();
	
	int x = m_ptCenterPointCoord[ch].x;
	int y = m_ptCenterPointCoord[ch].y;

	my_Pan2.CreatePen(PS_SOLID,3,RGB(255, 255, 255));
	old_pan = pcMemDC->SelectObject(&my_Pan2);
	pcMemDC->MoveTo(x-10, y);
	pcMemDC->LineTo(x+10, y);
	pcMemDC->MoveTo(x, y-10);
	pcMemDC->LineTo(x, y+10);
	my_Pan2.DeleteObject();

	CString TEXTDATA ="";
	CFont font;
	font.CreatePointFont(240, "Arial");  
	pcMemDC->SelectObject(&font);
	
	if(m_ptCenterPointCoord[ch].x == -99999 && m_ptCenterPointCoord[ch].y == -99999)
	{
		TEXTDATA.Format("중심점을 찾지 못하였습니다.");
		pcMemDC->SetTextColor(RGB(255, 0, 0));
	}
	else
	{
		TEXTDATA.Format("X = %d   Y = %d", m_ptCenterPointCoord[ch].x, m_ptCenterPointCoord[ch].y);
		pcMemDC->SetTextColor(RGB(0, 0, 255));
	}

	pcMemDC->TextOut(0 , CAM_IMAGE_HEIGHT - 80,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	font.DeleteObject();


	//--------------모니터 창에 맞추어 RGB정보를 할당한다.-------------------------
	::SetStretchBltMode(pcDC->m_hDC, COLORONCOLOR);
	::StretchBlt(	pcDC->m_hDC,
					0,  0,
					//720, 480,//1080, 720,
					360,228,
					pcMemDC->m_hDC,
					0, 0,
					CAM_IMAGE_WIDTH,
					CAM_IMAGE_HEIGHT,SRCCOPY);
	

	m_stCenterDisplay.ReleaseDC(pcDC);

	delete pcDIB;
	delete pcMemDC;	
	delete []RGBBuf;
	
//	return TRUE;
}
*/

void CCENTER_DETECT_Option::OnBnClickedBtnBinadd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	CString GetFileName,GetFilepath,GetFolderPath;
	CString DstFile = "";

	CFile file;	
	CFileDialog dlg(TRUE,"bin","*.bin",OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT|OFN_ALLOWMULTISELECT, 
	"bin 파일 (*.bin)|*.bin|모든 파일 (*.*)|*.*|");

	if(dlg.DoModal() == IDOK)
	{	
		GetFileName = dlg.GetFileTitle();
		GetFilepath = dlg.GetPathName();//폴더 및 파일경로를 가져온다.			
		DstFile = str_BinPath + ("\\") + GetFileName + ".bin";
	
		if(GetFilepath != DstFile){
			BinFiledel();//기존에 있던 파일을 지운다.
			CopyFile(GetFilepath,DstFile,false);
			DoEvents(100);
		}
		WritePrivateProfileString(str_model,"FILENAME",GetFileName,str_ModelPath);//BIN파일 경로를 저장한다.
		DoEvents(100);
		SetFileSel();
	}
}

void CCENTER_DETECT_Option::BinFiledel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CFileFind  finder;
	CString fname = "";
	CString strFile = str_BinPath +("\\*.bin");

	BOOL bWorking = finder.FindFile(strFile);
	while(bWorking)
	{
		bWorking = finder.FindNextFile();
		if(!finder.IsDots() && !finder.IsDirectory()){
			fname.Empty();
			fname = finder.GetFileTitle();
			DelFile(str_BinPath + "\\" + fname + ".bin");
		}
	}
}

void CCENTER_DETECT_Option::SetFileSel()//bin파일이 있는지 확인후에 경로 지정 및 이름지정하고 존재할 경우 binfilechk를 실행한다.
{
	if(CenterPrm.HWMODE == 1){
		BinfileName = "";
		oldBinfileName = "";
		return;
	}

	CFileFind  finder;
	BinfileName = GetPrivateProfileCString(str_model,"FILENAME",str_ModelPath);
	
	if(BinfileName == ""){
		model_bin_path = "";
	}else{
		model_bin_path = str_BinPath + "\\" + BinfileName + (".bin");
		if(finder.FindFile(model_bin_path) == TRUE){
			if(oldBinfileName != BinfileName){
				oldBinfileName = BinfileName;
				BinFileCHK();
			}
		}else{
			WritePrivateProfileString(str_model,"FILENAME","",str_ModelPath);//현재 선택된 셀을 저장한다.
			model_bin_path = "";
			BinfileName = "";
			oldBinfileName = "";
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_BINFILENAME))->SetWindowText(BinfileName);
	

	if(((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd != NULL){
		((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->pFileEdit->SetWindowText(BinfileName);
	}
//	((CImageTesterDlg *)m_pMomWnd)->m_pResWorkerOptWnd->Filename_Value(BinfileName);

}

void CCENTER_DETECT_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

BYTE CCENTER_DETECT_Option::BinFileCHK()
{
	CString BufFile;
	
	int stat = BinFileCHK(0); //일반 BIN파일인지 체크한다.
	if(stat == 1){//현재 BIN파일이 맞을때
		BufFile.Empty();
		BufFile.Format("[%s]\r\n FILE READ&OFFSET CHECK SUCCESS",BinfileName);
		StatePrintf(BufFile,2,10,3);
		return 1;
	}else if(stat == 2){
		return 0;
	}

	stat = BinFileCHK(1); //일반 BIN파일인지 체크한다.
	if(stat == 1){//현재 BIN파일이 맞을때
		BufFile.Empty();
		BufFile.Format("[%s]\r\n FILE MEGA READ&OFFSET CHECK SUCCESS",BinfileName);
		StatePrintf(BufFile,2,10,3);
		return 2;
	}else if(stat == 2){
		return 0;
	}

	stat = BinFileCHK(2); //일반 BIN파일인지 체크한다.
	if(stat == 1){//현재 BIN파일이 맞을때
		BufFile.Empty();
		BufFile.Format("[%s]\r\n FILE READ&OFFSET CHECK SUCCESS",BinfileName);
		StatePrintf(BufFile,2,10,3);
		return 3;
	}else if(stat == 2){
		return 0;
	}

	stat = BinFileCHK(3); //일반 BIN파일인지 체크한다.
	if(stat == 1){//현재 BIN파일이 맞을때
		BufFile.Empty();
		BufFile.Format("[%s]\r\n FILE READ&OFFSET CHECK SUCCESS",BinfileName);
		StatePrintf(BufFile,2,10,3);
		return 4;
	}else if(stat == 2){
		return 0;
	}

	stat = BinFileCHK(4); //일반 BIN파일인지 체크한다.
	if(stat == 1){//현재 BIN파일이 맞을때
		BufFile.Empty();
		BufFile.Format("[%s]\r\n FILE READ&OFFSET CHECK SUCCESS",BinfileName);
		StatePrintf(BufFile,2,10,3);
		return 5;
	}else if(stat == 2){
		return 0;
	}

	BufFile.Empty();
	BufFile.Format("[%s]\r\n FILE READ&OFFSET CHECK FAILS",BinfileName);
	StatePrintf(BufFile,2,10,3);
	return 0;
}

int CCENTER_DETECT_Option::BinFileCHK(int stat)
{
	// TODO: Add your control notification handler code here	
	CString GetFileName;
	CString BufFile;
	int n[40]={0};		

	CFile BINFILE;
	CFileStatus BINFILESTATUS;
	CFileException BINEX;

	for(int lop = 0;lop<4;lop++){
		AddOfOffset[lop] = 0;
	}
	m_BINSIZE = 0;
	B_FileChk = FALSE;
	B_FileRead = FALSE;
	
	if(stat == 0){//
		xoffdata = 0x10;
		yoffdata = 0x0c;
		m_DChkSum1 = 0xDE;
		m_DChkSum2 = 0xAC;
	}else if(stat == 1){
		xoffdata = 0;
		yoffdata = 0;
		m_DChkSum1 = 0xEC;
		m_DChkSum2 = 0x9C;
	}else if(stat == 2){
		xoffdata = 0;
		yoffdata = 0;
		m_DChkSum1 = 0xED;
		m_DChkSum2 = 0x2C;
	}else if(stat == 3){
		xoffdata = 0;
		yoffdata = 0;
		m_DChkSum1 = 0xEF;
		m_DChkSum2 = 0x35;
	}else if(stat == 4){
		xoffdata = 0;
		yoffdata = 0;
		m_DChkSum1 = 0x30;
		m_DChkSum2 = 0xC4;
	}
	
	unsigned long nRet = 0, i,k,m, AddressPage=0;
	unsigned long	addresscnt = 0;
	unsigned long	BuffSize = 0;
	unsigned char	buffer[16];
	unsigned long MAXSIZE,MAXADDR;
	unsigned long CNTSIZE = 0;
	if((BinfileName == "")||(model_bin_path == "")){
		StatePrintf("BIN FILE EMPTY");
		return 2;
	}

	GetFileName = model_bin_path;
	//------------------------------------------------------------------------------------ 읽을 파일을 결정한다.
	if(!BINFILE.Open(GetFileName, CFile::modeRead, &BINEX))
	{ //오류가 발생하면
		StatePrintf("BIN FILE READ FAIL");
		return 2;
	}
	else
	{
		memset(BINBUF,0xff,sizeof(BINBUF));

		BINFILE.GetStatus(BINFILESTATUS);
		MAXSIZE = BINFILESTATUS.m_size;
		MAXADDR = (unsigned long)(MAXSIZE/256);
		m_BINSIZE = MAXSIZE;
		addresscnt = 0;
		for(CNTSIZE = 0;CNTSIZE < MAXSIZE;CNTSIZE+=16)
		{
			BuffSize = sizeof(buffer);	
			nRet = BINFILE.Read(buffer,BuffSize);//원하는 버퍼사이즈만큼 읽는다.
			for(int lop = 0;lop < nRet;lop++)
			{
				BINBUF[CNTSIZE+lop] = buffer[lop];
			}
		}
		B_FileRead = TRUE;
		BINFILE.Close();

		BYTE offnumx = 0;
		BYTE offnumy = 0;
		for(CNTSIZE = 0;CNTSIZE < MAXSIZE;CNTSIZE++)
		{
			if((BINBUF[CNTSIZE] == (BYTE)(m_DChkSum1))&&(BINBUF[CNTSIZE+1] == (BYTE)(m_DChkSum2)))
			{
				if((BINBUF[CNTSIZE-2]&0xF0) == 0x70)
				{
					if(BINBUF[CNTSIZE-1] >= 0x10)
					{
						offnumx = BINBUF[CNTSIZE-1] - 0X08 + 0X01;
						offnumy = BINBUF[CNTSIZE-1] + 0X01;

						if((BINBUF[CNTSIZE+offnumx] == (BYTE)xoffdata)&&(BINBUF[CNTSIZE+offnumy] == (BYTE)yoffdata))
						{
							AddOfOffset[2]=CNTSIZE;
							AddOfOffset[3]=CNTSIZE+1;
							AddOfOffset[0]=CNTSIZE+(unsigned long)offnumx;
							AddOfOffset[1]=CNTSIZE+(unsigned long)offnumy;
							IndexOffset = (AddOfOffset[2]/0x1000)*0x1000;//256 
							IndexOffset2 = (AddOfOffset[1]/0x1000)*0x1000;//256 
							B_FileChk = TRUE;
							m_binmod = stat;
							m_ChkSumMode = BINBUF[CNTSIZE-2]&0x0F;

							return TRUE;
						}
					}
				}
			}
		}
	}		
}

void CCENTER_DETECT_Option::Save_parameter(){
	CString	   str = "";
	WritePrivateProfileString(str_model,"FILENAME",BinfileName,str_ModelPath);//BIN파일 경로를 저장한다.
	str.Empty();
	str.Format("%d",CenterPrm.MaxCount);
	WritePrivateProfileString(str_model,"MAXCONTROLCOUNT",str,str_ModelPath);
	str.Empty();
	str.Format("%d",CenterPrm.StandX);
	WritePrivateProfileString(str_model,"STANDOFFSETX",str,str_ModelPath);
	str.Empty();
	str.Format("%d",CenterPrm.StandY);
	WritePrivateProfileString(str_model,"STANDOFFSETY",str,str_ModelPath);
	str.Empty();
	str.Format("%6.2f",CenterPrm.PassDeviatX);
	WritePrivateProfileString(str_model,"PASSDEVIATX",str,str_ModelPath);
	str.Empty();
	str.Format("%6.2f",CenterPrm.PassDeviatY);
	WritePrivateProfileString(str_model,"PASSDEVIATY",str,str_ModelPath);
	str.Empty();
	str.Format("%6.2f",CenterPrm.PassMaxDevX);
	WritePrivateProfileString(str_model,"MAXPASSDEVX",str,str_ModelPath);
	str.Empty();
	str.Format("%6.2f",CenterPrm.PassMaxDevY);
	WritePrivateProfileString(str_model,"MAXPASSDEVY",str,str_ModelPath);

	str.Empty();
	str.Format("%6.2f",CenterPrm.RateX);
	WritePrivateProfileString(str_model,"RATEX",str,str_ModelPath);
	str.Empty();
	str.Format("%6.2f",CenterPrm.RateY);
	WritePrivateProfileString(str_model,"RATEY",str,str_ModelPath);
	str.Empty();
	str.Format("%d",CenterPrm.EnPixX);
	WritePrivateProfileString(str_model,"ENPIXX",str,str_ModelPath);
	str.Empty();
	str.Format("%d",CenterPrm.EnPixY);
	WritePrivateProfileString(str_model,"ENPIXY",str,str_ModelPath);
	str.Empty();
	str.Format("%d",CenterPrm.WriteMode);
	WritePrivateProfileString(str_model,"WRITEMODE",str,str_ModelPath);

	str.Empty();
	str.Format("%d",CenterPrm.MIDWRSEL);
	WritePrivateProfileString(str_model,"SectorWrite",str,str_ModelPath);
	str.Empty();
	str.Format("%d",CenterPrm.INITWRSEL);
	WritePrivateProfileString(str_model,"initWrite",str,str_ModelPath);

	//2016.09.02 수정:심재원 --------------------------------------------------
	str.Empty();
	str.Format("%d",CenterPrm.RetryCnt_Write);
	WritePrivateProfileString(str_model,"SectorWrite_RetryCnt",str,str_ModelPath);
	//end 2016.09.02 수정:심재원 ----------------------------------------------
}


void CCENTER_DETECT_Option::Load_parameter(){
	CString	   str = "";
	CString	   strna = "";
	CString	   loadfile = "";
	char cload[100] ={0.};
	int frnum=0;//폴더이름갯수

	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	CenterPrm.MaxCount = GetPrivateProfileInt(str_model,"MAXCONTROLCOUNT",-1,str_ModelPath);//테스트를 진행할 갯수
	if(CenterPrm.MaxCount == -1){
		CenterPrm.MaxCount = 3;
		str.Empty();
		str.Format("%d",CenterPrm.MaxCount);
		WritePrivateProfileString(str_model,"MAXCONTROLCOUNT",str,str_ModelPath);
	}

	CenterPrm.StandX = GetPrivateProfileInt(str_model,"STANDOFFSETX",-1,str_ModelPath);
	if(CenterPrm.StandX == -1){
		CenterPrm.StandX = Cam_PosX;
		str.Empty();
		str.Format("%d",CenterPrm.StandX);
		WritePrivateProfileString(str_model,"STANDOFFSETX",str,str_ModelPath);
	}
	CenterPrm.StandY = GetPrivateProfileInt(str_model,"STANDOFFSETY",-1,str_ModelPath);
	if(CenterPrm.StandY == -1){
		CenterPrm.StandY = Cam_PosY;
		str.Empty();
		str.Format("%d",CenterPrm.StandY);
		WritePrivateProfileString(str_model,"STANDOFFSETY",str,str_ModelPath);
	}
	CenterPrm.PassDeviatX = GetPrivateProfileDouble(str_model,"PASSDEVIATX",-1,str_ModelPath);
	if(CenterPrm.PassDeviatX == -1){
		CenterPrm.PassDeviatX = 3.5;
		str.Empty();
		str.Format("%6.2f",CenterPrm.PassDeviatX);
		WritePrivateProfileString(str_model,"PASSDEVIATX",str,str_ModelPath);
	}
	CenterPrm.PassDeviatY = GetPrivateProfileDouble(str_model,"PASSDEVIATY",-1,str_ModelPath);
	if(CenterPrm.PassDeviatY == -1){
		CenterPrm.PassDeviatY = 3.5;
		str.Empty();
		str.Format("%6.2f",CenterPrm.PassDeviatY);
		WritePrivateProfileString(str_model,"PASSDEVIATY",str,str_ModelPath);
	}
	
	CenterPrm.PassMaxDevX = GetPrivateProfileDouble(str_model,"MAXPASSDEVX",-1,str_ModelPath);
	if(CenterPrm.PassMaxDevX == -1){
		CenterPrm.PassMaxDevX = 1.0;
		str.Empty();
		str.Format("%6.2f",CenterPrm.PassMaxDevX);
		WritePrivateProfileString(str_model,"MAXPASSDEVX",str,str_ModelPath);
	}
	CenterPrm.PassMaxDevY = GetPrivateProfileDouble(str_model,"MAXPASSDEVY",-1,str_ModelPath);
	if(CenterPrm.PassMaxDevY == -1){
		CenterPrm.PassMaxDevY = 1.0;
		str.Empty();
		str.Format("%6.2f",CenterPrm.PassMaxDevY);
		WritePrivateProfileString(str_model,"MAXPASSDEVY",str,str_ModelPath);
	}

	CenterPrm.RateX = GetPrivateProfileDouble(str_model,"RATEX",-1,str_ModelPath);
	if(CenterPrm.RateX == -1){
		CenterPrm.RateX = 1.0;
		str.Empty();
		str.Format("%6.2f",CenterPrm.RateX);
		WritePrivateProfileString(str_model,"RATEX",str,str_ModelPath);
	}
	CenterPrm.RateY = GetPrivateProfileDouble(str_model,"RATEY",-1,str_ModelPath);
	if(CenterPrm.RateY == -1){
		CenterPrm.RateY = 1.0;
		str.Empty();
		str.Format("%6.2f",CenterPrm.RateY);
		WritePrivateProfileString(str_model,"RATEY",str,str_ModelPath);
	}
	CenterPrm.EnPixX = GetPrivateProfileInt(str_model,"ENPIXX",-1,str_ModelPath);
	if(CenterPrm.EnPixX == -1){
		CenterPrm.EnPixX = 10;
		str.Empty();
		str.Format("%d",CenterPrm.EnPixX);
		WritePrivateProfileString(str_model,"ENPIXX",str,str_ModelPath);
	}
	CenterPrm.EnPixY = GetPrivateProfileInt(str_model,"ENPIXY",-1,str_ModelPath);
	if(CenterPrm.EnPixY == -1){
		CenterPrm.EnPixY = 10;
		str.Empty();
		str.Format("%d",CenterPrm.EnPixY);
		WritePrivateProfileString(str_model,"ENPIXY",str,str_ModelPath);
	}
	CenterPrm.WriteMode = GetPrivateProfileInt(str_model,"WRITEMODE",-1,str_ModelPath);
	if(CenterPrm.WriteMode == -1){
		CenterPrm.WriteMode = 0;
		str.Empty();
		str.Format("%d",CenterPrm.WriteMode);
		WritePrivateProfileString(str_model,"WRITEMODE",str,str_ModelPath);
	}
	

	if(((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd != NULL){
		((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->pFileEdit->SetWindowText(BinfileName);
	}
//	((CImageTesterDlg *)m_pMomWnd)->m_pResWorkerOptWnd->Filename_Value(BinfileName);
	
	//===============================================================================
	CenterPrm.INITWRSEL = GetPrivateProfileInt(str_model,"initWrite",-1,str_ModelPath);
	if(CenterPrm.INITWRSEL == -1){
		CenterPrm.INITWRSEL = 2;//NONE
		str.Empty();
		str.Format("%d",CenterPrm.INITWRSEL);
		WritePrivateProfileString(str_model,"initWrite",str,str_ModelPath);
	}

	CenterPrm.MIDWRSEL = GetPrivateProfileInt(str_model,"SectorWrite",-1,str_ModelPath);
	if(CenterPrm.MIDWRSEL == -1){
		CenterPrm.MIDWRSEL = 1;//SECTOR
		str.Empty();
		str.Format("%d",CenterPrm.MIDWRSEL);
		WritePrivateProfileString(str_model,"SectorWrite",str,str_ModelPath);
	}
	pINTWROPTCombo->SetCurSel(CenterPrm.INITWRSEL);
	pWROPTCombo->SetCurSel(CenterPrm.MIDWRSEL);
	
	m_iSelWrMode = GetPrivateProfileInt(str_model,"HWMODE",-1,str_ModelPath);
	if(m_iSelWrMode == -1){
		m_iSelWrMode = 0;
		str.Empty();
		str.Format("%d",m_iSelWrMode);
		WritePrivateProfileString(str_model,"HWMODE",str,str_ModelPath);
	}
	CenterPrm.HWMODE = m_iSelWrMode;
	
	pWrModeCombo->SetCurSel(CenterPrm.HWMODE);
	m_RunMode = GetPrivateProfileInt(str_model,"RunMode",-1,str_ModelPath);
	if(m_RunMode == -1){
		m_RunMode = 0;
		str.Empty();
		str.Format("%d",m_RunMode);
		WritePrivateProfileString(str_model,"RunMode",str,str_ModelPath);
	}
	m_RunMode = 1;//광축측정만 사용
	pRunCombo->SetCurSel(m_RunMode);

	/*pRunCombo->GetLBText(m_RunMode,str);
	strna = "보임량 "+str;
	ModeName = strna;*/
	if(m_RunMode == 0){
		ModeName = "SW광축보정";
	}else{
		ModeName = "보임량광축측정";
	}

	//2016.09.02 수정:심재원 --------------------------------------------------
	CenterPrm.RetryCnt_Write = GetPrivateProfileInt(str_model,"SectorWrite_RetryCnt", -1, str_ModelPath);
	if(CenterPrm.RetryCnt_Write == -1)
	{
		CenterPrm.RetryCnt_Write = 1;
		str.Empty();
		str.Format("%d",CenterPrm.RetryCnt_Write);
		WritePrivateProfileString(str_model,"SectorWrite_RetryCnt",str,str_ModelPath);
	}
	
	m_cb_RetryCnt.SetCurSel(CenterPrm.RetryCnt_Write);
	//end 2016.09.02  ---------------------------------------------------------


    b_CanOverlayCheck = GetPrivateProfileInt(str_model,"CANOVERLAYCHECK", -1, str_ModelPath);
    if(b_CanOverlayCheck == -1)
    {
        b_CanOverlayCheck = 0;
        str.Empty();
        str.Format("%d",b_CanOverlayCheck);
        WritePrivateProfileString(str_model,"CANOVERLAYCHECK",str,str_ModelPath);
    }
	b_CanOverlayCheck = 0;//값고정
    UpdateData(FALSE);

	SetOptMode(CenterPrm.HWMODE);
	
	//((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->NameChange(strna);
	if(((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd != NULL){
		((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->pRunEdit->SetWindowText(str);
		((CComboBox *)GetDlgItem(IDC_OPT_WRMODE))->GetLBText(CenterPrm.WriteMode,str);
		((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->pWrModeEdit->SetWindowText(str);
	}

	UpdateData(TRUE);
	m_iStandX = CenterPrm.StandX;
	m_iStandY = CenterPrm.StandY;
	m_dPassDeviatX = CenterPrm.PassDeviatX;
	m_dPassDeviatY = CenterPrm.PassDeviatY;
	m_iMaxCount = CenterPrm.MaxCount;
	m_iStandX = CenterPrm.StandX;
	m_iStandY = CenterPrm.StandY;
	m_dPassDeviatX = CenterPrm.PassDeviatX;
	m_dPassDeviatY = CenterPrm.PassDeviatY;
	m_dPassMaxDevX = CenterPrm.PassMaxDevX;
	m_dPassMaxDevY = CenterPrm.PassMaxDevY;
	m_dRateX = CenterPrm.RateX;
	m_dRateY = CenterPrm.RateY;
	m_iEnPixX = CenterPrm.EnPixX;
	m_iEnPixY = CenterPrm.EnPixY;
	m_iWRMODE = CenterPrm.WriteMode;
	UpdateData(FALSE);

	SetFileSel();//bin 파일을 읽어들인후에 경로를 확인한다.
	//************************************************************hjm 추가******************************//

	for(int i=0; i<4; i++)
	{
		CString strNum;
		strNum.Format("CHARTMASTER%d_X", i+1);

		m_ptMasterCoord[i].x = GetPrivateProfileDouble(str_model,strNum,-1,str_ModelPath);
		if(m_ptMasterCoord[i].x == -1){
			m_ptMasterCoord[i].x = Cam_PosY + (i*m_CAM_SIZE_HEIGHT);
			str.Empty();
			str.Format("%d",m_ptMasterCoord[i].x);
			WritePrivateProfileString(str_model,strNum,str,str_ModelPath);
		}
		
		strNum.Format("CHARTMASTER%d_Y", i+1);

		m_ptMasterCoord[i].y = GetPrivateProfileDouble(str_model,strNum,-1,str_ModelPath);
		if(m_ptMasterCoord[i].y == -1){
			m_ptMasterCoord[i].y = (int)(Cam_PosY*2.25);
			str.Empty();
			str.Format("%d",m_ptMasterCoord[i].y);
			WritePrivateProfileString(str_model,strNum,str,str_ModelPath);
		}
	}

//	SetDlgItemInt(IDC_Chart_X, m_ptMasterCoord[0].x);
//	SetDlgItemInt(IDC_CHART_Y, m_ptMasterCoord[0].y);
}
void CCENTER_DETECT_Option::OnBnClickedOpticalstatSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	CenterPrm.MaxCount= m_iMaxCount;
	CenterPrm.RateX= m_dRateX;
	CenterPrm.RateY= m_dRateY;
	CenterPrm.PassMaxDevX= m_dPassMaxDevX;
	CenterPrm.PassMaxDevY= m_dPassMaxDevY;
	CenterPrm.EnPixX = m_iEnPixX;
	CenterPrm.EnPixY = m_iEnPixY;

	Save_parameter();
	
}

void CCENTER_DETECT_Option::OnBnClickedOpticalstatSave2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	CenterPrm.StandX= m_iStandX;
	CenterPrm.StandY= m_iStandY;
	CenterPrm.PassDeviatX= m_dPassDeviatX;
	CenterPrm.PassDeviatY= m_dPassDeviatY;
	CenterPrm.WriteMode= m_iWRMODE;

	CString str = "";
	if(((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd != NULL){
		((CComboBox *)GetDlgItem(IDC_OPT_WRMODE))->GetLBText(CenterPrm.WriteMode,str);
		((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->pWrModeEdit->SetWindowText(str);
	}

	if(((CImageTesterDlg *)m_pMomWnd)->m_pMasterMonitor != NULL){
		((CImageTesterDlg *)m_pMomWnd)->m_pMasterMonitor->InitSet();
	}

	Save_parameter();
}

void CCENTER_DETECT_Option::OnCbnSelchangeComboMode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	CString strna = "";
	m_RunMode = pRunCombo->GetCurSel();
	pRunCombo->GetLBText(m_RunMode,str);

	if(((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd != NULL){
		((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->pRunEdit->SetWindowText(str);
	}
	
	if(m_RunMode == 0){
		ModeName = "SW광축보정";
	}else{
		ModeName = "보임량광축측정";
	}
	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->NameChange(ModeName);
	
	str.Format("%d",m_RunMode);
	WritePrivateProfileString(str_model,"RunMode",str,str_ModelPath);
	
	SetOptMode(CenterPrm.HWMODE);
	
}

//int CCENTER_DETECT_Option::OnSendSerial(_EtcWrdata * INDATA){
//	int ret = 0;
//	ret = MicomOnSendSerial(INDATA);
//	return ret;
//}
				
int CCENTER_DETECT_Option::OffsetWrite(_TOTWrdata * INDATA)
{

	unsigned long nRet = 0, i,j,k,AddressPage, ApplyOffset[4]={0,0,0,0};// ApplyOffset[4]={X,Y,DE,AC}
	unsigned long BuffSize=256;//MAXSIZE,CNTSIZE,
	unsigned char EndOfWrite=0, LOOP = 32;
	unsigned int loopnum = 0, knum = 0, CRC=0;

	DoEvents(1000);

	BYTE SerialDATA[20] ={'!',0x07, 0,0,0,0, 0,0,0,0,'@'};

	((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->m_CtrFileProgress.SetPos(0);
//	((CImageTesterDlg *)m_pMomWnd)->m_pResWorkerOptWnd->m_CtrFileProgress.SetPos(0);

	SerialDATA[1] =0x07; SerialDATA[2] =0x90; SerialDATA[3]=0x03;SerialDATA[6]=0xFF;SerialDATA[7]=0xFF;SerialDATA[8]=0xFF;SerialDATA[9]=0xFF;
	if(!SendData8Byte(SerialDATA, 11,INDATA)){  
		return FALSE;	
	}  //  1ms  BL Mode 진입
	DoEvents(60);

	SerialDATA[1] =0x07;   SerialDATA[2] =0x91; 
	if(!SendData8Byte(SerialDATA, 11,INDATA)){  
		return FALSE;	
	}  //  2ms   Get Info
	DoEvents(60);

	SerialDATA[1] =0x07;     SerialDATA[2] =0x92; SerialDATA[3]=0xa5; SerialDATA[4]=0xF1; SerialDATA[5]=0xFF;//DATA[6]=0xFF;DATA[7]=0xFF;DATA[8]=0xFF;DATA[9]=0xFF;
	if(!SendData8Byte(SerialDATA, 11,INDATA)){  
		return FALSE;	
	}  //  2ms  Flash_Keys
	DoEvents(60);

	if((AddOfOffset[1] >> 12)==0x00){
		AddressPage = 0;
		LOOP = 16;
	}
	else {
		AddressPage = (AddOfOffset[1] >> 8)&0xFFFFFFF0;
		LOOP = 32;
	}

	for(i=0;i<LOOP;i++, AddressPage++){//
		OffsetBuffer[i+32-LOOP][256];
		memcpy(INDATA->t_EtcCh[0].buffer,OffsetBuffer[i+32-LOOP],256);
		memcpy(INDATA->t_EtcCh[1].buffer,OffsetBuffer[i+32-LOOP],256);
		memcpy(INDATA->t_EtcCh[2].buffer,OffsetBuffer[i+32-LOOP],256);
		memcpy(INDATA->t_EtcCh[3].buffer,OffsetBuffer[i+32-LOOP],256);

		if((B_FileChk==TRUE)&&(m_filemode != 0)){//파일의 옵셋값이 존재할때만 적용된다.
			if((AddOfOffset[0]>=AddressPage*BuffSize)&&(AddOfOffset[0]<(AddressPage+1)*BuffSize)){				
				for(int lop = 0;lop<4;lop++){	
					INDATA->t_EtcCh[lop].buffer[AddOfOffset[0]%BuffSize] =  xoffdata + INDATA->t_EtcCh[lop].m_offsetX;
				}
			}

			if((AddOfOffset[1]>=AddressPage*BuffSize)&&(AddOfOffset[1]<(AddressPage+1)*BuffSize)){			
				for(int lop = 0;lop<4;lop++){	
					INDATA->t_EtcCh[lop].buffer[AddOfOffset[1]%BuffSize] =  yoffdata + INDATA->t_EtcCh[lop].m_offsetY;
				}
			}

			if((AddOfOffset[2]>=AddressPage*BuffSize)&&(AddOfOffset[2]<(AddressPage+1)*BuffSize)){				
				if(m_filemode == 2){		//return = 2이면 메가이다. 
					for(int lop = 0;lop<4;lop++){
						if(INDATA->t_EtcCh[lop].m_offsetX+xoffdata < 0){
							if(INDATA->t_EtcCh[lop].m_offsetY+yoffdata < 0){
								INDATA->t_EtcCh[lop].buffer[AddOfOffset[2]%BuffSize] =  m_DChkSum1 -2; //둘다 -일때
							}else{
								INDATA->t_EtcCh[lop].buffer[AddOfOffset[2]%BuffSize] =  m_DChkSum1 -1; //둘중 하나만 -일때
							}
						}else{
							if(INDATA->t_EtcCh[lop].m_offsetY+yoffdata < 0){
								INDATA->t_EtcCh[lop].buffer[AddOfOffset[2]%BuffSize] =  m_DChkSum1 -1; //둘중 하나만 -일때
							}else{
								INDATA->t_EtcCh[lop].buffer[AddOfOffset[2]%BuffSize] =  m_DChkSum1; //둘다 +일때
							}
						}
					}
				}else{ //메가가 아닌 일반일경우 
					for(int lop = 0;lop<4;lop++){	
						INDATA->t_EtcCh[lop].buffer[AddOfOffset[2]%BuffSize] =  m_DChkSum1; //둘다 +일때
					}
				}
			}
			
			if((AddOfOffset[3]>=AddressPage*BuffSize)&&(AddOfOffset[3]<(AddressPage+1)*BuffSize)){				
				for(int lop = 0;lop<4;lop++){
					INDATA->t_EtcCh[lop].buffer[AddOfOffset[3]%BuffSize] =  m_DChkSum2 - (INDATA->t_EtcCh[lop].m_offsetX) - (INDATA->t_EtcCh[lop].m_offsetY);
				}
			}
		}

		SerialDATA[1] =0x07; SerialDATA[2] =0x93; SerialDATA[3] =0x00; SerialDATA[4] =0x00;SerialDATA[5] =0x00;SerialDATA[6] =0x00;
		SerialDATA[3] = 0x00;
		SerialDATA[5] = (AddressPage>> 0x00) & 0x000000FF ;
		SerialDATA[6] = (AddressPage>> 0x08) & 0x000000FF ;
		SerialDATA[4] = (AddressPage>> 0x10) & 0x000000FF ;  //??

		SerialDATA[8] =0x00;SerialDATA[9] =0x00;

		if(!SendData8Byte(SerialDATA, 11,INDATA)){  
			return FALSE;	
		}//set Address
		((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->m_CtrFileProgress.SetPos((unsigned int)(((i+1)*1000)/LOOP));
//		((CImageTesterDlg *)m_pMomWnd)->m_pResWorkerOptWnd->m_CtrFileProgress.SetPos((unsigned int)(((i+1)*1000)/LOOP));
		DoEvents(30); // 2ms

		SerialDATA[1] = 0x08;  // Set Data Mode  0x08

		for(j=0;j < 32; j++){ //1 page = 32 * 1 packet = 32* 8 = 256 //1 packet = 8 byte
			for(k=0;k<8;k++){ 
				for(k=0;k<8;k++){
					for(int lop = 0;lop<4;lop++){
						INDATA->t_EtcCh[lop].DATA[k+2] = INDATA->t_EtcCh[lop].buffer[j*8+k];
					}
				}
				if(!EtcSendData8Byte(INDATA, 11)){  
					return FALSE;	
				}
			}
		}
		DoEvents(150);
//		DoEvents(m_dwd1pdelay);	
		if(AddressPage%16==0){
//			DATA[1] =0x07; DATA[2] =0x94; DATA[3]=0x00;
//			if(!SendData8Byte(DATA, 11,INDATA)){  
//				return FALSE;	
//			}
			DoEvents(400);
//			DoEvents(m_dwd16pdelay);
		}
	}

	SerialDATA[1] =0x07; SerialDATA[2] =0x97; SerialDATA[3]=0x3D; SerialDATA[4]=0xC2; SerialDATA[5]=0xA5; SerialDATA[6]=0x1B; SerialDATA[7]=0xFF; SerialDATA[8]=0xFF; SerialDATA[9]=0xFF;
	if(!SendData8Byte(SerialDATA, 11,INDATA)){  
		return FALSE;	
	}  //  2ms  Write Signature
	DoEvents(60);

	SerialDATA[1] =0x07; SerialDATA[2] =0x92; SerialDATA[3]=0x00; SerialDATA[4]=0x00; //DATA[5]=0xA5; DATA[6]=0x1B; DATA[7]=0xFF; DATA[8]=0xFF; DATA[9]=0xFF;
	if(!SendData8Byte(SerialDATA, 11,INDATA)){  
		return FALSE;	
	}  //  2ms  Flash_Keys
	DoEvents(60);

	SerialDATA[1] =0x07;  SerialDATA[2] =0x98; SerialDATA[3]=0x00;
	if(!SendData8Byte(SerialDATA, 11,INDATA)){  
		return FALSE;	
	}  //  100ms  Software Reset
	DoEvents(500);
	((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->m_CtrFileProgress.SetPos(0);
//	((CImageTesterDlg *)m_pMomWnd)->m_pResWorkerOptWnd->m_CtrFileProgress.SetPos(0);
	DoEvents(500);

	return TRUE;
}



BOOL CCENTER_DETECT_Option::SendData8Byte(unsigned char * TxData, int Num,_TOTWrdata * INDATA)
{
	
	BOOL b_Ret;

	return b_Ret;
}

BOOL CCENTER_DETECT_Option::EtcSendData8Byte(_TOTWrdata * INDATA,int Num)
{
	char ret = 0;
	BOOL b_Ret = FALSE;
	for(int lop = 0;lop<4;lop++){
		if(INDATA->t_EtcCh[lop].m_Enable == TRUE){
			INDATA->t_EtcCh[lop].DATA[0] = '!';
			INDATA->t_EtcCh[lop].DATA[1] = 0x08;
			INDATA->t_EtcCh[lop].DATA[10] = '@';
//			ret = ((CImageTesterDlg *)m_pMomWnd)->m_pEtcCh[lop]->SendData8Byte(INDATA->t_EtcCh[lop].DATA,Num);
			if(ret == FALSE){
				INDATA->t_EtcCh[lop].m_Enable = FALSE;
				INDATA->t_EtcCh[lop].m_success = FALSE;
			}else{
				b_Ret = TRUE;
			}
		}
	}
	return b_Ret;
}

void CCENTER_DETECT_Option::centergen(double centx,double centy,_EtcWrdata *EtcDATA)
{
//	int mbuf;
	double TmpX, TmpY;
	double bufx, bufy;
	char m_Offsetx_buf;
	char m_Offsety_buf;
	CString str="";
	char m_F_Offsetx_buf;
	char m_F_Offsety_buf;
	
	long bufcenterx = (long)((centx+0.005) *100);
	long bufcentery = (long)((centy+0.005) *100);

	double d_F_X_ASIX = m_CAM_SIZE_WIDTH - (double)bufcenterx/100;
	double d_F_Y_ASIX = (double)bufcentery/100;//왜 이렇게 되어있는가?
	
	EtcDATA->m_F_X_ASIX = d_F_X_ASIX;
	EtcDATA->m_F_Y_ASIX = d_F_Y_ASIX;


	EtcDATA->m_F_X_OFFSET = (d_F_X_ASIX - CenterPrm.StandX); //물리적으로 광축이 벗어난 정도 
	EtcDATA->m_F_Y_OFFSET = (d_F_Y_ASIX - CenterPrm.StandY);

	if(CenterPrm.WriteMode == 0){ //좌우반전이므로 계측된 x축은 반대다.
		bufx = d_F_X_ASIX;//계측 광축 X 360근처
		bufy = d_F_Y_ASIX;//계측 광축 Y 240근처
		m_Offsetx_buf = (char)EtcDATA->m_F_X_OFFSET;
		m_Offsety_buf = (char)EtcDATA->m_F_Y_OFFSET;
	}else if(CenterPrm.WriteMode == 1){//상하반전이므로 계측된 y축은 반대다. 
		bufx = m_CAM_SIZE_WIDTH - d_F_X_ASIX;
		bufy = m_CAM_SIZE_HEIGHT - d_F_Y_ASIX;//계측 광축 Y 240근처
		m_Offsetx_buf = -(char)EtcDATA->m_F_X_OFFSET;
		m_Offsety_buf = -(char)EtcDATA->m_F_Y_OFFSET;
	}else if(CenterPrm.WriteMode == 2){//무반전이므로 계측된 x와 y를 그대로 적용한다. 
		bufx = m_CAM_SIZE_WIDTH - d_F_X_ASIX;
		bufy = d_F_Y_ASIX;
		m_Offsetx_buf = -(char)EtcDATA->m_F_X_OFFSET;
		m_Offsety_buf = (char)EtcDATA->m_F_Y_OFFSET;
	}else if(CenterPrm.WriteMode == 3){//로테이트이므로 x와 y 둘다 반대로 적용된다. 
		bufx = d_F_X_ASIX;
		bufy = m_CAM_SIZE_HEIGHT - d_F_Y_ASIX;
		m_Offsetx_buf = (char)EtcDATA->m_F_X_OFFSET;
		m_Offsety_buf = -(char)EtcDATA->m_F_Y_OFFSET;
	}else{
		bufx = m_CAM_SIZE_WIDTH - d_F_X_ASIX;//이 부분에 모드에 따라 표현되는 방식이 틀려진다.
		bufy = d_F_Y_ASIX;
	}
	
	if(EtcDATA->m_Init_X == 99999 && EtcDATA->m_Init_Y == 99999)
	{
		EtcDATA->m_Init_X = EtcDATA->m_F_X_ASIX;
		EtcDATA->m_Init_Y= EtcDATA->m_F_Y_ASIX;
	}

	EtcDATA->m_X_ASIX = bufx;
	EtcDATA->m_Y_ASIX = bufy;

	//TmpX = (bufx - CenterPrm.StandX); //물리적으로 광축이 벗어난 정도 
	//if((TmpX) > 0.0)	m_Offsetx_buf = (char)((TmpX)+0.5) ;//+ 0x10  ; //+ 0x1E/2 = 15[dec]
	//else m_Offsetx_buf = (char)((TmpX)-0.5) ;//+ 0x10  ; //+ 0x1E/2 = 15[dec]
	//
	//TmpY = (bufy - CenterPrm.StandY);
	//if((TmpY) > 0.0) m_Offsety_buf = (char)((TmpY)+0.5); //+ 0x16/2 = 11[dec]
	//else  m_Offsety_buf = (char)((TmpY) - 0.5); //+ 0x16/2 = 11[dec]

	char m_Offsetx_buf2 = (unsigned char)(m_Offsetx_buf * CenterPrm.RateX);
	char m_Offsety_buf2 = (unsigned char)(m_Offsety_buf * CenterPrm.RateY);
	
	EtcDATA->m_X_OFFSET = m_Offsetx_buf;
	EtcDATA->m_Y_OFFSET = m_Offsety_buf;
	
	if(EtcDATA->m_Init_Offset_X == 99999 && EtcDATA->m_Init_Offset_Y == 99999)
	{
		EtcDATA->m_Init_Offset_X = m_Offsetx_buf;
		EtcDATA->m_Init_Offset_Y = m_Offsety_buf;
	}


	if(((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd != NULL){
		str.Format("%d",EtcDATA->m_F_X_OFFSET);//-
		((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->OffsetX_Txt(EtcDATA->m_number,str);
		str.Format("%d",EtcDATA->m_F_Y_OFFSET);//-
		((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->OffsetY_Txt(EtcDATA->m_number,str);
		str.Format("%5.2f",EtcDATA->m_F_X_ASIX);
		((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->AxisX_Txt(EtcDATA->m_number,str);
		str.Format("%5.2f",EtcDATA->m_F_Y_ASIX);
		((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->AxisY_Txt(EtcDATA->m_number,str);
	}
	
	//중심점 검출후 합격 범위안에 들어오면 성공표시를 하고 테스트를 스톱한다.
	 
	int buf_offx = 0;
	int buf_offy = 0;

	if(EtcDATA->m_F_X_ASIX <= (CenterPrm.StandX + CenterPrm.PassDeviatX)&&
	   EtcDATA->m_F_X_ASIX >= (CenterPrm.StandX - CenterPrm.PassDeviatX)&&
	   EtcDATA->m_F_Y_ASIX >= (CenterPrm.StandY - CenterPrm.PassDeviatY)&&
	   EtcDATA->m_F_Y_ASIX <= (CenterPrm.StandY + CenterPrm.PassDeviatY)){
	   EtcDATA->m_success = TRUE;
		if((EtcDATA->m_count >= CenterPrm.MaxCount)||
			(EtcDATA->m_F_X_ASIX <= (CenterPrm.StandX + CenterPrm.PassMaxDevX)&&
			EtcDATA->m_F_X_ASIX >= (CenterPrm.StandX - CenterPrm.PassMaxDevX)&&
			EtcDATA->m_F_Y_ASIX >= (CenterPrm.StandY - CenterPrm.PassMaxDevY)&&
			EtcDATA->m_F_Y_ASIX <= (CenterPrm.StandY + CenterPrm.PassMaxDevY)))
		{
			EtcDATA->m_Enable = FALSE;
		}else{
			EtcDATA->m_count++;
			EtcDATA->m_Enable = TRUE;
			EtcDATA->m_oldoffsetX = EtcDATA->m_offsetX;
			EtcDATA->m_oldoffsetY = EtcDATA->m_offsetY;
			if(CenterPrm.HWMODE == 1){
				EtcDATA->m_offsetX = (-1)*(m_Offsetx_buf2);//펌웨어 다운로드의 경우 검출값을 그대로 쓴다.
				EtcDATA->m_offsetY = (m_Offsety_buf2);

				buf_offx = EtcDATA->m_offsetX + (int)((CImageTesterDlg *)m_pMomWnd)->m_RDOffset[0]; 
				buf_offy = EtcDATA->m_offsetY + (int)((CImageTesterDlg *)m_pMomWnd)->m_RDOffset[1];
				
				if(buf_offx > CenterPrm.EnPixX){
					if(EtcDATA->m_offsetX == EtcDATA->m_oldoffsetX){
						EtcDATA->m_Enable = FALSE;
						StatePrintf("X값 조정이 불가능합니다.");
					}
				}else if(buf_offx < -CenterPrm.EnPixX){
					if(EtcDATA->m_offsetX == EtcDATA->m_oldoffsetX){
						EtcDATA->m_Enable = FALSE;
						StatePrintf("X값 조정이 불가능합니다.");
					}
				}

				if(buf_offy > CenterPrm.EnPixY){
					if(EtcDATA->m_offsetY == EtcDATA->m_oldoffsetY){
						EtcDATA->m_Enable = FALSE;
						StatePrintf("Y값 조정이 불가능합니다.");
					}
				}else if(buf_offy < -CenterPrm.EnPixY){
					if(EtcDATA->m_offsetY == EtcDATA->m_oldoffsetY){
						EtcDATA->m_Enable = FALSE;
						StatePrintf("Y값 조정이 불가능합니다.");
					}
				}

			}else{
				EtcDATA->m_offsetX += (-1)*(m_Offsetx_buf2);//펌웨어 다운로드의 경우 검출값을 그대로 쓴다.
				EtcDATA->m_offsetY += (m_Offsety_buf2);

				if(EtcDATA->m_offsetX > CenterPrm.EnPixX){
					if(EtcDATA->m_offsetX != EtcDATA->m_oldoffsetX){
						EtcDATA->m_offsetX = CenterPrm.EnPixX;
						StatePrintf("X값이 조정할수 있는 범위를 넘어섰습니다");
					}else{
						EtcDATA->m_Enable = FALSE;
						StatePrintf("X값 조정이 불가능합니다.");
					}
				}else if(EtcDATA->m_offsetX < -CenterPrm.EnPixX){
					if(EtcDATA->m_offsetX != EtcDATA->m_oldoffsetX){
						EtcDATA->m_offsetX = -CenterPrm.EnPixX;
						StatePrintf("X값이 조정할수 있는 범위를 넘어섰습니다");
					}else{
						EtcDATA->m_Enable = FALSE;
						StatePrintf("X값 조정이 불가능합니다.");
					}
				}

				if(EtcDATA->m_offsetY > CenterPrm.EnPixY){
					if(EtcDATA->m_offsetY != EtcDATA->m_oldoffsetY){
						EtcDATA->m_offsetY = CenterPrm.EnPixY;
						StatePrintf("Y값이 조정할수 있는 범위를 넘어섰습니다");
					}else{
						EtcDATA->m_Enable = FALSE;
						StatePrintf("Y값 조정이 더이상 불가능합니다.");
					}
				}else if(EtcDATA->m_offsetY < -CenterPrm.EnPixY){
					if(EtcDATA->m_offsetY != EtcDATA->m_oldoffsetY){
						EtcDATA->m_offsetY = -CenterPrm.EnPixY;
						StatePrintf("Y값이 조정할수 있는 범위를 넘어섰습니다");
					}else{
						EtcDATA->m_Enable = FALSE;
						StatePrintf("Y값 조정이 더이상 불가능합니다.");
					}
				}			
			}
			
		}
	}else{//실패할경우 write에 적용할 수치를 적용하고 끝낸다. 
		if(EtcDATA->m_count >= CenterPrm.MaxCount){
			EtcDATA->m_success = FALSE;
			EtcDATA->m_Enable = FALSE;
		}else{
			EtcDATA->m_count++;
			EtcDATA->m_oldoffsetX = EtcDATA->m_offsetX;
			EtcDATA->m_oldoffsetY = EtcDATA->m_offsetY;
			if(CenterPrm.HWMODE == 1){
				EtcDATA->m_offsetX = (-1)*(m_Offsetx_buf2);//펌웨어 다운로드의 경우 검출값을 그대로 쓴다.
				EtcDATA->m_offsetY = (m_Offsety_buf2);
				if(buf_offx > CenterPrm.EnPixX){
					if(EtcDATA->m_offsetX == EtcDATA->m_oldoffsetX){
						EtcDATA->m_Enable = FALSE;
						EtcDATA->m_success = FALSE;
						StatePrintf("X값 조정이 불가능합니다.");
					}
				}else if(buf_offx < -CenterPrm.EnPixX){
					if(EtcDATA->m_offsetX == EtcDATA->m_oldoffsetX){
						EtcDATA->m_Enable = FALSE;
						StatePrintf("X값 조정이 불가능합니다.");
					}
				}

				if(buf_offy > CenterPrm.EnPixY){
					if(EtcDATA->m_offsetY == EtcDATA->m_oldoffsetY){
						EtcDATA->m_Enable = FALSE;
						EtcDATA->m_success = FALSE;
						StatePrintf("Y값 조정이 불가능합니다.");
					}
				}else if(buf_offy < -CenterPrm.EnPixY){
					if(EtcDATA->m_offsetY == EtcDATA->m_oldoffsetY){
						EtcDATA->m_Enable = FALSE;
						StatePrintf("Y값 조정이 불가능합니다.");
					}
				}

			}else{
				EtcDATA->m_offsetX += (-1)*(m_Offsetx_buf2);//펌웨어 다운로드의 경우 검출값을 그대로 쓴다.
				EtcDATA->m_offsetY += (m_Offsety_buf2);

				if(EtcDATA->m_offsetX >= CenterPrm.EnPixX){
					if(EtcDATA->m_offsetX != EtcDATA->m_oldoffsetX){
						EtcDATA->m_offsetX = CenterPrm.EnPixX;
						StatePrintf("X값이 조정할수 있는 범위를 넘어섰습니다");
					}else{
						EtcDATA->m_Enable = FALSE;
						StatePrintf("X값 조정이 불가능합니다.");
					}
				}else if(EtcDATA->m_offsetX <= -CenterPrm.EnPixX){
					if(EtcDATA->m_offsetX != EtcDATA->m_oldoffsetX){
						EtcDATA->m_offsetX = -CenterPrm.EnPixX;
						StatePrintf("X값이 조정할수 있는 범위를 넘어섰습니다");
					}else{
						EtcDATA->m_Enable = FALSE;
						EtcDATA->m_success = FALSE;
						StatePrintf("X값 조정이 불가능합니다.");
					}
				}

				if(EtcDATA->m_offsetY >= CenterPrm.EnPixY){
					if(EtcDATA->m_offsetY != EtcDATA->m_oldoffsetY){
						EtcDATA->m_offsetY = CenterPrm.EnPixY;
						StatePrintf("Y값이 조정할수 있는 범위를 넘어섰습니다");
					}else{
						EtcDATA->m_Enable = FALSE;
						EtcDATA->m_success = FALSE;
						StatePrintf("Y값 조정이 더이상 불가능합니다.");
					}
				}else if(EtcDATA->m_offsetY <= -CenterPrm.EnPixY){
					if(EtcDATA->m_offsetY != EtcDATA->m_oldoffsetY){
						EtcDATA->m_offsetY = -CenterPrm.EnPixY;
						StatePrintf("Y값이 조정할수 있는 범위를 넘어섰습니다");
					}else{
						EtcDATA->m_Enable = FALSE;
						EtcDATA->m_success = FALSE;
						StatePrintf("Y값 조정이 더이상 불가능합니다.");
					}
				}

			}
		}
	}
}

void CCENTER_DETECT_Option::Initgen(_EtcWrdata *EtcDATA)
{
	memset(EtcDATA->buffer,0,sizeof(EtcDATA->buffer));
	memset(EtcDATA->DATA,0,sizeof(EtcDATA->DATA));
	EtcDATA->m_Enable = TRUE;
	EtcDATA->m_success = FALSE;
	EtcDATA->m_X_ASIX = 0;
	EtcDATA->m_Y_ASIX = 0;
	EtcDATA->m_F_X_ASIX = 0;
	EtcDATA->m_F_Y_ASIX = 0;
	EtcDATA->m_X_OFFSET = 0;
	EtcDATA->m_Y_OFFSET = 0;
	EtcDATA->m_offsetX = 0;
	EtcDATA->m_offsetY = 0;
	EtcDATA->m_oldoffsetX = 0;
	EtcDATA->m_oldoffsetY = 0;
	EtcDATA->str_state = "";
	EtcDATA->m_Init_Offset_X = 99999;
	EtcDATA->m_Init_Offset_Y = 99999;
	EtcDATA->m_Init_X = 99999;
	EtcDATA->m_Init_Y = 99999;
	EtcDATA->m_count = 0;
	EtcPt[0].x= 0;
	EtcPt[0].y= 0;
	((CImageTesterDlg *)m_pMomWnd)->m_RDOffset[0] = 0;
	((CImageTesterDlg *)m_pMomWnd)->m_RDOffset[1] = 0;
}

void CCENTER_DETECT_Option::MoveCenter(_EtcWrdata * INDATA) 
{
	
	if(CenterPrm.HWMODE == 1){
	//	CanDLOffsetWr(INDATA,0);
	}else{
		StatePrintf("오프셋 기본값으로 셋팅합니다.");
		if(CenterPrm.INITWRSEL == 0){
			CanOnSendSerial(INDATA,0);
		}else if(CenterPrm.INITWRSEL == 1){
			CanOffsetWrite(INDATA,0);
		}
	}
}

int CCENTER_DETECT_Option::OnSendSerial(_EtcWrdata * INDATA) 
{
	int ret = TRUE;
	if(CenterPrm.HWMODE == 1) // 쓰기모드 : DCC CAN 조정모드
	{
		ret = CanDLOffsetWr(INDATA,1);
	}
	else
	{
		if(CenterPrm.MIDWRSEL == 0) // 조정 Write : FULL
		{
			ret = CanOnSendSerial(INDATA);
		}
		else if(CenterPrm.MIDWRSEL == 1) // 조정 Write : SECTOR
		{
			ret = CanOffsetWrite(INDATA);
		}
	}
	return ret;
}


int CCENTER_DETECT_Option::MicomOnSendSerial(_EtcWrdata * INDATA) 
{	

	/*((CImageTesterDlg *)m_pMomWnd)->m_pMicomCenterWnd->edit_offsetX.Format("%d",INDATA->m_offsetX);
	((CImageTesterDlg *)m_pMomWnd)->m_pMicomCenterWnd->edit_offsetY.Format("%d",INDATA->m_offsetY);
	((CImageTesterDlg *)m_pMomWnd)->m_pMicomCenterWnd->UpdateData(FALSE);

	if(!((CImageTesterDlg *)m_pMomWnd)->m_pMicomCenterWnd->SettingPosWrite()){
		return FALSE;
	}*/

	return TRUE;
}

int CCENTER_DETECT_Option::CanDLOffsetWr(_EtcWrdata * INDATA,bool stat) 
{
	char RDOFFSET_X = 0; 
	char RDOFFSET_Y = 0;

	if(stat == 0){
		RDOFFSET_X = 0;
		RDOFFSET_Y = 0;
	}else{
		if(((CImageTesterDlg *)m_pMomWnd)->DL_OFFSETRD() == FALSE){
			if(((CImageTesterDlg *)m_pMomWnd)->DL_OFFSETRD() == FALSE){
				INDATA->m_Enable = FALSE;
				INDATA->m_success = FALSE;
				INDATA->str_state = "OFFSET READ 오류";
				return FALSE;
			}
		}	

		RDOFFSET_X = (char)((CImageTesterDlg *)m_pMomWnd)->m_RDOffset[0];
		RDOFFSET_Y = (char)((CImageTesterDlg *)m_pMomWnd)->m_RDOffset[1];
		RDOFFSET_X += INDATA->m_offsetX;
		RDOFFSET_Y += INDATA->m_offsetY;

		if(RDOFFSET_X >CenterPrm.EnPixX){
			RDOFFSET_X = CenterPrm.EnPixX;
			StatePrintf("X값이 조정할수 있는 범위를 넘어섰습니다");
		}else if(RDOFFSET_X < -CenterPrm.EnPixX){
			RDOFFSET_X = -CenterPrm.EnPixX;
			StatePrintf("X값이 조정할수 있는 범위를 넘어섰습니다");
		}

		if(RDOFFSET_Y >CenterPrm.EnPixY){
			RDOFFSET_Y = CenterPrm.EnPixY;
			StatePrintf("Y값이 조정할수 있는 범위를 넘어섰습니다");
		}else if(RDOFFSET_Y < -CenterPrm.EnPixY){
			RDOFFSET_Y = -CenterPrm.EnPixY;
			StatePrintf("Y값이 조정할수 있는 범위를 넘어섰습니다");
		}

	}
	DoEvents(500);

	if(((CImageTesterDlg *)m_pMomWnd)->DL_OFFSETWR((BYTE)RDOFFSET_X,(BYTE)RDOFFSET_Y) == FALSE){
		if(((CImageTesterDlg *)m_pMomWnd)->DL_OFFSETWR((BYTE)RDOFFSET_X,(BYTE)RDOFFSET_Y) == FALSE){
			INDATA->m_Enable = FALSE;
			INDATA->m_success = FALSE;
			INDATA->str_state = "OFFSET WRITE 오류";
			
			//return FALSE;
			return -1;	// 2016.09.01 수정: 심재원
		}
	}
	
	DoEvents(500);
	if(((CImageTesterDlg *)m_pMomWnd)->DL_OFFSETRD() == FALSE){
		if(((CImageTesterDlg *)m_pMomWnd)->DL_OFFSETRD() == FALSE){
			INDATA->m_Enable = FALSE;
			INDATA->m_success = FALSE;
			INDATA->str_state = "OFFSET READ 오류";
			return FALSE;
		}
	}
	
	if((RDOFFSET_X == (char)((CImageTesterDlg *)m_pMomWnd)->m_RDOffset[0])&&(RDOFFSET_Y = (char)((CImageTesterDlg *)m_pMomWnd)->m_RDOffset[1]))
	{
		return TRUE;
	}else{
		return FALSE;
	}
}

int CCENTER_DETECT_Option::CanOnSendSerial(_EtcWrdata * INDATA) 
{
	return CanOnSendSerial(INDATA,1);
}

int CCENTER_DETECT_Option::CanOnSendSerial(_EtcWrdata * INDATA,bool stat) 
{	
	if(BinfileName == "")
	{
		INDATA->m_Enable = FALSE;
		INDATA->m_success = FALSE;
		INDATA->str_state = "BIN파일 READ 오류";

		return FALSE;
	}

	if(B_FileRead == FALSE)
	{ //오류가 발생하면
		INDATA->m_Enable = FALSE;
		INDATA->m_success = FALSE;
		INDATA->str_state = "BIN파일 READ 오류";
		StatePrintf("BIN파일 READ 오류");

		return FALSE;
	}

	((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->m_CtrFileProgress.SetPos(0);

	unsigned long MAXSIZE,CNTSIZE, BuffSize=0;
	unsigned long Address = 0;
	int mcnt = 0;
	int maxlop = 0,remain = 0,buf_maxlop = 0;
	int j = 0,aj =0,k =0;
	int lop = 0;

	BuffSize = sizeof(INDATA->buffer);
	if(((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(1) == FALSE)
	{
		if(((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(1) == FALSE)
		{
			INDATA->m_Enable = FALSE;
			INDATA->m_success = FALSE;
			INDATA->str_state = "다운로드실패";	
			StatePrintf("다운로드 모드진입 실패");
			return FALSE;	
		}
	}
	DoEvents(1000);

	if(((CImageTesterDlg *)m_pMomWnd)->DL_ADDRSET(0,m_BINSIZE) == FALSE)
	{
		if(((CImageTesterDlg *)m_pMomWnd)->DL_ADDRSET(0,m_BINSIZE) == FALSE)
		{
			INDATA->m_Enable = FALSE;
			INDATA->m_success = FALSE;
			INDATA->str_state = "다운로드실패";
			((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(0);
			StatePrintf("어드레스 셋팅 실패");

			return FALSE;	
		}
	}	

	BYTE BL_CNT = 0;
	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->ProgressEn(1);
	BINBUF[254] = 0x00;
	BINBUF[255] = 0x00;

#ifdef APPLY_CAN_WRITE_PATCH
	// 2016.09.05 수정 : 심재원 -------------------------------------------
	CString szLog;
	szLog.Format(_T("Address : %08X, Size : %04X"), 0, m_BINSIZE);
	LogWriteToFile_Center(szLog);

	szLog.Empty();
	for (UINT nIdx = 0; nIdx < m_BINSIZE; nIdx++)
	{				
		szLog.AppendChar(Hex2Ascii(BINBUF[nIdx]));
	}			
	LogWriteToFile_Center(szLog);
	// 2016.09.05 수정 끝 -------------------------------------------------
#endif

	for(CNTSIZE = 0;CNTSIZE < m_BINSIZE;CNTSIZE+=254)
	{
		BL_CNT++;
		memcpy(INDATA->buffer,&BINBUF[CNTSIZE],254);
		((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->m_CtrFileProgress.SetPos((unsigned int)((CNTSIZE*1000)/m_BINSIZE));
		((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->m_CtrProgress.SetPos((unsigned int)((CNTSIZE*1000)/m_BINSIZE));
		
		if((B_FileChk==TRUE)&&(stat == 1))
		{//파일의 옵셋값이 존재할때만 적용된다.
			if((AddOfOffset[0]>= CNTSIZE)&&(AddOfOffset[0]<(CNTSIZE + 254)))
			{				
				INDATA->buffer[AddOfOffset[0]%254] =  xoffdata + INDATA->m_offsetX;
			}

			if((AddOfOffset[1]>= CNTSIZE)&&(AddOfOffset[1]<(CNTSIZE + 254)))
			{			
				INDATA->buffer[AddOfOffset[1]%254] =  yoffdata + INDATA->m_offsetY;
			}
			
			if((AddOfOffset[2]>= CNTSIZE)&&(AddOfOffset[2]<(CNTSIZE + 254)))
			{			
				if(m_binmod == 0)
				{	//m_binmod = 1이면 메가이다.	//return = 2이면 메가이다. 
					INDATA->buffer[AddOfOffset[2]%254] =  m_DChkSum1; //둘다 +일때
				}
				else
				{ //메가가 아닌 일반일경우 
					mcnt = 0;
					if(INDATA->m_offsetX+xoffdata < 0)
					{
						mcnt++;
					}

					if(yoffdata + INDATA->m_offsetY < 0)
					{
						mcnt++;
					}

					INDATA->buffer[AddOfOffset[2]%254] =  m_DChkSum1 - mcnt; //둘다 -일때
				}
			}
			
			if((AddOfOffset[3]>= CNTSIZE)&&(AddOfOffset[3]<(CNTSIZE + 254)))
			{				
				INDATA->buffer[AddOfOffset[3]%254] =  m_DChkSum2 - (INDATA->m_offsetX) - (INDATA->m_offsetY);
			}
		}

		BYTE SESIZE = 0;
		int lopCnt = 0;

		if(CNTSIZE+254 <= m_BINSIZE)
		{
			SESIZE = 254;
		}
		else
		{
			SESIZE = (BYTE)(m_BINSIZE - CNTSIZE);
		}

		lopCnt = SESIZE/8;
		
		if(lopCnt == 31)
		{
			lopCnt+=1;
		}
		else
		{
			lopCnt+=2;
			memset(&INDATA->buffer[SESIZE],0,256-SESIZE);
		}
		
		for(int lop = 0;lop<lopCnt;lop++)
		{
			if(((CImageTesterDlg *)m_pMomWnd)->DL_DATASET(INDATA,lop) == FALSE)
			{
				if(((CImageTesterDlg *)m_pMomWnd)->DL_DATASET(INDATA,lop) == FALSE)
				{//실패시 한번 더 보낸다.
					INDATA->m_Enable = FALSE;
					INDATA->m_success = FALSE;
					INDATA->str_state = "다운로드실패";	
					((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(0);//다운로드 모드를 빠져나온다.
					((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->ProgressEn(0);

					//return FALSE;
					return -1;	// 2016.09.01 수정: 심재원
				}
			}
		//	DoEvents(3);
		}

		if(((CImageTesterDlg *)m_pMomWnd)->DL_DATAWR(BL_CNT,SESIZE) == FALSE)
		{
//			if(((CImageTesterDlg *)m_pMomWnd)->DL_DATAWR(BL_CNT) == FALSE){
				INDATA->m_Enable = FALSE;
				INDATA->m_success = FALSE;
				INDATA->str_state = "다운로드실패";	
				((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(0);//다운로드 모드를 빠져나온다.
				((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->ProgressEn(0);

				//return FALSE;
				return -1;	// 2016.09.01 수정: 심재원
//			}
		}
	}

	if(((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(0) == FALSE)
	{
		if(((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(0) == FALSE)
		{
				
		}
	}

	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->ProgressEn(0);

	return TRUE;
}

int CCENTER_DETECT_Option::CanOffsetWrite(_EtcWrdata * INDATA)
{
	return CanOffsetWrite(INDATA,1);
}

int CCENTER_DETECT_Option::CanOffsetWrite(_EtcWrdata * INDATA,bool stat) 
{	
	if(BinfileName == "")
	{
		INDATA->m_Enable = FALSE;
		INDATA->m_success = FALSE;
		INDATA->str_state = "BIN파일 설정오류";
		return FALSE;
	}

	if(B_FileRead == FALSE)
	{ //오류가 발생하면	
		INDATA->m_Enable = FALSE;
		INDATA->m_success = FALSE;
		INDATA->str_state = "BIN파일 READ 오류";
		return FALSE;
	}

	/*((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(1);
	DoEvents(1000);
	((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(0);
	return 1;*/

	unsigned long	AddressCnt = 0,EndAddressCnt = 0,DLSIZE = 0,CNTSIZE = 0;
	
	BYTE LOOP = 0;
	BYTE ENDLOOP = 0;
	int mcnt = 0;

	((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->m_CtrFileProgress.SetPos(0);

	if(IndexOffset == 0)
	{
		AddressCnt = 0;
	}
	else
	{
		AddressCnt = IndexOffset;
	}

	if(IndexOffset == IndexOffset2)
	{
		EndAddressCnt = AddressCnt + 0x1000;//0x1000;	
		DLSIZE = 0x1000;
		//EndAddressCnt = AddressCnt + AddOfOffset[1]+1;//0x1000;	
		//DLSIZE = AddOfOffset[1]+1;
	}
	else
	{
		EndAddressCnt = AddressCnt + 0x2000;
		DLSIZE = 0x2000;
	}

	if(((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(1) == FALSE)
	{
		if(((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(1) == FALSE)
		{
			INDATA->m_Enable = FALSE;
			INDATA->m_success = FALSE;
			INDATA->str_state = "다운로드실패";	

			return FALSE;	
		}
	}
	DoEvents(1000);
	/*AddressCnt = 0;
	DLSIZE = 0x222EA;*/
	if(((CImageTesterDlg *)m_pMomWnd)->DL_ADDRSET(AddressCnt,DLSIZE) == FALSE)
	{
		if(((CImageTesterDlg *)m_pMomWnd)->DL_ADDRSET(AddressCnt,DLSIZE) == FALSE)
		{
			INDATA->m_Enable = FALSE;
			INDATA->m_success = FALSE;
			INDATA->str_state = "다운로드실패";
			((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(0);

			return FALSE;
		}
	}	

	BYTE BL_CNT = 0;
	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->ProgressEn(1);

#ifdef APPLY_CAN_WRITE_PATCH
	// 2016.09.05 수정 : 심재원 -------------------------------------------
	CString szLog;
	szLog.Format(_T("Address : %08X, Size : %04X"), AddressCnt, DLSIZE);
	LogWriteToFile_Center(szLog);
	
	szLog.Empty();
	for (UINT nIdx = 0; nIdx < DLSIZE; nIdx++)
	{				
		szLog.AppendChar(Hex2Ascii(BINBUF[AddressCnt + nIdx]));
	}			
	LogWriteToFile_Center(szLog);
	// 2016.09.05 수정 끝 -------------------------------------------------
#endif

	for(CNTSIZE = AddressCnt;CNTSIZE < EndAddressCnt;CNTSIZE+=254)
	{
		BL_CNT++;
		memcpy(INDATA->buffer,&BINBUF[CNTSIZE],254);

		((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->m_CtrFileProgress.SetPos((unsigned int)(((CNTSIZE-AddressCnt)*1000)/(EndAddressCnt-AddressCnt)));
		((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->m_CtrProgress.SetPos((unsigned int)(((CNTSIZE-AddressCnt)*1000)/(EndAddressCnt-AddressCnt)));

		if((B_FileChk==TRUE)&&(stat == 1))
		{//파일의 옵셋값이 존재할때만 적용된다.
			if((AddOfOffset[0]>= CNTSIZE)&&(AddOfOffset[0]<(CNTSIZE + 254)))
			{
				INDATA->buffer[AddOfOffset[0]%254] =  xoffdata + INDATA->m_offsetX;
			}

			if((AddOfOffset[1]>= CNTSIZE)&&(AddOfOffset[1]<(CNTSIZE + 254)))
			{
				INDATA->buffer[AddOfOffset[1]%254] =  yoffdata + INDATA->m_offsetY;
			}
			
			if((AddOfOffset[2]>= CNTSIZE)&&(AddOfOffset[2]<(CNTSIZE + 254)))
			{
				if(m_binmod == 0)
				{	//m_binmod = 1이면 메가이다.	//return = 2이면 메가이다. 
					INDATA->buffer[AddOfOffset[2]%254] =  m_DChkSum1; //둘다 +일때
				}
				else
				{ //메가가 아닌 일반일경우 
					mcnt = 0;
					
					if(INDATA->m_offsetX+xoffdata < 0)
					{
						mcnt++;
					}
					
					if(yoffdata + INDATA->m_offsetY < 0)
					{
						mcnt++;
					}

					INDATA->buffer[AddOfOffset[2]%254] =  m_DChkSum1 - mcnt; //둘다 -일때
				}
			}
			
			if((AddOfOffset[3]>= CNTSIZE)&&(AddOfOffset[3]<(CNTSIZE + 254)))
			{		
				INDATA->buffer[AddOfOffset[3]%254] =  m_DChkSum2 - (INDATA->m_offsetX) - (INDATA->m_offsetY);
			}
		}

		BYTE SESIZE = 0;
		BYTE SENAM = 0;
		int lopCnt = 0;
		
		if(CNTSIZE+254 <= EndAddressCnt)
		{
			SESIZE = 254;
		}
		else
		{
			SESIZE = (BYTE)(EndAddressCnt - CNTSIZE);
		}

		lopCnt = SESIZE/8;
		
		if(lopCnt == 31)
		{
			lopCnt+=1;
		}
		else
		{
			lopCnt+=2;
		}

		memset(&INDATA->buffer[SESIZE],0,256-SESIZE);

		for(int lop = 0;lop<lopCnt;lop++)
		{
			if(((CImageTesterDlg *)m_pMomWnd)->DL_DATASET(INDATA,lop) == FALSE)
			{
				if(((CImageTesterDlg *)m_pMomWnd)->DL_DATASET(INDATA,lop) == FALSE)
				{//실패시 한번 더 보낸다.
					INDATA->m_Enable = FALSE;
					INDATA->m_success = FALSE;
					INDATA->str_state = "다운로드실패";	
					((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(0);//다운로드 모드를 빠져나온다.
					((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->ProgressEn(0);

					//return FALSE;	
					return -1;	// 2016.09.01 수정: 심재원
				}
			}
		}

		if(((CImageTesterDlg *)m_pMomWnd)->DL_DATAWR(BL_CNT,SESIZE) == FALSE)
		{
			//if(((CImageTesterDlg *)m_pMomWnd)->DL_DATAWR(BL_CNT) == FALSE){
				INDATA->m_Enable = FALSE;
				INDATA->m_success = FALSE;
				INDATA->str_state = "다운로드실패";	
				((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(0);//다운로드 모드를 빠져나온다.
				((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->ProgressEn(0);

				//return FALSE;	
				return -1;	// 2016.09.01 수정: 심재원
		//	}
		}
	}
	
	if(((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(0) == FALSE)
	{
		if(((CImageTesterDlg *)m_pMomWnd)->DL_UPDATEMODE(0) == FALSE)
		{
				
		}
	}

	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->ProgressEn(0);

	return TRUE;
}

//=============================================================================
// Method		: CanInitUpdate
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/8/29 - 19:41
// Desc.		:
//=============================================================================
void CCENTER_DETECT_Option::CanInitUpdate()
{
	Initgen(&CenterVal);
	//MoveCenter(&CenterVal);
	CanOnSendSerial(&CenterVal,0);
}

void CCENTER_DETECT_Option::OpticalAsixModify(_EtcWrdata * INDATA)
{
	bool mloop = 0;
	Initgen(INDATA);
	//	나중에 중간에 정지 시킬수 잇는 스톱 버튼을 만든다.
	if(((CImageTesterDlg *)m_pMomWnd)->b_StopCommand == TRUE)
	{
		StatePrintf("강제 종료 되었습니다.");
		INDATA->m_Enable = FALSE;
		INDATA->m_success = FALSE;
		INDATA->str_state = "강제 종료";
		return;
	}
	
	MoveCenter(INDATA);//명령이 입력되면 카메라가 알아서 리셋되는가?
	
// 	DoEvents(1000);
// 	((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(0);
// 	DoEvents(500);

	if(!((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK())
	{
		INDATA->m_Enable = FALSE;
		INDATA->m_success = FALSE;
		INDATA->str_state = "CAMERA OFF";
		return;
	}

	//카메라 리셋을 추가한다.
	int lopcnt = 0;
	int rebuf = 0;
	BOOL DETECTEN = TRUE;
	while(lopcnt <= (CenterPrm.MaxCount))
	{//4개 포트가 모두 TRUE일때 계속해서 돈다. 
		Find_offset(INDATA);
		if(((CImageTesterDlg *)m_pMomWnd)->b_StopCommand == TRUE)
		{
			StatePrintf("강제 종료 되었습니다.");
			INDATA->m_Enable = FALSE;
			INDATA->m_success = FALSE;
			INDATA->str_state = "강제 종료";
			return;
		}
		
		if(INDATA->m_Enable == TRUE)
		{//아직 테스트를 계속진행해야 할경우
			StatePrintf("%d 번째 광축조정(중심점 검출)을 진행합니다",lopcnt+1);
			
			rebuf = OnSendSerial(INDATA);//펌웨어 다운로드 알고리즘을 넣는다. 

#ifdef APPLY_CAN_WRITE_PATCH
			// 2016.09.01 수정: 심재원 ----------------------------------------

			// 전송 오류인 경우 재시도 회수 제한만큼 재 전송
			for (int iRetry = 0; iRetry < CenterPrm.RetryCnt_Write; iRetry++)
			{
				if (-1 == rebuf)
				{
					rebuf = OnSendSerial(INDATA);
				}
				else
				{
					break;
				}
			}
			// End 2016.09.01 -------------------------------------------------
#endif


// 			DoEvents(1000);
// 			((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(0);
// 			DoEvents(500);

			if(!rebuf)
			{//write를 진행한다.
				if(((CImageTesterDlg *)m_pMomWnd)->b_StopCommand == TRUE)
				{
					if(INDATA->m_Enable == TRUE)
					{
						INDATA->m_Enable = FALSE;
						INDATA->m_success = FALSE;
						INDATA->str_state = "강제 종료";
					}
					return;
				}
				else
				{
					if(lopcnt == (CenterPrm.MaxCount))
					{
						StatePrintf("광축값 적용을 실패하였습니다.");
						if(INDATA->m_Enable == TRUE)
						{
							INDATA->m_Enable = FALSE;
							INDATA->m_success = FALSE;
							INDATA->str_state = "OFFSET WRITE 실패";
						}
						return;
					}
					else
					{
						StatePrintf("광축값 적용을 실패하였습니다. 다시 시도합니다.");
					}
				}
				
			}
	//		DoEvents(1000);
//			((CImageTesterDlg *)m_pMomWnd)->Camera_Reset();
		}
		else
		{//테스트가 모두 끝났을 경우
			lopcnt = 100;
		}
		lopcnt++;
	}
}

//BOOL CCENTER_DETECT_Option::ChkTstEnd(_TOTWrdata * INDATA)
//{
//	for(int lop = 0;lop<4;lop++){
//		if(INDATA->t_EtcCh[lop].m_Enable == TRUE){
//			return TRUE;
//		}
//	}
//	return FALSE;
//}

BOOL CCENTER_DETECT_Option::ChkTstEnd(_EtcWrdata * INDATA)
{
	if(INDATA->m_Enable == TRUE){
		return TRUE;
	}
	return FALSE;
}


void CCENTER_DETECT_Option::Center_Pic(CDC* cdc)
{
	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	if((EtcPt[0].x <0)||(EtcPt[0].y <0)){
		return;
	}
	::SetBkMode(cdc->m_hDC,TRANSPARENT);

	//******************************************************************************
	CPen my_Pan, my_Pan2, *old_pan, *old_pan2;
//***********************초기 원점 탐색 영역******************************//
	my_Pan.CreatePen(PS_SOLID,3,RGB(255, 255, 255));
	old_pan = cdc->SelectObject(&my_Pan);
	cdc->MoveTo((Cam_PosX*0.60), (Cam_PosY*0.600));
	cdc->LineTo((Cam_PosX*1.40),(Cam_PosY*0.600));
	cdc->LineTo((Cam_PosX*1.40),(Cam_PosY*1.400));
	cdc->LineTo((Cam_PosX*0.60),(Cam_PosY*1.400));
	cdc->LineTo((Cam_PosX*0.60),(Cam_PosY*0.600));

	cdc->SelectObject(old_pan);
	old_pan->DeleteObject();
	my_Pan.DeleteObject();
	
	
	int x1 = EtcPt[0].x;
	int y1 = EtcPt[0].y;

	int x= CenterVal.m_F_X_OFFSET;//-
	int y= CenterVal.m_F_Y_OFFSET;//-
	CString TEXTDATA ="";

	CFont font, *old_font;
	font.CreatePointFont(220, "Arial"); 
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);

	my_Pan2.CreatePen(PS_SOLID,3,RGB(255, 255, 0));
	old_pan2 = cdc->SelectObject(&my_Pan2);
	cdc->MoveTo(x1-10, y1);
	cdc->LineTo(x1+10, y1);
	cdc->MoveTo(x1, y1-10);
	cdc->LineTo(x1, y1+10);

	cdc->SelectObject(old_pan2);
	old_pan2->DeleteObject();
	my_Pan2.DeleteObject();

	old_font = cdc->SelectObject(&font);
	
	if((x1 == 0)&&(y1 == 0)){
		TEXTDATA.Format("중심점을 찾지 못하였습니다.");
		cdc->SetTextColor(RGB(255, 0, 0));
	}else{
		TEXTDATA.Format("OFFSET X = %d   OFFSET Y = %d", x, y);
		if(b_TESTResult == TRUE){
			cdc->SetTextColor(RGB(0, 0, 255));
		}else{
			cdc->SetTextColor(RGB(255, 0, 0));
		}
	}

	cdc->TextOut(Cam_PosX , (Cam_PosY+200),TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	cdc->SelectObject(old_font);
	old_font->DeleteObject();
	font.DeleteObject();
//*********************************************************************

}

void CCENTER_DETECT_Option::Find_offset(_EtcWrdata * INDATA)
{
	INDATA->m_number = 0;
	double centx,centy;
	//((CImageTesterDlg *)m_pMomWnd)->REG_STANDARD_SETUP();

	if(!((CImageTesterDlg *)m_pMomWnd)->CAMERA_STAT_CHK()){
		INDATA->m_Enable = FALSE;
		INDATA->m_success = FALSE;
		INDATA->str_state = "CAMERA OFF";
		return;
	}

	if(INDATA->m_Enable == TRUE){//동작이 허가되었을때만 시도한다.			
		//여기에 광축 측정 알고리즘을 넣는
		BOOL	bstate = 1;
		int		errcnt = 0;
		while(bstate){
			memset(m_RGBScanbuf,0,sizeof(m_RGBScanbuf));
			((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = TRUE;
			
			for(int i =0;i<10;i++){
				if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == FALSE){
					break;
				}else{
					DoEvents(50);
				}
			}

			EtcPt[0] = GetCenterPoint(m_RGBScanbuf);
			if ((EtcPt[0].x <= CAM_IMAGE_WIDTH) && (EtcPt[0].x >= 0) && (EtcPt[0].y <= CAM_IMAGE_HEIGHT) && (EtcPt[0].y >= 0)){
				bstate = 0;
			}else{
				errcnt++;
				if(errcnt >4){
					bstate = 0;
				}
			}
		}
		
		centx = EtcPt[0].x;
		centy = EtcPt[0].y;

		m_dCurrCenterCoord.x = centx;
		m_dCurrCenterCoord.y =  centy;
		
		centergen(centx,centy,INDATA);
		CenterVal.str_state.Format("X- %3.1f ,Y- %3.1f",centx,centy);
		if	((centx > CenterPrm.StandX + 200)||
			(centx < CenterPrm.StandX - 200)||
			(centy > CenterPrm.StandY + 200)||
			(centy < CenterPrm.StandY - 200)){
			if(((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd != NULL){
				((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->OffsetX_Txt(0,"");
				((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->OffsetY_Txt(0,"");
				((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->AxisX_Txt(0,"측정실패");
				((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->AxisY_Txt(0,"측정실패");
				EtcPt[0].x = 0;
				EtcPt[0].y = 0;
				INDATA->str_state = "광축측정실패";
				INDATA->m_success = FALSE;
				INDATA->m_Enable = FALSE;
			}
		}	
	}
}

//---------------------------------------------------------------------WORKLIST
void CCENTER_DETECT_Option::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	InsertIndex = m_CenterWorklist.InsertItem(StartCnt,"",0);
	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_CenterWorklist.SetItemText(InsertIndex,0,strCnt);

	m_CenterWorklist.SetItemText(InsertIndex,1,((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_CenterWorklist.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_CenterWorklist.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");

	CString str ="";
	if(CenterPrm.WriteMode == 0)
	{
		str = "좌우반전";
	}
	if(CenterPrm.WriteMode == 1)
	{
		str = "상하반전";
	}
	if(CenterPrm.WriteMode == 2)
	{
		str ="오리지날";	
	}
	if(CenterPrm.WriteMode == 3)
	{
		str ="로테이트";	
	}

	m_CenterWorklist.SetItemText(InsertIndex,4,str);
	
}
void CCENTER_DETECT_Option::FAIL_UPLOAD()
{
//	if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1)
//	{
			InsertList();

			for (int t=0; t<5; t++)
			{
				m_CenterWorklist.SetItemText(InsertIndex,t+5,"X");

			}
			m_CenterWorklist.SetItemText(InsertIndex,9,"FAIL");
			m_CenterWorklist.SetItemText(InsertIndex,10,"STOP");
			
			Str_Mes[0][0] = "000.0";
			Str_Mes[1][0] = "000.0";
			Str_Mes[0][1] = "00";
			Str_Mes[1][1] = "00";

			((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex,WORKLIST_CENTER,"STOP");
			StartCnt++;
			b_StopFail = TRUE;
//	}
}


void CCENTER_DETECT_Option::Set_List(CListCtrl *List){
	
	int count=0;
	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"카메라상태",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"FINAL X",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"FINAL Y",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"Offset X",LVCFMT_CENTER, 80);
	List->InsertColumn(8,"Offset Y",LVCFMT_CENTER, 80);
	List->InsertColumn(9,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(10,"비고",LVCFMT_CENTER, 80);

	ListItemNum = 11;
	Copy_List(&m_CenterWorklist,((CImageTesterDlg *)m_pMomWnd)->m_pWorkList,ListItemNum);
}

CString CCENTER_DETECT_Option::Mes_Result()
{
	CString sz[8];
	int		nResult[8];

	//int nSel = m_CenterWorklist.GetItemCount()-1;

	//// 초기측정 X좌표
	//sz[0] = m_CenterWorklist.GetItemText(nSel, 5);

	//// 초기측정 Y좌표
	//sz[1] = m_CenterWorklist.GetItemText(nSel, 6);

	//// 초기 측정 offset X
	//sz[4] = m_CenterWorklist.GetItemText(nSel, 9);

	//// 초기 측정 offset Y
	//sz[5] = m_CenterWorklist.GetItemText(nSel, 10);

	//if(m_RunMode == 0)				// 광축 조정모드
	//{
	//	// 마지막 측정 X좌표
	//	sz[2] = m_CenterWorklist.GetItemText(nSel, 7);

	//	// 마지막 측정 Y좌표
	//	sz[3] = m_CenterWorklist.GetItemText(nSel, 8);

	//	// 마지막 측정 offset X
	//	sz[6] = m_CenterWorklist.GetItemText(nSel, 11);

	//	// 마지막 측정 offset Y
	//	sz[7] = m_CenterWorklist.GetItemText(nSel, 12);

	//}else if(m_RunMode == 1)		// 광축 측정모드
	//{
	//	// 마지막 측정 X좌표
	//	sz[2] = m_CenterWorklist.GetItemText(nSel, 5);

	//	// 마지막 측정 Y좌표
	//	sz[3] = m_CenterWorklist.GetItemText(nSel, 6);

	//	// 마지막 측정 offset X
	//	sz[6] = m_CenterWorklist.GetItemText(nSel, 9);

	//	// 마지막 측정 offset Y
	//	sz[7] = m_CenterWorklist.GetItemText(nSel, 10);
	//}

	//for(int _a=0; _a<8; _a++)
	//{
	//	if(CenterVal.m_success)
	//		nResult[_a] = 1;
	//	else
	//		nResult[_a] = 0;
	//}	

	//m_szMesResult.Format(_T("%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d,%s:%d"), sz[0], nResult[0], sz[1], nResult[1], sz[2], nResult[2], sz[3], nResult[3], 
	//	sz[4], nResult[4], sz[5], nResult[5], sz[6], nResult[6], sz[7], nResult[7]);

	m_szMesResult = _T("");
	if(b_StopFail == FALSE)
	{
		m_szMesResult.Format(_T("%s:%s,%s:%s"),Str_Mes[0][0],Str_Mes[0][1],Str_Mes[1][0],Str_Mes[1][1]);
	}else
	{
		m_szMesResult.Format(_T(":1,:1"));
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_stNewMesData[NewMES_CenterPoint] = m_szMesResult;

	return m_szMesResult;
}

CString CCENTER_DETECT_Option::Mes_Result_InitData()
{
	CString sz[8];
	int		nResult[8];

	m_szMesResult.Empty();

	int nSel = m_CenterWorklist.GetItemCount()-1;

	sz[0] = m_CenterWorklist.GetItemText(nSel, 5);

	// 초기측정 Y좌표
	sz[1] = m_CenterWorklist.GetItemText(nSel, 6);


	for(int _a=0; _a<2; _a++)
	{
		if(CenterVal.m_success)
			nResult[_a] = 1;
		else
			nResult[_a] = 0;
	}	

	


	if(b_StopFail == FALSE){
		m_szMesResult.Format(_T("%s:%d,%s:%d"),sz[0],nResult[0],sz[1],nResult[1]);
	}else{
		m_szMesResult.Format(_T(":1,:1"));
	}
	return m_szMesResult;
}


void CCENTER_DETECT_Option::EXCEL_SAVE(){
	CString Item[5]={"카메라상태","FINAL X","FINAL Y","Offset X","Offset Y"};//수정해야할 곳 0203

	CString Data ="";
	CString str="";
	str = "***********************광축 검사***********************\n";
	Data += str;
	
	str.Empty();
	str.Format("기준광축 X, %d,,,기준광축 Y, %d\n ",CenterPrm.StandX,CenterPrm.StandY);
	Data += str;
	str.Empty();
	str.Format("합격편차 X, %3.1f,,,합격편차 Y, %3.1f\n\n ",CenterPrm.PassMaxDevX,CenterPrm.PassMaxDevY);
	Data += str;
	
	str.Empty();
	str = "*************************************************\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("보임량광축",Item,5,&m_CenterWorklist,Data);
}

bool CCENTER_DETECT_Option::EXCEL_UPLOAD(){
	m_CenterWorklist.DeleteAllItems();
	
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("보임량광축",&m_CenterWorklist,1,&StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		StartCnt = 0;
		return FALSE;
	}
	return TRUE;
	
}

void CCENTER_DETECT_Option::Setup_List(){
	
	/*CString strCnt ="";
	for(int NUM=0; NUM<4; NUM++){
		if(((CImageTesterDlg *)m_pMomWnd)->TestData[0][NUM].m_Enable == 1){
			int count = NUM + ((CImageTesterDlg *)m_pMomWnd)->Totalint;
			
			InsertIndex = m_CenterWorklist.InsertItem(count,"",0);
			strCnt.Empty();
			strCnt.Format(_T("%d"), InsertIndex+1);
			m_CenterWorklist.SetItemText(InsertIndex,0,strCnt);
			
			for(int t=0; t<15; t++){
				m_CenterWorklist.SetItemText(InsertIndex,t+1,TESTLOT[NUM][t+1]);
			}			
		}
	}*/
}

void CCENTER_DETECT_Option::OnCbnSelchangeComboIntwropt()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CenterPrm.INITWRSEL = pINTWROPTCombo->GetCurSel();
	CString str="";
	str.Format("%d",CenterPrm.INITWRSEL);
	WritePrivateProfileString(str_model,"initWrite",str,str_ModelPath);
}
BOOL CCENTER_DETECT_Option::CheckSettingDetect_1()//모두 같으면 TRUE 틀리면 FALSE
{
	/*
	int buf = 0;
	buf = pINTWROPTCombo->GetCurSel();
	if(buf != CenterPrm.INITWRSEL){
		return FALSE; 
	}
	buf = pWROPTCombo->GetCurSel();
	if(buf != CenterPrm.MIDWRSEL){
		return FALSE;
	}

	buf = ((CComboBox *)GetDlgItem(IDC_OPT_WRMODE))->GetCurSel();
	if(buf != CenterPrm.WriteMode){
		return FALSE;
	}
	
	buf = GetDlgItemInt(IDC_MAXCONTROL_NUMBER);
	if(buf != CenterPrm.MaxCount){
		return FALSE;
	}

	buf = GetDlgItemInt(IDC_RATE_X);
	if(buf != CenterPrm.MaxCount){
		return FALSE;
	}
	*/


	return TRUE;//모두 같으면 PASS
}

void CCENTER_DETECT_Option::OnCbnSelchangeComboWropt2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CenterPrm.MIDWRSEL = pWROPTCombo->GetCurSel();
	CString str="";
	str.Format("%d",CenterPrm.MIDWRSEL);
	WritePrivateProfileString(str_model,"SectorWrite",str,str_ModelPath);
}

void CCENTER_DETECT_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if(bShow == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;

	}
}

double CCENTER_DETECT_Option::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt( (double)((x2-x1)*(x2-x1)) +  ((y2-y1)*(y2-y1)));

	return result;
}
BOOL CCENTER_DETECT_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
				return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CCENTER_DETECT_Option::OnBnClickedButton2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CPK_DATA_SUM();
}

void CCENTER_DETECT_Option::OnBnClickedButtonMicom()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//((CImageTesterDlg *)m_pMomWnd)->MicomCenter_Popup();
	((CImageTesterDlg *)m_pMomWnd)->Power_On();
	DoEvents(500);
	
	if(CenterPrm.HWMODE == 1){
		StatePrintf("오프셋 기본값으로 셋팅합니다.");
		Initgen(&CenterVal);
		CanDLOffsetWr(&CenterVal,0);
	}else{
		Initgen(&CenterVal);
		OnSendSerial(&CenterVal);
	}
}

void CCENTER_DETECT_Option::OnBnClickedButtontest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(0);
	
	if(!((CImageTesterDlg *)m_pMomWnd)->JIG_MOVE()){
		
	}

 	((CImageTesterDlg *)m_pMomWnd)->Power_On();
// 	DoEvents(500);
// 	((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(0);
// 	DoEvents(500);	

	//((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	Initgen(&CenterVal);
	Find_offset(&CenterVal);

// 	DoEvents(500);
// 	((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(1);
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(1);
	//
}

#pragma region LOT관련 함수
void CCENTER_DETECT_Option::LOT_Set_List(CListCtrl *List){

	while(List->DeleteColumn(0));
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT 명",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(5, "카메라상태", LVCFMT_CENTER, 80);
	List->InsertColumn(6, "FINAL X", LVCFMT_CENTER, 80);
	List->InsertColumn(7, "FINAL Y", LVCFMT_CENTER, 80);
	List->InsertColumn(8, "Offset X", LVCFMT_CENTER, 80);
	List->InsertColumn(9, "Offset Y", LVCFMT_CENTER, 80);
	List->InsertColumn(10, "RESULT", LVCFMT_CENTER, 80);
	List->InsertColumn(11, "비고", LVCFMT_CENTER, 80);
	Lot_InsertNum =12;
	
}

void CCENTER_DETECT_Option::LOT_InsertDataList(){
	CString strCnt="";

	int Index = m_Lot_CenterList.InsertItem(Lot_StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index+1);
	m_Lot_CenterList.SetItemText(Index,0,strCnt);

	int CopyIndex = m_CenterWorklist.GetItemCount()-1;

	int count =0;
	for(int t=0; t< Lot_InsertNum;t++){
		if((t != 2)&&(t!=0)){
			m_Lot_CenterList.SetItemText(Index,t, m_CenterWorklist.GetItemText(CopyIndex,count));
			count++;

		}else if(t==0){
			count++;
		}else{
			m_Lot_CenterList.SetItemText(Index,t,((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;

}
void CCENTER_DETECT_Option::LOT_EXCEL_SAVE(){
	CString Item[5]={"카메라상태","FINAL X","FINAL Y","Offset X","Offset Y"};//수정해야할 곳 0203

	CString Data ="";
	CString str="";
	str = "***********************광축 검사***********************\n";
	Data += str;
	
	str.Empty();
	str.Format("기준광축 X, %d,,,기준광축 Y, %d\n ",CenterPrm.StandX,CenterPrm.StandY);
	Data += str;
	str.Empty();
	str.Format("합격편차 X, %3.1f,,,합격편차 Y, %3.1f\n\n ",CenterPrm.PassDeviatX,CenterPrm.PassDeviatY);
	Data += str;

	str.Empty();
	str = "*************************************************\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("보임량광축",Item,5,&m_Lot_CenterList,Data);
}

bool CCENTER_DETECT_Option::LOT_EXCEL_UPLOAD(){
	m_Lot_CenterList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("보임량광축",&m_Lot_CenterList,1,&Lot_StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}

#pragma endregion 

void CCENTER_DETECT_Option::OnCbnSelchangeComobWrmode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_iSelWrMode = pWrModeCombo->GetCurSel();
	CenterPrm.HWMODE = m_iSelWrMode;

	CString str = "";
	str.Format("%d",CenterPrm.HWMODE);
	WritePrivateProfileString(str_model,"HWMODE",str,str_ModelPath);
	SetOptMode(CenterPrm.HWMODE);
	SetFileSel();
}

void CCENTER_DETECT_Option::SetOptMode(int num)
{

	if(m_RunMode == 0){
		if(num == 1){
			pINTWROPTCombo->ShowWindow(SW_HIDE);
			pWROPTCombo->ShowWindow(SW_HIDE);
			((CButton *)GetDlgItem(IDC_BTN_BINADD))->ShowWindow(SW_HIDE);
			((CEdit *)GetDlgItem(IDC_EDIT_BINFILENAME))->ShowWindow(SW_HIDE);
			((CStatic *)GetDlgItem(IDC_STATIC_BINNAME))->ShowWindow(SW_HIDE);
			((CStatic *)GetDlgItem(IDC_STATIC_INITWRNAME))->ShowWindow(SW_HIDE);
			((CStatic *)GetDlgItem(IDC_STATIC_MIDWRNAME))->ShowWindow(SW_HIDE);
		}else{
			pINTWROPTCombo->ShowWindow(SW_SHOW);
			pWROPTCombo->ShowWindow(SW_SHOW);
			((CButton *)GetDlgItem(IDC_BTN_BINADD))->ShowWindow(SW_SHOW);
			((CEdit *)GetDlgItem(IDC_EDIT_BINFILENAME))->ShowWindow(SW_SHOW);
			((CStatic *)GetDlgItem(IDC_STATIC_BINNAME))->ShowWindow(SW_SHOW);
			((CStatic *)GetDlgItem(IDC_STATIC_INITWRNAME))->ShowWindow(SW_SHOW);
			((CStatic *)GetDlgItem(IDC_STATIC_MIDWRNAME))->ShowWindow(SW_SHOW);
		}
		pWrModeCombo->ShowWindow(SW_SHOW);
		((CEdit *)GetDlgItem(IDC_PASS_MAXDEVX))->ShowWindow(SW_SHOW);
		((CEdit *)GetDlgItem(IDC_PASS_MAXDEVY))->ShowWindow(SW_SHOW);
		((CEdit *)GetDlgItem(IDC_RATE_X))->ShowWindow(SW_SHOW);
		((CEdit *)GetDlgItem(IDC_RATE_Y))->ShowWindow(SW_SHOW);
		((CEdit *)GetDlgItem(IDC_XENPIXEL))->ShowWindow(SW_SHOW);
		((CEdit *)GetDlgItem(IDC_YENPIXEL))->ShowWindow(SW_SHOW);
		((CEdit *)GetDlgItem(IDC_MAXCONTROL_NUMBER))->ShowWindow(SW_SHOW);
		((CButton *)GetDlgItem(IDC_OPTICALSTAT_SAVE))->ShowWindow(SW_SHOW);	
		((CButton *)GetDlgItem(IDC_BUTTON_MICOM))->ShowWindow(SW_SHOW);
		((CStatic *)GetDlgItem(IDC_STATIC_0))->ShowWindow(SW_SHOW);
		((CStatic *)GetDlgItem(IDC_STATIC_1))->ShowWindow(SW_SHOW);
		((CStatic *)GetDlgItem(IDC_STATIC_2))->ShowWindow(SW_SHOW);
		((CStatic *)GetDlgItem(IDC_STATIC_3))->ShowWindow(SW_SHOW);
		((CStatic *)GetDlgItem(IDC_STATIC_4))->ShowWindow(SW_SHOW);
		((CStatic *)GetDlgItem(IDC_STATIC_5))->ShowWindow(SW_SHOW);
		((CStatic *)GetDlgItem(IDC_STATIC_6))->ShowWindow(SW_SHOW);
		((CStatic *)GetDlgItem(IDC_STATIC_7))->ShowWindow(SW_SHOW);
		((CStatic *)GetDlgItem(IDC_STATIC_8))->ShowWindow(SW_SHOW);
		((CStatic *)GetDlgItem(IDC_ST_OPT_CEN_RETRY))->ShowWindow(SW_SHOW);
		((CStatic *)GetDlgItem(IDC_CB_OPT_CEN_RETRY))->ShowWindow(SW_SHOW);
	}else{
		pWrModeCombo->ShowWindow(SW_HIDE);
		pINTWROPTCombo->ShowWindow(SW_HIDE);
		pWROPTCombo->ShowWindow(SW_HIDE);
		((CButton *)GetDlgItem(IDC_BTN_BINADD))->ShowWindow(SW_HIDE);
		((CButton *)GetDlgItem(IDC_BUTTON_MICOM))->ShowWindow(SW_HIDE);
		
		((CEdit *)GetDlgItem(IDC_EDIT_BINFILENAME))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_STATIC_BINNAME))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_STATIC_INITWRNAME))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_STATIC_MIDWRNAME))->ShowWindow(SW_HIDE);
		
		((CEdit *)GetDlgItem(IDC_PASS_MAXDEVX))->ShowWindow(SW_HIDE);
		((CEdit *)GetDlgItem(IDC_PASS_MAXDEVY))->ShowWindow(SW_HIDE);
		((CEdit *)GetDlgItem(IDC_RATE_X))->ShowWindow(SW_HIDE);
		((CEdit *)GetDlgItem(IDC_RATE_Y))->ShowWindow(SW_HIDE);
		((CEdit *)GetDlgItem(IDC_XENPIXEL))->ShowWindow(SW_HIDE);
		((CEdit *)GetDlgItem(IDC_YENPIXEL))->ShowWindow(SW_HIDE);
		((CEdit *)GetDlgItem(IDC_MAXCONTROL_NUMBER))->ShowWindow(SW_HIDE);
		((CButton *)GetDlgItem(IDC_OPTICALSTAT_SAVE))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_STATIC_0))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_STATIC_1))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_STATIC_2))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_STATIC_3))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_STATIC_4))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_STATIC_5))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_STATIC_6))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_STATIC_7))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_STATIC_8))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_ST_OPT_CEN_RETRY))->ShowWindow(SW_HIDE);
		((CStatic *)GetDlgItem(IDC_CB_OPT_CEN_RETRY))->ShowWindow(SW_HIDE);
	}

	if(((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd != NULL){
		if(m_RunMode == 0){
			((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->SetOptMode(num);
		}else{
			((CImageTesterDlg *)m_pMomWnd)->m_pResCPointOptWnd->SetOptMode(1);
		}
	}
}

void CCENTER_DETECT_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CComboBox *)GetDlgItem(IDC_COMBO_MODE))->EnableWindow(0);
		((CComboBox *)GetDlgItem(IDC_OPT_WRMODE))->EnableWindow(0);
		pWrModeCombo->EnableWindow(0);
		pINTWROPTCombo->EnableWindow(0);
		pWROPTCombo->EnableWindow(0);
		

		((CEdit *)GetDlgItem(IDC_RATE_X))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_RATE_Y))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_STAND_OFFSETX))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_STAND_OFFSETY))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_PASS_DEVIATX))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_PASS_DEVIATY))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_PASS_MAXDEVX))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_PASS_MAXDEVY))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_MAXCONTROL_NUMBER))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_XENPIXEL))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_YENPIXEL))->EnableWindow(0);
		
		((CEdit *)GetDlgItem(IDC_OPTICALSTAT_SAVE))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_OPTICALSTAT_SAVE2))->EnableWindow(0);

		((CEdit *)GetDlgItem(IDC_BTN_BINADD))->EnableWindow(0);

	}
}

void CCENTER_DETECT_Option::OnCbnSelchangeOptWrmode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

//=============================================================================
// Method		: OnCbnSelchangeCbOptCenRetry
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/9/2 - 13:28
// Desc.		: 수정 : 심재원
//=============================================================================
void CCENTER_DETECT_Option::OnCbnSelchangeCbOptCenRetry()
{
	CenterPrm.RetryCnt_Write = m_cb_RetryCnt.GetCurSel();
		
	CString szValue;
	szValue.Format("%d",CenterPrm.RetryCnt_Write);
	WritePrivateProfileString(str_model, "SectorWrite_RetryCnt", szValue, str_ModelPath);
}

void CCENTER_DETECT_Option::OnBnClickedCheckCan()
{
    // TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
    if (b_CanOverlayCheck == TRUE)
    {
        b_CanOverlayCheck = FALSE;
    }else{
        b_CanOverlayCheck = TRUE;
    }
    UpdateData(FALSE);
    
    CString szValue;
    szValue.Format("%d",b_CanOverlayCheck);
    WritePrivateProfileString(str_model, "CANOVERLAYCHECK", szValue, str_ModelPath);
}


BOOL CCENTER_DETECT_Option::CheckOnOverlay(LPBYTE IN_RGB)
{
    IplImage *GrayImage = cvCreateImage(cvSize( m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);

    cvSetZero(GrayImage);

    for (int y = 0; y < m_CAM_SIZE_HEIGHT; y++)
    {
        for (int x = 0; x < m_CAM_SIZE_WIDTH; x++)
        {
            BYTE R, G, B;
            B = IN_RGB[y * (m_CAM_SIZE_WIDTH * 4) + x * 4 + 0];
            G = IN_RGB[y * (m_CAM_SIZE_WIDTH * 4) + x * 4 + 1];
            R = IN_RGB[y * (m_CAM_SIZE_WIDTH * 4) + x * 4 + 2];

            if ((R - G) > 180)
                GrayImage->imageData[y * GrayImage->widthStep + x] = 255;
            else
                GrayImage->imageData[y * GrayImage->widthStep + x] = 0;
        }
    }

    //         cvShowImage("결과", GrayImage);

    CvMemStorage* contour_storage = cvCreateMemStorage(0);
    CvSeq *contour = 0;

    cvFindContours(GrayImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

    CvRect rect;

    int Cnt = 0;

    for (; contour != 0; contour = contour->h_next)
    {
        rect = cvContourBoundingRect(contour, 1);

        if (rect.width > 360)
            Cnt++;
    }

    cvReleaseMemStorage(&contour_storage);
    cvReleaseImage(&GrayImage);
    
    if (Cnt > 0)
        return FALSE;
    else
        return TRUE;
}


BOOL CCENTER_DETECT_Option::OverlayCheck_TEST(){

//    if(!((CImageTesterDlg *)m_pMomWnd)->Overlay_Enable(0))
//     {
//        return FALSE;
//     }
//      DoEvents(500);
    
    memset(m_RGBScanbuf,0,sizeof(m_RGBScanbuf));
    ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = TRUE;

    for(int i =0;i<10;i++){
        if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == FALSE){
            break;
        }else{
            DoEvents(50);
        }
    }

    BOOL bResult = CheckOnOverlay(m_RGBScanbuf);
    
    
    return bResult;
    
}
void CCENTER_DETECT_Option::OnBnClickedButtonOverlaytest()
{
    // TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
    
    ((CEdit *)GetDlgItem(IDC_EDIT_CANRESULT))->SetWindowText("");
    if (!OverlayCheck_TEST())
    {
        ((CEdit *)GetDlgItem(IDC_EDIT_CANRESULT))->SetWindowText("FAIL");
    }else{
        ((CEdit *)GetDlgItem(IDC_EDIT_CANRESULT))->SetWindowText("PASS");

    }
}
