// UserSwitching.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "UserSwitching.h"


// CUserSwitching 대화 상자입니다.

IMPLEMENT_DYNAMIC(CUserSwitching, CDialog)

CUserSwitching::CUserSwitching(CWnd* pParent /*=NULL*/)
	: CDialog(CUserSwitching::IDD, pParent)
	, m_PassWord(_T(""))
{

}

CUserSwitching::~CUserSwitching()
{
}

void CUserSwitching::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_Password, m_PassWord);
}

// CUserSwitching 메시지 처리기입니다.

BEGIN_MESSAGE_MAP(CUserSwitching, CDialog)
	ON_BN_CLICKED(IDOK, &CUserSwitching::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CUserSwitching::OnBnClickedCancel)
END_MESSAGE_MAP()


// CUserSwitching 메시지 처리기입니다.
void CUserSwitching::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

BOOL CUserSwitching::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CUserSwitching::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		/*if(pMsg->wParam == VK_RETURN){
			return TRUE;
		}*/
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}
		
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CUserSwitching::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	if (m_PassWord == ((CImageTesterDlg *)m_pMomWnd)->MatchPassWord || m_PassWord == _T("luritech") || m_PassWord == _T("LURITECH") || m_PassWord == _T("luritechinit") || m_PassWord == _T("LURITECHINIT") || m_PassWord == _T("lginnotek"))
	{
		if((m_PassWord ==_T("luritechinit"))||(m_PassWord ==_T("LURITECHINIT")))
		{
			((CImageTesterDlg *)m_pMomWnd)->b_SecretOption=1;
		}
		else if (m_PassWord == _T("lginnotek"))
		{
			((CImageTesterDlg *)m_pMomWnd)->b_SecretLgOption = TRUE;
		}
		else
		{
			((CImageTesterDlg *)m_pMomWnd)->b_SecretLgOption = FALSE;
			((CImageTesterDlg *)m_pMomWnd)->b_SecretOption = 0;
		}
		OnOK();
		m_PassWord =_T("");
		UpdateData(FALSE);
		return;
	}
	else
	{
		AfxMessageBox(_T("비밀번호가 일치하지 않습니다."));

		((CImageTesterDlg *)m_pMomWnd)->b_SecretOption =0;

		OnCancel();
		m_PassWord =_T("");
		UpdateData(FALSE);
		return;
	}
}

void CUserSwitching::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}
