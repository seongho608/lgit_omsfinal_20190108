#pragma once


// CModelCopyDlg 대화 상자입니다.

class CModelCopyDlg : public CDialog
{
	DECLARE_DYNAMIC(CModelCopyDlg)

public:
	CModelCopyDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CModelCopyDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_NEWMODEL };

	CString str_copyname;
	void	Setup(CWnd* IN_pMomWnd);

private:
	CWnd		*m_pMomWnd;	

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();

	CString m_str_cpyname;
	afx_msg void OnEnSetfocusEditModel2();
};
