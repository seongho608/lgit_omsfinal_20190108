// SFR_ResultView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "SFR_ResultView.h"


// CSFR_ResultView 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSFR_ResultView, CDialog)

CSFR_ResultView::CSFR_ResultView(CWnd* pParent /*=NULL*/)
	: CDialog(CSFR_ResultView::IDD, pParent)
{

}

CSFR_ResultView::~CSFR_ResultView()
{
}

void CSFR_ResultView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CENTER, m_CenterList);
	DDX_Control(pDX, IDC_LIST_SIDE, m_SideList);
}


BEGIN_MESSAGE_MAP(CSFR_ResultView, CDialog)
END_MESSAGE_MAP()


// CSFR_ResultView 메시지 처리기입니다.
void CSFR_ResultView::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}
void CSFR_ResultView::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;

	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);

	GetDlgItem(nID)->SetFont(font);
}
BOOL CSFR_ResultView::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_CenterList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_CenterList.InsertColumn(0,"LOCAL",LVCFMT_CENTER, 50);
	m_CenterList.InsertColumn(1,"결과값",LVCFMT_CENTER, 100);
	m_CenterList.InsertColumn(2,"RESULT",LVCFMT_CENTER, 60);

	m_SideList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_SideList.InsertColumn(0,"LOCAL",LVCFMT_CENTER, 50);
	m_SideList.InsertColumn(1,"결과값",LVCFMT_CENTER, 100);
	m_SideList.InsertColumn(2,"RESULT",LVCFMT_CENTER, 60);

	Initstat();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void CSFR_ResultView::Initstat(){
	m_CenterList.DeleteAllItems();
	m_SideList.DeleteAllItems();
}
BOOL CSFR_ResultView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}
