#pragma once
#include "afxcmn.h"
#include "ETCUSER.H"
#include <vector>
// CPatternNoise_Option 대화 상자입니다.
typedef struct _tag_PNMaster
{

	CString str_MasterPath;

	int nMaster_Block_X;
	int nMaster_Block_Y;

	int nMaster_Detect_X;
	int nMaster_Detect_Y;

	std::vector<POINT> pt_StartPointList;
	std::vector<POINT> pt_StartPoint_PixPosList;

	std::vector<int> v_iSignal;
	std::vector<double> v_dNoise;
	std::vector<double> v_dResult;

	POINT pt_Min;
	double dMinResult;
	BOOL bResult;

	_tag_PNMaster()
	{
	};

	void Reset()
	{
		pt_StartPoint_PixPosList.erase(pt_StartPoint_PixPosList.begin(), pt_StartPoint_PixPosList.end());
		pt_StartPointList.erase(pt_StartPointList.begin(), pt_StartPointList.end());
		v_dResult.erase(v_dResult.begin(), v_dResult.end());
		v_dNoise.erase(v_dNoise.begin(), v_dNoise.end());
		v_iSignal.erase(v_iSignal.begin(), v_iSignal.end());

		nMaster_Block_X = 0;
		nMaster_Block_Y = 0;
		nMaster_Detect_X = 0;
		nMaster_Detect_Y = 0;

		pt_Min.x = 0;
		pt_Min.y = 0;
		dMinResult = -1;
		bResult = 0;
	};

	/*변수 교환 함수*/
	_tag_PNMaster& operator= (_tag_PNMaster& ref)
	{
		str_MasterPath = ref.str_MasterPath;
		nMaster_Block_X = ref.nMaster_Block_X;
		nMaster_Block_Y = ref.nMaster_Block_Y;

		nMaster_Detect_X = ref.nMaster_Detect_X;
		nMaster_Detect_Y = ref.nMaster_Detect_Y;
		
		return *this;
	};

}ST_PNMaster, *PST_PNMaster;

class CPatternNoise_Option : public CDialog
{
	DECLARE_DYNAMIC(CPatternNoise_Option)

public:
	CPatternNoise_Option(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPatternNoise_Option();
    
	ST_PNMaster st_PNMaster;

	int m_CAM_SIZE_WIDTH;
	int m_CAM_SIZE_HEIGHT;
	int StartCnt;
	bool    m_Success;
	tResultVal Run();
	void Pic(CDC *cdc);
	void InitPrm();
	void	Setup(CWnd* IN_pMomWnd);
	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
	void	StatePrintf(LPCSTR lpcszString, ...);
	bool	PatternNoisePic(CDC *cdc,int NUM);
	double	m_NoiseValue;
	bool	PatternNoiseGen(LPBYTE IN_RGB,int NUM);
	bool	PatternNoiseGen_12bit(LPWORD IN_RGB, int NUM);
	void	PatternNoiseGen_Block(LPBYTE IN_RGB, CRectData *Rect, int X, int Y);
	void	PatternNoiseGen_Block_12bit(LPWORD IN_RGB, CRectData *Rect, int X, int Y);
	bool	STDGen_16bit(WORD *GRAYScanBuf, int NUM);
	void	STDGen_Block_16bit(LPWORD GRAYScanBuf, CRectData *Rect, int X, int Y);
	void	SAVE_FILE_STD(CRectData *Rect, int X, int Y);
	CString PN_filename;
	void	Save_parameter();
	void	Save_parameter_set();
	void	Load_parameter();
	void	UploadList();
	CRectData PN_RECT[9];
	CRectData PN_Original[9];
	CRectData FULL_RECT[BLOCK_WIDTH*BLOCK_HEIGHT];
	void Rect_Set(CRectData *Rect, int Num);
	int ChangeItem[9];
	bool ChangeCheck;
	void EditWheelIntSet(int nID,int Val);
	int InIndex;
	int TestMod;
	int iSavedItem, iSavedSubitem;
	void List_COLOR_Change();
	bool Change_DATA_CHECK(bool FLAG);
	void Change_DATA();
	void	Save(int NUM);
	CRect rect;
	CString Result;
	int changecount;
	void EditMinMaxIntSet(int nID,int* Val,int Min,int Max);

	//CNoiseEnvironment *subdlg;
// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_PatternNoise };

	void SetList();

	//-------------------------------------------------------------------worklist
	void Set_List(CListCtrl *List);
	void InsertList();
	int InsertIndex;
	int Lot_InsertIndex;
	int ListItemNum;
	void EXCEL_SAVE();
	bool EXCEL_UPLOAD();
	void FAIL_UPLOAD();
	BOOL	b_StopFail;
	double dark;
	double noise;
	double differ;
	double noisePercent;

	int m_Brightness;
	int m_Contrast;

	// MES 정보저장
	CString Str_Mes[2];
	CString m_szMesResult;
	CString Mes_Result();

	#pragma region LOT관련

	void 	LOT_Set_List(CListCtrl *List);
	void	LOT_InsertDataList();

	int Lot_StartCnt;
	int Lot_InsertNum;

	void	LOT_EXCEL_SAVE();
	bool	LOT_EXCEL_UPLOAD();

	void	InitEVMS();
#pragma endregion

private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:

	int ThrDark,ThrNoise;
	int ThrDiff;
	afx_msg void OnBnClickedButtonPnSave();
	virtual BOOL OnInitDialog();
	CListCtrl m_PatternNoiseList;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnEnChangeThresoldY();
	afx_msg void OnEnChangeThresoldV();
	afx_msg void OnEnChangeThresoldD();
	//afx_msg void OnBnClickedButtonPnSave2();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedButtonPnLoad();
	int PN_Total_PosX;
	int PN_Total_PosY;
	int PN_Dis_Width;
	int PN_Dis_Height;
	int PN_Total_Width;
	int PN_Total_Height;
	CListCtrl PN_DATALIST;
	afx_msg void OnBnClickedButtonPnSavezone();
	afx_msg void OnNMClickListPatternnoise(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawListPatternnoise(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusEditAMod();
	afx_msg void OnNMDblclkListPatternnoise(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonsetpnzone();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnEnChangeEditAMod();
	afx_msg void OnEnChangePnPosx();
	afx_msg void OnEnChangePnPosy();
	afx_msg void OnEnChangePnDisW();
	afx_msg void OnEnChangePnDisH();
	afx_msg void OnEnChangePnWidth();
	afx_msg void OnEnChangePnHeight();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonCam();
	CString str_CamBrightness;
	CString str_CamContrast;
	afx_msg void OnEnChangeEditCamBright();
	afx_msg void OnEnChangeEditCamContrast();
	afx_msg void OnBnClickedButtontest();
	CListCtrl m_Lot_PatternNoiseList;
	BOOL b_BlockDataSave;
	afx_msg void OnBnClickedChkBlockDataSave();
	double m_dPatternNoiseOffset;
	afx_msg void OnBnClickedButtonMasterload();
	BOOL Master_Data_Load();
	bool SNRGen_16bit(WORD *GRAYScanBuf, int NUM);
	CString str_MasterPath;
};
void CModel_Create(CPatternNoise_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO);
void CModel_Delete(CPatternNoise_Option **pWnd);