// ControlDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "ControlDlg.h"

#define min_Volt 100
// CControlDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CControlDlg, CDialog)

CControlDlg::CControlDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CControlDlg::IDD, pParent)
{

	for(int t=0; t<5; t++){
		ChangeItem[t]=-1;
	}
	ChangeCheck=0;
	changecount=0;

}

CControlDlg::~CControlDlg()
{
}

void CControlDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_DATA, m_DataList);
	DDX_Control(pDX, IDC_SPIN_LEFT, m_SpinCtrl[0]);
	DDX_Control(pDX, IDC_SPIN_CENTER, m_SpinCtrl[1]);
	DDX_Control(pDX, IDC_SPIN_RIGHT, m_SpinCtrl[2]);
	DDX_Control(pDX, IDC_SPIN_DUST, m_SpinCtrl[3]);
	DDX_Control(pDX, IDC_SPIN_IR, m_SpinCtrl[4]);
}


BEGIN_MESSAGE_MAP(CControlDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_TOTAL_ON, &CControlDlg::OnBnClickedButtonTotalOn)
	ON_BN_CLICKED(IDC_BUTTON_LEFT_ON, &CControlDlg::OnBnClickedButtonLeftOn)
	ON_BN_CLICKED(IDC_BUTTON_CENTER_ON, &CControlDlg::OnBnClickedButtonCenterOn)
	ON_BN_CLICKED(IDC_BUTTON_RIGHT_ON, &CControlDlg::OnBnClickedButtonRightOn)
	ON_BN_CLICKED(IDC_BUTTON_DUST_ON, &CControlDlg::OnBnClickedButtonDustOn)
	ON_BN_CLICKED(IDC_BUTTON_IR_ON, &CControlDlg::OnBnClickedButtonIrOn)
	ON_BN_CLICKED(IDC_BUTTON_APPLY, &CControlDlg::OnBnClickedButtonApply)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CControlDlg::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_LOAD, &CControlDlg::OnBnClickedButtonLoad)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_DATA, &CControlDlg::OnNMDblclkListData)
	ON_EN_KILLFOCUS(IDC_EDIT_DATA, &CControlDlg::OnEnKillfocusEditData)
	ON_WM_MOUSEWHEEL()
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_LEFT, &CControlDlg::OnDeltaposSpinLeft)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_CENTER, &CControlDlg::OnDeltaposSpinCenter)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_RIGHT, &CControlDlg::OnDeltaposSpinRight)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_DUST, &CControlDlg::OnDeltaposSpinDust)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_IR, &CControlDlg::OnDeltaposSpinIr)
	ON_WM_CTLCOLOR()
	ON_EN_SETFOCUS(IDC_EDIT_STATUS, &CControlDlg::OnEnSetfocusEditStatus)
	ON_WM_SHOWWINDOW()
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_DATA, &CControlDlg::OnNMCustomdrawListData)
	ON_BN_CLICKED(IDC_BUTTON_TOTAL_OFF, &CControlDlg::OnBnClickedButtonTotalOff)
	ON_WM_MEASUREITEM()
	ON_BN_CLICKED(IDC_BUTTON_LEFT_OFF, &CControlDlg::OnBnClickedButtonLeftOff)
	ON_BN_CLICKED(IDC_BUTTON_CENTER_OFF, &CControlDlg::OnBnClickedButtonCenterOff)
	ON_BN_CLICKED(IDC_BUTTON_RIGHT_OFF, &CControlDlg::OnBnClickedButtonRightOff)
	ON_BN_CLICKED(IDC_BUTTON_DUST_OFF, &CControlDlg::OnBnClickedButtonDustOff)
	ON_BN_CLICKED(IDC_BUTTON_IR_OFF, &CControlDlg::OnBnClickedButtonIrOff)
END_MESSAGE_MAP()


// CControlDlg 메시지 처리기입니다.
void CControlDlg::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CControlDlg::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT)
{
	pTStat		= pTEDIT;
	str_model.Empty();
	str_model.Format("CONTROL OPTION");
	Setup(IN_pMomWnd);
}
BOOL CControlDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	Font_Size_Change(IDC_EDIT_NAME,&ft,300,20);
	GetDlgItem(IDC_EDIT_STATUS)->SetFont(&ft);
	Font_Size_Change(IDC_BUTTON_SAVE, &ft1, 300, 20);
	GetDlgItem(IDC_BUTTON_LOAD)->SetFont(&ft);
	

	((CEdit *)GetDlgItem(IDC_EDIT_NAME))->SetWindowText("광원 Control Program");

	for(int t=0; t<5;t++){
		m_SpinCtrl[t].SetRange(0,240);
	}
	
	SETLIST();
	InsertList();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
void CControlDlg::Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height)//나중에 전역변수로 간략화 할것임
{	
	LOGFONT		LogFont;

	GetDlgItem(nID)->GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight =Weight ;

	LogFont.lfHeight = Height;

	font->CreateFontIndirect(&LogFont);

	GetDlgItem(nID)->SetFont(font);
}
BOOL CControlDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

		if (pMsg->wParam == VK_RETURN){

			return TRUE;
		}

	}

	return CDialog::PreTranslateMessage(pMsg);
}


void CControlDlg::Save_Parameter(){

	str_ModelPath=((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	CString str;
	CString strTitle;
	for(int t=0; t<5;t++){
		strTitle.Format("Power_%d",t+1);
		str.Empty();
		str.Format("%d",Power_Ctl_Data[t]);
		WritePrivateProfileString(str_model,strTitle,str,str_ModelPath);
		strTitle.Format("Current_%d", t + 1);
		str.Empty();
		str.Format("%d", Current_Ctl_Data[t]);
		WritePrivateProfileString(str_model, strTitle, str, str_ModelPath);

		((CImageTesterDlg *)m_pMomWnd)->Power_Ctl_Data[t] = Power_Ctl_Data[t];
		((CImageTesterDlg *)m_pMomWnd)->Current_Ctl_Data[t] = Current_Ctl_Data[t];
	}
	InsertList();
}

void CControlDlg::Load_Parameter(){
	
	CString str;
	CString strTitle;
	str_ModelPath=((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	int buf_ctldata[5] = {1200,1200,1200,1200,1200};
	int buf_ctldata_Current[5] = { 1000, 1000, 1000, 1000, 1000 };
	int strbuf = GetPrivateProfileInt(str_model,"PowerFlag",-1,str_ModelPath);
	if(strbuf == -1){
		WritePrivateProfileString(str_model,"PowerFlag","1",str_ModelPath);
		for (int t=0; t<5; t++)
		{
			strTitle.Format("Power_%d",t+1);
			Power_Ctl_Data[t] =buf_ctldata[t];
			str.Empty();
			str.Format("%d",Power_Ctl_Data[t]);
			WritePrivateProfileString(str_model,strTitle,str,str_ModelPath);

			strTitle.Format("Current_%d", t + 1);
			Current_Ctl_Data[t] = buf_ctldata_Current[t];
			str.Empty();
			str.Format("%d", Current_Ctl_Data[t]);
			WritePrivateProfileString(str_model, strTitle, str, str_ModelPath);

			((CImageTesterDlg *)m_pMomWnd)->Power_Ctl_Data[t] = Power_Ctl_Data[t];
			((CImageTesterDlg *)m_pMomWnd)->Current_Ctl_Data[t] = Current_Ctl_Data[t];

			int num = ((int)(Power_Ctl_Data[t]))/5;
			m_SpinCtrl[t].SetPos(num);
		}
	}else{
		for (int t=0; t<5; t++)
		{
			strTitle.Format("Power_%d",t+1);
			Power_Ctl_Data[t]=GetPrivateProfileInt(str_model,strTitle,-1,str_ModelPath);
			if(Power_Ctl_Data[t] == -1){
				Power_Ctl_Data[t] =buf_ctldata[t];
				str.Empty();
				str.Format("%d",Power_Ctl_Data[t]);
				WritePrivateProfileString(str_model,strTitle,str,str_ModelPath);
			}

			strTitle.Format("Current_%d", t + 1);
			Current_Ctl_Data[t] = GetPrivateProfileInt(str_model, strTitle, -1, str_ModelPath);
			if (Current_Ctl_Data[t] == -1){
				Current_Ctl_Data[t] = buf_ctldata_Current[t];
				str.Empty();
				str.Format("%d", Current_Ctl_Data[t]);
				WritePrivateProfileString(str_model, strTitle, str, str_ModelPath);
			}
			((CImageTesterDlg *)m_pMomWnd)->Power_Ctl_Data[t] = Power_Ctl_Data[t];
			((CImageTesterDlg *)m_pMomWnd)->Current_Ctl_Data[t] = Current_Ctl_Data[t];

			int num = ((int)(Power_Ctl_Data[t]))/5;
			m_SpinCtrl[t].SetPos(num);
		}
	}

	InsertList();
}

void CControlDlg::SETLIST(){
	m_DataList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_DataList.ModifyStyle(LVS_OWNERDRAWFIXED, 0, 0);

	m_DataList.InsertColumn(0,"Zone",LVCFMT_CENTER, 100);
	m_DataList.InsertColumn(1,"Voltage",LVCFMT_CENTER, 110);
	m_DataList.InsertColumn(2, "Current", LVCFMT_CENTER, 110);
}
void CControlDlg::InsertList(){
	CString str;

	m_DataList.DeleteAllItems();
	for (int t=0; t<4; t++)
	{
		int InsertIndex = m_DataList.InsertItem(t,"",0);
		if (t==0)
		{
			str = "LEFT Chart";
		}
		if (t==1)
		{
			str = "CENTER Chart";
		}
		if (t==2)
		{
			str = "RIGHT Chart";
		}
		if (t==3)
		{
			str = "이물광원";
		}
		if (t==4)
		{
			str = "IR Filter";
		}

		m_DataList.SetItemText(InsertIndex,0,str);

		double DATA = (double)Power_Ctl_Data[t]/100.0;


		CString str2;
		str2.Format("%d",Power_Ctl_Data[t]);
		str.Format("%6.2f",DATA);
		m_DataList.SetItemText(InsertIndex,1,str);

		str2.Format("%d", Current_Ctl_Data[t]);
		m_DataList.SetItemText(InsertIndex, 2, str2);

		int num = ((int)(Power_Ctl_Data[t]))/5;
		m_SpinCtrl[t].SetPos(num);

	}


	
}
void CControlDlg::OnBnClickedButtonTotalOn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Enable_Button(0);
	for(int t=0; t<4; t++){
		((CImageTesterDlg *)m_pMomWnd)->Power_Ctl_Data[t] = Power_Ctl_Data[t];
		((CImageTesterDlg *)m_pMomWnd)->Current_Ctl_Data[t] = Current_Ctl_Data[t];
	}
	((CImageTesterDlg *)m_pMomWnd)->Power_Total_Control();
	Enable_Button(1);
}

void CControlDlg::OnBnClickedButtonLeftOn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Enable_Button(0);

	int t =0;
	((CImageTesterDlg *)m_pMomWnd)->Power_Ctl_Data[t] = Power_Ctl_Data[t];
	((CImageTesterDlg *)m_pMomWnd)->Current_Ctl_Data[t] = Current_Ctl_Data[t];
	((CImageTesterDlg *)m_pMomWnd)->Power_Each_Control(t);
	Enable_Button(1);
}

void CControlDlg::OnBnClickedButtonCenterOn()
{
	Enable_Button(0);
	int t =1;
	((CImageTesterDlg *)m_pMomWnd)->Power_Ctl_Data[t] = Power_Ctl_Data[t];
	((CImageTesterDlg *)m_pMomWnd)->Current_Ctl_Data[t] = Current_Ctl_Data[t];
	((CImageTesterDlg *)m_pMomWnd)->Power_Each_Control(t);
	Enable_Button(1);
}

void CControlDlg::OnBnClickedButtonRightOn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Enable_Button(0);
	int t =2;
	((CImageTesterDlg *)m_pMomWnd)->Power_Ctl_Data[t] = Power_Ctl_Data[t];
	((CImageTesterDlg *)m_pMomWnd)->Current_Ctl_Data[t] = Current_Ctl_Data[t];
	((CImageTesterDlg *)m_pMomWnd)->Power_Each_Control(t);
	Enable_Button(1);
}

void CControlDlg::OnBnClickedButtonDustOn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Enable_Button(0);
	int t =3;
	((CImageTesterDlg *)m_pMomWnd)->Power_Ctl_Data[t] = Power_Ctl_Data[t];
	((CImageTesterDlg *)m_pMomWnd)->Current_Ctl_Data[t] = Current_Ctl_Data[t];
	((CImageTesterDlg *)m_pMomWnd)->Power_Each_Control(t);
	Enable_Button(1);
}

void CControlDlg::OnBnClickedButtonIrOn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Enable_Button(0);
	int t =4;
	((CImageTesterDlg *)m_pMomWnd)->Power_Ctl_Data[t] = Power_Ctl_Data[t];
	((CImageTesterDlg *)m_pMomWnd)->Current_Ctl_Data[t] = Current_Ctl_Data[t];
	((CImageTesterDlg *)m_pMomWnd)->Power_Each_Control(t);
	Enable_Button(1);

}

void CControlDlg::OnBnClickedButtonApply()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	for(int t=0; t<5; t++){
		((CImageTesterDlg *)m_pMomWnd)->Power_Ctl_Data[t] = Power_Ctl_Data[t];
		((CImageTesterDlg *)m_pMomWnd)->Current_Ctl_Data[t] = Current_Ctl_Data[t];
	}
}

void CControlDlg::OnBnClickedButtonSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Save_Parameter();
	ChangeCheck=-1;

	for(int t=0; t<5; t++){
		ChangeItem[t]=-1;
	}
}

void CControlDlg::OnBnClickedButtonLoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Load_Parameter();
}

void CControlDlg::OnNMDblclkListData(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(pNMITEM->iSubItem == 0 || pNMITEM->iSubItem == -1 || pNMITEM->iItem == -1 )
		return ;
	iSavedItem = pNMITEM->iItem;
	iSavedSubitem = pNMITEM->iSubItem;


	if(pNMITEM->iItem != -1)
	{
		if(pNMITEM->iSubItem != 0)
		{		
			m_DataList.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			m_DataList.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_DATA))->SetWindowText(m_DataList.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_DATA))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
			((CEdit *)GetDlgItem(IDC_EDIT_DATA))->SetFocus();

		}
	}
	*pResult = 0;
}

void CControlDlg::OnEnKillfocusEditData()
{
	CString str = "";
	ChangeCheck=1;
	if((iSavedItem != -1)&&(iSavedSubitem != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_DATA, str));
		List_COLOR_Change();
		if(m_DataList.GetItemText(iSavedItem, iSavedSubitem) != str){
			Change_DATA();
			InsertList();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_DATA))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_DATA))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );
	iSavedItem = -1;
	iSavedSubitem = -1;
}
void CControlDlg::Change_DATA(){

	CString str;
	CString data = "";
	((CEdit *)GetDlgItemText(IDC_EDIT_DATA, str));
	m_DataList.SetItemText(iSavedItem, iSavedSubitem, str);

	double num = atof(str);
	int inum = atoi(str);
	if (num < 0){
		num = 0;

	}
	if (inum < 0){
		inum = 0;

	}
	if(iSavedSubitem ==1){
		int DATA = num*100.0;

		if (DATA %5!= 0)
		{
			num += 0.01;
		}

		Power_Ctl_Data[iSavedItem] = (int)(num*100.0);

	}
	if (iSavedSubitem == 2){

		Current_Ctl_Data[iSavedItem] = inum;

	}
	
	if(iSavedSubitem ==1){

		double DATA = (double)Power_Ctl_Data[iSavedItem]/100.0;

		data.Format("%6.2f",DATA);			


	}

	if (iSavedSubitem == 2){

		int DATA = Current_Ctl_Data[iSavedItem];

		data.Format("%d", DATA);


	}
	((CEdit *)GetDlgItem(IDC_EDIT_DATA))->SetWindowText(data);

}

void CControlDlg::List_COLOR_Change(){
	bool FLAG = TRUE;
	int Check=0;


	for(int t=0;t<5; t++){
		if(ChangeItem[t]==0)Check++;
		if(ChangeItem[t]==1)Check++;
		if(ChangeItem[t]==2)Check++;
		if(ChangeItem[t]==3)Check++;
		if(ChangeItem[t]==4)Check++;
	}
	if(Check !=5){
		FLAG =FALSE;
		ChangeItem[changecount]=iSavedItem;
	}

	if(!FLAG){
		for(int t=0; t<changecount+1; t++){
			for(int k=0; k<changecount+1; k++){

				if(ChangeItem[t] == ChangeItem[k]){
					if(t==k){break;	}			
					ChangeItem[k] =-1;
				}


			}
		}
		//----------------데이터 정렬
		int temp=0;

		for(int i=0; i<5; i++)
		{
			for(int j=i+1; j<5; j++)
			{
				if(ChangeItem[i] < ChangeItem[j])  // 내림차순
				{
					temp = ChangeItem[i];
					ChangeItem[i] = ChangeItem[j];
					ChangeItem[j] = temp;
				}
			}
		}
		//--------------------------------
		for(int t=0; t<5; t++){

			if(ChangeItem[t] ==-1){
				changecount =t;
				break;
			}
		}
	}
	for(int t=0; t<5; t++){
		m_DataList.Update(t);
	}

}
BOOL CControlDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	int znum = zDelta/120;
	CWnd *focusid;
	focusid = GetFocus();
	bool FLAG = FALSE;
	int Sumdata=0;
	if(znum > 0 ){
		FLAG = 1;
		Sumdata=5;

	}
	else if(znum < 0 ){
		FLAG =0;
		Sumdata=-5;
	}
	else if(znum == 0){
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	}
	int casenum = focusid->GetDlgCtrlID();
	switch(casenum){
		
		case IDC_EDIT_DATA :
			if(FLAG == 1){
				if((Change_DATA_CHECK(1) == TRUE) /*|| (znum ==-1)(WheelCheck ==0)*/){
					EditWheelIntSet(IDC_EDIT_DATA,Sumdata);
					Change_DATA();
					InsertList();
				}
			}
			else if(FLAG == 0){
				if((Change_DATA_CHECK(0) == TRUE)/* || (znum == 1)(WheelCheck ==1)*/){
					EditWheelIntSet(IDC_EDIT_DATA,Sumdata);
					Change_DATA();
					InsertList();
					//OnEnChangeEditAMod();
					//WheelCheck = 0;
				}
			}


			break;
	}
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

void CControlDlg::EditWheelIntSet(int nID,int Val)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = 0;
	}else{

		if (iSavedSubitem == 1)
		{
			double buf2 = atof(str_buf);
			CString str;
			str.Format("_%f", buf2);
			//	((CImageTesterDlg *)m_pMomWnd)->StatePrintf(str_buf+str);
			buf = buf2*100.0;
			buf += Val;
			if (buf >= 1200)
			{
				buf = 1200;
			}
			if (buf < min_Volt)
			{
				buf = min_Volt;
			}
			double input = (double)buf / 100.0;
			str_buf.Format("%6.2f", input);
			((CEdit *)GetDlgItem(nID))->SetWindowText(str_buf);
			((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
		}
		if (iSavedSubitem == 2)
		{
			int buf2 = atoi(str_buf);
			CString str;
			str.Format("_%d", buf2);
			buf = buf2;
			buf += Val;
			if (buf >= 3000)
			{
				buf = 3000;
			}
			if (buf < 0)
			{
				buf = 0;
			}
			int input = buf;
			str_buf.Format("%d", input);
			((CEdit *)GetDlgItem(nID))->SetWindowText(str_buf);
			((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
		}
	}


}

bool CControlDlg::Change_DATA_CHECK(bool FLAG){

	BOOL STAT = TRUE;
	//상한
	if(Power_Ctl_Data[iSavedItem] > 1200 )
		STAT = FALSE;
	//하한

	if(Power_Ctl_Data[iSavedItem]< min_Volt )
		STAT = FALSE;



	return STAT;
}
void CControlDlg::Enable_Button(bool MODE){

	((CButton *)GetDlgItem(IDC_BUTTON_TOTAL_ON))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BUTTON_LEFT_ON))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BUTTON_CENTER_ON))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BUTTON_RIGHT_ON))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BUTTON_DUST_ON))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BUTTON_IR_ON))->EnableWindow(MODE);

	((CButton *)GetDlgItem(IDC_BUTTON_TOTAL_OFF))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BUTTON_LEFT_OFF))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BUTTON_CENTER_OFF))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BUTTON_RIGHT_OFF))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BUTTON_DUST_OFF))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BUTTON_IR_OFF))->EnableWindow(MODE);

	((CButton *)GetDlgItem(IDC_BUTTON_LOAD))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BUTTON_APPLY))->EnableWindow(MODE);
	((CButton *)GetDlgItem(IDC_BUTTON_SAVE))->EnableWindow(MODE);
	for(int t=0; t<5;t++){
		m_SpinCtrl[t].EnableWindow(MODE);
	}

}
void CControlDlg::OnDeltaposSpinLeft(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Enable_Button(0);
	int Delta = pNMUpDown->iDelta;
	int p = pNMUpDown->iPos;
	 
	int DataNum = 0;


	CString str;
	int Power_Data =0;
	if(Delta == 1){
		str =m_DataList.GetItemText(DataNum,1);
		double Num = atof(str)+0.005;
			
		Power_Data = (int)(Num*100.0);
		Power_Data +=5;
		Power_Ctl_Data[DataNum] = Power_Data;
	}
	if(Delta == -1){
		str =m_DataList.GetItemText(DataNum,1);
		double Num = atof(str)+0.005;
		Power_Data = (int)(Num*100.0);

		Power_Data -=5;
		Power_Ctl_Data[DataNum] = Power_Data;
	}


	if (Power_Ctl_Data[DataNum] < min_Volt)
	{
		Power_Ctl_Data[DataNum] = min_Volt;
	}
	if (Power_Ctl_Data[DataNum] > 1200)
	{
		Power_Ctl_Data[DataNum] = 1200;
	}
	ChangeCheck =1;
	ChangeItem[DataNum]=DataNum;

	InsertList();
	OnBnClickedButtonLeftOn();
	
	*pResult = 0;
	Enable_Button(1);
}

void CControlDlg::OnDeltaposSpinCenter(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Enable_Button(0);
	int Delta = pNMUpDown->iDelta;
	int p = pNMUpDown->iPos;

	int DataNum = 1;

	CString str;
	int Power_Data =0;
	if(Delta == 1){
		str =m_DataList.GetItemText(DataNum,1);
		double Num = atof(str)+0.005;

		Power_Data = (int)(Num*100.0);
		Power_Data +=5;
		Power_Ctl_Data[DataNum] = Power_Data;
	}
	if(Delta == -1){
		str =m_DataList.GetItemText(DataNum,1);
		double Num = atof(str)+0.005;
		Power_Data = (int)(Num*100.0);

		Power_Data -=5;
		Power_Ctl_Data[DataNum] = Power_Data;
	}



	if (Power_Ctl_Data[DataNum] < min_Volt)
	{
		Power_Ctl_Data[DataNum] = min_Volt;
	}
	if (Power_Ctl_Data[DataNum] > 1200)
	{
		Power_Ctl_Data[DataNum] = 1200;
	}
	ChangeCheck =1;
	ChangeItem[DataNum]=DataNum;

	InsertList();
	OnBnClickedButtonCenterOn();

	*pResult = 0;
	Enable_Button(1);
}

void CControlDlg::OnDeltaposSpinRight(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Enable_Button(0);
	int Delta = pNMUpDown->iDelta;
	int p = pNMUpDown->iPos;

	int DataNum = 2;

	CString str;
	int Power_Data =0;
	if(Delta == 1){
		str =m_DataList.GetItemText(DataNum,1);
		double Num = atof(str)+0.005;

		Power_Data = (int)(Num*100.0);
		Power_Data +=5;
		Power_Ctl_Data[DataNum] = Power_Data;
	}
	if(Delta == -1){
		str =m_DataList.GetItemText(DataNum,1);
		double Num = atof(str)+0.005;
		Power_Data = (int)(Num*100.0);

		Power_Data -=5;
		Power_Ctl_Data[DataNum] = Power_Data;
	}


	if (Power_Ctl_Data[DataNum] < min_Volt)
	{
		Power_Ctl_Data[DataNum] = min_Volt;
	}
	if (Power_Ctl_Data[DataNum] > 1200)
	{
		Power_Ctl_Data[DataNum] = 1200;
	}
	ChangeCheck =1;
	ChangeItem[DataNum]=DataNum;

	InsertList();
	OnBnClickedButtonRightOn();

	*pResult = 0;
	Enable_Button(1);
}

void CControlDlg::OnDeltaposSpinDust(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	Enable_Button(0);
	int Delta = pNMUpDown->iDelta;
	int p = pNMUpDown->iPos;

	int DataNum =3;

	CString str;
	int Power_Data =0;
	if(Delta == 1){
		str =m_DataList.GetItemText(DataNum,1);
		double Num = atof(str)+0.005;

		Power_Data = (int)(Num*100.0);
		Power_Data +=5;
		Power_Ctl_Data[DataNum] = Power_Data;
	}
	if(Delta == -1){
		str =m_DataList.GetItemText(DataNum,1);
		double Num = atof(str)+0.005;
		Power_Data = (int)(Num*100.0);

		Power_Data -=5;
		Power_Ctl_Data[DataNum] = Power_Data;
	}

	if (Power_Ctl_Data[DataNum] < min_Volt)
	{
		Power_Ctl_Data[DataNum] = min_Volt;
	}
	if (Power_Ctl_Data[DataNum] > 1200)
	{
		Power_Ctl_Data[DataNum] = 1200;
	}
	ChangeCheck =1;
	ChangeItem[DataNum]=DataNum;

	InsertList();
	OnBnClickedButtonDustOn();

	*pResult = 0;
	Enable_Button(1);
}

void CControlDlg::OnDeltaposSpinIr(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	Enable_Button(0);
	int Delta = pNMUpDown->iDelta;
	int p = pNMUpDown->iPos;

	int DataNum =4;

	CString str;
	int Power_Data =0;
	if(Delta == 1){
		str =m_DataList.GetItemText(DataNum,1);
		double Num = atof(str)+0.005;

		Power_Data = (int)(Num*100.0);
		Power_Data +=5;
		Power_Ctl_Data[DataNum] = Power_Data;
	}
	if(Delta == -1){
		str =m_DataList.GetItemText(DataNum,1);
		double Num = atof(str)+0.005;
		Power_Data = (int)(Num*100.0);

		Power_Data -=5;
		Power_Ctl_Data[DataNum] = Power_Data;
	}

	if (Power_Ctl_Data[DataNum] < min_Volt)
	{
		Power_Ctl_Data[DataNum] = min_Volt;
	}
	if (Power_Ctl_Data[DataNum] > 1200)
	{
		Power_Ctl_Data[DataNum] = 1200;
	}
	ChangeCheck =1;
	ChangeItem[DataNum]=DataNum;

	InsertList();
	OnBnClickedButtonIrOn();

	*pResult = 0;
	Enable_Button(1);
}

HBRUSH CControlDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	if(pWnd->GetDlgCtrlID() == IDC_EDIT_NAME){
		pDC->SetTextColor(RGB(255, 255, 255));
		// 텍스트의 배경색상을 설정한다.
		//pDC->SetBkColor(RGB(138, 75, 36));
		pDC->SetBkColor(RGB(86,86,86));
		// 바탕색상을 설정한다.
	}
	if(pWnd->GetDlgCtrlID() ==IDC_EDIT_STATUS){
		pDC->SetTextColor(tx);
		pDC->SetBkColor(bk);
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CControlDlg::OnEnSetfocusEditStatus()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->AutoPortChk_ConCenter();
}

void CControlDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if (bShow)
	{
		if(((CImageTesterDlg *)m_pMomWnd)->m_ControlPort_Center.m_bConnected == TRUE) {
 			PORT_STATE(1);
 		}else{
 			PORT_STATE(0);
 		}
	}
}

void CControlDlg::PORT_STATE(bool MODE){
	if(MODE == 0){
		tx = RGB(0, 0, 0);
		bk = RGB(255, 255, 0);
	}else if(MODE == 1){
		tx = RGB(255, 255, 255);
		bk = RGB(47, 157, 39);
	}
	((CEdit *)GetDlgItem(IDC_EDIT_STATUS))->SetWindowText("광원 Port");
}
void CControlDlg::OnNMCustomdrawListData(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//*pResult = 0;

	 COLORREF text_color = 0;
	 COLORREF bg_color = RGB(255, 255, 255);
     /////////////////////////////////////////////////////////////default 컬러

     LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
		
	//	lplvcd->iPartId
        switch(lplvcd->nmcd.dwDrawStage){
            case CDDS_PREPAINT:
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
             
            // 배경 혹은 텍스트를 수정한다.
            case CDDS_ITEMPREPAINT:
                // 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
               /* if(lplvcd->nmcd.dwItemSpec == 0) text_color = RGB(255, 0, 0);
                else if(lplvcd->nmcd.dwItemSpec == 1) text_color = RGB(0, 255, 0);
                else text_color = RGB(0, 0, 255);
                lplvcd->clrText = text_color;*/
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
           
            // 서브 아이템의 배경 혹은 텍스트를 수정한다.
            case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
                if(lplvcd->iSubItem != 0){
                    // 1번째 행이라면...

					if(lplvcd->nmcd.dwItemSpec >=0 ||lplvcd->nmcd.dwItemSpec <5){
						
						if(ChangeCheck==1){
							if(lplvcd->iSubItem ==1 ){
								for(int t=0; t<5; t++){
									if(lplvcd->nmcd.dwItemSpec == ChangeItem[t]){
										text_color = RGB(0, 0, 0);
										bg_color = RGB(250, 230, 200);					
									}
								}
							}
						
						
						}
					
					}
					

				    lplvcd->clrText = text_color;
                    lplvcd->clrTextBk = bg_color;
					
                }
                *pResult = CDRF_NEWFONT;
                return;
        }
}

void CControlDlg::OnBnClickedButtonTotalOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CImageTesterDlg *)m_pMomWnd)->Power_Total_Control_off();

}

void CControlDlg::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDCtl == IDC_LIST_DATA)
	{
		lpMeasureItemStruct->itemHeight += 12;      //  - 연산 설정하면 높이가 줄어듭니다.
	}

	CDialog::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CControlDlg::OnBnClickedButtonLeftOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Enable_Button(0);
	((CImageTesterDlg *)m_pMomWnd)->Power_Each_Control_Off(0);
	Enable_Button(1);
}

void CControlDlg::OnBnClickedButtonCenterOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Enable_Button(0);
	((CImageTesterDlg *)m_pMomWnd)->Power_Each_Control_Off(1);
	Enable_Button(1);
}

void CControlDlg::OnBnClickedButtonRightOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Enable_Button(0);
	((CImageTesterDlg *)m_pMomWnd)->Power_Each_Control_Off(2);
	Enable_Button(1);
}

void CControlDlg::OnBnClickedButtonDustOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Enable_Button(0);
	((CImageTesterDlg *)m_pMomWnd)->Power_Each_Control_Off(3);
	Enable_Button(1);
}

void CControlDlg::OnBnClickedButtonIrOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Enable_Button(0);
	((CImageTesterDlg *)m_pMomWnd)->Power_Each_Control_Off(5);
	Enable_Button(1);
}

void CControlDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	Load_Parameter();

	OnBnClickedButtonTotalOn();
	CDialog::OnCancel();
}
