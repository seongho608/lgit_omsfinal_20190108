// Distortion_Option.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LT_EXE_ImageTester.h"
#include "LT_EXE_ImageTesterDlg.h"
#include "Distortion_Option.h"


// CDistortion_Option 대화 상자입니다.
extern BYTE	m_RGBScanbuf[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4];


IMPLEMENT_DYNAMIC(CDistortion_Option, CDialog)

CDistortion_Option::CDistortion_Option(CWnd* pParent /*=NULL*/)
	: CDialog(CDistortion_Option::IDD, pParent)
	, A_Total_PosX(0)
	, A_Total_PosY(0)
	, A_Dis_Width(0)
	, A_Dis_Height(0)
	, A_Total_Width(0)
	, A_Total_Height(0)
	, A_Thresold(0)
	, AngleKeepRun(FALSE)
	, str_focallength(_T(""))
	, str_camPixel(_T(""))
	, str_ChartDistance(_T(""))
	, str_Distortion(_T(""))
	, str_DEV_Distortion(_T(""))
	, str_MotorDis(_T(""))
	, str_DistortionOffset(_T(""))
	, m_strDistortionScale(_T(""))
{
	m_dDistortionScale = 0.0;

	iSavedItem=0;
	iSavedSubitem=0;
	iSavedSubitem2=0;
	state=0;
	EnterState=0;
	MasterMod =FALSE;
	NewItem =0;NewSubitem=0;
	disL=0;
	disR=0;
	disT=0;
	disB=0;
	TestMod=0;
	AutomationMod=0;
	ChangeCheck=0;
	WheelCheck=0;
	for(int t=0; t<5; t++){
		ChangeItem[t]=-1;
	}
	changecount=0;
	StartCnt=0;
	Lot_StartCnt = 0;
	b_EtcModel_TEST=0;
	TestMod =FALSE;
	b_StopFail = FALSE;

	d_Distortion = 0;

	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;

	//****************Side Circle 기준 탐색 점 설정*******************//
	Standard_SC_RECT[0].m_resultX = (CAM_IMAGE_WIDTH/2);	//중앙 Side circle
	Standard_SC_RECT[0].m_resultY = (CAM_IMAGE_HEIGHT/2);
	Standard_SC_RECT[1].m_resultX = 370;	//왼쪽 Side circle
	Standard_SC_RECT[1].m_resultY = (CAM_IMAGE_HEIGHT/2);
	Standard_SC_RECT[2].m_resultX = 915;	//오른쪽 Side circle
	Standard_SC_RECT[2].m_resultY = (CAM_IMAGE_HEIGHT/2);
	Standard_SC_RECT[3].m_resultX = (CAM_IMAGE_WIDTH/2);	//위쪽 Side circle
	Standard_SC_RECT[3].m_resultY = 0;
	Standard_SC_RECT[4].m_resultX = (CAM_IMAGE_WIDTH/2);	//아래쪽 Side circle
	Standard_SC_RECT[4].m_resultY = CAM_IMAGE_HEIGHT;
//****************************************************************//

}

CDistortion_Option::~CDistortion_Option()
{
//	KillTimer(110);
}

void CDistortion_Option::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_Angle, A_DATALIST);
	DDX_Text(pDX, IDC_Angle_PosX, A_Total_PosX);
	DDX_Text(pDX, IDC_Angle_PosY, A_Total_PosY);
	DDX_Text(pDX, IDC_Angle_Dis_W, A_Dis_Width);
	DDX_Text(pDX, IDC_Angle_Dis_H, A_Dis_Height);
	DDX_Text(pDX, IDC_Angle_Width, A_Total_Width);
	DDX_Text(pDX, IDC_Angle_Height, A_Total_Height);
	DDX_Text(pDX, IDC_Angle_Thresold, A_Thresold);
	DDX_Control(pDX, IDC_LIST_ANGLELIST, m_AngleList);
	DDX_Check(pDX, IDC_CHECK_Angle_Keeprun, AngleKeepRun);
	DDX_Control(pDX, IDC_CHECK_Angle_Keeprun, btn_AngleKeepRun);
	DDX_Text(pDX, IDC_EDIT_CAM_FC, str_focallength);
	DDX_Text(pDX, IDC_EDIT_CAM_PIXEL, str_camPixel);
	DDX_Text(pDX, IDC_EDIT_CHART_DIS, str_ChartDistance);
	DDX_Text(pDX, IDC_EDIT_DISTORTION, str_Distortion);
	DDX_Text(pDX, IDC_EDIT_DEV_Distortion, str_DEV_Distortion);
	DDX_Text(pDX, IDC_EDIT_CAM_DISTANCE, str_MotorDis);
	DDX_Control(pDX, IDC_LIST_ANGLELIST_LOT, m_Lot_DistortionList);
	DDX_Text(pDX, IDC_EDIT_DISTORTION_OFFSET, str_DistortionOffset);

	DDX_Text(pDX, IDC_EDIT_DISTORTION_SCALE, m_strDistortionScale);
}


BEGIN_MESSAGE_MAP(CDistortion_Option, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_setAngleZone, &CDistortion_Option::OnBnClickedButtonsetanglezone)
	ON_BN_CLICKED(IDC_BUTTON_A_RECT_SAVE, &CDistortion_Option::OnBnClickedButtonARectSave)
	ON_NOTIFY(NM_CLICK, IDC_LIST_Angle, &CDistortion_Option::OnNMClickListAngle)
//	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_MASTER_A, &CDistortion_Option::OnBnClickedButtonMasterA)
	ON_BN_CLICKED(IDC_CHECK_AUTO, &CDistortion_Option::OnBnClickedCheckAuto)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_Angle, &CDistortion_Option::OnNMCustomdrawListAngle)
//	ON_WM_SETFOCUS()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_Angle, &CDistortion_Option::OnNMDblclkListAngle)
	ON_EN_KILLFOCUS(IDC_EDIT_A_MOD, &CDistortion_Option::OnEnKillfocusEditAMod)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_A_Load, &CDistortion_Option::OnBnClickedButtonALoad)
//	ON_BN_CLICKED(IDC_CHECK_Angle_Keeprun, &CDistortion_Option::OnBnClickedCheckAngleKeeprun)
	ON_WM_DESTROY()
	ON_WM_KILLFOCUS()
	ON_WM_MOUSEWHEEL()
	ON_EN_CHANGE(IDC_Angle_PosX, &CDistortion_Option::OnEnChangeAnglePosx)
	ON_EN_CHANGE(IDC_Angle_PosY, &CDistortion_Option::OnEnChangeAnglePosy)
	ON_EN_CHANGE(IDC_Angle_Dis_W, &CDistortion_Option::OnEnChangeAngleDisW)
	ON_EN_CHANGE(IDC_Angle_Dis_H, &CDistortion_Option::OnEnChangeAngleDisH)
	ON_EN_CHANGE(IDC_Angle_Width, &CDistortion_Option::OnEnChangeAngleWidth)
	ON_EN_CHANGE(IDC_Angle_Height, &CDistortion_Option::OnEnChangeAngleHeight)
	ON_EN_CHANGE(IDC_Angle_Thresold, &CDistortion_Option::OnEnChangeAngleThresold)
	ON_EN_CHANGE(IDC_EDIT_A_MOD, &CDistortion_Option::OnEnChangeEditAMod)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_Angle, &CDistortion_Option::OnLvnItemchangedListAngle)
	ON_BN_CLICKED(IDC_BUTTON_test, &CDistortion_Option::OnBnClickedButtontest)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CDistortion_Option::OnBnClickedButtonSave)
	ON_EN_CHANGE(IDC_EDIT_CAM_FC, &CDistortion_Option::OnEnChangeEditCamFc)
	ON_EN_CHANGE(IDC_EDIT_CAM_PIXEL, &CDistortion_Option::OnEnChangeEditCamPixel)
	ON_EN_CHANGE(IDC_EDIT_CHART_DIS, &CDistortion_Option::OnEnChangeEditChartDis)
	ON_EN_CHANGE(IDC_EDIT_DISTORTION, &CDistortion_Option::OnEnChangeEditDistortion)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_DISTORTION, &CDistortion_Option::OnBnClickedButtonSaveDistortion)
	ON_EN_CHANGE(IDC_EDIT_DEV_Distortion, &CDistortion_Option::OnEnChangeEditDevDistortion)
	ON_EN_CHANGE(IDC_EDIT_CAM_DISTANCE, &CDistortion_Option::OnEnChangeEditCamDistance)
	ON_EN_CHANGE(IDC_EDIT_DISTORTION_OFFSET, &CDistortion_Option::OnEnChangeEditDistortionOffset)
	ON_EN_CHANGE(IDC_EDIT_DISTORT_DIST, &CDistortion_Option::OnEnChangeEditDistortDist)
	ON_BN_CLICKED(IDC_BTN_DISTORTION_MOVE_SAVE, &CDistortion_Option::OnBnClickedBtnDistortionMoveSave)
	ON_BN_CLICKED(IDC_BTN_CHART_TESTMOVE2, &CDistortion_Option::OnBnClickedBtnChartTestmove2)
END_MESSAGE_MAP()


// CDistortion_Option 메시지 처리기입니다.

void CDistortion_Option::Setup(CWnd* IN_pMomWnd)
{
	m_pMomWnd	= IN_pMomWnd;
}

void CDistortion_Option::Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO)
{
	m_pMomWnd	= IN_pMomWnd;
	pTStat		= pTEDIT;
	m_INFO      = INFO;
	str_model.Empty();
	str_model.Format(INFO.NAME);
}
// CDistortion_Option 메시지 처리기입니다.

BOOL CDistortion_Option::OnInitDialog()
{
	CDialog::OnInitDialog();

	//-------------------------------------영상 이미지 변수 저장//0721
	m_CAM_SIZE_WIDTH = CAM_IMAGE_WIDTH;
	m_CAM_SIZE_HEIGHT = CAM_IMAGE_HEIGHT;
	//----------------------------------------------------------


	((CEdit *) GetDlgItem(IDC_EDIT_A_MOD))->ShowWindow(FALSE);
	SETLIST();

	A_filename=((CImageTesterDlg *)m_pMomWnd)->modelfile_Name;
	
	Load_parameter();
	UpdateData(FALSE);

	UploadList();

	Set_List(&m_AngleList);
	LOT_Set_List(&m_Lot_DistortionList);


	UpdateData(FALSE);
	if(AutomationMod == 1){
		AutomationMod =0;	
		OnBnClickedCheckAuto();
		CheckDlgButton(IDC_CHECK_AUTO,TRUE);
	}

	((CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE))->EnableWindow(0);
	
	InitEVMS();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDistortion_Option::InitEVMS()
{
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		((CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_A_Load))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_CHECK_AUTO))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_MASTER_A))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_SAVE_DISTORTION))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BUTTON_SAVE))->EnableWindow(0);

		((CEdit *)GetDlgItem(IDC_EDIT_DISTORTION))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_DEV_Distortion))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_CHART_DIS))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_CAM_FC))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_EDIT_CAM_PIXEL))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Angle_PosX))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Angle_PosY))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Angle_Dis_W))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Angle_Dis_H))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Angle_Width))->EnableWindow(0);
		((CEdit *)GetDlgItem(IDC_Angle_Height))->EnableWindow(0);
	}
}

void CDistortion_Option::SETLIST(){
	A_DATALIST.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	A_DATALIST.InsertColumn(0,"Zone",LVCFMT_CENTER, 90);
	A_DATALIST.InsertColumn(1,"PosX",LVCFMT_CENTER, 60);
	A_DATALIST.InsertColumn(2,"PosY",LVCFMT_CENTER, 60);
	A_DATALIST.InsertColumn(3,"START X",LVCFMT_CENTER, 60);
	A_DATALIST.InsertColumn(4,"START Y",LVCFMT_CENTER, 60);
	A_DATALIST.InsertColumn(5,"EXIT X",LVCFMT_CENTER, 60);
	A_DATALIST.InsertColumn(6,"EXIT Y",LVCFMT_CENTER, 60);
	A_DATALIST.InsertColumn(7,"Width",LVCFMT_CENTER, 60);
	A_DATALIST.InsertColumn(8,"Height",LVCFMT_CENTER, 60);

	

}
void CDistortion_Option::OnBnClickedButtonsetanglezone()
{
	//int Cam_Width = CAM_IMAGE_WIDTH;
	//int Cam_Height = CAM_IMAGE_HEIGHT;

	ChangeCheck=1;
	UpdateData(TRUE);

	if(AutomationMod ==0){
		PosX=A_Total_PosX;
		PosY=A_Total_PosY;
		
		A_RECT[0].m_PosX = PosX-A_Dis_Width;	//좌 상단
		A_RECT[0].m_PosY = PosY-A_Dis_Height;	//
		A_RECT[1].m_PosX = PosX-A_Dis_Width;	//좌 하단
		A_RECT[1].m_PosY = PosY+A_Dis_Height;	//
		A_RECT[2].m_PosX = PosX;				//중앙 상단
		A_RECT[2].m_PosY = PosY-A_Dis_Height;	//
		A_RECT[3].m_PosX = PosX;				//중앙 하단
		A_RECT[3].m_PosY = PosY+A_Dis_Height;	//
		A_RECT[4].m_PosX = PosX+A_Dis_Width;	//우 상단
		A_RECT[4].m_PosY = PosY-A_Dis_Height;	//
		A_RECT[5].m_PosX = PosX+A_Dis_Width;	//우 하단
		A_RECT[5].m_PosY = PosY+A_Dis_Height;	//
	}

	for(int t=0; t<6; t++){
		A_RECT[t].m_Width= A_Total_Width;
		A_RECT[t].m_Height=A_Total_Height;
		A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);//////C

		if(A_RECT[t].m_Left < 0){//////////////////////수정
			A_RECT[t].m_Left = 0;
			A_RECT[t].m_Width = A_RECT[t].m_Right+1 -A_RECT[t].m_Left;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		if(A_RECT[t].m_Right >m_CAM_SIZE_WIDTH){
			A_RECT[t].m_Right = m_CAM_SIZE_WIDTH;
			A_RECT[t].m_Width = A_RECT[t].m_Right+1 -A_RECT[t].m_Left;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		if(A_RECT[t].m_Top< 0){
			A_RECT[t].m_Top = 0;
			A_RECT[t].m_Height = A_RECT[t].m_Bottom+1 -A_RECT[t].m_Top;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		if(A_RECT[t].m_Bottom >m_CAM_SIZE_HEIGHT){
			int gap = A_RECT[t].m_Bottom - m_CAM_SIZE_HEIGHT;
			A_RECT[t].m_Bottom = m_CAM_SIZE_HEIGHT;
			A_RECT[t].m_Height = A_RECT[t].m_Bottom -A_RECT[t].m_Top -gap;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);

		//A_RECT[t].d_Thresold = A_Thresold;
	}
	UpdateData(FALSE);

	UploadList();//리스트컨트롤의 입력


	for(int t=0; t<6; t++){
		ChangeItem[t]=t;
	}

	for(int t=0; t<6; t++){
		A_DATALIST.Update(t);
	}

	((CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE))->EnableWindow(1);
	//Save_parameter();

}

void CDistortion_Option::UploadList(){
	CString str="";
	
	A_DATALIST.DeleteAllItems();

	for(int t=0; t<6; t++){
		InIndex = A_DATALIST.InsertItem(t,"",0);
		
	
		if(t ==0){
			str.Empty();str="LEFT_TOP";
		}
		if(t ==1){
			str.Empty();str="LEFT_BOTTOM";
		}
		if(t ==2){
			str.Empty();str="CENTER_TOP";
		}
		if(t ==3){
			str.Empty();str="CENTER_BOTTOM";
		}
		if(t ==4){
			str.Empty();str="RIGHT_TOP";
		}
		if(t ==5){
			str.Empty();str="RIGHT_BOTTOM";
		}
		
		A_DATALIST.SetItemText(InIndex,0,str);
		str.Empty();str.Format("%d", A_RECT[t].m_PosX);
		A_DATALIST.SetItemText(InIndex,1,str);
		str.Empty();str.Format("%d", A_RECT[t].m_PosY);
		A_DATALIST.SetItemText(InIndex,2,str);
		str.Empty();
		str.Format("%d", A_RECT[t].m_Left);
		A_DATALIST.SetItemText(InIndex,3,str);
		str.Empty();
		str.Format("%d", A_RECT[t].m_Top);
		A_DATALIST.SetItemText(InIndex,4,str);
	
		str.Empty();
		str.Format("%d", A_RECT[t].m_Right+1);
		A_DATALIST.SetItemText(InIndex,5,str);
		str.Empty();
		str.Format("%d", A_RECT[t].m_Bottom+1);
		A_DATALIST.SetItemText(InIndex,6,str);

		str.Empty();
		str.Format("%d", A_RECT[t].m_Width);
		A_DATALIST.SetItemText(InIndex,7,str);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Height);
		A_DATALIST.SetItemText(InIndex,8,str);
		
	}
}

void CDistortion_Option::Save_parameter(){


	CString str="";
	CString strTitle="";

	//WritePrivateProfileString(str_model,NULL,"",A_filename);
	str.Empty();
	str.Format("%d",m_INFO.ID);
	WritePrivateProfileString(str_model,"ID",str,A_filename);
	str.Empty();
	str.Format("%s",m_INFO.MODE);
	WritePrivateProfileString(str_model,"MODE",str,A_filename);
	str.Empty();
	str.Format("%s",m_INFO.NAME);
	WritePrivateProfileString(str_model,"NAME",str,A_filename);

	strTitle.Empty();
	strTitle="DISTORTION_INIT";

	str.Empty();
	str.Format("%d",A_Total_PosX);
	WritePrivateProfileString(str_model,strTitle+"PosX",str,A_filename);
	str.Empty();
	str.Format("%d",A_Total_PosY);
	WritePrivateProfileString(str_model,strTitle+"PosY",str,A_filename);
	str.Empty();
	str.Format("%d",A_Dis_Width);
	WritePrivateProfileString(str_model,strTitle+"DisW",str,A_filename);
	str.Empty();
	str.Format("%d",A_Dis_Height);
	WritePrivateProfileString(str_model,strTitle+"DisH",str,A_filename);
	str.Empty();
	str.Format("%d",A_Total_Width);
	WritePrivateProfileString(str_model,strTitle+"Width",str,A_filename);
	str.Empty();
	str.Format("%d",A_Total_Height);
	WritePrivateProfileString(str_model,strTitle+"Height",str,A_filename);
	str.Empty();
	str.Format("%6.3f",A_Thresold);
	WritePrivateProfileString(str_model,strTitle+"Thresold",str,A_filename);


	str.Empty();
	str.Format("%6.4f", m_dOffset);
	WritePrivateProfileString(str_model, strTitle + "OFFSET", str, A_filename);

	str.Empty();
	str.Format("%6.4f", m_dDistortionScale);
	WritePrivateProfileString(str_model, strTitle + "SCALE", str, A_filename);

	str.Empty();
	str.Format("%6.3f",d_Focallength);
	WritePrivateProfileString(str_model,strTitle+"FOCALLENGTH",str,A_filename);
	str.Empty();
	str.Format("%6.3f",d_Campixel);
	WritePrivateProfileString(str_model,strTitle+"CAMPIXEL",str,A_filename);

	str.Empty();
	str.Format("%d",i_ChartDis);
	WritePrivateProfileString(str_model,strTitle+"CHARTDIS",str,A_filename);

	str.Empty();
	str.Format("%6.4f",d_Distortion_Master);
	WritePrivateProfileString(str_model,strTitle+"DISTORTION",str,A_filename);

	str.Empty();
	str.Format("%6.4f",d_Dev_Distortion);
	WritePrivateProfileString(str_model,strTitle+"DISTORTION_DEV",str,A_filename);

	if (m_dDistortionDistance <= 135){
		m_dDistortionDistance = 135;
	}
	else if (m_dDistortionDistance >= 615){
		m_dDistortionDistance = 615;
	}

	str.Empty();
	str.Format("%d", m_dDistortionDistance);
	WritePrivateProfileString(str_model, "DISTORTION_DISTANCE", str, A_filename);

	((CImageTesterDlg *)m_pMomWnd)->m_dDistortDistance = m_dDistortionDistance;

	if(AutomationMod ==1){
		WritePrivateProfileString(str_model,strTitle+"Auto","1",A_filename);}
	if(AutomationMod ==0){
		WritePrivateProfileString(str_model,strTitle+"Auto","0",A_filename);}
	for(int t=0; t<6; t++){
		
		if(t ==0){
			strTitle.Empty();
			strTitle="LEFT_TOP_";
		}
		if(t ==1){
			strTitle.Empty();
			strTitle="LEFT_BOTTOM_";		
		}
		if(t ==2){
			strTitle.Empty();
			strTitle="CENTER_TOP_";
		}
		if(t ==3){
			strTitle.Empty();
			strTitle="CENTER_BOTTOM_";
		}
		if(t ==4){
			strTitle.Empty();
			strTitle="RIGHT_TOP_";		
		}
		if(t ==5){
			strTitle.Empty();
			strTitle="RIGHT_BOTTOM_";
		}

	
		str.Empty();
		str.Format("%d",A_RECT[t].m_PosX);
		A_Original[t].m_PosX=A_RECT[t].m_PosX;
		WritePrivateProfileString(str_model,strTitle+"PosX",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_PosY);
		A_Original[t].m_PosY=A_RECT[t].m_PosY;
		WritePrivateProfileString(str_model,strTitle+"PosY",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Left);
		WritePrivateProfileString(str_model,strTitle+"StartX",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Top);
		WritePrivateProfileString(str_model,strTitle+"StartY",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Right);
		WritePrivateProfileString(str_model,strTitle+"ExitX",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Bottom);
		WritePrivateProfileString(str_model,strTitle+"ExitY",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Width);
		WritePrivateProfileString(str_model,strTitle+"Width",str,A_filename);
		str.Empty();
		str.Format("%d",A_RECT[t].m_Height);
		WritePrivateProfileString(str_model,strTitle+"Height",str,A_filename);
	}

}

void CDistortion_Option::OnBnClickedButtonARectSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Save_parameter();
	ChangeCheck =0;
	for(int t=0; t<6; t++){
		ChangeItem[t]=-1;
	}
	for(int t=0; t<6; t++){
		A_DATALIST.Update(t);
	}
}

void CDistortion_Option::Load_parameter(){
	CFileFind filefind;
	CString str="";
	CString strTitle="";
	CString EmptyStr="";

	
	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	if((GetPrivateProfileCString(str_model,"NAME",A_filename) != m_INFO.NAME)||(GetPrivateProfileCString(str_model,"MODE",A_filename) != m_INFO.MODE)){//ini파일이 없으면 파일을 생성한다.
		A_Total_PosX = Cam_PosX;
		A_Total_PosY = Cam_PosY;
		
		//A_Dis_Width = 200;
		//A_Dis_Height = 100;
		//A_Total_Width = 80;
		//A_Total_Height = 80;
		
		A_Dis_Width = m_CAM_SIZE_WIDTH * 0.27;
		A_Dis_Height = m_CAM_SIZE_HEIGHT * 0.20;
		A_Total_Width = ((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.13;
		A_Total_Height = ((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.13;
		A_Thresold = 5;

		m_dOffset = 1.0;
		m_dDistortionScale = 1.0;

		PosX = A_Total_PosX;
		PosY = A_Total_PosY;

		A_RECT[0].m_PosX = PosX - A_Dis_Width;	//좌 상단
		A_RECT[0].m_PosY = PosY - A_Dis_Height;	//
		A_RECT[1].m_PosX = PosX - A_Dis_Width;	//좌 하단
		A_RECT[1].m_PosY = PosY + A_Dis_Height;	//
		A_RECT[2].m_PosX = PosX;				//중앙 상단
		A_RECT[2].m_PosY = PosY - A_Dis_Height;	//
		A_RECT[3].m_PosX = PosX;				//중앙 하단
		A_RECT[3].m_PosY = PosY + A_Dis_Height;	//
		A_RECT[4].m_PosX = PosX + A_Dis_Width;	//우 상단
		A_RECT[4].m_PosY = PosY - A_Dis_Height;	//
		A_RECT[5].m_PosX = PosX + A_Dis_Width;	//우 하단
		A_RECT[5].m_PosY = PosY + A_Dis_Height;	//

		for (int t = 0; t < 6; t++){
			A_RECT[t].m_Width = A_Total_Width;
			A_RECT[t].m_Height = A_Total_Height;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX, A_RECT[t].m_PosY, A_RECT[t].m_Width, A_RECT[t].m_Height);//////C

			if (A_RECT[t].m_Left < 0){//////////////////////수정
				A_RECT[t].m_Left = 0;
				A_RECT[t].m_Width = A_RECT[t].m_Right + 1 - A_RECT[t].m_Left;
				A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX, A_RECT[t].m_PosY, A_RECT[t].m_Width, A_RECT[t].m_Height);
			}
			if (A_RECT[t].m_Right >m_CAM_SIZE_WIDTH){
				A_RECT[t].m_Right = m_CAM_SIZE_WIDTH;
				A_RECT[t].m_Width = A_RECT[t].m_Right + 1 - A_RECT[t].m_Left;
				A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX, A_RECT[t].m_PosY, A_RECT[t].m_Width, A_RECT[t].m_Height);
			}
			if (A_RECT[t].m_Top< 0){
				A_RECT[t].m_Top = 0;
				A_RECT[t].m_Height = A_RECT[t].m_Bottom + 1 - A_RECT[t].m_Top;
				A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX, A_RECT[t].m_PosY, A_RECT[t].m_Width, A_RECT[t].m_Height);
			}
			if (A_RECT[t].m_Bottom >m_CAM_SIZE_HEIGHT){
				int gap = A_RECT[t].m_Bottom - m_CAM_SIZE_HEIGHT;
				A_RECT[t].m_Bottom = m_CAM_SIZE_HEIGHT;
				A_RECT[t].m_Height = A_RECT[t].m_Bottom - A_RECT[t].m_Top - gap;
				A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX, A_RECT[t].m_PosY, A_RECT[t].m_Width, A_RECT[t].m_Height);
			}
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX, A_RECT[t].m_PosY, A_RECT[t].m_Width, A_RECT[t].m_Height);

			//A_RECT[t].d_Thresold = A_Thresold;
		}
// 		A_RECT[0].m_PosX=Cam_PosX;
// 		A_RECT[0].m_PosY=Cam_PosY;
// 		A_RECT[0].m_Width=A_Total_Width;
// 		A_RECT[0].m_Height=A_Total_Height;
// 
// 		A_RECT[1].m_PosX=Cam_PosX-A_Dis_Width;
// 		A_RECT[1].m_PosY=Cam_PosY;
// 		A_RECT[1].m_Width=A_Total_Width;
// 		A_RECT[1].m_Height=A_Total_Height;	
// 
// 		A_RECT[2].m_PosX=Cam_PosX+A_Dis_Width;
// 		A_RECT[2].m_PosY=Cam_PosY;
// 		A_RECT[2].m_Width=A_Total_Width;
// 		A_RECT[2].m_Height=A_Total_Width;
// 
// 		//----
// 		A_Original[0].m_PosX=Cam_PosX;
// 		A_Original[0].m_PosY=Cam_PosY;
// 		A_Original[1].m_PosX=Cam_PosX-A_Dis_Width;
// 		A_Original[1].m_PosY=Cam_PosY;
// 		A_Original[2].m_PosX=Cam_PosX+A_Dis_Width;;
// 		A_Original[2].m_PosY=Cam_PosY;
// 		for(int t=0; t<6; t++){
// 			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);//////C
// 			
// 		}

		//-------------------------------------
		AutomationMod =0;

		d_Focallength = 1.6;
		d_Campixel = 3;
		i_ChartDis = 220;
		d_Distortion_Master =0;
		d_Dev_Distortion = 3;
		m_dDistortionDistance = 300;

		Save_parameter();
	}else{
		
		strTitle="DISTORTION_INIT";

		A_Total_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,A_filename);
		if(A_Total_PosX == -1){
			A_Total_PosX =Cam_PosX;
			str.Empty();
			str.Format("%d",A_Total_PosX);
			WritePrivateProfileString(str_model,strTitle+"PosX",str,A_filename);
		}
		A_Total_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,A_filename);
		if(A_Total_PosY == -1){
			A_Total_PosY =Cam_PosY;
			str.Empty();
			str.Format("%d",A_Total_PosY);
			WritePrivateProfileString(str_model,strTitle+"PosY",str,A_filename);
		}
		A_Dis_Width=GetPrivateProfileInt(str_model,strTitle+"DisW",-1,A_filename);
		if(A_Dis_Width == -1){
			A_Dis_Width = m_CAM_SIZE_WIDTH * 0.27;
			str.Empty();
			str.Format("%d",A_Dis_Width);
			WritePrivateProfileString(str_model,strTitle+"DisW",str,A_filename);
		}
		A_Dis_Height=GetPrivateProfileInt(str_model,strTitle+"DisH",-1,A_filename);
		if(A_Dis_Height == -1){
			A_Dis_Height =m_CAM_SIZE_HEIGHT * 0.20;
			str.Empty();
			str.Format("%d",A_Dis_Height);
			WritePrivateProfileString(str_model,strTitle+"DisH",str,A_filename);
		}
		A_Total_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,A_filename);
		if(A_Total_Width == -1){
			A_Total_Width =((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.13;
			str.Empty();
			str.Format("%d",A_Total_Width);
			WritePrivateProfileString(str_model,strTitle+"Width",str,A_filename);
		}
		A_Total_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,A_filename);
		if(A_Total_Height == -1){
			A_Total_Height =((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.13;
			str.Empty();
			str.Format("%d",A_Total_Height);
			WritePrivateProfileString(str_model,strTitle+"Height",str,A_filename);
		}

		i_ChartDis=GetPrivateProfileInt(str_model,strTitle+"CHARTDIS",-1,A_filename);
		if(i_ChartDis == -1){
			i_ChartDis = 220;
			str.Empty();
			str.Format("%d",i_ChartDis);
			WritePrivateProfileString(str_model,strTitle+"CHARTDIS",str,A_filename);

		}
		d_Focallength = GetPrivateProfileDouble(str_model,strTitle+"FOCALLENGTH",-1,A_filename);
		if(d_Focallength == -1){
			d_Focallength = 1.6;
			str.Empty();
			str.Format("%6.3f",d_Focallength);
			WritePrivateProfileString(str_model,strTitle+"FOCALLENGTH",str,A_filename);
		}

		d_Campixel = GetPrivateProfileDouble(str_model,strTitle+"CAMPIXEL",-1,A_filename);
		if(d_Campixel == -1){
			d_Campixel = 3;
			str.Empty();
			str.Format("%6.3f",d_Campixel);
			WritePrivateProfileString(str_model,strTitle+"CAMPIXEL",str,A_filename);

		}
		d_Distortion_Master = GetPrivateProfileDouble(str_model,strTitle+"DISTORTION",-1,A_filename);
		if(d_Distortion_Master == -1){
			d_Distortion_Master =0;
			str.Empty();
			str.Format("%6.4f",d_Distortion_Master);
			WritePrivateProfileString(str_model,strTitle+"DISTORTION",str,A_filename);

		}
		d_Dev_Distortion = GetPrivateProfileDouble(str_model,strTitle+"DISTORTION_DEV",-1,A_filename);
		if(d_Dev_Distortion == -1){
			d_Dev_Distortion = 5;
			str.Empty();
			str.Format("%6.4f",d_Dev_Distortion);
			WritePrivateProfileString(str_model,strTitle+"DISTORTION_DEV",str,A_filename);
		}
		m_dOffset = GetPrivateProfileDouble(str_model, strTitle + "OFFSET", -999, A_filename);
		if (m_dOffset == -999){
			m_dOffset = 1.0;
			str.Empty();
			str.Format("%6.4f", m_dOffset);
			WritePrivateProfileString(str_model, strTitle + "OFFSET", str, A_filename);
		}

		m_dDistortionScale = GetPrivateProfileDouble(str_model, strTitle + "SCALE", -999, A_filename);
		if (m_dDistortionScale == -999)
		{
			m_dDistortionScale = 1.0;
			str.Empty();
			str.Format("%6.4f", m_dDistortionScale);
			WritePrivateProfileString(str_model, strTitle + "SCALE", str, A_filename);
		}

		m_dDistortionDistance = GetPrivateProfileInt(str_model, "DISTORTION_DISTANCE", -1, A_filename);
		if (m_dDistortionDistance == -1){
			m_dDistortionDistance = 300;
			str.Empty();
			str.Format("%d", m_dDistortionDistance);
			WritePrivateProfileString(str_model, "DISTORTION_DISTANCE", str, A_filename);
		}

		((CImageTesterDlg *)m_pMomWnd)->m_dDistortDistance = m_dDistortionDistance;

		SetDlgItemInt(IDC_EDIT_DISTORT_DIST, m_dDistortionDistance);

		CString Mod = GetPrivateProfileCString(str_model,strTitle+"Auto",A_filename);
		if("1" == Mod){
			AutomationMod = 1;
		}
		if(("0" == Mod)||("" == Mod)){
			AutomationMod = 0;
		}
		for(int t=0; t<6; t++){
			if(t ==0){
				strTitle.Empty();
				strTitle="LEFT_TOP_";
			}
			if(t ==1){
				strTitle.Empty();
				strTitle="LEFT_BOTTOM_";		
			}
			if(t ==2){
				strTitle.Empty();
				strTitle="CENTER_TOP_";
			}
			if(t ==3){
				strTitle.Empty();
				strTitle="CENTER_BOTTOM_";
			}
			if(t ==4){
				strTitle.Empty();
				strTitle="RIGHT_TOP_";		
			}
			if(t ==5){
				strTitle.Empty();
				strTitle="RIGHT_BOTTOM_";
			}
		
			A_RECT[t].m_PosX=GetPrivateProfileInt(str_model,strTitle+"PosX",-1,A_filename);
			if(A_RECT[t].m_PosX == -1){
				A_Total_PosX = Cam_PosX;
				A_Total_PosY = Cam_PosY;

				//A_Dis_Width = 200;
				//A_Dis_Height = 100;
				//A_Total_Width = 80;
				//A_Total_Height = 80;

				A_Dis_Width = m_CAM_SIZE_WIDTH * 0.27;
				A_Dis_Height = m_CAM_SIZE_HEIGHT * 0.20;
				A_Total_Width = ((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.13;
				A_Total_Height = ((m_CAM_SIZE_WIDTH+m_CAM_SIZE_HEIGHT)/2) * 0.13;

				A_RECT[0].m_PosX=Cam_PosX;
				A_RECT[0].m_PosY=Cam_PosY;
				A_RECT[0].m_Width=A_Total_Width;
				A_RECT[0].m_Height=A_Total_Height;

				A_RECT[1].m_PosX=Cam_PosX-A_Dis_Width;
				A_RECT[1].m_PosY=Cam_PosY;
				A_RECT[1].m_Width=A_Total_Width;
				A_RECT[1].m_Height=A_Total_Height;	

				A_RECT[2].m_PosX=Cam_PosX+A_Dis_Width;
				A_RECT[2].m_PosY=Cam_PosY;
				A_RECT[2].m_Width=A_Total_Width;
				A_RECT[2].m_Height=A_Total_Width;

				//----
				A_Original[0].m_PosX=Cam_PosX;
				A_Original[0].m_PosY=Cam_PosY;
				A_Original[1].m_PosX=Cam_PosX-A_Dis_Width;
				A_Original[1].m_PosY=Cam_PosY;
				A_Original[2].m_PosX=Cam_PosX+A_Dis_Width;;
				A_Original[2].m_PosY=Cam_PosY;
				for(int t=0; t<6; t++){
					A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);//////C
					
				}

				Save_parameter();
			}
			A_Original[t].m_PosX=A_RECT[t].m_PosX;
			A_RECT[t].m_PosY=GetPrivateProfileInt(str_model,strTitle+"PosY",-1,A_filename);
			A_Original[t].m_PosY=A_RECT[t].m_PosY;
			A_RECT[t].m_Left=GetPrivateProfileInt(str_model,strTitle+"StartX",-1,A_filename);
			A_RECT[t].m_Top=GetPrivateProfileInt(str_model,strTitle+"StartY",-1,A_filename);
			A_RECT[t].m_Right=GetPrivateProfileInt(str_model,strTitle+"ExitX",-1,A_filename);
			A_RECT[t].m_Bottom=GetPrivateProfileInt(str_model,strTitle+"ExitY",-1,A_filename);
			A_RECT[t].m_Width=GetPrivateProfileInt(str_model,strTitle+"Width",-1,A_filename);
			A_RECT[t].m_Height=GetPrivateProfileInt(str_model,strTitle+"Height",-1,A_filename);	
		}
	}

	str_ChartDistance.Format("%d",i_ChartDis);
	str_focallength.Format("%6.3f",d_Focallength);
	str_camPixel.Format("%6.3f",d_Campixel);
	str_Distortion.Format("%6.4f",d_Distortion_Master);
	str_DEV_Distortion.Format("%6.4f",d_Dev_Distortion);

	str_DistortionOffset.Format("%6.4f", m_dOffset);
	m_strDistortionScale.Format(_T("%6.4f"), m_dDistortionScale);

	i_MotorDis = ((CImageTesterDlg *)m_pMomWnd)->m_dChartDistance;
	str_MotorDis.Format("%d",((CImageTesterDlg *)m_pMomWnd)->m_dChartDistance);
	UpdateData(FALSE);


}
void CDistortion_Option::OnNMDblclkListAngle(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	if(((CImageTesterDlg *)m_pMomWnd)->EVMS_Mode == TRUE)
	{
		return;
	}

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
		//ChangeCheck=1;
	if(pNMITEM->iSubItem == 0 || pNMITEM->iSubItem == -1 || pNMITEM->iItem == -1 )
		return ;
		iSavedItem = pNMITEM->iItem;
		iSavedSubitem = pNMITEM->iSubItem;
	

	if(pNMITEM->iItem != -1)
	{
		if(pNMITEM->iSubItem != 0)
		{		
			A_DATALIST.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			A_DATALIST.ClientToScreen(rect);
			this->ScreenToClient(rect);

			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText(A_DATALIST.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
			((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetFocus();
			
		}
	}
	//List_COLOR_Change();
	
	*pResult = 0;
}
void CDistortion_Option::List_COLOR_Change(){
	bool FLAG = TRUE;
	int Check=0;


	for(int t=0;t<6; t++){
		if(ChangeItem[t]==0)Check++;
		if(ChangeItem[t]==1)Check++;
		if(ChangeItem[t]==2)Check++;
	}
 	if(Check !=3){
		FLAG =FALSE;
		ChangeItem[changecount]=iSavedItem;
	}

	if(!FLAG){
		for(int t=0; t<changecount+1; t++){
			for(int k=0; k<changecount+1; k++){
				
				if(ChangeItem[t] == ChangeItem[k]){
					if(t==k){break;	}			
					ChangeItem[k] =-1;
				}
			
			
			}
		}
		//----------------데이터 정렬
		int temp=0;

		for(int i=0; i<3; i++)
		{
			for(int j=i+1; j<3; j++)
			{
				if(ChangeItem[i] < ChangeItem[j])  // 내림차순
				{
					temp = ChangeItem[i];
					ChangeItem[i] = ChangeItem[j];
					ChangeItem[j] = temp;
				}
			}
		}
		//--------------------------------
		for(int t=0; t<6; t++){

			if(ChangeItem[t] ==-1){
				changecount =t;
				break;
			}
		}
	}
	for(int t=0; t<6; t++){
		A_DATALIST.Update(t);
	}

}

void CDistortion_Option::OnNMClickListAngle(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMITEM = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.


	*pResult = 0;
}

void CDistortion_Option::Change_DATA(){

		CString str;

		//int Cam_Width = CAM_IMAGE_WIDTH;
		//int Cam_Height = CAM_IMAGE_HEIGHT;

		((CEdit *)GetDlgItemText(IDC_EDIT_A_MOD, str));
		A_DATALIST.SetItemText(iSavedItem, iSavedSubitem, str);

		int num = _ttoi(str);
		if(num < 0){
			num =0;
			
		}

		if(iSavedSubitem ==1){
			A_RECT[iSavedItem].m_PosX = num;
			
		}
		if(iSavedSubitem ==2){
			A_RECT[iSavedItem].m_PosY = num;
			
		}
		if(iSavedSubitem ==3){
			A_RECT[iSavedItem].m_Left = num;
			A_RECT[iSavedItem].m_PosX=(A_RECT[iSavedItem].m_Right+1 +A_RECT[iSavedItem].m_Left) /2;
			A_RECT[iSavedItem].m_Width=A_RECT[iSavedItem].m_Right+1 -A_RECT[iSavedItem].m_Left;

		}
		if(iSavedSubitem ==4){
			A_RECT[iSavedItem].m_Top = num;
			A_RECT[iSavedItem].m_PosY=(A_RECT[iSavedItem].m_Top +A_RECT[iSavedItem].m_Bottom+1)/2;
			A_RECT[iSavedItem].m_Height=A_RECT[iSavedItem].m_Bottom+1 -A_RECT[iSavedItem].m_Top;

		}
		if(iSavedSubitem ==5){
			if(num ==0){
				A_RECT[iSavedItem].m_Right =0;
			}
			else{
				A_RECT[iSavedItem].m_Right = num;
			}
			A_RECT[iSavedItem].m_PosX=(A_RECT[iSavedItem].m_Right +A_RECT[iSavedItem].m_Left+1) /2;
			A_RECT[iSavedItem].m_Width=A_RECT[iSavedItem].m_Right+1 -A_RECT[iSavedItem].m_Left;
		}
		if(iSavedSubitem ==6){
			if(num ==0){
				A_RECT[iSavedItem].m_Bottom =0;
			}
			else{
				A_RECT[iSavedItem].m_Bottom = num;
			}
			A_RECT[iSavedItem].m_PosY=(A_RECT[iSavedItem].m_Top +A_RECT[iSavedItem].m_Bottom+1)/2;
			A_RECT[iSavedItem].m_Height=A_RECT[iSavedItem].m_Bottom+1 -A_RECT[iSavedItem].m_Top;
		}
		if(iSavedSubitem ==7){
			A_RECT[iSavedItem].m_Width = num;
		}
		if(iSavedSubitem ==8){
			A_RECT[iSavedItem].m_Height = num;
		}
		A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
			

		if(A_RECT[iSavedItem].m_Left < 0){//////////////////////수정
			A_RECT[iSavedItem].m_Left = 0;
			A_RECT[iSavedItem].m_Width = A_RECT[iSavedItem].m_Right+1 -A_RECT[iSavedItem].m_Left;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if(A_RECT[iSavedItem].m_Right >m_CAM_SIZE_WIDTH){
			A_RECT[iSavedItem].m_Right = m_CAM_SIZE_WIDTH;
			A_RECT[iSavedItem].m_Width = A_RECT[iSavedItem].m_Right+1 -A_RECT[iSavedItem].m_Left;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if(A_RECT[iSavedItem].m_Top< 0){
			A_RECT[iSavedItem].m_Top = 0;
			A_RECT[iSavedItem].m_Height = A_RECT[iSavedItem].m_Bottom+1 -A_RECT[iSavedItem].m_Top;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if(A_RECT[iSavedItem].m_Height >m_CAM_SIZE_HEIGHT){
			A_RECT[iSavedItem].m_Height = m_CAM_SIZE_HEIGHT;
			A_RECT[iSavedItem].m_Width = A_RECT[iSavedItem].m_Bottom+1 -A_RECT[iSavedItem].m_Top;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}

		if(A_RECT[iSavedItem].m_Height<= 0){
			A_RECT[iSavedItem].m_Height = 1;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if(A_RECT[iSavedItem].m_Height >=m_CAM_SIZE_HEIGHT){
			A_RECT[iSavedItem].m_Height = m_CAM_SIZE_HEIGHT;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}

		if(A_RECT[iSavedItem].m_Width <= 0){
			A_RECT[iSavedItem].m_Width = 1;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}
		if(A_RECT[iSavedItem].m_Width >=m_CAM_SIZE_WIDTH){
			A_RECT[iSavedItem].m_Width = m_CAM_SIZE_WIDTH;
			A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		}



		A_RECT[iSavedItem].EX_RECT_SET(A_RECT[iSavedItem].m_PosX,A_RECT[iSavedItem].m_PosY,A_RECT[iSavedItem].m_Width,A_RECT[iSavedItem].m_Height);
		

		CString data = "";

		if(iSavedSubitem ==1){
			data.Format("%d",A_RECT[iSavedItem].m_PosX);			
		}
		else if(iSavedSubitem ==2){
			data.Format("%d",A_RECT[iSavedItem].m_PosY);
		}
		else if(iSavedSubitem ==3){
			data.Format("%d",A_RECT[iSavedItem].m_Left);			
		}
		else if(iSavedSubitem ==4){
			data.Format("%d",A_RECT[iSavedItem].m_Top);			
		}
		else if(iSavedSubitem ==5){
			data.Format("%d",A_RECT[iSavedItem].m_Right);			
		}
		else if(iSavedSubitem ==6){
			data.Format("%d",A_RECT[iSavedItem].m_Bottom);			
		}
		else if(iSavedSubitem ==7){
			data.Format("%d",A_RECT[iSavedItem].m_Width);			
		}
		else if(iSavedSubitem ==8){
			data.Format("%d",A_RECT[iSavedItem].m_Height);			
		}		
		
		
		((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText(data);

}

bool CDistortion_Option::Change_DATA_CHECK(bool FLAG){
 
	BOOL STAT = TRUE;

	//int Cam_Width = CAM_IMAGE_WIDTH;
	//int Cam_Height = CAM_IMAGE_HEIGHT;

	//상한
	if(A_RECT[iSavedItem].m_Width > m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Height > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_PosX > m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_PosY > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Top > m_CAM_SIZE_HEIGHT )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Bottom > m_CAM_SIZE_HEIGHT  )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Left> m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Right> m_CAM_SIZE_WIDTH )
		STAT = FALSE;
	
	//하한

	if(A_RECT[iSavedItem].m_Width < 1 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Height < 1 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_PosX < 0 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_PosY < 0 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Top < 0 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Bottom < 0  )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Left< 0 )
		STAT = FALSE;
	if(A_RECT[iSavedItem].m_Right< 0 )
		STAT = FALSE;
	

	if(FLAG  == 1){
		if(A_RECT[iSavedItem].m_Width == 1 )
			STAT = FALSE;
		if(A_RECT[iSavedItem].m_Height == 1 )
			STAT = FALSE;
		
	}

	return STAT;
	
	

}

BOOL CDistortion_Option::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN){
		if(pMsg->wParam == VK_RETURN){
			if(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->GetSafeHwnd())//GetSafeHwnd() 자신의 핸들을 가져오는 함수
			{
				//ChangeCheck =1;
				int bufSubitem = iSavedSubitem + 1;
				
				if(bufSubitem ==11){
					if(iSavedItem == 0){
						iSavedItem = 1; bufSubitem =1;
					}
					else if(iSavedItem == 1){
						iSavedItem = 2; bufSubitem =1;
					}
					else if(iSavedItem == 2){
						iSavedItem = 0; bufSubitem =1;
					}
				}
					
				int bufItem = iSavedItem;
				A_DATALIST.EnsureVisible(iSavedItem, FALSE);
				A_DATALIST.GetSubItemRect(iSavedItem,bufSubitem , LVIR_BOUNDS, rect);
				A_DATALIST.ClientToScreen(rect);
				this->ScreenToClient(rect);
				A_DATALIST.SetSelectionMark(iSavedItem);
				A_DATALIST.SetFocus(); //저장
				iSavedItem = bufItem;
				iSavedSubitem = bufSubitem;
				((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText(A_DATALIST.GetItemText(iSavedItem, iSavedSubitem));
				((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW );
				((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetFocus();
			}
			
			if ((pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_PosX))->GetSafeHwnd())||  
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_PosY))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_Dis_W))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_Dis_H))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_Width))->GetSafeHwnd())||
				(pMsg->hwnd == ((CEdit *)GetDlgItem(IDC_Angle_Height))->GetSafeHwnd()))
			{	
				OnBnClickedButtonsetanglezone();
			}

			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE){
			return TRUE;
		}

	}

	return CDialog::PreTranslateMessage(pMsg);
}


//-------------------------------------기존----------------------------------
// bool CDistortion_Option::AngleGen(LPBYTE IN_RGB,int NUM){
// 
// 	int	dwk = 0,dwi = 0;
// 		DWORD	RGBPIX = 0,RGBLINE = 0;
// 		DWORD 	SumX = 0,SumY = 0;
// 		DWORD	N1 = 0;
// 		BYTE *BW;
// 		DWORD Total = A_RECT[NUM].Height()*A_RECT[NUM].Length();
// 		BW = new BYTE[Total];
// 		memset(BW,0,sizeof(BW));
// 			int index=0;
// 		A_RECT[NUM].m_Success =FALSE;
// 
// 		int offsetX1 = A_RECT[NUM].m_Left;
// 		int offsetX2 = A_RECT[NUM].m_Right+1;
// 		int offsetY1 = A_RECT[NUM].m_Top;
// 		int offsetY2 = A_RECT[NUM].m_Bottom+1;
// 
// 		unsigned int PostionX[15000]={0,};
// 		unsigned int PostionY[15000]={0,};
// 
// 		//if(m_EN_RUN != 1){
// 		//	m_Success = FALSE;
// 		//	return	FALSE;
// 		//}
// 		
// 		for (dwk=0; dwk<m_CAM_SIZE_HEIGHT; dwk++)
// 		{	
// 			for (dwi=0; dwi<m_CAM_SIZE_WIDTH; dwi++)
// 			{
// 				m_InB = IN_RGB[RGBLINE + RGBPIX];
// 				m_InG = IN_RGB[RGBLINE + RGBPIX + 1];
// 				m_InR = IN_RGB[RGBLINE + RGBPIX + 2];
// 				
// 				
// 
// 				if(dwi>offsetX1 && dwi<offsetX2){
// 					if(dwk >offsetY1 &&dwk<offsetY2){
// 					//	BW[index] = (0.29900*m_InB)+(0.58700*m_InG)+(0.11400*m_InR);
// 						
// 						if(NUM !=0){
// 							if(m_InR > m_InB + m_InG)
// 								BW[index] = 0;
// 							else
// 								BW[index] = 255;
// 
// 							if(BW[index]<50 ){
// 							//if(m_InR<50 && m_InG<50 && m_InB<50 ){ //&& RED1
// 								PostionX[N1]= dwi; PostionY[N1]=dwk;
// 								SumX += (double)PostionX[N1]; SumY += (double)PostionY[N1];
// 								N1 ++;
// 							}
// 							index++;
// 						}
// 						else{
// 							if(m_InR <70 && m_InB<70 && m_InG <70){
// 								BW[index] = 0;
// 							}
// 							else{
// 								BW[index] = 255;
// 							}
// 							if(BW[index]<50 ){
// 							//if(m_InR<50 && m_InG<50 && m_InB<50 ){ //&& RED1
// 								PostionX[N1]= dwi; PostionY[N1]=dwk;
// 								SumX += (double)PostionX[N1]; SumY += (double)PostionY[N1];
// 								N1 ++;
// 							}
// 							index++;
// 
// 						}
// 					}
// 				}
// 				RGBPIX += 4;
// 			}
// 			RGBLINE +=  (m_CAM_SIZE_WIDTH*4);
// 			RGBPIX = 0;
// 		}
// 		if(N1 > 5){
// 
// 		A_RECT[NUM].m_resultX = (int)((SumX/N1)+0.5);//반올림
// 		A_RECT[NUM].m_resultY = (int)((SumY/N1)+0.5);//반올림
// 		}
// 
// 		delete BW;
// 	return A_RECT[NUM].m_Success;
// 
// }
//---------------------------------------------------------------------------

//-------------------------------------신규----------------------------------
bool CDistortion_Option::AngleGen(LPBYTE IN_RGB, int NUM){


	IplImage *OriginImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(A_RECT[NUM].m_Width, A_RECT[NUM].m_Height), IPL_DEPTH_8U, 3);

	int Curr_Center_X = -99999;
	int Curr_Center_Y = -99999;

	A_RECT[NUM].m_resultX = -1;
	A_RECT[NUM].m_resultY = -1;

	BYTE R, G, B;
	double Sum_Y;

	BOOL detect = FALSE;

	cvSetZero(OriginImage);

	for (int y = A_RECT[NUM].m_Top; y<A_RECT[NUM].m_Bottom + 1; y++)
	{
		for (int x = A_RECT[NUM].m_Left; x<A_RECT[NUM].m_Right + 1; x++)
		{
			B = IN_RGB[y*m_CAM_SIZE_WIDTH * 3 + x * 3];
			G = IN_RGB[y*m_CAM_SIZE_WIDTH * 3 + x * 3 + 1];
			R = IN_RGB[y*m_CAM_SIZE_WIDTH * 3 + x * 3 + 2];

			Sum_Y = (BYTE)((0.29900*R) + (0.58700*G) + (0.11400*B));

			OriginImage->imageData[(y - (A_RECT[NUM].m_Top))*OriginImage->widthStep + (x - (A_RECT[NUM].m_Left))] = (char)Sum_Y;
		}
	}

	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 3, 1.0, 1.0);
	//	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);

	//cvSaveImage("D:\\OriginImage.jpg", OriginImage);

	//	cvSaveImage("D:\\SmoothImage.jpg", SmoothImage);

	//cvThreshold(SmoothImage, SmoothImage, 70, 255, CV_THRESH_BINARY);
	cvThreshold(SmoothImage, SmoothImage, 100, 255, CV_THRESH_OTSU);
	
	if (NUM != 2 && NUM != 3)// Center 아닌것만 반전
		cvNot(SmoothImage, SmoothImage);


	//	cvCanny(SmoothImage, CannyImage, 0, 50);

	cvDilate(SmoothImage, DilateImage);

	cvSaveImage("D:\\DilateImage.jpg", DilateImage);

	cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;

	int counter = 0;

	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
		counter++;

	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area = 0, arcCount = 0;
	counter = 0;
	double old_dist = 999999;

	for (; contour != 0; contour = contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;
		double circularity = (4.0*3.14*area) / (arcCount*arcCount);

		if(circularity > 0.7)
		{
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width / 2;
			center_y = rect.y + rect.height / 2;

			//if(center_x > (CAM_IMAGE_WIDTH/2) - (CAM_IMAGE_WIDTH/8) && center_x < (CAM_IMAGE_WIDTH/2) + (CAM_IMAGE_WIDTH/8) && center_y > (CAM_IMAGE_HEIGHT/2) - (CAM_IMAGE_HEIGHT/6) && center_y < (CAM_IMAGE_HEIGHT/2) + (CAM_IMAGE_HEIGHT/6))
			//{
			//	if(rect.width > 50 && rect.height > 50)
			{
				if (rect.width < A_RECT[NUM].m_Width && rect.height < A_RECT[NUM].m_Height)
				{
					
					//	//	double dist = GetDistance(center_x, center_y, (Standard_RD_RECT[0].m_Left+Standard_RD_RECT[0].m_Width/2), (Standard_RD_RECT[0].m_Top+Standard_RD_RECT[0].m_Height/2));
					//double dist = GetDistance(center_x, center_y, (OriginImage->width / 2), (OriginImage->height / 2));
					double dist = GetDistance(center_x, center_y, (A_RECT[NUM].m_Width / 2), (A_RECT[NUM].m_Height / 2));
					//if (dist != 0)
					{
						if (dist < old_dist)
						{
							if (NUM == 2){
								Curr_Center_X = rect.x + rect.width / 2;
								Curr_Center_Y = rect.y;
							}
							else if (NUM == 3){
								Curr_Center_X = rect.x + rect.width / 2;
								Curr_Center_Y = rect.y + rect.height;
							}else{
								Curr_Center_X = rect.x + rect.width / 2;
								Curr_Center_Y = rect.y + rect.height / 2;
							}

							old_dist = dist;

							detect = TRUE;
							cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(255,0, 0), 1, 8);
						}
					}
				}
			}
			cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 0, 255), 1, 8);
		}

		counter++;
	}

	if ((Curr_Center_X == -99999) || (Curr_Center_Y == -99999)){
		Curr_Center_X = -1;
		Curr_Center_Y = -1;

	}
	else{
		Curr_Center_X = Curr_Center_X + A_RECT[NUM].m_Left;
		Curr_Center_Y = Curr_Center_Y + A_RECT[NUM].m_Top;
	}


	A_RECT[NUM].m_resultX = Curr_Center_X;
	A_RECT[NUM].m_resultY = Curr_Center_Y;

	cvSaveImage("D:\\RGBResultImage.jpg", RGBResultImage);

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&CannyImage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);

	delete[]rectArray;
	delete[]areaArray;

	return detect;
}

//---------------------------------------------------------------------------

void CDistortion_Option::AngleSum(){//AVE값 및 평균 RGB값 추출 및 그리기.
	A_RECT[0].m_resultDis=0;
	A_RECT[0].m_Degree=0;

	
	if(MasterMod == TRUE){////////////////////////마스터 일경우
		A_RECT[1].m_resultDis=(sqrt(pow(A_RECT[0].m_resultX-A_RECT[1].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[1].m_resultY,2)));


		A_RECT[2].m_resultDis=(sqrt(pow(A_RECT[0].m_resultX-A_RECT[2].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[2].m_resultY,2)));

		for(int t=0; t<2; t++){
			A_RECT[t+1].m_resultDegree = Distortion_Sum(A_RECT[t+1].m_resultDis);	/////////거리가지고 Distortion 구함		
		}


		d_Distortion_Master = (A_RECT[1].m_resultDegree + A_RECT[2].m_resultDegree) /2.0;

		str_Distortion.Format("%6.4f",d_Distortion_Master);

		UpdateData(FALSE);

	}
	else{
		A_RECT[1].m_resultDis=(sqrt(pow(A_RECT[0].m_resultX-A_RECT[1].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[1].m_resultY,2)));
		A_RECT[2].m_resultDis=(sqrt(pow(A_RECT[0].m_resultX-A_RECT[2].m_resultX,2)+pow(A_RECT[0].m_resultY-A_RECT[2].m_resultY,2)));
	
		for(int t=0; t<2; t++){
			A_RECT[t+1].m_Degree = Distortion_Sum(A_RECT[t+1].m_resultDis);				/////////거리가지고 Distortion 구함		
			
		}	

		d_Distortion = (A_RECT[1].m_Degree + A_RECT[2].m_Degree) /2.0;


		m_Comp_MAX = d_Distortion_Master +d_Dev_Distortion;

		m_Comp_MIN= d_Distortion_Master -d_Dev_Distortion;
	

		if(((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

			CString str="";
				str.Empty();
				str.Format("%6.4f",d_Distortion);
				m_AngleList.SetItemText(InsertIndex,4,str);
			
		}
		for(int NUM =0; NUM<3; NUM++){			
			
			if(d_Distortion>= m_Comp_MIN&&d_Distortion <=m_Comp_MAX){
					A_RECT[NUM].m_Success = TRUE;
				
			
			}else{
					A_RECT[NUM].m_Success = FALSE;
					
			}
			A_RECT[0].m_Success = TRUE;
		}
		

	}
	
}
double CDistortion_Option::Degree_Sum(int Dis){

	double Sum=0;
	Sum = atan((d_Campixel*(double)Dis/1000.0)/d_Focallength)*180.0/3.141592; 

	//Sum = atan2((d_Campixel*(double)Dis/1000),d_Focallength)*180.0/3.141592;
	return Sum;
}

double CDistortion_Option::Distortion_Sum(int PixDis){

	double Distance =( (double)i_ChartDis * d_Focallength )/ (double)i_MotorDis;

	double Sum=0;
	Sum= (d_Campixel*(double)PixDis/1000.0)  / Distance *100.0;
		

	return Sum;
}

/*

int CDistortion_Option::Distance_Sum(int x1, int y1, int x2, int y2){

	int Sum=0;
	int X_result=0;
	int Y_result=0;
	X_result =int(sqrt(pow(x2-x,2)+pow(A_RECT[0].m_resultX-A_RECT[1].m_resultX,2)));

	return Sum;
}
*/
bool CDistortion_Option::AnglePic(CDC *cdc,int NUM){//AVE값 및 평균 RGB값 추출 및 그리기.
	
	CPen	my_Pan,my_Pan2,*old_pan,*old_pan2;
	CString RESULTDATA ="";
	CString TEXTDATA ="";
	int Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	int Cam_PosY = (m_CAM_SIZE_HEIGHT/2);
	  CFont m_font;

	//if(A_RECT[NUM].Chkdata() == FALSE){return 0;}

	//if(b_EtcModel_TEST ==1){
	//	CFont m_font;
	//	m_font.CreatePointFont(300,"Arial");
	//	cdc->SelectObject(&m_font);
	//	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);


	//	TEXTDATA.Format("TESTING..");
	//	cdc->TextOut(m_CAM_SIZE_WIDTH-150, m_CAM_SIZE_HEIGHT-30,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	//	m_font.DeleteObject();
	//	cdc->DeleteObject(m_font);

	//}

	::SetBkMode(cdc->m_hDC,TRANSPARENT);
	if(A_RECT[NUM].m_Success == TRUE){
		my_Pan.CreatePen(PS_SOLID,2,BLUE_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(BLUE_COLOR);
	}else{
		my_Pan.CreatePen(PS_SOLID,2,RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
		cdc->SetTextColor(RED_COLOR);
	}

	cdc->MoveTo(A_RECT[NUM].m_Left,A_RECT[NUM].m_Top);
	cdc->LineTo(A_RECT[NUM].m_Right,A_RECT[NUM].m_Top);
	cdc->LineTo(A_RECT[NUM].m_Right,A_RECT[NUM].m_Bottom);
	cdc->LineTo(A_RECT[NUM].m_Left,A_RECT[NUM].m_Bottom);
	cdc->LineTo(A_RECT[NUM].m_Left,A_RECT[NUM].m_Top);


	my_Pan2.CreatePen(PS_SOLID,2,WHITE_COLOR);
	old_pan2 = cdc->SelectObject(&my_Pan2);
	cdc->MoveTo(int(A_RECT[NUM].m_resultX)-5,int(A_RECT[NUM].m_resultY));
	cdc->LineTo(int(A_RECT[NUM].m_resultX)+5,int(A_RECT[NUM].m_resultY));
	cdc->MoveTo(int(A_RECT[NUM].m_resultX),int(A_RECT[NUM].m_resultY)-5);
	cdc->LineTo(int(A_RECT[NUM].m_resultX),int(A_RECT[NUM].m_resultY)+5);
	

	m_font.CreatePointFont(150,"Arial");
	cdc->SelectObject(&m_font);
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);


	///if(MasterMod == TRUE){
	//	TEXTDATA.Format("R:%03d G:%03d B:%03d",A_RECT[NUM].m_R,A_RECT[NUM].m_G,A_RECT[NUM].m_B);
//	}
//	else{
	TEXTDATA.Format("X:%6.2f Y:%6.2f",A_RECT[NUM].m_resultX,A_RECT[NUM].m_resultY);	
//	}
	cdc->TextOut(A_RECT[NUM].m_PosX,A_RECT[NUM].m_Top - 20,TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());

	
	
	

	
	cdc->SelectObject(old_pan);
	cdc->SelectObject(old_pan2);


	

	m_font.DeleteObject();
	m_font.CreatePointFont(300,"Arial");
	cdc->SelectObject(&m_font);
	cdc->SetTextAlign(TA_CENTER|TA_BASELINE);
	
	::SetTextColor(cdc->m_hDC,RGB(255, 255, 0)); 
	if(TestMod == FALSE){
		TEXTDATA.Format("Master_ %6.4f",d_Distortion_Master);

		cdc->TextOut(Cam_PosX,(Cam_PosY+200),TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
	}
	else{
		TEXTDATA.Format("%6.1f %%",d_Distortion);

		cdc->TextOut(Cam_PosX,(Cam_PosY+200),TEXTDATA.GetBuffer(0),TEXTDATA.GetLength());
		
	}
	cdc->SelectObject(old_pan);
	


	old_pan->DeleteObject();
	m_font.DeleteObject();
	my_Pan.DeleteObject();
	my_Pan2.DeleteObject();

	return TRUE;
}
void CDistortion_Option::OnBnClickedButtonMasterA()
{
// 	ChangeCheck=1;
// 
// 	TestMod =FALSE;
// 	 
// 	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
// 	
// 	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	
// 
// 	MasterMod =TRUE;
// 	BOOL FLAG = FALSE;
// 			
// 	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan =1;						
// 			
// 	for(int i =0;i<10;i++){
// 		if(((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0){
// 			break;
// 		}else{
// 			DoEvents(50);
// 		}
// 	}
// 
// 	if(AutomationMod ==1){
// 	// 오토메이션 모드
// 	//C_CHECKING[] PosX,PosY 받은 거 체킹	
// 
// 		//Automation_DATA_INPUT();// 성공하면 데이터 C_RECT에 넣기.
// 		//실패하면 그냥 진행...
// 		//Load_parameter();// 실패한 경우
// 		GetSideCircleCoordinate(m_RGBScanbuf);
// 
// 		FLAG = TRUE;
// 		for(int lop=0; lop<6; lop++)
// 		{
// 			C_CHECKING[lop].m_PosX = SC_RECT[lop].m_resultX;
// 			C_CHECKING[lop].m_PosY = SC_RECT[lop].m_resultY;					
// 			C_CHECKING[lop].m_Success = SC_RECT[lop].m_Success;					
// 
// 			/*if(!C_CHECKING[lop].m_Success)
// 			{
// 				FLAG = FALSE;
// 				break;
// 			}*/
// 		}
// 
// 		if(C_CHECKING[0].m_Success == FALSE || C_CHECKING[1].m_Success == FALSE || C_CHECKING[2].m_Success == FALSE)
// 			FLAG = FALSE;
// 		
// 		if(FLAG){
// 			Automation_DATA_INPUT();
// 		}else{Load_parameter();}
// 
// 				
// 		
// 	}/*else{
// 		Load_Original_pra();
// 	}*/
// 
// 	
// 	if(!FLAG){
// 		for(int lop=0; lop<6; lop++){
// 			
// 				AngleGen(m_RGBScanbuf,lop);
// 			
// 			
// 		}
// 	}
// 	else{
// 		for(int t=0; t<6; t++){
// 			A_RECT[t].m_resultX=C_CHECKING[t].m_PosX;
// 			A_RECT[t].m_resultY=C_CHECKING[t].m_PosY;
// 		}
// 	}
// 	AngleSum();
// 
// 	UploadList();
// 
// 	MasterMod =FALSE;
// 
// 	for(int t=0; t<6; t++){
// 		ChangeItem[t]=t;
// 	}
// 	for(int t=0; t<6; t++){
// 		A_DATALIST.Update(t);
// 	}
// 	((CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE))->EnableWindow(1);
}
void CDistortion_Option::Load_Original_pra(){

	for(int t=0; t<6; t++){
		A_RECT[t].m_PosX=A_Original[t].m_PosX;
		A_RECT[t].m_PosY=A_Original[t].m_PosY;
		A_RECT[t].EX_RECT_SET(A_Original[t].m_PosX,A_Original[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);//////C
	}

}

tResultVal CDistortion_Option::Run()
{	
	CString str="";
	TestMod =TRUE;
	b_StopFail = FALSE;
	if(ChangeCheck == TRUE){
		OnBnClickedButtonARectSave();
	}	
	
	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){

		InsertList();
	}

	int camcnt = 0;
	int count=0;


	int Resultcount=0;
	tResultVal retval = {0,};

	m_retval.Reset();

	BOOL FLAG = FALSE;
	int loop_cnt=0;

	count = 0;
	Resultcount = 0;
	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan = 1;
	((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray = 1;
	for (int i = 0; i < 50; i++)
	{
		DoEvents(10);
		if (((CImageTesterDlg *)m_pMomWnd)->m_ImgScan == 0 && ((CImageTesterDlg *)m_pMomWnd)->m_ImgScan_Gray == 0)
		{
			break;
		}
	}

	CString stateDATA = "Distortion_ ";

//------------------------------------신규20170810----------------------------------------------------------------
	BOOL detect_Cva[6];
	for (int i = 0; i < 6; i++)
	{
		detect_Cva[i] = FALSE;
	}

	for (int lop = 0; lop < 6; lop++){
		detect_Cva[lop] = AngleGen(m_RGBScanbuf, lop);
	}


	double A1 = GetDistance(A_RECT[0].m_resultX, A_RECT[0].m_resultY, A_RECT[1].m_resultX, A_RECT[1].m_resultY);
	double A2 = GetDistance(A_RECT[4].m_resultX, A_RECT[4].m_resultY, A_RECT[5].m_resultX, A_RECT[5].m_resultY);
	double A = (A1 + A2) / 2.0;
	double B = GetDistance(A_RECT[2].m_resultX, A_RECT[2].m_resultY, A_RECT[3].m_resultX, A_RECT[3].m_resultY) * 1.0;

	d_Distortion = (100.0 * (A - B) / B) * m_dOffset;
//---------------------------------------------------------------------------------------------------------------------

 	str.Empty();
 	str.Format("%6.4f %%", d_Distortion);
 	((CImageTesterDlg *)m_pMomWnd)->m_pResDistortionOptWnd->LC_VALUE(str);
 	 
 	 
 	stateDATA += str ;

	retval.m_ID = 0x0d;
	retval.ValString.Empty();
	
//------------------------------------신규20170810----------------------------------------------------------------
	if (detect_Cva[0] == TRUE && detect_Cva[1] == TRUE && detect_Cva[2] == TRUE
		&& detect_Cva[3] == TRUE && detect_Cva[4] == TRUE && detect_Cva[5] == TRUE)
	{
//		if (abs(d_Distortion) < d_Dev_Distortion)

		double dbData = abs(d_Distortion);
		double dbAbs_Thd = abs(d_Dev_Distortion);
		if (dbData + m_dDistortionScale >= dbAbs_Thd &&
			dbData - m_dDistortionScale <= dbAbs_Thd)
		{
			for (int lop = 0; lop < 6; lop++)
			{
				A_RECT[lop].m_Success = TRUE;
			}

			retval.m_Success = TRUE;
			((CImageTesterDlg *)m_pMomWnd)->m_pResDistortionOptWnd->LC_RESULT(1, "PASS");
			retval.ValString.Format("%6.4f 성공", d_Distortion);
			stateDATA += "_ PASS";

// 			if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 				str.Format("%6.4f", d_Distortion);
// 				m_Lot_DistortionList.SetItemText(Lot_InsertIndex, 5, str);
// 				m_Lot_DistortionList.SetItemText(Lot_InsertIndex, 6, "PASS");
// 			}
// 			else{
			if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
				((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_DISTORTION, "PASS");
				str.Format("%6.4f", d_Distortion);
				m_AngleList.SetItemText(InsertIndex, 4, str);
				m_AngleList.SetItemText(InsertIndex, 5, "PASS");
			}
		}
		else{
			for (int lop = 0; lop < 6; lop++){
				A_RECT[lop].m_Success = FALSE;
			}
			((CImageTesterDlg *)m_pMomWnd)->m_pResDistortionOptWnd->LC_RESULT(2, "FAIL");

			stateDATA += "_ FAIL";

			retval.ValString.Format("%6.4f 실패", d_Distortion);
			retval.m_Success = FALSE;

// 			if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 				str.Format("%6.4f", d_Distortion);
// 				m_Lot_DistortionList.SetItemText(Lot_InsertIndex, 5, str);
// 				m_Lot_DistortionList.SetItemText(Lot_InsertIndex, 6, "FAIL");
// 			}
// 			else{
			if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
				((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_DISTORTION, "FAIL");
				str.Format("%6.4f", d_Distortion);
				m_AngleList.SetItemText(InsertIndex, 4, str);
				m_AngleList.SetItemText(InsertIndex, 5, "FAIL");
			}
		}
	}
	else
	{
		((CImageTesterDlg *)m_pMomWnd)->m_pResDistortionOptWnd->LC_RESULT(2, "FAIL");

		stateDATA += "_ FAIL";

		retval.ValString.Format("검출 실패");
		retval.m_Success = FALSE;

// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 			str.Format("%6.4f", d_Distortion);
// 			m_Lot_DistortionList.SetItemText(Lot_InsertIndex, 5, str);
// 			m_Lot_DistortionList.SetItemText(Lot_InsertIndex, 6, "FAIL");
// 		}
// 		else{
		if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
			((CImageTesterDlg *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg *)m_pMomWnd)->InsertIndex, WORKLIST_DISTORTION, "FAIL");
			str.Format("%6.4f", d_Distortion);
			m_AngleList.SetItemText(InsertIndex, 4, str);
			m_AngleList.SetItemText(InsertIndex, 5, "FAIL");
		}
	}

//---------------------------------------------------------------------------------------------------------------------
	((CImageTesterDlg *)m_pMomWnd)->StatePrintf(stateDATA);
// 	if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 		Lot_StartCnt++;
// 	}
// 	else{
	if (((CImageTesterDlg *)m_pMomWnd)->b_StartCommand == TRUE){
		StartCnt++;
	}

	m_retval = retval;
	return retval;
}	



void CDistortion_Option::FAIL_UPLOAD()
{
//	if (((CImageTesterDlg  *)m_pMomWnd)->LotMod == 1)
//	{
// 		if (((CImageTesterDlg *)m_pMomWnd)->LotMod == 1){
// 			LOT_InsertDataList();
// 			m_AngleList.SetItemText(InsertIndex, 5, "X");
// 			m_AngleList.SetItemText(InsertIndex, 6, "FAIL");
// 			m_AngleList.SetItemText(InsertIndex, 7, "STOP");
// 			b_StopFail = TRUE;
// 			Lot_StartCnt++;
// 		}
// 		else{
			InsertList();

			m_AngleList.SetItemText(InsertIndex, 4, "X");
			m_AngleList.SetItemText(InsertIndex, 5, "FAIL");
			m_AngleList.SetItemText(InsertIndex, 6, "STOP");
			((CImageTesterDlg  *)m_pMomWnd)->MainWork_List.SetItemText(((CImageTesterDlg  *)m_pMomWnd)->InsertIndex, WORKLIST_DISTORTION, "STOP");
			StartCnt++;
			b_StopFail = TRUE;
//		}
//	}
}
void CDistortion_Option::Pic(CDC *cdc)
{
	for(int lop=0;lop<6;lop++){
		AnglePic(cdc,lop);
	}
}  

void CDistortion_Option::InitPrm()
{
	Load_parameter();
	UpdateData(FALSE);

	UploadList();

	for(int t=0; t<6;t++){
		A_RECT[t].m_Success = FALSE;
		A_RECT[t].m_resultX =0;
		A_RECT[t].m_resultY =0;
		A_RECT[t].m_Degree =0;
		A_RECT[t].m_resultDis =0;

	}
	d_Distortion= 0;
	disB = 0;
	disL =0;
	disR =0;
	disT =0;
}

void CDistortion_Option::Automation_DATA_INPUT(){

	for(int t=0; t<6; t++){
		A_RECT[t].m_PosX=C_CHECKING[t].m_PosX;
		A_RECT[t].m_PosY=C_CHECKING[t].m_PosY;
		A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);

		if(A_RECT[t].m_Left < 0){//////////////////////수정
			A_RECT[t].m_Left = 0;
			A_RECT[t].m_Width = A_RECT[t].m_Right+1 -A_RECT[t].m_Left;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		if(A_RECT[t].m_Right >m_CAM_SIZE_WIDTH){
			A_RECT[t].m_Right = m_CAM_SIZE_WIDTH;
			A_RECT[t].m_Width = A_RECT[t].m_Right+1 -A_RECT[t].m_Left;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		if(A_RECT[t].m_Top< 0){
			A_RECT[t].m_Top = 0;
			A_RECT[t].m_Height = A_RECT[t].m_Bottom+1 -A_RECT[t].m_Top;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		if(A_RECT[t].m_Bottom >m_CAM_SIZE_HEIGHT){
			int gap = A_RECT[t].m_Bottom - m_CAM_SIZE_HEIGHT;
			A_RECT[t].m_Bottom = m_CAM_SIZE_HEIGHT;
			A_RECT[t].m_Height = A_RECT[t].m_Bottom -A_RECT[t].m_Top -gap;
			A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
		}
		A_RECT[t].EX_RECT_SET(A_RECT[t].m_PosX,A_RECT[t].m_PosY,A_RECT[t].m_Width,A_RECT[t].m_Height);
	}

}

void CModel_Create(CDistortion_Option **pWnd,CTabCtrl *pTabWnd,CWnd* pMomWnd,CEdit	*pEdit,tINFO INFO){
	CRect OptionRect;
	if(pTabWnd != NULL){
		((CDistortion_Option *)*pWnd) = new CDistortion_Option(pMomWnd);
		((CDistortion_Option *)*pWnd)->Setup(pMomWnd,pEdit,INFO);
		((CDistortion_Option *)*pWnd)->Create(((CDistortion_Option *)*pWnd)->IDD,pTabWnd);//&m_CtrTab);
		((CDistortion_Option *)*pWnd)->GetWindowRect(OptionRect); //옵션창의 위치정보를 갱신한다.
		((CDistortion_Option *)*pWnd)->MoveWindow(20,40,OptionRect.Width(),OptionRect.Height());
	}
}

void CModel_Delete(CDistortion_Option **pWnd){
	if(((CDistortion_Option *)*pWnd) != NULL){
//		((CDistortion_Option *)*pWnd)->KillTimer(110);
		((CDistortion_Option *)*pWnd)->DestroyWindow();
		delete ((CDistortion_Option *)*pWnd);
		((CDistortion_Option *)*pWnd) = NULL;	
	}
}

void CDistortion_Option::StatePrintf(LPCSTR lpcszString, ...)
{	
	if(pTStat == NULL){return;}
	TCHAR			lpszBuf[256];
	UINT			bufLen;
	CString			cstr;
	
	ASSERT(AfxIsValidString(lpcszString, FALSE));
	va_list		args;	
	va_start(args, lpcszString);	
	wvsprintf(lpszBuf, lpcszString, args);	
	cstr.Empty();
	cstr.Format("%s\r\n",lpszBuf);
	va_end(args);
	
	TRACE(cstr);
	bufLen = pTStat ->GetWindowTextLength();

	int del_line = 0; 
	while (bufLen > MAX_LOG_LEN){
		int nLineIndex = pTStat ->LineIndex(1);
		if (nLineIndex == -1) break;//예외처리
		pTStat -> SetSel(0, nLineIndex);//두번째 라인의 앞부분을 선택한다.  
		pTStat -> Clear();			   //라인하나를 지운다
		bufLen = pTStat ->GetWindowTextLength();
		del_line++;
	}
	
	pTStat -> SetSel(-1,0);
	pTStat -> ReplaceSel(cstr);
	pTStat -> SetSel(-1,-1);
}

void CDistortion_Option::Save(int NUM){
	WritePrivateProfileString(str_model,NULL,"",A_filename);
	if(NUM != -1){
		str_model.Empty();
		str_model.Format("Model_%d",NUM);
		Save_parameter();
	}
}
//---------------------------------------------------------------------WORKLIST
void CDistortion_Option::InsertList(){
	CTime thetime = CTime::GetCurrentTime();
	CString strCnt,strStartMode;

	InsertIndex = m_AngleList.InsertItem(StartCnt,"",0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), InsertIndex+1);
	m_AngleList.SetItemText(InsertIndex,0,strCnt);
	
	m_AngleList.SetItemText(InsertIndex,1,((CImageTesterDlg *)m_pMomWnd)->m_modelName);
	m_AngleList.SetItemText(InsertIndex,2,((CImageTesterDlg *)m_pMomWnd)->strDay+"");
	m_AngleList.SetItemText(InsertIndex,3,((CImageTesterDlg *)m_pMomWnd)->strTime+"");

}
void CDistortion_Option::Set_List(CListCtrl *List){
	

	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();
	
	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start TIME",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"왜곡률",LVCFMT_CENTER, 80);
	List->InsertColumn(5,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"비고",LVCFMT_CENTER, 80);
	//List->InsertColumn(12,"Serial",LVCFMT_CENTER, 80);
	ListItemNum=7;
	Copy_List(&m_AngleList,((CImageTesterDlg *)m_pMomWnd)->m_pWorkList,ListItemNum);
}

CString CDistortion_Option::Mes_Result()
{
	CString sz;
	int		nResult = 0;

	m_szMesResult.Empty();

	// 제일 마지막 항목을 추가하자
	int nSel = m_AngleList.GetItemCount()-1;

	// 왜곡률
	sz = m_AngleList.GetItemText(nSel, 4);

	if(m_retval.m_Success)
		nResult = 1;
	else 
		nResult = 0;

	if(b_StopFail == FALSE){
	m_szMesResult.Format(_T("%s:%d"), sz, nResult);
	}else{
		m_szMesResult.Format(_T(":1"));
	}

	((CImageTesterDlg  *)m_pMomWnd)->m_stNewMesData[NewMES_Distortion] = m_szMesResult;

	return m_szMesResult;
}

void CDistortion_Option::EXCEL_SAVE(){
	CString Item[1]={"왜곡률"};
	CString Data ="";
	CString str="";
	str = "***********************Distortion***********************\n";
	Data += str;
	str.Empty();
	str.Format("좌- 중점 , %6.3f º,Threshold, ± %6.3f º,, ",A_RECT[1].m_resultDegree,A_RECT[1].d_Thresold);/////////////////수정해야함.
	Data += str;
	str.Empty();
	str.Format("우- 중점 , %6.3f º,Threshold, ± %6.3f º, \n",A_RECT[2].m_resultDegree,A_RECT[2].d_Thresold);
	Data += str;
	str.Empty();
	str.Format("상- 중점 , %6.3f º,Threshold, ± %6.3f º, ,",A_RECT[3].m_resultDegree,A_RECT[3].d_Thresold);
	Data += str;
	str.Empty();
	str.Format("하- 중점 , %6.3f º ,Threshold, ± %6.3f º,\n ",A_RECT[4].m_resultDegree,A_RECT[4].d_Thresold);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->SAVE_EXCEL("Distortion",Item,1,&m_AngleList,Data);
}

bool CDistortion_Option::EXCEL_UPLOAD(){
	m_AngleList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->EXCEL_UPLOAD("Distortion",&m_AngleList,1,&StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}


void CDistortion_Option::OnBnClickedCheckAuto()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
		if(AutomationMod ==0){
		
		AutomationMod =1;
		//A_DATALIST.EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_PosX)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_PosY)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_Dis_W)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_Dis_H)->EnableWindow(0);
		/*(CEdit *)GetDlgItem(IDC_Angle_Width)->EnableWindow(0);
		
		(CEdit *)GetDlgItem(IDC_Angle_Height)->EnableWindow(0);
		(CEdit *)GetDlgItem(IDC_Angle_Thresold)->EnableWindow(0);
		*/
//		(CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE)->EnableWindow(0);
		//(CButton *)GetDlgItem(IDC_BUTTON_setAngleZone)->EnableWindow(0);
		
		//////////////////////////////////////////////////////////////////////오토메이션 중심점으로 영역 잡는 소스 추가해야함.
	}
	else{
		AutomationMod =0;
		
		//A_DATALIST.EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_PosX)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_PosY)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_Dis_W)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_Dis_H)->EnableWindow(1);
		/*(CEdit *)GetDlgItem(IDC_Angle_Width)->EnableWindow(1);
		
		(CEdit *)GetDlgItem(IDC_Angle_Height)->EnableWindow(1);
		(CEdit *)GetDlgItem(IDC_Angle_Thresold)->EnableWindow(1);
		*/
//		(CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE)->EnableWindow(1);
		//(CButton *)GetDlgItem(IDC_BUTTON_setAngleZone)->EnableWindow(1);
	
	}

		for(int t=0; t<6; t++){
			A_DATALIST.Update(t);
		}
	CString strTitle;
	strTitle.Empty();
	strTitle="DISTORTION_INIT";
	if(AutomationMod ==1){
		WritePrivateProfileString(str_model,strTitle+"Auto","1",A_filename);}
	if(AutomationMod ==0){
		WritePrivateProfileString(str_model,strTitle+"Auto","0",A_filename);}

}

void CDistortion_Option::GetSideCircleCoordinate(LPBYTE IN_RGB)
{
	//int Cam_Width = m_CAM_SIZE_WIDTH;
	//int Cam_Height = m_CAM_SIZE_HEIGHT;

	double Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	double Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	IplImage *OriginImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);	
	IplImage *SmoothImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3);	

	BYTE R,G,B;
	double Sum_Y;

	for (int y=0; y<m_CAM_SIZE_HEIGHT; y++)
	{
		for (int x=0; x<m_CAM_SIZE_WIDTH; x++)
		{
			B = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 2];
			
			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;			
		}
	}
	
//	Capture_Gray_SAVE("C:\\RGBIMAGE", IN_RGB, 720, 480);	
//	cvSaveImage("C:\\Default_Image.bmp", OriginImage);

	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0); //객체 외곽의 Overshooting으로 인하여 한번 더 처리 함..

	cvCanny(SmoothImage, CannyImage, 0, 255);
	
	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);
	
	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;
	CvRect rect;
	double area=0, arcCount=0;
	double old_dist = 999999;
	
	SC_RECT[0].m_Success = FALSE;

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
	{
		area = cvContourArea(temp_contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(temp_contour, CV_WHOLE_SEQ, -1);
		
		double circularity = (4.0*3.14*area)/(arcCount*arcCount);
		
		if(circularity > 0.5)
		{			
			rect = cvContourBoundingRect(temp_contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;
			
			//if(center_x > 270 && center_x < 450 && center_y > 160 && center_y < 320)
			if(center_x > (int)(Cam_PosX*0.75) && center_x < (int)(Cam_PosX*1.25) && center_y > (int)(Cam_PosY*0.625) && center_y < (int)(Cam_PosY*1.375))
			{
				if(rect.width < Cam_PosX && rect.height < Cam_PosY)
				{
					double st_circle_center_x = Standard_SC_RECT[0].m_resultX;
					double st_circle_center_y = Standard_SC_RECT[0].m_resultY;
					double curr_circle_center_x = rect.x + rect.width/2;
					double curr_circle_center_y = rect.y + rect.height/2;

					double dist = GetDistance(st_circle_center_x, st_circle_center_y, curr_circle_center_x, curr_circle_center_y);
					
					if(dist < old_dist && dist < 120)
					{
						SC_RECT[0].m_resultX = curr_circle_center_x;
						SC_RECT[0].m_resultY = curr_circle_center_y;

						old_dist = dist;

						SC_RECT[0].m_Success = TRUE;
					}
				}
			}
			else
			{
		//		cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8);  
				counter++;
			}
		}		
	}
	
	int total_circle_count = counter;
	CvRect *rectArray = new CvRect[counter];
	counter = 0;
	
	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);		
	
		double circularity = (4.0*3.14*area)/(arcCount*arcCount);		

		if(circularity > 0.5)
		{			
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;		

			//if(center_x > 270 && center_x < 450 && center_y > 160 && center_y < 320)
			if(center_x > (int)(Cam_PosX*0.75) && center_x < (int)(Cam_PosX*1.25) && center_y > (int)(Cam_PosY*0.625) && center_y < (int)(Cam_PosY*1.375))
			{
				
			}
			else
			{
				cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 0, 255), 1, 8);  
				rectArray[counter] = rect;
				counter++;
			}
		}
	}
	
	old_dist = 999999;

	for(int i=1; i<3; i++)
	{
		SC_RECT[i].m_Success = FALSE;

		for(int j=0; j<total_circle_count; j++)
		{
			rect = rectArray[j];
			
			double st_circle_center_x = Standard_SC_RECT[i].m_resultX;
			double st_circle_center_y = Standard_SC_RECT[i].m_resultY;
			double curr_circle_center_x = rect.x + rect.width/2;
			double curr_circle_center_y = rect.y + rect.height/2;

			double dist = GetDistance(st_circle_center_x, st_circle_center_y, curr_circle_center_x, curr_circle_center_y);

			if(dist < old_dist && dist < 150)
			{	
				int total_R_Value = 0;
				int total_G_Value = 0;
				int total_B_Value = 0;
				int R, G, B, R_Avg, G_Avg, B_Avg;

				for(int k=rect.y; k<rect.y+rect.height; k++)
				{
					for(int q=rect.x; q<rect.x+rect.width; q++)
					{
						B = (unsigned char)IN_RGB[k * (m_CAM_SIZE_WIDTH*4) + q*4 +  0];
						G = (unsigned char)IN_RGB[k * (m_CAM_SIZE_WIDTH*4) + q*4 +  1];
						R = (unsigned char)IN_RGB[k * (m_CAM_SIZE_WIDTH*4) + q*4 +  2];
						
						total_R_Value += R;
						total_G_Value += G;
						total_B_Value += B;
					}
				}
				
				R_Avg = total_R_Value/(rect.width*rect.height);
				G_Avg = total_G_Value/(rect.width*rect.height);
				B_Avg = total_B_Value/(rect.width*rect.height);

				if(R_Avg > G_Avg+B_Avg)
				{
					SC_RECT[i].m_resultX = curr_circle_center_x;
					SC_RECT[i].m_resultY = curr_circle_center_y;

					old_dist = dist;

					SC_RECT[i].m_Success = TRUE;
			//		cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 255), 1, 8);  
				}
				
			}
			
		}

		old_dist = 999999;
	}
	
	CvPoint pt1 = GetCenterPoint(m_RGBScanbuf);

	SC_RECT[0].m_resultX = pt1.x;
	SC_RECT[0].m_resultY = pt1.y;

	for(int i=1; i<3; i++)
	{
		if(SC_RECT[i].m_Success == FALSE)
		{
			SC_RECT[i].m_resultX = Standard_SC_RECT[i].m_resultX;
			SC_RECT[i].m_resultY = Standard_SC_RECT[i].m_resultY;
		}
	}
	
//	SC_RECT[3].m_Success = TRUE;
//	SC_RECT[4].m_Success = TRUE;

//	cvSaveImage("D:\\SideCircleResultImage.bmp", RGBResultImage);
	
	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&CannyImage);	
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);	
	
	delete []rectArray;
}

double CDistortion_Option::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt( (double)((x2-x1)*(x2-x1)) +  ((y2-y1)*(y2-y1)));

	return result;
}
void CDistortion_Option::OnNMCustomdrawListAngle(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//*pResult = 0;

	 COLORREF text_color = 0;
	 COLORREF bg_color = RGB(255, 255, 255);
     /////////////////////////////////////////////////////////////default 컬러

     LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
		
	//	lplvcd->iPartId
        switch(lplvcd->nmcd.dwDrawStage){
            case CDDS_PREPAINT:
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
             
            // 배경 혹은 텍스트를 수정한다.
            case CDDS_ITEMPREPAINT:
                // 1번째 열 빨간색, 2번째 열 녹색, 3번째 이하는 파란색의 글자색을 갖는다.
               /* if(lplvcd->nmcd.dwItemSpec == 0) text_color = RGB(255, 0, 0);
                else if(lplvcd->nmcd.dwItemSpec == 1) text_color = RGB(0, 255, 0);
                else text_color = RGB(0, 0, 255);
                lplvcd->clrText = text_color;*/
                *pResult = CDRF_NOTIFYITEMDRAW;
                return;
           
            // 서브 아이템의 배경 혹은 텍스트를 수정한다.
            case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
                if(lplvcd->iSubItem != 0){
                    // 1번째 행이라면...

					if(lplvcd->nmcd.dwItemSpec >=0 ||lplvcd->nmcd.dwItemSpec <5){
						if(AutomationMod ==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 7 ){
								text_color = RGB(0, 0, 0);
								bg_color = RGB(200, 200, 200);
							}
						}
						else{
							text_color = RGB(0, 0, 0);
							bg_color = RGB(255, 255, 255);
						
						}

						if(ChangeCheck==1){
							if(lplvcd->iSubItem >= 1 && lplvcd->iSubItem < 11 ){
								for(int t=0; t<5; t++){
									if(lplvcd->nmcd.dwItemSpec == ChangeItem[t]){
										text_color = RGB(0, 0, 0);
										bg_color = RGB(250, 230, 200);					
									}
								}
							}
						
						
						}
					
					}
					

				    lplvcd->clrText = text_color;
                    lplvcd->clrTextBk = bg_color;
					
                }
                *pResult = CDRF_NEWFONT;
                return;
        }
}


void CDistortion_Option::OnEnKillfocusEditAMod()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str = "";
	ChangeCheck=1;
	if((iSavedItem != -1)&&(iSavedSubitem != -1)){
		((CEdit *)GetDlgItemText(IDC_EDIT_A_MOD, str));
		List_COLOR_Change();
		if(A_DATALIST.GetItemText(iSavedItem, iSavedSubitem) != str){
			Change_DATA();
			UploadList();
		}
	}
	((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowText("");
	((CEdit *)GetDlgItem(IDC_EDIT_A_MOD))->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW );
	iSavedItem = -1;
	iSavedSubitem = -1;
	((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	
}

void CDistortion_Option::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(bShow == TRUE){
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = m_INFO.Number;	//SDY
	}else{
		((CImageTesterDlg *)m_pMomWnd)->m_SubRunNum = -1;
		AngleKeepRun = FALSE;
//		((CImageTesterDlg *)m_pMomWnd)->keepRun = FALSE;
		UpdateData(FALSE);
	}
}

void CDistortion_Option::OnBnClickedButtonALoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Load_parameter();
	UpdateData(FALSE);
	UploadList();
}

//void CDistortion_Option::OnBnClickedCheckAngleKeeprun()
//{
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	AngleKeepRun = btn_AngleKeepRun.GetCheck();
//	UpdateData(FALSE);
//	if(AngleKeepRun == TRUE){
//		AutomationMod =1;
//		OnBnClickedCheckAuto();
//		(CButton *)GetDlgItem(IDC_CHECK_AUTO)->EnableWindow(0);
//		CheckDlgButton(IDC_CHECK_AUTO,FALSE);
////		((CImageTesterDlg *)m_pMomWnd)->keepRun = TRUE; 
////		((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->KeepRun();
//
//	}
//	else{
////		((CImageTesterDlg *)m_pMomWnd)->keepRun = FALSE;
//		(CButton *)GetDlgItem(IDC_CHECK_AUTO)->EnableWindow(1);
//	}
//	
//}

void CDistortion_Option::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CDistortion_Option::OnKillFocus(CWnd* pNewWnd)
{
	CDialog::OnKillFocus(pNewWnd);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	
}



BOOL CDistortion_Option::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	int znum = zDelta/120;
	CWnd *focusid;
	focusid = GetFocus();
	bool FLAG = FALSE;
	if(znum > 0 ){
		FLAG = 1;
	}
	else if(znum < 0 ){
		FLAG =0;
	}
	else if(znum == 0){
		return CDialog::OnMouseWheel(nFlags, zDelta, pt);
	}
	int casenum = focusid->GetDlgCtrlID();
	switch(casenum){
		case IDC_Angle_PosX   :
			EditWheelIntSet(IDC_Angle_PosX ,znum);
			OnEnChangeAnglePosx();
			break;
		case IDC_Angle_PosY :
			EditWheelIntSet(IDC_Angle_PosY,znum);
			OnEnChangeAnglePosy();
			break;
		case IDC_Angle_Dis_W :
			EditWheelIntSet(IDC_Angle_Dis_W ,znum);
			OnEnChangeAngleDisW();
			break;
		case IDC_Angle_Dis_H :
			EditWheelIntSet(IDC_Angle_Dis_H,znum);
			OnEnChangeAngleDisH();
			break;
		case  IDC_Angle_Width :
			EditWheelIntSet( IDC_Angle_Width,znum);
			OnEnChangeAngleWidth();
			break;
		case IDC_Angle_Height :
			EditWheelIntSet(IDC_Angle_Height,znum);
			OnEnChangeAngleHeight();
			break;
	
		
		case IDC_EDIT_A_MOD :
			if(FLAG == 1){
				if((Change_DATA_CHECK(1) == TRUE) /*|| (znum ==-1)(WheelCheck ==0)*/){
					EditWheelIntSet(IDC_EDIT_A_MOD,znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditAMod();
					//WheelCheck =1;
				}
			}
			else if(FLAG == 0){
				if((Change_DATA_CHECK(0) == TRUE)/* || (znum == 1)(WheelCheck ==1)*/){
					EditWheelIntSet(IDC_EDIT_A_MOD,znum);
					Change_DATA();
					UploadList();
					OnEnChangeEditAMod();
					//WheelCheck = 0;
				}
			}
			

			break;
	}
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

void CDistortion_Option::EditWheelIntSet(int nID,int Val)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = 0;
	}else{
		buf = GetDlgItemInt(nID);
		buf+= Val;
	}
	SetDlgItemInt(nID,buf,1);
	((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
}
void CDistortion_Option::EditMinMaxIntSet(int nID,int* Val,int Min,int Max)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = Min;
		retval = FALSE;
	}else{
		buf = GetDlgItemInt(nID);
		if(buf<Min){
			buf = *Val;
			retval = TRUE;
		}else if(buf > Max){
			buf = *Val;
			retval = TRUE;
		}else{
			retval = FALSE;
		}
	}
	*Val = buf;
	if(retval == TRUE){
		SetDlgItemInt(nID,buf,1);
		((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
	}
}

void CDistortion_Option::EditMinMaxdoubleSet(int nID,double* Val,double Min,double Max)//가능하다면 나중에 전역변수화 시킨다. 
{
	CString str_buf = "";
	int buf = 0;
	int retval = 0;
	GetDlgItemText(nID,str_buf);
	str_buf.Remove(' ');
	if(str_buf == ""){
		buf = Min;
		retval = FALSE;
	}else{
		buf = GetDlgItemInt(nID);
		if(buf<Min){
			buf = *Val;
			retval = TRUE;
		}else if(buf > Max){
			buf = *Val;
			retval = TRUE;
		}else{
			retval = FALSE;
		}
	}
	*Val = buf;
	if(retval == TRUE){
		SetDlgItemInt(nID,buf,1);
		((CEdit *)GetDlgItem(nID))->SetSel(-2, -1);
	}
}

void CDistortion_Option::OnEnChangeAnglePosx()
{
	
	EditMinMaxIntSet(IDC_Angle_PosX, &A_Total_PosX,0,m_CAM_SIZE_WIDTH);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);

	if((A_Total_PosX >= 0)&&(A_Total_PosX <= m_CAM_SIZE_WIDTH)&&
		(A_Total_PosY >= 0)&&(A_Total_PosY <= m_CAM_SIZE_HEIGHT)&&
		(A_Dis_Width>=0)&&(A_Dis_Width<=m_CAM_SIZE_WIDTH)&&
		(A_Dis_Height>=0)&&(A_Dis_Height<=m_CAM_SIZE_HEIGHT)&&
		(A_Total_Width>=0)&&(A_Total_Width<=m_CAM_SIZE_WIDTH)&&
		(A_Total_Height>=0)&&(A_Total_Height<=m_CAM_SIZE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CDistortion_Option::OnEnChangeAnglePosy()
{

	EditMinMaxIntSet(IDC_Angle_PosY, &A_Total_PosY,0,m_CAM_SIZE_HEIGHT);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);

	if((A_Total_PosX >= 0)&&(A_Total_PosX <= m_CAM_SIZE_WIDTH)&&
		(A_Total_PosY >= 0)&&(A_Total_PosY <= m_CAM_SIZE_HEIGHT)&&
		(A_Dis_Width>=0)&&(A_Dis_Width<=m_CAM_SIZE_WIDTH)&&
		(A_Dis_Height>=0)&&(A_Dis_Height<=m_CAM_SIZE_HEIGHT)&&
		(A_Total_Width>=0)&&(A_Total_Width<=m_CAM_SIZE_WIDTH)&&
		(A_Total_Height>=0)&&(A_Total_Height<=m_CAM_SIZE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CDistortion_Option::OnEnChangeAngleDisW()
{

	EditMinMaxIntSet(IDC_Angle_Dis_W, &A_Dis_Width,0,m_CAM_SIZE_WIDTH);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);


	if((A_Total_PosX >= 0)&&(A_Total_PosX <= m_CAM_SIZE_WIDTH)&&
		(A_Total_PosY >= 0)&&(A_Total_PosY <= m_CAM_SIZE_HEIGHT)&&
		(A_Dis_Width>=0)&&(A_Dis_Width<=m_CAM_SIZE_WIDTH)&&
		(A_Dis_Height>=0)&&(A_Dis_Height<=m_CAM_SIZE_HEIGHT)&&
		(A_Total_Width>=0)&&(A_Total_Width<=m_CAM_SIZE_WIDTH)&&
		(A_Total_Height>=0)&&(A_Total_Height<=m_CAM_SIZE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CDistortion_Option::OnEnChangeAngleDisH()
{
	
	EditMinMaxIntSet(IDC_Angle_Dis_H, &A_Dis_Height,0,m_CAM_SIZE_HEIGHT);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);

	if((A_Total_PosX >= 0)&&(A_Total_PosX <= m_CAM_SIZE_WIDTH)&&
		(A_Total_PosY >= 0)&&(A_Total_PosY <= m_CAM_SIZE_HEIGHT)&&
		(A_Dis_Width>=0)&&(A_Dis_Width<=m_CAM_SIZE_WIDTH)&&
		(A_Dis_Height>=0)&&(A_Dis_Height<=m_CAM_SIZE_HEIGHT)&&
		(A_Total_Width>=0)&&(A_Total_Width<=m_CAM_SIZE_WIDTH)&&
		(A_Total_Height>=0)&&(A_Total_Height<=m_CAM_SIZE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CDistortion_Option::OnEnChangeAngleWidth()
{

	EditMinMaxIntSet(IDC_Angle_Width, &A_Total_Width,0,m_CAM_SIZE_WIDTH);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);
	CString str;


	if((A_Total_PosX >= 0)&&(A_Total_PosX <= m_CAM_SIZE_WIDTH)&&
		(A_Total_PosY >= 0)&&(A_Total_PosY <= m_CAM_SIZE_HEIGHT)&&
		(A_Dis_Width>=0)&&(A_Dis_Width<=m_CAM_SIZE_WIDTH)&&
		(A_Dis_Height>=0)&&(A_Dis_Height<=m_CAM_SIZE_HEIGHT)&&
		(A_Total_Width>=0)&&(A_Total_Width<=m_CAM_SIZE_WIDTH)&&
		(A_Total_Height>=0)&&(A_Total_Height<=m_CAM_SIZE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CDistortion_Option::OnEnChangeAngleHeight()
{

	EditMinMaxIntSet(IDC_Angle_Height, &A_Total_Height,0,m_CAM_SIZE_HEIGHT);
	A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	A_Total_Height = GetDlgItemInt(IDC_Angle_Height);
	CString str;
	if((A_Total_PosX >= 0)&&(A_Total_PosX <= m_CAM_SIZE_WIDTH)&&
		(A_Total_PosY >= 0)&&(A_Total_PosY <= m_CAM_SIZE_HEIGHT)&&
		(A_Dis_Width>=0)&&(A_Dis_Width<=m_CAM_SIZE_WIDTH)&&
		(A_Dis_Height>=0)&&(A_Dis_Height<=m_CAM_SIZE_HEIGHT)&&
		(A_Total_Width>=0)&&(A_Total_Width<=m_CAM_SIZE_WIDTH)&&
		(A_Total_Height>=0)&&(A_Total_Height<=m_CAM_SIZE_HEIGHT)){
	/*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
			((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
		}else{*/
			((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
		//}

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	}
}

void CDistortion_Option::OnEnChangeAngleThresold()
{
	
	//EditMinMaxdoubleSet(IDC_Angle_Thresold, &A_Thresold,0,200);
	//A_Total_PosX = GetDlgItemInt(IDC_Angle_PosX);	
	//A_Total_PosY = GetDlgItemInt(IDC_Angle_PosY);
	//A_Dis_Width	= GetDlgItemInt( IDC_Angle_Dis_W );
	//A_Dis_Height = GetDlgItemInt( IDC_Angle_Dis_H);
	//A_Total_Width = GetDlgItemInt(IDC_Angle_Width);
	//A_Total_Height = GetDlgItemInt(IDC_Angle_Height);
	//CString str;
	//GetDlgItemText(IDC_Angle_Thresold,str);
	//A_Thresold = atof(str);

	//if((A_Total_PosX >= 0)&&(A_Total_PosX <= m_CAM_SIZE_WIDTH)&&
	//	(A_Total_PosY >= 0)&&(A_Total_PosY <= m_CAM_SIZE_HEIGHT)&&
	//	(A_Dis_Width>=0)&&(A_Dis_Width<=m_CAM_SIZE_WIDTH)&&
	//	(A_Dis_Height>=0)&&(A_Dis_Height<=m_CAM_SIZE_HEIGHT)&&
	//	(A_Total_Width>=0)&&(A_Total_Width<=m_CAM_SIZE_WIDTH)&&
	//	(A_Total_Height>=0)&&(A_Total_Height<=m_CAM_SIZE_HEIGHT)){
	///*	if((C_RECT[0].m_PosX == m_PosX)&&(C_RECT[0].m_PosY == m_PosY)&&(C_RECT[0].font == m_FontSize)){
	//		((CButton *)GetDlgItem(IDC_BUTTON_BRIGHTNESS_SETZONE))->EnableWindow(0);
	//	}else{*/
	//		((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(1);
	//	//}

	//}else{
	//	((CButton *)GetDlgItem(IDC_BUTTON_setAngleZone))->EnableWindow(0);
	//}
}

void CDistortion_Option::OnEnChangeEditAMod()
{
	
	int num = iSavedItem;
	if((A_RECT[num].m_PosX >= 0)&&(A_RECT[num].m_PosX <= m_CAM_SIZE_WIDTH)&&
		(A_RECT[num].m_PosY >= 0)&&(A_RECT[num].m_PosY <= m_CAM_SIZE_HEIGHT)&&
		(A_RECT[num].m_Left>=0)&&(A_RECT[num].m_Left<=m_CAM_SIZE_WIDTH)&&
		(A_RECT[num].m_Top>=0)&&(A_RECT[num].m_Top<=m_CAM_SIZE_HEIGHT)&&
		(A_RECT[num].m_Right>=0)&&(A_RECT[num].m_Right<=m_CAM_SIZE_WIDTH)&&
		(A_RECT[num].m_Bottom>=0)&&(A_RECT[num].m_Bottom<=m_CAM_SIZE_HEIGHT)&&
		(A_RECT[num].m_Width>=0)&&(A_RECT[num].m_Width<=m_CAM_SIZE_WIDTH)&&
		(A_RECT[num].m_Height>=0)&&(A_RECT[num].m_Height<=m_CAM_SIZE_HEIGHT)){

			((CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE))->EnableWindow(1);
		

	}else{
		((CButton *)GetDlgItem(IDC_BUTTON_A_RECT_SAVE))->EnableWindow(0);
	}
}

void CDistortion_Option::OnLvnItemchangedListAngle(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}

CvPoint CDistortion_Option::GetCenterPoint(LPBYTE IN_RGB)
{	

	double Cam_PosX = (m_CAM_SIZE_WIDTH/2);
	double Cam_PosY = (m_CAM_SIZE_HEIGHT/2);

	CvPoint resultPt = cvPoint(-99999, -99999);

	IplImage *OriginImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *PreprecessedImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *CannyImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 1);
	
	IplImage *RGBOrgImage = cvCreateImage(cvSize(m_CAM_SIZE_WIDTH, m_CAM_SIZE_HEIGHT), IPL_DEPTH_8U, 3);

	BYTE R,G,B;
	double Sum_Y;

	for (int y=0; y<m_CAM_SIZE_HEIGHT; y++)
	{
		for (int x=0; x<m_CAM_SIZE_WIDTH; x++)
		{
		/*	B = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(CAM_IMAGE_WIDTH*4) + x*4 + 2];
			
			Sum_Y = (BYTE)((0.29900*R)+(0.58700*G)+(0.11400*B));
			
			OriginImage->imageData[y*OriginImage->widthStep+x] = Sum_Y;		*/

			B = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4    ];
			G = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 1];
			R = IN_RGB[y*(m_CAM_SIZE_WIDTH*4) + x*4 + 2];

			if( R < 50 && G < 50 && B < 50)
				OriginImage->imageData[y*OriginImage->widthStep+x] = 255;
			else
				OriginImage->imageData[y*OriginImage->widthStep+x] = 0;

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}	
	
//	cvSaveImage("C:\\CenterImage.bmp", OriginImage);
//	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
//	cvSmooth(SmoothImage, SmoothImage, CV_GAUSSIAN, 3, 0, 0, 0);
	
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);

	cvCopyImage(OriginImage, SmoothImage);
	
	cvCanny(SmoothImage, CannyImage, 0, 255);

//	cvDilate(CannyImage, DilateImage);
	
	cvCopyImage(CannyImage, temp_PatternImage);
	
	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);
	
//	cvSaveImage("C:\\CenterImage.bmp", CannyImage);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	
	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);	
	
	temp_contour = contour;
	
	int counter = 0;

	for( ; temp_contour != 0; temp_contour=temp_contour->h_next)
		counter++;
	
	if(counter == 0)
		return resultPt;

	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area=0, arcCount=0;
	counter = 0;
	double old_dist = 999999;
	
	int old_center_pos_x = 0;
	int old_center_pos_y = 0;
	int center_pt_x = 0;
	int center_pt_y = 0;
	int obj_Cnt = 0;
	int size=0, old_size = 0;
	
	for( ; contour != 0; contour=contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;

		double circularity = (4.0*3.14*area)/(arcCount*arcCount);
		
		if(circularity > 0.7)
		{		
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width/2;
			center_y = rect.y + rect.height/2;

			//if(center_x > 270 && center_x < 450 && center_y > 160 && center_y < 320)
			if(center_x > (int)(Cam_PosX*0.75) && center_x < (int)(Cam_PosX*1.25) && center_y > (int)(Cam_PosY*0.625) && center_y < (int)(Cam_PosY*1.375))
			{
				if(rect.width < Cam_PosX && rect.height < Cam_PosY && rect.width > 20 && rect.height > 20)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(0, 255, 0), 1, 8);  
					
					/*center_pt_x = center_pt_x+ center_x;
					center_pt_y = center_pt_y+ center_y;
					obj_Cnt++;

					old_center_pos_x = center_pt_x/obj_Cnt;
					old_center_pos_y = center_pt_y/obj_Cnt;*/
					
					double distance = GetDistance(rect.x+rect.width/2, rect.y+rect.height/2, Cam_PosX, Cam_PosY);

					obj_Cnt++;
					
					size = rect.width * rect.height;
					if(distance < old_dist)
					{
						old_size = size;
						resultPt.x = center_x;
						resultPt.y = center_y;

						old_dist = distance;
					}

				//	cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width, rect.y+rect.height), CV_RGB(255, 0, 0), 1, 8);  
				
				}
			}
		}
		
		counter++;
	}
//	cvSaveImage("D:\\RGBResultImage.bmp", RGBResultImage);
	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
	cvReleaseImage(&RGBOrgImage);

	delete []rectArray;
	delete []areaArray;
	
	/*if(obj_Cnt != 0)
	{
		resultPt.x = center_pt_x / obj_Cnt;
		resultPt.y = center_pt_y / obj_Cnt;
	}*/
/*	if(obj_Cnt != 0)
	{
		resultPt.x = center_pt_x;
		resultPt.y = center_pt_y;
	}*/
		
	return resultPt;	
}
void CDistortion_Option::OnBnClickedButtontest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(0);
	b_EtcModel_TEST =1;
	((CImageTesterDlg *)m_pMomWnd)->m_psubmodel[m_INFO.Number]->OnBnClickedBtnRun();
	b_EtcModel_TEST =0;

	((CButton *)GetDlgItem(IDC_BUTTON_test))->EnableWindow(1);

}

void CDistortion_Option::OnBnClickedButtonSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString str="";
	
	//double d = atan2(1.0,1.0);
	/*double d = atan(1.0/sqrt(3.0))*180/3.141592;
	str.Format("%f",d);
	AfxMessageBox(str);*/

		
	d_Focallength = atof(str_focallength);
	d_Campixel = atof(str_camPixel);

	i_ChartDis = atoi(str_ChartDistance);

	if(i_ChartDis <0){
		i_ChartDis =0;
	}
	if(i_ChartDis > 1000){
		i_ChartDis = 1000;
	}

	if(d_Focallength < 0){
		d_Focallength =0;
	}
	if(d_Focallength > 30){
		d_Focallength =30;
	}

	if(d_Campixel < 0){
		d_Campixel =0;
	}
	if(d_Campixel > 20){
		d_Campixel =20;
	}

	str_ChartDistance.Format("%d",i_ChartDis);
	str_focallength.Format("%6.3f",d_Focallength);
	str_camPixel.Format("%6.3f",d_Campixel);

	UpdateData(FALSE);
	CString strTitle;
	strTitle.Empty();
	strTitle="DISTORTION_INIT";
	str.Empty();
	str.Format("%d",i_ChartDis);
	WritePrivateProfileString(str_model,strTitle+"CHARTDIS",str,A_filename);

	str.Empty();
	str.Format("%6.3f",d_Focallength);
	WritePrivateProfileString(str_model,strTitle+"FOCALLENGTH",str,A_filename);
	str.Empty();
	str.Format("%6.3f",d_Campixel);
	WritePrivateProfileString(str_model,strTitle+"CAMPIXEL",str,A_filename);


}

void CDistortion_Option::OnEnChangeEditCamFc()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_Option::OnEnChangeEditCamPixel()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_Option::OnEnChangeEditChartDis()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_Option::OnEnChangeEditDistortion()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_Option::OnEnChangeEditDevDistortion()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDistortion_Option::OnBnClickedButtonSaveDistortion()
{
	UpdateData(TRUE);

	CString str;
	d_Distortion_Master = atof(str_Distortion);
	d_Dev_Distortion =atof(str_DEV_Distortion);
	m_dOffset = atof(str_DistortionOffset);

	m_dDistortionScale = atof(m_strDistortionScale);

	str_Distortion.Format("%6.4f",d_Distortion_Master);
	str_DEV_Distortion.Format("%6.4f",d_Dev_Distortion);
	str_DistortionOffset.Format("%6.4f", m_dOffset);
	m_strDistortionScale.Format(_T("%6.4f"), m_dDistortionScale);

	CString strTitle;
	strTitle.Empty();
	strTitle="DISTORTION_INIT";
	str.Empty();
	str.Format("%6.4f",d_Distortion_Master);
	WritePrivateProfileString(str_model,strTitle+"DISTORTION",str,A_filename);

	str.Empty();
	str.Format("%6.4f",d_Dev_Distortion);
	WritePrivateProfileString(str_model,strTitle+"DISTORTION_DEV",str,A_filename);

	str.Empty();
	str.Format("%6.4f", m_dOffset);
	WritePrivateProfileString(str_model, strTitle + "OFFSET", str, A_filename);

	str.Empty();
	str.Format("%6.4f", m_dDistortionScale);
	WritePrivateProfileString(str_model, strTitle + "SCALE", str, A_filename);
}

void CDistortion_Option::OnEnChangeEditCamDistance()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.
	UpdateData(TRUE);
	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


#pragma region LOT관련 함수
void CDistortion_Option::LOT_Set_List(CListCtrl *List){

	while(List->DeleteColumn(0));
	((CImageTesterDlg *)m_pMomWnd)->m_pWorkList->DeleteAllItems();

	List->SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List->InsertColumn(0,"NO",LVCFMT_CENTER, 30);
	List->InsertColumn(1,"모델명",LVCFMT_CENTER, 100);
	List->InsertColumn(2,"LOT 명",LVCFMT_CENTER, 80);
	List->InsertColumn(3,"Start DATE",LVCFMT_CENTER, 80);
	List->InsertColumn(4,"Start TIME",LVCFMT_CENTER, 80);	
	List->InsertColumn(5,"왜곡률",LVCFMT_CENTER, 80);
	List->InsertColumn(6,"RESULT",LVCFMT_CENTER, 80);
	List->InsertColumn(7,"비고",LVCFMT_CENTER, 80);
	Lot_InsertNum =8;
	//Copy_List(&m_Lot_DistortionList, ((CImageTesterDlg *)m_pMomWnd)->m_pWorkList, Lot_InsertNum);
}

void CDistortion_Option::LOT_InsertDataList(){
	CString strCnt="";

	int Index = m_Lot_DistortionList.InsertItem(Lot_StartCnt, "", 0);

	strCnt.Empty();
	strCnt.Format(_T("%d"), Index + 1);
	m_Lot_DistortionList.SetItemText(Index, 0, strCnt);

	int CopyIndex = m_AngleList.GetItemCount() - 1;

	int count = 0;
	for (int t = 0; t < Lot_InsertNum; t++){
		if ((t != 2) && (t != 0)){
			m_Lot_DistortionList.SetItemText(Index, t, m_AngleList.GetItemText(CopyIndex, count));
			count++;

		}
		else if (t == 0){
			count++;
		}
		else{
			m_Lot_DistortionList.SetItemText(Index, t, ((CImageTesterDlg *)m_pMomWnd)->LOT_NUMBER);
		}
	}

	Lot_StartCnt++;

}
void CDistortion_Option::LOT_EXCEL_SAVE(){
	CString Item[1]={"왜곡률"};
	CString Data ="";
	CString str="";
	str = "***********************Distortion***********************\n";
	Data += str;
	str.Empty();
	str.Format("좌- 중점 , %6.3f º,Threshold, ± %6.3f º,, ",A_RECT[1].m_resultDegree,A_RECT[1].d_Thresold);/////////////////수정해야함.
	Data += str;
	str.Empty();
	str.Format("우- 중점 , %6.3f º,Threshold, ± %6.3f º, \n",A_RECT[2].m_resultDegree,A_RECT[2].d_Thresold);
	Data += str;
	str.Empty();
	str.Format("상- 중점 , %6.3f º,Threshold, ± %6.3f º, ,",A_RECT[3].m_resultDegree,A_RECT[3].d_Thresold);
	Data += str;
	str.Empty();
	str.Format("하- 중점 , %6.3f º ,Threshold, ± %6.3f º,\n ",A_RECT[4].m_resultDegree,A_RECT[4].d_Thresold);
	Data += str;
	str.Empty();
	str = "*************************************************\n\n";
	Data += str;
	((CImageTesterDlg *)m_pMomWnd)->LOT_SAVE_EXCEL("Distortion",Item,1,&m_Lot_DistortionList,Data);
}

bool CDistortion_Option::LOT_EXCEL_UPLOAD(){

	m_Lot_DistortionList.DeleteAllItems();
	if(((CImageTesterDlg *)m_pMomWnd)->LOT_EXCEL_UPLOAD("Distortion",&m_Lot_DistortionList,1,&Lot_StartCnt)==TRUE){
		return TRUE;	
	}
	else{
		Lot_StartCnt = 0;
		return FALSE;	
	}
	return TRUE;
}

#pragma endregion 

void CDistortion_Option::OnEnChangeEditDistortionOffset()
{
	UpdateData(TRUE);
}


void CDistortion_Option::OnEnChangeEditDistortDist()
{
	int buf = GetDlgItemInt(IDC_EDIT_DISTORT_DIST);
	if ((buf >= 135) && (buf <= 615)){
		if ((m_dDistortionDistance == buf)){
			((CButton *)GetDlgItem(IDC_BTN_DISTORTION_MOVE_SAVE))->EnableWindow(0);
		}
		else{
			((CButton *)GetDlgItem(IDC_BTN_DISTORTION_MOVE_SAVE))->EnableWindow(1);
		}
		((CButton *)GetDlgItem(IDC_BTN_CHART_TESTMOVE2))->EnableWindow(1);
	}
	else{
		((CButton *)GetDlgItem(IDC_BTN_DISTORTION_MOVE_SAVE))->EnableWindow(0);
		((CButton *)GetDlgItem(IDC_BTN_CHART_TESTMOVE2))->EnableWindow(0);
	}
}


void CDistortion_Option::OnBnClickedBtnDistortionMoveSave()
{
	m_dDistortionDistance = GetDlgItemInt(IDC_EDIT_DISTORT_DIST);

	if (m_dDistortionDistance <= 135){
		m_dDistortionDistance = 135;
	}
	else if (m_dDistortionDistance >= 615){
		m_dDistortionDistance = 615;
	}
	CString str = "";
	str.Empty();
	str.Format("%d", m_dDistortionDistance);
	((CImageTesterDlg *)m_pMomWnd)->m_dDistortDistance = m_dDistortionDistance;

	if (((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd != NULL){
		((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->i_MotorDis = m_dDistortionDistance;
		((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->str_MotorDis.Format("%d", m_dDistortionDistance);
		((CImageTesterDlg *)m_pMomWnd)->m_pDistortionOptWnd->UpdateData(FALSE);
	}
	WritePrivateProfileString(str_model, "DISTORTION_DISTANCE", str, A_filename);
	((CButton *)GetDlgItem(IDC_BTN_DISTORTION_MOVE_SAVE))->EnableWindow(0);
}


void CDistortion_Option::OnBnClickedBtnChartTestmove2()
{
	TESTMOVEBTN_Enable(0);
	int m_tstdata = GetDlgItemInt(IDC_EDIT_DISTORT_DIST);

	if (m_tstdata <= 135){
		m_tstdata = 135;
	}

	if (m_tstdata >= 615){
		m_tstdata = 615;
	}

	((CImageTesterDlg *)m_pMomWnd)->Move_Chart(m_tstdata);
	TESTMOVEBTN_Enable(1);
}
void CDistortion_Option::TESTMOVEBTN_Enable(bool MODE){

	(CButton *)GetDlgItem(IDC_BTN_CHART_TESTMOVE2)->EnableWindow(MODE);
	(CButton *)GetDlgItem(IDC_BTN_DISTORTION_MOVE_SAVE)->EnableWindow(MODE);
	(CButton *)GetDlgItem(IDC_BUTTON_test)->EnableWindow(MODE);

}