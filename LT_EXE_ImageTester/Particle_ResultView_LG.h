#pragma once


// CParticle_ResultView_LG 대화 상자입니다.

class CParticle_ResultView_LG : public CDialog
{
	DECLARE_DYNAMIC(CParticle_ResultView_LG)

public:
	CParticle_ResultView_LG(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CParticle_ResultView_LG();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_RESULT_PARTICLE_LG };
	CFont ft1;
	COLORREF txcol_TVal,bkcol_TVal;
	COLORREF txcol_TVal2,bkcol_TVal2;
	COLORREF txcol_TVal3,bkcol_TVal3;
	void	PARTICLE_TEXT(LPCSTR lpcszString, ...);
	void	PARTICLENUM_TEXT(LPCSTR lpcszString, ...);
	void	RESULT_TEXT(unsigned char col,LPCSTR lpcszString, ...);

	void	PARTICLE_TEXT2(LPCSTR lpcszString, ...);
	void	PARTICLENUM_TEXT2(LPCSTR lpcszString, ...);
	void	RESULT_TEXT2(unsigned char col,LPCSTR lpcszString, ...);

	void	PARTICLE_TEXT3(LPCSTR lpcszString, ...);
	void	PARTICLENUM_TEXT3(LPCSTR lpcszString, ...);
	void	RESULT_TEXT3(unsigned char col,LPCSTR lpcszString, ...);

	void	Setup(CWnd* IN_pMomWnd);
void	Initstat();
private :
	CWnd	*m_pMomWnd;	
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnSetfocusEditParticletxt();
	afx_msg void OnEnSetfocusEditParticlenum();
	afx_msg void OnEnSetfocusEditParticleresult();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnEnSetfocusEditParticletxt2();
	afx_msg void OnEnSetfocusEditParticlenum2();
	afx_msg void OnEnSetfocusEditParticleresult2();
	afx_msg void OnEnSetfocusEditParticletxt3();
	afx_msg void OnEnSetfocusEditParticlenum3();
	afx_msg void OnEnSetfocusEditParticleresult3();
};
