#pragma once
#include "afxcmn.h"


// COption_Verify 대화 상자입니다.

class COption_Verify : public CDialog
{
	DECLARE_DYNAMIC(COption_Verify)

public:
	COption_Verify(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COption_Verify();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTION_VERIFY };

	void	BinFileLoad(bool Mode);
	COLORREF	bkcol_Stat,txcol_Stat;
	BYTE	BINBUF[1024*256*16];
	BYTE	CAMBUF[1024*256*16];

	unsigned long m_BinSIZE;
	void	SET_LIST(void);
	int MaxByte;
	COLORREF tx_st_col,bk_st_col;
	void	TEST_STATE(int col,LPCSTR lpcszString, ...);
	CFont	ft,ft2;
	void	Font_Size_Change(int nID,CFont *font,LONG Weight,LONG Height);
	void	Save_parameter();
	void	Load_parameter();
	CString  Oldstr_binFile_path;
	void	InitPrm();
	tResultVal Run(void);

	void	Setup(CWnd* IN_pMomWnd,CEdit	*pTEDIT,tINFO INFO);
private:
	CWnd		*m_pMomWnd;
	CEdit		*pTStat;
	tINFO		m_INFO;
	CString		str_model;
	CString		strTitle;
	CString		str_ModelPath;
	CString		str_BinPath;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonBinOpen();
	CString str_binFile_path;
	BOOL b_Check_ViewList;
	CListCtrl m_ctrlBINDataList;
	CProgressCtrl m_ProgressFileRead;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedCheckViewList();
	CString m_MaxByte;
	afx_msg void OnBnClickedButtonSave();
	CProgressCtrl m_ProgressTest;
	afx_msg void OnBnClickedButtontest();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CString SAVEFILE_PATH;
	afx_msg void OnBnClickedButtonBinOpen2();
};
